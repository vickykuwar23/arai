<?php if( !defined('BASEPATH')) exit('No direct script access alloed');
	
	class Master_model extends CI_Model
	{
		function __construct()
		{
			parent::__construct(); // construct the Model class
		}
		/*
			# function getRecordCount($tbl_name,$condition=FALSE)
			# * indicates paramenter is must
			# Use : 
			1) return number of rows
			# Parameters : 
			$tbl_name* =name of table 
			$condition=array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
			
			# How to call:
			$this->master_model->getRecordCount('tbl_name',$condition_array);
		*/
		
		public function getRecordCount($tbl_name,$condition=FALSE)
		{
			if(is_array($condition))
			{
				if($condition!="" && count($condition)>0)
				{
					foreach($condition as $key=>$val)
					{
						$this->db->where($key,$val);
					}
				}
			}
			else if(!is_array($condition) && $condition!="")
			{$this->db->where($condition);}
			
			$num=$this->db->count_all_results($tbl_name);
			return $num;
		}
		
		/*
			# function getRecords($tbl_name,$condition=FALSE,$select=FALSE,$order_by=FALSE,$limit=FALSE,$start=FALSE)
			# * indicates paramenter is must
			# Use : 
			1) return array of records from table
			# Parameters : 
			1) $tbl_name* =name of table 
			2) $condition=array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
			3) $select=('col1,col2,col3');
			4) $order_by=array('colname1'=>order,'colname2'=>order); Order='ASC OR DESC'
			5) $limit= limit for paging
			6) $start=start for paging
			
			# How to call:
			$this->master_model->getRecords('tbl_name',$condition_array,$select,...);
			
			# In case where we need joins, you can pass joins in controller also.
			Ex: 
			$this->db->join('tbl_nameB','tbl_nameA.col=tbl_nameB.col','left');
			$this->master_model->getRecords('tbl_name',$condition_array,$select,...);
			
			# Instruction 
			1) check number of counts in controller or where you are displying records
			
		*/
		
		public function getRecords($tbl_name,$condition=FALSE,$select=FALSE,$order_by=FALSE,$start=FALSE,$limit=FALSE)
		{
			if($select!="")
			{$this->db->select($select);}
			
			if($condition && count($condition)>0 && $condition!="")
			{
				$condition=$condition;
			}
			else
			{$condition=array();}
			if($order_by && count($order_by)>0 && $order_by!="")
			{
				foreach($order_by as $key=>$val)
				{$this->db->order_by($key,$val);}
			}
			if($limit!="" || $start!="")
			{
				$this->db->limit($limit,$start);
			}
			$rst=$this->db->get_where($tbl_name,$condition);
			return $rst->result_array();
		}
		
		public function getRecordsWithLeftJoin($tbl_name1, $table_name2, $first_table_column, $second_table_column, $where = '', $groupby = '', $orderby = '')
		{
			$sql = "Select * from $tbl_name1 a Left join $table_name2 b on a.".$first_table_column." = b.".$second_table_column;
			if($where){
				$sql .= " where $where";
			}
			if($groupby){
				$sql .= " group by $groupby";	
			}
			if($orderby){
				$sql .= " order by $orderby";	
			}
			//echo $sql; die;
			$rst=$this->db->query($sql);
			return $rst->result_array();
		}
		
		public function insertRecord($tbl_name,$data_array,$insert_id=TRUE)
		{
			if($this->db->insert($tbl_name,$data_array))
			{
				if($insert_id==true)
				{return $this->db->insert_id();}
				else
				{return true;}
			}
			else
			{return false;}
		}
		
		/*
			# function updateRecord($tbl_name,$data_array,$pri_col,$id)
			# * indicates paramenter is must
			# Use : 
			1) updates record, on successful updates return true.
			# Parameters : 
			1) $tbl_name* = name of table 
			2) $data_array* = array('column_name1'=>$column_val1,'column_name2'=>$column_val2);
			3) $pri_col* = primary key or column name depending on which update query need to fire.
			4) $id* = primary column or condition column value.
			
			# How to call:
			$this->master_model->updateRecord('tbl_name',$data_array,$pri_col,$id)
		*/
		public function updateRecord($tbl_name,$data_array,$where_arr)
		{
			$this->db->where($where_arr,NULL,FALSE);
			if($this->db->update($tbl_name,$data_array))
			
			
			{return true;}
			else
			{return false;}
		}
		
		
		/*
			# function deleteRecord($tbl_name,$pri_col,$id)
			# * indicates paramenter is must
			# Use : 
			1) delete record from table, on successful deletion returns true.
			# Parameters : 
			1) $tbl_name* = name of table 
			2) $pri_col* = primary key or column name depending on which update query need to fire.
			3) $id* = primary column or condition column value.
			
			# How to call:
			$this->master_model->deleteRecord('tbl_name','pri_col',$id)
			# It will useful while deleting record from  single table. delete join will not work here.
		*/
		public function deleteRecord($tbl_name,$pri_col,$id)
		{
			$this->db->where($pri_col,$id);
			if($this->db->delete($tbl_name))
			{return true;}
			else
			{return false;}
		}
		
		/* 
			# function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
			# * indicates paramenter is must
			# Use : 
			1) create thumb of uploaded image.
			# Parameters : 
			1) $file_name* = name of uploaded file 
			2) $path* = path of directory
			3) $width* = width of thumb
			4) $height* = height of thumb
			5) $maintain_ratio = if need to maintain ration of original image then pass true, in this case thumb may vary in
			height and width provided. default it is FALSE.
			
			# How to call:
			$this->master_model->createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
			
			# !!Important : thumb foler  name must be 'thumb'
		*/
		public function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
		{
			$config_1['image_library']='gd2';
			$config_1['source_image']= $path.$file_name;	
			$config_1['create_thumb']=TRUE;
			$config_1['maintain_ratio']=$maintain_ratio;
			$config_1['thumb_marker']='';
			$config_1['new_image']=$path."thumb/".$file_name; 
			$config_1['width']=$width;
			$config_1['height']=$height;
			
			$this->image_lib->initialize($config_1);
			$this->image_lib->resize();
			
			if (!$this->image_lib->resize()) {
				echo $this->image_lib->display_errors();
			}
			$this->image_lib->clear();
		}
		
		public function resize_image($file_name,$path,$width,$height,$maintain_ratio=FALSE)
		{
			//echo $file_name ;exit;
			$config_1['image_library']='gd2';
			$config_1['source_image']= $path."original/".$file_name;	
			$config_1['create_thumb']=TRUE;
			$config_1['maintain_ratio']=$maintain_ratio;
			$config_1['thumb_marker']='';
			$config_1['new_image']=$path.$file_name;
			$config_1['width']=$width;
			$config_1['height']=$height;
			
			$this->image_lib->initialize($config_1);
			$this->image_lib->resize();
			
			if (!$this->image_lib->resize()) {
				echo $this->image_lib->display_errors();
			}
			
			unlink($path."original/".$file_name); 
			$this->image_lib->clear();
		}
		
		/* create slug */
		function create_slug($phrase,$tbl_name,$slug_col,$pri_col='',$id='',$maxLength=100000000000000)
		{
			$result = strtolower($phrase);
			$result = preg_replace("/[^A-Za-z0-9\s-._\/]/", "", $result);
			$result = trim(preg_replace("/[\s-]+/", " ", $result));
			$result = trim(substr($result, 0, $maxLength));
			$result = preg_replace("/\s/", "-", $result);
			$slug=$result;
			if($id!="")
			{
				$this->db->where($pri_col.' !=' ,$id);
			}
			$rst=$this->db->get_where($tbl_name,array($slug_col=>$slug));
			
			if($rst->num_rows() > 0)
			{
				$count=$rst->num_rows()+1;
				return $slug=$slug.$count;
			}
			else
			{return $slug;}
		}
		
		public function video_image($url)
		{
			$image_url = parse_url($url);
			if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
				$array = explode("&", $image_url['query']);
				return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
				} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
				$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
				return $hash[0]["thumbnail_large"];
			}
		}
		
		public function image_upload($file_name,$user_id,$config)
		{ 
			$this->load->library('upload');
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('product_image'))
			{ 
				return true;
			}
			else
			{
				print_r($this->upload->display_errors());	exit;
			}
		}
		
		public  function size_as_kb($yoursize)
		{
			if($yoursize < 1024) {
				return "{$yoursize} bytes";
				} elseif($yoursize < 1048576) {
				$size_kb = round($yoursize/1024);
				return "{$size_kb} KB";
				} else {
				$size_mb = round($yoursize/1048576, 1);
				return "{$size_mb} MB";
			}
		}
		
		// function to ulpoad multiple files -
		function upload_file($field_name = NULL, $files = NULL, $config = NULL, $multiple = FALSE)
		{
			// check for empty parameters -
			if($field_name == NULL or $files == NULL or $config == NULL)
			{
				return FALSE;
			}
			
			// define array for uploaded file names -
			$file_name_array = array();
			
			// define array for file upload error -
			$file_error_array = array();
			
			// get count of no. of files in array -
			$count = count($files[$field_name]['tmp_name']);
			
			for($i = 0; $i < $count; $i++)
			{
				// get each file details -
				if($multiple == TRUE)
				{
					$_FILES[$field_name]['name']        = $files[$field_name]['name'][$i];
					$_FILES[$field_name]['type']        = $files[$field_name]['type'][$i];
					$_FILES[$field_name]['tmp_name']    = $files[$field_name]['tmp_name'][$i];
					$_FILES[$field_name]['error']       = $files[$field_name]['error'][$i];
					$_FILES[$field_name]['size']        = $files[$field_name]['size'][$i]; 
				}
				else
				{
					$_FILES = $files;
				}
				
				if($_FILES[$field_name]['error'] === 0)
				{
					// initialize config for file
					
					//added by rahul for get original name with timestamp
					$config['encrypt_name']=FALSE;
					
					$config['file_name']=pathinfo($files[$field_name]['name'][$i], PATHINFO_FILENAME).'_'.time();
					
					$this->upload->initialize($config);
					
					// upload file -
					if($this->upload->do_upload($field_name))
					{
						// get uploaded file data -
						$file_info = $this->upload->data();
						
						// store file name in array -
						$file_name_array[] = $file_info['file_name'];
					}
					else
					{
						// get file upload error -
						$error =  $this->upload->display_errors();
						
						// srore error in array -
						$file_error_array = $error; //Rahul B
						// $file_error_array[$files[$field_name]['name'][$i]] = $error;// bhagwan
					}
				}
			}
			
			// define array for response -
			$response = array();
			
			// file name array and file error array store in response array -
			$response[0] = $file_name_array;
			$response[1] = $file_error_array;
			
			// return response array -
			return $response;
		}
		
		//FUNCTION ADDED BY SAGAR ON 25-08-2020
		function delete_single_file($table_name='', $pk_field_name='', $id=0, $field_name='', $file_path='')
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if($table_name != "" && $pk_field_name != "" && $id != 0 && $field_name != '')
			{
				$res_data = $this->getRecords($table_name, array($pk_field_name=>$id));
				if(count($res_data) == 0) 
				{ 
					$result['flag'] = "error"; 
				}
				else 
				{ 
					if($file_path != '')
					{
						//@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($res_data[0][$field_name]));
						@unlink($file_path.$encrptopenssl->decrypt($res_data[0][$field_name]));
					}
					
					$up_data[$field_name] = "";
					$this->updateRecord($table_name,$up_data,array($pk_field_name=>$id));			
					$result['flag'] = "success";
				}
			}
			else { $result['flag'] = "error"; }
			
			return $result;
		}
		
		public function array_sorting($res_arr=array(), $input_arr=array(), $sorting_key,$is_decrypted=0){
			$new_sort_arr = array();
			if(count($res_arr) > 0 && count($input_arr) > 0 && $sorting_key != '')
			{
				$encrptopenssl =  New Opensslencryptdecrypt();
				$new_arr = $final_arr = $other_arr = array();
				$other_flag = 0;
				
				foreach($res_arr as $res)
				{
					$enc_val = trim($encrptopenssl->decrypt($res[$sorting_key]));
					if ($is_decrypted==1) {
						$enc_val = trim($res[$sorting_key]);
					}
					
					if(strtolower($enc_val) != 'other')
					{                  
						$new_arr[$enc_val][$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$new_arr[$enc_val][$input] = $res[$input];
						}
					}
					else
					{
						$other_flag = 1;                      
						$other_arr[$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$other_arr[$input] = $res[$input];
						}
						$other_arr = $other_arr;
					}
					$final_arr = $new_arr;
				}
				
				//echo "<pre>"; print_r($final_arr); echo "</pre>"; exit;
				ksort($final_arr);
				$new_sort_arr = array();
				foreach($final_arr as $new_res)
				{
					$new_sort_arr[] = $new_res;
				}
				
				if($other_flag == 1)
				{
					$new_sort_arr[] = $other_arr;
				}
			}
			
			return $new_sort_arr;
		}
		
		
		public function store_json_for_search($user_id)
		{
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->db->select('user_id, user_category,sub_catname,title,first_name,middle_name,last_name,email,mobile,institution_type,institution_full_name,other_institution_type,other_domain_industry,other_public_prvt,domain_industry');
			$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
			$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
			$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
			
			$user_arr = array();
			
			if(count($user_data))
			{
				foreach($user_data as $row_val)
				{
					$row_val_new=array();
					
					if($encrptopenssl->decrypt($row_val['title']) != "") { $row_val_new['title'] = $encrptopenssl->decrypt($row_val['title']); }
					if($encrptopenssl->decrypt($row_val['first_name']) != "") {$row_val_new['first_name'] = $encrptopenssl->decrypt($row_val['first_name']); }
					if($encrptopenssl->decrypt($row_val['middle_name']) != "") {$row_val_new['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']); }
					if($encrptopenssl->decrypt($row_val['last_name']) != "") {$row_val_new['last_name'] = $encrptopenssl->decrypt($row_val['last_name']); }
					if($encrptopenssl->decrypt($row_val['email']) != "") {$row_val_new['email'] = $encrptopenssl->decrypt($row_val['email']); }
					if($encrptopenssl->decrypt($row_val['mobile']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['mobile']); }
					if($encrptopenssl->decrypt($row_val['membership_type']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['membership_type']); }
					if($encrptopenssl->decrypt($row_val['user_category']) != "") {$row_val_new['user_category'] = $encrptopenssl->decrypt($row_val['user_category']); }
					if($encrptopenssl->decrypt($row_val['sub_catname']) != "") {$row_val_new['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']); }
					if($encrptopenssl->decrypt($row_val['institution_type']) != "") {$row_val_new['institution_type'] = $encrptopenssl->decrypt($row_val['institution_type']); }
					if($encrptopenssl->decrypt($row_val['institution_full_name']) != "") {$row_val_new['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']); }
					if($row_val['other_institution_type'] != "") {$row_val_new['other_institution_type'] = $row_val['other_institution_type']; }
					if($row_val['other_domain_industry'] != "") {$row_val_new['other_domain_industry'] = $row_val['other_domain_industry']; }
					if($row_val['other_public_prvt'] != "") {$row_val_new['other_public_prvt'] = $row_val['other_public_prvt']; }
					
					$domain_plain='';
					if ($row_val['domain_industry']!='') 
					{
						$domain=explode(',', $row_val['domain_industry']);
						foreach ($domain as $key => $value) 
						{						
							$domain_data = $this->master_model->getRecords("domain_industry_master",array('id'=>$value),'domain_name' );
							
							if(isset($domain_data) && count($domain_data) > 0)
							{
								$domain_plain.=$encrptopenssl->decrypt($domain_data[0]['domain_name']).',';
							}
						}
					}
					$row_val_new['domain_industry'] = rtrim($domain_plain,',') ;
					//$user_arr[] = $row_val_new;
					
					//echo "<pre>"; echo json_encode($user_arr);echo "</pre>";		exit;
					
					$up_arr['json_str'] = json_encode($row_val_new);
					$this->master_model->updateRecord('arai_registration',$up_arr,array('user_id'=>$row_val['user_id']));
					return true;
					
				}
				
				
				
			}
			
		}
		
		function checkApplyTeamCondtions($apply_status=0, $challenge_owner_status=0)
		{
			$dispStatus = '-';
			$showBtnAccept = $showBtnReject = $editBtnDisplay = 0;
			
			if($apply_status == 'Pending')
			{
				$dispStatus = 'Pending';
				$editBtnDisplay = 1;
			}
			else if($apply_status == 'Withdrawn')
			{
				$dispStatus = 'Withdrawn';
			}
			else
			{
				if($apply_status == 'Applied' && $challenge_owner_status == 'Pending')
				{
					$dispStatus = 'Applied';
					$showBtnAccept = 1;
					$showBtnReject = 1;
					$editBtnDisplay = 1;
				}
				else if($apply_status == 'Applied' && $challenge_owner_status == 'Approved')
				{
					$dispStatus = 'Approved';
					$showBtnReject = 1;
				}
				else if($apply_status == 'Applied' && $challenge_owner_status == 'Rejected')
				{
					$dispStatus = 'Rejected';
				}
			}
			
			$return_data['dispStatus'] = $dispStatus;
			$return_data['showBtnAccept'] = $showBtnAccept; 
			$return_data['showBtnReject'] = $showBtnReject;
			$return_data['editBtnDisplay'] = $editBtnDisplay;
			
			return $return_data;
		}
	}
?>