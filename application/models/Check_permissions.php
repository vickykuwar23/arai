<?php if (!defined('BASEPATH')) {exit('No direct script access.');}
class Check_permissions extends CI_Model
{
    public function is_logged_in()
    {
        $_SESSION['intended_url'] = current_url();
        if ($this->session->userdata('user_id') == '') {
            $this->session->set_flashdata('error_is_logged_in', 'Access denied !');
            redirect(base_url('login'));

            // access
        }

    }

    public function show_link_if_loggedin()
    {
        if ($this->session->userdata('user_id') == '') {
            return false;
        }
        return true;
    }

    public function is_admin_approved()
    {
        $user_id = $this->session->userdata('user_id');
        if ($user_id != '') {

            $user_info = $this->master_model->getRecords('registration', array('user_id' => $user_id));

            if ($user_info[0]['admin_approval'] == 'no') {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $rediect_url = $_SERVER['HTTP_REFERER'];
                } else {
                    $rediect_url = base_url();
                }

                $this->session->set_flashdata('error_approval', 'Access denied !');
                redirect($rediect_url);
            } else {
                return true;
            }

        }

    }

    public function is_authorise($module_id)
    {
        $permissions     = $this->session->userdata('permissions');
        $permissions_arr = explode(',', $permissions);
        if (in_array($module_id, $permissions_arr)) {
            return true;
        } else {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $rediect_url = $_SERVER['HTTP_REFERER'];
            } else {
                $rediect_url = base_url();
            }

            $this->session->set_flashdata('error_pemission', 'Access denied !');
            redirect($rediect_url);

        }
    }

    public function is_admin_exist()
    {
        $user_details = $this->master_model->getRecords("user_master", array('user_id' => $this->session->userdata('admin_id'), 'status' => 'Active'), 'user_id');
        if (count($user_details)>0) {
            return true;
        }else{
            session_destroy();
            $this->session->set_flashdata('error', 'Access denied !');
            redirect(base_url('xAdmin'));
        }
    }
    public function is_authorise_admin($module_id)
    {
        $user_details = $this->master_model->getRecords("user_master", array('user_id' => $this->session->userdata('admin_id'), 'status' => 'Active'), 'module_permission,user_id');

        if (count($user_details) > 0) {
            if ($user_details[0]['user_id'] == 1) {
                return true;
            }

            $permissions     = $user_details[0]['module_permission'];
            $permissions_arr = explode(',', $permissions);
            if (in_array($module_id, $permissions_arr)) {
                return true;
            } else {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $rediect_url = $_SERVER['HTTP_REFERER'];
                } else {
                    $rediect_url = base_url('xAdmin/dashboard');
                }

                $this->session->set_flashdata('error_pemission', 'Access denied !');
                redirect($rediect_url);

            }
        } else {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $rediect_url = $_SERVER['HTTP_REFERER'];
            } else {
                $rediect_url = base_url('xAdmin');
            }
            $this->session->set_flashdata('error_pemission', 'Access denied !');
            redirect($rediect_url);
        }
    }

   public function is_authorise_admin_link($module_id)
    {
        $user_details = $this->master_model->getRecords("user_master", array('user_id' => $this->session->userdata('admin_id'), 'status' => 'Active'), 'module_permission,user_id');

        if (count($user_details) > 0) {
            if ($user_details[0]['user_id'] == 1) {
                return true;
            }

            $permissions     = $user_details[0]['module_permission'];
            $permissions_arr = explode(',', $permissions);
            if (in_array($module_id, $permissions_arr)) {
                return true;
            } else {
            	return false;
            }
        } else {
           return false;
        }
    }

    public function is_authorise_ajax($module_id)
    {
        $permissions     = $this->session->userdata('permissions');
        $permissions_arr = explode(',', $permissions);
        if (in_array($module_id, $permissions_arr)) {
            return true;
        } else {
            return false;

        }
    }

    public function is_profile_complete()
    {
        $user_id = $this->session->userdata('user_id');
        if ($user_id != '') {

            $category = $this->session->userdata('user_category');

            if ($category == 2) {
                $table_name = 'profile_organization';

            } else if ($category == 1) {
                $table_name = 'arai_student_profile';
            }
            $profileInfo = $this->master_model->getRecords($table_name, array('user_id' => $user_id));

            if (count($profileInfo)) {
                $profile_status = $profileInfo[0]['profile_completion_status'];
                if ($profile_status == 'incomplete') {
                    if ($category == 2) {
                        $redirect_url = base_url('organization');
                    } else if ($category == 1) {

                        $redirect_url = base_url('profile');
                    }
                    $this->session->set_flashdata('error_profile_competion', 'Access denied !');
                    redirect($redirect_url);
                } else {
                    return true;
                }
            } else {
                if ($category == 2) {
                    $redirect_url = base_url('organization');
                } else if ($category == 1) {

                    $redirect_url = base_url('profile');
                }
                $this->session->set_flashdata('error_profile_competion', 'Access denied !');
                redirect($redirect_url);
            }

        }

    }

    public function is_authorise_link($module_id)
    {
        $permissions     = $this->session->userdata('permissions');
        $permissions_arr = explode(',', $permissions);
        if (in_array($module_id, $permissions_arr)) {
            return true;
        } else {
            return false;

        }
    }

    public function is_profile_complete_without_redirect()
    {
        $user_id = $this->session->userdata('user_id');
        if ($user_id != '') {

            $category = $this->session->userdata('user_category');

            if ($category == 1) {
                $table_name = 'arai_student_profile';
            }
            $profileInfo = $this->master_model->getRecords($table_name, array('user_id' => $user_id));

            if (count($profileInfo)) {

                $profile_status = $profileInfo[0]['profile_completion_status'];
                if ($profile_status == 'incomplete') {

                    echo 'false';
                } else {
                    echo 'true';
                }
            } else {
                if ($category == 1) {

                    echo 'false';
                }

            }

        }

    }

    public function profile_complete_percentage()
    {
        $encrptopenssl                = new Opensslencryptdecrypt();
        $user_id                      = $this->session->userdata('user_id');
        $user_category                = $this->session->userdata('user_category');
        $user_sub_cat                 = $this->session->userdata('user_sub_category');
        $data                         = array();
        $data['profile_complete_per'] = '';
        $data['initial']              = '';
        if ($user_id != '' && $user_sub_cat) {

            $user_data          = $this->master_model->getRecords("arai_registration r", array('r.user_id' => $user_id), 'first_name,last_name');
            $first_name_initial = strtoupper(substr($encrptopenssl->decrypt($user_data[0]['first_name']), 0, 1));
            $last_name_initial  = strtoupper(substr($encrptopenssl->decrypt($user_data[0]['last_name']), 0, 1));
            $data['initial']    = $first_name_initial . $last_name_initial;

            if ($user_category == 1) {

                $profileInfo = $this->master_model->getRecords("student_profile", array('user_id' => $user_id));

                if ($user_sub_cat == 1) {
                    $mandatory_arr = array('employement_status', 'designation', 'domain_area_of_expertise', 'skill_sets', 'years_of_experience');
                    $optional_arr  = array('company_name', 'designation_description', 'corporate_linkage_code', 'profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country');
                }

                if ($user_sub_cat == 2) {

                    $mandatory_arr = array('status', 'university_course', 'university_since_year', 'university_to_year', 'university_name', 'university_location', 'domain_area_of_expertise', 'skill_sets', 'student_id_proof');

                    $optional_arr = array('profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country', 'event_experience_in_year', 'event_description_of_work', 'technical_experience_in_year', 'technical_experience_to_year', 'technical_description_of_work', 'additional_detail');

                }

                if ($user_sub_cat == 11) {

                    $mandatory_arr = array('employement_status', 'designation', 'domain_area_of_expertise', 'skill_sets', 'years_of_experience', 'bio_data', 'no_of_paper_publication', 'no_of_patents');

                    $optional_arr = array('company_name', 'designation_description', 'corporate_linkage_code', 'profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country', 'portal_name_multiple', 'portal_link_multiple', 'portal_description_multiple');
                }

                $data['profile_complete_per'] = $this->get_completeness_bar_per(40, 40, 20, $mandatory_arr, $optional_arr, $profileInfo);

            } else if ($user_category == 2) {

                $org_profile_data = $this->master_model->getRecords('profile_organization o', array("o.user_id" => $user_id));

                $mandatory_arr = array('org_sector', 'overview', 'spoc_bill_addr1', 'spoc_bill_addr2', 'spoc_bill_addr_city', 'spoc_bill_addr_state', 'spoc_bill_addr_country', 'spoc_bill_addr_pin', 'specialities_products', 'establishment_year', 'website', 'linkedin_page', 'pan_card', 'institution_reg_certificate', 'self_declaration');
                $optional_arr  = array('spoc_sec_num_country_code1', 'spoc_sec_num1', 'spoc_sec_num_country_code2', 'spoc_sec_num2', 'institution_size', 'company_evaluation', 'org_logo', 'address_proof', 'gst_reg_certificate');

                $data['profile_complete_per'] = $this->get_completeness_bar_per(40, 40, 20, $mandatory_arr, $optional_arr, $org_profile_data);
            }
        }
        return $data;
    }

    public function get_completeness_bar_per($reg_per = 0, $man_per = 0, $opt_per = 0, $mandatory_arr = '', $optional_arr = '', $result_arr = '')
    {
        $man_complete_per = $opt_complete_per = 0;
        if (count($mandatory_arr) > 0 && count($optional_arr) > 0 && count($result_arr) > 0) {
            if (!empty($result_arr)) {
                $total_man_cnt = $complete_man_cnt = 0;
                foreach ($mandatory_arr as $man_res) {
                    if ($result_arr[0][$man_res] != "") {$complete_man_cnt++;}
                    $total_man_cnt++;
                }

                $man_complete_per = round((($complete_man_cnt * $man_per) / $total_man_cnt), 2);

                $total_opt_cnt = $complete_opt_cnt = 0;
                foreach ($optional_arr as $opt_res) {
                    if ($result_arr[0][$opt_res] != "") {$complete_opt_cnt++;}
                    $total_opt_cnt++;
                }

                $opt_complete_per = round((($complete_opt_cnt * $opt_per) / $total_opt_cnt), 2);
            }
        }

        return $profile_complete_per = $reg_per + $man_complete_per + $opt_complete_per;
    }

    public function StoreOtherData($other_string, $table_name, $field_name)
    {
        $user_id = $this->session->userdata('user_id');
        if ($other_string != '' && $table_name != '' && $field_name != '') {
            $other_arr = explode(',', $other_string);
            if (count($other_arr) > 0) {

                foreach ($other_arr as $key => $other_value) {
                    $is_exist = $this->master_model->getRecords($table_name, array($field_name => $other_value));
                    if (count($is_exist) == 0) {
                        $add_arr[$field_name]        = $other_value;
                        $add_arr['status']           = 'Block';
                        $add_arr['added_by_user_id'] = $user_id;
                        $this->master_model->insertRecord($table_name, $add_arr);
                    }
                }
            }
        }

    }

}
