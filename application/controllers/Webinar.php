<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Webinar extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');	
			$this->load->library('Opensslencryptdecrypt'); 
			$this->load->helper('auth');
			$this->load->helper('text');
			//$this->check_permissions->is_logged_in();
			ini_set('upload_max_filesize', '100M');  
			ini_set('post_max_size', '100M');  
		} 
		
		############# START : WEBINAR SECTION ###############################################################################################
		public function index()
		{

			//$module_id = 8;
			//$this->check_permissions->is_authorise($module_id);   
			
			// ini_set('display_errors', '1');
			// ini_set('display_startup_errors', '1');
			// error_reporting(E_ALL);	
			
			$this->check_permissions->is_logged_in();			
			$encrptopenssl =  New Opensslencryptdecrypt();	
			$data['webinar_banner_limit'] = $webinar_banner_limit = 15;
			$data['featured_webinar_limit'] = $featured_webinar_limit = 15;
			$data['upcoming_webinar_limit'] = $upcoming_webinar_limit = 6;
			
			$data['webinar_banner'] = $this->master_model->getRecords("webinar_banner",array('status' => 'Active', "upload_type" => 'Image'),'',array('xOrder' => 'ASC'),0,$webinar_banner_limit);
			$data['technology_data'] = $this->master_model->array_sorting($this->master_model->getRecords("arai_webinar_technology_master", array("status" => 'Active')), array('id'),'technology_name');
			
			$data['page_title'] 	 = 'Webinar Wall';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/index';
			$this->load->view('front/front_combo',$data);
		}
		
		function getFeaturedUpcomingWebibarDataAjax()		
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$data_upcoming['featured_webinar_limit'] = $featured_webinar_limit = 15;
			$data_upcoming['upcoming_webinar_limit'] = $upcoming_webinar_limit = 6;
			$search_str = '';
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$f_start = $this->input->post('f_start', TRUE);
				$f_limit = $this->input->post('f_limit', TRUE);
				$u_start = $this->input->post('u_start', TRUE);
				$u_limit = $this->input->post('u_limit', TRUE);
				$is_show_more = $this->input->post('is_show_more', TRUE);
				$order_by                                  = $this->input->post('order_by', true);
				if($f_start != "" && $f_limit!= "" && $u_start != "" && $u_limit!= ""  && $is_show_more!= "" )
				{
					$result['flag'] = "success";
						
					//START : FOR SEARCH KEYWORD
					$keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword')); 
					if($keyword)
					{
						$search_str	.= " ( webinar_name LIKE '%".$keyword."%' OR webinar_technology_other LIKE '%".$keyword."%' OR webinar_breif_info LIKE '%".$keyword."%' OR webinar_key_points LIKE '%".$keyword."%' OR webinar_about_author LIKE '%".$keyword."%') ";
					}
					//END : FOR SEARCH KEYWORD
						
					//START : FOR SEARCH TECHNOLOGY
					$technology = trim($this->input->post('technology'));
					if($technology != '')
					{
						$technology_arr = explode(",",$technology);
						if(count($technology_arr) > 0)
						{
							$old_search_str = $search_str;
							if($old_search_str != '') { $search_str .= " AND "; }
														
							$search_str .= " (";
							$i = 0;
							foreach($technology_arr as $res)
							{ 
								$search_str .= " FIND_IN_SET('".$res."',webinar_technology)";
								if($i != (count($technology_arr) - 1)) { $search_str .= " OR "; }			
								$i++;
							}								
							$search_str .= " )";
						}
					}
					//END : FOR SEARCH TECHNOLOGY
					
					//START : FOR SEARCH PAY TYPE
					$pay_type = trim($this->input->post('pay_type'));
					if($pay_type != '')
					{
						$pay_type_arr = explode(",",$pay_type);
						if(count($pay_type_arr) > 0)
						{
							$old_search_str = $search_str;
							if($old_search_str != '') { $search_str .= " AND "; }
														
							$search_str .= " (";
							$i = 0;
							foreach($pay_type_arr as $res)
							{ 
								$search_str .= " webinar_cost_type = '".$res."'";
								if($i != (count($pay_type_arr) - 1)) { $search_str .= " OR "; }			
								$i++;
							}								
							$search_str .= " )";
						}
					}
					//END : FOR SEARCH PAY TYPE		
					
					//end:upcoming or closed		
					$webinar_time = trim($this->input->post('webinar_time'));
					$current_date = date('Y-m-d');
					if ($webinar_time!='') {
								$old_search_str = $search_str;
									if($old_search_str != '') { $search_str .= " AND "; }
									$search_str .= " (";
											if ($webinar_time=='upcoming') {
													$search_str .= " webinar_date >= '".$current_date."'";
											}else if($webinar_time=='closed'){
													$search_str .= " webinar_date < '".$current_date."'";
											}else{
													$search_str .= " webinar_date > 0 ";
											
											}
									$search_str .= " )";

							
							}		
							// echo $search_str;die;
						//end:upcoming or closed	

					// START : FOR FEATURED WEBINAR
					if($is_show_more == 0)
					{
						if($search_str != "") { $this->db->where($search_str); }			
						$this->db->order_by('ISNULL(xOrder), xOrder = 0, xOrder, webinar_name ASC','',FALSE);
						$data_featured['featured_webinar'] = $this->master_model->getRecords("webinar",array('is_deleted' => '0', 'admin_status' => 'Approved', 'is_featured' => 'Yes'),'','',$f_start,$f_limit);
						$data_featured['featured_webinar_qry'] = $this->db->last_query();
						$result['Featured_response'] = $this->load->view('front/webinar/incFeatureWebibarCommon', $data_featured, true);
					}
					// START : FOR FEATURED WEBINAR
					
					// START : FOR UPCOMING WEBINAR
					if($search_str != "") { $this->db->where($search_str); }		
					if ($order_by!='') {
						 $this->db->order_by('w_id', $order_by, false);   
					}	else{
					$this->db->order_by('ISNULL(xOrder), xOrder = 0, xOrder, webinar_name ASC','',FALSE);
					}		
					$data_upcoming['upcoming_webinar'] = $this->master_model->getRecords("webinar",array('is_deleted' => '0', 'admin_status' => 'Approved', 'is_featured' => 'No'),'','',$u_start,$u_limit);
					
					//START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
					if($search_str != "") { $this->db->where($search_str); }
					$data_upcoming['total_upcoming_webinar_count'] = $total_upcoming_webinar_count = $this->master_model->getRecordCount('arai_webinar', array('is_deleted' => '0', 'admin_status' => 'Approved', 'is_featured' => 'No'), 'w_id');
					//END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
						
					$data_upcoming['new_start'] = $new_start = $u_start + $u_limit;
					$result['Upcoming_response'] = $this->load->view('front/webinar/incUpcomingWebibarCommon', $data_upcoming, true);
					// END : FOR UPCOMING WEBINAR
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";	
			}
			
			echo json_encode($result);
		}
				
		public function mywebinar() // MY WEBINAR LIST
		{
			$this->check_permissions->is_logged_in();
			$user_id = $this->session->userdata('user_id');			
			//$data['webinar_listing'] = $this->master_model->getRecords("webinar",array('user_id' => $user_id),'',array('w_id' => 'DESC'));
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/listing';
			$this->load->view('front/front_combo',$data);			
		}
				
		public function webinar_listing() // MY WEBINAR LIST : GET SERVER SIDE DATATABLE DATA USING AJAX
		{		
			//$this->check_permissions->is_logged_in();		
			$csrf_test_name = $this->security->get_csrf_hash();			
			$user_id = $this->session->userdata('user_id');	
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;	
			
			$condition = "SELECT * FROM arai_webinar WHERE is_deleted = '0' AND user_id = '".$user_id."' ORDER BY w_id DESC";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$result 	= $this->db->query($query)->result();
			$rowCount = $this->getNumData($condition);
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $showResult){
					
					$web_id					= $showResult->w_id;
					$webinar_id				= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$showResult->webinar_id.'</a>';
					$webinar_name			= ucwords($showResult->webinar_name);
					$webinar_date			= $showResult->webinar_date;
					$webinar_start_time			= $showResult->webinar_start_time;
					$webinar_end_time			= $showResult->webinar_end_time;
					$dispTime = "<span style='white-space:nowrap;'>".date('h:i A', strtotime($webinar_start_time))." - ".date('h:i A', strtotime($webinar_end_time))."</span>";
					$webinar_cost_type		= $showResult->webinar_cost_type;
					$cost_price				= "<span style='white-space:nowrap;'>".$showResult->currency." ".$showResult->cost_price."</span>";
					$banner_img				= $showResult->banner_img;
					$registration_link		= $showResult->registration_link;
					$hosting_link			= $showResult->hosting_link;
					$webinar_breif_info		= $showResult->webinar_breif_info;
					$webinar_key_points		= $showResult->webinar_key_points;
					$webinar_about_author	= $showResult->webinar_about_author;
					
					if($showResult->admin_status == 'Withdraw'){$showText = 'Withdrawn';} else {$showText = $showResult->admin_status;}
					$admin_status			= '<span id="web-status-'.$web_id.'">'.$showText.'</span>';			
					$combineTime 			= date('d-m-Y', strtotime($webinar_date));
					
					$gettotal = $this->master_model->getRecords("webinar_attendence", array('w_id' => $web_id, 'status' => 'Active'));
					
					$applicationReceived 	= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($web_id)).'" class="applicant-listing">'.count($gettotal).'</a>';	
					$detailView = '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$webinar_name.'</a>';
					$withdraw = '';
					if($showResult->admin_status != 'Withdraw'){					
						$withdraw				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Withdraw" data-id="'.$web_id.'" class="webinar-withdraw btn btn-info btn-green-fresh"><i class="fa fa-ban" aria-hidden="true"></i></a> ';					
					}							
					$delete				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" data-id="'.$web_id.'" class="webinar-remove btn btn-info btn-green-fresh"><i class="fa fa-remove" aria-hidden="true"></i></a> ';			
					$edit				= '<a href="'.base_url('webinar/edit/'.base64_encode($web_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a> ';
					
					$view				= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($web_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a> ';				
					
					$conCat = $edit."&nbsp;".$withdraw; //."&nbsp;".$delete."&nbsp;";	
					if($showResult->admin_status == 'Withdraw' || $showResult->admin_status == 'Rejected')
					{
						$conCat .= "&nbsp;".$delete;
					}
					
					
					$i++;					
					$dataArr[] = array(
					$webinar_id,
					$webinar_name,
					$combineTime,	
					$dispTime,
					$cost_price,
					$admin_status,
					$applicationReceived,
					"<span style='white-space:nowrap;'>".$conCat."</span>"
					);					
			    $rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}				
				} else {			
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);			
				$response['token'] = $csrf_test_name;
			}			
			echo json_encode($response);
			
		} // End Function
				
		public function appliedWebinar() // MY APPLIED WEBINAR LIST
		{
			$this->check_permissions->is_logged_in();
			$user_id = $this->session->userdata('user_id');			
			//$data['webinar_listing'] = $this->master_model->getRecords("webinar",array('user_id' => $user_id),'',array('w_id' => 'DESC'));
			$data['page_title'] 	 = 'Applied Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/appliedWebinar_listing';
			$this->load->view('front/front_combo',$data);			
		}
		
		public function appliedWebinar_listing() // MY APPLIED WEBINAR LIST : GET SERVER SIDE DATATABLE DATA USING AJAX
		{		
			//$this->check_permissions->is_logged_in();		
			$csrf_test_name = $this->security->get_csrf_hash();			
			$user_id = $this->session->userdata('user_id');	
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;	
			
			$condition = "SELECT awa.id, awa.w_id, awa.user_id, awa.status, aw.* FROM arai_webinar_attendence awa INNER JOIN arai_webinar aw ON aw.w_id = awa.w_id WHERE awa.user_id = '".$user_id."' AND awa.status = 'Active' AND aw.is_deleted = '0' ORDER BY awa.createdAt DESC";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$result 	= $this->db->query($query)->result();
			$rowCount = $this->getNumData($condition);
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0)
			{				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $showResult)
				{					
					$web_id					= $showResult->w_id;
					$webinar_id				= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$showResult->webinar_id.'</a>';
					$webinar_name			= ucwords($showResult->webinar_name);
					$webinar_date			= $showResult->webinar_date;
					$webinar_start_time			= $showResult->webinar_start_time;
					$webinar_end_time			= $showResult->webinar_end_time;
					$dispTime = "<span style='white-space:nowrap;'>".date('h:i A', strtotime($webinar_start_time))." - ".date('h:i A', strtotime($webinar_end_time))."</span>";
					$webinar_cost_type		= $showResult->webinar_cost_type;
					$cost_price				= "<span style='white-space:nowrap;'>".$showResult->currency." ".$showResult->cost_price."</span>";
					$banner_img				= $showResult->banner_img;
					$registration_link		= $showResult->registration_link;
					$hosting_link			= $showResult->hosting_link;
					$webinar_breif_info		= $showResult->webinar_breif_info;
					$webinar_key_points		= $showResult->webinar_key_points;
					$webinar_about_author	= $showResult->webinar_about_author;
					
					if($showResult->admin_status == 'Withdraw'){$showText = 'Withdrawn';} else {$showText = $showResult->admin_status;}
					$admin_status			= '<span id="web-status-'.$web_id.'">'.$showText.'</span>';			
					$combineTime 			= date('d-m-Y', strtotime($webinar_date));
					
					$gettotal = $this->master_model->getRecords("webinar_attendence", array('w_id' => $web_id, 'status' => 'Active'));
					
					$applicationReceived 	= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($web_id)).'" class="applicant-listing">'.count($gettotal).'</a>';	
					$detailView = '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$webinar_name.'</a>';
					$withdraw = '';
					if($showResult->admin_status != 'Withdraw'){					
						$withdraw				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Withdraw" data-id="'.$web_id.'" class="webinar-withdraw btn btn-info btn-green-fresh"><i class="fa fa-ban" aria-hidden="true"></i></a> ';					
					}							
					$delete				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" data-id="'.$web_id.'" class="webinar-remove btn btn-info btn-green-fresh"><i class="fa fa-remove" aria-hidden="true"></i></a> ';			
					$edit				= '<a href="'.base_url('webinar/edit/'.base64_encode($web_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a> ';
					
					$view				= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($web_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a> ';				
					
					$conCat = $edit."&nbsp;".$withdraw; //."&nbsp;".$delete."&nbsp;";	
					if($showResult->admin_status == 'Withdraw' || $showResult->admin_status == 'Rejected')
					{
						$conCat .= "&nbsp;".$delete;
					}					
					
					$i++;					
					$dataArr[] = array(
					$webinar_id,
					$webinar_name,
					$combineTime,	
					$dispTime,
					$cost_price,
					$admin_status
					);					
			    $rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}				
			} 
			else 
			{			
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);			
				$response['token'] = $csrf_test_name;
			}			
			echo json_encode($response);
			
		} // End Function
		
		public function add() // ADD NEW WEBINAR
		{			
			$this->check_permissions->is_logged_in();	
			
			$module_id = 27;
			$this->check_permissions->is_authorise($module_id);  	
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->load->library('upload');			
			$user_id = $this->session->userdata('user_id');	
			
			$this->master_model->getRecords("challenge c", array("c.status" => 'Active', "c.challenge_status" => 'Approved'),'',array("c.c_id" => 'DESC'),0,6);	
			$getWebcode = $this->master_model->getRecords("track_webinar",'','',array('id' => 'DESC'), 0, 1);			
			$last_id	   = @$getWebcode[0]['webinar_code'];
			
			
			if($last_id == ""){				
				$data['webinar_code'] = 'TN-WEB-000001';
				} else {
				$last_code = $last_id + 1;
				$zerofilledNumber = str_pad((string) $last_code, 6, '0', STR_PAD_LEFT); 
				$data['webinar_code'] = 'TN-WEB-'.$zerofilledNumber;
			}
			
			// Check Validation
			$this->form_validation->set_rules('webinar_name', 'Webinar Name', 'required|xss_clean');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
			$this->form_validation->set_rules('start_time', 'Start Time', 'required|xss_clean');
			$this->form_validation->set_rules('end_time', 'End Time', 'required|xss_clean');
			$this->form_validation->set_rules('cost_type', 'Type of Registration', 'required|xss_clean');
			$this->form_validation->set_rules('webinar_technology[]', 'Webinar Technology', 'required|xss_clean');
			$this->form_validation->set_rules('webinar_technology_other', 'Webinar Technology Other', 'xss_clean');
			$this->form_validation->set_rules('exclusive_technovuus_event', 'Exclusive Technovuus Event', 'required|xss_clean');
			
			if(isset($_POST['exclusive_technovuus_event']) && $_POST['exclusive_technovuus_event'] == 'No')
			{			
				$this->form_validation->set_rules('registration_link', 'Registration Link', 'required');
			}
			
			$this->form_validation->set_rules('hosting_link', 'Hosting Link', 'required');
			$this->form_validation->set_rules('breif_desc', 'Breif Info', 'required|xss_clean');
			$this->form_validation->set_rules('key_points', 'Key Points', 'required|xss_clean');
			$this->form_validation->set_rules('about_author', 'About Author', 'required|xss_clean');
			
			if($this->form_validation->run())
			{
				$fileArr = $_FILES;
				$postArr = $this->input->post();			
				$banner_img = $_FILES['banner_img']['name'];
				$webinar_name = $this->input->post('webinar_name');
				$start_date = date("Y-m-d", strtotime($this->input->post('start_date')));
				$webinar_start_time = date("H:i", strtotime($this->input->post('start_time')));
				$webinar_end_time = date("H:i", strtotime($this->input->post('end_time')));
				$cost_type = $this->input->post('cost_type');
				$currency = $this->input->post('currency');
				$cost_price = $this->input->post('cost_price');
				$webinar_technology = $this->input->post('webinar_technology');
				$webinar_technology_other = $this->input->post('webinar_technology_other');
				$exclusive_technovuus_event	= $this->input->post('exclusive_technovuus_event');
				$registration_link = $_POST['registration_link'];
				$hosting_link = $_POST['hosting_link'];
				$breif_desc = $this->input->post('breif_desc');
				$key_points = $this->input->post('key_points');
				$about_author = $this->input->post('about_author');
				
				if($banner_img!="")
				{								
					$config['upload_path']      = 'assets/webinar';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
					
					$b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0]))
					{						
						$b_image = $upload_files[0];						
					}					
				} // Banner Image End
				
				$banner_names = $b_image[0];	
				
				if($cost_type == "PAID"){ } else { $currency = ''; $cost_price = ''; }	
				
				$add_rec['user_id'] = $user_id;
				$add_rec['webinar_name'] = $webinar_name;
				$add_rec['webinar_date'] = $start_date;
				$add_rec['webinar_start_time'] = $webinar_start_time;
				$add_rec['webinar_end_time'] = $webinar_end_time;
				$add_rec['webinar_cost_type'] = $cost_type;
				$add_rec['currency'] = $currency;
				$add_rec['cost_price'] = $cost_price;
				$add_rec['webinar_technology'] = implode(",",$webinar_technology);
				$add_rec['webinar_technology_other'] = $webinar_technology_other;
				$add_rec['banner_img'] = $banner_names;
				$add_rec['exclusive_technovuus_event'] = $exclusive_technovuus_event;
				if($exclusive_technovuus_event == 'Yes') { $registration_link = ""; }
				$add_rec['registration_link'] = $registration_link;
				$add_rec['hosting_link'] = $hosting_link;
				$add_rec['webinar_breif_info'] = $breif_desc;
				$add_rec['webinar_key_points'] = $key_points;
				$add_rec['webinar_about_author'] = $about_author;
				$add_rec['webinar_reason'] = '';		
				
				if ($webinar_technology_other!='') {
					 $other_arr = explode(',', $webinar_technology_other);
            if (count($other_arr) > 0) {

                foreach ($other_arr as $key => $other_value) {
                    $is_exist = $this->master_model->getRecords('arai_webinar_technology_master', array('technology_name' => $other_value));
                    if (count($is_exist) == 0) {
                        $add_arr['technology_name'] = $encrptopenssl->encrypt($other_value);
                        $add_arr['status']    = 'Block';
                        $add_arr['added_by_user_id']   = $user_id;
                        $this->master_model->insertRecord('arai_webinar_technology_master', $add_arr);
                    }
                }
            }
				}
				/*echo "<pre>"; print_r($_POST);
				print_r($add_rec);
				exit;  */
				
				$insertQuery = $this->master_model->insertRecord('webinar',$add_rec);
				if($insertQuery > 0)
				{				
					// Update Challene Code
					//$getCode = getLastWebinarID($insertQuery);
					//$webinar_code = 'TN-WEB-'.$getCode;
					//$updateArrID = array('webinar_id' => $webinar_code, 'challenge_status' => 'Pending');
					//$updateQuery = $this->master_model->updateRecord('webinar',$updateArrID,array('c_id' => $insertQuery));					
					
					$getCode = getLastWebinarID($insertQuery);
					$webinar_code = 'TN-WEB-'.$getCode;
					$updateArrID = array('webinar_id' => $webinar_code);
					$updateQuery = $this->master_model->updateRecord('webinar',$updateArrID,array('w_id' => $insertQuery));
					
					// Get Webinar Details
					$getWebinarDetails = $this->master_model->getRecords("webinar", array("w_id" => $insertQuery));
					$webinarName 	= ucfirst($getWebinarDetails[0]['webinar_name']);
					$webinarID 		= $getWebinarDetails[0]['webinar_id'];
					$webinarDate 	= date('d-m-Y', strtotime($getWebinarDetails[0]['webinar_date']));
					$webinarTime 	= date("h:i A", strtotime($getWebinarDetails[0]['webinar_start_time']))." - ".date("h:i A", strtotime($getWebinarDetails[0]['webinar_end_time']));
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($insertQuery)).'">View</a>';
					
					// Get User Details 
					$userDetails = $this->master_model->getRecords("registration", array("user_id" => $user_id));
					$presenterfullname = ucfirst($encrptopenssl->decrypt($userDetails[0]['title']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['last_name']));
					$presenter_email = $encrptopenssl->decrypt($userDetails[0]['email']);
					
					$buildEmailArray = array(	
					'webinar_name' 		=> $webinarName,
					'webinar_id' 		=> $webinarID,
					'webinar_date' 		=> $webinarDate,
					'webinarTime' 		=> $webinarTime,
					'webinar_link' 		=> $webinarLink,
					'presenter_name' 	=> $presenterfullname,
					'presenter_email' 	=> $presenter_email
					);
					// Send email to presenter
					$this->presenter_adding_webinar($buildEmailArray);
					
					// SEND EMAIL TO ADMIN : ADDED BY SAGAR ON 28-12-2020
					$this->adding_webinar_mail_to_admin($buildEmailArray);
					
					// Log Data Added 
					$filesData 			= $_FILES;
					$json_data 			= array_merge($postArr,$filesData);
					$json_encode_data 	= json_encode($json_data);				
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 			=> $user_id,
					'action_name' 		=> "Add Webinar",
					'module_name'		=> 'Webinar Front',
					'store_data'		=> $json_encode_data,
					'ip_address'		=> $ipAddr,
					'createdAt'			=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					// Successfully Redirect
					$this->session->set_flashdata('success','Webinar has been successfully posted');	
					redirect(site_url('webinar/mywebinar'));
				}
			}
			
			$data['technology_data'] = $this->master_model->array_sorting($this->master_model->getRecords("arai_webinar_technology_master", array("status" => 'Active')), array('id'),'technology_name');	
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/add';
			$this->load->view('front/front_combo',$data);
		}
		
		public function edit($id) // EDIT WEBINAR
		{			
			$this->check_permissions->is_logged_in();
			//$module_id = 8;
			//$this->check_permissions->is_authorise($module_id); 
			$web_id = base64_decode($id);
			$this->load->library('upload');			
			$user_id = $this->session->userdata('user_id');	
			
			$data['webinar_data'] = $webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $web_id, "is_deleted"=>0));
			if(count($webinar_data) == 0) { redirect(site_url('webinar/mywebinar')); }
			
			// Check Validation
			$this->form_validation->set_rules('webinar_name', 'Webinar Name', 'required|xss_clean');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
			$this->form_validation->set_rules('start_time', 'Start Time', 'required|xss_clean');
			$this->form_validation->set_rules('end_time', 'End Time', 'required|xss_clean');
			$this->form_validation->set_rules('cost_type', 'Type of Registration', 'required|xss_clean');	
			$this->form_validation->set_rules('webinar_technology[]', 'Webinar Technology', 'required|xss_clean');
			$this->form_validation->set_rules('webinar_technology_other', 'Webinar Technology Other', 'xss_clean');
			$this->form_validation->set_rules('exclusive_technovuus_event', 'Exclusive Technovuus Event', 'required|xss_clean');
			
			if(isset($_POST['exclusive_technovuus_event']) && $_POST['exclusive_technovuus_event'] == 'No')
			{			
				$this->form_validation->set_rules('registration_link', 'Registration Link', 'required');
			}
			
			$this->form_validation->set_rules('hosting_link', 'Hosting Link', 'required');
			$this->form_validation->set_rules('breif_desc', 'Breif Info', 'required|xss_clean');
			$this->form_validation->set_rules('key_points', 'Key Points', 'required|xss_clean');
			$this->form_validation->set_rules('about_author', 'About Author', 'required|xss_clean');
			
			if($this->form_validation->run())
			{			
				/* echo '<pre>'; print_r($_POST); exit; */
				$fileArr = $_FILES;
				$postArr = $this->input->post();
				$is_agree = $this->input->post('is_agree');
				$banner_img = $_FILES['banner_img']['name'];
				$webinar_name = $this->input->post('webinar_name');
				$start_date = date("Y-m-d", strtotime($this->input->post('start_date')));
				$webinar_start_time = date("H:i", strtotime($this->input->post('start_time')));
				$webinar_end_time = date("H:i", strtotime($this->input->post('end_time')));
				$cost_type = $this->input->post('cost_type');
				$currency = $this->input->post('currency');
				$cost_price = $this->input->post('cost_price');
				$webinar_technology = $this->input->post('webinar_technology');
				$webinar_technology_other = $this->input->post('webinar_technology_other');
				$exclusive_technovuus_event	= $this->input->post('exclusive_technovuus_event');
				$registration_link = $_POST['registration_link'];
				$hosting_link = $_POST['hosting_link'];
				$breif_desc = $this->input->post('breif_desc');
				$key_points = $this->input->post('key_points');
				$about_author = $this->input->post('about_author');
				$updatedAt = date('Y-m-d H:i:s');		
				if($banner_img!="")
				{					
					$config['upload_path']      = 'assets/webinar';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
					
					$b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0]))
					{						
						$b_image = $upload_files[0];						
					}					
				} // Banner Image End
				
				if($cost_type == "PAID"){ $price = $cost_price; } else { $price = ''; $currency = ''; }	
				
				if($banner_img!="")
				{					
					$banner_names = $b_image[0];
					$up_rec['banner_img'] = $banner_names;
				} 
				
				$up_rec['user_id'] = $user_id;
				$up_rec['webinar_name'] = $webinar_name;
				$up_rec['webinar_date'] = $start_date;
				$up_rec['webinar_start_time'] = $webinar_start_time;
				$up_rec['webinar_end_time'] = $webinar_end_time;
				$up_rec['webinar_cost_type'] = $cost_type;
				$up_rec['currency'] = $currency;		
				$up_rec['cost_price'] = $price;		
				$up_rec['webinar_technology'] = implode(",",$webinar_technology);
				$up_rec['webinar_technology_other'] = $webinar_technology_other;
				$up_rec['exclusive_technovuus_event'] = $exclusive_technovuus_event;
				if($exclusive_technovuus_event == 'Yes') { $registration_link = ""; }
				$up_rec['registration_link'] = $registration_link;
				$up_rec['hosting_link'] = $hosting_link;
				$up_rec['webinar_breif_info'] = $breif_desc;
				$up_rec['webinar_key_points'] = $key_points;
				$up_rec['webinar_about_author'] = $about_author;
				$up_rec['updatedAt'] = $updatedAt;
				
				$updateQuery = $this->master_model->updateRecord('webinar',$up_rec,array('w_id' => $web_id));
				
				if($updateQuery > 0)
				{				
					// Log Data Added 
					$filesData 			= $_FILES;
					$json_data 			= array_merge($postArr,$filesData);
					$json_encode_data 	= json_encode($json_data);				
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 			=> $user_id,
					'action_name' 		=> "Edit Webinar",
					'module_name'		=> 'Webinar Front',
					'store_data'		=> $json_encode_data,
					'ip_address'		=> $ipAddr,
					'createdAt'			=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					// Successfully Redirect
					$this->session->set_flashdata('success','Webinar has been successfully updated.');	
					redirect(site_url('webinar/mywebinar'));
				}
			}
			
			$data['technology_data'] = $this->master_model->array_sorting($this->master_model->getRecords("arai_webinar_technology_master", array("status" => 'Active')), array('id'),'technology_name');	
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/edit';
			$this->load->view('front/front_combo',$data);
			
		}	// Edit Webinar
				
		public function processWithrawn() //WITHDRAW WEBINAR		
		{			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 			 = $this->input->post('id');
			$user_id = $this->session->userdata('user_id');
			$updatedAt			= date('Y-m-d H:i:s');		 
			$updateQuery = $this->master_model->updateRecord('webinar',array('admin_status'=>'Withdraw', 'updatedAt' => $updatedAt),array('w_id' => $id)); 
			
			// Log Data Added
			$user_id = $this->session->userdata('user_id');
			$arrData = array('id' => $id, 'status' => 'Withdraw', 'user_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Webinar admin status updated by Login User Front End",
			'module_name'	=> 'Webinar',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		public function processRemoved() // REMOVED WEBINAR
		{			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 			 = $this->input->post('id');
			$user_id = $this->session->userdata('user_id');
			$updatedAt			= date('Y-m-d H:i:s');		
			$updateQuery = $this->master_model->updateRecord('webinar',array('is_deleted'=>'1', 'updatedAt' => $updatedAt),array('w_id' => $id)); 
			
			// Log Data Added
			$user_id = $this->session->userdata('user_id');
			$arrData = array('id' => $id, 'is_deleted' => '1', 'user_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Webinar deleted by Login User Front End",
			'module_name'	=> 'Webinar',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		public function viewDetail($id) // WEBINAR DETAIL PAGE
		{
			$module_id = 29;
			$this->check_permissions->is_authorise($module_id);

			$encrptopenssl =  New Opensslencryptdecrypt();
							
			$this->check_permissions->is_logged_in();	
			$web_id = base64_decode($id);	
			$user_id = $this->session->userdata('user_id');
			$data['webinar_details'] = $webinar_details = $this->master_model->getRecords("webinar",array('w_id' => $web_id, "is_deleted"=>0));
			
			$redirect_flag = 0;
			if(count($webinar_details) == 0) { $redirect_flag = 1; }
			else if($webinar_details[0]['admin_status'] != 'Approved' && $webinar_details[0]['user_id'] != $user_id) { $redirect_flag = 1; }
							
			if($redirect_flag == 1) { redirect(site_url('webinar')); }
			
			$disp_webinar_technology = '';
			if(count($webinar_details) > 0)
			{
				if($webinar_details[0]['webinar_technology'] != '')
				{
					$this->db->where_in('id',$webinar_details[0]['webinar_technology'], FALSE);
					$webinar_technology = $this->master_model->getRecords("arai_webinar_technology_master");
					if(count($webinar_technology) > 0)
					{
						foreach($webinar_technology as $res)
						{
							$tech_name = $encrptopenssl->decrypt($res['technology_name']);
							if(strtolower($tech_name) != 'other')
							{
								$disp_webinar_technology .= $tech_name.", ";
							}
						}
					}
					
					if($webinar_details[0]['webinar_technology_other'] != "")
					{
						$disp_webinar_technology .= $webinar_details[0]['webinar_technology_other'];
					}
				}
			}
			
			$data['check_entry'] = $this->master_model->getRecords("webinar_attendence",array('w_id' => $web_id, 'user_id' => $user_id));
			$data['disp_webinar_technology'] = rtrim($disp_webinar_technology,", ");
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/view';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function addAttendee() //ADD WEBINAR ATTENDANCE
		{					
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name 	= $this->security->get_csrf_hash();
			$id 			 	= $this->input->post('id');
			$user_id 			= $this->session->userdata('user_id');
			$createdAt			= date('Y-m-d H:i:s');

			$getWebinarDetails = $this->master_model->getRecords("webinar", array("w_id" => $id));
			$web_date = $getWebinarDetails[0]['webinar_date'];
			$current_date = date('Y-m-d');

			if ($current_date > $web_date) {
				$jsonData = array("token" => $csrf_test_name, "success" => 'warning', "msg" => 'This webinar is closed for participation.');
				echo json_encode($jsonData);
				exit();
			}
			
			$enctryExist = $this->master_model->getRecords("webinar_attendence",array('w_id' => $id, 'user_id' => $user_id));
			
			if(count($enctryExist) == 0)
			{			

				$insertArr = array('user_id' => $user_id, 'w_id' => $id);
				$this->master_model->insertRecord('webinar_attendence',$insertArr);				
				
				// Get Webinar Details
			
				$webinarName 	= ucfirst($getWebinarDetails[0]['webinar_name']);
				$webinarID 		= $getWebinarDetails[0]['webinar_id'];
				$presenterID	= $getWebinarDetails[0]['user_id'];
				$webinarDate 	= date('d-m-Y', strtotime($getWebinarDetails[0]['webinar_date']));
				$webinarTime 	= date("h:i A", strtotime($getWebinarDetails[0]['webinar_start_time']))." - ".date("h:i A", strtotime($getWebinarDetails[0]['webinar_end_time']));
				
				$webinarLink	= '<a href="'.$getWebinarDetails[0]['hosting_link'].'">View</a>';
				//echo '<pre>'; print_r($getWebinarDetails); echo '</pre>'; 
				//echo "<br><br>".$webinarLink; exit;
				/* $webinarLink = '';
				if($getWebinarDetails[0]['exclusive_technovuus_event'] == 'Yes')
				{
				}
				else if($getWebinarDetails[0]['exclusive_technovuus_event'] == 'No')
				{
					$webinarLink	= '<a href="'.$getWebinarDetails[0]['registration_link'].'">View</a>';
				} */
				
				// Get Presenter Details 
				$userDetails = $this->master_model->getRecords("registration", array("user_id" => $presenterID));
				$presenterfullname = ucfirst($encrptopenssl->decrypt($userDetails[0]['title']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['last_name']));
				$presenter_email = $encrptopenssl->decrypt($userDetails[0]['email']);
				
				// Get Applicant Details 
				$applicantDetails = $this->master_model->getRecords("registration", array("user_id" => $user_id));
				$applicantfullname = ucfirst($encrptopenssl->decrypt($applicantDetails[0]['title']))." ".ucfirst($encrptopenssl->decrypt($applicantDetails[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($applicantDetails[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($applicantDetails[0]['last_name']));
				$applicant_email = $encrptopenssl->decrypt($applicantDetails[0]['email']);
				
				$presenterArray = array(	
				'webinar_name' 		=> $webinarName,
				'webinar_id' 		=> $webinarID,
				'webinar_date' 		=> $webinarDate,
				'webinarTime' 		=> $webinarTime,
				'webinar_link' 		=> $webinarLink,
				'presenter_name' 	=> $presenterfullname,
				'presenter_email' 	=> $presenter_email,
				'applicant_name' 	=> $applicantfullname,
				'applicant_email' 	=> $applicant_email
				);
				
				// Send mail to presenter 
				$this->notification_to_presenter($presenterArray);
				
				// Send mail to user 
				$this->notification_to_user($presenterArray);
				
				// Log Data Added		
				$arrData = array('w_id' => $id, 'user_id' => $user_id);
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Webinar Registration Attendence (Through Registration Link)",
				'module_name'	=> 'Webinar',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				if($getWebinarDetails[0]['exclusive_technovuus_event'] == "No")
				{
					$dispMsg = 'Thank you for registering.';
				}
				else
				{
					$dispMsg = 'Thank you for registering. The link to the webinar will be sent to your registered email address.';
				}
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'success', "exclusive_technovuus_event" => $getWebinarDetails[0]['exclusive_technovuus_event'], "msg" => $dispMsg);
				echo json_encode($jsonData);				
			} 
			else 
			{				
				$jsonData = array("token" => $csrf_test_name, "success" => 'warning', "msg" => 'You already register for this webinar.');
				echo json_encode($jsonData);	
			}			
		}
		
		public function applicantListing($id) //WEBINAR APPLICANT LISTING
		{
			$this->check_permissions->is_logged_in();
			$web_id = base64_decode($id);
			/*$this->db->join('arai_webinar_attendence a','w.w_id = a.w_id','LEFT',FALSE);
				$this->db->where("a.status","Active");	
			$data['webinar_applicant'] = $this->master_model->getRecords("webinar as w",array('w.w_id' => $web_id, 'w.is_deleted' => "0"));*/
			$data['web_id'] 	 = $web_id;
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'webinar/applicant-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		public function applicant_listing() //WEBINAR APPLICANT LISTING : GET SERVER SIDE DATATABLE DATA USING AJAX
		{		
			error_reporting(E_ALL);			
			//$this->check_permissions->is_logged_in();	
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();	
			
			$csrf_test_name = $this->security->get_csrf_hash();			
			$user_id = $this->session->userdata('user_id');	
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;	
			
			$webinarId = $this->input->post('webinarId');
			
			$condition = "SELECT arai_webinar.w_id, arai_webinar.webinar_id, arai_webinar.webinar_name, arai_webinar.webinar_date,
			arai_webinar.webinar_start_time, arai_webinar.webinar_end_time, arai_webinar.webinar_cost_type, arai_webinar.cost_price, arai_webinar.currency, arai_webinar.registration_link, arai_webinar.hosting_link
			, arai_webinar.admin_status , arai_webinar_attendence.user_id FROM arai_webinar
			JOIN arai_webinar_attendence ON arai_webinar.w_id=arai_webinar_attendence.w_id 
			WHERE arai_webinar.is_deleted = '0' AND arai_webinar.user_id = '".$user_id."' AND arai_webinar.w_id = '".$webinarId."'";		
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$result 	= $this->db->query($query)->result();
			$rowCount = $this->getNumData($condition);
			$rowCnt = 0;
			$dataArr = array();		
			if($rowCount > 0){			
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $showResult){
					
					$web_id					= $showResult->w_id;
					$webinar_id				= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$showResult->webinar_id.'</a>';
					$webinar_name			= ucwords($showResult->webinar_name);
					$webinar_date			= $showResult->webinar_date;
					$webinar_start_time			= $showResult->webinar_start_time;
					$webinar_end_time			= $showResult->webinar_end_time;
					$webinar_cost_type		= $showResult->webinar_cost_type;					
					$cost_price = "<span style='white-space:nowrap;'>".$showResult->currency." ".$showResult->cost_price."</span>";									
					$banner_img				= $showResult->banner_img;
					$registration_link		= $showResult->registration_link;
					$hosting_link			= $showResult->hosting_link;
					$webinar_breif_info		= $showResult->webinar_breif_info;
					$webinar_key_points		= $showResult->webinar_key_points;
					$webinar_about_author	= $showResult->webinar_about_author;
					$applicantId			= $showResult->user_id;
					
					if($showResult->admin_status == 'Withdraw'){$showText = 'Withdrawn';} else {$showText = $showResult->admin_status;}
					$admin_status			= '<span id="web-status-'.$web_id.'">'.$showText.'</span>';			
					$DispDate 			= date('d-m-Y', strtotime($webinar_date));
					$DispTime			= "<span style='white-space:nowrap;'>".date('h:i A', strtotime($webinar_start_time))." - ".date('h:i A', strtotime($webinar_end_time))."</span>";
					
					$userDetails	= $this->master_model->getRecords("registration",array('user_id' => $applicantId));
					$fullname		= ucfirst($encrptopenssl->decrypt($userDetails[0]['title']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['last_name']));
					
					$applicationReceived 	= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($web_id)).'" class="applicant-listing">0</a>';	
					$detailView = '<a href="'.base_url('webinar/viewDetail/'.base64_encode($web_id)).'" class="applicant-listing">'.$webinar_name.'</a>';
					$withdraw = '';
					if($showResult->admin_status != 'Withdraw'){
						
						$withdraw				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Withdraw" data-id="'.$web_id.'" class="webinar-withdraw btn btn-info btn-green-fresh"><i class="fa fa-ban" aria-hidden="true"></i></a> ';		
						
					}		
					$delete				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" data-id="'.$web_id.'" class="webinar-remove btn btn-info btn-green-fresh"><i class="fa fa-remove" aria-hidden="true"></i></a> ';			
					$edit				= '<a href="'.base_url('webinar/edit/'.base64_encode($web_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a> ';
					$conCat				= $edit."&nbsp;".$withdraw."&nbsp;".$delete;				
					$i++;					
					$dataArr[] = array(
					$webinar_id,
					$webinar_name,
					$fullname,
					$DispDate,
					$DispTime,
					$cost_price,	
					$hosting_link,
					$admin_status
					);					
			    $rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}				
				} else {			
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);			
				$response['token'] = $csrf_test_name;
			}			
			echo json_encode($response);
			
		}
		
		public function presenter_adding_webinar($arr) //SEND EMAIL TO PRESENTER WHEN WEBINAR ADDED
		{			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application');
			$presenter_webinar = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
			$content = '';
			if(count($presenter_webinar) > 0){
				
				$subject 	 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($presenter_webinar[0]['from_email']);
				//$from_admin 	= 'kamlesh.chaube@esds.co.in';	
				$sendername  	= $setting_table[0]['field_1'];	
				
				// Get Day
				$day = date('l', strtotime($arr['webinar_date']));
				
				$arr_words = ['[FULLNAME]', '[Webinar_Name]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[LINK_WEBINAR_STATUS]'];
				$rep_array = [$arr['presenter_name'],  $arr['webinar_name'],  $arr['webinar_id'], $day,  $arr['webinar_date'],  $arr['webinarTime'],  $arr['webinar_link'] ];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
				//email admin sending code 
				$info_arr=array(
				'to'		=>	$arr['presenter_email'], 		
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$subject,
				'view'		=>  'common-file'
				);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				return true;
			}			
		}
				
		public function adding_webinar_mail_to_admin($arr)// SENT EMAIL TO ADMIN WHEN ADD WEBINAR : ADDED BY SAGAR ON 28-12-2020
		{			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$slug = $encrptopenssl->encrypt('webinar_mail_to_admin_on_application');
			$email_data = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
			$content = '';
			if(count($email_data) > 0)
			{				
				$subject 	 	= $encrptopenssl->decrypt($email_data[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($email_data[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($email_data[0]['from_email']);
				$sendername  	= $setting_table[0]['field_1'];	
				
				// Get Day
				$day = date('l', strtotime($arr['webinar_date']));
				
				$webinar_admin_Link	= '<a href="'.base_url('xAdmin/webinar').'">View</a>';
				
				$arr_words = ['[USERNAME]', '[WEBINAR_NAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[HYPERLINK_TO_WEBINAR_ACCEPT_REJECT_PAGE]'];
				$rep_array = [$arr['presenter_name'],  $arr['webinar_name'],  $arr['webinar_id'], $day,  $arr['webinar_date'],  $arr['webinarTime'], $webinar_admin_Link];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
				//email admin sending code 
				$info_arr=array(
				'to' =>	'jitendra.battise@esds.co.in', //ADMIN MAIL GOES HERE 		
				'cc' =>	'',
				'from' =>	$from_admin,
				'subject'	=> $subject,
				'view' => 'common-file'
				);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				return true;
			}			
		}
		
		public function notification_to_presenter($arr) //SEND NOTIFICATION TO PRESENTOR
		{			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_register_button');
			$presenter_webinar = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
			$content = '';
			if(count($presenter_webinar) > 0){
				
				$subject 	 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($presenter_webinar[0]['from_email']);
				$sendername  	= $setting_table[0]['field_1'];	
				
				// Get Day
				$day = date('l', strtotime($arr['webinar_date']));
				
				$arr_words = ['[FULLNAME]', '[APPLICANTNAME]', '[WEBINARNAME]', '[WEBINARID]'];
				$rep_array = [$arr['presenter_name'],  $arr['applicant_name'],  $arr['webinar_name'],  $arr['webinar_id']];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
				//email admin sending code 
				$info_arr=array(
				'to'		=>	$arr['presenter_email'], //$arr['presenter_email']				
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$subject,
				'view'		=>  'common-file'
				);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				return true;
			}
			
		} // End Function
		
		public function notification_to_user($arr) // SEND NOTIFICATION TO USER
		{			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$slug = $encrptopenssl->encrypt('webinar_mail_to_user_on_register_button');
			$presenter_webinar = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
			$content = '';
			if(count($presenter_webinar) > 0){
				
				$subject 	 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($presenter_webinar[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($presenter_webinar[0]['from_email']);
				$sendername  	= $setting_table[0]['field_1'];	
				
				// Get Day
				$day = date('l', strtotime($arr['webinar_date']));
				
				$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[WEBINAR_LINK]'];
				$rep_array = [$arr['applicant_name'],  $arr['webinar_name'],  $arr['webinar_id'], $day, $arr['webinar_date'], $arr['webinarTime'], $arr['webinar_link']];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
				//email admin sending code 
				$info_arr=array(
				'to'		=>	$arr['applicant_email']	, //$arr['applicant_email']				
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$subject,
				'view'		=>  'common-file'
				);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				return true;
			}
			
		}		
		############# START : WEBINAR SECTION ###############################################################################################
		
		
		public function getNumData($query)// CALCULATE NUM ROWS
		{
			//echo $query;
			return $rowCount = $this->db->query($query)->num_rows();
		}
		
		public function add_question()
		{			
			$this->check_permissions->is_logged_in();	
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->load->library('upload');			
			$user_id = $this->session->userdata('user_id');	
			
			
			$getWebcode = $this->master_model->getRecords("track_question",'','',array('id' => 'DESC'), 0, 1);			
			$last_id	   = @$getWebcode[0]['webinar_code'];
			
			
			if($last_id == ""){				
				$data['webinar_code'] = 'TN-QUE-000001';
				} else {
				$last_code = $last_id + 1;
				$zerofilledNumber = str_pad((string) $last_code, 6, '0', STR_PAD_LEFT); 
				$data['webinar_code'] = 'TN-QUE-'.$zerofilledNumber;
			}
			
			// Check Validation
			$this->form_validation->set_rules('question', 'Question Name', 'required|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$fileArr			= $_FILES;
				$postArr			= $this->input->post();			
				$banner_img  		= $_FILES['banner_img']['name'];
				$question		= $this->input->post('question');
				$tags			= implode(',',$this->input->post('tags[]') );
				
				
				if($banner_img!=""){			
					
					$config['upload_path']      = 'uploads/questions';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
					
					$b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$b_image = $upload_files[0];
						
					}
					
				} // Banner Image End
				
				$banner_names = $b_image[0];	
				
				$insertArr = array(	
				"custum_question_id" 			=>  '',
				"user_id" 				=>  $user_id,
				"question" =>$question,
				"tags"=>$tags,
				"image"=>$banner_names,
				
				);
				$insertQuery = $this->master_model->insertRecord('questions',$insertArr);
				if($insertQuery > 0){				
					
					$getCode = getQestionID($insertQuery);
					$webinar_code = 'TN-QUE-'.$getCode;
					$updateArrID = array('custum_question_id' => $webinar_code);
					$updateQuery = $this->master_model->updateRecord('questions',$updateArrID,array('q_id' => $insertQuery));
					
					// Get Webinar Details
					// $getWebinarDetails = $this->master_model->getRecords("webinar", array("w_id" => $insertQuery));
					// $webinarName 	= ucfirst($getWebinarDetails[0]['webinar_name']);
					// $webinarID 		= $getWebinarDetails[0]['webinar_id'];
					// $webinarDate 	= date('d-m-Y', strtotime($getWebinarDetails[0]['webinar_date']));
					// $webinarTime 	= $getWebinarDetails[0]['webinar_time'];
					// $webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($insertQuery)).'">View</a>';
					
					// // Get User Details 
					// $userDetails = $this->master_model->getRecords("registration", array("user_id" => $user_id));
					// $presenterfullname = ucfirst($encrptopenssl->decrypt($userDetails[0]['title']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($userDetails[0]['last_name']));
					// $presenter_email = $encrptopenssl->decrypt($userDetails[0]['email']);
					
					// $buildEmailArray = array(	
					// 							'webinar_name' 		=> $webinarName,
					// 							'webinar_id' 		=> $webinarID,
					// 							'webinar_date' 		=> $webinarDate,
					// 							'webinar_time' 		=> $webinarTime,
					// 							'webinar_link' 		=> $webinarLink,
					// 							'presenter_name' 	=> $presenterfullname,
					// 							'presenter_email' 	=> $presenter_email
					// 						);
					// // Send email to presenter
					// $this->presenter_adding_webinar($buildEmailArray);
					
					// Log Data Added 
					$filesData 			= $_FILES;
					$json_data 			= array_merge($postArr,$filesData);
					$json_encode_data 	= json_encode($json_data);				
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 			=> $user_id,
					'action_name' 		=> "Add Question",
					'module_name'		=> 'Question Front',
					'store_data'		=> $json_encode_data,
					'ip_address'		=> $ipAddr,
					'createdAt'			=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					// Successfully Redirect
					$this->session->set_flashdata('success','Webinar has been successfully posted');	
					redirect(site_url('webinar/myquestions'));
				}
			}
			$data['tags'] = $this->master_model->getRecords("forum_tags",array('status'=>'Active'));
			
			$data['page_title'] 	 = 'Webinar';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'forum/add';
			$this->load->view('front/front_combo',$data);
		}
		
		function myquestions()
		{
			$this->check_permissions->is_logged_in();
			$user_id = $this->session->userdata('user_id');			
			$data['webinar_listing'] = $this->master_model->getRecords("questions",array('user_id' => $user_id),'',array('q_id' => 'DESC'));
			$data['page_title'] 	 = 'QA forum';
			$data['submodule_name']  = '';	
			
			$data['middle_content']	 = 'forum/listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// Ajax Webinar	listing
		public function question_listing()
		{				
			error_reporting(E_ALL);			
			//$this->check_permissions->is_logged_in();		
			$csrf_test_name = $this->security->get_csrf_hash();			
			$user_id = $this->session->userdata('user_id');	
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;	
			
			$condition = "SELECT * FROM arai_questions WHERE is_deleted = '0' AND user_id = '".$user_id."' ORDER BY q_id DESC";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$result 	= $this->db->query($query)->result();
			$rowCount = $this->getNumData($condition);
			$rowCnt = 0;
			$dataArr = array();
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $showResult){
					
					$q_id     = $showResult->q_id;
					$custum_q_id     = $showResult->custum_question_id;
					$question = ucwords($showResult->question);
					$tags     = $showResult->tags;
					
					$this->db->select('tag_name');
					$this->db->where("id IN (".$tags.") AND status = 'Active'");
					$tags_data = $this->master_model->getRecords('forum_tags');
					
					
					$tag_names = '';
					if (count($tags_data)) {
						
						foreach ($tags_data as $key => $value) { 													
							$tag_names.= '<span class="badge badge-pill badge-info">'.$value['tag_name'].' </span>';
						}  
						
					}
					
					$delete				= '<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" data-id="'.$q_id.'" class="question-remove btn btn-info btn-green-fresh"><i class="fa fa-remove" aria-hidden="true"></i></a> ';			
					$edit				= '<a href="'.base_url('webinar/question_edit/'.base64_encode($q_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a> ';
					
					$view				= '<a href="'.base_url('webinar/applicantListing/'.base64_encode($q_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a> ';				
					$conCat				= $edit."&nbsp;".$delete."&nbsp;";				
					$i++;					
					$dataArr[] = array(
					$custum_q_id,
					$question,
					$tag_names,	
					$conCat
					);					
			    $rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}				
				} else {			
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);			
				$response['token'] = $csrf_test_name;
			}			
			echo json_encode($response);
			
		} 
		
		
		public function question_edit($q_id)
		{			
			$this->check_permissions->is_logged_in();	
			
			$q_id = base64_decode($q_id);
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->load->library('upload');			
			$user_id = $this->session->userdata('user_id');	
			
			// Check Validation
			$this->form_validation->set_rules('question', 'Question Name', 'required|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				
				$fileArr			= $_FILES;
				$postArr			= $this->input->post();			
				$banner_img  		= $_FILES['banner_img']['name'];
				$question		= $this->input->post('question');
				$tags			= implode(',',$this->input->post('tags[]') );
				
				$update_arr = array(	
				"question" =>$question,
				"tags"=>$tags,
				);
				
				
				if($banner_img!=""){			
					
					$config['upload_path']      = 'uploads/questions';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
					
					$b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$b_image = $upload_files[0];
						$banner_names = $b_image[0];
						$update_arr['image'] = $banner_names;
						
					}
					
				} // Banner Image End
				
				
				
				
				$updateQuery = $this->master_model->updateRecord('questions',$update_arr,array('q_id'=>$q_id) );
				if($updateQuery > 0){				
					// Log Data Added 
					$filesData 			= $_FILES;
					$json_data 			= array_merge($postArr,$filesData);
					$json_encode_data 	= json_encode($json_data);				
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 			=> $user_id,
					'action_name' 		=> "Edit Question",
					'module_name'		=> 'Question Front',
					'store_data'		=> $json_encode_data,
					'ip_address'		=> $ipAddr,
					'createdAt'			=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					// Successfully Redirect
					$this->session->set_flashdata('success','Webinar has been successfully posted');	
					redirect(site_url('webinar/myquestions'));
				}
			}
			$data['tags'] = $this->master_model->getRecords("forum_tags",array('status'=>'Active'));
			$data['record'] = $this->master_model->getRecords('questions',array('q_id'=>$q_id));
			
			$data['page_title'] 	 = 'QA Forum';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'forum/edit';
			$this->load->view('front/front_combo',$data);
		}
		
		
		public function get_client_ip() 
		{
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}	
		
	}		