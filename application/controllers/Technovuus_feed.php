<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Technovuus_feed extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');	
			$this->load->library('Opensslencryptdecrypt'); 
			$this->load->helper('auth');
			$this->load->helper('text');
			//$this->check_permissions->is_logged_in();
			ini_set('upload_max_filesize', '100M');  
			ini_set('post_max_size', '100M');  
		} 
		
		public function index()
		{
			//$module_id = 8;
			//$this->check_permissions->is_authorise($module_id);   
			
			ini_set('display_errors', '1');
			ini_set('display_startup_errors', '1');
			error_reporting(E_ALL);	
			
			$this->check_permissions->is_logged_in();			
			$encrptopenssl =  New Opensslencryptdecrypt();	
						
			$data['page_title'] 	 = 'TechNovuus Feed';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'technovuus_feed/technovuus_feed_all';
			$this->load->view('front/front_combo',$data);
		}
	}		