<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Tasks extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->model('Common_model_sm');
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');		 	
			if($this->login_user_id == "") { redirect(site_url()); }
			
			// ini_set('display_errors', '1');
			// ini_set('display_startup_errors', '1');
			// error_reporting(E_ALL);
			
			//NEED TO ADD PERMISSION CODE FOR APPLY CHALLENGE
		}
		
		public function index()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			if(isset($_POST) && isset($_POST['challenge_id']) && $_POST['challenge_id'] != '')
			{
				$clear_session['TEAM_ACCEPT_USER_ID'] = $clear_session['TEAM_ACCEPT_CHALLENGE_ID'] = '';
				$this->session->set_userdata($clear_session);
				
				$new_session['TEAM_ACCEPT_USER_ID'] = $this->login_user_id;
				$new_session['TEAM_ACCEPT_CHALLENGE_ID'] = $_POST['challenge_id'];
				$this->session->set_userdata($new_session);
				
				$result['flag'] = "success";
				$result['csrf_test_name'] = $csrf_test_name;
				echo json_encode($result);
			}
			else { redirect(site_url('challenge')); }
		}
		
		############# START ############################
		public function create( $team_id=0)
		{
			$this->check_permissions->is_admin_approved();
			$this->check_permissions->is_profile_complete();
			$tasks_setting = $this->master_model->getRecords('arai_task_setting',array("id"=>'1'));
			$min_team_custom=$tasks_setting[0]['min_no'];
			$max_team_custom=$tasks_setting[0]['max_no'];
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$challenge_id = 0;
			if($team_id != '0') { $encoded_team_id = $team_id; $team_id = base64_decode($team_id); } else { $encoded_team_id = 0; }
			
			$data['default_disp_photo'] = $default_disp_photo = base_url('assets/profile_picture/user_dummy.png');
			
			$skillset_data = $team_slot_data = $slot_application_data = $team_files_data = $team_files_private_data = array();
			
			//START : CHECK FOR THE CONDITION AS ADD OR UPDATE			
			if($team_id == '0') { $mode = 'Add'; }
			else
			{
				$team_data = $this->master_model->getRecords('arai_byt_teams',array("team_id"=>$team_id));
				$data['team_data'] = $team_data;/* echo $this->db->last_query(); exit; */				
				if(count($team_data) == 0) 
				{ 
					$this->session->set_flashdata('error','Invalid team selection'); redirect(site_url('myteams/myCreatedteams')); 
				}
				else 
				{
					if($team_data[0]['user_id'] != $this->login_user_id)
					{
						$this->session->set_flashdata('error','Invalid team selection'); redirect(site_url('myteams/myCreatedteams')); 
					}
					else if($team_data[0]['c_id'] != $challenge_id)
					{
						$this->session->set_flashdata('error','Team is not belongs the selected challenge'); redirect(site_url('myteams/myCreatedteams')); 
					}
					else if($team_data[0]['is_deleted'] != '0')
					{
						$this->session->set_flashdata('error','Team is deleted'); redirect(site_url('myteams/myCreatedteams')); 
					}
					else if($team_data[0]['challenge_owner_status'] == 'Approved')
					{
						$this->session->set_flashdata('warning','You can not update your team as it is already approved'); redirect(site_url('myteams/myCreatedteams')); 
					}
					else if($team_data[0]['challenge_owner_status'] == 'Rejected')
					{
						$this->session->set_flashdata('warning','You can not update your team as it is rejected'); redirect(site_url('myteams/myCreatedteams')); 
					}
					/* else if($team_data[0]['team_status'] != 'Incomplete')
						{
						$this->session->set_flashdata('error','Team is completed'); redirect(site_url('myteams/myCreatedteams')); 
					} */
					else if($team_data[0]['apply_status'] == 'Withdrawn')
					{
						$this->session->set_flashdata('error','Team is already withdrawn for this challenge'); redirect(site_url('myteams/myCreatedteams')); 
					}					
					else if($team_data[0]['status'] != 'Active')
					{
						$this->session->set_flashdata('warning','Team is not active'); redirect(site_url('myteams/myCreatedteams')); 
					}
					
					$mode = "Update"; 
					$data['team_files_data'] = $this->master_model->getRecords('arai_byt_team_files',array("team_id"=>$team_id, "user_id"=>$this->login_user_id, "c_id"=>$challenge_id, "is_deleted"=>'0', "file_type"=>'Public'), '', array('file_id'=>'ASC'));
					$data['team_files_private_data'] = $this->master_model->getRecords('arai_byt_team_files',array("team_id"=>$team_id, "user_id"=>$this->login_user_id, "c_id"=>$challenge_id, "is_deleted"=>'0', "file_type"=>'Private'), '', array('file_id'=>'ASC'));
					
					/********** THIS IS USE TO DISPLAY APPROVED MEMBER DATA IN EDIT TEAM MEMBER SLOT ***************/
					$data['team_slot_data'] = $team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array("team_id"=>$team_id, "user_id"=>$this->login_user_id, "c_id"=>$challenge_id, "is_deleted"=>'0'), '', array('slot_id'=>'ASC'));
					
					$team_slot_new_arr = array();
					if(count($team_slot_data) > 0)
					{
						foreach($team_slot_data as $res)
						{
							$slot_temp_arr['slot_id'] = $res['slot_id'];
							$slot_temp_arr['user_id'] = $res['user_id'];
							$slot_temp_arr['c_id'] = $res['c_id'];
							$slot_temp_arr['team_id'] = $res['team_id'];
							$slot_temp_arr['slot_type'] = $res['slot_type'];
							$slot_temp_arr['skills'] = $res['skills'];
							$slot_temp_arr['other_skill'] = $res['other_skill'];
							$slot_temp_arr['role_name'] = $res['role_name'];
							$slot_temp_arr['is_deleted'] = $res['is_deleted'];
							$slot_temp_arr['hiring_status'] = $res['hiring_status'];
							
							$DispUserName = '&nbsp;';
							$profile_img = base_url('assets/profile_picture/user_dummy.png');
							if($res['hiring_status'] == 'Closed')
							{
								$this->db->select('sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id, sa.status, r.title, r.first_name, r.last_name, r.user_category_id');
								$this->db->join('arai_registration r', 'r.user_id = sa.apply_user_id','INNER', FALSE);
								$slotApplicants = $this->master_model->getRecords('arai_byt_slot_applications sa',array("sa.c_id"=>$res['c_id'], "sa.team_id"=>$res['team_id'], "sa.slot_id"=>$res['slot_id'], "sa.status"=>'Approved', "sa.is_deleted"=>'0'));
								
								if(count($slotApplicants) > 0) 
								{
									$DispUserName = $encrptopenssl->decrypt($slotApplicants[0]['title'])." ".$encrptopenssl->decrypt($slotApplicants[0]['first_name'])." ".$encrptopenssl->decrypt($slotApplicants[0]['last_name']);
									
									if($slotApplicants[0]['user_category_id'] == 1)
									{
										$this->db->select('profile_picture');
										$profileImgdata = $this->master_model->getRecords("student_profile",array('user_id'=>$slotApplicants[0]['apply_user_id']));
										
										if (isset($profileImgdata[0]['profile_picture']) &&  $profileImgdata[0]['profile_picture'] !='' ) 
										{ 
											$profile_img = base_url('assets/profile_picture/'.$profileImgdata[0]['profile_picture']);
										}
									}
									else
									{
										$this->db->select('org_logo');
										$profileImgdata = $this->master_model->getRecords("arai_profile_organization",array('user_id'=>$slotApplicants[0]['apply_user_id']));
										
										if(isset($profileImgdata[0]['org_logo']) &&  $profileImgdata[0]['org_logo'] !='' ) 
										{ 
											$profile_img = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profileImgdata[0]['org_logo']));
										}
									}
								}
							}
							
							$slot_temp_arr['DispUserName'] = $DispUserName;
							$slot_temp_arr['profile_img'] = $profile_img;
							
							$team_slot_new_arr[] = $slot_temp_arr;
						}
					}
					$data['team_slot_new_arr'] = $team_slot_new_arr;					
				}
			}
			//END : CHECK FOR THE CONDITION AS ADD OR UPDATE			
			
			if ($mode=='Add') 
			{ 
				
				$this->check_permissions->is_authorise(12); 
				
			}
			else { $this->check_permissions->is_authorise(18); }
			
			$login_user_data = $this->master_model->getRecords('arai_registration',array("user_id"=>$this->login_user_id)); 
			
			if(count($login_user_data) == 0) //Check logged in user exist or not
			{ 
				$this->session->set_flashdata('error','User not exist'); 
				redirect(site_url() ); 
			}
			else 
			{
				if($login_user_data[0]['status'] == 'Block')
				{
					$this->session->set_flashdata('warning','Your account is blocked'); redirect(site_url() ); 
				}
			}
			
			$data['min_team'] = $tasks_setting[0]['min_no'];
			$data['max_team'] = $tasks_setting[0]['max_no'];		
			
			$data['team_banner_error'] = $data['CustomVAlidationErr'] = '';
			$file_upload_flag = $slotCntValidationFlag = $valid_data_flag = 0;
			
			if(isset($_POST) && count($_POST) > 0) //AFTER FORM SUBMIT ACTION
			{	
				$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required|xss_clean', array('required'=>'Please enter the %s', 'alpha_numeric_spaces'=>'Please enter only alpha numeric character in %s'));//alpha_numeric_spaces|
				$this->form_validation->set_rules('team_details', 'Team Details', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				$this->form_validation->set_rules('share_files', 'Share Files', 'trim|xss_clean');
				//$this->form_validation->set_rules('xxx', 'xxx', 'trim|required|xss_clean');
				
				//CHECK SERVER SIDE CUSTOM VALIDATION CODE
				$chkSlotCnt = count($this->input->post('team_role')); 
				if($min_team_custom <= $chkSlotCnt && $chkSlotCnt <= $max_team_custom) { } 
				else { $slotCntValidationFlag = 1; }
				
				if($mode == "Add")
				{
					$team_type_validation = $this->input->post('team_type'); 
					if(count($team_type_validation) > 0) { foreach($team_type_validation as $res) { if($res == "") { $valid_data_flag = 1; } } } else { $valid_data_flag = 1; }
				}
				
				$team_skillset_validation = $this->input->post('team_skillset'); 
				if(count($team_skillset_validation) > 0) { foreach($team_skillset_validation as $res) { if($res == "") { $valid_data_flag = 1; } } } else { $valid_data_flag = 1; }
				
				$team_role_validation = $this->input->post('team_role'); 				
				if(count($team_role_validation) > 0) { foreach($team_role_validation as $res) { if($res == "") { $valid_data_flag = 1; } } } else { $valid_data_flag = 1; }
				
				if($this->form_validation->run())
				{
					if($slotCntValidationFlag == 1)
					{
						$data['CustomVAlidationErr'] = 'Please select team slot between '.$min_team_custom.' to '.$max_team_custom.' only';
					}
					else if($valid_data_flag == 1)
					{
						$data['CustomVAlidationErr'] = 'Please enter valid data';
					}
					else
					{
						//echo $this->convert_number_format("10","4");
						//echo "<pre>"; print_r($challenge_data); echo "</pre>";
						//echo "<pre>"; print_r($_POST); echo "</pre>"; exit;
						//echo "<pre>"; print_r($_FILES); echo "</pre>"; exit;
						
						if($_FILES['team_banner']['name'] != "") //UPLOAD TEAM BANNER IMAGE
						{
							$team_banner = $this->Common_model_sm->upload_single_file("team_banner", array('png','jpg','jpeg','gif'), "team_banner_".date("YmdHis"), "./uploads/byt", "png|jpeg|jpg|gif");
							if($team_banner['response'] == 'error')
							{
								$data['team_banner_error'] = $team_banner['message'];
								$file_upload_flag = 1;
							}
							else if($team_banner['response'] == 'success')
							{
								$add_data['team_banner'] = $team_banner['message'];	
								/* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
							}
						}
						
						if($file_upload_flag == 0)
						{	
							
							$add_data['task_name'] = $this->input->post('task_name');
							$add_data['team_name'] = $this->input->post('team_name');
							$add_data['brief_team_info'] = $this->input->post('team_details');
							$add_data['proposed_approach'] = $this->input->post('proposed_approach');
							$add_data['additional_information'] = $this->input->post('additional_information');
							$add_data['team_type'] = 'task';
							
							if($this->input->post('launch_date')!=""){
								$s_date = date('Y-m-d',strtotime($this->input->post('launch_date')));
								} else{
								$s_date = NULL;
							}
							
							if($this->input->post('close_date')!=""){
								$e_date = date('Y-m-d',strtotime($this->input->post('close_date')));
								} else{
								$e_date = NULL;
							}
							
							$add_data['task_start_date'] = $s_date;
							$add_data['task_end_date'] = $e_date;
							
							$add_data['share_files'] = 'no';
							$share_files = $this->input->post('share_files');
							if(isset($share_files) && $share_files == 'on') { $add_data['share_files'] = 'yes'; }
							
							//$add_data['applied_challenge_status'] = 0;
							//$add_data['team_status'] = 0;
							
							if($mode == 'Add') //INSERT TEAM RECORD
							{
								$add_data['user_id'] = $this->login_user_id;
								$add_data['c_id'] = $challenge_id;
								$add_data['post_challenge_user_id'] = 0;
								$add_data['created_on'] = date("Y-m-d H:i:s");
								$build_team_id = $this->master_model->insertRecord('byt_teams',$add_data,TRUE);
							}
							else //UPDATE TEAM RECORD
							{
								$add_data['modified_on'] = date("Y-m-d H:i:s");
								$this->master_model->updateRecord('byt_teams',$add_data,array('team_id' => $team_id));
								$build_team_id = $team_id;
							}
							
							
							if($build_team_id != '')
							{
								
								if($mode == 'Add')
								{
									$custom_team_id='TEAM-'.$this->convert_number_format($build_team_id,"4");
									$custom_task_id='TASK-'.$this->convert_number_format($build_team_id,"4");
									$this->master_model->updateRecord('byt_teams',array('custom_team_id'=>$custom_team_id,'custom_task_id'=>$custom_task_id),array('team_id' => $build_team_id));
									
									
								}
								
								
								//START : TEAM SLOT INSERT/UPDATE
								$team_slot_id = $this->input->post('team_slot_id'); //echo "<pre>"; print_r($team_slot_id); echo "</pre>"; //exit;
								$team_type = $this->input->post('team_type'); //echo "<pre>"; print_r($team_type); echo "</pre>"; //exit;
								$team_skillset = $this->input->post('team_skillset'); //echo "<pre>"; print_r($team_skillset); echo "</pre>"; //exit;
								$other_skill = $this->input->post('other_skill');
								$team_role = $this->input->post('team_role');
								
								$byt_member_slot_cnt = $this->input->post('byt_member_slot_cnt'); 
								for($i=0; $i < $byt_member_slot_cnt; $i++)
								{	
									$sel_type_val = $team_type['team_type'.$i]; 
									$sel_team_slot_id = $team_slot_id['team_slot_id'.$i]; 
									if(isset($sel_team_slot_id) && $sel_team_slot_id != '')
									{
										//START : CODE FOR ADDING NEW SKILLSET IN MASTER
										$skill_set_str = '';
										$skill_set_arr = $team_skillset['team_skillset'.$i];
										if($other_skill['other_skill'.$i] != '') { $skill_set_arr[]=$other_skill['other_skill'.$i]; }						 
										foreach(array_unique($skill_set_arr) as $skill_res)
										{
											$chk_skill_id = $this->master_model->getRecords('skill_sets',array("CAST(id AS CHAR) = "=>$skill_res)); //echo $this->db->last_query(); exit;
											if(count($chk_skill_id) > 0)
											{
												$skillSetName = strtolower($encrptopenssl->decrypt($chk_skill_id[0]['name']));
												if($skillSetName != 'other') { $skill_set_str .= $skill_res.","; }
											}
											else
											{
												$chk_skill_name = $this->master_model->getRecords('skill_sets',array("name"=>$encrptopenssl->encrypt($skill_res))); 
												if(count($chk_skill_name) > 0)
												{
													$skill_set_str .= $chk_skill_name[0]['id'].",";
												}
												else
												{
													$add_skillSet['name'] = $encrptopenssl->encrypt($skill_res);
													$add_skillSet['profile_id'] = $this->login_user_id;
													$add_skillSet['status'] = 'Block';
													$add_skillSet['createdAt'] = date("Y-m-d H:i:s");
													$newSkillId = $this->master_model->insertRecord('skill_sets',$add_skillSet,TRUE);
													$skill_set_str .= $newSkillId.",";
													
													$skillset_data[] = $add_skillSet;//FOR LOG
												}
											}
										}
										//END : CODE FOR ADDING NEW SKILLSET IN MASTER
										
										$add_data_skill['skills'] = rtrim($skill_set_str,","); 
										//if($other_skill['other_skill'.$i] != "") { $add_data_skill['other_skill'] = $other_skill['other_skill'.$i]; } else { $add_data_skill['other_skill'] = ""; }
										$add_data_skill['role_name'] = $team_role['team_role'.$i];
										
										if($team_slot_id['team_slot_id'.$i] == '0')
										{
											$add_data_skill['user_id'] = $this->login_user_id;
											$add_data_skill['c_id'] = $challenge_id;
											$add_data_skill['team_id'] = $build_team_id;
											$add_data_skill['slot_type'] = $sel_type_val;
											$add_data_skill['created_on'] = date("Y-m-d H:i:s");
											$slot_id = $this->master_model->insertRecord('byt_team_slots',$add_data_skill,TRUE);
										}
										else
										{										
											$slot_id = $team_slot_id['team_slot_id'.$i];
											$add_data_skill['modified_on'] = date("Y-m-d H:i:s");
											$this->master_model->updateRecord('byt_team_slots',$add_data_skill,array('slot_id' => $slot_id));
										}					 				
										
										$team_slot_data[] = $add_data_skill; //FOR LOG										
										
										if($i===0 && $mode == 'Add') //BY DEFAULT, INSERT TEAM CREATORS APPLICATION IN FIRST SLOT APPLICATION 
										{
											$add_data_apllication['c_id']=$challenge_id;
											$add_data_apllication['team_id']=$build_team_id;
											$add_data_apllication['slot_id']=$slot_id;
											$add_data_apllication['apply_user_id']=$this->login_user_id;
											$add_data_apllication['status']='Approved';
											$this->master_model->insertRecord('byt_slot_applications',$add_data_apllication,TRUE);
											$slot_application_data[] = $add_data_apllication; //FOR LOG									
											
											$this->master_model->updateRecord('byt_team_slots',array('hiring_status'=>'Closed'),array('slot_id' => $slot_id));
										}
									}
								}
								
								
								//END : TEAM SLOT INSERT/UPDATE
								
								//START : INSERT TEAM FILES PUBLIC
								$team_files = $_FILES['team_files'];
								if(count($team_files) > 0)
								{
									for($i=0; $i < count($team_files['name']); $i++)
									{
										if($team_files['name'][$i] != '')
										{
											$team_file = $this->Common_model_sm->upload_single_file('team_files', array('png','jpg','jpeg','gif','pdf','xls','xlsx','doc','docx'), "team_files_".$build_team_id."_".date("YmdHis").rand(), "./uploads/byt", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx",'1',$i);
											
											if($team_file['response'] == 'error'){ }
											else if($team_file['response'] == 'success')
											{
												$add_file_data['user_id'] = $this->login_user_id;
												$add_file_data['c_id'] = $challenge_id;
												$add_file_data['team_id'] = $build_team_id;
												$add_file_data['file_name'] = ($team_file['message']);
												$add_file_data['file_type'] = "Public";
												$add_file_data['created_on'] = date("Y-m-d H:i:s");
												$this->master_model->insertRecord('byt_team_files',$add_file_data,TRUE);
												$team_files_data[] = $add_file_data; //FOR LOG
											}
										}
									}
								}						
								//END : INSERT TEAM FILES PUBLIC
								
								//START : INSERT TEAM FILES PRIVATE
								$team_files_private = $_FILES['team_files_private'];
								if(count($team_files_private) > 0)
								{
									for($i=0; $i < count($team_files_private['name']); $i++)
									{
										if($team_files_private['name'][$i] != '')
										{
											$team_file = $this->Common_model_sm->upload_single_file('team_files_private', array('png','jpg','jpeg','gif','pdf','xls','xlsx','doc','docx'), "team_files_private_".$build_team_id."_".date("YmdHis").rand(), "./uploads/byt", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx",'1',$i);
											
											if($team_file['response'] == 'error'){ }
											else if($team_file['response'] == 'success')
											{
												$add_file_data['user_id'] = $this->login_user_id;
												$add_file_data['c_id'] = $challenge_id;
												$add_file_data['team_id'] = $build_team_id;
												$add_file_data['file_name'] = ($team_file['message']);
												$add_file_data['file_type'] = "Private";
												$add_file_data['created_on'] = date("Y-m-d H:i:s");
												$this->master_model->insertRecord('byt_team_files',$add_file_data,TRUE);
												$team_files_data[] = $add_file_data; //FOR LOG
											}
										}
									}
								}						
								//END : INSERT TEAM FILES PRIVATE
								
								if($mode == 'Add')
								{
									$invite_email = $this->input->post('invite_email');
									if (count($invite_email)) 
									{				
										//START : SEND INVITATION MAIL CODE GOES HERE
										$email_info = $this->get_team_info_for_mail($build_team_id);
										
										$email_send='';
										$slug = $encrptopenssl->encrypt('byt_invitaton_mail_create_team_form_tasks');
										$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
										$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
										$sub_content = '';
										
										if(count($subscriber_mail) > 0)
										{
											$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
											$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
											$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);                
											$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
											$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
											
											$tasks_teams_link = site_url('tasks/taskSearch');
											$task_team_listing_url = '<a href='.$tasks_teams_link.' target="_blank">here</a>';
											$site_url = '<a href='.site_url().' target="_blank">'.site_url().'</a>';
											
											$arr_words = [
											'[team_owner_name]',
											'[Team_Name]',
											'[Team_ID]',
											'[hyperlink_to_techNovuus_home_page]',
											'[hyperlink_to_task_teams_listing]'
											];
											
											$rep_array = [
											$email_info['team_owner_name'],
											$email_info['team_name'],
											$email_info['team_id'],
											$site_url,
											$task_team_listing_url
											
											];
											
											$sub_content = str_replace($arr_words, $rep_array, $desc);
										}
										//END : SEND INVITATION MAIL CODE GOES HERE
										
										for ($i=0; $i < count($invite_email) ; $i++) 
										{ 										
											if(count($subscriber_mail) > 0)
											{
												$info_array=array(
												'to' => $invite_email[$i],                
												'cc' => '',
												'from' => $fromadmin,
												'subject' => $subject_title,
												'view' => 'common-file'
												);
												
												$other_infoarray = array('content' => $sub_content); 
												$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
											}
											
											$add_email['team_id'] = $build_team_id;
											$add_email['email_id'] = $invite_email[$i];
											$add_email['invite_message'] = '';
											$add_email['created_on'] = date("Y-m-d H:i:s");
											$this->master_model->insertRecord('arai_byt_team_invitation',$add_email,TRUE);											
											
										}
									}
								}
								
								if($mode == 'Add') 
								{ 
									$this->session->set_flashdata('success','Your task team has been formed.');
									
									
									$this->mail_send_to_team_owner($build_team_id);
									$this->mail_send_to_admin($build_team_id);
								}
								else 
								{ 
									$this->session->set_flashdata('success','You have successfully updated your team.'); 
								}
							}
							else
							{
								$this->session->set_flashdata('error','Error occurred. Please try after sometime.');
							}
							
							//START : INSERT LOG 
							$fileArr = $_FILES;
							$postArr = $this->input->post();
							$log_data['post_data'] 	= array_merge($fileArr,$postArr);
							$log_data['team_data'] 	= $add_data;
							$log_data['skillset_data'] 	= $skillset_data;
							$log_data['team_slot_data'] 	= $team_slot_data;
							$log_data['slot_application_data'] 	= $slot_application_data;
							$log_data['team_files_data'] 	= $team_files_data;
							$log_data['team_files_private_data'] 	= $team_files_private_data;
							
							$logDetails['user_id'] = $this->login_user_id;
							$logDetails['module_name'] = 'Team : create';
							$logDetails['store_data'] = json_encode($log_data);
							$logDetails['ip_address'] = $this->get_client_ip();
							$logDetails['createdAt'] = date('Y-m-d H:i:s');
							
							if($mode == 'Add') { $logDetails['action_name'] = 'ADD New Team'; }
							else { $logDetails['action_name'] = 'Update Existing Team'; }
							$logData = $this->master_model->insertRecord('logs',$logDetails);
							//END : INSERT LOG
							
							redirect(site_url('myteams/myCreatedteams'));
						}
					}
				}
			}
			
			//START : DISPLAY DEFAULT USER IMAGE FOR FIRST TEAM MEMBER
			$disp_photo = $default_disp_photo;
			$disp_skill_sets = array();
			if($login_user_data[0]['user_category_id'] == 2)//Organization
			{
				$profile_info = $this->master_model->getRecords('arai_profile_organization',array("user_id"=>$this->login_user_id), 'org_logo'); 
				if(!empty($profile_info) && isset($profile_info[0]['org_logo']) && $profile_info[0]['org_logo'] != "") 
				{ 
					$disp_photo = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profile_info[0]['org_logo']));
				}
			}
			else
			{
				$profile_info = $this->master_model->getRecords('arai_student_profile',array("user_id"=>$this->login_user_id), 'skill_sets, profile_picture');
				if(!empty($profile_info) && isset($profile_info[0]['profile_picture']) && $profile_info[0]['profile_picture'] != "") 
				{ 
					$disp_photo = base_url('assets/profile_picture/'.$profile_info[0]['profile_picture']);
				}
				
				if(!empty($profile_info) && isset($profile_info[0]['skill_sets']) && $profile_info[0]['skill_sets'] != "")
				{
					$skill_sets = $encrptopenssl->decrypt($profile_info[0]['skill_sets']); 
					if($skill_sets != "")
					{
						$explode_skillset = explode(",",$skill_sets);
						$disp_skill_sets = $explode_skillset;
					}
				}
			}
			//END : DISPLAY DEFAULT USER IMAGE FOR FIRST TEAM MEMBER
			
			//START : GET SKILL SET DATA
			$user_all_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array("user_id"=>$this->login_user_id));
			$chkTeamSlotIdsStr = '00,';
			if(count($user_all_slot_data) > 0)
			{
				foreach($user_all_slot_data as $teamSlot)
				{
					$chkTeamSlotIdsStr .= $teamSlot['skills'].',';
				}				
			}
			$chkTeamSlotIdsStr = rtrim($chkTeamSlotIdsStr,",");					
			$this->db->where("status = 'Active' OR (id IN (".$chkTeamSlotIdsStr.")) OR profile_id = '".$this->login_user_id."'");		
			$data['skill_sets_data'] = $this->master_model->array_sorting($this->master_model->getRecords('arai_skill_sets'), array('id'), 'name'); 
			//echo $this->db->last_query(); exit;
			//END : GET SKILL SET DATA
			
			//$data['role_data'] = $this->master_model->array_sorting($this->master_model->getRecords('arai_byt_role_master',array("status"=>'Active')), array('id'), 'name'); 
			
			$data['mode'] = $mode;
			$data['challenge_id'] = '';
			$data['encoded_team_id'] = $encoded_team_id;
			
			$data['login_user_data'] = $login_user_data;
			
			$data['disp_photo'] = $disp_photo;
			$data['disp_skill_sets'] = $disp_skill_sets;
			$data['page_title'] = 'Apply For Challenge';
			$data['type_data'] = $this->master_model->getRecords('arai_type_master',array( "status"=>'Active')); 
			$data['middle_content'] = 'tasks/create_team';
			$this->load->view('front/front_combo',$data);
		}
		
		function mail_send_to_team_owner($build_team_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$email_info = $this->get_team_info_for_mail($build_team_id);   
			$email_send='';
			$slug = $encrptopenssl->encrypt('byt_mail_to_owner_on_create_task_team');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$sub_content = '';
			
			if(count($subscriber_mail) > 0 && count($email_info) > 0)
			{
				$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);                
				$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				//echo "<pre>"; print_r($email_info);	echo "</pre>";
				//echo $email_info['hyperlink_to_owner_view']; exit;
				/*echo "<pre>"; print_r($desc); //exit;  */	
				
				
				$arr_words = [
				'[team_owner_name]',
				'[Team_Name]',
				'[Team_ID]',
				'[hyperlink_to_owner_view]'
				];
				
				$rep_array = [
				$email_info['team_owner_name'],
				$email_info['team_name'],
				$email_info['team_id'],
				$email_info['hyperlink_to_owner_view']
				];
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				
				$info_array=array(
				'to' => $email_info['team_owner_email'],          
				'cc' => '',
				'from' => $fromadmin,
				'subject' => $subject_title,
				'view' => 'common-file'
				);
				
				
				$other_infoarray    =    array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			}		
		}
		
		function mail_send_to_admin($build_team_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$email_info = $this->get_team_info_for_mail($build_team_id);   
			$email_send='';
			$slug = $encrptopenssl->encrypt('byt_mail_to_admin_on_task_team_create');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$sub_content = '';
			
			if(count($subscriber_mail) > 0 && count($email_info) > 0)
			{
				$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);                
				$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$admin_email = $encrptopenssl->decrypt($setting_table[0]['field_2']);
				$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				//echo "<pre>"; print_r($email_info);	echo "</pre>";
				//echo $email_info['hyperlink_to_owner_view']; exit;
				//echo "<pre>"; print_r($desc); exit;  /**/	
				
				// $arr_words_sub = ['[Team_name]','[Challenge_name]'];
				// $rep_array_sub = [$email_info['team_name'],$email_info['challenge_title'] ];
				// $subject_title_new = str_replace($arr_words_sub, $rep_array_sub, $subject_title);
				
				$arr_words = [ '[team_owner_name]','[Team_Name]','[Team_ID]' ]; 
				$rep_array = [
				$email_info['team_owner_name'],
				$email_info['team_name'],
				$email_info['team_id']
				];
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				//echo $sub_content; exit;
				
				$info_array=array(
				// 'to' => $admin_email,
				'to' =>'vishal.phadol@esds.co.in',          
				'cc' => '',
				'from' => $fromadmin,
				'subject' => $subject_title,
				'view' => 'common-file'
				);
				$other_infoarray = array('content' => $sub_content); 
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			}		
		}
		
		function get_team_info_for_mail($team_id="")
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$team_details = $this->master_model->getRecords('arai_byt_teams',array('team_id'=>$team_id));
			
			if(count($team_details) > 0)
			{
				$user_id = $this->session->userdata('user_id');
				$this->db->select('title,first_name,middle_name,last_name,email');
				$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$team_owner_data = $this->master_model->getRecords("registration",array('user_id'=>$team_details[0]['user_id'] ));
				$user_arr = array();
				if(count($user_data) > 0)
				{
					foreach($user_data as $row_val)
					{	
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$user_arr[] = $row_val;
					}
				}
				
				
				$owner_arr = array();
				if(count($team_owner_data))
				{	
					foreach($team_owner_data as $row_val)
					{									
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$owner_arr[] = $row_val;
					}
				}
				$logged_in_user_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$team_owner_name = $owner_arr[0]['title']." ".$owner_arr[0]['first_name']." ".$owner_arr[0]['last_name'];
				
				
				$t_link = base_url('myteams/team_details_owner/').base64_encode( $team_details[0]['team_id'] );
				$owner_page_link = '<a href='.$t_link.' target="_blank">here</a>';
				
				$email_array=array(
				'team_name'=>$team_details[0]['team_name'],
				'logged_in_user_name'=>$logged_in_user_name,
				'team_owner_name'=>$team_owner_name,
				'team_owner_email'=>$owner_arr[0]['email'],
				'logged_in_user_email'=>$user_arr[0]['email'],
				'team_id'=>$team_details[0]['custom_team_id'],
				'hyperlink_to_owner_view'=>$owner_page_link,
				);
				
				return $email_array;
				//echo "<pre>";  print_r($email_array);  echo "</pre>";
				
				//echo "in";
			}			
		}
		
		
		function convert_number_format($val,$length)
		{
			return str_pad($val, $length, "0", STR_PAD_LEFT);
		}
		
		function delete_team_file_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$file_id = $encrptopenssl->decrypt($this->input->post('file_id', TRUE));
			
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$this->master_model->updateRecord('arai_byt_team_files',array('is_deleted'=>'1'),array('file_id' => $file_id));
			$result['flag'] = 'success'; 
			$result['file_id'] = $file_id;
			
			//START : INSERT LOG 
			$postArr = $this->input->post();
			$json_encode_data 	= json_encode($postArr);										
			$logDetails['user_id'] = $this->login_user_id;
			$logDetails['module_name'] = 'Team : delete_team_file_ajax';
			$logDetails['store_data'] = $json_encode_data;
			$logDetails['ip_address'] = $this->get_client_ip();
			$logDetails['createdAt'] = date('Y-m-d H:i:s');
			$logDetails['action_name'] = 'Delete Team File';
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			//END : INSERT LOG
			
			echo json_encode($result);
		}
		
		function delete_team_slot_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$team_slot_id = $this->input->post('team_slot_id', TRUE);
			
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$slot_data = $this->master_model->getRecords('arai_byt_team_slots sl',array('sl.slot_id' => $team_slot_id));
			
			if(count($slot_data) > 0)
			{
				//START : SEND ONE EMAIL TO TEAM OWNER
				$my_teams_link = site_url('myteams/myCreatedteams' );
				$hyperlink_my_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';
				
				$email_info = $this->get_mail_content_apply_slot($slot_data[0]['team_id'],$slot_data[0]['user_id'],$slot_data[0]['slot_id']);
				
				$email_send='';
				$slug = $encrptopenssl->encrypt('byt_mail_to_team_owner_when_he_delete_slot');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
				
				if(count($subscriber_mail) > 0) 
				{	
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
					'[team_owner_name]',	
					'[Team_Name]',
					'[Team_ID]',
					'[challenge_Name_with_hyperlink]',
					'[Challenge_ID]',
					'[hyperlink_to_my_teams_page]',
					'[slot_name]',
					'[applicant_Type]'
					]; 
					
					$rep_array = [
					$email_info['team_owner_name'],
					$email_info['team_name'],
					$email_info['team_id'],
					$email_info['challenge_link'],
					$email_info['challenge_id'],
					$hyperlink_my_teams_link,	
					$email_info['slot_name'],
					$email_info['slot_type'],
					];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					
					$info_array=array(
					'to' =>	$email_info['team_owner_email'],				
					'cc' =>	'',
					'from' =>	$fromadmin,
					'subject' => $subject_title,
					'view' => 'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				}
				//END : SEND ONE EMAIL TO TEAM OWNER
				
				//START : SEND ONE EMAIL TO ALL APPROVED MEMBER IN ALL REMAINING SLOTS + PENDING MEMBER FROM CURRENT SLOT 
				$my_teams_link = site_url('myteams/appliedTeams' );
				$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';
				
				$this->db->where(" (slot_id = '".$team_slot_id."' AND (status = 'Approved' OR status = 'Pending') AND is_deleted = '0') OR (team_id = '".$slot_data[0]['team_id']."' AND status = 'Approved' AND is_deleted = '0')");
				$slotMembers = $this->master_model->getRecords("arai_byt_slot_applications");
				
				if(count($slotMembers) > 0)
				{
					foreach($slotMembers as $slotRes)
					{
						$email_info = $this->get_mail_content_apply_slot($slotRes['team_id'], $slotRes['apply_user_id'], $slotRes['slot_id']);
						$email_send='';
						$slug = $encrptopenssl->encrypt('byt_mail_to_team_members_when_owner_delete_slot'); 
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0)
						{								
							$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
							$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
							
							$arr_words = [
							'[applicant_name]',	
							'[Team_Name]',
							'[Team_ID]',
							'[challenge_Name_with_hyperlink]',
							'[Challenge_ID]',
							'[hyperlink_to_my_teams_page]',
							'[slot_name]',
							'[applicant_Type]'
							]; 
							
							$rep_array = [
							$email_info['applicant_name'],
							$email_info['team_name'],
							$email_info['team_id'],
							$email_info['challenge_link'],
							$email_info['challenge_id'],
							$my_applied_teams_link,	
							$email_info['slot_name'],
							$email_info['slot_type'],
							];
							
							$sub_content = str_replace($arr_words, $rep_array, $desc);
							
							$info_array=array(
							'to' =>	$email_info['applicant_email'],		
							'cc' =>	'',
							'from' =>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view' =>  'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content);
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						}
					}
				}
				//END : SEND ONE EMAIL TO ALL APPROVED MEMBER IN ALL REMAINING SLOTS + PENDING MEMBER FROM CURRENT SLOT
			}
			
			$this->master_model->updateRecord('arai_byt_team_slots',array('is_deleted'=>'1'),array('slot_id' => $team_slot_id));
			$this->master_model->updateRecord('arai_byt_slot_applications',array('is_deleted'=>'1'),array('slot_id' => $team_slot_id));
			$result['flag'] = 'success'; 
			
			//START : INSERT LOG 
			$postArr = $this->input->post();
			$json_encode_data 	= json_encode($postArr);										
			$logDetails['user_id'] = $this->login_user_id;
			$logDetails['module_name'] = 'Team : delete_team_slot_ajax';
			$logDetails['store_data'] = $json_encode_data;
			$logDetails['ip_address'] = $this->get_client_ip();
			$logDetails['createdAt'] = date('Y-m-d H:i:s');
			$logDetails['action_name'] = 'Delete Team Slot';
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			//END : INSERT LOG
			
			echo json_encode($result);
		}
		
		public function get_mail_content_apply_slot($team_id,$appy_user_id,$apply_slot_id)
		{	
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('challenge.c_id,challenge.u_id,byt_teams.share_files,byt_teams.user_id,byt_teams.team_id,apply_status,byt_teams.team_status,share_files,team_name,team_size,team_banner,brief_team_info,proposed_approach,additional_information,custom_team_id,challenge_id,challenge_title');
			$this->db->join('challenge','challenge.c_id=byt_teams.c_id');
			$team_details= $this->master_model->getRecords('byt_teams',array('team_id'=>$team_id));
			
			if (count($team_details)) {
				
				$this->db->select('title,first_name,middle_name,last_name,email,sub_catname');
				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
				$apply_user_data = $this->master_model->getRecords("registration",array('user_id'=>$appy_user_id ));				
				
				$user_id = $team_details[0]['user_id'];
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$team_owner_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
				
				
				$user_arr = array();
				if(count($apply_user_data)){	
					
					foreach($apply_user_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$user_arr[] = $row_val;
					}
					
				}
				
				$owner_arr = array();
				if(count($team_owner_data)){	
					
					foreach($team_owner_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$owner_arr[] = $row_val;
					}
					
				}
				
				
				$applicant_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$applicant_type=$user_arr[0]['sub_catname'];
				$applicant_email=$user_arr[0]['email'];
				
				$team_owner_name = $owner_arr[0]['title']." ".$owner_arr[0]['first_name']." ".$owner_arr[0]['last_name'];
				
				
				$c_name= $encrptopenssl->decrypt($team_details[0]['challenge_title']);
				
				$c_link = base_url('challenge/challengeDetails/').base64_encode( $team_details[0]['c_id'] );
				$challenge_link = '<a href='.$c_link.' target="_blank">'.$c_name.'</a>';
				
				$my_ch_link = base_url('challenge/myChallenges');
				$challengepage_link = '<a href='.$my_ch_link.' target="_blank">here</a>';
				
				$t_link = base_url('myteams/team_details_owner/').base64_encode( $team_details[0]['team_id'] );
				$owner_page_link = '<a href='.$t_link.' target="_blank">here</a>';
				
				$go_hiring_url = base_url('myteams/goHiring/').base64_encode( $team_details[0]['team_id']."/".base64_encode($apply_slot_id) );
				
				$go_hiring_link = '<a href='.$go_hiring_url.' target="_blank">here</a>';
				
				$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
				$slot_data = $this->master_model->getRecords("byt_team_slots",array('slot_id'=>$apply_slot_id),'role_name,type_master.name' );
				
				
				
				$slot_name= $slot_data[0]['role_name'];
				$slot_type= $slot_data[0]['name'];  
				
				
				$email_array=array(
				'team_name'=>$team_details[0]['team_name'],
				'challenge_title'=>$c_name,
				'applicant_name'=>$applicant_name,
				'applicant_type'=>$applicant_type,
				'applicant_email'=>$applicant_email,
				'slot_name'=>$slot_name,
				'slot_type'=>$slot_type,
				'team_owner_name'=>$team_owner_name,
				'team_owner_email'=>$owner_arr[0]['email'],
				'chal_owner_email'=>$user_arr[0]['email'],
				'team_id'=>$team_details[0]['custom_team_id'],
				'challenge_id'=>$team_details[0]['challenge_id'],
				'challenge_link'=>$challenge_link,
				'challengepage_link'=>$challengepage_link,
				'hyperlink_to_owner_view'=>$owner_page_link,
				'go_hiring_link'=>$go_hiring_link,
				);
				
				return $email_array;
				
			}
			
			
		}
		############# END ############################	
		
		public function sorting()
		{
			$role_data = $this->master_model->getRecords('arai_byt_role_master',array("status"=>'Active'),'id, name, status');
			$domain_data = $this->master_model->getRecords("arai_domain_master",array('status'=>'Active'));
			$result_arr = $this->array_sorting($domain_data, array('id'), 'domain_name');
			
			echo "<pre>"; print_r($domain_data); echo "</pre>";
			echo "<pre>"; print_r($result_arr); echo "</pre>";			
		}
		
		public function array_sorting($res_arr=array(), $input_arr=array(), $sorting_key)
		{
			$new_sort_arr = array();
			if(count($res_arr) > 0 && count($input_arr) > 0 && $sorting_key != '')
			{
				$encrptopenssl =  New Opensslencryptdecrypt();
				$new_arr = $final_arr = $other_arr = array();
				$other_flag = 0;
				
				foreach($res_arr as $res)
				{
					$enc_val = trim($encrptopenssl->decrypt($res[$sorting_key]));
					
					if(strtolower($enc_val) != 'other')
					{					
						$new_arr[$enc_val][$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$new_arr[$enc_val][$input] = $res[$input];
						}
					}
					else
					{
						$other_flag = 1;						
						$other_arr[$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$other_arr[$input] = $res[$input];
						}
						$other_arr = $other_arr;
					}
					$final_arr = $new_arr;
				}
				
				//echo "<pre>"; print_r($final_arr); echo "</pre>"; exit;
				ksort($final_arr);
				$new_sort_arr = array();
				foreach($final_arr as $new_res)
				{
					$new_sort_arr[] = $new_res;
				}
				
				if($other_flag == 1)
				{
					$new_sort_arr[] = $other_arr;
				}
			}
			
			return $new_sort_arr;
		}
		
		public function base64_en($id)
		{
			echo base64_encode($id);
		}
		
		public function base64_de($id)
		{
			echo base64_decode($id);
		}
		
		/**************Added By Vicky*****************/
		
		// BYT Application Received Listing Page
		public function applicationListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			if(count($challengeDetails)){
				
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/application-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Application Listing 
		public function application_listing(){
			
			//error_reporting(E_ALL);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			
			$draw = $_POST['draw'];
			$row  = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			//echo "<pre>";print_r($_POST);die();
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status, 
			arai_registration.institution_full_name, arai_registration.user_category_id, arai_registration.title, arai_registration.first_name,arai_registration.middle_name, arai_registration.last_name FROM arai_byt_teams
			JOIN arai_registration ON arai_byt_teams.user_id=arai_registration.user_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."'  ".$c_data."";
			
			
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "<pre>";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$title					= $encrptopenssl->decrypt($get_request->title);
					$first_name				= $encrptopenssl->decrypt($get_request->first_name);
					$middle_name			= $encrptopenssl->decrypt($get_request->middle_name);
					$last_name				= $encrptopenssl->decrypt($get_request->last_name);
					$fullnames				= ucwords($title)." ".ucwords($first_name)." ".ucwords($middle_name)." ".ucwords($last_name);
					$categoryID				= $get_request->user_category_id;
					$cmp_name				= $encrptopenssl->decrypt($get_request->institution_full_name);
					
					$getCatSubCat = $this->master_model->getRecords("registration_usercategory", array('id' => $categoryID));
					$categoryName = $encrptopenssl->decrypt($getCatSubCat[0]['user_category']);
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('team/show_team_profile/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name		= $team_id." - ".$team_name;	
					
					$conCat				= $view."&nbsp;".$withdraw;
					
					/*if($categoryID == 1){
						$getCompDetails = $this->master_model->getRecords("student_profile", array('user_id' => $creator_user_id));
						$companyName = ucwords($encrptopenssl->decrypt($getCompDetails[0]['company_name']));
					}*/
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$i,
					$categoryName,
					$companyName,
					$teamid_name,	
					$fullnames,
					$team_status,
					$team_size,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		} // Team Listing ENd
		
		// BYT My Teams Listing 
		public function myCreatedteams(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/my-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// My Created Team Listing
		public function myTeamsListing(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			// Login User
			$user_id = $this->session->userdata('user_id');	
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status							
			FROM arai_byt_teams 							
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.user_id = '".$user_id."' GROUP BY arai_byt_teams.team_id ".$c_data."";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');			
			$result 	= $this->db->query($query)->result();			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$sqlSlot = $this->master_model->getRecords("arai_byt_team_slots", array('team_id' => $team_id));
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('challenge/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					$chllenge_code_name		= $category_id." - ".$chName;	
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,	
					$available_Slot,
					$hiddenDiv,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		// BYT Team Profile View
		public function show_team_profile($tid){
			//error_reporting(0);
			
			$teamId = base64_decode($tid);		
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->join('arai_challenge','arai_byt_teams.c_id = arai_challenge.c_id');
			$getTeamDetails 	= $this->master_model->getRecords("byt_teams", array('team_id' => $teamId));
			
			// Show Team Members
			$getTeamSlot 	= $this->master_model->getRecords("byt_team_slots", array('team_id' => $teamId));
			
			$data['show_details']	 = $getTeamDetails;
			$data['show_slotlist'] = $getTeamSlot; 	
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/team-profile';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT My Teams Listing 
		public function appliedTeams(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/applied-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function appliedChallengeListing(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();	
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,arai_byt_slot_applications.status as applicationStatus,
			arai_byt_slot_applications.apply_user_id, arai_byt_slot_applications.if_agree, arai_byt_slot_applications.introduce_urself, arai_byt_slot_applications.app_id 
			FROM arai_byt_slot_applications 
			JOIN arai_byt_teams ON arai_byt_slot_applications.apply_user_id=arai_byt_teams.user_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_slot_applications.is_deleted = '0' AND arai_byt_slot_applications.apply_user_id = '".$user_id."' GROUP BY arai_byt_teams.team_id  ".$c_data."";
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();	
			//echo $this->db->last_query();die();	
			//echo "<pre>";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$myAppstatus			= $get_request->applicationStatus;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('challenge/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					$chllenge_code_name		= $category_id." - ".$chName;
					$refId = "";
					
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$refId,
					$myAppstatus,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		///////////////////
		
		public function teamListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			if(count($challengeDetails)){
				
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/team-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Team Listing 
		public function team_listing(){
			
			error_reporting(E_ALL);
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			//echo ">>>>>>>>".$c_id;die();
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.skills, arai_byt_team_slots.role_name, arai_byt_team_slots.slot_id 
			FROM arai_byt_teams 
			JOIN arai_byt_team_slots ON arai_byt_teams.c_id=arai_byt_team_slots.c_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."' GROUP BY arai_byt_teams.team_id ".$c_data."";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "<pre>";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('myteams/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					$chllenge_code_name		= $category_id." - ".$chName;	
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,	
					$available_Slot,
					$hiddenDiv,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}  // Team Listing ENd
		
		
		
		// BYT Team Final Selection By Challenge Owner
		public function team_selection(){
			
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Post Values
			$c_id 		= $this->input->post('c_id');	
			$t_status 	= $this->input->post('t_status');
			$t_id 		= $this->input->post('t_id');
			$updateAt	= date('Y-m-d H:i:s');
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			if($t_status == 'Accept'){
				$teamFinalStatus = 'Approved';
				} else {
				$teamFinalStatus = 'Rejected';
			}
			
			
			// Update Status
			$updateArr = array('challenge_owner_status' => $teamFinalStatus, 'modified_on' => $updateAt);
			$updateQuery = $this->master_model->updateRecord('byt_teams',$updateArr,array('team_id' => $t_id, 'user_id' => $user_id));	
			
			if($updateQuery){
				
				$getStatus 	= $this->master_model->getRecords("byt_teams", array('team_id' => $t_id));
				
				if($getStatus[0]['challenge_owner_status'] == 'Approved'){
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully selected');
					} else {
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully rejected');
				}
				
				} else {
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'warning', "content_text" => 'Somethign Wrong, Tray Again');
			}
			
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Log Details
			$logDetails 		= array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Team Approved/Rejected Selection",
			'module_name'	=> 'BYT Module',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);	
			
			echo json_encode($jsonData);
		}
		
		// BYT Apply Slot By Applicant For Challenge
		public function applySlot($id){
			
			if($id == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Decode ID
			$slotid = base64_decode($id);		
			$slotDetails = $this->master_model->getRecords("byt_team_slots", array('slot_id' => $slotid, 'is_deleted' => '0'));			
			//echo "<pre>";print_r($slotDetails);
			$data['slot_details']	 = $slotDetails;
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/apply-slot';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// Slot Application Received Ajax Functionality
		public function slot_app_received(){
			
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			// Post Values
			$intro_urself 		= $this->input->post('intro_urself');	
			$is_agree 			= $this->input->post('is_agree');
			$team_id 			= $this->input->post('team_id');
			$c_id 				= $this->input->post('ch_id');
			$slot_id 			= $this->input->post('slot_id');
			$updateAt			= date('Y-m-d H:i:s');	
			
			// Log Captured		
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Fetch Record
			$fetchRecord = $this->master_model->getRecords("byt_slot_applications", array('slot_id' => $slot_id, 'team_id' => $team_id, 'c_id' => $c_id, 'apply_user_id' => $user_id, 'is_deleted' => '0'));			
			
			if(count($fetchRecord) > 0){
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - Already Exist",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'error', "content_text" => 'You already apply for this position.', "t_id" => $team_id);
				
				} else {
				
				// Insert SQL 
				$insertArr 	= array(
				'c_id' 				=> $c_id,
				'team_id' 			=> $team_id,
				'slot_id'			=> $slot_id,
				'apply_user_id'		=> $user_id,
				'introduce_urself'	=> $intro_urself,
				'if_agree'			=> $is_agree
				);
				$this->master_model->insertRecord('byt_slot_applications',$insertArr);	
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - New Application",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'You successfully applied for the position.', "t_id" => $team_id);
			}
			
			echo json_encode($jsonData);
			
		}
		
		public function getNumData($query){
			//echo $query;
			return $rowCount = $this->db->query($query)->num_rows();
		}
		
		// Search Task Today 23rd Oct 2020 Start Today
		public function taskSearch()
		{
			$data['limit']=6;
			//Sill-Set Management			
			$skillset_mgt= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');			
			
			//Sill-Set Management More			
			$skillset_mgt2= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');		
			
			// Get Apply Teams Details Listing 
			// $teamDetails  = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active", 'apply_status !='=> "Withdrawn", 'team_type'=> "task"),'',array('team_id'  => 'DESC'),0,6);
			// $totalCnts = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active", 'apply_status !='=> "Withdrawn", 'team_type'=> "task"),'',array('team_id'  => 'DESC'));
			
			// $data['team_details']    = $teamDetails;
			// $data['team_cnts']    	 = $totalCnts;
			$data['skill_set']		 = $skillset_mgt;
			$data['skillmore']		 = $skillset_mgt2;
			$data['page_title'] 	 = 'TASK';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'tasks/search-task';
			$this->load->view('front/front_combo',$data);
			
		}
		
		function load_teams_new()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$search_str = '';
			
			$start = $this->input->post('start', TRUE);
			$data['limit'] = $limit = $this->input->post('limit', TRUE);
			$data['first_flag'] = $first_flag = $this->input->post('first_flag', TRUE);
			$is_show_more = $this->input->post('is_show_more', TRUE);
			$c_data = "";
			
			if(isset($_POST) && count($_POST) > 0)
			{	
			$keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING);  
			$skillset 		= $this->input->post('skillset');

				if($skillset!=""){
					$skillset = explode(",",$skillset);
					$c_data .= " AND (";
					$i = 0;
					foreach($skillset as $t){
						
						if($i!=(count($skillset) - 1)){
							$c_data .= " FIND_IN_SET('".$t."',`skills`) OR ";
							} else {
							$c_data .= " FIND_IN_SET('".$t."',`skills`))";
						}				
						$i++;
					}
					
				}
				
				if($keyword)
				{
				//$enryptString = $encrptopenssl->encrypt($keyword);
					$designation = "$keyword";			
				
				//$str1 = '"%'.$designation.'%"';
					$c_data	.= " AND (t.team_name LIKE '%".$designation."%' || t.brief_team_info LIKE '%".$designation."%' || 
					t.task_name LIKE '%".$designation."%' || 
					ss.name LIKE '%".$encrptopenssl->encrypt($designation)."%' ||
					r.json_str LIKE '%".$designation."%' ||
					r.first_name_decrypt LIKE '%".$designation."%' ||
					r.middle_name_decrypt LIKE '%".$designation."%' ||
					r.last_name_decrypt LIKE '%".$designation."%' ||
					r2.first_name_decrypt LIKE '%".$designation."%' ||
					r2.middle_name_decrypt LIKE '%".$designation."%' ||
					r2.last_name_decrypt LIKE '%".$designation."%' ||
					CONCAT_WS( ' ', r.first_name_decrypt,r.middle_name_decrypt,r.last_name_decrypt ) LIKE  '%".$designation."%' ||
					CONCAT_WS( ' ', r.first_name_decrypt,r.last_name_decrypt ) LIKE  '%".$designation."%' ||
					t.proposed_approach LIKE '%".$designation."%' || t.additional_information LIKE '%".$designation."%')";
				
					//$c_data	.= " AND (ss.name LIKE '%".$encrptopenssl->encrypt($designation)."%' )";
				
				}
			
				$sql = "
				SELECT t.team_id, t.user_id, t.c_id, t.team_name, t.task_name, t.team_size, t.brief_team_info, t.proposed_approach, t.team_banner, t.additional_information, t.team_status,  ts.skills, ts.slot_type, ts.role_name, ss.id, ss.name 
				FROM arai_byt_teams t 
				LEFT JOIN arai_byt_team_slots ts ON ts.team_id = t.team_id
				LEFT JOIN arai_registration r ON r.user_id = t.user_id
				LEFT JOIN arai_byt_slot_applications sa ON sa.apply_user_id = r.user_id
				LEFT JOIN arai_registration r2 ON r2.user_id = sa.apply_user_id
				LEFT JOIN arai_skill_sets ss ON FIND_IN_SET (ss.id,ts.skills)
				
				WHERE t.status = 'Active' AND t.is_deleted = '0' AND t.apply_status != 'Withdrawn' AND t.team_type = 'task'
				".$c_data." GROUP BY t.team_id ORDER BY t.team_id DESC LIMIT ".$start.",".$limit." ";

				$sql_for_count = "
				SELECT t.team_id, t.user_id, t.c_id, t.team_name, t.task_name, t.team_size, t.brief_team_info, t.proposed_approach, t.team_banner, t.additional_information, t.team_status,  ts.skills, ts.slot_type, ts.role_name, ss.id, ss.name 
				FROM arai_byt_teams t 
				LEFT JOIN arai_byt_team_slots ts ON ts.team_id = t.team_id
				LEFT JOIN arai_registration r ON r.user_id = t.user_id
				LEFT JOIN arai_byt_slot_applications sa ON sa.apply_user_id = r.user_id
				LEFT JOIN arai_registration r2 ON r2.user_id = sa.apply_user_id
				LEFT JOIN arai_skill_sets ss ON FIND_IN_SET (ss.id,ts.skills)
				
				WHERE t.status = 'Active' AND t.is_deleted = '0' AND t.apply_status != 'Withdrawn' AND t.team_type = 'task'
				".$c_data." GROUP BY t.team_id ORDER BY t.team_id DESC ";
			
			// echo $sql;
			// die;
			$result 	= $this->db->query($sql)->result_array();
			$result_count 	= $this->db->query($sql_for_count)->result_array();
			

			$teamDetails = $result;
			// $teamDetails  = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active", 'apply_status !='=> "Withdrawn", 'team_type'=> "task"),'',array('team_id'  => 'DESC'),$start,$limit);
			$totalCnts = $this->master_model->getRecordCount('byt_teams',array('is_deleted'=>"0", 'status'=>"Active", 'apply_status !='=> "Withdrawn", 'team_type'=> "task"),'',array('team_id'  => 'DESC'));
				// print_r($teamDetails);
				
				$data['team_details']    = $teamDetails;
				$data['team_cnts']    	 = count($result_count);
				
				$data['new_start'] = $new_start = $start + $limit;
				
				$showData = $this->load->view('front/tasks/incTaskTeams',$data,true);
				
				$jsonData = array("html" => $showData, "token" => $csrf_new_token, "totalCnts" => $totalCnts);
				echo json_encode($jsonData);
			}
		}
		
		// Filter Functionality For Teams 
		public function load_more_teams()
		{
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$id = $this->input->post('id');
			
			// Get Teams Total Count 
			// $total_cnt  = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active", 'apply_status !='=> "Withdrawn", 'team_type'=> "task"),'',array('team_id', 'DESC'));
			
			$showLimit = 1;
			
			$sql = "SELECT * FROM arai_byt_teams WHERE is_deleted = '0' AND status='Active' AND apply_status != 'Withdrawn' AND team_type = 'task' ORDER BY team_id DESC LIMIT ".$id.",".$showLimit.""; 
			$result 	= $this->db->query($sql)->result();
			$total_cnt =count($result);	
			
			$showData = '';		
			if(count($result) > 0)
			{			
				foreach($result as $c_data)
				{
					$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $c_data->c_id));
					
					// Team Slot Details
					$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $c_data->c_id, 'team_id'=> $c_data->team_id, 'is_deleted' => '0'));
					//$maxTeam = $encrptopenssl->decrypt($getSlotMaxMember[0]['max_team']);
					
					// Team Slot Details
					$fixSlot  = $this->master_model->getRecords('task_setting',array('id'=> "1"));
					$maxTeam = $fixSlot[0]['max_no'];
					
					
					$minTeamCnt = count($slotDetails);
					$availableCnt = $maxTeam - $minTeamCnt;
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $c_data->c_id, 'team_id' => $c_data->team_id, 'is_deleted' => '0'));
					//echo $this->db->last_query(); exit;
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					$array_skills = array();
					foreach($slotDetails as $slot_list){
						if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }							
					}
					
					$combine = implode(",",$array_skills);
					$xp = explode(',',$combine);
					$arrayUnique = array_unique($xp);
					
					if($c_data->team_banner!=""){
						$banner_team = base_url('uploads/byt/'.$c_data->team_banner);
						} else {
						$banner_team = base_url('assets/no-img.png'); 
					}
					
					$showData .= '<div class="col-sm-12 justify-content-center mt-4 team-box">
					<div class="boxSection25">
					<ul class="list-group list-group-horizontal counts">
					<li class="list-group-item">
					<div class="inner-box" style="border-bottom:none">
					<img src="'.$banner_team.'" alt="Banner" class="img-fluid">
					<h3 class="minheight" style="font-size:16px;">'.ucfirst($c_data->team_name).'</h3>
					<ul>
					<li> '.$totalSlotCnt.' Members </li> 
					<li>-</li>
					<li>'.$totalPendingCnt.' Available SLots</li>
					</ul>
					</div>
					
					</li>
					<li class="list-group-item">
					<div class="inner-box">
					<strong style="display:block; text-align:left; margin:15px 0 0px 0px;">Brief Info about Team </strong>
					<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0">'.substr($c_data->brief_team_info,0,90).'...<a href="javascript:void();" class="click-more" data-id="'.$c_data->team_id.'">view more</a></p>
					
					
					<h4>Looking For</h4>
					<div class="Skillset text-center">';
					$s = 0;	
					foreach($arrayUnique as $commonSkill){
						if($s < 9){
							$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$teamslist['user_id']."')",NULL, false);
							$skill_now = $this->master_model->getRecords('skill_sets');
							$skill_names = $encrptopenssl->decrypt($skill_now[0]['name']);												
							$showData .='<span class="badge badge-pill badge-info">'.$skill_names.'</span>';
						}
						$s++;
					}
					
					$showData .='</div>
					</div>														
					</li>
					<li class="list-group-item" style="height: 0 !important;">
					<div class="team_search owl-carousel owl-theme">';	
					
					// Slot Deatils Foreach	
					$TeamSr = 1;
					$m = 1;
					foreach($slotDetails as $slotD)
					{
						$skills = array();
						$skillTitle = 'Skills';											
						
						$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
						
						if(count($getSlotDetail) > 0)
						{												
							$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
							$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
							
							$catID = $getUserDetail[0]['user_category_id'];	
							$getNames = $encrptopenssl->decrypt($getUserDetail[0]['first_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['middle_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['last_name']); 
							$fullname = ucwords($getNames);
							
							if($catID == 1)
							{
								$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
								$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
								
								$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
								if ($getProfileDetail[0]['profile_picture']!="") {
									
									$imageUser = $imgPath;
									//$imageUser = $imgPath;
								} 
								else 
								{
									$imageUser = base_url('assets/no-img.png');
								}
								
								if($getProfileDetail[0]['skill_sets'] != "")
								{
									$skill_set_ids = $encrptopenssl->decrypt($getProfileDetail[0]['skill_sets']);
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_skill_sets");														
									}
								}
								
								if($getProfileDetail[0]['other_skill_sets'] != "") 
								{ 
									$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
								}														
							}  
							else if($catID == 2)
							{
								$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
								
								if(count($getOrgDetail) > 0)
								{															
									$profilePic = $encrptopenssl->decrypt($getOrgDetail[0]['org_logo']);
									$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
									if ($profilePic!="") {
										
										$imageUser = $imgPath;
										//$imageUser = $imgPath;
										} else {
										$imageUser = base_url('assets/no-img.png');
									}
									
								}
								
								$skillTitle = 'Sector';
								if($getOrgDetail[0]['org_sector'] != "")
								{
									$skill_set_ids = $getOrgDetail[0]['org_sector'];
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_organization_sector");														
									}
								}
								
								if($getOrgDetail[0]['org_sector_other'] != "") 
								{ 
									$skills[]['name'] = $encrptopenssl->decrypt($getOrgDetail[0]['org_sector_other']); 
								}														
							}									
							
							if($getSlotDetail[0]['apply_user_id'] == $c_data->user_id){
								$styles_hide = "hide-class";
								} else {
								$styles_hide = "d-inline-block";
							}
							
						} 
						else 
						{
							$imageUser = base_url('assets/no-img.png');
							$styles_hide = "d-inline-block";
							$fullname = "Open Slot";											
							
							$skill = $slotD['skills'];											
							$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
							$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');													
						} // Else End
						
						
						
						$showData .='<div class="item text-center">
						<img src="'.$imageUser.'" alt="'.$fullname.'" title="'.$fullname.'" />
						<h4>'.$fullname.'</h4>
						<div class="Skillset text-center">';
						if(count($skills)) 
						{
							$i=0;
							if($i < 6)
							{
								foreach ($skills as $key => $value) 
								{ 
									$showData .='<span class="badge badge-pill badge-info">'; if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { $showData .= $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { $showData .= $value['name']; } } $showData .='</span>';
									$i++;
								}
							}
						}
						$showData .='</div>
						<h4>Team Member '.$TeamSr.'</h4>';						
						$showData .='</div>';
						$m++; $TeamSr++;
					} // Slot Details Foeach End
					$showData .= '</div> 
					</li>
					
					<div class="container" style="clear:both;" >
					<div class="row buttonView mb-3">
					<div class="col-md-3">
					<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
					</div>
					<div class="col-md-6">
					<a href="'.base_url('myteams/task_details_member/'.base64_encode($c_data->team_id)).'" class="btn btn-primary w-100">View Details</a>
					</div>
					<div class="col-md-3">
					<a href="'.base_url('myteams/taskApplySlot/').base64_encode($c_data->team_id)."/".base64_encode($c_data->c_id).'" class="btn btn-primary w-100">Apply</a>
					</div>
					</div>
					</div>	
					
					
					</ul>
					</div>
					</div>';			
					
					$nextid = $c_data->team_id;
					
				} // Foreach End
				
				if($total_cnt > $showLimit || 1){
					
					$showData .= '<div class="row justify-content-center mt-4 show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white mb-4" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b></b></div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
			
			
		} // Load More Team End
		
		public function team_filter(){		
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);		
			
			// Post Values
			$skillset 		= $this->input->post('skillset');
			$keyword 		= rtrim($this->input->post('search_txt'));
			//echo "<pre>";print_r($this->input->post());
			$c_data = "";
			
			//skill set
			if(count((array)$skillset) > 0){
				
				$c_data .= " AND (";
				$i = 0;
				foreach($skillset as $t){
					
					if($i!=(count($skillset) - 1)){
						$c_data .= " FIND_IN_SET('".$t."',`skills`) OR ";
						} else {
						$c_data .= " FIND_IN_SET('".$t."',`skills`))";
					}				
					$i++;
				}
				
			} // End Skill Set IF
			
			if($keyword)
			{
				//$enryptString = $encrptopenssl->encrypt($keyword);
				$designation = "$keyword";			
				
				//$str1 = '"%'.$designation.'%"';
				$c_data	.= " AND (t.team_name LIKE '%".$designation."%' || t.brief_team_info LIKE '%".$designation."%' || 
				t.task_name LIKE '%".$designation."%' || 
				ss.name LIKE '%".$encrptopenssl->encrypt($designation)."%' ||
				r.json_str LIKE '%".$designation."%' ||
				r.first_name_decrypt LIKE '%".$designation."%' ||
				r.middle_name_decrypt LIKE '%".$designation."%' ||
				r.last_name_decrypt LIKE '%".$designation."%' ||
				r2.first_name_decrypt LIKE '%".$designation."%' ||
				r2.middle_name_decrypt LIKE '%".$designation."%' ||
				r2.last_name_decrypt LIKE '%".$designation."%' ||
				CONCAT_WS( ' ', r.first_name_decrypt,r.middle_name_decrypt,r.last_name_decrypt ) LIKE  '%".$designation."%' ||
				CONCAT_WS( ' ', r.first_name_decrypt,r.last_name_decrypt ) LIKE  '%".$designation."%' ||
				t.proposed_approach LIKE '%".$designation."%' || t.additional_information LIKE '%".$designation."%')";
				
				//$c_data	.= " AND (ss.name LIKE '%".$encrptopenssl->encrypt($designation)."%' )";
				
			}
			
			
			
			$sql = "
			SELECT t.team_id, t.user_id, t.c_id, t.team_name, t.task_name, t.team_size, t.brief_team_info, t.proposed_approach, t.team_banner, t.additional_information, t.team_status,  ts.skills, ts.slot_type, ts.role_name, ss.id, ss.name 
			FROM arai_byt_teams t 
			LEFT JOIN arai_byt_team_slots ts ON ts.team_id = t.team_id
			LEFT JOIN arai_registration r ON r.user_id = t.user_id
			LEFT JOIN arai_byt_slot_applications sa ON sa.apply_user_id = r.user_id
			LEFT JOIN arai_registration r2 ON r2.user_id = sa.apply_user_id
			LEFT JOIN arai_skill_sets ss ON FIND_IN_SET (ss.id,ts.skills)
			
			WHERE t.status = 'Active' AND t.is_deleted = '0' AND t.apply_status != 'Withdrawn' AND t.team_type = 'task'
			".$c_data." GROUP BY t.team_id ORDER BY t.team_id DESC";
			
			// echo $sql;
			// die;
			$result 	= $this->db->query($sql)->result();	
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$showData = '';
			
			if(count($result) > 0){
				
				foreach($result as $c_data){
					
					$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $c_data->c_id));
					
					// Team Slot Details
					$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $c_data->c_id, 'team_id'=> $c_data->team_id, 'is_deleted' => '0'));
					//$maxTeam = $encrptopenssl->decrypt($getSlotMaxMember[0]['max_team']);
					
					// Team Slot Details
					$fixSlot  = $this->master_model->getRecords('task_setting',array('id'=> "1"));
					$maxTeam = $fixSlot[0]['max_no'];
					$minTeamCnt = count($slotDetails);
					$availableCnt = $maxTeam - $minTeamCnt;
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $c_data->c_id, 'team_id' => $c_data->team_id, 'is_deleted' => '0'));
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					
					$array_skills = array();
					foreach($slotDetails as $slot_list){
						if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }
					}
					
					$combine = implode(",",$array_skills);
					$xp = explode(',',$combine);
					$arrayUnique = array_unique($xp);
					
					if($c_data->team_banner!=""){
						$banner_team = base_url('uploads/byt/'.$c_data->team_banner);
						} else {
						$banner_team = base_url('assets/no-img.png');
					}	
					
					$showData .= '<div class="col-sm-12 justify-content-center mt-4 team-box '.$c_data->team_id.'">
					<div class="boxSection25">
					<ul class="list-group list-group-horizontal counts">
					<li class="list-group-item">
					<div class="inner-box">
					<img src="'.$banner_team.'" alt="Banner" class="img-fluid">
					<h3>'.ucfirst($c_data->team_name).'</h3>
					<ul>
					<li> '.$totalSlotCnt.' Members </li> 
					<li>-</li>
					<li>'.$totalPendingCnt.' Available Slots</li>
					</ul>
					</div>
					
					</li>
					<li class="list-group-item">
					<div class="inner-box">
					<strong style="display:block; text-align:left; margin:15px 0 0px 0px;">Brief Info about Team </strong>
					<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0">'.substr($c_data->brief_team_info,0,90).'...<a href="javascript:void();" class="click-more" data-id="'.$c_data->team_id.'">view more</a></p>
					<h4>Looking For</h4>
					<div class="Skillset text-center">';
					$s = 0;	
					foreach($arrayUnique as $commonSkill){
						if($s < 9){
							$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$c_data->user_id."')",NULL, false);
							$skill_now = $this->master_model->getRecords('skill_sets');
							$skill_names = $encrptopenssl->decrypt($skill_now[0]['name']);												
							$showData .='<span class="badge badge-pill badge-info">'.$skill_names.'</span>';
						}
						$s++;
					}
					
					$showData .='</div>
					</div>
					
					</li>
					<li class="list-group-item" style="height: 0 !important;">
					<div class="team_search owl-carousel owl-theme">';	
					
					// Slot Deatils Foreach	
					$TeamSr = 1;
					$m = 1;
					foreach($slotDetails as $slotD)
					{
						$skills = array();
						$skillTitle = 'Skills';
						$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
						if(count($getSlotDetail) > 0)
						{												
							$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
							$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
							
							$catID = $getUserDetail[0]['user_category_id'];	
							$getNames = $encrptopenssl->decrypt($getUserDetail[0]['first_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['middle_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['last_name']); 
							$fullname = ucwords($getNames);
							
							if($catID == 1)
							{
								$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
								$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
								
								$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
								if ($getProfileDetail[0]['profile_picture']!="") {
									
									$imageUser = $imgPath;
									//$imageUser = $imgPath;
									} else {
									$imageUser = base_url('assets/no-img.png');
								}									
								
								if($getProfileDetail[0]['skill_sets'] != "")
								{
									$skill_set_ids = $encrptopenssl->decrypt($getProfileDetail[0]['skill_sets']);
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_skill_sets");														
									}
								}
								
								if($getProfileDetail[0]['other_skill_sets'] != "") 
								{ 
									$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
								}
							}  
							else if($catID == 2)
							{														
								$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
								
								if(count($getOrgDetail) > 0)
								{															
									$profilePic = $encrptopenssl->decrypt($getOrgDetail[0]['org_logo']);
									$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
									if ($profilePic!="") {
										
										$imageUser = $imgPath;
										//$imageUser = $imgPath;
										} else {
										$imageUser = base_url('assets/no-img.png');
									}
									
								}
								
								$skillTitle = 'Sector';
								if($getOrgDetail[0]['org_sector'] != "")
								{
									$skill_set_ids = $getOrgDetail[0]['org_sector'];
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_organization_sector");														
									}
								}
								
								if($getOrgDetail[0]['org_sector_other'] != "") 
								{ 
									$skills[]['name'] = $encrptopenssl->decrypt($getOrgDetail[0]['org_sector_other']); 
								}														
							}									
							
							if($getSlotDetail[0]['apply_user_id'] == $c_data->user_id){
								$styles_hide = "hide-class";
								} else {
								$styles_hide = "d-inline-block";
							}
							
						} 
						else 
						{											
							$imageUser = base_url('assets/no-img.png');
							$styles_hide = "d-inline-block";
							$fullname = "Open Slot";											
							
							$skill = $slotD['skills'];											
							$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
							$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');													
						} // Else End
						
						
						
						$showData .='<div class="item text-center">
						<img src="'.$imageUser.'" alt="'.$fullname.'" title="'.$fullname.'" />
						<h4>'.$fullname.'</h4>
						<div class="Skillset text-center">';
						if(count($skills)) 
						{
							$i=0;
							if($i < 6)
							{
								foreach ($skills as $key => $value) 
								{ 
									$showData .='<span class="badge badge-pill badge-info">'; if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { $showData .= $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { $showData .= $value['name']; } } $showData .='</span>';
									$i++;
								}
							}
						}
						$showData .='	</div>
						<h4>Team Member '.$TeamSr.'</h4>';
						
						$showData .='</div>';
						$m++;  $TeamSr++;
					} // Slot Details Foeach End
					$showData .= '</div> 
					</li>
					<div class="container" style="clear:both;" >
					<div class="row buttonView mb-3">
					<div class="col-md-3">
					<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
					</div>
					<div class="col-md-6">
					<a href="'.base_url('myteams/task_details_member/'.base64_encode($c_data->team_id)).'" class="btn btn-primary w-100">View Details</a>
					</div>
					<div class="col-md-3">
					<a href="'.base_url('myteams/taskApplySlot/').base64_encode($c_data->team_id)."/".base64_encode($c_data->c_id).'" class="btn btn-primary w-100">Apply</a>
					</div>
					</div>
					</div>		
					
					</ul>
					</div>
					</div>';			
					
					$nextid = $c_data->team_id;
					
				} // Foreach End			
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name,"skillset"=>$skillset,"keyword"=>$keyword, 'qry'=>$sql);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div class="text-center" style="width:100%;">No Record Found</div>', 'qry'=>$sql);
				echo json_encode($jsonData);
				
			}
			
		} // End Filter
		
		// IP Address Track
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
	}															