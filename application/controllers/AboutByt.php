<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class AboutByt extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->model('Common_model_sm');
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			//$this->check_permissions->is_logged_in();
		}		
		
		public function index()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		
		
			$data['page_title']='About BYT';
			$data['middle_content']='about_byt';
			$this->load->view('front/front_combo',$data);
		}		
		
		public function temp_search_existing_team()
		{
			$data['page_title']='';
			$data['middle_content']='search_existing_team';
			$this->load->view('front/front_combo',$data);
		}		
		
		public function temp_team_owner_view()
		{
			$data['page_title']='';
			$data['middle_content']='byt/team_owner_view';
			$this->load->view('front/front_combo',$data);
		}		
		
		public function temp_team_detail()
		{
			$data['page_title']='';
			$data['middle_content']='byt/team_detail';
			$this->load->view('front/front_combo',$data);
		}
		
		
		
	}							