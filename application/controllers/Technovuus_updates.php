<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Technovuus_updates extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        $this->load->helper('common');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {

        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        $data['limit'] = $limit = 10;

        $this->db->order_by('technology_name', 'ASC');
        $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $data['technology_data'] = $this->master_model->getRecords("arai_blog_technology_master", array("status" => 'Active'));

        $data['tag_data'] = $this->master_model->getRecords("arai_blog_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['page_title']     = 'Feeds';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technovuus_updates/index';
        $this->load->view('front/front_combo', $data);

    }

    public function get_feeds_ajax()
    {
        $user_id       = $this->session->userdata('user_id');
        $encrptopenssl = new Opensslencryptdecrypt();

        $start                = $this->input->post('start', true);
        $data['limit']        = $limit        = $this->input->post('limit', true);
        $data['new_start']    = $new_start    = $start + $limit;
        $data['is_show_more'] = $this->input->post('is_show_more', true);

        $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING);

        $search_str_res     = '';
        $search_str_kr      = '';
        $search_str_blog    = '';
        $search_str_webinar = '';
        $search_str_expert  = '';
        $search_str_admin   = '';
        $search_str_tech    = '';
        if ($keyword) {

            $search_str_admin = " ( au.feed_title LIKE '%" . $keyword . "%' ) ";
         
            }

        $feed_type = trim($this->input->post('feed_type'));


        //START ADMIN UPDATES

        if ($search_str_admin != "") {$this->db->where($search_str_admin);}
        if ($feed_type != "") {$this->db->where($this->feed_type_search($feed_type, 'au'));}

        $this->db->where('au.created_on BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()');
        $select_admin_updates = "au.*";

        $data['admin_updates'] = $admin_updates = $this->master_model->getRecords("admin_update au", array('au.is_deleted' => '0', 'au.admin_status' => '1', 'au.is_block' => '0'), $select_admin_updates, '');
        //END ADMIN UPDATES

        //START TECHNOLOGY TRANSFER
        if ($feed_type != "") {$this->db->where($this->feed_type_search($feed_type, 't'));}
        if ($search_str_tech != "") {$this->db->where($search_str_tech);}

        $data['technology_transfer'] = $technology_transfer = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_block' => '0'));
        //END TECHNOLOGY TRANSFER

        $feeds_merged = $admin_updates;

        // Check common helper for below custom array sorting and filter functions

        $feeds = $admin_updates;

        $all_nonfeatured_feeds = filertNonFeaturedFeeds($feeds);
        $featured_feeds        = filertFeaturedFeeds($feeds);

        //pagination is done using array slice method
        $nonfeatured_feeds = array_slice($all_nonfeatured_feeds, $start, $limit);
        // echo "<pre>";
        // print_r($Nonfeatured_feeds);die;
        $data['nonfeatured_feeds']           = $nonfeatured_feeds;
        $data['all_nonfeatured_feeds_count'] = count($all_nonfeatured_feeds);
        $data['featured_feeds']              = $featured_feeds;

        $data['Actiondata']  = $this->getActiondata($user_id);
        $data['feeds_count'] = count($feeds_merged);
        $default_image = $this->master_model->getRecords("arai_feeds_banner",array('banner_id'=>'1'));
       
        // echo "<pre>";    print_r($data);die;
        $data['default_img_url']=$default_image[0]['banner'];;

        $result['feeds_response'] = $this->load->view('front/technovuus_updates/incFeeds', $data, true);
        $result['flag']           = "success";

        echo json_encode($result);

    }

    public function feed_type_search($feed_type, $table_slug)
    {
        $search_str_type = "";
        $feed_type_arr   = explode(",", $feed_type);
        if (count($feed_type_arr) > 0) {

            $search_str_type .= " (";
            $i = 0;
            foreach ($feed_type_arr as $res) {
                $search_str_type .= $table_slug . ".feeds_module_id =  " . $res . "  ";
                if ($i != (count($feed_type_arr) - 1)) {$search_str_type .= " OR ";}
                $i++;
            }
            $search_str_type .= " )";
        }
        return $search_str_type;
    }

    public function getActiondata($user_id = 0)
    {
        $user_like_kr = $this->master_model->getRecords("knowledge_repository_likes", array('user_id' => $user_id), 'kr_id');

        $user_like_res = $this->master_model->getRecords("resource_sharing_likes", array('user_id' => $user_id), 'r_id');

        $user_like_blog = $this->master_model->getRecords("arai_blogs_likes", array('user_id' => $user_id), 'blog_id');

        $user_like_feed = $this->master_model->getRecords("arai_admin_update_likes", array('user_id' => $user_id), 'feed_id');

        $like_arr_kr = $like_arr_res = $like_arr_blog = $like_arr_feed = $return_arr = array();

        if (count($user_like_kr) > 0) {
            foreach ($user_like_kr as $res) {
                $like_arr_kr[$res['kr_id']] = $res['kr_id'];
            }
        }

        if (count($user_like_res)) {
            foreach ($user_like_res as $res) {
                $like_arr_res[$res['r_id']] = $res['r_id'];
            }
        }

        if (count($user_like_blog)) {
            foreach ($user_like_blog as $res) {
                $like_arr_blog[$res['blog_id']] = $res['blog_id'];
            }
        }

        if (count($user_like_feed)) {
            foreach ($user_like_feed as $res) {
                $like_arr_feed[$res['feed_id']] = $res['feed_id'];
            }
        }

        $return_arr['like_kr']   = $like_arr_kr;
        $return_arr['like_res']  = $like_arr_res;
        $return_arr['like_blog'] = $like_arr_blog;
        $return_arr['like_feed'] = $like_arr_feed;

        return $return_arr;
    }

    public function like_unlike_feed_ajax() // LIKE - UNLIKE BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $feed_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag    = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id = $this->session->userdata('user_id');

            $result['flag']    = "success";
            $result['feed_id'] = $feed_id;

            if ($flag == 1) {
                $action_name            = "Like KR";
                $add_like['feed_id']    = $feed_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('admin_update_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';
            } else {
                $this->db->where('feed_id', $feed_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('admin_update_likes');

                $action_name = "Un-Like KR";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';
            }
            $getTotalLikes         = $this->master_model->getRecordCount('admin_update_likes', array('feed_id' => $feed_id), 'like_id');
            $result['total_likes'] = $getTotalLikes;

            $onclick_fun = "like_unlike_feed('" . base64_encode($feed_id) . "','" . $LikeFlag . "')";

            $result['response'] = '<span style="cursor:pointer;"  onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . '</span>';

            // $result['total_likes_html'] = '<span style="cursor:auto">'.$getTotalLikes.' &nbsp <small>Likes</small></span>';
            $result['total_likes_html'] = '<span style="cursor:auto;background: #32f0ff;color: #000;">' . $getTotalLikes . '</span>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'KR Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function check_permissions_ajax()
    {
        $module_id = $this->input->post('module_id');
        $result    = array();
        if ($module_id != '') {

            if ($this->check_permissions->is_authorise_ajax($module_id)) {
                $result['success'] = true;
            } else {
                $result['success'] = false;
            }
        } else {
            $result['success'] = false;
        }
        echo json_encode($result);
    }

    public function details($feed_id = 0, $comment_flag = 0) //BLOG DETAILS PAGE

    {
        $this->check_permissions->is_logged_in();
        // $module_id = 32;
        // $this->check_permissions->is_authorise($module_id);

        $encrptopenssl   = new Opensslencryptdecrypt();
        $user_id         = $this->session->userdata('user_id');
        $data['user_id'] = $user_id;

        if ($feed_id == '0') {redirect(site_url('feeds'));} else {
            $feed_id = base64_decode($feed_id);

            $data['feed_id']       = $feed_id;
            $data['comment_limit'] = $comment_limit = 5;
            $data['comment_flag']  = $comment_flag;

            $data['form_data'] = $form_data = $this->master_model->getRecords("admin_update ad", array('ad.is_deleted' => '0', 'ad.feed_id' => $feed_id));
            if (count($form_data)==0 ) {
                redirect(base_url('feeds'));
            }

            $data['video_data'] = $video_data = $this->master_model->getRecords('arai_admin_video_details', array("feed_id" => $feed_id, "is_deleted" => '0'), '', array('vid_id' => 'ASC'));

            // echo "<pre>"; print_r($data); echo "</pre>";die;

            /*$redirect_flag = 0;
        if (count($form_data) == 0) {$redirect_flag = 1;} else if ($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;} else if ($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;}

        if ($redirect_flag == 1) {redirect(site_url('knowledge_repository'));}*/
        }
        $files = $this->master_model->getRecords('admin_update_files', array("feed_id" => $feed_id, "is_deleted" => '0'), 'file_name,file_id');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/admin_update_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }
        $data['getTotalLikes']  = $this->master_model->getRecordCount('arai_admin_update_likes', array('feed_id' => $feed_id), 'like_id');
        $data['files_size']     = $this->formatSizeUnits($totalSize);
        $data['files_count']    = count($files);
        $data['Actiondata']     = $this->getActiondata($user_id);
        $data['page_title']     = 'Blog Details';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'feeds/details';
        // echo "<pre>";print_r($data);die;
        $this->load->view('front/front_combo', $data);
    }

    public function open_download_modal_ajax()
    {

        $data['feed_id'] = $feed_id = $this->input->post('feed_id');

        $this->check_permissions->is_logged_in();
        // $module_id = 32;
        // $this->check_permissions->is_authorise($module_id);

        $encrptopenssl   = new Opensslencryptdecrypt();
        $user_id         = $this->session->userdata('user_id');
        $data['user_id'] = $user_id;

        if ($feed_id == '0') {redirect(site_url('feeds'));} else {
            $data['feed_id'] = $feed_id;

            $data['form_data'] = $form_data = $this->master_model->getRecords("admin_update ad", array('ad.is_deleted' => '0', 'ad.feed_id' => $feed_id));

        }
        $files = $this->master_model->getRecords('admin_update_files', array("feed_id" => $feed_id, "is_deleted" => '0'), 'file_name,file_id,file_original_name');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/admin_update_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }
        $data['getTotalLikes'] = $this->master_model->getRecordCount('arai_admin_update_likes', array('feed_id' => $feed_id), 'like_id');
        $data['files_size']    = $this->formatSizeUnits($totalSize);
        $data['files_count']   = count($files);
        $data['files']         = $files;
        $data['image']         = $this->create_captcha();

        $this->load->view('front/feeds/download_form', $data);
    }

    public function download_feed($files_id, $feed_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $files_ids     = $files_id;

        if ($files_ids == "") {
            redirect(base_url('feeds'));
        }
        $this->db->where("file_id IN (" . $files_ids . ")", null, false);
        $files = $this->master_model->getRecords('admin_update_files', '', 'file_name,file_id');
        if (count($files)) {

            $kr_data = $this->master_model->getRecords('admin_update', array("feed_id" => $feed_id, "is_deleted" => '0'));

            $disp_id = $kr_data[0]['id_disp'];

            $user_id = $this->session->userdata('user_id');

            $insert_Arr = array(
                'feed_id' => $feed_id,
                'user_id' => $user_id,
                'files'   => json_encode($files),
            );
            //$this->master_model->insertRecord('arai_knowledge_download_log', $insert_Arr);
            /*
            $email_info = $this->get_mail_data($kr_id);

            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_downloading_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

            $arr_words = [
            '[kr_owner_name]',
            '[kr_title]',
            ];
            $rep_array = [
            $email_info['kr_owner_name'],
            $email_info['kr_title'],
            ];

            $sub_content = str_replace($arr_words, $rep_array, $desc);

            $info_array = array(
            'to'      => $email_info['kr_owner_email'],
            'cc'      => '',
            'from'    => $fromadmin,
            'subject' => $subject_title,
            'view'    => 'common-file',
            );

            $other_infoarray = array('content' => $sub_content);
            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            // END: MAIL TO USER
             */

            $zip      = new ZipArchive();
            $zip_name = $disp_id . ".zip"; // Zip name
            $zip->open($zip_name, ZipArchive::CREATE);
            foreach ($files as $file) {
                $path = "uploads/admin_update_files/" . $file['file_name'];
                if (file_exists($path)) {
                    $zip->addFromString(basename($path), file_get_contents($path));
                }

            }
            $zip->close();
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . $zip_name . "\"");
            readfile($zip_name);
            unlink($zip_name);

        } else {
            redirect(base_url('knowledge_repository'));
        }

    }

    public function create_captcha($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }
    public function check_captcha_ajax()
    {
        $code = $_POST['code'];

        if ($code == '' || $_SESSION["dwcaptcha"] != $code) {
            //$this->session->unset_userdata("dwcaptcha");
            //$this->form_validation->set_message('check_captcha', 'Invalid %s.');
            echo "false";
        }
        if ($_SESSION["dwcaptcha"] == $code && $this->session->userdata("used") == 0) {
            //$this->session->unset_userdata("dwcaptcha");
            echo "true";
        }

    }

    public function refresh()
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 17,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Display captcha image
        echo $captcha['image'];
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function post_comment_ajax()
    {
        $this->check_permissions->is_logged_in();
        $encObj = new Opensslencryptdecrypt();

        if (isset($_POST) && count($_POST) > 0) {
            $result['flag']  = "success";
            $user_id         = trim($this->security->xss_clean($this->input->post('user_id')));
            $feed_id         = trim($this->security->xss_clean($this->input->post('feed_id')));
            $comment_id      = trim($this->security->xss_clean($this->input->post('comment_id')));
            $comment_content = trim($this->security->xss_clean($this->input->post('comment_content')));

            $add_data['feed_id']           = $feed_id;
            $add_data['user_id']           = $user_id;
            $add_data['parent_comment_id'] = $comment_id;
            $add_data['comment']           = $comment_content;
            $add_data['status']            = 1;
            $add_data['created_on']        = date('Y-m-d H:i:s');

            $result['new_comment_id'] = $last_inserted_comment = $this->master_model->insertRecord('arai_admin_update_comments', $add_data, true);

            //THIS IS FOR COMMENT REPLY
            $comment_reply_cnt               = $this->master_model->getRecordCount("arai_admin_update_comments", array('status' => '1', 'parent_comment_id' => $comment_id), 'comment_id');
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_id . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $comment_reply_cnt . ')</p>
                </span>';

            $select = "bc.comment_id, bc.feed_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture";
            $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
            $comment_data = $this->master_model->getRecords("arai_admin_update_comments bc", array('bc.comment_id' => $last_inserted_comment), $select, '', 0, 1);

            $CommentActiondata = $this->getFeedCommentsActiondata($user_id);

            $response_html = '';
            $iconImg       = base_url('assets/no-img.png');
            if ($comment_data[0]['user_category_id'] == 2) {
                if ($comment_data[0]['org_logo'] != "") {$iconImg = base_url('uploads/organization_profile/' . $encObj->decrypt($comment_data[0]['org_logo']));}
            } else {
                if ($comment_data[0]['profile_picture'] != "") {$iconImg = base_url('assets/profile_picture/' . $comment_data[0]['profile_picture']);}
            }

            $title       = $encObj->decrypt($comment_data[0]['title']);
            $first_name  = $encObj->decrypt($comment_data[0]['first_name']);
            $middle_name = $encObj->decrypt($comment_data[0]['middle_name']);
            $last_name   = $encObj->decrypt($comment_data[0]['last_name']);

            $disp_name = $title . " " . $first_name;
            if ($middle_name != '') {$disp_name .= " " . $middle_name . " ";}
            $disp_name .= $last_name;

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['like_comments'])) {
                $Like_label = "Like";
                $LikeFlag   = '0';
                $likeIcon   = 'fa-thumbs-up';
                $bg_color   = "style='color:#1562a6; font-weight:500;'";} else {
                $Like_label = "Like";
                $LikeFlag   = '1';
                $likeIcon   = 'fa-thumbs-o-up';
                $bg_color   = "style='color:#000'";}

            $onclick_like_dislike_fun = "like_dislike_comment('" . base64_encode($comment_data[0]['comment_id']) . "','" . $LikeFlag . "')";
            $onclick_del_fun          = "delete_comment('" . base64_encode($comment_data[0]['comment_id']) . "', 'reply')";

            $response_html .= ' <div id="append_reply_div_' . $comment_id . '"></div>
                                                        <div class="comment_block_common comment_block_common_reply" id="comment_block_' . $comment_data[0]['comment_id'] . '">
                                                            <div class="comment_img"><img src="' . $iconImg . '"></div>
                                                                <div class="comment_inner">
                                                                    <p class="comment_name">' . $disp_name . '</p>
                                                                    <p class="comment_time">' . $this->Common_model_sm->time_Ago(strtotime($comment_data[0]['created_on']), $comment_data[0]['created_on']) . '</p>
                                                                    <p class="comment_content">' . $comment_data[0]['comment'] . '</p>
                                                                    <span id="like_unlike_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
                                                                        <p class="comment_like_btn" onclick="' . $onclick_like_dislike_fun . '" ' . $bg_color . '><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $comment_data[0]['total_likes'] . ')</p>
                                                                    </span>';

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['self_comments'])) {
                $response_html .= ' &nbsp;&nbsp;|&nbsp;&nbsp;
                                                                        <span id="delete_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
                                                                            <p class="comment_like_btn" onclick="' . $onclick_del_fun . '"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
                                                                        </span>';
            }
            $response_html .= ' </div><div class="clearfix"></div>
                                                            </div>';
            $result['response'] = $response_html;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_comment_data_ajax()
    {
        $encrptopenssl            = new Opensslencryptdecrypt();
        $result['csrf_new_token'] = $csrf_new_token = $this->security->get_csrf_hash();
        $data['comment_limit']    = $comment_limit    = 5;

        if (isset($_POST) && count($_POST) > 0) {
            $data['feed_id']        = $feed_id        = $this->input->post('feed_id', true);
            $start                  = $this->input->post('start', true);
            $limit                  = $this->input->post('limit', true);
            $data['user_id']        = $user_id        = $this->input->post('user_id', true);
            $data['sort_order']     = $sort_order     = $this->input->post('sort_order', true);
            $data['new_comment_id'] = $new_comment_id = $this->input->post('new_comment_id', true);
            $is_show_more           = $this->input->post('is_show_more', true);

            if ($feed_id != "" && $start != "" && $limit != "" && $user_id != "" && $sort_order != "" && $new_comment_id != "" && $is_show_more != "") {
                $result['flag'] = "success";

                if ($new_comment_id != '0') {$this->db->order_by("FIELD(bc.comment_id, '" . $new_comment_id . "')", "DESC", false);}
                if ($sort_order == 'Newest') {$this->db->order_by('bc.created_on', 'DESC', false);} else if ($sort_order == 'Oldest') {$this->db->order_by('bc.created_on', 'ASC', false);} else if ($sort_order == 'Best') {
                    $this->db->order_by('bc.total_likes', 'DESC', false);
                    $this->db->order_by('bc.created_on', 'DESC', false);}

                $select = "bc.comment_id, bc.feed_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture,
                    (SELECT COUNT(bc2.comment_id) FROM arai_admin_update_comments bc2 INNER JOIN arai_registration r2 ON r2.user_id = bc2.user_id WHERE bc2.parent_comment_id = bc.comment_id AND bc2.status = 1 AND r2.is_verified = 'yes' AND r2.status = 'Active' AND r2.is_valid = '1' AND r2.is_deleted = '0') AS TotalReply";
                $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
                $data['comment_data']       = $this->master_model->getRecords("arai_admin_update_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.feed_id' => $feed_id, 'r.is_verified' => "yes", 'r.status' => "Active", 'r.is_valid' => "1", 'r.is_deleted' => "0"), $select, '', $start, $limit);
                $result['comment_data_qry'] = $this->db->last_query();

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
                $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
                $data['comment_data_count'] = $comment_data_count = $this->master_model->getRecordCount("arai_admin_update_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.feed_id' => $feed_id, 'r.is_verified' => "yes", 'r.status' => "Active", 'r.is_valid' => "1", 'r.is_deleted' => "0"), 'bc.comment_id');
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT

                $data['feed_data']           = $this->master_model->getRecords("admin_update", array('is_deleted' => '0', 'feed_id' => $feed_id));
                $data['new_start']           = $new_start           = $start + $limit;
                $data['CommentActiondata']   = $this->getFeedCommentsActiondata($user_id);
                $result['response']          = $this->load->view('front/feeds/inc_comment_section', $data, true);
                $result['total_comment_cnt'] = $comment_data_count;
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function delete_comment_ajax() // DELETE BLOG COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $comment_type         = trim($this->security->xss_clean($this->input->post('comment_type')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $del_data['status']     = 2;
            $del_data['deleted_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('arai_admin_update_comments', $del_data, array('comment_id' => $comment_id));

            $new_comment_reply_cnt = $parent_comment_id = 0;
            $comment_data          = $this->master_model->getRecords("arai_admin_update_comments", array('comment_id' => $comment_id), 'parent_comment_id', '', 0, 1);
            if ($comment_type == 'reply') {

                $new_comment_reply_cnt = $this->master_model->getRecordCount("arai_admin_update_comments", array('status' => '1', 'parent_comment_id' => $comment_data[0]['parent_comment_id']), 'comment_id');
                $parent_comment_id     = $comment_data[0]['parent_comment_id'];
            }
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_data[0]['parent_comment_id'] . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $new_comment_reply_cnt . ')</p>';
            $result['parent_comment_id']     = $parent_comment_id;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete Feed Comment",
                'module_name' => 'Feed Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            //$this->session->set_flashdata('success_blog_comment_del_msg','Blog Comment has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getFeedCommentsActiondata($user_id = 0)
    {
        $user_like_comment = $this->master_model->getRecords("arai_admin_update_comment_likes", array('user_id' => $user_id), 'comment_id');

        $like_arr = $self_comment_arr = $return_arr = array();
        if (count($user_like_comment) > 0) {
            foreach ($user_like_comment as $res) {
                $like_arr[$res['comment_id']] = $res['comment_id'];
            }
        }

        $user_self_comment = $this->master_model->getRecords("arai_admin_update_comments", array('user_id' => $this->session->userdata('user_id')), 'comment_id');
        if (count($user_self_comment) > 0) {
            foreach ($user_self_comment as $res) {
                $self_comment_arr[$res['comment_id']] = $res['comment_id'];
            }
        }
        $return_arr['like_comments'] = $like_arr;
        $return_arr['self_comments'] = $self_comment_arr;

        return $return_arr;
    }

    public function like_unlike_comment_ajax() // LIKE - UNLIKE COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag                 = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $comment_data   = $this->master_model->getRecords("arai_admin_update_comments", array('comment_id' => $comment_id), 'total_likes', '', 0, 1);
            $new_total_like = $comment_data[0]['total_likes'];
            if ($flag == 1) {
                $action_name            = "Like Comment Technology Wall";
                $add_like['comment_id'] = $comment_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_admin_update_comment_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Like';
                $likeIcon   = 'fa-thumbs-up';
                $bg_color   = "style='color:#1562a6; font-weight:500;'";

                $new_total_like = $new_total_like + 1;
            } else {
                $this->db->where('comment_id', $comment_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_admin_update_comment_likes');

                $action_name = "Un-Like Comment Technology Wall";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-o-up';
                $bg_color    = "style='color:#000'";

                $new_total_like = $new_total_like - 1;
            }

            $up_data['total_likes'] = $new_total_like;
            $this->master_model->updateRecord('arai_admin_update_comments', $up_data, array('comment_id' => $comment_id));

            $onclick_fun        = "like_dislike_comment('" . base64_encode($comment_id) . "','" . $LikeFlag . "')";
            $result['response'] = '<p class="comment_like_btn" onclick="' . $onclick_fun . '" ' . $bg_color . '><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $new_total_like . ')' . '</p>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

}
