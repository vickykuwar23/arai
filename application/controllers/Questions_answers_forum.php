<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Questions_answers_forum extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');	
			$this->load->library('Opensslencryptdecrypt'); 
			$this->load->helper('auth');
			$this->load->helper('text');
			
			ini_set('upload_max_filesize', '100M');  
			ini_set('post_max_size', '100M');  
		} 
		
		public function index() //QUESTION & ANSWER FORUM LISTING
		{
			//$module_id = 8;
			//$this->check_permissions->is_authorise($module_id);   
			
			ini_set('display_errors', '1');
			ini_set('display_startup_errors', '1');
			error_reporting(E_ALL);	
			
      $this->check_permissions->is_logged_in();			
			$encrptopenssl =  New Opensslencryptdecrypt();  
			$data['featured_question_limit'] = $featured_question_limit = 15;
			$data['question_limit'] = $question_limit = 10;
			
			$data['tag_data'] = $this->master_model->getRecords("arai_qa_forum_tags", array("status" => 'Active'), '', array('tag_name'=>'ASC'));
			
			$data['page_title'] 	 = 'Questions & Answers Forum';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'questions_answers_forum/index';
			$this->load->view('front/front_combo',$data);
		}
		
		function getQaForumDataAjax()	//QUESTION & ANSWER FORUM LISTING DATA USING AJAX	
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$search_str = '';
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$fqa_start = $this->input->post('fqa_start', TRUE);
				$fqa_limit = $this->input->post('fqa_limit', TRUE);
				$qa_start = $this->input->post('qa_start', TRUE);
				$qa_limit = $this->input->post('qa_limit', TRUE);
				$featured_question_limit = $this->input->post('featured_question_limit', TRUE);
				$question_limit = $this->input->post('question_limit', TRUE);
				$is_show_more = $this->input->post('is_show_more', TRUE);
				if($fqa_start != "" && $fqa_limit!= "" && $qa_start != "" && $qa_limit!= "" && $featured_question_limit != "" && $question_limit != "" && $is_show_more!= "" )
				{
					$result['flag'] = "success";
					$user_id = $this->session->userdata('user_id');			
					$QaActiondata = $this->getQaActiondata($user_id);
					
					//START : FOR SEARCH KEYWORD
					$keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword')); 
					if($keyword)
					{ 
						$search_str	.= " ( qa.custum_question_id LIKE '%".$keyword."%' OR qa.question LIKE '%".$keyword."%' OR qa.tags_other LIKE '%".$keyword."%' OR r.first_name_decrypt LIKE '%".$keyword."%' OR r.middle_name_decrypt LIKE '%".$keyword."%' OR r.last_name_decrypt LIKE '%".$keyword."%') ";
					}
					//END : FOR SEARCH KEYWORD
					
					//START : FOR SEARCH TAG
					$tag = trim($this->input->post('tag'));
					if($tag != '')
					{
						$tag_arr = explode(",",$tag);
						if(count($tag_arr) > 0)
						{
							$old_search_str = $search_str;
							if($old_search_str != '') { $search_str .= " AND "; }
							
							$search_str .= " (";
							$i = 0;
							foreach($tag_arr as $res)
							{ 
								$search_str .= " FIND_IN_SET('".$res."',qa.tags)";
								if($i != (count($tag_arr) - 1)) { $search_str .= " OR "; }			
								$i++;
							}								
							$search_str .= " )";
						}
					}
					//END : FOR SEARCH TAG
					
					//START : FOR SEARCH TYPE
					$search_type = trim($this->input->post('search_type'));
					if($search_type != '')
					{
						$search_type_arr = explode(",",$search_type);
						if(count($search_type_arr) > 0)
						{
							$old_search_str = $search_str;
							if($old_search_str != '') { $search_str .= " AND "; }
														
							$search_str .= " (";
							$i = 0;
							foreach($search_type_arr as $res)
							{ 
								if($res == '0') { $search_str .= " r.user_category_id = '2'"; }
								else { $search_str .= " r.user_sub_category_id = '".$res."'"; }
								
								if($i != (count($search_type_arr) - 1)) { $search_str .= " OR "; }			
								$i++;
							}								
							$search_str .= " )";
						}
					}					
					//END : FOR SEARCH TYPE

					//END : FOR SEARCH DATE
	                $from_date = trim($this->input->post('from_date'));
	                $to_date   = trim($this->input->post('to_date'));

	                if ($from_date != '' && $to_date != '') 
									{

										$from_date = date('Y-m-d', strtotime($from_date));
										$to_date   = date('Y-m-d', strtotime($to_date));

	                	$old_search_str = $search_str;
										if ($old_search_str != '') {$search_str .= " AND ";}
										$search_str .= "( DATE(qa.created_on) BETWEEN '" . $from_date . "' and '" . $to_date . "' )";
	                }
					
					// START : FOR FEATURED QUESTION
					$featured_question_total_cnt = 0;
					if($is_show_more == 0)
					{
						if($search_str != "") { $this->db->where($search_str); }			
						//$this->db->order_by('qa.q_id','DESC',FALSE);						
						$this->db->order_by('ISNULL(qa.xOrder), qa.xOrder = 0, qa.xOrder, qa.created_on DESC','',FALSE);						
						$select_fqa = "qa.q_id, qa.user_id, qa.custum_question_id, qa.question, qa.qa_image, qa.tags, qa.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_qa_forum_tags WHERE FIND_IN_SET(id,tags)) AS DispTags, (SELECT COUNT(like_id) FROM arai_qa_forum_likes WHERE q_id = qa.q_id) AS LikeCnt, (SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE q_id = qa.q_id AND status = 1 AND is_block = 0 AND parent_comment_id = 0) AS CommentCnt, qa.tags_other, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, org.org_logo, sp.profile_picture";
						$this->db->join('arai_registration r','r.user_id = qa.user_id', 'LEFT', FALSE);
						$this->db->join('arai_profile_organization org','org.user_id = qa.user_id', 'LEFT', FALSE);
						$this->db->join('arai_student_profile sp','sp.user_id = qa.user_id', 'LEFT', FALSE);
						$data_fqa['fqa_forum_data'] = $fqa_forum_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.is_deleted' => '0', 'qa.admin_status' => '1', 'qa.is_featured' => '1', 'qa.is_block' => '0'),$select_fqa,'',$fqa_start,$fqa_limit);						
						
						$featured_question_total_cnt = count($fqa_forum_data);						
						$data_fqa['QaActiondata'] = $QaActiondata;
						$data_fqa['CommentActiondata'] = $this->getCommentsActiondata($user_id);
						$result['Featured_response'] = $this->load->view('front/questions_answers_forum/incFeatureQACommon', $data_fqa, true);
					} 
					// END : FOR FEATURED QUESTION
					
					// START : FOR QA FORUM DATA 
					if($search_str != "") { $this->db->where($search_str); }					
					//$this->db->order_by('qa.xOrder','ASC',FALSE);
					//$this->db->order_by('qa.q_id','DESC',FALSE);
					$this->db->order_by('ISNULL(qa.xOrder), qa.xOrder = 0, qa.xOrder, qa.created_on DESC','',FALSE);
					$select_qa = "qa.q_id, qa.user_id, qa.custum_question_id, qa.question, qa.qa_image, qa.tags, qa.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_qa_forum_tags WHERE FIND_IN_SET(id,tags)) AS DispTags, (SELECT COUNT(like_id) FROM arai_qa_forum_likes WHERE q_id = qa.q_id) AS LikeCnt,(SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE q_id = qa.q_id AND status = 1 AND is_block = 0 AND parent_comment_id = 0) AS CommentCnt,  qa.tags_other, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, org.org_logo, sp.profile_picture";
					$this->db->join('arai_registration r','r.user_id = qa.user_id', 'LEFT', FALSE);
					$this->db->join('arai_profile_organization org','org.user_id = qa.user_id', 'LEFT', FALSE);
					$this->db->join('arai_student_profile sp','sp.user_id = qa.user_id', 'LEFT', FALSE);
					$data_qa['qa_forum_data'] = $qa_forum_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.is_deleted' => '0', 'qa.admin_status' => '1', 'qa.is_featured' => '0', 'qa.is_block' => '0'),$select_qa,'',$qa_start,$qa_limit);
					$result['qa_qry'] = $this->db->last_query();
					$data_qa['QaActiondata'] = $QaActiondata;
					$data_qa['CommentActiondata'] = $this->getCommentsActiondata($user_id);
					// END : FOR QA FORUM DATA
					
					//START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR QA FORUM DATA
					if($search_str != "") { $this->db->where($search_str); }
					$this->db->join('arai_registration r','r.user_id = qa.user_id', 'LEFT', FALSE);
					$data_qa['qa_forum_total_cnt'] = $qa_forum_total_cnt = $this->master_model->getRecordCount('arai_qa_forum qa', array('qa.is_deleted' => '0', 'qa.admin_status' => '1', 'qa.is_featured' => '0', 'qa.is_block' => '0'), 'qa.q_id');
					//END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR QA FORUM DATA
					
					$data_qa['featured_question_total_cnt'] = $featured_question_total_cnt;
					$data_qa['featured_question_limit'] = $featured_question_limit;										
					$data_qa['question_limit'] = $question_limit;										
					$data_qa['new_start'] = $new_start = $qa_start + $qa_limit;
					$result['qa_forum_question_total_cnt'] = count($qa_forum_data);
					$result['QA_forum_response'] = $this->load->view('front/questions_answers_forum/incAllQaForumCommon', $data_qa, true);					
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";	
			}
			
			echo json_encode($result);
		}
		
		public function getQaActiondata($user_id=0) //QUESTION & ANSWER LIKE, REPORT DATA
		{
			$user_likes = $this->master_model->getRecords("arai_qa_forum_likes",array('user_id' => $user_id),'q_id');
			
			$like_arr = $self_arr = $reported_arr = $return_arr = array();
			if(count($user_likes) > 0)
			{
				foreach($user_likes as $res)
				{
					$like_arr[$res['q_id']] = $res['q_id'];
				}
			}
			
			$user_self_qa = $this->master_model->getRecords("arai_qa_forum",array('user_id' => $user_id),'q_id');
			if(count($user_self_qa) > 0)
			{
				foreach($user_self_qa as $res)
				{
					$self_arr[$res['q_id']] = $res['q_id'];
				}
			}
			
			$user_reported_qa = $this->master_model->getRecords("arai_qa_forum_reported",array('user_id' => $user_id),'q_id');			
			if(count($user_reported_qa) > 0)
			{
				foreach($user_reported_qa as $res)
				{
					$reported_arr[$res['q_id']] = $res['q_id'];
				}
			}
			
			$return_arr['like_arr'] = $like_arr;
			$return_arr['self_arr'] = $self_arr;
			$return_arr['reported_arr'] = $reported_arr;
			
			return $return_arr;
		}
		
		public function like_unlike_question_ajax()// LIKE - UNLIKE QUESTION
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$flag = trim($this->security->xss_clean($this->input->post('flag')));	
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				$result['q_id'] = $q_id;
				
				if($flag == 1)
				{
					$add_like['q_id'] = $q_id;
					$add_like['user_id'] = $user_id;
					$add_like['created_on'] = date('Y-m-d H:i:s');
					$this->master_model->insertRecord('arai_qa_forum_likes',$add_like);
					
					$action_name = "Like Question Technology Wall";
					$Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down'; $LikeClass = 'invert_color'; 
				}
				else
				{
					$this->db->where('q_id', $q_id);
					$this->db->where('user_id', $user_id);
					$this->db->delete('arai_qa_forum_likes');
					
					$action_name = "Un-Like Question Technology Wall";
					$Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa fa-thumbs-o-up'; $LikeClass = '';
				}
				
				$total_liker_cnt = $this->master_model->getRecordCount('arai_qa_forum_likes', array('q_id' => $q_id), 'like_id');
				
				$onclick_fun = "like_unlike_question('".base64_encode($q_id)."','".$LikeFlag."')";
				$result['response'] = '<span class="mr-1">'.$total_liker_cnt.'</span><span class="'.$LikeClass.'" onclick="'.$onclick_fun.'"><i class="fa '.$likeIcon.'" aria-hidden="true"></i> '.$Like_label.'</span>';
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 	=> $action_name,
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}		
		
		public function report_qa_ajax()// REPORT QUESTION
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('q_id'))));					
				$popupQuestionReportComment = trim($this->security->xss_clean($this->input->post('popupQuestionReportComment')));					
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				$result['q_id'] = $q_id;
				
				$action_name = "Reported Question Technology Wall";
				$add_reported['q_id'] = $q_id;
				$add_reported['comments'] = $popupQuestionReportComment;
				$add_reported['user_id'] = $user_id;
				$add_reported['created_on'] = date('Y-m-d H:i:s');
				$reported_id = $this->master_model->insertRecord('arai_qa_forum_reported',$add_reported, TRUE);
				
				$up_data['custom_reported_id'] = "TN-QA-RP-".sprintf("%07d", $reported_id);
				$this->master_model->updateRecord('arai_qa_forum_reported',$up_data,array('reported_id' => $reported_id));
				
				//START : SEND MAIL TO QUESTION OWNER WHEN Question IS REPORTED - qa_mail_to_user_on_question_reported
				$email_info = $this->get_mail_data_qa($q_id);
					
				$email_send='';
				$slug = $encrptopenssl->encrypt('qa_mail_to_user_on_question_reported');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
					
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
					'[QUESTIONPOSTER_NAME]',
					'[QUESTION]',
					'[REASON]'											
					]; 
					$rep_array = [
					$email_info['QUESTION_OWNER_NAME'],
					$email_info['QUESTION'],
					$popupQuestionReportComment								
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);					
					
					$info_array=array(
					'to' => $email_info['QUESTION_OWNER_EMAIL'], 
					'cc' => '',
					'from' => $fromadmin,
					'subject' => $subject_title,
					'view' => 'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				}
				//END : SEND MAIL TO QUESTION OWNER WHEN Question IS REPORTED - qa_mail_to_user_on_question_reported 
				
				
				//START : SEND MAIL TO ADMIN WHEN Question IS REPORTED - qa_mail_to_admin_on_question_reported            
				$email_info = $this->get_mail_data_qa($q_id,$user_id);
					
				$email_send='';
				$slug = $encrptopenssl->encrypt('qa_mail_to_admin_on_question_reported');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
					
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
					'[QUESTION]',
					'[QUESTIONPOSTER_NAME]',
					'[REASON]'										
					]; 
					$rep_array = [
					$email_info['QUESTION'],
					$email_info['QUESTION_OWNER_NAME'],
					$popupQuestionReportComment	
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);           		
					
					$info_array=array(
					'to' =>	$email_info['ADMIN_EMAIL'],
					'cc' =>	'',
					'from' =>	$fromadmin,
					'subject'	=> $subject_title,
					'view' => 'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				}
				//END : SEND MAIL TO ADMIN WHEN Question IS REPORTED - qa_mail_to_admin_on_question_reported
				
				$Reported_label = 'Question Already Reported';	
				$ReportedFlag = '0';
				
				$onclick_fun = "report_qa('".base64_encode($q_id)."','".$ReportedFlag."', '', '')";
				$result['response'] = '<a class="btn btn-sm btn-danger btn_report" href="javascript:void(0)"  title="'.$Reported_label.'" onclick="'.$onclick_fun.'"> Report</a>';

				

				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 	=> $action_name,
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function show_likes_user_data_ajax()// SHOW LIKES USER DATA
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				
				$result['flag'] = "success";				
				$result['q_id'] = $q_id;
				
				$this->db->order_by('q_id','DESC',FALSE);
				$select = "qa.like_id, qa.q_id, qa.user_id, qa.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
				$this->db->join('arai_registration r','r.user_id = qa.user_id', 'INNER', FALSE);
				$data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_qa_forum_likes qa",array('qa.q_id'=>$q_id, 'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"),$select);
				//$result['like_qry'] = $this->db->last_query();
				$result['response'] = $this->load->view('front/questions_answers_forum/incLikesModal', $data, true);
				
				$qa_data = $this->master_model->getRecords("arai_qa_forum",array('q_id'=>$q_id),"custum_question_id");				
				$result['question'] = "Question Likes : ".$qa_data[0]['custum_question_id'];
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function show_comment_likes_user_data_ajax()// SHOW COMMENt / ANSWER LIKES USER DATA
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$comment_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				
				$result['flag'] = "success";				
				$result['comment_id'] = $comment_id;
				
				$this->db->order_by('like_id','DESC',FALSE);
				$select = "qa.like_id, qa.comment_id, qa.user_id, qa.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
				$this->db->join('arai_registration r','r.user_id = qa.user_id', 'INNER', FALSE);
				$data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_qa_comment_likes qa",array('qa.comment_id'=>$comment_id, 'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"),$select);
				//$result['like_qry'] = $this->db->last_query();
				$result['response'] = $this->load->view('front/questions_answers_forum/incLikesModal', $data, true);
				
				$comment_data = $this->master_model->getRecords("arai_qa_forum_comments",array('comment_id'=>$comment_id),"disp_comment_id");				
				$result['comment'] = "Comment Likes : ".$comment_data[0]['disp_comment_id'];
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function show_reported_user_ajax()// SHOW REPORTED USER DATA
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('q_id'))));	
				
				$result['flag'] = "success";				
				$result['q_id'] = $q_id;
				
				$this->db->order_by('q_id','DESC',FALSE);
				$select = "br.reported_id, custom_reported_id, br.q_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
				$this->db->join('arai_registration r','r.user_id = br.user_id', 'INNER', FALSE);
				$data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_qa_forum_reported br",array('br.q_id'=>$q_id),$select);
				$result['response'] = $this->load->view('front/questions_answers_forum/incReportedModal', $data, true);				
				
				$qa_data = $this->master_model->getRecords("arai_qa_forum",array('q_id'=>$q_id),"custum_question_id, question");				
				$result['question'] = "<b>QID :</b> ".$qa_data[0]['custum_question_id'];
				$result['question_name'] = "<b>Question :</b> ".$qa_data[0]['question'];
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function add_qa($id=0) // ADD NEW QUESTION
		{			
			$this->check_permissions->is_logged_in();	
			
			$module_id = 36;
			$this->check_permissions->is_authorise($module_id);  	
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->load->library('upload');			
			$user_id = $this->session->userdata('user_id');
			
			if($id == '0') { $data['mode'] = $mode = "Add";}
			else
			{
				$id = base64_decode($id);
				
				$data['form_data'] = $form_data = $this->master_model->getRecords("arai_qa_forum",array('q_id' => $id, 'user_id' => $user_id, 'is_deleted' => 0));
				if(count($form_data) > 0)
				{
					$data['mode'] = $mode = "Update";					
				}
				else { $data['mode'] = $mode = "Add"; }
			}
			
			$data['qa_image_error'] = $data['tags_error'] = $error_flag = '';
			$file_upload_flag = 0;
			if(isset($_POST) && count($_POST) > 0)
			{
				//$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');			
				$this->form_validation->set_rules('question', 'Question', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));			
				
				if($mode == 'Add' && ((!isset($_FILES['qa_image'])) || $_FILES['qa_image']['size'] == 0))
				{
					//$data['qa_image_error'] = 'Please select the Question Image';		
					//$error_flag = 1;
				}
				
				if(!isset($_POST['tags']))
				{
					$data['tags_error'] = 'Please select the Tags';		
					$error_flag = 1;
				}
				
				if($this->form_validation->run() && $error_flag == '')
				{
					$postArr = $this->input->post();
					
					if($_FILES['qa_image']['name'] != "") //UPLOAD BLOG BANNER IMAGE
					{
						$qa_image = $this->Common_model_sm->upload_single_file("qa_image", array('png','jpg','jpeg','gif'), "qa_image_".date("YmdHis"), "./uploads/qa_image", "png|jpeg|jpg|gif");
						if($qa_image['response'] == 'error')
						{
							$data['qa_image_error'] = $qa_image['message'];
							$file_upload_flag = 1;
						}
						else if($qa_image['response'] == 'success')
						{
							$add_data['qa_image'] = $qa_image['message'];	
							/* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
						}
					}
					
					if($file_upload_flag == 0)
					{
						$add_data['user_id'] = $user_id;
						$add_data['question'] = $this->input->post('question');
						$add_data['tags'] = implode(",",$this->input->post('tags'));
						
						$tags_other = trim($this->security->xss_clean($this->input->post('tags_other')));
						if(isset($tags_other) && $tags_other != "")
						{
							$add_data['tags_other'] = trim($this->security->xss_clean($tags_other));
							    if ($mode=='Add') {
                    $this->check_permissions->StoreOtherData($tags_other, 'arai_qa_forum_tags','tag_name');
                }
						}
						else { $add_data['tags_other'] = ''; }
						//print_r($add_data); exit;
						
						if($mode == 'Add')
						{
							$add_data['admin_status'] = 0;
							$add_data['is_deleted'] = 0;
							$add_data['is_block'] = 0;
							$add_data['created_on'] = date('Y-m-d H:i:s');
							
							$q_id = $this->master_model->insertRecord('arai_qa_forum',$add_data, TRUE);
							$up_data['custum_question_id'] = "TN-QA-Q-".sprintf("%07d", $q_id);
							$this->master_model->updateRecord('arai_qa_forum',$up_data,array('q_id' => $q_id));
							
							//$email_info = $this->get_mail_data_qa($blog_id);
							
							//START : SEND MAIL TO USER WHEN BLOG IS POSTED - blog_mail_to_user_while_writing_blog    
							/* $email_send='';
								$slug = $encrptopenssl->encrypt('blog_mail_to_user_while_writing_blog');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								if(count($subscriber_mail) > 0)
								{								
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[USER_NAME]',
								'[Blog_Title]',
								'[Blog_Date]'														
								]; 
								$rep_array = [
								$email_info['USER_NAME'],
								$email_info['Blog_Title'],
								$email_info['Blog_Date']											
								];
								$sub_content = str_replace($arr_words, $rep_array, $desc);								
								
								$info_array=array(
								'to'		=>	$email_info['USER_EMAIL'],
								'cc'		=>	'',
								'from'		=>	$fromadmin,
								'subject'	=> 	$subject_title,
								'view'		=>  'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
							} */	
							//END : SEND MAIL TO USER WHEN BLOG IS POSTED - blog_mail_to_user_while_writing_blog
							
							//START : SEND MAIL TO ADMIN WHEN BLOG IS POSTED - blog_mail_to_admin_while_writing_blog
							/* $email_send='';
								$slug = $encrptopenssl->encrypt('blog_mail_to_admin_while_writing_blog');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								if(count($subscriber_mail) > 0)
								{								
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[USER_NAME]',
								'[Blog_Title]',
								'[USER_ID]'														
								]; 
								$rep_array = [
								$email_info['USER_NAME'],
								$email_info['Blog_Title'],
								$email_info['USER_ID']											
								];
								$sub_content = str_replace($arr_words, $rep_array, $desc);								
								
								$info_array=array(
								'to'		=>	$email_info['admin_email'],
								'cc'		=>	'',
								'from'		=>	$fromadmin,
								'subject'	=> 	$subject_title,
								'view'		=>  'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
							}	 */
							//END : SEND MAIL TO ADMIN WHEN BLOG IS POSTED - blog_mail_to_admin_while_writing_blog
							
							// Log Data Added 
							$filesData 			= $_FILES;
							$json_data 			= array_merge($postArr,$filesData);
							$json_encode_data 	= json_encode($json_data);				
							$ipAddr			  	= $this->get_client_ip();
							$createdAt			= date('Y-m-d H:i:s');
							$logDetails 		= array(
							'user_id' 			=> $user_id,
							'action_name' 		=> "Add Question Technology Wall",
							'module_name'		=> 'Question & Answer Forum Technology Wall Front',
							'store_data'		=> $json_encode_data,
							'ip_address'		=> $ipAddr,
							'createdAt'			=> $createdAt
							);
							$logData = $this->master_model->insertRecord('logs',$logDetails);
							
							$this->session->set_flashdata('success','Question has been successfully posted');								
						}
						else if($mode == 'Update')
						{
							$add_data['updated_on'] = date('Y-m-d H:i:s');
							
							$q_id = $id;
							$this->master_model->updateRecord('arai_qa_forum',$add_data,array('q_id' => $q_id));
							//echo $this->db->last_query(); exit;
							
							// Log Data Added 
							$filesData 			= $_FILES;
							$json_data 			= array_merge($postArr,$filesData);
							$json_encode_data 	= json_encode($json_data);				
							$ipAddr			  	= $this->get_client_ip();
							$createdAt			= date('Y-m-d H:i:s');
							$logDetails 		= array(
							'user_id' 			=> $user_id,
							'action_name' 		=> "Update Question Technology Wall",
							'module_name'		=> 'Question & Answer Forum Technology Wall Front',
							'store_data'		=> $json_encode_data,
							'ip_address'		=> $ipAddr,
							'createdAt'			=> $createdAt
							);
							$logData = $this->master_model->insertRecord('logs',$logDetails);
							
							$this->session->set_flashdata('success','Question has been successfully updated');								
						}
						
						redirect(site_url('questions_answers_forum/myQuestions'));
					}
				}
			}
			
			$this->db->order_by('FIELD(tag_name, "Other") ASC, tag_name ASC');
			$data['tag_data'] = $this->master_model->getRecords("arai_qa_forum_tags", array("status" => 'Active'));	
			
			$data['page_title'] = 'QA Forum';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'questions_answers_forum/add_qa';
			$this->load->view('front/front_combo',$data);
		}
		
		public function myQuestions() // MY QUESTION LIST
		{
			$this->check_permissions->is_logged_in();
			$user_id = $this->session->userdata('user_id');			
			$data['page_title'] = 'Questions & Answers Forum';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'questions_answers_forum/myQuestions';
			$this->load->view('front/front_combo',$data);			
		}
		
		public function myQuestionsAjax() // MY QUESTION LIST : GET SERVER SIDE DATATABLE DATA USING AJAX
		{		
			//$this->check_permissions->is_logged_in();		
			$csrf_test_name = $this->security->get_csrf_hash();			
			$user_id = $this->session->userdata('user_id');	
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = @$_POST['order'][0]['column']; // Column index
			$columnName = @$_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = @$_POST['order'][0]['dir']; // asc or desc
			$searchValue = @$_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;	
			
			$condition = "SELECT * FROM arai_qa_forum WHERE is_deleted = '0' AND user_id = '".$user_id."' ORDER BY q_id DESC";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$result 	= $this->db->query($query)->result();
			$rowCount = $this->getNumData($condition);
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0)
			{				
				$no = $_POST['start'];
				$i = 0;
				foreach($result as $showResult)
				{					
					$q_id = $showResult->q_id;
					$custum_question_id = '<a href="'.base_url('questions_answers_forum/QaDetails/'.base64_encode($q_id)).'" class="applicant-listing">'.$showResult->custum_question_id.'</a>';
					$question = ucwords($showResult->question);
					$posted_on = date("d M, Y h:i a", strtotime($showResult->created_on));
					
					$admin_status = $action = '';
					
					if($showResult->admin_status == 0) { $admin_status = 'Pending'; }
					else if($showResult->admin_status == 1) { $admin_status = 'Approved'; }
					else if($showResult->admin_status == 2) { $admin_status = 'Rejected'; }					
					
					$action	.= '<a href="'.base_url('questions_answers_forum/add_qa/'.base64_encode($q_id)).'" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;';
					
					if($showResult->is_block == '0') { $q_title = 'Block'; $q_icon = 'fa fa-ban'; } else if($showResult->is_block == '1') { $q_title = 'Unblock'; $q_icon = 'fa fa-unlock-alt'; }
					$onclick_block_fun = "block_unblock_question('".base64_encode($q_id)."', '".$q_title."', '".$showResult->custum_question_id."', '".$showResult->question."')";
					
					$action	.= '<a href="javascript:void(0);" data-toggle="tooltip" title="'.$q_title.'" class="btn btn-info btn-green-fresh" onclick="'.$onclick_block_fun.'"><i class="'.$q_icon.'" aria-hidden="true"></i></a>&nbsp;&nbsp;';
					
					$onclick_delete_fun = "delete_question('".base64_encode($q_id)."')";
					$action	.= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" class="btn btn-info btn-green-fresh" onclick="'.$onclick_delete_fun.'"><i class="fa fa-remove" aria-hidden="true"></i></a> ';
					
					$onclick_like_fun = "show_question_likes('".base64_encode($q_id)."')";
					$getTotalLikes = $this->master_model->getRecordCount('arai_qa_forum_likes', array('q_id' => $q_id), 'like_id');
					$disp_likes = '<div class="text-center"><a style="min-width:30px;" href="javascript:void(0);" data-toggle="tooltip" title="Likes" class="btn btn-info btn-green-fresh" onclick="'.$onclick_like_fun.'">'.$getTotalLikes.'</a></div>';
					
					$this->db->join("arai_registration r","r.user_id = b.user_id", "INNER", FALSE);
					$getTotalComments = $this->master_model->getRecordCount('arai_qa_forum_comments b', array('b.q_id' => $q_id, 'b.status'=>1, 'b.is_block'=>0, 'b.parent_comment_id'=>0), 'b.comment_id');
					$disp_comments = '<div class="text-center"><a style="min-width:30px;" href="'.site_url('questions_answers_forum/QaDetails/'.base64_encode($q_id).'/1').'" data-toggle="tooltip" title="Likes" class="btn btn-info btn-green-fresh">'.$getTotalComments.'</a></div>';
					
					$onclick_reported_fun = "show_question_reported('".base64_encode($q_id)."')";
					$getTotalReported = $this->master_model->getRecordCount('arai_qa_forum_reported', array('q_id' => $q_id), 'reported_id');
					$disp_reported = '<div class="text-center"><a style="min-width:30px;" href="javascript:void(0);" data-toggle="tooltip" title="Reported" class="btn btn-info btn-green-fresh" onclick="'.$onclick_reported_fun.'">'.$getTotalReported.'</a></div>';
					
					$i++;			 		
					$dataArr[] = array(
					$custum_question_id,
					$question,
					$posted_on,
					$admin_status,
					$disp_likes,
					$disp_comments,
					$disp_reported,
					"<span style='white-space:nowrap;'>".$action."</span>"
					);					
			    $rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}				
			} 
			else 
			{			
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);			
				$response['token'] = $csrf_test_name;
			}			
			echo json_encode($response);
			
		} // End Function
		
		public function block_unblock_question()// BLOCK/UNBLOCK QUESTION
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupQuestionBlockReason = trim($this->security->xss_clean($this->input->post('popupQuestionBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					//START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked        
					/* $email_info = $this->get_mail_data_qa($blog_id);
						
						$email_send='';
						$slug = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0)
						{								
						$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[Blog_Title]',        								
						'[BLOCKED_REASON]'        								
						]; 
						$rep_array = [
						$email_info['Blog_Title'],
						$popupQuestionBlockReason
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);        		
						
						$info_array=array(
						'to'		=>	$email_info['admin_email'], 
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
					} */        
					//END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupQuestionBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum',$up_data,array('q_id' => $q_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Block Question Technology Wall",
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function delete_question()// DELETE QUESTION
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				$result['q_id'] = $q_id;
				
				$del_data['is_deleted'] = 1;
				$del_data['deleted_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum',$del_data,array('q_id' => $q_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Delete Question Technology Wall",
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('success_question_del_msg','Question has been successfully deleted');		
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		
		function QaDetails($q_id=0, $comment_flag=0) //QUESTION DETAILS PAGE
		{
			$module_id = 37;
			$this->check_permissions->is_authorise($module_id);

			$this->check_permissions->is_logged_in();
			$encrptopenssl =  New Opensslencryptdecrypt();
			$data['user_id'] = $user_id = $this->session->userdata('user_id');
			$data['comment_limit'] = $comment_limit = 5;
			$data['comment_flag'] = $comment_flag;
			
			if($q_id == '0') { redirect(site_url('questions_answers_forum')); }
			else
			{
				$q_id = base64_decode($q_id);				
				$data['q_id'] = $q_id;
				
				$select = "qa.q_id, qa.user_id, qa.custum_question_id, qa.question, qa.qa_image, qa.tags, qa.admin_status, qa.is_block, qa.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_qa_forum_tags WHERE FIND_IN_SET(id,tags)) AS DispTags, (SELECT COUNT(like_id) FROM arai_qa_forum_likes WHERE q_id = qa.q_id) AS LikeCnt, qa.tags_other, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, org.org_logo, sp.profile_picture, sp.designation";				
				$this->db->join('arai_registration r','r.user_id = qa.user_id', 'LEFT', FALSE);
				$this->db->join('arai_profile_organization org','org.user_id = qa.user_id', 'LEFT', FALSE);
				$this->db->join('arai_student_profile sp','sp.user_id = qa.user_id', 'LEFT', FALSE);				
				$data['form_data'] = $form_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.q_id' => $q_id, 'qa.is_deleted' => '0'),$select); //, 'qa.admin_status' => '1', 'qa.is_block' => '0'
				
				//echo "<pre>"; print_r($form_data); echo "</pre>";
				
				$redirect_flag = 0;
				if(count($form_data) == 0) { $redirect_flag = 1; }
				else if($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) { $redirect_flag = 1; }
				else if($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) { $redirect_flag = 1; }				
				
				if($redirect_flag == 1) { redirect(site_url('questions_answers_forum')); }
			}
			
			$data['QaActiondata'] = $this->getQaActiondata($user_id);
			$data['page_title'] = 'Question Details';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'questions_answers_forum/qa_details';
			$this->load->view('front/front_combo',$data);
		}
		
		function get_comment_data_ajax() //QUESTION COMMENTS/ANSWER DATA
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$result['csrf_new_token'] = $csrf_new_token = $this->security->get_csrf_hash();
			$data['comment_limit'] = $comment_limit = 5;			
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$data['q_id'] = $q_id = $this->input->post('q_id', TRUE);
				$start = $this->input->post('start', TRUE);
				$limit = $this->input->post('limit', TRUE);
				$data['user_id'] = $user_id = $this->input->post('user_id', TRUE);
				$data['sort_order'] = $sort_order = $this->input->post('sort_order', TRUE);
				$data['new_comment_id'] = $new_comment_id = $this->input->post('new_comment_id', TRUE);
				$is_show_more = $this->input->post('is_show_more', TRUE);
				
				if($q_id != "" && $start!= "" && $limit != "" && $user_id!= "" && $sort_order!= ""  && $new_comment_id!= ""  && $is_show_more!= "" )
				{
					$result['flag'] = "success";
					
					if($new_comment_id != '0') { $this->db->order_by("FIELD(bc.comment_id, '".$new_comment_id."')", "DESC", false); }
					if($sort_order == 'Newest') { $this->db->order_by('bc.created_on','DESC',FALSE); }
					else if($sort_order == 'Oldest') { $this->db->order_by('bc.created_on','ASC',FALSE); }
					else if($sort_order == 'Best') { $this->db->order_by('bc.total_likes','DESC',FALSE); $this->db->order_by('bc.created_on','DESC',FALSE); }					
					
					$select = "bc.comment_id, bc.q_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id,r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture, 
					(SELECT COUNT(bc2.comment_id) FROM arai_qa_forum_comments bc2 INNER JOIN arai_registration r2 ON r2.user_id = bc2.user_id WHERE bc2.parent_comment_id = bc.comment_id AND bc2.status = 1 AND bc2.is_block = 0 AND r2.is_verified = 'yes' AND r2.status = 'Active' AND r2.is_valid = '1' AND r2.is_deleted = '0') AS TotalReply";
					$this->db->join('arai_registration r','r.user_id = bc.user_id', 'LEFT', FALSE);
					$this->db->join('arai_profile_organization org','org.user_id = bc.user_id', 'LEFT', FALSE);
					$this->db->join('arai_student_profile sp','sp.user_id = bc.user_id', 'LEFT', FALSE);					
					$data['comment_data'] = $this->master_model->getRecords("arai_qa_forum_comments bc",array('bc.status' => '1', 'bc.is_block' => '0', 'bc.parent_comment_id' => '0', 'bc.q_id' => $q_id, 'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"),$select,'',$start,$limit);
					$result['comment_data_qry'] = $this->db->last_query();
					
					//START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
					$this->db->join('arai_registration r','r.user_id = bc.user_id', 'LEFT', FALSE);
					$data['comment_data_count'] = $comment_data_count = $this->master_model->getRecordCount("arai_qa_forum_comments bc",array('bc.status' => '1', 'bc.is_block' => '0', 'bc.parent_comment_id' => '0', 'bc.q_id' => $q_id, 'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"),'bc.comment_id');
					//END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
					
					$data['qa_data'] = $this->master_model->getRecords("arai_qa_forum",array('is_deleted' => '0', 'q_id' => $q_id));
					$data['new_start'] = $new_start = $start + $limit;
					$data['CommentActiondata'] = $this->getCommentsActiondata($user_id);
					$result['response'] = $this->load->view('front/questions_answers_forum/inc_comment_section', $data, true);
					$result['total_comment_cnt'] = $comment_data_count;
					// END : FOR NON FEATURED BLOG
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";	
			}
			
			echo json_encode($result);
		}
		
		function post_comment_ajax()
		{
			$encObj =  New Opensslencryptdecrypt();
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$result['flag'] = "success";
				$user_id = trim($this->security->xss_clean($this->input->post('user_id')));	
				$q_id = trim($this->security->xss_clean($this->input->post('q_id')));	
				$comment_id = trim($this->security->xss_clean($this->input->post('comment_id')));	
				$comment_content = trim($this->security->xss_clean($this->input->post('comment_content')));	
				
				$add_data['q_id'] = $q_id;				
				$add_data['user_id'] = $user_id;
				$add_data['parent_comment_id'] = $comment_id;
				$add_data['comment'] = $comment_content;
				$add_data['status'] = 1;
				$add_data['created_on'] = date('Y-m-d H:i:s');
				
				$result['new_comment_id'] = $last_inserted_comment = $this->master_model->insertRecord('arai_qa_forum_comments',$add_data, TRUE);
				$up_data_id['disp_comment_id'] = "TN-QA-A-".sprintf("%08d", $last_inserted_comment);
				$this->master_model->updateRecord('arai_qa_forum_comments',$up_data_id,array('comment_id' => $last_inserted_comment));
				
				if($comment_id == 0)
				{
					$email_info = $this->get_mail_data_qa($q_id,$user_id);
						
					//START : SEND MAIL TO QUESTION OWNER WHEN SOMEONE ANSWER/COMMENT THE QUESTION - qa_mail_to_user_on_question_is_answered
					$email_send='';
					$slug = $encObj->encrypt('qa_mail_to_user_on_question_is_answered');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
						
					if(count($subscriber_mail) > 0)
					{								
						$subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encObj->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encObj->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encObj->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encObj->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[QUESTIONPOSTER_NAME]',
						'[ANSWERED_NAME]',
						'[QUESTION]',
						'[LINK_OF_THE_QUESTION]'						
						]; 
						$rep_array = [
						$email_info['QUESTION_OWNER_NAME'],
						$email_info['COMMENTING_USER_NAME'],
						$email_info['QUESTION'],
						$email_info['LINK_OF_QUESTION']							
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);					
						
						$info_array=array(
						'to' => $email_info['QUESTION_OWNER_EMAIL'],
						'cc' => '',
						'from' =>	$fromadmin,
						'subject'	=> $subject_title,
						'view' => 'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
					}
					//END : SEND MAIL TO QUESTION OWNER WHEN SOMEONE ANSWER/COMMENT THE QUESTION - qa_mail_to_user_on_question_is_answered
				}
				else if($comment_id != '0')
				{
					$parent_comment_data = $this->master_model->getRecords("arai_qa_forum_comments", array("comment_id" => $comment_id));					
					if(count($parent_comment_data) > 0)
					{					
						$email_info = $this->get_mail_data_qa($q_id,$user_id);
							
						//START : SEND MAIL TO QUESTION OWNER WHEN SOMEONE REPLIED ON ANSWER/COMMENT - qa_mail_to_user_on_reply_to_answer
						$email_send='';
						$slug = $encObj->encrypt('qa_mail_to_user_on_reply_to_answer');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
							
						if(count($subscriber_mail) > 0)
						{								
							$subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
							$desc = $encObj->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin = $encObj->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name = $encObj->decrypt($setting_table[0]['field_1']);
							$phoneNO = $encObj->decrypt($setting_table[0]['contact_no']);
							
							$arr_words = [
							'[QUESTIONPOSTER_NAME]',
							'[REPLIED_NAME]',
							'[QUESTION]',	
							'[LINK_OF_THE_QUESTION]'						
							]; 
							$rep_array = [
							$email_info['QUESTION_OWNER_NAME'],
							$email_info['COMMENTING_USER_NAME'],							
							$email_info['QUESTION'],
							$email_info['LINK_OF_QUESTION']								
							];
							$sub_content = str_replace($arr_words, $rep_array, $desc);					
							
							$info_array=array(
							'to' =>	$email_info['QUESTION_OWNER_EMAIL'],
							'cc' =>	'',
							'from' =>	$fromadmin,
							'subject'	=> $subject_title,
							'view' => 'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
						}						
						//END : SEND MAIL TO QUESTION OWNER WHEN SOMEONE REPLIED ON ANSWER/COMMENT - qa_mail_to_user_on_reply_to_answer
						
						//START : SEND MAIL TO COMMENTER/ANSWERED USER WHEN SOMEONE REPLIED ON ANSWER/COMMENT - qa_mail_to_answered_user_on_reply_to_answer
						$email_send='';
						$slug = $encObj->encrypt('qa_mail_to_answered_user_on_reply_to_answer');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0)
						{								
							$subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
							$desc = $encObj->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin = $encObj->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name = $encObj->decrypt($setting_table[0]['field_1']);
							$phoneNO = $encObj->decrypt($setting_table[0]['contact_no']);
							
							$comment_owner_data = $this->master_model->getRecords("arai_registration", array("user_id" => $parent_comment_data[0]['user_id']), 'title, first_name, middle_name, last_name, email');
							if(count($comment_owner_data) > 0)
							{
								$comment_owner_name = $encObj->decrypt($comment_owner_data[0]['title'])." ".$encObj->decrypt($comment_owner_data[0]['first_name'])." ".$encObj->decrypt($comment_owner_data[0]['last_name']);
								
								$arr_words = [
								'[ANSWERED_NAME]',
								'[REPLIED_NAME]',
								'[QUESTION]',						
								'[LINK_OF_THE_QUESTION]'						
								]; 
								$rep_array = [
								$comment_owner_name,
								$email_info['COMMENTING_USER_NAME'],							
								$email_info['QUESTION'],								
								$email_info['LINK_OF_QUESTION']								
								];
								$sub_content = str_replace($arr_words, $rep_array, $desc);					
								
								$info_array=array(
								'to' =>	$encObj->decrypt($comment_owner_data[0]['email']),
								'cc' =>	'',
								'from' =>	$fromadmin,
								'subject'	=> $subject_title,
								'view' => 'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}					
						}
						//END : SEND MAIL TO COMMENTER/ANSWERED USER WHEN SOMEONE REPLIED ON ANSWER/COMMENT - qa_mail_to_answered_user_on_reply_to_answer
					}
				}
				
				//THIS IS FOR COMMENT REPLY
				$comment_reply_cnt = $this->master_model->getRecordCount("arai_qa_forum_comments",array('status' => '1', 'is_block' => '0', 'parent_comment_id' => $comment_id),'comment_id');
				$result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply('.$comment_id.')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply ('.$comment_reply_cnt.')</p>
				</span>';				
				
				$select = "bc.comment_id, bc.q_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture";
				$this->db->join('arai_registration r','r.user_id = bc.user_id', 'LEFT', FALSE);
				$this->db->join('arai_profile_organization org','org.user_id = bc.user_id', 'LEFT', FALSE);
				$this->db->join('arai_student_profile sp','sp.user_id = bc.user_id', 'LEFT', FALSE);
				$comment_data = $this->master_model->getRecords("arai_qa_forum_comments bc",array('bc.comment_id' => $last_inserted_comment),$select,'',0,1);
				
				$CommentActiondata = $this->getCommentsActiondata($user_id);
				
				$response_html = '';				
				$iconImg = base_url('assets/no-img.png');
				if($comment_data[0]['user_category_id'] == 2)
				{
					if($comment_data[0]['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encObj->decrypt($comment_data[0]['org_logo'])); }
				}
				else 
				{
					if($comment_data[0]['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$comment_data[0]['profile_picture']); }
				} 
				
				$title = $encObj->decrypt($comment_data[0]['title']);
				$first_name = $encObj->decrypt($comment_data[0]['first_name']);
				$middle_name = $encObj->decrypt($comment_data[0]['middle_name']);
				$last_name = $encObj->decrypt($comment_data[0]['last_name']);
				
				$disp_name = $title." ".$first_name;
				if($middle_name != '') { $disp_name .= " ".$middle_name." "; }
				$disp_name .= $last_name;
				
				$dispHighlight = ''; 
				if($comment_data[0]['user_category_id'] == 2) { $dispHighlight = 'Organization'; } else { if($comment_data[0]['user_sub_category_id'] == 11) { $dispHighlight = 'Expert'; } }
				if($dispHighlight != '') { $dispHighlight = " <span style='font-weight: 500; font-size: 12px; color: #595959;'>(".$dispHighlight.")</span>"; }
				
				if(in_array($comment_data[0]['comment_id'], $CommentActiondata['like_comments'])) { $Like_label = "Like"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-up'; $bg_color="style='color:#1562a6; font-weight:500;'"; }
				else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-o-up'; $bg_color="style='color:#000'"; }
				
				$onclick_like_dislike_fun = "like_dislike_comment('".base64_encode($comment_data[0]['comment_id'])."','".$LikeFlag."')";
				$onclick_del_fun = "delete_comment('".base64_encode($comment_data[0]['comment_id'])."', 'reply')";
				
				$response_html .='	<div id="append_reply_div_'.$comment_id.'"></div>
				<div class="comment_block_common comment_block_common_reply" id="comment_block_'.$comment_data[0]['comment_id'].'">
				<div class="comment_img"><img src="'.$iconImg.'"></div>
				<div class="comment_inner">
				<p class="comment_name">'.$disp_name.$dispHighlight.'</p>
				<p class="comment_time">'.$this->Common_model_sm->time_Ago(strtotime($comment_data[0]['created_on']), $comment_data[0]['created_on']).'</p>
				<p class="comment_content">'.nl2br($comment_data[0]['comment']).'</p>
				<span id="like_unlike_comment_btn_outer_'.$comment_data[0]['comment_id'].'">
				<p class="comment_like_btn" onclick="'.$onclick_like_dislike_fun.'" '.$bg_color.'><i class="fa '.$likeIcon.'" aria-hidden="true"></i> '.$Like_label.' ('.$comment_data[0]['total_likes'].')</p>
				</span>';
				
				if(in_array($comment_data[0]['comment_id'], $CommentActiondata['self_comments'])) 
				{	
					$response_html .='	&nbsp;&nbsp;|&nbsp;&nbsp;
					<span id="delete_comment_btn_outer_'.$comment_data[0]['comment_id'].'">
					<p class="comment_like_btn" onclick="'.$onclick_del_fun.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
					</span>';
				}
				$response_html .='	</div><div class="clearfix"></div>						
				</div>';	
				$result['response'] = $response_html;				
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function like_unlike_comment_ajax()// LIKE - UNLIKE COMMENT
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$comment_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$flag = trim($this->security->xss_clean($this->input->post('flag')));	
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				$result['comment_id'] = $comment_id;
				
				$comment_data = $this->master_model->getRecords("arai_qa_forum_comments",array('comment_id' => $comment_id),'total_likes','',0,1);
				$new_total_like = $comment_data[0]['total_likes'];
				if($flag == 1)
				{
					$action_name = "Like Comment QA Technology Wall";
					$add_like['comment_id'] = $comment_id;
					$add_like['user_id'] = $user_id;
					$add_like['created_on'] = date('Y-m-d H:i:s');
					$this->master_model->insertRecord('arai_qa_comment_likes',$add_like);
					
					$LikeFlag = 0;
					$Like_label = 'Like';
					$likeIcon = 'fa-thumbs-up';
					$bg_color="style='color:#1562a6; font-weight:500;'";
					
					$new_total_like = $new_total_like + 1;					
				}
				else
				{
					$this->db->where('comment_id', $comment_id);
					$this->db->where('user_id', $user_id);
					$this->db->delete('arai_qa_comment_likes');
					
					$action_name = "Un-Like Comment QA Technology Wall";
					$LikeFlag = 1;
					$Like_label = 'Like';
					$likeIcon = 'fa-thumbs-o-up';
					$bg_color="style='color:#000'";
					
					$new_total_like = $new_total_like - 1;
				}
				
				$up_data['total_likes'] = $new_total_like;
				$this->master_model->updateRecord('arai_qa_forum_comments',$up_data,array('comment_id' => $comment_id));
				
				$onclick_fun = "like_dislike_comment('".base64_encode($comment_id)."','".$LikeFlag."')";
				$result['response'] = '<p class="comment_like_btn" onclick="'.$onclick_fun.'" '.$bg_color.'><i class="fa '.$likeIcon.'" aria-hidden="true"></i> '.$Like_label.' ('.$new_total_like.')'.'</p>';
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 	=> $action_name,
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function delete_comment_ajax()// DELETE COMMENT COMMENT
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$comment_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$comment_type = trim($this->security->xss_clean($this->input->post('comment_type')));	
				$user_id = $this->session->userdata('user_id');
				$result['flag'] = "success";				
				$result['comment_id'] = $comment_id;
				
				$del_data['status'] = 2;
				$del_data['deleted_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum_comments',$del_data,array('comment_id' => $comment_id));
				
				$new_comment_reply_cnt = $parent_comment_id = 0;
				if($comment_type == 'reply')
				{
					$comment_data = $this->master_model->getRecords("arai_qa_forum_comments",array('comment_id' => $comment_id),'parent_comment_id','',0,1);
					
					$new_comment_reply_cnt = $this->master_model->getRecordCount("arai_qa_forum_comments",array('status' => '1', 'is_block' => '0', 'parent_comment_id' => $comment_data[0]['parent_comment_id']),'comment_id');
					$parent_comment_id = $comment_data[0]['parent_comment_id'];
				}				
				$result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply('.$comment_data[0]['parent_comment_id'].')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply ('.$new_comment_reply_cnt.')</p>';
				$result['parent_comment_id'] = $parent_comment_id;
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Delete QA Comment Technology Wall",
				'module_name'		=> 'Question & Answer Forum Technology Wall Front',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				//$this->session->set_flashdata('success_blog_comment_del_msg','Blog Comment has been successfully deleted');		
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function getCommentsActiondata($user_id=0)
		{
			$user_like_comment = $this->master_model->getRecords("arai_qa_comment_likes",array('user_id' => $user_id),'comment_id');
			
			$like_arr = $self_comment_arr = $return_arr = array();
			if(count($user_like_comment) > 0)
			{
				foreach($user_like_comment as $res)
				{
					$like_arr[$res['comment_id']] = $res['comment_id'];
				}
			}
			
			$user_self_comment = $this->master_model->getRecords("arai_qa_forum_comments",array('user_id' => $this->session->userdata('user_id')),'comment_id');
			if(count($user_self_comment) > 0)
			{
				foreach($user_self_comment as $res)
				{
					$self_comment_arr[$res['comment_id']] = $res['comment_id'];
				}
			}
			$return_arr['like_comments'] = $like_arr;
			$return_arr['self_comments'] = $self_comment_arr;
			
			return $return_arr;
		}
		
		public function get_mail_data_qa($q_id,$commenting_user_id=false)
		{		
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('r.title, r.first_name, r.middle_name, r.last_name, r.email, qa.custum_question_id, qa.question, qa.created_on, r.user_id');
			$this->db->join('arai_registration r','r.user_id = qa.user_id','LEFT', FALSE);
			$qa_details= $this->master_model->getRecords('arai_qa_forum qa',array('qa.q_id'=>$q_id));
			
			if(count($qa_details)) 
			{				
				$user_arr = array();
				if(count($qa_details))
				{						
					foreach($qa_details as $row_val)
					{								
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$row_val['user_id'] = $row_val['user_id'];
						$user_arr[] = $row_val;
					}					
				}
				$user_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$user_id = $user_arr[0]['user_id'];
				$user_email = $user_arr[0]['email'];
				$question = $qa_details[0]['question'];
				$custum_question_id = $qa_details[0]['custum_question_id'];
				$question_date =date('d/m/Y' ,strtotime( $qa_details[0]['created_on'] ));
				$detail_link = site_url('questions_answers_forum/QaDetails/').base64_encode($q_id);
				$hyperlink_detail = '<a href='.$detail_link.' target="_blank">here</a>';
				
				$email_array=array(
				'QUESTION_OWNER_NAME'=>$user_name,
				'QUESTION_OWNER_EMAIL'=>$user_email,
				'QUESTION_OWNER_ID'=>$user_id,
				'QUESTION'=>$question,
				'QUESTION_ID_DISPLAY'=>$custum_question_id,
				'QUESTION_POSTED_DATE'=>$question_date,
				'ADMIN_EMAIL'=>'test@esds.co.in',
				'LINK_OF_QUESTION'=>$hyperlink_detail
				);
				
				if($commenting_user_id!="")
				{
					$this->db->select('title, first_name, middle_name, last_name, email');
					$commenting_user_data = $this->master_model->getRecords("registration",array('user_id'=>$commenting_user_id));
				    
					$commenting_user_arr = array();
					if(count($commenting_user_data))
					{	    					
						foreach($commenting_user_data as $row_val)
						{		    						
							$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
							$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
							$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
							$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
							$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
							$commenting_user_arr[] = $row_val;
						}    					
					}
				
					$commenting_user_name = $commenting_user_arr[0]['title']." ".$commenting_user_arr[0]['first_name']." ".$commenting_user_arr[0]['last_name'];
					$email_array['COMMENTING_USER_NAME']=$commenting_user_name;
					$email_array['COMMENTING_USER_EMAIL']=$commenting_user_arr[0]['email'];
				}
				
				return $email_array ;
			}
		}
		
		
		public function getNumData($query)// CALCULATE NUM ROWS
		{
			//echo $query;
			return $rowCount = $this->db->query($query)->num_rows();
		}
		
		public function get_client_ip() 
		{
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
	}			