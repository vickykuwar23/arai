<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Team extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->model('Common_model_sm');
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');			
			if($this->login_user_id == "") { redirect(site_url()); }
			
			//NEED TO ADD PERMISSION CODE FOR APPLY CHALLENGE
		}
		
		############# START ############################
		public function create($challenge_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$challenge_id = base64_decode($challenge_id);
			$data['default_disp_photo'] = $default_disp_photo = base_url('assets/profile_picture/user_dummy.png');
			
			//Check challenge exist or not
			$this->db->select('c.*, e.min_team, e.max_team');
			$this->db->join('arai_eligibility_expectation e', 'e.c_id = c.c_id', '', FALSE);
			$challenge_data = $this->master_model->getRecords('arai_challenge c',array("c.c_id"=>$challenge_id)); 
			if(count($challenge_data) == 0) 
			{ 
				echo 'Challenge not exist'; exit;
				$this->session->set_flashdata('error','Challenge not exist');
				redirect(site_url('challenge')); 
			}
			else 
			{
				if($challenge_data[0]['challenge_status'] != 'Approved') 
				{ 
					echo 'Challenge is not approved'; exit;
					$this->session->set_flashdata('error','Challenge is not approved');
					redirect(site_url('challenge')); 
				}
				else if($challenge_data[0]['status'] != 'Active') 
				{ 
					echo 'Challenge is not Active'; exit;
					$this->session->set_flashdata('error','Challenge is not Active');
					redirect(site_url('challenge')); 
				}
				/* else if($challenge_data[0]['is_publish'] != 'Publish') 
				{ 
					echo 'Challenge is not Publish'; exit;
					$this->session->set_flashdata('error','Challenge is not Publish');
					redirect(site_url('challenge')); 
				} */
				else if($challenge_data[0]['challenge_launch_date'] > date("Y-m-d") || $challenge_data[0]['challenge_close_date'] < date("Y-m-d")) 
				{
					echo 'Challenge launch date or close date is invalid'; exit;
					$this->session->set_flashdata('error','Challenge launch date or close date is invalid');
					redirect(site_url('challenge')); 
				}
			}
			
			$login_user_data = $this->master_model->getRecords('arai_registration',array("user_id"=>$this->login_user_id, "status"=>'Active', "admin_approval"=>'yes')); 
			if(count($login_user_data) == 0) 
			{ 
				//echo 'User not exist'; exit;
				$this->session->set_flashdata('error','User not exist');
				redirect(site_url('challenge')); 
			}
			
			if($login_user_data[0]['user_category_id'] == 2)
			{
				//echo 'Organization can not apply for challenge'; exit;
				$this->session->set_flashdata('error','Organization can not apply for challenge');
				redirect(site_url('challenge'));
			}
			
			//CHECK IF USER IS NOT CHALLENGE OWNER
			if($this->login_user_id == $challenge_data[0]['u_id'])
			{
				//echo "Challenge owner can not apply for own challenge"; exit;
				$this->session->set_flashdata('error','Challenge owner can not apply for own challenge');
				redirect(site_url('challenge/challengeDetails/'.base64_encode($challenge_id)));
			}
			
			//Check if user already applied for this challenge
			// $this->db->where("(team_status = '0' or team_status = '1')");
			$already_applied = $this->master_model->getRecordCount('arai_byt_teams',array("user_id"=>$this->login_user_id, 'c_id'=>$challenge_id,'status'=>'Active'));
			if($already_applied > 0)
			{
				//echo "You have already participated for this challenge"; exit;
				$this->session->set_flashdata('error','You have already participated for this challenge');
				redirect(site_url('challenge/challengeDetails/'.base64_encode($challenge_id)));
			}	
						
			$data['team_banner_error'] = '';
			$file_upload_flag = 0;
			
			if(isset($_POST) && count($_POST) > 0)
			{


				$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				// $this->form_validation->set_rules('team_size', 'Team Size', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				$this->form_validation->set_rules('team_details', 'Team Details', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				//$this->form_validation->set_rules('xxx', 'xxx', 'trim|required|xss_clean');
				
				if($this->form_validation->run())
				{	
					/* echo "<pre>"; print_r($challenge_data); echo "</pre>";
					echo "<pre>"; print_r($_POST); echo "</pre>";
					echo "<pre>"; print_r($_FILES); echo "</pre>"; exit; */
					
					if($_FILES['team_banner']['name'] != "")
					{
						$team_banner = $this->Common_model_sm->upload_single_file("team_banner", array('png','jpg','jpeg','gif'), "team_banner_".date("YmdHis"), "./uploads/byt", "png|jpeg|jpg|gif");
						if($team_banner['response'] == 'error')
						{
							$data['team_banner_error'] = $team_banner['message'];
							$file_upload_flag = 1;
						}
						else if($team_banner['response'] == 'success')
						{
							$add_data['team_banner'] = $team_banner['message'];	
							/* @unlink("./uploads/byt/".$encrptopenssl->decrypt($res_data[0]['team_banner'])); */
						}
					}
					
					if($file_upload_flag == 0)
					{	
						$add_data['user_id'] = $this->login_user_id;
						$add_data['c_id'] = $challenge_id;
						$add_data['team_name'] = $this->input->post('team_name');
						//$add_data['team_size'] = $team_size = $this->input->post('team_size');
						$add_data['team_size'] = $team_size = $this->input->post('byt_member_total_block');

						$add_data['brief_team_info'] = $this->input->post('team_details');
						$add_data['proposed_approach'] = $this->input->post('proposed_approach');

						$add_data['additional_information'] = $this->input->post('additional_information');
						
						//$add_data['applied_challenge_status'] = 0;
						$add_data['post_challenge_user_id'] = $challenge_data[0]['u_id'];
						//$add_data['team_status'] = 0;
						$add_data['created_on'] = date("Y-m-d H:i:s");
						$build_team_id = $this->master_model->insertRecord('byt_teams',$add_data,TRUE);
						if($build_team_id != '')
						{
							$custom_team_id='TEAM-00'.$build_team_id;
							$this->master_model->updateRecord('byt_teams',array('custom_team_id'=>$custom_team_id),array('team_id' => $build_team_id));

							$team_skillset = $this->input->post('team_skillset');

							$team_type = $this->input->post('team_type');
							
							$team_role = $this->input->post('team_role');

							$other_skill = $this->input->post('other_skill');

							for($i=0; $i < $team_size; $i++)
							{
								$add_data_skill['user_id'] = $this->login_user_id;
								$add_data_skill['c_id'] = $challenge_id;
								$add_data_skill['team_id'] = $build_team_id;
								$add_data_skill['slot_type'] = $team_type['team_type'.$i][0];
							 	$add_data_skill['skills'] = implode(",",$team_skillset['team_skillset'.$i]); 
							 	$add_data_skill['other_skill'] = $other_skill[$i];
								$add_data_skill['role_name'] = $team_role['team_role'.$i][0];
								$add_data_skill['created_on'] = date("Y-m-d H:i:s");
								$skill_id = $this->master_model->insertRecord('byt_team_slots',$add_data_skill,TRUE);

								
							}
							
							$team_files = $_FILES['team_files'];
							if(count($team_files) > 0 && $team_files['name'][0] != "")
							{
								for($i=0; $i < count($team_files['name']); $i++)
								{
									if($team_files['name'][$i] != '')
									{
										$team_file = $this->Common_model_sm->upload_single_file('team_files', array('png','jpg','jpeg','gif','pdf','xls','xlsx','doc','docx'), "team_files_".$build_team_id."_".date("YmdHis").rand(), "./uploads/byt", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx",'1',$i);
										
										if($team_file['response'] == 'error'){ }
										else if($team_file['response'] == 'success')
										{
											$add_file_data['user_id'] = $this->login_user_id;
											$add_file_data['c_id'] = $challenge_id;
											$add_file_data['team_id'] = $build_team_id;
											$add_file_data['file_name'] = ($team_file['message']);
											$add_file_data['created_on'] = date("Y-m-d H:i:s");
											$this->master_model->insertRecord('byt_team_files',$add_file_data,TRUE);
										}
									}
								}
							}

							$invite_email = $this->input->post('invite_email');
							if (count($invite_email)) {
								
								for ($i=0; $i < count($invite_email) ; $i++) { 
									
									$add_email['user_id'] = $this->login_user_id;
									$add_email['c_id'] = $challenge_id;
									$add_email['team_id'] = $build_team_id;
									$add_email['email_id'] = $invite_email[$i];
									$this->master_model->insertRecord('byt_invitation',$add_email,TRUE);
									 

								}	
							}



						}
						
						$this->session->set_flashdata('success','You have successfully build your team');
						redirect(site_url('myTeams'));
					}
				}
			}
			
			//// Display default user image for first team member
			$disp_photo = $default_disp_photo;
			$disp_skill_sets = array();
			if($login_user_data[0]['user_category_id'] == 2)//Organization
			{
				$profile_info = $this->master_model->getRecords('arai_profile_organization',array("user_id"=>$this->login_user_id), 'org_logo'); 
				if(!empty($profile_info) && isset($profile_info[0]['org_logo']) && $profile_info[0]['org_logo'] != "") 
				{ 
					$disp_photo = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profile_info[0]['org_logo']));
				}
			}
			else
			{
				$profile_info = $this->master_model->getRecords('arai_student_profile',array("user_id"=>$this->login_user_id), 'skill_sets, profile_picture');
				if(!empty($profile_info) && isset($profile_info[0]['profile_picture']) && $profile_info[0]['profile_picture'] != "") 
				{ 
					$disp_photo = base_url('assets/profile_picture/'.$profile_info[0]['profile_picture']);
				}
				
				if(!empty($profile_info) && isset($profile_info[0]['skill_sets']) && $profile_info[0]['skill_sets'] != "")
				{
					$skill_sets = $encrptopenssl->decrypt($profile_info[0]['skill_sets']); 
					if($skill_sets != "")
					{
						$explode_skillset = explode(",",$skill_sets);
						$disp_skill_sets = $explode_skillset;
					}
				}
			}
			
			$data['skill_sets_data'] = $this->master_model->array_sorting($this->master_model->getRecords('arai_skill_sets',array("status"=>'Active')), array('id'), 'name'); 
			$data['role_data'] = $this->master_model->array_sorting($this->master_model->getRecords('arai_byt_role_master',array("status"=>'Active')), array('id'), 'name'); 
			$data['challenge_id'] = $challenge_id;
			$data['challenge_data'] = $challenge_data;
			$data['login_user_data'] = $login_user_data;
			$data['min_team'] = $encrptopenssl->decrypt($challenge_data[0]['min_team']);
			$data['max_team'] = $encrptopenssl->decrypt($challenge_data[0]['max_team']);
			$data['disp_photo'] = $disp_photo;
			$data['disp_skill_sets'] = $disp_skill_sets;
			$data['page_title'] = 'Apply For Challenge';
			$data['type_data'] = $this->master_model->getRecords('arai_type_master',array( "status"=>'Active')); 
			$data['middle_content'] = 'byt/create_team';
			$this->load->view('front/front_combo',$data);
		}
		############# END ############################		
		
		public function sorting()
		{
			$role_data = $this->master_model->getRecords('arai_byt_role_master',array("status"=>'Active'),'id, name, status');
			$domain_data = $this->master_model->getRecords("arai_domain_master",array('status'=>'Active'));
			$result_arr = $this->array_sorting($domain_data, array('id'), 'domain_name');
			
			echo "<pre>"; print_r($domain_data); echo "</pre>";
			echo "<pre>"; print_r($result_arr); echo "</pre>";			
		}
		
		public function array_sorting($res_arr=array(), $input_arr=array(), $sorting_key)
		{
			$new_sort_arr = array();
			if(count($res_arr) > 0 && count($input_arr) > 0 && $sorting_key != '')
			{
				$encrptopenssl =  New Opensslencryptdecrypt();
				$new_arr = $final_arr = $other_arr = array();
				$other_flag = 0;
				
				foreach($res_arr as $res)
				{
					$enc_val = trim($encrptopenssl->decrypt($res[$sorting_key]));
					
					if(strtolower($enc_val) != 'other')
					{					
						$new_arr[$enc_val][$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$new_arr[$enc_val][$input] = $res[$input];
						}
					}
					else
					{
						$other_flag = 1;						
						$other_arr[$sorting_key] = $enc_val;
						foreach($input_arr as $input)
						{
							$other_arr[$input] = $res[$input];
						}
						$other_arr = $other_arr;
					}
					$final_arr = $new_arr;
				}
				
				//echo "<pre>"; print_r($final_arr); echo "</pre>"; exit;
				ksort($final_arr);
				$new_sort_arr = array();
				foreach($final_arr as $new_res)
				{
					$new_sort_arr[] = $new_res;
				}
				
				if($other_flag == 1)
				{
					$new_sort_arr[] = $other_arr;
				}
			}
			
			return $new_sort_arr;
		}
		
		public function base64_en($id)
		{
			echo base64_encode($id);
		}
		
		public function base64_de($id)
		{
			echo base64_decode($id);
		}
}				