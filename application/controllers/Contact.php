<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct() {
        parent::__construct();
		
    }
	public function index()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		if (isset($_POST['submit'])) {
		
			$this->form_validation->set_rules('fullname', 'Name', 'trim|required|max_length[100]|xss_clean');
			$this->form_validation->set_rules('mobile_no', 'Mobile number.', 'trim|required|xss_clean|min_length[9]|max_length[10]|numeric');
			$this->form_validation->set_rules('email_id', 'Email', 'trim|xss_clean|valid_email|required');
			$this->form_validation->set_rules('subject_details', 'Message', 'trim|xss_clean');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');

			if($this->form_validation->run())
			{	
				$insertArr=array(
				'fullname' => $encrptopenssl->encrypt($this->input->post('fullname')),
				'mobile_no' => $encrptopenssl->encrypt($this->input->post('mobile_no')),
				'email_id' => $encrptopenssl->encrypt($this->input->post('email_id')),
				'subject_details' => $encrptopenssl->encrypt($this->input->post('subject_details')),
				);
						
				$insert_id = $this->master_model->insertRecord('contactus',$insertArr);
				if($insert_id > 0){

					//email  sending code 
					$this->session->set_flashdata('success_message','Thanks for contacting us.');
				    	redirect(base_url().'contact');
				
				} else {
					$this->session->set_flashdata('error_message','Something went wrong! Please try again.');
					redirect(base_url('contact'));
				}
			}

			
		}

		$data['image'] = $this->create_captcha();
		$data['page_title']='Contact Us';
		$data['middle_content']='contact';
		$this->load->view('front/front_combo',$data);
	}

	public function create_captcha($value='')
	{
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Send captcha image to view
		return $captcha['image'];
	}


	

	public function check_captcha($code) 
	{

				if($code == '' || $_SESSION["mycaptcha"] != $code    )
				{
					$this->session->unset_userdata("mycaptcha");
					$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
					return false;
				}
				if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
				{
					$this->session->unset_userdata("mycaptcha");
					return true;
				}
		
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
      		'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
		
	
}
