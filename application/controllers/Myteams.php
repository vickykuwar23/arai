<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Myteams extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->model('Common_model_sm');
			$this->load->helper('security');
			$this->load->helper('text');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');			
			if($this->login_user_id == "") { redirect(site_url()); }

			// ini_set('display_errors', '1');
			// ini_set('display_startup_errors', '1');
			// error_reporting(E_ALL);
			
			//NEED TO ADD PERMISSION CODE FOR VIEW TEAMS
		}
		
		############# START ############################
		public function index()
		{
			redirect(site_url('myteams/myCreatedteams'));
			$login_user_data = $this->master_model->getRecords('arai_registration',array("user_id"=>$this->login_user_id, "status"=>'Active', "admin_approval"=>'yes')); 
			if(count($login_user_data) == 0) 
			{ 
				echo 'User not exist'; exit;
				$this->session->set_flashdata('error','User not exist');
				redirect(site_url('challenge')); 
			}
			
			/* if($login_user_data[0]['user_category_id'] == 2)
				{
				echo 'Organization dont have my teams'; exit;
				$this->session->set_flashdata('error','Organization dont have my teams');
				redirect(site_url('challenge'));
			} */
			
			$data['page_title'] = 'Challenge - My Teams';
			$data['middle_content'] = 'byt/my_teams';
			$this->load->view('front/front_combo',$data);
		}
		############# END ############################		
		
		public function get_my_team_list_ajax()
		{
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			//echo "<pre>";print_r($this->input->post());
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;
			
			$condition = "SELECT c.team_id, c.c_id, c.team_name, c.team_size, c.team_status, ac.challenge_title
			FROM arai_byt_teams c
			LEFT JOIN arai_challenge ac ON ac.c_id = c.c_id
			WHERE c.user_id = '".$this->login_user_id."'";
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			//echo $query;die();
			$result = $this->db->query($query)->result();
			$rowCount = $this->db->query($condition)->num_rows();	
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0)
			{				
				$no = $_POST['start'];
				$i = 0;
				foreach($result as $res)
				{					
					$build_team_id = $res->team_id;		
					$challenge_title 	= $encrptopenssl->decrypt($res->challenge_title);
					$team_name = $res->team_name;		
					$team_size = $res->team_size;		
					$team_status = $res->team_status;	
					$v_link =   base_url('myteams/team_details_owner/').base64_encode($res->team_id);
					$view = '<a href="'.$v_link.'" class="btn btn-success btn-green-fresh"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					// if($team_status == 0) { $disp_team_status = 'Pending'; }
					// else if($team_status == 1) { $disp_team_status = 'Approved'; }
					// else if($team_status == 2) { $disp_team_status = 'Cancelled'; }
					// else if($team_status == 3) { $disp_team_status = 'Completed'; }
					
					/* $edit = '<a href="'.$e_link.'" class="btn btn-info btn-green-fresh"><i class="fa fa-pencil" aria-hidden="true"></i></a>'; 
						$view = '<a href="'.$v_link.'" class="btn btn-success btn-green-fresh"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					$conCat = $edit."&nbsp;".$view."&nbsp;".$withdraw; */
					$conCat = '';
					
					$i++;
					$dataArr[] = array(
					$build_team_id,
					$challenge_title,
					$team_name,
					$team_size,						  
					$team_status,
					$view,
					$conCat
					);
					
					$rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);					
				}
			} 
			else 
			{				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
			}			
			echo json_encode($response);	
		}
		
		
		public function team_details_owner($team_id='')
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if ($team_id=='') {
				redirect(base_url('myteams'));
			}	
			
			$data['team_id'] = $team_id=base64_decode($team_id);
			
			$this->db->select('bt.c_id, bt.team_id, bt.share_files, bt.user_id, bt.team_id, bt.apply_status, bt.team_status, bt.challenge_owner_status, bt.share_files, bt.team_name, bt.team_size, bt.team_banner, bt.brief_team_info, bt.proposed_approach, bt.additional_information, bt.custom_team_id, c.challenge_id, c.challenge_title, c.challenge_status');
			$this->db->join('arai_challenge c','c.c_id=bt.c_id','INNER', FALSE);
			$team_details= $this->master_model->getRecords('arai_byt_teams bt', array('bt.team_id'=>$team_id, 'bt.is_deleted'=>'0'));
			
			if (count($team_details) ==0) { redirect(base_url('myteams')); }
			
			$user_id = $this->session->userdata('user_id');
			
			$owner_id = $team_details[0]['user_id'];
			
			if ($user_id !=  $owner_id) {
				redirect(base_url('myteams/myCreatedteams'));	
			}
			
			$this->db->select('byt_team_slots.slot_id,byt_team_slots.skills,byt_team_slots.user_id,c_id,team_id,role_name,type_master.name');
			
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			// $this->db->join('registration','registration.user_id=byt_team_slots.user_id');
			$this->db->order_by("byt_team_slots.slot_id","ASC");
			$team_slots = $this->master_model->getRecords('byt_team_slots',array('team_id'=>$team_id,'is_deleted'=>'0'));
			//echo $this->db->last_query(); exit;
			
			
			
			$data['owner_info'] = $this->get_user_info($user_id);
			$data['team_details']=$team_details;
			$data['team_files_public']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0', 'file_type'=>'public'), 'file_name, file_type');
			$data['team_files_private']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0', 'file_type'=>'private'), 'file_name, file_type');
			$data['team_slots']=$team_slots;
			$data['loggedin_user_id']=$user_id;
			$data['member_remove_reason_data'] = $this->master_model->array_sorting($this->master_model->getRecords('team_member_withdrawal',array('status'=>'Active')),array('id'),'reason');
			
			$data['page_title'] = 'Team Details';
			$data['middle_content'] = 'byt/team_owner_view';
			$this->load->view('front/front_combo',$data);
			
		}

		public function task_details_owner($team_id='')
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if ($team_id=='') {
				redirect(base_url('myteams'));
			}	
			
			$data['team_id'] = $team_id=base64_decode($team_id);
			
			$this->db->select('bt.team_id,bt.task_start_date,bt.task_end_date, bt.share_files, bt.user_id, bt.team_id, bt.apply_status, bt.team_status, bt.challenge_owner_status, bt.share_files, bt.team_name, bt.team_size, bt.team_banner, bt.brief_team_info, bt.proposed_approach, bt.additional_information, bt.custom_team_id,bt.task_name, bt.team_type ,bt.custom_task_id');
			
			$team_details= $this->master_model->getRecords('arai_byt_teams bt', array('bt.team_id'=>$team_id, 'bt.is_deleted'=>'0'));


			if (count($team_details) ==0) { redirect(base_url('myteams')); }
			
			$user_id = $this->session->userdata('user_id');
			
			$owner_id = $team_details[0]['user_id'];
			
			if ($user_id !=  $owner_id) {
				redirect(base_url('myteams/myCreatedteams'));	
			}
			
			$this->db->select('byt_team_slots.slot_id,byt_team_slots.skills,byt_team_slots.user_id,c_id,team_id,role_name,type_master.name');
			
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			// $this->db->join('registration','registration.user_id=byt_team_slots.user_id');
			$this->db->order_by("byt_team_slots.slot_id","ASC");
			$team_slots = $this->master_model->getRecords('byt_team_slots',array('team_id'=>$team_id,'is_deleted'=>'0'));
			//echo $this->db->last_query(); exit;
			
			
			
			$data['owner_info'] = $this->get_user_info($user_id);
			$data['team_details']=$team_details;
			$data['team_files_public']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0', 'file_type'=>'public'), 'file_name, file_type');
			$data['team_files_private']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0', 'file_type'=>'private'), 'file_name, file_type');
			$data['team_slots']=$team_slots;
			$data['loggedin_user_id']=$user_id;
			$data['member_remove_reason_data'] = $this->master_model->array_sorting($this->master_model->getRecords('team_member_withdrawal',array('status'=>'Active')),array('id'),'reason');
			
			$data['page_title'] = 'Team Details';
			$data['middle_content'] = 'tasks/team_owner_view';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function team_details_member($team_id='')
		{
			if ($team_id=='') {
				redirect(base_url('myteams'));
			}	
			
			$data['team_id'] = $team_id = base64_decode($team_id);
			
			$this->db->select('c.c_id, c.u_id, bt.share_files, bt.user_id, bt.team_id, bt.apply_status, bt.team_status, bt.share_files, bt.team_name, bt.team_size, bt.team_banner, bt.brief_team_info, bt.proposed_approach, bt.additional_information, bt.custom_team_id, bt.challenge_owner_status, c.challenge_id, c.challenge_title');
			$this->db->join('arai_challenge c','c.c_id=bt.c_id', 'INNER', FALSE);
			$team_details= $this->master_model->getRecords('arai_byt_teams bt',array('bt.team_id'=>$team_id,'bt.is_deleted'=>'0'));
			if (count($team_details) ==0) {
				redirect(base_url('myteams'));
			}
			
			
			
			$user_id = $this->session->userdata('user_id');
			
			
			$this->db->select('byt_team_slots.slot_id,byt_team_slots.skills,byt_team_slots.user_id,c_id,team_id,role_name,type_master.name');
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			// $this->db->join('registration','registration.user_id=byt_team_slots.user_id');
			$this->db->order_by("byt_team_slots.slot_id","ASC");
			$team_slots = $this->master_model->getRecords('byt_team_slots',array('team_id'=>$team_id,'is_deleted'=>'0'));
			
			
			
			$data['owner_info'] = $this->get_user_info($user_id);
			$data['team_details']=$team_details;
			$data['team_files']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0'), 'file_name, file_type');
			$data['team_slots']=$team_slots;
			$data['loggedin_user_id']=$user_id;
			
			
			
			$data['page_title'] = 'Team Details';
			$data['middle_content'] = 'byt/team_detail';
			$this->load->view('front/front_combo',$data);
			
		}

		public function task_details_member($team_id='')
		{
			if ($team_id=='') {
				redirect(base_url('myteams'));
			}	
			
			$data['team_id'] = $team_id = base64_decode($team_id);
			
			$this->db->select('bt.team_id, bt.share_files, bt.user_id, bt.team_id, bt.apply_status, bt.team_status, bt.challenge_owner_status, bt.share_files, bt.team_name, bt.team_size, bt.team_banner, bt.brief_team_info, bt.proposed_approach, bt.additional_information, bt.custom_team_id,bt.task_name, bt.team_type ,bt.custom_task_id');
			
			$team_details= $this->master_model->getRecords('arai_byt_teams bt',array('bt.team_id'=>$team_id,'bt.is_deleted'=>'0'));
			if (count($team_details) ==0) {
				redirect(base_url('myteams'));
			}
			
			
			
			$user_id = $this->session->userdata('user_id');
			
			
			$this->db->select('byt_team_slots.slot_id,byt_team_slots.skills,byt_team_slots.user_id,c_id,team_id,role_name,type_master.name');
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			// $this->db->join('registration','registration.user_id=byt_team_slots.user_id');
			$this->db->order_by("byt_team_slots.slot_id","ASC");
			$team_slots = $this->master_model->getRecords('byt_team_slots',array('team_id'=>$team_id,'is_deleted'=>'0'));
			
			
			
			$data['owner_info'] = $this->get_user_info($user_id);
			$data['team_details']=$team_details;
			$data['team_files']=$this->master_model->getRecords('byt_team_files',array('team_id'=>$team_id,'is_deleted'=>'0'), 'file_name, file_type');
			$data['team_slots']=$team_slots;
			$data['loggedin_user_id']=$user_id;
			
			
			
			$data['page_title'] = 'Team Details';
			$data['middle_content'] = 'tasks/team_detail';
			$this->load->view('front/front_combo',$data);
			
		}
		
		
		
		
		
		public function get_user_info($user_id='')
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('user_category,user_sub_category_id,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
			$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
			$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
			$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
			// print_r($user_data);
			
			$user_arr = array();
			if(count($user_data)){	
				
				foreach($user_data as $row_val){		
					
					$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
					$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
					$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
					$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
					$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
					$row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
					$row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
					$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
					$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
					$user_arr[] = $row_val;
				}
				
			}
			
			return $user_arr;
			
			
		}
		
		public function share_files()
		{
			$share_files=$_POST['share_files'];
			$team_id=$_POST['team_id'];
			
			if ($team_id!='') {
				
				$query=$this->master_model->updateRecord('byt_teams',array('share_files'=>$share_files),array('team_id' => $team_id));
				if ($query) {
					echo true;
					
					}else{
					echo false;
				}
				
				}else{
				echo false;
			}
			
			
		}
		
		public function challenge_owner_approval()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$appr=$_POST['appr'];
			$team_id=$_POST['team_id'];
			$reject_reason=$_POST['reject_reason']; 
			
			$c_link = base_url('challenge' );
			$challenge_listing_link = '<a href='.$c_link.' target="_blank">here</a>';
			
			$t_link = base_url('myteams/myCreatedteams' );
			$team_listing_link = '<a href='.$t_link.' target="_blank">here</a>';
			
			
			
			if ($team_id!='' && $appr!='') {
				
				if ($appr=='accept') {
					$update_arr = array('challenge_owner_status'=>'Approved','reject_reason'=>$reject_reason);
					
					//email to team owner on acceptance
					
					$email_info = $this->get_team_info_for_mail($team_id);
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_team_owner_on_participation_accepted');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					
					if(count($subscriber_mail) > 0){			
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[team_owner_name]',
						'[Team_Name]',
						'[Team_ID]',
						'[challenge_Name_with_hyperlink]',
						'[Challenge_ID]',
						'[hyperlink_to_challenge_listing_page]',
						
						]; 
						$rep_array = [
						$email_info['team_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						$challenge_listing_link,
						
						
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						
						$info_array=array(
						'to'		=>	$email_info['team_owner_email'],				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					}
					
					// end email acceptance	
					
					//start email to challenge owner accept
					$email_info = $this->get_team_info_for_mail($team_id);
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_participation_accepted');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					
					if(count($subscriber_mail) > 0){			
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[chl_owner_name]',
						'[Team_Name]',
						'[Team_ID]',
						'[challenge_Name_with_hyperlink]',
						'[Challenge_ID]',
						'[hyperlink_to_challenge_listing_page]',
						
						]; 
						$rep_array = [
						$email_info['chl_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						$challenge_listing_link,
						
						
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						
						$info_array=array(
						'to'		=>	$email_info['chal_owner_email'],				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					}
					
					//end email to challenge owner accept
					
					
					}elseif($appr=='reject'){
					$update_arr = array('challenge_owner_status'=>'Rejected','reject_reason'=>$reject_reason);
					
					$email_info = $this->get_team_info_for_mail($team_id);
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_team_owner_on_participation_rejected');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					
					if(count($subscriber_mail) > 0){			
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						
						
						$arr_words = [
						'[team_owner_name]',
						'[Team_Name]',
						'[Team_ID]',
						'[challenge_Name_with_hyperlink]',
						'[Challenge_ID]',
						'[hyperlink_to_my_teams_page]',
						
						]; 
						$rep_array = [
						$email_info['team_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						$team_listing_link,
						
						
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						
						$info_array=array(
						'to'		=>	$email_info['team_owner_email'],				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
					}
					
					
					//start email to challenge owner reject
					$email_info = $this->get_team_info_for_mail($team_id);
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_participation_rejected');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					
					if(count($subscriber_mail) > 0){			
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[chl_owner_name]',
						'[Team_Name]',
						'[Team_ID]',
						'[challenge_Name_with_hyperlink]',
						'[Challenge_ID]',
						
						]; 
						$rep_array = [
						$email_info['chl_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						
						
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						
						$info_array=array(
						'to'		=>	$email_info['chal_owner_email'],				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					}
					
					//end email to challenge owner reject
					
					}else{
					$update_arr = array('challenge_owner_status'=>'Pending','reject_reason'=>$reject_reason);
				}
				
				$query=$this->master_model->updateRecord('byt_teams',$update_arr,array('team_id' => $team_id));
				
				if ($query) {
					echo 'success';
					
					}else{
					echo 'error';
				}
				
				}else{
				echo 'error';
			}
			
			
		}
		
		public function is_team_complete()
		{
			$team_complete=$_POST['team_complete'];
			$team_id=$_POST['team_id'];
			
			if ($team_id!='') {
				
				$query=$this->master_model->updateRecord('byt_teams',array('team_status'=>$team_complete),array('team_id' => $team_id));
				if ($query) {
					echo true;
					
					}else{
					echo false;
				}
				
				}else{
				echo false;
			}
			
			
		}
		
		
		
		public function apply_for_challenge(){
			
			$team_id=$_POST['team_id'];
			$encrptopenssl =  New Opensslencryptdecrypt();
			$email_info=$this->get_team_info_for_mail($team_id);
			
			if ($team_id!='') 
			{				
				//$teamdata = $this->master_model->getRecords("arai_byt_teams", array("team_id" => $team_id, "apply_status"=>"Pending", "is_deleted"=>'0'));

				$teamdata = $this->master_model->getRecords("arai_byt_teams", array("team_id" => $team_id, "is_deleted"=>'0'));
				$last_query = $this->db->last_query();
				if(count($teamdata) > 0)
				{
					$query=$this->master_model->updateRecord('byt_teams',array('apply_status'=>'Applied'),array('team_id' => $team_id));
					if ($query) 
					{					
						$email_send='';
						$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_apply_for_team');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0){			
							
							$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
							$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
							
							
							$arr_words_sub = ['[Team_name]','[Challenge_name]'];
							$rep_array_sub = [$email_info['team_name'],$email_info['challenge_title'] ];
							$subject_title_new = str_replace($arr_words_sub, $rep_array_sub, $subject_title);
							
							$arr_words = ['[name]','[Team_Name]','[Team_ID]','[challenge_Name_with_hyperlink]','[Challenge_ID]','[hyperlink_to_challenge_page]']; 
							$rep_array = [
							$email_info['chl_owner_name'],
							$email_info['team_name'],
							$email_info['team_id'],
							$email_info['challenge_link'],
							$email_info['challenge_id'],
							$email_info['challengepage_link']
							];
							
							
							
							$sub_content = str_replace($arr_words, $rep_array, $desc);
							
							$info_array=array(
							'to'		=>	$email_info['chal_owner_email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title_new,
							'view'		=>  'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						}	
						
						$res = ['success'=>true];
						echo  json_encode($res) ;
						exit();
						
						}else{
						$res = ['success'=>false, 'response'=>'three'];
						echo  json_encode($res) ;
						exit();
					}
				}
				else
				{
					$res = ['success'=>false, 'response'=>'two'];
					echo  json_encode($res) ;
					exit();
				}	
			}
			else
			{
				$res = ['success'=>false, 'response'=>'one'];
				echo  json_encode($res) ;
				exit();
			}		
		}


	public function disolve_team(){
			
			$team_id= base64_decode($_POST['team_id']);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			if ($team_id!='') 
			{				
				$teamdata = $this->master_model->getRecords("arai_byt_teams", array("team_id" => $team_id, "is_deleted"=>'0'));
				if(count($teamdata) > 0)
				{	
					$email_info=$this->get_team_info_for_mail($team_id);
					
					$team_type = $teamdata[0]['team_type'];
					$query=$this->master_model->updateRecord('byt_teams',array('is_deleted'=>'1'),array('team_id' => $team_id));
					if ($query) 
					{	
						if ($team_type=='task' || $team_type=='challenge') {
							
						$t_link = base_url('myteams/myCreatedteams' );
						$team_listing_link = '<a href='.$t_link.' target="_blank">here</a>';	
						
						//START MAIL TO TEAM ADMIN
						$email_send='';
						$slug = $encrptopenssl->encrypt('byt_mail_to_admin_on_dissolve_task_team');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0){			
							
							$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
							$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
							
							
							
							$arr_words = ['[team_owner_name]',
							'[Team_Name]',
							'[Team_ID]',
							'[hyperlink_to_my_teams_page]']; 
							$rep_array = [
							$email_info['team_owner_name'],
							$email_info['team_name'],
							$email_info['team_id'],
							$team_listing_link
							
							];
							$sub_content = str_replace($arr_words, $rep_array, $desc);
							
							$info_array=array(
							'to'		=>	'Jitendra.Battise@esds.co.in',				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}
						 }


					// END SEND MAIL TO ADIMN

					//START MAIL TO MEMBERS	
					$this->db->where('sa.apply_user_id !=',$teamdata[0]['user_id']);
					$where = '(sa.status="Approved" or sa.status = "Pending")';
       				$this->db->where($where);
					$this->db->select("sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id, sa.status, r.email,r.first_name,r.last_name,r.title");
					$this->db->join("arai_registration r", "r.user_id = sa.apply_user_id", "INNER", FALSE);
					$remainingSlotMember = $this->master_model->getRecords('arai_byt_slot_applications sa', array("sa.team_id"=>$team_id, "sa.is_deleted"=>'0'));


					$my_teams_link = site_url('myteams/appliedTeams' );
					$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';

					if(count($remainingSlotMember) > 0)
					{
						if ($team_type=='challenge') 
						{
							//START : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED
							$c_link = site_url('challenge');	
							$challenge_listing_link = '<a href='.$c_link.' target="_blank">here</a>';
							
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_mail_to_members_on_dissolve_task_team');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							if(count($subscriber_mail) > 0)
							{												
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[Team_Name]',
								'[Team_ID]',
								'[Hyperlink_to_the_Team_I_have_applied_to_Page]'
																	
								]; 
								
								$rep_array = [

								$email_info['team_name'],
								$email_info['team_id'],
								$my_applied_teams_link
								];
								$sub_content = str_replace($arr_words, $rep_array, $desc);									
							}
								//END : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED
				    	}
				    	
				    	if($team_type=='task'){
						   
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_mail_to_members_on_dissolve_task_team');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							if(count($subscriber_mail) > 0)
							{												
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[Team_Name]',
								'[Team_ID]',
								'[Hyperlink_to_the_Team_I_have_applied_to_Page]'
																	
								]; 
								
								$rep_array = [

								$email_info['team_name'],
								$email_info['team_id'],
								$my_applied_teams_link
								];
								$sub_content = str_replace($arr_words, $rep_array, $desc);									
							}	
							     
							    //END : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED	TASKS
					    }
								
								
						foreach($remainingSlotMember as $res)
						{
							$full_name= $encrptopenssl->decrypt($res['title'])." ".$encrptopenssl->decrypt($res['first_name'])." ".$encrptopenssl->decrypt($res['last_name']);
							$sub_content = str_replace("[Team_Member_Full_Name]",$full_name , $sub_content);	
							$applicant_mail = $encrptopenssl->decrypt($res['email']);
							$info_array=array(
							// 'to' =>	$applicant_mail,	
							'to' =>	$applicant_mail,				
							'cc' =>	'',
							'from' =>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view' => 'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						}
					}

					//END MAIL TO MEMBERS


					//START : MAIL TO TEAM OWNER
					if ($team_type=='task' ||  $team_type=='challenge') {
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_mail_to_owner_on_dissolve_task_team');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							if(count($subscriber_mail) > 0){			
								
								$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = ['[team_owner_name]','[Team_Name]','[Team_ID]','[hyperlink_to_my_teams_page]']; 
								$rep_array = [
								$email_info['team_owner_name'],
								$email_info['team_name'],
								$email_info['team_id'],
								$team_listing_link
								
								];
								
								$sub_content = str_replace($arr_words, $rep_array, $desc);
								
								$info_array=array(
								'to'		=>	$email_info['team_owner_email'],				
								'cc'		=>	'',
								'from'		=>	$fromadmin,
								'subject'	=> 	$subject_title,
								'view'		=>  'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
								}
						}	
					//END: MAIL TO TEAM OWNER

					//START MAIL TO CHALLENGE OWNER
					/*$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_apply_for_team');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					
					if(count($subscriber_mail) > 0){			
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words_sub = ['[Team_name]','[Challenge_name]'];
						$rep_array_sub = [$email_info['team_name'],$email_info['challenge_title'] ];
						$subject_title_new = str_replace($arr_words_sub, $rep_array_sub, $subject_title);
						
						$arr_words = ['[name]','[Team_Name]','[Team_ID]','[challenge_Name_with_hyperlink]','[Challenge_ID]','[hyperlink_to_challenge_page]']; 
						$rep_array = [
						$email_info['chl_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						$email_info['challengepage_link']
						];
						
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						$info_array=array(
						'to'		=>	'vishal.phadol@esds.co.in',				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title_new,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
					}*/
					// END MAIL TO CHALLENGE OWNER	

					
					$res = ['success'=>true];
					echo  json_encode($res) ;
					exit();
					
					}else{
					$res = ['success'=>false, 'response'=>'three'];
					echo  json_encode($res) ;
					exit();
					}
				}
				else
				{
					$res = ['success'=>false, 'response'=>'two'];
					echo  json_encode($res) ;
					exit();
				}	
			}
			else
			{
				$res = ['success'=>false, 'response'=>'one'];
				echo  json_encode($res) ;
				exit();
			}		
		}
		
		public function get_team_info_for_mail($team_id){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('challenge.c_id,challenge.u_id,byt_teams.share_files,byt_teams.user_id,byt_teams.team_id,apply_status,byt_teams.team_status,share_files,team_name,team_size,team_banner,brief_team_info,proposed_approach,additional_information,custom_team_id,challenge_id,challenge_title');
			$this->db->join('challenge','challenge.c_id=byt_teams.c_id','LEFT');
			$team_details= $this->master_model->getRecords('byt_teams',array('team_id'=>$team_id));
			
			if (count($team_details)) {
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$user_data = $this->master_model->getRecords("registration",array('user_id'=>$team_details[0]['u_id']));
				
				
				$user_id = $team_details[0]['user_id'];;
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$team_owner_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
				
				
				
				$user_arr = array();
				if(count($user_data)){	
					
					foreach($user_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$user_arr[] = $row_val;
					}
					
				}
				$name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				
				$owner_arr = array();
				if(count($team_owner_data)){	
					
					foreach($team_owner_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$owner_arr[] = $row_val;
					}
					
				}
				
				
				$name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				
				$team_owner_name = $owner_arr[0]['title']." ".$owner_arr[0]['first_name']." ".$owner_arr[0]['last_name'];
				
				
				$c_name= $encrptopenssl->decrypt($team_details[0]['challenge_title']);
				
				$c_link = base_url('challenge/challengeDetails/').base64_encode( $team_details[0]['c_id'] );
				$challenge_link = '<a href='.$c_link.' target="_blank">'.$c_name.'</a>';
				
				$my_ch_link = base_url('challenge/myChallenges');
				$challengepage_link = '<a href='.$my_ch_link.' target="_blank">here</a>';
				
				$t_link = base_url('myteams/team_details_owner/').base64_encode( $team_details[0]['team_id'] );
				$owner_page_link = '<a href='.$t_link.' target="_blank">here</a>';
				
				$email_array=array(
				'team_name'=>$team_details[0]['team_name'],
				'challenge_title'=>$c_name,
				'chl_owner_name'=>$name,
				'team_owner_name'=>$team_owner_name,
				'team_owner_email'=>$owner_arr[0]['email'],
				'chal_owner_email'=>$user_arr[0]['email'],
				'team_id'=>$team_details[0]['custom_team_id'],
				'challenge_id'=>$team_details[0]['challenge_id'],
				'challenge_link'=>$challenge_link,
				'challengepage_link'=>$challengepage_link,
				'hyperlink_to_owner_view'=>$owner_page_link,
				);
				
				return $email_array;
			}
			
			
		}
		
		public function withdraw_for_challenge()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();			
			$team_id=$_POST['team_id'];
			if ($team_id!='') 
			{				
				$query=$this->master_model->updateRecord('byt_teams',array('apply_status'=>'Withdrawn'),array('team_id' => $team_id));
				if($query) 
				{
					//START : SEND MAIL TO CHALLENGE OWNER WHEN TEAM OWNER WITHDRAW TEAM
					$my_ch_link = site_url('challenge/myChallenges' );	
					$hyperlink_my_challenges_page = '<a href='.$my_ch_link.' target="_blank">here</a>';
					$email_info = $this->get_team_info_for_mail($team_id);
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_team_withdrawn');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
	
					if(count($subscriber_mail) > 0)
					{								
						$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$arr_words = [
						'[chl_owner_name]',
						'[Team_Name]',
						'[Team_ID]',
						'[challenge_Name_with_hyperlink]',
						'[Challenge_ID]',
						'[hyperlink_my_challenges_page]',						
						]; 
						$rep_array = [
						$email_info['chl_owner_name'],
						$email_info['team_name'],
						$email_info['team_id'],
						$email_info['challenge_link'],
						$email_info['challenge_id'],
						$hyperlink_my_challenges_page,				
						];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						
						
						$info_array=array(
						'to'		=>	$email_info['chal_owner_email'],				
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
						
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
					}
					//END : SEND MAIL TO CHALLENGE OWNER WHEN TEAM OWNER WITHDRAW TEAM
		
					$res = ['success'=>true];
					echo  json_encode($res) ;
					exit();					
				}
				else
				{
					$res = ['success'=>false];
					echo  json_encode($res) ;
					exit();
				}				
			}
			else
			{
				$res = ['success'=>false];
				echo  json_encode($res) ;
				exit();
			}		
		}
		
		// BYT Application Listing 
		public function applicationListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			if(count($challengeDetails)){
				
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/application-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Application Listing 
		public function application_listing(){
			
			error_reporting(0);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status, 
			arai_registration.institution_full_name, arai_registration.user_category_id, arai_registration.title, arai_registration.first_name,arai_registration.middle_name, arai_registration.last_name FROM arai_byt_teams
			JOIN arai_registration ON arai_byt_teams.user_id=arai_registration.user_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."'  ".$c_data."";			
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$title					= $encrptopenssl->decrypt($get_request->title);
					$first_name				= $encrptopenssl->decrypt($get_request->first_name);
					$middle_name			= $encrptopenssl->decrypt($get_request->middle_name);
					$last_name				= $encrptopenssl->decrypt($get_request->last_name);
					$fullnames				= ucwords($title)." ".ucwords($first_name)." ".ucwords($middle_name)." ".ucwords($last_name);
					$categoryID				= $get_request->user_category_id;
					$cmp_name				= $encrptopenssl->decrypt($get_request->institution_full_name);
					
					$getCatSubCat = $this->master_model->getRecords("registration_usercategory", array('id' => $categoryID));
					$categoryName = $encrptopenssl->decrypt($getCatSubCat[0]['user_category']);
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('myteams/show_team_profile/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name		= $team_id." - ".$team_name;	
					
					$conCat				= $view."&nbsp;".$withdraw;
					
					/*if($categoryID == 1){
						$getCompDetails = $this->master_model->getRecords("student_profile", array('user_id' => $creator_user_id));
						$companyName = ucwords($encrptopenssl->decrypt($getCompDetails[0]['company_name']));
					}*/
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$i,
					$categoryName,
					$companyName,
					$teamid_name,	
					$fullnames,
					$team_status,
					$team_size,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		} // Team Listing ENd
		
		// BYT My Teams Listing 
		public function myCreatedteams(){
			
			error_reporting(0);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			
			$user_id = $this->session->userdata('user_id');	
			
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/my-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// My Created Team Listing
		public function myTeamsListing(){
			
			error_reporting(0);
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			// Login User
			$user_id = $this->session->userdata('user_id');	
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.custom_team_id,arai_byt_teams.team_type,arai_byt_teams.task_name,custom_task_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,arai_byt_teams.apply_status,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.skills, arai_byt_team_slots.role_name, arai_byt_team_slots.slot_id 
			FROM arai_byt_teams 
			JOIN arai_byt_team_slots ON arai_byt_teams.c_id=arai_byt_team_slots.c_id 
			JOIN arai_registration r ON r.user_id = arai_byt_teams.user_id
			WHERE arai_byt_teams.is_deleted = '0' AND r.is_deleted = '0' AND arai_byt_teams.user_id = '".$user_id."' GROUP BY arai_byt_teams.team_id ".$c_data." ORDER BY arai_byt_teams.team_id DESC";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			$last_query = $this->db->last_query();
			$result 	= $this->db->query($query)->result();
			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request)
				{
					$custom_team_id				= $get_request->custom_team_id;
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$apply_status			= $get_request->apply_status;
					$team_type				= $get_request->team_type;
					$custom_task_id			=$get_request->custom_task_id;
					
					$TeamConditions = $this->master_model->checkApplyTeamCondtions($get_request->apply_status, $get_request->challenge_owner_status);
					
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['challenge_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved', 'apply_user_id != ' => $creator_user_id));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'apply_user_id != ' => $creator_user_id));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					// Withdraw Slot COunt
					$getWithdrawnSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Withdrawn'));
					
					$withdraw = '';	
					//$edit = '';
					
					//$edit = '<a href="'.base_url('team/create/').base64_encode($get_request->c_id)."/".base64_encode($get_request->team_id).'" data-toggle="tooltip" class="btn btn-success btn-green-fresh" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';	
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));

					if ($team_type == 'challenge') {
						$dispStatus = $TeamConditions['dispStatus'];
						$actualChallengeStatus = $getChallenge[0]['challenge_status'];
						$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$category_id." - ".$chName.'</a>';
						$teamForAnychallenge = 'Yes';

						$view				= '<a href="'.base_url('myteams/team_details_owner/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" class="btn btn-success btn-green-fresh" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';

						if($apply_status == 'Withdrawn' || $challenge_owner_status == 'Approved' || $challenge_owner_status == 'Rejected' || $actualChallengeStatus == "Pending" || $actualChallengeStatus == "Rejected" || $actualChallengeStatus == "Withdrawn" || $actualChallengeStatus == "Closed")
						{
							$edit = '';
						} 
						else
						{
							$edit = '<a href="'.base_url('team/create/').base64_encode($get_request->c_id)."/".base64_encode($get_request->team_id).'" data-toggle="tooltip" class="btn btn-success btn-green-fresh" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';							
						}

					}else{
						$dispStatus = 'NA';
						$actualChallengeStatus = 'NA';
						//$chllenge_code_name		= $custom_task_id." - ".$get_request->task_name;
						
						$v_link				= base_url('myteams/task_details_owner/'.base64_encode($get_request->team_id));
						
						$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$custom_task_id." - ".$get_request->task_name.'</a>';
						
						$teamForAnychallenge = 'No';
						
						$view				= '<a href="'.base_url('myteams/task_details_owner/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" class="btn btn-success btn-green-fresh" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
						
						$edit = '<a href="'.base_url('tasks/create/').base64_encode($get_request->team_id).'" data-toggle="tooltip" class="btn btn-success btn-green-fresh" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';	
					}

					$disolve = '<a href="javascript:void(0)" data-tid='.base64_encode($get_request->team_id).' data-toggle="tooltip" class="btn btn-success btn-green-fresh disolve_team_btn" title="Disolve"><i class="fa fa-remove" aria-hidden="true"></i></a>';	



					
					
					//$companyName		= 'No';
					$teamid_name			= $custom_team_id." - ".$team_name;
					//$chllenge_code_name		= $category_id." - ".$chName;
					
						
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					<span>Application Withdrawn  : '.count($getWithdrawnSlot).'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $get_request->c_id, 'team_id' => $get_request->team_id, 'is_deleted' => '0'));
					/* echo $this->db->last_query(); exit; */
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					// Action Buttons
					$conCat					= '<div style="white-space:nowrap;">'.$view."&nbsp;".$edit."".$withdraw.'&nbsp'.$disolve. '</div>';
					
					
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlotCnt,	
					$totalPendingCnt,
					$hiddenDiv,
					$team_status,
					$actualChallengeStatus,
					$dispStatus,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr,
					"last_query" => $query
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		
		// BYT My Teams Listing 
		public function appliedTeams(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/applied-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function appliedChallengeListing(){
			error_reporting(0);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();	
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.custom_team_id,arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name, arai_byt_teams.apply_status,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach,arai_byt_teams.team_type,arai_byt_teams.task_name,custom_task_id,  
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,arai_byt_slot_applications.status as applicationStatus,
			arai_byt_slot_applications.apply_user_id, arai_byt_slot_applications.if_agree, arai_byt_slot_applications.introduce_urself, arai_byt_slot_applications.app_id 
			FROM arai_byt_slot_applications 
			JOIN arai_byt_teams ON arai_byt_slot_applications.team_id=arai_byt_teams.team_id 
			JOIN arai_byt_team_slots ON arai_byt_team_slots.slot_id = arai_byt_slot_applications.slot_id
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_slot_applications.is_deleted = '0' AND arai_byt_team_slots.is_deleted = '0' AND arai_byt_teams.user_id!='".$user_id."' AND arai_byt_slot_applications.apply_user_id = '".$user_id."' ".$c_data."";
			//AND arai_byt_teams.user_id!='".$user_id."' 
			$query = $condition.' GROUP BY arai_byt_teams.team_id LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "<pre>".$this->db->last_query(); print_r($result);die();
			$rowCount = $this->getNumData($condition." GROUP BY arai_byt_teams.team_id");	
			
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					$custom_team_id 	= $get_request->custom_team_id;
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$myAppstatus			= $get_request->applicationStatus;
					$app_id					= $get_request->app_id;
					$team_type				= $get_request->team_type;
					$custom_task_id			=$get_request->custom_task_id;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['challenge_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved', 'apply_user_id != ' => $creator_user_id));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
				
					$removeLink			= base_url('myteams/removeMemberrequest/'.base64_encode($get_request->app_id));				
					
					$delete = "";
					if($get_request->applicationStatus == 'Approved')
					{
						$onclick_fun = "withdrawn_application('".$get_request->app_id."')";
						$delete				= '<a href="javascript:void(0)" onclick="'.$onclick_fun.'" data-toggle="tooltip" title="Withdraw" data-id="'.$get_request->app_id.'" class="btn btn-success btn-green-fresh remove-app"><i class="fa fa-remove" aria-hidden="true"></i></a> ';
					}
					
					//$companyName		= 'No';
					$teamid_name			= $custom_team_id." - ".$team_name;
					//$chllenge_code_name		= $category_id." - ".$chName;



					$refId = "";
					
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					
					
					
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$TeamConditions = $this->master_model->checkApplyTeamCondtions($get_request->apply_status, $get_request->challenge_owner_status);
					

					if ($team_type == 'challenge') {
						$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$category_id." - ".$chName.'</a>';
						$dispStatus = $TeamConditions['dispStatus'];
						$teamForAnychallenge = 'Yes';
						if($myAppstatus != "Approved")
						{
							$team_status = $dispStatus = "-";
						}
						$view				= '<a href="'.base_url('myteams/team_details_member/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View" class="btn btn-success btn-green-fresh"><i class="fa fa-eye" aria-hidden="true"></i></a>';
					}else{
						
						$v_link				= base_url('myteams/task_details_member/'.base64_encode($get_request->team_id));
						
						$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$custom_task_id." - ".$get_request->task_name.'</a>';
						
						//$chllenge_code_name		= $custom_task_id." - ".$get_request->task_name;
						$dispStatus = 'NA';
						$teamForAnychallenge = 'No';
						if($myAppstatus != "Approved")
						{
							$team_status = $dispStatus = "-";
						}
						//$team_status = 
						//$dispStatus = $myAppstatus;
						
						$view				= '<a href="'.base_url('myteams/task_details_member/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View" class="btn btn-success btn-green-fresh"><i class="fa fa-eye" aria-hidden="true"></i></a>';
					}

					$conCat		= '<span style="white-space:nowrap">'.$view."&nbsp;".$delete.'</span>';
					
					
					
					$dataArr[] = array(
					$teamid_name,
					$myAppstatus,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,
					$team_status,
					$dispStatus,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		// Remove Slot application-listing
		public function removeMemberrequest(){
			
			// Login User
			$user_id = $this->session->userdata('user_id');	
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			$csrf_test_name = $this->security->get_csrf_hash();	
			
			// Post Values	
			$id = $this->input->post('id');	
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('status' => 'Withdrawn', 'updatedAt' => $updateAt);
			$updateQuery = $this->master_model->updateRecord('byt_slot_applications',$updateArr,array('app_id' => $id, 'is_deleted' => '0'));	
			
			$slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications sa',array('sa.app_id'=> $id, 'sa.is_deleted'=>'0'));			
			$up_slot['hiring_status'] = 'Open';						
			$up_slot['modified_on'] = date("Y-m-d H:i:s");
			$this->master_model->updateRecord('arai_byt_team_slots',$up_slot,array('slot_id' => $slot_application_data[0]['slot_id']));					
			
			//START : SEND MAIL TO TEAM OWNER
			//END : SEND MAIL TO TEAM OWNER
			
			// Log Added
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Log Details
			$logDetails 		= array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Slot application withdrawn By Login User",
			'module_name'	=> 'BYT Module',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			// Return Json Data
			$jsonData = array("token" => $csrf_test_name, "success" => 'success');
			
			echo json_encode($jsonData);	
			
			
		} // End Remove Slot Member
		
		
		public function teamListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			if(count($challengeDetails)){
				
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/team-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Team Listing 
		public function team_listing(){
			
			// error_reporting(E_ALL);
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			//echo ">>>>>>>>".$c_id;die();
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.skills, arai_byt_team_slots.role_name, arai_byt_team_slots.slot_id 
			FROM arai_byt_teams 
			JOIN arai_byt_team_slots ON arai_byt_teams.c_id=arai_byt_team_slots.c_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."' GROUP BY arai_byt_teams.team_id ".$c_data."";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "<pre>";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('myteams/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					//$chllenge_code_name		= $category_id." - ".$chName;
					$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$category_id." - ".$chName.'</a>';		
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,	
					$available_Slot,
					$hiddenDiv,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
					$rowCnt = $rowCount;
					$response = array(
					"draw" => $draw,
					"recordsTotal" => $rowCnt?$rowCnt:0,
					"recordsFiltered" => $rowCnt?$rowCnt:0,
					"data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}  // Team Listing ENd
		
		// BYT Team Profile View
		public function show_team_profile($tid){
			//error_reporting(0);
			
			$teamId = base64_decode($tid);		
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->join('arai_challenge','arai_byt_teams.c_id = arai_challenge.c_id');
			$getTeamDetails 	= $this->master_model->getRecords("byt_teams", array('team_id' => $teamId));
			
			// Show Team Members
			$getTeamSlot 	= $this->master_model->getRecords("byt_team_slots", array('team_id' => $teamId));
			
			$data['show_details']	 = $getTeamDetails;
			$data['show_slotlist'] = $getTeamSlot; 	
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/team-profile';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Team Final Selection By Challenge Owner
		public function team_selection(){
			
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Post Values
			$c_id 		= $this->input->post('c_id');	
			$t_status 	= $this->input->post('t_status');
			$t_id 		= $this->input->post('t_id');
			$updateAt	= date('Y-m-d H:i:s');
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			if($t_status == 'Accept'){
				$teamFinalStatus = 'Approved';
				} else {
				$teamFinalStatus = 'Rejected';
			}
			
			
			// Update Status
			$updateArr = array('challenge_owner_status' => $teamFinalStatus, 'modified_on' => $updateAt);
			$updateQuery = $this->master_model->updateRecord('byt_teams',$updateArr,array('team_id' => $t_id, 'user_id' => $user_id));	
			
			if($updateQuery){
				
				$getStatus 	= $this->master_model->getRecords("byt_teams", array('team_id' => $t_id));
				
				if($getStatus[0]['challenge_owner_status'] == 'Approved'){
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully selected');
					} else {
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully rejected');
				}
				
				} else {
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'warning', "content_text" => 'Somethign Wrong, Tray Again');
			}
			
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Log Details
			$logDetails 		= array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Team Approved/Rejected Selection",
			'module_name'	=> 'BYT Module',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);	
			
			echo json_encode($jsonData);
		}
		
		// BYT Apply Slot By Applicant For Challenge
		public function applySlot($team_id,$c_id,$slot_id=0){
			
			$user_id = $this->session->userdata('user_id');
			$team_id = base64_decode($team_id);		
			$challenge_id = base64_decode($c_id);	
			$user_sub_cat = $this->session->userdata('user_sub_category');
			$this->check_permissions->is_profile_complete();
			
			$this->db->select('c.*, e.min_team, e.max_team');
			$this->db->join('arai_eligibility_expectation e', 'e.c_id = c.c_id', '', FALSE);
			$challenge_data = $this->master_model->getRecords('arai_challenge c',array("c.c_id"=>$challenge_id));
			//echo $this->db->last_query();
			if(count($challenge_data) == 0) //Check challenge exist or not
			{ 
				$this->session->set_flashdata('error','Challenge not exist'); 
				redirect(base_url('challenge/challengeDetails/'.$c_id));
			}
			else 
			{
				if($challenge_data[0]['challenge_status'] != 'Approved') //Check challenge approves or not
				{ 
					$this->session->set_flashdata('warning','Challenge is not approved'); 
					redirect(base_url('challenge/challengeDetails/'.$c_id)); 
				}
				
				else if($challenge_data[0]['u_id'] == $user_id) //Check challenge Active or not
				{ 
					$this->session->set_flashdata('warning','Challenge owner can not participate in team'); 
					redirect(base_url('challenge/challengeDetails/'.$c_id));
				}
				
				else if($challenge_data[0]['status'] != 'Active') //Check challenge Active or not
				{ 
					$this->session->set_flashdata('warning','Challenge is not Active'); 
					redirect(base_url('challenge/challengeDetails/'.$c_id));
				}
				/* else if($challenge_data[0]['is_publish'] != 'Publish') //Check challenge Publish or not
					{ 
					$this->session->set_flashdata('error','Challenge is not Publish'); redirect(site_url('challenge')); 
				} */
				
				else 
				{
					if($challenge_data[0]['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $challenge_data[0]['challenge_close_date']) //Check challenge valid date
					{ }
					else { 
						$this->session->set_flashdata('warning','Participations closed for selected challenge');
						redirect(base_url('challenge/challengeDetails/'.$c_id));
					}
				}
			}
			
			
			
			
			if( $team_id == "" ||  $c_id=='' ){
				redirect(base_url('challenge'));
			}
			
			
			$this->db->select('app_id, status, is_deleted');
			$this->db->order_by('app_id DESC');
			$already_applied_check = $this->master_model->getRecords("byt_slot_applications", array('team_id' => $team_id, 'apply_user_id' => $user_id));			
			
			if(count($already_applied_check) > 0  ) 
			{
				if($already_applied_check[0]['status'] == "Withdrawn")
				{
					$this->session->set_flashdata('warning','You cannot apply to this team as you have withdrawn your application before');
					redirect(base_url('challenge/challengeDetails/'.$c_id));
				}
				else if($already_applied_check[0]['status'] == "Rejected")
				{
					$this->session->set_flashdata('warning','As your last application to this team was declined, further application is not permitted');
					redirect(base_url('challenge/challengeDetails/'.$c_id));
				}
				else
				{
					if($already_applied_check[0]['is_deleted'] == '0')
					{
						$this->session->set_flashdata('warning','You have already applied to this team, kindly visit the Teams listing section to modify or withdraw your application');
						redirect(base_url('challenge/challengeDetails/'.$c_id));
					}
				}
			}
			// Decode ID
			
			
			$this->db->where('FIND_IN_SET('.$user_sub_cat.', arai_type_master.category_id)');
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			$slotDetails = $this->master_model->getRecords("byt_team_slots", array('team_id' => $team_id, 'is_deleted' => '0','hiring_status'=>'Open'));
			
			
			
			if (count($slotDetails) == 0  ) {
				$this->session->set_flashdata('warning','You are not eligible to apply for this team');
				redirect(base_url('challenge/challengeDetails/'.$c_id));
			}
			
			
			
			$this->db->select('bt.c_id,bt.team_id,bt.share_files,bt.user_id,bt.team_id,bt.apply_status,bt.team_status,bt.share_files,bt.team_name,bt.team_size,bt.team_banner,bt.brief_team_info,bt.proposed_approach,bt.additional_information,bt.custom_team_id,c.challenge_id,c.challenge_title');
			$this->db->join('arai_challenge c','c.c_id=bt.c_id','LEFT',FALSE);
			$team_details= $this->master_model->getRecords('byt_teams bt',	array('bt.team_id'=>$team_id, 'bt.is_deleted'=>'0'));	
			//echo $this->db->last_query();die();
			$team_ower_id = $team_details[0]['user_id'];
			
			if ($user_id==$team_ower_id) {
				$this->session->set_flashdata('warning','Team owner can not apply for his own team');
				redirect(base_url('challenge/challengeDetails/'.$c_id));
			}	
			//START: Code added by vishal - when some one clicks on mail invitation link he will be approved direclty
			$slot_id = base64_decode($slot_id);
			
			if ($slot_id!='' && $slot_id > 0) {
					$insertArr 	= array(
					'c_id' 				=> $c_id,
					'team_id' 			=> $team_id,
					'slot_id'			=> $slot_id,
					'apply_user_id'		=> $user_id,
					'status' => 'Approved',
					'introduce_urself'	=> '',
					'if_agree'			=> ''
					);
					if ($this->master_model->insertRecord('byt_slot_applications',$insertArr)) {
					
					$up_slot['hiring_status'] = 'Closed';
					$up_slot['modified_on'] = date("Y-m-d H:i:s");
					$this->master_model->updateRecord('arai_byt_team_slots',$up_slot,array('slot_id' => $slot_id));

						$this->session->set_flashdata('success','You have successfully applied to this team');
						redirect(base_url('challenge/challengeDetails/'.$c_id));
					}
			}
				//END: Code added by vishal - when some one clicks on mail invitation link he will be approved direclty
			
			$data['slot_details']	 = $slotDetails;
			$data['team_details']    = $team_details;
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'byt/apply-slot';
			$this->load->view('front/front_combo',$data);
			
		}

		// BYT Apply Slot By Applicant For Challenge
		public function taskApplySlot($team_id,$slot_id=0){
			
			$user_id = $this->session->userdata('user_id');
			$team_id = base64_decode($team_id);		
			$user_sub_cat = $this->session->userdata('user_sub_category');
			$this->check_permissions->is_profile_complete();
			if( $team_id == ""  ){
				redirect(base_url());
			}

			$this->db->select('app_id, status, is_deleted');
			$this->db->order_by('app_id DESC');
			$already_applied_check = $this->master_model->getRecords("byt_slot_applications", array('team_id' => $team_id, 'apply_user_id' => $user_id));			
			
			if(count($already_applied_check) > 0  ) 
			{

				if($already_applied_check[0]['status'] == "Withdrawn")
				{
					$this->session->set_flashdata('warning','You cannot apply to this team as you have withdrawn your application before');
					redirect(base_url('myteams/appliedTeams'));
				}
				else if($already_applied_check[0]['status'] == "Rejected")
				{
					$this->session->set_flashdata('warning','As your last application to this team was declined, further application is not permitted');
						redirect(base_url('myteams/appliedTeams'));
				}
				else
				{
					if($already_applied_check[0]['is_deleted'] == '0')
					{
						$this->session->set_flashdata('warning','You have already applied to this team, kindly visit the Teams listing section to modify or withdraw your application');
						redirect(base_url('tasks/taskSearch/'));
					}
				}
			}
			// Decode ID
			
			
			$this->db->where('FIND_IN_SET('.$user_sub_cat.', arai_type_master.category_id)');
			$this->db->join('type_master','type_master.tid=byt_team_slots.slot_type');
			$slotDetails = $this->master_model->getRecords("byt_team_slots", array('team_id' => $team_id, 'is_deleted' => '0','hiring_status'=>'Open'));
			
			if (count($slotDetails) == 0  ) {
				$this->session->set_flashdata('warning','You are not eligible to apply for this team');
				redirect(base_url('tasks/taskSearch/'));
			}
			
			$this->db->select('bt.c_id,bt.team_id,bt.share_files,bt.user_id,bt.team_id,bt.apply_status,bt.team_status,bt.share_files,bt.team_name,bt.team_size,bt.team_banner,bt.brief_team_info,bt.proposed_approach,bt.additional_information,bt.custom_team_id,bt.custom_team_id,bt.task_name, bt.team_type ,bt.custom_task_id,bt.task_start_date,bt.task_end_date');
			
			$team_details= $this->master_model->getRecords('byt_teams bt',	array('bt.team_id'=>$team_id, 'bt.is_deleted'=>'0'));	
			//echo $this->db->last_query();die();

			if ( count($team_details)==0) {

				redirect(base_url('tasks/taskSearch/'));
			}
			if ( $team_details[0]['task_end_date']!=NULL ) {

				if($team_details[0]['task_start_date'] <= date("Y-m-d") && date("Y-m-d") <= $team_details[0]['task_end_date']) //Check challenge valid date
					{ }
					else { 
						$this->session->set_flashdata('warning','Participations closed for selected task');
						redirect(base_url('tasks/taskSearch/'));
					}

			}

			$team_ower_id = $team_details[0]['user_id'];
			if ($user_id==$team_ower_id) {
				$this->session->set_flashdata('warning','Team owner can not apply for his own team');
				redirect(base_url('tasks/taskSearch/'));
			}	

			$slot_id = base64_decode($slot_id);
			
			if ($slot_id!='' && $slot_id > 0) {
					$insertArr 	= array(
					'c_id' 				=> 0,
					'team_id' 			=> $team_id,
					'slot_id'			=> $slot_id,
					'apply_user_id'		=> $user_id,
					'status' => 'Approved',
					'introduce_urself'	=> '',
					'if_agree'			=> ''
					);
					if ($this->master_model->insertRecord('byt_slot_applications',$insertArr)) {
					
					$up_slot['hiring_status'] = 'Closed';
					$up_slot['modified_on'] = date("Y-m-d H:i:s");
					$this->master_model->updateRecord('arai_byt_team_slots',$up_slot,array('slot_id' => $slot_id));

						$this->session->set_flashdata('success','You have successfully applied to this team');
						redirect(base_url('myteams/task_details_member/'.base64_encode($team_id)));
					}
			}
			
			$data['slot_details']	 = $slotDetails;
			$data['team_details']    = $team_details;
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'tasks/apply-slot';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function getSlotSkillsAjax()
		{ 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$result['flag'] = "error";
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$slotId = $this->input->post('slotId', TRUE);
				
				if($slotId!= "")
				{
					$result['flag'] = "success";
					$html = '';
					$team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('slot_id'=>$slotId));
					if(count($team_slot_data) > 0)
					{
						$skillIdStr = $team_slot_data[0]['skills'];
						$this->db->where("id IN (".$skillIdStr.") AND (status = 'Active' OR profile_id = '".$team_slot_data[0]['user_id']."')",NULL, false);
						$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
						
						foreach ($skills as $key => $value) 
						{ 
						
							if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') 
							{ 
								$html .='<span class="badge badge-pill badge-info">'.$encrptopenssl->decrypt($value['name']).'</span>';
							}
						}
					}				
					
					$result['response'] = $html;
				}
				else
				{
					$result['flag'] = "success";
					$result['response'] = "";
				}
			}
			
			echo json_encode($result);
		}
		
		// Slot Application Received Ajax Functionality
		public function slot_app_received(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			// Post Values
			$intro_urself 		= $this->input->post('intro_urself');	
			$is_agree 			= $this->input->post('is_agree');
			$team_id 			= $this->input->post('team_id');
			$c_id 				= $this->input->post('ch_id');
			$slot_id 			= $this->input->post('slot_id');
			$updateAt			= date('Y-m-d H:i:s');	
			
			// Log Captured		
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Fetch Record
			$fetchRecord = $this->master_model->getRecords("byt_slot_applications", array('slot_id' => $slot_id, 'team_id' => $team_id, 'c_id' => $c_id, 'apply_user_id' => $user_id, 'is_deleted' => '0'));			
			
			if(count($fetchRecord) > 0){
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - Already Exist",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('warning','You already apply for this position');
				$jsonData = array("token" => $csrf_test_name, "success" => 'error', "content_text" => 'You already apply for this position.', "t_id" => $team_id);
				
				} else {
				
				// Insert SQL 
				$insertArr 	= array(
				'c_id' 				=> $c_id,
				'team_id' 			=> $team_id,
				'slot_id'			=> $slot_id,
				'apply_user_id'		=> $user_id,
				'introduce_urself'	=> $intro_urself,
				'if_agree'			=> $is_agree
				);
				$this->master_model->insertRecord('byt_slot_applications',$insertArr);	

				$team_data = $this->master_model->getRecords('arai_byt_teams',array('team_id'=> $team_id ));

				$team_type=$team_data[0]['team_type'];

				if ($team_type=='challenge') 
				{
				
				$email_info = $this->get_mail_content_apply_slot($team_id,$user_id,$slot_id);
				
				//send email to team owner
				
				$email_send='';
				$slug = $encrptopenssl->encrypt('byt_mail_to_team_owner_on_application_received');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
				
				if(count($subscriber_mail) > 0){			
					
					$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					
					
					$arr_words = [
					'[team_owner_name]',
					'[Team_Name]',
					'[Team_ID]',
					'[challenge_Name_with_hyperlink]',
					'[Challenge_ID]',
					'[Applicant_Name]',
					'[Applicant_Type]',
					'[Slot_name]',
					'[go_hiring_link]' 
					]; 
					$rep_array = [
					$email_info['team_owner_name'],
					$email_info['team_name'],
					$email_info['team_id'],
					$email_info['challenge_link'],
					$email_info['challenge_id'],
					$email_info['applicant_name'],
					$email_info['applicant_type'],
					$email_info['slot_name'],
					$email_info['go_hiring_link'],
					
					
					];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					
					$info_array=array(
					'to'		=>	$email_info['team_owner_email'],				
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				}
				
				//end mail code


				// send mail to applicant

				$my_teams_link = site_url('myteams/appliedTeams' );
				$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';

				$email_send='';
				$slug = $encrptopenssl->encrypt('byt_mail_to_applicant_when_apply_to_slot');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';

				if(count($subscriber_mail) > 0){			
					
					$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
					$arr_words = [
					'[Team_Name]',
					'[Team_ID]',
					'[challenge_Name_with_hyperlink]',
					'[Challenge_ID]',
					'[applicant_name]',
					'[Hyperlink_to_the_Team_I_have_applied_to_Page]' 
					]; 
					$rep_array = [
					$email_info['team_name'],
					$email_info['team_id'],
					$email_info['challenge_link'],
					$email_info['challenge_id'],
					$email_info['applicant_name'],
					$my_applied_teams_link,	
					];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					
					$info_array=array(
					'to'		=>	$email_info['applicant_email'],				
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				}

				// end : send mail to applicant
				}else{

				// task mails

				$email_info = $this->get_mail_content_apply_slot_tasks($team_id,$user_id,$slot_id);
				
				$email_send='';
				$slug = $encrptopenssl->encrypt('byt_task_mail_to_team_owner_on_application_received');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
				
				if(count($subscriber_mail) > 0){			
					
					$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					
					
					$arr_words = [
					'[team_owner_name]',
					'[Team_Name]',
					'[Team_ID]',
					'[Applicant_Name]',
					'[Applicant_Type]',
					'[Slot_name]',
					'[skills]',
					'[go_hiring_link]' 
					]; 
					$rep_array = [
					$email_info['team_owner_name'],
					$email_info['team_name'],
					$email_info['team_id'],
					$email_info['applicant_name'],
					$email_info['applicant_type'],
					$email_info['slot_name'],
					$email_info['skills'],
					$email_info['go_hiring_link'],
					
					
					];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					
					$info_array=array(
					'to'		=>	$email_info['team_owner_email'],				
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				}
				
				// send mail to applicant

				$my_teams_link = site_url('myteams/appliedTeams' );
				$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';

				$email_send='';
				$slug = $encrptopenssl->encrypt('byt_task_mail_to_applicant_when_apply_to_slot');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';

				if(count($subscriber_mail) > 0){			
					
					$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
					$arr_words = [
					'[Team_Name]',
					'[Team_ID]',
					'[applicant_name]',
					'[Hyperlink_to_the_Team_I_have_applied_to_Page]' 
					]; 
					$rep_array = [
					$email_info['team_name'],
					$email_info['team_id'],
					$email_info['applicant_name'],
					$my_applied_teams_link,	
					];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					
					$info_array=array(
					'to'		=>	$email_info['applicant_email'],				
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				}

					// end task mails
					
				}
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - New Application",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('success','You have successfully applied to this team');
				$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'You successfully apply for position.', "t_id" => $team_id);
			}
			
			echo json_encode($jsonData);
			
		}


		
		
		public function getNumData($query){
			//echo $query;
			return $rowCount = $this->db->query($query)->num_rows();
		}
		
		// IP Address Track
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
		// Search Teams Today 20th Oct 2020 Start  Today
		public function teamSearch(){
			
			
			//$slotDetails = $this->master_model->getRecords("byt_team_slots", array('slot_id' => $slotid, 'is_deleted' => '0'));			
			
			//Sill-Set Management			
			$skillset_mgt= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');			
			
			//Sill-Set Management More			
			$skillset_mgt2= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');		
			
			// Get Apply Teams Details Listing 
			$teamDetails  = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active"),'',array('team_id'  => 'ASC'),0,6);//, 'apply_status !='=> "Withdrawn")
			$totalCnts = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active"),'',array('team_id'  => 'ASC'));//, 'apply_status !='=> "Withdrawn") 
			
			$data['team_details']    = $teamDetails;
			$data['team_cnts']    	 = $totalCnts;
			$data['skill_set']		 = $skillset_mgt;
			$data['skillmore']		 = $skillset_mgt2;
			$data['page_title'] 	 = 'BYT';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'byt/search-teams';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// Filter Functionality For Teams 
		public function load_more_teams()
		{
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$id = $this->input->post('id');
			
			// Get Teams Total Count 
			$total_cnt  = $this->master_model->getRecords('byt_teams',array('is_deleted'=>"0", 'status'=>"Active"),'',array('team_id', 'ASC'));//, 'apply_status !='=> "Withdrawn")
			
			$showLimit = 6;
			
			$sql = "SELECT * FROM arai_byt_teams WHERE is_deleted = '0' AND status='Active' ORDER BY team_id ASC LIMIT ".$id.",".$showLimit."";//AND apply_status != 'Withdrawn' 
			$result 	= $this->db->query($sql)->result();	
			
			$showData = '';		
			if(count($result) > 0)
			{			
				foreach($result as $c_data)
				{
					$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $c_data->c_id));
					
					// Team Slot Details
					$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $c_data->c_id, 'team_id'=> $c_data->team_id, 'is_deleted' => '0'));
					$maxTeam = $encrptopenssl->decrypt($getSlotMaxMember[0]['max_team']);
					$minTeamCnt = count($slotDetails);
					$availableCnt = $maxTeam - $minTeamCnt;
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $c_data->c_id, 'team_id' => $c_data->team_id, 'is_deleted' => '0'));
					//echo $this->db->last_query(); exit;
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					$array_skills = array();
					foreach($slotDetails as $slot_list){
						if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }							
					}
					
					$combine = implode(",",$array_skills);
					$xp = explode(',',$combine);
					$arrayUnique = array_unique($xp);
					
					$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $c_data->c_id));
					$dCha = $encrptopenssl->decrypt($chDetails[0]['challenge_details']);
					
					if($c_data->team_banner!=""){
						$banner_team = base_url('uploads/byt/'.$c_data->team_banner);
						} else {
						$banner_team = base_url('assets/no-img.png'); 
					}
					
					$showData .= '<div class="col-sm-12 justify-content-center mt-4 team-box">
					<div class="boxSection25">
					<ul class="list-group list-group-horizontal counts">
					<li class="list-group-item">
					<div class="inner-box" style="border-bottom:none">
					<img src="'.$banner_team.'" alt="Banner" class="img-fluid">
					<h3 class="minheight" style="font-size:16px;">'.ucfirst($c_data->team_name).'</h3>
					<ul>
					<li> '.$totalSlotCnt.' Members </li> 
					<li>-</li>
					<li>'.$totalPendingCnt.' Available SLots</li>
					</ul>
					</div>
					
					</li>
					<li class="list-group-item">
					<div class="inner-box">
					<strong style="display:block; text-align:left; margin:15px 0 0px 0px;">Brief Info about Team </strong>
					<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0">'.substr($c_data->brief_team_info,0,90).'...<a href="javascript:void();" class="click-more" data-id="'.$c_data->team_id.'">view more</a></p>
					
					
					<h4>Looking For</h4>
					<div class="Skillset text-center">';
					$s = 0;	
					foreach($arrayUnique as $commonSkill){
						if($s < 9){
							$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$teamslist['user_id']."')",NULL, false);
							$skill_now = $this->master_model->getRecords('skill_sets');
							$skill_names = $encrptopenssl->decrypt($skill_now[0]['name']);												
							$showData .='<span class="badge badge-pill badge-info">'.$skill_names.'</span>';
						}
						$s++;
					}
					
					$showData .='</div>
					</div>														
					</li>
					<li class="list-group-item" style="height: 0 !important;">
					<div class="team_search owl-carousel owl-theme">';	
					
					// Slot Deatils Foreach	
					$TeamSr = 1;
					$m = 1;
					foreach($slotDetails as $slotD)
					{
						$skills = array();
						$skillTitle = 'Skills';											
						//$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id']));
						$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
						
						if(count($getSlotDetail) > 0)
						{												
							$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
							$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
							
							$catID = $getUserDetail[0]['user_category_id'];	
							$getNames = $encrptopenssl->decrypt($getUserDetail[0]['first_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['middle_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['last_name']); 
							$fullname = ucwords($getNames);
							
							if($catID == 1)
							{
								$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
								$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
								
								$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
								if ($getProfileDetail[0]['profile_picture']!="") {
									
									$imageUser = $imgPath;
									//$imageUser = $imgPath;
								} 
								else 
								{
									$imageUser = base_url('assets/no-img.png');
								}
								
								if($getProfileDetail[0]['skill_sets'] != "")
								{
									$skill_set_ids = $encrptopenssl->decrypt($getProfileDetail[0]['skill_sets']);
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_skill_sets");														
									}
								}
								
								if($getProfileDetail[0]['other_skill_sets'] != "") 
								{ 
									$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
								}														
							}  
							else if($catID == 2)
							{
								$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
								
								if(count($getOrgDetail) > 0)
								{															
									$profilePic = $encrptopenssl->decrypt($getOrgDetail[0]['org_logo']);
									$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
									if ($profilePic!="") {
										
										$imageUser = $imgPath;
										//$imageUser = $imgPath;
										} else {
										$imageUser = base_url('assets/no-img.png');
									}
									
								}
								
								$skillTitle = 'Sector';
								if($getOrgDetail[0]['org_sector'] != "")
								{
									$skill_set_ids = $getOrgDetail[0]['org_sector'];
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_organization_sector");														
									}
								}
								
								if($getOrgDetail[0]['org_sector_other'] != "") 
								{ 
									$skills[]['name'] = $encrptopenssl->decrypt($getOrgDetail[0]['org_sector_other']); 
								}														
							}									
							
							if($getSlotDetail[0]['apply_user_id'] == $c_data->user_id){
								$styles_hide = "hide-class";
								} else {
								$styles_hide = "d-inline-block";
							}
							
						} 
						else 
						{
							$imageUser = base_url('assets/no-img.png');
							$styles_hide = "d-inline-block";
							$fullname = "Open Slot";											
							
							$skill = $slotD['skills'];											
							$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
							$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');													
						} // Else End
						
						
						
						$showData .='<div class="item text-center">
						<img src="'.$imageUser.'" alt="'.$fullname.'" title="'.$fullname.'" />
						<h4>'.$fullname.'</h4>
						<div class="Skillset text-center">';
						if(count($skills)) 
						{
							$i=0;
							if($i < 6)
							{
								foreach ($skills as $key => $value) 
								{ 
									$showData .='<span class="badge badge-pill badge-info">'; if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { $showData .= $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { $showData .= $value['name']; } } $showData .='</span>';
									$i++;
								}
							}
						}
						$showData .='</div>
						<h4>Team Member '.$TeamSr.'</h4>';
						/*if(@$getSlotDetail[0]['status']!='Approved'): 
							$showData .='<a href="javascript:void(0);" class="'.$styles_hide.' apply-popup">Apply</a>';
						endif;*/
						$showData .='</div>';
						$m++; $TeamSr++;
					} // Slot Details Foeach End
					$showData .= '</div> 
					</li>
					
					<div class="container" style="clear:both;" >
					<div class="row buttonView mb-3">
					<div class="col-md-3">
					<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
					</div>
					<div class="col-md-6">
					<a href="'.base_url('myteams/team_details_member/'.base64_encode($c_data->team_id)).'" class="btn btn-primary w-100">View Details</a>
					</div>
					<div class="col-md-3">
					<a href="'.base_url('myteams/applySlot/').base64_encode($c_data->team_id)."/".base64_encode($c_data->c_id).'" class="btn btn-primary w-100">Apply</a>
					</div>
					</div>
					</div>	
					
					
					</ul>
					</div>
					</div>';			
					
					$nextid = $c_data->team_id;
					
				} // Foreach End
				
				if($total_cnt > $showLimit){
					
					$showData .= '<div class="row justify-content-center mt-4 show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b></b></div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
			
			
		} // Load More Team End
		
		public function team_filter(){		
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);		
			
			// Post Values
			$skillset 		= $this->input->post('skillset');
			$keyword 		= rtrim($this->input->post('search_txt'));
			//echo "<pre>";print_r($this->input->post());
			$c_data = "";
			
			//skill set
			if(count((array)$skillset) > 0){
				
				$c_data .= " AND (";
				$i = 0;
				foreach($skillset as $t){
					
					if($i!=(count($skillset) - 1)){
						$c_data .= " FIND_IN_SET('".$t."',`skills`) OR ";
						} else {
						$c_data .= " FIND_IN_SET('".$t."',`skills`))";
					}				
					$i++;
				}
				
			} // End Skill Set IF
			
			//domain set
			/*if(count((array)$domain_exp) > 0){
				
				$c_data .= " AND (";
				$i = 0;
				foreach($domain_exp as $t){
				
				if($i!=(count($domain_exp) - 1)){
				$c_data .= " FIND_IN_SET('".$t."',`domain_area_of_expertise_search`) OR ";
				} else {
				$c_data .= " FIND_IN_SET('".$t."',`domain_area_of_expertise_search`))";
				}				
				$i++;
				}
				
				} // End domain If 
				
				// Employement Status
				if($emp_status)
				{
				$c_data	.= " AND arai_student_profile.employement_status = '".$encrptopenssl->encrypt($emp_status)."'";
				}
				
				// Year Of Experience
				if($year_of_exp)
				{
				$c_data	.= " AND years_of_experience_search >= '".$year_of_exp."'";
				}
				
				// No Of Publications
				if($no_of_pub)
				{
				$c_data	.= " AND no_of_paper_publication >= '".$no_of_pub."'";
				}
				
				// No Of Patents Here
				if($no_of_patents)
				{
				$c_data	.= " AND no_of_patents >= '".$no_of_patents."'";
			}*/
			
			if($keyword)
			{
				//$enryptString = $encrptopenssl->encrypt($keyword);
				$designation = "$keyword";			
				
				//$str1 = '"%'.$designation.'%"';
				$c_data	.= " AND (`arai_byt_teams`.`team_name` LIKE '%".$designation."%' || `arai_byt_teams`.`brief_team_info` LIKE '%".$designation."%' || `arai_byt_teams`.`proposed_approach` LIKE '%".$designation."%' || `arai_byt_teams`.`additional_information` LIKE '%".$designation."%')";
			}
			
			$sql = "SELECT arai_byt_teams.team_id, arai_byt_teams.user_id, arai_byt_teams.c_id, arai_byt_teams.team_name,  
			arai_byt_teams.team_size,arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, arai_byt_teams.team_banner, 
			arai_byt_teams.additional_information, arai_byt_teams.team_status, arai_byt_team_slots.skills,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.role_name FROM `arai_byt_teams` 
			LEFT JOIN `arai_byt_team_slots` ON `arai_byt_teams`.`team_id`=`arai_byt_team_slots`.`team_id` 
			WHERE `arai_byt_teams`.`status` = 'Active' AND `arai_byt_teams`.`is_deleted` = '0' 
			".$c_data." GROUP BY arai_byt_teams.team_id ORDER BY arai_byt_teams.team_id ASC";//AND `arai_byt_teams`.`apply_status` != 'Withdrawn'
			//echo $sql;
			$result 	= $this->db->query($sql)->result();	
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$showData = '';
			
			if(count($result) > 0){
				
				foreach($result as $c_data){
					
					$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $c_data->c_id));
					
					// Team Slot Details
					$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $c_data->c_id, 'team_id'=> $c_data->team_id, 'is_deleted' => '0'));
					$maxTeam = $encrptopenssl->decrypt($getSlotMaxMember[0]['max_team']);
					$minTeamCnt = count($slotDetails);
					$availableCnt = $maxTeam - $minTeamCnt;
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $c_data->c_id, 'team_id' => $c_data->team_id, 'is_deleted' => '0'));
					//echo $this->db->last_query(); exit;
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					
					$array_skills = array();
					foreach($slotDetails as $slot_list){
						if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }
					}
					
					$combine = implode(",",$array_skills);
					$xp = explode(',',$combine);
					$arrayUnique = array_unique($xp);
					
					$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $c_data->c_id));
					$dCha = $encrptopenssl->decrypt($chDetails[0]['challenge_details']);
					
					if($c_data->team_banner!=""){
						$banner_team = base_url('uploads/byt/'.$c_data->team_banner);
						} else {
						$banner_team = base_url('assets/no-img.png');
					}	
					
					$showData .= '<div class="col-sm-12 justify-content-center mt-4 team-box '.$c_data->team_id.'">
					<div class="boxSection25">
					<ul class="list-group list-group-horizontal counts">
					<li class="list-group-item">
					<div class="inner-box">
					<img src="'.$banner_team.'" alt="Banner" class="img-fluid">
					<h3>'.ucfirst($c_data->team_name).'</h3>
					<ul>
					<li> '.$totalSlotCnt.' Members </li> 
					<li>-</li>
					<li>'.$totalPendingCnt.' Available Slots</li>
					</ul>
					</div>
					
					</li>
					<li class="list-group-item">
					<div class="inner-box">
					<strong style="display:block; text-align:left; margin:15px 0 0px 0px;">Brief Info about Team </strong>
					<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0">'.substr($c_data->brief_team_info,0,90).'...<a href="javascript:void();" class="click-more" data-id="'.$c_data->team_id.'">view more</a></p>
					<h4>Looking For</h4>
					<div class="Skillset text-center">';
					$s = 0;	
					foreach($arrayUnique as $commonSkill){
						if($s < 9){
							$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$c_data->user_id."')",NULL, false);
							$skill_now = $this->master_model->getRecords('skill_sets');
							$skill_names = $encrptopenssl->decrypt($skill_now[0]['name']);												
							$showData .='<span class="badge badge-pill badge-info">'.$skill_names.'</span>';
						}
						$s++;
					}
					
					$showData .='</div>
					</div>
					
					</li>
					<li class="list-group-item" style="height: 0 !important;">
					<div class="team_search owl-carousel owl-theme">';	
					
					// Slot Deatils Foreach	
					$TeamSr = 1;
					$m = 1;
					foreach($slotDetails as $slotD)
					{
						$skills = array();
						$skillTitle = 'Skills';
						$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
						if(count($getSlotDetail) > 0)
						{												
							$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
							$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
							
							$catID = $getUserDetail[0]['user_category_id'];	
							$getNames = $encrptopenssl->decrypt($getUserDetail[0]['first_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['middle_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['last_name']); 
							$fullname = ucwords($getNames);
							
							if($catID == 1)
							{
								$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
								$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
								
								$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
								if ($getProfileDetail[0]['profile_picture']!="") {
									
									$imageUser = $imgPath;
									//$imageUser = $imgPath;
									} else {
									$imageUser = base_url('assets/no-img.png');
								}									
								
								if($getProfileDetail[0]['skill_sets'] != "")
								{
									$skill_set_ids = $encrptopenssl->decrypt($getProfileDetail[0]['skill_sets']);
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_skill_sets");														
									}
								}
								
								if($getProfileDetail[0]['other_skill_sets'] != "") 
								{ 
									$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
								}
							}  
							else if($catID == 2)
							{														
								$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
								
								if(count($getOrgDetail) > 0)
								{															
									$profilePic = $encrptopenssl->decrypt($getOrgDetail[0]['org_logo']);
									$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
									if ($profilePic!="") {
										
										$imageUser = $imgPath;
										//$imageUser = $imgPath;
										} else {
										$imageUser = base_url('assets/no-img.png');
									}
									
								}
								
								$skillTitle = 'Sector';
								if($getOrgDetail[0]['org_sector'] != "")
								{
									$skill_set_ids = $getOrgDetail[0]['org_sector'];
									
									if($skill_set_ids != "")
									{
										$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
										$skills = $this->master_model->getRecords("arai_organization_sector");														
									}
								}
								
								if($getOrgDetail[0]['org_sector_other'] != "") 
								{ 
									$skills[]['name'] = $encrptopenssl->decrypt($getOrgDetail[0]['org_sector_other']); 
								}														
							}									
							
							if($getSlotDetail[0]['apply_user_id'] == $c_data->user_id){
								$styles_hide = "hide-class";
								} else {
								$styles_hide = "d-inline-block";
							}
							
						} 
						else 
						{											
							$imageUser = base_url('assets/no-img.png');
							$styles_hide = "d-inline-block";
							$fullname = "Open Slot";											
							
							$skill = $slotD['skills'];											
							$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
							$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');													
						} // Else End
						
						
						
						$showData .='<div class="item text-center">
						<img src="'.$imageUser.'" alt="'.$fullname.'" title="'.$fullname.'" />
						<h4>'.$fullname.'</h4>
						<div class="Skillset text-center">';
						if(count($skills)) 
						{
							$i=0;
							if($i < 6)
							{
								foreach ($skills as $key => $value) 
								{ 
									$showData .='<span class="badge badge-pill badge-info">'; if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { $showData .= $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { $showData .= $value['name']; } } $showData .='</span>';
									$i++;
								}
							}
						}
						$showData .='	</div>
						<h4>Team Member '.$TeamSr.'</h4>';
						/*if($getSlotDetail[0]['status']!='Approved'): 
							$showData .='<a href="javascript:void(0);" class="'.$styles_hide.' apply-popup">Apply</a>';
						endif;*/
						$showData .='</div>';
						$m++;  $TeamSr++;
					} // Slot Details Foeach End
					$showData .= '</div> 
					</li>
					<div class="container" style="clear:both;" >
					<div class="row buttonView mb-3">
					<div class="col-md-3">
					<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
					</div>
					<div class="col-md-6">
					<a href="'.base_url('myteams/team_details_member/'.base64_encode($c_data->team_id)).'" class="btn btn-primary w-100">View Details</a>
					</div>
					<div class="col-md-3">
					<a href="'.base_url('myteams/applySlot/').base64_encode($c_data->team_id)."/".base64_encode($c_data->c_id).'" class="btn btn-primary w-100">Apply</a>
					</div>
					</div>
					</div>		
					
					</ul>
					</div>
					</div>';			
					
					$nextid = $c_data->team_id;
					
				} // Foreach End			
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div class="text-center" style="width:100%;">No Record Found</div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
			
		} // End Filter
		
		
		
		function goHiring($team_id=0, $slot_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if($team_id == '0' || $slot_id == '0')
			{
				redirect(site_url('myteams/myCreatedteams'));
			}
			
			$data['team_id'] = $team_id = base64_decode($team_id);
			$data['slot_id'] = $slot_id = base64_decode($slot_id);
			
			$team_data = $this->master_model->getRecords('arai_byt_teams',array('team_id'=> $team_id, 'user_id'=>$this->login_user_id));
			$team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'user_id'=>$this->login_user_id));
			
			$this->db->join('arai_registration r','r.user_id = sa.apply_user_id','INNER',FALSE);
			$this->db->join('arai_registration_usersubcategory sc','sc.subcat_id = r.user_sub_category_id','LEFT',FALSE);
			$team_slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications sa',array('sa.team_id'=> $team_id, 'sa.slot_id'=>$slot_id, 'sa.is_deleted'=>'0'), 'sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id, sa.introduce_urself, sa.if_agree, sa.status, r.user_id, r.first_name, r.middle_name, r.last_name, r.user_category_id, r.user_sub_category_id, sc.sub_catname');
			
			if(count($team_data) == 0 || count($team_slot_data) == 0)//   || count($team_slot_application_data) == 0 
			{ 
				$this->session->set_flashdata('error','Error occurred. Please try again.');
				redirect(site_url('myteams/myCreatedteams')); 
			}
			
			if(isset($_POST) && count($_POST) > 0)
			{

				$up_data = $up_slot = array();
				$this->form_validation->set_rules('app_id', 'Application Id', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				$this->form_validation->set_rules('go_hiring_radio', 'Status', 'trim|required|xss_clean', array('required'=>'Please select the %s'));
				if($this->form_validation->run())
				{	
					$team_type=$team_data[0]['team_type'];
					$app_id = base64_decode($this->input->post('app_id'));
					$application_status = $this->input->post('go_hiring_radio');
					$reject_reason = $this->input->post('reject_reason');
					
					$slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications', array("app_id"=>$app_id, "team_id"=>$team_id, "slot_id"=>$slot_id, "is_deleted"=>'0'));
					
					if(count($slot_application_data) > 0)
					{				
						$up_data['status'] = $application_status;
						$up_data['updatedAt'] = date("Y-m-d H:i:s");
						if($application_status == 'Rejected') {
							$up_data['reject_reason'] = $reject_reason;
						}
						
						$this->master_model->updateRecord('arai_byt_slot_applications',$up_data,array('app_id' => $app_id));
						
						//FOR ACCEPTED MAIL SENDING
						if($application_status == 'Approved') 
						{							
							if ($team_type=='challenge') 
							{
							
							//START : SEND MAIL TO APPROVED USER							
							$email_info = $this->get_mail_content_apply_slot($team_id,$slot_application_data[0]['apply_user_id'],$slot_id);							
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_mail_to_applicant_on_application_accepted');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							if(count($subscriber_mail) > 0)
							{											
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[applicant_name]',
								'[Team_Name]',
								'[Team_ID]',
								'[challenge_Name_with_hyperlink]',
								'[Challenge_ID]',								
								]; 
								
								$rep_array = [
								$email_info['applicant_name'],
								$email_info['team_name'],
								$email_info['team_id'],
								$email_info['challenge_link'],
								$email_info['challenge_id'],					
								];
								
								$sub_content = str_replace($arr_words, $rep_array, $desc);
								
								$info_array=array(
								'to' =>	$email_info['applicant_email'],				
								'cc' =>	'',
								'from' => $fromadmin,
								'subject' => $subject_title,
								'view' => 'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}
							//END : SEND MAIL TO APPROVED USER
						   }else{
						   	//START : SEND MAIL TO APPROVED USER TASK

						   	$email_info = $this->get_mail_content_apply_slot_tasks($team_id,$slot_application_data[0]['apply_user_id'],$slot_id);							
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_tasks_mail_to_team_applicant_on_application_accepted');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							if(count($subscriber_mail) > 0)
							{											
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[applicant_name]',
								'[Team_Name]',
								'[Team_ID]',
								]; 
								
								$rep_array = [
								$email_info['applicant_name'],
								$email_info['team_name'],
								$email_info['team_id'],
								];
								
								$sub_content = str_replace($arr_words, $rep_array, $desc);
								
								$info_array=array(
								'to' =>	$email_info['applicant_email'],				
								'cc' =>	'',
								'from' => $fromadmin,
								'subject' => $subject_title,
								'view' => 'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}


						   	//END : SEND MAIL TO APPROVED USER TASK
						   }
							
						
							
							//IF MEMBER IS APPROVED THEN MAKE ALL REMAING MEMBER FLUSHED
							$this->db->select("sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id, sa.status, r.email");
							$this->db->join("arai_registration r", "r.user_id = sa.apply_user_id", "INNER", FALSE);
							$remainingSlotMember = $this->master_model->getRecords('arai_byt_slot_applications sa', array("sa.team_id"=>$team_id, "sa.slot_id"=>$slot_id, "sa.is_deleted"=>'0', "sa.status !="=>'Approved'));
							if(count($remainingSlotMember) > 0)
							{
								if ($team_type=='challenge') 
								{
								//START : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED
								$c_link = site_url('challenge' );	
								$challenge_listing_link = '<a href='.$c_link.' target="_blank">here</a>';
								
								$email_info = $this->get_team_info_for_mail($team_id);								
								$email_send='';
								$slug = $encrptopenssl->encrypt('byt_mail_to_all_other_applicant_on_application_accepted');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								if(count($subscriber_mail) > 0)
								{												
									$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
									$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
									$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
									$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
									
									$arr_words = [
									'[Team_Name]',
									'[Team_ID]',
									'[challenge_Name_with_hyperlink]',
									'[Challenge_ID]',
									'[hyperlink_to_challenge_listing_page]'									
									]; 
									
									$rep_array = [
									$email_info['team_name'],
									$email_info['team_id'],
									$email_info['challenge_link'],
									$email_info['challenge_id'],
									$challenge_listing_link
									];
									$sub_content = str_replace($arr_words, $rep_array, $desc);									
								}
								//END : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED
							    }else{
							    //START : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED TASKS

								
								$email_info = $this->get_team_info_for_mail($team_id);								
								$email_send='';
								$slug = $encrptopenssl->encrypt('byt_tasks_mail_to_all_other_applicant_on_application_accepted');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								if(count($subscriber_mail) > 0)
								{												
									$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
									$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
									$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
									$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

									$tasks_teams_link = site_url('tasks/taskSearch');
									$task_team_listing_url = '<a href='.$tasks_teams_link.' target="_blank">here</a>';
									
									$arr_words = [
									'[Team_Name]',
									'[Team_ID]',
									'[hyperlink_to_task_teams_listing]'									
									]; 
									
									$rep_array = [
									$email_info['team_name'],
									$email_info['team_id'],
									$task_team_listing_url
									];
									$sub_content = str_replace($arr_words, $rep_array, $desc);									
								}	
							     
							    //END : SEND MAIL TO USER AS ONE MEMBER IS ACCEPTED	TASKS
							    }
								
								
								foreach($remainingSlotMember as $res)
								{
									$up_member['is_deleted'] = '1';
									$up_member['updatedAt'] = date("Y-m-d H:i:s");
									$this->master_model->updateRecord('arai_byt_slot_applications',$up_member,array('app_id' => $res['app_id']));
									
									if($res['status'] == 'Pending' && count($subscriber_mail) > 0)
									{		
										$applicant_mail = $encrptopenssl->decrypt($res['email']);
										$info_array=array(
										'to' =>	$applicant_mail,				
										'cc' =>	'',
										'from' =>	$fromadmin,
										'subject'	=> 	$subject_title,
										'view' => 'common-file'
										);
										
										$other_infoarray	=	array('content' => $sub_content); 
										$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
									}
								}
							}	
						 				
						}
						else if($application_status == 'Rejected') 
						{ 
							if ($tem_type=='challenge') {

							$email_info = $this->get_mail_content_apply_slot($team_id,$slot_application_data[0]['apply_user_id'],$slot_id);
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_mail_to_applicant_on_application_rejected');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							$c_link = site_url('challenge' );
							$challenge_listing_link = '<a href='.$c_link.' target="_blank">'.$c_link.'</a>';							
							
							if(count($subscriber_mail) > 0)
							{											
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
								
								$arr_words = [
								'[applicant_name]',
								'[Team_Name]',
								'[Team_ID]',
								'[challenge_Name_with_hyperlink]',
								'[Challenge_ID]',								
								'[hyperlink_to_challenge_listing_page]',								
								]; 
								
								$rep_array = [
								$email_info['applicant_name'],
								$email_info['team_name'],
								$email_info['team_id'],
								$email_info['challenge_link'],
								$email_info['challenge_id'],								
								$challenge_listing_link
								];
								
								$sub_content = str_replace($arr_words, $rep_array, $desc);
								
								$info_array=array(
								'to' =>	$email_info['applicant_email'],				
								'cc' =>	'',
								'from' =>	$fromadmin,
								'subject'	=> 	$subject_title,
								'view' =>  'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}
							}else{

							//START : SEND MAIL TO REJECTED USER TASK

						   	$email_info = $this->get_mail_content_apply_slot_tasks($team_id,$slot_application_data[0]['apply_user_id'],$slot_id);							
							$email_send='';
							$slug = $encrptopenssl->encrypt('byt_tasks_mail_to_team_applicant_on_application_rejected');
							$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
							$sub_content = '';
							
							if(count($subscriber_mail) > 0)
							{											
								$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
								$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
								$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
								$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
								$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);


								$tasks_teams_link = site_url('tasks/taskSearch');
								$task_team_listing_url = '<a href='.$tasks_teams_link.' target="_blank">here</a>';
								
								$arr_words = [
								'[applicant_name]',
								'[Team_Name]',
								'[Team_ID]',
								'[hyperlink_to_task_teams_listing]'
								]; 
								
								$rep_array = [
								$email_info['applicant_name'],
								$email_info['team_name'],
								$email_info['team_id'],
								$task_team_listing_url
								];
								
								$sub_content = str_replace($arr_words, $rep_array, $desc);
								
								$info_array=array(
								'to' =>	$email_info['applicant_email'],				
								'cc' =>	'',
								'from' => $fromadmin,
								'subject' => $subject_title,
								'view' => 'common-file'
								);
								
								$other_infoarray	=	array('content' => $sub_content); 
								
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}


						   	//END : SEND MAIL TO APPROVED USER TASK
							}
						}
												
						if($application_status == 'Approved') { $up_slot['hiring_status'] = 'Closed'; } 
						else { $up_slot['hiring_status'] = 'Open'; }
						
						$up_slot['modified_on'] = date("Y-m-d H:i:s");
						$this->master_model->updateRecord('arai_byt_team_slots',$up_slot,array('slot_id' => $slot_id));					
						$this->session->set_flashdata('success','Application status successfully updated for slot');
					}
					else
					{
						$this->session->set_flashdata('error','Error occurred. Please try again.');
					}
					
					//START : INSERT LOG 
					$postArr = $this->input->post();
					$log_data['post_data'] 	= $postArr;
					$log_data['slot_application_data'] 	= $up_data;
					$log_data['slot_data'] 	= $up_slot;
					
					$logDetails['user_id'] = $this->login_user_id;
					$logDetails['module_name'] = 'Myteams : goHiring';
					$logDetails['store_data'] = json_encode($log_data);
					$logDetails['ip_address'] = $this->get_client_ip();
					$logDetails['createdAt'] = date('Y-m-d H:i:s');
					
					$logDetails['action_name'] = 'Slot Application Status Changed';
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					//END : INSERT LOG
					
					
					if($application_status == 'Approved') 
					{ 
						if ($team_type=='challenge')
						{
							redirect(site_url('myteams/team_details_owner/'.base64_encode($team_id))); 
						}
						else 
						{
							redirect(site_url('myteams/task_details_owner/'.base64_encode($team_id)));
						}
					}
					else { redirect(site_url('myteams/goHiring/'.base64_encode($team_id).'/'.base64_encode($slot_id))); }
					
				}
			}
			
			$TeamSkillStr = '';
			$this->db->where("id IN (".$team_slot_data[0]['skills'].")");
			$TeamSkillSetData = $this->master_model->getRecords("arai_skill_sets");
			if(count($TeamSkillSetData) > 0)
			{
				foreach($TeamSkillSetData as $TeamSkill)
				{
					$TeamskillSetName = $encrptopenssl->decrypt($TeamSkill['name']);
					if(strtolower($TeamskillSetName) != 'other')
					{
						$TeamSkillStr .= $TeamskillSetName.", ";
					}
				}
			}
			
			$accetpted_application_id = '';
			$accetpted_application_data = $this->master_model->getRecords('arai_byt_slot_applications', array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'is_deleted'=>'0', 'status'=>'Approved'));
			if(count($accetpted_application_data) > 0) { $accetpted_application_id = $accetpted_application_data[0]['app_id']; }
			
			$data['team_slot_data'] = $team_slot_data;
			$data['TeamSkillStr'] = $TeamSkillStr;
			$data['accetpted_application_id'] = $accetpted_application_id;
			$data['team_slot_application_data'] = $team_slot_application_data;
			$data['page_title'] = 'Teams - Go Hiring';
			$data['middle_content'] = 'byt/go_hiring';
			$this->load->view('front/front_combo',$data);
		}
		
		function remove_slot_member_ajax($team_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				if($team_id == '0') { redirect(site_url('myteams/myCreatedteams')); }
				else
				{
					$team_data = $this->master_model->getRecords('arai_byt_teams',array("team_id"=>base64_decode($team_id)));
					$data['team_data'] = $team_data;
					if(count($team_data) == 0) 
					{ 
						$this->session->set_flashdata('error','Invalid team selection'); redirect(site_url('myteams/myCreatedteams')); 
					}
					else 
					{
						if($team_data[0]['user_id'] != $this->login_user_id)
						{
							$this->session->set_flashdata('error','Invalid team selection'); 
							redirect(site_url('myteams/team_details_owner/'.$team_id));
						}
						else if($team_data[0]['is_deleted'] != '0')
						{
							$this->session->set_flashdata('error','Team is deleted');  
							redirect(site_url('myteams/team_details_owner/'.$team_id));
						}
						else if($team_data[0]['status'] != 'Active')
						{
							$this->session->set_flashdata('warning','Team is not active'); 
							redirect(site_url('myteams/team_details_owner/'.$team_id));
						}
					}
				}
				
				$this->form_validation->set_rules('app_id', 'App id', 'trim|required|xss_clean', array('required'=>'Please enter the %s'));
				$this->form_validation->set_rules('remove_reason', 'Reason', 'trim|required|xss_clean', array('required'=>'Please select the %s'));
				
				if($this->form_validation->run())
				{	
					$team_type=$team_data[0]['team_type'];
					$app_id = $this->input->post('app_id');
					$remove_reason = $this->input->post('remove_reason');
					
					if($app_id != "")
					{
						$app_id = base64_decode($this->input->post('app_id'));				
						$slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications sa',array('sa.app_id'=> $app_id, 'sa.is_deleted'=>'0'));
						
						if(count($slot_application_data) > 0)
						{
							$team_id = $slot_application_data[0]['team_id'];
							$slot_id = $slot_application_data[0]['slot_id'];
							$apply_user_id = $slot_application_data[0]['apply_user_id'];
							
							$team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'user_id'=>$this->login_user_id));
							
							if(count($team_slot_data) == 0) 
							{ 
								$this->session->set_flashdata('error','Error occurred');
							}
							else
							{
								$chk_remove_reason_other = $this->master_model->getRecords('arai_challenge_withdrawal',array('status'=>'Active', 'id'=>$remove_reason));
								
								if(count($chk_remove_reason_other) > 0 && strtolower($encrptopenssl->decrypt($chk_remove_reason_other[0]['reason'])) == 'other')
								{
									$other_reason = $this->input->post('other_reason');
									if($other_reason == "") { $this->session->set_flashdata('error','Error occurred'); redirect(site_url('myteams/team_details_owner/'.base64_encode($team_id))); }
									$up_data['remove_reason_other'] = $other_reason;
								}
								else { $up_data['remove_reason_other'] = ""; }
								
								$up_data['remove_reason'] = $remove_reason;							
								$up_data['status'] = 'Rejected';
								$up_data['is_deleted'] = '1';
								$up_data['updatedAt'] = date("Y-m-d H:i:s");
								$this->master_model->updateRecord('arai_byt_slot_applications',$up_data,array('app_id' => $app_id));
								
								$up_slot['hiring_status'] = 'Open';						
								$up_slot['modified_on'] = date("Y-m-d H:i:s");
								$this->master_model->updateRecord('arai_byt_team_slots',$up_slot,array('slot_id' => $slot_id));					
								$this->session->set_flashdata('success','Member successfully removed from slot');
								
								//START : SEND REMOVE MAIL TO REMOVED MEMBER
								if ($team_type=='challenge') {
								$email_info = $this->get_mail_content_apply_slot($team_id,$apply_user_id,$slot_id);
								
								$email_send='';
								$slug = $encrptopenssl->encrypt('byt_mail_to_applicant_on_application_removed');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								$my_teams_link = site_url('myteams/appliedTeams' );
								$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">'.$my_teams_link.'</a>';
								
								if(count($subscriber_mail) > 0)
								{												
									$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
									$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
									$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
									$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
									
									$arr_words = [
									'[applicant_name]',
									'[Team_Name]',
									'[Team_ID]',
									'[Challenge_ID]',
									'[Hyperlink_to_the_Team_I_have_applied_to_Page]',	
									'[challenge_Name_with_hyperlink]',								
									]; 
									
									$rep_array = [
									$email_info['applicant_name'],
									$email_info['team_name'],
									$email_info['team_id'],
									$email_info['challenge_id'],
									$my_applied_teams_link,
									$email_info['challenge_link'],
									];
									
									$sub_content = str_replace($arr_words, $rep_array, $desc);
									
									$info_array=array(
									'to' =>	$email_info['applicant_email'],				
									'cc' =>	'',
									'from' =>	$fromadmin,
									'subject'	=> 	$subject_title,
									'view' => 'common-file'
									);
									
									$other_infoarray	=	array('content' => $sub_content);									
									$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
									}
								//END : SEND REMOVE MAIL TO REMOVED MEMBER

							    }else{
						    	//START : SEND REMOVE MAIL TO REMOVED MEMBER FOR TASK
						    	$email_info = $this->get_mail_content_apply_slot_tasks($team_id,$apply_user_id,$slot_id);
								
								$email_send='';
								$slug = $encrptopenssl->encrypt('byt_tasks_mail_to_applicant_on_application_removed');
								$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
								$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
								$sub_content = '';
								
								$my_teams_link = site_url('myteams/appliedTeams');
								$my_applied_teams_link = '<a href='.$my_teams_link.' target="_blank">here</a>';

								$task_team_listing_url = site_url('tasks/taskSearch');

								$tasks_teams_link = site_url('tasks/taskSearch');
								$task_team_listing_url = '<a href='.$tasks_teams_link.' target="_blank">here</a>';
								
								if(count($subscriber_mail) > 0)
								{												
									$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
									$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
									$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
									$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
									
									$arr_words = [
									'[applicant_name]',
									'[Team_Name]',
									'[Team_ID]',
									'[Hyperlink_to_the_Team_I_have_applied_to_Page]',	
									'[hyperlink_to_task_teams_listing]'
									]; 
									
									$rep_array = [
									$email_info['applicant_name'],
									$email_info['team_name'],
									$email_info['team_id'],
									$my_applied_teams_link,
									$task_team_listing_url
									];
									
									$sub_content = str_replace($arr_words, $rep_array, $desc);
									
									$info_array=array(
									'to' =>	$email_info['applicant_email'],				
									'cc' =>	'',
									'from' =>	$fromadmin,
									'subject'	=> 	$subject_title,
									'view' => 'common-file'
									);
									
									$other_infoarray	=	array('content' => $sub_content);									
									$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
									}
									//END : SEND REMOVE MAIL TO REMOVED MEMBER FOR TASK
							    }
								
								//START : SEND REMOVE MAIL TO TEAM OWNER
								//END : SEND REMOVE MAIL TO TEAM OWNER
								
								//START : SEND REMOVE MAIL TO OTHER TEAM MEMBER
								//END : SEND REMOVE MAIL TO OTHER TEAM MEMBER
								
								//START : INSERT LOG 
								$postArr = $this->input->post();
								$log_data['post_data'] 	= $postArr;
								$log_data['slot_application_data'] 	= $up_data;
								$log_data['slot_data'] 	= $up_slot;
								
								$logDetails['user_id'] = $this->login_user_id;
								$logDetails['module_name'] = 'Myteams : remove_slot_member_ajax';
								$logDetails['store_data'] = json_encode($log_data);
								$logDetails['ip_address'] = $this->get_client_ip();
								$logDetails['createdAt'] = date('Y-m-d H:i:s');
								
								$logDetails['action_name'] = 'Slot Remove Member';
								$logData = $this->master_model->insertRecord('logs',$logDetails);
								//END : INSERT LOG
								
								//IF WANT TO SEND MAIL OF REJECTION THEN ADD MAIL CODE HERE
							}	
							
							redirect(site_url('myteams/team_details_owner/'.base64_encode($team_id)));
						}
						else 
						{ 
							$this->session->set_flashdata('error','Error occurred'); 
						}
					}
					else 
					{ 
						$this->session->set_flashdata('error','Error occurred'); 
					}					
					redirect(site_url('myteams/team_details_owner/'.$team_id));
				}
				else 
				{ 
					$this->session->set_flashdata('error','Error occurred');
					redirect(site_url('myteams/team_details_owner/'.$team_id));
				}
			}
			
			redirect(site_url('myteams/myCreatedteams'));
		}
		
		
		public function get_mail_content_apply_slot($team_id,$appy_user_id,$apply_slot_id){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('challenge.c_id,challenge.u_id,byt_teams.share_files,byt_teams.user_id,byt_teams.team_id,apply_status,byt_teams.team_status,share_files,team_name,team_size,team_banner,brief_team_info,proposed_approach,additional_information,custom_team_id,challenge_id,challenge_title');
			$this->db->join('challenge','challenge.c_id=byt_teams.c_id');
			$team_details= $this->master_model->getRecords('byt_teams',array('team_id'=>$team_id));
			
			if (count($team_details)) {
				
				$this->db->select('title,first_name,middle_name,last_name,email,sub_catname');
				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
				$apply_user_data = $this->master_model->getRecords("registration",array('user_id'=>$appy_user_id ));
				
				
				$user_id = $team_details[0]['user_id'];
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$team_owner_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
				
				
				$user_arr = array();
				if(count($apply_user_data)){	
					
					foreach($apply_user_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$user_arr[] = $row_val;
					}
					
				}
				
				$owner_arr = array();
				if(count($team_owner_data)){	
					
					foreach($team_owner_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$owner_arr[] = $row_val;
					}
					
				}
				
				
				$applicant_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$applicant_type=$user_arr[0]['sub_catname'];
				$applicant_email=$user_arr[0]['email'];
				
				$team_owner_name = $owner_arr[0]['title']." ".$owner_arr[0]['first_name']." ".$owner_arr[0]['last_name'];
				
				
				$c_name= $encrptopenssl->decrypt($team_details[0]['challenge_title']);
				
				$c_link = base_url('challenge/challengeDetails/').base64_encode( $team_details[0]['c_id'] );
				$challenge_link = '<a href='.$c_link.' target="_blank">'.$c_name.'</a>';
				
				$my_ch_link = base_url('challenge/myChallenges');
				$challengepage_link = '<a href='.$my_ch_link.' target="_blank">here</a>';
				
				$t_link = base_url('myteams/team_details_owner/').base64_encode( $team_details[0]['team_id'] );
				$owner_page_link = '<a href='.$t_link.' target="_blank">here</a>';
				
				$go_hiring_url = base_url('myteams/goHiring/').base64_encode( $team_details[0]['team_id']."/".base64_encode($apply_slot_id) );
				
				$go_hiring_link = '<a href='.$go_hiring_url.' target="_blank">here</a>';


				$slot_link = base_url('myteams/applySlot/').base64_encode( $team_details[0]['team_id'] ).'/'.base64_encode( $team_details[0]['c_id'] ).'/'.base64_encode( $apply_slot_id );
				$apply_slot_url = 'click <a href='.$slot_link.' target="_blank">here</a> to apply';
				
				$slot_data = $this->master_model->getRecords("byt_team_slots",array('slot_id'=>$apply_slot_id),'role_name' );

			//https://tipapp01uat.araiindia.com/arai_testing/myteams/applySlot/MjM5/MTM3
				
				
				$slot_name= $slot_data[0]['role_name']; 
				
				
				$email_array=array(
				'team_name'=>$team_details[0]['team_name'],
				'challenge_title'=>$c_name,
				'applicant_name'=>$applicant_name,
				'applicant_type'=>$applicant_type,
				'applicant_email'=>$applicant_email,
				'slot_name'=>$slot_name,
				'team_owner_name'=>$team_owner_name,
				'team_owner_email'=>$owner_arr[0]['email'],
				'chal_owner_email'=>$user_arr[0]['email'],
				'team_id'=>$team_details[0]['custom_team_id'],
				'challenge_id'=>$team_details[0]['challenge_id'],
				'challenge_link'=>$challenge_link,
				'challengepage_link'=>$challengepage_link,
				'hyperlink_to_owner_view'=>$owner_page_link,
				'go_hiring_link'=>$go_hiring_link,
				'apply_slot_url'=>$apply_slot_url
				);
				
				return $email_array;
				// echo "<pre>";
				// print_r($email_array);
			}
			
			
		}


		public function get_mail_content_apply_slot_tasks($team_id,$appy_user_id,$apply_slot_id){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
		
			$team_details= $this->master_model->getRecords('byt_teams',array('team_id'=>$team_id));
			
			if (count($team_details)) {
				
				$this->db->select('title,first_name,middle_name,last_name,email,sub_catname');
				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
				$apply_user_data = $this->master_model->getRecords("registration",array('user_id'=>$appy_user_id ));
				
				
				$user_id = $team_details[0]['user_id'];
				
				$this->db->select('title,first_name,middle_name,last_name,email');
				$team_owner_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
				
				
				$user_arr = array();
				if(count($apply_user_data)){	
					
					foreach($apply_user_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$user_arr[] = $row_val;
					}
					
				}
				
				$owner_arr = array();
				if(count($team_owner_data)){	
					
					foreach($team_owner_data as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$owner_arr[] = $row_val;
					}
					
				}
				
				
				$applicant_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$applicant_type=$user_arr[0]['sub_catname'];
				$applicant_email=$user_arr[0]['email'];
				
				$team_owner_name = $owner_arr[0]['title']." ".$owner_arr[0]['first_name']." ".$owner_arr[0]['last_name'];
				
				
				$t_link = base_url('myteams/team_details_owner/').base64_encode( $team_details[0]['team_id'] );
				$owner_page_link = '<a href='.$t_link.' target="_blank">here</a>';
				
				$go_hiring_url = base_url('myteams/goHiring/').base64_encode( $team_details[0]['team_id']."/".base64_encode($apply_slot_id) );
				
				$go_hiring_link = '<a href='.$go_hiring_url.' target="_blank">here</a>';
				
				$slot_data = $this->master_model->getRecords("byt_team_slots",array('slot_id'=>$apply_slot_id),'role_name,skills,user_id' );

				$slot_link = base_url('myteams/taskApplySlot/').base64_encode( $team_details[0]['team_id'] ).'/'.base64_encode( $apply_slot_id );
				$apply_slot_url = '<a href='.$slot_link.' target="_blank">here</a>';

				if(count($slot_data) > 0)
					{
						$html = '';
						$skillIdStr = $slot_data[0]['skills'];
						$this->db->where("id IN (".$skillIdStr.") AND (status = 'Active' OR profile_id = '".$slot_data[0]['user_id']."')",NULL, false);
						$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
						
						foreach ($skills as $key => $value) 
						{ 
						
							if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') 
							{ 
								$html .='<span>'.$encrptopenssl->decrypt($value['name']).'</span>'.',';
							}
						}
					}
				
				$slot_name= $slot_data[0]['role_name']; 
				
				
				$email_array=array(
				'team_name'=>$team_details[0]['team_name'],
				'applicant_name'=>$applicant_name,
				'applicant_type'=>$applicant_type,
				'applicant_email'=>$applicant_email,
				'slot_name'=>$slot_name,
				'skills'=>$html,
				'team_owner_name'=>$team_owner_name,
				'team_owner_email'=>$owner_arr[0]['email'],
				'team_id'=>$team_details[0]['custom_team_id'],
				'hyperlink_to_owner_view'=>$owner_page_link,
				'go_hiring_link'=>$go_hiring_link,
				'apply_slot_url'=>$apply_slot_url
				);
				
				return $email_array;
				// echo "<pre>";
				// print_r($email_array);
			}
			
			
		}
		
		
		function sendInvitation($team_id=0, $slot_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if($team_id == '0' || $slot_id == '0') { redirect(site_url('myteams/myCreatedteams')); }
			
			$data['team_id'] = $team_id = base64_decode($team_id);
			$data['slot_id'] = $slot_id = base64_decode($slot_id);
			
			$team_data = $this->master_model->getRecords('arai_byt_teams',array('team_id'=> $team_id, 'user_id'=>$this->login_user_id));
			$data['team_slot_data'] = $team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'user_id'=>$this->login_user_id));
			if(count($team_data) == 0 || count($team_slot_data) == 0) 
			{ 
				$this->session->set_flashdata('error','Error occurred. Please try again.');
				redirect(site_url('myteams/myCreatedteams')); 
			}
			
			$data['team_slot_application_data'] = $team_slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications sa',array('sa.team_id'=> $team_id, 'sa.slot_id'=>$slot_id, 'sa.is_deleted'=>'0'), 'sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id');
			
			$TeamSkillStr = '';
			$this->db->where("id IN (".$team_slot_data[0]['skills'].")");
			$TeamSkillSetData = $this->master_model->getRecords("arai_skill_sets");
			if(count($TeamSkillSetData) > 0)
			{
				foreach($TeamSkillSetData as $TeamSkill)
				{
					$TeamskillSetName = $encrptopenssl->decrypt($TeamSkill['name']);
					if(strtolower($TeamskillSetName) != 'other')
					{
						$TeamSkillStr .= $TeamskillSetName.", ";
					}
				}
			}
			$data['TeamSkillStr'] = $TeamSkillStr;		
			
			$data['skill_set'] = $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');
			$data['domain_list'] = $this->master_model->array_sorting($this->master_model->getRecords('domain_master',array('status'=>"Active")), array('id'),'domain_name'); 
			$data['employement_status'] = $this->master_model->getRecords('employement_master',array('status'=>"Active"));
			$data['year_of_exp'] = $this->master_model->getRecords('year_of_exp',array('status'=>"Active"));
			$data['no_of_publication'] = $this->master_model->getRecords('no_paper_publications',array('status'=>"Active"));
			$data['no_of_patents'] = $this->master_model->getRecords('no_of_patents',array('status'=>"Active"));
			
			$data['page_title'] = 'Teams - Go Hiring';
			$data['middle_content'] = 'byt/sendInvitation';
			$this->load->view('front/front_combo',$data);
		}
		
		function getInvitationDataAjax($team_id=0, $slot_id=0)
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$search_str = '';
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$start = $this->input->post('start', TRUE);
				$limit = $this->input->post('limit', TRUE);
				if($start != "" && $limit!= "" )
				{
					$data['team_id'] = $team_id = base64_decode($team_id);
					$data['slot_id'] = $slot_id = base64_decode($slot_id);
					
					$team_data = $this->master_model->getRecords('arai_byt_teams',array('team_id'=> $team_id, 'user_id'=>$this->login_user_id));
					$team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'user_id'=>$this->login_user_id));
					if(count($team_data) == 0 || count($team_slot_data) == 0) { $result['flag'] = "error"; }
					else
					{
						$team_slot_application_data = $this->master_model->getRecords('arai_byt_slot_applications sa',array('sa.team_id'=> $team_id, 'sa.is_deleted'=>'0'), 'sa.app_id, sa.c_id, sa.team_id, sa.slot_id, sa.apply_user_id');
						
						$excludeUsers = "";
						if(count($team_slot_application_data) > 0)
						{
							foreach($team_slot_application_data as $res)
							{
								$excludeUsers .= $res['apply_user_id'].",";
							}
						}
						$result['flag'] = "success";
						
						//START : FOR SEARCH KEYWORD
						$keyword = trim($this->input->post('keyword'));
						if($keyword)
						{
							$search_str	.= " (sp.designation LIKE '%".$keyword."%' OR sp.company_name = '".$encrptopenssl->encrypt($keyword)."' OR r.json_str LIKE '%".$keyword."%')";
						}
						//END : FOR SEARCH KEYWORD
						
						//START : FOR SEARCH SKILLSET
						$skillset = trim($this->input->post('skillset'));
						if($skillset != '')
						{
							$skillset_arr = explode(",",$skillset);
							if(count($skillset_arr) > 0)
							{
								$old_search_str = $search_str;
								if($old_search_str != '') { $search_str .= " AND "; }
								$search_str .= ' r.user_category_id != 2';
								
								$search_str .= " AND (";
								$i = 0;
								foreach($skillset_arr as $res)
								{					
									$search_str .= " FIND_IN_SET('".$res."',sp.skill_sets_search)";
									if($i != (count($skillset_arr) - 1)) { $search_str .= " OR "; }			
									$i++;
								}								
								$search_str .= " )";
							}
						}
						//END : FOR SEARCH SKILLSET
						
						if($search_str != "") { $this->db->where($search_str); }
						
						$this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', FALSE);
						$this->db->join('arai_profile_organization o', 'o.user_id = r.user_id', 'LEFT', FALSE);
						$this->db->join('arai_registration_usersubcategory sc','sc.subcat_id = r.user_sub_category_id','LEFT',FALSE);
						$this->db->join('arai_byt_team_invitation ti','ti.invited_user_id = r.user_id','LEFT',FALSE);
						$this->db->where("(sp.profile_completion_status ='complete' OR o.profile_completion_status = 'complete')");
						if($excludeUsers != "") { $this->db->where("r.user_id NOT IN (".rtrim($excludeUsers,",").")"); }
						$this->db->limit($limit, $start);
						$this->db->order_by("r.first_name","ASC");
						$this->db->group_by("r.user_id");

						$data['invitation_users'] = $invitation_users = $this->master_model->getRecords('arai_registration r',array('r.discoverable'=>"yes",'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"), 'r.contactable,r.user_id, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, sp.profile_picture, sp.skill_sets, sp.other_skill_sets, sp.skill_sets_search, sp.domain_area_of_expertise_search, sp.employement_status, sp.years_of_experience_search, sp.no_of_paper_publication, sp.no_of_patents,  o.org_logo, o.org_sector, o.org_sector_other, sc.sub_catname, ti.invitation_id,ti.team_id, sp.profile_completion_status as StudProStatus, o.profile_completion_status AS OrgProStatus');
						//$result['qry'] = $this->db->last_query();
						//$result['search_str'] = $search_str;						
						
						//START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
						if($search_str != "") { $this->db->where($search_str); }
						
						$this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', FALSE);
						$this->db->join('arai_profile_organization o', 'o.user_id = r.user_id', 'LEFT', FALSE);
						$this->db->where("(sp.profile_completion_status ='complete' OR o.profile_completion_status = 'complete')");
						if($excludeUsers != "") { $this->db->where("r.user_id NOT IN (".rtrim($excludeUsers,",").")"); }
						$data['total_user_count'] = $total_user_count = $this->master_model->getRecordCount('arai_registration r',array('r.discoverable'=>"yes",'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"), 'r.user_id');
						//END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
						
						$data['new_start'] = $new_start = $start + $limit;
						$result['response'] = $this->load->view('front/byt/incSendInvitationCommon', $data, true);
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";	
			}
			
			echo json_encode($result);
		}
		
		function sendInvitationAjax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$invitation_msg = $this->input->post('invitation_msg', TRUE);
				$user_id = $this->input->post('user_id', TRUE);
				$team_id = $this->input->post('team_id', TRUE);
				$slot_id = $this->input->post('slot_id', TRUE);
				$user_name = $this->input->post('user_name', TRUE);
				$user_email = $this->input->post('user_email', TRUE);
				
				if($invitation_msg != "" && $user_id!= ""  && $team_id!= ""  && $slot_id!= ""   && $user_name!= ""   && $user_email!= "" )
				{
					$team_id = base64_decode($team_id);
					$slot_id = base64_decode($slot_id);
					$user_email = base64_decode($user_email);
					
					$team_data = $this->master_model->getRecords('arai_byt_teams',array('team_id'=> $team_id, 'user_id'=>$this->login_user_id));
					$team_slot_data = $this->master_model->getRecords('arai_byt_team_slots',array('team_id'=> $team_id, 'slot_id'=>$slot_id, 'user_id'=>$this->login_user_id));
					if(count($team_data) == 0 || count($team_slot_data) == 0) { $result['flag'] = "error"; }
					else
					{
						$team_type=$team_data[0]['team_type'];

						$add_data['team_id'] = $team_id;
						$add_data['slot_id'] = $slot_id;
						$add_data['invited_user_id'] = $user_id;
						$add_data['invite_message'] = $invitation_msg;
						$add_data['created_on'] = date("Y-m-d H:i:s");
						$this->master_model->insertRecord('arai_byt_team_invitation',$add_data,TRUE);
						
						//SEND MAIL CODE GOES HERE
						if ($team_type=='challenge') {
						$email_info = $this->get_mail_content_apply_slot($team_id,$user_id,$slot_id); 
						// echo "<pre>"; print_r($email_info);die;
						$email_send='';
						$slug = $encrptopenssl->encrypt('byt_invitaton_mail_go_hring_page');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0){			
							
							$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
							$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
							
							$arr_words = [
							'[invitee_name]',
							'[Team_Name]',
							'[Team_ID]',
							'[challenge_Name_with_hyperlink]',
							'[Challenge_ID]',	
							'[slot_name]',							
							]; 
							
							$rep_array = [
							$email_info['applicant_name'],
							$email_info['team_name'],
							$email_info['team_id'],
							$email_info['apply_slot_url'],
							$email_info['challenge_id'],
							$email_info['slot_name'],
							
							];
							
							$sub_content = str_replace($arr_words, $rep_array, $desc);
							
							$info_array=array(
							'to'		=>	$email_info['applicant_email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						}

						}else{

						$email_info = $this->get_mail_content_apply_slot_tasks($team_id,$user_id,$slot_id); 
						$email_send='';
						$slug = $encrptopenssl->encrypt('byt_invitaton_mail_task_team_go_hring_page');
						$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
						$sub_content = '';
						
						if(count($subscriber_mail) > 0){			
							
							$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
							$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
							$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
							$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);

							$tasks_teams_link = site_url('tasks/taskSearch');
							$task_team_listing_url = '<a href='.$tasks_teams_link.' target="_blank">here</a>';
							
							$arr_words = [
							'[invitee_name]',
							'[Team_Name]',
							'[team_owner_name]',
							'[Team_ID]',
							'[slot_name]',
							'[hyperlink_to_task_teams_listing]'							
							]; 
							
							$rep_array = [
							$email_info['applicant_name'],
							$email_info['team_name'],
							$email_info['team_owner_name'],
							$email_info['team_id'],
							$email_info['slot_name'],
							$email_info['apply_slot_url']
							
							];
							
							$sub_content = str_replace($arr_words, $rep_array, $desc);
							
							$info_array=array(
							'to'		=>	$email_info['applicant_email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
							
							$other_infoarray	=	array('content' => $sub_content); 
							
							$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						}	

						}
						
						$result['flag'] = "success";
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";	
			}
			
			echo json_encode($result);
		}
		
		function CommonUserDetailsAjax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$result['flag'] = "error";
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$user_id = $this->input->post('user_id', TRUE);
				
				if($user_id!= "")
				{
					$user_id = base64_decode($user_id);
					
					$this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', FALSE);
					$this->db->join('arai_profile_organization o', 'o.user_id = r.user_id', 'LEFT', FALSE);
					$this->db->join('arai_registration_usersubcategory sc','sc.subcat_id = r.user_sub_category_id','LEFT',FALSE);
					$user_details = $this->master_model->getRecords('arai_registration r',array('r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0", 'r.user_id'=>$user_id), 'r.user_id, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, sp.profile_picture, sp.skill_sets, sp.other_skill_sets, sp.skill_sets_search, sp.domain_area_of_expertise_search, sp.employement_status, sp.years_of_experience_search, sp.no_of_paper_publication, sp.no_of_patents,  o.org_logo, o.org_sector, o.org_sector_other, sc.sub_catname');
					//$result['qry'] = $this->db->last_query();
					
					if(count($user_details) > 0)
					{
						$display_name = $encrptopenssl->decrypt($user_details[0]['first_name']);  
						if($encrptopenssl->decrypt($user_details[0]['middle_name']) != "") { $display_name .= " ".$encrptopenssl->decrypt($user_details[0]['middle_name']); } 
						$display_name .= " ".$encrptopenssl->decrypt($user_details[0]['last_name']);
						
						$profile_img_icon = base_url('assets/profile_picture/user_dummy.png');
						$skillset_str = '';
						$skillTitle = 'Skills';
						if($user_details[0]['user_category_id'] == 1)
						{
							if($user_details[0]['profile_picture'] !='' ) 
							{ 
								$profile_img_icon = base_url('assets/profile_picture/'.$user_details[0]['profile_picture']);
							}
							
							if($user_details[0]['skill_sets'] != "")
							{
								$skill_set_ids = $encrptopenssl->decrypt($user_details[0]['skill_sets']);
								
								if($skill_set_ids != "")
								{
									$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
									$SkillSetData = $this->master_model->getRecords("arai_skill_sets");
									if(count($SkillSetData) > 0)
									{
										foreach($SkillSetData as $SkillRes)
										{
											$skillSetName = $encrptopenssl->decrypt($SkillRes['name']);
											if(strtolower($skillSetName) != 'other')
											{
												$skillset_str .= $skillSetName.", ";
											}
										}
									}
								}
							}
							
							if($user_details[0]['other_skill_sets'] != "") { $skillset_str .= $encrptopenssl->decrypt($user_details[0]['other_skill_sets']); }
						}
						else
						{
							if($user_details[0]['org_logo'] !='' ) 
							{ 
								$profile_img_icon = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($user_details[0]['org_logo']));
							}
							
							$skillTitle = 'Sector';
							if($user_details[0]['org_sector'] != "")
							{
								$skill_set_ids = $user_details[0]['org_sector'];
								
								if($skill_set_ids != "")
								{
									$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
									$SkillSetData = $this->master_model->getRecords("arai_organization_sector");
									if(count($SkillSetData) > 0)
									{
										foreach($SkillSetData as $SkillRes)
										{
											$skillSetName = $SkillRes['name'];
											if(strtolower($skillSetName) != 'other')
											{
												$skillset_str .= $skillSetName.", ";
											}
										}
									}
								}
							}
							
							if($user_details[0]['org_sector_other'] != "") { $skillset_str .= $encrptopenssl->decrypt($user_details[0]['org_sector_other']); }
						} 
						
						
						$result['flag'] = "success";
						$html = '	<div class="modal-header">
						<h5 class="modal-title text-capitalize" id="UserDetailModalLabel">'.$display_name.'</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						</div>
						
						<div class="modal_custom_content">
						<table class="table table-bordered">
						<tbody>													
						<tr><td colspan="2" class="text-center"> <a href="'.$profile_img_icon.'" target="_blank"><img src="'.$profile_img_icon.'" style="max-height:100px; max-width:100px;"></a></td></tr>
						<tr><td><strong>Name</strong> </td><td class="text-capitalize"> '.$display_name.'</td></tr>
						<tr><td><strong>Type</strong> </td><td> '.$encrptopenssl->decrypt($user_details[0]['sub_catname']).'</td></tr>
						<tr><td><strong>'.$skillTitle.'</strong> </td><td> '.rtrim($skillset_str,", ").'</td></tr>
						</tbody>
						</table>
						<div class="modal-footer p-2">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						</div>';
						
						
						$result['response'] = $html;
					}
				}
			}
			
			echo json_encode($result);
		}
		
	}							