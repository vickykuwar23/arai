<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Resource_sharing extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        // $this->login_user_id = $this->session->userdata('user_id');
        // if($this->login_user_id == "") { redirect(site_url()); }

        //     ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {
        //$module_id = 8;
        //$this->check_permissions->is_authorise($module_id);

//             ini_set('display_errors', '1');
        //             ini_set('display_startup_errors', '1');
        //             error_reporting(E_ALL);

        $this->check_permissions->is_logged_in();
        $encrptopenssl              = new Opensslencryptdecrypt();
        $data['featured_limit']     = $featured_limit     = 15;
        $data['non_featured_limit'] = $non_featured_limit = 9;

        // $this->db->order_by('type_name', 'ASC');
        // $this->db->order_by("FIELD(type_name, 'other')", "DESC", false);
        // $data['type_data'] = $this->master_model->getRecords("knowledge_repository_type_master", array("status" => 'Active'));

        $data['type_data'] = array();

        $data['tag_data'] = $this->master_model->getRecords("arai_resource_sharing_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['page_title']     = 'Resource sharing';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'resource_sharing/index';
        $this->load->view('front/front_combo', $data);
    }

    public function getFeaturedNonFeaturedResourceDataAjax()
    {
        $encrptopenssl                           = new Opensslencryptdecrypt();
        $csrf_new_token                          = $this->security->get_csrf_hash();
        $result['csrf_new_token']                = $csrf_new_token;
        $data_non_featured['featured_limit']     = $featured_limit     = 15;
        $data_non_featured['non_featured_limit'] = $non_featured_limit = 9;

        $search_str = '';

        $login_user_arr = $this->get_login_user_data();

        $data_non_featured['login_user_name']  = $login_user_arr['loggedin_user_name'];
        $data_non_featured['login_user_email'] = $login_user_arr['loggedin_user_email'];
        $data_featured['login_user_name']      = $login_user_arr['loggedin_user_name'];
        $data_featured['login_user_email']     = $login_user_arr['loggedin_user_email'];

        if (isset($_POST) && count($_POST) > 0) {
            $f_start                                   = $this->input->post('f_start', true);
            $f_limit                                   = $this->input->post('f_limit', true);
            $nf_start                                  = $this->input->post('nf_start', true);
            $nf_limit                                  = $this->input->post('nf_limit', true);
            $is_show_more                              = $this->input->post('is_show_more', true);
            $data_non_featured['kr_type_id_load_more'] = $this->input->post('kr_type_id_load_more', true);
             $order_by                                  = $this->input->post('order_by', true);
            if ($f_start != "" && $f_limit != "" && $nf_start != "" && $nf_limit != "" && $is_show_more != "") {
                $result['flag'] = "success";
                $user_id        = $this->session->userdata('user_id');
                $BlogActiondata = $this->getBlogActiondata($user_id);

                //START : FOR SEARCH KEYWORD
                $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword'));
                if ($keyword) {
                    $search_str .= " ( r.id_disp LIKE '%" . $keyword . "%' OR r.resouce_name LIKE '%" . $keyword . "%' OR r.resource_details LIKE '%" . $keyword . "%' OR r.resource_specification LIKE '%" . $keyword . "%' OR r.owner_name LIKE '%" . $keyword . "%' OR c.city_name LIKE '%" . $keyword . "%' ) ";
                }
                //END : FOR SEARCH KEYWORD

                //START : FOR SEARCH TYPE
                $technology = trim($this->input->post('technology'));
                if ($technology != '') {
                    $technology_arr = explode(",", $technology);
                    if (count($technology_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($technology_arr as $res) {
                            $search_str .= " k.kr_type =  " . $res . "  ";
                            if ($i != (count($technology_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TYPE

                //START : FOR SEARCH TAG
                $tag = trim($this->input->post('tag'));
                if ($tag != '') {
                    $tag_arr = explode(",", $tag);
                    if (count($tag_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($tag_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',r.tags)";
                            if ($i != (count($tag_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TAG

                //START : FOR TERMS
                $resource_terms = trim($this->input->post('resource_terms'));
                if ($resource_terms != '') {
                    $resource_terms_arr = explode(",", $resource_terms);
                    if (count($resource_terms_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($resource_terms_arr as $res) {
                            $search_str .= " r.commercial_terms = '" . $res . "'";
                            if ($i != (count($resource_terms_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR TERMS

                //END : FOR SEARCH DATE
                $from_date = trim($this->input->post('from_date'));
                $to_date   = trim($this->input->post('to_date'));

                if ($from_date != '' && $to_date != '') {

                    $from_date = date('Y-m-d', strtotime($from_date));
                    $to_date   = date('Y-m-d', strtotime($to_date));

                    $old_search_str = $search_str;
                    if ($old_search_str != '') {$search_str .= " AND ";}
                    $search_str .= "( DATE(r.created_on) BETWEEN '" . $from_date . "' and '" . $to_date . "' )";
                }

                // START : FOR FEATURED
                $featured_blog_total_cnt = 0;
                if ($is_show_more == 0) {
                    if ($search_str != "") {$this->db->where($search_str);}
                    // $this->db->order_by('r_id','DESC',FALSE);
                    $this->db->order_by('r.xOrder', 'ASC', false);
                    $select_f = "r.*,c.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other' ) AS DispTags";

                    $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
                    $data_featured['featured_kr']    = $featured_kr    = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_featured' => '1', 'r.is_block' => '0'), $select_f, '', $f_start, $f_limit);
                    $featured_kr_total_cnt           = count($featured_kr);
                    $data_featured['BlogActiondata'] = $BlogActiondata;

                    $result['featured_kr_qry']   = $this->db->last_query();
                    $result['Featured_response'] = $this->load->view('front/resource_sharing/incFeatureResCommon', $data_featured, true);
                }
                // START : FOR FEATURED

                // START : FOR NON FEATURED
                if ($search_str != "") {$this->db->where($search_str);}
                $select_nf = "r.*,c.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags";
                if ($order_by != '') {
                    $this->db->order_by('r_id', $order_by, false);                    
                }else{

                    $this->db->order_by('r_id', 'DESC', false);
                }
                // $this->db->order_by('r.xOrder','ASC',FALSE);
                $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
                $data_non_featured['non_featured_kr'] = $non_featured_kr = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_featured' => '0', 'r.is_block' => '0'), $select_nf, '', $nf_start, $nf_limit);
                // echo $this->db->last_query();die;
                $data_non_featured['BlogActiondata'] = $BlogActiondata;
                //$result['non_featured_blog_qry'] = $this->db->last_query();

                if ($search_str != "") {$this->db->where($search_str);}
                $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
                $data_non_featured['total_non_featured_kr_count'] = $total_non_featured_kr_count = $this->master_model->getRecordCount('arai_resource_sharing r', array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_featured' => '0', 'r.is_block' => '0'), 'r.r_id');

                if ($search_str != "") {$this->db->where($search_str);}
                $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
                $featured_kr_count = $this->master_model->getRecordCount("arai_resource_sharing r", array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_featured' => '1', 'r.is_block' => '0'));

                $data_non_featured['featured_kr_total_cnt'] = $featured_kr_count;
                $data_non_featured['new_start']             = $new_start             = $nf_start + $nf_limit;
                $result['NonFeatured_response']             = $this->load->view('front/resource_sharing/incNonFeatureResCommon', $data_non_featured, true);

                // END : FOR NON FEATURED
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function my_resource() // MY BLOGS LIST

    {
        $this->check_permissions->is_logged_in();
        $user_id                = $this->session->userdata('user_id');
        $data['page_title']     = 'Resource';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'resource_sharing/listing';
        $this->load->view('front/front_combo', $data);
    }

    public function my_listing() // MY BLOGS LIST : GET SERVER SIDE DATATABLE DATA USING AJAX

    {
        $this->check_permissions->is_logged_in();
        $csrf_test_name  = $this->security->get_csrf_hash();
        $user_id         = $this->session->userdata('user_id');
        $draw            = $_POST['draw'];
        $row             = $_POST['start'];
        $rowperpage      = $_POST['length']; // Rows display per page
        $columnIndex     = @$_POST['order'][0]['column']; // Column index
        $columnName      = @$_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = @$_POST['order'][0]['dir']; // asc or desc
        $searchValue     = @$_POST['search']['value']; // Search value
        $pageNo          = ($_POST['start'] / $_POST['length']) + 1;

        $condition = "SELECT * FROM arai_resource_sharing WHERE is_deleted = '0' AND user_id = '" . $user_id . "' ORDER BY r_id DESC";
        $query     = $condition . ' LIMIT  ' . $this->input->post('length') . ' OFFSET ' . $this->input->post('start');
        $result    = $this->db->query($query)->result();
        $rowCount  = $this->getNumData($condition);
        $rowCnt    = 0;
        $dataArr   = array();

        if ($rowCount > 0) {
            $no = $_POST['start'];
            $i  = 0;
            foreach ($result as $showResult) {
                $r_id         = $showResult->r_id;
                $id_disp      = '<a href="' . base_url('resource_sharing/details/' . base64_encode($r_id)) . '" class="applicant-listing">' . $showResult->id_disp . '</a>';
                $resouce_name = ucwords($showResult->resouce_name);
                $owner_name   = $showResult->owner_name;
                $posted_on    = date("d M, Y h:i a", strtotime($showResult->created_on));

                $admin_status = $action = '';

                if ($showResult->admin_status == 0) {$admin_status = 'Pending';} else if ($showResult->admin_status == 1) {$admin_status = 'Approved';} else if ($showResult->admin_status == 2) {$admin_status = 'Rejected';}

                $action .= '<a href="' . base_url('resource_sharing/add/' . base64_encode($r_id)) . '" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                if ($showResult->is_block == '0') {
                    $b_title = 'block';
                    $b_icon  = 'fa fa-ban';} else if ($showResult->is_block == '1') {
                    $b_title = 'unblock';
                    $b_icon  = 'fa fa-unlock-alt';}
                $onclick_block_fun = "block_unblock_kr('" . base64_encode($r_id) . "', '" . $b_title . "')";

                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="' . $b_title . '" class="btn btn-info btn-green-fresh" onclick="' . $onclick_block_fun . '"><i class="' . $b_icon . '" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                $onclick_delete_fun = "delete_resource('" . base64_encode($r_id) . "')";
                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" class="btn btn-info btn-green-fresh" onclick="' . $onclick_delete_fun . '"><i class="fa fa-remove" aria-hidden="true"></i></a> ';

                $onclick_like_fun = "show_resource_likes('" . base64_encode($r_id) . "')";
                $getTotalLikes    = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $r_id), 'like_id');
                $disp_likes       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';

                $onclick_report_fun = "show_resource_requests('" . base64_encode($r_id) . "')";
                $getTotalRequests   = $this->master_model->getRecordCount('arai_resource_sharing_connect_request', array('r_id' => $r_id), 'reported_id');
                $disp_requests      = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalRequests . '</a></div>';

                $i++;
                $dataArr[] = array(
                    $id_disp,
                    $resouce_name,
                    // $owner_name,
                    $disp_likes,
                    $disp_requests,
                    $posted_on,
                    $admin_status,
                    "<span style='white-space:nowrap;'>" . $action . "</span>",
                );
                $rowCnt   = $rowCount;
                $response = array(
                    "draw"            => $draw,
                    "recordsTotal"    => $rowCnt ? $rowCnt : 0,
                    "recordsFiltered" => $rowCnt ? $rowCnt : 0,
                    "data"            => $dataArr,
                );
                $response['token'] = $csrf_test_name;
            }
        } else {
            $rowCnt   = $rowCount;
            $response = array(
                "draw"            => $draw,
                "recordsTotal"    => 0,
                "recordsFiltered" => 0,
                "data"            => $dataArr,
            );
            $response['token'] = $csrf_test_name;
        }
        echo json_encode($response);

    } // End Function

    public function add($id = 0) // ADD NEW

    {
        $this->check_permissions->is_logged_in();

        $module_id = 33;
        $this->check_permissions->is_authorise($module_id);

        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("arai_resource_sharing", array('r_id' => $id, 'user_id' => $user_id, 'is_deleted' => 0));
            // print_r($form_data);die;
            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else { $data['mode'] = $mode = "Add";}

            $data['r_files_data'] = $r_files_data = $this->master_model->getRecords('arai_resource_sharing_files', array("r_id" => $id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));

        }

        $data['banner_img_error'] = $data['location_error'] = $data['tags_error'] = $error_flag = '';
        $file_upload_flag         = 0;
        if (isset($_POST) && count($_POST) > 0) {

            //$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');
            $this->form_validation->set_rules('resouce_name', 'resouce name', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('resource_details', 'resource details', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('resource_specification', 'resource specification', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('owner_name', 'owner name', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('owner_email', 'owner email', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('owner_contact_number', 'owner contact number', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('location', 'location', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));

            if ($mode == 'Add' && ((!isset($_FILES['banner_img'])) || $_FILES['banner_img']['size'] == 0)) {
                $data['banner_img_error'] = 'Please select the Banner';
                $error_flag               = 1;
            }

            if (!isset($_POST['location'])) {
                $data['location_error'] = 'Please select the location';
                $error_flag             = 1;
            }

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['banner_img']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $banner_img = $this->Common_model_sm->upload_single_file("banner_img", array('png', 'jpg', 'jpeg', 'gif'), "banner_img_" . date("YmdHis"), "./uploads/resource_banner_img", "png|jpeg|jpg|gif");
                    if ($banner_img['response'] == 'error') {
                        $data['banner_img_error'] = $banner_img['message'];
                        $file_upload_flag         = 1;
                    } else if ($banner_img['response'] == 'success') {
                        $add_data['banner_img'] = $banner_img['message'];
                        /* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
                    }
                }

                if ($file_upload_flag == 0) {
                    $add_data['user_id']                = $user_id;
                    $add_data['resouce_name']           = $this->input->post('resouce_name');
                    $add_data['resource_details']       = $this->input->post('resource_details');
                    $add_data['resource_specification'] = $this->input->post('resource_specification');
                    $add_data['owner_name']             = $this->input->post('owner_name');
                    $add_data['owner_email']            = $this->input->post('owner_email');
                    $add_data['country_code']           = $this->input->post('country_code');
                    $add_data['owner_contact_number']   = $this->input->post('owner_contact_number');
                    $add_data['location']               = $this->input->post('location');
                    $add_data['commercial_terms']       = $this->input->post('commercial_terms');
                    $add_data['other_details_check']    = $this->input->post('other_details_check');
                    $add_data['terms_condition_check']  = $this->input->post('terms_condition_check');
                    $add_data['tags']                   = implode(",", $this->input->post('tags'));
                    $add_data['web_url']                = $this->input->post('web_url');

                    if ($this->input->post('commercial_terms') == 'Paid') {
                        $add_data['currency']   = $this->input->post('currency');
                        $add_data['cost_price'] = $this->input->post('cost_price');
                    } else {
                        $add_data['currency']   = '';
                        $add_data['cost_price'] = '';
                    }

                    $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                    if (isset($tag_other) && $tag_other != "") {
                        $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));

                         if ($mode=='Add') {
                            $this->check_permissions->StoreOtherData($tag_other, 'arai_resource_sharing_tags_master','tag_name');
                        }
                    }

                    $other_details = trim($this->security->xss_clean($this->input->post('other_details')));
                    if (isset($other_details) && $other_details != "") {
                        $add_data['other_details'] = trim($this->security->xss_clean($other_details));
                    } else { $add_data['other_details'] = '';}

                    $terms_condition = $this->input->post('terms_condition');
                    if (isset($terms_condition) && $terms_condition != "") {
                        $add_data['terms_condition'] = trim($this->security->xss_clean($terms_condition));
                    } else { $add_data['terms_condition'] = '';}

                    $location_other = trim($this->security->xss_clean($this->input->post('location_other')));
                    if (isset($location_other) && $location_other != "") {
                        $add_data['location_other'] = trim($this->security->xss_clean($location_other));
                    } else { $add_data['location_other'] = '';}

                    if ($mode == 'Add') {
                        $add_data['is_featured']  = 0;
                        $add_data['admin_status'] = 0;
                        $add_data['is_deleted']   = 0;
                        $add_data['is_block']     = 0;
                        $add_data['created_on']   = date('Y-m-d H:i:s');

                        $last_r_id          = $this->master_model->insertRecord('arai_resource_sharing', $add_data, true);
                        $up_data['id_disp'] = $r_disp_id = "TNRID-" . sprintf("%07d", $last_r_id);
                        $this->master_model->updateRecord('arai_resource_sharing', $up_data, array('r_id' => $last_r_id));

                        $email_info = $this->get_mail_data($last_r_id);

                        // START: MAIL TO USER
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_user_after_adding_resource');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[OWNER_NAME]',
                                '[CONTACT_PERSON_NAME]',
                                '[CONTACT_PERSON_EMAIL_ID]',
                                '[RESOURCE_ID]',
                                '[RESOURCE_NAME]',
                            ];
                            $rep_array = [
                                $email_info['OWNER_NAME'],
                                $this->input->post('owner_name'),
                                $this->input->post('owner_email'),
                                $email_info['RESOURCE_ID'],
                                $email_info['RESOURCE_NAME'],

                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['OWNER_EMAIL'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                        // END: MAIL TO USER

                        //START: MAIL TO ADMIN

                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_admin_after_adding_resource');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[OWNER_NAME]',
                                '[CONTACT_PERSON_NAME]',
                                '[CONTACT_PERSON_EMAIL_ID]',
                                '[RESOURCE_ID]',
                                '[RESOURCE_NAME]',
                            ];
                            $rep_array = [
                                $email_info['OWNER_NAME'],
                                $this->input->post('owner_name'),
                                $this->input->post('owner_email'),
                                $email_info['RESOURCE_ID'],
                                $email_info['RESOURCE_NAME'],

                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['admin_email'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }

                        //END: MAIL TO ADMIN

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Add Resource",
                            'module_name' => 'Resource Front',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Resource is sent for admin approval. Your  ' . $r_disp_id);
                    } else if ($mode == 'Update') {
                        $add_data['admin_status'] = 0;
                        $add_data['updated_on']   = date('Y-m-d H:i:s');

                        $r_id      = $id;
                        $last_r_id = $id;

                        $this->master_model->updateRecord('arai_resource_sharing', $add_data, array('r_id' => $r_id));
                        //echo $this->db->last_query(); exit;

                        $email_info = $this->get_mail_data($r_id);

                        // START: MAIL TO USER ON UPDATE
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_user_after_updating_resource');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[OWNER_NAME]',
                                '[CONTACT_PERSON_NAME]',
                                '[CONTACT_PERSON_EMAIL_ID]',
                                '[RESOURCE_ID]',
                                '[RESOURCE_NAME]',
                                '[LINK_OF_RESOURCE_INFORMATION]',
                            ];
                            $rep_array = [
                                $email_info['OWNER_NAME'],
                                $this->input->post('owner_name'),
                                $this->input->post('owner_email'),
                                $email_info['RESOURCE_ID'],
                                $email_info['RESOURCE_NAME'],
                                $email_info['hyperlink_res_detail'],
                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['OWNER_EMAIL'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                        // END: MAIL TO USER ON UPDATE

                        //START: MAIL TO ADMIN

                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_admin_after_updating_resource');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[OWNER_NAME]',
                                '[CONTACT_PERSON_NAME]',
                                '[CONTACT_PERSON_EMAIL_ID]',
                                '[RESOURCE_ID]',
                                '[RESOURCE_NAME]',
                                '[LINK_OF_RESOURCE_INFORMATION]',
                            ];
                            $rep_array = [
                                $email_info['OWNER_NAME'],
                                $this->input->post('owner_name'),
                                $this->input->post('owner_email'),
                                $email_info['RESOURCE_ID'],
                                $email_info['RESOURCE_NAME'],
                                $email_info['hyperlink_res_detail'],

                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['admin_email'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }

                        //END: MAIL TO ADMIN

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Update Resource",
                            'module_name' => 'Resource Sharing Front',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Resource has been successfully updated');
                    }

                    //START : INSERT  FILES
                    if ($last_r_id != "") {
                        $r_files = $_FILES['r_files'];
                        if (count($r_files) > 0) {
                            for ($i = 0; $i < count($r_files['name']); $i++) {
                                if ($r_files['name'][$i] != '') {
                                    $kr_file = $this->Common_model_sm->upload_single_file('r_files', array('png', 'jpg', 'jpeg'), "resource_file_" . $last_r_id . "_" . date("YmdHis") . rand(), "./uploads/resource_files", "png|jpeg|jpg", '1', $i);

                                    if ($kr_file['response'] == 'error') {} else if ($kr_file['response'] == 'success') {
                                        $add_file_data['user_id']    = $user_id;
                                        $add_file_data['r_id']       = $last_r_id;
                                        $add_file_data['file_name']  = ($kr_file['message']);
                                        $add_file_data['created_on'] = date("Y-m-d H:i:s");
                                        $this->master_model->insertRecord('arai_resource_sharing_files', $add_file_data, true);
                                        // $r_files_data[] = $add_file_data; //FOR LOG
                                    }
                                }
                            }
                        }
                    }
                    //END : INSERT  FILES

                    redirect(site_url('resource_sharing/my_resource'));
                }
            }
            // echo validation_errors();die;
        }

        // $this->db->order_by('city_name', 'ASC');
        $this->db->order_by('FIELD(city_name, "Other") ASC, city_name ASC');
        $data['city_data'] = $this->master_model->getRecords("cities");

       $this->db->order_by('FIELD(tag_name, "Other") ASC, tag_name ASC');
        $data['tag_data'] = $this->master_model->getRecords("resource_sharing_tags_master", array("status" => 'Active'));

        $data['country_codes'] = $this->master_model->getRecords('country', '', '', array('iso' => 'asc'));

        $data['currency'] = $this->master_model->getRecords('arai_currency', '', '', array('currency' => 'asc'));

        $data['page_title']     = 'Resource Sharing';
        $data['submodule_name'] = '';

        $data['middle_content'] = 'resource_sharing/add';
        $this->load->view('front/front_combo', $data);
    }

    public function delete_rs_file_ajax()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $file_id       = $encrptopenssl->decrypt($this->input->post('file_id', true));

        $csrf_new_token           = $this->security->get_csrf_hash();
        $result['csrf_new_token'] = $csrf_new_token;

        $this->master_model->updateRecord('arai_resource_sharing_files', array('is_deleted' => '1'), array('file_id' => $file_id));
        $result['flag']    = 'success';
        $result['file_id'] = $file_id;

        //START : INSERT LOG
        $postArr                   = $this->input->post();
        $json_encode_data          = json_encode($postArr);
        $logDetails['user_id']     = $this->login_user_id;
        $logDetails['module_name'] = 'Resource_sharing : delete_team_file_ajax';
        $logDetails['store_data']  = $json_encode_data;
        $logDetails['ip_address']  = $this->get_client_ip();
        $logDetails['createdAt']   = date('Y-m-d H:i:s');
        $logDetails['action_name'] = 'Delete KR File';
        $logData                   = $this->master_model->insertRecord('logs', $logDetails);
        //END : INSERT LOG

        echo json_encode($result);
    }

    public function get_autor_details() // USED TO GET AUTHOR DETAILS ON ADD/EDIT BLOG PAGE

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $login_user_id = $this->session->userdata('user_id');
            $this->db->select('r.user_id, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, sp.employement_status, sp.other_emp_status');
            $this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', false);
            $user_data = $this->master_model->getRecords("arai_registration r", array('r.user_id' => $login_user_id));
            //$result['qry'] = $this->db->last_query();

            $result['flag'] = "success";
            $author_name    = $professional_status    = $org_name    = '';

            if (count($user_data) > 0) {
                $title       = $encrptopenssl->decrypt($user_data[0]['title']);
                $first_name  = $encrptopenssl->decrypt($user_data[0]['first_name']);
                $middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
                $last_name   = $encrptopenssl->decrypt($user_data[0]['last_name']);

                $author_name = $title . " " . $first_name;
                if ($middle_name != '') {$author_name .= " " . $middle_name . " ";}
                $author_name .= $last_name;

                if ($user_data[0]['user_category_id'] == 2) //Organization
                {
                    $professional_status = '';
                    $org_name            = $encrptopenssl->decrypt($user_data[0]['institution_full_name']);
                } else {
                    $professional_status = $encrptopenssl->decrypt($user_data[0]['employement_status']);
                }
            }

            $result['author_name']         = $author_name;
            $result['professional_status'] = $professional_status;
            $result['org_name']            = $org_name;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function block_unblock_kr() // BLOCK/UNBLOCK BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $r_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $type           = trim($this->security->xss_clean($this->input->post('type')));
            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";

            if ($type == 'block') {
                //START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
                /*$email_info = $this->get_mail_data_blog($r_id);

                $email_send='';
                $slug = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content = '';

                if(count($subscriber_mail) > 0)
                {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                '[Blog_Title]'
                ];
                $rep_array = [
                $email_info['Blog_Title']
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array=array(
                'to'        =>    $email_info['admin_email'],
                'cc'        =>    '',
                'from'        =>    $fromadmin,
                'subject'    =>     $subject_title,
                'view'        =>  'common-file'
                );

                $other_infoarray    =    array('content' => $sub_content);
                $email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
                } */
                //END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked

                $up_data['is_block'] = 1;

            } else if ($type == 'unblock') {$up_data['is_block'] = 0;}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('resource_sharing', $up_data, array('r_id' => $r_id));

            // echo $this->db->last_query();die;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Block Resource",
                'module_name' => 'Resource Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function delete_resource() // DELETE BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $r_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";
            $result['r_id'] = $r_id;

            $del_data['is_deleted'] = 1;
            $del_data['deleted_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('resource_sharing', $del_data, array('r_id' => $r_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete Resource",
                'module_name' => 'Resource Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success_blog_del_msg', 'Resource sharing has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_ajax() // LIKE - UNLIKE BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $r_id    = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag    = trim($this->security->xss_clean($this->input->post('flag')));
            $is_feed = @$this->input->post('is_feed');

            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";
            $result['r_id'] = $r_id;

            if ($flag == 1) {
                $action_name            = "Like Resource";
                $add_like['r_id']       = $r_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_resource_sharing_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';
            } else {
                $this->db->where('r_id', $r_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_resource_sharing_likes');

                $action_name = "Un-Like Resource";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';
            }
            $getTotalLikes         = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $r_id), 'like_id');
            $result['total_likes'] = $getTotalLikes;
            if ($is_feed) {
                $onclick_fun = "like_unlike_resource('" . base64_encode($r_id) . "','" . $LikeFlag . "')";
            } else {
                $onclick_fun = "like_unlike('" . base64_encode($r_id) . "','" . $LikeFlag . "')";
            }

            $result['response']         = '<span style="cursor:pointer;" onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . '</span>';
            $result['total_likes_html'] = '<span style="cursor:auto">' . $getTotalLikes . '</span>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'KR Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_comment_ajax() // LIKE - UNLIKE COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag                 = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $comment_data   = $this->master_model->getRecords("arai_blog_comments", array('comment_id' => $comment_id), 'total_likes', '', 0, 1);
            $new_total_like = $comment_data[0]['total_likes'];
            if ($flag == 1) {
                $action_name            = "Like Comment Technology Wall";
                $add_like['comment_id'] = $comment_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_blog_comment_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';

                $new_total_like = $new_total_like + 1;
            } else {
                $this->db->where('comment_id', $comment_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_blog_comment_likes');

                $action_name = "Un-Like Comment Technology Wall";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';

                $new_total_like = $new_total_like - 1;
            }

            $up_data['total_likes'] = $new_total_like;
            $this->master_model->updateRecord('arai_blog_comments', $up_data, array('comment_id' => $comment_id));

            $onclick_fun        = "like_dislike_comment('" . base64_encode($comment_id) . "','" . $LikeFlag . "')";
            $result['response'] = '<p class="comment_like_btn" onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $new_total_like . ')' . '</p>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_resource_likes_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $r_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag'] = "success";
            $result['r_id'] = $r_id;

            $this->db->order_by('r_id', 'DESC', false);
            $select = "bl.like_id, bl.r_id, bl.user_id, bl.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_resource_sharing_likes bl", array('bl.r_id' => $r_id), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/resource_sharing/incBlogLikesModal', $data, true);

            $kr_data              = $this->master_model->getRecords("resource_sharing", array('r_id' => $r_id), "resouce_name");
            $result['blog_title'] = "Resource Likes : " . $kr_data[0]['resouce_name'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_kr_downloads_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']  = "success";
            $result['kr_id'] = $kr_id;

            $this->db->order_by('kr_id', 'DESC', false);
            $select = "bl.d_id, bl.kr_id, bl.user_id, bl.createdAt, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_knowledge_download_log bl", array('bl.kr_id' => $kr_id), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/resource_sharing/incKrDownloadsModal', $data, true);

            $kr_data              = $this->master_model->getRecords("knowledge_repository", array('kr_id' => $kr_id), "title_of_the_content");
            $result['blog_title'] = "Resource Downloads : " . $kr_data[0]['title_of_the_content'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function my_resource_applications() // SHOW REPORTED COUNT

    {

        $user_id = $this->session->userdata('user_id');

        $this->db->order_by('br.r_id', 'DESC', false);

        $this->db->join('arai_resource_sharing r', 'r.r_id = br.r_id', 'INNER', false);
        $data['applications'] = $applications = $this->master_model->getRecords("arai_resource_sharing_connect_request br", array('br.user_id' => $user_id));
        // echo "<pre>";print_r($applications);die;
        $data['page_title']     = 'Resource sharing';
        $data['middle_content'] = 'resource_sharing/my_applications_listing';
        $this->load->view('front/front_combo', $data);

    }

    public function show_resource_requests_ajax() // SHOW REPORTED COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $r_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag'] = "success";
            $result['r_id'] = $r_id;

            $this->db->order_by('r_id', 'DESC', false);
            $select = "br.connect_id,br.app_id, br.r_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = br.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_resource_sharing_connect_request br", array('br.r_id' => $r_id), $select);
            $result['response'] = $this->load->view('front/resource_sharing/incBlogReportedModal', $data, true);

            $blog_data            = $this->master_model->getRecords("resource_sharing", array('r_id' => $r_id), "resouce_name");
            $result['blog_title'] = "Resource applications : " . $blog_data[0]['resouce_name'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getread_more_descriptionAjax() // GET BLOG DESCRIPTION DETAIL

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id   = base64_decode(trim($this->security->xss_clean($this->input->post('blog_id'))));
            $blog_data = $this->master_model->getRecords("arai_blog", array("blog_id" => $blog_id), 'blog_title, blog_description');

            $result['flag']             = "success";
            $result['blog_title']       = $blog_data[0]['blog_title'];
            $result['blog_description'] = htmlspecialchars_decode($blog_data[0]['blog_description']);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getBlogActiondata($user_id = 0)
    {
        $user_like_blog = $this->master_model->getRecords("arai_resource_sharing_likes", array('user_id' => $user_id), 'r_id');

        $like_arr = $self_blog_arr = $reported_arr = $return_arr = array();
        if (count($user_like_blog) > 0) {
            foreach ($user_like_blog as $res) {
                $like_arr[$res['r_id']] = $res['r_id'];
            }
        }

        $user_self_blog = $this->master_model->getRecords("arai_blog", array('user_id' => $this->session->userdata('user_id')), 'blog_id');
        if (count($user_self_blog) > 0) {
            foreach ($user_self_blog as $res) {
                $self_blog_arr[$res['blog_id']] = $res['blog_id'];
            }
        }

        $user_reported_blog = $this->master_model->getRecords("arai_resource_sharing_connect_request", array('user_id' => $user_id), 'r_id');
        if (count($user_reported_blog) > 0) {
            foreach ($user_reported_blog as $res) {
                $reported_arr[$res['r_id']] = $res['r_id'];
            }
        }

        $return_arr['like_blog']     = $like_arr;
        $return_arr['self_blog']     = $self_blog_arr;
        $return_arr['reported_blog'] = $reported_arr;

        return $return_arr;
    }

    public function getBlogCommentsActiondata($user_id = 0)
    {
        $user_like_comment = $this->master_model->getRecords("arai_blog_comment_likes", array('user_id' => $user_id), 'comment_id');

        $like_arr = $self_comment_arr = $return_arr = array();
        if (count($user_like_comment) > 0) {
            foreach ($user_like_comment as $res) {
                $like_arr[$res['comment_id']] = $res['comment_id'];
            }
        }

        $user_self_comment = $this->master_model->getRecords("arai_blog_comments", array('user_id' => $this->session->userdata('user_id')), 'comment_id');
        if (count($user_self_comment) > 0) {
            foreach ($user_self_comment as $res) {
                $self_comment_arr[$res['comment_id']] = $res['comment_id'];
            }
        }
        $return_arr['like_comments'] = $like_arr;
        $return_arr['self_comments'] = $self_comment_arr;

        return $return_arr;
    }

    public function details($r_id = 0, $comment_flag = 0) //BLOG DETAILS PAGE

    {
        $module_id = 34;
        $this->check_permissions->is_authorise($module_id);

        $encrptopenssl   = new Opensslencryptdecrypt();
        $user_id         = $this->session->userdata('user_id');
        $admin_user_id   = $this->session->userdata('admin_id');
        $data['user_id'] = $user_id;

        if ($r_id == '0') {redirect(site_url('resource_sharing'));} else {
            $r_id = base64_decode($r_id);

            $data['r_id'] = $r_id;

            $select_f = "r.*,c.*,cr.iso,cr.phonecode,cur.currency, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags";

            $this->db->join('arai_currency cur', 'cur.c_id=r.currency', 'LEFT', false);
            $this->db->join('arai_country cr', 'cr.id=r.country_code', 'LEFT', false);
            $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
            $data['form_data'] = $form_data = $this->master_model->getRecords("arai_resource_sharing r", array('r.r_id' => $r_id, 'r.is_deleted' => '0'), $select_f);

            if (count($form_data) == 0) {
                redirect(base_url('resource_sharing'));
            }

            // echo "<pre>"; print_r($data); echo "</pre>";die;

            $redirect_flag = 0;

            if (count($form_data) == 0) {$redirect_flag = 1;} else if ($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;} else if ($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;}

            if ($admin_user_id != '') {$redirect_flag = 0;}

            if ($redirect_flag == 1) {redirect(site_url('resource_sharing'));}
        }

        $data['files'] = $this->master_model->getRecords('arai_resource_sharing_files', array("r_id" => $r_id, "is_deleted" => '0'), 'file_name,file_id');

        $login_user_arr = $this->get_login_user_data();

        $data['BlogActiondata']   = $this->getBlogActiondata($user_id);
        $data['page_title']       = 'Resource Details';
        $data['login_user_name']  = $login_user_arr['loggedin_user_name'];
        $data['login_user_email'] = $login_user_arr['loggedin_user_email'];
        // echo "<pre>";print_r($data);die;
        $data['submodule_name'] = '';
        $data['middle_content'] = 'resource_sharing/details';
        $this->load->view('front/front_combo', $data);
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function connect_resource_ajax() // REPORT BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id                  = trim($this->security->xss_clean(base64_decode($this->input->post('kr_id'))));
            $popupBlogReportComment = trim($this->security->xss_clean($this->input->post('popupBlogReportComment')));
            $user_id                = $this->session->userdata('user_id');
            $result['flag']         = "success";
            $result['kr_id']        = $kr_id;

            $app_id = '';
            for ($i = 0; $i < 9; $i++) {
                $app_id .= mt_rand(0, 9);
            }

            $action_name                = "Connect Resource";
            $add_reported['r_id']       = $kr_id;
            $add_reported['app_id']     = $app_id;
            $add_reported['comments']   = $popupBlogReportComment;
            $add_reported['user_id']    = $user_id;
            $add_reported['created_on'] = date('Y-m-d H:i:s');
            $this->master_model->insertRecord('arai_resource_sharing_connect_request', $add_reported);

            $result['app_id'] = $app_id;

            if ($popupBlogReportComment == '') {
                $reason = 'NA';
            } else {
                $reason = $popupBlogReportComment;
            }

            $email_info = $this->get_mail_data($kr_id);

            // START: MAIL TO USER ON CONNECT REQUEST
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_user_after_connect_request');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[APPLICANT_USER_NAME]',
                    '[APPLICANT_USER_ID]',
                    '[RESOURCE_ID]',
                    '[RESOURCE_NAME]',
                    '[APPLICANT_EMAILID]',
                    '[LINK_OF_RESOURCE_POST]',
                    '[PURPOSE]',
                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['loggedin_user_name'],
                    $email_info['loggedin_user_id'],
                    $email_info['RESOURCE_ID'],
                    $email_info['RESOURCE_NAME'],
                    $email_info['loggedin_user_email'],
                    $email_info['hyperlink_res_detail'],
                    $reason,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );
                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            // END: MAIL TO USER ON CONNECT REQUEST

            // START: MAIL TO APPLICANR ON CONNECT REQUEST
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('resource_sharing_mail_to_applicant_after_connect_request');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $my_app_link                 = site_url('resource_sharing/my_resource_applications/');
                $hyperlink_to_my_application = '<a href=' . $my_app_link . ' target="_blank">here</a>';

                $arr_words = [
                    '[APPLICANT_NAME]',
                    '[OWNER_NAME]',
                    '[RESOURCE_ID]',
                    '[RESOURCE_NAME]',
                    '[LINK_OF_APPLICATION]',
                    '[PURPOSE]',
                ];
                $rep_array = [
                    $email_info['loggedin_user_name'],
                    $email_info['OWNER_NAME'],
                    $email_info['RESOURCE_ID'],
                    $email_info['RESOURCE_NAME'],
                    $hyperlink_to_my_application,
                    $reason,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['loggedin_user_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO APPLICANR ON CONNECT REQUEST

            $Reported_label = 'Request Already Sent';
            $ReportedFlag   = '0';

            $onclick_fun        = "connect_resource('" . base64_encode($kr_id) . "','" . $ReportedFlag . "', '', '')";
            $result['response'] = '<button style="margin-top:0px !important;" class="btn btn-primary float-right" href="javascript:void(0)" title="' . $Reported_label . '" onclick="' . $onclick_fun . '">Connect</button>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Resource Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_mail_data($r_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,id_disp,resouce_name,resource_sharing.created_on,registration.user_id');
        $this->db->join('registration', 'resource_sharing.user_id=registration.user_id', 'LEFT');
        $r_details = $this->master_model->getRecords('resource_sharing', array('r_id' => $r_id));

        if (count($r_details)) {
            $user_arr = array();
            if (count($r_details)) {
                foreach ($r_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $OWNER_NAME          = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $OWNER_ID            = $user_arr[0]['user_id'];
            $OWNER_EMAIL         = $user_arr[0]['email'];
            $RESOURCE_NAME       = $r_details[0]['resouce_name'];
            $RESOURCE_ID         = $r_details[0]['id_disp'];
            $DATE                = date('d/m/Y', strtotime($r_details[0]['created_on']));
            $res_detail_link     = site_url('resource_sharing/details/') . base64_encode($r_id);
            $hyperlink_kr_detail = '<a href=' . $res_detail_link . ' target="_blank">here</a>';

            $email_array = array(
                'OWNER_NAME'           => $OWNER_NAME,
                'OWNER_EMAIL'          => $OWNER_EMAIL,
                'OWNER_ID'             => $OWNER_ID,
                'RESOURCE_NAME'        => $RESOURCE_NAME,
                'RESOURCE_ID'          => $RESOURCE_ID,
                'DATE'                 => $DATE,
                'admin_email'          => 'test@esds.co.in',
                'hyperlink_res_detail' => $hyperlink_kr_detail,
            );

            $this->db->select('user_id,title,first_name,middle_name,last_name,email');
            $loggedin_user_data = $this->master_model->getRecords("registration", array('user_id' => $this->session->userdata('user_id')));

            $loggedin_user_arr = array();
            if (count($loggedin_user_data)) {
                foreach ($loggedin_user_data as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $loggedin_user_arr[]    = $row_val;
                }
            }

            $loggedin_user_name                 = $loggedin_user_arr[0]['title'] . " " . $loggedin_user_arr[0]['first_name'] . " " . $loggedin_user_arr[0]['last_name'];
            $email_array['loggedin_user_name']  = $loggedin_user_name;
            $email_array['loggedin_user_email'] = $loggedin_user_arr[0]['email'];
            $email_array['loggedin_user_id']    = $loggedin_user_arr[0]['user_id'];
            return $email_array;
            // echo "<pre>";
            // print_r($email_array);
        }
    }

    public function get_login_user_data()
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('user_id,title,first_name,middle_name,last_name,email');
        $loggedin_user_data = $this->master_model->getRecords("registration", array('user_id' => $this->session->userdata('user_id')));

        $loggedin_user_arr = array();
        if (count($loggedin_user_data)) {
            foreach ($loggedin_user_data as $row_val) {
                $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                $loggedin_user_arr[]    = $row_val;
            }
        }

        $loggedin_user_name                = $loggedin_user_arr[0]['title'] . " " . $loggedin_user_arr[0]['first_name'] . " " . $loggedin_user_arr[0]['last_name'];
        $user_array['loggedin_user_name']  = $loggedin_user_name;
        $user_array['loggedin_user_email'] = $loggedin_user_arr[0]['email'];
        $user_array['loggedin_user_id']    = $loggedin_user_arr[0]['user_id'];
        return $user_array;
    }

    public function getNumData($query) // CALCULATE NUM ROWS

    {
        //echo $query;
        return $rowCount = $this->db->query($query)->num_rows();
    }

    public function open_download_modal_ajax()
    {

        $data['kr_id'] = $kr_id = $this->input->post('kr_id');

        $select_f = "rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags, k.author_name, k.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture,k.admin_status,k.is_block";
        $this->db->join('arai_resource_sharing_type_master rt', 'rt.id = k.kr_type', 'LEFT', false);
        $this->db->join('arai_registration r', 'r.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_profile_organization org', 'org.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_student_profile sp', 'sp.user_id = k.user_id', 'LEFT', false);
        $data['form_data'] = $form_data = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.kr_id' => $kr_id, 'k.is_block' => '0'), $select_f);
        $data['image']     = $this->create_captcha();

        $files = $this->master_model->getRecords('knowledge_repository_files', array("kr_id" => $kr_id, "is_deleted" => '0'), 'file_name,file_id');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/kr_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }
        $data['files_size']  = $this->formatSizeUnits($totalSize);
        $data['files_count'] = count($files);
        $data['files']       = $files;

        $this->load->view('front/resource_sharing/download_form', $data);
    }

    public function download_kr($files_id, $kr_id)
    {

        $files_ids = $files_id;

        if ($files_ids == "") {
            redirect(base_url('knowledge_repository'));
        }
        $this->db->where("file_id IN (" . $files_ids . ")", null, false);
        $files = $this->master_model->getRecords('knowledge_repository_files', '', 'file_name,file_id');
        if (count($files)) {

            $kr_data = $this->master_model->getRecords('knowledge_repository', array("kr_id" => $kr_id, "is_deleted" => '0'));

            $disp_id = $kr_data[0]['kr_id_disp'];

            $user_id = $this->session->userdata('user_id');

            $insert_Arr = array(
                'kr_id'   => $kr_id,
                'user_id' => $user_id,
                'files'   => json_encode($files),
            );
            $this->master_model->insertRecord('arai_knowledge_download_log', $insert_Arr);

            $zip      = new ZipArchive();
            $zip_name = $disp_id . ".zip"; // Zip name
            $zip->open($zip_name, ZipArchive::CREATE);
            foreach ($files as $file) {
                $path = "uploads/kr_files/" . $file['file_name'];
                if (file_exists($path)) {
                    $zip->addFromString(basename($path), file_get_contents($path));
                }

            }
            $zip->close();
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . $zip_name . "\"");
            readfile($zip_name);
            unlink($zip_name);

        } else {
            redirect(base_url('knowledge_repository'));
        }

        // $file_name = 'hgt.pdf';
        //    $file_url = 'assets/' . $file_name;
        //    header('Content-Type: application/octet-stream');
        //    header("Content-Transfer-Encoding: Binary");
        //    header("Content-disposition: attachment; filename=\"".$file_name."\"");
        //    readfile($file_url);

    }

    public function create_captcha($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }

    public function check_captcha_ajax()
    {
        $code = $_POST['code'];

        if ($code == '' || $_SESSION["dwcaptcha"] != $code) {
            //$this->session->unset_userdata("dwcaptcha");
            //$this->form_validation->set_message('check_captcha', 'Invalid %s.');
            echo "false";
        }
        if ($_SESSION["dwcaptcha"] == $code && $this->session->userdata("used") == 0) {
            //$this->session->unset_userdata("dwcaptcha");
            echo "true";
        }

    }

    public function refresh()
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 17,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Display captcha image
        echo $captcha['image'];
    }

    // Tool Connect Popup Window
    public function open_tool_connect()
    {
        $this->load->view('front/resource_sharing/open_tool_connect_form');
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
