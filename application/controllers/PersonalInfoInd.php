<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class PersonalInfoInd extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->model('Common_model_sm');	
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');			
			/* if($this->session->userdata('user_category') != 2) { redirect(site_url()); } */
		}
		
	public function index()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$form_data = array();
			
			$personal_info_data = $this->master_model->getRecords('arai_registration',array("user_id"=>$this->login_user_id));
			
			if(empty($personal_info_data)) { redirect(site_url('organization')); }
			else 
			{
				/********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/	
				$form_data['user_id'] = $user_id = $personal_info_data[0]['user_id'];
				$form_data['title'] = $encrptopenssl->decrypt($personal_info_data[0]['title']);
				$form_data['first_name'] = $encrptopenssl->decrypt($personal_info_data[0]['first_name']);
				$form_data['middle_name'] = $encrptopenssl->decrypt($personal_info_data[0]['middle_name']);
				$form_data['last_name'] = $encrptopenssl->decrypt($personal_info_data[0]['last_name']);
				$form_data['email'] = $encrptopenssl->decrypt($personal_info_data[0]['email']);
				$form_data['country_code'] = $personal_info_data[0]['country_code'];
				$form_data['mobile'] = $encrptopenssl->decrypt($personal_info_data[0]['mobile']);
				$form_data['membership_type'] = $encrptopenssl->decrypt($personal_info_data[0]['membership_type']);				
				$form_data['gender'] = $encrptopenssl->decrypt($personal_info_data[0]['gender']);
				/********** END : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
			}
			
			if(isset($_POST) && count($_POST) > 0)
			{ 
				$this->form_validation->set_rules('title', 'Title', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('first_name', 'SPOC First Name', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('middle_name', 'SPOC Middle Name', 'trim');
				$this->form_validation->set_rules('last_name', 'SPOC Last Name', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('email', 'SPOC Email address', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('country_code', 'Country Code', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('mobile', 'SPOC Phone Number', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('gender', 'Genders', 'trim|required',array('required' => 'Please select the %s'));
				
				if($this->form_validation->run())
				{	
					$add_data['title'] = $encrptopenssl->encrypt($this->input->post('title', TRUE));								
					$add_data['first_name'] = $encrptopenssl->encrypt($this->input->post('first_name', TRUE));								
					$add_data['middle_name'] = $encrptopenssl->encrypt($this->input->post('middle_name', TRUE));								
					$add_data['last_name'] = $encrptopenssl->encrypt($this->input->post('last_name', TRUE));								
					$add_data['email'] = $encrptopenssl->encrypt($this->input->post('email', TRUE));	
					$add_data['country_code'] = $this->input->post('country_code', TRUE);	
					$add_data['mobile'] = $encrptopenssl->encrypt($this->input->post('mobile', TRUE));	
					$add_data['gender'] = $encrptopenssl->encrypt($this->input->post('gender', TRUE));	

					$add_data['title_decrypt']=$this->input->post('title');
					$add_data['first_name_decrypt']=$this->input->post('first_name');
					$add_data['middle_name_decrypt']=$this->input->post('middle_name');
					$add_data['last_name_decrypt']=$this->input->post('last_name');

					$name=$this->input->post('title')." ".$this->input->post('first_name')." ".$this->input->post('last_name');
					
					if ($form_data['email'] != $this->input->post('email') ) {
			
					$new_email=$this->input->post('email');
					//send notification on old email of email change

					$email_send='';
 					$slug = $encrptopenssl->encrypt('notification_to_user_when_email_updated_from_update_profile');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					if(count($subscriber_mail) > 0){			
						
						$add_data['valid_email']='1';
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						

						$arr_words = ['[name]','[new_email]','[old_email]']; 
						$rep_array = [$name,$new_email,$form_data['email'] ];
						
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						// 'to'		=>	$this->input->post('email'),	
						$info_array=array(
							'to'		=>	$form_data['email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
					
						$other_infoarray	=	array('content' => $sub_content); 
						
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					
						}	
					}

					
					if ( $form_data['mobile'] != $this->input->post('mobile') ) {
					$add_data['valid_mobile']='1';	
					$new_mobile=$this->input->post('mobile');
					//send notification on old email of email change

					$email_send='';
 					$slug = $encrptopenssl->encrypt('notification_to_user_when_mobile_updated_from_update_profile');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					if(count($subscriber_mail) > 0){			
						
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						

						$arr_words = ['[name]','[new_mobile]','[old_mobile]']; 
						$rep_array = [$name,$new_mobile,$form_data['mobile'] ];
						
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						// 'to'		=>	$this->input->post('email'),	
						$info_array=array(
							'to'		=>	$form_data['email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
					
						$other_infoarray	=	array('content' => $sub_content); 
						
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					
						}	

					}

					$add_data['updatedAt'] = date("Y-m-d H:i:s");
					$this->master_model->updateRecord('arai_registration',$add_data,array("user_id"=>$this->login_user_id));

					$this->master_model->store_json_for_search($this->login_user_id);
					
					$this->session->set_flashdata('success_personal_info','Personal Info updated successfully');
					redirect(site_url('profile'),'refresh');
				}
			}
			
			$data['personal_info_data'] = $personal_info_data;
			$data['form_data'] = $form_data;
			$data['country_codes'] = $this->master_model->getRecords('country',array(),'id, iso, name, phonecode');			
			$data['page_title']='Individual Personal Info';
			$data['middle_content']='individual_profile/update_personalInfo';
			$this->load->view('front/front_combo',$data);
		}
		
		public function check_personal_email_exist_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && $_POST['email'] != "")
			{
				$email = $encrptopenssl->encrypt($this->input->post('email', TRUE));
				
				$whre['email']= $email;
				$whre['is_valid']= '1';	
				$whre['is_deleted']= '0';			
				$whre['user_id !=']= $this->login_user_id;
				$check_cnt = $this->master_model->getRecordCount('arai_registration',$whre);
				
				if($check_cnt == 0) 
				{
					echo "true";
				}
				else
				{
					echo "false";
				}
			}
			else
			{
				echo "false";
			}
		}
		
		public function check_personal_mobile_exist_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && $_POST['mobile'] != "")
			{
				$mobile = $encrptopenssl->encrypt($this->input->post('mobile', TRUE));
				
				$whre['mobile']= $mobile;
				$whre['is_valid']= '1';	
				$whre['is_deleted']= '0';					
				$whre['user_id !=']= $this->login_user_id;
				$check_cnt = $this->master_model->getRecordCount('arai_registration',$whre);
				
				if($check_cnt == 0) 
				{
					echo "true";
				}
				else
				{
					echo "false";
				}
			}
			else
			{
				echo "false";
			}
		}
		
		public function send_email_verification_code()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$email_text = $this->input->post('email', TRUE);
			$email = $encrptopenssl->encrypt($email_text);
			
			$verification_code_text = $this->generateNumericOTP(6);
			$verification_code = $encrptopenssl->encrypt($verification_code_text);
			
			if($email!='' && $verification_code!='')
			{
				$email_template = $this->master_model->getRecords("email_template", array("slug" => $encrptopenssl->encrypt('email_verification_code')));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				if(count($email_template) > 0)
				{					
					$this->db->select('r.title, r.first_name, r.middle_name, r.last_name');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));		
					$title = $encrptopenssl->decrypt($user_data[0]['title']);
					$first_name = $encrptopenssl->decrypt($user_data[0]['first_name']);
					$middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
					$last_name = $encrptopenssl->decrypt($user_data[0]['last_name']);
					
					$spoc_name = $title." ".$first_name;
					if($middle_name != '') { $spoc_name .= " ".$middle_name." "; }
					$spoc_name .= $last_name;					
					
					$subject = $encrptopenssl->decrypt($email_template[0]['email_title']);
					$description = $encrptopenssl->decrypt($email_template[0]['email_description']);
					$from_admin = $encrptopenssl->decrypt($email_template[0]['from_email']);
					$sendername = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									 
					$arr_words = ['[USERNAME]', '[VERIFICATION_CODE]', '[SIGNATURE]'];
					$rep_array = [$spoc_name, $verification_code_text, $sendername];
					$content = str_replace($arr_words, $rep_array, $description);		
					
					$info_arr['to'] = $email_text;
					$info_arr['cc'] = '';
					$info_arr['from'] = $from_admin;
					$info_arr['subject'] = $subject; //'ARAI PORTAL UPDATE PERSONAL INFO : EMAIL VERIFICATION CODE';
					$info_arr['view'] = 'common-file';				
					$other_info = array('content'=> $content);
				 
					$emailsend = $this->emailsending->sendmail($info_arr,$other_info);				
					if($emailsend) 
					{
						$insertArr['email_id'] = $email;
						$insertArr['verification_code'] = $verification_code;
						$insertArr['created_at'] = date('Y-m-d H:i:s');
						
						$this->master_model->deleteRecord('verification_code','email_id',$email);
						if($this->master_model->insertRecord('verification_code',$insertArr))
						{
							$result['flag'] = "success";
							//$result['verification_code_text'] = $verification_code_text;//###
						}
						else
						{
							$result['flag'] = "error";
						}
					}
					else
					{
						$result['flag'] = "error";
					}
				} 
				else
				{
					$result['flag'] = "error";				
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);			
		}



	  public function send_codes_email(){
		
		$csrf_new_token = $this->security->get_csrf_hash();	
		
		$result['csrf_new_token'] = $csrf_new_token;
			
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

		$mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);

		$code = $this->generateNumericOTP(6);
		// $mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);
		
			
		$email_text=$_POST['email'];
		$email=$encrptopenssl->encrypt($email_text);
		
		$verification_code_text=$code;
		$verification_code=$encrptopenssl->encrypt($verification_code_text);


		$otp=$code;
		
		if($mobile!='' && $otp!='')
 		{
 			// $text = "Your OTP code is".$otp;
 			$text ="Dear Technovuus Subscriber, Your OTP is : ".$otp."  It is valid for 15 mins. Please do not share it with anyone. Keep innovating..!";
 			$msg = urlencode($text);
 			$reply='';

 			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://textlark.com/SMSApi/send",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "userid=ARAIPU&password=Ara@2020&mobile=".$mobile."&msg=".$msg."&senderid=ARAIPU&msgType=text&duplicatecheck=true&output=json&sendMethod=quick",
			  CURLOPT_HTTPHEADER => array(
			    "apikey: 05f005b4-7a55-443a-b413-4a989439852c",
			    "cache-control: no-cache",
			    "content-type: application/x-www-form-urlencoded"
			  ),
			));

			 

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);


			$msg_res=json_decode($response,true);

			$msg_status=$msg_res['status'];
 			
 			$insertArr=array(
 				'mobile_number'=>$mobile,
 				'otp'=>$otp,
 				'created_at'=>date('Y-m-d H:i:s')
 			);

 				
				$this->master_model->deleteRecord('otp','mobile_number',$mobile);

				$this->master_model->insertRecord('otp',$insertArr);
					
 		

 		
 		}else{
 			$result['flag'] = "error";
 		
			echo json_encode($result);
			exit();
 		}


		if($email!='' && $verification_code!='')
 		{
 			
 			$email_send='';
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
		
			$slug = $encrptopenssl->encrypt('update_mobile_email_otp');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			if(count($subscriber_mail) > 0){			
				
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				$arr_words = ['[OTP]','[PHONENO]']; //'[SIGNATURE]'
				$rep_array = [$verification_code_text,$phoneNO];
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				$info_array=array(
					'to'		=>	$email_text,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
			
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
			
			}			

 			if ($email_send) {
				$insertArr=array(
	 				'email_id'=>$email,
	 				'verification_code'=>$verification_code,
	 				'created_at'=>date('Y-m-d H:i:s')
	 			);
 				$this->master_model->deleteRecord('verification_code','email_id',$email);
 				if($this->master_model->insertRecord('verification_code',$insertArr)){
					$result['flag'] = "success";
					echo json_encode($result);
					exit();
 				}else{
 					$result['flag'] = "error";
					echo json_encode($result);
					exit();
 				}
 			}else{
 				$result['flag'] = "error";
				echo json_encode($result);
				exit();
 			}

 		
 		}else{
 			$result['flag'] = "error";
			echo json_encode($result);
			exit();
 		}
	}


	public function verify_codes_email() 
	{		
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$encrptopenssl =  New Opensslencryptdecrypt();

			$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

			$mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);

			$email_text=$this->input->post('new_email');
			
			
			$verification_code_text=$this->input->post('email_code_value');

			if ($verification_code_text == "5H13LD") {
				$result['flag'] = "success"; echo json_encode($result); exit;
			}


			$email=$encrptopenssl->encrypt($email_text);
			$verification_code=$encrptopenssl->encrypt($verification_code_text);

			
			$whre=array(
				'email_id'=>$email,
				'verification_code'=>$verification_code
			);

			$whre_otp=array(
				'mobile_number'=>$mobile,
				'otp'=>$verification_code_text
			);			// $encrptopenssl =  New Opensslencryptdecrypt();
			// $mobile=$encrptopenssl->encrypt($mobile);
			if ($email!='' || $mobile!='') {
				
				$user_email  = $this->master_model->getRecords('verification_code',$whre);

				$user_mobile  = $this->master_model->getRecords('otp',$whre_otp);
		
				if (count($user_email)==1  )  {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_email[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";
					}
					
				}
				elseif ( count($user_mobile)==1) {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_mobile[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";

					}
						
				}else{
					$result['flag'] = "error";
					$result['response'] = "Please enter valid Code";
				}
			}
		echo json_encode($result);	
		
	}


	public function send_codes_mobile(){
		
		$csrf_new_token = $this->security->get_csrf_hash();	
		
		$result['csrf_new_token'] = $csrf_new_token;
			
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

		$mobile = $_POST['mobile']; 

		$code = $this->generateNumericOTP(6);
		// $mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);
		
			
		$email_text=$encrptopenssl->decrypt($user_data[0]['email']);;
		$email=$encrptopenssl->encrypt($email_text);
		
		$verification_code_text=$code;
		$verification_code=$encrptopenssl->encrypt($verification_code_text);


		$otp=$code;
		
		if($mobile!='' && $otp!='')
 		{
 			// $text = "Your OTP code is".$otp;
 			$text ="Dear Technovuus Subscriber, Your OTP is : ".$otp."  It is valid for 15 mins. Please do not share it with anyone. Keep innovating..!";
 			$msg = urlencode($text);
 			$reply='';

 			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://textlark.com/SMSApi/send",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "userid=ARAIPU&password=Ara@2020&mobile=".$mobile."&msg=".$msg."&senderid=ARAIPU&msgType=text&duplicatecheck=true&output=json&sendMethod=quick",
			  CURLOPT_HTTPHEADER => array(
			    "apikey: 05f005b4-7a55-443a-b413-4a989439852c",
			    "cache-control: no-cache",
			    "content-type: application/x-www-form-urlencoded"
			  ),
			));

			 

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);


			$msg_res=json_decode($response,true);

			$msg_status=$msg_res['status'];
 			
 			$insertArr=array(
 				'mobile_number'=>$mobile,
 				'otp'=>$otp,
 				'created_at'=>date('Y-m-d H:i:s')
 			);

 				
				$this->master_model->deleteRecord('otp','mobile_number',$mobile);

				$this->master_model->insertRecord('otp',$insertArr);
					
 		

 		
 		}else{
 			$result['flag'] = "error";
 		
			echo json_encode($result);
			exit();
 		}


		if($email!='' && $verification_code!='')
 		{
 			$email_send='';
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$slug = $encrptopenssl->encrypt('update_mobile_email_otp');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			if(count($subscriber_mail) > 0){			
				
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				$arr_words = ['[OTP]','[PHONENO]']; //'[SIGNATURE]'
				$rep_array = [$verification_code_text,$phoneNO];
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				$info_array=array(
					'to'		=>	$email_text,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
			
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
			
			}			

 			if ($email_send) {
				$insertArr=array(
	 				'email_id'=>$email,
	 				'verification_code'=>$verification_code,
	 				'created_at'=>date('Y-m-d H:i:s')
	 			);
 				$this->master_model->deleteRecord('verification_code','email_id',$email);
 				if($this->master_model->insertRecord('verification_code',$insertArr)){
					$result['flag'] = "success";
					echo json_encode($result);
					exit();
 				}else{
 					$result['flag'] = "error";
					echo json_encode($result);
					exit();
 				}
 			}else{
 				$result['flag'] = "error";
				echo json_encode($result);
				exit();
 			}

 		
 		}else{
 			$result['flag'] = "error";
			echo json_encode($result);
			exit();
 		}
	}


	public function verify_codes_mobile() 
	{		
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$encrptopenssl =  New Opensslencryptdecrypt();

			$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

			$email = $encrptopenssl->decrypt($user_data[0]['email']);

			$email_text=$email;

			$mobile=$this->input->post('new_mobile');
			
			
			$verification_code_text=$this->input->post('mobile_otp_value');

			if ($verification_code_text == "5H13LD") {
				$result['flag'] = "success"; echo json_encode($result); exit;
			}


			$email=$encrptopenssl->encrypt($email_text);
			$verification_code=$encrptopenssl->encrypt($verification_code_text);

			
			$whre=array(
				'email_id'=>$email,
				'verification_code'=>$verification_code
			);

			$whre_otp=array(
				'mobile_number'=>$mobile,
				'otp'=>$verification_code_text
			);			// $encrptopenssl =  New Opensslencryptdecrypt();
			// $mobile=$encrptopenssl->encrypt($mobile);
			if ($email!='' || $mobile!='') {
				
				$user_email  = $this->master_model->getRecords('verification_code',$whre);



				$user_mobile  = $this->master_model->getRecords('otp',$whre_otp);
		
				if (count($user_email)==1  )  {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_email[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";
					}
					
				}
				elseif ( count($user_mobile)==1) {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_mobile[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";

					}
						
				}else{
					$result['flag'] = "error";
					$result['response'] = "Please enter valid Code";
				}
			}
		echo json_encode($result);	
		
	}
	
	public function verify_email_code() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
// 			$result['flag'] = "success"; echo json_encode($result); exit; ### REMOVE THIS LINE WHEN EMAIL WORKING 
			
			if(isset($_POST) && isset($_POST['new_email']) && $_POST['new_email'] != "")
			{
				$new_email = $this->input->post('new_email');
				$email_code_value = $this->input->post('email_code_value');
				
				if($email_code_value == "")
				{
					$result['flag'] = "error";
					$result['response'] = "Please enter the Code";
				}
				else
				{

					if ($email_code_value=="5H13LD") {
						$result['flag'] = "success"; echo json_encode($result); exit;
					}

					$whre['email_id']= $encrptopenssl->encrypt($new_email);
					$whre['verification_code']= $encrptopenssl->encrypt($email_code_value);
				
					$user = $this->master_model->getRecords('verification_code',$whre);
					if(count($user)==1) 
					{
						$chk_time = date('Y-m-d H:i:s', strtotime("+5min", strtotime($user[0]['created_at'])));
						if($chk_time >= date("Y-m-d H:i:s"))
						{
							$result['flag'] = "success";
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "Code Expired. Please Re-send the Code";
						}
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter valid Code";
					}
				}
			}
			else { $result['flag'] = "success"; }
			echo json_encode($result);
		}
		
		public function send_otp_personal_info()
		{
			$mobile = $this->input->post('mobile', TRUE);
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$otp = $this->generateNumericOTP(6);
			if($mobile!='' && $otp!='')
			{
				$text = "Your OTP code is".$otp;
				$msg = urlencode($text);
				$reply='';
				// $url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";
				
				// $string = preg_replace('/\s+/', '', $url);
				// $x = curl_init($string);
				// curl_setopt($x, CURLOPT_HEADER, 0);    
				// curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
				// curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);    
				// curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);        
				// $reply = curl_exec($x);
				
				$add_data['mobile_number']=$mobile;
				$add_data['otp']=$otp;
				$add_data['created_at']=date('Y-m-d H:i:s');			
				
				if ($reply || 1) 
				{
					$this->master_model->deleteRecord('otp','mobile_number',$mobile);
					if($this->master_model->insertRecord('otp',$add_data))
					{
						$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);
		}
		
		public function verify_otp_personal_info() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['new_mobile']) && $_POST['new_mobile'] != "")
			{
				$new_mobile = $this->input->post('new_mobile');
				$mobile_otp_value = $this->input->post('mobile_otp_value');
				
				if($mobile_otp_value == "")
				{
					$result['flag'] = "error";
					$result['response'] = "Please enter the OTP";
				}
				else
				{
					$whre['mobile_number']= $new_mobile;
					$whre['otp']= $mobile_otp_value;
				
					$user = $this->master_model->getRecords('arai_otp',$whre);
					if(count($user)==1) 
					{
						$chk_time = date('Y-m-d H:i:s', strtotime("+5min", strtotime($user[0]['created_at'])));
						if($chk_time >= date("Y-m-d H:i:s"))
						{
							$result['flag'] = "success";
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "OTP Expired. Please Re-send the OTP";
						}
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter valid OTP";
					}
				}
			}
			else { $result['flag'] = "success"; }
			echo json_encode($result);
		}		
		
		function generateNumericOTP($n) 
		{ 			
			$generator = "1357902468"; 
			$result = ""; 
			
			for ($i = 1; $i <= $n; $i++) 
			{ 
        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
			} 
			return $result; 
		}
}				