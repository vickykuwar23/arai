<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Delete_single_file extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			
		}
		
		function index()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$tbl_nm = $encrptopenssl->decrypt($this->input->post('tbl_nm', TRUE));	
			$pk_nm = $encrptopenssl->decrypt($this->input->post('pk_nm', TRUE));	
			$del_id = $encrptopenssl->decrypt($this->input->post('del_id', TRUE));	
			$input_nm = $this->input->post('input_nm', TRUE);
			$file_path = $encrptopenssl->decrypt($this->input->post('file_path', TRUE));	
						
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$response = $this->master_model->delete_single_file($tbl_nm, $pk_nm, $del_id, $input_nm, $file_path);
			$result['flag'] = $response['flag'];
			
			//START : INSERT LOG 
			$login_user_id = $this->session->userdata('user_id');						
			
			$postArr = $this->input->post();
			$json_encode_data 	= json_encode($postArr);										
			$logDetails['user_id'] = $login_user_id;
			$logDetails['module_name'] = 'Delete File : Delete_single_file';
			$logDetails['store_data'] = $json_encode_data;
			$logDetails['ip_address'] = $this->get_client_ip();
			$logDetails['createdAt'] = date('Y-m-d H:i:s');
			$logDetails['action_name'] = 'Delete File Common';
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			//END : INSERT LOG
			
			echo json_encode($result);
		}
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
				$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
				$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			   $ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
				$ipaddress = getenv('REMOTE_ADDR');
			else
				$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
}				