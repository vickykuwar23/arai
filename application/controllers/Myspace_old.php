<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Myspace_old extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        // $this->login_user_id = $this->session->userdata('user_id');
        // if($this->login_user_id == "") { redirect(site_url()); }

        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
    }

    function index(){

        $user_id = $this->session->userdata('user_id');
       
        $this->db->order_by('r.r_id','DESC',FALSE);
        $data['resource_sharing'] = $resource_sharing = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'user_id'=>$user_id) );

        $this->db->order_by('k.kr_id','DESC',FALSE);
        $data['knowledge_repository'] = $knowledge_repository = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'user_id'=>$user_id));

        $data['blogs'] = $blogs = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0',  'user_id'=>$user_id ));    
        
        // Most commneted blog
        $sql="SELECT arai_blog.user_id,blog_banner,arai_blog.blog_id,`blog_title`, max(totalcomments) as highest_comments FROM ( SELECT `blog_id` , count(`blog_id`) as totalcomments FROM arai_blog_comments where arai_blog_comments.status=1 GROUP BY `blog_id` ) as t LEFT JOIN arai_blog ON arai_blog.blog_id = t.blog_id WHERE  arai_blog.admin_status=1 AND arai_blog.is_deleted=0 AND arai_blog.is_block=0 AND arai_blog.user_id=".$user_id." ";
        $query = $this->db->query($sql);

        $data['most_commented_blog'] = $most_commented_blog= $query->result_array();
         // End most commneted blog

        $sql_like="SELECT arai_blog.user_id,blog_banner,arai_blog.blog_id,`blog_title`, max(total_likes) as highest_likes FROM ( SELECT `blog_id` , count(`blog_id`) as total_likes FROM arai_blogs_likes GROUP BY `blog_id` ) as t LEFT JOIN arai_blog ON arai_blog.blog_id = t.blog_id WHERE  arai_blog.admin_status=1 AND arai_blog.is_deleted=0 AND arai_blog.is_block=0 AND  arai_blog.user_id=".$user_id." ";
        $query_like = $this->db->query($sql_like);

        $data['most_likeed_blog'] = $most_likeed_blog= $query_like->result_array();
        // print_r($most_likeed_blog);die;
       

        $data['technology_transfer'] = $technology_transfer = $this->master_model->getRecords("arai_technology_transfer t",array('t.is_deleted' => '0','user_id'=>$user_id) );

        $this->db->order_by('qa.q_id','DESC',FALSE);
        $data['fqa_forum_data'] = $fqa_forum_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.is_deleted' => '0','user_id'=>$user_id) );

        $this->db->order_by('qc.comment_id','DESC',FALSE);
        $data['fqa_forum_answers_data'] = $fqa_forum_answers_data = $this->master_model->getRecords("arai_qa_forum_comments qc",array('user_id'=>$user_id) );

        $data['webinars'] = $webinars = $this->master_model->getRecords('arai_webinar w',array('user_id'=>$user_id));


        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/index';
        $this->load->view('front/front_combo', $data);
    }

}