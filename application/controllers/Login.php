<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller 
{
   function __construct()
	{
		 parent::__construct();
		 $this->load->library('googleplus'); 
				
	}
	public function index()
	{   
		if($this->session->userdata('user_id') != ""){			
			redirect(base_url());
			$this->load->helper('cookie');
		}

	  $data['loginURL'] = $this->googleplus->loginURL();
	   $ip = $this->get_client_ip();
 	   $encrptopenssl =  New Opensslencryptdecrypt();

	   
	   if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
			if($this->form_validation->run())
			{ 
				$email =$encrptopenssl->encrypt($this->input->post('email'));
				$password = 	$this->input->post('password'); 
				$input_array=array('email'=>$email,
								'password'=>$password,
								'is_valid'=>'1',
								'is_deleted'=>'0'

							);

			
				$user_info=$this->master_model->getRecords('registration',$input_array);
			     //echo $this->db->last_query();exit;
				$payment_status=$user_info[0]['payment_status'];
			    
			    if(is_array($user_info) && count($user_info)>0)
			    { 
			    	if ($user_info[0]['status']=='Active') {			    		# code...
			    		
		    		if ($user_info[0]['is_verified']=='yes') {

		    			if ($user_info[0]['membership_type']!='') {
		    				
		    				 $membership_type=$encrptopenssl->decrypt( $user_info[0]['membership_type'] );
		    				
		    				if ($membership_type=='Paid' && $payment_status=='pending') {
		    					redirect(base_url().'registration/payment/'.base64_encode($user_info[0]['user_id']));
								exit();
		    				}

		    			}

		    			if($user_info[0]['user_category_id']==2 && $payment_status=='pending'){
		    				redirect(base_url().'registration/payment/'.base64_encode($user_info[0]['user_id']));
							exit();
		    			}	



						$membership_type_dec =$encrptopenssl->decrypt($user_info[0]['membership_type']);
						
						if ($user_info[0]['user_category_id']==1 && $membership_type_dec=='Paid') {
							$whr=array(
							'sub_cat_id'=>1,
							'membership_type'=>'paid'
							);
						}else if ($user_info[0]['user_category_id']==1 && $membership_type_dec=='Free'){
							$whr=array(
							'sub_cat_id'=>$user_info[0]['user_sub_category_id'],
							'membership_type'=>'Free'
							);
						}else if($user_info[0]['user_category_id']==2){
							$whr=array(
							'sub_cat_id'=>$user_info[0]['user_sub_category_id'],
							'membership_type'=>'Paid'
							);
						}

			    		$type_data = $this->master_model->getRecords("usertype",$whr,'id');
			    		
			    		if (count($type_data)>0) {
    						$permissions_data = $this->master_model->getRecords("user_actions",array('user_type_id'=>$type_data[0]['id']));
			    			$permissions_data=$permissions_data[0]['sub_id'];
			    			if ($permissions_data!='') {
			    				$permissions=$permissions_data;
			    				}else{
			    					$permissions='';
			    				}
		    			}else{
			    				$permissions='';
		    			}	
			    		// echo $this->db->last_query();
			    		$user_data=array('user_email'=>$user_info[0]['email'],
										 'user_id'=>$user_info[0]['user_id'],
										 'user_category'=>$user_info[0]['user_category_id'],
										 'permissions'=>$permissions,
										 'permissions_array'=>explode(',', $permissions),
										 'user_sub_category'=>$user_info['0']['user_sub_category_id']
										);
    					$this->session->set_userdata($user_data);

    					// print_r($user_info);
    					// echo "<pre>";
    					// print_r($_SESSION);
    					// die;
    					$intended_url='';
						if ( isset($_SESSION['intended_url']) && $_SESSION['intended_url'] !='' ){
    						$intended_url = $_SESSION['intended_url'] ;	
    					}
    					
    					if ( $intended_url=='') {
    						$intended_url=base_url();
    					}


    					if ($user_info[0]['user_category_id']==1) {

    						$guid = md5(uniqid(rand(), true));
						   	$this->input->set_cookie('login_cookie', $guid, 360000);

						   	$this->session->set_userdata('profile_reminder', 'remind');

    						redirect($intended_url);
    					}else if($user_info[0]['user_category_id']==2){
    						redirect($intended_url);
    					}
    					
			    		}else{
			    		$this->session->set_flashdata('error_message','Your account is not verified yet.');
					    redirect(base_url().'login');
			    		
			    		}
			    	}else{
			    		$this->session->set_flashdata('error_message','Your account is blocked.<br><small> Please contact the TechNovuus Admin at info@technovus.araiindia.com </small>');
					    redirect(base_url().'login');
			    	}

			    }
			   else
			    {
					$this->session->set_flashdata('error_message','Invalid credential');
					redirect(base_url().'login');
			    }
		    }
		}
	
	//-----------------------Login with google-----------------------

		if(isset($_GET['code'])) #checking if the data is coming from API in variable
		{
			$this->googleplus->getAuthenticate(); #generating token
			$this->session->set_userdata('login',true); #creating session

			$google_info = $this->googleplus->getUserInfo();

			$this->session->set_userdata('userProfile',$google_info); #session start

			$google_email = $encrptopenssl->encrypt($google_info['email']);

			$input_array_google=array('email'=>$google_email,
									'is_valid'=>'1',
									'is_deleted'=>'0'

							);
			$regis = $this->master_model->getRecords('registration',$input_array_google); #checking if the google user is present in our database

			$this->session->userdata($google_email);

			#Inserting the non registered member who are logging in using google
			if(!empty($regis))
			{
				if(is_array($regis) && count($regis)>0)
			    { 
			    	if ($regis[0]['status']=='Active') 
			    	{	

		    		if ($regis[0]['is_verified']=='yes') 
		    		{

		    			$payment_status=$regis[0]['payment_status'];

		    			if ($regis[0]['membership_type']!='') 
		    			{
		    				$membership_type=$encrptopenssl->decrypt( $regis[0]['membership_type'] );
		    				
		    				if ($membership_type=='Paid' && $payment_status=='pending') 
		    				{
		    					redirect(base_url().'registration/payment/'.base64_encode($regis[0]['user_id']));
								exit();
		    				}
		    			}

		    			if($regis[0]['user_category_id']==2 && $payment_status=='pending')
		    			{
		    				redirect(base_url().'registration/payment/'.base64_encode($regis[0]['user_id']));
							exit();
		    			}	

						$membership_type_dec =$encrptopenssl->decrypt($regis[0]['membership_type']);
						
						if ($regis[0]['user_category_id']==1 && $membership_type_dec=='Paid') 
						{
							$whr=array(
							'sub_cat_id'=>1,
							'membership_type'=>'paid'
							);
						}
						else if ($regis[0]['user_category_id']==1 && $membership_type_dec=='Free')
						{
							$whr=array(
							'sub_cat_id'=>$regis[0]['user_sub_category_id'],
							'membership_type'=>'Free'
							);
						}
						else if($regis[0]['user_category_id']==2)
						{
							$whr=array(
							'sub_cat_id'=>$regis[0]['user_sub_category_id'],
							'membership_type'=>'Paid'
							);
						}

			    		$type_data = $this->master_model->getRecords("usertype",$whr,'id');
			    		
			    		if (count($type_data)>0) 
			    		{
    						$permissions_data = $this->master_model->getRecords("user_actions",array('user_type_id'=>$type_data[0]['id']));
			    			$permissions_data=$permissions_data[0]['sub_id'];
			    			if ($permissions_data!='') 
			    			{
			    				$permissions=$permissions_data;
			    			}
			    			else
			    			{
			    				$permissions='';
			    			}
		    			}
		    			else
		    			{
			    			$permissions='';
		    			}	
			    		// echo $this->db->last_query();
			    		$user_data=array(
										'user_email'        =>$regis[0]['email'],
										'user_id'           =>$regis[0]['user_id'],
										'user_category'     =>$regis[0]['user_category_id'],
										'permissions'       =>$permissions,
										'permissions_array' =>explode(',', $permissions),
										'user_sub_category' =>$regis['0']['user_sub_category_id']
										);
    					$this->session->set_userdata($user_data);

    					$intended_url='';
						if ( isset($_SESSION['intended_url']) && $_SESSION['intended_url'] !='' ){
    						$intended_url = $_SESSION['intended_url'] ;	
    					}
    					
    					if ( $intended_url=='') {
    						$intended_url=base_url();
    					}

    					if ($regis[0]['user_category_id']==1) 
    					{

    						$guid = md5(uniqid(rand(), true));
						   	$this->input->set_cookie('login_cookie', $guid, 360000);

    						redirect($intended_url);
    					}
    					else if($regis[0]['user_category_id']==2)
    					{
    						redirect($intended_url);
    					}
			    	}
			    	else
			    	{
			    		$this->session->set_flashdata('error_message','Your account is not verified yet.');
					    redirect(base_url().'login');
			    	}
			    }
			    	else
			    	{
			    		$this->session->set_flashdata('error_message','Your account is blocked.');
					    redirect(base_url().'login');
			    	}
			    }
			   else
			    {
					$this->session->set_flashdata('error_message','Invalid credential');
					redirect(base_url().'login');
			    }
			}

			else
			{
				$this->session->set_flashdata('register_with_google','Invalid credential');
				redirect(base_url('registration?google=true'));
			}
			#Inserting the non registered member who are logging in using google
		}

		//-------------------------------Login with google----------------------	

	$content  = $this->master_model->getRecords('login_content',array('id'=>1));
	$content_arr = array();
	if(count($content)){	
					
		foreach($content as $row_val){		
					
			$row_val['text_content'] = $encrptopenssl->decrypt($row_val['text_content']);
			$row_val['content_img'] = $encrptopenssl->decrypt($row_val['content_img']);
			$content_arr[] = $row_val;
		}
		
	}

	$data['content'] = $content_arr;

	 $login_url = '';
	$data['login_url'] = $login_url;
		//login with google end
	$data['image'] = $this->create_captcha();
	$data['page_title']='Login';
	$data['middle_content']='registration/login';
	$this->load->view('front/front_combo',$data);
		
	}



public function linkedin(){ 
		include_once "vendor/autoload.php";

		//  $this->load->view('linkedin',$data); 

		   $provider = new League\OAuth2\Client\Provider\LinkedIn([
		'clientId'          => '787lffl52sjmuw',
		'clientSecret'      => 'X3klT3Qow1QjSWtD',
		'redirectUri'       => 'http://localhost/e-com/welcome/linkedin',
		]);
		if (!isset($_GET['code'])) {

		// If we don't have an authorization code then get one
		$options = [
		'state' => 'OPTIONAL_CUSTOM_CONFIGURED_STATE',
		'scope' => ['r_liteprofile','r_emailaddress'] // array or string
		];
		$authUrl = $provider->getAuthorizationUrl($options);
		$_SESSION['oauth2state'] = $provider->getState();
		header('Location: '.$authUrl);
		exit;

		// Check given state against previously stored one to mitigate CSRF attack
		} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

		unset($_SESSION['oauth2state']);
		exit('Invalid state');

		} else {

		// Try to get an access token (using the authorization code grant)
		$token = $provider->getAccessToken('authorization_code', [
		    'code' => $_GET['code']
		]);

		// Optional: Now you have a token you can look up a users profile data
		try {

		    // We got an access token, let's now get the user's details
		    $user = $provider->getResourceOwner($token);

		    // Use these details to create a new profile
		    printf('Hello %s!', $user->getFirstName());

		} catch (Exception $e) {

		    // Failed to get user details
		    exit('Oh dear...');
		}

		// Use this to interact with an API on the users behalf
		echo $token->getToken();
		die;
	}
} 

	public function create_captcha($value='')
	{
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
            'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Send captcha image to view
		return $captcha['image'];
	}

	public function check_captcha($code) 
	{

				if($code == '' || $_SESSION["mycaptcha"] != $code    )
				{
					$this->session->unset_userdata("mycaptcha");
					$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
					return false;
				}
				if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
				{
					$this->session->unset_userdata("mycaptcha");
					return true;
				}
		
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
      		'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
			'font_size'     => 17,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}
		
  	
	public function change_password()
	{
	   //echo $this->session->userdata('username'); 
	   if(!$this->session->userdata('username') &&  empty($this->session->userdata('username')))
	   {
	   	 
			redirect(base_url().'adminlogin/');
	   }
	   if(isset($_POST['submit']))
		{ 
			$this->form_validation->set_rules('old_pw', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_pw', 'New Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('c_pw', 'Confirm Password', 'trim|required|xss_clean|matches[new_pw]');		
			//$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha_adminlogin');
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean');
 			if($this->form_validation->run())
			{ 
				$old_pw = $this->input->post('old_pw');  
				$new_pw = $this->input->post('new_pw');
				$c_pw = $this->input->post('c_pw');
			//	print_r($this->session); exit;
				$input_array=array('adminpass'=>$old_pw,'adminuser'=>$this->session->userdata('username'));
				$user_info=$this->master_model->getRecords('adminlogin',$input_array);
				$old_pass_1=$user_info['0']['old_pass_1'];
				$old_pass_2=$user_info['0']['old_pass_2'];
				 			
				 // echo "<pre>";
				 // print_r($_POST);
				 // die; 
				if(count($user_info) > 0)
				{
					if ($old_pass_1!='') {
						if($new_pw == $old_pass_1)
						{
						$this->session->set_flashdata('invalidadminlogin','Password can not be same as old 3 password');
						redirect(base_url().'adminlogin/change_password');
						}
					}

					if ($old_pass_2!='') {
						if($new_pw == $old_pass_2)
						{
						$this->session->set_flashdata('invalidadminlogin','Password can not be same as old 3 password');
						redirect(base_url().'adminlogin/change_password');
						}
					}

					if($new_pw == $old_pw)
					{
						$this->session->set_flashdata('invalidadminlogin','New Password is same as old password');
						redirect(base_url().'adminlogin/change_password');
					}

					else
					{ 
						
						$query1 = "UPDATE adminlogin
						SET old_pass_1='".$old_pw."'
						WHERE adminuser='".$this->session->userdata('username')."'"; 
						$this->db->query($query1);
						
						if ($old_pass_1!='') {
						$query2 = "UPDATE adminlogin
						SET old_pass_2='".$old_pass_1."'
						WHERE adminuser='".$this->session->userdata('username')."'"; 
						$this->db->query($query2);
					    }

						$query = "UPDATE adminlogin
						SET adminpass='".$new_pw."'
						WHERE adminuser='".$this->session->userdata('username')."'"; 
						
						$q=$this->db->query($query);
 						$this->session->set_flashdata('success_msg','Password has been reset.');
						redirect(base_url().'adminlogin/change_password');
 					}
 				}
				else
				{ 
					$this->session->set_flashdata('invalidadminlogin','Invalid old password');
					redirect(base_url().'adminlogin/change_password');
				}
			}
			
		}
	    $this->load->helper('captcha');
		$vals = array(
						'img_path' => './images/captcha/',
						'img_url' => base_url().'images/captcha/',
						);
          $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  //$this->session->unset_userdata("mycaptcha");
		  
	    $_SESSION["adminlogincaptcha"]=$cap['word'];
		$data['middlecontent']='change_pw';
	    $this->load->view('admintemplate',$data);
	}
	
	public function adminlogout()
	{
	    $user_data=array('username'=>'',
						'adminid'=>'',
						'type'=>'');
		$this->session->unset_userdata($user_data);
		delete_cookie("random_cookie");
		redirect(base_url().'adminlogin');
	 }

	 public function logout()
	{
		session_destroy();
		// $this->session->unset_userdata($user_data);
		// delete_cookie("random_cookie");
		redirect(base_url().'login');
	 }
	 
	 
	function get_client_ip() 
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return  $ipaddress;
    }

    public function forgot_password()
    {
    	$encrptopenssl =  New Opensslencryptdecrypt();
    	$this->form_validation->set_rules('email', 'Email', 'required');
		
		if($this->form_validation->run())
		{	
			$email = $encrptopenssl->encrypt($this->input->post('email'));
			$email_text = $this->input->post('email');
			$challenge_mail = $this->master_model->getRecords("email_template", array("id" => '28'));
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			
			$user_whre=array(
				'email'=>$email,
				'is_valid'=>'1',
				'is_deleted'=>'0'
			);
			$user = $this->master_model->getRecords('registration',$user_whre);

	
			if(!empty($user))
			{
				$url=base_url('login/forgotYourPassoword/'.base64_encode($user[0]['user_id']));
				$link="<a href=".$url."> Click Here</a>";
				
				$email_send='';
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$subscriber_mail = $this->master_model->getRecords("email_template", array("id" => '61'));
				$sub_content = '';
				if(count($subscriber_mail) > 0){			
					
					
					$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = ['[LINK]','[PHONENO]']; //'[SIGNATURE]'
					$rep_array = [$link,$phoneNO];
					
					$sub_content = str_replace($arr_words, $rep_array, $desc);
					$info_array=array(
						'to'		=>	$email_text,					
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
					
					

					$other_infoarray	=	array('content' => $sub_content); 
					// print_r($other_infoarray);
					// print_r($info_array);die;
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
					
						
					
				}

				if($email_send)
					{
						$this->session->set_flashdata('success','Please check your email for further process');
						redirect(base_url('login/forgot_password'));
					}
				
				
			}
			else
			{
				$this->session->set_flashdata('error','This Email is not present in our database');
				redirect(base_url('login/forgot_password'));
			}

		}
    	$data['page_title']='Login';
		$data['middle_content']='registration/forgot_password';
		$this->load->view('front/front_combo',$data);
    }

    public function forgotYourPassoword($id)
    {
    	$userId = base64_decode($id);
   			
   		$data['user_id'] = $userId;
    	$data['page_title']='Login';
		$data['middle_content']='registration/forgot_password_form';
		$this->load->view('front/front_combo',$data);
    }

     public function storeYourNewPassword()
    {
    	$userId = $this->input->post('id');		
		$newPassword = $this->input->post('new_password');
		
		$user = $this->master_model->getRecords('registration',array('user_id'=>$userId));
		
		if($user && ($user[0]['password'] == $newPassword)){
			 $this->session->set_flashdata('error','Old and new passwords should be different');
			 redirect(base_url('login/forgotYourPassoword/'.base64_encode($userId)));
		}
		
	   		if(isset($newPassword) && isset($userId))
	   		{
	   			$insertArr = array('password' => $newPassword);
	   			$updateQuery = $this->master_model->updateRecord('registration',$insertArr,array('user_id' => $userId));

	   			if($updateQuery > 0)
	   			{
	   				$this->session->set_flashdata('success_message_cp','Password Updated Successfully');
						redirect(base_url('login'));
	   			}
	   		}
	   		else
	   		{
	   			$this->session->set_flashdata('error','Something went wrong');
				redirect(base_url('login/forgotYourPassoword'));
	   		}
    	

    

    }

	public function check_pass()
   		{

   			$currentPassword = sha1($this->input->post('new_password'));
	   		$userId = $this->input->post('user_id');
	   		
	   		$user = $this->master_model->getRecords('registration',array('user_id'=>$userId,'password'=>$currentPassword));
	   		if (count($user)) {
	   			echo "false";
	   		}else{
	   			echo "true";
	   		}
   		}
    
   public function set_session(){
            session_destroy();
        	$user_data=array('user_email'=>'vishal@esds.co.in');
    		$this->session->set_userdata($user_data);
    		
    		$_SESSION['name']='Vishal';
    		print_r($_SESSION);
    }
    
     public function get_session(){
        print_r($_SESSION);
    }
    
    public function info(){
        print_r(phpinfo());
    }

    function remove_reminder(){
    	$_SESSION['profile_reminder']='';
    	echo "done";
    }
	
}