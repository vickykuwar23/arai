<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Team_discussion extends CI_Controller 
	{

		
		function __construct() 
		{
			parent::__construct();
			$this->load->model('Common_model_sm');
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			
			$this->login_user_id = $this->session->userdata('user_id');			
			if($this->login_user_id == "") { redirect(site_url()); }
			
		}
		
		############# START ############################
		public function get_chats()
		{
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$user_id = $this->session->userdata('user_id');
			$team_id=$this->input->post('team_id');
			if ($team_id!='') {
				
				 $this->db->select('msg_id,title,first_name,last_name,message,team_discussion.user_id,registration.user_category_id,team_discussion.created_at,student_profile.profile_picture,profile_organization.org_logo');	
				 $this->db->join('profile_organization','profile_organization.user_id=team_discussion.user_id','LEFT');	
			 	$this->db->join('student_profile','student_profile.user_id=team_discussion.user_id','LEFT');			
				   $this->db->join('registration','registration.user_id=team_discussion.user_id');	
			$chat_data= $this->master_model->getRecords('team_discussion',
				array('team_id'=>$team_id, 'team_discussion.is_deleted'=>'0'),'',array('msg_id'=>'asc') );

				if (count($chat_data)) {
						
						$chat_arr = array();
				    	foreach ($chat_data as $key => $row_val) {
				    			$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				    			$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				    			$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
				    			$chat_arr[] = $row_val;
				    	}
				    	$chat_str = '';
				    	foreach ($chat_arr as $key => $chat) {
				    	$class= '';
				    	$show_delete=false;	
				    	if ($chat['user_id'] == $user_id) {
				    		$class='self_msg';
				    		$show_delete=true;
				    	}
				    	$chat_id=$chat['msg_id'];
				    	$onclick_delete_chat = "delete_chat('".$chat_id."')";
				    	$chat_str.=	'<div class="comments '.$class.'">
										<div class="comment-box">
											<span class="commenter-pic">';

									if ($chat['user_category_id'] == 1 && $chat['profile_picture']!='') 
										{
											$pro_pic = base_url('assets/profile_picture/'.$chat['profile_picture']);
										}
									elseif ($chat['user_category_id'] == 2 && $chat['org_logo']!='') {
											$pro_pic = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($chat['org_logo']));		
										}else{
											$pro_pic = base_url('assets/profile_picture/user_dummy.png');
										}			
						$chat_str.=	  '<img src="'.$pro_pic.'" class="img-fluid">';

						$chat_str.=	 '</span>
											<span class="commenter-name">
												<a href="javascript:void(0)">'.$chat["title"]." ".$chat["first_name"]." ".$chat["last_name"].'</a> <span class="comment-time">'.$this->time_elapsed_string($chat['created_at'],true).'</span>
											</span>       
											<p class="comment-txt more">'.$chat['message'].'

											</p>';
								if ($show_delete) {
												
						$chat_str.=	'<a class="pull-right" onclick="'.$onclick_delete_chat.'" href="javascript:void(0)">Delete </a>';
									}

										'</div>
									</div>';
				    	}

				    	echo $chat_str;


				}

			

				
			}
						  

		}	

		function insert_chat(){
			$team_id=$this->input->post('team_id');
			$msg_content=$this->input->post('msg_content');
			$user_id = $this->session->userdata('user_id');

			if ($team_id!='' && $msg_content!='' && $user_id!='') {
				$insert_arr=array(
				'team_id'=>$team_id,
				'user_id'=>$user_id,
				'message'=>$msg_content,
				'created_at'=>date("Y-m-d H:i:s")
				);

				$insert = $this->master_model->insertRecord('arai_team_discussion',$insert_arr);
					
			}
		}

		function time_elapsed_string($datetime, $full = false) {
		    $now = new DateTime;
		    $ago = new DateTime($datetime);
		    $diff = $now->diff($ago);

		    $diff->w = floor($diff->d / 7);
		    $diff->d -= $diff->w * 7;

		    $string = array(
		        'y' => 'year',
		        'm' => 'month',
		        'w' => 'week',
		        'd' => 'day',
		        'h' => 'hour',
		        'i' => 'minute',
		        // 's' => 'second',
		    );
		    foreach ($string as $k => &$v) {
		        if ($diff->$k) {
		            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		        } else {
		            unset($string[$k]);
		        }
		    }

		    if (!$full) $string = array_slice($string, 0, 1);
		    return $string ? implode(', ', $string) . ' ago' : 'just now';
		}

		public function team_details_owner($team_id='')
		{
				
			$team_id=base64_decode($team_id);

						
			$team_details= $this->master_model->getRecords('byt_teams',
				array('team_id'=>$team_id, 'is_deleted'=>'0'));
			if (count($team_details) ==0) {
				redirect(base_url('myteams'));
			}


			$owner_id = $team_details[0]['user_id'];

			if ($user_id !=  $owner_id) {
				redirect(base_url('myteams/myCreatedteams'));	
			}



		}

		public function delete_chat()// DELETE QUESTION
		{
			if(isset($_POST) && count($_POST) > 0)
			{
				$msg_id = $this->input->post('chat_id');
				$result['flag'] = "success";				
				$del_data['is_deleted'] = '1';
				$this->master_model->updateRecord('team_discussion',$del_data,array('msg_id' => $msg_id));
				
					
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
}		