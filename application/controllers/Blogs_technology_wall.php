<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blogs_technology_wall extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');

        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');
    }

    public function index()
    {
        //$module_id = 8;
        //$this->check_permissions->is_authorise($module_id);

        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);

        $this->check_permissions->is_logged_in();
        $encrptopenssl                   = new Opensslencryptdecrypt();
        $data['featured_blog_limit']     = $featured_blog_limit     = 15;
        $data['non_featured_blog_limit'] = $non_featured_blog_limit = 10;

        $this->db->order_by('technology_name', 'ASC');
        $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $data['technology_data'] = $this->master_model->getRecords("arai_blog_technology_master", array("status" => 'Active'));

        $data['tag_data'] = $this->master_model->getRecords("arai_blog_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['page_title']     = 'Blogs';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'blogs_technology_wall/index';
        $this->load->view('front/front_combo', $data);
    }

    public function getBlogDataAjax()
    {
        $encrptopenssl                                = new Opensslencryptdecrypt();
        $csrf_new_token                               = $this->security->get_csrf_hash();
        $result['csrf_new_token']                     = $csrf_new_token;
        $data_non_featured['featured_blog_limit']     = $featured_blog_limit     = 15;
        $data_non_featured['non_featured_blog_limit'] = $non_featured_blog_limit = 10;
        $search_str                                   = '';

        if (isset($_POST) && count($_POST) > 0) {
            $f_start      = $this->input->post('f_start', true);
            $f_limit      = $this->input->post('f_limit', true);
            $nf_start     = $this->input->post('nf_start', true);
            $nf_limit     = $this->input->post('nf_limit', true);
            $is_show_more = $this->input->post('is_show_more', true);
            if ($f_start != "" && $f_limit != "" && $nf_start != "" && $nf_limit != "" && $is_show_more != "") {
                $result['flag'] = "success";
                $user_id        = $this->session->userdata('user_id');
                $BlogActiondata = $this->getBlogActiondata($user_id);

                //START : FOR SEARCH KEYWORD
                $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword'));
                if ($keyword) {
                    $search_str .= " ( b.blog_id_disp LIKE '%" . $keyword . "%' OR b.blog_title LIKE '%" . $keyword . "%' OR b.blog_description LIKE '%" . $keyword . "%' OR b.technology_other LIKE '%" . $keyword . "%' OR b.author_name LIKE '%" . $keyword . "%' OR b.author_professional_status LIKE '%" . $keyword . "%' OR b.author_org_name LIKE '%" . $keyword . "%' OR b.author_description LIKE '%" . $keyword . "%' OR b.author_about LIKE '%" . $keyword . "%') ";
                }
                //END : FOR SEARCH KEYWORD

                //START : FOR SEARCH TECHNOLOGY
                $technology = trim($this->input->post('technology'));
                if ($technology != '') {
                    $technology_arr = explode(",", $technology);
                    if (count($technology_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($technology_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',b.technology_ids)";
                            if ($i != (count($technology_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TECHNOLOGY

                //START : FOR SEARCH TAG
                $tag = trim($this->input->post('tag'));
                if ($tag != '') {
                    $tag_arr = explode(",", $tag);
                    if (count($tag_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($tag_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',b.tags)";
                            if ($i != (count($tag_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TAG

                //START : FOR SEARCH BLOG TYPE
                $blog_type = trim($this->input->post('blog_type'));
                if ($blog_type != '') {
                    $blog_type_arr = explode(",", $blog_type);
                    if (count($blog_type_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($blog_type_arr as $res) {
                            $search_str .= " b.is_technical = '" . $res . "'";
                            if ($i != (count($blog_type_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH BLOG TYPE

                //START : FOR SEARCH BLOG TYPE 2
                $blog_type2 = trim($this->input->post('blog_type2'));
                if ($blog_type2 != '') {
                    $blog_type2_arr = explode(",", $blog_type2);
                    if (count($blog_type2_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($blog_type2_arr as $res) {
                            if ($res == '0') {$search_str .= " r.user_category_id = '2'";} else { $search_str .= " r.user_sub_category_id = '" . $res . "'";}

                            if ($i != (count($blog_type2_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH BLOG TYPE 2

                //END : FOR SEARCH DATE
                $from_date = trim($this->input->post('from_date'));
                $to_date   = trim($this->input->post('to_date'));

                if ($from_date != '' && $to_date != '') {

                    $from_date = date('Y-m-d', strtotime($from_date));
                    $to_date   = date('Y-m-d', strtotime($to_date));

                    $old_search_str = $search_str;
                    if ($old_search_str != '') {$search_str .= " AND ";}
                    $search_str .= "( DATE(b.created_on) BETWEEN '" . $from_date . "' and '" . $to_date . "' )";
                }

                // START : FOR FEATURED BLOG & TRENDING BLOGS
                $featured_blog_total_cnt = 0;
                if ($is_show_more == 0) {
                    // START : FOR FEATURED BLOG
                    if ($search_str != "") {$this->db->where($search_str);}
                    $this->db->order_by('blog_id', 'DESC', false);
                    $select_f = "b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description,b.tag_other, b.author_about, b.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                    $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
                    $this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
                    $this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
                    $data_featured['featured_blog']  = $featured_blog  = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_featured' => '1', 'b.is_block' => '0'), $select_f, '', $f_start, $f_limit);
                    $featured_blog_total_cnt         = count($featured_blog);
                    $data_featured['BlogActiondata'] = $BlogActiondata;
                    //$result['featured_blog_qry'] = $this->db->last_query();
                    $result['Featured_response'] = $this->load->view('front/blogs_technology_wall/incFeatureBlogCommon', $data_featured, true);
                    // END : FOR FEATURED BLOG

                    // START : FOR TRENDING BLOG
                    if ($search_str != "") {$this->db->where($search_str);}
                    $this->db->order_by('b.xOrder', 'ASC', false);
                    $select_t = "b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description,b.tag_other, b.author_about, b.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                    $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
                    $this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
                    $this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
                    $data_trending['trending_blog']  = $trending_blog  = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_featured' => '0', 'b.is_block' => '0', 'b.xOrder !=' => '0'), $select_t, '', $f_start, $f_limit);
                    $trending_blog_total_cnt         = count($trending_blog);
                    $data_trending['BlogActiondata'] = $BlogActiondata;
                    //$result['trending_blog_qry'] = $this->db->last_query();
                    $result['Trending_response'] = $this->load->view('front/blogs_technology_wall/incTrendingBlogCommon', $data_trending, true);
                    // END : FOR TRENDING BLOG
                }
                // START : FOR FEATURED BLOG & TRENDING BLOGS

                // START : FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
                $this->db->order_by('blog_id', 'DESC', false);
                $select_nf = "b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description,b.tag_other, b.author_about, b.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id,  org.org_logo, sp.profile_picture";
                $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
                $this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
                $this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
                $data_non_featured['non_featured_blog'] = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_featured' => '0', 'b.is_block' => '0', 'b.xOrder' => '0'), $select_nf, '', $nf_start, $nf_limit);
                $data_non_featured['BlogActiondata']    = $BlogActiondata;
                //$result['non_featured_blog_qry'] = $this->db->last_query();

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
                $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
                $data_non_featured['total_non_featured_blog_count'] = $total_non_featured_blog_count = $this->master_model->getRecordCount('arai_blog b', array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_featured' => '0', 'b.is_block' => '0', 'b.xOrder' => '0'), 'b.blog_id');
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG xOrder

                $data_non_featured['featured_blog_total_cnt'] = $featured_blog_total_cnt;
                $data_non_featured['trending_blog_total_cnt'] = $trending_blog_total_cnt;
                $data_non_featured['new_start']               = $new_start               = $nf_start + $nf_limit;
                $result['NonFeatured_response']               = $this->load->view('front/blogs_technology_wall/incNonFeatureBlogCommon', $data_non_featured, true);
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function myBlogs() // MY BLOGS LIST

    {
        $this->check_permissions->is_logged_in();
        $user_id                = $this->session->userdata('user_id');
        $data['page_title']     = 'Blogs';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'blogs_technology_wall/listing';
        $this->load->view('front/front_combo', $data);
    }

    public function blogs_technology_wall_listing() // MY BLOGS LIST : GET SERVER SIDE DATATABLE DATA USING AJAX

    {
        //$this->check_permissions->is_logged_in();
        $csrf_test_name  = $this->security->get_csrf_hash();
        $user_id         = $this->session->userdata('user_id');
        $draw            = $_POST['draw'];
        $row             = $_POST['start'];
        $rowperpage      = $_POST['length']; // Rows display per page
        $columnIndex     = @$_POST['order'][0]['column']; // Column index
        $columnName      = @$_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = @$_POST['order'][0]['dir']; // asc or desc
        $searchValue     = @$_POST['search']['value']; // Search value
        $pageNo          = ($_POST['start'] / $_POST['length']) + 1;

        $condition = "SELECT * FROM arai_blog WHERE is_deleted = '0' AND user_id = '" . $user_id . "' ORDER BY blog_id DESC";
        $query     = $condition . ' LIMIT  ' . $this->input->post('length') . ' OFFSET ' . $this->input->post('start');
        $result    = $this->db->query($query)->result();
        $rowCount  = $this->getNumData($condition);
        $rowCnt    = 0;
        $dataArr   = array();

        if ($rowCount > 0) {
            $no = $_POST['start'];
            $i  = 0;
            foreach ($result as $showResult) {
                $blog_id      = $showResult->blog_id;
                $blog_id_disp = '<a href="' . base_url('blogs_technology_wall/blogDetails/' . base64_encode($blog_id)) . '" class="applicant-listing">' . $showResult->blog_id_disp . '</a>';
                $blog_title   = ucwords($showResult->blog_title);
                $author_name  = $showResult->author_name;
                $posted_on    = date("d M, Y h:i a", strtotime($showResult->created_on));

                $admin_status = $category = $action = '';

                if ($showResult->admin_status == 0) {$admin_status = 'Pending';} else if ($showResult->admin_status == 1) {$admin_status = 'Approved';} else if ($showResult->admin_status == 2) {$admin_status = 'Rejected';}

                if ($showResult->is_technical == 0) {$category = 'Non-Technical';} else if ($showResult->is_technical == 1) {$category = 'Technical';}

                $action .= '<a href="' . base_url('blogs_technology_wall/add_blog/' . base64_encode($blog_id)) . '" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                if ($showResult->is_block == '0') {
                    $b_title = 'Block';
                    $b_icon  = 'fa fa-ban';} else if ($showResult->is_block == '1') {
                    $b_title = 'Unblock';
                    $b_icon  = 'fa fa-unlock-alt';}
                $onclick_block_fun = "block_unblock_blog('" . base64_encode($blog_id) . "', '" . $b_title . "', '" . $showResult->blog_id_disp . "', '" . $showResult->blog_title . "')";

                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="' . $b_title . '" class="btn btn-info btn-green-fresh" onclick="' . $onclick_block_fun . '"><i class="' . $b_icon . '" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                $onclick_delete_fun = "delete_blog('" . base64_encode($blog_id) . "')";
                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" class="btn btn-info btn-green-fresh" onclick="' . $onclick_delete_fun . '"><i class="fa fa-remove" aria-hidden="true"></i></a> ';

                $onclick_like_fun = "show_blog_likes('" . base64_encode($blog_id) . "')";
                $getTotalLikes    = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $blog_id), 'like_id');
                $disp_likes       = '<div class="text-center"><a style="min-width:30px;" href="javascript:void(0);" data-toggle="tooltip" title="Likes" class="btn btn-info btn-green-fresh" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';

                $this->db->join("arai_registration r", "r.user_id = b.user_id", "INNER", false);
                $getTotalComments = $this->master_model->getRecordCount('arai_blog_comments b', array('b.blog_id' => $blog_id, 'b.status' => 1, 'b.parent_comment_id' => 0), 'b.comment_id');
                $disp_comments    = '<div class="text-center"><a style="min-width:30px;" href="' . site_url('blogs_technology_wall/blogDetails/' . base64_encode($blog_id) . '/1') . '" data-toggle="tooltip" title="Likes" class="btn btn-info btn-green-fresh">' . $getTotalComments . '</a></div>';

                $onclick_reported_fun = "show_blog_reported('" . base64_encode($blog_id) . "')";
                $getTotalReported     = $this->master_model->getRecordCount('arai_blogs_reported', array('blog_id' => $blog_id), 'reported_id');
                $disp_reported        = '<div class="text-center"><a style="min-width:30px;" href="javascript:void(0);" data-toggle="tooltip" title="Reported" class="btn btn-info btn-green-fresh" onclick="' . $onclick_reported_fun . '">' . $getTotalReported . '</a></div>';

                $i++;
                $dataArr[] = array(
                    $blog_id_disp,
                    $blog_title,
                    $author_name,
                    $posted_on,
                    $admin_status,
                    $category,
                    $disp_likes,
                    $disp_comments,
                    $disp_reported,
                    "<span style='white-space:nowrap;'>" . $action . "</span>",
                );
                $rowCnt   = $rowCount;
                $response = array(
                    "draw"            => $draw,
                    "recordsTotal"    => $rowCnt ? $rowCnt : 0,
                    "recordsFiltered" => $rowCnt ? $rowCnt : 0,
                    "data"            => $dataArr,
                );
                $response['token'] = $csrf_test_name;
            }
        } else {
            $rowCnt   = $rowCount;
            $response = array(
                "draw"            => $draw,
                "recordsTotal"    => 0,
                "recordsFiltered" => 0,
                "data"            => $dataArr,
            );
            $response['token'] = $csrf_test_name;
        }
        echo json_encode($response);

    } // End Function

    public function add_blog($id = 0) // ADD NEW BLOG

    {
        $this->check_permissions->is_logged_in();

        $module_id = 24;
        $this->check_permissions->is_authorise($module_id);

        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("arai_blog", array('blog_id' => $id, 'user_id' => $user_id, 'is_deleted' => 0));
            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else { $data['mode'] = $mode = "Add";}
        }

        $data['blog_banner_error'] = $data['technology_ids_error'] = $data['tags_error'] = $error_flag = '';
        $file_upload_flag          = 0;
        if (isset($_POST) && count($_POST) > 0) {
            //$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');
            $this->form_validation->set_rules('blog_title', 'Blog title', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('blog_description', 'Blog Description', 'required', array('required' => 'Please enter the %s'));

            if ($mode == 'Add' && ((!isset($_FILES['blog_banner'])) || $_FILES['blog_banner']['size'] == 0)) {
                $data['blog_banner_error'] = 'Please select the Blog Banner';
                $error_flag                = 1;
            }

            if (!isset($_POST['technology_ids'])) {
                $data['technology_ids_error'] = 'Please select the Blog Technology';
                $error_flag                   = 1;
            }

            if (!isset($_POST['tags'])) {
                $data['tags_error'] = 'Please select the Tags';
                $error_flag         = 1;
            }

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['blog_banner']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $blog_banner = $this->Common_model_sm->upload_single_file("blog_banner", array('png', 'jpg', 'jpeg', 'gif'), "blog_banner_" . date("YmdHis"), "./uploads/blog_banner", "png|jpeg|jpg|gif");
                    if ($blog_banner['response'] == 'error') {
                        $data['blog_banner_error'] = $blog_banner['message'];
                        $file_upload_flag          = 1;
                    } else if ($blog_banner['response'] == 'success') {
                        $add_data['blog_banner'] = $blog_banner['message'];
                        /* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
                    }
                }

                if ($file_upload_flag == 0) {
                    $add_data['user_id']          = $user_id;
                    $add_data['blog_title']       = $this->input->post('blog_title');
                    $add_data['blog_description'] = $this->input->post('blog_description');
                    $add_data['technology_ids']   = implode(",", $this->input->post('technology_ids'));

                    $technology_ids_other = trim($this->security->xss_clean($this->input->post('technology_ids_other')));
                    if (isset($technology_ids_other) && $technology_ids_other != "") {
                        $add_data['technology_other'] = trim($this->security->xss_clean($technology_ids_other));
                        if ($mode=='Add') {$this->check_permissions->StoreOtherData($technology_ids_other, 'arai_blog_technology_master','technology_name');}
                        
                    } else { $add_data['technology_other'] = '';}

                    $add_data['tags']         = implode(",", $this->input->post('tags'));
                    $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                    if (isset($tag_other) && $tag_other != "") {
                        $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
                         if ($mode=='Add') {$this->check_permissions->StoreOtherData($tag_other, 'arai_blog_tags_master','tag_name');}
                    } else { $add_data['tag_other'] = '';}
                    
                    $add_data['is_technical'] = $this->input->post('is_technical');
                    $add_data['author_name']  = trim($this->security->xss_clean($this->input->post('author_name')));

                    $author_professional_status_chk = $this->input->post('author_professional_status_chk');
                    if (isset($author_professional_status_chk) && $author_professional_status_chk == 'on') {
                        $add_data['author_professional_status'] = trim($this->security->xss_clean($this->input->post('author_professional_status')));
                    } else { $add_data['author_professional_status'] = '';}

                    $author_org_name_chk = $this->input->post('author_org_name_chk');
                    if (isset($author_org_name_chk) && $author_org_name_chk == 'on') {
                        $add_data['author_org_name'] = trim($this->security->xss_clean($this->input->post('author_org_name')));
                    } else { $add_data['author_org_name'] = '';}

                    $author_description_chk = $this->input->post('author_description_chk');
                    if (isset($author_description_chk) && $author_description_chk == 'on') {
                        $add_data['author_description'] = trim($this->security->xss_clean($this->input->post('author_description')));
                    } else { $add_data['author_description'] = '';}

                    $author_about_chk = $this->input->post('author_about_chk');
                    if (isset($author_about_chk) && $author_about_chk == 'on') {
                        $add_data['author_about'] = trim($this->security->xss_clean($this->input->post('author_about')));
                    } else { $add_data['author_about'] = '';}

                    //print_r($add_data); exit;

                    if ($mode == 'Add') {
                        $add_data['is_featured']  = 0;
                        $add_data['admin_status'] = 0;
                        $add_data['is_deleted']   = 0;
                        $add_data['is_block']     = 0;
                        $add_data['created_on']   = date('Y-m-d H:i:s');

                        $blog_id                 = $this->master_model->insertRecord('arai_blog', $add_data, true);
                        $up_data['blog_id_disp'] = "TN-BLG-" . sprintf("%07d", $blog_id);
                        $this->master_model->updateRecord('arai_blog', $up_data, array('blog_id' => $blog_id));

                        $email_info = $this->get_mail_data_blog($blog_id);

                        //START : SEND MAIL TO USER WHEN BLOG IS POSTED - blog_mail_to_user_while_writing_blog
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('blog_mail_to_user_while_writing_blog');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[USER_NAME]',
                                '[Blog_Title]',
                                '[Blog_Date]',
                            ];
                            $rep_array = [
                                $email_info['USER_NAME'],
                                $email_info['Blog_Title'],
                                $email_info['Blog_Date'],
                            ];
                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['USER_EMAIL'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                        //END : SEND MAIL TO USER WHEN BLOG IS POSTED - blog_mail_to_user_while_writing_blog

                        //START : SEND MAIL TO ADMIN WHEN BLOG IS POSTED - blog_mail_to_admin_while_writing_blog
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('blog_mail_to_admin_while_writing_blog');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[USER_NAME]',
                                '[Blog_Title]',
                                '[USER_ID]',
                            ];
                            $rep_array = [
                                $email_info['USER_NAME'],
                                $email_info['Blog_Title'],
                                $email_info['USER_ID'],
                            ];
                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['admin_email'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                        //END : SEND MAIL TO ADMIN WHEN BLOG IS POSTED - blog_mail_to_admin_while_writing_blog

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Add Blog Technology Wall",
                            'module_name' => 'Blog Technology Wall Front',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Blog has been successfully posted');
                    } else if ($mode == 'Update') {
                        $add_data['admin_status'] = 0;
                        $add_data['updated_on']   = date('Y-m-d H:i:s');

                        $blog_id = $id;
                        $this->master_model->updateRecord('arai_blog', $add_data, array('blog_id' => $blog_id));
                        //echo $this->db->last_query(); exit;

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Update Blog Technology Wall",
                            'module_name' => 'Blog Technology Wall Front',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Blog has been successfully updated');
                    }

                    redirect(site_url('blogs_technology_wall/myBlogs'));
                }
            }
        }

        // $this->db->order_by('technology_name', 'ASC');
        // $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $this->db->order_by('FIELD(technology_name, "Other") ASC, technology_name ASC');
        $data['technology_data'] = $this->master_model->getRecords("arai_blog_technology_master", array("status" => 'Active'));

        //$this->db->order_by("FIELD(tag_name, 'other')", "DESC", false);
         $this->db->order_by('FIELD(tag_name, "Other") ASC, tag_name ASC');
        $data['tag_data'] = $this->master_model->getRecords("arai_blog_tags_master", array("status" => 'Active'));

        $data['page_title']     = 'Blogs';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'blogs_technology_wall/add_blog';
        $this->load->view('front/front_combo', $data);
    }

    public function get_autor_details() // USED TO GET AUTHOR DETAILS ON ADD/EDIT BLOG PAGE

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $login_user_id = $this->session->userdata('user_id');
            $this->db->select('r.user_id, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, sp.employement_status, sp.other_emp_status, sp.company_name');
            $this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', false);
            $user_data = $this->master_model->getRecords("arai_registration r", array('r.user_id' => $login_user_id));
            //$result['qry'] = $this->db->last_query();

            $result['flag'] = "success";
            $author_name    = $professional_status    = $org_name    = '';

            if (count($user_data) > 0) {
                $title       = $encrptopenssl->decrypt($user_data[0]['title']);
                $first_name  = $encrptopenssl->decrypt($user_data[0]['first_name']);
                $middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
                $last_name   = $encrptopenssl->decrypt($user_data[0]['last_name']);

                $author_name = $title . " " . $first_name;
                if ($middle_name != '') {$author_name .= " " . $middle_name . " ";}
                $author_name .= $last_name;

                if ($user_data[0]['user_category_id'] == 2) //Organization
                {
                    $professional_status = '';
                    $org_name            = $encrptopenssl->decrypt($user_data[0]['institution_full_name']);
                } else {
                    if ($user_data[0]['user_sub_category_id'] == 1 || $user_data[0]['user_sub_category_id'] == 11) //FOR INDIVIDUAL USER OR EXPERT USER
                    {
                        $org_name = $encrptopenssl->decrypt($user_data[0]['company_name']);
                    }
                    $professional_status = $encrptopenssl->decrypt($user_data[0]['employement_status']);
                }
            }

            $result['author_name']         = $author_name;
            $result['professional_status'] = $professional_status;
            $result['org_name']            = $org_name;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function block_unblock_blog() // BLOCK/UNBLOCK BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id              = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));
            $type                 = trim($this->security->xss_clean($this->input->post('type')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";

            if ($type == 'Block') {
                //START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
                $email_info = $this->get_mail_data_blog($blog_id);

                $email_send      = '';
                $slug            = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content     = '';

                if (count($subscriber_mail) > 0) {
                    $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                    $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                    $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                    $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                    $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                    $arr_words = [
                        '[Blog_Title]',
                        '[BLOCKED_REASON]',
                    ];
                    $rep_array = [
                        $email_info['Blog_Title'],
                        $popupBlogBlockReason,
                    ];
                    $sub_content = str_replace($arr_words, $rep_array, $desc);

                    $info_array = array(
                        'to'      => $email_info['admin_email'],
                        'cc'      => '',
                        'from'    => $fromadmin,
                        'subject' => $subject_title,
                        'view'    => 'common-file',
                    );

                    $other_infoarray = array('content' => $sub_content);
                    $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                }
                //END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked

                $up_data['is_block']     = 1;
                $up_data['block_reason'] = $popupBlogBlockReason;
            } else if ($type == 'Unblock') {
                $up_data['is_block']     = 0;
                $up_data['block_reason'] = '';}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('arai_blog', $up_data, array('blog_id' => $blog_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Block Blog Technology Wall",
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function delete_blog() // DELETE BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $user_id           = $this->session->userdata('user_id');
            $result['flag']    = "success";
            $result['blog_id'] = $blog_id;

            $del_data['is_deleted'] = 1;
            $del_data['deleted_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('arai_blog', $del_data, array('blog_id' => $blog_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete Blog Technology Wall",
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success_blog_del_msg', 'Blog has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_blog_ajax() // LIKE - UNLIKE BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag              = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id           = $this->session->userdata('user_id');
            $result['flag']    = "success";
            $result['blog_id'] = $blog_id;

            if ($flag == 1) {
                $action_name            = "Like Blog Technology Wall";
                $add_like['blog_id']    = $blog_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_blogs_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';
            } else {
                $this->db->where('blog_id', $blog_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_blogs_likes');

                $action_name = "Un-Like Blog Technology Wall";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';
            }
            $getTotalLikes         = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $blog_id), 'like_id');

            $onclick_fun        = "like_unlike_blog('" . base64_encode($blog_id) . "','" . $LikeFlag . "')";
            $result['response'] = '<span style="cursor:pointer" onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . '</span>';
             $result['total_likes_html'] = '<span style="cursor:auto;">' . $getTotalLikes . '</span>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_comment_ajax() // LIKE - UNLIKE COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag                 = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $comment_data   = $this->master_model->getRecords("arai_blog_comments", array('comment_id' => $comment_id), 'total_likes', '', 0, 1);
            $new_total_like = $comment_data[0]['total_likes'];
            if ($flag == 1) {
                $action_name            = "Like Comment Technology Wall";
                $add_like['comment_id'] = $comment_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_blog_comment_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Like';
                $likeIcon   = 'fa-thumbs-up';
                $bg_color   = "style='color:#1562a6; font-weight:500;'";

                $new_total_like = $new_total_like + 1;
            } else {
                $this->db->where('comment_id', $comment_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_blog_comment_likes');

                $action_name = "Un-Like Comment Technology Wall";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-o-up';
                $bg_color    = "style='color:#000'";

                $new_total_like = $new_total_like - 1;
            }

            $up_data['total_likes'] = $new_total_like;
            $this->master_model->updateRecord('arai_blog_comments', $up_data, array('comment_id' => $comment_id));

            $onclick_fun        = "like_dislike_comment('" . base64_encode($comment_id) . "','" . $LikeFlag . "')";
            $result['response'] = '<p class="comment_like_btn" onclick="' . $onclick_fun . '" ' . $bg_color . '><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $new_total_like . ')' . '</p>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_blog_likes_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']    = "success";
            $result['blog_id'] = $blog_id;

            $this->db->order_by('blog_id', 'DESC', false);
            $select = "bl.like_id, bl.blog_id, bl.user_id, bl.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_blogs_likes bl", array('bl.blog_id' => $blog_id, 'r.is_verified' => "yes", 'r.status' => "Active", 'r.is_valid' => "1", 'r.is_deleted' => "0"), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/blogs_technology_wall/incBlogLikesModal', $data, true);

            $blog_data            = $this->master_model->getRecords("arai_blog", array('blog_id' => $blog_id), "blog_title");
            $result['blog_title'] = "Blog Likes : " . $blog_data[0]['blog_title'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_blog_reported_ajax() // SHOW REPORTED COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id = trim($this->security->xss_clean(base64_decode($this->input->post('blog_id'))));

            $result['flag']    = "success";
            $result['blog_id'] = $blog_id;

            $this->db->order_by('blog_id', 'DESC', false);
            $select = "br.reported_id, br.blog_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = br.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_blogs_reported br", array('br.blog_id' => $blog_id), $select);
            $result['response'] = $this->load->view('front/blogs_technology_wall/incBlogReportedModal', $data, true);

            $blog_data            = $this->master_model->getRecords("arai_blog", array('blog_id' => $blog_id), "blog_title");
            $result['blog_title'] = "Blog Reports : " . $blog_data[0]['blog_title'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getread_more_descriptionAjax() // GET BLOG DESCRIPTION DETAIL

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id   = base64_decode(trim($this->security->xss_clean($this->input->post('blog_id'))));
            $blog_data = $this->master_model->getRecords("arai_blog", array("blog_id" => $blog_id), 'blog_title, blog_description');

            $result['flag']             = "success";
            $result['blog_title']       = $blog_data[0]['blog_title'];
            $result['blog_description'] = htmlspecialchars_decode($blog_data[0]['blog_description']);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getBlogActiondata($user_id = 0)
    {
        $user_like_blog = $this->master_model->getRecords("arai_blogs_likes", array('user_id' => $user_id), 'blog_id');

        $like_arr = $self_blog_arr = $reported_arr = $return_arr = array();
        if (count($user_like_blog) > 0) {
            foreach ($user_like_blog as $res) {
                $like_arr[$res['blog_id']] = $res['blog_id'];
            }
        }

        $user_self_blog = $this->master_model->getRecords("arai_blog", array('user_id' => $this->session->userdata('user_id')), 'blog_id');
        if (count($user_self_blog) > 0) {
            foreach ($user_self_blog as $res) {
                $self_blog_arr[$res['blog_id']] = $res['blog_id'];
            }
        }

        $user_reported_blog = $this->master_model->getRecords("arai_blogs_reported", array('user_id' => $user_id), 'blog_id');
        if (count($user_reported_blog) > 0) {
            foreach ($user_reported_blog as $res) {
                $reported_arr[$res['blog_id']] = $res['blog_id'];
            }
        }

        $return_arr['like_blog']     = $like_arr;
        $return_arr['self_blog']     = $self_blog_arr;
        $return_arr['reported_blog'] = $reported_arr;

        return $return_arr;
    }

    public function getBlogCommentsActiondata($user_id = 0)
    {
        $user_like_comment = $this->master_model->getRecords("arai_blog_comment_likes", array('user_id' => $user_id), 'comment_id');

        $like_arr = $self_comment_arr = $return_arr = array();
        if (count($user_like_comment) > 0) {
            foreach ($user_like_comment as $res) {
                $like_arr[$res['comment_id']] = $res['comment_id'];
            }
        }

        $user_self_comment = $this->master_model->getRecords("arai_blog_comments", array('user_id' => $this->session->userdata('user_id')), 'comment_id');
        if (count($user_self_comment) > 0) {
            foreach ($user_self_comment as $res) {
                $self_comment_arr[$res['comment_id']] = $res['comment_id'];
            }
        }
        $return_arr['like_comments'] = $like_arr;
        $return_arr['self_comments'] = $self_comment_arr;

        return $return_arr;
    }

    public function blogDetails($blog_id = 0, $comment_flag = 0) //BLOG DETAILS PAGE

    {
        $this->check_permissions->is_logged_in();
        $module_id = 25;
        $this->check_permissions->is_authorise($module_id);

        $encrptopenssl         = new Opensslencryptdecrypt();
        $data['user_id']       = $user_id       = $this->session->userdata('user_id');
        $data['comment_limit'] = $comment_limit = 5;
        $data['comment_flag']  = $comment_flag;

        if ($blog_id == '0') {redirect(site_url('blogs_technology_wall'));} else {
            $blog_id = base64_decode($blog_id);

            $data['blog_id'] = $blog_id;

            $select = "b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about,b.tag_other, b.admin_status, b.is_block, b.created_on, (SELECT GROUP_CONCAT(technology_name SEPARATOR '##') FROM arai_blog_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR ', ') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='Other')  AS DispTags, r.user_category_id, org.org_logo, sp.profile_picture, sp.designation";
            $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
            $data['form_data'] = $form_data = $this->master_model->getRecords("arai_blog b", array('b.blog_id' => $blog_id, 'b.is_deleted' => '0'), $select); //, 'b.admin_status' => '1', 'b.is_block' => '0'

            //echo "<pre>"; print_r($form_data); echo "</pre>";

            $redirect_flag = 0;
            if (count($form_data) == 0) {$redirect_flag = 1;} else if ($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;} else if ($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;}

            if ($redirect_flag == 1) {redirect(site_url('blogs_technology_wall'));}
        }

        $data['BlogActiondata'] = $this->getBlogActiondata($user_id);
        $data['page_title']     = 'Blog Details';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'blogs_technology_wall/blog_details';
        $this->load->view('front/front_combo', $data);
    }

    public function report_blog_ajax() // REPORT BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id                = trim($this->security->xss_clean(base64_decode($this->input->post('blog_id'))));
            $popupBlogReportComment = trim($this->security->xss_clean($this->input->post('popupBlogReportComment')));
            $user_id                = $this->session->userdata('user_id');
            $result['flag']         = "success";
            $result['blog_id']      = $blog_id;

            $action_name                = "Reported Blog Technology Wall";
            $add_reported['blog_id']    = $blog_id;
            $add_reported['comments']   = $popupBlogReportComment;
            $add_reported['user_id']    = $user_id;
            $add_reported['created_on'] = date('Y-m-d H:i:s');
            $this->master_model->insertRecord('arai_blogs_reported', $add_reported);

            //START : SEND MAIL TO USER WHEN BLOG IS REPORTED - blog_mail_to_user_on_reported
            $email_info = $this->get_mail_data_blog($blog_id);

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('blog_mail_to_user_on_reported');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[Blog_Title]',
                    '[USER_NAME]',
                    '[reason]',
                ];
                $rep_array = [
                    $email_info['Blog_Title'],
                    $email_info['USER_NAME'],
                    $popupBlogReportComment,
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['user_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO USER WHEN BLOG IS REPORTED - blog_mail_to_user_on_reported

            //START : SEND MAIL TO ADMIN WHEN BLOG IS REPORTED - blog_mail_to_admin_on_reported
            $email_info = $this->get_mail_data_blog($blog_id, $user_id);

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('blog_mail_to_admin_on_reported');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[Blog_Title]',
                    '[USER_NAME]',
                    '[USER_ID]',
                    '[Commenting_User]',
                    '[reason]',
                    '[LINK_OF_POST]',
                ];
                $rep_array = [
                    $email_info['Blog_Title'],
                    $email_info['USER_NAME'],
                    $email_info['USER_ID'],
                    $email_info['Commenting_User'],
                    $popupBlogReportComment,
                    $email_info['LINK_OF_POST'],
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['user_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO ADMIN WHEN BLOG IS REPORTED - blog_mail_to_admin_on_reported

            $Reported_label = 'Blog Already Reported';
            $ReportedFlag   = '0';

            $onclick_fun        = "report_blog('" . base64_encode($blog_id) . "','" . $ReportedFlag . "', '', '')";
            $result['response'] = '<a class="btn btn-sm btn-danger btn_report" href="javascript:void(0)"  title="' . $Reported_label . '" onclick="' . $onclick_fun . '">Report</a>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function post_comment_ajax()
    {
        $this->check_permissions->is_logged_in();
        $encObj = new Opensslencryptdecrypt();

        if (isset($_POST) && count($_POST) > 0) {
            $result['flag']  = "success";
            $user_id         = trim($this->security->xss_clean($this->input->post('user_id')));
            $blog_id         = trim($this->security->xss_clean($this->input->post('blog_id')));
            $comment_id      = trim($this->security->xss_clean($this->input->post('comment_id')));
            $comment_content = trim($this->security->xss_clean($this->input->post('comment_content')));

            $add_data['blog_id']           = $blog_id;
            $add_data['user_id']           = $user_id;
            $add_data['parent_comment_id'] = $comment_id;
            $add_data['comment']           = $comment_content;
            $add_data['status']            = 1;
            $add_data['created_on']        = date('Y-m-d H:i:s');

            $result['new_comment_id'] = $last_inserted_comment = $this->master_model->insertRecord('arai_blog_comments', $add_data, true);

            if ($comment_id == 0) {
                $email_info = $this->get_mail_data_blog($blog_id, $user_id);

                //START : SEND MAIL TO USER WHEN COMMENT IS POSTED ON BLOG - blog_mail_to_user_on_comment_posted
                $email_send      = '';
                $slug            = $encObj->encrypt('blog_mail_to_user_on_comment_posted');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content     = '';

                if (count($subscriber_mail) > 0) {
                    $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                    $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                    $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                    $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                    $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                    $arr_words = [
                        '[USER_NAME]',
                        '[Blog_Title]',
                        '[Commenting_User]',
                        '[Blog_Date]',
                        '[LINK_OF_POST]',
                    ];
                    $rep_array = [
                        $email_info['USER_NAME'],
                        $email_info['Blog_Title'],
                        $email_info['Commenting_User'],
                        $email_info['Blog_Date'],
                        $email_info['LINK_OF_POST'],
                    ];
                    $sub_content = str_replace($arr_words, $rep_array, $desc);

                    $info_array = array(
                        'to'      => $email_info['USER_EMAIL'],
                        'cc'      => '',
                        'from'    => $fromadmin,
                        'subject' => $subject_title,
                        'view'    => 'common-file',
                    );

                    $other_infoarray = array('content' => $sub_content);
                    $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                }
                //END : SEND MAIL TO USER WHEN COMMENT IS POSTED ON BLOG - blog_mail_to_user_on_comment_posted
            } else if ($comment_id != '0') {
                $parent_comment_data = $this->master_model->getRecords("arai_blog_comments", array("comment_id" => $comment_id));
                if (count($parent_comment_data) > 0) {
                    $email_info = $this->get_mail_data_blog($blog_id, $parent_comment_data[0]['user_id']);

                    //START : SEND MAIL TO COMMENTING USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_commenting_user_on_reply_posted
                    $email_send      = '';
                    $slug            = $encObj->encrypt('blog_mail_to_commenting_user_on_reply_posted');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[Commenting_User]',
                            '[Blog_Title]',
                            '[LINK_OF_POST]',
                        ];
                        $rep_array = [
                            $email_info['Commenting_User'],
                            $email_info['Blog_Title'],
                            $email_info['LINK_OF_POST'],
                        ];
                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['USER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    //END : SEND MAIL TO COMMENTING USER WHEN REPLY IS POSTED ON BLOG COMMENT  - blog_mail_to_commenting_user_on_reply_posted

                    //START : SEND MAIL TO USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_user_on_reply_received
                    $email_send      = '';
                    $slug            = $encObj->encrypt('blog_mail_to_user_on_reply_received');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[USER_NAME]',
                            '[Blog_Title]',
                            '[LINK_OF_THE_POST]',
                        ];
                        $rep_array = [
                            $email_info['USER_NAME'],
                            $email_info['Blog_Title'],
                            $email_info['LINK_OF_POST'],
                        ];
                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['USER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    //END : SEND MAIL TO USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_user_on_reply_received
                }
            }

            //THIS IS FOR COMMENT REPLY
            $comment_reply_cnt               = $this->master_model->getRecordCount("arai_blog_comments", array('status' => '1', 'parent_comment_id' => $comment_id), 'comment_id');
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_id . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $comment_reply_cnt . ')</p>
				</span>';

            $select = "bc.comment_id, bc.blog_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture";
            $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
            $comment_data = $this->master_model->getRecords("arai_blog_comments bc", array('bc.comment_id' => $last_inserted_comment), $select, '', 0, 1);

            $CommentActiondata = $this->getBlogCommentsActiondata($user_id);

            $response_html = '';
            $iconImg       = base_url('assets/no-img.png');
            if ($comment_data[0]['user_category_id'] == 2) {
                if ($comment_data[0]['org_logo'] != "") {$iconImg = base_url('uploads/organization_profile/' . $encObj->decrypt($comment_data[0]['org_logo']));}
            } else {
                if ($comment_data[0]['profile_picture'] != "") {$iconImg = base_url('assets/profile_picture/' . $comment_data[0]['profile_picture']);}
            }

            $title       = $encObj->decrypt($comment_data[0]['title']);
            $first_name  = $encObj->decrypt($comment_data[0]['first_name']);
            $middle_name = $encObj->decrypt($comment_data[0]['middle_name']);
            $last_name   = $encObj->decrypt($comment_data[0]['last_name']);

            $disp_name = $title . " " . $first_name;
            if ($middle_name != '') {$disp_name .= " " . $middle_name . " ";}
            $disp_name .= $last_name;

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['like_comments'])) {
                $Like_label = "Like";
                $LikeFlag   = '0';
                $likeIcon   = 'fa-thumbs-up';
                $bg_color   = "style='color:#1562a6; font-weight:500;'";} else {
                $Like_label = "Like";
                $LikeFlag   = '1';
                $likeIcon   = 'fa-thumbs-o-up';
                $bg_color   = "style='color:#000'";}

            $onclick_like_dislike_fun = "like_dislike_comment('" . base64_encode($comment_data[0]['comment_id']) . "','" . $LikeFlag . "')";
            $onclick_del_fun          = "delete_comment('" . base64_encode($comment_data[0]['comment_id']) . "', 'reply')";

            $response_html .= '	<div id="append_reply_div_' . $comment_id . '"></div>
        	<div class="comment_block_common comment_block_common_reply" id="comment_block_' . $comment_data[0]['comment_id'] . '">
        		<div class="comment_img"><img src="' . $iconImg . '"></div>
        			<div class="comment_inner">
        				<p class="comment_name">' . $disp_name . '</p>
        				<p class="comment_time">' . $this->Common_model_sm->time_Ago(strtotime($comment_data[0]['created_on']), $comment_data[0]['created_on']) . '</p>
        				<p class="comment_content">' .nl2br($comment_data[0]['comment']) . '</p>
        				<span id="like_unlike_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
        					<p class="comment_like_btn" onclick="' . $onclick_like_dislike_fun . '" ' . $bg_color . '><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $comment_data[0]['total_likes'] . ')</p>
        				</span>';

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['self_comments'])) {
                $response_html .= '	&nbsp;&nbsp;|&nbsp;&nbsp;
							<span id="delete_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
								<p class="comment_like_btn" onclick="' . $onclick_del_fun . '"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
							</span>';
            }
            $response_html .= '	</div><div class="clearfix"></div>
															</div>';
            $result['response'] = $response_html;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_comment_data_ajax()
    {
        $encrptopenssl            = new Opensslencryptdecrypt();
        $result['csrf_new_token'] = $csrf_new_token = $this->security->get_csrf_hash();
        $data['comment_limit']    = $comment_limit    = 5;

        if (isset($_POST) && count($_POST) > 0) {
            $data['blog_id']        = $blog_id        = $this->input->post('blog_id', true);
            $start                  = $this->input->post('start', true);
            $limit                  = $this->input->post('limit', true);
            $data['user_id']        = $user_id        = $this->input->post('user_id', true);
            $data['sort_order']     = $sort_order     = $this->input->post('sort_order', true);
            $data['new_comment_id'] = $new_comment_id = $this->input->post('new_comment_id', true);
            $is_show_more           = $this->input->post('is_show_more', true);

            if ($blog_id != "" && $start != "" && $limit != "" && $user_id != "" && $sort_order != "" && $new_comment_id != "" && $is_show_more != "") {
                $result['flag'] = "success";

                if ($new_comment_id != '0') {$this->db->order_by("FIELD(bc.comment_id, '" . $new_comment_id . "')", "DESC", false);}
                if ($sort_order == 'Newest') {$this->db->order_by('bc.created_on', 'DESC', false);} else if ($sort_order == 'Oldest') {$this->db->order_by('bc.created_on', 'ASC', false);} else if ($sort_order == 'Best') {
                    $this->db->order_by('bc.total_likes', 'DESC', false);
                    $this->db->order_by('bc.created_on', 'DESC', false);}

                $select = "bc.comment_id, bc.blog_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture,
					(SELECT COUNT(bc2.comment_id) FROM arai_blog_comments bc2 INNER JOIN arai_registration r2 ON r2.user_id = bc2.user_id WHERE bc2.parent_comment_id = bc.comment_id AND bc2.status = 1 AND r2.is_verified = 'yes' AND r2.status = 'Active' AND r2.is_valid = '1' AND r2.is_deleted = '0') AS TotalReply";
                $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
                $data['comment_data']       = $this->master_model->getRecords("arai_blog_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.blog_id' => $blog_id, 'r.is_verified' => "yes", 'r.status' => "Active", 'r.is_valid' => "1", 'r.is_deleted' => "0"), $select, '', $start, $limit);
                $result['comment_data_qry'] = $this->db->last_query();

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
                $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
                $data['comment_data_count'] = $comment_data_count = $this->master_model->getRecordCount("arai_blog_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.blog_id' => $blog_id, 'r.is_verified' => "yes", 'r.status' => "Active", 'r.is_valid' => "1", 'r.is_deleted' => "0"), 'bc.comment_id');
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT

                $data['blog_data']           = $this->master_model->getRecords("arai_blog", array('is_deleted' => '0', 'blog_id' => $blog_id));
                $data['new_start']           = $new_start           = $start + $limit;
                $data['CommentActiondata']   = $this->getBlogCommentsActiondata($user_id);
                $result['response']          = $this->load->view('front/blogs_technology_wall/inc_comment_section', $data, true);
                $result['total_comment_cnt'] = $comment_data_count;
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function delete_comment_ajax() // DELETE BLOG COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $comment_type         = trim($this->security->xss_clean($this->input->post('comment_type')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $del_data['status']     = 2;
            $del_data['deleted_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('arai_blog_comments', $del_data, array('comment_id' => $comment_id));

            $new_comment_reply_cnt = $parent_comment_id = 0;
            if ($comment_type == 'reply') {
                $comment_data = $this->master_model->getRecords("arai_blog_comments", array('comment_id' => $comment_id), 'parent_comment_id', '', 0, 1);

                $new_comment_reply_cnt = $this->master_model->getRecordCount("arai_blog_comments", array('status' => '1', 'parent_comment_id' => $comment_data[0]['parent_comment_id']), 'comment_id');
                $parent_comment_id     = $comment_data[0]['parent_comment_id'];
            }
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_data[0]['parent_comment_id'] . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $new_comment_reply_cnt . ')</p>';
            $result['parent_comment_id']     = $parent_comment_id;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete Blog Comment Technology Wall",
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            //$this->session->set_flashdata('success_blog_comment_del_msg','Blog Comment has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_mail_data_blog($blog_id, $commenting_user_id = false)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,blog_id_disp,blog_title,blog.created_on,registration.user_id');
        $this->db->join('registration', 'blog.user_id=registration.user_id', 'LEFT');
        $blog_details = $this->master_model->getRecords('blog ', array('blog_id' => $blog_id));

        if (count($blog_details)) {
            $user_arr = array();
            if (count($blog_details)) {
                foreach ($blog_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $user_name             = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $user_id               = $user_arr[0]['user_id'];
            $user_email            = $user_arr[0]['email'];
            $blog_title            = $blog_details[0]['blog_title'];
            $blog_id_disp          = $blog_details[0]['blog_id_disp'];
            $blog_date             = date('d/m/Y', strtotime($blog_details[0]['created_on']));
            $blog_detail_link      = site_url('blogs_technology_wall/blogDetails/') . base64_encode($blog_id);
            $hyperlink_blog_detail = '<a href=' . $blog_detail_link . ' target="_blank">here</a>';

            $email_array = array(
                'USER_NAME'    => $user_name,
                'USER_EMAIL'   => $user_email,
                'USER_ID'      => $user_id,
                'Blog_Title'   => $blog_title,
                'blog_id_disp' => $blog_id_disp,
                'Blog_Date'    => $blog_date,
                'admin_email'  => 'test@esds.co.in',
                'LINK_OF_POST' => $hyperlink_blog_detail,
            );

            if ($commenting_user_id != "") {
                $this->db->select('title,first_name,middle_name,last_name,email');
                $commenting_user_data = $this->master_model->getRecords("registration", array('user_id' => $commenting_user_id));

                $commenting_user_arr = array();
                if (count($commenting_user_data)) {
                    foreach ($commenting_user_data as $row_val) {
                        $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                        $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                        $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                        $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                        $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                        $commenting_user_arr[]  = $row_val;
                    }
                }

                $commenting_user_name                 = $commenting_user_arr[0]['title'] . " " . $commenting_user_arr[0]['first_name'] . " " . $commenting_user_arr[0]['last_name'];
                $email_array['Commenting_User']       = $commenting_user_name;
                $email_array['commenting_user_email'] = $commenting_user_arr[0]['email'];
            }

            return $email_array;
        }
    }

    public function getNumData($query) // CALCULATE NUM ROWS

    {
        //echo $query;
        return $rowCount = $this->db->query($query)->num_rows();
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
