<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Knowledge_repository extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        $this->login_user_id = $this->session->userdata('user_id');
        // if($this->login_user_id == "") { redirect(site_url()); }

        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {
        //$module_id = 8;
        //$this->check_permissions->is_authorise($module_id);

        //ini_set('display_errors', '1');
        //ini_set('display_startup_errors', '1');
        //error_reporting(E_ALL);

        $this->check_permissions->is_logged_in();
        $encrptopenssl              = new Opensslencryptdecrypt();
        $data['featured_limit']     = $featured_limit     = 15;
        $data['non_featured_limit'] = $non_featured_limit = 9;

        $this->db->order_by('xOrder', 'ASC');
        $this->db->order_by("FIELD(type_name, 'other')", "DESC", false);
        $data['type_data'] = $this->master_model->getRecords("knowledge_repository_type_master", array("status" => 'Active'));

        // $this->db->order_by('xOrder', 'ASC');
        $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $data['technology_data'] = $this->master_model->getRecords("arai_knowledge_repository_technology_master", array("status" => 'Active'));

        $data['tag_data'] = $this->master_model->getRecords("knowledge_repository_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['page_title']     = 'Knowledge Rpository';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'knowledge_repository/index';
        $this->load->view('front/front_combo', $data);
    }

    public function getKrDataAjax()
    {
        $encrptopenssl                           = new Opensslencryptdecrypt();
        $csrf_new_token                          = $this->security->get_csrf_hash();
        $result['csrf_new_token']                = $csrf_new_token;
        $data_non_featured['featured_limit']     = $featured_limit     = 15;
        $data_non_featured['non_featured_limit'] = $non_featured_limit = 9;
        $search_str                              = '';

        if (isset($_POST) && count($_POST) > 0) {
            $f_start                                   = $this->input->post('f_start', true);
            $f_limit                                   = $this->input->post('f_limit', true);
            $nf_start                                  = $this->input->post('nf_start', true);
            $nf_limit                                  = $this->input->post('nf_limit', true);
            $is_show_more                              = $this->input->post('is_show_more', true);
            $data_non_featured['kr_type_id_load_more'] = $this->input->post('kr_type_id_load_more', true);
            if ($f_start != "" && $f_limit != "" && $nf_start != "" && $nf_limit != "" && $is_show_more != "") {
                $result['flag'] = "success";
                $user_id        = $this->session->userdata('user_id');
                $BlogActiondata = $this->getBlogActiondata($user_id);

                //START : FOR SEARCH KEYWORD
                $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword'));
                if ($keyword) {
                    $search_str .= " ( k.kr_id_disp LIKE '%" . $keyword . "%' OR k.title_of_the_content LIKE '%" . $keyword . "%' OR k.kr_description LIKE '%" . $keyword . "%' OR k.kr_type_other LIKE '%" . $keyword . "%' OR k.author_name LIKE '%" . $keyword . "%' ) ";
                }
                //END : FOR SEARCH KEYWORD

                //START : FOR SEARCH TYPE
                $type = trim($this->input->post('types'));
                if ($type != '') {

                    $type_arr = explode(",", $type);
                    if (count($type_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($type_arr as $res) {
                            $search_str .= " k.kr_type =  " . $res . "  ";
                            if ($i != (count($type_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }

                }

                $technology = trim($this->input->post('technology'));
                if ($technology != '') {
                    $technology_arr = explode(",", $technology);
                    if (count($technology_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($technology_arr as $res) {
                            $search_str .= " k.technology_ids =  " . $res . "  ";
                            if ($i != (count($technology_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TYPE

                //START : FOR SEARCH TAG
                $tag = trim($this->input->post('tag'));
                if ($tag != '') {
                    $tag_arr = explode(",", $tag);
                    if (count($tag_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($tag_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',k.tags)";
                            if ($i != (count($tag_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH DATE
                $from_date = trim($this->input->post('from_date'));
                $to_date   = trim($this->input->post('to_date'));

                if ($from_date != '' && $to_date != '') {

                    $from_date = date('Y-m-d', strtotime($from_date));
                    $to_date   = date('Y-m-d', strtotime($to_date));

                    $old_search_str = $search_str;
                    if ($old_search_str != '') {$search_str .= " AND ";}
                    $search_str .= "( DATE(k.created_on) BETWEEN '" . $from_date . "' and '" . $to_date . "' )";
                }

                // START : FOR FEATURED
                $featured_blog_total_cnt = 0;
                if ($is_show_more == 0) {
                    if ($search_str != "") {$this->db->where($search_str);}
                    // $this->db->order_by('kr_id','DESC',FALSE);
                    // $this->db->order_by('k.xOrder','ASC',FALSE);
                    $this->db->order_by('ISNULL(k.xOrder), k.xOrder = 0, k.xOrder, k.created_on DESC', '', false);
                    $select_f = "rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags,k.tag_other, k.author_name, k.created_on,k.technology_other,(SELECT GROUP_CONCAT(technology_name SEPARATOR '##') FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                    $this->db->join('arai_knowledge_repository_type_master rt', 'rt.id = k.kr_type', 'LEFT', false);
                    $this->db->join('arai_registration r', 'r.user_id = k.user_id', 'LEFT', false);
                    $this->db->join('arai_profile_organization org', 'org.user_id = k.user_id', 'LEFT', false);
                    $this->db->join('arai_student_profile sp', 'sp.user_id = k.user_id', 'LEFT', false);
                    $data_featured['featured_kr'] = $featured_kr = $this->master_model->getRecords("knowledge_repository k", array(' k.is_deleted' => '0', 'k.admin_status' => '1', 'k.is_featured' => '1', 'k.is_block' => '0'), $select_f, '', $f_start, $f_limit);

                    $featured_kr_total_cnt           = count($featured_kr);
                    $data_featured['BlogActiondata'] = $BlogActiondata;
                    $result['featured_kr_qry']       = $this->db->last_query();
                    $result['Featured_response']     = $this->load->view('front/knowledge_repository/incFeatureKrCommon', $data_featured, true);
                }

                // START : FOR FEATURED

                // START : FOR NON FEATURED
                if ($search_str != "") {$this->db->where($search_str);}
                $featured_kr = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.is_featured' => '1', 'k.is_block' => '0'));

                $featured_kr_total_cnt = count($featured_kr);

                $search_str_type = '';
                if ($type != '') {

                    $type_arr_filter = explode(",", $type);
                    if (count($type_arr_filter) > 0) {

                        $search_str_type .= " (";
                        $i = 0;
                        foreach ($type_arr_filter as $res_filter) {
                            $search_str_type .= " id =  " . $res_filter . "  ";
                            if ($i != (count($type_arr_filter) - 1)) {$search_str_type .= " OR ";}
                            $i++;
                        }
                        $search_str_type .= " )";
                    }

                }
                if ($search_str_type != '') {
                    $this->db->where($search_str_type);
                }
                $this->db->order_by('xOrder', 'ASC');
                $this->db->order_by("FIELD(type_name, 'other')", "DESC", false);
                $data_non_featured['type_data'] = $this->master_model->getRecords("knowledge_repository_type_master", array("status" => 'Active'));

                /*if($search_str != "") { $this->db->where($search_str); }
                $this->db->order_by('kr_id','DESC',FALSE);
                $select_nf = "k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags, k.author_name, k.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                $this->db->join('arai_registration r','r.user_id = k.user_id', 'LEFT', FALSE);
                $this->db->join('arai_profile_organization org','org.user_id = k.user_id', 'LEFT', FALSE);
                $this->db->join('arai_student_profile sp','sp.user_id = k.user_id', 'LEFT', FALSE);
                $data_non_featured['non_featured_kr'] = $this->master_model->getRecords("knowledge_repository k",array('k.is_deleted' => '0', 'k.admin_status' => '0', 'k.is_featured' => '0', 'k.is_block' => '0'),$select_nf,'',$nf_start,$nf_limit);
                 */
                $data_non_featured['BlogActiondata'] = $BlogActiondata;
                $result['non_featured_kr_qry']       = $this->db->last_query();

                $result['featured_kr_total_cnt'] = $featured_kr_total_cnt;

                $data_non_featured['is_show_more']          = $is_show_more;
                $data_non_featured['nf_start']              = $nf_start;
                $data_non_featured['nf_limit']              = $nf_limit;
                $data_non_featured['search_str']            = $search_str;
                $data_non_featured['featured_kr_total_cnt'] = $featured_kr_total_cnt;

                $data_non_featured['new_start'] = $new_start = $nf_start + $nf_limit;
                $result['NonFeatured_response'] = $this->load->view('front/knowledge_repository/incNonFeatureKrCommon', $data_non_featured, true);
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function my_knowledge_repository() // MY BLOGS LIST

    {
        $this->check_permissions->is_logged_in();
        $user_id                = $this->session->userdata('user_id');
        $data['page_title']     = 'Knowledge Repository';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'knowledge_repository/listing';
        $this->load->view('front/front_combo', $data);
    }

    public function my_kr_listing() // MY BLOGS LIST : GET SERVER SIDE DATATABLE DATA USING AJAX

    {
        //$this->check_permissions->is_logged_in();
        $csrf_test_name  = $this->security->get_csrf_hash();
        $user_id         = $this->session->userdata('user_id');
        $draw            = $_POST['draw'];
        $row             = $_POST['start'];
        $rowperpage      = $_POST['length']; // Rows display per page
        $columnIndex     = @$_POST['order'][0]['column']; // Column index
        $columnName      = @$_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = @$_POST['order'][0]['dir']; // asc or desc
        $searchValue     = @$_POST['search']['value']; // Search value
        $pageNo          = ($_POST['start'] / $_POST['length']) + 1;

        $condition = "SELECT * FROM arai_knowledge_repository LEFT JOIN arai_knowledge_repository_type_master ON arai_knowledge_repository.kr_type=arai_knowledge_repository_type_master.id WHERE is_deleted = '0' AND user_id = '" . $user_id . "' ORDER BY kr_id DESC";
        $query     = $condition . ' LIMIT  ' . $this->input->post('length') . ' OFFSET ' . $this->input->post('start');
        $result    = $this->db->query($query)->result();
        $rowCount  = $this->getNumData($condition);
        $rowCnt    = 0;
        $dataArr   = array();

        if ($rowCount > 0) {
            $no = $_POST['start'];
            $i  = 0;
            foreach ($result as $showResult) {
                $kr_id                = $showResult->kr_id;
                $kr_id_disp           = '<a href="' . base_url('Knowledge_repository/details/' . base64_encode($kr_id)) . '" class="applicant-listing">' . $showResult->kr_id_disp . '</a>';
                $title_of_the_content = ucwords($showResult->title_of_the_content);
                $category             = ucwords($showResult->type_name);
                $author_name          = $showResult->author_name;
                $posted_on            = date("d M, Y h:i a", strtotime($showResult->created_on));

                $admin_status = $action = '';

                if ($showResult->admin_status == 0) {$admin_status = 'Pending';} else if ($showResult->admin_status == 1) {$admin_status = 'Approved';} else if ($showResult->admin_status == 2) {$admin_status = 'Rejected';}

                $onclick_like_fun = "show_blog_likes('" . base64_encode($kr_id) . "')";
                $getTotalLikes    = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $kr_id), 'like_id');
                $disp_likes       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';

                $onclick_download_fun = "show_downloads('" . base64_encode($kr_id) . "')";
                $getTotalDownl        = $this->master_model->getRecordCount('arai_knowledge_download_log', array('kr_id' => $kr_id), 'd_id');
                $disp_downloads       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_download_fun . '">' . $getTotalDownl . '</a></div>';

                $onclick_report_fun = "show_blog_reports('" . base64_encode($kr_id) . "')";
                $getTotalReports    = $this->master_model->getRecordCount('arai_knowledge_repository_reported', array('kr_id' => $kr_id), 'reported_id');

                $disp_reported = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalReports . '</a></div>';

                $action .= '<a href="' . base_url('Knowledge_repository/add/' . base64_encode($kr_id)) . '" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                if ($showResult->is_block == '0') {
                    $b_title = 'Delete';
                    $b_icon  = 'fa fa-ban';} else if ($showResult->is_block == '1') {
                    $b_title = 'Restore';
                    $b_icon  = 'fa fa-unlock-alt';}
                $onclick_block_fun = "block_unblock_kr('" . base64_encode($kr_id) . "', '" . $b_title . "')";

                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="' . $b_title . '" class="btn btn-info btn-green-fresh" onclick="' . $onclick_block_fun . '"><i class="' . $b_icon . '" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                if ($showResult->is_block == '1') {
                    $onclick_delete_fun = " delete_kr_new('" . base64_encode($kr_id) . "','$showResult->kr_id_disp','$showResult->title_of_the_content') ";
                } else {
                    $onclick_delete_fun = "preventDelete()";
                }

                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" class="btn btn-info btn-green-fresh" onclick="' . $onclick_delete_fun . '"><i class="fa fa-remove" aria-hidden="true"></i></a> ';

                $i++;
                $dataArr[] = array(
                    $kr_id_disp,
                    $title_of_the_content,
                    // $author_name,
                    $posted_on,
                    $admin_status,
                    $category,
                    $disp_likes,
                    $disp_downloads,
                    $disp_reported,
                    "<span style='white-space:nowrap;'>" . $action . "</span>",
                );
                $rowCnt   = $rowCount;
                $response = array(
                    "draw"            => $draw,
                    "recordsTotal"    => $rowCnt ? $rowCnt : 0,
                    "recordsFiltered" => $rowCnt ? $rowCnt : 0,
                    "data"            => $dataArr,
                );
                $response['token'] = $csrf_test_name;
            }
        } else {
            $rowCnt   = $rowCount;
            $response = array(
                "draw"            => $draw,
                "recordsTotal"    => 0,
                "recordsFiltered" => 0,
                "data"            => $dataArr,
            );
            $response['token'] = $csrf_test_name;
        }
        echo json_encode($response);

    } // End Function

    public function add($id = 0) // ADD NEW BLOG

    {
        $this->check_permissions->is_logged_in();

        $module_id = 30;
        $this->check_permissions->is_authorise($module_id);

        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("knowledge_repository", array('kr_id' => $id, 'user_id' => $user_id, 'is_deleted' => 0));
            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else { $data['mode'] = $mode = "Add";}

            $data['kr_files_data'] = $kr_files_data = $this->master_model->getRecords('knowledge_repository_files', array("kr_id" => $id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));
        }

        $data['kr_banner_error'] = $data['technology_ids_error'] = $data['kr_type_error'] = $data['tags_error'] = $error_flag = '';
        $file_upload_flag        = 0;
        if (isset($_POST) && count($_POST) > 0) {

            //$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');
            $this->form_validation->set_rules('title_of_the_content', 'knowledge repository title', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('kr_description', 'knowledge repository description', 'required', array('required' => 'Please enter the %s'));

            if ($mode == 'Add' && ((!isset($_FILES['kr_banner'])) || $_FILES['kr_banner']['size'] == 0)) {
                $data['kr_banner_error'] = 'Please select the Banner';
                $error_flag              = 1;
            }

            if (!isset($_POST['kr_type'])) {
                $data['kr_type_error'] = 'Please select the Blog Technology';
                $error_flag            = 1;
            }

            if (!isset($_POST['tags'])) {
                $data['tags_error'] = 'Please select the Tags';
                $error_flag         = 1;
            }

            if (!isset($_POST['technology_ids'])) {
                $data['technology_ids_error'] = 'Please select the Technology Domain';
                $error_flag                   = 1;
            }

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['kr_banner']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $kr_banner = $this->Common_model_sm->upload_single_file("kr_banner", array('png', 'jpg', 'jpeg', 'gif'), "kr_banner_" . date("YmdHis"), "./uploads/kr_banner", "png|jpeg|jpg|gif");
                    if ($kr_banner['response'] == 'error') {
                        $data['kr_banner_error'] = $kr_banner['message'];
                        $file_upload_flag        = 1;
                    } else if ($kr_banner['response'] == 'success') {
                        $add_data['kr_banner'] = $kr_banner['message'];
                        /* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
                    }
                }

                if ($file_upload_flag == 0) {
                    $add_data['user_id']              = $user_id;
                    $add_data['title_of_the_content'] = $this->input->post('title_of_the_content');
                    $add_data['kr_description']       = $this->input->post('kr_description');
                    $add_data['kr_type']              = $this->input->post('kr_type');
                    $add_data['technology_ids']       = implode(",", $this->input->post('technology_ids'));

                    $add_data['terms_condition_check'] = $this->input->post('terms_condition_check');

                    $terms_condition = trim($this->security->xss_clean($this->input->post('terms_condition')));
                    if (isset($terms_condition) && $terms_condition != "") {
                        $add_data['terms_condition'] = trim($this->security->xss_clean($terms_condition));
                    } else { $add_data['terms_condition'] = '';}

                    $kr_type_other = trim($this->security->xss_clean($this->input->post('kr_type_other')));
                    if (isset($kr_type_other) && $kr_type_other != "") {
                        $add_data['kr_type_other'] = trim($this->security->xss_clean($kr_type_other));
                        
                        if ($mode=='Add') {
                        $this->check_permissions->StoreOtherData($kr_type_other, 'knowledge_repository_type_master','type_name');
                        }

                    } else { $add_data['kr_type_other'] = '';}

                    $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                    if (isset($tag_other) && $tag_other != "") {
                        $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));

                        if ($mode=='Add') {
                            $this->check_permissions->StoreOtherData($tag_other, 'knowledge_repository_tags_master','tag_name');
                        }
                        /*if ($mode == 'Add') {
                            $other_tags_arr = explode(',', $tag_other);
                            foreach ($other_tags_arr as $key => $other_tag_value) {
                                $is_exist_tag = $this->master_model->getRecords("knowledge_repository_tags_master", array("tag_name" => $other_tag_value));
                                if (count($is_exist_tag) == 0) {
                                    $tag_arr['tag_name'] = $other_tag_value;
                                    $tag_arr['status']   = 'Block';
                                    $tag_arr['user_id']  = $user_id;
                                    $this->master_model->insertRecord("knowledge_repository_tags_master", $tag_arr);
                                }
                            }
                        }*/
                    } else { $add_data['tag_other'] = '';}

                    $technology_ids_other = trim($this->security->xss_clean($this->input->post('technology_ids_other')));
                    if (isset($technology_ids_other) && $technology_ids_other != "") {
                        $add_data['technology_other'] = trim($this->security->xss_clean($technology_ids_other));
                       if ($mode=='Add') {
                        $this->check_permissions->StoreOtherData($technology_ids_other, 'arai_knowledge_repository_technology_master','technology_name');
                        }


                    } else { $add_data['technology_other'] = '';}

                    $add_data['tags']        = implode(",", $this->input->post('tags'));
                    $add_data['author_name'] = trim($this->security->xss_clean($this->input->post('author_name')));

                    if ($mode == 'Add') {
                        $add_data['is_featured']  = 0;
                        $add_data['admin_status'] = 0;
                        $add_data['is_deleted']   = 0;
                        $add_data['is_block']     = 0;
                        $add_data['created_on']   = date('Y-m-d H:i:s');

                        $last_kr_id            = $this->master_model->insertRecord('arai_knowledge_repository', $add_data, true);
                        $up_data['kr_id_disp'] = $kr_id_disp = "TNKID-" . sprintf("%07d", $last_kr_id);
                        $this->master_model->updateRecord('arai_knowledge_repository', $up_data, array('kr_id' => $last_kr_id));

                        $email_info = $this->get_mail_data($last_kr_id);

                        // print_r($email_info);die;

                        // START: MAIL TO USER
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_adding_kr');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[kr_owner_name]',
                                '[kr_title]',
                                '[date]',
                            ];
                            $rep_array = [
                                $email_info['kr_owner_name'],
                                $email_info['kr_title'],
                                $email_info['date'],
                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['kr_owner_email'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                        // END: MAIL TO USER

                        //START: MAIL TO ADMIN

                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('kr_mail_to_admin_after_adding_kr');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[kr_owner_name]',
                                '[kr_owner_id]',
                                '[kr_title]',
                                '[hyperlink_kr_detail]',
                            ];
                            $rep_array = [
                                $email_info['kr_owner_name'],
                                $email_info['kr_owner_id'],
                                $email_info['kr_title'],
                                $email_info['hyperlink_kr_detail'],
                            ];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);

                            $info_array = array(
                                'to'      => $email_info['admin_email'],
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }

                        //END: MAIL TO ADMIN

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Add Knowledge repository",
                            'module_name' => 'Knowledge repository Front',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Content is sent for Admin approval successfully. Your ' . $kr_id_disp);
                    } else if ($mode == 'Update') {
                        $add_data['admin_status'] = 0;
                        $add_data['updated_on']   = date('Y-m-d H:i:s');

                        $kr_id      = $id;
                        $last_kr_id = $id;
                        $this->master_model->updateRecord('arai_knowledge_repository', $add_data, array('kr_id' => $kr_id));
                        //echo $this->db->last_query(); exit;

                        // Log Data Added
                        $filesData        = $_FILES;
                        $json_data        = array_merge($postArr, $filesData);
                        $json_encode_data = json_encode($json_data);
                        $ipAddr           = $this->get_client_ip();
                        $createdAt        = date('Y-m-d H:i:s');
                        $logDetails       = array(
                            'user_id'     => $user_id,
                            'action_name' => "Update Knowledge repository",
                            'module_name' => 'Blog Knowledge repository',
                            'store_data'  => $json_encode_data,
                            'ip_address'  => $ipAddr,
                            'createdAt'   => $createdAt,
                        );
                        $logData = $this->master_model->insertRecord('logs', $logDetails);

                        $this->session->set_flashdata('success', 'Knowledge repository has been successfully updated');
                    }

                    //START : INSERT  FILES
                    if ($last_kr_id != "") {
                        $kr_files = $_FILES['kr_files'];
                        // print_r($kr_files);die;
                        if (count($kr_files) > 0) {
                            for ($i = 0; $i < count($kr_files['name']); $i++) {
                                if ($kr_files['name'][$i] != '') {
                                    $kr_file = $this->Common_model_sm->upload_single_file('kr_files', array('png', 'jpg', 'jpeg', 'gif', 'pdf', 'xls', 'xlsx', 'doc', 'docx', 'pptx', 'ppt'), "kr_files_" . $last_kr_id . "_" . date("YmdHis") . rand(), "./uploads/kr_files", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx|pptx|ppt", '1', $i);

                                    if ($kr_file['response'] == 'error') {} else if ($kr_file['response'] == 'success') {
                                        $add_file_data['user_id']            = $user_id;
                                        $add_file_data['kr_id']              = $last_kr_id;
                                        $add_file_data['file_name']          = ($kr_file['message']);
                                        $add_file_data['file_original_name'] = $kr_files['name'][$i];
                                        $add_file_data['created_on']         = date("Y-m-d H:i:s");
                                        $this->master_model->insertRecord('arai_knowledge_repository_files', $add_file_data, true);

                                        // $kr_files_data[] = $add_file_data; //FOR LOG
                                    }
                                }
                            }

                        }
                    }
                    //END : INSERT  FILES

                    redirect(site_url('knowledge_repository/my_knowledge_repository'));
                }
            }
        }

        // $this->db->order_by('technology_name', 'ASC');
        // $this->db->order_by("FIELD(technology_name, 'Other')", "ASC", false);
        $this->db->order_by('FIELD(technology_name, "Other") ASC, technology_name ASC');
        $data['technology_data'] = $this->master_model->getRecords("arai_knowledge_repository_technology_master", array("status" => 'Active'));

        // $this->db->order_by('type_name', 'ASC');
        // $this->db->order_by("FIELD(type_name, 'Other')", "ASC", false);
        $this->db->order_by('FIELD(type_name, "Other") ASC, type_name ASC');
        $data['type_data'] = $this->master_model->getRecords("knowledge_repository_type_master", array("status" => 'Active'));



        // $this->db->order_by("FIELD(tag_name, 'other')", "DESC", false);
        $this->db->order_by('FIELD(tag_name, "Other") ASC, tag_name ASC');
        $data['tag_data'] = $this->master_model->getRecords("knowledge_repository_tags_master", array("status" => 'Active'));

        $data['page_title']     = 'Blogs';
        $data['submodule_name'] = '';

        $data['middle_content'] = 'knowledge_repository/add';
        $this->load->view('front/front_combo', $data);
    }

    public function delete_kr_file_ajax()
    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        $file_id       = $encrptopenssl->decrypt($this->input->post('file_id', true));

        $csrf_new_token           = $this->security->get_csrf_hash();
        $result['csrf_new_token'] = $csrf_new_token;

        $this->master_model->updateRecord('knowledge_repository_files', array('is_deleted' => '1'), array('file_id' => $file_id));
        $result['flag']    = 'success';
        $result['file_id'] = $file_id;

        //START : INSERT LOG
        $postArr                   = $this->input->post();
        $json_encode_data          = json_encode($postArr);
        $logDetails['user_id']     = $this->login_user_id;
        $logDetails['module_name'] = 'KR : delete_team_file_ajax';
        $logDetails['store_data']  = $json_encode_data;
        $logDetails['ip_address']  = $this->get_client_ip();
        $logDetails['createdAt']   = date('Y-m-d H:i:s');
        $logDetails['action_name'] = 'Delete KR File';
        $logData                   = $this->master_model->insertRecord('logs', $logDetails);
        //END : INSERT LOG

        echo json_encode($result);
    }

    public function get_autor_details() // USED TO GET AUTHOR DETAILS ON ADD/EDIT BLOG PAGE

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $login_user_id = $this->session->userdata('user_id');
            $this->db->select('r.user_id, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, sp.employement_status, sp.other_emp_status');
            $this->db->join('arai_student_profile sp', 'sp.user_id = r.user_id', 'LEFT', false);
            $user_data = $this->master_model->getRecords("arai_registration r", array('r.user_id' => $login_user_id));
            //$result['qry'] = $this->db->last_query();

            $result['flag'] = "success";
            $author_name    = $professional_status    = $org_name    = '';

            if (count($user_data) > 0) {
                $title       = $encrptopenssl->decrypt($user_data[0]['title']);
                $first_name  = $encrptopenssl->decrypt($user_data[0]['first_name']);
                $middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
                $last_name   = $encrptopenssl->decrypt($user_data[0]['last_name']);

                $author_name = $title . " " . $first_name;
                if ($middle_name != '') {$author_name .= " " . $middle_name . " ";}
                $author_name .= $last_name;

                if ($user_data[0]['user_category_id'] == 2) //Organization
                {
                    $professional_status = '';
                    $author_name         = $encrptopenssl->decrypt($user_data[0]['institution_full_name']);
                } else {
                    $professional_status = $encrptopenssl->decrypt($user_data[0]['employement_status']);
                }
            }

            $result['author_name']         = $author_name;
            $result['professional_status'] = $professional_status;
            $result['org_name']            = $org_name;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function block_unblock_kr() // BLOCK/UNBLOCK BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id          = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $type           = trim($this->security->xss_clean($this->input->post('type')));
            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";

            if ($type == 'Delete') {

                $up_data['is_block'] = 1;

            } else if ($type == 'Restore') {$up_data['is_block'] = 0;}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('knowledge_repository', $up_data, array('kr_id' => $kr_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Block knowledge repository",
                'module_name' => 'knowledge repository Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function delete_kr() // DELETE BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id                = trim($this->security->xss_clean(base64_decode($this->input->post('kr_id'))));
            $popupKrDeleteComment = trim($this->security->xss_clean($this->input->post('popupKrDeleteComment')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['kr_id']      = $kr_id;

            $del_data['is_deleted']    = 1;
            $del_data['delete_reason'] = $popupKrDeleteComment;
            $del_data['deleted_on']    = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('knowledge_repository', $del_data, array('kr_id' => $kr_id));

            $email_info = $this->get_mail_data($kr_id);

            // print_r($email_info);die;

            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_deletion_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                    '[reason]',

                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                    $popupKrDeleteComment,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['kr_owner_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO USER

            //START: MAIL TO ADMIN

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_admin_after_user_delete_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                    '[kr_owner_id]',
                    '[reason]',
                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                    $email_info['kr_owner_id'],
                    $popupKrDeleteComment,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['admin_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            //END: MAIL TO ADMIN

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete knowledge repository",
                'module_name' => 'knowledge repository Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success_blog_del_msg', 'Blog has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_blog_ajax() // LIKE - UNLIKE BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag            = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id         = $this->session->userdata('user_id');
            $is_feed         = @$this->input->post('is_feed');
            $result['flag']  = "success";
            $result['kr_id'] = $kr_id;

            if ($flag == 1) {
                $action_name            = "Like KR";
                $add_like['kr_id']      = $kr_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('knowledge_repository_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';
            } else {
                $this->db->where('kr_id', $kr_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('knowledge_repository_likes');

                $action_name = "Un-Like KR";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';
            }
            $getTotalLikes         = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $kr_id), 'like_id');
            $result['total_likes'] = $getTotalLikes;
            if ($is_feed) {
                $onclick_fun = "like_unlike_kr('" . base64_encode($kr_id) . "','" . $LikeFlag . "')";
            } else {
                $onclick_fun = "like_unlike_blog('" . base64_encode($kr_id) . "','" . $LikeFlag . "')";
            }
            $result['response'] = '<span style="cursor:pointer;"  onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . '</span>';

            // $result['total_likes_html'] = '<span style="cursor:auto">'.$getTotalLikes.' &nbsp <small>Likes</small></span>';
            $result['total_likes_html'] = '<span style="cursor:auto;">' . $getTotalLikes . '</span>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'KR Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function like_unlike_comment_ajax() // LIKE - UNLIKE COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $flag                 = trim($this->security->xss_clean($this->input->post('flag')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $comment_data   = $this->master_model->getRecords("arai_blog_comments", array('comment_id' => $comment_id), 'total_likes', '', 0, 1);
            $new_total_like = $comment_data[0]['total_likes'];
            if ($flag == 1) {
                $action_name            = "Like Comment Technology Wall";
                $add_like['comment_id'] = $comment_id;
                $add_like['user_id']    = $user_id;
                $add_like['created_on'] = date('Y-m-d H:i:s');
                $this->master_model->insertRecord('arai_blog_comment_likes', $add_like);

                $LikeFlag   = 0;
                $Like_label = 'Unlike';
                $likeIcon   = 'fa-thumbs-down';

                $new_total_like = $new_total_like + 1;
            } else {
                $this->db->where('comment_id', $comment_id);
                $this->db->where('user_id', $user_id);
                $this->db->delete('arai_blog_comment_likes');

                $action_name = "Un-Like Comment Technology Wall";
                $LikeFlag    = 1;
                $Like_label  = 'Like';
                $likeIcon    = 'fa-thumbs-up';

                $new_total_like = $new_total_like - 1;
            }

            $up_data['total_likes'] = $new_total_like;
            $this->master_model->updateRecord('arai_blog_comments', $up_data, array('comment_id' => $comment_id));

            $onclick_fun        = "like_dislike_comment('" . base64_encode($comment_id) . "','" . $LikeFlag . "')";
            $result['response'] = '<p class="comment_like_btn" onclick="' . $onclick_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $new_total_like . ')' . '</p>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_blog_likes_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']  = "success";
            $result['kr_id'] = $kr_id;

            $this->db->order_by('kr_id', 'DESC', false);
            $select = "bl.like_id, bl.kr_id, bl.user_id, bl.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_knowledge_repository_likes bl", array('bl.kr_id' => $kr_id), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/knowledge_repository/incBlogLikesModal', $data, true);

            $kr_data              = $this->master_model->getRecords("knowledge_repository", array('kr_id' => $kr_id), "title_of_the_content");
            $result['blog_title'] = "Knowledge repository Likes : " . $kr_data[0]['title_of_the_content'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_kr_downloads_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']  = "success";
            $result['kr_id'] = $kr_id;

            $this->db->order_by('kr_id', 'DESC', false);
            $select = "bl.d_id, bl.kr_id, bl.user_id, bl.createdAt, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_knowledge_download_log bl", array('bl.kr_id' => $kr_id), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/knowledge_repository/incKrDownloadsModal', $data, true);

            $kr_data              = $this->master_model->getRecords("knowledge_repository", array('kr_id' => $kr_id), "title_of_the_content");
            $result['blog_title'] = "Knowledge Repository Downloads : " . $kr_data[0]['title_of_the_content'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function show_blog_reported_ajax() // SHOW REPORTED COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id = trim($this->security->xss_clean(base64_decode($this->input->post('kr_id'))));

            $result['flag']  = "success";
            $result['kr_id'] = $kr_id;

            $this->db->order_by('kr_id', 'DESC', false);
            $select = "br.reported_id, br.kr_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = br.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_knowledge_repository_reported br", array('br.kr_id' => $kr_id), $select);
            $result['response'] = $this->load->view('front/knowledge_repository/incBlogReportedModal', $data, true);

            $blog_data            = $this->master_model->getRecords("knowledge_repository", array('kr_id' => $kr_id), "title_of_the_content");
            $result['blog_title'] = "Knowledge Repository Reports : " . $blog_data[0]['title_of_the_content'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getread_more_descriptionAjax() // GET BLOG DESCRIPTION DETAIL

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $blog_id   = base64_decode(trim($this->security->xss_clean($this->input->post('blog_id'))));
            $blog_data = $this->master_model->getRecords("arai_blog", array("blog_id" => $blog_id), 'blog_title, blog_description');

            $result['flag']             = "success";
            $result['blog_title']       = $blog_data[0]['blog_title'];
            $result['blog_description'] = htmlspecialchars_decode($blog_data[0]['blog_description']);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getBlogActiondata($user_id = 0)
    {
        $user_like_blog = $this->master_model->getRecords("knowledge_repository_likes", array('user_id' => $user_id), 'kr_id');

        $like_arr = $self_blog_arr = $reported_arr = $return_arr = array();
        if (count($user_like_blog) > 0) {
            foreach ($user_like_blog as $res) {
                $like_arr[$res['kr_id']] = $res['kr_id'];
            }
        }

        $user_self_blog = $this->master_model->getRecords("knowledge_repository", array('user_id' => $this->session->userdata('user_id')), 'kr_id');
        if (count($user_self_blog) > 0) {
            foreach ($user_self_blog as $res) {
                $self_blog_arr[$res['kr_id']] = $res['kr_id'];
            }
        }

        $user_reported_blog = $this->master_model->getRecords("arai_knowledge_repository_reported", array('user_id' => $user_id), 'kr_id');
        if (count($user_reported_blog) > 0) {
            foreach ($user_reported_blog as $res) {
                $reported_arr[$res['kr_id']] = $res['kr_id'];
            }
        }

        $return_arr['like_blog']     = $like_arr;
        $return_arr['self_blog']     = $self_blog_arr;
        $return_arr['reported_blog'] = $reported_arr;

        return $return_arr;
    }

    public function getBlogCommentsActiondata($user_id = 0)
    {
        $user_like_comment = $this->master_model->getRecords("arai_blog_comment_likes", array('user_id' => $user_id), 'comment_id');

        $like_arr = $self_comment_arr = $return_arr = array();
        if (count($user_like_comment) > 0) {
            foreach ($user_like_comment as $res) {
                $like_arr[$res['comment_id']] = $res['comment_id'];
            }
        }

        $user_self_comment = $this->master_model->getRecords("arai_blog_comments", array('user_id' => $this->session->userdata('user_id')), 'comment_id');
        if (count($user_self_comment) > 0) {
            foreach ($user_self_comment as $res) {
                $self_comment_arr[$res['comment_id']] = $res['comment_id'];
            }
        }
        $return_arr['like_comments'] = $like_arr;
        $return_arr['self_comments'] = $self_comment_arr;

        return $return_arr;
    }

    public function details($kr_id = 0, $comment_flag = 0) //BLOG DETAILS PAGE

    {
        $this->check_permissions->is_logged_in();
        $module_id = 32;
        $this->check_permissions->is_authorise($module_id);

        $encrptopenssl   = new Opensslencryptdecrypt();
        $user_id         = $this->session->userdata('user_id');
        $data['user_id'] = $user_id;

        if ($kr_id == '0') {redirect(site_url('knowledge_repository'));} else {
            $kr_id = base64_decode($kr_id);

            $data['kr_id'] = $kr_id;

            $select_f = "rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags,k.tag_other, k.author_name, k.created_on,k.technology_other,(SELECT GROUP_CONCAT(technology_name SEPARATOR '##') FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture,k.admin_status,k.is_block";
            $this->db->join('arai_knowledge_repository_type_master rt', 'rt.id = k.kr_type', 'LEFT', false);
            $this->db->join('arai_registration r', 'r.user_id = k.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = k.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = k.user_id', 'LEFT', false);
            $data['form_data'] = $form_data = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'k.kr_id' => $kr_id), $select_f);
            if (count($form_data) == 0) {
                redirect(base_url('knowledge_repository'));
            }

            // echo "<pre>"; print_r($data); echo "</pre>";die;

            $redirect_flag = 0;
            if (count($form_data) == 0) {$redirect_flag = 1;} else if ($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;} else if ($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;}

            if ($redirect_flag == 1) {redirect(site_url('knowledge_repository'));}
        }
        $files = $this->master_model->getRecords('knowledge_repository_files', array("kr_id" => $kr_id, "is_deleted" => '0'), 'file_name,file_id');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/kr_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }
        $data['getTotalLikes']  = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $kr_id), 'like_id');
        $data['files_size']     = $this->formatSizeUnits($totalSize);
        $data['files_count']    = count($files);
        $data['BlogActiondata'] = $this->getBlogActiondata($user_id);
        $data['page_title']     = 'Blog Details';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'knowledge_repository/details';
        $this->load->view('front/front_combo', $data);
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function report_blog_ajax() // REPORT BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id                  = trim($this->security->xss_clean(base64_decode($this->input->post('kr_id'))));
            $popupBlogReportComment = trim($this->security->xss_clean($this->input->post('popupBlogReportComment')));
            $user_id                = $this->session->userdata('user_id');
            $result['flag']         = "success";
            $result['kr_id']        = $kr_id;

            $action_name                = "Reported knowledge repository";
            $add_reported['kr_id']      = $kr_id;
            $add_reported['comments']   = $popupBlogReportComment;
            $add_reported['user_id']    = $user_id;
            $add_reported['created_on'] = date('Y-m-d H:i:s');
            $this->master_model->insertRecord('arai_knowledge_repository_reported', $add_reported);

            //START : SEND MAIL TO USER WHEN KR IS REPORTED -
            $email_info = $this->get_mail_data($kr_id);

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_report_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                    '[reason]',
                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                    $popupBlogReportComment,
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['kr_owner_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO USER WHEN KR IS REPORTED

            //START : SEND MAIL TO ADMIN WHEN KR IS REPORTED -

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_admin_after_report_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_title]',
                    '[kr_owner_id]',
                    '[reason]',
                    '[hyperlink_kr_detail]',

                ];
                $rep_array = [
                    $email_info['kr_title'],
                    $email_info['kr_owner_id'],
                    $popupBlogReportComment,
                    $email_info['hyperlink_kr_detail'],
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['admin_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO ADMIN WHEN KR IS REPORTED -

            $Reported_label = 'Knowledge Repository Already Reported';
            $ReportedFlag   = '0';

            $onclick_fun = "report_blog('" . base64_encode($kr_id) . "','" . $ReportedFlag . "', '', '')";
            //$result['response'] = '<a href="javascript:void(0)" title="' . $Reported_label . '" onclick="' . $onclick_fun . '"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>';
            $result['response'] = '<a class="btn btn-sm btn-danger btn_report" href="javascript:void(0)"  title="' . $Reported_label . '" onclick="' . $onclick_fun . '"> Report</a>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'knowledge repository Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function post_comment_ajax()
    {
        $encObj = new Opensslencryptdecrypt();

        if (isset($_POST) && count($_POST) > 0) {
            $result['flag']  = "success";
            $user_id         = trim($this->security->xss_clean($this->input->post('user_id')));
            $blog_id         = trim($this->security->xss_clean($this->input->post('blog_id')));
            $comment_id      = trim($this->security->xss_clean($this->input->post('comment_id')));
            $comment_content = trim($this->security->xss_clean($this->input->post('comment_content')));

            $add_data['blog_id']           = $blog_id;
            $add_data['user_id']           = $user_id;
            $add_data['parent_comment_id'] = $comment_id;
            $add_data['comment']           = $comment_content;
            $add_data['status']            = 1;
            $add_data['created_on']        = date('Y-m-d H:i:s');

            $result['new_comment_id'] = $last_inserted_comment = $this->master_model->insertRecord('arai_blog_comments', $add_data, true);

            if ($comment_id == 0) {
                $email_info = $this->get_mail_data_blog($blog_id, $user_id);

                //START : SEND MAIL TO USER WHEN COMMENT IS POSTED ON BLOG - blog_mail_to_user_on_comment_posted
                $email_send      = '';
                $slug            = $encObj->encrypt('blog_mail_to_user_on_comment_posted');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content     = '';

                if (count($subscriber_mail) > 0) {
                    $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                    $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                    $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                    $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                    $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                    $arr_words = [
                        '[USER_NAME]',
                        '[Blog_Title]',
                        '[Commenting_User]',
                        '[Blog_Date]',
                        '[LINK_OF_POST]',
                    ];
                    $rep_array = [
                        $email_info['USER_NAME'],
                        $email_info['Blog_Title'],
                        $email_info['Commenting_User'],
                        $email_info['Blog_Date'],
                        $email_info['LINK_OF_POST'],
                    ];
                    $sub_content = str_replace($arr_words, $rep_array, $desc);

                    $info_array = array(
                        'to'      => $email_info['USER_EMAIL'],
                        'cc'      => '',
                        'from'    => $fromadmin,
                        'subject' => $subject_title,
                        'view'    => 'common-file',
                    );

                    $other_infoarray = array('content' => $sub_content);
                    $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                }
                //END : SEND MAIL TO USER WHEN COMMENT IS POSTED ON BLOG - blog_mail_to_user_on_comment_posted
            } else if ($comment_id != '0') {
                $parent_comment_data = $this->master_model->getRecords("arai_blog_comments", array("comment_id" => $comment_id));
                if (count($parent_comment_data) > 0) {
                    $email_info = $this->get_mail_data_blog($blog_id, $parent_comment_data[0]['user_id']);

                    //START : SEND MAIL TO COMMENTING USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_commenting_user_on_reply_posted
                    $email_send      = '';
                    $slug            = $encObj->encrypt('blog_mail_to_commenting_user_on_reply_posted');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[Commenting_User]',
                            '[Blog_Title]',
                            '[LINK_OF_POST]',
                        ];
                        $rep_array = [
                            $email_info['Commenting_User'],
                            $email_info['Blog_Title'],
                            $email_info['LINK_OF_POST'],
                        ];
                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['USER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    //END : SEND MAIL TO COMMENTING USER WHEN REPLY IS POSTED ON BLOG COMMENT  - blog_mail_to_commenting_user_on_reply_posted

                    //START : SEND MAIL TO USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_user_on_reply_received
                    $email_send      = '';
                    $slug            = $encObj->encrypt('blog_mail_to_user_on_reply_received');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encObj->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encObj->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encObj->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encObj->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encObj->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[USER_NAME]',
                            '[Blog_Title]',
                            '[LINK_OF_THE_POST]',
                        ];
                        $rep_array = [
                            $email_info['USER_NAME'],
                            $email_info['Blog_Title'],
                            $email_info['LINK_OF_POST'],
                        ];
                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['USER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    //END : SEND MAIL TO USER WHEN REPLY IS POSTED ON BLOG COMMENT - blog_mail_to_user_on_reply_received
                }
            }

            //THIS IS FOR COMMENT REPLY
            $comment_reply_cnt               = $this->master_model->getRecordCount("arai_blog_comments", array('status' => '1', 'parent_comment_id' => $comment_id), 'comment_id');
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_id . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $comment_reply_cnt . ')</p>
                </span>';

            $select = "bc.comment_id, bc.blog_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture, (SELECT COUNT(comment_id) FROM arai_blog_comments WHERE parent_comment_id = bc.comment_id AND status = 1) AS TotalReply";
            $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
            $comment_data = $this->master_model->getRecords("arai_blog_comments bc", array('bc.comment_id' => $last_inserted_comment), $select, '', 0, 1);

            $CommentActiondata = $this->getBlogCommentsActiondata($user_id);

            $response_html = '';
            $iconImg       = base_url('assets/no-img.png');
            if ($comment_data[0]['user_category_id'] == 2) {
                if ($comment_data[0]['org_logo'] != "") {$iconImg = base_url('uploads/organization_profile/' . $encObj->decrypt($comment_data[0]['org_logo']));}
            } else {
                if ($comment_data[0]['profile_picture'] != "") {$iconImg = base_url('assets/profile_picture/' . $comment_data[0]['profile_picture']);}
            }

            $title       = $encObj->decrypt($comment_data[0]['title']);
            $first_name  = $encObj->decrypt($comment_data[0]['first_name']);
            $middle_name = $encObj->decrypt($comment_data[0]['middle_name']);
            $last_name   = $encObj->decrypt($comment_data[0]['last_name']);

            $disp_name = $title . " " . $first_name;
            if ($middle_name != '') {$disp_name .= " " . $middle_name . " ";}
            $disp_name .= $last_name;

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['like_comments'])) {
                $Like_label = "Unlike";
                $LikeFlag   = '0';
                $likeIcon   = 'fa-thumbs-down';} else {
                $Like_label = "Like";
                $LikeFlag   = '1';
                $likeIcon   = 'fa-thumbs-up';}

            $onclick_like_dislike_fun = "like_dislike_comment('" . base64_encode($comment_data[0]['comment_id']) . "','" . $LikeFlag . "')";
            $onclick_del_fun          = "delete_comment('" . base64_encode($comment_data[0]['comment_id']) . "', 'reply')";

            $response_html .= ' <div id="append_reply_div_' . $comment_id . '"></div>
                                                        <div class="comment_block_common comment_block_common_reply" id="comment_block_' . $comment_data[0]['comment_id'] . '">
                                                            <div class="comment_img"><img src="' . $iconImg . '"></div>
                                                                <div class="comment_inner">
                                                                    <p class="comment_name">' . $disp_name . '</p>
                                                                    <p class="comment_time">' . $this->Common_model_sm->time_Ago(strtotime($comment_data[0]['created_on']), $comment_data[0]['created_on']) . '</p>
                                                                    <p class="comment_content">' . $comment_data[0]['comment'] . '</p>
                                                                    <span id="like_unlike_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
                                                                        <p class="comment_like_btn" onclick="' . $onclick_like_dislike_fun . '"><i class="fa ' . $likeIcon . '" aria-hidden="true"></i> ' . $Like_label . ' (' . $comment_data[0]['total_likes'] . ')</p>
                                                                    </span>';

            if (in_array($comment_data[0]['comment_id'], $CommentActiondata['self_comments'])) {
                $response_html .= ' &nbsp;&nbsp;|&nbsp;&nbsp;
                                                                        <span id="delete_comment_btn_outer_' . $comment_data[0]['comment_id'] . '">
                                                                            <p class="comment_like_btn" onclick="' . $onclick_del_fun . '"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
                                                                        </span>';
            }
            $response_html .= ' </div><div class="clearfix"></div>
                                                            </div>';
            $result['response'] = $response_html;
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_comment_data_ajax()
    {
        $encrptopenssl            = new Opensslencryptdecrypt();
        $result['csrf_new_token'] = $csrf_new_token = $this->security->get_csrf_hash();
        $data['comment_limit']    = $comment_limit    = 5;

        if (isset($_POST) && count($_POST) > 0) {
            $data['blog_id']        = $blog_id        = $this->input->post('blog_id', true);
            $start                  = $this->input->post('start', true);
            $limit                  = $this->input->post('limit', true);
            $data['user_id']        = $user_id        = $this->input->post('user_id', true);
            $data['sort_order']     = $sort_order     = $this->input->post('sort_order', true);
            $data['new_comment_id'] = $new_comment_id = $this->input->post('new_comment_id', true);
            $is_show_more           = $this->input->post('is_show_more', true);

            if ($blog_id != "" && $start != "" && $limit != "" && $user_id != "" && $sort_order != "" && $new_comment_id != "" && $is_show_more != "") {
                $result['flag'] = "success";

                if ($new_comment_id != '0') {$this->db->order_by("FIELD(bc.comment_id, '" . $new_comment_id . "')", "DESC", false);}
                if ($sort_order == 'Newest') {$this->db->order_by('bc.created_on', 'DESC', false);} else if ($sort_order == 'Oldest') {$this->db->order_by('bc.created_on', 'ASC', false);} else if ($sort_order == 'Best') {
                    $this->db->order_by('bc.total_likes', 'DESC', false);
                    $this->db->order_by('bc.created_on', 'DESC', false);}

                $select = "bc.comment_id, bc.blog_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture, (SELECT COUNT(comment_id) FROM arai_blog_comments WHERE parent_comment_id = bc.comment_id AND status = 1) AS TotalReply";
                $this->db->join('arai_registration r', 'r.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_profile_organization org', 'org.user_id = bc.user_id', 'LEFT', false);
                $this->db->join('arai_student_profile sp', 'sp.user_id = bc.user_id', 'LEFT', false);
                $data['comment_data']       = $this->master_model->getRecords("arai_blog_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.blog_id' => $blog_id), $select, '', $start, $limit);
                $result['comment_data_qry'] = $this->db->last_query();

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
                $data['comment_data_count'] = $comment_data_count = $this->master_model->getRecordCount("arai_blog_comments bc", array('bc.status' => '1', 'bc.parent_comment_id' => '0', 'bc.blog_id' => $blog_id), 'bc.comment_id');
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT

                $data['blog_data']           = $this->master_model->getRecords("arai_blog", array('is_deleted' => '0', 'blog_id' => $blog_id));
                $data['new_start']           = $new_start           = $start + $limit;
                $data['CommentActiondata']   = $this->getBlogCommentsActiondata($user_id);
                $result['response']          = $this->load->view('front/blogs_technology_wall/inc_comment_section', $data, true);
                $result['total_comment_cnt'] = $comment_data_count;
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function delete_comment_ajax() // DELETE BLOG COMMENT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $comment_id           = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $comment_type         = trim($this->security->xss_clean($this->input->post('comment_type')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['comment_id'] = $comment_id;

            $del_data['status']     = 2;
            $del_data['deleted_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('arai_blog_comments', $del_data, array('comment_id' => $comment_id));

            $new_comment_reply_cnt = $parent_comment_id = 0;
            if ($comment_type == 'reply') {
                $comment_data = $this->master_model->getRecords("arai_blog_comments", array('comment_id' => $comment_id), 'parent_comment_id', '', 0, 1);

                $new_comment_reply_cnt = $this->master_model->getRecordCount("arai_blog_comments", array('status' => '1', 'parent_comment_id' => $comment_data[0]['parent_comment_id']), 'comment_id');
                $parent_comment_id     = $comment_data[0]['parent_comment_id'];
            }
            $result['new_comment_reply_cnt'] = '<p class="comment_like_btn" onclick="show_hide_reply(' . $comment_data[0]['parent_comment_id'] . ')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply (' . $new_comment_reply_cnt . ')</p>';
            $result['parent_comment_id']     = $parent_comment_id;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete Blog Comment Technology Wall",
                'module_name' => 'Blog Technology Wall Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            //$this->session->set_flashdata('success_blog_comment_del_msg','Blog Comment has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function get_mail_data_blog($blog_id, $commenting_user_id = false)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,blog_id_disp,blog_title,blog.created_on,registration.user_id');
        $this->db->join('registration', 'blog.user_id=registration.user_id', 'LEFT');
        $blog_details = $this->master_model->getRecords('blog ', array('blog_id' => $blog_id));

        if (count($blog_details)) {
            $user_arr = array();
            if (count($blog_details)) {
                foreach ($blog_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $user_name             = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $user_id               = $user_arr[0]['user_id'];
            $user_email            = $user_arr[0]['email'];
            $blog_title            = $blog_details[0]['blog_title'];
            $blog_id_disp          = $blog_details[0]['blog_id_disp'];
            $blog_date             = date('d/m/Y', strtotime($blog_details[0]['created_on']));
            $blog_detail_link      = site_url('blogs_technology_wall/blogDetails/') . base64_encode($blog_id);
            $hyperlink_blog_detail = '<a href=' . $blog_detail_link . ' target="_blank">here</a>';

            $email_array = array(
                'USER_NAME'    => $user_name,
                'USER_EMAIL'   => $user_email,
                'USER_ID'      => $user_id,
                'Blog_Title'   => $blog_title,
                'blog_id_disp' => $blog_id_disp,
                'Blog_Date'    => $blog_date,
                'admin_email'  => 'vishal.phadol@esds.co.in',
                'LINK_OF_POST' => $hyperlink_blog_detail,
            );

            if ($commenting_user_id != "") {
                $this->db->select('title,first_name,middle_name,last_name,email');
                $commenting_user_data = $this->master_model->getRecords("registration", array('user_id' => $commenting_user_id));

                $commenting_user_arr = array();
                if (count($commenting_user_data)) {
                    foreach ($commenting_user_data as $row_val) {
                        $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                        $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                        $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                        $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                        $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                        $commenting_user_arr[]  = $row_val;
                    }
                }

                $commenting_user_name                 = $commenting_user_arr[0]['title'] . " " . $commenting_user_arr[0]['first_name'] . " " . $commenting_user_arr[0]['last_name'];
                $email_array['Commenting_User']       = $commenting_user_name;
                $email_array['commenting_user_email'] = $commenting_user_arr[0]['email'];
            }

            return $email_array;
        }
    }

    public function getNumData($query) // CALCULATE NUM ROWS

    {
        //echo $query;
        return $rowCount = $this->db->query($query)->num_rows();
    }

    public function open_download_modal_ajax()
    {

        $data['kr_id'] = $kr_id = $this->input->post('kr_id');

        $select_f = "rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags, k.author_name, k.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags) ) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture,k.admin_status,k.is_block";
        $this->db->join('arai_knowledge_repository_type_master rt', 'rt.id = k.kr_type', 'LEFT', false);
        $this->db->join('arai_registration r', 'r.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_profile_organization org', 'org.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_student_profile sp', 'sp.user_id = k.user_id', 'LEFT', false);
        $data['form_data'] = $form_data = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.kr_id' => $kr_id, 'k.is_block' => '0'), $select_f);
        $data['image']     = $this->create_captcha();

        $files = $this->master_model->getRecords('knowledge_repository_files', array("kr_id" => $kr_id, "is_deleted" => '0'), 'file_name,file_id,file_original_name');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/kr_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }
        $data['files_size']  = $this->formatSizeUnits($totalSize);
        $data['files_count'] = count($files);
        $data['files']       = $files;

        $this->load->view('front/knowledge_repository/download_form', $data);
    }

    public function download_kr($files_id, $kr_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $files_ids     = $files_id;

        if ($files_ids == "") {
            redirect(base_url('knowledge_repository'));
        }
        $this->db->where("file_id IN (" . $files_ids . ")", null, false);
        $files = $this->master_model->getRecords('knowledge_repository_files', '', 'file_name,file_id');
        if (count($files)) {

            $kr_data = $this->master_model->getRecords('knowledge_repository', array("kr_id" => $kr_id, "is_deleted" => '0'));

            $disp_id = $kr_data[0]['kr_id_disp'];

            $user_id = $this->session->userdata('user_id');

            $insert_Arr = array(
                'kr_id'   => $kr_id,
                'user_id' => $user_id,
                'files'   => json_encode($files),
            );
            $this->master_model->insertRecord('arai_knowledge_download_log', $insert_Arr);

            $email_info = $this->get_mail_data($kr_id);

            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_downloading_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['kr_owner_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            // END: MAIL TO USER

            $zip      = new ZipArchive();
            $zip_name = $disp_id . ".zip"; // Zip name
            $zip->open($zip_name, ZipArchive::CREATE);
            foreach ($files as $file) {
                $path = "uploads/kr_files/" . $file['file_name'];
                if (file_exists($path)) {
                    $zip->addFromString(basename($path), file_get_contents($path));
                }

            }
            $zip->close();
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . $zip_name . "\"");
            readfile($zip_name);
            unlink($zip_name);

        } else {
            redirect(base_url('knowledge_repository'));
        }

        // $file_name = 'hgt.pdf';
        //    $file_url = 'assets/' . $file_name;
        //    header('Content-Type: application/octet-stream');
        //    header("Content-Transfer-Encoding: Binary");
        //    header("Content-disposition: attachment; filename=\"".$file_name."\"");
        //    readfile($file_url);

    }

    public function create_captcha($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }

    public function check_captcha_ajax()
    {
        $code = $_POST['code'];

        if ($code == '' || $_SESSION["dwcaptcha"] != $code) {
            //$this->session->unset_userdata("dwcaptcha");
            //$this->form_validation->set_message('check_captcha', 'Invalid %s.');
            echo "false";
        }
        if ($_SESSION["dwcaptcha"] == $code && $this->session->userdata("used") == 0) {
            //$this->session->unset_userdata("dwcaptcha");
            echo "true";
        }

    }

    public function refresh()
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 17,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('dwcaptcha');
        $this->session->set_userdata('dwcaptcha', $captcha['word']);

        // Display captcha image
        echo $captcha['image'];
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function get_mail_data($kr_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,kr_id_disp,title_of_the_content,knowledge_repository.created_on,registration.user_id');
        $this->db->join('registration', 'knowledge_repository.user_id=registration.user_id', 'LEFT');
        $kr_details = $this->master_model->getRecords('knowledge_repository ', array('kr_id' => $kr_id));

        if (count($kr_details)) {
            $user_arr = array();
            if (count($kr_details)) {
                foreach ($kr_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $kr_owner_name       = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $kr_owner_id         = $user_arr[0]['user_id'];
            $kr_owner_email      = $user_arr[0]['email'];
            $kr_title            = $kr_details[0]['title_of_the_content'];
            $kr_id_disp          = $kr_details[0]['kr_id_disp'];
            $kr_date             = date('d/m/Y', strtotime($kr_details[0]['created_on']));
            $kr_detail_link      = site_url('knowledge_repository/details/') . base64_encode($kr_id);
            $hyperlink_kr_detail = '<a href=' . $kr_detail_link . ' target="_blank">here</a>';

            $email_array = array(
                'kr_owner_name'       => $kr_owner_name,
                'kr_owner_email'      => $kr_owner_email,
                'kr_owner_id'         => $kr_owner_id,
                'kr_title'            => $kr_title,
                'kr_id_disp'          => $kr_id_disp,
                'date'                => $kr_date,
                'admin_email'         => 'test@esds.co.in',
                'hyperlink_kr_detail' => $hyperlink_kr_detail,
            );

            $this->db->select('title,first_name,middle_name,last_name,email');
            $loggedin_user_data = $this->master_model->getRecords("registration", array('user_id' => $this->session->userdata('user_id')));

            $loggedin_user_arr = array();
            if (count($loggedin_user_data)) {
                foreach ($loggedin_user_data as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $loggedin_user_arr[]    = $row_val;
                }
            }

            $loggedin_user_name                 = $loggedin_user_arr[0]['title'] . " " . $loggedin_user_arr[0]['first_name'] . " " . $loggedin_user_arr[0]['last_name'];
            $email_array['loggedin_user_name']  = $loggedin_user_name;
            $email_array['loggedin_user_email'] = $loggedin_user_arr[0]['email'];
            return $email_array;
            // echo "<pre>";
            // print_r($email_array);
        }
    }



}
