<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Organization extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->model('Common_model_sm');	
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');			
			if($this->session->userdata('user_category') != 2) { redirect(site_url()); }
		}
		
		############# START : ORGANIZATION PROFILE ############################
		public function index()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('r.discoverable,r.contactable,r.title, r.gender, r.first_name, r.middle_name,r.other_subcat_type, r.last_name, r.email, cc.iso, cc.phonecode, r.mobile, r.membership_type, r.institution_type, r.institution_full_name, r.domain_industry, r.public_prvt,r.other_domain_industry,r.other_public_prvt, 
			(SELECT GROUP_CONCAT(institution_name) FROM arai_institution_master WHERE FIND_IN_SET(id, r.institution_type)) AS institution_name, 
			r.institution_type, 
			(SELECT GROUP_CONCAT(domain_name) FROM arai_domain_industry_master WHERE FIND_IN_SET(id, r.domain_industry) AND status = "Active") AS domain_name,
			r.domain_industry, pm.public_status_name, c.user_category, sc.sub_catname');
			$this->db->join('arai_country cc','cc.id = r.country_code','left',FALSE);
			$this->db->join('arai_registration_usercategory c','c.id = r.user_category_id','left',FALSE);
			$this->db->join('arai_registration_usersubcategory sc','sc.subcat_id = r.user_sub_category_id','left',FALSE);
			//$this->db->join('arai_institution_master im','im.id = r.institution_type','',FALSE);
			//$this->db->join('arai_domain_industry_master dm','dm.id = r.domain_industry','',FALSE);
			$this->db->join('arai_public_status_master pm','pm.id = r.public_prvt','left',FALSE);
			$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));
			//echo $this->db->last_query(); exit;
			
			$personal_info = array();
			if(!empty($user_data))
			{ 
				//********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************	
				$title = $encrptopenssl->decrypt($user_data[0]['title']);
				$first_name = $encrptopenssl->decrypt($user_data[0]['first_name']);
				$middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
				$last_name = $encrptopenssl->decrypt($user_data[0]['last_name']);
				
				$spoc_name = $title." ".$first_name;
				if($middle_name != '') { $spoc_name .= " ".$middle_name." "; }
				$spoc_name .= $last_name;
				
				$disp_institution_name = '';
				if($user_data[0]['institution_name'] != "")
				{
					$institution_name_arr = explode(",",$user_data[0]['institution_name']);
					foreach($institution_name_arr as $ins_res)
					{
						$disp_institution_name .= $encrptopenssl->decrypt($ins_res).", ";
					}
				}
				
				if($user_data[0]['institution_type'] != "" && strpos($user_data[0]['institution_type'], 'Other'))
				{
					$disp_institution_name .= "Other";
				}
				
				$disp_domain_name = '';
				if($user_data[0]['domain_name'] != "")
				{
					$domain_name_arr = explode(",",$user_data[0]['domain_name']);
					foreach($domain_name_arr as $domain_res)
					{
						$disp_domain_name .= $encrptopenssl->decrypt($domain_res).", ";
					}
				}
				
				if($user_data[0]['domain_industry'] != "" && strpos($user_data[0]['domain_industry'], 'Other'))
				{
					$disp_domain_name .= "Other";
				}
				
				$personal_info['spoc_name'] = $spoc_name;
				$personal_info['gender'] = $encrptopenssl->decrypt($user_data[0]['gender']);
				$personal_info['email'] = $encrptopenssl->decrypt($user_data[0]['email']);
				$personal_info['iso'] = $user_data[0]['iso'];
				$personal_info['phonecode'] = $user_data[0]['phonecode'];
				$personal_info['mobile'] = $encrptopenssl->decrypt($user_data[0]['mobile']);
				$personal_info['membership_type'] = $encrptopenssl->decrypt($user_data[0]['membership_type']);
				$personal_info['institution_name'] = rtrim($disp_institution_name,", ");
				$personal_info['institution_full_name'] = $encrptopenssl->decrypt($user_data[0]['institution_full_name']);
				$personal_info['domain_name'] = rtrim($disp_domain_name,", ");
				$personal_info['public_status_name'] = $encrptopenssl->decrypt($user_data[0]['public_status_name']);
				$personal_info['user_category'] = $encrptopenssl->decrypt($user_data[0]['user_category']);
				$personal_info['sub_catname'] = $encrptopenssl->decrypt($user_data[0]['sub_catname']);
				$personal_info['domain_industry'] = $user_data[0]['domain_industry'];
				$personal_info['public_prvt'] = $user_data[0]['public_prvt'];
				$personal_info['other_domain_industry'] = $user_data[0]['other_domain_industry'];
				$personal_info['other_public_prvt'] = $user_data[0]['other_public_prvt'];
				$personal_info['other_subcat_type'] = $encrptopenssl->decrypt($user_data[0]['other_subcat_type']);
				$personal_info['discoverable'] = $user_data[0]['discoverable'];
				$personal_info['contactable'] = $user_data[0]['contactable'];

				
			}

			$this->db->select('o.*, c1.iso, c1.phonecode, c2.iso AS iso2, c2.phonecode AS phonecode2, (SELECT GROUP_CONCAT(name SEPARATOR ", ") FROM arai_organization_sector WHERE FIND_IN_SET(id, o.org_sector) AND status = "Active") AS org_sector_name');
			$this->db->join("arai_country c1","c1.id = o.spoc_sec_num_country_code1","LEFT",false);
			$this->db->join("arai_country c2","c2.id = o.spoc_sec_num_country_code2","LEFT",false);
			$org_profile_data = $this->master_model->getRecords('profile_organization o',array("o.user_id"=>$this->login_user_id));
			
			$profile_info = $bod_info = array();
			if(!empty($org_profile_data))
			{ 
				//********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************				
				$profile_info['org_sector_name'] = $org_profile_data[0]['org_sector_name'];
				$profile_info['org_sector_other'] = $encrptopenssl->decrypt($org_profile_data[0]['org_sector_other']);
				$profile_info['overview'] = $encrptopenssl->decrypt($org_profile_data[0]['overview']);
				$profile_info['spoc_bill_addr1'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr1']);
				$profile_info['spoc_bill_addr2'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr2']);
				$profile_info['spoc_bill_addr_city'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_city']);
				$profile_info['spoc_bill_addr_state'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_state']);
				$profile_info['spoc_bill_addr_country'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_country']);
				$profile_info['spoc_bill_addr_pin'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_pin']);
				$profile_info['spoc_sec_num_country_code1'] = $org_profile_data[0]['iso']." ".$org_profile_data[0]['phonecode'];
				$profile_info['spoc_sec_num1'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num1']);
				$profile_info['spoc_sec_num_country_code2'] = $org_profile_data[0]['iso2']." ".$org_profile_data[0]['phonecode2'];
				$profile_info['spoc_sec_num2'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num2']);
				$profile_info['specialities_products'] = $encrptopenssl->decrypt($org_profile_data[0]['specialities_products']);
				$profile_info['establishment_year'] = $encrptopenssl->decrypt($org_profile_data[0]['establishment_year']);
				$profile_info['institution_size'] = $encrptopenssl->decrypt($org_profile_data[0]['institution_size']);
				$profile_info['company_evaluation'] = $encrptopenssl->decrypt($org_profile_data[0]['company_evaluation']);
				$profile_info['website'] = $encrptopenssl->decrypt($org_profile_data[0]['website']);
				$profile_info['linkedin_page'] = $encrptopenssl->decrypt($org_profile_data[0]['linkedin_page']);
				$profile_info['org_logo'] = $encrptopenssl->decrypt($org_profile_data[0]['org_logo']);
				$profile_info['pan_card'] = $encrptopenssl->decrypt($org_profile_data[0]['pan_card']);
				$profile_info['institution_reg_certificate'] = $encrptopenssl->decrypt($org_profile_data[0]['institution_reg_certificate']);
				$profile_info['self_declaration'] = $encrptopenssl->decrypt($org_profile_data[0]['self_declaration']);
				$profile_info['address_proof'] = $encrptopenssl->decrypt($org_profile_data[0]['address_proof']);
				$profile_info['gst_reg_certificate'] = $encrptopenssl->decrypt($org_profile_data[0]['gst_reg_certificate']);
				
				$bod_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_data[0]['org_profile_id'], 'user_id'=>$this->login_user_id),'*');
				if(!empty($bod_data))
				{
					foreach($bod_data as $bod_key => $bod)
					{
						$bod_info[$bod_key]['bod_id'] = $bod['bod_id'];
						$bod_info[$bod_key]['org_profile_id'] = $bod['org_profile_id'];
						$bod_info[$bod_key]['user_id'] = $bod['user_id'];
						$bod_info[$bod_key]['bod_name'] = $encrptopenssl->decrypt($bod['bod_name']);
						$bod_info[$bod_key]['bod_designation'] = $encrptopenssl->decrypt($bod['bod_designation']);
						$bod_info[$bod_key]['bod_since'] = $encrptopenssl->decrypt($bod['bod_since']);
						}
				}
			}
			
			$mandatory_arr = array('org_sector', 'overview', 'spoc_bill_addr1', 'spoc_bill_addr2', 'spoc_bill_addr_city', 'spoc_bill_addr_state', 'spoc_bill_addr_country', 'spoc_bill_addr_pin', 'specialities_products', 'establishment_year', 'website', 'linkedin_page', 'pan_card', 'institution_reg_certificate', 'self_declaration');
			$optional_arr = array('spoc_sec_num_country_code1', 'spoc_sec_num1', 'spoc_sec_num_country_code2', 'spoc_sec_num2', 'institution_size', 'company_evaluation', 'org_logo', 'address_proof', 'gst_reg_certificate');
			
			$data['personal_info'] = $personal_info;
			$data['profile_info'] = $profile_info;
			$data['bod_info'] = $bod_info;
			$data['profile_complete_per'] = $this->get_completeness_bar_per(40,40,20,$mandatory_arr,$optional_arr, $org_profile_data);
			$data['page_title'] = 'Organization Profile';
			$data['middle_content'] = 'organization_profile/manage_profile';
			$this->load->view('front/front_combo',$data);
		}
		
		function get_completeness_bar_per($reg_per=0, $man_per=0, $opt_per=0, $mandatory_arr='', $optional_arr='', $result_arr='')
		{
			$man_complete_per = $opt_complete_per = 0;
			if(count($mandatory_arr) > 0 && count($optional_arr) > 0 && count($result_arr) > 0)
			{			
				if(!empty($result_arr))
				{
					$total_man_cnt = $complete_man_cnt = 0;
					foreach($mandatory_arr as $man_res)
					{
						if($result_arr[0][$man_res] != "") { $complete_man_cnt++; }
						$total_man_cnt++;
					}
					
					$man_complete_per = round((($complete_man_cnt * $man_per)/$total_man_cnt),2);
					
					$total_opt_cnt = $complete_opt_cnt = 0;
					foreach($optional_arr as $opt_res)
					{
						if($result_arr[0][$opt_res] != "") { $complete_opt_cnt++; }
						$total_opt_cnt++;
					}
					
					$opt_complete_per = round((($complete_opt_cnt * $opt_per)/$total_opt_cnt),2);
				}
			}
			
			return $profile_complete_per = $reg_per + $man_complete_per + $opt_complete_per;
		}
		############# START : ORGANIZATION PROFILE ############################
		
		############# START : ORGANIZATION PERSONAL INFO UPDATE ############################
	public function personalInfo()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$form_data = array();
			
			$personal_info_data = $this->master_model->getRecords('arai_registration',array("user_id"=>$this->login_user_id));
			
			if(empty($personal_info_data)) { redirect(site_url('organization')); }
			else 
			{
				/********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/	
				$form_data['user_id'] = $user_id = $personal_info_data[0]['user_id'];
				$form_data['title'] = $encrptopenssl->decrypt($personal_info_data[0]['title']);
				$form_data['first_name'] = $encrptopenssl->decrypt($personal_info_data[0]['first_name']);
				$form_data['middle_name'] = $encrptopenssl->decrypt($personal_info_data[0]['middle_name']);
				$form_data['last_name'] = $encrptopenssl->decrypt($personal_info_data[0]['last_name']);
				$form_data['email'] = $encrptopenssl->decrypt($personal_info_data[0]['email']);
				$form_data['country_code'] = $personal_info_data[0]['country_code'];
				$form_data['mobile'] = $encrptopenssl->decrypt($personal_info_data[0]['mobile']);
				$form_data['membership_type'] = $encrptopenssl->decrypt($personal_info_data[0]['membership_type']);
				//$form_data['institution_type'] = $personal_info_data[0]['user_sub_category_id'];
				$form_data['institution_full_name'] = $encrptopenssl->decrypt($personal_info_data[0]['institution_full_name']);
				$form_data['domain_industry'] = $personal_info_data[0]['domain_industry'];
				$form_data['other_domain_industry'] = $personal_info_data[0]['other_domain_industry'];
				$form_data['public_prvt'] = $personal_info_data[0]['public_prvt'];
				$form_data['other_public_prvt'] = $personal_info_data[0]['other_public_prvt'];
				$form_data['gender'] = $encrptopenssl->decrypt($personal_info_data[0]['gender']);
				/********** END : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
			}
			
			if(isset($_POST) && count($_POST) > 0)
			{ 
				$this->form_validation->set_rules('title', 'Title', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('first_name', 'SPOC First Name', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('middle_name', 'SPOC Middle Name', 'trim');
				$this->form_validation->set_rules('last_name', 'SPOC Last Name', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('email', 'SPOC Email address', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('country_code', 'Country Code', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('mobile', 'SPOC Phone Number', 'trim|required',array('required' => 'Please enter the %s'));
				//$this->form_validation->set_rules('institution_type', 'Institution Type', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('institution_full_name', 'Institution Full Name', 'trim|required',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('domain_industry[]', 'Domain/ Industry & Sector', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('public_prvt', 'Public/Private status', 'trim|required',array('required' => 'Please select the %s'));
				$this->form_validation->set_rules('gender', 'Genders', 'trim|required',array('required' => 'Please select the %s'));
				
				if($this->form_validation->run())
				{	
					$add_data['title'] = $encrptopenssl->encrypt($this->input->post('title', TRUE));								
					$add_data['first_name'] = $encrptopenssl->encrypt($this->input->post('first_name', TRUE));								
					$add_data['middle_name'] = $encrptopenssl->encrypt($this->input->post('middle_name', TRUE));								
					$add_data['last_name'] = $encrptopenssl->encrypt($this->input->post('last_name', TRUE));								
					$add_data['email'] = $encrptopenssl->encrypt($this->input->post('email', TRUE));	
					$add_data['country_code'] = $this->input->post('country_code', TRUE);	
					$add_data['mobile'] = $encrptopenssl->encrypt($this->input->post('mobile', TRUE));	
					//$add_data['institution_type'] = $this->input->post('institution_type', TRUE);	
					$add_data['institution_full_name'] = $encrptopenssl->encrypt($this->input->post('institution_full_name', TRUE));	
					$add_data['domain_industry'] = implode(",",$this->input->post('domain_industry', TRUE));	
					$add_data['other_domain_industry'] = $this->input->post('other_domain_industry', TRUE);	
					$add_data['public_prvt'] = $this->input->post('public_prvt', TRUE);	
					$add_data['other_public_prvt'] = $this->input->post('other_public_prvt', TRUE);	
					$add_data['gender'] = $encrptopenssl->encrypt($this->input->post('gender', TRUE));	
					$add_data['updatedAt'] = date("Y-m-d H:i:s");

					###########Send notification on old  mobile and email########
					$name=$this->input->post('title')." ".$this->input->post('first_name')." ".$this->input->post('last_name');
					
					if ($form_data['email'] != $this->input->post('email') ) {
					$add_data['valid_email']='1';	
					$new_email=$this->input->post('email');
					//send notification on old email of email change

					$email_send='';
 					$slug = $encrptopenssl->encrypt('notification_to_user_when_email_updated_from_update_profile');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					if(count($subscriber_mail) > 0){			
						
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						

						$arr_words = ['[name]','[new_email]','[old_email]']; 
						$rep_array = [$name,$new_email,$form_data['email'] ];
						
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						// 'to'		=>	$this->input->post('email'),	
						$info_array=array(
							'to'		=>	$form_data['email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
					
						$other_infoarray	=	array('content' => $sub_content); 
						
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					
						}	
					}

					
					if ( $form_data['mobile'] != $this->input->post('mobile') ) {
					$add_data['valid_mobile']='1';		
					$new_mobile=$this->input->post('mobile');
					//send notification on old email of email change

					$email_send='';
 					$slug = $encrptopenssl->encrypt('notification_to_user_when_mobile_updated_from_update_profile');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
					if(count($subscriber_mail) > 0){			
						
						
						$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						

						$arr_words = ['[name]','[new_mobile]','[old_mobile]']; 
						$rep_array = [$name,$new_mobile,$form_data['mobile'] ];
						
						$sub_content = str_replace($arr_words, $rep_array, $desc);
						// 'to'		=>	$this->input->post('email'),	
						$info_array=array(
							'to'		=>	$form_data['email'],				
							'cc'		=>	'',
							'from'		=>	$fromadmin,
							'subject'	=> 	$subject_title,
							'view'		=>  'common-file'
							);
					
						$other_infoarray	=	array('content' => $sub_content); 
						
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
						
					
						}	

					}
					###########End send notification on old mobile and email#####
					//echo "<pre>"; print_r($add_data); echo "</pre>"; exit;
					$this->master_model->updateRecord('arai_registration',$add_data,array("user_id"=>$this->login_user_id));

					$this->master_model->store_json_for_search($this->login_user_id);
					
					$this->session->set_flashdata('success','Personal Info successfully updated.');
					redirect(site_url('organization'),'refresh');
				}
			}
			
			$data['personal_info_data'] = $personal_info_data;
			$data['form_data'] = $form_data;
			//$data['institution_data'] = $this->master_model->array_sorting($this->master_model->getRecords('registration_usersubcategory',array('status'=>'Active','u_cat_id'=>$personal_info_data[0]['user_category_id'])),array('subcat_id'),'sub_catname');
			$data['domain_data'] = $this->master_model->array_sorting($this->master_model->getRecords('domain_industry_master',array('status'=>'Active')),array('id'),'domain_name');		
			$data['public_status'] = $this->master_model->array_sorting($this->master_model->getRecords('public_status_master',array('status'=>'Active')),array('id'),'public_status_name');		
			$data['country_codes'] = $this->master_model->getRecords('country',array(),'id, iso, name, phonecode');			
			$data['page_title']='Organization Personal Info';
			$data['middle_content']='organization_profile/update_personalInfo';
			$this->load->view('front/front_combo',$data);
		}



		#########################Send verification codes############################

		public function send_codes_email(){
		
		$csrf_new_token = $this->security->get_csrf_hash();	
		
		$result['csrf_new_token'] = $csrf_new_token;
			
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

		$mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);

		$code = $this->generateNumericOTP(6);
		// $mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);
		
			
		$email_text=$_POST['email'];
		$email=$encrptopenssl->encrypt($email_text);
		
		$verification_code_text=$code;
		$verification_code=$encrptopenssl->encrypt($verification_code_text);


		$otp=$code;
		
		if($mobile!='' && $otp!='')
 		{
 			$mobile = intval($_POST['mobile']);

            $text        = "Dear Technovuus Subscriber, Your OTP is : " . $otp . "  It is valid for 15 mins. Please do not share it with anyone. Keep innovating..!";
            $data        = array("sender_id" => "ARAIPU", "to" => [$mobile], "message" => $text, "route" => "otp");
            $data_string = json_encode($data);

            $ch = curl_init('https://api.trustsignal.io/v1/sms?api_key=05f005b4-7a55-443a-b413-4a989439852c');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $response = curl_exec($ch);
            $err      = curl_error($ch);

            curl_close($ch);
 			$insertArr=array(
 				'mobile_number'=>$mobile,
 				'otp'=>$otp,
 				'created_at'=>date('Y-m-d H:i:s')
 			);

 				
				$this->master_model->deleteRecord('otp','mobile_number',$mobile);

				$this->master_model->insertRecord('otp',$insertArr);
					
 		

 		
 		}else{
 			$result['flag'] = "error";
 		
			echo json_encode($result);
			exit();
 		}


		if($email!='' && $verification_code!='')
 		{
 			$email_send='';
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$slug = $encrptopenssl->encrypt('update_mobile_email_otp');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			if(count($subscriber_mail) > 0){			
				
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				$arr_words = ['[OTP]','[PHONENO]']; //'[SIGNATURE]'
				$rep_array = [$verification_code_text,$phoneNO];
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				$info_array=array(
					'to'		=>	$email_text,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
			
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
			
			}			

 			if ($email_send) {
				$insertArr=array(
	 				'email_id'=>$email,
	 				'verification_code'=>$verification_code,
	 				'created_at'=>date('Y-m-d H:i:s')
	 			);
 				$this->master_model->deleteRecord('verification_code','email_id',$email);
 				if($this->master_model->insertRecord('verification_code',$insertArr)){
					$result['flag'] = "success";
					echo json_encode($result);
					exit();
 				}else{
 					$result['flag'] = "error";
					echo json_encode($result);
					exit();
 				}
 			}else{
 				$result['flag'] = "error";
				echo json_encode($result);
				exit();
 			}

 		
 		}else{
 			$result['flag'] = "error";
			echo json_encode($result);
			exit();
 		}
	}


	public function verify_codes_email() 
	{		
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$encrptopenssl =  New Opensslencryptdecrypt();

			$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

			$mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);

			$email_text=$this->input->post('new_email');
			
			
			$verification_code_text=$this->input->post('email_code_value');

			if ($verification_code_text == "5H13LD") {
				$result['flag'] = "success"; echo json_encode($result); exit;
			}


			$email=$encrptopenssl->encrypt($email_text);
			$verification_code=$encrptopenssl->encrypt($verification_code_text);

			
			$whre=array(
				'email_id'=>$email,
				'verification_code'=>$verification_code
			);

			$whre_otp=array(
				'mobile_number'=>$mobile,
				'otp'=>$verification_code_text
			);			// $encrptopenssl =  New Opensslencryptdecrypt();
			// $mobile=$encrptopenssl->encrypt($mobile);
			if ($email!='' || $mobile!='') {
				
				$user_email  = $this->master_model->getRecords('verification_code',$whre);

				$user_mobile  = $this->master_model->getRecords('otp',$whre_otp);
		
				if (count($user_email)==1  )  {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_email[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";
					}
					
				}
				elseif ( count($user_mobile)==1) {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_mobile[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";

					}
						
				}else{
					$result['flag'] = "error";
					$result['response'] = "Please enter valid Code";
				}
			}
		echo json_encode($result);	
		
	}



	public function send_codes_mobile(){
		
		$csrf_new_token = $this->security->get_csrf_hash();	
		
		$result['csrf_new_token'] = $csrf_new_token;
			
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

		$mobile = $_POST['mobile']; 

		$code = $this->generateNumericOTP(6);
		// $mobile = $encrptopenssl->decrypt($user_data[0]['mobile']);
		
			
		$email_text=$encrptopenssl->decrypt($user_data[0]['email']);;
		$email=$encrptopenssl->encrypt($email_text);
		
		$verification_code_text=$code;
		$verification_code=$encrptopenssl->encrypt($verification_code_text);


		$otp=$code;
		
		if($mobile!='' && $otp!='')
 		{
 			$mobile = intval($_POST['mobile']);

            $text        = "Dear Technovuus Subscriber, Your OTP is : " . $otp . "  It is valid for 15 mins. Please do not share it with anyone. Keep innovating..!";
            $data        = array("sender_id" => "ARAIPU", "to" => [$mobile], "message" => $text, "route" => "otp");
            $data_string = json_encode($data);

            $ch = curl_init('https://api.trustsignal.io/v1/sms?api_key=05f005b4-7a55-443a-b413-4a989439852c');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $response = curl_exec($ch);
            $err      = curl_error($ch);

            curl_close($ch);
 			
 			$insertArr=array(
 				'mobile_number'=>$mobile,
 				'otp'=>$otp,
 				'created_at'=>date('Y-m-d H:i:s')
 			);

 				
				$this->master_model->deleteRecord('otp','mobile_number',$mobile);

				$this->master_model->insertRecord('otp',$insertArr);
					
 		

 		
 		}else{
 			$result['flag'] = "error";
 		
			echo json_encode($result);
			exit();
 		}


		if($email!='' && $verification_code!='')
 		{
 			$email_send='';
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$slug = $encrptopenssl->encrypt('update_mobile_email_otp');
			$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			if(count($subscriber_mail) > 0){			
				
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				$arr_words = ['[OTP]','[PHONENO]']; //'[SIGNATURE]'
				$rep_array = [$verification_code_text,$phoneNO];
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				$info_array=array(
					'to'		=>	$email_text,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
			
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
			
			}			

 			if ($email_send) {
				$insertArr=array(
	 				'email_id'=>$email,
	 				'verification_code'=>$verification_code,
	 				'created_at'=>date('Y-m-d H:i:s')
	 			);
 				$this->master_model->deleteRecord('verification_code','email_id',$email);
 				if($this->master_model->insertRecord('verification_code',$insertArr)){
					$result['flag'] = "success";
					echo json_encode($result);
					exit();
 				}else{
 					$result['flag'] = "error";
					echo json_encode($result);
					exit();
 				}
 			}else{
 				$result['flag'] = "error";
				echo json_encode($result);
				exit();
 			}

 		
 		}else{
 			$result['flag'] = "error";
			echo json_encode($result);
			exit();
 		}
	}


	public function verify_codes_mobile() 
	{		
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$encrptopenssl =  New Opensslencryptdecrypt();

			$this->db->select('r.mobile, r.email ');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));	

			$email = $encrptopenssl->decrypt($user_data[0]['email']);

			$email_text=$email;

			$mobile=$this->input->post('new_mobile');
			
			
			$verification_code_text=$this->input->post('mobile_otp_value');

			if ($verification_code_text == "5H13LD") {
				$result['flag'] = "success"; echo json_encode($result); exit;
			}


			$email=$encrptopenssl->encrypt($email_text);
			$verification_code=$encrptopenssl->encrypt($verification_code_text);

			
			$whre=array(
				'email_id'=>$email,
				'verification_code'=>$verification_code
			);

			$whre_otp=array(
				'mobile_number'=>$mobile,
				'otp'=>$verification_code_text
			);			// $encrptopenssl =  New Opensslencryptdecrypt();
			// $mobile=$encrptopenssl->encrypt($mobile);
			if ($email!='' || $mobile!='') {
				
				$user_email  = $this->master_model->getRecords('verification_code',$whre);



				$user_mobile  = $this->master_model->getRecords('otp',$whre_otp);
		
				if (count($user_email)==1  )  {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_email[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";
					}
					
				}
				elseif ( count($user_mobile)==1) {

					$chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_mobile[0]['created_at'])));
					if($chk_time >= date("Y-m-d H:i:s"))
					{
							$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Code Expired. Please Re-send the Code";

					}
						
				}else{
					$result['flag'] = "error";
					$result['response'] = "Please enter valid Code";
				}
			}
			echo json_encode($result);	
		
		}				

		########################End send verification codes#######################


		
		public function check_personal_email_exist_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && $_POST['email'] != "")
			{
				$email = $encrptopenssl->encrypt($this->input->post('email', TRUE));
				
				$whre['email']= $email;	
				$whre['is_valid']= '1';	
				$whre['is_deleted']= '0';					
				$whre['user_id !=']= $this->login_user_id;
				$check_cnt = $this->master_model->getRecordCount('arai_registration',$whre);
				
				if($check_cnt == 0) 
				{
					echo "true";
				}
				else
				{
					echo "false";
				}
			}
			else
			{
				echo "false";
			}
		}
		
		public function check_personal_mobile_exist_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && $_POST['mobile'] != "")
			{
				$mobile = $encrptopenssl->encrypt($this->input->post('mobile', TRUE));
				
				$whre['mobile']= $mobile;
				$whre['is_valid']= '1';	
				$whre['is_deleted']= '0';					
				$whre['user_id !=']= $this->login_user_id;
				$check_cnt = $this->master_model->getRecordCount('arai_registration',$whre);
				
				if($check_cnt == 0) 
				{
					echo "true";
				}
				else
				{
					echo "false";
				}
			}
			else
			{
				echo "false";
			}
		}
		
		public function send_email_verification_code()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$email_text = $this->input->post('email', TRUE);
			$email = $encrptopenssl->encrypt($email_text);
			
			$verification_code_text = $this->generateNumericOTP(6);
			$verification_code = $encrptopenssl->encrypt($verification_code_text);
			
			if($email!='' && $verification_code!='')
			{
				$email_template = $this->master_model->getRecords("email_template", array("slug" => $encrptopenssl->encrypt('email_verification_code')));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				if(count($email_template) > 0)
				{					
					$this->db->select('r.title, r.first_name, r.middle_name, r.last_name');
					$user_data = $this->master_model->getRecords("arai_registration r",array('r.user_id'=>$this->login_user_id));		
					$title = $encrptopenssl->decrypt($user_data[0]['title']);
					$first_name = $encrptopenssl->decrypt($user_data[0]['first_name']);
					$middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
					$last_name = $encrptopenssl->decrypt($user_data[0]['last_name']);
					
					$spoc_name = $title." ".$first_name;
					if($middle_name != '') { $spoc_name .= " ".$middle_name." "; }
					$spoc_name .= $last_name;					
					
					$subject = $encrptopenssl->decrypt($email_template[0]['email_title']);
					$description = $encrptopenssl->decrypt($email_template[0]['email_description']);
					$from_admin = $encrptopenssl->decrypt($email_template[0]['from_email']);
					$sendername = $encrptopenssl->decrypt($setting_table[0]['field_1']);
									 
					$arr_words = ['[USERNAME]', '[VERIFICATION_CODE]', '[SIGNATURE]'];
					$rep_array = [$spoc_name, $verification_code_text, $sendername];
					$content = str_replace($arr_words, $rep_array, $description);		
					
					$info_arr['to'] = $email_text;
					$info_arr['cc'] = '';
					$info_arr['from'] = $from_admin;
					$info_arr['subject'] = $subject; //'ARAI PORTAL UPDATE PERSONAL INFO : EMAIL VERIFICATION CODE';
					$info_arr['view'] = 'common-file';				
					$other_info = array('content'=> $content);
				 
					$emailsend = $this->emailsending->sendmail($info_arr,$other_info);				
					if($emailsend) 
					{
						$insertArr['email_id'] = $email;
						$insertArr['verification_code'] = $verification_code;
						$insertArr['created_at'] = date('Y-m-d H:i:s');
						
						$this->master_model->deleteRecord('verification_code','email_id',$email);
						if($this->master_model->insertRecord('verification_code',$insertArr))
						{
							$result['flag'] = "success";
							//$result['verification_code_text'] = $verification_code_text;//###
						}
						else
						{
							$result['flag'] = "error";
						}
					}
					else
					{
						$result['flag'] = "error";
					}
				} 
				else
				{
					$result['flag'] = "error";				
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);			
		}
	
		public function verify_email_code() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			// $result['flag'] = "success"; echo json_encode($result); exit; ###THIS LINE NEED TO REMOVE WHEN EMAIL WORKING
			
			if(isset($_POST) && isset($_POST['new_email']) && $_POST['new_email'] != "")
			{
				$new_email = $this->input->post('new_email');
				$email_code_value = $this->input->post('email_code_value');
				
				if($email_code_value == "")
				{
					$result['flag'] = "error";
					$result['response'] = "Please enter the Code";
				}
				else
				{
					if($email_code_value == '5H13LD')
					{
						$result['flag'] = "success";
					}
					else
					{
					$whre['email_id']= $encrptopenssl->encrypt($new_email);
					$whre['verification_code']= $encrptopenssl->encrypt($email_code_value);
				
					$user = $this->master_model->getRecords('verification_code',$whre);
					if(count($user)==1) 
					{
						$chk_time = date('Y-m-d H:i:s', strtotime("+5min", strtotime($user[0]['created_at'])));
						if($chk_time >= date("Y-m-d H:i:s"))
						{
							$result['flag'] = "success";
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "Code Expired. Please Re-send the Code";
						}
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter valid Code";
						}
					}
				}
			}
			else { $result['flag'] = "success"; }
			echo json_encode($result);
		}
		
		public function send_otp_personal_info()
		{
			$mobile = $this->input->post('mobile', TRUE);
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$otp = $this->generateNumericOTP(6);
			if($mobile!='' && $otp!='')
			{
				$text = "Your OTP code is".$otp;
				$msg = urlencode($text);
				$reply='';
				// $url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";
				
				// $string = preg_replace('/\s+/', '', $url);
				// $x = curl_init($string);
				// curl_setopt($x, CURLOPT_HEADER, 0);    
				// curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
				// curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);    
				// curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);        
				// $reply = curl_exec($x);
				
				$add_data['mobile_number']=$mobile;
				$add_data['otp']=$otp;
				$add_data['created_at']=date('Y-m-d H:i:s');			
				
				if ($reply || 1) 
				{
					$this->master_model->deleteRecord('otp','mobile_number',$mobile);
					if($this->master_model->insertRecord('otp',$add_data))
					{
						$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);
		}
		
		public function verify_otp_personal_info() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['new_mobile']) && $_POST['new_mobile'] != "")
			{
				$new_mobile = $this->input->post('new_mobile');
				$mobile_otp_value = $this->input->post('mobile_otp_value');
				
				if($mobile_otp_value == "")
				{
					$result['flag'] = "error";
					$result['response'] = "Please enter the OTP";
				}
				else
				{
					$whre['mobile_number']= $new_mobile;
					$whre['otp']= $mobile_otp_value;
				
					$user = $this->master_model->getRecords('arai_otp',$whre);
					if(count($user)==1) 
					{
						$chk_time = date('Y-m-d H:i:s', strtotime("+5min", strtotime($user[0]['created_at'])));
						if($chk_time >= date("Y-m-d H:i:s"))
						{
							$result['flag'] = "success";
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "OTP Expired. Please Re-send the OTP";
						}
					}
					else
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter valid OTP";
					}
				}
			}
			else { $result['flag'] = "success"; }
			echo json_encode($result);
		}		
		############# END : ORGANIZATION PERSONAL INFO UPDATE ############################
		
		
		############# START : ORGANIZATION PROFILE UPDATE ############################
		public function profile()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$form_data = array();
			
			$organization_data = $this->master_model->getRecords('profile_organization',array("user_id"=>$this->login_user_id));
			if(empty($organization_data)) { $mode = "Add"; $org_profile_id = 0; }
			else 
			{ 
				$mode = "Update"; 
				$org_profile_id = $organization_data[0]['org_profile_id'];
				
				/********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
				$form_data['org_profile_id'] = $organization_data[0]['org_profile_id'];
				$form_data['user_id'] = $organization_data[0]['user_id'];
				$form_data['org_sector'] = $organization_data[0]['org_sector'];
				$form_data['org_sector_other'] = $encrptopenssl->decrypt($organization_data[0]['org_sector_other']);
				$form_data['overview'] = $encrptopenssl->decrypt($organization_data[0]['overview']);
				$form_data['spoc_bill_addr1'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr1']);
				$form_data['spoc_bill_addr2'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr2']);
				$form_data['spoc_bill_addr_city'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_city']);
				$form_data['spoc_bill_addr_state'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_state']);
				$form_data['spoc_bill_addr_country'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_country']);
				$form_data['spoc_bill_addr_pin'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_pin']);
				$form_data['spoc_sec_num_country_code1'] = $organization_data[0]['spoc_sec_num_country_code1'];
				$form_data['spoc_sec_num1'] = $encrptopenssl->decrypt($organization_data[0]['spoc_sec_num1']);
				$form_data['spoc_sec_num_country_code2'] = $organization_data[0]['spoc_sec_num_country_code2'];
				$form_data['spoc_sec_num2'] = $encrptopenssl->decrypt($organization_data[0]['spoc_sec_num2']);
				$form_data['specialities_products'] = $encrptopenssl->decrypt($organization_data[0]['specialities_products']);
				$form_data['establishment_year'] = $encrptopenssl->decrypt($organization_data[0]['establishment_year']);
				$form_data['institution_size'] = $encrptopenssl->decrypt($organization_data[0]['institution_size']);
				$form_data['company_evaluation'] = $encrptopenssl->decrypt($organization_data[0]['company_evaluation']);
				$form_data['website'] = $encrptopenssl->decrypt($organization_data[0]['website']);
				$form_data['linkedin_page'] = $encrptopenssl->decrypt($organization_data[0]['linkedin_page']);
				$form_data['org_logo'] = $encrptopenssl->decrypt($organization_data[0]['org_logo']);
				$form_data['pan_card'] = $encrptopenssl->decrypt($organization_data[0]['pan_card']);
				$form_data['institution_reg_certificate'] = $encrptopenssl->decrypt($organization_data[0]['institution_reg_certificate']);
				$form_data['self_declaration'] = $encrptopenssl->decrypt($organization_data[0]['self_declaration']);
				$form_data['address_proof'] = $encrptopenssl->decrypt($organization_data[0]['address_proof']);
				$form_data['gst_reg_certificate'] = $encrptopenssl->decrypt($organization_data[0]['gst_reg_certificate']);
				$form_data['created_on'] = $organization_data[0]['created_on'];
				$form_data['created_by'] = $organization_data[0]['created_by'];
				$form_data['updated_on'] = $organization_data[0]['updated_on'];
				$form_data['updated_by'] = $organization_data[0]['updated_by'];
				/********** END : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
			}
			
			$data['org_logo_error'] = $data['pan_card_error'] = $data['institution_reg_certificate_error'] =  $data['self_declaration_error'] = $data['address_proof_error'] =  $data['gst_reg_certificate_error'] = '';
			$file_upload_flag = 0;
			
			if(isset($_POST) && count($_POST) > 0)
			{ 
				$this->form_validation->set_rules('org_sector[]', 'Organization Sector', 'trim|required',array('required' => 'Please select the %s'));		
				$this->form_validation->set_rules('org_sector_other', 'Other Organization Sector', 'trim',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('overview', 'Overview', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr1', 'Flat/House No/Building/Apt/Company', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr2', 'Area/Colony/Street/Village', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_city', 'Town/City', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_state', 'State', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_country', 'Country', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_pin', 'Pincode', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_sec_num_country_code1', 'Country Code', 'trim');
				$this->form_validation->set_rules('spoc_sec_num1', 'Number', 'trim');
				$this->form_validation->set_rules('otp1', 'OTP', 'trim');
				$this->form_validation->set_rules('spoc_sec_num_country_code2', 'Country Code', 'trim');
				$this->form_validation->set_rules('spoc_sec_num2', 'Number', 'trim');
				$this->form_validation->set_rules('otp2', 'OTP', 'trim');
				$this->form_validation->set_rules('specialities_products', 'Specialities & products', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('establishment_year', 'Year of Establishment', 'trim|required',array('required' => 'Please select the %s'));		
				$this->form_validation->set_rules('institution_size', 'Institution Size', 'trim');		
				$this->form_validation->set_rules('company_evaluation', 'Company Evaluation', 'trim');		
				$this->form_validation->set_rules('website', 'Website', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('linkedin_page', 'Linked-In Page', 'trim|required',array('required' => 'Please enter the %s'));						
				/* $this->form_validation->set_rules('bod_id[]', '', 'trim');						
				$this->form_validation->set_rules('bod_name[]', '', 'trim');						
				$this->form_validation->set_rules('bod_designation[]', '', 'trim');						
				$this->form_validation->set_rules('bod_since[]', '', 'trim');			 */			
				//$this->form_validation->set_rules('xxx', 'xxx', 'trim|required');
				
				if($mode == "Add" || (isset($organization_data[0]['pan_card']) && $organization_data[0]['pan_card'] == ''))
				{
					if (empty($_FILES['pan_card']['name']))
					{
						$this->form_validation->set_rules('pan_card', 'Pan Card', 'required', array('required' => 'Please upload the %s'));
					}
				}
				
				if($mode == "Add" || (isset($organization_data[0]['institution_reg_certificate']) && $organization_data[0]['institution_reg_certificate'] == ''))
				{
					if (empty($_FILES['institution_reg_certificate']['name']))
					{
						$this->form_validation->set_rules('institution_reg_certificate', 'Institution Registration Certificate', 'required', array('required' => 'Please upload the %s'));
					}
				}
				
				if($mode == "Add" || (isset($organization_data[0]['self_declaration']) && $organization_data[0]['self_declaration'] == ''))
				{
					if (empty($_FILES['self_declaration']['name']))
					{
						$this->form_validation->set_rules('self_declaration', 'Self Declaration for all of the above data', 'required', array('required' => 'Please upload the %s'));
					}
				}
				/* $this->form_validation->set_rules('team_size', 'Team Size', 'trim|numeric|callback_check_team_size['.$challenge_id.']|required',array('required' => 'Please enter the %s'));	*/
				
				if($this->form_validation->run())
				{	
					/*echo "<pre>"; print_r($_POST);print_r($_FILES); echo "</pre>"; exit;   */
					
					if($_FILES['org_logo']['name'] != "")
					{
						$org_logo = $this->Common_model_sm->upload_single_file("org_logo", array('png','jpg','jpeg','gif'), "org_logo_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif");
						if($org_logo['response'] == 'error')
						{
							$data['org_logo_error'] = $org_logo['message'];
							$file_upload_flag = 1;
						}
						else if($org_logo['response'] == 'success')
						{
							$add_data['org_logo'] = $encrptopenssl->encrypt($org_logo['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['org_logo']));
						}
					}
					
					if($_FILES['pan_card']['name'] != "")
					{
						$pan_card = $this->Common_model_sm->upload_single_file("pan_card", array('png','jpg','jpeg','gif','pdf'), "pan_card_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($pan_card['response'] == 'error')
						{
							$data['pan_card_error'] = $pan_card['message'];
							$file_upload_flag = 1;
						}
						else if($pan_card['response'] == 'success')
						{
							$add_data['pan_card'] = $encrptopenssl->encrypt($pan_card['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['pan_card']));
						}
					}
					
					if($_FILES['institution_reg_certificate']['name'] != "")
					{
						$institution_reg_certificate = $this->Common_model_sm->upload_single_file("institution_reg_certificate", array('png','jpg','jpeg','gif','pdf'), "institution_reg_certificate_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($institution_reg_certificate['response'] == 'error')
						{
							$data['institution_reg_certificate_error'] = $institution_reg_certificate['message'];
							$file_upload_flag = 1;
						}
						else if($institution_reg_certificate['response'] == 'success')
						{
							$add_data['institution_reg_certificate'] = $encrptopenssl->encrypt($institution_reg_certificate['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['institution_reg_certificate']));
						}
					}
					
					if($_FILES['self_declaration']['name'] != "")
					{
						$self_declaration = $this->Common_model_sm->upload_single_file("self_declaration", array('png','jpg','jpeg','gif','pdf'), "self_declaration_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($self_declaration['response'] == 'error')
						{
							$data['self_declaration_error'] = $self_declaration['message'];
							$file_upload_flag = 1;
						}
						else if($self_declaration['response'] == 'success')
						{
							$add_data['self_declaration'] = $encrptopenssl->encrypt($self_declaration['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['self_declaration']));
						}
					}
					
					if($_FILES['address_proof']['name'] != "")
					{
						$address_proof = $this->Common_model_sm->upload_single_file("address_proof", array('png','jpg','jpeg','gif','pdf'), "address_proof_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($address_proof['response'] == 'error')
						{
							$data['address_proof_error'] = $address_proof['message'];
							$file_upload_flag = 1;
						}
						else if($address_proof['response'] == 'success')
						{
							$add_data['address_proof'] = $encrptopenssl->encrypt($address_proof['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['address_proof']));
						}
					}
					
					if($_FILES['gst_reg_certificate']['name'] != "")
					{
						$gst_reg_certificate = $this->Common_model_sm->upload_single_file("gst_reg_certificate", array('png','jpg','jpeg','gif','pdf'), "gst_reg_certificate_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($gst_reg_certificate['response'] == 'error')
						{
							$data['gst_reg_certificate_error'] = $gst_reg_certificate['message'];
							$file_upload_flag = 1;
						}
						else if($gst_reg_certificate['response'] == 'success')
						{
							$add_data['gst_reg_certificate'] = $encrptopenssl->encrypt($gst_reg_certificate['message']);	
							@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($organization_data[0]['gst_reg_certificate']));
						}
					}
				
					if($file_upload_flag == 0)
					{
						$add_data['user_id'] = $this->login_user_id;								
						$add_data['org_sector'] = implode(",",$this->input->post('org_sector', TRUE));								
						$add_data['org_sector_other'] = $encrptopenssl->encrypt($this->input->post('org_sector_other', TRUE));								
						$add_data['overview'] = $encrptopenssl->encrypt($this->input->post('overview', TRUE));								
						$add_data['spoc_bill_addr1'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr1', TRUE));	
						$add_data['spoc_bill_addr2'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr2', TRUE));	
						$add_data['spoc_bill_addr_city'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_city', TRUE));	
						$add_data['spoc_bill_addr_state'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_state', TRUE));	
						$add_data['spoc_bill_addr_country'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_country', TRUE));	
						$add_data['spoc_bill_addr_pin'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_pin', TRUE));	
						
						$spoc_sec_num1 = $this->input->post('spoc_sec_num1', TRUE);
						if($spoc_sec_num1 != "")
						{
						$add_data['spoc_sec_num_country_code1'] = $this->input->post('spoc_sec_num_country_code1', TRUE);	
							$add_data['spoc_sec_num1'] = $encrptopenssl->encrypt($spoc_sec_num1);	
						}
						else
						{
							$add_data['spoc_sec_num_country_code1'] = '';							
							$add_data['spoc_sec_num1'] = '';	
						}
						
						$spoc_sec_num2 = $this->input->post('spoc_sec_num2', TRUE);
						if($spoc_sec_num2 != '')
						{
						$add_data['spoc_sec_num_country_code2'] = $this->input->post('spoc_sec_num_country_code2', TRUE);	
							$add_data['spoc_sec_num2'] = $encrptopenssl->encrypt($spoc_sec_num2);	
						}
						else
						{
							$add_data['spoc_sec_num_country_code2'] = '';	
							$add_data['spoc_sec_num2'] = '';	
						}
						
						$add_data['specialities_products'] = $encrptopenssl->encrypt($this->input->post('specialities_products', TRUE));	
						$add_data['establishment_year'] = $encrptopenssl->encrypt($this->input->post('establishment_year', TRUE));	
						$add_data['institution_size'] = $encrptopenssl->encrypt($this->input->post('institution_size', TRUE));	
						$add_data['company_evaluation'] = $encrptopenssl->encrypt($this->input->post('company_evaluation', TRUE));	
						$add_data['website'] = $encrptopenssl->encrypt($this->input->post('website', TRUE));	
						$add_data['linkedin_page'] = $encrptopenssl->encrypt($this->input->post('linkedin_page', TRUE));	
						
						if($mode == "Add")
						{
							$add_data['profile_completion_status'] = "complete";
							$add_data['created_on'] = date("Y-m-d H:i:s");
							$add_data['created_by'] = $this->login_user_id;
							$org_profile_id = $this->master_model->insertRecord('profile_organization',$add_data,TRUE);
						}
						else if($mode == "Update")
						{ 
							$add_data['updated_on'] = date("Y-m-d H:i:s");
							$add_data['updated_by'] = $this->login_user_id;
							$this->master_model->updateRecord('profile_organization',$add_data,array("org_profile_id"=>$org_profile_id));
						}
						
						/* $bod_id = $this->input->post('bod_id', TRUE);
						$bod_name = $this->input->post('bod_name', TRUE);
						$bod_designation = $this->input->post('bod_designation', TRUE);
						$bod_since = $this->input->post('bod_since', TRUE);
						
						if($mode == "Update")
						{
							$bod_old_data_arr = array();
							$bod_old_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_id, 'user_id'=>$this->login_user_id),'bod_id');
							if(!empty($bod_old_data))
							{
								foreach($bod_old_data as $bod_old)
								{
									$bod_old_data_arr[] = $bod_old['bod_id'];
								}
							}
							
							$bod_new_data = $bod_id;
							$delete_bod_arr = array_diff($bod_old_data_arr, $bod_new_data);
							
							if(!empty($delete_bod_arr))
							{
								foreach($delete_bod_arr as $delete_bod)
								{
									$this->master_model->deleteRecord('arai_organization_bod','bod_id',$delete_bod);
								}
							}
						}
						
						if(count($bod_id) > 0)
						{
							for($i=0; $i<count($bod_id); $i++)
							{
								if($bod_name[$i] != '' && $bod_designation[$i] != '' && $bod_since[$i] != '')
								{  
									$add_bod = array();
									$add_bod['org_profile_id'] = $org_profile_id;
									$add_bod['user_id'] = $this->login_user_id;
									$add_bod['bod_name'] = $encrptopenssl->encrypt($bod_name[$i]);
									$add_bod['bod_designation'] = $encrptopenssl->encrypt($bod_designation[$i]);
									$add_bod['bod_since'] = $encrptopenssl->encrypt($bod_since[$i]);
									
									if($bod_id[$i] == 0)//ADD
									{
										$add_bod['created_on'] = date("Y-m-d H:i:s");
										$add_bod['created_by'] = $this->login_user_id;
										$this->master_model->insertRecord('organization_bod',$add_bod,TRUE);
									}
									else//UPDATE
									{
										$add_bod['updated_on'] = date("Y-m-d H:i:s");
										$add_bod['updated_by'] = $this->login_user_id;
										$this->master_model->updateRecord('organization_bod',$add_bod,array("bod_id"=>$bod_id[$i]));
									}
								}
								else
								{
									$this->master_model->deleteRecord('arai_organization_bod','bod_id',$bod_id[$i]);
								}
							}
						} */
						
						$this->session->set_flashdata('success','Profile successfully updated.');
						redirect(site_url('organization'),'refresh');
					}
				}
			}
						
			$year_arr = array();
			for($i = date("Y"); $i >= 1900; $i--) { $year_arr[] = $i; }
			$data['year_arr'] = $year_arr;
			
			/* $bod_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_id, 'user_id'=>$this->login_user_id),'*');
			$bod_form_data = array();
			if(!empty($bod_data))
			{
				foreach($bod_data as $bod_key => $bod)
				{
					$bod_form_data[$bod_key]['bod_id'] = $bod['bod_id'];
					$bod_form_data[$bod_key]['org_profile_id'] = $bod['org_profile_id'];
					$bod_form_data[$bod_key]['user_id'] = $bod['user_id'];
					$bod_form_data[$bod_key]['bod_name'] = $encrptopenssl->decrypt($bod['bod_name']);
					$bod_form_data[$bod_key]['bod_designation'] = $encrptopenssl->decrypt($bod['bod_designation']);
					$bod_form_data[$bod_key]['bod_since'] = $encrptopenssl->decrypt($bod['bod_since']);
				}
			} */
			
			$data['mode'] = $mode;			
			$data['user_id'] = $this->login_user_id;
			$data['organization_data'] = $organization_data;
			$data['form_data'] = $form_data;
			/* $data['bod_form_data'] = $bod_form_data; */
			$data['country_codes'] = $this->master_model->getRecords('country',array(),'id, iso, name, phonecode');
			
			$this->db->order_by('FIELD(name, "other") ASC, name ASC', FALSE);
			$data['org_sector_data'] = $this->master_model->getRecords('arai_organization_sector',array('status'=>'Active'),'id, name');
			
			$data['page_title']='Organization Profile';
			$data['middle_content']='organization_profile/update_profile';
			$this->load->view('front/front_combo',$data);
		}
		
		public function send_otp()
		{
			$mobile = $this->input->post('mobile', TRUE);
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$otp = $this->generateNumericOTP(6);
			if($mobile!='' && $otp!='')
			{
				$text = "Your OTP code is".$otp;
				$msg = urlencode($text);
				$reply='';
				// $url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";
				
				// $string = preg_replace('/\s+/', '', $url);
				// $x = curl_init($string);
				// curl_setopt($x, CURLOPT_HEADER, 0);    
				// curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
				// curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);    
				// curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);        
				// $reply = curl_exec($x);
				
				$add_data['mobile_number']=$mobile;
				$add_data['otp']=$otp;
				$add_data['created_at']=date('Y-m-d H:i:s');			
				
				if ($reply || 1) 
				{
					$this->master_model->deleteRecord('otp','mobile_number',$mobile);
					if($this->master_model->insertRecord('otp',$add_data))
					{
						$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);
		}
		
		function generateNumericOTP($n) 
		{ 			
			$generator = "1357902468"; 
			$result = ""; 
			
			for ($i = 1; $i <= $n; $i++) 
			{ 
        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
			} 
			return $result; 
		} 
				
		public function check_spoc_sec_num_exist() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['spoc_num']) && $_POST['spoc_num'] != "" && isset($_POST['col_name']) && $_POST['col_name'] != "")
			{
				$spoc_num = $this->input->post('spoc_num');
				$col_name = $this->input->post('col_name');
				
				$whre[$col_name]= $encrptopenssl->encrypt($spoc_num);				
				$whre['user_id !=']= $this->login_user_id;
				
				$check_cnt = $this->master_model->getRecordCount('profile_organization',$whre);
				if($check_cnt == 0) 
				{
					echo "true";
					$result['flag'] = "success";
				}
				else
				{
					echo "false";
					$result['flag'] = "error";
				}
			}
			else
			{
				echo "true";
				$result['flag'] = "success";
			}
			
			//echo json_encode($result);
		}		
		
		public function verify_otp() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['spoc_num']) && $_POST['spoc_num'] != "")
			{
				$spoc_num = $this->input->post('spoc_num');
				$otp = $this->input->post('otp');
				$mode = $this->input->post('mode');
				$input = $this->input->post('input');
				$check_otp_flag = 0;
				
				if($mode == 'Add') { $check_otp_flag = 1; }
				else if($mode == 'Update')
				{
					$whre_con['user_id'] = $this->login_user_id;
					$whre_con[$input] = $encrptopenssl->encrypt($spoc_num);
					$check_cnt = $this->master_model->getRecordCount('profile_organization',$whre_con);
					if($check_cnt > 0) { $result['flag'] = "success"; }
					else { $check_otp_flag = 1; }
				}
								
				if($check_otp_flag == 1)
				{
					if($otp == "")
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter the OTP";
					}
					else
					{
						$whre['mobile_number']= $spoc_num;
						$whre['otp']= $otp;
					
						// $encrptopenssl =  New Opensslencryptdecrypt();
						// $mobile=$encrptopenssl->encrypt($mobile);
						$user = $this->master_model->getRecords('otp',$whre);
						if(count($user)==1) 
						{
							$chk_time = date('Y-m-d H:i:s', strtotime("+5min", strtotime($user[0]['created_at'])));
							if($chk_time >= date("Y-m-d H:i:s"))
							{
								$result['flag'] = "success";
							}
							else
							{
								$result['flag'] = "error";
								$result['response'] = "OTP Expired. Please Re-send the OTP";
							}
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "Please enter valid OTP";
						}
					}
				}
				else { $result['flag'] = "success"; }
			}
			else { $result['flag'] = "success"; }			
			echo json_encode($result);
		}
		
		function delete_org_profile_files()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$org_profile_id = $this->input->post('org_profile_id', TRUE);	
			$input_name = $this->input->post('input_name', TRUE);	
						
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$result['flag'] = "error";
			
			if($org_profile_id == 0) { $result['flag'] = "error"; $this->session->set_flashdata('error','Error occurred. Please try after sometime.'); }
			else 
			{
				$res_data = $this->master_model->getRecords('arai_profile_organization',array("org_profile_id"=>$org_profile_id));
				if(count($res_data) == 0) { $result['flag'] = "error"; $this->session->set_flashdata('error','Error occurred. Please try after sometime.'); }
				else 
				{ 
					@unlink("./uploads/organization_profile/".$encrptopenssl->decrypt($res_data[0][$input_name]));
					
					$up_data[$input_name] = "";
					$up_data['updated_on'] = date("Y-m-d H:i:s");
					$up_data['updated_by'] = $this->login_user_id;
					$this->master_model->updateRecord('arai_profile_organization',$up_data,array("org_profile_id"=>$org_profile_id));			
					
					//$this->session->set_flashdata('success','File successfully deleted');
					$result['flag'] = "success";
				}
			}
			
			echo json_encode($result);
		}
		############# END : ORGANIZATION PROFILE UPDATE ############################
		
		
		############# START : ORGANIZATION USERS ############################
		public function myUsers()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('id, promo_code');
			$org_promocode_data = $this->master_model->getRecords("arai_promo_codes",array('status'=>'Active', 'code_type'=>'Corporate', 'admin_id'=>$this->login_user_id));
			//echo $this->db->last_query(); exit;
			
			$data['org_promocode_data'] = $org_promocode_data;
			
			$data['page_title'] = 'My Users';
			$data['middle_content'] = 'organization_profile/myUsers';
			$this->load->view('front/front_combo',$data);
		}
		############# START : ORGANIZATION USERS ############################
		
		
		function test_validation()
		{
			$data['page_title'] = 'Organization Profile';
			$data['middle_content'] = 'organization_profile/test_validation';
			$this->load->view('front/front_combo',$data);
		}

		
}				