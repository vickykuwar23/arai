<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{	
		$data['page_title']='Home';
		$data['middle_content']='index';
		$this->load->view('front/front_combo',$data);
	}
	public function about($value='')
	{
		$data['page_title']='About Us';
		$data['middle_content']='about';
		$this->load->view('front/front_combo',$data);
	}
}
