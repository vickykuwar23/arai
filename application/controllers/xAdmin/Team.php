<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Class : Team Listing
Author : Vicky K
 */
class Team extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(8);
    }
    public function index_new()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('excel');
        $this->load->library('Opensslencryptdecrypt');
        $this->db->select('c.c_id, c.challenge_id,r.title_decrypt,r.first_name_decrypt,r.last_name_decrypt ,c.challenge_title,
							t.*');
        $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'left', false);
        $this->db->join('arai_challenge c', 'c.c_id = t.c_id', 'left', false);
        $data['records'] = $records = $this->master_model->getRecords("arai_byt_teams t", array('t.is_deleted' => '0', 't.team_type' => 'challenge'), '', array('team_id' => 'DESC'));

        // echo "<pre>"; print_r($records);die;
        if (isset($_POST['export']) && !empty($_POST['export'])) {
            if (count($records) > 0) {
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                $i = 0;
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Team ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Team Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Challenge Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Team Owner Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Team Competion Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Team Breif Info');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Challenge Owner Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Created DAte');
                $rowCount = 2;
                foreach ($records as $row_val) {
                    $i++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['team_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['custom_team_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['team_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $encrptopenssl->decrypt($row_val['challenge_title']));
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row_val['title_decrypt'] . " " . $row_val['first_name_decrypt'] . " " . $row_val['last_name_decrypt']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row_val['team_status']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row_val['brief_team_info']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row_val['challenge_owner_status']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, date('d-m-Y', strtotime($row_val['created_on'])));
                    $rowCount++;
                }

                // create file name
                header("Content-Type: application/vnd.ms-excel");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
                header('Content-Disposition: attachment;filename="Byt_challenge_teams' . date("YmdHis") . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
                ob_end_clean();
                $objWriter->save('php://output');
                //$objWriter->save($fileName);
                exit;
            }
        }
        // echo "<pre>";print_r($records);die;
        $data['module_name']    = 'Team';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'team/index_new';
        $this->load->view('admin/admin_combo', $data);
    }
    public function index()
    {
        error_reporting(0);

        $data['records']        = '';
        $data['module_name']    = 'Team';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'team/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function get_team_data()
    {

        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $response = array();
        ## Read value
        $draw            = @$this->input->post('draw');
        $start           = @$this->input->post('start');
        $rowperpage      = @$this->input->post('length'); // Rows display per page
        $columnIndex     = @$this->input->post('order')[0]['column']; // Column index
        $columnName      = @$this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = @$this->input->post('order')[0]['dir']; // asc or desc
        $searchValue     = @$this->input->post('search')['value']; // Search value

        $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

        ## Search
        $search_arr  = array();
        $searchQuery = "";
        if ($keyword != '') {
            $string       = $encrptopenssl->encrypt($keyword);
            $search_arr[] = " (arai_challenge.challenge_id like '%" . $keyword . "%' or
		arai_challenge.challenge_title like '%" . $string . "%' or
		arai_challenge.company_name like '%" . $string . "%' or
		arai_challenge.challenge_status like '%" . $keyword . "%' or
		arai_challenge.contact_person_name like '%" . $string . "%' or
		arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
		arai_challenge_contact.office_no like '%" . $keyword . "%' or
		arai_challenge_contact.email_id like '%" . $keyword . "%') ";

        }

        /*if($searchCity != ''){
        $search_arr[] = " city='".$searchCity."' ";
        }
        if($searchGender != ''){
        $search_arr[] = " gender='".$searchGender."' ";
        }
        if($searchName != ''){
        $search_arr[] = " name like '%".$searchName."%' ";
        }
        if(count($search_arr) > 0){
        $searchQuery = implode(" and ",$search_arr);
        }*/

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
        $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
        $records = $this->master_model->getRecords("byt_teams", array('byt_teams.is_deleted' => '0', 'byt_teams.team_type' => 'challenge'), '', array('byt_teams.team_id' => 'DESC'));

        $totalRecords = count($records);
        ## Total number of record with filtering
        $totalRecordwithFilter = count($records);
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
            $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
            $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
            $recordsFilter         = $this->master_model->getRecords("byt_teams", array('byt_teams.is_deleted' => '0', 'byt_teams.team_type' => 'challenge'), '', array('team_id' => 'DESC'));
            $totalRecordwithFilter = count($recordsFilter);
        }

        ## Fetch records
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
        }

        $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
        $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
        $records = $this->master_model->getRecords("byt_teams", array('byt_teams.is_deleted' => '0', 'byt_teams.team_type' => 'challenge'), '', array('team_id' => 'DESC'), $start, $rowperpage);

        $data = array();
        $i    = 0;
        foreach ($records as $row_val) {

            $i++;
            $row_val['c_id']                   = $row_val['c_id'];
            $row_val['challenge_id']           = $row_val['challenge_id'];
            $row_val['challenge_title']        = $encrptopenssl->decrypt($row_val['challenge_title']);
            $row_val['team_id']                = $row_val['team_id'];
            $row_val['team_name']              = $row_val['team_name'];
            $row_val['team_size']              = $row_val['team_size'];
            $row_val['challenge_owner_status'] = $row_val['challenge_owner_status'];
            $row_val['team_status']            = $row_val['team_status'];
            $row_val['created_on']             = $row_val['created_on'];
            $row_val['user_id']                = $row_val['user_id'];

            // Team Owner Name
            $user_id      = $row_val['user_id'];
            $registerData = $this->master_model->getRecords("registration", array("user_id" => $user_id));
            $fullname     = ucfirst($encrptopenssl->decrypt($registerData[0]['title'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['first_name'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['middle_name'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['last_name']));

            $addedDate = date('d-m-Y H:i:s', strtotime($row_val['created_on']));

            $dropdown = "";

            if ($row_val['challenge_owner_status'] == 'Pending'): $chkStatus   = "selected";else:$chkStatus   = "";endif;
            if ($row_val['challenge_owner_status'] == 'Approved'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($row_val['challenge_owner_status'] == 'Rejected'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;

            $dropdown .= "<input type='hidden' name='team_id' id='team_id' value='" . $row_val['team_id'] . "' />
		<select name='status' id='status' class='up-status' data-id='" . $row_val['team_id'] . "'>
			<option value='Pending' " . $chkStatus . ">Pending</option>
			<option value='Approved' " . $chkStatus1 . ">Approved</option>
			<option value='Rejected' " . $chkStatus2 . ">Rejected</option>";
            $dropdown .= "</select>";

            // Deleted Link
            $team_delete = "<a href='javascript:void(0)' class='btn btn-outline-success btn-sm' onClick='return teamDelete(" . $row_val['team_id'] . ");'>
			Delete
		</a>";

            $team_view = "<a href='javascript:void(0)' class='btn btn-outline-primary btn-sm view-more' data-id='" . $row_val['team_id'] . "'>
			View
		</a>";

            //$showButtons = $dropdown." ".$team_delete." ".$team_view;
            $showButtons = $team_delete . " " . $team_view;

            $data[] = array(
                $i,
                $row_val['team_id'],
                $row_val['team_name'],
                $row_val['challenge_id'],
                $row_val['challenge_title'],
                // $row_val['team_size'],
                $row_val['team_status'],
                $row_val['challenge_owner_status'],
                $addedDate,
                $showButtons,
            );

        } // Foreach End

        $csrf_test_name = $this->security->get_csrf_hash();
        ## Response
        $response = array(
            "draw"                 => intval($draw),
            "iTotalRecords"        => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData"               => $data,
            "token"                => $csrf_test_name,
        );

        echo json_encode($response);
    }

    public function restoreList()
    {

        $data['records']        = '';
        $data['module_name']    = 'Team';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'team/restore';
        $this->load->view('admin/admin_combo', $data);
    }

    public function restore_team_data()
    {

        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $response = array();
        ## Read value
        $draw            = @$this->input->post('draw');
        $start           = @$this->input->post('start');
        $rowperpage      = @$this->input->post('length'); // Rows display per page
        $columnIndex     = @$this->input->post('order')[0]['column']; // Column index
        $columnName      = @$this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = @$this->input->post('order')[0]['dir']; // asc or desc
        $searchValue     = @$this->input->post('search')['value']; // Search value

        $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

        ## Search
        $search_arr  = array();
        $searchQuery = "";
        if ($keyword != '') {
            $string       = $encrptopenssl->encrypt($keyword);
            $search_arr[] = " (arai_challenge.challenge_id like '%" . $keyword . "%' or
		arai_challenge.challenge_title like '%" . $string . "%' or
		arai_challenge.company_name like '%" . $string . "%' or
		arai_challenge.challenge_status like '%" . $keyword . "%' or
		arai_challenge.contact_person_name like '%" . $string . "%' or
		arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
		arai_challenge_contact.office_no like '%" . $keyword . "%' or
		arai_challenge_contact.email_id like '%" . $keyword . "%') ";

        }

        /*if($searchCity != ''){
        $search_arr[] = " city='".$searchCity."' ";
        }
        if($searchGender != ''){
        $search_arr[] = " gender='".$searchGender."' ";
        }
        if($searchName != ''){
        $search_arr[] = " name like '%".$searchName."%' ";
        }
        if(count($search_arr) > 0){
        $searchQuery = implode(" and ",$search_arr);
        }*/

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
        $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
        $records = $this->master_model->getRecords("byt_teams", array('arai_byt_teams.is_deleted' => '1', 'arai_byt_teams.team_type' => 'challenge'), '', array('arai_byt_teams.team_id' => 'DESC'));

        $totalRecords = count($records);
        ## Total number of record with filtering
        $totalRecordwithFilter = count($records);
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
            $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
            $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
            $recordsFilter         = $this->master_model->getRecords("byt_teams", array('arai_byt_teams.is_deleted' => '1', 'arai_byt_teams.team_type' => 'challenge'), '', array('arai_byt_teams.team_id' => 'DESC'));
            $totalRecordwithFilter = count($recordsFilter);
        }

        ## Fetch records
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
        }

        $this->db->select('arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
		arai_byt_teams.user_id, arai_byt_teams.team_name, arai_byt_teams.team_id,
		arai_byt_teams.team_size, arai_byt_teams.challenge_owner_status,
		arai_byt_teams.team_status, arai_byt_teams.created_on');
        $this->db->join('challenge', 'arai_challenge.c_id = arai_byt_teams.c_id', 'left');
        $records = $this->master_model->getRecords("byt_teams", array('arai_byt_teams.is_deleted' => '1', 'arai_byt_teams.team_type' => 'challenge'), '', array('team_id' => 'DESC'), $start, $rowperpage);

        $data = array();
        $i    = 0;
        foreach ($records as $row_val) {

            $i++;
            $row_val['c_id']                   = $row_val['c_id'];
            $row_val['challenge_id']           = $row_val['challenge_id'];
            $row_val['challenge_title']        = $encrptopenssl->decrypt($row_val['challenge_title']);
            $row_val['team_id']                = $row_val['team_id'];
            $row_val['team_name']              = $row_val['team_name'];
            $row_val['team_size']              = $row_val['team_size'];
            $row_val['challenge_owner_status'] = $row_val['challenge_owner_status'];
            $row_val['team_status']            = $row_val['team_status'];
            $row_val['created_on']             = $row_val['created_on'];
            $row_val['user_id']                = $row_val['user_id'];

            // Team Owner Name
            $user_id      = $row_val['user_id'];
            $registerData = $this->master_model->getRecords("registration", array("user_id" => $user_id));
            $fullname     = ucfirst($encrptopenssl->decrypt($registerData[0]['title'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['first_name'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['middle_name'])) . " " . ucfirst($encrptopenssl->decrypt($registerData[0]['last_name']));

            $addedDate = date('d-m-Y H:i:s', strtotime($row_val['created_on']));

            $dropdown = "";

            if ($row_val['challenge_owner_status'] == 'Pending'): $chkStatus   = "selected";else:$chkStatus   = "";endif;
            if ($row_val['challenge_owner_status'] == 'Approved'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($row_val['challenge_owner_status'] == 'Rejected'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;

            // Deleted Link
            $team_delete = "<a href='javascript:void(0)' class='btn btn-outline-success btn-sm' onClick='return teamRestore(" . $row_val['team_id'] . ");'>
			Restore
		</a>";

            $showButtons = $team_delete;

            $data[] = array(
                $i,
                $row_val['team_id'],
                $row_val['team_name'],
                $row_val['challenge_id'],
                $row_val['challenge_title'],
                $row_val['team_size'],
                $row_val['team_status'],
                $row_val['challenge_owner_status'],
                $addedDate,
                $showButtons,
            );

        } // Foreach End

        $csrf_test_name = $this->security->get_csrf_hash();
        ## Response
        $response = array(
            "draw"                 => intval($draw),
            "iTotalRecords"        => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData"               => $data,
            "token"                => $csrf_test_name,
        );

        echo json_encode($response);
    }

    public function updateTeamStatus()
    {

        $team_id        = $this->input->post('id');
        $tstatus        = $this->input->post('tstatus');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('byt_teams', array('challenge_owner_status' => $tstatus), array('team_id' => $team_id));

        $status   = "Team Status Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);

    }

    public function viewDetails()
    {

        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $id                              = $this->input->post('id');
        $data_team['team_details']       = $records       = $this->master_model->getRecords("byt_teams", array("team_id" => $id));
        $brief_team_info                 = $records[0]['brief_team_info'];
        $proposed_approach               = $records[0]['proposed_approach'];
        $additional_information          = $records[0]['additional_information'];
        $user_id                         = $records[0]['user_id'];
        $data_team['team_files_public']  = $this->master_model->getRecords('byt_team_files', array('team_id' => $id, 'is_deleted' => '0', 'file_type' => 'public'), 'file_name, file_type');
        $data_team['team_files_private'] = $this->master_model->getRecords('byt_team_files', array('team_id' => $id, 'is_deleted' => '0', 'file_type' => 'private'), 'file_name, file_type');

        $this->db->select('title,first_name,middle_name,last_name');
        $register = $this->master_model->getRecords("registration", array("user_id" => $user_id));
        $fullname = ucfirst($encrptopenssl->decrypt($register[0]['title'])) . " " . ucfirst($encrptopenssl->decrypt($register[0]['first_name'])) . " " . ucfirst($encrptopenssl->decrypt($register[0]['middle_name'])) . " " . ucfirst($encrptopenssl->decrypt($register[0]['last_name']));

        $this->db->join('arai_byt_team_slots t', 's.slot_id = t.slot_id', 'INNER', false);
        $fetchRecord = $this->master_model->getRecords("byt_slot_applications s", array('s.team_id' => $id, 's.is_deleted' => '0'));
        //echo $this->db->last_query();
        //echo "<pre>";print_r($fetchRecord);
        $table = '';
        $table .= '<table class="table table-bordered">
				<tr>
					<td width="15%"><b>Team Owner Name</b></td>
					<td>' . $fullname . '</td>
				</tr>
				<tr>
					<td width="15%"><b>Team Breif Info</b></td>
					<td>' . $brief_team_info . '</td>
				</tr>
				<tr>
					<td width="15%"><b>Proposed Approach</b></td>
					<td>' . $proposed_approach . '</td>
				</tr>
				<tr>
					<td width="15%"><b>Additional Information</b></td>
					<td>' . $additional_information . '</td>
				</tr>
			  </table><br />';
        $table .= '<h4>Team Member Listing</h4>';
        $table .= '<table class="table table-bordered">
				<tr>
					<th>Name</th>
					<th>Role Name</th>
					<!--<th>Hiring Status</th>-->
					<th>Status</th>
				</tr>';

        foreach ($fetchRecord as $teamMembers) {

            $memberList  = $this->master_model->getRecords("registration", array("user_id" => $teamMembers['apply_user_id']));
            $member_name = ucfirst($encrptopenssl->decrypt($memberList[0]['title'])) . " " . ucfirst($encrptopenssl->decrypt($memberList[0]['first_name'])) . " " . ucfirst($encrptopenssl->decrypt($memberList[0]['middle_name'])) . " " . ucfirst($encrptopenssl->decrypt($memberList[0]['last_name']));

            $table .= '<tr>
					<td>' . $member_name . '</td>
					<td class="text-center">' . ucwords($teamMembers['role_name']) . '</td>
					<!--<td>' . ucwords($teamMembers['hiring_status']) . '</td>	-->
					<td>' . $teamMembers['status'] . '</td>
				</tr>';

        }

        $table .= '</table>';
        $table .= $this->load->view('admin/team/incTeamDetails', $data_team, true);

        echo $table;

    }

    // Delete Team
    public function deleteTeam($team_id)
    {

        $csrf_test_name = $this->security->get_csrf_hash();

        $id          = $team_id;
        $updateQuery = $this->master_model->updateRecord('byt_teams', array('is_deleted' => '1'), array('team_id' => $id));
        //echo $this->db->last_query();die();
        if ($updateQuery) {
            $this->session->set_flashdata('success', 'Team successfully deleted');
            redirect(base_url('xAdmin/team/index_new'));
        } else {
            $this->session->set_flashdata('error', 'Something Went Wrong');
            redirect(base_url('xAdmin/team/index_new'));
        }
        // $jsonData = array("token" => $csrf_test_name, "message" => 'Team successfully deleted');

        // echo json_encode($jsonData);

    }

    // Delete Team
    public function restoreTeam()
    {

        $csrf_test_name = $this->security->get_csrf_hash();

        $id          = $this->input->post('team_id');
        $updateQuery = $this->master_model->updateRecord('byt_teams', array('is_deleted' => '0'), array('team_id' => $id));
        //echo $this->db->last_query();die();
        $jsonData = array("token" => $csrf_test_name, "message" => 'Team successfully restore');

        echo json_encode($jsonData);

    }
    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
