<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Categories
Author : Vicky K
*/

class Usercategories extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
    	$cat_data = $this->master_model->getRecords("user_categories");
		
		$res_arr = array();
		if(count($cat_data)){	
						
			foreach($cat_data as $row_val){		
						
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_arr[] = $row_val;
			}
			
		}
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_Categories';	
    	$data['middle_content']='usercategories/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
        // Check Validation
		$this->form_validation->set_rules('usertype', 'User Category', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usertype = $encrptopenssl->encrypt($this->input->post('usertype'));
			
			$insertArr = array( 'user_category' => $usertype);			
			$insertQuery = $this->master_model->insertRecord('user_categories',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User Category successfully created');
				redirect(base_url('xAdmin/usercategories'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usercategories/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_Categories';
        $data['middle_content']='usercategories/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$usertype_data = $this->master_model->getRecords("user_categories", array('id' => $id));
		
		$res_arr = array();
		if(count($usertype_data) > 0){ 
			$row_val['user_category'] = $encrptopenssl->decrypt($usertype_data[0]['user_category']);
			$res_arr[] = $row_val;
		}	
		
		$data['usertype_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('usertype', 'User Category', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usertype = $encrptopenssl->encrypt($this->input->post('usertype'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'user_category' => $usertype, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('user_categories',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User Category successfully updated');
				redirect(base_url('xAdmin/usercategories'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usercategories/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_Categories';
        $data['middle_content']='usercategories/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('user_categories',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/usercategories'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('user_categories',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/usercategories'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('user_categories',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User Categories successfully deleted');
		 redirect(base_url('xAdmin/usercategories'));	
		 
	 }


}