<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class How_it_works extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("how_it_works");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['subject_title'] = $encrptopenssl->decrypt($row_val['subject_title']);
				$row_val['first_title'] = $encrptopenssl->decrypt($row_val['first_title']);
				$row_val['first_description'] = $encrptopenssl->decrypt($row_val['first_description']);
				$row_val['first_icon_image'] = $encrptopenssl->decrypt($row_val['first_icon_image']);
				$row_val['second_title'] = $encrptopenssl->decrypt($row_val['second_title']);
				$row_val['second_description'] = $encrptopenssl->decrypt($row_val['second_description']);
				$row_val['second_icon_image'] = $encrptopenssl->decrypt($row_val['second_icon_image']);
				$row_val['third_title'] = $encrptopenssl->decrypt($row_val['third_title']);
				$row_val['third_description'] = $encrptopenssl->decrypt($row_val['third_description']);
				$row_val['third_icon_image'] = $encrptopenssl->decrypt($row_val['third_icon_image']);
				$row_val['background_image'] = $encrptopenssl->decrypt($row_val['background_image']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'How_it_works';	
    	$data['middle_content']='how_it_works/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
        // Check Validation
		$this->form_validation->set_rules('subject_title', 'Subject Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_title', 'First Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_description', 'First Description', 'required');
		$this->form_validation->set_rules('second_title', 'Second Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('second_description', 'Second Description', 'required');
		$this->form_validation->set_rules('third_title', 'Third Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('third_description', 'Third Description', 'required');
		
		if($this->form_validation->run())
		{	
			$subject_title = $encrptopenssl->encrypt($this->input->post('subject_title'));
			$first_title = $encrptopenssl->encrypt($this->input->post('first_title'));
			$first_description = $encrptopenssl->encrypt($this->input->post('first_description'));
			$second_title = $encrptopenssl->encrypt($this->input->post('second_title'));
			$second_description = $encrptopenssl->encrypt($this->input->post('second_description'));
			$third_title = $encrptopenssl->encrypt($this->input->post('third_title'));
			$third_description = $encrptopenssl->encrypt($this->input->post('third_description'));
			
			if($_FILES['first_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('first_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['first_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$first_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			if($_FILES['second_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('second_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['second_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$second_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			if($_FILES['third_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('third_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['third_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$third_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			if($_FILES['background_image']['name']!=""){			
				$config['upload_path']      = 'assets/background_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('background_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['background_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$background_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			$insertArr = array( 
						'subject_title' => $subject_title,
						'first_title' => $first_title,
						'second_title' => $second_title,
						'third_title' => $third_title,
						'first_description' => $first_description,
						'second_description' => $second_description,
						'third_description' => $third_description,
						'first_icon_image' => $first_icon_image_file,
						'second_icon_image' => $second_icon_image_file,
						'third_icon_image' => $third_icon_image_file,
						'background_image' => $background_image_file
					);			
			$insertQuery = $this->master_model->insertRecord('how_it_works',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','How It Works Information successfully created');
				redirect(base_url('xAdmin/how_it_works'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/how_it_works/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'How_it_works';
        $data['middle_content']='how_it_works/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		$tech_data = $this->master_model->getRecords("how_it_works", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['subject_title'] = $encrptopenssl->decrypt($tech_data[0]['subject_title']);
			$row_val['first_title'] = $encrptopenssl->decrypt($tech_data[0]['first_title']);
			$row_val['second_title'] = $encrptopenssl->decrypt($tech_data[0]['second_title']);
			$row_val['third_title'] = $encrptopenssl->decrypt($tech_data[0]['third_title']);
			$row_val['first_description'] = $encrptopenssl->decrypt($tech_data[0]['first_description']);
			$row_val['second_description'] = $encrptopenssl->decrypt($tech_data[0]['second_description']);
			$row_val['third_description'] = $encrptopenssl->decrypt($tech_data[0]['third_description']);
			$row_val['first_icon_image'] = $encrptopenssl->decrypt($tech_data[0]['first_icon_image']);
			$row_val['second_icon_image'] = $encrptopenssl->decrypt($tech_data[0]['second_icon_image']);
			$row_val['third_icon_image'] = $encrptopenssl->decrypt($tech_data[0]['third_icon_image']);
			$row_val['background_image'] = $encrptopenssl->decrypt($tech_data[0]['background_image']);
			$res_arr[] = $row_val;
		}
		// echo "<pre>";
		// print_r($res_arr);die();
		$data['how_it_works_data'] = $res_arr;
        // Check Validation
        // Check Validation
		$this->form_validation->set_rules('subject_title', 'Subject Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_title', 'First Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_description', 'First Description', 'required');
		$this->form_validation->set_rules('second_title', 'Second Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('second_description', 'Second Description', 'required');
		$this->form_validation->set_rules('third_title', 'Third Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('third_description', 'Third Description', 'required');
		
		if($this->form_validation->run())
		{
			$subject_title = $encrptopenssl->encrypt($this->input->post('subject_title'));
			$first_title = $encrptopenssl->encrypt($this->input->post('first_title'));
			$first_description = $encrptopenssl->encrypt($this->input->post('first_description'));
			$second_title = $encrptopenssl->encrypt($this->input->post('second_title'));
			$second_description = $encrptopenssl->encrypt($this->input->post('second_description'));
			$third_title = $encrptopenssl->encrypt($this->input->post('third_title'));
			$third_description = $encrptopenssl->encrypt($this->input->post('third_description'));

			$first_icon_image_file = '';
			if($_FILES['first_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('first_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['first_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$first_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			$second_icon_image_file = '';
			if($_FILES['second_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('second_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['second_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$second_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			$third_icon_image_file = '';
			if($_FILES['third_icon_image']['name']!=""){			
				$config['upload_path']      = 'assets/icon_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('third_icon_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['third_icon_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$third_icon_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			$background_image_file = '';
			if($_FILES['background_image']['name']!=""){			
				$config['upload_path']      = 'assets/background_image';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('background_image', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/how_it_works/add'));	
				}			
				$filesize = $_FILES['background_image']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$background_image_file = $encrptopenssl->encrypt($b_image[0]);				   
			}

			$tech_data = $this->master_model->getRecords("how_it_works", array('id' => $id));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 
							'subject_title' => $subject_title,
							'first_title' => $first_title,
							'second_title' => $second_title,
							'third_title' => $third_title,
							'first_description' => $first_description,
							'second_description' => $second_description,
							'third_description' => $third_description,
							'first_icon_image' => !empty($first_icon_image_file) ? $first_icon_image_file : $tech_data[0]['first_icon_image'],
							'second_icon_image' => !empty($second_icon_image_file) ? $second_icon_image_file : $tech_data[0]['second_icon_image'],
							'third_icon_image' => !empty($third_icon_image_file) ? $third_icon_image_file : $tech_data[0]['third_icon_image'],
							'background_image' => !empty($background_image_file) ? $background_image_file : $tech_data[0]['background_image'],
							'updatedAt' => $updateAt
						);

			$updateQuery = $this->master_model->updateRecord('how_it_works',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','How It Works Information successfully updated');
				redirect(base_url('xAdmin/how_it_works'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/how_it_works/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'How_it_works';
        $data['middle_content']='how_it_works/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('how_it_works',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/how_it_works'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('how_it_works',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/how_it_works'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}