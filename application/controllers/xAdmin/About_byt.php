<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : About BYT
Author : Vicky K
*/

class About_byt extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("byt_landing_page");
    	
    	$data['records'] = $response_data; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'About_BYT';	
    	$data['middle_content']='about-byt/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation
		$this->form_validation->set_rules('quote_text', 'Quote', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('byt_content', 'Description', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_content', 'Content One', 'required');
		$this->form_validation->set_rules('second_content', 'Content Two', 'required');
		$this->form_validation->set_rules('third_content', 'Content Three', 'required');
		
		
		
		if($this->form_validation->run())
		{	
			$quote_text 	= $this->input->post('quote_text');
			$byt_content 	= $this->input->post('byt_content');
			$first_content 	= $this->input->post('first_content');
			$second_content = $this->input->post('second_content');
			$third_content 	= $this->input->post('third_content');

			if($_FILES['banner_img']['name']!=""){			
				$config['upload_path']      = 'uploads/byt';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '50000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png", "gif" => "image/gif");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/about_byt/add'));	
				}			
				$filesize = $_FILES['banner_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$banner_file = $b_image[0];				   
			}
			
			$insertArr = array( 'banner_img' => $banner_file, 'banner_quote' => $quote_text, 'byt_content' => $byt_content, 'first_content' => $first_content, 'second_content' => $second_content, 'third_content' => $third_content );			
			$insertQuery = $this->master_model->insertRecord('byt_landing_page',$insertArr);
			//echo $this->db->last_query();die();
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Record successfully created');
				redirect(base_url('xAdmin/about_byt'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/about_byt/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'About_BYT';
        $data['middle_content']='about-byt/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		$a_id = base64_decode($id);
		$about_data = $this->master_model->getRecords("byt_landing_page", array('a_id' => $a_id));
		$data['byt_data'] = $about_data;
		
        // Check Validation
		$this->form_validation->set_rules('quote_text', 'Quote', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('byt_content', 'Description', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('first_content', 'Content One', 'required');
		$this->form_validation->set_rules('second_content', 'Content Two', 'required');
		$this->form_validation->set_rules('third_content', 'Content Three', 'required');
		
		if($this->form_validation->run())
		{	
			$quote_text 	= $this->input->post('quote_text');
			$byt_content 	= $this->input->post('byt_content');
			$first_content 	= $this->input->post('first_content');
			$second_content = $this->input->post('second_content');
			$third_content 	= $this->input->post('third_content');

			$banner_img = '';
			
			if($_FILES['banner_img']['name']!=""){			
				$config['upload_path']      = 'uploads/byt';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '50000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				
				$banner_img = $b_image[0];				   
			} // End File

			$tech_data = $this->master_model->getRecords("byt_landing_page", array('a_id' => $a_id));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 
						'banner_quote' 	=> $quote_text,
						'byt_content' 	=> $byt_content,
						'banner_img' 	=> !empty($banner_img) ? $banner_img : $tech_data[0]['banner_img'],
						'first_content' => $first_content,
						'second_content' => $second_content,
						'third_content' => $third_content,
						'updatedAt' 	=> $updateAt
					);
			//echo "<pre>";print_r($updateArr);die();			
			$updateQuery = $this->master_model->updateRecord('byt_landing_page',$updateArr,array('a_id' => $a_id));
			//echo  $this->db->last_query();die();
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Record successfully updated');
				redirect(base_url('xAdmin/about_byt'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/about_byt/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'About_BYT';
        $data['middle_content']='about-byt/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  public function viewDetails(){
		 
		//error_reporting(0);
		$encrptopenssl =  New Opensslencryptdecrypt();
		 
		$id=$this->input->post('id'); 
		$records = $this->master_model->getRecords("byt_landing_page",array("a_id" => $id));
		$banner_quote = $records[0]['banner_quote'];
		$byt_content = $records[0]['byt_content'];
		$first_content = $records[0]['first_content'];
		$second_content = $records[0]['second_content'];
		$third_content = $records[0]['third_content'];		
		
		$table = '<table class="table table-bordered">
						<tr>
							<td width="15%"><b>Quote</b></td>
							<td>'.$banner_quote.'</td>
						</tr>		
						<tr>
							<td width="15%"><b>Description</b></td>
							<td>'.$byt_content.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Content One</b></td>
							<td>'.$first_content.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Content Two</b></td>
							<td>'.$second_content.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Content Three</b></td>
							<td>'.$third_content.'</td>
						</tr>
				  </table>';
		echo $table;
		 
	 }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('services',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/services'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('services',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/services'));		
		 }
		 
	 }

}