<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class  : Email Template
Author : Vicky K
*/

class Subscriber_email extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(23);
    }

    public function index()
    {
		
		redirect(base_url('xAdmin/subscriber_email/add'));
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("email_template");
		//echo "<pre>";print_r($response_data);die();
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['email_title'] = $encrptopenssl->decrypt($row_val['email_title']);
				$row_val['from_email'] 	= $encrptopenssl->decrypt($row_val['from_email']);
				$row_val['cc_email_id'] 	= $encrptopenssl->decrypt($row_val['cc_email_id']);
				$row_val['email_description'] = $encrptopenssl->decrypt($row_val['email_description']);	 
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);die();
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Subscriber_email';
		$data['submodule_name'] = 'Subscriber_email';	
    	$data['middle_content']='trigger-mail/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
			// Check Validation
			$this->form_validation->set_rules('category', 'Category', 'required|xss_clean');
			$this->form_validation->set_rules('email_title', 'Email Title', 'required|min_length[3]|xss_clean');
			$this->form_validation->set_rules('from_email', 'Email ID', 'required|xss_clean');
			$this->form_validation->set_rules('cc_email_id', 'Email ID', 'required|xss_clean');
			$this->form_validation->set_rules('email_description', 'Email Description', 'required|xss_clean');
			
			if($this->form_validation->run())
			{	
				$category 			= $this->input->post('category');
				$subcategory 		= $this->input->post('subcategory');
				$email_title 		= $this->input->post('email_title');
				$cc_email_id 		= $this->input->post('cc_email_id');
				$from_emai 			= $this->input->post('from_email');
				$email_description  = $this->input->post('email_description');
			
			/*if(($category == 1 || $category == 2) && $subcategory!=""){
				
				$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
				$get_result = $this->master_model->getRecords('registration',array('status'=>'Active','user_category_id'=>$category, 'user_sub_category_id'=>$subcategory, 'is_valid'=> '1', 'payment_status' => 'complete')); //pending
			
			}
			
			if(($category == 1 || $category == 2) && $subcategory==""){
				
				$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
				$get_result = $this->master_model->getRecords('registration',array('status'=>'Active','user_category_id'=>$category, 'is_valid'=> '1', 'payment_status' => 'complete'));
				
			}
			
			if($category == 3){
				
				$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
				$get_result = $this->master_model->getRecords('registration',array('status'=>'Active', 'is_valid'=> '1', 'payment_status' => 'complete'));
				
			}
			
			if($category == 4){
				
				$this->db->select('email_id');
				$get_result = $this->master_model->getRecords('newsletter',array('status'=>'Active', 'is_deleted'=> '0'));
				
			}
			
			if(count($get_result) > 0){
				
				echo "<pre>";print_r($get_result);
				
			}*/
			
			/*$insertArr = array( 'email_title' => $email_title, 'from_email' => $from_emai, 'cc_email_id' => $cc_email_id,  'email_description' => $email_description);			
			$insertQuery = $this->master_model->insertRecord('email_template',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Email content successfully created');
				redirect(base_url('xAdmin/email_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/email_master/add'));
			}*/
		}
		$data['module_name'] = 'Subscriber_email';
		$data['submodule_name'] = 'Subscriber_email';	
        $data['middle_content']='trigger-mail/add';
        $this->load->view('admin/admin_combo',$data);
    }
	
	public function notify_users(){
		
		// Object Creation
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		// Post Values
		$category 			= $this->input->post('category');
		$subcategory 		= $this->input->post('subcategory');
		$email_title 		= $this->input->post('email_title');
		$cc_email_id 		= $this->input->post('cc_email_id');
		$from_email			= $this->input->post('from_email');
		$email_description  = $this->input->post('email_description');
		
		//Conditions Apply To build sql
		if(($category == 1 || $category == 2) && $subcategory!=""){
			
			$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
			$get_result = $this->master_model->getRecords('registration',array('status'=>'Active','user_category_id'=>$category, 'user_sub_category_id'=>$subcategory, 'is_valid'=> '1', 'payment_status' => 'complete')); //pending
		
		}
		
		if(($category == 1 || $category == 2) && $subcategory==""){
			
			$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
			$get_result = $this->master_model->getRecords('registration',array('status'=>'Active','user_category_id'=>$category, 'is_valid'=> '1', 'payment_status' => 'complete'));
			
		}
		
		if($category == 3){
			
			$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
			$get_result = $this->master_model->getRecords('registration',array('status'=>'Active', 'is_valid'=> '1', 'payment_status' => 'complete'));
			
		}
		
		if($category == 4){
			
			$this->db->select('email_id');
			$get_result = $this->master_model->getRecords('newsletter',array('status'=>'Active', 'is_deleted'=> '0'));
			
		}
		
		if($category == 5){
			
			//Subscribe Users
			$this->db->select('email_id');
			$newsletter_result = $this->master_model->getRecords('newsletter',array('status'=>'Active', 'is_deleted'=> '0'));
			
			// Register Users
			//$this->db->select('email as email_id, title, first_name, middle_name, last_name, mobile, user_category_id, user_sub_category_id');
			$this->db->select('email as email_id');
			$register_result = $this->master_model->getRecords('registration',array('status'=>'Active', 'is_valid'=> '1', 'payment_status' => 'complete'));
			
			$get_result = array_merge($newsletter_result, $register_result);
			
			//$get_result = array_unique($get_result_merge);
		}
		
		//echo ">>>".count($get_result);
		//echo "<pre>";print_r($get_result);die();
		
		$captureUniqueEmail = array();
		if(count($get_result) > 0){
			$i = 0;
			foreach($get_result as $records){
				
				$email_id 		= $encrptopenssl->decrypt($records['email_id']);
				array_push($captureUniqueEmail, $email_id);	
				
				/*$info_arr		=	array(
											'to'		=>	$email_id,//$email_id					
											'cc'		=>	'',
											'from'		=>	$from_email, //$from_email
											'subject'	=> 	$email_title,
											'view'		=>  'common-file'
										);
					
				$other_info		=	array('content'=>$email_description); 					
				
				$emailsend 		= 	$this->emailsending->sendmail($info_arr, $other_info);*/
				
				
				
			} // Foreach End			
			
			
			$storeEmail = array_unique($captureUniqueEmail, SORT_REGULAR);
			//echo "<pre>";print_r($storeEmail);die();
			if(count($storeEmail) > 0){
				
				foreach($storeEmail as $subs_emails){
					
					$info_arr		=	array(
											'to'		=>	$subs_emails,//$email_id					
											'cc'		=>	'',
											'from'		=>	$from_email, //$from_email
											'subject'	=> 	$email_title,
											'view'		=>  'common-file'
										);
					
					$other_info		=	array('content'=>$email_description); 					
					
					$emailsend 		= 	$this->emailsending->sendmail($info_arr, $other_info);
					
					$i++;
					
				}
				
			}
			
			
			if(count($storeEmail) == $i){
				
				$response=array(
							'success'=>'Email successfully send.'
							);
		
	
				echo json_encode($response);
				
			}
		} // Count End
		
	}
	 
	 
	public function get_sub_category()
	{
		$encrptopenssl =  New Opensslencryptdecrypt();
		$str='';
		$cat_id = htmlentities($this->input->post('cat_id'));
		
		$user_sub_category=$this->master_model->getRecords('registration_usersubcategory',array('status'=>'Active','u_cat_id'=>$cat_id));


		$sub_category_arr = array();
		if(count($user_sub_category)){
						
			foreach($user_sub_category as $row_val){		
						
				$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$sub_category_arr[] = $row_val;
			}
			
		}
		$this->sortBy('sub_catname',   $sub_category_arr);

		$set_select=$this->input->post('setSelect');
		$str ='<option value=""> -- Please Select -- </option>';
		
		foreach($sub_category_arr as $sub_cat)
		{ 
			if(isset($set_select) && $set_select==$sub_cat['subcat_id']) {
				$sel = 'selected';
				
			}else{
				$sel='';
			}
			$str.= "<option value='".$sub_cat['subcat_id']."' ".$sel.">". $sub_cat['sub_catname'] . "</option>";
		
		}
		
	    $response=array(
	    	'name'=>$this->security->get_csrf_token_name(),
			'value'=>$this->security->get_csrf_hash(),
			'str'=>$str
	    );
		
	
		echo json_encode($response);

	}
	
	public function sortBy($field, &$array, $direction = 'asc')
	{
		usort($array, create_function('$a, $b', '
			$a = $a["' . $field . '"];
			$b = $b["' . $field . '"];
			$a = trim($a);
			$b = trim($b);

			if ($a == $b) return 0;

			@$direction = strtolower(trim($direction));
			return ($a ' . (@$direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
		'));

		return true;
	}

}