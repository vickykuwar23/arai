<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Student_profile
Author : Suraj M
*/

class Individual_profile extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("student_profile",array('type'=>'Individual'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['company_name'] = $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['designation'] = $encrptopenssl->decrypt($row_val['designation']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Individual_profile';	
    	$data['middle_content']='individual_profile/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation
		$this->form_validation->set_rules('employement_status', 'Employement Status', 'required');
		$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('designation', 'Designation', 'required');
		$this->form_validation->set_rules('designation_since_year', 'Designation Since Year', 'required');
		$this->form_validation->set_rules('designation_description', 'Description', 'required');
		$this->form_validation->set_rules('corporate_linkage_code', 'Corporate Linkage Code', 'required');
		$this->form_validation->set_rules('technical_experience_in_year', 'Technical Experience', 'required');
		$this->form_validation->set_rules('technical_description_of_work', 'Description of Work', 'required');
		$this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[5]|max_length[5]');
		$this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
		$this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
		$this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('specify_fields_area_that_you_would_like', 'specify fields area that you would like to be an expert', 'required');
		$this->form_validation->set_rules('bio_data', 'Bio Data', 'required');
		$this->form_validation->set_rules('years_of_experience', 'Years of Experience', 'required');
		$this->form_validation->set_rules('no_of_paper_publication', 'No of Paper Publication', 'required');
		// $this->form_validation->set_rules('paper_year', 'Paper Year', 'required');
		// $this->form_validation->set_rules('paper_conf_name', 'Paper Conf. Name', 'required');
		// $this->form_validation->set_rules('paper_title', 'Paper Title', 'required');
		$this->form_validation->set_rules('no_of_patents', 'No of Patents', 'required');
		// $this->form_validation->set_rules('patent_year', 'Patent Year', 'required');
		// $this->form_validation->set_rules('patent_number', 'Patent Number', 'required');
		// $this->form_validation->set_rules('patent_title', 'Patent Title', 'required');
		$this->form_validation->set_rules('portal_name', 'Portal Name', 'required');
		$this->form_validation->set_rules('portal_link', 'Portal Link', 'required');
		$this->form_validation->set_rules('portal_description', 'Portal Description', 'required');


		if($this->form_validation->run())
		{	
			// echo "<pre>";
			// print_r($this->input->post());die;
			$type = $this->input->post('type');
			$employement_status = $encrptopenssl->encrypt($this->input->post('employement_status'));
			$company_name = $encrptopenssl->encrypt($this->input->post('company_name'));
			$designation = $encrptopenssl->encrypt($this->input->post('designation'));
			$designation_since_year = $encrptopenssl->encrypt($this->input->post('designation_since_year'));
			$designation_description = $encrptopenssl->encrypt($this->input->post('designation_description'));
			$corporate_linkage_code = $encrptopenssl->encrypt($this->input->post('corporate_linkage_code'));
			
			$technical_experience_in_year = $encrptopenssl->encrypt($this->input->post('technical_experience_in_year'));
			$technical_description_of_work = $encrptopenssl->encrypt($this->input->post('technical_description_of_work'));
			$pincode = $encrptopenssl->encrypt($this->input->post('pincode'));
			$flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
			$area_colony_street_village = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
			$town_city_and_state = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
			$country = $encrptopenssl->encrypt($this->input->post('country'));
			$specify_fields_area_that_you_would_like = $encrptopenssl->encrypt($this->input->post('specify_fields_area_that_you_would_like'));
			$bio_data = $encrptopenssl->encrypt($this->input->post('bio_data'));
			$years_of_experience = $encrptopenssl->encrypt($this->input->post('years_of_experience'));
			$no_of_paper_publication = $encrptopenssl->encrypt($this->input->post('no_of_paper_publication'));
			$paper_year = $encrptopenssl->encrypt($this->input->post('paper_year'));
			$paper_conf_name = $encrptopenssl->encrypt($this->input->post('paper_conf_name'));
			$paper_title = $encrptopenssl->encrypt($this->input->post('paper_title'));
			$no_of_patents = $encrptopenssl->encrypt($this->input->post('no_of_patents'));
			$patent_year = $encrptopenssl->encrypt($this->input->post('patent_year'));
			$patent_number = $encrptopenssl->encrypt($this->input->post('patent_number'));
			$patent_title = $encrptopenssl->encrypt($this->input->post('patent_title'));
			$portal_name = $encrptopenssl->encrypt($this->input->post('portal_name'));
			$portal_link = $encrptopenssl->encrypt($this->input->post('portal_link'));
			$portal_description = $encrptopenssl->encrypt($this->input->post('portal_description'));

			$profile_picture  = $_FILES['profile_picture']['name'];
			
			if($_FILES['profile_picture']['name']!=""){			
				
				$config['upload_path']      = 'assets/profile_picture';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('profile_picture', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/individual_profile'));	
				}
						
				$filesize = $_FILES['profile_picture']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			} // Student ID Proof Image End
			
			$file_names = $encrptopenssl->encrypt($b_image[0]);
			
			$insertArr = array('type' => $type, 'employement_status' => $employement_status, 'company_name' => $company_name, 'designation' => $designation, 'designation_since_year' => $designation_since_year, 'designation_description' => $designation_description, 'corporate_linkage_code' => $corporate_linkage_code, 'technical_experience_in_year' => $technical_experience_in_year, 'technical_description_of_work' => $technical_description_of_work, 'pincode' => $pincode, 'flat_house_building_apt_company' => $flat_house_building_apt_company, 'area_colony_street_village' => $area_colony_street_village, 'town_city_and_state' => $town_city_and_state, 'country' => $country, 'specify_fields_area_that_you_would_like' => $specify_fields_area_that_you_would_like, 'bio_data' => $bio_data, 'years_of_experience' => $years_of_experience, 'no_of_paper_publication' => $no_of_paper_publication, 'paper_year' => $paper_year, 'paper_conf_name' => $paper_conf_name, 'paper_title' => $paper_title, 'no_of_patents' => $no_of_patents, 'patent_year' => $patent_year, 'patent_number' => $patent_number, 'patent_title' => $patent_title, 'portal_name' => $portal_name, 'portal_link' => $portal_link, 'portal_description' => $portal_description, 'profile_picture' => $file_names);

			$insertQuery = $this->master_model->insertRecord('student_profile',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Individual / Expert Profile successfully created');
				redirect(base_url('xAdmin/individual_profile'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/student_profile/add'));
			}
		}

		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Individual_profile';
        $data['middle_content']='individual_profile/add';
        $this->load->view('admin/admin_combo',$data);
     }

}