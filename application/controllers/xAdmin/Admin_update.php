<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_update extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(16);

        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {
        error_reporting(0);

        // load excel library
        $this->load->library('excel');

        if (isset($_POST['export']) && !empty($_POST['export'])) {
            /* print_r($_POST); exit; */
            //error_reporting(0);
            $encrptopenssl = new Opensslencryptdecrypt();
            $response      = array();

            $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	b.blog_id_disp like '%" . $keyword . "%' or b.blog_title like '%" . $string . "%' or b.author_name like '%" . $string . "%') ";
                // or arai_challenge.json_str like '%".$keyword."%'
            }

            if (count($search_arr) > 0) {$searchQuery = implode(" and ", $search_arr);}

            ## Total number of records without filtering
            /* $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_blog_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_blog_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
            $records = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
            $totalRecords = count($records); */

            ## Total number of record with filtering
            /* $totalRecordwithFilter = count($records);
            if($searchQuery != '')
            {
            $this->db->where($searchQuery);
            $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason');
            $recordsFilter = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
            $totalRecordwithFilter = count($recordsFilter);
            }
             */

            ## Fetch records
            if ($searchQuery != '') {$this->db->where($searchQuery);}
            $this->db->select('k.technology_other,k.tag_other,k.xOrder,rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_description, k.kr_type, k.kr_type_other, k.tags,  k.author_name, k.xOrder, k.is_featured, k.admin_status, k.is_block, k.block_reason,(SELECT GROUP_CONCAT(technology_name SEPARATOR ",") FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id, k.tags) AND status = "Active" AND tag_name != "other") AS TagName');
            $this->db->join('arai_knowledge_repository_type_master rt', 'k.kr_type = rt.id', 'LEFT', false);
            $records = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0'), '', array('k.kr_id' => 'DESC'));

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                // set Header
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'KR ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'KR Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'KR Description');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Type');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Other Type');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Tags');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Other Tag');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Technology');
                $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Other Technology');
                $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Author Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'xOrder');
                $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Featured Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Admin Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Block Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Block Reason');

                // set Row
                $rowCount = 2;
                foreach ($records as $row_val) {

                    if ($row_val['is_featured'] == '0') {$isFeatured = 'Not Featured';} else { $isFeatured = 'Featured';}
                    if ($row_val['admin_status'] == '0') {$adminStatus = 'Pending';} else if ($row_val['admin_status'] == '1') {$adminStatus = 'Approved';} else { $adminStatus = 'Rejected';}
                    if ($row_val['is_block'] == '0') {$isBlock = 'Not Block';} else { $isBlock = 'Block';}

                    $i++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['kr_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['kr_id_disp']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['title_of_the_content']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, htmlspecialchars_decode($row_val['kr_description']));
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row_val['type_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row_val['kr_type_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row_val['TagName']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row_val['tag_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row_val['DispTechnology']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row_val['technology_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row_val['author_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row_val['xOrder']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $isFeatured);
                    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $adminStatus);
                    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $isBlock);
                    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row_val['block_reason']);
                    $rowCount++;
                } // Foreach End
            } // Count Check

            // create file name
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="Blog_' . date("YmdHis") . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        $data['records']        = '';
        $data['module_name']    = 'admin_update';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'admin_update/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function add($id = 0)
    {
        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('admin_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("admin_update", array('feed_id' => $id, 'is_deleted' => 0));

            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else { $data['mode'] = $mode = "Add";}

            $data['files_data'] = $files_data = $this->master_model->getRecords('admin_update_files', array("feed_id" => $id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));

            $data['video_data'] = $video_data = $this->master_model->getRecords('admin_video_details', array("feed_id" => $id, "is_deleted" => '0'), '', array('vid_id' => 'ASC'));
            // echo "<pre>";
            // print_r($data);die;

        }

        $data['banner_img_error'] = $data['location_error'] = $data['tags_error'] = $error_flag = '';
        $file_upload_flag         = 0;
        if (isset($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('news_title', 'news title', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('news_description', 'news description', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['banner_img']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $banner_img = $this->Common_model_sm->upload_single_file("banner_img", array('png', 'jpg', 'jpeg', 'gif'), "banner_img_" . date("YmdHis"), "./uploads/admin_update_banner", "png|jpeg|jpg|gif");
                    if ($banner_img['response'] == 'error') {
                        $data['banner_img_error'] = $banner_img['message'];
                        $file_upload_flag         = 1;
                    } else if ($banner_img['response'] == 'success') {
                        $add_data['banner'] = $banner_img['message'];
                        /* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
                    }
                }

                $add_data['admin_id']         = $user_id;
                $add_data['feed_title']       = $this->input->post('news_title');
                $add_data['feed_description'] = $this->input->post('news_description');
                $add_data['banner_check']     = $this->input->post('banner_check');
                $add_data['video_check']      = $this->input->post('video_check');
                $add_data['files_check']      = $this->input->post('files_check');
                $add_data['comment']          = $this->input->post('comment');

                if ($mode == 'Add') {
                    $add_data['is_featured']  = 0;
                    $add_data['admin_status'] = 0;
                    $add_data['is_deleted']   = 0;
                    $add_data['is_block']     = 0;
                    $add_data['created_on']   = date('Y-m-d H:i:s');

                    $last_id = $this->master_model->insertRecord('arai_admin_update', $add_data, true);
                    // echo $this->db->last_query();die;
                    $up_data['id_disp'] = $disp_id = "TNFID-" . sprintf("%07d", $last_id);
                    $this->master_model->updateRecord('arai_admin_update', $up_data, array('feed_id' => $last_id));

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $user_id,
                        'action_name' => "Add Admin Update",
                        'module_name' => 'Admin Update',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Content added successfully');
                } else if ($mode == 'Update') {
                    $add_data['admin_status'] = 0;
                    $add_data['updated_on']   = date('Y-m-d H:i:s');

                    $last_id = $id;

                    $this->master_model->updateRecord('arai_admin_update', $add_data, array('feed_id' => $last_id));
                    //echo $this->db->last_query(); exit;

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $user_id,
                        'action_name' => "admin updates",
                        'module_name' => 'admin updates',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Content has been successfully updated');
                }

                
                if ($this->input->post('video_check') == 1) {

                    $this->master_model->updateRecord('admin_video_details', array('is_deleted' => '1'), array('feed_id' => $id));

                    $caption_arr   = $this->input->post('caption');
                    $video_url_arr = $this->input->post('video_url');
                    if (count($caption_arr)) {
                        foreach ($caption_arr as $key => $value) {
                            $add_video_info['feed_id']    = $last_id;
                            $add_video_info['caption']    = $caption_arr[$key];
                            $add_video_info['video_url']  = $video_url_arr[$key];
                            $add_video_info['created_on'] = date("Y-m-d H:i:s");

                            $this->master_model->insertRecord('arai_admin_video_details', $add_video_info, true);
                        }
                    }
                }
                //START : INSERT  FILES
                if ($last_id != "") {
                    $r_files = $_FILES['r_files'];
                    if (count($r_files) > 0) {

                        for ($i = 0; $i < count($r_files['name']); $i++) {
                            if ($r_files['name'][$i] != '') {
                                $kr_file = $this->Common_model_sm->upload_single_file('r_files', array('png', 'jpg', 'jpeg', 'gif', 'pdf', 'xls', 'xlsx', 'doc', 'docx', 'pptx', 'ppt'), "admin_file_" . $last_id . "_" . date("YmdHis") . rand(), "./uploads/admin_update_files", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx|pptx|ppt", '1', $i);

                                if ($kr_file['response'] == 'error') {} else if ($kr_file['response'] == 'success') {
                                    $add_file_data['admin_id']           = $user_id;
                                    $add_file_data['feed_id']            = $last_id;
                                    $add_file_data['file_name']          = ($kr_file['message']);
                                    $add_file_data['file_original_name'] = $r_files['name'][1];
                                    $add_file_data['created_on']         = date("Y-m-d H:i:s");
                                    $this->master_model->insertRecord('arai_admin_update_files', $add_file_data, true);
                                    // $kr_files_data[] = $add_file_data; //FOR LOG
                                }
                            }
                        }
                    }
                }
                //END : INSERT  FILES

                redirect(site_url('xAdmin/admin_update/index'));

            }
            // echo validation_errors();die;
        }

        $data['module_name']    = 'Admin Update';
        $data['submodule_name'] = '';

        $data['middle_content'] = 'admin_update/add';
        $this->load->view('admin/admin_combo', $data);
    }

    public function get_data_listing()
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $table        = 'arai_admin_update a';
        $column_order = array(
            'a.feed_id',
            'a.feed_id',
            'a.id_disp',
            'a.feed_title',
            'a.created_on',
            'a.admin_status',
            'a.xOrder',
            'a.is_featured',
            'a.is_block',

        ); //SET COLUMNS FOR SORT

        $column_search = array(
            'a.id_disp',
            'a.feed_title',
            'a.created_on',
            'IF(a.admin_status = 0, "Pending", IF(a.admin_status = 1, "Approved", IF(a.admin_status = 2, "Rejected", "")))',
            'a.xOrder',
            'IF(a.is_featured=1,"Featured","Non Featured")',
            'IF(a.is_block = 0, "Unblock","Block")',

        ); //SET COLUMN FOR SEARCH

        $order = array('a.feed_id' => 'DESC'); // DEFAULT ORDER

        $WhereForTotal = "WHERE a.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
        $Where         = "WHERE a.is_deleted = 0 	";

        if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);
            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        // $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = k.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_order)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        $Rows        = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));

        $data = array();
        $no   = $_POST['start'];

        foreach ($Rows as $Res) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $Res['feed_id'];
            $row[] = $Res['id_disp'];
            $row[] = $Res['feed_title'];

            $onclick_like_fun = "show_update_likes('" . base64_encode($Res['feed_id']) . "')";
            $getTotalLikes    = $this->master_model->getRecordCount('arai_admin_update_likes', array('feed_id' => $Res['feed_id']), 'like_id');
            $disp_likes       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';
            $row[]=$disp_likes;

            // $row[] = $dispName;
            $row[] = $Res['created_on'];

            $dropdown = "";

            if ($Res['admin_status'] == '0'): $chkStatus  = "selected";else:$chkStatus  = "";endif;
            if ($Res['admin_status'] == '1'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($Res['admin_status'] == '2'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;


            //if($Res['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            //if($Res['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            $dropdown .= "<input type='hidden' name='feed_id' id='feed_id' value='" . $Res['feed_id'] . "' />
			<select name='status' id='status' class='change-status'>
			<option value='0' " . $chkStatus . ">Pending</option>
			<option value='1' " . $chkStatus1 . ">Approved</option>
			<option value='2' " . $chkStatus2 . ">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
            $dropdown .= "</select>";
            $row[] = $dropdown;

            $xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='" . $Res['xOrder'] . "' name='xOrder' id='xOrder' value='" . $Res['xOrder'] . "' onblur='update_sort_order(this.value, " . $Res['feed_id'] . ")' onkeyup='update_sort_order(this.value, " . $Res['feed_id'] . ")' />";

            $row[] = $xOrder;

            /*$onchange_priority_fun = 'change_question_priority("' . base64_encode($Res['q_id']) . '", this.value)';
            if ($Res['priority'] == '0') {$chkPriority0 = "selected";} else { $chkPriority0 = "";}
            if ($Res['priority'] == '1') {$chkPriority1 = "selected";} else { $chkPriority1 = "";}
            if ($Res['priority'] == '2') {$chkPriority2 = "selected";} else { $chkPriority2 = "";}
            $disp_priority = "
            <select name='priority' id='priority' onchange='" . $onchange_priority_fun . "'>
            <option value='0' " . $chkPriority0 . ">Low</option>
            <option value='1' " . $chkPriority1 . ">Medium</option>
            <option value='2' " . $chkPriority2 . ">High</option>
            </select>";
            $row[] = $disp_priority;*/

            $EditBtn = "<a href='" . base_url('xAdmin/admin_update/add/' . base64_encode($Res['feed_id'])) . "' > <button class='btn btn-success btn-sm'>View/Edit</button> </a>";

            if ($Res['is_featured'] == '1') {$textShowFeatured = "Featured";} else { $textShowFeatured = "Not Featured";}
            $FeaturedBtn = "<a href='javascript:void(0)' data-id='" . $Res['feed_id'] . "' class='btn btn-success btn-sm featured-check' id='change-stac-" . $Res['feed_id'] . "'>" . $textShowFeatured . "</a>";

            /* $onclick_featured_fun = "featured_notfeatured_question('" . base64_encode($Res['feed_id']) . "', '" . $FeaturedFunText . "')";
            $FeaturedBtn          = '<a href="javascript:void(0)" onclick="' . $onclick_featured_fun . '" class="btn btn-success btn-sm featured-check" title="' . $textShowFeatured . '"><i class="fa ' . $FeatureIcon . '" aria-hidden="true"></i></a>';*/

            if ($Res['is_block'] == '0') {$textShowBlockStatus = 'Block';} else if ($Res['is_block'] == '1') {$textShowBlockStatus = 'Unblock';}
            $onclick_block_fun = "block_unblock('" . base64_encode($Res['feed_id']) . "', '" . $textShowBlockStatus . "', '" . $Res['id_disp'] . "', '" . $Res['feed_title'] . "')";
            $BlockBtn          = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="' . $onclick_block_fun . '" >' . $textShowBlockStatus . '</a>';

            $DeleteBtn = "<a href='javascript:void(0)' class='remove' data-id='" . $Res['feed_id'] . "'><button class='btn btn-success btn-sm'>Remove</button></a>";

            $ActionButtons = "<span style='white-space:nowrap;'>" . $EditBtn . " " . $FeaturedBtn . " " . $BlockBtn . " " . $DeleteBtn . "</span>";
            $row[]         = $ActionButtons;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $TotalResult, //All result count
            "recordsFiltered" => $FilteredResult, //Disp result count
            //"Query" => $print_query,
            "data"            => $data,
        );
        //output to json format
        // echo "<pre>"; print_r($output);die;
        echo json_encode($output);
    }

     public function show_likes_ajax() // SHOW LIKE COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $feed_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']  = "success";
            $result['feed_id'] = $feed_id;

            $this->db->order_by('feed_id', 'DESC', false);
            $select = "bl.like_id, bl.feed_id, bl.user_id, bl.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = bl.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_admin_update_likes bl", array('bl.feed_id' => $feed_id), $select);
            //$result['like_qry'] = $this->db->last_query();
            $result['response'] = $this->load->view('front/feeds/incLikesModal', $data, true);

            $feed_data              = $this->master_model->getRecords("arai_admin_update", array('feed_id' => $feed_id), "feed_title");
            $result['blog_title'] = "Technovuus Update Likes : " . $feed_data[0]['feed_title'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

      public function delete_file_ajax()
    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        $file_id       = $encrptopenssl->decrypt($this->input->post('file_id', true));

        $csrf_new_token           = $this->security->get_csrf_hash();
        $result['csrf_new_token'] = $csrf_new_token;

        $this->master_model->updateRecord('arai_admin_update_files', array('is_deleted' => '1'), array('file_id' => $file_id));
        $result['flag']    = 'success';
        $result['file_id'] = $file_id;

        //START : INSERT LOG
        $postArr                   = $this->input->post();
        $json_encode_data          = json_encode($postArr);
        $logDetails['user_id']     = $this->session->userdata('admin_id');
        $logDetails['module_name'] = 'Delete Technovuus update file';
        $logDetails['store_data']  = $json_encode_data;
        $logDetails['ip_address']  = $this->get_client_ip();
        $logDetails['createdAt']   = date('Y-m-d H:i:s');
        $logDetails['action_name'] = 'Delete Technovuus update File';
        $logData                   = $this->master_model->insertRecord('logs', $logDetails);
        //END : INSERT LOG

        echo json_encode($result);
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function updateStatus()
    {
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $status         = $this->input->post('status');
        $reject_reason  = $this->input->post('reject_reason');

        $updateQuery = $this->master_model->updateRecord('arai_admin_update', array('admin_status' => $status), array('feed_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Admin update Status Updated By Admin",
            'module_name' => 'Admin update',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        if ($status == '2') //Rejected
        {
            $updateQuery1 = $this->master_model->updateRecord('arai_admin_update', array('reject_reason' => $reject_reason), array('feed_id' => $id));

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            // echo $this->db->last_query();die;
            echo json_encode($jsonData);
        } else if ($status == '1') //Approved
        {

            $updateQuery1 = $this->master_model->updateRecord('arai_admin_update', array('reject_reason' => ''), array('feed_id' => $id));

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        } else {
            $updateQuery1 = $this->master_model->updateRecord('arai_admin_update', array('reject_reason' => ''), array('feed_id' => $id));
            $jsonData     = array("token" => $csrf_test_name, "status" => 'done');

            echo json_encode($jsonData);
        }
    }

    public function block_unblock() // BLOCK/UNBLOCK BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $feed_id              = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));
            $type                 = trim($this->security->xss_clean($this->input->post('type')));
            $user_id              = $this->session->userdata('admin_id');
            $result['flag']       = "success";

            if ($type == 'Block') {

                $up_data['is_block']     = 1;
                $up_data['block_reason'] = $popupBlogBlockReason;
            } else if ($type == 'Unblock') {
                $up_data['is_block']     = 0;
                $up_data['block_reason'] = '';}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('admin_update', $up_data, array('feed_id' => $feed_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Admin updates By Admin",
                'module_name' => 'Admin updates Admin',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function featured()
    {
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        // $value = ucfirst($this->uri->segment(5));
        //echo $id;
        $c_data = $this->master_model->getRecords("admin_update", array("feed_id" => $id));
        if ($c_data[0]['is_featured'] == "0") {$is_featured = '1';} else { $is_featured = '0';}

        $updateQuery = $this->master_model->updateRecord('admin_update', array('is_featured' => $is_featured), array('feed_id' => $id));
        //echo $this->db->last_query();
        $c_res = $this->master_model->getRecords("admin_update", array("feed_id" => $id));

        if ($c_res[0]['is_featured'] == "0") {$text = 'Featured';} else { $text = 'Not Featured';}

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Admin update Featured Status",
            'module_name' => 'ADmin Update',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("c_status" => $is_featured, "u_featured" => $text, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function deleted()
    {
        $user_id        = $this->session->userdata('admin_id');
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $updateQuery    = $this->master_model->updateRecord('admin_update', array('is_deleted' => 1), array('feed_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Admin update Deleted From Admin",
            'module_name' => 'ADmin Update',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function setOrder()
    {

        $feed_id        = $this->input->post('feed_id');
        $xOrder         = $this->input->post('xOrder');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('admin_update', array('xOrder' => $xOrder), array('feed_id' => $feed_id));

        $status   = "Order Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

     public function banner()
    {
          $this->load->library('upload');
                
        if( isset($_POST['submit']) && $_POST['submit']!='' )
        {   
            
             if ($_FILES['banner_img']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $banner_img = $this->Common_model_sm->upload_single_file("banner_img", array('png', 'jpg', 'jpeg', 'gif'), "banner_img_" . date("YmdHis"), "./uploads/admin_update_files/banner", "png|jpeg|jpg|gif");
                    if ($banner_img['response'] == 'error') {
                        $data['banner_img_error'] = $banner_img['message'];
                        $file_upload_flag         = 1;
                    } else if ($banner_img['response'] == 'success') {
                        $add_data['banner'] = $banner_img['message'];
                        
                    }
                }

                $add_data['updated_on']   = date('Y-m-d H:i:s');
                $this->master_model->updateRecord('arai_feeds_banner', $add_data, array('banner_id' => '1'));
            
            
            
        } 
        $data['content_data'] = $this->master_model->getRecords("arai_feeds_banner",array('banner_id'=>'1'));
        $data['module_name'] = 'Login Page';
        $data['submodule_name'] = 'Login Page';
        $data['middle_content']='admin_update/banner';
        $this->load->view('admin/admin_combo',$data);
     }

}
