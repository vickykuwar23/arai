<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Plan
Author : Vicky K
*/

class Plan extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("plan");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['plan_type'] 		= $row_val['plan_type'];
				$row_val['plan_category'] 	= $row_val['plan_category'];
				$row_val['plan_desc'] 		= $encrptopenssl->decrypt($row_val['plan_desc']);
				$row_val['plan_name'] 		= $encrptopenssl->decrypt($row_val['plan_name']);
				$row_val['plan_amount'] 	= $encrptopenssl->decrypt($row_val['plan_amount']);
				$row_val['plan_duration'] 	= $encrptopenssl->decrypt($row_val['plan_duration']);
				$row_val['status'] 			= $row_val['status'];
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Plan';
		$data['submodule_name'] = '';	
    	$data['middle_content']='plan/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation		
		$this->form_validation->set_rules('p_type', 'Plan Type', 'required|xss_clean');
		$this->form_validation->set_rules('p_name', 'Plan Name', 'required|xss_clean');
		$this->form_validation->set_rules('p_amount', 'Plan Amount', 'required|xss_clean');
		$this->form_validation->set_rules('p_duration', 'Plan Duration', 'required|xss_clean');
		$this->form_validation->set_rules('p_category', 'Plan Category', 'required|xss_clean');
		$this->form_validation->set_rules('plan_desc', 'Plan Description', 'required|xss_clean');
		if($this->input->post('p_category')!= 1){
			
			$this->form_validation->set_rules('no_of_users', 'No Of Users', 'required|xss_clean');
		}
		
		
		
		if($this->form_validation->run())
		{	
			
			$plan_type 		= $this->input->post('p_type');
			$plan_name 		= $encrptopenssl->encrypt($this->input->post('p_name'));
			$plan_amount 	= $encrptopenssl->encrypt($this->input->post('p_amount'));
			$plan_duration 	= $encrptopenssl->encrypt($this->input->post('p_duration'));
			$plan_desc 		= $encrptopenssl->encrypt($this->input->post('plan_desc'));
			$p_category		= $this->input->post('p_category');
			$no_of_users	= $this->input->post('no_of_users');
			$admin_id		= $this->session->userdata('admin_id');
			$insertArr = array( 'admin_id' => $admin_id, 'plan_category' => $p_category, 'plan_desc' => $plan_desc, 'no_of_users' => $no_of_users, 'plan_type' => $plan_type, 'plan_name' => $plan_name, 'plan_amount' => $plan_amount, 'plan_duration' => $plan_duration);			
			
			//echo "<pre>";print_r($insertArr);die();
			$insertQuery = $this->master_model->insertRecord('plan',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Plan successfully created');
				redirect(base_url('xAdmin/plan'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/plan/add'));
			}
		}
		$data['module_name'] = 'Plan';
		$data['submodule_name'] = '';
        $data['middle_content']='plan/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$plan_data = $this->master_model->getRecords("plan", array('plan_id' => $id));
		$res_arr = array();
		if(count($plan_data) > 0){ 
			$row_val['plan_type'] 		= $plan_data[0]['plan_type'];
			$row_val['plan_category'] 	= $plan_data[0]['plan_category'];
			$row_val['plan_name'] 		= $encrptopenssl->decrypt($plan_data[0]['plan_name']);
			$row_val['plan_amount'] 	= $encrptopenssl->decrypt($plan_data[0]['plan_amount']);
			$row_val['plan_duration'] 	= $encrptopenssl->decrypt($plan_data[0]['plan_duration']);
			$row_val['plan_desc'] 		= $encrptopenssl->decrypt($plan_data[0]['plan_desc']);
			$row_val['no_of_users'] 	= $plan_data[0]['no_of_users'];
			$res_arr[] = $row_val;
		}
		
		$data['plan_data'] = $res_arr;
        
		// Check Validation
		$this->form_validation->set_rules('p_type', 'Plan Type', 'required|xss_clean');
		$this->form_validation->set_rules('p_name', 'Plan Name', 'required|xss_clean');
		$this->form_validation->set_rules('p_amount', 'Plan Amount', 'required|xss_clean');
		$this->form_validation->set_rules('p_duration', 'Plan Duration', 'required|xss_clean');
		$this->form_validation->set_rules('p_category', 'Plan Category', 'required|xss_clean');
		$this->form_validation->set_rules('plan_desc', 'Plan Description', 'required|xss_clean');
		if($this->input->post('p_category')!= 1){
			
			$this->form_validation->set_rules('no_of_users', 'No Of Users', 'required|xss_clean');
		}
		
		if($this->form_validation->run())
		{	
	
			$plan_type 		= $this->input->post('p_type');
			$plan_name 		= $encrptopenssl->encrypt($this->input->post('p_name'));
			$plan_amount 	= $encrptopenssl->encrypt($this->input->post('p_amount'));
			$plan_duration 	= $encrptopenssl->encrypt($this->input->post('p_duration'));
			$plan_desc 		= $encrptopenssl->encrypt($this->input->post('plan_desc'));
			$p_category		= $this->input->post('p_category');
			$no_of_users	= $this->input->post('no_of_users');		
			$admin_id		= $this->session->userdata('admin_id');
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'admin_id' => $admin_id, 'plan_category' => $p_category, 'plan_desc' => $plan_desc, 'no_of_users' => $no_of_users, 'plan_type' => $plan_type, 'plan_name' => $plan_name, 'plan_amount' => $plan_amount, 'plan_duration' => $plan_duration, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('plan',$updateArr,array('plan_id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Plan successfully updated');
				redirect(base_url('xAdmin/plan'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/plan/edit/'.$id));
			}
		}
		$data['module_name'] = 'Plan';
		$data['submodule_name'] = '';
        $data['middle_content']='plan/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('plan',array('status'=>$value),array('plan_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/plan'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('plan',array('status'=>$value),array('plan_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/plan'));		
		 }
		 
	 }
	 
	 public function generateCode(){
		 
			$csrf_test_name = $this->security->get_csrf_hash();
		 
			$limit = 6;
		    $string =  substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
			
			$jsonData = array('random' => $string, 'token' => $csrf_test_name);
			
			echo json_encode($jsonData);
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('promo_codes',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/promocode'));	
		 
	 }


}