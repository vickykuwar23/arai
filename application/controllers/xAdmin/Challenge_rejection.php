<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Challenge_rejection Master
Author : Vicky K
*/

class Challenge_rejection extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("challenge_rejection");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['reject_reason'] = $encrptopenssl->decrypt($row_val['reject_reason']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Reason';
		$data['submodule_name'] = 'Challenge_rejection';	
    	$data['middle_content']='challenge-rejection/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('reject_reason', 'Rejection Reason', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$reject_reason = $encrptopenssl->encrypt($this->input->post('reject_reason'));
			
			$insertArr = array( 'reject_reason' => $reject_reason);			
			$insertQuery = $this->master_model->insertRecord('challenge_rejection',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Reason successfully created');
				redirect(base_url('xAdmin/challenge_rejection'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challenge_rejection/add'));
			}
		}
		$data['module_name'] = 'Reason';
		$data['submodule_name'] = 'Challenge_rejection';
        $data['middle_content']='challenge-rejection/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("challenge_rejection", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['reject_reason'] = $encrptopenssl->decrypt($tech_data[0]['reject_reason']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['rejection_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('reject_reason', 'Rejection Reason', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$reject_reason = $encrptopenssl->encrypt($this->input->post('reject_reason'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'reject_reason' => $reject_reason, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('challenge_rejection',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Reason successfully updated');
				redirect(base_url('xAdmin/challenge_rejection'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challenge_rejection/edit/'.$id));
			}
		}
		$data['module_name'] = 'Reason';
		$data['submodule_name'] = 'Challenge_rejection';
        $data['middle_content']='challenge-rejection/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('challenge_rejection',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/challenge_rejection'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('challenge_rejection',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/challenge_rejection'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('challenge_rejection',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Reason successfully deleted');
		 redirect(base_url('xAdmin/challenge_rejection'));	
		 
	 }


}