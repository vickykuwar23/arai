<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : FAQ
Author : Vicky K
*/

class Faq extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		$this->load->helper('auth');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		$this->check_permissions->is_authorise_admin(4);
  }

    public function index()
    {
		//isLogin();
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("faq");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['faq_title'] = $encrptopenssl->decrypt($row_val['faq_title']);
				$row_val['faq_desc']  = $encrptopenssl->decrypt($row_val['faq_desc']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'FAQ';
		$data['submodule_name'] = '';	
    	$data['middle_content']='faq/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {		
		// Upload Library Initialization
		$this->load->library('upload');
		
		// Object Created
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('faq_title', 'FAQ Title', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('faq_desc', 'FAQ Description', 'required|xss_clean');		
		
		if($this->form_validation->run())
		{	
			$faq_title 	= $encrptopenssl->encrypt($this->input->post('faq_title'));
			$faq_desc  	= $encrptopenssl->encrypt($this->input->post('faq_desc'));
			$faq_img  	= $_FILES['faq_img']['name'];
			$faq_order 	= $this->input->post('faq_order');
			
			
			if($_FILES['faq_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/faq';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '50000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('faq_img', $_FILES, $config, FALSE);
			    $faq_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$faq_image = $upload_files[0];
						
				}

				/*$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($faq_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/blog'));	
				}
						
				$filesize = $_FILES['faq_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // FAQ File End
			
			if($faq_image[0]!="")
			{
				$faq_img = $encrptopenssl->encrypt($faq_image[0]);	
			} else {
				$faq_img = '';
			}
			
			if($faq_order!="")
			{
				$orders = $faq_order;	
			} else {
				$orders = 0;
			}
			
			
			$insertArr = array( 'faq_title' => $faq_title, 'faq_desc' => $faq_desc, 'faq_img' => $faq_img,  'xOrder' => $orders);			
			$insertQuery = $this->master_model->insertRecord('faq',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','FAQ successfully created');
				redirect(base_url('xAdmin/faq'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/faq/add'));
			}
		}
		$data['module_name'] = 'FAQ';
		$data['submodule_name'] = '';	
        $data['middle_content']='faq/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		// Upload Library Initialization
		$this->load->library('upload');
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$faq_data = $this->master_model->getRecords("faq", array('id' => $id));
		
		$res_arr = array();
		if(count($faq_data) > 0){ 
			$row_val['faq_title'] 	= $encrptopenssl->decrypt($faq_data[0]['faq_title']);
			$row_val['faq_desc']  	= $encrptopenssl->decrypt($faq_data[0]['faq_desc']);
			$row_val['faq_img']  	= $encrptopenssl->decrypt($faq_data[0]['faq_img']);
			$row_val['xOrder']  	= $faq_data[0]['xOrder'];
			$res_arr[] = $row_val;
		}
		
		$data['faq_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('faq_title', 'FAQ Title', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('faq_desc', 'FAQ Description', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$faq_title = $encrptopenssl->encrypt($this->input->post('faq_title'));
			$faq_desc  = $encrptopenssl->encrypt($this->input->post('faq_desc'));
			$faq_img  	= $_FILES['faq_img']['name'];
			$faq_order 	= $this->input->post('faq_order');
			
			if($_FILES['faq_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/faq';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '50000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('faq_img', $_FILES, $config, FALSE);
			    $faq_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$faq_image = $upload_files[0];
						
				}

				/*$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($faq_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/blog'));	
				}
						
				$filesize = $_FILES['faq_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // FAQ File End
			
			if($faq_order!="")
			{
				$orders = $faq_order;
				
			} else {
				$orders = 0;
			}
			
			if($faq_image[0]!="")
			{
				$faq_img = $encrptopenssl->encrypt($faq_image[0]);	
				$updateArr = array('faq_title' => $faq_title, 'faq_desc' => $faq_desc, 'faq_img' => $faq_img,  'xOrder' => $orders, 'updatedAt' => $updateAt);
			
			} else {
				$updateArr = array('faq_title' => $faq_title, 'faq_desc' => $faq_desc, 'xOrder' => $orders, 'updatedAt' => $updateAt);
			}
			
			
			
			$updateAt = date('Y-m-d H:i:s');
			//$updateArr = array('faq_title' => $faq_title, 'faq_desc' => $faq_desc, 'faq_img' => $faq_img,  'xOrder' => $orders, 'updatedAt' => $updateAt);			
			
			$updateQuery = $this->master_model->updateRecord('faq',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','FAQ successfully updated');
				redirect(base_url('xAdmin/faq'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/faq/edit/'.$id));
			}
		}
		$data['module_name'] = 'FAQ';
		$data['submodule_name'] = '';
        $data['middle_content']='faq/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('faq',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/faq'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('faq',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/faq'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('faq',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/faq'));	
		 
	 }


}