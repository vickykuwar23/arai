<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class Services extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("services");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['service_title'] = $encrptopenssl->decrypt($row_val['service_title']);
				$row_val['service_desc'] = $encrptopenssl->decrypt($row_val['service_desc']);
				$row_val['service_icon'] = $encrptopenssl->decrypt($row_val['service_icon']);
				$row_val['service_url'] = $encrptopenssl->decrypt($row_val['service_url']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Services';	
    	$data['middle_content']='services/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation
		$this->form_validation->set_rules('service_title', 'Service Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('service_desc', 'Service Description', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('service_url', 'Service URL', 'required');
		
		
		if($this->form_validation->run())
		{	
			$service_title = $encrptopenssl->encrypt($this->input->post('service_title'));
			$service_desc = $encrptopenssl->encrypt($this->input->post('service_desc'));
			$service_url = $encrptopenssl->encrypt($this->input->post('service_url'));

			if($_FILES['service_icon']['name']!=""){			
				$config['upload_path']      = 'assets/service_icon';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('service_icon', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/services/add'));	
				}			
				$filesize = $_FILES['service_icon']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$service_icon_file = $encrptopenssl->encrypt($b_image[0]);				   
			}
			
			$insertArr = array( 'service_title' => $service_title, 'service_desc' => $service_desc, 'service_icon' => $service_icon_file, 'service_url' => $service_url );			
			$insertQuery = $this->master_model->insertRecord('services',$insertArr);
			//echo $this->db->last_query();die();
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Services successfully created');
				redirect(base_url('xAdmin/services'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/services/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Services';
        $data['middle_content']='services/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		// $id = base64_decode($did);
		$tech_data = $this->master_model->getRecords("services", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['service_title'] = $encrptopenssl->decrypt($tech_data[0]['service_title']);
			$row_val['service_desc'] = $encrptopenssl->decrypt($tech_data[0]['service_desc']);
			$row_val['service_icon'] = $encrptopenssl->decrypt($tech_data[0]['service_icon']);
			$row_val['service_url'] = $encrptopenssl->decrypt($tech_data[0]['service_url']);
			$res_arr[] = $row_val;
		}

		$data['services_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('service_title', 'Service Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('service_desc', 'Service Description', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('service_url', 'Service URL', 'required');
		if($this->form_validation->run())
		{	
			$service_title = $encrptopenssl->encrypt($this->input->post('service_title'));
			$service_desc = $encrptopenssl->encrypt($this->input->post('service_desc'));
			$service_url = $encrptopenssl->encrypt($this->input->post('service_url'));

			$service_icon_file = '';
			if($_FILES['service_icon']['name']!=""){			
				$config['upload_path']      = 'assets/service_icon';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;			 
				$upload_files  = @$this->master_model->upload_file('service_icon', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];		
				}
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");		
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);					
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/services/add'));	
				}			
				$filesize = $_FILES['service_icon']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$service_icon_file = $encrptopenssl->encrypt($b_image[0]);				   
			} // End File

			$tech_data = $this->master_model->getRecords("services", array('id' => $id));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 
						'service_title' => $service_title,
						'service_desc' => $service_desc,
						'service_icon' => !empty($service_icon_file) ? $service_icon_file : $tech_data[0]['service_icon'],
						'service_url' => $service_url,
						'updatedAt' => $updateAt
					);
			//echo "<pre>";print_r($updateArr);die();			
			$updateQuery = $this->master_model->updateRecord('services',$updateArr,array('id' => $id));
			//echo  $this->db->last_query();die();
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Service successfully updated');
				redirect(base_url('xAdmin/services'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/services/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Services';
        $data['middle_content']='services/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('services',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/services'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('services',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/services'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}