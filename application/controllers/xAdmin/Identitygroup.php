<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Categories
Author : Vicky K
*/

class Identitygroup extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$this->db->select('usertype.user_type, identity_group.id, identity_group.identity_group_name, identity_group.subscription_amount, identity_group.status, usertype.id AS ut_id');
		$this->db->join('usertype','identity_group.id=usertype.id','left');	
		$response_data = $this->master_model->getRecords("identity_group",'','',array('id'=>'DESC'));
		//echo "<pre>";print_r($response_data);die();
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['user_type'] 				= $encrptopenssl->decrypt($row_val['user_type']);
				$row_val['id']   					= $row_val['id'];
				$row_val['subscription_amount']   	= $row_val['subscription_amount'];
				$row_val['user_type_id']   			= $row_val['ut_id'];
				$row_val['identity_group_name']   	= $encrptopenssl->decrypt($row_val['identity_group_name']);
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);
		$data['records'] = $res_arr;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Identitygroup';	
    	$data['middle_content']='identity_group/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$usertype_cat = $this->master_model->getRecords("usertype", array('is_deleted' => 0, 'status' => "Active"));
		
		$res_arr = array();
		if(count($usertype_cat)){	
						
			foreach($usertype_cat as $row_val){		
						
				$row_val['user_type'] = $encrptopenssl->decrypt($row_val['user_type']);
				$res_arr[] = $row_val;
			}
			
		}
		$data['usertype_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('user_type', 'User Type', 'required');
		$this->form_validation->set_rules('identity_name', 'Identity Group', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$user_type 	= $encrptopenssl->encrypt($this->input->post('user_type'));
			$identity_name = $encrptopenssl->encrypt($this->input->post('identity_name'));
			
			$insertArr = array( 'user_type_id' => $user_type, 'identity_group_name' => $identity_name );			
			$insertQuery = $this->master_model->insertRecord('identity_group',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Idenity Group successfully created');
				redirect(base_url('xAdmin/identitygroup'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/identitygroup/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Identitygroup';	
    	$data['middle_content']='identity_group/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$usertype_cat = $this->master_model->getRecords("usertype", array('is_deleted' => 0, 'status' => "Active"));
		$usertype_data = $this->master_model->getRecords("identity_group", array('id' => $id));
        $res_arr = array();
		$res_cat = array();
		if(count($usertype_data) > 0){
			$row_val['identity_group_name'] = $encrptopenssl->decrypt($usertype_data[0]['identity_group_name']);
			$row_val['user_type_id'] = $encrptopenssl->decrypt($usertype_data[0]['user_type_id']);
			$res_arr[] = $row_val;
		}
		
		if(count($usertype_cat)){	
						
			foreach($usertype_cat as $row_val){		
						
				$row_val['id'] = $row_val['id'];
				$row_val['user_type'] = $encrptopenssl->decrypt($row_val['user_type']);
				$res_cat[] = $row_val;
			}
			
		}
		
		$data['usertype_data'] = $res_arr;
		$data['usertype_cat'] = $res_cat;
		
		// Check Validation
		$this->form_validation->set_rules('user_type', 'User Type', 'required');
		$this->form_validation->set_rules('identity_name', 'Identity Group', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$user_type 		= $encrptopenssl->encrypt($this->input->post('user_type'));
			$identity_name  = $encrptopenssl->encrypt($this->input->post('identity_name'));
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'user_type_id' => $user_type, 'identity_group_name' => $identity_name, 'updatedAt' => $updateAt );			
			$updateQuery = $this->master_model->updateRecord('identity_group',$updateArr,array('id' => $id));
			
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Idenity Group successfully updated');
				redirect(base_url('xAdmin/identitygroup'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/identitygroup/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Identitygroup';
        $data['middle_content']='identity_group/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('user_subcat',array('status'=>$value),array('subcat_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/usersubcat'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('user_subcat',array('status'=>$value),array('subcat_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/usersubcat'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('user_subcat',array('is_deleted'=>1),array('subcat_id' => $id));
		 $this->session->set_flashdata('success','User Sub Category successfully deleted');
		 redirect(base_url('xAdmin/usersubcat'));	
		 
	 }
	 
	 public function subscription(){
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$encode_id 	= $this->uri->segment(4);
		$decode_id  = base64_decode($encode_id);

		$usertype_data = $this->master_model->getRecords("identity_group", array('id' => $decode_id));
        $res_arr = array();
		if(count($usertype_data) > 0){
			$row_val['subscription_amount'] = $usertype_data[0]['subscription_amount'];
			$res_arr[] = $row_val;
		}	
		//echo "<pre>";print_r($res_arr);die();
		// Check Validation
		$this->form_validation->set_rules('subscription_amt', 'Subscription Amount', 'required|xss_clean');
		if($this->form_validation->run())
		{
			 $subAmt = $this->input->post('subscription_amt');
			 $updateQuery = $this->master_model->updateRecord('identity_group',array('subscription_amount'=>$subAmt),array('id' => $decode_id));
			 $this->session->set_flashdata('success','Subscription Amount successfully updated');
			 redirect(base_url('xAdmin/identitygroup'));	
		}		
		
		// User Type Name
		$user_type_name = $this->master_model->getRecords("usertype",array('id'=>$decode_id));
		$data['user_type_name'] = $encrptopenssl->decrypt($user_type_name[0]['user_type']);
		
		$data['subscription_data'] = $res_arr;
		$data['module_name'] = 'Master';	
		$data['submodule_name'] = 'Identitygroup';	
    	$data['middle_content']='identity_group/subscription';
		$this->load->view('admin/admin_combo',$data);
		
	}
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('user_access_type',array('id' => $id));
		 $this->session->set_flashdata('success','User Access Type successfully deleted');
		 redirect(base_url('xAdmin/useraccesstype'));	
		 
	 }*/


}