<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Promo Code
Author : Vicky K
*/

class Promocode extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("promo_codes",'','',array('id' => 'DESC'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['promo_code'] 		= $encrptopenssl->decrypt($row_val['promo_code']);
				$row_val['discount_apply'] 	= $encrptopenssl->decrypt($row_val['discount_apply']);
				$row_val['start_date'] 		= $encrptopenssl->decrypt($row_val['start_date']);
				$row_val['end_date'] 		= $encrptopenssl->decrypt($row_val['end_date']);
				$row_val['status'] 			= $row_val['status'];
				$row_val['use_cnt'] 			= $row_val['use_cnt'];
				$row_val['code_type'] 			= $row_val['code_type'];
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Promocode';
		$data['submodule_name'] = '';	
    	$data['middle_content']='promo-code/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
        $this->form_validation->set_rules('plan_id', 'Plan', 'required|xss_clean'); 
		$this->form_validation->set_rules('code_type', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('discount_apply', 'Promo Code Discount', 'required|xss_clean');
		$this->form_validation->set_rules('p_code', 'Promo Code', 'required|xss_clean');
		$this->form_validation->set_rules('use_cnt', 'Allowed Usage Count', 'required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|xss_clean');
		
		
		if($this->form_validation->run())
		{	
			$code_type 		= $this->input->post('code_type');
			$plan_id 		= $this->input->post('plan_id');
			$discount_apply = $encrptopenssl->encrypt($this->input->post('discount_apply'));
			$p_code 		= $encrptopenssl->encrypt($this->input->post('p_code'));
			$start_date 	= $encrptopenssl->encrypt($this->input->post('start_date'));
			$end_date 		= $encrptopenssl->encrypt($this->input->post('end_date'));
			$use_cnt 		= $this->input->post('use_cnt');
			$admin_id		= $this->session->userdata('admin_id');
			$insertArr = array( 'plan_id'=>$plan_id,'admin_id' => $admin_id, 'code_type' => $code_type, 'discount_apply' => $discount_apply, 'promo_code' => $p_code, 'start_date' => $start_date, 'end_date' => $end_date, 'use_cnt' => $use_cnt);			
			$insertQuery = $this->master_model->insertRecord('promo_codes',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Promo Code successfully created');
				redirect(base_url('xAdmin/promocode'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/promocode/add'));
			}
		}

		$plans  = $this->master_model->getRecords('plan',array('status'=>'Active') );
		$plans_arr = array();
		if(count($plans)){	
						
			foreach($plans as $row_val){		
						
				$row_val['plan_name'] = $encrptopenssl->decrypt($row_val['plan_name']);
				$row_val['plan_amount'] = $encrptopenssl->decrypt($row_val['plan_amount']);
				$row_val['plan_desc'] = $encrptopenssl->decrypt($row_val['plan_desc']);
				$row_val['plan_duration'] = $encrptopenssl->decrypt($row_val['plan_duration']);
				$plans_arr[] = $row_val;
			}
			
		} 
		
		// echo "<pre>"; print_r($plans_arr);die;
    	$data['plans'] = $plans_arr;

		$data['module_name'] = 'Promocode';
		$data['submodule_name'] = '';
        $data['middle_content']='promo-code/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$promo_data = $this->master_model->getRecords("promo_codes", array('id' => $id));
		$res_arr = array();
		if(count($promo_data) > 0){ 
			$row_val['plan_id'] 		= $promo_data[0]['plan_id'];
			$row_val['promo_code'] 		= $encrptopenssl->decrypt($promo_data[0]['promo_code']);
			$row_val['discount_apply'] 	= $encrptopenssl->decrypt($promo_data[0]['discount_apply']);
			$row_val['start_date'] 		= $encrptopenssl->decrypt($promo_data[0]['start_date']);
			$row_val['end_date'] 		= $encrptopenssl->decrypt($promo_data[0]['end_date']);
			$row_val['code_type'] 		= $promo_data[0]['code_type'];
			$row_val['use_cnt'] 		= $promo_data[0]['use_cnt'];
			$res_arr[] = $row_val;
		}
		
		$data['promo_data'] = $res_arr;
		// print_r($data['promo_data']);die;
        // Check Validation
        $this->form_validation->set_rules('plan_id', 'Plan', 'required|xss_clean'); 
		$this->form_validation->set_rules('discount_apply', 'Promo Code Discount', 'required|xss_clean');
		$this->form_validation->set_rules('p_code', 'Promo Code', 'required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|xss_clean');
		$this->form_validation->set_rules('code_type', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('use_cnt', 'Allowed Usage Count', 'required|xss_clean');

		
		if($this->form_validation->run())
		{				

			$plan_id 		= $this->input->post('plan_id');
			$code_type 		= $this->input->post('code_type');
			$use_cnt 		= $this->input->post('use_cnt');
			$discount_apply = $encrptopenssl->encrypt($this->input->post('discount_apply'));
			$p_code 		= $encrptopenssl->encrypt($this->input->post('p_code'));
			$start_date 	= $encrptopenssl->encrypt($this->input->post('start_date'));
			$end_date 		= $encrptopenssl->encrypt($this->input->post('end_date'));
			$admin_id		= $this->session->userdata('admin_id');
			
			$updateAt = date('Y-m-d H:i:s');
			//$updateArr = array( 'admin_id' => $admin_id, 'discount_apply' => $discount_apply, 'promo_code' => $p_code, 'start_date' => $start_date, 'end_date' => $end_date, 'updatedAt' => $updateAt);			
			$updateArr = array('plan_id'=>$plan_id, 'updated_by_id' => $admin_id, 'code_type' => $code_type, 'discount_apply' => $discount_apply, 'start_date' => $start_date, 'end_date' => $end_date, 'use_cnt' => $use_cnt, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('promo_codes',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Promo Code successfully updated');
				redirect(base_url('xAdmin/promocode'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/promocode/edit/'.$id));
			}
		}
		$plans  = $this->master_model->getRecords('plan',array('status'=>'Active') );
		$plans_arr = array();
		if(count($plans)){	
						
			foreach($plans as $row_val){		
				$row_val['plan_name'] = $encrptopenssl->decrypt($row_val['plan_name']);
				$row_val['plan_amount'] = $encrptopenssl->decrypt($row_val['plan_amount']);
				$row_val['plan_desc'] = $encrptopenssl->decrypt($row_val['plan_desc']);
				$row_val['plan_duration'] = $encrptopenssl->decrypt($row_val['plan_duration']);
				$plans_arr[] = $row_val;
			}
			
		} 
		
		// echo "<pre>"; print_r($plans_arr);die;
    	$data['plans'] = $plans_arr;
		$data['module_name'] = 'Promocode';
		$data['submodule_name'] = '';
        $data['middle_content']='promo-code/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('promo_codes',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/promocode'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('promo_codes',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/promocode'));		
		 }
		 
	 }
	 
	 public function generateCode(){
		 
			$csrf_test_name = $this->security->get_csrf_hash();
		 
			$limit = 6;
		    $string =  substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
			
			$jsonData = array('random' => $string, 'token' => $csrf_test_name);
			
			echo json_encode($jsonData);
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('promo_codes',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/promocode'));	
		 
	 }


}