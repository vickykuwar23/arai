<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Challengecategories extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt'); 
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
    	$response_data = $this->master_model->getRecords("challengecategories");		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['challenge_cat'] = $encrptopenssl->decrypt($row_val['challenge_cat']);						
				$res_arr[] = $row_val;
			}
			
		}
		
    	$data['records'] = $res_arr;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengecategories';	
    	$data['middle_content']='challengecategories/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
        // Check Validation
		$this->form_validation->set_rules('challenge_cat', 'Challenge Categories', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$challenge_cat = $encrptopenssl->encrypt($this->input->post('challenge_cat'));
			
			$insertArr = array( 'challenge_cat' => $challenge_cat);			
			$insertQuery = $this->master_model->insertRecord('challengecategories',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Challenge categories successfully created');
				redirect(base_url('xAdmin/challengecategories'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challengecategories/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengecategories';
        $data['middle_content']='challengecategories/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$data['usertype_data'] = $this->master_model->getRecords("challengecategories", array('id' => $id));
        // Check Validation
		$this->form_validation->set_rules('challenge_cat', 'Challenge Categories', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$challenge_cat = $encrptopenssl->encrypt($this->input->post('challenge_cat'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'challenge_cat' => $challenge_cat, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('challengecategories',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Challenge categories successfully updated');
				redirect(base_url('xAdmin/challengecategories'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challengecategories/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengecategories';
        $data['middle_content']='challengecategories/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('challengecategories',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/challengecategories'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('challengecategories',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/challengecategories'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('challengecategories',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Challenge categories successfully deleted');
		 redirect(base_url('xAdmin/challengecategories'));	
		 
	 }


}