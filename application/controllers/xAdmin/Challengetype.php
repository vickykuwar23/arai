<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Challengetype extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("challengetype");
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['challenge_type'] = $encrptopenssl->decrypt($row_val['challenge_type']);						
				$res_arr[] = $row_val;
			}
			
		}
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengetype';	
    	$data['middle_content']='challengetype/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('challenge_type', 'Challenge Visibility', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$challenge_type = $encrptopenssl->encrypt($this->input->post('challenge_type'));
			
			$insertArr = array( 'challenge_type' => $challenge_type);			
			$insertQuery = $this->master_model->insertRecord('challengetype',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Challenge Visibility successfully created');
				redirect(base_url('xAdmin/challengetype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challengetype/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengetype';
        $data['middle_content']='challengetype/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$data['usertype_data'] = $this->master_model->getRecords("challengetype", array('id' => $id));
        // Check Validation
		$this->form_validation->set_rules('challenge_type', 'Challenge Visibility', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$challenge_type = $encrptopenssl->encrypt($this->input->post('challenge_type'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'challenge_type' => $challenge_type, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('challengetype',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Challenge Visibility successfully updated');
				redirect(base_url('xAdmin/challengetype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/challengetype/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Challengetype';
        $data['middle_content']='challengetype/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('challengetype',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/challengetype'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('challengetype',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/challengetype'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('challengetype',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Challenge Visibility successfully deleted');
		 redirect(base_url('xAdmin/challengetype'));	
		 
	 }


}