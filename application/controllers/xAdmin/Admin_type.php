<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin Type
Author : Vicky K
*/

class Admin_type extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("admin_type", array("id!=" => '3'));
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['admin_type_name'] = $row_val['admin_type_name'];	
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);die();
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Admin_Type';	
    	$data['middle_content']='admin-type/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('admin_name', 'Admin Type', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$admin_name = $encrptopenssl->encrypt($this->input->post('admin_name'));
			
			$insertArr = array( 'admin_type_name' => $admin_name);			
			$insertQuery = $this->master_model->insertRecord('admin_type',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Admin Type successfully created');
				redirect(base_url('xAdmin/admin_type'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/admin_type/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Admin_Type';
        $data['middle_content']='admin-type/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$cat_data = $this->master_model->getRecords("admin_type", array('id' => $id));
		$res_data = array();
		if(count($cat_data) > 0){
			$row_val['admin_type_name'] = $cat_data[0]['admin_type_name'];
			$res_data[] = $row_val;
		}
		
		$data['admin_data'] = $res_data;
		
        // Check Validation
		$this->form_validation->set_rules('admin_name', 'Admin Type', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$admin_name = $this->input->post('admin_name');
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array(  'admin_type_name' => $admin_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('admin_type',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Admin Type successfully updated');
				redirect(base_url('xAdmin/admin_type'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/admin_type/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Admin_Type';
        $data['middle_content']='admin-type/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 
	 public function permission(){
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$encode_id 	= $this->uri->segment(4);
		$decode_id  = base64_decode($encode_id);		
		
		$ad_data = $this->master_model->getRecords("admin_type", array('id' => $decode_id));
		
		/*$res_cat = array();
		if(count($ad_data) > 0){
			$row_val['admin_type_name'] = $encrptopenssl->decrypt($ad_data[0]['admin_type_name']);
			$res_cat[] = $row_val;
		}
		
		print_r($ad_data);die();*/
		$cat_data = $this->master_model->getRecords("categories", array("status" => 'Active'));
		$res_data = array();
		
		if(count($cat_data) > 0){
			
			foreach($cat_data as $key => $catDetails){
				
				$subcat_details = $this->master_model->getRecords("subcategory",array('category_id'=>$catDetails['id']),'',array('sub_id '=>'ASC'));
				$i = 0;
				$row_val['category_name'] = $encrptopenssl->decrypt($catDetails['category_name']);
				foreach($subcat_details as $subcatlist){					
					$res_data[$key][$row_val['category_name']][$subcatlist['sub_id']] = $encrptopenssl->decrypt($subcatlist['subcategory_name']);
					$i++;
				}
				
				
			} // Foreach end	
			
		} //count end	
		
		// Selected Values
		$this->db->select('permission');	
		$subids = $this->master_model->getRecords("admin_type",array('id'=>$decode_id));
		
		// Check Validation
		$this->form_validation->set_rules('sub_cat_id[]', 'Checked Actions', 'required|xss_clean');
		if($this->form_validation->run())
		{
			$posts = $this->input->post('sub_cat_id');		
			$implode = implode(",", $posts);	
			
			 $updateQuery = $this->master_model->updateRecord('admin_type',array('permission'=>$implode),array('id' => $decode_id));
			 $this->session->set_flashdata('success','Permission successfully updated');
			 redirect(base_url('xAdmin/admin_type/permission/'.$encode_id));			
		}
		
		$data['added_values'] = $subids;	
		$data['admin_data'] = $ad_data;	
		$data['categories_data'] = $res_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Admin_Type';	
    	$data['middle_content']='admin-type/actions';
		$this->load->view('admin/admin_combo',$data);
		 
	 }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('categories',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/categories'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('categories',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/categories'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('categories',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Category successfully deleted');
		 redirect(base_url('xAdmin/categories'));	
		 
	 }


}