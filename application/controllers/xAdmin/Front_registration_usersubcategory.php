<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Front registration usersubcategory
Author : Suraj M
*/

class Front_registration_usersubcategory extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("registration_usersubcategory");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['usersubcategory_name'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usersubcategory';	
    	$data['middle_content']='front_registration_usersubcategory/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();

		$usercategories = $this->master_model->getRecords("registration_usercategory", array('status' => "Active"));

		$res_arr = array();
		if(count($usercategories)){	
						
			foreach($usercategories as $row_val){		
						
				$row_val['usercategory_name'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_arr[] = $row_val;
			}
			
		}

		$data['usercategory_data'] = $res_arr;
		
        // Check Validation
        $this->form_validation->set_rules('usercategory_name', 'User Category Name', 'required');
		$this->form_validation->set_rules('usersubcategory_name', 'User Subcategory Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{
			$usercategory_name = $this->input->post('usercategory_name');
			$usersubcategory_name = $encrptopenssl->encrypt($this->input->post('usersubcategory_name'));
			
			$insertArr = array( 'sub_catname' => $usersubcategory_name, 'u_cat_id' => $usercategory_name);			
			$insertQuery = $this->master_model->insertRecord('registration_usersubcategory',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User Subcategory successfully created');
				redirect(base_url('xAdmin/front_registration_usersubcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/front_registration_usersubcategory/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usersubcategory';
        $data['middle_content']='front_registration_usersubcategory/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();

		$usercategories = $this->master_model->getRecords("registration_usercategory", array('status' => "Active"));

		$res_arr = array();
		if(count($usercategories)){	
						
			foreach($usercategories as $row_val){		
						
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_arr[] = $row_val;
			}
			
		}
		
		$data['usercategory_data'] = $res_arr;
		
		$tech_data = $this->master_model->getRecords("registration_usersubcategory", array('subcat_id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['usersubcategory_name'] = $encrptopenssl->decrypt($tech_data[0]['sub_catname']);
			$row_val['usercategory_id'] = $tech_data[0]['u_cat_id'];
			$res_arr[] = $row_val;
		}
		// echo "<pre>";
		// print_r($res_arr);die;
		$data['Usersubcategory_data'] = $res_arr;
        // Check Validation
        $this->form_validation->set_rules('usercategory_name', 'User Category Name', 'required');
		$this->form_validation->set_rules('usersubcategory_name', 'User Subcategory Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usercategory_name = $this->input->post('usercategory_name');
			$usersubcategory_name = $encrptopenssl->encrypt($this->input->post('usersubcategory_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'sub_catname' => $usersubcategory_name, 'u_cat_id' => $usercategory_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('registration_usersubcategory',$updateArr,array('subcat_id ' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User Subcategory Name successfully updated');
				redirect(base_url('xAdmin/front_registration_usersubcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/front_registration_usersubcategory/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usersubcategory';
        $data['middle_content']='front_registration_usersubcategory/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('registration_usersubcategory',array('status'=>$value),array('subcat_id ' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/front_registration_usersubcategory'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('registration_usersubcategory',array('status'=>$value),array('subcat_id ' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/front_registration_usersubcategory'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}