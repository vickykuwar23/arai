<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class Area_of_interest_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("area_of_interest");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['area_of_interest'] = $encrptopenssl->decrypt($row_val['area_of_interest']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Area_of_interest_master';	
    	$data['middle_content']='area_of_interest/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('area_of_interest', 'Area of Interest', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$area_of_interest = $encrptopenssl->encrypt($this->input->post('area_of_interest'));
			
			$insertArr = array( 'area_of_interest' => $area_of_interest);			
			$insertQuery = $this->master_model->insertRecord('area_of_interest',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Area of Interest successfully created');
				redirect(base_url('xAdmin/area_of_interest_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/area_of_interest_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Area_of_interest_master';
        $data['middle_content']='area_of_interest/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("area_of_interest", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['area_of_interest'] = $encrptopenssl->decrypt($tech_data[0]['area_of_interest']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['area_of_interest_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('area_of_interest', 'Area of Interest Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$area_of_interest = $encrptopenssl->encrypt($this->input->post('area_of_interest'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'area_of_interest' => $area_of_interest, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('area_of_interest',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Area of Interest Name successfully updated');
				redirect(base_url('xAdmin/area_of_interest_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/area_of_interest_master/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Area_of_interest_master';
        $data['middle_content']='area_of_interest/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('area_of_interest',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/area_of_interest_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('area_of_interest',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/area_of_interest_master'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}