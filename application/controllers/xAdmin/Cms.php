<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : CMS Type
Author : Vicky K
*/

class Cms extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("cms");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['page_title'] 		 = $encrptopenssl->decrypt($row_val['page_title']);
				$row_val['page_description'] = $encrptopenssl->decrypt($row_val['page_description']);	
				$res_arr[] = $row_val;
			}
			
		}
		
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'CMS';	
    	$data['middle_content']='cms/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('page_title', 'Page Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('page_description', 'Page Description', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$page_title = $encrptopenssl->encrypt($this->input->post('page_title'));
			$page_desc  = $encrptopenssl->encrypt($this->input->post('page_description'));

			$video_url  = $this->input->post('video_url');
			
			$insertArr = array( 'page_title' => $page_title, 'page_description' => $page_desc,'video_url'=>$video_url);			
			$insertQuery = $this->master_model->insertRecord('cms',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','CMS content successfully created');
				redirect(base_url('xAdmin/cms'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/cms/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'CMS';
        $data['middle_content']='cms/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($cid)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$id 		= base64_decode($cid);
		$cms_data 	= $this->master_model->getRecords("cms", array('id' => $id));
		
		$res_arr = array();
		if(count($cms_data)){
			
			$row_val['page_title'] = $encrptopenssl->decrypt($cms_data[0]['page_title']);
			$row_val['page_description'] = $encrptopenssl->decrypt($cms_data[0]['page_description']);
			$row_val['video_url'] = $cms_data[0]['video_url'];
			$res_arr[] = $row_val;	
		}
		
        // Check Validation
		$this->form_validation->set_rules('page_title', 'Page Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('page_description', 'Page Description', 'required|xss_clean');
		if($this->form_validation->run())
		{	
			$page_title = $encrptopenssl->encrypt($this->input->post('page_title'));
			$page_desc  = $encrptopenssl->encrypt($this->input->post('page_description'));
			$video_url  = $this->input->post('video_url');
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'page_title' => $page_title, 'page_description' => $page_desc ,'video_url'=>$video_url,'updatedAt' => $updateAt);	
			
			$updateQuery = $this->master_model->updateRecord('cms',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','CMS content successfully updated');
				redirect(base_url('xAdmin/cms'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/cms/edit/'.$cid));
			}
		}
		// print_r($res_arr);die;
		$data['cms_data'] = $res_arr;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'CMS';
        $data['middle_content']='cms/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('cms',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/cms'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('cms',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/cms'));		
		 }
		 
	 }
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('cms',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','CMS page successfully deleted');
		 redirect(base_url('xAdmin/cms'));	
		 
	 }*/


}