<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Setting extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->helper('url');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
    }
	 
	 public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		//$data['setting_data'] = $this->master_model->getRecords("setting");
		$setting_data = $this->master_model->getRecords("setting");
		
		$res_arr = array();
		if(count($setting_data)){	
						
			foreach($setting_data as $row_val){		
						
				$row_val['meta_title'] 			= $encrptopenssl->decrypt($row_val['meta_title']);
				$row_val['meta_description'] 	= $encrptopenssl->decrypt($row_val['meta_description']);
				$row_val['site_title'] 			= $encrptopenssl->decrypt($row_val['site_title']);
				$row_val['email_id'] 			= $encrptopenssl->decrypt($row_val['email_id']);
				$row_val['contact_no'] 			= $encrptopenssl->decrypt($row_val['contact_no']);
				$row_val['favicon_icon'] 		= $encrptopenssl->decrypt($row_val['favicon_icon']);
				$row_val['site_logo'] 			= $encrptopenssl->decrypt($row_val['site_logo']);
				$row_val['field_1'] 			= $encrptopenssl->decrypt($row_val['field_1']);
				$row_val['field_2'] 			= $encrptopenssl->decrypt($row_val['field_2']);
				$row_val['field_4'] 			= $encrptopenssl->decrypt($row_val['field_4']);
				$row_val['field_3'] 			= $encrptopenssl->decrypt($row_val['field_3']);
				$row_val['facebook_url'] 		= $encrptopenssl->decrypt($row_val['facebook_url']);
				$row_val['twitter_url'] 		= $encrptopenssl->decrypt($row_val['twitter_url']);
				$row_val['linkedin_url'] 		= $encrptopenssl->decrypt($row_val['linkedin_url']);
				$row_val['google_plus_url'] 	= $encrptopenssl->decrypt($row_val['google_plus_url']);
				$row_val['off_address'] 		= $encrptopenssl->decrypt($row_val['off_address']);
				$row_val['challenge_alert'] 	= $row_val['challenge_alert'];
				$row_val['news_alert'] 			= $row_val['news_alert'];
				$row_val['expert_connect'] 			= $row_val['expert_connect'];
				$res_arr[] = $row_val;
			}
			
		}		
		
		$data['setting_data'] = $res_arr;	
		
        // Check Validation
		$this->form_validation->set_rules('site_title', 'Site Title', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('contact_no', 'Contact No', 'required|xss_clean');
		$this->form_validation->set_rules('meta_title', 'Meta Title', 'required|xss_clean');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'required|xss_clean');
		$this->form_validation->set_rules('off_address', 'Office Address', 'required|xss_clean');
		$this->form_validation->set_rules('home_quote_title', 'Quote Title', 'required|xss_clean');
		$this->form_validation->set_rules('home_partner_title', 'Partner Title', 'required|xss_clean');
				
		if($this->form_validation->run())
		{	
			//echo "<pre>";print_r($this->input->post());die();
			$site_title 		= $encrptopenssl->encrypt($this->input->post('site_title'));
			$email_id 			= $encrptopenssl->encrypt($this->input->post('email_id'));
			$contact_no 		= $encrptopenssl->encrypt($this->input->post('contact_no'));
			$meta_title 		= $encrptopenssl->encrypt($this->input->post('meta_title'));
			$meta_description 	= $encrptopenssl->encrypt($this->input->post('meta_description'));
			$off_address 		= $encrptopenssl->encrypt($this->input->post('off_address'));
			$field_1 			= $encrptopenssl->encrypt($this->input->post('field_1')); 
			$field_2 			= $encrptopenssl->encrypt($this->input->post('field_2'));
			$facebook_url 		= $encrptopenssl->encrypt($this->input->post('facebook_url'));
			$twitter_url 		= $encrptopenssl->encrypt($this->input->post('twitter_url'));
			$linkedin_url 		= $encrptopenssl->encrypt($this->input->post('linkedin_url'));
			$google_plus_url 	= $encrptopenssl->encrypt($this->input->post('google_plus_url'));
			$home_partner_title 	= $encrptopenssl->encrypt($this->input->post('home_partner_title'));
			$home_quote_title 	= $encrptopenssl->encrypt($this->input->post('home_quote_title'));
			$news_alert			= $this->input->post('news_alert');
			$challenge_alert	= $this->input->post('challenge_alert');
			$expert_connect	= $this->input->post('expert_connect');
			$favicon_icon		= $_FILES['favicon_icon']['name'];
			$site_logo			= $_FILES['site_logo']['name'];
			$favi_img			= "";
			$site_logo			= "";
			
			if($news_alert){$nVal = 'Y';} else {$nVal = 'N';}
			if($challenge_alert){$cVal = 'Y';} else {$cVal = 'N';}
			$expert_connect_val='N';
			if ($expert_connect=='Y') {$expert_connect_val='Y';}
			
			if($_FILES['favicon_icon']['name']!=""){			
				
				$config['upload_path']      = 'assets/setting';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('favicon_icon', $_FILES, $config, FALSE);
			    $fav_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$fav_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($fav_image[0], PATHINFO_EXTENSION);
				//echo "===".$ext;die();				
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));	
				}
				$favi_img = $fav_image[0];		
				$filesize = $_FILES['favicon_icon']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Favicon Icon End
			
			if($_FILES['site_logo']['name']!=""){			
			
				$config['upload_path']      = 'assets/setting';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('site_logo', $_FILES, $config, FALSE);
			    $logo_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$logo_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($logo_image[0], PATHINFO_EXTENSION);
				//echo "===".$ext;die();				
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));	
				}
				$site_logo = $logo_image[0];		
				$filesize = $_FILES['site_logo']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;	
			
			} // Logo Icon End			
			
			
			if($favi_img == ""){
				
				$file_favicon = $encrptopenssl->encrypt($data['setting_data'][0]['favicon_icon']);	
				
			} else {
				
				$file_favicon = $encrptopenssl->encrypt($favi_img);
			} 
			
			if($site_logo == ""){
				
				$file_logo = $encrptopenssl->encrypt($data['setting_data'][0]['site_logo']);
				
			} else {
				
				$file_logo = $encrptopenssl->encrypt($site_logo);
			}
			
			$createdAt		=   date('Y-m-d H:i:s');
			
			
			$updateArr = array(	
								'meta_title' 		=> $meta_title, 
								'meta_description' 	=> $meta_description, 
								'site_title' 		=> $site_title, 
								'email_id' 			=> $email_id,
								'contact_no' 		=> $contact_no,
								'off_address'		=> $off_address,
								'favicon_icon' 		=> $file_favicon,
								'site_logo' 		=> $file_logo,
								'field_1' 			=> $field_1,
								'field_2' 			=> $field_2,
								'field_3' 			=> $home_quote_title,
								'field_4' 			=> $home_partner_title,
								'facebook_url' 		=> $facebook_url,
								'twitter_url' 		=> $twitter_url,
								'linkedin_url' 		=> $linkedin_url,
								'google_plus_url' 	=> $google_plus_url,
								'news_alert' 		=> $nVal,
								'challenge_alert' 	=> $cVal,
								'expert_connect'=> $expert_connect_val,
								'updatedAt' 		=> $createdAt								
							);
			// echo "<pre>";print_r($updateArr);die;
								
			$updateQuery = $this->master_model->updateRecord('setting', $updateArr,array('id' => '1'));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Setting successfully updated');
				redirect(base_url('xAdmin/Setting'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/Setting/edit/'.$id));
			}
			
			
		} // Validation End
		
		$data['module_name'] = 'Setting';
		$data['submodule_name'] = '';
        $data['middle_content']='setting/index';
        $this->load->view('admin/admin_combo',$data);
     }


}