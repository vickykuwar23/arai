<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Payment_gateway extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->helper('url');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }
	 
	 public function index()
    {
		error_reporting(0);
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$setting_data = $this->master_model->getRecords("payment_details");
		
		$res_arr = array();
		if(count($setting_data)){	
						
			foreach($setting_data as $row_val){		
						
				$row_val['live_key'] 			= $encrptopenssl->decrypt($row_val['live_key']);
				$row_val['live_secret_key'] 	= $encrptopenssl->decrypt($row_val['live_secret_key']);
				$row_val['live_url'] 			= $encrptopenssl->decrypt($row_val['live_url']);
				$row_val['test_key'] 			= $encrptopenssl->decrypt($row_val['test_key']);
				$row_val['test_secret_key'] 	= $encrptopenssl->decrypt($row_val['test_secret_key']);
				$row_val['test_url'] 			= $encrptopenssl->decrypt($row_val['test_url']);
				$row_val['status'] 				= $row_val['status'];
				$res_arr[] = $row_val;
			}
			
		}		
		
		$data['setting_data'] = $res_arr;	
		
        // Check Validation
		$this->form_validation->set_rules('live_key', 'Live Key', 'required|xss_clean');
		$this->form_validation->set_rules('live_secret_key', 'Live Secret Key', 'required|xss_clean');
		$this->form_validation->set_rules('live_url', 'Live URL', 'required|xss_clean');
		$this->form_validation->set_rules('test_key', 'Test Key', 'required|xss_clean');
		$this->form_validation->set_rules('test_secret_key', 'Test Secret Key', 'required|xss_clean');
		$this->form_validation->set_rules('test_url', 'Test URL', 'required|xss_clean');
		
				
		if($this->form_validation->run())
		{	
			
			$live_key 			= $encrptopenssl->encrypt($this->input->post('live_key'));
			$live_secret_key 	= $encrptopenssl->encrypt($this->input->post('live_secret_key'));
			$live_url 			= $encrptopenssl->encrypt($this->input->post('live_url'));
			$test_key 			= $encrptopenssl->encrypt($this->input->post('test_key'));
			$test_secret_key 	= $encrptopenssl->encrypt($this->input->post('test_secret_key'));
			$test_url 			= $encrptopenssl->encrypt($this->input->post('test_url'));
			$status				= $this->input->post('status');
			$createdAt			=   date('Y-m-d H:i:s');
			if($status == 1){$check = 'Block';}else{$check = 'Active';}
			//echo "<pre>";print_r($this->input->post());die();
			$updateArr = array(	
								'live_key' 			=> $live_key, 
								'live_secret_key' 	=> $live_secret_key, 
								'live_url' 			=> $live_url, 
								'test_key' 			=> $test_key,
								'test_secret_key' 	=> $test_secret_key,
								'test_url' 			=> $test_url,
								'status' 			=> $check,
								'updatedAt' 		=> $createdAt								
							);
								
			$updateQuery = $this->master_model->updateRecord('payment_details', $updateArr,array('id' => '1'));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Payment Gateway successfully updated');
				redirect(base_url('xAdmin/payment_gateway'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/payment_gateway'));
			}
			
			
		} // Validation End
		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Payment_Gateway';
        $data['middle_content']='payment_gateway/index';
        $this->load->view('admin/admin_combo',$data);
     }


}