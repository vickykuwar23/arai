<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Popups extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(24);
    }

    public function index()
    {
			$response_data = $this->master_model->getRecords("arai_popups");
			$data['records'] = $response_data; 
			$data['module_name'] = 'popups';
			$data['submodule_name'] = 'Email';	
			$data['middle_content']='popups/index';
			$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$this->form_validation->set_rules('title', 'Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('content', 'Content', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean');
		if($this->form_validation->run())
		{	

			$title = $this->input->post('title');
			$slug = $this->input->post('slug');
			$content  = $this->input->post('content');
			$insertArr = array( 'title' => $title,  'content' => $content,  'slug' => $slug);			
			$insertQuery = $this->master_model->insertRecord('popups',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Content successfully created');
				redirect(base_url('xAdmin/popups'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/popups/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Email';
		$data['middle_content']='popups/add';
		$this->load->view('admin/admin_combo',$data);
     }
	 
public function edit($id){
		$popup_data = $this->master_model->getRecords("popups", array('id' => $id));
		$this->form_validation->set_rules('title', 'Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('content', 'Content', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean');
		if($this->form_validation->run())
		{	
			$title = $this->input->post('title');
			$slug = $this->input->post('slug');
			$content  = $this->input->post('content');
			
			$updateArr = array( 'title' => $title,  'content' => $content,  'slug' => $slug);		
			
			$updateQuery = $this->master_model->updateRecord('popups',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Content successfully updated');
				redirect(base_url('xAdmin/popups'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/popups/edit/'.$id));
			}
		}
		$data['popup_data'] = $popup_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Email';
		$data['middle_content']='popups/edit';
		$this->load->view('admin/admin_combo',$data);
}
	 
	 public function changeStatus(){
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 $updateQuery = $this->master_model->updateRecord('popups',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/popups'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('popups',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/popups'));		
		 }
		 
	 }
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('cms',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','CMS page successfully deleted');
		 redirect(base_url('xAdmin/cms'));	
		 
	 }*/


}