<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin for admin basic function
Author : Vishal P.
*/
class Membership extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index()
    {		
		$encrptopenssl =  New Opensslencryptdecrypt();
    	$memerlist  = $this->master_model->getRecords('membership');
		
		$res_arr = array();
		if(count($memerlist)){	
						
			foreach($memerlist as $row_val){		
						
				$row_val['membership_name'] = $encrptopenssl->decrypt($row_val['membership_name']);
				$res_arr[] = $row_val;
			}
			
		}
		
    	$data['records'] = $res_arr;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Membership';	
    	$data['middle_content']='membership/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('membership', 'Membership Type', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$membership = $encrptopenssl->encrypt($this->input->post('membership'));
			
			$insertArr = array( 'membership_name' => $membership);			
			$insertQuery = $this->master_model->insertRecord('membership',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Membership successfully created');
				redirect(base_url('xAdmin/membership'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/membership/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Membership';	
        $data['middle_content']='membership/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$membership_data = $this->master_model->getRecords("membership", array('id' => $id));
		$res_data = array();
		if(count($membership_data) > 0){
			$row_val['membership_name'] = $encrptopenssl->decrypt($membership_data[0]['membership_name']);
			$res_data[] = $row_val;
		}
		
		$data['membership_data'] = $res_data;
		
		
		
        // Check Validation
		$this->form_validation->set_rules('membership', 'Membership Type', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$membership = $encrptopenssl->encrypt($this->input->post('membership'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'membership_name' => $membership, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('membership',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Membership successfully updated');
				redirect(base_url('xAdmin/membership'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/membership/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Membership';	
        $data['middle_content']='membership/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('membership',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/membership'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('membership',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/membership'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('membership',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Membership successfully deleted');
		 redirect(base_url('xAdmin/membership'));	
		 
	 }


}