<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : UserS Listing
Author : Vicky K
*/

class Promocode_assoc extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
        $this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {					
        $encrptopenssl =  New Opensslencryptdecrypt();
    				     $this->db->select('arai_subscriptions.promo_code,promo_code_type,registration.title,registration.first_name,registration.middle_name,registration.last_name,registration.email,institution_full_name,mobile');		
    					 $this->db->join('arai_subscriptions','arai_subscriptions.user_id=registration.user_id');
    	$response_data = $this->master_model->getRecords("registration",array('is_valid'=>'1','is_deleted'=>'0'),'',array('user_id','desc'));
    

        $res_arr = array();
        if(count($response_data)){  
                        
            foreach($response_data as $row_val){        
                
                $row_val['promo_code']                       = $encrptopenssl->decrypt($row_val['promo_code']);
                $row_val['title']                       = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']                  = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']                 = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']                   = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']                       = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']                       = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['institution_full_name']                   = $encrptopenssl->decrypt($row_val['institution_full_name']);
                $res_arr[] = $row_val;
            }
            
        }
        $data['records'] = $res_arr; 
        $data['module_name'] = 'Promo Code Association ';
        $data['submodule_name'] = '';   
        $data['middle_content']='promo_assoc/index';
        $this->load->view('admin/admin_combo',$data);
    }

}