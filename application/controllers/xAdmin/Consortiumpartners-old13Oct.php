<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Consortium Partners
Author : Vicky K
*/

class Consortiumpartners extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("partners",array('partner_type' => "C"));
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['partner_name'] = $encrptopenssl->decrypt($row_val['partner_name']);
				$row_val['partner_img'] = $encrptopenssl->decrypt($row_val['partner_img']);
				$row_val['xOrder'] = $row_val['xOrder'];
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);die();
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'C_Partners';	
    	$data['middle_content']='c_partners/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
        // Check Validation
		$this->form_validation->set_rules('partner_name', 'Partner Name', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('xorder', 'Logo Order', 'required');
		//$this->form_validation->set_rules('partner_img', 'Partner Logo', 'required|xss_clean');
		if (empty($_FILES['partner_img']['name']))
		{
			$this->form_validation->set_rules('partner_img', 'Partner Logo', 'required');
		}
		if($this->form_validation->run())
		{	
			$partner_name 	= $encrptopenssl->encrypt($this->input->post('partner_name'));	
			$xorder 	= $encrptopenssl->encrypt($this->input->post('xorder'));			
			$partner_img  	= $_FILES['partner_img']['name'];
			
			if($_FILES['partner_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/partners';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('partner_img', $_FILES, $config, FALSE);
			    $p_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$p_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($p_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/consortiumpartners/add'));	
				}
						
				$filesize = $_FILES['partner_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Partners Image End
			
			$file_names = $encrptopenssl->encrypt($p_image[0]);	
			
			$insertArr = array('partner_type' => "C", 'partner_name' => $partner_name, 'partner_img' => $file_names, 'xOrder' => $xorder);			
			$insertQuery = $this->master_model->insertRecord('partners',$insertArr);
			//echo $this->db->last_query();die();
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Consortium Partners successfully created');
				redirect(base_url('xAdmin/consortiumpartners'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/consortiumpartners/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'C_Partners';
        $data['middle_content']='c_partners/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		$partner_data = $this->master_model->getRecords("partners", array('id' => $id));
		$res_data = array();
		if(count($partner_data) > 0){
			$row_val['partner_name'] = $encrptopenssl->decrypt($partner_data[0]['partner_name']);
			$row_val['partner_img'] = $encrptopenssl->decrypt($partner_data[0]['partner_img']);
			$row_val['xOrder'] = $partner_data[0]['xOrder'];
			$res_data[] = $row_val;
		}
		
		
		$data['partner_data'] = $res_data;
		
        // Check Validation
		$this->form_validation->set_rules('partner_name', 'Partner Name', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('xorder', 'Logo Order', 'required');
		//$this->form_validation->set_rules('partner_img', 'Partner Logo', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$partner_name 	= $encrptopenssl->encrypt($this->input->post('partner_name'));	
			$xorder 		= $this->input->post('xorder');		
			$partner_img  	= $_FILES['partner_img']['name'];
			
			$partnerLogo = '';
			
			if($_FILES['partner_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/partners';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('partner_img', $_FILES, $config, FALSE);
			    $p_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$p_image = $upload_files[0];
						
				}

				
				
				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($p_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/consortiumpartners/add'));	
				}
				
				$partnerLogo = $p_image[0];	
				
				$filesize = $_FILES['partner_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Partners Image End
			
			if($partnerLogo == ""){
				
				$file_names = $partner_data[0]['partner_img'];
				
			} else {
				
				$file_names = $encrptopenssl->encrypt($partnerLogo);
			} 
			
			
			//echo ">>>".$xorder;die();
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('partner_name' => $partner_name, 'partner_img' => $file_names, 'updatedAt' => $updateAt, 'xOrder' => $xorder);			
			//echo $this->db->last_query();die();
			$updateQuery = $this->master_model->updateRecord('partners',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Consortium Partners successfully updated');
				redirect(base_url('xAdmin/consortiumpartners'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/consortiumpartners/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'C_Partners';
        $data['middle_content']='c_partners/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 
	 
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('partners',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/consortiumpartners'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('partners',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/consortiumpartners'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('partners',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Consortium Partenrs successfully deleted');
		 redirect(base_url('xAdmin/consortiumpartners'));	
		 
	 }


}