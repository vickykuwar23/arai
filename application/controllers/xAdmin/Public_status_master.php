<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Public status master
Author : Suraj M
*/

class Public_status_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("public_status_master");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['public_status_name'] = $encrptopenssl->decrypt($row_val['public_status_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Public_status_master';	
    	$data['middle_content']='public_status/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('public_status_name', 'Public Status Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$public_status_name = $encrptopenssl->encrypt($this->input->post('public_status_name'));
			
			$insertArr = array( 'public_status_name' => $public_status_name);			
			$insertQuery = $this->master_model->insertRecord('public_status_master',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Status successfully created');
				redirect(base_url('xAdmin/public_status_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/public_status_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Public_status_master';
        $data['middle_content']='public_status/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("public_status_master", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['public_status_name'] = $encrptopenssl->decrypt($tech_data[0]['public_status_name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['status_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('public_status_name', 'Status Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$public_status_name = $encrptopenssl->encrypt($this->input->post('public_status_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'public_status_name' => $public_status_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('public_status_master',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Status Name successfully updated');
				redirect(base_url('xAdmin/public_status_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/public_status_master/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Public_status_master';
        $data['middle_content']='public_status/edit';
        $this->load->view('admin/admin_combo',$data);
    }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('public_status_master',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/public_status_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('public_status_master',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/public_status_master'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}