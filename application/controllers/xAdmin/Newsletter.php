<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Newsletter extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(6);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$response_data = $this->master_model->getRecords("newsletter", array("is_deleted" => 0));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){
				
				$row_val['email_id'] = $encrptopenssl->decrypt($row_val['email_id']);
				$row_val['news_notification'] = $row_val['news_notification'];
				$row_val['challenge_notification'] = $row_val['challenge_notification'];
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Newsletter';
		$data['submodule_name'] = '';	
    	$data['middle_content']='newsletter/index';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	 public function restoreList()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$response_data = $this->master_model->getRecords("newsletter", array("is_deleted" => 1));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){
				
				$row_val['email_id'] = $encrptopenssl->decrypt($row_val['email_id']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Newsletter';
		$data['submodule_name'] = '';	
    	$data['middle_content']='newsletter/restore';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('newsletter',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/newsletter'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('newsletter',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/newsletter'));		
		 }
		 
	 }
	 
	 public function deleteStatus(){
		 
		error_reporting(0); 		
		$id 		= $this->input->post('id');
		//echo "<pre>";print_r($this->input->post());die();
		
		$csrf_test_name = $this->security->get_csrf_hash(); 
		
		$user_data = $this->master_model->getRecords("newsletter", array("id" => $id));	
		
		 if($user_data[0]['is_deleted'] == '0'){ 			
			$updateQuery 	= $this->master_model->updateRecord('newsletter',array('is_deleted'=> '1'),array('id' => $id));
			$user_data 		= $this->master_model->getRecords("newsletter", array("id" => $id));	
			$status 		= $user_data[0]['is_deleted'];
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$textShows		= 'Subscribe'; 
			$anchorShows	= 'Unsubscribe'; 
			$jsonData = array("response_id" => $status, "token" => $csrf_test_name, "dynamicText" => $textShows, "spanText" => $anchorShows, "remove_id" => $id);
			echo json_encode($jsonData); 
			
		 } else if($user_data[0]['is_deleted'] == '1'){
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$updateQuery 	= $this->master_model->updateRecord('newsletter',array('is_deleted'=>'0'),array('id' => $id));
			$user_data 		= $this->master_model->getRecords("newsletter", array("id" => $id));	
			
			$status 		= $user_data[0]['is_deleted'];
			$textShows		= 'Unsubscribe'; 
			$anchorShows	= 'Subscribe';
			$jsonData = array("response_id" => $status, "token" => $csrf_test_name, "dynamicText" => $textShows, "spanText" => $anchorShows, "remove_id" => $id);
			echo json_encode($jsonData); 	
		 }		
		 
	 }
	 
	 public function modifiedAlerts(){
		 
		$csrf_test_name = $this->security->get_csrf_hash();

		 $getType 	= $this->input->post('post_type');
		 $id 		= $this->input->post('id');
		 $chkStatus	= $this->input->post('chkStatus');
		 $updatedAt		=   date('Y-m-d H:i:s');
		 if($getType == 'Blog'){
			 $updateQuery = $this->master_model->updateRecord('newsletter',array('news_notification'=>$chkStatus, 'updatedAt' => $updatedAt),array('id' => $id)); 
			 //echo $this->db->last_query();
		 } else if($getType == 'Challenge'){
			 $updateQuery = $this->master_model->updateRecord('newsletter',array('challenge_notification'=>$chkStatus, 'updatedAt' => $updatedAt),array('id' => $id));  
			//echo $this->db->last_query();
		 }
		
		$json_data = array( "message" => '1');		
		$json_data['token'] = $csrf_test_name;
		
        echo json_encode($json_data); 
		
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('newsletter',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Newsletter successfully deleted');
		 redirect(base_url('xAdmin/newsletter'));	
		 
	 }


}