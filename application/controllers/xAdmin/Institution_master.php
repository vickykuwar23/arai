<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Institution Master
Author : Suraj M
*/

class Institution_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("institution_master");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['institution_name'] = $encrptopenssl->decrypt($row_val['institution_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Institution';	
    	$data['middle_content']='institution/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('institution_name', 'Institution Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$institution_name = $encrptopenssl->encrypt($this->input->post('institution_name'));
			
			$insertArr = array( 'institution_name' => $institution_name);			
			$insertQuery = $this->master_model->insertRecord('institution_master',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Institution successfully created');
				redirect(base_url('xAdmin/institution_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/institution_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Institution';
        $data['middle_content']='institution/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("institution_master", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['institution_name'] = $encrptopenssl->decrypt($tech_data[0]['institution_name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['institution_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('institution_name', 'Institution Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$institution_name = $encrptopenssl->encrypt($this->input->post('institution_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'institution_name' => $institution_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('institution_master',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Institution Name successfully updated');
				redirect(base_url('xAdmin/institution_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/institution_master/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Institution';
        $data['middle_content']='institution/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('institution_master',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/institution_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('institution_master',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/institution_master'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}