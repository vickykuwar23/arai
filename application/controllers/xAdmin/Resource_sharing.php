<?php 
	if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Resource_sharing extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->library('excel');
			if($this->session->userdata('admin_id') == "")
			{			
				redirect(base_url('xAdmin/admin'));
			}
			$this->check_permissions->is_authorise_admin(14);
		}
		
    public function index()
    {
			error_reporting(0);
			
			// load excel library
			$this->load->library('excel');	
			
			if(isset($_POST['export']) && !empty($_POST['export'])) 
			{
				/* print_r($_POST); exit; */
				//error_reporting(0);
				$encrptopenssl =  New Opensslencryptdecrypt();				
				$response = array();
				
				$keyword = @$this->input->post('keyword')?$this->input->post('keyword'):'';
				
				## Search 
				$search_arr = array();
				$searchQuery = "";
				if($keyword != '')
				{
					$string = $encrptopenssl->encrypt($keyword);
					$search_arr[] = " (	b.blog_id_disp like '%".$keyword."%' or b.blog_title like '%".$string."%' or b.owner_name like '%".$string."%') ";
					// or arai_challenge.json_str like '%".$keyword."%'
				}
				
				if(count($search_arr) > 0) { $searchQuery = implode(" and ",$search_arr); }		 
				
				## Total number of records without filtering
				/* $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.owner_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_blog_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_blog_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
				$records = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
				$totalRecords = count($records); */
				
				## Total number of record with filtering		
				/* $totalRecordwithFilter = count($records);
				if($searchQuery != '')
				{
					$this->db->where($searchQuery);
					$this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.owner_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason');
					$recordsFilter = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
					$totalRecordwithFilter = count($recordsFilter);
				}		
				 */
				 
				## Fetch records		
				if($searchQuery != '') { $this->db->where($searchQuery); }				
				

				$select = "r.*,c.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR ',') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags)) AS TagName";	
					$this->db->order_by('r_id','DESC',FALSE);
					$this->db->join('arai_cities c','c.city_id = r.location', 'LEFT', FALSE);
					$records = $this->master_model->getRecords("arai_resource_sharing r",array('r.is_deleted' => '0'),$select,array('r.r_id' => 'DESC'));

					// print_r($records);die;

				
								
				$data = array();
				$i = 0;
				
				// Count Check
				if(count($records) > 0)
				{			
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex(0);
					// set Header				
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Resource ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Resource Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Resource Details');
					$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Resource Specification');
					$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Tags');
					$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Owner Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Owner Email');
					$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Owner Mobile');
					$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Location');
					$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'xOrder');
					$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Featured Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Admin Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Block Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Block Reason');					
					
					// set Row
					$rowCount = 2;					
					foreach($records as $row_val)
					{			
						
						if($row_val['is_featured'] == '0') { $isFeatured = 'Not Featured'; }else { $isFeatured = 'Featured'; } 
						if($row_val['admin_status'] == '0') { $adminStatus = 'Pending'; } else if($row_val['admin_status'] == '1') { $adminStatus = 'Approved'; } else { $adminStatus = 'Rejected'; } 
						if($row_val['is_block'] == '0') { $isBlock = 'Not Block'; }else { $isBlock = 'Block'; } 
						
						$i++;						
						$objPHPExcel->getActiveSheet()->SetCellValue('A'. $rowCount, $row_val['r_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'. $rowCount, $row_val['id_disp']);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'. $rowCount, $row_val['resouce_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('D'. $rowCount, htmlspecialchars_decode($row_val['resource_details']));
						$objPHPExcel->getActiveSheet()->SetCellValue('E'. $rowCount, $row_val['resource_specification']);
						$objPHPExcel->getActiveSheet()->SetCellValue('F'. $rowCount, $row_val['TagName']);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'. $rowCount, $row_val['owner_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('H'. $rowCount, $row_val['owner_email']);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'. $rowCount, $row_val['owner_contact_number']);
						$objPHPExcel->getActiveSheet()->SetCellValue('J'. $rowCount, $row_val['city_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('K'. $rowCount, $row_val['xOrder']);
						$objPHPExcel->getActiveSheet()->SetCellValue('L'. $rowCount, $isFeatured);
						$objPHPExcel->getActiveSheet()->SetCellValue('M'. $rowCount, $adminStatus);
						$objPHPExcel->getActiveSheet()->SetCellValue('N'. $rowCount, $isBlock);
						$objPHPExcel->getActiveSheet()->SetCellValue('O'. $rowCount, $row_val['block_reason']);						
						$rowCount++;						
					} // Foreach End					
				} // Count Check  
				
				// create file name
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="Resource_sharing_'.date("YmdHis").'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit;  
				
			}
    	
    	$data['records'] = ''; 
			$data['module_name'] = 'Resource sharing';
			$data['submodule_name'] = '';	
    	$data['middle_content']='resource_sharing/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_kr_data()
		{			
			//error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
	 		$table        = 'arai_resource_sharing r';

	 		$column_order = array('r.r_id','r.r_id','r.id_disp','r.resouce_name','r.created_on','r.admin_status','r.xOrder',
				'(SELECT COUNT(like_id) FROM arai_resource_sharing_likes WHERE r_id = r.r_id) AS TotalLikes',
				'(SELECT COUNT(connect_id) FROM arai_resource_sharing_connect_request WHERE r_id = r.r_id ) AS TotalReq','r.is_block',

        ); //SET COLUMNS FOR SORT

	 		 $column_search = array(
            'r.id_disp'
          ); 
      //SET COLUMN FOR SEARCH

 		  $order = array('r.r_id' => 'DESC'); // DEFAULT ORDER

      $WhereForTotal = "WHERE r.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
      $Where         = "WHERE r.is_deleted = 0 	";

       if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
        		// echo $_POST['order']['0']['column'];die;
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);

            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        $join_qry .= " LEFT JOIN arai_registration reg ON reg.user_id = r.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_order)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        $Rows        = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));


        $data = array();
        $no   = $_POST['start'];
  
		
			foreach($Rows as $row_val )
			{				
				$i++;
				$row_val['r_id'] = $row_val['r_id'];
				$row_val['id_disp'] = $row_val['id_disp'];				
				$row_val['resouce_name'] = ucfirst($row_val['resouce_name']);						
				$row_val['owner_name'] = ucfirst($row_val['owner_name']);						
				$row_val['xOrder'] = $row_val['xOrder'];	
				$row_val['is_featured'] = $row_val['is_featured'];	
				$row_val['admin_status'] = $row_val['admin_status'];	
				$row_val['is_deleted'] = $row_val['is_deleted'];	
				$row_val['is_block'] = $row_val['is_block'];	
				$row_val['created_on'] = $row_val['created_on'];	
				
				
				if($row_val['admin_status'] == '0'){ $textShowAdminStatus = "Pending";  } else if($row_val['admin_status'] == '1'){ $textShowAdminStatus = "Approved";  } else if($row_val['admin_status'] == '2') { $textShowAdminStatus = "Rejected";  } 
				
				$onclick_like_fun = "show_resource_likes('".base64_encode($row_val['r_id'])."')";
				$getTotalLikes = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $row_val['r_id']), 'like_id');
					$disp_likes = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="'.$onclick_like_fun.'">'.$getTotalLikes.'</a></div>';

				$onclick_report_fun = "show_resource_requests('".base64_encode($row_val['r_id'])."')";
				$getTotalRequests = $this->master_model->getRecordCount('arai_resource_sharing_connect_request', array('r_id' => $row_val['r_id']), 'reported_id');
					$disp_requests = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="'.$onclick_report_fun.'">'.$getTotalRequests.'</a></div>';			
				
		$edit_btn= "<a href='".base_url('xAdmin/resource_sharing/edit/'.base64_encode($row_val['r_id']))."' > <button class='btn btn-success btn-sm'>Edit</button> </a>";				
				
				if($row_val['is_featured'] == '1'){ $textShowFeatured = "Featured";  } else { $textShowFeatured = "Not Featured";  } 
				$featuredUser = "<a href='javascript:void(0)' data-id='".$row_val['r_id']."' class='btn btn-success btn-sm featured-check' id='change-stac-".$row_val['r_id']."'>".$textShowFeatured."</a>";
				
				if($row_val['is_block'] == '0') { $textShowBlockStatus = 'Block'; } else if($row_val['is_block'] == '1') { $textShowBlockStatus = 'Unblock'; }
				$onclick_block_fun = "block_unblock('".base64_encode($row_val['r_id'])."', '".$textShowBlockStatus."', '".$row_val['id_disp']."', '".$row_val['resouce_name']."')";
				$blockUser = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="'.$onclick_block_fun.'" >'.$textShowBlockStatus.'</a>';
												
				$remove = "<a href='javascript:void(0)' class='remove' data-id='".$row_val['r_id']."'><button class='btn btn-success btn-sm'>Remove</button></a>";
				
				$dropdown	= "";
				
				if($row_val['admin_status'] == '0'): $chkStatus="selected"; else: $chkStatus="";   endif;
				if($row_val['admin_status'] == '1'): $chkStatus1="selected"; else: $chkStatus1=""; endif;
				if($row_val['admin_status'] == '2'): $chkStatus2="selected"; else: $chkStatus2=""; endif;
				//if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
				//if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;
					
				$dropdown .= 	"<input type='hidden' name='r_id' id='r_id' value='".$row_val['r_id']."' />
				<select name='status' id='status' class='change-status'>
				<option value='0' ".$chkStatus.">Pending</option>
				<option value='1' ".$chkStatus1.">Approved</option>
				<option value='2' ".$chkStatus2.">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
				$dropdown .=	"</select>";					
				
				
				$xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='".$row_val['xOrder']."' name='xOrder' id='xOrder' value='". $row_val['xOrder']."' onblur='update_sort_order(this.value, ". $row_val['r_id'] .")' onkeyup='update_sort_order(this.value, ". $row_val['r_id'].")' />";				
				
				$showButtons = 	"<span style='white-space:nowrap;'>".$edit_btn."  ".$featuredUser."  ".$blockUser." ".$remove." ".$dropdown."</span>";
				
				/* $conclude = $row_val['webinar_date'];
				$dispTime = "<span style='white-space: nowrap;'>".date("h:i A", strtotime($row_val['webinar_start_time']))." - ".date("h:i A", strtotime($row_val['webinar_end_time']))."<span>"; */
				
				
				$data[] = array( 
				$i,
				$row_val['r_id'],
				$row_val['id_disp'],
				$row_val['resouce_name'],
				// $row_val['owner_name'],
				$row_val['created_on'],
				$textShowAdminStatus,
				$xOrder,
				$disp_likes,
				$disp_requests,
				$showButtons
				);				
			}
			
			$csrf_test_name = $this->security->get_csrf_hash();
			## Response
			$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"aaData" => $data,
			"token" => $csrf_test_name
			);
			
			echo json_encode($response);
		}
		
		public function setOrder(){
			
			$r_id 			= $this->input->post('r_id'); 
			$xOrder 		= $this->input->post('xOrder'); 
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$updateQuery 	= $this->master_model->updateRecord('resource_sharing',array('xOrder'=>$xOrder),array('r_id' => $r_id));
			
			$status = "Order Successfully Updated.";
			$jsonData = array("msg" => $status, "token" => $csrf_test_name);
			echo json_encode($jsonData); 
		}		 
		
		/* public function viewDetails(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$id=$this->input->post('id');
			$this->db->join('arai_registration','arai_webinar_attendence.user_id=arai_registration.user_id','left');	
			$records = $this->master_model->getRecords("webinar_attendence",array("webinar_attendence.w_id" => $id, "webinar_attendence.status" => 'Active'));
			//echo $this->db->last_query();
			
			$table = '';
			if(count($records) > 0){
				$table .= '<table class="table table-bordered">
				<tr>
				<td><b>Fullname</b></td>
				<td><b>Email</b></td>							
				</tr>';
				foreach($records as $details){
					$firstname 	= $encrptopenssl->decrypt($details['first_name']);
					$lastname 	= $encrptopenssl->decrypt($details['last_name']);
					$middlename = $encrptopenssl->decrypt($details['middle_name']);
					$fullname 	= $firstname." ".$middlename." ".$lastname;
					$email 		= $encrptopenssl->decrypt($details['email']);		
					$table .= '<tr>
					<td>'.ucwords($fullname).'</td>
					<td>'.$email.'</td>
					</tr>';
				}		
				$table .= '</table>';
				} else {
				
				$table = "<p class='text-center'><b>No Record Found</b></p>";
			}
			
			echo $table;
			
		}
		 */
		
		public function edit($r_id)
		{	 				
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$r_id = base64_decode($r_id);
			
			// Load Library
			$this->load->library('upload');	
			$user_id = $this->session->userdata('admin_id');		
			
			$response_data = $this->master_model->getRecords("arai_resource_sharing",array('r_id' => $r_id, 'is_deleted' => 0));
			if(count($response_data) == 0) { redirect(site_url('xAdmin/resource_sharing')); }

			$data['r_files_data'] =$r_files_data= $this->master_model->getRecords('arai_resource_sharing_files',array("r_id"=>$r_id, "is_deleted"=>'0'), '', array('file_id'=>'ASC'));
			
			$data['banner_img_error'] = $data['location_error'] = $data['tags_error'] = $error_flag = '';
			$file_upload_flag = 0;
			if(isset($_POST) && count($_POST) > 0)
			{
				//$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');			
				$this->form_validation->set_rules('resouce_name', 'resouce name', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('resource_details', 'resource details', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('resource_specification', 'resource specification', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('owner_name', 'owner name', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('owner_email', 'owner email', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('owner_contact_number', 'owner contact number', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				$this->form_validation->set_rules('location', 'location', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));
				
				
				
				if(!isset($_POST['location'])) 
				{
					$data['location_error'] = 'Please select the location';		
					$error_flag = 1;
				}
				
				if($this->form_validation->run() && $error_flag == '')
				{
					$postArr = $this->input->post();
					
					if($_FILES['banner_img']['name'] != "") //UPLOAD BLOG BANNER IMAGE
					{
						$banner_img = $this->Common_model_sm->upload_single_file("banner_img", array('png','jpg','jpeg','gif'), "banner_img_".date("YmdHis"), "./uploads/resource_banner_img", "png|jpeg|jpg|gif");
						if($banner_img['response'] == 'error')
						{
							$data['banner_img_error'] = $banner_img['message'];
							$file_upload_flag = 1;
						}
						else if($banner_img['response'] == 'success')
						{
							$add_data['banner_img'] = $banner_img['message'];	
							/* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
						}
					}
					
					if($file_upload_flag == 0)
					{
						//$add_data['user_id'] = $user_id;
						$add_data['resouce_name'] = $this->input->post('resouce_name');
						$add_data['resource_details'] = $this->input->post('resource_details');
						$add_data['resource_specification'] = $this->input->post('resource_specification');
						$add_data['owner_name'] = $this->input->post('owner_name');
						$add_data['owner_email'] = $this->input->post('owner_email');
						$add_data['country_code'] = $this->input->post('country_code');
						$add_data['owner_contact_number'] = $this->input->post('owner_contact_number');
						$add_data['location'] = $this->input->post('location');
						$add_data['commercial_terms'] = $this->input->post('commercial_terms');
						$add_data['other_details_check'] = $this->input->post('other_details_check');
						$add_data['terms_condition_check'] = $this->input->post('terms_condition_check');
						$add_data['tags'] = implode(",",$this->input->post('tags'));
						$add_data['web_url'] = $this->input->post('web_url');

						if ($this->input->post('commercial_terms')=='Paid') {
							$add_data['currency'] = $this->input->post('currency');
							$add_data['cost_price'] = $this->input->post('cost_price');
						}else{
							$add_data['currency'] = '';
							$add_data['cost_price'] = '';
						}

						$tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
						if(isset($tag_other) && $tag_other != "")
						{
							$add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
						}

						$other_details = trim($this->security->xss_clean($this->input->post('other_details')));
						if(isset($other_details) && $other_details != "")
						{
							$add_data['other_details'] = trim($this->security->xss_clean($other_details));
						}
						else { $add_data['other_details'] = ''; }

						$terms_condition = trim($this->security->xss_clean($this->input->post('terms_condition')));
						if(isset($terms_condition) && $terms_condition != "")
						{
							$add_data['terms_condition'] = trim($this->security->xss_clean($terms_condition));
						}
						else { $add_data['terms_condition'] = ''; }

						$location_other = trim($this->security->xss_clean($this->input->post('location_other')));
						if(isset($location_other) && $location_other != "")
						{
							$add_data['location_other'] = trim($this->security->xss_clean($location_other));
						}
						else { $add_data['location_other'] = ''; }		
						
						$add_data['updated_on'] = date('Y-m-d H:i:s');
						
						$this->master_model->updateRecord('arai_resource_sharing',$add_data,array('r_id' => $r_id));
						//echo $this->db->last_query(); exit;

						//START : INSERT  FILES 
						$r_files = $_FILES['r_files'];
								if(count($r_files) > 0)
								{

									for($i=0; $i < count($r_files['name']); $i++)
									{
										if($r_files['name'][$i] != '')
										{
											$kr_file = $this->Common_model_sm->upload_single_file('r_files', array('png','jpg','jpeg'), "resource_file_".$r_id."_".date("YmdHis").rand(), "./uploads/resource_files", "png|jpeg|jpg",'1',$i);
											
											if($kr_file['response'] == 'error'){ }
											else if($kr_file['response'] == 'success')
											{
												
												$add_file_data['user_id'] = $user_id;
												$add_file_data['r_id'] = $r_id;
												$add_file_data['file_name'] = ($kr_file['message']);
												$add_file_data['created_on'] = date("Y-m-d H:i:s");
												$this->master_model->insertRecord('arai_resource_sharing_files',$add_file_data,TRUE);
												// $r_files_data[] = $add_file_data; //FOR LOG
											}
										}
									}
								}					
							//END : INSERT  FILES 
						
						// Log Data Added 
						$filesData 			= $_FILES;
						$json_data 			= array_merge($postArr,$filesData);
						$json_encode_data 	= json_encode($json_data);				
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
						'user_id' 			=> $user_id,
						'action_name' 		=> "Update Resource By Admin",
						'module_name'		=> 'Resource Sharing Admin',
						'store_data'		=> $json_encode_data,
						'ip_address'		=> $ipAddr,
						'createdAt'			=> $createdAt
						);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						$this->session->set_flashdata('success','Resource has been successfully updated');
						redirect(site_url('xAdmin/resource_sharing'));
					}
				}
			}
			
			$this->db->order_by('city_name', 'ASC');
			// $this->db->order_by("FIELD(city_name, 'other')", "DESC", false);
			$data['city_data'] = $this->master_model->getRecords("cities");
			
			$this->db->order_by("FIELD(tag_name, 'other')", "DESC", false);
			$data['tag_data'] = $this->master_model->getRecords("resource_sharing_tags_master", array("status" => 'Active'), '', array('tag_name'=>'ASC'));	

			$data['country_codes']  = $this->master_model->getRecords('country','','',array('iso'=>'asc'));

			$data['currency']  = $this->master_model->getRecords('arai_currency','','',array('currency'=>'asc'));
			
			$data['form_data'] = $response_data;
			$data['mode']='Update';
			$data['module_name'] = 'Resource Sharing';
			$data['submodule_name'] = '';
			$data['mode']='Update';
			$data['middle_content'] ='resource_sharing/edit';
			$this->load->view('admin/admin_combo',$data);			
		}

		function delete_kr_file_ajax()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			$file_id = $encrptopenssl->decrypt($this->input->post('file_id', TRUE));
			
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$this->master_model->updateRecord('knowledge_repository_files',array('is_deleted'=>'1'),array('file_id' => $file_id));
			$result['flag'] = 'success'; 
			$result['file_id'] = $file_id;
			
			//START : INSERT LOG 
			$postArr = $this->input->post();
			$json_encode_data 	= json_encode($postArr);										
			$logDetails['user_id'] = $user_id;
			$logDetails['module_name'] = 'KR : Admin delete_team_file_ajax';
			$logDetails['store_data'] = $json_encode_data;
			$logDetails['ip_address'] = $this->get_client_ip();
			$logDetails['createdAt'] = date('Y-m-d H:i:s');
			$logDetails['action_name'] = 'Delete KR File Admin';
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			//END : INSERT LOG
			
			echo json_encode($result);
		}
		
		public function updateStatus()
		{
			// Create Object
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$reject_reason = $this->input->post('reject_reason');
			
			$email_info = $this->get_mail_data($id);
			
			//echo "<pre>";print_r($this->input->post());die();			
			
			$updateQuery = $this->master_model->updateRecord('resource_sharing',array('admin_status'=>$status),array('r_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Resource Status Updated By Admin",
			'module_name'	=> 'resource_sharing',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
						
			if($status == '2') //Rejected
			{				
				$updateQuery1 = $this->master_model->updateRecord('resource_sharing',array('reject_reason'=>$reject_reason),array('r_id' => $id)); 
						
					// SEND MAIL	

					$email_send='';
		        	$slug = $encrptopenssl->encrypt('resource_sharing_mail_to_user_after_admin_rejection');
		        	$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
		        	$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
		        	$sub_content = '';
		        
		        	if(count($subscriber_mail) > 0)
		        	{								
		        		$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
		        		$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
		        		$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
		        		$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
		        		$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
		        		
		        		$arr_words = [
							'[OWNER_NAME]',
							'[RESOURCE_NAME]',
							'[RESOURCE_ID]',
							'[Reason]'
																
						]; 
						$rep_array = [
							$email_info['OWNER_NAME'],
							$email_info['RESOURCE_NAME'],
							$email_info['RESOURCE_ID'],
							$reject_reason

						];

		        		$sub_content = str_replace($arr_words, $rep_array, $desc);        		
		        		
		        		$info_array=array(
		        		'to'		=>	$email_info['OWNER_EMAIL'], 
		        		'cc'		=>	'',
		        		'from'		=>	$fromadmin,
		        		'subject'	=> 	$subject_title,
		        		'view'		=>  'common-file'
		        		);
		        		
		        		$other_infoarray	=	array('content' => $sub_content); 
		        		$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
		        	}
			
			
			
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);				
			} 
			else if($status == '1') //Approved
			{
				
				$email_send='';
		        	$slug = $encrptopenssl->encrypt('resource_sharing_mail_to_user_after_admin_approval');
		        	$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
		        	$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
		        	$sub_content = '';
		        
		        	if(count($subscriber_mail) > 0)
		        	{								
		        		$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
		        		$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
		        		$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
		        		$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
		        		$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
		        		
		        		$arr_words = [
							'[OWNER_NAME]',
							'[RESOURCE_NAME]',
							'[RESOURCE_ID]',
							'[LINK_OF_RESOURCE_POST]',
																
						]; 
						$rep_array = [
							$email_info['OWNER_NAME'],
							$email_info['RESOURCE_NAME'],
							$email_info['RESOURCE_ID'],
							$email_info['hyperlink_res_detail'],

						];

		        		$sub_content = str_replace($arr_words, $rep_array, $desc);        		
		        		
		        		$info_array=array(
		        		'to'		=>	$email_info['OWNER_EMAIL'], 
		        		'cc'		=>	'',
		        		'from'		=>	$fromadmin,
		        		'subject'	=> 	$subject_title,
		        		'view'		=>  'common-file'
		        		);
		        		
		        		$other_infoarray	=	array('content' => $sub_content); 
		        		$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
		        	}
			    
				$updateQuery1 = $this->master_model->updateRecord('resource_sharing',array('reject_reason'=>''),array('r_id' => $id));  
				
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);
			} 
			else 
			{
				$updateQuery1 = $this->master_model->updateRecord('resource_sharing',array('reject_reason'=>''),array('r_id' => $id));  
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData); 
			}
		}
		
		/* public function changeStatus(){
			
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Publish'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change - 2",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);	
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));	
				
				} else if($value == 'Block'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id)); 
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));		
			}
			
		}  
		 */
		
		public function block_unblock()// BLOCK/UNBLOCK BLOG
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$r_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('admin_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					//START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked        
        	/* $email_info = $this->get_mail_data_blog($r_id);
        	
        	$email_send='';
        	$slug = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
        	$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
        	$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
        	$sub_content = '';
        
        	if(count($subscriber_mail) > 0)
        	{								
        		$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
        		$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
        		$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
        		$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
        		$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
        		
        		$arr_words = [
        			'[Blog_Title]',        								
        			'[BLOCKED_REASON]'        								
        		]; 
        		$rep_array = [
        			$email_info['Blog_Title'],
							$popupBlogBlockReason
        		];
        		$sub_content = str_replace($arr_words, $rep_array, $desc);        		
        		
        		$info_array=array(
        		'to'		=>	$email_info['admin_email'], 
        		'cc'		=>	'',
        		'from'		=>	$fromadmin,
        		'subject'	=> 	$subject_title,
        		'view'		=>  'common-file'
        		);
        		
        		$other_infoarray	=	array('content' => $sub_content); 
        		$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
        	}  */       
        	//END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupBlogBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('resource_sharing',$up_data,array('r_id' => $r_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Resource block/unblock By Admin",
				'module_name'		=> 'Resource Admin',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function featured()
		{			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');
			// $value = ucfirst($this->uri->segment(5));
			//echo $id;
			$c_data = $this->master_model->getRecords("resource_sharing", array("r_id" => $id));
			if($c_data[0]['is_featured'] == "0"){ $is_featured = '1';	} else { $is_featured = '0'; }
			
			$updateQuery = $this->master_model->updateRecord('resource_sharing',array('is_featured'=>$is_featured),array('r_id' => $id));
			//echo $this->db->last_query();
			$c_res = $this->master_model->getRecords("resource_sharing", array("r_id" => $id));		
			
			if($c_res[0]['is_featured'] == "0") { $text = 'Featured';	} else { $text = 'Not Featured'; }
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr = $this->get_client_ip();
			$createdAt = date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' => $user_id,
			'action_name' => "Resource Featured Admin",
			'module_name'	=> 'Admin',
			'store_data' => $json_encode_data,
			'ip_address' => $ipAddr,
			'createdAt' => $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("c_status" => $is_featured, "u_featured" => $text,"token" => $csrf_test_name);
			echo json_encode($jsonData);			
		}
		
		public function deleted()
		{			
			$user_id = $this->session->userdata('admin_id');
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');	 
			$updateQuery = $this->master_model->updateRecord('resource_sharing',array('is_deleted'=>1),array('r_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "KR Deleted From Admin",
			'module_name'	=> 'KR',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
		public function get_mail_data($r_id)
		{		
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('title,first_name,middle_name,last_name,email,id_disp,resouce_name,resource_sharing.created_on,registration.user_id');
			$this->db->join('registration','resource_sharing.user_id=registration.user_id','LEFT');
			$r_details= $this->master_model->getRecords('resource_sharing',array('r_id'=>$r_id));
			
			if (count($r_details)) 
			{				
				$user_arr = array();
				if(count($r_details))
				{						
					foreach($r_details as $row_val)
					{								
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$row_val['user_id'] = $row_val['user_id'];
						$user_arr[] = $row_val;
					}					
				}
				$OWNER_NAME = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$OWNER_ID = $user_arr[0]['user_id'];
				$OWNER_EMAIL = $user_arr[0]['email'];
				$RESOURCE_NAME = $r_details[0]['resouce_name'];
				$RESOURCE_ID = $r_details[0]['id_disp'];
				$DATE =date('d/m/Y' ,strtotime( $r_details[0]['created_on'] ));
				$res_detail_link = site_url('resource_sharing/details/').base64_encode($r_id);
				$hyperlink_kr_detail = '<a href='.$res_detail_link.' target="_blank">here</a>';
				
				$email_array=array(
				'OWNER_NAME'=>$OWNER_NAME,
				'OWNER_EMAIL'=>$OWNER_EMAIL,
				'OWNER_ID'=>$OWNER_ID,
				'RESOURCE_NAME'=>$RESOURCE_NAME,
				'RESOURCE_ID'=>$RESOURCE_ID,
				'DATE'=>$DATE,
				'admin_email'=>'vishal.phadol@esds.co.in',
				'hyperlink_res_detail'=>$hyperlink_kr_detail
				);
				
				
			    $this->db->select('user_id,title,first_name,middle_name,last_name,email');
			    $loggedin_user_data = $this->master_model->getRecords("registration",array('user_id'=>$this->session->userdata('user_id')));
			    
			    $loggedin_user_arr = array();
				if(count($loggedin_user_data))
					{	    					
					foreach($loggedin_user_data as $row_val)
						{		    						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$loggedin_user_arr[] = $row_val;
					}    					
				}
			
				$loggedin_user_name = $loggedin_user_arr[0]['title']." ".$loggedin_user_arr[0]['first_name']." ".$loggedin_user_arr[0]['last_name'];
				$email_array['loggedin_user_name']=$loggedin_user_name;
				$email_array['loggedin_user_email']=$loggedin_user_arr[0]['email'];
				$email_array['loggedin_user_id']=$loggedin_user_arr[0]['user_id'];
				return $email_array; 
				// echo "<pre>";
				// print_r($email_array);
			}
		}
	}		