<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin for admin basic function
Author : Vishal P.
*/

class Menu extends CI_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->helper('captcha');
		if ($this->session->userdata('admin_id')!='')  {
			redirect(base_url('xAdmin/admin'));
		}
		  
    }

    public function index($value='')
    {
    	$menu = $this->master_model->getRecords("menu");
    	$data['records'] = $menu; 
    	$data['middle_content']='menu/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
        $menu = $this->master_model->getRecords("menu");
        $data['records'] = $menu; 
        $data['middle_content']='menu/add';
        $this->load->view('admin/admin_combo',$data);
     }


}