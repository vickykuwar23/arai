<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Configuration Setting
Author : Vicky K
*/

class Setconfigure extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }
	 
	 public function index()
    {	//echo "++++++++++++++";die();
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		$setting_data = $this->master_model->getRecords("email_sms");
		
		$res_arr = array();
		if(count($setting_data)){	
						
			foreach($setting_data as $row_val){		
						
				$row_val['protocol'] 	= $encrptopenssl->decrypt($row_val['protocol']);
				$row_val['live_user'] 	= $encrptopenssl->decrypt($row_val['live_user']);
				$row_val['live_pass'] 	= $encrptopenssl->decrypt($row_val['live_pass']);
				$row_val['live_port'] 	= $encrptopenssl->decrypt($row_val['live_port']);
				$row_val['live_host'] 	= $encrptopenssl->decrypt($row_val['live_host']);
				$row_val['test_user'] 	= $encrptopenssl->decrypt($row_val['test_user']);
				$row_val['test_pass'] 	= $encrptopenssl->decrypt($row_val['test_pass']);
				$row_val['test_port'] 	= $encrptopenssl->decrypt($row_val['test_port']);
				$row_val['test_host'] 	= $encrptopenssl->decrypt($row_val['test_host']);
				$row_val['sms_user'] 	= $encrptopenssl->decrypt($row_val['sms_user']);
				$row_val['sms_field_1'] = $encrptopenssl->decrypt($row_val['sms_field_1']);
				$row_val['sms_sign'] 	= $encrptopenssl->decrypt($row_val['sms_sign']);
				$row_val['sms_pin'] 	= $encrptopenssl->decrypt($row_val['sms_pin']);
				$row_val['status'] 		= $row_val['status'];
				$res_arr[] = $row_val;
			}
			
		}		
		
		$data['setting_data'] = $res_arr;	
		
        // Check Validation
		$this->form_validation->set_rules('protocol', 'Live Protocol', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('live_user', 'Live Username', 'required|xss_clean');
		$this->form_validation->set_rules('live_pass', 'Live Password', 'required|xss_clean');
		$this->form_validation->set_rules('live_port', 'Live Port', 'required|xss_clean');
		$this->form_validation->set_rules('live_host', 'Live Host', 'required|xss_clean');
		$this->form_validation->set_rules('test_user', 'Test Username', 'required|xss_clean');
		$this->form_validation->set_rules('test_pass', 'Test Password', 'required|xss_clean');
		$this->form_validation->set_rules('test_port', 'Test Port', 'required|xss_clean');
		$this->form_validation->set_rules('test_host', 'Test Host', 'required|xss_clean');
		//$this->form_validation->set_rules('sms_user', 'SMS user', 'required|xss_clean');
		//$this->form_validation->set_rules('sms_field_1', 'SMS Password', 'required|xss_clean');
		//$this->form_validation->set_rules('sms_sign', 'SMS Signature', 'required|xss_clean');
		//$this->form_validation->set_rules('sms_pin', 'SMS Pin', 'required|xss_clean');
		
				
		if($this->form_validation->run())
		{	
			
			$protocol 		= $encrptopenssl->encrypt($this->input->post('protocol'));
			$live_user 		= $encrptopenssl->encrypt($this->input->post('live_user'));
			$live_pass 		= $encrptopenssl->encrypt($this->input->post('live_pass'));
			$live_port 		= $encrptopenssl->encrypt($this->input->post('live_port'));
			$live_host 		= $encrptopenssl->encrypt($this->input->post('live_host'));
			$test_user 		= $encrptopenssl->encrypt($this->input->post('test_user')); 
			$test_pass 		= $encrptopenssl->encrypt($this->input->post('test_pass'));
			$test_port 		= $encrptopenssl->encrypt($this->input->post('test_port'));
			$test_host 		= $encrptopenssl->encrypt($this->input->post('test_host'));
			$sms_user 		= $encrptopenssl->encrypt($this->input->post('sms_user'));
			$sms_field_1 	= $encrptopenssl->encrypt($this->input->post('sms_field_1'));
			$sms_sign 		= $encrptopenssl->encrypt($this->input->post('sms_sign'));
			$sms_pin 		= $encrptopenssl->encrypt($this->input->post('sms_pin'));
			$status 		= $this->input->post('status');
			if($status == 1){$check = 'Block';}else{$check = 'Active';}
			$createdAt		=   date('Y-m-d H:i:s');
			
			$updateArr = array(	
								'protocol' 		=> $protocol, 
								'live_user' 	=> $live_user, 
								'live_pass' 	=> $live_pass, 
								'live_port' 	=> $live_port,
								'live_host' 	=> $live_host,
								'test_user' 	=> $test_user,
								'test_pass' 	=> $test_pass,
								'test_port' 	=> $test_port,
								'test_host' 	=> $test_host,
								'sms_user' 		=> $sms_user,
								'sms_field_1' 	=> $sms_field_1,
								'sms_sign' 		=> $sms_sign,
								'sms_pin' 		=> $sms_pin,
								'status' 		=> $check,
								'updatedAt' 	=> $createdAt								
							);
								
			$updateQuery = $this->master_model->updateRecord('email_sms', $updateArr,array('id' => '1'));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Set configuration successfully updated');
				redirect(base_url('xAdmin/setconfigure'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/setconfigure'));
			}
			
			
		} // Validation End
		
		$data['module_name'] = 'Configuration';
		$data['submodule_name'] = '';
        $data['middle_content']='setting/configuration';
        $this->load->view('admin/admin_combo',$data);
     }


}