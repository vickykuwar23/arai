<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
		Class : Categories
		Author : Vicky K
	*/
	
	class Participation_log extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');	
			$this->load->library('Opensslencryptdecrypt');
			if($this->session->userdata('admin_id') == ""){			
				redirect(base_url('xAdmin/admin'));
			}	
			$this->check_permissions->is_authorise_admin(21);
		}
		
    public function index()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('pl.id, ac.challenge_id, ac.challenge_title, ar.first_name, ar.middle_name, ar.last_name, pl.created_on');
			$this->db->join('arai_challenge ac','ac.c_id = pl.c_id','',FALSE);
			$this->db->join('arai_registration ar','ar.user_id = pl.user_id','',FALSE);
			$this->db->order_by('pl.created_on','DESC');
    	$response_data = $this->master_model->getRecords("arai_participation_log pl");
			/* echo $this->db->last_query(); exit; */
			$res_arr = array();
			
			if(count($response_data))
			{					
				foreach($response_data as $row_val)
				{
					$row_val['challenge_id'] = $row_val['challenge_id'];						
					$row_val['challenge_title'] = $encrptopenssl->decrypt($row_val['challenge_title']);						
					
					$user_name = $encrptopenssl->decrypt($row_val['first_name']);
					if($row_val['middle_name'] != "") { $user_name .= " ".$encrptopenssl->decrypt($row_val['middle_name']); }
					if($row_val['last_name'] != "") { $user_name .= " ".$encrptopenssl->decrypt($row_val['last_name']); }
					$row_val['user_name'] = $user_name;						
					$row_val['created_on'] = $row_val['created_on'];
					$res_arr[] = $row_val;
				}				
			}
    	$data['records'] = $res_arr; 
			$data['module_name'] = 'Participation Log';
			$data['submodule_name'] = 'Participation Log';	
    	$data['middle_content']='participation_log/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
    public function add()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			// Check Validation
			$this->form_validation->set_rules('cat_name', 'Category Name', 'required|min_length[3]|xss_clean');
			
			if($this->form_validation->run())
			{	
				$category_name = $encrptopenssl->encrypt($this->input->post('cat_name'));
				
				$insertArr = array( 'category_name' => $category_name);			
				$insertQuery = $this->master_model->insertRecord('categories',$insertArr);
				if($insertQuery > 0){
					$this->session->set_flashdata('success','Category successfully created');
					redirect(base_url('xAdmin/categories'));
					} else {
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/categories/add'));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'Cagegories';
			$data['middle_content']='categories/add';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function edit($id)
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$cat_data = $this->master_model->getRecords("categories", array('id' => $id));
			$res_data = array();
			if(count($cat_data) > 0){
				$row_val['category_name'] = $encrptopenssl->decrypt($cat_data[0]['category_name']);
				$res_data[] = $row_val;
			}
			
			$data['cat_data'] = $res_data;
			
			// Check Validation
			$this->form_validation->set_rules('cat_name', 'Category Name', 'required|min_length[3]|xss_clean');
			
			if($this->form_validation->run())
			{	
				$category_name = $encrptopenssl->encrypt($this->input->post('cat_name'));
				$updateAt = date('Y-m-d H:i:s');
				$updateArr = array(  'category_name' => $category_name, 'updatedAt' => $updateAt);			
				$updateQuery = $this->master_model->updateRecord('categories',$updateArr,array('id' => $id));
				if($updateQuery > 0){
					$this->session->set_flashdata('success','Category successfully updated');
					redirect(base_url('xAdmin/categories'));
					} else {
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/categories/edit/'.$id));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'Cagegories';
			$data['middle_content']='categories/edit';
			$this->load->view('admin/admin_combo',$data);
		}
		
		
		public function get_actions(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$encode_id 	= $this->uri->segment(4);
			$decode_id  = base64_decode($encode_id);		
			
			$cat_data = $this->master_model->getRecords("categories", array('id' => $decode_id));
			$res_cat = array();
			if(count($cat_data) > 0){
				$row_val['category_name'] = $encrptopenssl->decrypt($cat_data[0]['category_name']);
				$res_cat[] = $row_val;
			}	
			
			$subcat_details = $this->master_model->getRecords("subcategory",array('category_id'=>$decode_id),'',array('sub_id '=>'ASC'));
			$res_data = array();
			foreach($subcat_details as $subcat){
				$row_val['sub_id'] = $subcat['sub_id'];
				$row_val['subcategory_name'] = $encrptopenssl->decrypt($subcat['subcategory_name']);		
				$res_data[] = $row_val;
			}		
			
			// Selected Values
			$this->db->select('sub_id');	
			$data['added_values'] = $this->master_model->getRecords("user_actions",array('category_id'=>$decode_id));
			
			// Check Validation
			$this->form_validation->set_rules('sub_cat_id[]', 'Checked Actions', 'required|xss_clean');
			if($this->form_validation->run())
			{
				$posts = $this->input->post('sub_cat_id');
				$deleteRows = $this->master_model->deleteRecord('user_actions','category_id',$decode_id);
				
				foreach($posts as $action){
					
					$insertArr = array( 'category_id' =>  $decode_id, 'sub_id' => $action);	
					$insertQuery = $this->master_model->insertRecord('user_actions',$insertArr);
					
				}
				
				// Success Message
				$this->session->set_flashdata('success','Action successfully updated');
				redirect(base_url('xAdmin/categories/get_actions/'.$encode_id));die();
				
			}
			
			//$data['added_values'] = $subids;	
			$data['category_name'] = $res_cat;	
			$data['subcat_details'] = $res_data;		
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'Cagegories';	
    	$data['middle_content']='categories/actions';
			$this->load->view('admin/admin_combo',$data);
			
		}
		
		public function changeStatus(){
			
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Active'){
				
				$updateQuery = $this->master_model->updateRecord('categories',array('status'=>$value),array('id' => $id));
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/categories'));	
				
				} else if($value == 'Block'){
				
				$updateQuery = $this->master_model->updateRecord('categories',array('status'=>$value),array('id' => $id)); 
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/categories'));		
			}
			
		}
		
		public function delete($id){
			
			$id 	= $this->uri->segment(4);		 
			$updateQuery = $this->master_model->updateRecord('categories',array('is_deleted'=>1),array('id' => $id));
			$this->session->set_flashdata('success','Category successfully deleted');
			redirect(base_url('xAdmin/categories'));	
			
		}
		
		
	}	