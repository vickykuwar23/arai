<?php 
	if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Blogs_technology_wall extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->library('excel');
			if($this->session->userdata('admin_id') == "")
			{			
				redirect(base_url('xAdmin/admin'));
			}
			 $this->check_permissions->is_authorise_admin(11);
		}
		
    public function index()
    {
			error_reporting(0);
			
			// load excel library
			$this->load->library('excel');	
			
			if(isset($_POST['export']) && !empty($_POST['export'])) 
			{
				/* print_r($_POST); exit; */
				//error_reporting(0);
				$encrptopenssl =  New Opensslencryptdecrypt();				
				$response = array();
				
				$keyword = @$this->input->post('keyword')?$this->input->post('keyword'):'';
				
				## Search 
				$search_arr = array();
				$searchQuery = "";
				if($keyword != '')
				{
					$string = $encrptopenssl->encrypt($keyword);
					$search_arr[] = " (	b.blog_id_disp like '%".$keyword."%' or b.blog_title like '%".$string."%' or b.author_name like '%".$string."%') ";
					// or arai_challenge.json_str like '%".$keyword."%'
				}
				
				if(count($search_arr) > 0) { $searchQuery = implode(" and ",$search_arr); }		 
				
				## Total number of records without filtering
				/* $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_blog_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_blog_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
				$records = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
				$totalRecords = count($records); */
				
				## Total number of record with filtering		
				/* $totalRecordwithFilter = count($records);
				if($searchQuery != '')
				{
					$this->db->where($searchQuery);
					$this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason');
					$recordsFilter = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
					$totalRecordwithFilter = count($recordsFilter);
				}		
				 */
				 
				## Fetch records		
				if($searchQuery != '') { $this->db->where($searchQuery); }				
				$this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_blog_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_blog_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
				$records = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
								
				$data = array();
				$i = 0;
				
				// Count Check
				if(count($records) > 0)
				{			
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex(0);
					// set Header				
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Blog ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Blog Title');
					$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Blog Description');
					$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Technologies');
					$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Other Technology');
					$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Tags');
					$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Is Technical');
					$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Author Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Author Professional Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Author Organization');
					$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Author Description');	
					$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Author About Info');	
					$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'xOrder');
					$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Featured Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Admin Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Block Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Block Reason');					
					
					// set Row
					$rowCount = 2;					
					foreach($records as $row_val)
					{			
						if($row_val['is_technical'] == '0') { $isTechnical = 'Non technical'; }else { $isTechnical = 'Technical'; } 
						if($row_val['is_featured'] == '0') { $isFeatured = 'Not Featured'; }else { $isFeatured = 'Featured'; } 
						if($row_val['admin_status'] == '0') { $adminStatus = 'Pending'; } else if($row_val['admin_status'] == '1') { $adminStatus = 'Approved'; } else { $adminStatus = 'Rejected'; } 
						if($row_val['is_block'] == '0') { $isBlock = 'Not Block'; }else { $isBlock = 'Block'; } 
						
						$i++;						
						$objPHPExcel->getActiveSheet()->SetCellValue('A'. $rowCount, $row_val['blog_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'. $rowCount, $row_val['blog_id_disp']);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'. $rowCount, $row_val['blog_title']);
						$objPHPExcel->getActiveSheet()->SetCellValue('D'. $rowCount, htmlspecialchars_decode($row_val['blog_description']));
						$objPHPExcel->getActiveSheet()->SetCellValue('E'. $rowCount, $row_val['TechnologyName']);
						$objPHPExcel->getActiveSheet()->SetCellValue('F'. $rowCount, $row_val['technology_other']);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'. $rowCount, $row_val['TagName']);
						$objPHPExcel->getActiveSheet()->SetCellValue('H'. $rowCount, $isTechnical);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'. $rowCount, $row_val['author_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('J'. $rowCount, $row_val['author_professional_status']);	
						$objPHPExcel->getActiveSheet()->SetCellValue('K'. $rowCount, $row_val['author_org_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('L'. $rowCount, $row_val['author_description']);
						$objPHPExcel->getActiveSheet()->SetCellValue('M'. $rowCount, $row_val['author_about']);
						$objPHPExcel->getActiveSheet()->SetCellValue('N'. $rowCount, $row_val['xOrder']);
						$objPHPExcel->getActiveSheet()->SetCellValue('O'. $rowCount, $isFeatured);
						$objPHPExcel->getActiveSheet()->SetCellValue('P'. $rowCount, $adminStatus);
						$objPHPExcel->getActiveSheet()->SetCellValue('Q'. $rowCount, $isBlock);
						$objPHPExcel->getActiveSheet()->SetCellValue('R'. $rowCount, $row_val['block_reason']);						
						$rowCount++;						
					} // Foreach End					
				} // Count Check  
				
				// create file name
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="Blog_'.date("YmdHis").'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit;  
				
			}
    	
    	$data['records'] = ''; 
			$data['module_name'] = 'Blog technology Wall';
			$data['submodule_name'] = '';	
    	$data['middle_content']='blogs_technology_wall/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_blog_data()
		{			
			//error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$table        = 'arai_blog b';

	 		$column_order = array('b.blog_id','b.blog_id','b.blog_id_disp','b.blog_title','b.author_name','b.is_technical','b.created_on','b.admin_status','b.xOrder',
	 			'(SELECT COUNT(like_id) FROM arai_blogs_likes WHERE blog_id = b.blog_id) AS TotalLikes',
				'(SELECT COUNT(reported_id) FROM arai_blogs_reported WHERE blog_id = b.blog_id ) AS TotalRep','b.is_block'

        ); //SET COLUMNS FOR SORT

	 		 $column_search = array(
            'b.blog_id_disp'
          ); 
      //SET COLUMN FOR SEARCH

 		  $order = array('b.blog_id' => 'DESC'); // DEFAULT ORDER

      $WhereForTotal = "WHERE b.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
      $Where         = "WHERE b.is_deleted = 0 	";

       if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
        		// echo $_POST['order']['0']['column'];die;
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);

            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        $join_qry .= " LEFT JOIN arai_registration reg ON reg.user_id = b.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_order)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        $Rows        = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));


        $data = array();
        $no   = $_POST['start'];
			foreach($Rows as $row_val )
			{				
				$i++;
				$row_val['blog_id'] = $row_val['blog_id'];
				$row_val['blog_id_disp'] = $row_val['blog_id_disp'];				
				$row_val['blog_title'] = ucfirst($row_val['blog_title']);						
				$row_val['is_technical'] = $row_val['is_technical'];	
				$row_val['author_name'] = ucfirst($row_val['author_name']);						
				$row_val['xOrder'] = $row_val['xOrder'];	
				$row_val['is_featured'] = $row_val['is_featured'];	
				$row_val['admin_status'] = $row_val['admin_status'];	
				$row_val['is_deleted'] = $row_val['is_deleted'];	
				$row_val['is_block'] = $row_val['is_block'];	
				$row_val['created_on'] = $row_val['created_on'];	
				
				if($row_val['is_technical'] == '1'){ $textShowCategory = "Technical";  } else { $textShowCategory = "Non Technical";  } 
				
				if($row_val['admin_status'] == '0'){ $textShowAdminStatus = "Pending";  } else if($row_val['admin_status'] == '1'){ $textShowAdminStatus = "Approved";  } else if($row_val['admin_status'] == '2') { $textShowAdminStatus = "Rejected";  } 
				
				$onclick_like_fun = "show_blog_likes('".base64_encode($row_val['blog_id'])."')";
				$getTotalLikes = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $row_val['blog_id']), 'like_id');
				$disp_likes = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="'.$onclick_like_fun.'">'.$getTotalLikes.'</a></div>';
				
				$onclick_report_fun = "show_blog_reports('".base64_encode($row_val['blog_id'])."')";
				$getTotalReports = $this->master_model->getRecordCount('arai_blogs_reported', array('blog_id' => $row_val['blog_id']), 'reported_id');
				$disp_reported = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="'.$onclick_report_fun.'">'.$getTotalReports.'</a></div>';				
				
				$modifiedStatus = "<a href='".base_url('xAdmin/blogs_technology_wall/edit/'.base64_encode($row_val['blog_id']))."' > <button class='btn btn-success btn-sm'>Edit</button> </a>";				
				
				if($row_val['is_featured'] == '1'){ $textShowFeatured = "Featured";  } else { $textShowFeatured = "Not Featured";  } 
				$featuredUser = "<a href='javascript:void(0)' data-id='".$row_val['blog_id']."' class='btn btn-success btn-sm featured-check' id='change-stac-".$row_val['blog_id']."'>".$textShowFeatured."</a>";
				
				if($row_val['is_block'] == '0') { $textShowBlockStatus = 'Block'; } else if($row_val['is_block'] == '1') { $textShowBlockStatus = 'Unblock'; }
				$onclick_block_fun = "block_unblock_blog('".base64_encode($row_val['blog_id'])."', '".$textShowBlockStatus."', '".$row_val['blog_id_disp']."', '".$row_val['blog_title']."')";
				$blockUser = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="'.$onclick_block_fun.'" >'.$textShowBlockStatus.'</a>';
												
				$remove = "<a href='javascript:void(0)' class='remove' data-id='".$row_val['blog_id']."'><button class='btn btn-success btn-sm'>Remove</button></a>";
				
				$dropdown	= "";
				
				if($row_val['admin_status'] == '0'): $chkStatus="selected"; else: $chkStatus="";   endif;
				if($row_val['admin_status'] == '1'): $chkStatus1="selected"; else: $chkStatus1=""; endif;
				if($row_val['admin_status'] == '2'): $chkStatus2="selected"; else: $chkStatus2=""; endif;
				//if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
				//if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;
					
				$dropdown .= 	"<input type='hidden' name='blog_id' id='blog_id' value='".$row_val['blog_id']."' />
				<select name='status' id='status' class='change-status'>
				<option value='0' ".$chkStatus.">Pending</option>
				<option value='1' ".$chkStatus1.">Approved</option>
				<option value='2' ".$chkStatus2.">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
				$dropdown .=	"</select>";					
				
				
				$xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='".$row_val['xOrder']."' name='xOrder' id='xOrder' value='". $row_val['xOrder']."' onblur='update_sort_order(this.value, ". $row_val['blog_id'] .")' onkeyup='update_sort_order(this.value, ". $row_val['blog_id'].")' />";				
				
				$showButtons = 	"<span style='white-space:nowrap;'>".$modifiedStatus." ".$featuredUser." ".$blockUser." ".$remove." ".$dropdown."</span>";
				
				/* $conclude = $row_val['webinar_date'];
				$dispTime = "<span style='white-space: nowrap;'>".date("h:i A", strtotime($row_val['webinar_start_time']))." - ".date("h:i A", strtotime($row_val['webinar_end_time']))."<span>"; */
				
				
				$data[] = array( 
				$i,
				$row_val['blog_id'],
				$row_val['blog_id_disp'],
				$row_val['blog_title'],
				$row_val['author_name'],
				$textShowCategory,
				$row_val['created_on'],
				$textShowAdminStatus,
				$xOrder,
				$disp_likes,
				$disp_reported,
				$showButtons
				);				
			}
			
			$csrf_test_name = $this->security->get_csrf_hash();
			## Response
			$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"aaData" => $data,
			"token" => $csrf_test_name
			);
			
			echo json_encode($response);
		}
		
		public function setOrder(){
			
			$blog_id 			= $this->input->post('blog_id'); 
			$xOrder 		= $this->input->post('xOrder'); 
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$updateQuery 	= $this->master_model->updateRecord('arai_blog',array('xOrder'=>$xOrder),array('blog_id' => $blog_id));
			
			$status = "Order Successfully Updated.";
			$jsonData = array("msg" => $status, "token" => $csrf_test_name);
			echo json_encode($jsonData); 
		}		 
		
		/* public function viewDetails(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$id=$this->input->post('id');
			$this->db->join('arai_registration','arai_webinar_attendence.user_id=arai_registration.user_id','left');	
			$records = $this->master_model->getRecords("webinar_attendence",array("webinar_attendence.w_id" => $id, "webinar_attendence.status" => 'Active'));
			//echo $this->db->last_query();
			
			$table = '';
			if(count($records) > 0){
				$table .= '<table class="table table-bordered">
				<tr>
				<td><b>Fullname</b></td>
				<td><b>Email</b></td>							
				</tr>';
				foreach($records as $details){
					$firstname 	= $encrptopenssl->decrypt($details['first_name']);
					$lastname 	= $encrptopenssl->decrypt($details['last_name']);
					$middlename = $encrptopenssl->decrypt($details['middle_name']);
					$fullname 	= $firstname." ".$middlename." ".$lastname;
					$email 		= $encrptopenssl->decrypt($details['email']);		
					$table .= '<tr>
					<td>'.ucwords($fullname).'</td>
					<td>'.$email.'</td>
					</tr>';
				}		
				$table .= '</table>';
				} else {
				
				$table = "<p class='text-center'><b>No Record Found</b></p>";
			}
			
			echo $table;
			
		}
		 */
		
		public function edit($blog_id)
		{	 				
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$blog_id = base64_decode($blog_id);
			
			// Load Library
			$this->load->library('upload');	
			$user_id = $this->session->userdata('admin_id');		
			
			$response_data = $this->master_model->getRecords("arai_blog", array("blog_id" => $blog_id, "is_deleted"=>0));
			if(count($response_data) == 0) { redirect(site_url('xAdmin/blogs_technology_wall')); }
			
			$data['blog_banner_error'] = $data['technology_ids_error'] = $data['tags_error'] = $error_flag = '';
			$file_upload_flag = 0;
			if(isset($_POST) && count($_POST) > 0)
			{
				//$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');			
				$this->form_validation->set_rules('blog_title', 'Blog title', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));			
				$this->form_validation->set_rules('blog_description', 'Blog Description', 'required',array('required' => 'Please enter the %s'));
				
				if(!isset($_POST['technology_ids'])) 
				{
					$data['technology_ids_error'] = 'Please select the Blog Technology';		
					$error_flag = 1;
				}
				
				if(!isset($_POST['tags']))
				{
					$data['tags_error'] = 'Please select the Tags';		
					$error_flag = 1;
				}
				
				if($this->form_validation->run() && $error_flag == '')
				{
					$postArr = $this->input->post();
					
					if($_FILES['blog_banner']['name'] != "") //UPLOAD BLOG BANNER IMAGE
					{
						$blog_banner = $this->Common_model_sm->upload_single_file("blog_banner", array('png','jpg','jpeg','gif'), "blog_banner_".date("YmdHis"), "./uploads/blog_banner", "png|jpeg|jpg|gif");
						if($blog_banner['response'] == 'error')
						{
							$data['blog_banner_error'] = $blog_banner['message'];
							$file_upload_flag = 1;
						}
						else if($blog_banner['response'] == 'success')
						{
							$add_data['blog_banner'] = $blog_banner['message'];								
						}
					}
					
					if($file_upload_flag == 0)
					{
						//$add_data['user_id'] = $user_id;
						$add_data['blog_title'] = $this->input->post('blog_title');
						$add_data['blog_description'] = $this->input->post('blog_description');
						$add_data['technology_ids'] = implode(",",$this->input->post('technology_ids'));
						
						$technology_ids_other = trim($this->security->xss_clean($this->input->post('technology_ids_other')));
						if(isset($technology_ids_other) && $technology_ids_other != "")
						{
							$add_data['technology_other'] = trim($this->security->xss_clean($technology_ids_other));
						}
						else { $add_data['technology_other'] = ''; }
						
						$add_data['tags'] = implode(",",$this->input->post('tags'));
						$tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
            if (isset($tag_other) && $tag_other != "") {
                $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
            } else { $add_data['tag_other'] = '';}
						$add_data['is_technical'] = $this->input->post('is_technical');
						$add_data['author_name'] = trim($this->security->xss_clean($this->input->post('author_name')));
						
						$author_professional_status_chk = $this->input->post('author_professional_status_chk');
						if(isset($author_professional_status_chk) && $author_professional_status_chk == 'on')
						{
							$add_data['author_professional_status'] = trim($this->security->xss_clean($this->input->post('author_professional_status')));
						} else { $add_data['author_professional_status'] = ''; }
						
						$author_org_name_chk = $this->input->post('author_org_name_chk');
						if(isset($author_org_name_chk) && $author_org_name_chk == 'on')
						{
							$add_data['author_org_name'] = trim($this->security->xss_clean($this->input->post('author_org_name')));
						} else { $add_data['author_org_name'] = ''; }
						
						$author_description_chk = $this->input->post('author_description_chk');
						if(isset($author_description_chk) && $author_description_chk == 'on')
						{
							$add_data['author_description'] = trim($this->security->xss_clean($this->input->post('author_description')));
						} else { $add_data['author_description'] = ''; }
						
						$author_about_chk = $this->input->post('author_about_chk');
						if(isset($author_about_chk) && $author_about_chk == 'on')
						{
							$add_data['author_about'] = trim($this->security->xss_clean($this->input->post('author_about')));
						} else { $add_data['author_about'] = ''; }						
						
						$add_data['updated_on'] = date('Y-m-d H:i:s');
						
						$this->master_model->updateRecord('arai_blog',$add_data,array('blog_id' => $blog_id));
						//echo $this->db->last_query(); exit;
						
						// Log Data Added 
						$filesData 			= $_FILES;
						$json_data 			= array_merge($postArr,$filesData);
						$json_encode_data 	= json_encode($json_data);				
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
						'user_id' 			=> $user_id,
						'action_name' 		=> "Update Blog Technology Wall By Admin",
						'module_name'		=> 'Blog Technology Wall Admin',
						'store_data'		=> $json_encode_data,
						'ip_address'		=> $ipAddr,
						'createdAt'			=> $createdAt
						);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						$this->session->set_flashdata('success','Blog has been successfully updated');
						redirect(site_url('xAdmin/blogs_technology_wall'));
					}
				}
			}
			
			
			$this->db->order_by('technology_name', 'ASC');
			$this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
			$data['technology_data'] = $this->master_model->getRecords("arai_blog_technology_master", array("status" => 'Active'));
			
			//$this->db->order_by("FIELD(tag_name, 'other')", "DESC", false);
			$data['tag_data'] = $this->master_model->getRecords("arai_blog_tags_master", array("status" => 'Active'), '', array('tag_name'=>'ASC'));
			
			$data['form_data'] = $response_data;
			$data['module_name'] = 'Blog technology Wall';
			$data['submodule_name'] = '';
			$data['middle_content'] ='blogs_technology_wall/edit';
			$this->load->view('admin/admin_combo',$data);			
		}
		
		public function updateStatus()
		{
			// Create Object
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$reject_reason = $this->input->post('reject_reason');
			
			$email_info = $this->get_mail_data_blog($id);
			
			//echo "<pre>";print_r($this->input->post());die();			
			
			$updateQuery = $this->master_model->updateRecord('arai_blog',array('admin_status'=>$status),array('blog_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Blog Status Updated By Admin",
			'module_name'	=> 'Blog',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
						
			if($status == '2') //Rejected
			{				
				$updateQuery1 = $this->master_model->updateRecord('arai_blog',array('reject_reason'=>$reject_reason),array('blog_id' => $id)); 
				
				//START : SEND MAIL TO USER WHEN BLOG IS REJECTED - blog_mail_to_user_on_blog_rejected
				$email_send='';
				$slug = $encrptopenssl->encrypt('blog_mail_to_user_on_blog_rejected');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
			
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
						'[USER_NAME]',
						'[Blog_Title]',
						'[Reason]'											
					]; 
					$rep_array = [
						$email_info['USER_NAME'],
						$email_info['Blog_Title'],
						$reject_reason								
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);					
					
					$info_array=array(
					'to'		=>	$email_info['USER_EMAIL'],
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				}					
				//END : SEND MAIL TO USER WHEN BLOG IS REJECTED - blog_mail_to_user_on_blog_rejected
			
				// SEND MAIL TO CHALLENGE OWNER
				/* $slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_rejected'); 
				
				$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				
				$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
				
				if(count($webinar_mail) > 0)
				{					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$webinar_reason = $webinar_data[0]['webinar_reason'];
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $webinar_reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				} */
			
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);				
			} 
			else if($status == '1') //Approved
			{
				//START : SEND MAIL TO USER WHEN BLOG IS APPROVED - blog_mail_to_user_on_blog_approved
				$email_send='';
				$slug = $encrptopenssl->encrypt('blog_mail_to_user_on_blog_approved');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
            
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
						'[USER_NAME]',
						'[Blog_Title]',
						'[LINK_OF_POST]'											
					]; 
					$rep_array = [
						$email_info['USER_NAME'],
						$email_info['Blog_Title'],
						$email_info['LINK_OF_POST']								
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);					
					
					$info_array=array(
					'to'		=>	$email_info['USER_EMAIL'],
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				}
				//END : SEND MAIL TO USER WHEN BLOG IS APPROVED	- blog_mail_to_user_on_blog_approved
			    
				$updateQuery1 = $this->master_model->updateRecord('arai_blog',array('reject_reason'=>''),array('blog_id' => $id));  
				
				// SEND MAIL TO CHALLENGE OWNER
				/* $slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_accepted'); 
				
				$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				
				$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
				
				if(count($webinar_mail) > 0)
				{					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$reason = '';
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					//return true;
				} */
				
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);
			} 
			else 
			{
				$updateQuery1 = $this->master_model->updateRecord('arai_blog',array('reject_reason'=>''),array('blog_id' => $id));  
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData); 
			}
		}
		
		/* public function changeStatus(){
			
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Publish'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change - 2",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);	
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));	
				
				} else if($value == 'Block'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id)); 
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));		
			}
			
		}  
		 */
		
		public function block_unblock_blog()// BLOCK/UNBLOCK BLOG
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$blog_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('admin_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					//START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked        
        	/* $email_info = $this->get_mail_data_blog($blog_id);
        	
        	$email_send='';
        	$slug = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
        	$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
        	$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
        	$sub_content = '';
        
        	if(count($subscriber_mail) > 0)
        	{								
        		$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
        		$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
        		$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
        		$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
        		$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
        		
        		$arr_words = [
        			'[Blog_Title]',        								
        			'[BLOCKED_REASON]'        								
        		]; 
        		$rep_array = [
        			$email_info['Blog_Title'],
							$popupBlogBlockReason
        		];
        		$sub_content = str_replace($arr_words, $rep_array, $desc);        		
        		
        		$info_array=array(
        		'to'		=>	$email_info['admin_email'], 
        		'cc'		=>	'',
        		'from'		=>	$fromadmin,
        		'subject'	=> 	$subject_title,
        		'view'		=>  'common-file'
        		);
        		
        		$other_infoarray	=	array('content' => $sub_content); 
        		$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
        	}  */       
        	//END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupBlogBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_blog',$up_data,array('blog_id' => $blog_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Block Blog Technology Wall By Admin",
				'module_name'		=> 'Blog Technology Wall Admin',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function featured()
		{			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');
			// $value = ucfirst($this->uri->segment(5));
			//echo $id;
			$c_data = $this->master_model->getRecords("arai_blog", array("blog_id" => $id));
			if($c_data[0]['is_featured'] == "0"){ $is_featured = '1';	} else { $is_featured = '0'; }
			
			$updateQuery = $this->master_model->updateRecord('arai_blog',array('is_featured'=>$is_featured),array('blog_id' => $id));
			//echo $this->db->last_query();
			$c_res = $this->master_model->getRecords("arai_blog", array("blog_id" => $id));		
			
			if($c_res[0]['is_featured'] == "0") { $text = 'Featured';	} else { $text = 'Not Featured'; }
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr = $this->get_client_ip();
			$createdAt = date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' => $user_id,
			'action_name' => "Blog Featured Status - 3",
			'module_name'	=> 'Blog',
			'store_data' => $json_encode_data,
			'ip_address' => $ipAddr,
			'createdAt' => $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("c_status" => $is_featured, "u_featured" => $text,"token" => $csrf_test_name);
			echo json_encode($jsonData);			
		}
		
		public function deleted()
		{			
			$user_id = $this->session->userdata('admin_id');
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');	 
			$updateQuery = $this->master_model->updateRecord('arai_blog',array('is_deleted'=>1),array('blog_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Blog Deleted From Admin",
			'module_name'	=> 'Blog',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
		public function get_mail_data_blog($blog_id){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('title,first_name,middle_name,last_name,email,blog_id_disp,blog_title,blog.created_on,registration.user_id');
			$this->db->join('registration','blog.user_id=registration.user_id','LEFT');
			$blog_details= $this->master_model->getRecords('blog ',array('blog_id'=>$blog_id));
			
			if (count($blog_details)) {
				
				$user_arr = array();
				if(count($blog_details)){	
					
					foreach($blog_details as $row_val){		
						
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$row_val['user_id'] = $row_val['user_id'];
						$user_arr[] = $row_val;
					}
					
				}
				$user_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$user_id = $user_arr[0]['user_id'];
				$user_email = $user_arr[0]['email'];
				$blog_title = $blog_details[0]['blog_title'];
				$blog_id_disp = $blog_details[0]['blog_id_disp'];
				$blog_date =date('d/m/Y' ,strtotime( $blog_details[0]['created_on'] ));
				$blog_detail_link = site_url('blogs_technology_wall/blogDetails/').base64_encode($blog_id);
				$hyperlink_blog_detail = '<a href='.$blog_detail_link.' target="_blank">here</a>';
				
				$email_array=array(
				'USER_NAME'=>$user_name,
				'USER_EMAIL'=>$user_email,
				'USER_ID'=>$user_id,
				'Blog_Title'=>$blog_title,
				'blog_id_disp'=>$blog_id_disp,
				'Blog_Date'=>$blog_date,
				'admin_email'=>'vishal.phadol@esds.co.in',
				'LINK_OF_POST'=>$hyperlink_blog_detail
				);
				
				return $email_array ;
			}
		}
	}		