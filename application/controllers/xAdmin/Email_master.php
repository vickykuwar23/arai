<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class  : Email Template
Author : Vicky K
*/

class Email_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(22);
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("email_template");
		//echo "<pre>";print_r($response_data);die();
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['email_title'] = $encrptopenssl->decrypt($row_val['email_title']);
				$row_val['from_email'] 	= $encrptopenssl->decrypt($row_val['from_email']);
				$row_val['slug'] 	= $encrptopenssl->decrypt($row_val['slug']);
				$row_val['email_description'] = $encrptopenssl->decrypt($row_val['email_description']);	 
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);die();
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'email_master';
		$data['submodule_name'] = 'Email';	
    	$data['middle_content']='email-template/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('email_title', 'Email Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('from_emai', 'Email ID', 'required|xss_clean');
		//$this->form_validation->set_rules('cc_email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('email_description', 'Email Description', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$email_title = $encrptopenssl->encrypt($this->input->post('email_title'));
			$cc_email_id = $encrptopenssl->encrypt($this->input->post('cc_email_id'));
			$from_emai = $encrptopenssl->encrypt($this->input->post('from_emai'));
			$slug = $encrptopenssl->encrypt($this->input->post('slug'));
			$email_description  = $encrptopenssl->encrypt($this->input->post('email_description'));
			
			$insertArr = array( 'email_title' => $email_title, 'from_email' => $from_emai, 'cc_email_id' => $cc_email_id,  'email_description' => $email_description,  'slug' => $slug);			
			$insertQuery = $this->master_model->insertRecord('email_template',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Email content successfully created');
				redirect(base_url('xAdmin/email_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/email_master/add'));
			}
		}
		$data['module_name'] = 'email_master';
		$data['submodule_name'] = 'Email';
        $data['middle_content']='email-template/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$email_data = $this->master_model->getRecords("email_template", array('id' => $id));
		
		$res_arr = array();
		if(count($email_data)){
			
			$row_val['email_title'] 		= $encrptopenssl->decrypt($email_data[0]['email_title']);
			$row_val['cc_email_id'] 		= $encrptopenssl->decrypt($email_data[0]['cc_email_id']);
			$row_val['from_email'] 			= $encrptopenssl->decrypt($email_data[0]['from_email']);
			$row_val['email_description'] 	= $encrptopenssl->decrypt($email_data[0]['email_description']);
			$row_val['slug'] 				= $encrptopenssl->decrypt($email_data[0]['slug']);
			
			$res_arr[] = $row_val;
		}
		
		
        // Check Validation
		$this->form_validation->set_rules('email_title', 'Email Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('email_description', 'Email Description', 'required|xss_clean');
		//$this->form_validation->set_rules('cc_email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('from_emai', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean');
		if($this->form_validation->run())
		{	
			$email_title 		= $encrptopenssl->encrypt($this->input->post('email_title'));
			$email_description  = $encrptopenssl->encrypt($this->input->post('email_description'));
			$cc_email_id 		= $encrptopenssl->encrypt($this->input->post('cc_email_id'));
			$from_emai 			= $encrptopenssl->encrypt($this->input->post('from_emai'));
			$slug 				= $encrptopenssl->encrypt($this->input->post('slug'));
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'email_title' => $email_title, 'cc_email_id' => $cc_email_id, 'from_email' => $from_emai, 'email_description' => $email_description,  'slug' => $slug, 'updatedAt' => $updateAt);	
			
			$updateQuery = $this->master_model->updateRecord('email_template',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Email content successfully updated');
				redirect(base_url('xAdmin/email_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/email_master/edit/'.$id));
			}
		}
		
		$data['email_data'] = $res_arr;
		$data['module_name'] = 'email_master';
		$data['submodule_name'] = 'Email';
        $data['middle_content']='email-template/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('email_template',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/email_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('email_template',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/email_master'));		
		 }
		 
	 }
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('cms',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','CMS page successfully deleted');
		 redirect(base_url('xAdmin/cms'));	
		 
	 }*/


}