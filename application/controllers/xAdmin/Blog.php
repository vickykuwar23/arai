<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : News & Blog
Author : Vicky K
*/

class Blog extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		$this->load->helper('text');	
		$this->load->helper('auth');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		$this->check_permissions->is_authorise_admin(7);
    }

    public function index()
    {
		//isLogin();
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("news_blog");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['blog_name'] = $encrptopenssl->decrypt($row_val['blog_name']);		
				$row_val['blog_desc'] 	= $encrptopenssl->decrypt($row_val['blog_desc']);
				$row_val['blog_url'] = $encrptopenssl->decrypt($row_val['blog_url']);
				$row_val['blog_img'] 	= $encrptopenssl->decrypt($row_val['blog_img']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Blog';
		$data['submodule_name'] = '';	
    	$data['middle_content']='blog/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation
		$this->form_validation->set_rules('blog_name', 'News Title', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('blog_desc', 'News Description', 'required|xss_clean');
		$this->form_validation->set_rules('blog_url', 'News URL', 'required|xss_clean');
		
		
		if($this->form_validation->run())
		{	
			$blog_name = $encrptopenssl->encrypt($this->input->post('blog_name'));
			$blog_desc = $encrptopenssl->encrypt($this->input->post('blog_desc'));
			$blog_url  = $encrptopenssl->encrypt($this->input->post('blog_url'));
			$blog_img  = $_FILES['blog_img']['name'];
			
			if($_FILES['blog_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/blog';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('blog_img', $_FILES, $config, FALSE);
				//print_r($upload_files);die();
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}

				/*$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png, .jfif format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/blog'));	
				}*/
						
				$filesize = $_FILES['blog_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Banner Image End
			
			$file_names = $encrptopenssl->encrypt($b_image[0]);		
			
			$insertArr = array( 'blog_name' => $blog_name, 'blog_img' => $file_names, 'blog_desc' => $blog_desc,'blog_url' => $blog_url,'createdAt' => date('Y-m-d H:i:s') );
			//echo "<pre>";print_r($insertArr);	
			$insertQuery = $this->master_model->insertRecord('news_blog',$insertArr);
			//echo $this->db->last_query();die();
			if($insertQuery > 0){
				$this->session->set_flashdata('success','News successfully uploaded.');
				redirect(base_url('xAdmin/blog'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/blog/add'));
			}
		}
		$data['module_name'] = 'Blog';
		$data['submodule_name'] = '';	
        $data['middle_content']='blog/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		$blog_data = $this->master_model->getRecords("news_blog", array('id' => $id));
		
		$res_arr = array();
		if(count($blog_data) > 0){ 
			$row_val['blog_name'] = $encrptopenssl->decrypt($blog_data[0]['blog_name']);
			//$row_val['blog_img']  = $encrptopenssl->decrypt($blog_data[0]['blog_img']);
			$row_val['blog_desc'] = $encrptopenssl->decrypt($blog_data[0]['blog_desc']);
			$row_val['blog_url']  = $encrptopenssl->decrypt($blog_data[0]['blog_url']);
			$res_arr[] = $row_val;
		}
		
		$data['blog_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('blog_name', 'News Title', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('blog_desc', 'News Description', 'required|xss_clean');
		$this->form_validation->set_rules('blog_url', 'News URL', 'required|xss_clean');		
		
		if($this->form_validation->run())
		{	
			$blog_name = $encrptopenssl->encrypt($this->input->post('blog_name'));
			$blog_desc = $encrptopenssl->encrypt($this->input->post('blog_desc'));
			$blog_url  = $encrptopenssl->encrypt($this->input->post('blog_url'));
			$blog_img  = $_FILES['blog_img']['name'];
			
			$postImg   = "";
			if($_FILES['blog_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/blog';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('blog_img', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}

				/*$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/blog'));	
				}*/
				
				$postImg = $b_image[0];	
				$filesize = $_FILES['blog_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Banner Image End
			
			
			if($postImg == ""){
				
				$file_names = $blog_data[0]['blog_img'];
				
			} else {
				
				$file_names = $encrptopenssl->encrypt($b_image[0]);
			} 
									
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'blog_name' => $blog_name, 'blog_img' => $file_names, 'blog_desc' => $blog_desc,'blog_url' => $blog_url, 'updatedAt' => $updateAt);			
			//echo "<pre>";print_r($updateArr);die();
			$updateQuery = $this->master_model->updateRecord('news_blog',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','News successfully updated');
				redirect(base_url('xAdmin/blog'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/blog/edit/'.$id));
			}
		}
		$data['module_name'] = 'Blog';
		$data['submodule_name'] = '';
        $data['middle_content']='blog/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function details($id){
		
		$blog_id = base64_decode($id);
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("news_blog", array('id' => $blog_id));
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['blog_name'] = $encrptopenssl->decrypt($row_val['blog_name']);		
				$row_val['blog_desc'] 	= $encrptopenssl->decrypt($row_val['blog_desc']);
				$row_val['blog_url'] = $encrptopenssl->decrypt($row_val['blog_url']);
				$row_val['blog_img'] 	= $encrptopenssl->decrypt($row_val['blog_img']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['blog_data'] = $res_arr; 
		
		//echo "<pre>";print_r($data['blog_data']);die();	
		$data['module_name'] = 'Blog';
		$data['submodule_name'] = '';
        $data['middle_content']='blog/details';
        $this->load->view('admin/admin_combo',$data);
	 }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('news_blog',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/blog'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('news_blog',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/blog'));		
		 }
		 
	 }
	 
	 public function markfeatured(){
		 
		 $csrf_test_name = $this->security->get_csrf_hash();
		 
		 $id 		= $this->input->post('id');
		 $chkStatus	= $this->input->post('status');
		 $updatedAt		=   date('Y-m-d H:i:s');
		 
		$blog_data = $this->master_model->getRecords("news_blog", array("id" => $id));
		
		if($blog_data[0]['is_featured'] == "N"){
			$is_featured = 'Y';
			$text = "Featured";
		} else {
			$is_featured = 'N';
			$text = "Not-Featured";
		}
		 $updateQuery = $this->master_model->updateRecord('news_blog',array('is_featured'=>$chkStatus),array('id' => $id)); 
			//echo $this->db->last_query();die();	

		$blog_get = $this->master_model->getRecords("news_blog", array("id" => $id));
		$val_get = $blog_get[0]['is_featured'];
			
		 $jsonData = array("changeVal" => $val_get, "u_featured" => $text,"token" => $csrf_test_name);
		 
		 echo json_encode($jsonData);
		 		
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('news_blog',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','News successfully deleted');
		 redirect(base_url('xAdmin/blog'));	
		 
	 }


}