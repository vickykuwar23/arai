<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Class : UserS Listing
Author : Vicky K
 */

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(2);
    }

    public function index()
    {
    	ini_set('max_execution_time', '0'); 
    	ini_set('memory_limit', '-1');
    	// error_reporting(-1);
			// ini_set('display_errors', 'On');
        // load excel library
        $this->load->library('excel');
        $this->load->library('Opensslencryptdecrypt');

        if (isset($_POST['export']) && !empty($_POST['export'])) {

            //error_reporting(E_ALL);
            $encrptopenssl = new Opensslencryptdecrypt();

            $response = array();

            $keyword      = @$this->input->post('keyword') ? $this->input->post('keyword') : '';
            $profile_type = @$this->input->post('profile_type') ? $this->input->post('profile_type') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	arai_registration.first_name LIKE '%" . $string . "%' OR
									arai_registration.middle_name LIKE '%" . $string . "%' OR
									arai_registration.last_name LIKE '%" . $string . "%' OR
									arai_registration.email LIKE '%" . $string . "%' OR
									arai_registration.mobile LIKE'%" . $string . "%' OR arai_registration.json_str LIKE '%" . $keyword . "%') ";

            }

            if ($profile_type != '') {
                $search_arr[] = " arai_registration.user_category_id='" . $profile_type . "' ";
            }

            if (count($search_arr) > 0) {
                $searchQuery = implode(" AND ", $search_arr);
            }

            ## Total number of records without filtering
            $records = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));

            $totalRecords = count($records);

            ## Total number of record with filtering
            $totalRecordwithFilter = count($records);
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
                $recordsFilter         = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));
                $totalRecordwithFilter = count($recordsFilter);
            }

            ## Fetch records
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
            }
            //$this->db->limit(3000,6001);
            $this->db->select('registration.user_id,registration.user_category_id,registration.priority,registration.payment_status,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,
				registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
				registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
				country.phonecode,registration_usercategory.user_category,registration_usersubcategory.sub_catname');
            $this->db->join('country', 'registration.country_code=country.id', 'left');
            $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
            $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
            $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
            $records = $this->master_model->getRecords("registration", array('registration.is_valid' => '1', 'registration.is_deleted' => '0'), '', array('registration.user_id' => 'DESC'));
            //echo $this->db->last_query(); exit;

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);

                // set Header
                if ($profile_type == 1) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Admin Approval');

                } else if ($profile_type == 2) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'LinkedIn Page');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Admin Approval');

                } else {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'LinkedIn Page');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AR1', 'Admin Approval');

                }

                // set Row
                $rowCount = 2;
                $res_arr  = array();
                foreach ($records as $row_val) {

                    $i++;
                    $row_val['user_id']               = $row_val['user_id'];
                    $row_val['user_category_id']      = $row_val['user_category_id'];
                    $row_val['membership_type']       = $encrptopenssl->decrypt($row_val['membership_type']);
                    $row_val['title']                 = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']            = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name']           = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']             = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']                 = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['phonecode']             = $row_val['phonecode'];
                    $row_val['mobile']                = $encrptopenssl->decrypt($row_val['mobile']);
                    $row_val['xOrder']                = $row_val['xOrder'];
                    $row_val['user_category']         = $encrptopenssl->decrypt($row_val['user_category']);
                    $row_val['sub_catname']           = $encrptopenssl->decrypt($row_val['sub_catname']);
                    $row_val['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);
                    $row_val['priority']              = $row_val['priority'];

                    $fullname1   = ucfirst($row_val['title']) . " " . ucfirst($row_val['first_name']) . " " . ucfirst($row_val['middle_name']) . " " . ucfirst($row_val['last_name']);
                    $countryCode = $row_val['phonecode'] . "-" . $row_val['mobile'];

                    $userID    = $row_val['user_id'];
                    $full_name = $fullname1;

                    if ($row_val['user_category_id'] == 1) {

                        $uid = $row_val['user_id'];
                        $this->db->select('type,status,employement_status,other_emp_status,other_domian,other_domian,other_skill_sets,university_course
										  university_since_year,university_to_year,university_name,company_name,university_location,designation,
										  designation_description,event_description_of_work,event_experience_in_year,technical_experience_in_year,
										  technical_experience_to_year,pincode,flat_house_building_apt_company,area_colony_street_village,
										  town_city_and_state,country,additional_detail,specify_fields_area_that_you_would_like,years_of_experience,
										  bio_data,no_of_paper_publication,no_of_patents,portal_name,portal_link,portal_description,portal_name_multiple,
										  portal_link_multiple,portal_description_multiple,domain_area_of_expertise_search,skill_sets_search');
                        $studentProfile = $this->master_model->getRecords("student_profile", array('user_id' => $uid));

                        $row_val['student_profile'] = $studentProfile;

                    } else if ($row_val['user_category_id'] == 2) {

                        if ($row_val['user_category_id'] == 2) {

                            $uid = $row_val['user_id'];
                            $this->db->select('org_sector, org_sector_other, overview, spoc_bill_addr1, spoc_bill_addr2, spoc_bill_addr_city,
											  spoc_bill_addr_state, spoc_bill_addr_country, spoc_bill_addr_pin, spoc_sec_num_country_code1,
											  spoc_sec_num1, spoc_sec_num_country_code2, spoc_sec_num2, self_declaration, specialities_products,
											  establishment_year, institution_size, company_evaluation, website, linkedin_page');
                            $orgProfile = $this->master_model->getRecords("profile_organization", array('user_id' => $uid));

                            $row_val['org_profile'] = $orgProfile;

                        } // Organization Result

                    }

                    //$rowCount++;
                    $res_arr[] = $row_val;

                } // Foreach End
                //echo ">>>>".$profile_type;
                //echo "<pre>";print_r($res_arr);die();
                foreach ($res_arr as $res_input) {

                    $i++;
                    $user_id               = $res_input['user_id'];
                    $user_category_id      = $res_input['user_category_id'];
                    $membership_type       = $res_input['membership_type'];
                    $title                 = $res_input['title'];
                    $first_name            = $res_input['first_name'];
                    $middle_name           = $res_input['middle_name'];
                    $last_name             = $res_input['last_name'];
                    $email                 = $res_input['email'];
                    $payment_status        = $res_input['payment_status'];
                    $phonecode             = $res_input['phonecode'];
                    $mobile                = $res_input['mobile'];
                    $user_category         = $res_input['user_category'];
                    $sub_catname           = $res_input['sub_catname'];
                    $institution_full_name = $res_input['institution_full_name'];
                    $fullname1             = ucfirst($res_input['title']) . " " . ucfirst($res_input['first_name']) . " " . ucfirst($res_input['middle_name']) . " " . ucfirst($res_input['last_name']);
                    $countryCode           = $phonecode . "-" . $mobile;
                    $full_name             = $fullname1;
                    $pay_status            = ucfirst($payment_status);
                    $admin_approval        = $res_input['admin_approval'];
                    $userStatus            = $res_input['status'];

                    if ($row_val['priority'] == 1) {
                        $priorityStatus = 'High';
                    }

                    if ($row_val['priority'] == 2) {
                        $priorityStatus = 'Medium';
                    }

                    if ($row_val['priority'] == 3) {
                        $priorityStatus = 'Low';
                    }

                    if ($profile_type) {

                        if ($user_category_id == 1) {

                            $employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                            $other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                            $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                            $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                            $designation                  = json_decode($res_input['student_profile'][0]['designation']);
                            $designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                            $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                            $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                            $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                            $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                            $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                            $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                            $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                            $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                            $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                            $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                            $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                            $technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $admin_approval);

                        } else if ($user_category_id == 2) {

                            $org_sector                 = $res_input['org_profile'][0]['org_sector'];
                            $org_sector_other           = $encrptopenssl->decrypt($res_input['org_profile'][0]['org_sector_other']);
                            $overview                   = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                            $spoc_bill_addr1            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                            $spoc_bill_addr2            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                            $spoc_bill_addr_city        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                            $spoc_bill_addr_state       = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                            $spoc_bill_addr_country     = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                            $spoc_bill_addr_pin         = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                            $spoc_sec_num_country_code1 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                            $spoc_sec_num1              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                            $spoc_sec_num_country_code2 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                            $spoc_sec_num2              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                            $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                            $specialities_products      = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                            $establishment_year         = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                            $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                            $company_evaluation         = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                            $website                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                            $linkedin_page              = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);

                            $explodeSec = explode(",", $org_sector);

                            $addArr = array();
                            foreach ($explodeSec as $orgn) {

                                $orgCapture = $this->master_model->getRecords("organization_sector", array('id' => $orgn));
                                $getOrgnm   = $orgCapture[0]['name'];
                                array_push($addArr, $getOrgnm);

                            }

                            $impOrg = implode(", ", $addArr);

                            $removeOtherOrg = str_replace("other", "", $impOrg);

                            $rOtherOrg = rtrim($removeOtherOrg, ',');

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $rOtherOrg);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $admin_approval);
                        }

                    } else {

                        if ($user_category_id == 1) {
                        		$employmentStatus='';
                        		if (isset($res_input['student_profile'][0]['employement_status']) && $res_input['student_profile'][0]['employement_status']!='') {
                            	$employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                        		}

                        		$other_emp_status='';
                        		if (isset($res_input['student_profile'][0]['other_emp_status']) && $res_input['student_profile'][0]['other_emp_status']!='') {
                            	$other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                        		}
                            
                            $other_domian='';
                        		if (isset($res_input['student_profile'][0]['other_domian']) && $res_input['student_profile'][0]['other_domian']!='') {
                            	 $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                        		}

                        		$other_skill_sets='';
                        		if (isset($res_input['student_profile'][0]['other_skill_sets']) && $res_input['student_profile'][0]['other_skill_sets']!='') {
                            	 $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                        		}

                            $designation='';
                        		if (isset($res_input['student_profile'][0]['designation']) && $res_input['student_profile'][0]['designation']!='') {
                            	$designation                  = json_decode($res_input['student_profile'][0]['designation']);
                        		}

                            $designation_description='';
                        		if (isset($res_input['student_profile'][0]['designation_description']) && $res_input['student_profile'][0]['designation_description']!='') {
                            	$designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                        		}

                            $company_name='';
                        		if (isset($res_input['student_profile'][0]['company_name']) && $res_input['student_profile'][0]['company_name']!='') {
                            	 $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                        		}

                        		$years_of_experience='';
                        		if (isset($res_input['student_profile'][0]['years_of_experience']) && $res_input['student_profile'][0]['years_of_experience']!='') {
                            	 $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                        		}
                           
                           	$domainAreaExp='';
                        		if (isset($res_input['student_profile'][0]['domain_area_of_expertise_search']) && $res_input['student_profile'][0]['domain_area_of_expertise_search']!='') {
                            	 $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                        		}
                            
                            $skillSetArr='';
                        		if (isset($res_input['student_profile'][0]['skill_sets_search']) && $res_input['student_profile'][0]['skill_sets_search']!='') {
                            	  $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                        		}
                            
                            $university_since_year ='';
                        		if (isset($res_input['student_profile'][0]['university_since_year']) && $res_input['student_profile'][0]['university_since_year']!='') {
                            	   $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                        		}
                           
                           $university_to_year ='';
                        		if (isset($res_input['student_profile'][0]['university_to_year']) && $res_input['student_profile'][0]['university_to_year']!='') {
                            	   $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                        		}

                        		$university_name ='';
                        		if (isset($res_input['student_profile'][0]['university_name']) && $res_input['student_profile'][0]['university_name']!='') {
                            	   $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                        		}
                           
                           $university_location ='';
                        		if (isset($res_input['student_profile'][0]['university_location']) && $res_input['student_profile'][0]['university_location']!='') {
                            	   $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                        		}
                            
                            
                            $event_description_of_work ='';
                        		if (isset($res_input['student_profile'][0]['event_description_of_work']) && $res_input['student_profile'][0]['event_description_of_work']!='') {
                            	 $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                        		}
                           
                            $event_experience_in_year ='';
                        		if (isset($res_input['student_profile'][0]['event_experience_in_year']) && $res_input['student_profile'][0]['event_experience_in_year']!='') {
                            	    $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                        		}

                        		$technical_experience_in_year ='';
                        		if (isset($res_input['student_profile'][0]['technical_experience_in_year']) && $res_input['student_profile'][0]['technical_experience_in_year']!='') {
                            	       $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                        		}

                         		$technical_experience_to_year ='';
                        		if (isset($res_input['student_profile'][0]['technical_experience_to_year']) && $res_input['student_profile'][0]['technical_experience_to_year']!='') {
                      	       	$technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);
                        		}
                         
                            

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $org_sector             = '';
                            $org_sector_other       = '';
                            $overview               = '';
                            $spoc_bill_addr1        = '';
                            $spoc_bill_addr2        = '';
                            $spoc_bill_addr_city    = '';
                            $spoc_bill_addr_state   = '';
                            $spoc_bill_addr_country = '';
                            $spoc_bill_addr_pin     = '';
                            $self_declaration       = '';
                            $specialities_products  = '';
                            $establishment_year     = '';
                            $institution_size       = '';
                            $company_evaluation     = '';
                            $website                = '';
                            $linkedin_page          = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $org_sector);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $admin_approval);

                        } else if ($user_category_id == 2) {

                      			$org_sector ='';
                        		if (isset($res_input['student_profile'][0]['org_sector']) && $res_input['student_profile'][0]['org_sector']!='') {
                      	       	$org_sector                 = $res_input['org_profile'][0]['org_sector'];
                        		}

                        		$org_sector_other ='';
                        		if (isset($res_input['student_profile'][0]['org_sector_other']) && $res_input['student_profile'][0]['org_sector_other']!='') {
                      	       	 $org_sector_other           = $encrptopenssl->decrypt($res_input['org_profile'][0]['org_sector_other']);
                        		}

                        		$overview ='';
                        		if (isset($res_input['student_profile'][0]['overview']) && $res_input['student_profile'][0]['overview']!='') {
                      	       	 $overview           = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                        		}

                        		$spoc_bill_addr1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr1']) && $res_input['student_profile'][0]['spoc_bill_addr1']!='') {
                      	       	 $spoc_bill_addr1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                        		}

                        		$spoc_bill_addr2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr2']) && $res_input['student_profile'][0]['spoc_bill_addr2']!='') {
                      	       	 $spoc_bill_addr2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                        		}

                        		$spoc_bill_addr_city ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_city']) && $res_input['student_profile'][0]['spoc_bill_addr_city']!='') {
                      	       	 $spoc_bill_addr_city           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                        		}
                            
                            $spoc_bill_addr_state ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_state']) && $res_input['student_profile'][0]['spoc_bill_addr_state']!='') {
                      	       	 $spoc_bill_addr_state           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                        		}

                        		$spoc_bill_addr_country ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_country']) && $res_input['student_profile'][0]['spoc_bill_addr_country']!='') {
                      	       	 $spoc_bill_addr_country           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                        		}

                        		$spoc_bill_addr_pin ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_pin']) && $res_input['student_profile'][0]['spoc_bill_addr_pin']!='') {
                      	       	 $spoc_bill_addr_pin           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                        		}

                        		$spoc_sec_num_country_code1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num_country_code1']) && $res_input['student_profile'][0]['spoc_sec_num_country_code1']!='') {
                      	       	 $spoc_sec_num_country_code1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                        		}

                        		$spoc_sec_num1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num1']) && $res_input['student_profile'][0]['spoc_sec_num1']!='') {
                      	       	 $spoc_sec_num1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                        		}
                 						
                 						$spoc_sec_num_country_code2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num_country_code2']) && $res_input['student_profile'][0]['spoc_sec_num_country_code2']!='') {
                      	       	 $spoc_sec_num_country_code2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                        		}

                        		$spoc_sec_num2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num2']) && $res_input['student_profile'][0]['spoc_sec_num2']!='') {
                      	       	 $spoc_sec_num2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                        		}
                    
                        		$self_declaration ='';
                        		if (isset($res_input['student_profile'][0]['self_declaration']) && $res_input['student_profile'][0]['self_declaration']!='') {
                      	       	 $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                        		}

                      			$specialities_products ='';
                        		if (isset($res_input['student_profile'][0]['specialities_products']) && $res_input['student_profile'][0]['specialities_products']!='') {
                      	       	 $specialities_products           = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                        		}

                        		$establishment_year ='';
                        		if (isset($res_input['student_profile'][0]['establishment_year']) && $res_input['student_profile'][0]['establishment_year']!='') {
                      	       	 $establishment_year           = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                        		}
                      	
                      			$institution_size ='';
                        		if (isset($res_input['student_profile'][0]['institution_size']) && $res_input['student_profile'][0]['institution_size']!='') {
                      	       	 $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                        		}

                        		$company_evaluation ='';
                        		if (isset($res_input['student_profile'][0]['company_evaluation']) && $res_input['student_profile'][0]['company_evaluation']!='') {
                      	       	 $company_evaluation           = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                        		}
          									
          									$website ='';
                        		if (isset($res_input['student_profile'][0]['website']) && $res_input['student_profile'][0]['website']!='') {
                      	       	 $website           = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                        		}

                        		$linkedin_page ='';
                        		if (isset($res_input['student_profile'][0]['linkedin_page']) && $res_input['student_profile'][0]['linkedin_page']!='') {
                      	       	 $linkedin_page           = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);
                        		}
                           
               					    $rOtherOrg ='';
                        		if($org_sector!=''){
	                            $explodeSec = explode(",", $org_sector);
	                            $addArr     = array();

	                            foreach ($explodeSec as $orgn) {

	                                $orgCapture = $this->master_model->getRecords("organization_sector", array('id' => $orgn));
	                                $getOrgnm   = $orgCapture[0]['name'];
	                                array_push($addArr, $getOrgnm);

	                            	}
	                            	$impOrg = implode(", ", $addArr);

		                            $removeOtherOrg = str_replace("other", "", $impOrg);
		                            $rOtherOrg      = rtrim($removeOtherOrg, ',');
                          	}

                            

                            $employmentStatus    = '';
                            $other_emp_status    = '';
                            $other_domian        = '';
                            $company_name        = '';
                            $desig               = '';
                            $desig_des           = '';
                            $years_of_experience = '';
                            $impDomain           = '';
                            $impSkill            = '';
                            $uni_since_year      = '';
                            $uni_to_year         = '';
                            $uni_name            = '';
                            $uni_loc             = '';
                            $event_desc_work     = '';
                            $event_exp           = '';
                            $tech_exp            = '';
                            $tech_to             = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $rOtherOrg);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $admin_approval);
                        }

                    }
                    $rowCount++;

                } // For loop end

            } // Count Check

            //die();

            // create file name
            //$fileName = 'users-'.time().'.xlsx';
            /*
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="users-' . time() . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            */
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
						header('Content-Type: application/vnd.ms-excel');
						header('Content-Disposition: attachment;filename="userList.xls"');
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        //$data['records'] = $res_arr;
        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/index';
        $this->load->view('admin/admin_combo', $data);
    }

		public function export_to_excel($keyword=0, $profile_type=0)
		{
			
					$this->load->library('excel');
        $this->load->library('Opensslencryptdecrypt');
            //error_reporting(E_ALL);
            $encrptopenssl = new Opensslencryptdecrypt();

            $response = array();

            //$keyword      = @$this->input->post('keyword') ? $this->input->post('keyword') : '';
            //$profile_type = @$this->input->post('profile_type') ? $this->input->post('profile_type') : '';
						
						if($keyword == '0') { $keyword = ''; }
						if($profile_type == '0') { $profile_type = ''; }
						
            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	arai_registration.first_name LIKE '%" . $string . "%' OR
									arai_registration.middle_name LIKE '%" . $string . "%' OR
									arai_registration.last_name LIKE '%" . $string . "%' OR
									arai_registration.email LIKE '%" . $string . "%' OR
									arai_registration.mobile LIKE'%" . $string . "%' OR arai_registration.json_str LIKE '%" . $keyword . "%') ";

            }

            if ($profile_type != '') {
                $search_arr[] = " arai_registration.user_category_id='" . $profile_type . "' ";
            }

            if (count($search_arr) > 0) {
                $searchQuery = implode(" AND ", $search_arr);
            }

            ## Total number of records without filtering
            $records = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));

            $totalRecords = count($records);

            ## Total number of record with filtering
            $totalRecordwithFilter = count($records);
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
                $recordsFilter         = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));
                $totalRecordwithFilter = count($recordsFilter);
            }

            ## Fetch records
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
            }
            //$this->db->limit(3000,6001);
            $this->db->select('registration.user_id,registration.user_category_id,registration.priority,registration.payment_status,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,
				registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
				registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
				country.phonecode,registration_usercategory.user_category,registration_usersubcategory.sub_catname');
            $this->db->join('country', 'registration.country_code=country.id', 'left');
            $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
            $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
            $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
            $records = $this->master_model->getRecords("registration", array('registration.is_valid' => '1', 'registration.is_deleted' => '0'), '', array('registration.user_id' => 'DESC'));
            //echo $this->db->last_query(); exit;

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);

                // set Header
                if ($profile_type == 1) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Admin Approval');

                } else if ($profile_type == 2) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'LinkedIn Page');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Admin Approval');

                } else {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'LinkedIn Page');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AR1', 'Admin Approval');

                }

                // set Row
                $rowCount = 2;
                $res_arr  = array();
                foreach ($records as $row_val) {

                    $i++;
                    $row_val['user_id']               = $row_val['user_id'];
                    $row_val['user_category_id']      = $row_val['user_category_id'];
                    $row_val['membership_type']       = $encrptopenssl->decrypt($row_val['membership_type']);
                    $row_val['title']                 = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']            = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name']           = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']             = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']                 = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['phonecode']             = $row_val['phonecode'];
                    $row_val['mobile']                = $encrptopenssl->decrypt($row_val['mobile']);
                    $row_val['xOrder']                = $row_val['xOrder'];
                    $row_val['user_category']         = $encrptopenssl->decrypt($row_val['user_category']);
                    $row_val['sub_catname']           = $encrptopenssl->decrypt($row_val['sub_catname']);
                    $row_val['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);
                    $row_val['priority']              = $row_val['priority'];

                    $fullname1   = ucfirst($row_val['title']) . " " . ucfirst($row_val['first_name']) . " " . ucfirst($row_val['middle_name']) . " " . ucfirst($row_val['last_name']);
                    $countryCode = $row_val['phonecode'] . "-" . $row_val['mobile'];

                    $userID    = $row_val['user_id'];
                    $full_name = $fullname1;

                    if ($row_val['user_category_id'] == 1) {

                        $uid = $row_val['user_id'];
                        $this->db->select('type,status,employement_status,other_emp_status,other_domian,other_domian,other_skill_sets,university_course
										  university_since_year,university_to_year,university_name,company_name,university_location,designation,
										  designation_description,event_description_of_work,event_experience_in_year,technical_experience_in_year,
										  technical_experience_to_year,pincode,flat_house_building_apt_company,area_colony_street_village,
										  town_city_and_state,country,additional_detail,specify_fields_area_that_you_would_like,years_of_experience,
										  bio_data,no_of_paper_publication,no_of_patents,portal_name,portal_link,portal_description,portal_name_multiple,
										  portal_link_multiple,portal_description_multiple,domain_area_of_expertise_search,skill_sets_search');
                        $studentProfile = $this->master_model->getRecords("student_profile", array('user_id' => $uid));

                        $row_val['student_profile'] = $studentProfile;

                    } else if ($row_val['user_category_id'] == 2) {

                        if ($row_val['user_category_id'] == 2) {

                            $uid = $row_val['user_id'];
                            $this->db->select('org_sector, org_sector_other, overview, spoc_bill_addr1, spoc_bill_addr2, spoc_bill_addr_city,
											  spoc_bill_addr_state, spoc_bill_addr_country, spoc_bill_addr_pin, spoc_sec_num_country_code1,
											  spoc_sec_num1, spoc_sec_num_country_code2, spoc_sec_num2, self_declaration, specialities_products,
											  establishment_year, institution_size, company_evaluation, website, linkedin_page');
                            $orgProfile = $this->master_model->getRecords("profile_organization", array('user_id' => $uid));

                            $row_val['org_profile'] = $orgProfile;

                        } // Organization Result

                    }

                    //$rowCount++;
                    $res_arr[] = $row_val;

                } // Foreach End
                //echo ">>>>".$profile_type;
                //echo "<pre>";print_r($res_arr);die();
                foreach ($res_arr as $res_input) {

                    $i++;
                    $user_id               = $res_input['user_id'];
                    $user_category_id      = $res_input['user_category_id'];
                    $membership_type       = $res_input['membership_type'];
                    $title                 = $res_input['title'];
                    $first_name            = $res_input['first_name'];
                    $middle_name           = $res_input['middle_name'];
                    $last_name             = $res_input['last_name'];
                    $email                 = $res_input['email'];
                    $payment_status        = $res_input['payment_status'];
                    $phonecode             = $res_input['phonecode'];
                    $mobile                = $res_input['mobile'];
                    $user_category         = $res_input['user_category'];
                    $sub_catname           = $res_input['sub_catname'];
                    $institution_full_name = $res_input['institution_full_name'];
                    $fullname1             = ucfirst($res_input['title']) . " " . ucfirst($res_input['first_name']) . " " . ucfirst($res_input['middle_name']) . " " . ucfirst($res_input['last_name']);
                    $countryCode           = $phonecode . "-" . $mobile;
                    $full_name             = $fullname1;
                    $pay_status            = ucfirst($payment_status);
                    $admin_approval        = $res_input['admin_approval'];
                    $userStatus            = $res_input['status'];

                    if ($row_val['priority'] == 1) {
                        $priorityStatus = 'High';
                    }

                    if ($row_val['priority'] == 2) {
                        $priorityStatus = 'Medium';
                    }

                    if ($row_val['priority'] == 3) {
                        $priorityStatus = 'Low';
                    }

                    if ($profile_type) {

                        if ($user_category_id == 1) {

                            $employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                            $other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                            $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                            $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                            $designation                  = json_decode($res_input['student_profile'][0]['designation']);
                            $designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                            $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                            $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                            $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                            $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                            $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                            $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                            $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                            $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                            $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                            $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                            $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                            $technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $admin_approval);

                        } else if ($user_category_id == 2) {

                            $org_sector                 = $res_input['org_profile'][0]['org_sector'];
                            $org_sector_other           = $encrptopenssl->decrypt($res_input['org_profile'][0]['org_sector_other']);
                            $overview                   = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                            $spoc_bill_addr1            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                            $spoc_bill_addr2            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                            $spoc_bill_addr_city        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                            $spoc_bill_addr_state       = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                            $spoc_bill_addr_country     = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                            $spoc_bill_addr_pin         = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                            $spoc_sec_num_country_code1 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                            $spoc_sec_num1              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                            $spoc_sec_num_country_code2 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                            $spoc_sec_num2              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                            $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                            $specialities_products      = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                            $establishment_year         = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                            $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                            $company_evaluation         = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                            $website                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                            $linkedin_page              = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);

                            $explodeSec = explode(",", $org_sector);

                            $addArr = array();
                            foreach ($explodeSec as $orgn) {

                                $orgCapture = $this->master_model->getRecords("organization_sector", array('id' => $orgn));
                                $getOrgnm   = $orgCapture[0]['name'];
                                array_push($addArr, $getOrgnm);

                            }

                            $impOrg = implode(", ", $addArr);

                            $removeOtherOrg = str_replace("other", "", $impOrg);

                            $rOtherOrg = rtrim($removeOtherOrg, ',');

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $rOtherOrg);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $admin_approval);
                        }

                    } else {

                        if ($user_category_id == 1) {
                        		$employmentStatus='';
                        		if (isset($res_input['student_profile'][0]['employement_status']) && $res_input['student_profile'][0]['employement_status']!='') {
                            	$employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                        		}

                        		$other_emp_status='';
                        		if (isset($res_input['student_profile'][0]['other_emp_status']) && $res_input['student_profile'][0]['other_emp_status']!='') {
                            	$other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                        		}
                            
                            $other_domian='';
                        		if (isset($res_input['student_profile'][0]['other_domian']) && $res_input['student_profile'][0]['other_domian']!='') {
                            	 $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                        		}

                        		$other_skill_sets='';
                        		if (isset($res_input['student_profile'][0]['other_skill_sets']) && $res_input['student_profile'][0]['other_skill_sets']!='') {
                            	 $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                        		}

                            $designation='';
                        		if (isset($res_input['student_profile'][0]['designation']) && $res_input['student_profile'][0]['designation']!='') {
                            	$designation                  = json_decode($res_input['student_profile'][0]['designation']);
                        		}

                            $designation_description='';
                        		if (isset($res_input['student_profile'][0]['designation_description']) && $res_input['student_profile'][0]['designation_description']!='') {
                            	$designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                        		}

                            $company_name='';
                        		if (isset($res_input['student_profile'][0]['company_name']) && $res_input['student_profile'][0]['company_name']!='') {
                            	 $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                        		}

                        		$years_of_experience='';
                        		if (isset($res_input['student_profile'][0]['years_of_experience']) && $res_input['student_profile'][0]['years_of_experience']!='') {
                            	 $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                        		}
                           
                           	$domainAreaExp='';
                        		if (isset($res_input['student_profile'][0]['domain_area_of_expertise_search']) && $res_input['student_profile'][0]['domain_area_of_expertise_search']!='') {
                            	 $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                        		}
                            
                            $skillSetArr='';
                        		if (isset($res_input['student_profile'][0]['skill_sets_search']) && $res_input['student_profile'][0]['skill_sets_search']!='') {
                            	  $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                        		}
                            
                            $university_since_year ='';
                        		if (isset($res_input['student_profile'][0]['university_since_year']) && $res_input['student_profile'][0]['university_since_year']!='') {
                            	   $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                        		}
                           
                           $university_to_year ='';
                        		if (isset($res_input['student_profile'][0]['university_to_year']) && $res_input['student_profile'][0]['university_to_year']!='') {
                            	   $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                        		}

                        		$university_name ='';
                        		if (isset($res_input['student_profile'][0]['university_name']) && $res_input['student_profile'][0]['university_name']!='') {
                            	   $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                        		}
                           
                           $university_location ='';
                        		if (isset($res_input['student_profile'][0]['university_location']) && $res_input['student_profile'][0]['university_location']!='') {
                            	   $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                        		}
                            
                            
                            $event_description_of_work ='';
                        		if (isset($res_input['student_profile'][0]['event_description_of_work']) && $res_input['student_profile'][0]['event_description_of_work']!='') {
                            	 $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                        		}
                           
                            $event_experience_in_year ='';
                        		if (isset($res_input['student_profile'][0]['event_experience_in_year']) && $res_input['student_profile'][0]['event_experience_in_year']!='') {
                            	    $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                        		}

                        		$technical_experience_in_year ='';
                        		if (isset($res_input['student_profile'][0]['technical_experience_in_year']) && $res_input['student_profile'][0]['technical_experience_in_year']!='') {
                            	       $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                        		}

                         		$technical_experience_to_year ='';
                        		if (isset($res_input['student_profile'][0]['technical_experience_to_year']) && $res_input['student_profile'][0]['technical_experience_to_year']!='') {
                      	       	$technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);
                        		}
                         
                            

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $org_sector             = '';
                            $org_sector_other       = '';
                            $overview               = '';
                            $spoc_bill_addr1        = '';
                            $spoc_bill_addr2        = '';
                            $spoc_bill_addr_city    = '';
                            $spoc_bill_addr_state   = '';
                            $spoc_bill_addr_country = '';
                            $spoc_bill_addr_pin     = '';
                            $self_declaration       = '';
                            $specialities_products  = '';
                            $establishment_year     = '';
                            $institution_size       = '';
                            $company_evaluation     = '';
                            $website                = '';
                            $linkedin_page          = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $org_sector);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $admin_approval);

                        } else if ($user_category_id == 2) {

                      			$org_sector ='';
                        		if (isset($res_input['student_profile'][0]['org_sector']) && $res_input['student_profile'][0]['org_sector']!='') {
                      	       	$org_sector                 = $res_input['org_profile'][0]['org_sector'];
                        		}

                        		$org_sector_other ='';
                        		if (isset($res_input['student_profile'][0]['org_sector_other']) && $res_input['student_profile'][0]['org_sector_other']!='') {
                      	       	 $org_sector_other           = $encrptopenssl->decrypt($res_input['org_profile'][0]['org_sector_other']);
                        		}

                        		$overview ='';
                        		if (isset($res_input['student_profile'][0]['overview']) && $res_input['student_profile'][0]['overview']!='') {
                      	       	 $overview           = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                        		}

                        		$spoc_bill_addr1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr1']) && $res_input['student_profile'][0]['spoc_bill_addr1']!='') {
                      	       	 $spoc_bill_addr1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                        		}

                        		$spoc_bill_addr2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr2']) && $res_input['student_profile'][0]['spoc_bill_addr2']!='') {
                      	       	 $spoc_bill_addr2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                        		}

                        		$spoc_bill_addr_city ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_city']) && $res_input['student_profile'][0]['spoc_bill_addr_city']!='') {
                      	       	 $spoc_bill_addr_city           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                        		}
                            
                            $spoc_bill_addr_state ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_state']) && $res_input['student_profile'][0]['spoc_bill_addr_state']!='') {
                      	       	 $spoc_bill_addr_state           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                        		}

                        		$spoc_bill_addr_country ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_country']) && $res_input['student_profile'][0]['spoc_bill_addr_country']!='') {
                      	       	 $spoc_bill_addr_country           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                        		}

                        		$spoc_bill_addr_pin ='';
                        		if (isset($res_input['student_profile'][0]['spoc_bill_addr_pin']) && $res_input['student_profile'][0]['spoc_bill_addr_pin']!='') {
                      	       	 $spoc_bill_addr_pin           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                        		}

                        		$spoc_sec_num_country_code1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num_country_code1']) && $res_input['student_profile'][0]['spoc_sec_num_country_code1']!='') {
                      	       	 $spoc_sec_num_country_code1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                        		}

                        		$spoc_sec_num1 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num1']) && $res_input['student_profile'][0]['spoc_sec_num1']!='') {
                      	       	 $spoc_sec_num1           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                        		}
                 						
                 						$spoc_sec_num_country_code2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num_country_code2']) && $res_input['student_profile'][0]['spoc_sec_num_country_code2']!='') {
                      	       	 $spoc_sec_num_country_code2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                        		}

                        		$spoc_sec_num2 ='';
                        		if (isset($res_input['student_profile'][0]['spoc_sec_num2']) && $res_input['student_profile'][0]['spoc_sec_num2']!='') {
                      	       	 $spoc_sec_num2           = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                        		}
                    
                        		$self_declaration ='';
                        		if (isset($res_input['student_profile'][0]['self_declaration']) && $res_input['student_profile'][0]['self_declaration']!='') {
                      	       	 $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                        		}

                      			$specialities_products ='';
                        		if (isset($res_input['student_profile'][0]['specialities_products']) && $res_input['student_profile'][0]['specialities_products']!='') {
                      	       	 $specialities_products           = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                        		}

                        		$establishment_year ='';
                        		if (isset($res_input['student_profile'][0]['establishment_year']) && $res_input['student_profile'][0]['establishment_year']!='') {
                      	       	 $establishment_year           = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                        		}
                      	
                      			$institution_size ='';
                        		if (isset($res_input['student_profile'][0]['institution_size']) && $res_input['student_profile'][0]['institution_size']!='') {
                      	       	 $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                        		}

                        		$company_evaluation ='';
                        		if (isset($res_input['student_profile'][0]['company_evaluation']) && $res_input['student_profile'][0]['company_evaluation']!='') {
                      	       	 $company_evaluation           = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                        		}
          									
          									$website ='';
                        		if (isset($res_input['student_profile'][0]['website']) && $res_input['student_profile'][0]['website']!='') {
                      	       	 $website           = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                        		}

                        		$linkedin_page ='';
                        		if (isset($res_input['student_profile'][0]['linkedin_page']) && $res_input['student_profile'][0]['linkedin_page']!='') {
                      	       	 $linkedin_page           = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);
                        		}
                           
               					    $rOtherOrg ='';
                        		if($org_sector!=''){
	                            $explodeSec = explode(",", $org_sector);
	                            $addArr     = array();

	                            foreach ($explodeSec as $orgn) {

	                                $orgCapture = $this->master_model->getRecords("organization_sector", array('id' => $orgn));
	                                $getOrgnm   = $orgCapture[0]['name'];
	                                array_push($addArr, $getOrgnm);

	                            	}
	                            	$impOrg = implode(", ", $addArr);

		                            $removeOtherOrg = str_replace("other", "", $impOrg);
		                            $rOtherOrg      = rtrim($removeOtherOrg, ',');
                          	}

                            

                            $employmentStatus    = '';
                            $other_emp_status    = '';
                            $other_domian        = '';
                            $company_name        = '';
                            $desig               = '';
                            $desig_des           = '';
                            $years_of_experience = '';
                            $impDomain           = '';
                            $impSkill            = '';
                            $uni_since_year      = '';
                            $uni_to_year         = '';
                            $uni_name            = '';
                            $uni_loc             = '';
                            $event_desc_work     = '';
                            $event_exp           = '';
                            $tech_exp            = '';
                            $tech_to             = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $rOtherOrg);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $userStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $rowCount, $admin_approval);
                        }

                    }
                    $rowCount++;

                } // For loop end

            } // Count Check

            //die();

            // create file name
            //$fileName = 'users-'.time().'.xlsx';
            /*
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="users-' . time() . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            */
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
						header('Content-Type: application/vnd.ms-excel');
						header('Content-Disposition: attachment;filename="userList.xls"');
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

       

		}
    /*public function userList()
    {
    // load excel library
    $this->load->library('excel');
    $this->load->library('Opensslencryptdecrypt');

    if(isset($_POST['export']) && !empty($_POST['export']))
    {

    error_reporting(E_ALL);
    $encrptopenssl =  New Opensslencryptdecrypt();

    $response = array();

    $keyword         = @$this->input->post('keyword')?$this->input->post('keyword'):'';
    $profile_type     = @$this->input->post('profile_type')?$this->input->post('profile_type'):'';

    ## Search
    $search_arr = array();
    $searchQuery = "";
    if($keyword != ''){
    $string = $encrptopenssl->encrypt($keyword);
    $search_arr[] = " (    arai_registration.first_name LIKE '%".$string."%' OR
    arai_registration.middle_name LIKE '%".$string."%' OR
    arai_registration.last_name LIKE '%".$string."%' OR
    arai_registration.email LIKE '%".$string."%' OR
    arai_registration.mobile LIKE'%".$string."%' OR arai_registration.json_str LIKE '%".$keyword."%') ";

    }

    if($profile_type != ''){
    $search_arr[] = " arai_registration.user_category_id='".$profile_type."' ";
    }

    if(count($search_arr) > 0){
    $searchQuery = implode(" AND ",$search_arr);
    }

    ## Total number of records without filtering
    $records = $this->master_model->getRecords("registration",array('is_valid'=>'1','is_deleted'=>'0'));

    $totalRecords = count($records);

    ## Total number of record with filtering
    $totalRecordwithFilter = count($records);
    if($searchQuery != ''){
    $this->db->where($searchQuery);
    $recordsFilter = $this->master_model->getRecords("registration",array('is_valid'=>'1','is_deleted'=>'0'));
    $totalRecordwithFilter = count($recordsFilter);
    }

    ## Fetch records
    if($searchQuery != ''){
    $this->db->where($searchQuery);
    }

    $this->db->select('registration.user_id,registration.user_category_id,registration.priority,registration.payment_status,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,
    registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
    registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
    country.phonecode,registration_usercategory.user_category,registration_usersubcategory.sub_catname');
    $this->db->join('country','registration.country_code=country.id','left');
    $this->db->join('student_profile','registration.user_id=student_profile.user_id','left');
    $this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id','left');
    $this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id','left');
    $records = $this->master_model->getRecords("registration",array('registration.is_valid'=>'1','registration.is_deleted'=>'0'),'',array('registration.user_id' => 'DESC'));

    $data = array();
    $i = 0;

    // Count Check
    if(count($records) > 0){

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);

    // set Header
    if($profile_type == 1){

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');

    } else if($profile_type == 2){

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Payment Status');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Orgnaization Sector');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Orgnaization Sector Other');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Overview');
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Spoc Bill Address 1');
    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Spoc Bill Address 2');
    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'City');
    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'State');
    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Country');
    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Pincode');
    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Self Declaration');
    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Specialities Product');
    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Establishment Year');
    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Institution Size');
    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Company Evalution');
    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Website');
    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'LinkedIn Page');

    } else {

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');

    }

    // set Row
    $rowCount = 2;
    $res_arr = array();
    foreach($records as $row_val){

    $i++;
    $row_val['user_id']                     = $row_val['user_id'];
    $row_val['user_category_id']             = $row_val['user_category_id'];
    $row_val['membership_type']             = $encrptopenssl->decrypt($row_val['membership_type']);
    $row_val['title']                         = $encrptopenssl->decrypt($row_val['title']);
    $row_val['first_name']                     = $encrptopenssl->decrypt($row_val['first_name']);
    $row_val['middle_name']                 = $encrptopenssl->decrypt($row_val['middle_name']);
    $row_val['last_name']                     = $encrptopenssl->decrypt($row_val['last_name']);
    $row_val['email']                         = $encrptopenssl->decrypt($row_val['email']);
    $row_val['phonecode']                     = $row_val['phonecode'];
    $row_val['mobile']                         = $encrptopenssl->decrypt($row_val['mobile']);
    $row_val['xOrder']                         = $row_val['xOrder'];
    $row_val['user_category']                 = $encrptopenssl->decrypt($row_val['user_category']);
    $row_val['sub_catname']                 = $encrptopenssl->decrypt($row_val['sub_catname']);
    $row_val['institution_full_name']         = $encrptopenssl->decrypt($row_val['institution_full_name']);
    $row_val['priority']                     = $row_val['priority'];

    $fullname1    = ucfirst($row_val['title'])." ".ucfirst($row_val['first_name'])." ".ucfirst($row_val['middle_name']) ." ".ucfirst($row_val['last_name']);
    $countryCode = $row_val['phonecode']."-".$row_val['mobile'];

    $userID         = $row_val['user_id'];
    $full_name         = $fullname1;

    if($row_val['user_category_id'] == 1){

    $uid = $row_val['user_id'];
    $this->db->select('type,status,employement_status,other_emp_status,other_domian,other_domian,other_skill_sets,university_course
    university_since_year,university_to_year,university_name,company_name,university_location,designation,
    designation_description,event_description_of_work,event_experience_in_year,technical_experience_in_year,
    technical_experience_to_year,pincode,flat_house_building_apt_company,area_colony_street_village,
    town_city_and_state,country,additional_detail,specify_fields_area_that_you_would_like,years_of_experience,
    bio_data,no_of_paper_publication,no_of_patents,portal_name,portal_link,portal_description,portal_name_multiple,
    portal_link_multiple,portal_description_multiple,domain_area_of_expertise_search,skill_sets_search');
    $studentProfile = $this->master_model->getRecords("student_profile",array('user_id' => $uid));

    $row_val['student_profile'] = $studentProfile;

    } else if($row_val['user_category_id'] == 2){

    if($row_val['user_category_id'] == 2){

    $uid = $row_val['user_id'];
    $this->db->select('org_sector, org_sector_other, overview, spoc_bill_addr1, spoc_bill_addr2, spoc_bill_addr_city,
    spoc_bill_addr_state, spoc_bill_addr_country, spoc_bill_addr_pin, spoc_sec_num_country_code1,
    spoc_sec_num1, spoc_sec_num_country_code2, spoc_sec_num2, self_declaration, specialities_products,
    establishment_year, institution_size, company_evaluation, website, linkedin_page');
    $orgProfile = $this->master_model->getRecords("profile_organization",array('user_id' => $uid));

    $row_val['org_profile'] = $orgProfile;

    }  // Organization Result

    }

    //$rowCount++;
    $res_arr[] = $row_val;

    } // Foreach End
    //echo ">>>>".$profile_type;
    //echo "<pre>";print_r($res_arr);die();
    foreach($res_arr as $res_input){

    $i++;
    $user_id                     = $res_input['user_id'];
    $user_category_id             = $res_input['user_category_id'];
    $membership_type             = $res_input['membership_type'];
    $title                         = $res_input['title'];
    $first_name                 = $res_input['first_name'];
    $middle_name                 = $res_input['middle_name'];
    $last_name                     = $res_input['last_name'];
    $email                         = $res_input['email'];
    $payment_status                = $res_input['payment_status'];
    $phonecode                     = $res_input['phonecode'];
    $mobile                     = $res_input['mobile'];
    $user_category                = $res_input['user_category'];
    $sub_catname                 = $res_input['sub_catname'];
    $institution_full_name         = $res_input['institution_full_name'];
    $fullname1                    = ucfirst($res_input['title'])." ".ucfirst($res_input['first_name'])." ".ucfirst($res_input['middle_name']) ." ".ucfirst($res_input['last_name']);
    $countryCode                 = $phonecode."-".$mobile;
    $full_name                     = $fullname1;
    $pay_status                    = ucfirst($payment_status);
    $admin_approval                = $res_input['admin_approval'];

    if($row_val['priority'] == 1){
    $priorityStatus = 'High';
    }

    if($row_val['priority'] == 2){
    $priorityStatus = 'Medium';
    }

    if($row_val['priority'] == 3){
    $priorityStatus = 'Low';
    }

    if($profile_type){

    if($user_category_id == 1){

    $employmentStatus    = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
    $other_emp_status    = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
    $other_domian        = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
    $other_skill_sets    = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
    $designation        = json_decode($res_input['student_profile'][0]['designation']);
    $designation_description    = json_decode($res_input['student_profile'][0]['designation_description']);
    $company_name        = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
    $years_of_experience = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
    $domainAreaExp         = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
    $skillSetArr         = $res_input['student_profile'][0]['skill_sets_search'];
    $university_since_year    = json_decode($res_input['student_profile'][0]['university_since_year']);
    $university_to_year        = json_decode($res_input['student_profile'][0]['university_to_year']);
    $university_name        = json_decode($res_input['student_profile'][0]['university_name']);
    $university_location    = json_decode($res_input['student_profile'][0]['university_location']);
    $event_description_of_work        = json_decode($res_input['student_profile'][0]['event_description_of_work']);
    $event_experience_in_year        = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
    $technical_experience_in_year    = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
    $technical_experience_to_year    = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);

    if($designation!=""){$desig = $designation[0];} else {$desig = '';}
    if($designation_description!=""){$desig_des = $designation_description[0];} else {$desig_des = '';}
    if($university_since_year!=""){$uni_since_year = $university_since_year[0];} else {$uni_since_year = '';}
    if($university_to_year!=""){$uni_to_year = $university_to_year[0];} else {$uni_to_year = '';}
    if($university_name!=""){$uni_name = $university_name[0];} else {$uni_name = '';}
    if($university_location!=""){$uni_loc = $university_location[0];} else {$uni_loc = '';}
    if($event_description_of_work!=""){$event_desc_work = $event_description_of_work[0];} else {$event_desc_work = '';}
    if($event_experience_in_year!=""){$event_exp = $event_experience_in_year[0];} else {$event_exp = '';}
    if($technical_experience_in_year!=""){$tech_exp = $technical_experience_in_year[0];} else {$tech_exp = '';}
    if($technical_experience_to_year!=""){$tech_to = $technical_experience_to_year[0];} else {$tech_to = '';}
    // Domain Array
    $imArr = array();
    if($domainAreaExp!=""){

    $exmplodeD = explode(",", $domainAreaExp);

    foreach($exmplodeD as $exId){

    $domainNames = $this->master_model->getRecords("domain_master",array('id' => $exId));
    $dname         = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
    array_push($imArr, $dname);
    }
    $impDomain = implode(", ", $imArr);
    } else {
    $impDomain = '';
    }

    // Skill Set Array
    $skArr = array();
    if($skillSetArr!=""){

    $exmplodeSkill = explode(",", $skillSetArr);

    foreach($exmplodeSkill as $exSk){

    $skillNames = $this->master_model->getRecords("skill_sets",array('id' => $exSk));
    $sname         = $encrptopenssl->decrypt($skillNames[0]['name']);
    array_push($skArr, $sname);
    }
    $impSkill = implode(", ", $skArr);
    } else {
    $impSkill = '';
    }

    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
    $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
    $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
    $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
    $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
    $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
    $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
    $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
    $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
    $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);

    } else if($user_category_id == 2){

    $org_sector                    = $res_input['org_profile'][0]['org_sector'];
    $org_sector_other            = $res_input['org_profile'][0]['org_sector_other'];
    $overview                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
    $spoc_bill_addr1            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
    $spoc_bill_addr2            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
    $spoc_bill_addr_city        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
    $spoc_bill_addr_state        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
    $spoc_bill_addr_country        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
    $spoc_bill_addr_pin            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
    $spoc_sec_num_country_code1    = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
    $spoc_sec_num1                = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
    $spoc_sec_num_country_code2    = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
    $spoc_sec_num2                = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
    $self_declaration            = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
    $specialities_products        = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
    $establishment_year            = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
    $institution_size            = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
    $company_evaluation            = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
    $website                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
    $linkedin_page                = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);

    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $pay_status);
    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $org_sector);
    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $org_sector_other);
    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $overview);
    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $spoc_bill_addr1);
    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $spoc_bill_addr2);
    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $spoc_bill_addr_city);
    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $spoc_bill_addr_state);
    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $spoc_bill_addr_country);
    $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $spoc_bill_addr_pin);
    $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $self_declaration);
    $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $specialities_products);
    $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $establishment_year);
    $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $institution_size);
    $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $company_evaluation);
    $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $website);
    $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $linkedin_page);
    //$objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $company_evaluation);
    //$objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $linkedin_page);

    }

    } else {

    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);

    }

    $rowCount++;

    } // For loop end

    } // Count Check

    //die();

    // create file name
    //$fileName = 'users-'.time().'.xlsx';
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    header('Content-Disposition: attachment;filename="users-'.time().'.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    ob_end_clean();
    $objWriter->save('php://output');
    //$objWriter->save($fileName);
    exit;

    }

    //$data['records'] = $res_arr;
    $data['module_name'] = 'Users';
    $data['submodule_name'] = '';
    $data['middle_content']='users/test';
    $this->load->view('admin/admin_combo',$data);
    }*/

    public function userList()
    {
        // load excel library
        $this->load->library('excel');
        $this->load->library('Opensslencryptdecrypt');

        if (isset($_POST['export']) && !empty($_POST['export'])) {

            //error_reporting(E_ALL);
            $encrptopenssl = new Opensslencryptdecrypt();

            $response = array();

            $keyword      = @$this->input->post('keyword') ? $this->input->post('keyword') : '';
            $profile_type = @$this->input->post('profile_type') ? $this->input->post('profile_type') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	arai_registration.first_name LIKE '%" . $string . "%' OR
									arai_registration.middle_name LIKE '%" . $string . "%' OR
									arai_registration.last_name LIKE '%" . $string . "%' OR
									arai_registration.email LIKE '%" . $string . "%' OR
									arai_registration.mobile LIKE'%" . $string . "%' OR arai_registration.json_str LIKE '%" . $keyword . "%') ";

            }

            if ($profile_type != '') {
                $search_arr[] = " arai_registration.user_category_id='" . $profile_type . "' ";
            }

            if (count($search_arr) > 0) {
                $searchQuery = implode(" AND ", $search_arr);
            }

            ## Total number of records without filtering
            $records = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));

            $totalRecords = count($records);

            ## Total number of record with filtering
            $totalRecordwithFilter = count($records);
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
                $recordsFilter         = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));
                $totalRecordwithFilter = count($recordsFilter);
            }

            ## Fetch records
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
            }

            $this->db->select('registration.user_id,registration.user_category_id,registration.priority,registration.payment_status,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,
				registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
				registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
				country.phonecode,registration_usercategory.user_category,registration_usersubcategory.sub_catname');
            $this->db->join('country', 'registration.country_code=country.id', 'left');
            $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
            $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
            $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
            $records = $this->master_model->getRecords("registration", array('registration.is_valid' => '1', 'registration.is_deleted' => '0'), '', array('registration.user_id' => 'DESC'));

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);

                // set Header
                if ($profile_type == 1) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');

                } else if ($profile_type == 2) {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'LinkedIn Page');

                } else {

                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User ID');
                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fullname');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Category');
                    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Subcategory');
                    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Membership');
                    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Institution Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Payment Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Other Employement Status');
                    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Domain');
                    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Company Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Designation');
                    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Designation Description');
                    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Year Of Experience');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Domain Area Of Expertise');
                    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Skill Sets');
                    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'University Since Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'University To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'University Name');
                    $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'University Location');
                    $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Event Description Of Work');
                    $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Event Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Technical Experience In Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Technical Experience To Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Orgnaization Sector');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Orgnaization Sector Other');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'Overview');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Spoc Bill Address 1');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Spoc Bill Address 2');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'City');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'State');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Country');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Pincode');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'Self Declaration');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'Specialities Product');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Establishment Year');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'Institution Size');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Company Evalution');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Website');
                    $objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'LinkedIn Page');

                }

                // set Row
                $rowCount = 2;
                $res_arr  = array();
                foreach ($records as $row_val) {

                    $i++;
                    $row_val['user_id']               = $row_val['user_id'];
                    $row_val['user_category_id']      = $row_val['user_category_id'];
                    $row_val['membership_type']       = $encrptopenssl->decrypt($row_val['membership_type']);
                    $row_val['title']                 = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']            = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name']           = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']             = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']                 = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['phonecode']             = $row_val['phonecode'];
                    $row_val['mobile']                = $encrptopenssl->decrypt($row_val['mobile']);
                    $row_val['xOrder']                = $row_val['xOrder'];
                    $row_val['user_category']         = $encrptopenssl->decrypt($row_val['user_category']);
                    $row_val['sub_catname']           = $encrptopenssl->decrypt($row_val['sub_catname']);
                    $row_val['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);
                    $row_val['priority']              = $row_val['priority'];

                    $fullname1   = ucfirst($row_val['title']) . " " . ucfirst($row_val['first_name']) . " " . ucfirst($row_val['middle_name']) . " " . ucfirst($row_val['last_name']);
                    $countryCode = $row_val['phonecode'] . "-" . $row_val['mobile'];

                    $userID    = $row_val['user_id'];
                    $full_name = $fullname1;

                    if ($row_val['user_category_id'] == 1) {

                        $uid = $row_val['user_id'];
                        $this->db->select('type,status,employement_status,other_emp_status,other_domian,other_domian,other_skill_sets,university_course
										  university_since_year,university_to_year,university_name,company_name,university_location,designation,
										  designation_description,event_description_of_work,event_experience_in_year,technical_experience_in_year,
										  technical_experience_to_year,pincode,flat_house_building_apt_company,area_colony_street_village,
										  town_city_and_state,country,additional_detail,specify_fields_area_that_you_would_like,years_of_experience,
										  bio_data,no_of_paper_publication,no_of_patents,portal_name,portal_link,portal_description,portal_name_multiple,
										  portal_link_multiple,portal_description_multiple,domain_area_of_expertise_search,skill_sets_search');
                        $studentProfile = $this->master_model->getRecords("student_profile", array('user_id' => $uid));

                        $row_val['student_profile'] = $studentProfile;

                    } else if ($row_val['user_category_id'] == 2) {

                        if ($row_val['user_category_id'] == 2) {

                            $uid = $row_val['user_id'];
                            $this->db->select('org_sector, org_sector_other, overview, spoc_bill_addr1, spoc_bill_addr2, spoc_bill_addr_city,
											  spoc_bill_addr_state, spoc_bill_addr_country, spoc_bill_addr_pin, spoc_sec_num_country_code1,
											  spoc_sec_num1, spoc_sec_num_country_code2, spoc_sec_num2, self_declaration, specialities_products,
											  establishment_year, institution_size, company_evaluation, website, linkedin_page');
                            $orgProfile = $this->master_model->getRecords("profile_organization", array('user_id' => $uid));

                            $row_val['org_profile'] = $orgProfile;

                        } // Organization Result

                    }

                    //$rowCount++;
                    $res_arr[] = $row_val;

                } // Foreach End
                //echo ">>>>".$profile_type;
                //echo "<pre>";print_r($res_arr);die();
                foreach ($res_arr as $res_input) {

                    $i++;
                    $user_id               = $res_input['user_id'];
                    $user_category_id      = $res_input['user_category_id'];
                    $membership_type       = $res_input['membership_type'];
                    $title                 = $res_input['title'];
                    $first_name            = $res_input['first_name'];
                    $middle_name           = $res_input['middle_name'];
                    $last_name             = $res_input['last_name'];
                    $email                 = $res_input['email'];
                    $payment_status        = $res_input['payment_status'];
                    $phonecode             = $res_input['phonecode'];
                    $mobile                = $res_input['mobile'];
                    $user_category         = $res_input['user_category'];
                    $sub_catname           = $res_input['sub_catname'];
                    $institution_full_name = $res_input['institution_full_name'];
                    $fullname1             = ucfirst($res_input['title']) . " " . ucfirst($res_input['first_name']) . " " . ucfirst($res_input['middle_name']) . " " . ucfirst($res_input['last_name']);
                    $countryCode           = $phonecode . "-" . $mobile;
                    $full_name             = $fullname1;
                    $pay_status            = ucfirst($payment_status);
                    $admin_approval        = $res_input['admin_approval'];

                    if ($row_val['priority'] == 1) {
                        $priorityStatus = 'High';
                    }

                    if ($row_val['priority'] == 2) {
                        $priorityStatus = 'Medium';
                    }

                    if ($row_val['priority'] == 3) {
                        $priorityStatus = 'Low';
                    }

                    if ($profile_type) {

                        if ($user_category_id == 1) {

                            $employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                            $other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                            $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                            $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                            $designation                  = json_decode($res_input['student_profile'][0]['designation']);
                            $designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                            $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                            $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                            $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                            $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                            $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                            $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                            $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                            $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                            $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                            $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                            $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                            $technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);

                        } else if ($user_category_id == 2) {

                            $org_sector                 = $res_input['org_profile'][0]['org_sector'];
                            $org_sector_other           = $res_input['org_profile'][0]['org_sector_other'];
                            $overview                   = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                            $spoc_bill_addr1            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                            $spoc_bill_addr2            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                            $spoc_bill_addr_city        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                            $spoc_bill_addr_state       = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                            $spoc_bill_addr_country     = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                            $spoc_bill_addr_pin         = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                            $spoc_sec_num_country_code1 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                            $spoc_sec_num1              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                            $spoc_sec_num_country_code2 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                            $spoc_sec_num2              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                            $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                            $specialities_products      = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                            $establishment_year         = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                            $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                            $company_evaluation         = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                            $website                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                            $linkedin_page              = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $org_sector);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $linkedin_page);

                        }

                    } else {

                        if ($user_category_id == 1) {

                            $employmentStatus             = $encrptopenssl->decrypt($res_input['student_profile'][0]['employement_status']);
                            $other_emp_status             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_emp_status']);
                            $other_domian                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_domian']);
                            $other_skill_sets             = $encrptopenssl->decrypt($res_input['student_profile'][0]['other_skill_sets']);
                            $designation                  = json_decode($res_input['student_profile'][0]['designation']);
                            $designation_description      = json_decode($res_input['student_profile'][0]['designation_description']);
                            $company_name                 = $encrptopenssl->decrypt($res_input['student_profile'][0]['company_name']);
                            $years_of_experience          = $encrptopenssl->decrypt($res_input['student_profile'][0]['years_of_experience']);
                            $domainAreaExp                = $res_input['student_profile'][0]['domain_area_of_expertise_search'];
                            $skillSetArr                  = $res_input['student_profile'][0]['skill_sets_search'];
                            $university_since_year        = json_decode($res_input['student_profile'][0]['university_since_year']);
                            $university_to_year           = json_decode($res_input['student_profile'][0]['university_to_year']);
                            $university_name              = json_decode($res_input['student_profile'][0]['university_name']);
                            $university_location          = json_decode($res_input['student_profile'][0]['university_location']);
                            $event_description_of_work    = json_decode($res_input['student_profile'][0]['event_description_of_work']);
                            $event_experience_in_year     = json_decode($res_input['student_profile'][0]['event_experience_in_year']);
                            $technical_experience_in_year = json_decode($res_input['student_profile'][0]['technical_experience_in_year']);
                            $technical_experience_to_year = json_decode($res_input['student_profile'][0]['technical_experience_to_year']);

                            if ($designation != "") {$desig = $designation[0];} else { $desig = '';}
                            if ($designation_description != "") {$desig_des = $designation_description[0];} else { $desig_des = '';}
                            if ($university_since_year != "") {$uni_since_year = $university_since_year[0];} else { $uni_since_year = '';}
                            if ($university_to_year != "") {$uni_to_year = $university_to_year[0];} else { $uni_to_year = '';}
                            if ($university_name != "") {$uni_name = $university_name[0];} else { $uni_name = '';}
                            if ($university_location != "") {$uni_loc = $university_location[0];} else { $uni_loc = '';}
                            if ($event_description_of_work != "") {$event_desc_work = $event_description_of_work[0];} else { $event_desc_work = '';}
                            if ($event_experience_in_year != "") {$event_exp = $event_experience_in_year[0];} else { $event_exp = '';}
                            if ($technical_experience_in_year != "") {$tech_exp = $technical_experience_in_year[0];} else { $tech_exp = '';}
                            if ($technical_experience_to_year != "") {$tech_to = $technical_experience_to_year[0];} else { $tech_to = '';}
                            // Domain Array
                            $imArr = array();
                            if ($domainAreaExp != "") {

                                $exmplodeD = explode(",", $domainAreaExp);

                                foreach ($exmplodeD as $exId) {

                                    $domainNames = $this->master_model->getRecords("domain_master", array('id' => $exId));
                                    $dname       = $encrptopenssl->decrypt($domainNames[0]['domain_name']);
                                    array_push($imArr, $dname);
                                }
                                $impDomain = implode(", ", $imArr);
                            } else {
                                $impDomain = '';
                            }

                            // Skill Set Array
                            $skArr = array();
                            if ($skillSetArr != "") {

                                $exmplodeSkill = explode(",", $skillSetArr);

                                foreach ($exmplodeSkill as $exSk) {

                                    $skillNames = $this->master_model->getRecords("skill_sets", array('id' => $exSk));
                                    $sname      = $encrptopenssl->decrypt($skillNames[0]['name']);
                                    array_push($skArr, $sname);
                                }
                                $impSkill = implode(", ", $skArr);
                            } else {
                                $impSkill = '';
                            }

                            $org_sector             = '';
                            $org_sector_other       = '';
                            $overview               = '';
                            $spoc_bill_addr1        = '';
                            $spoc_bill_addr2        = '';
                            $spoc_bill_addr_city    = '';
                            $spoc_bill_addr_state   = '';
                            $spoc_bill_addr_country = '';
                            $spoc_bill_addr_pin     = '';
                            $self_declaration       = '';
                            $specialities_products  = '';
                            $establishment_year     = '';
                            $institution_size       = '';
                            $company_evaluation     = '';
                            $website                = '';
                            $linkedin_page          = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $org_sector);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);

                        } else if ($user_category_id == 2) {

                            $org_sector                 = $res_input['org_profile'][0]['org_sector'];
                            $org_sector_other           = $res_input['org_profile'][0]['org_sector_other'];
                            $overview                   = $encrptopenssl->decrypt($res_input['org_profile'][0]['overview']);
                            $spoc_bill_addr1            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr1']);
                            $spoc_bill_addr2            = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr2']);
                            $spoc_bill_addr_city        = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_city']);
                            $spoc_bill_addr_state       = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_state']);
                            $spoc_bill_addr_country     = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_country']);
                            $spoc_bill_addr_pin         = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_bill_addr_pin']);
                            $spoc_sec_num_country_code1 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code1']);
                            $spoc_sec_num1              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num1']);
                            $spoc_sec_num_country_code2 = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num_country_code2']);
                            $spoc_sec_num2              = $encrptopenssl->decrypt($res_input['org_profile'][0]['spoc_sec_num2']);
                            $self_declaration           = $encrptopenssl->decrypt($res_input['org_profile'][0]['self_declaration']);
                            $specialities_products      = $encrptopenssl->decrypt($res_input['org_profile'][0]['specialities_products']);
                            $establishment_year         = $encrptopenssl->decrypt($res_input['org_profile'][0]['establishment_year']);
                            $institution_size           = $encrptopenssl->decrypt($res_input['org_profile'][0]['institution_size']);
                            $company_evaluation         = $encrptopenssl->decrypt($res_input['org_profile'][0]['company_evaluation']);
                            $website                    = $encrptopenssl->decrypt($res_input['org_profile'][0]['website']);
                            $linkedin_page              = $encrptopenssl->decrypt($res_input['org_profile'][0]['linkedin_page']);

                            $employmentStatus    = '';
                            $other_emp_status    = '';
                            $other_domian        = '';
                            $company_name        = '';
                            $desig               = '';
                            $desig_des           = '';
                            $years_of_experience = '';
                            $impDomain           = '';
                            $impSkill            = '';
                            $uni_since_year      = '';
                            $uni_to_year         = '';
                            $uni_name            = '';
                            $uni_loc             = '';
                            $event_desc_work     = '';
                            $event_exp           = '';
                            $tech_exp            = '';
                            $tech_to             = '';

                            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $user_id);
                            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $email);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $countryCode);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $user_category);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $sub_catname);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $membership_type);
                            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $institution_full_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $pay_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $employmentStatus);
                            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $other_emp_status);
                            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $other_domian);
                            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $company_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $desig);
                            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $desig_des);
                            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $years_of_experience);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $impDomain);
                            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $impSkill);
                            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $uni_since_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $uni_to_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $uni_name);
                            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $uni_loc);
                            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $event_desc_work);
                            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $event_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $tech_exp);
                            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $tech_to);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $org_sector);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $org_sector_other);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $overview);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $spoc_bill_addr1);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $spoc_bill_addr2);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $spoc_bill_addr_city);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $spoc_bill_addr_state);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $spoc_bill_addr_country);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $spoc_bill_addr_pin);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $self_declaration);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $specialities_products);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $establishment_year);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $institution_size);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $company_evaluation);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $website);
                            $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $linkedin_page);

                        }

                    }
                    $rowCount++;

                } // For loop end

            } // Count Check

            //die();

            // create file name
            //$fileName = 'users-'.time().'.xlsx';
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="users-' . time() . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        //$data['records'] = $res_arr;
        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/test';
        $this->load->view('admin/admin_combo', $data);
    }

    public function get_user_data()
    {

        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $response = array();

        $columns = array(
            0 => 'user_id',
            1 => 'user_id',
            2 => 'first_name_decrypt',
            3 => 'email',
            4 => 'mobile',
            5 => 'user_category_id',
            6 => 'user_sub_category_id',
            7 => 'registration.xOrder',
        );
        ## Read value
        $draw            = @$this->input->post('draw');
        $start           = @$this->input->post('start');
        $rowperpage      = @$this->input->post('length'); // Rows display per page
        $columnIndex     = @$this->input->post('order')[0]['column']; // Column index
        $columnName      = @$this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = @$this->input->post('order')[0]['dir']; // asc or desc
        $searchValue     = @$this->input->post('search')['value']; // Search value

        $sort_column = $columns[$_POST['order'][0]['column']];
        $sort_order  = $_POST['order'][0]['dir'];
        

        $keyword      = @$this->input->post('keyword') ? $this->input->post('keyword') : '';
        $profile_type = @$this->input->post('profile_type') ? $this->input->post('profile_type') : '';
        //$frm_date         = @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
        //$office_subtype    = @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';

        ## Search
        $search_arr  = array();
        $searchQuery = "";
        if ($keyword != '') {
            $string       = $encrptopenssl->encrypt($keyword);
            $search_arr[] = " (arai_registration.first_name like '%" . $string . "%' or
								arai_registration.middle_name like '%" . $string . "%' or
								arai_registration.last_name like '%" . $string . "%' or
								 arai_registration.email like '%" . $string . "%' or
								 arai_registration.mobile like '%" . $string . "%' or arai_registration.json_str like '%" . $keyword . "%') ";

        }

        if ($profile_type != '') {
            $search_arr[] = " arai_registration.user_category_id='" . $profile_type . "' ";
        }

        /*if($searchCity != ''){
        $search_arr[] = " city='".$searchCity."' ";
        }
        if($searchGender != ''){
        $search_arr[] = " gender='".$searchGender."' ";
        }
        if($searchName != ''){
        $search_arr[] = " name like '%".$searchName."%' ";
        }
        if(count($search_arr) > 0){
        $searchQuery = implode(" and ",$search_arr);
        }*/

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $records = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));

        $totalRecords = count($records);

        ## Total number of record with filtering
        $totalRecordwithFilter = count($records);
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
            $recordsFilter         = $this->master_model->getRecords("registration", array('is_valid' => '1', 'is_deleted' => '0'));
            $totalRecordwithFilter = count($recordsFilter);
        }

        ## Fetch records
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
        }
        if ($sort_column != '' && $sort_order != '') {
            $this->db->order_by($sort_column, $sort_order);
        }

        $this->db->select('registration.user_id,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,registration.priority,
			registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
			registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
			country.phonecode,student_profile.type,student_profile.university_course,student_profile.university_since_year,student_profile.university_to_year,
			student_profile.university_name,student_profile.university_location,student_profile.current_study_course,student_profile.current_study_since_year,
			student_profile.current_study_to_year,student_profile.current_study_description,student_profile.domain_and_area_of_training_and_study,student_profile.skill_sets,
			student_profile.event_experience_in_year,student_profile.event_description_of_work,student_profile.technical_experience_in_year,student_profile.technical_description_of_work,
			student_profile.pincode,student_profile.flat_house_building_apt_company,student_profile.area_colony_street_village,student_profile.town_city_and_state,
			student_profile.country,registration_usercategory.user_category,registration_usersubcategory.sub_catname,registration_usercategory.id AS cat_id');
        $this->db->join('country', 'registration.country_code=country.id', 'left');
        $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
        $records = $this->master_model->getRecords("registration", array('registration.is_valid' => '1', 'registration.is_deleted' => '0'), '', array('registration.user_id' => 'DESC'), $start, $rowperpage);
        //echo $this->db->last_query();    die();
        //echo "<pre>";print_r($records);die();

        $data = array();
        $i    = 0;
        foreach ($records as $row_val) {

            $i++;
            $row_val['user_id']               = $row_val['user_id'];
            $row_val['membership_type']       = $encrptopenssl->decrypt($row_val['membership_type']);
            $row_val['title']                 = $encrptopenssl->decrypt($row_val['title']);
            $row_val['first_name']            = $encrptopenssl->decrypt($row_val['first_name']);
            $row_val['middle_name']           = $encrptopenssl->decrypt($row_val['middle_name']);
            $row_val['last_name']             = $encrptopenssl->decrypt($row_val['last_name']);
            $row_val['email']                 = $encrptopenssl->decrypt($row_val['email']);
            $row_val['phonecode']             = $row_val['phonecode'];
            $row_val['mobile']                = $encrptopenssl->decrypt($row_val['mobile']);
            $row_val['xOrder']                = $row_val['xOrder'];
            $row_val['priority']              = $row_val['priority'];
            $row_val['user_category']         = $encrptopenssl->decrypt($row_val['user_category']);
            $row_val['sub_catname']           = $encrptopenssl->decrypt($row_val['sub_catname']);
            $row_val['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);
            $fullname1                        = ucfirst($row_val['title']) . " " . ucfirst($row_val['first_name']) . " " . ucfirst($row_val['middle_name']) . " " . ucfirst($row_val['last_name']);
            $countryCode                      = $row_val['phonecode'] . "-" . $row_val['mobile'];

            if ($row_val['cat_id'] == 1) {
                $file     = 'viewDetails';
                $fullname = $fullname1;

            } else {
                $file     = 'organizationDetails';
                $fullname = ucwords($row_val['institution_full_name']);
            }

            /*if($row_val['is_featured'] == 'Y'){
            $textShow = "Featured";
            } else {
            $textShow = "Not-Featured";
            }*/

            $textFeatureBox = "";
            if ($row_val['user_sub_category_id'] == 11) {

                $selValues                                    = '';
                $selValues2                                   = '';
                $selValues3                                   = '';
                if ($row_val['priority'] == '1'): $selValues  = "selected='selected'";endif;
                if ($row_val['priority'] == '2'): $selValues2 = "selected='selected'";endif;
                if ($row_val['priority'] == '3'): $selValues3 = "selected='selected'";endif;
                $textFeatureBox                               = " <input type='hidden' name='uid' id='uid' value='" . $row_val['user_id'] . "' />
										<select name='user_priority' id='user_priority' data-id='" . $row_val['user_id'] . "' class='user-priority'>
											<option value=''>-- Select --</option>
											<option value='1' " . $selValues . ">High</option>
											<option value='2' " . $selValues2 . ">Medium</option>
											<option value='3' " . $selValues3 . ">Low</option>
										</select>";

            }

            if ($row_val['is_featured'] == 'Y') {
                $textShow = "Featured";

            } else {
                $textShow = "Not-Featured";
            }

            if ($row_val['admin_approval'] == 'yes') {
                $ApprovalStr = "Approved";
            } else {
                $ApprovalStr = "Not-Approved";
            }

            if ($row_val['is_deleted'] == 1) {
                $userShow = "Restore";
            } else {
                $userShow = "Delete";
            }
            $xOrder = "";
            if ($row_val['user_sub_category_id'] == 11 && $row_val['is_featured'] == 'Y') {
                $xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='" . $row_val['user_id'] . "' name='order_val' id='order_val' value='" . $row_val['xOrder'] . "'
						onblur='update_sort_order(this.value, " . $row_val['user_id'] . ")' onkeyup='update_sort_order(this.value, " . $row_val['user_id'] . ")' />";
            }

            $approval = "<a href='javascript:void(0)' data-id='" . $row_val['user_id'] . "' class='approval-check btn btn-info btn-sm'  id='change-appr-" . $row_val['user_id'] . "'>" . $ApprovalStr . "</a>";

            $view = "<a href='" . base_url('xAdmin/users/' . $file . '/' . base64_encode($row_val['user_id'])) . "'><button class='btn btn-primary btn-sm'> View</button></a>";

            if ($row_val['status'] == 'Active'): $userStatus = "Block";else:$userStatus = "Active";endif;

            $modifiedStatus = "<a href='javascript:void(0)' data-id='" . $row_val['user_id'] . "' class='status-check btn btn-warning btn-sm' id='change-val-" . $row_val['user_id'] . "'>
									" . $userStatus . "
									</a>";
            $featuredUser = "";
            if ($row_val['cat_id'] == 1) {

                $featuredUser = "<a href='javascript:void(0)' data-id='" . $row_val['user_id'] . "' class='featured-check btn btn-info btn-sm'  id='change-stac-" . $row_val['user_id'] . "'>
								" . $textShow . "
								</a>";

            }

            $deleteUser = "<a href='javascript:void(0)' data-id='" . $row_val['user_id'] . "' class='deleted-check btn btn-success btn-sm'  id='deleted-stac-" . $row_val['user_id'] . "'>
								" . $userShow . "
								</a>";
            $showButtons = $view . " " . $modifiedStatus . " " . $featuredUser . " " . $deleteUser;

            $data[] = array(
                $i,
                $row_val['user_id'],
                $fullname,
                $row_val['email'],
                $countryCode,
                $row_val['user_category'],
                $row_val['sub_catname'],
                $xOrder,
                $textFeatureBox,
                $approval,
                $showButtons,
            );

        } // Foreach End

        $csrf_test_name = $this->security->get_csrf_hash();
        ## Response
        $response = array(
            "draw"                 => intval($draw),
            "iTotalRecords"        => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData"               => $data,
            "token"                => $csrf_test_name,
        );

        echo json_encode($response);

    } // Function End

    public function trashListing()
    {

        $encrptopenssl = new Opensslencryptdecrypt();
        $this->db->select('registration.user_id,registration.membership_type,registration.title,registration.first_name,registration.user_sub_category_id,registration.is_deleted,
			registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
			registration.mobile,registration.are_you_student,registration.is_verified,registration.status,registration.is_featured,registration.admin_approval,registration.is_featured,registration.xOrder,
			student_profile.type,student_profile.university_course,student_profile.university_since_year,student_profile.university_to_year,
			student_profile.university_name,student_profile.university_location,student_profile.current_study_course,student_profile.current_study_since_year,
			student_profile.current_study_to_year,student_profile.current_study_description,student_profile.domain_and_area_of_training_and_study,student_profile.skill_sets,
			student_profile.event_experience_in_year,student_profile.event_description_of_work,student_profile.technical_experience_in_year,student_profile.technical_description_of_work,
			student_profile.pincode,student_profile.flat_house_building_apt_company,student_profile.area_colony_street_village,student_profile.town_city_and_state,
			student_profile.country,registration_usercategory.user_category,registration_usersubcategory.sub_catname,registration_usercategory.id AS cat_id');
        $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
        //$response_data = $this->master_model->getRecords("registration", array("registration.user_category_id" => '1'));
        //$response_data = $this->master_model->getRecords("registration",array('is_valid'=>'1','is_deleted'=>'0'),'',array('user_id','desc'));
        $response_data = $this->master_model->getRecords("registration", array('is_valid' => '0', 'is_deleted' => '1'), '', array('user_id', 'desc'));
        //echo $this->db->last_query();die();
        $res_arr = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['user_id']                               = $row_val['user_id'];
                $row_val['membership_type']                       = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['title']                                 = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']                            = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']                           = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']                             = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']                                 = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']                                = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['country_code']                          = $row_val['country_code'];
                $row_val['are_you_student']                       = $row_val['are_you_student'];
                $row_val['university_course']                     = $encrptopenssl->decrypt($row_val['university_course']);
                $row_val['university_since_year']                 = $encrptopenssl->decrypt($row_val['university_since_year']);
                $row_val['university_to_year']                    = $encrptopenssl->decrypt($row_val['university_to_year']);
                $row_val['university_name']                       = $encrptopenssl->decrypt($row_val['university_name']);
                $row_val['university_location']                   = $encrptopenssl->decrypt($row_val['university_location']);
                $row_val['current_study_course']                  = $encrptopenssl->decrypt($row_val['current_study_course']);
                $row_val['current_study_since_year']              = $encrptopenssl->decrypt($row_val['current_study_since_year']);
                $row_val['current_study_to_year']                 = $encrptopenssl->decrypt($row_val['current_study_to_year']);
                $row_val['current_study_description']             = $encrptopenssl->decrypt($row_val['current_study_description']);
                $row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
                $row_val['skill_sets']                            = $encrptopenssl->decrypt($row_val['skill_sets']);
                $row_val['event_experience_in_year']              = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
                $row_val['event_description_of_work']             = $encrptopenssl->decrypt($row_val['event_description_of_work']);
                $row_val['technical_experience_in_year']          = $encrptopenssl->decrypt($row_val['technical_experience_in_year']);
                $row_val['technical_description_of_work']         = $encrptopenssl->decrypt($row_val['technical_description_of_work']);
                $row_val['flat_house_building_apt_company']       = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']            = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']                   = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                               = $encrptopenssl->decrypt($row_val['country']);
                $row_val['pincode']                               = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['user_category']                         = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']                           = $encrptopenssl->decrypt($row_val['sub_catname']);
                $row_val['institution_full_name']                 = $encrptopenssl->decrypt($row_val['institution_full_name']);
                $res_arr[]                                        = $row_val;
            }

        }
        //echo "<pre>";print_r($res_arr);die();
        $data['records']        = $res_arr;
        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/trash';
        $this->load->view('admin/admin_combo', $data);
    }

    public function invalid_records()
    {

        $encrptopenssl = new Opensslencryptdecrypt();
        $this->db->select('registration.user_id,registration.membership_type,registration.title,registration.first_name,
			registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.institution_full_name,
			registration.mobile,registration.are_you_student,registration.is_verified,registration.status,,registration.is_featured,registration.admin_approval,
			student_profile.type,student_profile.university_course,student_profile.university_since_year,student_profile.university_to_year,
			student_profile.university_name,student_profile.university_location,student_profile.current_study_course,student_profile.current_study_since_year,
			student_profile.current_study_to_year,student_profile.current_study_description,student_profile.domain_and_area_of_training_and_study,student_profile.skill_sets,
			student_profile.event_experience_in_year,student_profile.event_description_of_work,student_profile.technical_experience_in_year,student_profile.technical_description_of_work,
			student_profile.pincode,student_profile.flat_house_building_apt_company,student_profile.area_colony_street_village,student_profile.town_city_and_state,
			student_profile.country,registration_usercategory.user_category,registration_usersubcategory.sub_catname,registration_usercategory.id AS cat_id');
        $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
        //$response_data = $this->master_model->getRecords("registration", array("registration.user_category_id" => '1'));
        $response_data = $this->master_model->getRecords("registration", array('user_category_id' => '2', 'is_valid' => '0'), '', array('user_id', 'desc'));
        //echo $this->db->last_query();die();
        $res_arr = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['user_id']                               = $row_val['user_id'];
                $row_val['membership_type']                       = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['title']                                 = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']                            = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']                           = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']                             = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']                                 = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']                                = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['country_code']                          = $row_val['country_code'];
                $row_val['are_you_student']                       = $row_val['are_you_student'];
                $row_val['university_course']                     = $encrptopenssl->decrypt($row_val['university_course']);
                $row_val['university_since_year']                 = $encrptopenssl->decrypt($row_val['university_since_year']);
                $row_val['university_to_year']                    = $encrptopenssl->decrypt($row_val['university_to_year']);
                $row_val['university_name']                       = $encrptopenssl->decrypt($row_val['university_name']);
                $row_val['university_location']                   = $encrptopenssl->decrypt($row_val['university_location']);
                $row_val['current_study_course']                  = $encrptopenssl->decrypt($row_val['current_study_course']);
                $row_val['current_study_since_year']              = $encrptopenssl->decrypt($row_val['current_study_since_year']);
                $row_val['current_study_to_year']                 = $encrptopenssl->decrypt($row_val['current_study_to_year']);
                $row_val['current_study_description']             = $encrptopenssl->decrypt($row_val['current_study_description']);
                $row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
                $row_val['skill_sets']                            = $encrptopenssl->decrypt($row_val['skill_sets']);
                $row_val['event_experience_in_year']              = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
                $row_val['event_description_of_work']             = $encrptopenssl->decrypt($row_val['event_description_of_work']);
                $row_val['technical_experience_in_year']          = $encrptopenssl->decrypt($row_val['technical_experience_in_year']);
                $row_val['technical_description_of_work']         = $encrptopenssl->decrypt($row_val['technical_description_of_work']);
                $row_val['flat_house_building_apt_company']       = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']            = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']                   = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                               = $encrptopenssl->decrypt($row_val['country']);
                $row_val['pincode']                               = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['user_category']                         = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']                           = $encrptopenssl->decrypt($row_val['sub_catname']);
                $row_val['institution_full_name']                 = $encrptopenssl->decrypt($row_val['institution_full_name']);
                $res_arr[]                                        = $row_val;
            }

        }
        //echo "<pre>";print_r($res_arr);die();
        $data['records']        = $res_arr;
        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function viewDetails($id)
    {

        $encrptopenssl = new Opensslencryptdecrypt();
        $decode_id     = base64_decode($id);

        $this->db->select('contactable,discoverable,registration.user_id,registration.membership_type,registration.title,registration.first_name,
			registration.middle_name,registration.last_name,registration.email,registration.country_code,registration.user_sub_category_id,
			registration.mobile,registration.are_you_student,registration.valid_email,registration.valid_mobile,registration.is_verified,registration.status,student_profile.aplied_for_expert,
			student_profile.specify_fields_area_that_you_would_like,student_profile.bio_data,student_profile.years_of_experience,
			student_profile.no_of_paper_publication,student_profile.paper_year,student_profile.paper_conf_name,student_profile.paper_title,
			student_profile.no_of_patents,student_profile.patent_year,student_profile.patent_number,student_profile.patent_title,student_profile.portal_name,
			student_profile.portal_link,student_profile.portal_description,student_profile.profile_picture,student_profile.previous_user_sub_cat_id,
			student_profile.type,student_profile.university_course,student_profile.university_since_year,student_profile.university_to_year,
			student_profile.university_name,student_profile.university_location,student_profile.current_study_course,student_profile.current_study_since_year,
			student_profile.current_study_to_year,student_profile.current_study_description,student_profile.domain_and_area_of_training_and_study,student_profile.skill_sets,
			student_profile.event_experience_in_year,student_profile.event_description_of_work,student_profile.technical_experience_in_year,student_profile.technical_description_of_work,
			student_profile.pincode,student_profile.flat_house_building_apt_company,student_profile.area_colony_street_village,student_profile.town_city_and_state,
			student_profile.country,registration_usercategory.user_category,registration_usersubcategory.sub_catname,registration.is_featured,student_profile.portal_name_multiple,student_profile.portal_link_multiple,student_profile.portal_description_multiple');

        $this->db->join('student_profile', 'registration.user_id=student_profile.user_id', 'left');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id', 'left');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id', 'left');
        $response_data = $this->master_model->getRecords("registration", array("registration.user_id" => $decode_id));
        //echo $this->db->last_query();die();
        $res_arr = array();
        if (count($response_data)) {

            $row_val['user_id']                               = $decode_id;
            $row_val['valid_email']                           = $response_data[0]['valid_email'];
            $row_val['valid_mobile']                          = $response_data[0]['valid_mobile'];
            $row_val['user_sub_category_id']                  = $response_data[0]['user_sub_category_id'];
            $row_val['membership_type']                       = $encrptopenssl->decrypt($response_data[0]['membership_type']);
            $row_val['title']                                 = $encrptopenssl->decrypt($response_data[0]['title']);
            $row_val['first_name']                            = $encrptopenssl->decrypt($response_data[0]['first_name']);
            $row_val['middle_name']                           = $encrptopenssl->decrypt($response_data[0]['middle_name']);
            $row_val['last_name']                             = $encrptopenssl->decrypt($response_data[0]['last_name']);
            $row_val['email']                                 = $encrptopenssl->decrypt($response_data[0]['email']);
            $row_val['mobile']                                = $encrptopenssl->decrypt($response_data[0]['mobile']);
            $row_val['country_code']                          = $response_data[0]['country_code'];
            $row_val['are_you_student']                       = $response_data[0]['are_you_student'];
            $row_val['university_course']                     = $encrptopenssl->decrypt($response_data[0]['university_course']);
            $row_val['university_since_year']                 = $encrptopenssl->decrypt($response_data[0]['university_since_year']);
            $row_val['university_to_year']                    = $encrptopenssl->decrypt($response_data[0]['university_to_year']);
            $row_val['university_name']                       = $encrptopenssl->decrypt($response_data[0]['university_name']);
            $row_val['university_location']                   = $encrptopenssl->decrypt($response_data[0]['university_location']);
            $row_val['current_study_course']                  = $encrptopenssl->decrypt($response_data[0]['current_study_course']);
            $row_val['current_study_since_year']              = $encrptopenssl->decrypt($response_data[0]['current_study_since_year']);
            $row_val['current_study_to_year']                 = $encrptopenssl->decrypt($response_data[0]['current_study_to_year']);
            $row_val['current_study_description']             = $encrptopenssl->decrypt($response_data[0]['current_study_description']);
            $row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($response_data[0]['domain_and_area_of_training_and_study']);
            $row_val['skill_sets']                            = $encrptopenssl->decrypt($response_data[0]['skill_sets']);
            $row_val['event_experience_in_year']              = $encrptopenssl->decrypt($response_data[0]['event_experience_in_year']);
            $row_val['event_description_of_work']             = $encrptopenssl->decrypt($response_data[0]['event_description_of_work']);
            $row_val['technical_experience_in_year']          = $encrptopenssl->decrypt($response_data[0]['technical_experience_in_year']);
            $row_val['technical_description_of_work']         = $encrptopenssl->decrypt($response_data[0]['technical_description_of_work']);
            $row_val['flat_house_building_apt_company']       = $encrptopenssl->decrypt($response_data[0]['flat_house_building_apt_company']);
            $row_val['area_colony_street_village']            = $encrptopenssl->decrypt($response_data[0]['area_colony_street_village']);
            $row_val['town_city_and_state']                   = $encrptopenssl->decrypt($response_data[0]['town_city_and_state']);
            $row_val['country']                               = $encrptopenssl->decrypt($response_data[0]['country']);
            $row_val['pincode']                               = $encrptopenssl->decrypt($response_data[0]['pincode']);
            $row_val['user_category']                         = $encrptopenssl->decrypt($response_data[0]['user_category']);
            $row_val['sub_catname']                           = $encrptopenssl->decrypt($response_data[0]['sub_catname']);
            // Expert Details
            $row_val['specify_fields_area_that_you_would_like'] = $encrptopenssl->decrypt($response_data[0]['specify_fields_area_that_you_would_like']);
            $row_val['bio_data']                                = $encrptopenssl->decrypt($response_data[0]['bio_data']);
            $row_val['years_of_experience']                     = $encrptopenssl->decrypt($response_data[0]['years_of_experience']);
            $row_val['no_of_paper_publication']                 = $encrptopenssl->decrypt($response_data[0]['no_of_paper_publication']);
            $row_val['paper_year']                              = $encrptopenssl->decrypt($response_data[0]['paper_year']);
            $row_val['paper_conf_name']                         = $encrptopenssl->decrypt($response_data[0]['paper_conf_name']);
            $row_val['paper_title']                             = $encrptopenssl->decrypt($response_data[0]['paper_title']);
            $row_val['no_of_patents']                           = $encrptopenssl->decrypt($response_data[0]['no_of_patents']);
            $row_val['sub_catname']                             = $encrptopenssl->decrypt($response_data[0]['sub_catname']);
            $row_val['patent_year']                             = $encrptopenssl->decrypt($response_data[0]['patent_year']);
            $row_val['patent_number']                           = $encrptopenssl->decrypt($response_data[0]['patent_number']);
            $row_val['patent_title']                            = $encrptopenssl->decrypt($response_data[0]['patent_title']);
            $row_val['portal_name']                             = $encrptopenssl->decrypt($response_data[0]['portal_name']);
            $row_val['portal_link']                             = $encrptopenssl->decrypt($response_data[0]['portal_link']);
            $row_val['portal_description']                      = $encrptopenssl->decrypt($response_data[0]['portal_description']);
            $row_val['profile_picture']                         = $response_data[0]['profile_picture'];
            $row_val['previous_user_sub_cat_id']                = $response_data[0]['previous_user_sub_cat_id'];
            $row_val['aplied_for_expert']                       = $response_data[0]['aplied_for_expert'];
            $row_val['contactable']                             = $response_data[0]['contactable'];
            $row_val['discoverable']                            = $response_data[0]['discoverable'];

            $res_arr[] = $row_val;

        }
        //echo "<pre>";print_r($res_arr);die();
        $expert_data = $this->master_model->getRecords("student_profile", array('aplied_for_expert' => 'yes', 'user_id' => $decode_id));

        $expert_arr = array();
        if (count($expert_data)) {
            $row_val['user_id']                                 = $expert_data[0]['user_id'];
            $row_val['specify_fields_area_that_you_would_like'] = json_decode($expert_data[0]['specify_fields_area_that_you_would_like']);
            $row_val['bio_data']                                = $encrptopenssl->decrypt($expert_data[0]['bio_data']);
            $row_val['years_of_experience']                     = $encrptopenssl->decrypt($expert_data[0]['years_of_experience']);
            $row_val['no_of_paper_publication']                 = $expert_data[0]['no_of_paper_publication'];
            $row_val['paper_year']                              = json_decode($expert_data[0]['paper_year']);
            $row_val['paper_conf_name']                         = json_decode($expert_data[0]['paper_conf_name']);
            $row_val['paper_title']                             = json_decode($expert_data[0]['paper_title']);
            $row_val['no_of_patents']                           = $expert_data[0]['no_of_patents'];
            $row_val['patent_year']                             = json_decode($expert_data[0]['patent_year']);
            $row_val['patent_number']                           = json_decode($expert_data[0]['patent_number']);
            $row_val['patent_title']                            = json_decode($expert_data[0]['patent_title']);
            $row_val['portal_name']                             = $expert_data[0]['portal_name'];
            $row_val['portal_link']                             = $encrptopenssl->decrypt($expert_data[0]['portal_link']);
            $row_val['portal_description']                      = $encrptopenssl->decrypt($expert_data[0]['portal_description']);
            $row_val['aplied_for_expert']                       = $expert_data[0]['aplied_for_expert'];
            $row_val['expert_claim_admin_approval']             = $expert_data[0]['expert_claim_admin_approval'];
            $row_val['claim_expert_reason']                     = $expert_data[0]['claim_expert_reason'];

            $row_val['portal_name_multiple'] = json_decode($expert_data[0]['portal_name_multiple']);
            if ($row_val['portal_name'] != '') {
                array_unshift($row_val['portal_name_multiple'], $row_val['portal_name']);
            }

            $row_val['portal_link_multiple'] = json_decode($expert_data[0]['portal_link_multiple']);
            if ($row_val['portal_link'] != '') {
                array_unshift($row_val['portal_link_multiple'], $row_val['portal_link']);
            }

            $row_val['portal_description_multiple'] = json_decode($expert_data[0]['portal_description_multiple']);
            if ($row_val['portal_description'] != '') {
                array_unshift($row_val['portal_description_multiple'], $row_val['portal_description']);
            }

            $expert_arr[] = $row_val;
        }

        $data['expert_data']    = $expert_arr;
        $data['user_data']      = $res_arr;
        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/details';
        $this->load->view('admin/admin_combo', $data);

    }

    public function organizationDetails($id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $decode_id     = base64_decode($id);

        $this->db->select('r.user_id,r.contactable,r.discoverable,r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, cc.iso, cc.phonecode, r.mobile, r.membership_type, r.institution_type, r.institution_full_name, r.domain_industry,r.valid_email,r.valid_mobile, r.public_prvt,r.other_domain_industry,r.other_public_prvt,
		(SELECT GROUP_CONCAT(institution_name) FROM arai_institution_master WHERE FIND_IN_SET(id, r.institution_type)) AS institution_name,
		r.institution_type,
		(SELECT GROUP_CONCAT(domain_name) FROM arai_domain_industry_master WHERE FIND_IN_SET(id, r.domain_industry) AND status = "Active") AS domain_name,
		r.domain_industry, pm.public_status_name, c.user_category, sc.sub_catname');
        $this->db->join('arai_country cc', 'cc.id = r.country_code', 'left', false);
        $this->db->join('arai_registration_usercategory c', 'c.id = r.user_category_id', 'left', false);
        $this->db->join('arai_registration_usersubcategory sc', 'sc.subcat_id = r.user_sub_category_id', 'left', false);
        //$this->db->join('arai_institution_master im','im.id = r.institution_type','',FALSE);
        //$this->db->join('arai_domain_industry_master dm','dm.id = r.domain_industry','',FALSE);
        $this->db->join('arai_public_status_master pm', 'pm.id = r.public_prvt', 'left', false);
        $user_data = $this->master_model->getRecords("arai_registration r", array('r.user_id' => $decode_id));
        //echo $this->db->last_query(); exit;

        $personal_info = array();
        if (!empty($user_data)) {
            //********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************
            $title       = $encrptopenssl->decrypt($user_data[0]['title']);
            $first_name  = $encrptopenssl->decrypt($user_data[0]['first_name']);
            $middle_name = $encrptopenssl->decrypt($user_data[0]['middle_name']);
            $last_name   = $encrptopenssl->decrypt($user_data[0]['last_name']);

            $spoc_name = $title . " " . $first_name;
            if ($middle_name != '') {$spoc_name .= " " . $middle_name . " ";}
            $spoc_name .= $last_name;

            $disp_institution_name = '';
            if ($user_data[0]['institution_name'] != "") {
                $institution_name_arr = explode(",", $user_data[0]['institution_name']);
                foreach ($institution_name_arr as $ins_res) {
                    $disp_institution_name .= $encrptopenssl->decrypt($ins_res) . ", ";
                }
            }

            if ($user_data[0]['institution_type'] != "" && strpos($user_data[0]['institution_type'], 'Other')) {
                $disp_institution_name .= "Other";
            }

            $disp_domain_name = '';
            if ($user_data[0]['domain_name'] != "") {
                $domain_name_arr = explode(",", $user_data[0]['domain_name']);
                foreach ($domain_name_arr as $domain_res) {
                    $disp_domain_name .= $encrptopenssl->decrypt($domain_res) . ", ";
                }
            }

            if ($user_data[0]['domain_industry'] != "" && strpos($user_data[0]['domain_industry'], 'Other')) {
                $disp_domain_name .= "Other";
            }

            $personal_info['spoc_name']             = $spoc_name;
            $personal_info['gender']                = $encrptopenssl->decrypt($user_data[0]['gender']);
            $personal_info['email']                 = $encrptopenssl->decrypt($user_data[0]['email']);
            $personal_info['iso']                   = $user_data[0]['iso'];
            $personal_info['phonecode']             = $user_data[0]['phonecode'];
            $personal_info['mobile']                = $encrptopenssl->decrypt($user_data[0]['mobile']);
            $personal_info['membership_type']       = $encrptopenssl->decrypt($user_data[0]['membership_type']);
            $personal_info['institution_name']      = rtrim($disp_institution_name, ", ");
            $personal_info['institution_full_name'] = $encrptopenssl->decrypt($user_data[0]['institution_full_name']);
            $personal_info['domain_name']           = rtrim($disp_domain_name, ", ");
            $personal_info['public_status_name']    = $encrptopenssl->decrypt($user_data[0]['public_status_name']);
            $personal_info['user_category']         = $encrptopenssl->decrypt($user_data[0]['user_category']);
            $personal_info['sub_catname']           = $encrptopenssl->decrypt($user_data[0]['sub_catname']);
            $personal_info['domain_industry']       = $user_data[0]['domain_industry'];
            $personal_info['public_prvt']           = $user_data[0]['public_prvt'];
            $personal_info['other_domain_industry'] = $user_data[0]['other_domain_industry'];
            $personal_info['other_public_prvt']     = $user_data[0]['other_public_prvt'];
            $personal_info['valid_email']           = $user_data[0]['valid_email'];
            $personal_info['valid_mobile']          = $user_data[0]['valid_mobile'];
            $personal_info['contactable']           = $user_data[0]['contactable'];
            $personal_info['discoverable']          = $user_data[0]['discoverable'];
            $personal_info['user_id']               = $user_data[0]['user_id'];

        }

        $this->db->select('o.*, c1.iso, c1.phonecode, c2.iso AS iso2, c2.phonecode AS phonecode2, (SELECT GROUP_CONCAT(name SEPARATOR ", ") FROM arai_organization_sector WHERE FIND_IN_SET(id, o.org_sector) AND status = "Active") AS org_sector_name');
        $this->db->join("arai_country c1", "c1.id = o.spoc_sec_num_country_code1", "LEFT", false);
        $this->db->join("arai_country c2", "c2.id = o.spoc_sec_num_country_code2", "LEFT", false);
        $org_profile_data = $this->master_model->getRecords('profile_organization o', array("o.user_id" => $decode_id));

        $profile_info = $bod_info = array();
        if (!empty($org_profile_data)) {
            //********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************

            $profile_info['org_sector_name']             = $org_profile_data[0]['org_sector_name'];
            $profile_info['org_sector_other']            = $encrptopenssl->decrypt($org_profile_data[0]['org_sector_other']);
            $profile_info['overview']                    = $encrptopenssl->decrypt($org_profile_data[0]['overview']);
            $profile_info['spoc_bill_addr1']             = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr1']);
            $profile_info['spoc_bill_addr2']             = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr2']);
            $profile_info['spoc_bill_addr_city']         = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_city']);
            $profile_info['spoc_bill_addr_state']        = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_state']);
            $profile_info['spoc_bill_addr_country']      = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_country']);
            $profile_info['spoc_bill_addr_pin']          = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_pin']);
            $profile_info['spoc_sec_num_country_code1']  = $org_profile_data[0]['iso'] . " " . $org_profile_data[0]['phonecode'];
            $profile_info['spoc_sec_num1']               = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num1']);
            $profile_info['spoc_sec_num_country_code2']  = $org_profile_data[0]['iso2'] . " " . $org_profile_data[0]['phonecode2'];
            $profile_info['spoc_sec_num2']               = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num2']);
            $profile_info['specialities_products']       = $encrptopenssl->decrypt($org_profile_data[0]['specialities_products']);
            $profile_info['establishment_year']          = $encrptopenssl->decrypt($org_profile_data[0]['establishment_year']);
            $profile_info['institution_size']            = $encrptopenssl->decrypt($org_profile_data[0]['institution_size']);
            $profile_info['company_evaluation']          = $encrptopenssl->decrypt($org_profile_data[0]['company_evaluation']);
            $profile_info['website']                     = $encrptopenssl->decrypt($org_profile_data[0]['website']);
            $profile_info['linkedin_page']               = $encrptopenssl->decrypt($org_profile_data[0]['linkedin_page']);
            $profile_info['org_logo']                    = $encrptopenssl->decrypt($org_profile_data[0]['org_logo']);
            $profile_info['pan_card']                    = $encrptopenssl->decrypt($org_profile_data[0]['pan_card']);
            $profile_info['institution_reg_certificate'] = $encrptopenssl->decrypt($org_profile_data[0]['institution_reg_certificate']);
            $profile_info['self_declaration']            = $encrptopenssl->decrypt($org_profile_data[0]['self_declaration']);
            $profile_info['address_proof']               = $encrptopenssl->decrypt($org_profile_data[0]['address_proof']);
            $profile_info['gst_reg_certificate']         = $encrptopenssl->decrypt($org_profile_data[0]['gst_reg_certificate']);

            $bod_data = $this->master_model->getRecords('organization_bod', array('org_profile_id' => $org_profile_data[0]['org_profile_id'], 'user_id' => $decode_id), '*');
            if (!empty($bod_data)) {
                foreach ($bod_data as $bod_key => $bod) {
                    $bod_info[$bod_key]['bod_id']          = $bod['bod_id'];
                    $bod_info[$bod_key]['org_profile_id']  = $bod['org_profile_id'];
                    $bod_info[$bod_key]['user_id']         = $bod['user_id'];
                    $bod_info[$bod_key]['bod_name']        = $encrptopenssl->decrypt($bod['bod_name']);
                    $bod_info[$bod_key]['bod_designation'] = $encrptopenssl->decrypt($bod['bod_designation']);
                    $bod_info[$bod_key]['bod_since']       = $encrptopenssl->decrypt($bod['bod_since']);
                }
            }
        }

        // echo "<pre>";print_r($res_arr);die();
        $data['personal_info'] = $personal_info;
        $data['profile_info']  = $profile_info;
        $data['bod_info']      = $bod_info;

        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/orgdetails';
        $this->load->view('admin/admin_combo', $data);
    }

    public function edit($id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $decode_id     = base64_decode($id);
        $this->load->library('upload');
        $this->db->join('eligibility_expectation', 'challenge.c_id=eligibility_expectation.c_id', 'left');
        $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
        $this->db->join('technology_master', 'challenge.technology_id=technology_master.id', 'left');
        $this->db->join('tags', 'challenge.tags_id=tags.id', 'left');
        $this->db->join('audience_pref', 'challenge.audience_pref_id=audience_pref.id', 'left');
        $this->db->join('ip_clause', 'challenge.ip_clause=ip_clause.id', 'left');
        $response_data = $this->master_model->getRecords("challenge", array("challenge.c_id" => $decode_id));

        $res_arr = array();
        if (count($response_data) > 0) {

            $row_val['challenge_title']             = $encrptopenssl->decrypt($response_data[0]['challenge_title']);
            $row_val['company_name']                = $encrptopenssl->decrypt($response_data[0]['company_name']);
            $row_val['company_profile']             = $encrptopenssl->decrypt($response_data[0]['company_profile']);
            $row_val['banner_img']                  = $encrptopenssl->decrypt($response_data[0]['banner_img']);
            $row_val['challenge_details']           = $encrptopenssl->decrypt($response_data[0]['challenge_details']);
            $row_val['challenge_abstract']          = $encrptopenssl->decrypt($response_data[0]['challenge_abstract']);
            $row_val['challenge_launch_date']       = $response_data[0]['challenge_launch_date'];
            $row_val['challenge_close_date']        = $response_data[0]['challenge_close_date'];
            $row_val['technology_name']             = $encrptopenssl->decrypt($response_data[0]['technology_name']);
            $row_val['tag_name']                    = $encrptopenssl->decrypt($response_data[0]['tag_name']);
            $row_val['preference_name']             = $encrptopenssl->decrypt($response_data[0]['preference_name']);
            $row_val['fund_reward']                 = $response_data[0]['fund_reward'];
            $row_val['fund_reward_amount']          = $encrptopenssl->decrypt($response_data[0]['fund_reward_amount']);
            $row_val['contact_person_name']         = $encrptopenssl->decrypt($response_data[0]['contact_person_name']);
            $row_val['challenge_visibility']        = $response_data[0]['challenge_visibility'];
            $row_val['future_opportunities']        = $encrptopenssl->decrypt($response_data[0]['future_opportunities']);
            $row_val['email_id']                    = $encrptopenssl->decrypt($response_data[0]['email_id']);
            $row_val['mobile_no']                   = $encrptopenssl->decrypt($response_data[0]['mobile_no']);
            $row_val['office_no']                   = $encrptopenssl->decrypt($response_data[0]['office_no']);
            $row_val['status']                      = $response_data[0]['status'];
            $row_val['ip_name']                     = $response_data[0]['ip_name'];
            $row_val['audience_pref_id']            = $response_data[0]['audience_pref_id'];
            $row_val['tags_id']                     = $response_data[0]['tags_id'];
            $row_val['technology_id']               = $response_data[0]['technology_id'];
            $row_val['is_publish']                  = $response_data[0]['is_publish'];
            $row_val['is_featured']                 = $response_data[0]['is_featured'];
            $row_val['is_external_funding']         = $response_data[0]['is_external_funding'];
            $row_val['external_fund_details']       = $encrptopenssl->decrypt($response_data[0]['external_fund_details']);
            $row_val['is_exclusive_challenge']      = $response_data[0]['is_exclusive_challenge'];
            $row_val['exclusive_challenge_details'] = $encrptopenssl->decrypt($response_data[0]['exclusive_challenge_details']);
            $res_arr[]                              = $row_val;

        }

        // Technology Master
        $technology_data = $this->master_model->getRecords("technology_master", array("status" => 'Active'));
        $res_tec         = array();
        if (count($technology_data)) {
            foreach ($technology_data as $row_val) {
                $row_val['technology_name'] = $encrptopenssl->decrypt($row_val['technology_name']);
                $row_val['id']              = $row_val['id'];
                $res_tec[]                  = $row_val;
            }
        }

        // Tags Master
        $tag_data = $this->master_model->getRecords("tags", array("status" => 'Active'));
        $res_tags = array();
        if (count($tag_data)) {
            foreach ($tag_data as $row_val) {
                $row_val['tag_name'] = $encrptopenssl->decrypt($row_val['tag_name']);
                $row_val['id']       = $row_val['id'];
                $res_tags[]          = $row_val;
            }
        }

        // Audience Preference Master
        $audi_data = $this->master_model->getRecords("audience_pref", array("status" => 'Active'));
        $res_audi  = array();
        if (count($audi_data)) {
            foreach ($audi_data as $row_val) {
                $row_val['preference_name'] = $encrptopenssl->decrypt($row_val['preference_name']);
                $row_val['id']              = $row_val['id'];
                $res_audi[]                 = $row_val;
            }
        }

        $data['technology_data'] = $res_tec;
        $data['tag_data']        = $res_tags;
        $data['audience_data']   = $res_audi;
        $data['response_data']   = $res_arr;

        // Check Validation
        $this->form_validation->set_rules('company_profile', 'Company Profile', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('launch_date', 'Launch Date', 'required|xss_clean');
        $this->form_validation->set_rules('close_date', 'Close Date', 'required|xss_clean');
        $this->form_validation->set_rules('techonogy_id', 'Technology Name', 'required|xss_clean');
        $this->form_validation->set_rules('tags_id', 'Tag Name', 'required|xss_clean');
        $this->form_validation->set_rules('audi_id[]', 'Audience Preference', 'required|xss_clean');
        $this->form_validation->set_rules('contact_person', 'Contact Person Name', 'required|xss_clean');
        $this->form_validation->set_rules('emailid', 'Contact Email ID', 'required|xss_clean');
        $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|min_length[10]|max_length[10]|xss_clean');
        $this->form_validation->set_rules('office_no', 'Office No', 'required|xss_clean');
        $this->form_validation->set_rules('challenge_visibility', 'Visibility', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $company_profile      = $encrptopenssl->encrypt($this->input->post('company_profile'));
            $launch_date          = $this->input->post('launch_date');
            $close_date           = $this->input->post('close_date');
            $techonogy_id         = $this->input->post('techonogy_id');
            $tags_id              = $this->input->post('tags_id');
            $audi_id              = $this->input->post('audi_id');
            $contact_person       = $encrptopenssl->encrypt($this->input->post('contact_person'));
            $emailid              = $encrptopenssl->encrypt($this->input->post('emailid'));
            $mobile               = $encrptopenssl->encrypt($this->input->post('mobile'));
            $office_no            = $encrptopenssl->encrypt($this->input->post('office_no'));
            $challenge_visibility = $this->input->post('challenge_visibility');
            $external_funding     = $this->input->post('external_funding');
            $funding_amt          = $encrptopenssl->encrypt($this->input->post('funding_amt'));
            $is_exclusive         = $this->input->post('is_exclusive');
            $challenge_ex_details = $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
            $updateAt             = date('Y-m-d H:i:s');
            $banner_img           = $_FILES['banner_img']['name'];

            $implodeArr = implode(",", $audi_id);

            if ($banner_img != "") {

                $config['upload_path']   = 'assets/challenge';
                $config['allowed_types'] = '*';
                $config['max_size']      = '5000';
                $config['encrypt_name']  = true;

                $upload_files = @$this->master_model->upload_file('banner_img', $_FILES, $config, false);
                $b_image      = "";
                if (isset($upload_files[0]) && !empty($upload_files[0])) {

                    $b_image = $upload_files[0];

                }

                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");

                $ext = pathinfo($b_image[0], PATHINFO_EXTENSION);

                if (!array_key_exists($ext, $allowed)) {

                    $data['imageError'] = "Upload banner in .jpg, .jpeg, .png format only";
                    $this->session->set_flashdata('error', $data['imageError']);
                    redirect(base_url('xAdmin/challenge'));
                }

                $filesize = $_FILES['banner_img']['size'];
                // Verify file size - 5MB maximum
                $maxsize = 5 * 1024 * 1024;

                /*if($filesize > $maxsize)
            {
            $data['imageError'] = "File size is larger than the allowed limit";
            $this->session->set_flashdata('error',$data['imageError']);
            redirect(base_url('xAdmin/setting'));
            }*/

            } // Banner Image End

            // Get Banner Image Name
            if ($banner_img != "") {
                $banner_names = $encrptopenssl->encrypt($b_image[0]);
                $updateArr    = array('banner_img' => $banner_names,
                    'company_profile'                  => $company_profile,
                    'challenge_launch_date'            => $launch_date,
                    'challenge_close_date'             => $close_date,
                    'technology_id'                    => $techonogy_id,
                    'tags_id'                          => $tags_id,
                    'audience_pref_id'                 => $implodeArr,
                    'contact_person_name'              => $contact_person,
                    'challenge_visibility'             => $challenge_visibility,
                    'is_external_funding'              => $external_funding,
                    'external_fund_details'            => $funding_amt,
                    'is_exclusive_challenge'           => $is_exclusive,
                    'exclusive_challenge_details'      => $challenge_ex_details,
                    'updatedAt'                        => $updateAt,
                    'updated_by_admin_id'              => '',

                );
            } else {

                $updateArr = array(
                    'company_profile'             => $company_profile,
                    'challenge_launch_date'       => $launch_date,
                    'challenge_close_date'        => $close_date,
                    'technology_id'               => $techonogy_id,
                    'tags_id'                     => $tags_id,
                    'audience_pref_id'            => $implodeArr,
                    'contact_person_name'         => $contact_person,
                    'challenge_visibility'        => $challenge_visibility,
                    'is_external_funding'         => $external_funding,
                    'external_fund_details'       => $funding_amt,
                    'is_exclusive_challenge'      => $is_exclusive,
                    'exclusive_challenge_details' => $challenge_ex_details,
                    'updatedAt'                   => $updateAt,
                    'updated_by_admin_id'         => '',

                );
            }

            $updateQuery = $this->master_model->updateRecord('challenge', $updateArr, array('c_id' => $decode_id));

            if ($updateQuery > 0) {

                $contactArr = array(
                    'email_id'  => $emailid,
                    'mobile_no' => $mobile,
                    'office_no' => $office_no,
                    'updatedAt' => $updateAt,
                );
                $this->master_model->updateRecord('challenge_contact', $contactArr, array('c_id' => $decode_id));

                $this->session->set_flashdata('success', 'Challenge successfully updated');
                redirect(base_url('xAdmin/challenge'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('xAdmin/challenge/edit/' . $id));
            }
        }

        $data['module_name']    = 'Challenge';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'challenge/edit';
        $this->load->view('admin/admin_combo', $data);
    }

    public function updateStatus()
    {
        $encrptopenssl  = new Opensslencryptdecrypt();
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $id));

        if ($user_data[0]['status'] == "Active") {
            $status = 'Block';
        } else {
            $status = 'Active';
        }

        $updateQuery = $this->master_model->updateRecord('registration', array('status' => $status), array('user_id' => $id));

        $user_next = $this->master_model->getRecords("registration", array("user_id" => $id));
        if ($user_next[0]['status'] == "Active") {
            $StoreStr = "Active";

            // Status Active
            $subscriber_mail    = $this->master_model->getRecords("email_template", array("id" => '52'));
            $setting_table_1    = $this->master_model->getRecords("setting", array("id" => '1'));
            $content_Subscriber = '';

            if (count($subscriber_mail) > 0) {

                $usersId         = $this->session->userdata('user_id');
                $senders_details = $this->master_model->getRecords("registration", array("user_id" => $usersId));
                //$receiver_email     = $encrptopenssl->decrypt($senders_details[0]['email']);
                $receiver_email = 'vicky.kuwar@esds.co.in';
                $subject_1      = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $description_1  = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $from_admin_1   = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $contact_no1    = $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);

                $arr_words = ['[USERID]', '[PHONENO]'];
                $rep_array = [$receiver_email, $contact_no1];

                $content_Subscriber = str_replace($arr_words, $rep_array, $description_1);

                $info_arr_c = array(
                    'to'      => $receiver_email,
                    'cc'      => '',
                    'from'    => $from_admin_1,
                    'subject' => $subject_1,
                    'view'    => 'common-file',
                );

                $other_info_c = array('content' => $content_Subscriber);

                $emailsend_1 = $this->emailsending->sendmail($info_arr_c, $other_info_c);

            }

        } else {
            $StoreStr = "Block";

            // Status Active
            $subscriber_mail    = $this->master_model->getRecords("email_template", array("id" => '53'));
            $setting_table_1    = $this->master_model->getRecords("setting", array("id" => '1'));
            $content_Subscriber = '';

            if (count($subscriber_mail) > 0) {

                $usersId         = $this->session->userdata('user_id');
                $senders_details = $this->master_model->getRecords("registration", array("user_id" => $usersId));
                //$receiver_email     = $encrptopenssl->decrypt($senders_details[0]['email']);
                $receiver_email = 'vicky.kuwar@esds.co.in';
                $subject_1      = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $description_1  = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $from_admin_1   = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $contact_no1    = $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);

                $arr_words = ['[USERID]', '[PHONENO]'];
                $rep_array = [$receiver_email, $contact_no1];

                $content_Subscriber = str_replace($arr_words, $rep_array, $description_1);

                $info_arr_c = array(
                    'to'      => $receiver_email,
                    'cc'      => '',
                    'from'    => $from_admin_1,
                    'subject' => $subject_1,
                    'view'    => 'common-file',
                );

                $other_info_c = array('content' => $content_Subscriber);

                $emailsend_1 = $this->emailsending->sendmail($info_arr_c, $other_info_c);

            }

        }

        $jsonData = array("u_status" => $StoreStr, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function changeApproval()
    {
        $encrptopenssl  = new Opensslencryptdecrypt();
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $id));

        if ($user_data[0]['admin_approval'] == "yes") {
            $status = 'no';
        } else {
            $status = 'yes';
        }

        $updateQuery = $this->master_model->updateRecord('registration', array('admin_approval' => $status), array('user_id' => $id));

        $user_next = $this->master_model->getRecords("registration", array("user_id" => $id));
        if ($user_next[0]['admin_approval'] == "yes") {
            $ApprovalStr = "Approved";

            // Status Active
            $subscriber_mail    = $this->master_model->getRecords("email_template", array("id" => '52'));
            $setting_table_1    = $this->master_model->getRecords("setting", array("id" => '1'));
            $content_Subscriber = '';

            if (count($subscriber_mail) > 0) {

                $usersId         = $this->session->userdata('user_id');
                $senders_details = $this->master_model->getRecords("registration", array("user_id" => $usersId));
                //$receiver_email     = $encrptopenssl->decrypt($senders_details[0]['email']);
                $receiver_email = 'vishal.p@esds.co.in';
                $subject_1      = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $description_1  = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $from_admin_1   = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $contact_no1    = $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);

                $arr_words = ['[USERID]', '[PHONENO]'];
                $rep_array = [$receiver_email, $contact_no1];

                $content_Subscriber = str_replace($arr_words, $rep_array, $description_1);

                $info_arr_c = array(
                    'to'      => $receiver_email,
                    'cc'      => '',
                    'from'    => $from_admin_1,
                    'subject' => $subject_1,
                    'view'    => 'common-file',
                );

                $other_info_c = array('content' => $content_Subscriber);

                $emailsend_1 = $this->emailsending->sendmail($info_arr_c, $other_info_c);

            }

        } else {
            $ApprovalStr = "Not-Approved";

            // Status Active
            $subscriber_mail    = $this->master_model->getRecords("email_template", array("id" => '53'));
            $setting_table_1    = $this->master_model->getRecords("setting", array("id" => '1'));
            $content_Subscriber = '';

            if (count($subscriber_mail) > 0) {

                $usersId         = $this->session->userdata('user_id');
                $senders_details = $this->master_model->getRecords("registration", array("user_id" => $usersId));
                //$receiver_email     = $encrptopenssl->decrypt($senders_details[0]['email']);
                $receiver_email = 'vishal.p@esds.co.in';
                $subject_1      = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $description_1  = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $from_admin_1   = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $contact_no1    = $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);

                $arr_words = ['[USERID]', '[PHONENO]'];
                $rep_array = [$receiver_email, $contact_no1];

                $content_Subscriber = str_replace($arr_words, $rep_array, $description_1);

                $info_arr_c = array(
                    'to'      => $receiver_email,
                    'cc'      => '',
                    'from'    => $from_admin_1,
                    'subject' => $subject_1,
                    'view'    => 'common-file',
                );

                $other_info_c = array('content' => $content_Subscriber);

                $emailsend_1 = $this->emailsending->sendmail($info_arr_c, $other_info_c);

            }

        }

        $jsonData = array("u_status" => $ApprovalStr, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function changeStatus()
    {

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $id));

        if ($user_data[0]['is_featured'] == "N") {
            $is_featured = 'Y';
        } else {
            $is_featured = 'N';
        }
        $updateQuery = $this->master_model->updateRecord('registration', array('is_featured' => $is_featured), array('user_id' => $id));

        $user_next = $this->master_model->getRecords("registration", array("user_id" => $id));
        if ($user_next[0]['is_featured'] == "N") {
            $text = "Not-Featured";
        } else {
            $text = "Featured";
        }

        $jsonData = array("u_featured" => $text, "token" => $csrf_test_name);
        echo json_encode($jsonData);

    }

    public function markexpert()
    {
        error_reporting(0);
        $isexpert = $this->input->post('isexpert');
        $uid      = $this->input->post('uid');
        $sid      = $this->input->post('sid');
        $cid      = $this->input->post('cid');
        //echo "<pre>";print_r($this->input->post());die();
        $datas          = $this->input->post();
        $csrf_test_name = $this->security->get_csrf_hash();
        if ($isexpert == 'approved') {

            $updateQuery   = $this->master_model->updateRecord('student_profile', array('aplied_for_expert' => $isexpert, 'previous_user_sub_cat_id' => $cid), array('user_id' => $uid));
            $registerQuery = $this->master_model->updateRecord('registration', array('user_sub_category_id' => '11'), array('user_id' => $uid));

        } else if ($isexpert == 'no') {

            $updateQuery   = $this->master_model->updateRecord('student_profile', array('aplied_for_expert' => $isexpert, 'previous_user_sub_cat_id' => $cid), array('user_id' => $uid));
            $registerQuery = $this->master_model->updateRecord('registration', array('user_sub_category_id' => $sid), array('user_id' => $uid));
        }

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $uid));
        $subCatid  = $user_data[0]['user_sub_category_id'];

        $profile_data     = $this->master_model->getRecords("student_profile", array("user_id" => $uid));
        $previoussubCatid = $profile_data[0]['previous_user_sub_cat_id'];

        $jsonData = array("user_cat_id" => $subCatid, "previous_cat_id" => $previoussubCatid, "token" => $csrf_test_name);
        echo json_encode($jsonData);

    }

    public function markexpertNew()
    {
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        $isexpert      = $this->input->post('isexpert');
        $uid           = $this->input->post('uid');
        $sid           = $this->input->post('sid');
        $cid           = $this->input->post('cid');
        $reason        = $this->input->post('reason');
        //echo "<pre>";print_r($this->input->post());die();
        $datas          = $this->input->post();
        $csrf_test_name = $this->security->get_csrf_hash();

        $user_arr = $this->user_info($uid);

        $name = $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];

        if ($isexpert == 'Approved') {

            $updateQuery = $this->master_model->updateRecord('student_profile', array('expert_claim_admin_approval' => $isexpert, 'previous_user_sub_cat_id' => $cid, 'claim_expert_reason' => $reason), array('user_id' => $uid));

            $registerQuery = $this->master_model->updateRecord('registration', array('user_sub_category_id' => '11'), array('user_id' => $uid));

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('expert_request_approved');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[username]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$name, $phoneNO];
                // sys.tip@technovuus.araiindia.com
                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => $user_arr[0]['email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

            }

        } else if ($isexpert == 'Rejected') {

            $updateQuery   = $this->master_model->updateRecord('student_profile', array('expert_claim_admin_approval' => $isexpert, 'previous_user_sub_cat_id' => $cid, 'claim_expert_reason' => $reason), array('user_id' => $uid));
            $registerQuery = $this->master_model->updateRecord('registration', array('user_sub_category_id' => '1'), array('user_id' => $uid));

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('expert_request_reject');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[username]', '[reason]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$name, $reason, $phoneNO];
                // sys.tip@technovuus.araiindia.com
                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => $user_arr[0]['email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

            }
        }

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $uid));
        $subCatid  = $user_data[0]['user_sub_category_id'];

        $profile_data     = $this->master_model->getRecords("student_profile", array("user_id" => $uid));
        $previoussubCatid = $profile_data[0]['previous_user_sub_cat_id'];

        $jsonData =
        array(
            "user_cat_id"     => $subCatid,
            "previous_cat_id" => $previoussubCatid,
            "token"           => $csrf_test_name,
        );
        echo json_encode($jsonData);

    }

    public function user_info($uid)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $uid));
        // print_r($user_data);

        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        return $user_arr;

    }

    public function challengeStatus()
    {

        $id    = $this->uri->segment(4);
        $value = ucfirst($this->uri->segment(5));
        if ($value == 'Open') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('challenge_status' => $value), array('id' => $id));
            $this->session->set_flashdata('success', 'Challenge status successfully changed');
            redirect(base_url('xAdmin/challenge'));

        } else if ($value == 'Closed') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('challenge_status' => $value), array('id' => $id));
            $this->session->set_flashdata('success', 'Challenge status successfully changed');
            redirect(base_url('xAdmin/challenge'));
        }

    }

    public function deleteStatus()
    {

        error_reporting(0);
        $id = $this->input->post('id');
        //echo "<pre>";print_r($this->input->post());die();

        $csrf_test_name = $this->security->get_csrf_hash();

        $user_data = $this->master_model->getRecords("registration", array("user_id" => $id));

        if ($user_data[0]['is_deleted'] == '0') {
            $updateQuery    = $this->master_model->updateRecord('registration', array('is_deleted' => '1', 'is_valid' => '0'), array('user_id' => $id));
            $user_data      = $this->master_model->getRecords("registration", array("user_id" => $id));
            $status         = $user_data[0]['is_deleted'];
            $csrf_test_name = $this->security->get_csrf_hash();

            /******************Challenge, Team & Slot Related Functionality**********************/

            // Reference Challenge Data Removed
            $challengeArray  = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));
            $updateChallenge = $this->master_model->updateRecord('challenge', $challengeArray, array('u_id' => $id));

            // Reference Teams Data Removed
            $teamArray  = array('is_deleted' => '1', 'modified_on' => date('Y-m-d H:i:s'));
            $updateTeam = $this->master_model->updateRecord('byt_teams', $teamArray, array('user_id' => $id));

            // Reference Slot Data Removed
            $slotArray  = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));
            $updateSlot = $this->master_model->updateRecord('byt_slot_applications', $slotArray, array('apply_user_id' => $id));

            /******************End Challenge, Team & Slot Related Functionality**********************/

            $textShows   = 'Deleted';
            $anchorShows = 'Restore';
            $jsonData    = array("response_id" => $status, "token" => $csrf_test_name, "dynamicText" => $textShows, "spanText" => $anchorShows, "remove_id" => $id);
            echo json_encode($jsonData);

        } else if ($user_data[0]['is_deleted'] == '1') {
            $csrf_test_name = $this->security->get_csrf_hash();
            $updateQuery    = $this->master_model->updateRecord('registration', array('is_deleted' => '0', 'is_valid' => '1'), array('user_id' => $id));
            $user_data      = $this->master_model->getRecords("registration", array("user_id" => $id));
            $status         = $user_data[0]['is_deleted'];

            /******************Challenge, Team & Slot Related Functionality**********************/

            // Reference Challenge Data Removed
            $challengeArray  = array('is_deleted' => '0', 'updatedAt' => date('Y-m-d H:i:s'));
            $updateChallenge = $this->master_model->updateRecord('challenge', $challengeArray, array('u_id' => $id));

            // Reference Teams Data Removed
            $teamArray  = array('is_deleted' => '0', 'modified_on' => date('Y-m-d H:i:s'));
            $updateTeam = $this->master_model->updateRecord('byt_teams', $teamArray, array('user_id' => $id));

            // Reference Slot Data Removed
            $slotArray  = array('is_deleted' => '0', 'updatedAt' => date('Y-m-d H:i:s'));
            $updateSlot = $this->master_model->updateRecord('byt_slot_applications', $slotArray, array('user_id' => $id));

            /******************End Challenge, Team & Slot Related Functionality**********************/

            $textShows   = 'Restore';
            $anchorShows = 'Delete';
            $jsonData    = array("response_id" => $status, "token" => $csrf_test_name, "dynamicText" => $textShows, "spanText" => $anchorShows, "remove_id" => $id);
            echo json_encode($jsonData);
        }

    }

    public function setOrder()
    {

        $id             = $this->input->post('id');
        $order_val      = $this->input->post('order_val');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('registration', array('xOrder' => $order_val), array('user_id' => $id));
        //echo $this->db->last_query();
        $status   = "Order Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function updatePriority()
    {

        $id             = $this->input->post('id');
        $priority       = $this->input->post('priority');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('registration', array('priority' => $priority), array('user_id' => $id));
        //echo $this->db->last_query();
        $status   = "Order Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);

    }

    public function restoreList()
    {

        $encrptopenssl = new Opensslencryptdecrypt();

        $response_data = $this->master_model->getRecords("newsletter", array("is_deleted" => 1));

        $res_arr = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['email_id'] = $encrptopenssl->decrypt($row_val['email_id']);
                $res_arr[]           = $row_val;
            }

        }

        $data['module_name']    = 'Users';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'users/restore';
        $this->load->view('admin/admin_combo', $data);
    }

    public function delete($id)
    {

        $id          = $this->uri->segment(4);
        $updateQuery = $this->master_model->updateRecord('usertype', array('is_deleted' => 1), array('id' => $id));
        $this->session->set_flashdata('success', 'User type successfully deleted');
        redirect(base_url('xAdmin/usertype'));

    }

}
