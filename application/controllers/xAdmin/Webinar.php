<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
		Class : Webinar
		Author : Vicky K
	*/
	
	class Webinar extends CI_Controller 
	{
		function __construct() {
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->model('Common_model_sm');
			$this->load->library('excel');
			if($this->session->userdata('admin_id') == ""){			
				redirect(base_url('xAdmin/admin'));
			}
			 $this->check_permissions->is_authorise_admin(10);
		}
		
    public function index()
    {
			error_reporting(0);
			
			// load excel library
			$this->load->library('excel');	
			
			if(isset($_POST['export']) && !empty($_POST['export'])) 
			{
				
				//error_reporting(0);
				$encrptopenssl =  New Opensslencryptdecrypt();
				
				$response = array();		
				
				$keyword 		= @$this->input->post('keyword')?$this->input->post('keyword'):'';
				
				## Search 
				$search_arr = array();
				$searchQuery = "";
				if($keyword != ''){
					$string = $encrptopenssl->encrypt($keyword);
					$search_arr[] = " (	arai_challenge.challenge_id like '%".$keyword."%' or 
					arai_challenge.challenge_title like '%".$string."%' or 
					arai_challenge.company_name like '%".$string."%' or 
					arai_challenge.challenge_status like '%".$keyword."%' or 	
					arai_challenge.contact_person_name like '%".$string."%' or
					arai_challenge_contact.mobile_no like '%".$keyword."%' or 
					arai_challenge_contact.office_no like '%".$keyword."%' or 	
					arai_challenge_contact.email_id like '%".$keyword."%') ";
					// or arai_challenge.json_str like '%".$keyword."%'
				}
				
				if(count($search_arr) > 0){
					$searchQuery = implode(" and ",$search_arr);
				}		 
				
				## Total number of records without filtering
				$this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
				$this->db->join('registration','challenge.u_id=registration.user_id','left');		
				$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');	
				$records = $this->master_model->getRecords("challenge",array('challenge_status!=' => 'Draft'),'',array('c_id' => 'DESC'));
				
				$totalRecords = count($records);
				
				## Total number of record with filtering		
				$totalRecordwithFilter = count($records);
				if($searchQuery != ''){
					$this->db->where($searchQuery);
					$this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
					$this->db->join('registration','challenge.u_id=registration.user_id','left');		
					$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');	
					$recordsFilter = $this->master_model->getRecords("challenge",array('challenge_status!=' => 'Draft'),'',array('c_id' => 'DESC'));
					$totalRecordwithFilter = count($recordsFilter);
				}
				
				
				## Fetch records		
				if($searchQuery != ''){
					$this->db->where($searchQuery);
				}
				
				$this->db->select('challenge.c_id,challenge.challenge_id,challenge.challenge_title,challenge.company_name,challenge.company_profile,
				challenge.company_profile,challenge.challenge_details,challenge.challenge_abstract,challenge.technology_id,challenge.other_techonology,
				challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name,
				challenge.tags_id,challenge.added_tag_name,challenge.audience_pref_id,challenge.other_audience,challenge.if_funding,
				challenge.if_reward,challenge.is_amount,challenge.fund_reward,challenge.trl_solution,challenge.is_agree,challenge.terms_txt,
				challenge.contact_person_name,challenge.share_details,challenge.challenge_visibility,challenge.future_opportunities,challenge.ip_clause,challenge.is_external_funding,
				challenge.external_fund_details,challenge.is_exclusive_challenge,challenge.exclusive_challenge_details,challenge.challenge_status,challenge.is_featured,challenge.status,
				challenge_contact.email_id,challenge_contact.mobile_no,challenge_contact.office_no,eligibility_expectation.education,eligibility_expectation.from_age,
				eligibility_expectation.to_age,eligibility_expectation.domain,eligibility_expectation.geographical_states,eligibility_expectation.min_team,eligibility_expectation.max_team');
				$this->db->join('registration','challenge.u_id=registration.user_id','left');		
				$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');
				$this->db->join('eligibility_expectation','challenge.c_id=eligibility_expectation.c_id','left');	
				$records = $this->master_model->getRecords("challenge",array('challenge_status!=' => 'Draft'),'',array('c_id' => 'DESC'));
				//echo $this->db->last_query();	die();	
				//echo "<pre>";print_r($records);die();
				
				$data = array();
				$i = 0;
				
				// Count Check
				if(count($records) > 0){				
					
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex(0);
					// set Header				
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Challenge ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Challenge Title');
					$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Challenge Owner');
					$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Company Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Company Profile');
					$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Challenge Details');
					$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Challnege Abstract');
					$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Launch Date');
					$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Close Date');	
					$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Technologies');
					$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Technology');
					$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Tags');
					$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Other Tags');
					$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Audience Preference');
					$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Funding Required');
					$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Reward Required');
					$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Funding Amount');
					$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Funding Reward');
					$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'TRL Solution');
					$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Agree');
					$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Challenge Terms & Conditions');	
					$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Contact Person Name');
					$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Email ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Mobile No');
					$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Office No');
					$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Education');
					$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'From Age');
					$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'To Age');
					$objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Domain');
					$objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Geographical State');
					$objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'Minimum Team');
					$objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'Maximum Team');
					$objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Share Details');
					$objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Challenge Visibility');
					$objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'Future Opportunities');
					$objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'IP Clause');
					$objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Is External Funding');
					$objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'External Fund Details');
					$objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Is Exclusive challenge');
					$objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Exclusive Challenge Details');
					$objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'Is Featured Challenge');
					$objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Challenge Status');
					// set Row
					$rowCount = 2;
					
					foreach($records as $row_val){
						
						$i++;
						$row_val['c_id'] 						= $row_val['c_id'];	
						$row_val['challenge_id'] 				= $row_val['challenge_id'];	
						$row_val['challenge_title'] 			= $encrptopenssl->decrypt($row_val['challenge_title']);
						$row_val['fullname'] 					= $encrptopenssl->decrypt(ucfirst($row_val['title']))." ".$encrptopenssl->decrypt(ucfirst($row_val['first_name']))." ".$encrptopenssl->decrypt(ucfirst($row_val['middle_name']))." ".$encrptopenssl->decrypt(ucfirst($row_val['last_name']));	
						$row_val['challenge_launch_date'] 		= date("d-m-Y", strtotime($row_val['challenge_launch_date']));
						$row_val['company_name'] 				= $encrptopenssl->decrypt($row_val['company_name']);
						$row_val['challenge_close_date'] 		= date("d-m-Y", strtotime($row_val['challenge_close_date']));
						$row_val['is_featured'] 	    		= $row_val['is_featured'];
						$row_val['exclusive_challenge_details'] = $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
						$row_val['external_fund_details'] 		= $encrptopenssl->decrypt($row_val['external_fund_details']);
						$row_val['future_opportunities'] 		= $encrptopenssl->decrypt($row_val['future_opportunities']);
						$row_val['max_team'] 					= $encrptopenssl->decrypt($row_val['max_team']);
						$row_val['min_team'] 					= $encrptopenssl->decrypt($row_val['min_team']);
						$row_val['to_age'] 						= $encrptopenssl->decrypt($row_val['to_age']);
						$row_val['from_age'] 					= $encrptopenssl->decrypt($row_val['from_age']);
						$row_val['education'] 					= $encrptopenssl->decrypt($row_val['education']);
						$row_val['office_no'] 					= $encrptopenssl->decrypt($row_val['office_no']);
						$row_val['mobile_no'] 					= $encrptopenssl->decrypt($row_val['mobile_no']);
						$row_val['email_id'] 					= $encrptopenssl->decrypt($row_val['email_id']);
						$row_val['contact_person_name'] 		= $encrptopenssl->decrypt($row_val['contact_person_name']);
						$row_val['terms_txt'] 					= $encrptopenssl->decrypt($row_val['terms_txt']);
						$row_val['fund_reward'] 				= $encrptopenssl->decrypt($row_val['fund_reward']);
						$row_val['is_amount'] 					= $encrptopenssl->decrypt($row_val['is_amount']);
						$row_val['added_tag_name'] 				= $encrptopenssl->decrypt($row_val['added_tag_name']);
						$row_val['other_techonology'] 			= $encrptopenssl->decrypt($row_val['other_techonology']);
						$row_val['challenge_abstract'] 			= $encrptopenssl->decrypt($row_val['challenge_abstract']);
						$row_val['challenge_details'] 			= $encrptopenssl->decrypt($row_val['challenge_details']);
						$row_val['company_profile'] 			= $encrptopenssl->decrypt($row_val['company_profile']);
						
						
						
						// Technology Data
						$techArr = $row_val['technology_id'];
						$tcArr = array();
						if($row_val['technology_id']!=""){
							
							$exmplodeTech = explode(",", $techArr);
							
							foreach($exmplodeTech as $exTech){
								
								$techNames = $this->master_model->getRecords("technology_master",array('id' => $exTech));
								$tname		 = $encrptopenssl->decrypt($techNames[0]['technology_name']);
								array_push($tcArr, $tname);
							}							
							$impTech = implode(", ", $tcArr);	
							
							} else {
							$impTech = '';
						}
						
						
						
						// Tags Data
						$tagsArr = $row_val['tags_id'];
						$tgArr = array();
						if($row_val['tags_id']!=""){
							
							$exmplodeTags = explode(",", $tagsArr);
							
							foreach($exmplodeTags as $exTag){
								
								$tagNames = $this->master_model->getRecords("tags",array('id' => $exTag));
								$tagname		 = $encrptopenssl->decrypt($tagNames[0]['tag_name']);
								array_push($tgArr, $tagname);
							}							
							$impTgs = implode(", ", $tgArr);	
							
							} else {
							$impTgs = '';
						}
						
						// Audience Preference
						$audiArr = $row_val['audience_pref_id'];
						$aArr = array();
						if($row_val['audience_pref_id']!=""){
							
							$exmplodeAudi = explode(",", $audiArr);
							
							foreach($exmplodeAudi as $audi){
								
								$audiNames = $this->master_model->getRecords("audience_pref",array('id' => $audi));
								$audname		 = $encrptopenssl->decrypt($audiNames[0]['preference_name']);
								array_push($aArr, $audname);
							}							
							$impAudi = implode(", ", $aArr);	
							
							} else {
							$impAudi = '';
						}
						
						// TRL Solution 
						if($row_val['trl_solution']!=""){
							$trlID = $row_val['trl_solution'];
							$trlNames = $this->master_model->getRecords("trl",array('id' => $trlID));
							$trl_name		 = $encrptopenssl->decrypt($trlNames[0]['trl_name']);
							} else {
							$trl_name		 =  '';
						}
						
						// Domain Name 					
						$domainArr = $row_val['domain'];
						$dmArr = array();
						if($row_val['domain']!=""){
							
							$exmplodeDomain = explode(",", $domainArr);
							
							foreach($exmplodeDomain as $exDomain){
								
								$domain_names = $this->master_model->getRecords("domain_master",array('id' => $exDomain));
								$domname		 = $encrptopenssl->decrypt($domain_names[0]['domain_name']);
								array_push($dmArr, $domname);
							}							
							$impDom = implode(", ", $dmArr);	
							
							} else {
							$impDom = '';
						}
						
						// Geographical States 
						$geoArr = $row_val['geographical_states'];
						$geographicalArr = array();
						if($row_val['geographical_states']!=""){
							
							$exmplodeGeo = explode(",", $geoArr);
							
							foreach($exmplodeGeo as $exStates){
								
								$state_names = $this->master_model->getRecords("states",array('id' => $exStates));
								$state_name		 = $state_names[0]['name'];
								array_push($geographicalArr, $state_name);
							}							
							$impState = implode(", ", $geographicalArr);	
							
							} else {
							$impState = '';
						}
						
						// IP Clause
						$ip_clause = $row_val['ip_clause'];
						if($ip_clause!=""){
							
							$ip_names 	= $this->master_model->getRecords("ip_clause",array('id' => $ip_clause));
							$ipname		= $state_names[0]['ip_name'];
							
						}
						
						// Date Convert
						if($row_val['challenge_launch_date']!=""){
							$startDate = date('d-m-Y', strtotime($row_val['challenge_launch_date']));
						}
						
						if($row_val['challenge_close_date']!=""){
							$endDate = date('d-m-Y', strtotime($row_val['challenge_close_date']));
						}
						
						// Agree Terms Conditions
						if($row_val['is_agree'] == 1){
							$agree = 'Yes';
							} else {
							$agree = 'No';
						}
						
						// Share Details
						if($row_val['share_details'] == 1){
							$share_details = 'Yes';
							} else {
							$share_details = 'No';
						}
						
						// Share Details
						if($row_val['is_external_funding'] == 1){
							$ex_fund_apply = 'Yes';
							} else {
							$ex_fund_apply = 'No';
						}
						
						// Exclusive Challenge					
						if($row_val['is_exclusive_challenge'] == 1){
							$ex_ex_apply = 'Yes';
							} else {
							$ex_ex_apply = 'No';
						}			
						
						
						$objPHPExcel->getActiveSheet()->SetCellValue('A'. $rowCount, $row_val['c_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'. $rowCount, $row_val['challenge_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'. $rowCount, $row_val['challenge_title']);
						$objPHPExcel->getActiveSheet()->SetCellValue('D'. $rowCount, $row_val['fullname']);
						$objPHPExcel->getActiveSheet()->SetCellValue('E'. $rowCount, $row_val['company_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('F'. $rowCount, $row_val['company_profile']);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'. $rowCount, $row_val['challenge_details']);
						$objPHPExcel->getActiveSheet()->SetCellValue('H'. $rowCount, $row_val['challenge_abstract']);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'. $rowCount, $startDate);
						$objPHPExcel->getActiveSheet()->SetCellValue('J'. $rowCount, $endDate);	
						$objPHPExcel->getActiveSheet()->SetCellValue('K'. $rowCount, $impTech);
						$objPHPExcel->getActiveSheet()->SetCellValue('L'. $rowCount, $row_val['other_techonology']);
						$objPHPExcel->getActiveSheet()->SetCellValue('M'. $rowCount, $impTgs);
						$objPHPExcel->getActiveSheet()->SetCellValue('N'. $rowCount, $row_val['added_tag_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('O'. $rowCount, $impAudi);
						$objPHPExcel->getActiveSheet()->SetCellValue('P'. $rowCount, $row_val['if_funding']);
						$objPHPExcel->getActiveSheet()->SetCellValue('Q'. $rowCount, $row_val['if_reward']);
						$objPHPExcel->getActiveSheet()->SetCellValue('R'. $rowCount, $row_val['is_amount']);
						$objPHPExcel->getActiveSheet()->SetCellValue('S'. $rowCount, $row_val['fund_reward']);
						$objPHPExcel->getActiveSheet()->SetCellValue('T'. $rowCount, $trl_name);
						$objPHPExcel->getActiveSheet()->SetCellValue('U'. $rowCount, $agree);
						$objPHPExcel->getActiveSheet()->SetCellValue('V'. $rowCount, $row_val['terms_txt']);	
						$objPHPExcel->getActiveSheet()->SetCellValue('W'. $rowCount, $row_val['contact_person_name']);
						$objPHPExcel->getActiveSheet()->SetCellValue('X'. $rowCount, $row_val['email_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('Y'. $rowCount, $row_val['mobile_no']);
						$objPHPExcel->getActiveSheet()->SetCellValue('Z'. $rowCount, $row_val['office_no']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AA'. $rowCount, $row_val['education']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AB'. $rowCount, $row_val['from_age']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AC'. $rowCount, $row_val['to_age']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AD'. $rowCount, $impDom);
						$objPHPExcel->getActiveSheet()->SetCellValue('AE'. $rowCount, $impState);
						$objPHPExcel->getActiveSheet()->SetCellValue('AF'. $rowCount, $row_val['min_team']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AG'. $rowCount, $row_val['max_team']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AH'. $rowCount, $share_details);
						$objPHPExcel->getActiveSheet()->SetCellValue('AI'. $rowCount, $row_val['challenge_visibility']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AJ'. $rowCount, $row_val['future_opportunities']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AK'. $rowCount, $ipname);
						$objPHPExcel->getActiveSheet()->SetCellValue('AL'. $rowCount, $ex_fund_apply);
						$objPHPExcel->getActiveSheet()->SetCellValue('AM'. $rowCount, $row_val['external_fund_details']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AN'. $rowCount, $ex_ex_apply);
						$objPHPExcel->getActiveSheet()->SetCellValue('AO'. $rowCount, $row_val['exclusive_challenge_details']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AP'. $rowCount, $row_val['is_featured']);
						$objPHPExcel->getActiveSheet()->SetCellValue('AQ'. $rowCount, $row_val['challenge_status']);
						$rowCount++;
						
						
					} // Foreach End
					
				} // Count Check  
				
				// create file name
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="challenge-'.time().'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit;  
				
			}
    	
    	$data['records'] = ''; 
			$data['module_name'] = 'Webinar';
			$data['submodule_name'] = '';	
    	$data['middle_content']='webinar/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_webinar_data(){
			
			//error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$table        = 'arai_webinar w';

	 		$column_order = array('w.w_id','w.w_id','w.webinar_id','w.webinar_name','w.webinar_date','w.webinar_start_time','w.cost_price','w.admin_status','(SELECT COUNT(id) FROM arai_webinar_attendence WHERE w_id = w.w_id) AS TotalApp','w.is_featured'

        ); //SET COLUMNS FOR SORT

	 		$column_select = array('w.w_id','w.w_id','w.webinar_id','w.webinar_name','w.webinar_date','w.webinar_start_time','w.currency','w.cost_price','w.admin_status','w.is_featured'

        );

	 		 $column_search = array(
            'w.webinar_id'
          ); 
      //SET COLUMN FOR SEARCH

 		  $order = array('w.w_id' => 'DESC'); // DEFAULT ORDER

      $WhereForTotal = "WHERE w.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
      $Where         = "WHERE w.is_deleted = 0 	";

       if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
        		// echo $_POST['order']['0']['column'];die;
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);

            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        $join_qry .= " LEFT JOIN arai_registration reg ON reg.user_id = w.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_select)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        $Rows        = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));


        $data = array();
        $no   = $_POST['start'];
			foreach($Rows as $row_val ){
		
				$i++;
				$row_val['w_id'] 	    			= $row_val['w_id'];
				$row_val['webinar_id'] 	    		= $row_val['webinar_id'];				
				$row_val['webinar_name'] 			= ucfirst($row_val['webinar_name']);						
				$row_val['webinar_date'] 			= date('d-m-Y', strtotime($row_val['webinar_date']));
				$row_val['webinar_start_time'] 			= $row_val['webinar_start_time'];
				$row_val['webinar_end_time'] 			= $row_val['webinar_end_time'];
				$row_val['is_featured'] 			= $row_val['is_featured'];
				$row_val['currency'] 				= $row_val['currency'];
				$row_val['cost_price'] 				= $row_val['cost_price'];
				$row_val['webinar_cost_type'] 		= $row_val['webinar_cost_type'];
				$row_val['admin_status'] 			= $row_val['admin_status'];
				$row_val['xOrder'] 					= $row_val['xOrder'];
				$fullname	= ucfirst($encrptopenssl->decrypt($row_val['title']))." ".ucfirst($encrptopenssl->decrypt($row_val['first_name']))." ".ucfirst($encrptopenssl->decrypt($row_val['middle_name'])) ." ".ucfirst($encrptopenssl->decrypt($row_val['last_name']));
				
				
				if($row_val['is_featured'] == 'Yes'){
					$textShow = "Featured"; 
					} else {
					$textShow = "Not-Featured"; 
				} 
				
				$modifiedStatus = "<a href='".base_url('xAdmin/webinar/edit/'.base64_encode($row_val['w_id']))."' >
				<button class='btn btn-success btn-sm'>Edit</button>
				</a>";
				
				
				$featuredUser = "<a href='javascript:void(0)' data-id='".$row_val['w_id']."' class='btn btn-outline-success btn-sm featured-check'  id='change-stac-".$row_val['w_id']."'>
				".$textShow."
				</a>";
				$dropdown	= "";
				
				if($row_val['admin_status'] == 'Withdraw'){
					$dropdown .= 	"<select name='ch_sta' id='ch_sta' class='up-status'>
					<option value='Withdraw'>Withdraw</option>
					</select>";
					} else {
					
					if($row_val['admin_status'] == 'Pending'): $chkStatus="selected"; else: $chkStatus="";   endif;
					if($row_val['admin_status'] == 'Approved'): $chkStatus1="selected"; else: $chkStatus1=""; endif;
					if($row_val['admin_status'] == 'Rejected'): $chkStatus2="selected"; else: $chkStatus2=""; endif;
					if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
					//if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;
					
					$dropdown .= 	"<input type='hidden' name='w_id' id='w_id' value='".$row_val['w_id']."' />
					<select name='status' id='status' class='change-status'>
					<option value='Pending' ".$chkStatus.">Pending</option>
					<option value='Approved' ".$chkStatus1.">Approved</option>
					<option value='Rejected' ".$chkStatus2.">Rejected</option>
					<option value='Withdraw' ".$chkStatus3.">Withdraw</option>";
					$dropdown .=	"</select>";
					
				}	
				
				$xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='".$row_val['xOrder']."' name='order_val' id='order_val' value='". $row_val['xOrder']."' 
				onblur='update_sort_order(this.value, ". $row_val['w_id'] .")' onkeyup='update_sort_order(this.value, ". $row_val['w_id'].")' />";
				
				$remove = "<a href='javascript:void(0)' class='remove' data-id='".$row_val['w_id']."'>
				<button class='btn btn-success btn-sm'>Remove</button>
				</a>";
				
				$showButtons = 	$modifiedStatus." ".$featuredUser." ".$remove." ".$dropdown;
				
				$conclude = $row_val['webinar_date'];
				$dispTime = "<span style='white-space: nowrap;'>".date("h:i A", strtotime($row_val['webinar_start_time']))." - ".date("h:i A", strtotime($row_val['webinar_end_time']))."<span>";
				
				$getUserCnt = $this->master_model->getRecords("webinar_attendence",array('w_id' => $row_val['w_id'], 'status' => 'Active'));
				
				
				$showPrice = "<span style='white-space: nowrap;'>".$row_val['currency']." ".$row_val['cost_price']."<span>";
				$receivedApplication = '<a href="javascript:void(0)" class="view-modal" data-id="'.$row_val['w_id'].'">'.count($getUserCnt).'</a>';
				$data[] = array( 
				$i,
				$row_val['w_id'],
				$row_val['webinar_id'],
				$row_val['webinar_name'],
				$conclude,
				$dispTime,
				$showPrice,
				$row_val['admin_status'],
				$receivedApplication,
				$row_val['is_featured'],
				$xOrder,
				$showButtons
				); 
				
			} // Foreach End
			
			$csrf_test_name = $this->security->get_csrf_hash();
			## Response
			$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordwithFilter,
			"aaData" => $data,
			"token" => $csrf_test_name
			);
		
			echo json_encode($response);
		}
		
		public function setOrder(){
			
			$id 			= $this->input->post('id'); 
			$order_val 		= $this->input->post('order_val'); 
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$updateQuery 	= $this->master_model->updateRecord('webinar',array('xOrder'=>$order_val),array('w_id' => $id));
			
			$status = "Order Successfully Updated.";
			$jsonData = array("msg" => $status, "token" => $csrf_test_name);
			echo json_encode($jsonData); 
		}
		
		
		public function viewDetails(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$id=$this->input->post('id');
			$this->db->join('arai_registration','arai_webinar_attendence.user_id=arai_registration.user_id','left');	
			$records = $this->master_model->getRecords("webinar_attendence",array("webinar_attendence.w_id" => $id, "webinar_attendence.status" => 'Active'));
			//echo $this->db->last_query();
			
			$table = '';
			if(count($records) > 0){
				$table .= '<table class="table table-bordered">
				<tr>
				<td><b>Fullname</b></td>
				<td><b>Email</b></td>							
				</tr>';
				foreach($records as $details){
					$firstname 	= $encrptopenssl->decrypt($details['first_name']);
					$lastname 	= $encrptopenssl->decrypt($details['last_name']);
					$middlename = $encrptopenssl->decrypt($details['middle_name']);
					$fullname 	= $firstname." ".$middlename." ".$lastname;
					$email 		= $encrptopenssl->decrypt($details['email']);		
					$table .= '<tr>
					<td>'.ucwords($fullname).'</td>
					<td>'.$email.'</td>
					</tr>';
				}		
				$table .= '</table>';
				} else {
				
				$table = "<p class='text-center'><b>No Record Found</b></p>";
			}
			
			echo $table;
			
		}
		
		public function edit($id)
		{	 				
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$web_id 	   = base64_decode($id);
			
			// Load Library
			$this->load->library('upload');	
			
			$user_id = $this->session->userdata('admin_id');		
			
			$response_data = $this->master_model->getRecords("webinar", array("w_id" => $web_id, "is_deleted"=>0));
			if(count($response_data) == 0) { redirect(site_url('xAdmin/webinar')); }
			
			if(isset($_POST) && count($_POST) > 0)
			{	
				// Check Validation
				$this->form_validation->set_rules('webinar_name', 'Webinar Name', 'required|xss_clean');
				$this->form_validation->set_rules('start_date', 'Start Date', 'required|xss_clean');
				$this->form_validation->set_rules('start_time', 'Start Time', 'required|xss_clean');
				$this->form_validation->set_rules('end_time', 'End Time', 'required|xss_clean');
				$this->form_validation->set_rules('cost_type', 'Type of Registration', 'required|xss_clean');
				
				$this->form_validation->set_rules('webinar_technology[]', 'Webinar Technology', 'required|xss_clean');
				$this->form_validation->set_rules('webinar_technology_other', 'Webinar Technology Other', 'xss_clean');
				$this->form_validation->set_rules('exclusive_technovuus_event', 'Exclusive Technovuus Event', 'required|xss_clean');
				
				if(isset($_POST['exclusive_technovuus_event']) && $_POST['exclusive_technovuus_event'] == 'No')
				{			
					$this->form_validation->set_rules('registration_link', 'Registration Link', 'required');
				}
				
				$this->form_validation->set_rules('hosting_link', 'Hosting Link', 'required');
				$this->form_validation->set_rules('breif_desc', 'Breif Info', 'required|xss_clean');
				$this->form_validation->set_rules('key_points', 'Key Points', 'required|xss_clean');
				$this->form_validation->set_rules('about_author', 'About Author', 'required|xss_clean');
				
				
				if($this->form_validation->run())
				{ 
					//echo "<pre>";print_r($this->input->post());die();							
					$banner_img  		= $_FILES['banner_img']['name'];
					$webinar_name		= $this->input->post('webinar_name');
					$start_date			= date("Y-m-d", strtotime($this->input->post('start_date')));
					$webinar_start_time			= date("H:i", strtotime($this->input->post('start_time')));
					$webinar_end_time			= date("H:i", strtotime($this->input->post('end_time')));
					$cost_type			= $this->input->post('cost_type');
					$currency			= $this->input->post('currency');
					$cost_price			= $this->input->post('cost_price');
					$webinar_technology = $this->input->post('webinar_technology');
					$webinar_technology_other = $this->input->post('webinar_technology_other');
					$exclusive_technovuus_event	= $this->input->post('exclusive_technovuus_event');
					$registration_link = $_POST['registration_link'];
					$hosting_link = $_POST['hosting_link'];
					$breif_desc			= $this->input->post('breif_desc');
					$key_points			= $this->input->post('key_points');
					$about_author		= $this->input->post('about_author');
					$updatedAt			= date('Y-m-d H:i:s');	
					$json_store			= $this->input->post();
					
					if($banner_img!=""){			
						
						$config['upload_path']      = 'assets/webinar';
						$config['allowed_types']    = '*';  
						$config['max_size']         = '5000000';         
						$config['encrypt_name']     = TRUE;
						
						$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
						
						$b_image = "";
						if(isset($upload_files[0]) && !empty($upload_files[0])){
							
							$b_image = $upload_files[0];
							
						}
						
					} // Banner Image End
					
					
					if($cost_type == "PAID"){$price = $cost_price;}else{$price = ''; $currency = '';}	
					
					if($banner_img!="")
					{						
						$banner_names = $b_image[0];
						$up_rec['banner_img'] = $banner_names;
					} 
					
					//$up_rec['user_id'] = $user_id;
					$up_rec['webinar_name'] = $webinar_name;
					$up_rec['webinar_date'] = $start_date;
					$up_rec['webinar_start_time'] = $webinar_start_time;
					$up_rec['webinar_end_time'] = $webinar_end_time;
					$up_rec['webinar_cost_type'] = $cost_type;
					$up_rec['currency'] = $currency;			
					$up_rec['cost_price'] = $price;			
					$up_rec['webinar_technology'] = implode(",",$webinar_technology);
					$up_rec['webinar_technology_other'] = $webinar_technology_other;
					$up_rec['exclusive_technovuus_event'] = $exclusive_technovuus_event;
					if($exclusive_technovuus_event == 'Yes') { $registration_link = ""; }
					$up_rec['registration_link'] = $registration_link;
					$up_rec['hosting_link'] = $hosting_link;
					$up_rec['webinar_breif_info'] = $breif_desc;
					$up_rec['webinar_key_points'] = $key_points;
					$up_rec['webinar_about_author'] = $about_author;
					$up_rec['updatedAt'] = $updatedAt;
					
					$updateQuery = $this->master_model->updateRecord('webinar',$up_rec,array('w_id' => $web_id));
					
					// Log Data Added
					$fileArr			= array('banner_img' => $banner_img);
					$postArr			= $this->input->post();				
					$arrLog 			= array_merge($fileArr,$postArr);					
					$json_encode_data 	= json_encode($arrLog);
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 		=> $user_id,
					'action_name' 	=> "Update",
					'module_name'	=> 'Webinar - Updated By Admin',
					'store_data'	=> $json_encode_data,
					'ip_address'	=> $ipAddr,
					'createdAt'		=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);		
					
					$this->session->set_flashdata('success','Webinar successfully updated.');	
					
					redirect(base_url('xAdmin/webinar'));
					
				} // Validation End
				
				
			} // Post Array END
			
			$data['technology_data'] = $this->master_model->array_sorting($this->master_model->getRecords("arai_webinar_technology_master", array("status" => 'Active')), array('id'),'technology_name');
			$data['webinar_data'] 		= $response_data;
			$data['module_name'] 		= 'Webinar';
			$data['submodule_name'] 	= '';
			$data['middle_content']		='webinar/edit';
			$this->load->view('admin/admin_combo',$data);
			
		} // End Edit
		
		public function updateStatus(){
			// Create Object
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 			 = $this->input->post('id');
			$status 		 = $this->input->post('status');
			$webinar_reason = $this->input->post('webinar_reason');
			
			//echo "<pre>";print_r($this->input->post());die();
			
			
			$updateQuery = $this->master_model->updateRecord('webinar',array('admin_status'=>$status),array('w_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Webinar Status Updated By Admin",
			'module_name'	=> 'Webinar',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			
			if($status == 'Rejected'){
				
				$updateQuery1 = $this->master_model->updateRecord('webinar',array('webinar_reason'=>$webinar_reason),array('w_id' => $id)); 
				
				// SEND MAIL TO CHALLENGE OWNER
				$slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_rejected'); 
				
				$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				
				$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
				
				if(count($webinar_mail) > 0){
					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$webinar_reason = $webinar_data[0]['webinar_reason'];
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $webinar_reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					
					$jsonData = array("token" => $csrf_test_name, "status" => 'done');
					echo json_encode($jsonData);
				}
				
				} else if($status == 'Approved'){
				
				$updateQuery1 = $this->master_model->updateRecord('webinar',array('webinar_reason'=>''),array('w_id' => $id));  
				// SEND MAIL TO CHALLENGE OWNER
				$slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_accepted'); 
				
				$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				
				$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
				
				if(count($webinar_mail) > 0){
					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$reason = '';
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					
					$jsonData = array("token" => $csrf_test_name, "status" => 'done');
					echo json_encode($jsonData);
					
					//return true;
				}
				
			} // Approved Status End
			
			else {
				$updateQuery1 = $this->master_model->updateRecord('webinar',array('webinar_reason'=>''),array('w_id' => $id));  
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);
			}
		}
		
		public function changeStatus(){
			
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Publish'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change - 2",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);	
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));	
				
				} else if($value == 'Block'){
				
				$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id)); 
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Status Change",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$this->session->set_flashdata('success','Publish status successfully changed');
				redirect(base_url('xAdmin/challenge'));		
			}
			
		}  
		
		public function featured(){
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');
			// $value = ucfirst($this->uri->segment(5));
			//echo $id;
			$c_data = $this->master_model->getRecords("webinar", array("w_id" => $id));	
			
			if($c_data[0]['is_featured'] == "No"){
				$is_featured = 'Yes';			
				} else {
				$is_featured = 'No';
			}
			
			$updateQuery = $this->master_model->updateRecord('webinar',array('is_featured'=>$is_featured),array('w_id' => $id));
			//echo $this->db->last_query();
			$c_res = $this->master_model->getRecords("webinar", array("w_id" => $id));		
			
			if($c_res[0]['is_featured'] == "No"){
				$text = 'Featured';			
				} else {
				$text = 'Not-Featured';
			}
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Webinar Featured Status - 3",
			'module_name'	=> 'Webinar',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("c_status" => $is_featured, "u_featured" => $text,"token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		}
		
		public function deleted(){
			
			$user_id = $this->session->userdata('admin_id');
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= $this->input->post('id');	 
			$updateQuery = $this->master_model->updateRecord('webinar',array('is_deleted'=>1),array('w_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Webinar Deleted",
			'module_name'	=> 'Webinar',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
	}		