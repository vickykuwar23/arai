<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }
	
	public function index()
	{
		//echo "<pre>";print_r($this->session->userdata());echo $this->session->userdata('admin_id');die()
		$data['module_name'] = 'Dashboard';
		$data['submodule_name'] = '';	
		$data['middle_content']='dashboard/index';
		$this->load->view('admin/admin_combo',$data);
	}
}
