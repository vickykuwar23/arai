<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Login_page extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->helper('url');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		$this->check_permissions->is_authorise_admin(4);
    }
	 
	 public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');		
		$setting_data = $this->master_model->getRecords("login_content");
		
		$res_arr = array();
		if(count($setting_data)){	
						
			foreach($setting_data as $row_val){		
						
				$row_val['text_content'] 	= $encrptopenssl->decrypt($row_val['text_content']);
				$row_val['content_img'] 	= $encrptopenssl->decrypt($row_val['content_img']);				
				$res_arr[] = $row_val;
			}
			
		}		
		
		$data['content_data'] = $res_arr;	
		
        // Check Validation
		$this->form_validation->set_rules('text_content', 'Content', 'required|min_length[5]|xss_clean');
				
		if($this->form_validation->run())
		{	
			//echo "<pre>";print_r($this->input->post());die();
			$text_content 		= $encrptopenssl->encrypt($this->input->post('text_content'));			
			$content_img		= $_FILES['content_img']['name'];
			
			if($_FILES['content_img']['name']!=""){			
			
				$config['upload_path']      = 'assets/setting';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('content_img', $_FILES, $config, FALSE);
			    $logo_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$logo_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($logo_image[0], PATHINFO_EXTENSION);
				//echo "===".$ext;die();				
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/login_page'));	
				}
				$content_img = $logo_image[0];		
				$filesize = $_FILES['content_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;	
			
			} // Logo Icon End	
			
			if($content_img == ""){
				
				$file_logo = $encrptopenssl->encrypt($data['content_data'][0]['content_img']);
				
			} else {
				
				$file_logo = $encrptopenssl->encrypt($content_img);
			}
			
			$createdAt		=   date('Y-m-d H:i:s');
			
			
			$updateArr = array(	
								'text_content' 		=> $text_content,
								'content_img' 		=> $file_logo,
								'updatedAt' 		=> $createdAt								
							);
								
			$updateQuery = $this->master_model->updateRecord('login_content', $updateArr,array('id' => '1'));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Login content successfully updated');
				redirect(base_url('xAdmin/login_page'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/login_page/'.$id));
			}
			
			
		} // Validation End
		
		$data['module_name'] = 'Login Page';
		$data['submodule_name'] = 'Login Page';
        $data['middle_content']='login-page/index';
        $this->load->view('admin/admin_combo',$data);
     }


}