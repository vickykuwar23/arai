<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Currency_master extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('Opensslencryptdecrypt');
			if($this->session->userdata('admin_id') == ""){			
				redirect(base_url('xAdmin/admin'));
			}	
			$this->check_permissions->is_authorise_admin(4);
		}
		
    public function index($value='')
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$response_data = $this->master_model->getRecords("arai_currency");
			$res_arr = array();
			if(count($response_data))
			{	
				foreach($response_data as $row_val)
				{		
					$row_val['currency'] = $row_val['currency'];
					$res_arr[] = $row_val;
				}
			}
    	$data['records'] = $res_arr; 
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'currency_master';	
    	$data['middle_content']='currency/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
    public function add()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			// Check Validation
			$this->form_validation->set_rules('currency', 'Tag Name', 'required|min_length[2]|xss_clean');
			if($this->form_validation->run())
			{	
				$currency = $this->input->post('currency');
				$insertArr = array( 'currency' => $currency);			
				$insertQuery = $this->master_model->insertRecord('arai_currency',$insertArr);
				if($insertQuery > 0)
				{
					$this->session->set_flashdata('success','Blog Tags successfully created');
					redirect(base_url('xAdmin/currency_master'));
				} 
				else 
				{
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/currency_master/add'));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'knowledge_repository_tags';
			$data['middle_content']='currency/add';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function edit($id)
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$tech_data = $this->master_model->getRecords("arai_currency", array('c_id' => $id));
			$res_arr = array();
			if(count($tech_data) > 0)
			{ 
				$row_val['currency'] = $tech_data[0]['currency'];
				$res_arr[] = $row_val;
			}
			//print_r($res_arr);die();
			$data['tag_data'] = $res_arr;
			// Check Validation
			$this->form_validation->set_rules('currency', 'Tag Name', 'required|min_length[2]|xss_clean');
			if($this->form_validation->run())
			{	
				$currency = $this->input->post('currency');
				$updateAt = date('Y-m-d H:i:s');
				$updateArr = array( 'currency' => $currency);			
				$updateQuery = $this->master_model->updateRecord('arai_currency',$updateArr,array('c_id' => $id));
				if($updateQuery > 0)
				{
					$this->session->set_flashdata('success','Blog Tags Name successfully updated');
					redirect(base_url('xAdmin/currency_master'));
				} 
				else 
				{
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/currency_master/edit/'.$id));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'knowledge_repository_tags';
			$data['middle_content']='currency/edit';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function changeStatus()
		{
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Active')
			{
				$updateQuery = $this->master_model->updateRecord('arai_currency',array('status'=>$value),array('c_id' => $id));
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/currency_master'));	
			} 
			else if($value == 'Block')
			{
				$updateQuery = $this->master_model->updateRecord('arai_currency',array('status'=>$value),array('c_id' => $id)); 
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/currency_master'));		
			}
		}
		
		public function delete($id)
		{
			$id 	= $this->uri->segment(4);		 
			$updateQuery = $this->master_model->updateRecord('arai_currency',array('is_deleted'=>1),array('c_id' => $id));
			$this->session->set_flashdata('success','Blog Tags successfully deleted');
			redirect(base_url('xAdmin/currency_master'));	
		}
	}	