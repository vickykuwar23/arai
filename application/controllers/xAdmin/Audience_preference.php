<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Techology Master
Author : Vicky K
*/

class Audience_preference extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("audience_pref");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['preference_name'] = $encrptopenssl->decrypt($row_val['preference_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Audience_Preference';	
    	$data['middle_content']='audience-preference/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('audience_name', 'Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$audience_name = $encrptopenssl->encrypt($this->input->post('audience_name'));
			
			$insertArr = array( 'preference_name' => $audience_name);			
			$insertQuery = $this->master_model->insertRecord('audience_pref',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Audience name successfully created');
				redirect(base_url('xAdmin/audience_preference'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/audience_preference/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Audience_Preference';
        $data['middle_content']='audience-preference/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("audience_pref", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['preference_name'] = $encrptopenssl->decrypt($tech_data[0]['preference_name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['pref_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('audience_name', 'Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$audience_name = $encrptopenssl->encrypt($this->input->post('audience_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'preference_name' => $audience_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('audience_pref',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Audience name successfully updated');
				redirect(base_url('xAdmin/audience_preference'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/audience_preference/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Audience_Preference';
        $data['middle_content']='audience-preference/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('audience_pref',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/audience_preference'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('audience_pref',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/audience_preference'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('audience_pref',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Audience successfully deleted');
		 redirect(base_url('xAdmin/audience_preference'));	
		 
	 }


}