<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Responses
Author : Vicky K
*/

class Responses extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');		
		$this->load->library('Opensslencryptdecrypt'); 
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
	
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
    	$response_data = $this->master_model->getRecords("responses");
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['response_title'] = $encrptopenssl->decrypt($row_val['response_title']);						
				$res_arr[] = $row_val;
			}
			
		}
		
		$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Response';	
    	$data['middle_content']='responses/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
        // Check Validation
		$this->form_validation->set_rules('response', 'Response Title', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$response =  $encrptopenssl->encrypt($this->input->post('response'));			
			$insertArr = array( 'response_title' => $response);			
			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Response successfully created');
				redirect(base_url('xAdmin/responses'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/responses/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Response';	
        $data['middle_content']='responses/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$data['response_data'] = $this->master_model->getRecords("responses", array('id' => $id));
        // Check Validation
		$this->form_validation->set_rules('response', 'Response Title', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$response = $encrptopenssl->encrypt($this->input->post('response'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'response_title' => $response, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('responses',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Response successfully updated');
				redirect(base_url('xAdmin/responses'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/responses/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Response';	
        $data['middle_content']='responses/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('responses',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/responses'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('responses',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/responses'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('responses',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Response successfully deleted');
		 redirect(base_url('xAdmin/responses'));	
		 
	 }


}