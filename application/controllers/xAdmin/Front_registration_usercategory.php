<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Front registration usercategory
Author : Suraj M
*/

class Front_registration_usercategory extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("registration_usercategory");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usercategory';	
    	$data['middle_content']='front_registration_usercategory/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('usercategory_name', 'User Category Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usercategory_name = $encrptopenssl->encrypt($this->input->post('usercategory_name'));
			
			$insertArr = array( 'user_category' => $usercategory_name);			
			$insertQuery = $this->master_model->insertRecord('registration_usercategory',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User Category successfully created');
				redirect(base_url('xAdmin/front_registration_usercategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/front_registration_usercategory/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usercategory';
        $data['middle_content']='front_registration_usercategory/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("registration_usercategory", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['user_category'] = $encrptopenssl->decrypt($tech_data[0]['user_category']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['institution_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('usercategory_name', 'User Category Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usercategory_name = $encrptopenssl->encrypt($this->input->post('usercategory_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'user_category' => $usercategory_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('registration_usercategory',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User Category Name successfully updated');
				redirect(base_url('xAdmin/front_registration_usercategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/front_registration_usercategory/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Front_registration_usercategory';
        $data['middle_content']='front_registration_usercategory/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('registration_usercategory',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/front_registration_usercategory'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('registration_usercategory',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/front_registration_usercategory'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}