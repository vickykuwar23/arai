<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Subcategory
Author : Vicky K
*/

class Subcategory extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->db->select('categories.category_name,categories.id,subcategory.*');
		$this->db->join('categories','categories.id=subcategory.category_id','left');	
		$response_data = $this->master_model->getRecords("subcategory",'','',array('sub_id'=>'DESC'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['category_name'] = $encrptopenssl->decrypt($row_val['category_name']);
				$row_val['subcategory_name']   = $encrptopenssl->decrypt($row_val['subcategory_name']);
				$res_arr[] = $row_val;
			}
			
		}
		
		$data['records'] = $res_arr;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Subcategories';	
    	$data['middle_content']='subcategory/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$category = $this->master_model->getRecords("categories",'','',array('id'=>'ASC'));
		
		$res_arr = array();
		if(count($category)){	
						
			foreach($category as $row_val){		
				$row_val['id'] = $row_val['id'];		
				$row_val['category_name'] = $encrptopenssl->decrypt($row_val['category_name']);
				$res_arr[] = $row_val;
			}
			
		}
		$data['category_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('category_name', 'Category Name', 'required');
		$this->form_validation->set_rules('subcat_name', 'Sub Category Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$category_name 	= $this->input->post('category_name');
			$subcat_name = $encrptopenssl->encrypt($this->input->post('subcat_name'));
			
			$insertArr = array( 'category_id' => $category_name, 'subcategory_name' => $subcat_name );			
			$insertQuery = $this->master_model->insertRecord('subcategory',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Sub Category successfully created');
				redirect(base_url('xAdmin/subcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/subcategory/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Subcategories';	
    	$data['middle_content']='subcategory/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$category = $this->master_model->getRecords("categories",'','',array('id'=>'ASC'));
		$sub_data = $this->master_model->getRecords("subcategory", array('sub_id' => $id));
        $res_arr = array();
		$res_cat = array();
		if(count($sub_data) > 0){
			$row_val['subcategory_name'] = $encrptopenssl->decrypt($sub_data[0]['subcategory_name']);
			$row_val['category_id'] = $sub_data[0]['category_id'];
			$res_cat[] = $row_val;
		}
		
		if(count($category)){	
						
			foreach($category as $row_val){		
				$row_val['id'] = $row_val['id'];		
				$row_val['category_name'] = $encrptopenssl->decrypt($row_val['category_name']);
				$res_arr[] = $row_val;
			}
			
		}
		$data['sub_data'] = $res_cat;		
		$data['category_data'] = $res_arr;
		
		
		$this->form_validation->set_rules('category_name', 'Category Name', 'required');
		$this->form_validation->set_rules('subcat_name', 'Sub Category Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$category_name 	= $this->input->post('category_name');
			$subcat_name = $encrptopenssl->encrypt($this->input->post('subcat_name'));
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'category_id' => $category_name, 'subcategory_name' => $subcat_name, 'updatedAt' => $updateAt );			
			$updateQuery = $this->master_model->updateRecord('subcategory',$updateArr,array('sub_id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Subcategory successfully updated');
				redirect(base_url('xAdmin/subcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/subcategory/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Subcategories';
        $data['middle_content']='subcategory/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('subcategory',array('status'=>$value),array('sub_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/subcategory'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('subcategory',array('status'=>$value),array('sub_id' => $id)); 
			// echo $this->db->last_query();die;
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/subcategory'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('subcategory',array('is_deleted'=>1),array('sub_id' => $id));
		 $this->session->set_flashdata('success','User Sub Category successfully deleted');
		 redirect(base_url('xAdmin/usersubcat'));	
		 
	 }


}