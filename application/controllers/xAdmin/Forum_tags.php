<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Techology Master
Author : Vicky K
*/

class Forum_tags extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		$response_data = $this->master_model->getRecords("forum_tags");
    	
    	$data['records'] = $response_data; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Forum Tags';	
    	$data['middle_content']='forum_tags/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		
        // Check Validation
		$this->form_validation->set_rules('tag_name', 'Tag Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$tag_name = $this->input->post('tag_name');
			
			$insertArr = array( 'tag_name' => $tag_name);			
			$insertQuery = $this->master_model->insertRecord('forum_tags',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Tags successfully created');
				redirect(base_url('xAdmin/forum_tags'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/forum_tags/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Forum Tags';
        $data['middle_content']='forum_tags/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$tech_data = $this->master_model->getRecords("forum_tags", array('id' => $id));
		
		$data['tag_data'] = $tech_data;
        // Check Validation
		$this->form_validation->set_rules('tag_name', 'Tag Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$tag_name = $this->input->post('tag_name');
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'tag_name' => $tag_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('forum_tags',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Tags Name successfully updated');
				redirect(base_url('xAdmin/forum_tags'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/forum_tags/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Forum Tags';
        $data['middle_content']='forum_tags/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('forum_tags',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/forum_tags'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('forum_tags',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/forum_tags'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('tags',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Tags successfully deleted');
		 redirect(base_url('xAdmin/tags'));	
		 
	 }


}