<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class Skill_sets_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("skill_sets");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Skill_sets_master';	
    	$data['middle_content']='skill_sets_master/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('name', 'Skill Sets', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$name = $encrptopenssl->encrypt($this->input->post('name'));
			$plainName = $this->input->post('name');
			$insertArr = array( 'name' => $name, 'skillset_name' => $plainName);			
			$insertQuery = $this->master_model->insertRecord('skill_sets',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Skill Sets successfully created');
				redirect(base_url('xAdmin/skill_sets_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/skill_sets_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Skill_sets_master';
        $data['middle_content']='skill_sets_master/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("skill_sets", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['name'] = $encrptopenssl->decrypt($tech_data[0]['name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['skill_sets_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('name', 'Skill Sets Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$name = $encrptopenssl->encrypt($this->input->post('name'));
			$plainName = $this->input->post('name');
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'name' => $name, 'skillset_name' => $plainName, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('skill_sets',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Skill Sets Name successfully updated');
				redirect(base_url('xAdmin/skill_sets_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/skill_sets_master/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Skill_sets_master';
        $data['middle_content']='skill_sets_master/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('skill_sets',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/skill_sets_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('skill_sets',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/skill_sets_master'));
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}