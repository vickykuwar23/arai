<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Categories
Author : Vicky K
*/

class Usersubcat extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$this->db->join('user_categories','user_categories.id=user_subcat.u_cat_id','left');	
		$response_data = $this->master_model->getRecords("user_subcat",'','',array('subcat_id'=>'DESC'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$row_val['sub_catname']   = $encrptopenssl->decrypt($row_val['sub_catname']);
				$res_arr[] = $row_val;
			}
			
		}
		
		$data['records'] = $res_arr;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_SubCategories';	
    	$data['middle_content']='usersubcat/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$usertype_cat = $this->master_model->getRecords("user_categories", array('is_deleted' => 0, 'status' => "Active"));
		
		$res_arr = array();
		if(count($usertype_cat)){	
						
			foreach($usertype_cat as $row_val){		
						
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_arr[] = $row_val;
			}
			
		}
		$data['usertype_cat'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('catname', 'User Category Name', 'required');
		$this->form_validation->set_rules('subcatname', 'User Sub Category', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$catname 	= $this->input->post('catname');
			$subcatname = $encrptopenssl->encrypt($this->input->post('subcatname'));
			
			$insertArr = array( 'u_cat_id' => $catname, 'sub_catname' => $subcatname );			
			$insertQuery = $this->master_model->insertRecord('user_subcat',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User Sub Category successfully created');
				redirect(base_url('xAdmin/usersubcat'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usersubcat/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_SubCategories';	
    	$data['middle_content']='usersubcat/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		//$data['usertype_cat'] = $this->master_model->getRecords("user_categories", array('is_deleted' => 0, 'status' => "Active"));
		
		$usertype_cat = $this->master_model->getRecords("user_categories", array('is_deleted' => 0, 'status' => "Active"));
		$usertype_data = $this->master_model->getRecords("user_subcat", array('subcat_id' => $id));
        $res_arr = array();
		$res_cat = array();
		if(count($usertype_data) > 0){
			$row_val['sub_catname'] = $encrptopenssl->decrypt($usertype_data[0]['sub_catname']);
			$row_val['u_cat_id'] = $usertype_data[0]['u_cat_id'];
			$res_arr[] = $row_val;
		}
		
		if(count($usertype_cat)){	
						
			foreach($usertype_cat as $row_val){		
						
				//$row_val['id'] = $encrptopenssl->decrypt($row_val['id']);
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$res_cat[] = $row_val;
			}
			
		}
		
		$data['usertype_data'] = $res_arr;
		$data['usertype_cat'] = $res_cat;
		//echo "<pre>";print_r($data['usertype_data']);die();
		// Check Validation
		$this->form_validation->set_rules('catname', 'User Category Name', 'required');
		$this->form_validation->set_rules('subcatname', 'User Sub Category', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$catname 	= $this->input->post('catname');
			$subcatname = $encrptopenssl->encrypt($this->input->post('subcatname'));			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'u_cat_id' => $catname, 'sub_catname' => $subcatname, 'updatedAt' => $updateAt );			
			$updateQuery = $this->master_model->updateRecord('user_subcat',$updateArr,array('subcat_id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User Sub Category successfully updated');
				redirect(base_url('xAdmin/usersubcat'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usersubcat/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_SubCategories';
        $data['middle_content']='usersubcat/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('user_subcat',array('status'=>$value),array('subcat_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/usersubcat'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('user_subcat',array('status'=>$value),array('subcat_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/usersubcat'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('user_subcat',array('is_deleted'=>1),array('subcat_id' => $id));
		 $this->session->set_flashdata('success','User Sub Category successfully deleted');
		 redirect(base_url('xAdmin/usersubcat'));	
		 
	 }


}