<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Class : Challenge
Author : Vicky K
 */

class Challenge extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(3);
    }

    public function index()
    {
        error_reporting(0);

        // load excel library
        $this->load->library('excel');

        if (isset($_POST['export']) && !empty($_POST['export'])) {

            //error_reporting(0);
            $encrptopenssl = new Opensslencryptdecrypt();

            $response = array();

            $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	arai_challenge.challenge_id like '%" . $keyword . "%' or
									arai_challenge.challenge_title like '%" . $string . "%' or
									arai_challenge.company_name like '%" . $string . "%' or
									arai_challenge.challenge_status like '%" . $keyword . "%' or
									arai_challenge.contact_person_name like '%" . $string . "%' or
									arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
									arai_challenge_contact.office_no like '%" . $keyword . "%' or
									arai_challenge_contact.email_id like '%" . $keyword . "%') ";
                // or arai_challenge.json_str like '%".$keyword."%'
            }

            if (count($search_arr) > 0) {
                $searchQuery = implode(" and ", $search_arr);
            }

            ## Total number of records without filtering
            $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
            $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
            $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
            $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));

            $totalRecords = count($records);

            ## Total number of record with filtering
            $totalRecordwithFilter = count($records);
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
                $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
                $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
                $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
                $recordsFilter         = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));
                $totalRecordwithFilter = count($recordsFilter);
            }

            ## Fetch records
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
            }

            $this->db->select('challenge.c_id,challenge.challenge_id,challenge.challenge_title,challenge.company_name,challenge.company_profile,
								challenge.company_profile,challenge.challenge_details,challenge.challenge_abstract,challenge.technology_id,challenge.other_techonology,
								challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name,
								challenge.tags_id,challenge.added_tag_name,challenge.audience_pref_id,challenge.other_audience,challenge.if_funding,
								challenge.if_reward,challenge.is_amount,challenge.fund_reward,challenge.trl_solution,challenge.is_agree,challenge.terms_txt,
								challenge.contact_person_name,challenge.share_details,challenge.challenge_visibility,challenge.future_opportunities,challenge.ip_clause,challenge.is_external_funding,
								challenge.external_fund_details,challenge.is_exclusive_challenge,challenge.exclusive_challenge_details,challenge.challenge_status,challenge.is_featured,challenge.status,
								challenge_contact.email_id,challenge_contact.mobile_no,challenge_contact.office_no,eligibility_expectation.education,eligibility_expectation.from_age,
								eligibility_expectation.to_age,eligibility_expectation.domain,eligibility_expectation.geographical_states,eligibility_expectation.min_team,eligibility_expectation.max_team');
            $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
            $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
            $this->db->join('eligibility_expectation', 'challenge.c_id=eligibility_expectation.c_id', 'left');
            $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));
            //echo $this->db->last_query();    die();
            //echo "<pre>";print_r($records);die();

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                // set Header
                /*$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Challenge ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Challenge Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Challenge Owner');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Company Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Launch Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Close Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Is Featured');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Challenge Status');*/

                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Challenge ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Challenge Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Challenge Owner');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Company Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Company Profile');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Challenge Details');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Challnege Abstract');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Launch Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Close Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Technologies');
                $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Other Technology');
                $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Tags');
                $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Other Tags');
                $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Audience Preference');
                $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Funding Required');
                $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Reward Required');
                $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Funding Amount');
                $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Funding Reward');
                $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'TRL Solution');
                $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Agree');
                $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Challenge Terms & Conditions');
                $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'Contact Person Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Email ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'Mobile No');
                $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'Office No');
                $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'Education');
                $objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'From Age');
                $objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'To Age');
                $objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'Domain');
                $objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Geographical State');
                $objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'Minimum Team');
                $objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'Maximum Team');
                $objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Share Details');
                $objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'Challenge Visibility');
                $objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'Future Opportunities');
                $objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'IP Clause');
                $objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'Is External Funding');
                $objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'External Fund Details');
                $objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Is Exclusive challenge');
                $objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'Exclusive Challenge Details');
                $objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'Is Featured Challenge');
                $objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Challenge Status');
                // set Row
                $rowCount = 2;

                foreach ($records as $row_val) {

                    $i++;
                    $row_val['c_id']                        = $row_val['c_id'];
                    $row_val['challenge_id']                = $row_val['challenge_id'];
                    $row_val['challenge_title']             = $encrptopenssl->decrypt($row_val['challenge_title']);
                    $row_val['fullname']                    = $encrptopenssl->decrypt(ucfirst($row_val['title'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['first_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['middle_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['last_name']));
                    $row_val['challenge_launch_date']       = date("d-m-Y", strtotime($row_val['challenge_launch_date']));
                    $row_val['company_name']                = $encrptopenssl->decrypt($row_val['company_name']);
                    $row_val['challenge_close_date']        = date("d-m-Y", strtotime($row_val['challenge_close_date']));
                    $row_val['is_featured']                 = $row_val['is_featured'];
                    $row_val['exclusive_challenge_details'] = $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
                    $row_val['external_fund_details']       = $encrptopenssl->decrypt($row_val['external_fund_details']);
                    $row_val['future_opportunities']        = $encrptopenssl->decrypt($row_val['future_opportunities']);
                    $row_val['max_team']                    = $encrptopenssl->decrypt($row_val['max_team']);
                    $row_val['min_team']                    = $encrptopenssl->decrypt($row_val['min_team']);
                    $row_val['to_age']                      = $encrptopenssl->decrypt($row_val['to_age']);
                    $row_val['from_age']                    = $encrptopenssl->decrypt($row_val['from_age']);
                    $row_val['education']                   = $encrptopenssl->decrypt($row_val['education']);
                    $row_val['office_no']                   = $encrptopenssl->decrypt($row_val['office_no']);
                    $row_val['mobile_no']                   = $encrptopenssl->decrypt($row_val['mobile_no']);
                    $row_val['email_id']                    = $encrptopenssl->decrypt($row_val['email_id']);
                    $row_val['contact_person_name']         = $encrptopenssl->decrypt($row_val['contact_person_name']);
                    $row_val['terms_txt']                   = $encrptopenssl->decrypt($row_val['terms_txt']);
                    $row_val['fund_reward']                 = $encrptopenssl->decrypt($row_val['fund_reward']);
                    $row_val['is_amount']                   = $encrptopenssl->decrypt($row_val['is_amount']);
                    $row_val['added_tag_name']              = $encrptopenssl->decrypt($row_val['added_tag_name']);
                    $row_val['other_techonology']           = $encrptopenssl->decrypt($row_val['other_techonology']);
                    $row_val['challenge_abstract']          = $encrptopenssl->decrypt($row_val['challenge_abstract']);
                    $row_val['challenge_details']           = $encrptopenssl->decrypt($row_val['challenge_details']);
                    $row_val['company_profile']             = $encrptopenssl->decrypt($row_val['company_profile']);

                    // Technology Data
                    $techArr = $row_val['technology_id'];
                    $tcArr   = array();
                    if ($row_val['technology_id'] != "") {

                        $exmplodeTech = explode(",", $techArr);

                        foreach ($exmplodeTech as $exTech) {

                            $techNames = $this->master_model->getRecords("technology_master", array('id' => $exTech));
                            $tname     = $encrptopenssl->decrypt($techNames[0]['technology_name']);
                            array_push($tcArr, $tname);
                        }
                        $impTech = implode(", ", $tcArr);

                    } else {
                        $impTech = '';
                    }

                    // Tags Data
                    $tagsArr = $row_val['tags_id'];
                    $tgArr   = array();
                    if ($row_val['tags_id'] != "") {

                        $exmplodeTags = explode(",", $tagsArr);

                        foreach ($exmplodeTags as $exTag) {

                            $tagNames = $this->master_model->getRecords("tags", array('id' => $exTag));
                            $tagname  = $encrptopenssl->decrypt($tagNames[0]['tag_name']);
                            array_push($tgArr, $tagname);
                        }
                        $impTgs = implode(", ", $tgArr);

                    } else {
                        $impTgs = '';
                    }

                    // Audience Preference
                    $audiArr = $row_val['audience_pref_id'];
                    $aArr    = array();
                    if ($row_val['audience_pref_id'] != "") {

                        $exmplodeAudi = explode(",", $audiArr);

                        foreach ($exmplodeAudi as $audi) {

                            $audiNames = $this->master_model->getRecords("audience_pref", array('id' => $audi));
                            $audname   = $encrptopenssl->decrypt($audiNames[0]['preference_name']);
                            array_push($aArr, $audname);
                        }
                        $impAudi = implode(", ", $aArr);

                    } else {
                        $impAudi = '';
                    }

                    // TRL Solution
                    if ($row_val['trl_solution'] != "") {
                        $trlID    = $row_val['trl_solution'];
                        $trlNames = $this->master_model->getRecords("trl", array('id' => $trlID));
                        $trl_name = $encrptopenssl->decrypt($trlNames[0]['trl_name']);
                    } else {
                        $trl_name = '';
                    }

                    // Domain Name
                    $domainArr = $row_val['domain'];
                    $dmArr     = array();
                    if ($row_val['domain'] != "") {

                        $exmplodeDomain = explode(",", $domainArr);

                        foreach ($exmplodeDomain as $exDomain) {

                            $domain_names = $this->master_model->getRecords("domain_master", array('id' => $exDomain));
                            $domname      = $encrptopenssl->decrypt($domain_names[0]['domain_name']);
                            array_push($dmArr, $domname);
                        }
                        $impDom = implode(", ", $dmArr);

                    } else {
                        $impDom = '';
                    }

                    // Geographical States
                    $geoArr          = $row_val['geographical_states'];
                    $geographicalArr = array();
                    if ($row_val['geographical_states'] != "") {

                        $exmplodeGeo = explode(",", $geoArr);

                        foreach ($exmplodeGeo as $exStates) {

                            $state_names = $this->master_model->getRecords("states", array('id' => $exStates));
                            $state_name  = $state_names[0]['name'];
                            array_push($geographicalArr, $state_name);
                        }
                        $impState = implode(", ", $geographicalArr);

                    } else {
                        $impState = '';
                    }

                    // IP Clause
                    $ip_clause = $row_val['ip_clause'];
                    if ($ip_clause != "") {

                        $ip_names = $this->master_model->getRecords("ip_clause", array('id' => $ip_clause));
                        $ipname   = $state_names[0]['ip_name'];

                    }

                    // Date Convert
                    if ($row_val['challenge_launch_date'] != "") {
                        $startDate = date('d-m-Y', strtotime($row_val['challenge_launch_date']));
                    }

                    if ($row_val['challenge_close_date'] != "") {
                        $endDate = date('d-m-Y', strtotime($row_val['challenge_close_date']));
                    }

                    // Agree Terms Conditions
                    if ($row_val['is_agree'] == 1) {
                        $agree = 'Yes';
                    } else {
                        $agree = 'No';
                    }

                    // Share Details
                    if ($row_val['share_details'] == 1) {
                        $share_details = 'Yes';
                    } else {
                        $share_details = 'No';
                    }

                    // Share Details
                    if ($row_val['is_external_funding'] == 1) {
                        $ex_fund_apply = 'Yes';
                    } else {
                        $ex_fund_apply = 'No';
                    }

                    // Exclusive Challenge
                    if ($row_val['is_exclusive_challenge'] == 1) {
                        $ex_ex_apply = 'Yes';
                    } else {
                        $ex_ex_apply = 'No';
                    }

                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['c_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['challenge_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['challenge_title']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row_val['fullname']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row_val['company_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row_val['company_profile']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row_val['challenge_details']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row_val['challenge_abstract']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $startDate);
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $endDate);
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $impTech);
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row_val['other_techonology']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $impTgs);
                    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $row_val['added_tag_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $impAudi);
                    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row_val['if_funding']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $row_val['if_reward']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $row_val['is_amount']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $row_val['fund_reward']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $trl_name);
                    $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $agree);
                    $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $row_val['terms_txt']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $row_val['contact_person_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $row_val['email_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $row_val['mobile_no']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $row_val['office_no']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $row_val['education']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $row_val['from_age']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $row_val['to_age']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $impDom);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $impState);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $row_val['min_team']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $row_val['max_team']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $share_details);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $row_val['challenge_visibility']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $row_val['future_opportunities']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $rowCount, $ipname);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $rowCount, $ex_fund_apply);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $rowCount, $row_val['external_fund_details']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $rowCount, $ex_ex_apply);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $rowCount, $row_val['exclusive_challenge_details']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $rowCount, $row_val['is_featured']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $rowCount, $row_val['challenge_status']);
                    $rowCount++;

                } // Foreach End

            } // Count Check

            // create file name
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="challenge-' . time() . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        $data['records']        = '';
        $data['module_name']    = 'Challenge';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'challenge/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function challengeList()
    {
        // load excel library
        $this->load->library('excel');

        if (isset($_POST['export']) && !empty($_POST['export'])) {

            //error_reporting(0);
            $encrptopenssl = new Opensslencryptdecrypt();

            $response = array();

            $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	arai_challenge.challenge_id like '%" . $keyword . "%' or
									arai_challenge.challenge_title like '%" . $string . "%' or
									arai_challenge.company_name like '%" . $string . "%' or
									arai_challenge.challenge_status like '%" . $keyword . "%' or
									arai_challenge.contact_person_name like '%" . $string . "%' or
									arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
									arai_challenge_contact.office_no like '%" . $keyword . "%' or
									arai_challenge_contact.email_id like '%" . $keyword . "%') ";
                // or arai_challenge.json_str like '%".$keyword."%'
            }

            if (count($search_arr) > 0) {
                $searchQuery = implode(" and ", $search_arr);
            }

            ## Total number of records without filtering
            $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
            $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
            $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
            $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));

            $totalRecords = count($records);

            ## Total number of record with filtering
            $totalRecordwithFilter = count($records);
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
                $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
                $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
                $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
                $recordsFilter         = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));
                $totalRecordwithFilter = count($recordsFilter);
            }

            ## Fetch records
            if ($searchQuery != '') {
                $this->db->where($searchQuery);
            }

            $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,
								challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,
								challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,
								registration.last_name,challenge_contact.email_id,challenge_contact.mobile_no,challenge_contact.office_no');
            $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
            $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
            $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));
            //echo $this->db->last_query();    die();
            //echo "<pre>";print_r($records);die();

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                // set Header
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Challenge ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Challenge Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Challenge Owner');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Company Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Launch Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Close Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Is Featured');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Challenge Status');
                // set Row
                $rowCount = 2;

                foreach ($records as $row_val) {

                    $i++;
                    $row_val['c_id']                  = $row_val['c_id'];
                    $row_val['challenge_id']          = $row_val['challenge_id'];
                    $row_val['challenge_title']       = $encrptopenssl->decrypt($row_val['challenge_title']);
                    $row_val['fullname']              = $encrptopenssl->decrypt(ucfirst($row_val['title'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['first_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['middle_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['last_name']));
                    $row_val['challenge_launch_date'] = date("d-m-Y", strtotime($row_val['challenge_launch_date']));
                    $row_val['company_name']          = $encrptopenssl->decrypt($row_val['company_name']);
                    $row_val['challenge_close_date']  = date("d-m-Y", strtotime($row_val['challenge_close_date']));
                    $row_val['is_featured']           = $row_val['is_featured'];

                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['c_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['challenge_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['challenge_title']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row_val['fullname']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row_val['company_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row_val['challenge_launch_date']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row_val['challenge_close_date']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row_val['is_featured']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row_val['challenge_status']);
                    $rowCount++;

                } // Foreach End

            } // Count Check

            // create file name
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="challenge-' . time() . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        //$data['records'] = $res_arr;
        $data['module_name']    = 'Challenge';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'challenge/test';
        $this->load->view('admin/admin_combo', $data);
    }

    public function get_challenge_data()
    {

        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $response = array();
        ## Read value
        $draw            = @$this->input->post('draw');
        $start           = @$this->input->post('start');
        $rowperpage      = @$this->input->post('length'); // Rows display per page
        $columnIndex     = @$this->input->post('order')[0]['column']; // Column index
        $columnName      = @$this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = @$this->input->post('order')[0]['dir']; // asc or desc
        $searchValue     = @$this->input->post('search')['value']; // Search value

        $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';
        //$frm_date         = @$this->input->post('frm_date')?$this->input->post('frm_date'):'';
        //$office_subtype    = @$this->input->post('office_subtype')?$this->input->post('office_subtype'):'';

        ## Search
        $search_arr  = array();
        $searchQuery = "";
        if ($keyword != '') {
            $string       = $encrptopenssl->encrypt($keyword);
            $search_arr[] = " (	arai_challenge.challenge_id like '%" . $keyword . "%' or
								arai_challenge.challenge_title like '%" . $string . "%' or
								arai_challenge.company_name like '%" . $string . "%' or
								arai_challenge.challenge_status like '%" . $keyword . "%' or
								arai_challenge.contact_person_name like '%" . $string . "%' or
								arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
								arai_challenge_contact.office_no like '%" . $keyword . "%' or
								arai_challenge_contact.email_id like '%" . $keyword . "%') ";

            // or arai_challenge.json_str like '%".$keyword."%'

        }

        /*if($searchCity != ''){
        $search_arr[] = " city='".$searchCity."' ";
        }
        if($searchGender != ''){
        $search_arr[] = " gender='".$searchGender."' ";
        }
        if($searchName != ''){
        $search_arr[] = " name like '%".$searchName."%' ";
        }
        if(count($search_arr) > 0){
        $searchQuery = implode(" and ",$search_arr);
        }*/

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
        $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
        $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
        $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));

        $totalRecords = count($records);

        ## Total number of record with filtering
        $totalRecordwithFilter = count($records);
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
            $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
            $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
            $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
            $recordsFilter         = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'));
            $totalRecordwithFilter = count($recordsFilter);
        }

        ## Fetch records
        if ($searchQuery != '') {
            $this->db->where($searchQuery);
        }

        $columns = array(
            0 => 'c_id',
            1 => 'c_id',
            2 => 'challenge_id',
            3 => 'challenge_title_decrypt',
            4 => 'registration.first_name_decrypt',
            5 => 'challenge.company_name',
            6 => 'challenge_launch_date',
            7 => 'challenge_close_date',
            8 => 'is_featured'
            
        );

        $sort_column = $columns[$_POST['order'][0]['column']];
        $sort_order  = $_POST['order'][0]['dir'];

        if ($sort_column != '' && $sort_order != '') {
            $this->db->order_by($sort_column, $sort_order);
        }

        $this->db->select('challenge.c_id,challenge.is_featured,challenge.challenge_id,challenge.company_profile,challenge.challenge_status,challenge.status,challenge.challenge_title,challenge.company_name,challenge.challenge_launch_date,challenge.challenge_close_date,registration.title,registration.first_name,registration.middle_name,registration.last_name');
        $this->db->join('registration', 'challenge.u_id=registration.user_id', 'left');
        $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
        $records = $this->master_model->getRecords("challenge", array('challenge_status!=' => 'Draft'), '', array('c_id' => 'DESC'), $start, $rowperpage);

        $data = array();
        $i    = 0;
        foreach ($records as $row_val) {

            $i++;
            $row_val['c_id']                  = $row_val['c_id'];
            $row_val['challenge_id']          = $row_val['challenge_id'];
            $row_val['fullname']              = $encrptopenssl->decrypt(ucfirst($row_val['title'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['first_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['middle_name'])) . " " . $encrptopenssl->decrypt(ucfirst($row_val['last_name']));
            $row_val['challenge_title']       = $encrptopenssl->decrypt($row_val['challenge_title']);
            $row_val['company_name']          = $encrptopenssl->decrypt($row_val['company_name']);
            $row_val['challenge_launch_date'] = $row_val['challenge_launch_date'];
            $row_val['challenge_close_date']  = $row_val['challenge_close_date'];
            $row_val['is_featured']           = $row_val['is_featured'];
            $row_val['challenge_status']      = $row_val['challenge_status'];
            $fullname1                        = ucfirst($row_val['title']) . " " . ucfirst($row_val['first_name']) . " " . ucfirst($row_val['middle_name']) . " " . ucfirst($row_val['last_name']);

            if ($row_val['is_featured'] == 'Y') {
                $textShow = "Featured";
            } else {
                $textShow = "Not-Featured";
            }

            $view = "<a href='" . base_url('xAdmin/challenge/challengeDetails/' . base64_encode($row_val['c_id'])) . "'><button class='btn btn-primary btn-sm'> View</button></a>";

            if ($row_val['status'] == 'Active'): $userStatus = "Block";else:$userStatus = "Active";endif;

            $modifiedStatus = "<a href='" . base_url('xAdmin/challenge/edit/' . base64_encode($row_val['c_id'])) . "' >
									<button class='btn btn-success btn-sm'>Edit</button>
									</a>";

            $featuredUser = "<a href='javascript:void(0)' data-id='" . $row_val['c_id'] . "' class='btn btn-outline-success btn-sm featured-check'  id='change-stac-" . $row_val['c_id'] . "'>
								" . $textShow . "
								</a>";
            $dropdown = "";

            /*if($row_val['challenge_status'] == 'Pending'): $chkStatus="selected"; else: $chkStatus="";   endif;
            if($row_val['challenge_status'] == 'Approved'): $chkStatus1="selected"; else: $chkStatus1=""; endif;
            if($row_val['challenge_status'] == 'Rejected'): $chkStatus2="selected"; else: $chkStatus2=""; endif;
            if($row_val['challenge_status'] == 'Withdrawn'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            if($row_val['challenge_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            $dropdown .=     "<input type='hidden' name='ch_id' id='ch_id' value='".$row_val['c_id']."' />
            <select name='ch_sta' id='ch_sta' class='up-status'>
            <option value='Pending' ".$chkStatus.">Pending</option>
            <option value='Approved' ".$chkStatus1.">Approved</option>
            <option value='Rejected' ".$chkStatus2.">Rejected</option>
            <option value='Withdrawn' ".$chkStatus3.">Withdrawn</option>
            <option value='Closed' ".$chkStatus4.">Closed</option>";
            $dropdown .=    "</select>";*/

            if ($row_val['challenge_status'] == 'Withdrawn') {
                $dropdown .= "<select name='ch_sta' id='ch_sta' class='up-status'>
										<option value='Withdrawn'>Withdrawn</option>
									</select>";
            } else {

                if ($row_val['challenge_status'] == 'Pending'): $chkStatus    = "selected";else:$chkStatus    = "";endif;
                if ($row_val['challenge_status'] == 'Approved'): $chkStatus1  = "selected";else:$chkStatus1  = "";endif;
                if ($row_val['challenge_status'] == 'Rejected'): $chkStatus2  = "selected";else:$chkStatus2  = "";endif;
                if ($row_val['challenge_status'] == 'Withdrawn'): $chkStatus3 = "selected";else:$chkStatus3 = "";endif;
                if ($row_val['challenge_status'] == 'Closed'): $chkStatus4    = "selected";else:$chkStatus4    = "";endif;

                $dropdown .= "<input type='hidden' name='ch_id' id='ch_id' value='" . $row_val['c_id'] . "' />
								<select name='ch_sta' id='ch_sta' class='up-status'>
									<option value='Pending' " . $chkStatus . ">Pending</option>
									<option value='Approved' " . $chkStatus1 . ">Approved</option>
									<option value='Rejected' " . $chkStatus2 . ">Rejected</option>
									<option value='Withdrawn' " . $chkStatus3 . ">Withdrawn</option>
									<option value='Closed' " . $chkStatus4 . ">Closed</option>";
                $dropdown .= "</select>";

            }

            //$showButtons =     $view." ".$modifiedStatus." ".$featuredUser." ".$deleteUser;
            $showButtons = $view . " " . $modifiedStatus . " " . $featuredUser . " " . $dropdown;

            $data[] = array(
                $i,
                $row_val['c_id'],
                $row_val['challenge_id'],
                $row_val['challenge_title'],
                $row_val['fullname'],
                $row_val['company_name'],
                $row_val['challenge_launch_date'],
                $row_val['challenge_close_date'],
                $row_val['is_featured'],
                $showButtons,
            );

        } // Foreach End

        $csrf_test_name = $this->security->get_csrf_hash();
        ## Response
        $response = array(
            "draw"                 => intval($draw),
            "iTotalRecords"        => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData"               => $data,
            "token"                => $csrf_test_name,
        );

        echo json_encode($response);
    }

    public function challengeDetails($id)
    {
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        $decode_id     = base64_decode($id);

        //$this->db->select('');
        $this->db->join('eligibility_expectation', 'challenge.c_id=eligibility_expectation.c_id', 'left');
        $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
        $this->db->join('technology_master', 'challenge.technology_id=technology_master.id', 'left');
        $this->db->join('tags', 'challenge.tags_id=tags.id', 'left');
        //$this->db->join('audience_pref','challenge.audience_pref_id=audience_pref.id','left');
        $this->db->join('ip_clause', 'challenge.ip_clause=ip_clause.id', 'left');
        $response_data = $this->master_model->getRecords("challenge", array("challenge.c_id" => $decode_id));
        //echo "<pre>";print_r($response_data);die();
        $res_arr = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $exp  = explode(",", $row_val['audience_pref_id']);
                $pref = array();
                foreach ($exp as $aname) {

                    $pref_data = $this->master_model->getRecords("audience_pref", array("id" => $aname));
                    $pref_name = $encrptopenssl->decrypt($pref_data[0]['preference_name']);
                    array_push($pref, $pref_name);
                }
                $impNmae = implode(",", $pref);

                // Tag Names
                $tags_store = explode(",", $row_val['tags_id']);
                $tags       = array();
                foreach ($tags_store as $tagname) {

                    $tag_data = $this->master_model->getRecords("tags", array("id" => $tagname));
                    $tag_name = $encrptopenssl->decrypt($tag_data[0]['tag_name']);
                    array_push($tags, $tag_name);
                }
                $impTagNmae = implode(",", $tags);

                // Technology Names
                $tech_store = explode(",", $row_val['technology_id']);
                $techStore  = array();
                foreach ($tech_store as $techname) {

                    $tech_data = $this->master_model->getRecords("technology_master", array("id" => $techname));
                    $tech_name = $encrptopenssl->decrypt($tech_data[0]['technology_name']);
                    array_push($techStore, $tech_name);
                }
                $impTechnologyNmae = implode(",", $techStore);

                // Domain Master
                $domainID = $row_val['domain'];

                $explodeDomain = explode(",", $domainID);
                $res_domain    = array();
                foreach ($explodeDomain as $expD) {
                    $domains_data = $this->master_model->getRecords("domain_master", array("id" => $expD));
                    if (count($domains_data) > 0) {
                        $dm_name = $encrptopenssl->decrypt($domains_data[0]['domain_name']);
                        array_push($res_domain, $dm_name);
                    }
                }
                $implodeDm = implode(",", $res_domain);

                /*********/

                $row_val['challenge_title']       = $encrptopenssl->decrypt($row_val['challenge_title']);
                $row_val['company_name']          = $encrptopenssl->decrypt($row_val['company_name']);
                $row_val['company_profile']       = $encrptopenssl->decrypt($row_val['company_profile']);
                $row_val['banner_img']            = $encrptopenssl->decrypt($row_val['banner_img']);
                $row_val['challenge_details']     = $encrptopenssl->decrypt($row_val['challenge_details']);
                $row_val['challenge_abstract']    = $encrptopenssl->decrypt($row_val['challenge_abstract']);
                $row_val['challenge_launch_date'] = $row_val['challenge_launch_date'];
                $row_val['challenge_close_date']  = $row_val['challenge_close_date'];
                //$row_val['technology_name']         = $encrptopenssl->decrypt($row_val['technology_name']);
                $row_val['domain_name']                 = $implodeDm;
                $row_val['preference_name']             = $impNmae;
                $row_val['fund_reward']                 = $row_val['fund_reward'];
                $row_val['technology_name']             = $impTechnologyNmae;
                $row_val['tag_name']                    = $impTagNmae;
                $row_val['contact_person_name']         = $encrptopenssl->decrypt($row_val['contact_person_name']);
                $row_val['challenge_visibility']        = $row_val['challenge_visibility'];
                $row_val['future_opportunities']        = $encrptopenssl->decrypt($row_val['future_opportunities']);
                $row_val['status']                      = $row_val['status'];
                $row_val['ip_name']                     = $row_val['ip_name'];
                $row_val['is_publish']                  = $row_val['is_publish'];
                $row_val['is_featured']                 = $row_val['is_featured'];
                $row_val['external_fund_details']       = $encrptopenssl->decrypt($row_val['external_fund_details']);
                $row_val['is_exclusive_challenge']      = $row_val['is_exclusive_challenge'];
                $row_val['exclusive_challenge_details'] = $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
                $res_arr[]                              = $row_val;
            }

        }

        //echo "<pre>";print_r($res_arr);die();
        $data['challenge_data'] = $res_arr;
        $data['module_name']    = 'Challenge';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'challenge/details';
        $this->load->view('admin/admin_combo', $data);

    }

    public function edit($id)
    {

        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        $challenge_id  = base64_decode($id);
        $this->load->library('upload');

        // Load Library
        $this->load->library('upload');

        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        //echo "<pre>";print_r($this->session->userdata());die();
        $user_id = $this->session->userdata('admin_id');

        $this->db->join('eligibility_expectation', 'challenge.c_id=eligibility_expectation.c_id', 'left');
        $this->db->join('challenge_contact', 'challenge.c_id=challenge_contact.c_id', 'left');
        $this->db->join('ip_clause', 'challenge.ip_clause=ip_clause.id', 'left');
        $response_data = $this->master_model->getRecords("challenge", array("challenge.c_id" => $challenge_id));

        $data['eligibility']       = $this->master_model->getRecords("eligibility_expectation", array("c_id" => $challenge_id));
        $data['challenge_contact'] = $this->master_model->getRecords("challenge_contact", array("c_id" => $challenge_id));

        $res_challenges = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $trid              = $encrptopenssl->decrypt($row_val['trl_solution']);
                $eligibility       = $this->master_model->getRecords("eligibility_expectation", array("c_id" => $challenge_id));
                $challenge_contact = $this->master_model->getRecords("challenge_contact", array("c_id" => $challenge_id));
                $tr_detail         = $this->master_model->getRecords("trl", array("id" => $trid));
                $trl_fullname      = $tr_detail[0]['trl_name'];

                // Domain Master
                $domainIds = $eligibility[0]['domain'];

                $domainDetails = explode(",", $domainIds);
                $res_domains   = array();
                foreach ($domainDetails as $dNames) {
                    $d_view_data = $this->master_model->getRecords("domain_master", array("id" => $dNames));
                    $domain_name = $encrptopenssl->decrypt($d_view_data[0]['domain_name']);
                    array_push($res_domains, $domain_name);
                }
                $implodeD = implode(",", $res_domains);

                // Geography Master
                $geographyMaster = $eligibility[0]['geographical_states'];

                $geoDetails = explode(",", $geographyMaster);
                $res_geo    = array();
                foreach ($geoDetails as $games) {
                    $state_data = $this->master_model->getRecords("states", array("id" => $games));
                    $state_name = $state_data[0]['name'];
                    array_push($res_geo, $state_name);
                }
                $implodeState = implode(",", $res_geo);

                $row_val['education']                   = $encrptopenssl->decrypt($eligibility[0]['education']);
                $row_val['from_age']                    = $encrptopenssl->decrypt($eligibility[0]['from_age']);
                $row_val['to_age']                      = $encrptopenssl->decrypt($eligibility[0]['to_age']);
                $row_val['domain']                      = $eligibility[0]['domain'];
                $row_val['geographical_states']         = $eligibility[0]['geographical_states'];
                $row_val['min_team']                    = $encrptopenssl->decrypt($eligibility[0]['min_team']);
                $row_val['max_team']                    = $encrptopenssl->decrypt($eligibility[0]['max_team']);
                $row_val['email_id']                    = @$encrptopenssl->decrypt($challenge_contact[0]['email_id']);
                $row_val['mobile_no']                   = @$encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
                $row_val['office_no']                   = @$encrptopenssl->decrypt($challenge_contact[0]['office_no']);
                $row_val['challenge_title']             = $encrptopenssl->decrypt($row_val['challenge_title']);
                $row_val['company_name']                = $encrptopenssl->decrypt($row_val['company_name']);
                $row_val['company_profile']             = $encrptopenssl->decrypt($row_val['company_profile']);
                $row_val['challenge_details']           = $encrptopenssl->decrypt($row_val['challenge_details']);
                $row_val['challenge_abstract']          = $encrptopenssl->decrypt($row_val['challenge_abstract']);
                $row_val['banner_img']                  = $encrptopenssl->decrypt($row_val['banner_img']);
                $row_val['company_logo']                = $encrptopenssl->decrypt($row_val['company_logo']);
                $row_val['challenge_launch_date']       = $row_val['challenge_launch_date'];
                $row_val['challenge_close_date']        = $row_val['challenge_close_date'];
                $row_val['technology_id']               = $row_val['technology_id'];
                $row_val['share_details']               = $row_val['share_details'];
                $row_val['other_techonology']           = $encrptopenssl->decrypt($row_val['other_techonology']);
                $row_val['tag_name']                    = $row_val['tags_id'];
                $row_val['added_tag_name']              = $encrptopenssl->decrypt($row_val['added_tag_name']);
                $row_val['audience_pref_id']            = $row_val['audience_pref_id'];
                $row_val['if_funding']                  = $row_val['if_funding'];
                $row_val['domainlist']                  = $implodeD;
                $row_val['stateData']                   = $implodeState;
                $row_val['is_amount']                   = $encrptopenssl->decrypt($row_val['is_amount']);
                $row_val['if_reward']                   = $row_val['if_reward'];
                $row_val['fund_reward']                 = $encrptopenssl->decrypt($row_val['fund_reward']);
                $row_val['terms_txt']                   = $encrptopenssl->decrypt($row_val['terms_txt']);
                $row_val['trl_id']                      = $encrptopenssl->decrypt($row_val['trl_solution']);
                $row_val['tr_name']                     = $encrptopenssl->decrypt($trl_fullname);
                $row_val['other_audience']              = $encrptopenssl->decrypt($row_val['other_audience']);
                $row_val['contact_person_name']         = $encrptopenssl->decrypt($row_val['contact_person_name']);
                $row_val['challenge_visibility']        = $row_val['challenge_visibility'];
                $row_val['future_opportunities']        = $encrptopenssl->decrypt($row_val['future_opportunities']);
                $row_val['status']                      = $row_val['status'];
                $row_val['ip_name']                     = $row_val['ip_name'];
                $row_val['is_publish']                  = $row_val['is_publish'];
                $row_val['is_featured']                 = $row_val['is_featured'];
                $row_val['is_external_funding']         = $row_val['is_external_funding'];
                $row_val['external_fund_details']       = $encrptopenssl->decrypt($row_val['external_fund_details']);
                $row_val['is_exclusive_challenge']      = $row_val['is_exclusive_challenge'];
                $row_val['exclusive_challenge_details'] = $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);

                $res_challenges[] = $row_val;
            }

        }

        //echo "<pre>ECHO";print_r($res_challenges);die();
        $ch_owner_id = $response_data[0]['user_id'];
        $getDetails  = $this->master_model->getRecords("registration", array("user_id" => $ch_owner_id));
        if ($getDetails[0]['user_category_id'] == '2') {

            $data['orgName'] = $encrptopenssl->decrypt($getDetails[0]['institution_full_name']);

        } else {

            $data['orgName'] = "";

        }

        // Technology Master
        $res_arr = $this->master_model->array_sorting($this->master_model->getRecords('technology_master', array('status' => "Active")), array('id'), 'technology_name');

        // Tags Master
        $res_tags = $this->master_model->array_sorting($this->master_model->getRecords('tags', array('status' => "Active")), array('id'), 'tag_name');

        // Audience Preference Master
        $res_audi = $this->master_model->array_sorting($this->master_model->getRecords('audience_pref', array('status' => "Active")), array('id'), 'preference_name');

        // Domain Master
        $res_domain = $this->master_model->array_sorting($this->master_model->getRecords('domain_master', array('status' => "Active")), array('id'), 'domain_name');

        // TRL Master
        $res_trl = $this->master_model->array_sorting($this->master_model->getRecords('trl', array('status' => "Active")), array('id'), 'trl_name');

        // IP Clause Master
        $data['ip_clause_data'] = $this->master_model->getRecords("ip_clause", array("status" => 'Active'));

        // State Master
        $data['state'] = $this->master_model->getRecords("states", array("status" => 'Active'), '', array('name' => 'ASC'));

        if (isset($_POST) && count($_POST) > 0) {
            // Check Validation
            $this->form_validation->set_rules('company_profile', 'Company Profile', 'xss_clean');
            $this->form_validation->set_rules('launch_date', 'Launch Date', 'required|xss_clean');
            $this->form_validation->set_rules('close_date', 'Close Date', 'required|xss_clean');
            $this->form_validation->set_rules('contact_name', 'Contact Name', 'required|xss_clean');
            $this->form_validation->set_rules('contact_email', 'Contact Email', 'required|xss_clean');
            $this->form_validation->set_rules('mobile', 'Contact Mobile', 'required|min_length[10]|max_length[10]|xss_clean');
            $this->form_validation->set_rules('office_no', 'Office Number', 'required|xss_clean');
            $this->form_validation->set_rules('visibility', 'Visibility', 'required|xss_clean');
            $this->form_validation->set_rules('is_exclusive_challenge', 'Challenge exclusively listed on TechNovuus', 'required|xss_clean');

            if ($this->form_validation->run()) {
                //echo "<pre>";print_r($this->input->post());die();
                $pref   = $this->input->post('audi_id');
                $au_arr = implode(",", $pref);

                $techID   = $this->input->post('techonogy_id');
                $tech_arr = implode(",", $techID);

                $tagsID   = $this->input->post('tags_id');
                $tgse_arr = implode(",", $tagsID);

                if ($this->input->post('if_reward') == "") {
                    $reward = '';
                } else {
                    $reward = $this->input->post('if_reward');
                }

                $ex_fund_if = '0';
                if ($this->input->post('external_funding')) {
                    $ex_fund_if = $this->input->post('external_funding');
                }

                $otherTagname = $this->input->post('other_tag');

                if (in_array("0", $tagsID)) {
                    $otherTagNames = $encrptopenssl->encrypt($otherTagname);
                } else {
                    $otherTagNames = '';
                }

                $userTechnolgy = $this->input->post('other_technology');

                if (in_array("0", $techID)) {
                    $otherTchNames = $encrptopenssl->encrypt($userTechnolgy);
                } else {
                    $otherTchNames = '';
                }

                $domainA = $this->input->post('domain');
                $dArr    = implode(",", $domainA);

                $geographicalA = $this->input->post('geographical');
                $gArr          = implode(",", $geographicalA);

                $challenge_title  = $encrptopenssl->encrypt($this->input->post('challenge_title'));
                $company_name     = $encrptopenssl->encrypt($this->input->post('company_name'));
                $breif_info       = $encrptopenssl->encrypt($this->input->post('challenge_details'));
                $abstract_details = $encrptopenssl->encrypt($this->input->post('company_details'));
                $trl_solution     = $encrptopenssl->encrypt($this->input->post('trl_id'));
                $min_team         = $encrptopenssl->encrypt($this->input->post('min_team'));
                $max_team         = $encrptopenssl->encrypt($this->input->post('max_team'));
                //$terms_txt          = $encrptopenssl->encrypt($this->input->post('challenge_terms'));
                $terms_txt    = $this->input->post('challenge_terms');
                $educational  = $encrptopenssl->encrypt($this->input->post('educational'));
                $from_age     = $encrptopenssl->encrypt($this->input->post('from_age'));
                $to_age       = $encrptopenssl->encrypt($this->input->post('to_age'));
                $domain       = $dArr;
                $geographical = $gArr;
                $min_team     = $encrptopenssl->encrypt($this->input->post('min_team'));
                $max_team     = $encrptopenssl->encrypt($this->input->post('max_team'));
                $future_opp   = $encrptopenssl->encrypt($this->input->post('future_opportunities'));

                $company_profile   = $encrptopenssl->encrypt($this->input->post('company_profile'));
                $launch_date       = date('Y-m-d', strtotime($this->input->post('launch_date')));
                $close_date        = date('Y-m-d', strtotime($this->input->post('close_date')));
                $techonogy_id      = $tech_arr;
                $tags_id           = $tgse_arr;
                $otherExtraTag     = $otherTagNames;
                $audi_id           = $au_arr;
                $other_techonology = $otherTchNames;
                $other_audience    = $encrptopenssl->encrypt($this->input->post('other_audience'));
                $if_funding        = $this->input->post('if_funding');
                $if_reward         = $reward;
                $is_amount         = $encrptopenssl->encrypt($this->input->post('is_amount'));
                $fund_reward       = $encrptopenssl->encrypt($this->input->post('fund_reward'));
                $contact_name      = $encrptopenssl->encrypt($this->input->post('contact_name'));
                $contact_email     = $encrptopenssl->encrypt($this->input->post('contact_email'));
                $mobile            = $encrptopenssl->encrypt($this->input->post('mobile'));
                $office_no         = $encrptopenssl->encrypt($this->input->post('office_no'));
                $visibility        = $this->input->post('visibility');
                $ip_cls            = $this->input->post('ip_cls');
                $details_share     = $this->input->post('details_share');
                $is_external_fund  = $this->input->post('is_external_funding');
                $funding_amt       = $encrptopenssl->encrypt($this->input->post('funding_amt'));
                $customTerms       = $encrptopenssl->encrypt($terms_txt);
                $is_exclusive      = $this->input->post('is_exclusive_challenge');
                $banner_img        = $_FILES['banner_img']['name'];
                $comp_logo         = $_FILES['comp_logo']['name'];
                $json_store        = $this->input->post();

                if ($banner_img != "") {

                    $config['upload_path']   = 'assets/challenge';
                    $config['allowed_types'] = '*';
                    $config['max_size']      = '1000000';
                    $config['encrypt_name']  = true;

                    $upload_files = @$this->master_model->upload_file('banner_img', $_FILES, $config, false);
                    $b_image      = "";
                    if (isset($upload_files[0]) && !empty($upload_files[0])) {

                        $b_image = $upload_files[0];

                    }

                } // Banner Image End

                if ($comp_logo != "") {

                    $config['upload_path']   = 'assets/challenge';
                    $config['allowed_types'] = '*';
                    $config['max_size']      = '1000000';
                    $config['encrypt_name']  = true;

                    $upload_files = @$this->master_model->upload_file('comp_logo', $_FILES, $config, false);
                    $c_image      = "";
                    if (isset($upload_files[0]) && !empty($upload_files[0])) {

                        $c_image = $upload_files[0];

                    }

                } // Logo Image End

                if ($is_exclusive == 1) {
                    $challenge_ex = "";
                } else {
                    $challenge_ex = $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
                }
                $shareDetails = '0';
                if ($details_share) {
                    $shareDetails = '1';
                }

                if ($comp_logo == "") {

                    if ($res_challenges[0]['company_logo'] == "") {
                        $logo_names = '';
                    } else {
                        $logo_names = $encrptopenssl->encrypt($res_challenges[0]['company_logo']);
                    }

                } else {
                    $comlogoImg = $c_image[0];
                    $logo_names = $encrptopenssl->encrypt($c_image[0]);
                }

                // Get Banner Image Name
                if ($banner_img == "") {
                    $bnImg = '';

                    $updateArr = array(
                        'challenge_title'             => $challenge_title,
                        'company_name'                => $company_name,
                        'company_profile'             => $company_profile,
                        'company_logo'                => $logo_names,
                        'challenge_details'           => $breif_info,
                        'challenge_abstract'          => $abstract_details,
                        'challenge_launch_date'       => $launch_date,
                        'challenge_close_date'        => $close_date,
                        'technology_id'               => $techonogy_id,
                        'other_techonology'           => $other_techonology,
                        'tags_id'                     => $tags_id,
                        'added_tag_name'              => $otherExtraTag,
                        'last_updated_date'           => date('Y-m-d'),
                        'audience_pref_id'            => $audi_id,
                        'other_audience'              => $other_audience,
                        'if_funding'                  => $if_funding,
                        'if_reward'                   => $if_reward,
                        'is_amount'                   => $is_amount,
                        'fund_reward'                 => $fund_reward,
                        'trl_solution'                => $trl_solution,
                        'contact_person_name'         => $contact_name,
                        'terms_txt'                   => $terms_txt,
                        'challenge_visibility'        => $visibility,
                        'future_opportunities'        => $future_opp,
                        'ip_clause'                   => $ip_cls,
                        'is_external_funding'         => $ex_fund_if,
                        'external_fund_details'       => $funding_amt,
                        'share_details'               => $shareDetails,
                        'terms_txt'                   => $customTerms,
                        'is_exclusive_challenge'      => $is_exclusive,
                        'exclusive_challenge_details' => $challenge_ex,
                        'json_str'                    => json_encode($json_store),
                    );
                } else {
                    $bnImg        = $b_image[0];
                    $banner_names = $encrptopenssl->encrypt($b_image[0]);
                    // Update Array Built
                    $updateArr = array(
                        'challenge_title'             => $challenge_title,
                        'company_name'                => $company_name,
                        'company_profile'             => $company_profile,
                        'challenge_details'           => $breif_info,
                        'challenge_abstract'          => $abstract_details,
                        'banner_img'                  => $banner_names,
                        'company_logo'                => $logo_names,
                        'company_profile'             => $company_profile,
                        'challenge_launch_date'       => $launch_date,
                        'challenge_close_date'        => $close_date,
                        'technology_id'               => $techonogy_id,
                        'other_techonology'           => $other_techonology,
                        'tags_id'                     => $tags_id,
                        'added_tag_name'              => $otherExtraTag,
                        'last_updated_date'           => date('Y-m-d'),
                        'audience_pref_id'            => $audi_id,
                        'other_audience'              => $other_audience,
                        'if_funding'                  => $if_funding,
                        'if_reward'                   => $if_reward,
                        'is_amount'                   => $is_amount,
                        'fund_reward'                 => $fund_reward,
                        'trl_solution'                => $trl_solution,
                        'terms_txt'                   => $terms_txt,
                        'share_details'               => $shareDetails,
                        'contact_person_name'         => $contact_name,
                        'challenge_visibility'        => $visibility,
                        'future_opportunities'        => $future_opp,
                        'ip_clause'                   => $ip_cls,
                        'is_external_funding'         => $ex_fund_if,
                        'external_fund_details'       => $funding_amt,
                        'terms_txt'                   => $customTerms,
                        'is_exclusive_challenge'      => $is_exclusive,
                        'exclusive_challenge_details' => $challenge_ex,
                        'json_str'                    => json_encode($json_store),
                    );

                }

                //echo "<pre>";print_r($this->input->post());die();

                // Insert SQL For Tags
                if ($otherTagname) {

                    $tags_delete = $this->master_model->deleteRecord('tags', 'c_id', $challenge_id);

                    $newTagExp = explode(",", $otherTagname);
                    foreach ($newTagExp as $createTag) {
                        $tagOtherContent = $encrptopenssl->encrypt($createTag);
                        $tagDataInsert   = array('tag_name' => $tagOtherContent, 'c_id' => $challenge_id, 'status' => 'Block');
                        $insertTagsQuery = $this->master_model->insertRecord('tags', $tagDataInsert);

                    }

                }

                if ($userTechnolgy) {

                    $tech_delete = $this->master_model->deleteRecord('technology_master', 'c_id', $challenge_id);

                    // Insert Technology Name into Tag Table
                    $newTechnologyExp = explode(",", $userTechnolgy);
                    foreach ($newTechnologyExp as $createTechnology) {
                        $techOtherContent = $encrptopenssl->encrypt($createTechnology);
                        $techDataInsert   = array('technology_name' => $techOtherContent, 'c_id' => $challenge_id, 'status' => 'Block');
                        $insertTechQuery  = $this->master_model->insertRecord('technology_master', $techDataInsert);

                    }

                }

                $updateQuery = $this->master_model->updateRecord('challenge', $updateArr, array('c_id' => $challenge_id));

                $getCountContact = $this->master_model->getRecords("challenge_contact", array('c_id' => $challenge_id));

                if (count($getCountContact) > 0) {
                    // Update SQL For Contact Details
                    $contactArr = array('c_id' => $challenge_id,
                        'email_id'                 => $contact_email,
                        'mobile_no'                => $mobile,
                        'office_no'                => $office_no,
                        'updatedAt'                => date('Y-m-d H:i:s'),
                    );
                    $this->master_model->updateRecord('challenge_contact', $contactArr, array('c_id' => $challenge_id));
                } else {

                    // Insert SQL For Contact Details
                    $contactArr = array('c_id' => $challenge_id,
                        'email_id'                 => $contact_email,
                        'mobile_no'                => $mobile,
                        'office_no'                => $office_no,
                        'createdAt'                => date('Y-m-d H:i:s'),
                    );
                    $this->master_model->insertRecord('challenge_contact', $contactArr);
                    //echo $this->db->last_query();
                }

                $getCountrecord = $this->master_model->getRecords("eligibility_expectation", array('c_id' => $challenge_id));

                if (count($getCountrecord) > 0) {

                    // Update SQL For Contact Details
                    $eligibilityArr = array('education' => $educational,
                        'from_age'                          => $from_age,
                        'to_age'                            => $to_age,
                        'domain'                            => $dArr,
                        'geographical_states'               => $gArr,
                        'min_team'                          => $min_team,
                        'max_team'                          => $max_team,
                        'updatedAt'                         => date('Y-m-d H:i:s'),
                    );
                    $this->master_model->updateRecord('eligibility_expectation', $eligibilityArr, array('c_id' => $challenge_id));
                } else {
                    // Insert SQL
                    $eligibilityArr1 = array('c_id' => $challenge_id,
                        'education'                     => $educational,
                        'from_age'                      => $from_age,
                        'to_age'                        => $to_age,
                        'domain'                        => $dArr,
                        'geographical_states'           => $gArr,
                        'min_team'                      => $min_team,
                        'max_team'                      => $max_team,
                        'createdAt'                     => date('Y-m-d H:i:s'),
                    );
                    $this->master_model->insertRecord('eligibility_expectation', $eligibilityArr1);
                    //echo "<br />".$this->db->last_query();die();
                }

                // Log Data Added
                $fileArr          = array('banner_img' => $bnImg);
                $postArr          = $this->input->post();
                $arrLog           = array_merge($fileArr, $postArr);
                $json_encode_data = json_encode($arrLog);
                $ipAddr           = $this->get_client_ip();
                $createdAt        = date('Y-m-d H:i:s');
                $logDetails       = array(
                    'user_id'     => $user_id,
                    'action_name' => "Update",
                    'module_name' => 'Challenge - Updated By Admin',
                    'store_data'  => $json_encode_data,
                    'ip_address'  => $ipAddr,
                    'createdAt'   => $createdAt,
                );
                $logData = $this->master_model->insertRecord('logs', $logDetails);

                $this->session->set_flashdata('success', 'Challenge successfully updated.');

                redirect(base_url('xAdmin/challenge'));

            } // Validation End

        } // Post Array END

        //echo "<pre>";print_r($res_challenges);die();
        $data['technology_data'] = $res_arr;
        $data['domain_data']     = $res_domain;
        $data['tag_data']        = $res_tags;
        $data['audience_data']   = $res_audi;
        $data['trl_data']        = $res_trl;
        $data['response_data']   = $res_challenges;
        $data['module_name']     = 'Challenge';
        $data['submodule_name']  = '';
        $data['middle_content']  = 'challenge/edit';
        $this->load->view('admin/admin_combo', $data);

    } // End Edit

    public function updateStatus()
    {
        // Create Object
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $status         = $this->input->post('status');
        // echo "";print_r();
        $updateQuery = $this->master_model->updateRecord('challenge', array('challenge_status' => $status), array('c_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Challenge Publish Status - 1 - Updated By Admin",
            'module_name' => 'Challenge',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        if ($status == 'Rejected') {

            // SEND MAIL TO CHALLENGE OWNER
            $slug           = $encrptopenssl->encrypt('admin_challenge_rejected');
            $challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
            $content        = '';

            $challenge_Status = $this->master_model->getRecords("challenge", array("c_id" => $id));

            if (count($challenge_mail) > 0) {

                $userId         = $challenge_Status[0]['u_id'];
                $sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));

                $fullname = $encrptopenssl->decrypt($sender_details[0]['first_name']) . " " . $encrptopenssl->decrypt($sender_details[0]['last_name']);
                $to_email = $encrptopenssl->decrypt($sender_details[0]['email']);
                //$to_email = 'vicky.kuwar@esds.co.in';
                $registerType    = $sender_details[0]['user_category_id'];
                $registerSubType = $sender_details[0]['user_sub_category_id'];
                $membershipType  = $encrptopenssl->decrypt($sender_details[0]['membership_type']);
                $registerCat     = $this->master_model->getRecords("registration_usercategory", array("id" => $registerType));
                $categoryName    = $encrptopenssl->decrypt($registerCat[0]['user_category']);

                $registerSubType = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $registerSubType));
                $subcategoryName = $encrptopenssl->decrypt($registerSubType[0]['sub_catname']);

                $subject     = $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
                $description = $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
                $from_admin  = $encrptopenssl->decrypt($challenge_mail[0]['from_email']);
                $sendername  = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNo     = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[PHONENO]'];
                $rep_array = [$phoneNo];

                $content = str_replace($arr_words, $rep_array, $description);

                //email admin sending code
                $info_arr = array(
                    'to'      => $to_email,
                    'cc'      => '',
                    'from'    => $from_admin,
                    'subject' => $subject,
                    'view'    => 'common-file',
                );

                $other_info = array('content' => $content);

                $emailsend = $this->emailsending->sendmail($info_arr, $other_info);

            }

        } else if ($status == 'Withdrawn') {

            // Admin Email Functionality
            /*$slug = $encrptopenssl->encrypt('admin_challenge_rejected');
        $challenge_mail = $this->master_model->getRecords("email_template", array("id" => '44'));
        $setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
        $content = '';

        $challenge_Status = $this->master_model->getRecords("challenge", array("c_id" => $id));

        if(count($challenge_mail) > 0){

        $userId = $challenge_Status[0]['u_id'];
        $sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));

        $fullname = $encrptopenssl->decrypt($sender_details[0]['first_name'])." ".$encrptopenssl->decrypt($sender_details[0]['last_name']);
        //$to_email = $encrptopenssl->decrypt($sender_details[0]['email']);
        $to_email = 'vicky.kuwar@esds.co.in';
        $registerType = $sender_details[0]['user_category_id'];
        $registerSubType = $sender_details[0]['user_sub_category_id'];
        $membershipType = $encrptopenssl->decrypt($sender_details[0]['membership_type']);
        $registerCat = $this->master_model->getRecords("registration_usercategory", array("id" => $registerType));
        $categoryName = $encrptopenssl->decrypt($registerCat[0]['user_category']);

        $registerSubType = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $registerSubType));
        $subcategoryName = $encrptopenssl->decrypt($registerSubType[0]['sub_catname']);

        $subject          = $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
        $description     = $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
        $from_admin     = $encrptopenssl->decrypt($challenge_mail[0]['from_email']);
        $sendername      = $encrptopenssl->decrypt($setting_table[0]['field_1']);
        $phoneNo          = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

        $arr_words = ['[ACCOUNT]', '[TYPE]', '[COMPANYNAME]', '[SUBSCRIPTION]', '[CHALLENGESTATEMENT]', '[PHONENO]'];
        $rep_array = [$phoneNo, $categoryName, ,$membershipType,,$phoneNo];

        $content = str_replace($arr_words, $rep_array, $description);

        //email admin sending code
        $info_arr=array(
        'to'        =>    $to_email,
        'cc'        =>    '',
        'from'        =>    $from_admin,
        'subject'    =>     $subject,
        'view'        =>  'common-file'
        );

        $other_info=array('content'=>$content);

        $emailsend=$this->emailsending->sendmail($info_arr,$other_info);

        }     // Slug Exist

         */

        } else if ($status == 'Closed') {

            // Subscriber Email Functionality
            $slug           = $encrptopenssl->encrypt('notification_subscriber_challenge_closure');
            $challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
            $content        = '';

            $challenge_Status = $this->master_model->getRecords("challenge", array("c_id" => $id));

            if (count($challenge_mail) > 0) {

                $userId         = $challenge_Status[0]['u_id'];
                $sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));

                $fullname = $encrptopenssl->decrypt($sender_details[0]['first_name']) . " " . $encrptopenssl->decrypt($sender_details[0]['last_name']);
                $to_email = $encrptopenssl->decrypt($sender_details[0]['email']);
                //$to_email = 'vicky.kuwar@esds.co.in';
                $registerType    = $sender_details[0]['user_category_id'];
                $registerSubType = $sender_details[0]['user_sub_category_id'];
                $membershipType  = $encrptopenssl->decrypt($sender_details[0]['membership_type']);
                $registerCat     = $this->master_model->getRecords("registration_usercategory", array("id" => $registerType));
                $categoryName    = $encrptopenssl->decrypt($registerCat[0]['user_category']);

                $registerSubType = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $registerSubType));
                $subcategoryName = $encrptopenssl->decrypt($registerSubType[0]['sub_catname']);

                $subject     = $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
                $description = $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
                $from_admin  = $encrptopenssl->decrypt($challenge_mail[0]['from_email']);
                $sendername  = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNo     = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
                //$chUrl             = base_url('challenge/challengeDetails/'.base64_encode($id));
                $chUrl = base_url('challenge');
                $link  = '<a href="' . $chUrl . '" target="_blank">LINK</a>';

                $arr_words = ['[LINK]', '[PHONENO]'];
                $rep_array = [$link, $phoneNo];

                $content = str_replace($arr_words, $rep_array, $description);

                //email admin sending code
                $info_arr = array(
                    'to'      => $to_email,
                    'cc'      => '',
                    'from'    => $from_admin,
                    'subject' => $subject,
                    'view'    => 'common-file',
                );

                $other_info = array('content' => $content);

                $emailsend = $this->emailsending->sendmail($info_arr, $other_info);

            } // Slug Exist

        } else if ($status == 'Approved') {

            // Admin Email Functionality
            $slug           = $encrptopenssl->encrypt('challenge_approved');
            $challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
            $content        = '';

            $challenge_Status = $this->master_model->getRecords("challenge", array("c_id" => $id));

            if (count($challenge_mail) > 0) {

                $userId         = $challenge_Status[0]['u_id'];
                $sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));

                $fullname = $encrptopenssl->decrypt($sender_details[0]['first_name']) . " " . $encrptopenssl->decrypt($sender_details[0]['last_name']);
                $to_email = $encrptopenssl->decrypt($sender_details[0]['email']);
                //$to_email = 'vicky.kuwar@esds.co.in';
                $registerType    = $sender_details[0]['user_category_id'];
                $registerSubType = $sender_details[0]['user_sub_category_id'];
                $membershipType  = $encrptopenssl->decrypt($sender_details[0]['membership_type']);
                $registerCat     = $this->master_model->getRecords("registration_usercategory", array("id" => $registerType));
                $categoryName    = $encrptopenssl->decrypt($registerCat[0]['user_category']);

                $registerSubType = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $registerSubType));
                $subcategoryName = $encrptopenssl->decrypt($registerSubType[0]['sub_catname']);

                $subject     = $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
                $description = $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
                $from_admin  = $encrptopenssl->decrypt($challenge_mail[0]['from_email']);
                $sendername  = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNo     = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[PHONENO]'];
                $rep_array = [$phoneNo];

                $content = str_replace($arr_words, $rep_array, $description);

                //email admin sending code
                $info_arr = array(
                    'to'      => $to_email,
                    'cc'      => '',
                    'from'    => $from_admin,
                    'subject' => $subject,
                    'view'    => 'common-file',
                );

                $other_info = array('content' => $content);

                $emailsend = $this->emailsending->sendmail($info_arr, $other_info);

            } // Slug Exist

        } // Approved Status End

        $jsonData = array("token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function changeStatus()
    {

        $id    = $this->uri->segment(4);
        $value = ucfirst($this->uri->segment(5));
        if ($value == 'Publish') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('is_publish' => $value), array('c_id' => $id));

            // Log Data Added
            $user_id          = $this->session->userdata('admin_id');
            $arrData          = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
            $json_encode_data = json_encode($arrData);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Challenge Status Change - 2",
                'module_name' => 'Challenge',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success', 'Publish status successfully changed');
            redirect(base_url('xAdmin/challenge'));

        } else if ($value == 'Block') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('is_publish' => $value), array('c_id' => $id));

            // Log Data Added
            $user_id          = $this->session->userdata('admin_id');
            $arrData          = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
            $json_encode_data = json_encode($arrData);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Challenge Status Change",
                'module_name' => 'Challenge',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success', 'Publish status successfully changed');
            redirect(base_url('xAdmin/challenge'));
        }

    }

    public function featured()
    {

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        // $value = ucfirst($this->uri->segment(5));
        //echo $id;
        $c_data = $this->master_model->getRecords("challenge", array("c_id" => $id));

        if ($c_data[0]['is_featured'] == "N") {
            $is_featured = 'Y';
        } else {
            $is_featured = 'N';
        }

        $updateQuery = $this->master_model->updateRecord('challenge', array('is_featured' => $is_featured), array('c_id' => $id));
        //echo $this->db->last_query();
        $c_res = $this->master_model->getRecords("challenge", array("c_id" => $id));

        if ($c_res[0]['is_featured'] == "N") {
            $text = 'Featured';
        } else {
            $text = 'Not-Featured';
        }

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Challenge Featured Status - 3",
            'module_name' => 'Challenge',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("c_status" => $is_featured, "u_featured" => $text, "token" => $csrf_test_name);
        echo json_encode($jsonData);

    }

    public function challengeStatus()
    {

        $id    = $this->uri->segment(4);
        $value = ucfirst($this->uri->segment(5));
        if ($value == 'Open') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('challenge_status' => $value), array('id' => $id));

            // Log Data Added
            $user_id          = $this->session->userdata('admin_id');
            $arrData          = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
            $json_encode_data = json_encode($arrData);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Challenge Status - 4",
                'module_name' => 'Challenge',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success', 'Challenge status successfully changed');
            redirect(base_url('xAdmin/challenge'));

        } else if ($value == 'Closed') {

            $updateQuery = $this->master_model->updateRecord('challenge', array('challenge_status' => $value), array('id' => $id));

            // Log Data Added
            $user_id          = $this->session->userdata('admin_id');
            $arrData          = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
            $json_encode_data = json_encode($arrData);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Challenge Status - 4",
                'module_name' => 'Challenge',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success', 'Challenge status successfully changed');
            redirect(base_url('xAdmin/challenge'));
        }

    }

    public function delete($id)
    {

        $id          = $this->uri->segment(4);
        $updateQuery = $this->master_model->updateRecord('usertype', array('is_deleted' => 1), array('id' => $id));
        $this->session->set_flashdata('success', 'User type successfully deleted');
        redirect(base_url('xAdmin/usertype'));

    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
