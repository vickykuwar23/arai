<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class Trl extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("trl");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['trl_name'] = $encrptopenssl->decrypt($row_val['trl_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'TRL';	
    	$data['middle_content']='trl/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('trl_name', 'TRL Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$trl_name = $encrptopenssl->encrypt($this->input->post('trl_name'));
			
			$insertArr = array( 'trl_name' => $trl_name);			
			$insertQuery = $this->master_model->insertRecord('trl',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','TRL Solution successfully created');
				redirect(base_url('xAdmin/trl'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/trl/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'TRL';
        $data['middle_content']='trl/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($did)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$id = base64_decode($did);
		$tech_data = $this->master_model->getRecords("trl", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['trl_name'] = $encrptopenssl->decrypt($tech_data[0]['trl_name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['domain_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('trl_name', 'TRL Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$trl_name = $encrptopenssl->encrypt($this->input->post('trl_name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'trl_name' => $trl_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('trl',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','TRL Solution successfully updated');
				redirect(base_url('xAdmin/trl'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/trl/edit/'.$did));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'TRL';
        $data['middle_content']='trl/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('trl',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/trl'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('trl',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/trl'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}