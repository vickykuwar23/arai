<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Domain Master
Author : Suraj M
*/

class Domain_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("domain_master");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['domain_name'] = $encrptopenssl->decrypt($row_val['domain_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Domain';	
    	$data['middle_content']='domain/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('domain_name', 'Domain Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$domain_name = $encrptopenssl->encrypt($this->input->post('domain_name'));
			$planName = $this->input->post('domain_name');
			$insertArr = array( 'domain_name' => $domain_name, 'domain_title' => $planName);			
			$insertQuery = $this->master_model->insertRecord('domain_master',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Domain successfully created');
				redirect(base_url('xAdmin/domain_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/domain_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Domain';
        $data['middle_content']='domain/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($did)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$id = base64_decode($did);
		$tech_data = $this->master_model->getRecords("domain_master", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['domain_name'] = $encrptopenssl->decrypt($tech_data[0]['domain_name']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['domain_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('domain_name', 'Domain Name', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$domain_name = $encrptopenssl->encrypt($this->input->post('domain_name'));
			$planName = $this->input->post('domain_name');
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'domain_name' => $domain_name, 'domain_title' => $planName, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('domain_master',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Domain Name successfully updated');
				redirect(base_url('xAdmin/domain_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/domain_master/edit/'.$did));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Domain';
        $data['middle_content']='domain/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('domain_master',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/domain_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('domain_master',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/domain_master'));		
		 }
		 
	 }
	 
	 // public function delete($id){
		 
		//  $id 	= $this->uri->segment(4);		 
		//  $updateQuery = $this->master_model->updateRecord('institution_master',array('is_deleted'=>1),array('id' => $id));
		//  $this->session->set_flashdata('success','User type successfully deleted');
		//  redirect(base_url('xAdmin/institution_master'));	
		 
	 // }


}