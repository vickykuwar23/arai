<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Usertype extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("usertype");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['user_type'] = $encrptopenssl->decrypt($row_val['user_type']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Usertype';	
    	$data['middle_content']='usertype/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('usertype', 'User Type', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usertype = $encrptopenssl->encrypt($this->input->post('usertype'));
			
			$insertArr = array( 'user_type' => $usertype);			
			$insertQuery = $this->master_model->insertRecord('usertype',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User type successfully created');
				redirect(base_url('xAdmin/usertype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usertype/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Usertype';
        $data['middle_content']='usertype/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$usertype_data = $this->master_model->getRecords("usertype", array('id' => $id));
		$res_arr = array();
		if(count($usertype_data) > 0){ 
			$row_val['user_type'] = $encrptopenssl->decrypt($usertype_data[0]['user_type']);
			$res_arr[] = $row_val;
		}
		//print_r($res_arr);die();
		$data['usertype_data'] = $res_arr;
        // Check Validation
		$this->form_validation->set_rules('usertype', 'User Type', 'required|min_length[5]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$usertype = $encrptopenssl->encrypt($this->input->post('usertype'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'user_type' => $usertype, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('usertype',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User type successfully updated');
				redirect(base_url('xAdmin/usertype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/usertype/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Usertype';
        $data['middle_content']='usertype/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 
	  public function get_actions(){
		//error_reporting(0); 
		$encrptopenssl =  New Opensslencryptdecrypt();
		$encode_id 	= $this->uri->segment(4);
		$decode_id  = base64_decode($encode_id);


		$cat_data = $this->master_model->getRecords("categories", array("status" => 'Active'));
		$res_collect = array();
		
		if(count($cat_data) > 0){
			
			foreach($cat_data as $key => $catDetails){
				
				$subcat_details = $this->master_model->getRecords("subcategory",array('category_id'=>$catDetails['id'],"status" => 'Active'),'',array('sub_id '=>'ASC'));
				$i = 0;
				$row_val['category_name'] = $encrptopenssl->decrypt($catDetails['category_name']);
				foreach($subcat_details as $subcatlist){					
					$res_collect[$key][$row_val['category_name']][$subcatlist['sub_id']] = $encrptopenssl->decrypt($subcatlist['subcategory_name']);
					$i++;
				}
				
				
			} // Foreach end	
			
		} //count end		
		
		// Selected Values
		$this->db->select('sub_id');	
		$data['added_values'] = $this->master_model->getRecords("user_actions",array('user_type_id'=>$decode_id));
		$user_type_name = $this->master_model->getRecords("usertype",array('id'=>$decode_id));
		$data['user_type_name'] = $encrptopenssl->decrypt($user_type_name[0]['user_type']);
		
		// Check Validation
		$this->form_validation->set_rules('sub_cat_id[]', 'Checked Actions', 'required|xss_clean');
		if($this->form_validation->run())
		{
			$posts = $this->input->post('sub_cat_id');
			$deleteRows = $this->master_model->deleteRecord('user_actions','user_type_id',$decode_id);
			
			$addArr = implode(",", $posts);
			$insertArr = array( 'user_type_id' => $decode_id, 'sub_id' => $addArr);	
			$insertQuery = $this->master_model->insertRecord('user_actions',$insertArr);				
			
			// Success Message
			$this->session->set_flashdata('success','Action successfully updated');
			redirect(base_url('xAdmin/usertype/get_actions/'.$encode_id));die();
			
		}
		
		$data['categories_data'] = $res_collect;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Usertype';	
    	$data['middle_content']='usertype/actions';
		$this->load->view('admin/admin_combo',$data);
		 
	 }	 
	
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('usertype',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/usertype'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('usertype',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/usertype'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('usertype',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/usertype'));	
		 
	 }


}