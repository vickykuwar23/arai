<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Studentmaster Master
Author : Vicky K
*/

class Studentmaster extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("student_master");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['id'] = $row_val['id'];
				$row_val['status'] = $row_val['status'];	
				$row_val['degree_name'] = $encrptopenssl->decrypt($row_val['degree_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Student Master';	
    	$data['middle_content']='student-master/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('degree_name', 'Degree Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$degree_name = $encrptopenssl->encrypt($this->input->post('degree_name'));
			
			$insertArr = array( 'degree_name' => $degree_name);			
			$insertQuery = $this->master_model->insertRecord('student_master',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Student Degree successfully created');
				redirect(base_url('xAdmin/studentmaster'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/studentmaster/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Student Master';
        $data['middle_content']='student-master/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$tech_data = $this->master_model->getRecords("student_master", array('id' => $id));
		$res_arr = array();
		if(count($tech_data) > 0){ 
			$row_val['degree_name'] = $encrptopenssl->decrypt($tech_data[0]['degree_name']);
			$res_arr[] = $row_val;
		}
		
		$data['student_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('degree_name', 'Degree Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$degree_name = $encrptopenssl->encrypt($this->input->post('degree_name'));
			$updateAt 	= date('Y-m-d H:i:s');
			$updateArr = array( 'degree_name' => $degree_name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('student_master',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Student Degree successfully updated');
				redirect(base_url('xAdmin/studentmaster'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/studentmaster/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Student Master';
        $data['middle_content']='student-master/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 
		 //echo $id."==".$value;die();
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('student_master',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/studentmaster'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('student_master',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/studentmaster'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('student_master',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Tags successfully deleted');
		 redirect(base_url('xAdmin/studentmaster'));	
		 
	 }


}