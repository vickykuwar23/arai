<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Student_profile
Author : Suraj M
*/

class Student_profile extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$response_data = $this->master_model->getRecords("student_profile",array('type'=>'Student'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['university_course'] = $encrptopenssl->decrypt($row_val['university_course']);
				$row_val['university_name'] = $encrptopenssl->decrypt($row_val['university_name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Student_profile';	
    	$data['middle_content']='student_profile/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('university_course', 'Course', 'required');
		$this->form_validation->set_rules('university_since_year', 'Since Year', 'required');
		$this->form_validation->set_rules('university_to_year', 'To Year', 'required');
		$this->form_validation->set_rules('university_name', 'Name', 'required');
		$this->form_validation->set_rules('university_location', 'Location', 'required');
		$this->form_validation->set_rules('current_study_course', 'Study Course', 'required');
		$this->form_validation->set_rules('current_study_since_year', 'Location', 'required');
		$this->form_validation->set_rules('current_study_to_year', 'Location', 'required');
		$this->form_validation->set_rules('current_study_description', 'Location', 'required');
		$this->form_validation->set_rules('event_experience_in_year', 'Event Experience', 'required');
		$this->form_validation->set_rules('event_description_of_work', 'Description of Work', 'required');
		$this->form_validation->set_rules('technical_experience_in_year', 'Technical Experience', 'required');
		$this->form_validation->set_rules('technical_description_of_work', 'Description of Work', 'required');
		$this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[5]|max_length[5]');
		$this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
		$this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
		$this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');

		if($this->form_validation->run())
		{	
			// echo "<pre>";
			// print_r($this->input->post());die;
			$type = $this->input->post('type');
			$status = $encrptopenssl->encrypt($this->input->post('status'));
			$university_course = $encrptopenssl->encrypt($this->input->post('university_course'));
			$university_since_year = $encrptopenssl->encrypt($this->input->post('university_since_year'));
			$university_to_year = $encrptopenssl->encrypt($this->input->post('university_to_year'));
			$university_name = $encrptopenssl->encrypt($this->input->post('university_name'));
			$university_location = $encrptopenssl->encrypt($this->input->post('university_location'));
			$current_study_course = $encrptopenssl->encrypt($this->input->post('current_study_course'));
			$current_study_since_year = $encrptopenssl->encrypt($this->input->post('current_study_since_year'));
			$current_study_to_year = $encrptopenssl->encrypt($this->input->post('current_study_to_year'));
			$current_study_description = $encrptopenssl->encrypt($this->input->post('current_study_description'));

			$domain = $interestArea = $skillSets = [];
			$domain = $this->input->post('domain_and_area_of_training_and_study');
			$domainString = $encrptopenssl->encrypt(implode(',', $domain));
			$interestArea = $this->input->post('area_of_interest');
			$AreaOfInterest = $encrptopenssl->encrypt(implode(',', $interestArea));
			$skillSets = $this->input->post('skill_sets');
			$SkillSets = $encrptopenssl->encrypt(implode(',', $skillSets));

			$event_experience_in_year = $encrptopenssl->encrypt($this->input->post('event_experience_in_year'));
			$event_description_of_work = $encrptopenssl->encrypt($this->input->post('event_description_of_work'));
			$technical_experience_in_year = $encrptopenssl->encrypt($this->input->post('technical_experience_in_year'));
			$technical_description_of_work = $encrptopenssl->encrypt($this->input->post('technical_description_of_work'));
			$pincode = $encrptopenssl->encrypt($this->input->post('pincode'));
			$flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
			$area_colony_street_village = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
			$town_city_and_state = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
			$country = $encrptopenssl->encrypt($this->input->post('country'));

			$student_id_proof  = $_FILES['student_id_proof']['name'];
			
			if($_FILES['student_id_proof']['name']!=""){			
				
				$config['upload_path']      = 'assets/id_proof';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('student_id_proof', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/student_profile'));	
				}
						
				$filesize = $_FILES['student_id_proof']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			} // Student ID Proof Image End
			
			$file_names = $encrptopenssl->encrypt($b_image[0]);
			
			
			$insertArr = array('type' => $type, 'status' => $status, 'university_course' => $university_course, 'university_since_year' => $university_since_year, 'university_to_year' => $university_to_year, 'university_name' => $university_name, 'university_location' => $university_location, 'current_study_course' => $current_study_course, 'current_study_since_year' => $current_study_since_year, 'current_study_to_year' => $current_study_to_year, 'current_study_description' => $current_study_description, 'domain_and_area_of_training_and_study' => $domainString, 'areas_of_interest' => $AreaOfInterest, 'skill_sets' => $SkillSets, 'event_experience_in_year' => $event_experience_in_year, 'event_description_of_work' => $event_description_of_work, 'technical_experience_in_year' => $technical_experience_in_year, 'technical_description_of_work' => $technical_description_of_work, 'pincode' => $pincode, 'flat_house_building_apt_company' => $flat_house_building_apt_company, 'area_colony_street_village' => $area_colony_street_village, 'town_city_and_state' => $town_city_and_state, 'country' => $country, 'student_id_proof' => $file_names);			
			$insertQuery = $this->master_model->insertRecord('student_profile',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Student Profile successfully created');
				redirect(base_url('xAdmin/student_profile'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/student_profile/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Student_profile';
        $data['middle_content']='student_profile/add';
        $this->load->view('admin/admin_combo',$data);
     }

}