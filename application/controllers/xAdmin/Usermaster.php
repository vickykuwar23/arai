<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Class : User Master
Author : Vicky K
 */

class Usermaster extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Opensslencryptdecrypt');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
//         ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
        $this->check_permissions->is_authorise_admin(1);  
    }

    public function index()
    {

        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->join('admin_type', 'user_master.admin_roles=admin_type.id', 'left');
        $response_data = $this->master_model->getRecords("user_master", array("user_id!=" => '1'));

        $res_arr = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                //$row_val['user_type'] = $encrptopenssl->decrypt($row_val['user_type']);
                $row_val['admin_email']    = $encrptopenssl->decrypt($row_val['admin_email']);
                $row_val['admin_name']     = $encrptopenssl->decrypt($row_val['admin_name']);
                $row_val['admin_username'] = $encrptopenssl->decrypt($row_val['admin_username']);
                $row_val['admin_type']     = $row_val['admin_type_name'];
                $row_val['user_id']        = $row_val['user_id'];
                $res_arr[]                 = $row_val;
            }

        }
        //echo "<pre>";print_r($res_arr);die();
        $data['records']        = $res_arr;
        $data['module_name']    = 'Master';
        $data['submodule_name'] = 'Usermaster';
        $data['middle_content'] = 'user_master/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function add()
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $data['admin_type'] = $this->master_model->getRecords("admin_type", array("status" => 'Active'));

        // Check Validation
        $this->form_validation->set_rules('admin_username', 'Username', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('admin_password', 'Password', 'required|min_length[8]|xss_clean');
        $this->form_validation->set_rules('admin_name', 'Fullname', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('admin_email', 'Email ID', 'required|xss_clean');
        $this->form_validation->set_rules('role_id', 'Role', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $admin_username = $encrptopenssl->encrypt($this->input->post('admin_username'));
            $admin_password = $encrptopenssl->encrypt(md5($this->input->post('admin_password')));
            $admin_name     = $encrptopenssl->encrypt($this->input->post('admin_name'));
            $admin_email    = $encrptopenssl->encrypt($this->input->post('admin_email'));
            $usertype       = $this->input->post('role_id');
            $added_on       = date('Y-m-d H:i:s');
            $insertArr      = array('admin_username' => $admin_username,
                'admin_password'                         => $admin_password,
                'admin_name'                             => $admin_name,
                'admin_email'                            => $admin_email,
                'admin_roles'                            => $usertype,
                'added_on'                               => $added_on,
            );
            $insertQuery = $this->master_model->insertRecord('user_master', $insertArr);
            if ($insertQuery > 0) {
                $this->session->set_flashdata('success', 'Admin user successfully created');
                redirect(base_url('xAdmin/usermaster'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('xAdmin/usermaster/add'));
            }
        }
        $data['module_name']    = 'Master';
        $data['submodule_name'] = 'Usermaster';
        $data['middle_content'] = 'user_master/add';
        $this->load->view('admin/admin_combo', $data);
    }

    public function edit($id)
    {
        $encrptopenssl      = new Opensslencryptdecrypt();
        $decode_id          = base64_decode($id);
        $data['admin_type'] = $this->master_model->getRecords("admin_type", array("status" => 'Active'));

        $user_data = $this->master_model->getRecords("user_master", array('user_id' => $decode_id));
        $res_arr   = array();
        if (count($user_data) > 0) {
            $row_val['admin_username'] = $encrptopenssl->decrypt($user_data[0]['admin_username']);
            $row_val['admin_password'] = $encrptopenssl->decrypt($user_data[0]['admin_password']);
            $row_val['admin_name']     = $encrptopenssl->decrypt($user_data[0]['admin_name']);
            $row_val['admin_email']    = $encrptopenssl->decrypt($user_data[0]['admin_email']);
            $row_val['admin_roles']    = $user_data[0]['admin_roles'];
            $res_arr[]                 = $row_val;
        }

        $data['user_data'] = $res_arr;

        // Check Validation
        $this->form_validation->set_rules('admin_username', 'Username', 'required|min_length[3]|xss_clean');
        if ($this->input->post('admin_password') != "") {
            $this->form_validation->set_rules('admin_password', 'Password', 'required|min_length[8]|xss_clean');
        }
        $this->form_validation->set_rules('admin_name', 'Fullname', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('admin_email', 'Email ID', 'required|xss_clean');
        $this->form_validation->set_rules('role_id', 'Role', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $admin_username = $encrptopenssl->encrypt($this->input->post('admin_username'));
            //$admin_password = $encrptopenssl->encrypt(md5($this->input->post('admin_password')));
            $admin_name  = $encrptopenssl->encrypt($this->input->post('admin_name'));
            $admin_email = $encrptopenssl->encrypt($this->input->post('admin_email'));
            $usertype    = $this->input->post('role_id');
            $updateAt    = date('Y-m-d H:i:s');

            if ($this->input->post('admin_password') != "") {

                $admin_password = $encrptopenssl->encrypt(md5($this->input->post('admin_password')));
                $updateArr      = array('admin_username' => $admin_username,
                    'admin_password'                         => $admin_password,
                    'admin_name'                             => $admin_name,
                    'admin_email'                            => $admin_email,
                    'admin_roles'                            => $usertype,
                    'updatedAt'                              => $updateAt,
                );

            } else {

                $updateArr = array('admin_username' => $admin_username,
                    'admin_name'                        => $admin_name,
                    'admin_email'                       => $admin_email,
                    'admin_roles'                       => $usertype,
                    'updatedAt'                         => $updateAt,
                );

            }

            $updateQuery = $this->master_model->updateRecord('user_master', $updateArr, array('user_id' => $decode_id));

            if ($updateQuery > 0) {
                $this->session->set_flashdata('success', 'Admin user successfully updated');
                redirect(base_url('xAdmin/usermaster'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('xAdmin/usermaster/edit/' . $id));
            }
        }

        $data['module_name']    = 'Master';
        $data['submodule_name'] = 'Usermaster';
        $data['middle_content'] = 'user_master/edit';
        $this->load->view('admin/admin_combo', $data);
    }

    public function changeStatus()
    {

        $id    = $this->uri->segment(4);
        $value = ucfirst($this->uri->segment(5));
        if ($value == 'Active') {

            $updateQuery = $this->master_model->updateRecord('usertype', array('status' => $value), array('id' => $id));
            $this->session->set_flashdata('success', 'Status successfully changed');
            redirect(base_url('xAdmin/usertype'));

        } else if ($value == 'Block') {

            $updateQuery = $this->master_model->updateRecord('usertype', array('status' => $value), array('id' => $id));
            $this->session->set_flashdata('success', 'Status successfully changed');
            redirect(base_url('xAdmin/usertype'));
        }

    }

    public function delete($id)
    {

        $id          = $this->uri->segment(4);
        $updateQuery = $this->master_model->updateRecord('usertype', array('is_deleted' => 1), array('id' => $id));
        $this->session->set_flashdata('success', 'User type successfully deleted');
        redirect(base_url('xAdmin/usertype'));

    }

    public function permissions($enc_user_id)
    {
        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        if ($enc_user_id == '' || $enc_user_id == '0') {
            redirect(base_url('xAdmin/dashboard'));
        }
        $user_id = base64_decode($enc_user_id);

        $this->db->select('module_permission,admin_username');
        $data['added_values'] = $this->master_model->getRecords("user_master", array('user_id' => $user_id));

        $this->form_validation->set_rules('modules_ids[]', 'Checked Actions', 'required|xss_clean');
        if ($this->form_validation->run()) {
            $modules = $this->input->post('modules_ids');

            $modules_string = implode(",", $modules);
            $update_arr     = array('module_permission' => $modules_string);
            $updateQuery    = $this->master_model->updateRecord('user_master', $update_arr, array('user_id' => $user_id));
            // Success Message
            $this->session->set_flashdata('success', 'Action successfully updated');
            redirect(base_url('xAdmin/usermaster/permissions/' . $enc_user_id));die();

        }
        $data['module_data'] = $this->master_model->getRecords("arai_admin_modules", array('status' => 'Active'));

        $data['module_name']    = 'Master';
        $data['submodule_name'] = 'Usermaster';
        $data['middle_content'] = 'user_master/actions';
        $this->load->view('admin/admin_combo', $data);

    }

}
