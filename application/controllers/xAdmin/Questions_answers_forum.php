<?php 
	if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Questions_answers_forum extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->library('excel');
			if($this->session->userdata('admin_id') == "")
			{			
				redirect(base_url('xAdmin/admin'));
			}
		 $this->check_permissions->is_authorise_admin(12);
		}
		
		public function index()
		{
			error_reporting(0);
			
			// load excel library
			$this->load->library('excel');	
			
			if(isset($_POST['export']) && !empty($_POST['export'])) 
			{
				/* print_r($_POST); exit; */
				//error_reporting(0);
				$encrptopenssl =  New Opensslencryptdecrypt();				
				$response = array();
				
				$keyword = @$this->input->post('keyword')?$this->input->post('keyword'):'';
				
				## Search 
				$search_arr = array();
				$searchQuery = "";
				if($keyword != '')
				{
					$string = $encrptopenssl->encrypt($keyword);
					$search_arr[] = " (	b.q_id_disp like '%".$keyword."%' or b.blog_title like '%".$string."%' or b.author_name like '%".$string."%') ";
					// or arai_challenge.json_str like '%".$keyword."%'
				}
				
				if(count($search_arr) > 0) { $searchQuery = implode(" and ",$search_arr); }		 
				
				## Total number of records without filtering
				/* $this->db->select('b.q_id, b.user_id, b.q_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_qa_forum_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_qa_forum_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
					$records = $this->master_model->getRecords("arai_qa_forum b",array('b.is_deleted' => '0'),'',array('b.q_id' => 'DESC'));
				$totalRecords = count($records); */
				
				## Total number of record with filtering		
				/* $totalRecordwithFilter = count($records);
					if($searchQuery != '')
					{
					$this->db->where($searchQuery);
					$this->db->select('b.q_id, b.user_id, b.q_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason');
					$recordsFilter = $this->master_model->getRecords("arai_qa_forum b",array('b.is_deleted' => '0'),'',array('b.q_id' => 'DESC'));
					$totalRecordwithFilter = count($recordsFilter);
					}		
				*/
				
				## Fetch records		
				if($searchQuery != '') { $this->db->where($searchQuery); }	
				$this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', FALSE);
				$this->db->select('b.q_id, b.user_id, b.custum_question_id, b.question, b.tags, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_qa_forum_tags WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName, b.created_on, r.title, r.first_name, r.middle_name, r.last_name');
				$records = $this->master_model->getRecords("arai_qa_forum b",array('b.is_deleted' => '0'),'',array('b.q_id' => 'DESC'));
				
				$data = array();
				$i = 0;
				
				// Count Check
				if(count($records) > 0)
				{			
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex(0);
					// set Header				
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Question ID');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Question');
					$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Posted By');
					$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Tags');
					$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'xOrder');
					$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Featured Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Admin Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Block Status');
					$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Block Reason');					
					$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Posted On');					
					
					// set Row
					$rowCount = 2;					
					foreach($records as $row_val)
					{			
						if($row_val['is_featured'] == '0') { $isFeatured = 'Not Featured'; }else { $isFeatured = 'Featured'; } 
						if($row_val['admin_status'] == '0') { $adminStatus = 'Pending'; } else if($row_val['admin_status'] == '1') { $adminStatus = 'Approved'; } else { $adminStatus = 'Rejected'; } 
						if($row_val['is_block'] == '0') { $isBlock = 'Not Block'; }else { $isBlock = 'Block'; } 
						
						$i++;						
						$objPHPExcel->getActiveSheet()->SetCellValue('A'. $rowCount, $row_val['q_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('B'. $rowCount, $row_val['custum_question_id']);
						$objPHPExcel->getActiveSheet()->SetCellValue('C'. $rowCount, $row_val['question']);
						
						$dispName = $encrptopenssl->decrypt($row_val['title']).' '.$encrptopenssl->decrypt($row_val['first_name']);
						if($row_val['middle_name'] != '') { $dispName .= " ".$encrptopenssl->decrypt($row_val['middle_name']); }
						$dispName .= " ".$encrptopenssl->decrypt($row_val['last_name']);
        				
						$objPHPExcel->getActiveSheet()->SetCellValue('D'. $rowCount, $dispName);
						$objPHPExcel->getActiveSheet()->SetCellValue('E'. $rowCount, $row_val['TagName']);
						$objPHPExcel->getActiveSheet()->SetCellValue('F'. $rowCount, $row_val['xOrder']);
						$objPHPExcel->getActiveSheet()->SetCellValue('G'. $rowCount, $isFeatured);
						$objPHPExcel->getActiveSheet()->SetCellValue('H'. $rowCount, $adminStatus);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'. $rowCount, $isBlock);
						$objPHPExcel->getActiveSheet()->SetCellValue('J'. $rowCount, $row_val['block_reason']);						
						$objPHPExcel->getActiveSheet()->SetCellValue('K'. $rowCount, date("d M, Y h:i a", strtotime($row_val['created_on'])));						
						$rowCount++;						
					} // Foreach End					
				} // Count Check  
				
				// create file name
				header("Content-Type: application/vnd.ms-excel");	
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				header('Content-Disposition: attachment;filename="Qa_forum_'.date("YmdHis").'.xlsx"');
				header('Cache-Control: max-age=0');			
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
				ob_end_clean();
				$objWriter->save('php://output');
				//$objWriter->save($fileName);
				exit;  
				
			}
			
			$data['records'] = ''; 
			$data['module_name'] = 'Question & Answer Forum';
			$data['submodule_name'] = '';	
			$data['middle_content']='questions_answers_forum/qa_forum_listing';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_listing_data()
		{			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$table = 'arai_qa_forum b';
      $column_order = array('b.q_id', 'b.q_id', 'b.custum_question_id', 'b.question', 'b.user_id', 'b.created_on', 'b.admin_status', 'b.xOrder', '(SELECT COUNT(like_id) FROM arai_qa_forum_likes WHERE q_id = b.q_id) AS TotalLikes', '(SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE q_id = b.q_id AND status = 1 AND parent_comment_id = 0) AS TotalComments', '(SELECT COUNT(reported_id) FROM arai_qa_forum_reported WHERE q_id = b.q_id) AS TotalReports', 'b.priority', 'b.is_featured', 'b.is_block', 'r.title', 'r.first_name', 'r.middle_name', 'r.last_name', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMNS FOR SORT
			
      $column_search = array('b.custum_question_id', 'b.question', 'b.created_on', 'IF(b.admin_status = 0, "Pending", IF(b.admin_status = 1, "Approved", IF(b.admin_status = 2, "Rejected", "")))', 'b.xOrder', '(SELECT COUNT(like_id) FROM arai_qa_forum_likes WHERE q_id = b.q_id)', '(SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE q_id = b.q_id AND status = 1 AND parent_comment_id = 0)', '(SELECT COUNT(reported_id) FROM arai_qa_forum_reported WHERE q_id = b.q_id)', 'IF(b.priority=0,"LOW", IF(b.priority=1,"Medium", "High"))', 'IF(b.is_featured=1,"Featured","Non Featured")', 'IF(b.is_block = 0, "Unblock","Block")', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMN FOR SEARCH 
			
      $order = array('b.q_id' => 'DESC'); // DEFAULT ORDER
      
      $WhereForTotal = "WHERE b.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS 
      $Where = "WHERE b.is_deleted = 0 	";
      if($_POST['search']['value']) // DATATABLE SEARCH
      {
        $Where .= " AND (";
        for($i=0; $i<count($column_search); $i++) { $Where .= $column_search[$i]." LIKE '%".( $this->Common_model_sm->custom_safe_string($_POST['search']['value']) )."%' ESCAPE '!' OR "; }
        
				$Where = substr_replace( $Where, "", -3 );
        $Where .= ')';
      }
      
      $Order = ""; //DATATABLE SORT
      if(isset($_POST['order'])) { $explode_arr = explode("AS",$column_order[$_POST['order']['0']['column']]);  $Order = "ORDER BY ".$explode_arr[0]." ".$_POST['order']['0']['dir']; }
      else if(isset($order)) { $Order = "ORDER BY ".key($order)." ".$order[key($order)]; }
      
      $Limit = ""; if ($_POST['length'] != '-1' ) { $Limit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] ); } // DATATABLE LIMIT	
      
      $join_qry = " ";
      $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = b.user_id ";
      //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";
      
      $print_query = "SELECT ".str_replace(" , ", " ", implode(", ", $column_order))." FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
      $Result = $this->db->query($print_query);  
      $Rows = $Result->result_array();
      
      $TotalResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$WhereForTotal));
      $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$Where));
      
      $data = array();
      $no = $_POST['start'];
      
			foreach ($Rows as $Res) 
      {
        $no++;
        $row = array();
        
        $row[] = $no;
        $row[] = $Res['q_id'];
        $row[] = $Res['custum_question_id'];
        $row[] = $Res['question'];
				
				$dispName = $encrptopenssl->decrypt($Res['title']).' '.$encrptopenssl->decrypt($Res['first_name']);
				if($Res['middle_name'] != '') { $dispName .= " ".$encrptopenssl->decrypt($Res['middle_name']); }
				$dispName .= " ".$encrptopenssl->decrypt($Res['last_name']);
        $row[] = $dispName;
        $row[] = $Res['created_on'];
				
				$AdminStatus	= "";				
				if($Res['admin_status'] == '0'){ $chkStatus="selected"; } else{ $chkStatus=""; }
				if($Res['admin_status'] == '1'){ $chkStatus1="selected"; } else{ $chkStatus1=""; }
				if($Res['admin_status'] == '2'){ $chkStatus2="selected"; } else{ $chkStatus2=""; }
				//if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
				//if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;
								
				$AdminStatus .= 	"<input type='hidden' name='q_id' id='q_id' value='".$Res['q_id']."' />
				<select name='status' id='status' class='change-status'>
				<option value='0' ".$chkStatus.">Pending</option>
				<option value='1' ".$chkStatus1.">Approved</option>
				<option value='2' ".$chkStatus2.">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
				$AdminStatus .=	"</select>";
        $row[] = $AdminStatus;
				
				$xOrder = "<input type='text' class='order-class allowd_only_numbers text-center' style='width:50px;' data-id='".$Res['xOrder']."' name='xOrder' id='xOrder' value='". $Res['xOrder']."' onblur='update_sort_order(this.value, ". $Res['q_id'] .")' onkeyup='update_sort_order(this.value, ". $Res['q_id'].")' />";
        $row[] = $xOrder;				
				
				$onclick_like_fun = "show_question_likes('".base64_encode($Res['q_id'])."')";
				$totalLikes = '<a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="'.$onclick_like_fun.'">'.$Res['TotalLikes'].'</a>';
        $row[] = $totalLikes; 
				
				$totalComments = '<a target="_blank" href="'.site_url('xAdmin/questions_answers_forum/view_comments/'.base64_encode($Res['q_id'])).'" data-toggle="tooltip" title="Comments">'.$Res['TotalComments'].'</a>';
        $row[] = $totalComments;
				
				$onclick_report_fun = "show_question_reports('".base64_encode($Res['q_id'])."')";
				$totalReports = '<a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="'.$onclick_report_fun.'">'.$Res['TotalReports'].'</a>';
        $row[] = $totalReports;
								
				$onchange_priority_fun = 'change_question_priority("'.base64_encode($Res['q_id']).'", this.value)';
				if($Res['priority'] == '0'){ $chkPriority0="selected"; } else { $chkPriority0=""; }
				if($Res['priority'] == '1'){ $chkPriority1="selected"; } else { $chkPriority1=""; }
				if($Res['priority'] == '2'){ $chkPriority2="selected"; } else { $chkPriority2=""; }
				$disp_priority = "
					<select name='priority' id='priority' onchange='".$onchange_priority_fun."'>
						<option value='0' ".$chkPriority0.">Low</option>
						<option value='1' ".$chkPriority1.">Medium</option>
						<option value='2' ".$chkPriority2.">High</option>
					</select>";
        $row[] = $disp_priority;
        
				
				$EditBtn = "<a href='".base_url('xAdmin/questions_answers_forum/edit/'.base64_encode($Res['q_id']))."' class='btn btn-success btn-sm' alt='Edit' title='Edit'><i class='fa fa-edit' aria-hidden='true'></i></a>";
				
				if($Res['is_featured'] == '1'){ $textShowFeatured = "Mark as Non Featured"; $FeatureIcon = 'fa-times'; $FeaturedFunText = 'Non Featured'; } 
				else { $textShowFeatured = "Mark as Featured"; $FeatureIcon = 'fa-check-square'; $FeaturedFunText = 'Featured'; } 
				
				$onclick_featured_fun = "featured_notfeatured_question('".base64_encode($Res['q_id'])."', '".$FeaturedFunText."')";
				$FeaturedBtn = '<a href="javascript:void(0)" onclick="'.$onclick_featured_fun.'" class="btn btn-success btn-sm featured-check" title="'.$textShowFeatured.'"><i class="fa '.$FeatureIcon.'" aria-hidden="true"></i></a>';
				
				if($Res['is_block'] == '0') { $textShowBlockStatus = 'Block'; $blockIcon = 'fa-ban'; } else if($Res['is_block'] == '1') { $textShowBlockStatus = 'Unblock'; $blockIcon = 'fa-unlock-alt'; }
				$onclick_block_fun = "block_unblock_question('".base64_encode($Res['q_id'])."', '".$textShowBlockStatus."', '".$Res['custum_question_id']."', '".$this->Common_model_sm->convertValidStringForJs($Res['question'])."')";
				$BlockBtn = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="'.$onclick_block_fun.'" alt="'.$textShowBlockStatus.'" title="'.$textShowBlockStatus.'"><i class="fa '.$blockIcon.'" aria-hidden="true"></i></a>';
				
				$onclick_delete_fun = "delete_question('".base64_encode($Res['q_id'])."')";
				$DeleteBtn = '<a href="javascript:void(0)" class="remove btn btn-success btn-sm" title="Delete" onclick="'.$onclick_delete_fun.'"><i aria-hidden="true" class="fa fa-trash"></i></a>';
				
				$ActionButtons = 	"<span style='white-space:nowrap;'>".$EditBtn." ".$FeaturedBtn." ".$BlockBtn." ".$DeleteBtn."</span>";				
				$row[] = $ActionButtons;
        
				$data[] = $row; 
      }			
      
      $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $TotalResult, //All result count
      "recordsFiltered" => $FilteredResult, //Disp result count
      //"Query" => $print_query,
      "data" => $data,
      );
      //output to json format
      echo json_encode($output);
		}
		
		public function view_comments($q_id = 0)
		{			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$question_data = array();
			if($q_id == '0') { redirect(site_url('xAdmin/questions_answers_forum')); }
			else
			{
				$q_id = base64_decode($q_id);
				$question_data = $this->master_model->getRecords("arai_qa_forum", array("q_id" => $q_id, "is_deleted"=>0));
				if(count($question_data) == 0) { redirect(site_url('xAdmin/questions_answers_forum')); }				
			}
			
			$data['q_id'] = $q_id;
			$data['question_data'] = $question_data;
			$data['module_name'] = 'Question & Answer Forum';
			$data['submodule_name'] = '';	
			$data['middle_content']='questions_answers_forum/qa_comment_listing';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_parent_comment_listing_data()
		{			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$q_id = $this->input->post('q_id');
			$table = 'arai_qa_forum_comments q';
      $column_order = array('q.comment_id', 'q.disp_comment_id', 'q.user_id', 'q.created_on', 'q.comment', '(SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE parent_comment_id = q.comment_id AND status = 1) AS TotalReplies', 'q.is_block', 'q.total_likes', 'r.title', 'r.first_name', 'r.middle_name', 'r.last_name', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMNS FOR SORT
			
      $column_search = array('q.disp_comment_id', 'q.created_on', 'q.comment', '(SELECT COUNT(comment_id) FROM arai_qa_forum_comments WHERE parent_comment_id = q.comment_id AND status = 1)', 'q.is_block', 'q.total_likes', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMN FOR SEARCH 
			
      $order = array('q.comment_id' => 'DESC'); // DEFAULT ORDER
      
      $WhereForTotal = "WHERE q.status = 1 AND q.parent_comment_id = 0 AND q.q_id = '".$q_id."' "; //DEFAULT WHERE CONDITION FOR ALL RECORDS 
      $Where = "WHERE q.status = 1 AND q.parent_comment_id = 0 AND q.q_id = '".$q_id."' 	";
      if($_POST['search']['value']) // DATATABLE SEARCH
      {
        $Where .= " AND (";
        for($i=0; $i<count($column_search); $i++) { $Where .= $column_search[$i]." LIKE '%".( $this->Common_model_sm->custom_safe_string($_POST['search']['value']) )."%' ESCAPE '!' OR "; }
        
				$Where = substr_replace( $Where, "", -3 );
        $Where .= ')';
      }
      
      $Order = ""; //DATATABLE SORT
      if(isset($_POST['order'])) { $explode_arr = explode("AS",$column_order[$_POST['order']['0']['column']]);  $Order = "ORDER BY ".$explode_arr[0]." ".$_POST['order']['0']['dir']; }
      else if(isset($order)) { $Order = "ORDER BY ".key($order)." ".$order[key($order)]; }
      
      $Limit = ""; if ($_POST['length'] != '-1' ) { $Limit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] ); } // DATATABLE LIMIT	
      
      $join_qry = " ";
      $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = q.user_id ";
      //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";
      
      $print_query = "SELECT ".str_replace(" , ", " ", implode(", ", $column_order))." FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
      $Result = $this->db->query($print_query);  
      $Rows = $Result->result_array();
      
      $TotalResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$WhereForTotal));
      $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$Where));
      
      $data = array();
      $no = $_POST['start'];
      
			foreach ($Rows as $Res) 
      {
        $no++;
        $row = array();
        			
        $row[] = $no;
        $row[] = $Res['disp_comment_id'];
        
				$dispName = $encrptopenssl->decrypt($Res['title']).' '.$encrptopenssl->decrypt($Res['first_name']);
				if($Res['middle_name'] != '') { $dispName .= " ".$encrptopenssl->decrypt($Res['middle_name']); }
				$dispName .= " ".$encrptopenssl->decrypt($Res['last_name']);
        $row[] = $dispName;
				
        $row[] = $Res['created_on'];
        $row[] = $Res['comment'];
				
				$TotalReplies = '<a target="_blank" href="'.site_url('xAdmin/questions_answers_forum/view_reply/'.base64_encode($Res['comment_id'])).'" data-toggle="tooltip" title="Reply">'.$Res['TotalReplies'].'</a>';
        $row[] = $TotalReplies;
				
				if($Res['is_block'] == '0') { $textShowBlockStatus = 'Block'; $blockIcon = 'fa-ban'; } else if($Res['is_block'] == '1') { $textShowBlockStatus = 'Unblock'; $blockIcon = 'fa-unlock-alt'; }
				$onclick_block_fun = "block_unblock_comment('".base64_encode($Res['comment_id'])."', '".$textShowBlockStatus."', '".$Res['disp_comment_id']."', '".$this->Common_model_sm->convertValidStringForJs($Res['comment'])."')";
				$BlockBtn = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="'.$onclick_block_fun.'" alt="'.$textShowBlockStatus.'" title="'.$textShowBlockStatus.'"><i class="fa '.$blockIcon.'" aria-hidden="true"></i></a>';
				$row[] = $BlockBtn;
				
				$onclick_like_fun = "show_comment_likes('".base64_encode($Res['comment_id'])."')";
				$totalLikes = '<a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="'.$onclick_like_fun.'">'.$Res['total_likes'].'</a>';
        $row[] = $totalLikes;
				
				$data[] = $row; 
      }			
      
      $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $TotalResult, //All result count
      "recordsFiltered" => $FilteredResult, //Disp result count
      "Query" => $print_query,
      "data" => $data,
      );
      //output to json format
      echo json_encode($output);
		}
		
		public function view_reply($comment_id = 0)
		{			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$comment_data = array();
			if($comment_id == '0') { redirect(site_url('xAdmin/questions_answers_forum')); }
			else
			{
				$comment_id = base64_decode($comment_id);
				$comment_data = $this->master_model->getRecords("arai_qa_forum_comments", array("comment_id" => $comment_id, "status"=>1));
				if(count($comment_data) == 0) { redirect(site_url('xAdmin/questions_answers_forum')); }				
			}
			
			$data['comment_id'] = $comment_id;
			$data['comment_data'] = $comment_data;
			$data['module_name'] = 'Question & Answer Forum';
			$data['submodule_name'] = '';	
			$data['middle_content']='questions_answers_forum/qa_reply_listing';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function get_comment_reply_listing_data()
		{			
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$comment_id = $this->input->post('comment_id');
			$table = 'arai_qa_forum_comments q';
      $column_order = array('q.comment_id', 'q.disp_comment_id', 'q.comment', 'q.created_on', 'q.user_id', 'q.is_block', 'q.total_likes', 'r.title', 'r.first_name', 'r.middle_name', 'r.last_name', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMNS FOR SORT
			
      $column_search = array('q.disp_comment_id', 'q.comment', 'q.created_on', 'q.user_id', 'q.is_block', 'q.total_likes', 'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMN FOR SEARCH 
			
      $order = array('q.comment_id' => 'DESC'); // DEFAULT ORDER
      
      $WhereForTotal = "WHERE q.status = 1 AND q.parent_comment_id = '".$comment_id."' "; //DEFAULT WHERE CONDITION FOR ALL RECORDS 
      $Where = "WHERE q.status = 1 AND q.parent_comment_id = '".$comment_id."' ";
      if($_POST['search']['value']) // DATATABLE SEARCH
      {
        $Where .= " AND (";
        for($i=0; $i<count($column_search); $i++) { $Where .= $column_search[$i]." LIKE '%".( $this->Common_model_sm->custom_safe_string($_POST['search']['value']) )."%' ESCAPE '!' OR "; }
        
				$Where = substr_replace( $Where, "", -3 );
        $Where .= ')';
      }
      
      $Order = ""; //DATATABLE SORT
      if(isset($_POST['order'])) { $explode_arr = explode("AS",$column_order[$_POST['order']['0']['column']]);  $Order = "ORDER BY ".$explode_arr[0]." ".$_POST['order']['0']['dir']; }
      else if(isset($order)) { $Order = "ORDER BY ".key($order)." ".$order[key($order)]; }
      
      $Limit = ""; if ($_POST['length'] != '-1' ) { $Limit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] ); } // DATATABLE LIMIT	
      
      $join_qry = " ";
      $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = q.user_id ";
      //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";
      
      $print_query = "SELECT ".str_replace(" , ", " ", implode(", ", $column_order))." FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
      $Result = $this->db->query($print_query);  
      $Rows = $Result->result_array();
      
      $TotalResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$WhereForTotal));
      $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0],$table." ".$join_qry,$Where));
      
      $data = array();
      $no = $_POST['start'];
      
			foreach ($Rows as $Res) 
      {
        $no++;
        $row = array();
        			
        $row[] = $no;
        $row[] = $Res['disp_comment_id'];
        $row[] = $Res['comment'];
        $row[] = $Res['created_on'];
        
				$dispName = $encrptopenssl->decrypt($Res['title']).' '.$encrptopenssl->decrypt($Res['first_name']);
				if($Res['middle_name'] != '') { $dispName .= " ".$encrptopenssl->decrypt($Res['middle_name']); }
				$dispName .= " ".$encrptopenssl->decrypt($Res['last_name']);
        $row[] = $dispName;
				
				if($Res['is_block'] == '0') { $textShowBlockStatus = 'Block'; $blockIcon = 'fa-ban'; } else if($Res['is_block'] == '1') { $textShowBlockStatus = 'Unblock'; $blockIcon = 'fa-unlock-alt'; }
				$onclick_block_fun = "block_unblock_reply('".base64_encode($Res['comment_id'])."', '".$textShowBlockStatus."', '".$Res['disp_comment_id']."', '".$this->Common_model_sm->convertValidStringForJs($Res['comment'])."')";
				$BlockBtn = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="'.$onclick_block_fun.'" alt="'.$textShowBlockStatus.'" title="'.$textShowBlockStatus.'"><i class="fa '.$blockIcon.'" aria-hidden="true"></i></a>';
				$row[] = $BlockBtn;  
				
				$data[] = $row; 
      }			
      
      $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $TotalResult, //All result count
      "recordsFiltered" => $FilteredResult, //Disp result count
      "Query" => $print_query,
      "data" => $data,
      );
      //output to json format
      echo json_encode($output);
		}
		
		public function setOrder(){
			
			$q_id 			= $this->input->post('q_id'); 
			$xOrder 		= $this->input->post('xOrder'); 
			$csrf_test_name = $this->security->get_csrf_hash(); 
			$updateQuery 	= $this->master_model->updateRecord('arai_qa_forum',array('xOrder'=>$xOrder),array('q_id' => $q_id));
			
			$status = "Order Successfully Updated.";
			$jsonData = array("msg" => $status, "token" => $csrf_test_name);
			echo json_encode($jsonData); 
		}		 
		
		/* public function viewDetails(){
			
			$encrptopenssl =  New Opensslencryptdecrypt();
			$id=$this->input->post('id');
			$this->db->join('arai_registration','arai_webinar_attendence.user_id=arai_registration.user_id','left');	
			$records = $this->master_model->getRecords("webinar_attendence",array("webinar_attendence.w_id" => $id, "webinar_attendence.status" => 'Active'));
			//echo $this->db->last_query();
			
			$table = '';
			if(count($records) > 0){
			$table .= '<table class="table table-bordered">
			<tr>
			<td><b>Fullname</b></td>
			<td><b>Email</b></td>							
			</tr>';
			foreach($records as $details){
			$firstname 	= $encrptopenssl->decrypt($details['first_name']);
			$lastname 	= $encrptopenssl->decrypt($details['last_name']);
			$middlename = $encrptopenssl->decrypt($details['middle_name']);
			$fullname 	= $firstname." ".$middlename." ".$lastname;
			$email 		= $encrptopenssl->decrypt($details['email']);		
			$table .= '<tr>
			<td>'.ucwords($fullname).'</td>
			<td>'.$email.'</td>
			</tr>';
			}		
			$table .= '</table>';
			} else {
			
			$table = "<p class='text-center'><b>No Record Found</b></p>";
			}
			
			echo $table;
			
			}
		*/
		
		public function edit($q_id)
		{	 				
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$q_id = base64_decode($q_id);
			
			// Load Library
			$this->load->library('upload');	
			$user_id = $this->session->userdata('admin_id');		
			
			$response_data = $this->master_model->getRecords("arai_qa_forum", array("q_id" => $q_id, "is_deleted"=>0));
			if(count($response_data) == 0) { redirect(site_url('xAdmin/questions_answers_forum')); }
			
			$data['qa_image_error'] = $data['tags_error'] = $error_flag = '';
			$file_upload_flag = 0;
			if(isset($_POST) && count($_POST) > 0)
			{
				//$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');			
				$this->form_validation->set_rules('question', 'Question', 'required|trim|xss_clean',array('required' => 'Please enter the %s'));			
								
            if(!isset($_POST['tags']))
				{
					$data['tags_error'] = 'Please select the Tags';		
					$error_flag = 1;
				}
				
				if($this->form_validation->run() && $error_flag == '')
				{
					$postArr = $this->input->post();
					
					if($_FILES['qa_image']['name'] != "") //UPLOAD Question BANNER IMAGE
					{
						$qa_image = $this->Common_model_sm->upload_single_file("qa_image", array('png','jpg','jpeg','gif'), "qa_image_".date("YmdHis"), "./uploads/qa_image", "png|jpeg|jpg|gif");
						if($qa_image['response'] == 'error')
						{
							$data['qa_image_error'] = $qa_image['message'];
							$file_upload_flag = 1;
						}
						else if($qa_image['response'] == 'success')
						{
							$add_data['qa_image'] = $qa_image['message'];	
							/* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
						}
					}
					
					if($file_upload_flag == 0)
					{
						$add_data['question'] = $this->input->post('question');
						$add_data['tags'] = implode(",",$this->input->post('tags'));
						$add_data['updated_on'] = date('Y-m-d H:i:s');
						
						$tags_other = trim($this->security->xss_clean($this->input->post('tags_other')));
						if(isset($tags_other) && $tags_other != "")
						{
							$add_data['tags_other'] = trim($this->security->xss_clean($tags_other));
						}
						else { $add_data['tags_other'] = ''; }
						
						$this->master_model->updateRecord('arai_qa_forum',$add_data,array('q_id' => $q_id));
						//echo $this->db->last_query(); exit;
						
						// Log Data Added 
						$filesData 			= $_FILES;
						$json_data 			= array_merge($postArr,$filesData);
						$json_encode_data 	= json_encode($json_data);				
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
						'user_id' 			=> $user_id,
						'action_name' 		=> "Update Question Technology Wall By Admin",
						'module_name'		=> 'Question & Answer Forum Technology Wall Admin',
						'store_data'		=> $json_encode_data,
						'ip_address'		=> $ipAddr,
						'createdAt'			=> $createdAt
						);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						$this->session->set_flashdata('success','Question has been successfully updated');
						redirect(site_url('xAdmin/questions_answers_forum'));
					}
				}
			}			
			
			$data['tag_data'] = $this->master_model->getRecords("arai_qa_forum_tags", array("status" => 'Active'), '', array('tag_name'=>'ASC'));
			
			$data['form_data'] = $response_data;
			$data['module_name'] = 'Question & Answer Forum';
			$data['submodule_name'] = '';
			$data['middle_content'] ='questions_answers_forum/edit';
			$this->load->view('admin/admin_combo',$data);			
		}
		
		public function change_question_priority_ajax()
		{
			// Create Object
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$result = array();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$result['token'] = $csrf_test_name;
			
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = $this->input->post('q_id');
				$priority = $this->input->post('priority');
							
				$updateQuery = $this->master_model->updateRecord('arai_qa_forum',array('priority'=>$priority),array('q_id' => base64_decode($q_id)));
				//$result['qry'] = $this->db->last_query();
				
				// Log Data Added
				$user_id = $this->session->userdata('admin_id');
				$arrData = array('id' => $q_id, 'priority' => $priority, 'admin_id' => $user_id);
				$json_encode_data 	= json_encode($arrData);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails = array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Question Priority Updated By Admin",
				'module_name'	=> 'Question & Answer Forum',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$result['status'] = 'success';
			}
			else
			{
				$result['status'] = 'error';
			}
			echo json_encode($result);
		}
		
		public function updateStatus()
		{
			// Create Object
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$reject_reason = $this->input->post('reject_reason');
			
			//$email_info = $this->get_mail_data_blog($id);
			
			//echo "<pre>";print_r($this->input->post());die();			
			
			$updateQuery = $this->master_model->updateRecord('arai_qa_forum',array('admin_status'=>$status),array('q_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Question Status Updated By Admin",
			'module_name'	=> 'Question & Answer Forum',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			if($status == '2') //Rejected
			{				
				$updateQuery1 = $this->master_model->updateRecord('arai_qa_forum',array('reject_reason'=>$reject_reason),array('q_id' => $id)); 
				
				//START : SEND MAIL TO USER WHEN Question IS REJECTED - blog_mail_to_user_on_blog_rejected
				/* $email_send='';
				$slug = $encrptopenssl->encrypt('blog_mail_to_user_on_blog_rejected');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
				
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
					'[USER_NAME]',
					'[Blog_Title]',
					'[Reason]'											
					]; 
					$rep_array = [
					$email_info['USER_NAME'],
					$email_info['Blog_Title'],
					$reject_reason								
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);					
					
					$info_array=array(
					'to'		=>	$email_info['USER_EMAIL'],
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				}					
				 *///END : SEND MAIL TO USER WHEN Question IS REJECTED - blog_mail_to_user_on_blog_rejected
				
				// SEND MAIL TO CHALLENGE OWNER
				/* $slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_rejected'); 
					
					$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$content = '';
					
					$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
					
					if(count($webinar_mail) > 0)
					{					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$webinar_reason = $webinar_data[0]['webinar_reason'];
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $webinar_reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				} */
				
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);				
			} 
			else if($status == '1') //Approved
			{
				//START : SEND MAIL TO USER WHEN Question IS APPROVED - blog_mail_to_user_on_blog_approved
				/* $email_send='';
				$slug = $encrptopenssl->encrypt('blog_mail_to_user_on_blog_approved');
				$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
				$sub_content = '';
				
				if(count($subscriber_mail) > 0)
				{								
					$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
					$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
					$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
					$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
					$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
					
					$arr_words = [
					'[USER_NAME]',
					'[Blog_Title]',
					'[LINK_OF_POST]'											
					]; 
					$rep_array = [
					$email_info['USER_NAME'],
					$email_info['Blog_Title'],
					$email_info['LINK_OF_POST']								
					];
					$sub_content = str_replace($arr_words, $rep_array, $desc);					
					
					$info_array=array(
					'to'		=>	$email_info['USER_EMAIL'],
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
					
					$other_infoarray	=	array('content' => $sub_content); 
					$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);						
				} */
				//END : SEND MAIL TO USER WHEN Question IS APPROVED	- blog_mail_to_user_on_blog_approved
				
				$updateQuery1 = $this->master_model->updateRecord('arai_qa_forum',array('reject_reason'=>''),array('q_id' => $id));  
				
				// SEND MAIL TO CHALLENGE OWNER
				/* $slug = $encrptopenssl->encrypt('webinar_mail_to_presenter_on_application_accepted'); 
					
					$webinar_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$content = '';
					
					$webinar_data = $this->master_model->getRecords("webinar", array("w_id" => $id));
					
					if(count($webinar_mail) > 0)
					{					
					$userId = $webinar_data[0]['user_id'];
					$webinarTitle = ucfirst($webinar_data[0]['webinar_name']);
					$webinar_date = $webinar_data[0]['webinar_date'];
					$webinar_id = $webinar_data[0]['webinar_id'];
					$webinarTime = date('h:i A', strtotime($webinar_data[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_data[0]['webinar_end_time']));
					$registration_link = $webinar_data[0]['registration_link'];
					$reason = '';
					$webinarLink	= '<a href="'.base_url('webinar/viewDetail/'.base64_encode($id)).'">View</a>';
					$day = date('l', strtotime($webinar_date));
					
					$webinarDate 	= date('d-m-Y', strtotime($webinar_date));
					
					$receiver_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					
					$fullname = ucfirst($encrptopenssl->decrypt($receiver_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($receiver_details[0]['last_name']));
					$to_email = $encrptopenssl->decrypt($receiver_details[0]['email']);
					//$to_email = 'vicky.kuwar@esds.co.in';
					
					$subject 	 	= $encrptopenssl->decrypt($webinar_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($webinar_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($webinar_mail[0]['from_email']);				
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);	
					$phoneNo  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);		
					
					$arr_words = ['[FULLNAME]', '[WEBINARNAME]', '[WEBINARID]', '[DAY]', '[DATE]', '[TIME]', '[REASON]', '[LINK_STATUSPAGE]'];
					$rep_array = [$fullname, $webinarTitle, $webinar_id, $day, $webinarDate, $webinarTime, $reason, $webinarLink];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
					//email admin sending code 
					$info_arr=array(
					'to'		=>	$to_email, //$to_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
					
					$other_info=array('content'=>$content); 
					
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					//return true;
				} */
				
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData);
			} 
			else 
			{
				$updateQuery1 = $this->master_model->updateRecord('arai_qa_forum',array('reject_reason'=>''),array('q_id' => $id));  
				$jsonData = array("token" => $csrf_test_name, "status" => 'done');
				echo json_encode($jsonData); 
			}
		}
		
		/* public function changeStatus(){
			
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Publish'){
			
			$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Challenge Status Change - 2",
			'module_name'	=> 'Challenge',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);	
			
			$this->session->set_flashdata('success','Publish status successfully changed');
			redirect(base_url('xAdmin/challenge'));	
			
			} else if($value == 'Block'){
			
			$updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id)); 
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Challenge Status Change",
			'module_name'	=> 'Challenge',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$this->session->set_flashdata('success','Publish status successfully changed');
			redirect(base_url('xAdmin/challenge'));		
			}
			
			}  
		*/
		
		public function block_unblock_question()// BLOCK/UNBLOCK QUESTION
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$q_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupQuestionBlockReason = trim($this->security->xss_clean($this->input->post('popupQuestionBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('admin_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					//START : SEND MAIL TO ALL MEMBERS WHO ANSWERED/COMMENTED ON QUESTION WHEN ADMIN BLOCKED THE Question - qa_mail_to_user_on_question_blocked        
					$email_info = $this->get_mail_data_qa($q_id);
						
					$email_send='';
					$slug = $encrptopenssl->encrypt('qa_mail_to_user_on_question_blocked');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
						
					if(count($subscriber_mail) > 0)
					{								
						$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$this->db->group_by('qac.user_id');
						$this->db->join('arai_registration r', 'r.user_id = qac.user_id', 'INNER', FALSE);
						$all_commentor_users = $this->master_model->getRecords("arai_qa_forum_comments qac", array("qac.q_id" => $q_id, 'r.is_deleted'=>'0', 'r.admin_approval'=>'yes', 'r.status'=>'Active'), 'qac.user_id, r.title, r.first_name, r.middle_name, r.last_name, r.email ');
												
						if(count($all_commentor_users) > 0)
						{
							foreach($all_commentor_users as $row_val)
							{								
								$title = $encrptopenssl->decrypt($row_val['title']);
								$first_name = $encrptopenssl->decrypt($row_val['first_name']);
								$middle_name = $encrptopenssl->decrypt($row_val['middle_name']);
								$last_name = $encrptopenssl->decrypt($row_val['last_name']);
								
								$user_name = $title." ".$first_name." ".$last_name;
								$user_email = $encrptopenssl->decrypt($row_val['email']);
								
								$info_array=array(
								'to' => $user_email, 
								'cc' => '',
								'from' => $fromadmin,
								'subject' => $subject_title,
								'view' => 'common-file'
								);
								
								$arr_words = ['[QUESTION]', '[REASON]', '[ANSWERED_NAME]']; 
								$rep_array = [$email_info['QUESTION'], $popupQuestionBlockReason, $user_name];
								$sub_content = str_replace($arr_words, $rep_array, $desc);
						
								$other_infoarray	=	array('content' => $sub_content); 
								$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
							}
						}
					}       
					//END : SEND MAIL TO ALL MEMBERS WHO ANSWERED/COMMENTED ON QUESTION WHEN ADMIN BLOCKED THE Question - qa_mail_to_user_on_question_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupQuestionBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum',$up_data,array('q_id' => $q_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Block Question Answer Forum Technology Wall By Admin",
				'module_name'		=> 'Question Answer Forum Technology Wall Admin',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function block_unblock_comment()// BLOCK/UNBLOCK COMMENT
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$comment_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupQuestionBlockReason = trim($this->security->xss_clean($this->input->post('popupQuestionBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('admin_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					$comment_data = $this->master_model->getRecords("arai_qa_forum_comments", array("comment_id" => $comment_id));
					//START : SEND MAIL TO MEMBERS WHO ANSWERED/COMMENTED ON QUESTION WHEN ADMIN BLOCKED THE ANSWER/COMMENT - qa_mail_to_answered_user_on_answer_blocked        
					$email_info = $this->get_mail_data_qa($comment_data[0]['q_id'], $comment_data[0]['user_id']);
					//print_r($email_info); exit;
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('qa_mail_to_answered_user_on_answer_blocked');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
						
					if(count($subscriber_mail) > 0)
					{								
						$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$this->db->group_by('qac.user_id');
						$this->db->join('arai_registration r', 'r.user_id = qac.user_id', 'INNER', FALSE);
						$all_commentor_users = $this->master_model->getRecords("arai_qa_forum_comments qac", array("qac.q_id" => $q_id, 'r.is_deleted'=>'0', 'r.admin_approval'=>'yes', 'r.status'=>'Active'), 'qac.user_id, r.title, r.first_name, r.middle_name, r.last_name, r.email ');
												
						$info_array=array(
						'to' => $email_info['COMMENTING_USER_EMAIL'], 
						'cc' => '',
						'from' => $fromadmin,
						'subject' => $subject_title,
						'view' => 'common-file'
						);
						
						$arr_words = ['[ANSWERED_NAME]', '[QUESTION]', '[REASON]']; 
						$rep_array = [$email_info['COMMENTING_USER_NAME'], $email_info['QUESTION'], $popupQuestionBlockReason];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
				
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
					}       
					//END : SEND MAIL TO MEMBERS WHO ANSWERED/COMMENTED ON QUESTION WHEN ADMIN BLOCKED THE ANSWER/COMMENT - qa_mail_to_answered_user_on_answer_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupQuestionBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum_comments',$up_data,array('comment_id' => $comment_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Block Comment - Question Answer Forum Technology Wall By Admin",
				'module_name'		=> 'Question Answer Forum Technology Wall Admin',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function block_unblock_reply()// BLOCK/UNBLOCK REPLY
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			if(isset($_POST) && count($_POST) > 0)
			{
				$comment_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));	
				$popupQuestionBlockReason = trim($this->security->xss_clean($this->input->post('popupQuestionBlockReason')));	
				$type = trim($this->security->xss_clean($this->input->post('type')));	
				$user_id = $this->session->userdata('admin_id');
				$result['flag'] = "success";				
				
				if($type == 'Block') 
				{ 
					$comment_data = $this->master_model->getRecords("arai_qa_forum_comments", array("comment_id" => $comment_id));
					//START : SEND MAIL TO MEMBERS WHO REPLIED TO ANSWER/COMMENT ON QUESTION WHEN ADMIN BLOCKED THE REPLY - qa_mail_to_replied_user_on_reply_blocked        
					$email_info = $this->get_mail_data_qa($comment_data[0]['q_id'], $comment_data[0]['user_id']);
					//print_r($email_info); exit;
					
					$email_send='';
					$slug = $encrptopenssl->encrypt('qa_mail_to_replied_user_on_reply_blocked');
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$sub_content = '';
						
					if(count($subscriber_mail) > 0)
					{								
						$subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
						$sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						
						$this->db->group_by('qac.user_id');
						$this->db->join('arai_registration r', 'r.user_id = qac.user_id', 'INNER', FALSE);
						$all_commentor_users = $this->master_model->getRecords("arai_qa_forum_comments qac", array("qac.q_id" => $q_id, 'r.is_deleted'=>'0', 'r.admin_approval'=>'yes', 'r.status'=>'Active'), 'qac.user_id, r.title, r.first_name, r.middle_name, r.last_name, r.email ');
												
						$info_array=array(
						'to' => $email_info['COMMENTING_USER_EMAIL'], 
						'cc' => '',
						'from' => $fromadmin,
						'subject' => $subject_title,
						'view' => 'common-file'
						); 
						
						$arr_words = ['[REPLIED_NAME]', '[QUESTION]', '[REASON]']; 
						$rep_array = [$email_info['COMMENTING_USER_NAME'], $email_info['QUESTION'], $popupQuestionBlockReason];
						$sub_content = str_replace($arr_words, $rep_array, $desc);
				
						$other_infoarray	=	array('content' => $sub_content); 
						$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
					}       
					//END : SEND MAIL TO MEMBERS WHO REPLIED TO ANSWER/COMMENT ON QUESTION WHEN ADMIN BLOCKED THE REPLY - qa_mail_to_replied_user_on_reply_blocked
					
					$up_data['is_block'] = 1;				    
					$up_data['block_reason'] = $popupQuestionBlockReason;				    
				}
				else if($type == 'Unblock') { $up_data['is_block'] = 0; $up_data['block_reason'] = ''; }
				$up_data['updated_on'] = date('Y-m-d H:i:s');
				$this->master_model->updateRecord('arai_qa_forum_comments',$up_data,array('comment_id' => $comment_id));
				
				$postArr = $this->input->post();
				// Log Data Added 
				$json_encode_data 	= json_encode($postArr);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 			=> $user_id,
				'action_name' 		=> "Block Reply - Question Answer Forum Technology Wall By Admin",
				'module_name'		=> 'Question Answer Forum Technology Wall Admin',
				'store_data'		=> $json_encode_data,
				'ip_address'		=> $ipAddr,
				'createdAt'			=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
			}
			else
			{
				$result['flag'] = "error";
			}
			
			$result['token'] = $this->security->get_csrf_hash();
			echo json_encode($result);
		}
		
		public function featured()
		{			
			$csrf_test_name = $this->security->get_csrf_hash();
			$id = base64_decode($this->input->post('id'));
			// $value = ucfirst($this->uri->segment(5));
			//echo $id;
			$c_data = $this->master_model->getRecords("arai_qa_forum", array("q_id" => $id));
			if($c_data[0]['is_featured'] == "0"){ $is_featured = '1';	} else { $is_featured = '0'; }
			
			$updateQuery = $this->master_model->updateRecord('arai_qa_forum',array('is_featured'=>$is_featured),array('q_id' => $id));
			//echo $this->db->last_query();
			$c_res = $this->master_model->getRecords("arai_qa_forum", array("q_id" => $id));		
			
			if($c_res[0]['is_featured'] == "0") { $text = 'Featured';	} else { $text = 'Not Featured'; }
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('q_id' => $id, 'status' => $text, 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr = $this->get_client_ip();
			$createdAt = date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' => $user_id,
			'action_name' => "QA Featured Status - 3",
			'module_name'	=> 'Question & Answer Forum',
			'store_data' => $json_encode_data,
			'ip_address' => $ipAddr,
			'createdAt' => $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("c_status" => $is_featured, "u_featured" => $text,"token" => $csrf_test_name);
			echo json_encode($jsonData);			
		}
		
		public function deleted()
		{			
			$user_id = $this->session->userdata('admin_id');
			$csrf_test_name = $this->security->get_csrf_hash();
			$id 	= base64_decode($this->input->post('id'));	 
			$updateQuery = $this->master_model->updateRecord('arai_qa_forum',array('is_deleted'=>1),array('q_id' => $id));
			
			// Log Data Added
			$user_id = $this->session->userdata('admin_id');
			$arrData = array('q_id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);	
			$json_encode_data 	= json_encode($arrData);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails = array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Question Deleted From Admin",
			'module_name'	=> 'Question & Answer Forum',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$jsonData = array("token" => $csrf_test_name);
			echo json_encode($jsonData);
		}
		
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
		public function get_mail_data_qa($q_id,$commenting_user_id=false)
		{ 		
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('r.title, r.first_name, r.middle_name, r.last_name, r.email, qa.custum_question_id, qa.question, qa.created_on, r.user_id');
			$this->db->join('arai_registration r','r.user_id = qa.user_id','LEFT', FALSE);
			$qa_details= $this->master_model->getRecords('arai_qa_forum qa',array('qa.q_id'=>$q_id));
			
			if(count($qa_details)) 
			{				
				$user_arr = array();
				if(count($qa_details))
				{						
					foreach($qa_details as $row_val)
					{								
						$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
						$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
						$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
						$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
						$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
						$row_val['user_id'] = $row_val['user_id'];
						$user_arr[] = $row_val;
					}					
				}
				$user_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
				$user_id = $user_arr[0]['user_id'];
				$user_email = $user_arr[0]['email'];
				$question = $qa_details[0]['question'];
				$custum_question_id = $qa_details[0]['custum_question_id'];
				$question_date =date('d/m/Y' ,strtotime( $qa_details[0]['created_on'] ));
				$detail_link = site_url('questions_answers_forum/QaDetails/').base64_encode($q_id);
				$hyperlink_detail = '<a href='.$detail_link.' target="_blank">here</a>';
				
				$email_array=array(
				'QUESTION_OWNER_NAME'=>$user_name,
				'QUESTION_OWNER_EMAIL'=>$user_email,
				'QUESTION_OWNER_ID'=>$user_id,
				'QUESTION'=>$question,
				'QUESTION_ID_DISPLAY'=>$custum_question_id,
				'QUESTION_POSTED_DATE'=>$question_date,
				'ADMIN_EMAIL'=>'sagar.matale@esds.co.in',
				'LINK_OF_QUESTION'=>$hyperlink_detail
				);
				
				if($commenting_user_id!="")
				{
					$this->db->select('title, first_name, middle_name, last_name, email');
					$commenting_user_data = $this->master_model->getRecords("registration",array('user_id'=>$commenting_user_id));
				    
					$commenting_user_arr = array();
					if(count($commenting_user_data))
					{	    					
						foreach($commenting_user_data as $row_val)
						{		    						
							$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
							$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
							$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
							$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
							$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
							$commenting_user_arr[] = $row_val;
						}    					
					}
				
					$commenting_user_name = $commenting_user_arr[0]['title']." ".$commenting_user_arr[0]['first_name']." ".$commenting_user_arr[0]['last_name'];
					$email_array['COMMENTING_USER_NAME']=$commenting_user_name;
					$email_array['COMMENTING_USER_EMAIL']=$commenting_user_arr[0]['email'];
				}
				
				return $email_array ;
			}
		}
		
	}		   	