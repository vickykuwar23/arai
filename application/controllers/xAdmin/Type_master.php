<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Type_master
Author : Vicky K
*/

class Type_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index()
    {
	
		//$this->db->join('categories','categories.id=type_master.category_id','left');	
		$response_data = $this->master_model->getRecords("type_master",'','',array('tid'=>'DESC'));
		//print_r($response_data);die();
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Type_Master';	
    	$data['middle_content']='type-master/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('category_id[]', 'Category', 'required');
		$this->form_validation->set_rules('name', 'Type Master Name', 'required|min_length[4]|xss_clean');
		//$data]['category_data'] = $this->master_model->getRecords("arai_registration_usersubcategory",'','',array('id'=>'DESC'));
		//$res_cat = $this->master_model->array_sorting($this->master_model->getRecords('arai_registration_usersubcategory',array('status'=>"Active")), array('subcat_id'),'sub_catname');

		$res_cat = $this->master_model->array_sorting($this->master_model->getRecords('arai_registration_usersubcategory'), array('subcat_id'),'sub_catname');	
		
		if($this->form_validation->run())
		{	
			$category_id = $this->input->post('category_id');
			$name = $this->input->post('name');
			//print_r($this->input->post());
			 $impCat = implode(",",$category_id);
			
			$insertArr = array( 'name' => $name, 'category_id' => $impCat);			
			$insertQuery = $this->master_model->insertRecord('type_master',$insertArr);
			//echo $this->db->last_query();die();
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Type master successfully created');
				redirect(base_url('xAdmin/type_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/type_master/add'));
			}
		}
		$data['category_data'] = $res_cat;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Type_Master';	
    	$data['middle_content']='type-master/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$tid = base64_decode($id);	
		
		//$res_cat= $this->master_model->array_sorting($this->master_model->getRecords('arai_registration_usersubcategory',array('status'=>"Active")), array('subcat_id'),'sub_catname');			
		
		
		$res_cat= $this->master_model->array_sorting($this->master_model->getRecords('arai_registration_usersubcategory'), array('subcat_id'),'sub_catname');	
		$rec = $this->master_model->getRecords("type_master",array('tid'=>$tid));
	
		$data['cap_record'] = $rec;
		
		
		$this->form_validation->set_rules('category_id[]', 'Category', 'required');
		$this->form_validation->set_rules('name', 'Type Master Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	//echo "<pre>";print_r($this->input->post());
			$category_id = $this->input->post('category_id');
			$name = $this->input->post('name');			
			$impCat = implode(",",$category_id);//die();
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'category_id' => $impCat, 'name' => $name, 'updatedAt' => $updateAt );			
			$updateQuery = $this->master_model->updateRecord('type_master',$updateArr,array('tid' => $tid));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Type master successfully updated');
				redirect(base_url('xAdmin/type_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/type_master/edit/'.$id));
			}
		}
		//echo "<pre>";print_r($rec);die();
		$data['category_data'] = $res_cat;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Type_Master';
        $data['middle_content']='type-master/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('type_master',array('status'=>$value),array('tid' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/type_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('type_master',array('status'=>$value),array('tid' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/type_master'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('type_master',array('is_deleted'=>1),array('tid' => $id));
		 $this->session->set_flashdata('success','Type master successfully deleted');
		 redirect(base_url('xAdmin/type_master'));	
		 
	 }


}