<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Access Type
Author : Vicky K
*/

class Useraccesstype extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$this->db->select('user_access_type.id, user_access_type.user_access_name, user_access_type.status, identity_group.identity_group_name');
		$this->db->join('identity_group','user_access_type.identity_group_id=identity_group.id','left');	
		$response_data = $this->master_model->getRecords("user_access_type",'','');
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['identity_group_name'] = $encrptopenssl->decrypt($row_val['identity_group_name']);
				$row_val['id']   = $row_val['id'];
				$row_val['user_access_name']   = $encrptopenssl->decrypt($row_val['user_access_name']);
				$res_arr[] = $row_val;
			}
			
		}
		//echo "<pre>";print_r($res_arr);die();
		$data['records'] = $res_arr;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_TYPE_ACCESS';	
    	$data['middle_content']='user_access_type/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$this->db->select('id, identity_group_name');
		$this->db->group_by('identity_group_name'); 
		$identity_group = $this->master_model->getRecords("identity_group",'','',array('id'=>'ASC'));
		//$identity_group = $this->master_model->getRecords("identity_group", array('status' => "Active"), 'GROUP BY' => "identity_group_name");
		
		$res_arr = array();
		if(count($identity_group)){	
						
			foreach($identity_group as $row_val){		
				$row_val['id'] = $row_val['id'];		
				$row_val['identity_group_name'] = $encrptopenssl->decrypt($row_val['identity_group_name']);
				$res_arr[] = $row_val;
			}
			
		}
		$data['identity_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('identity_group', 'Idenity Group', 'required');
		$this->form_validation->set_rules('user_access', 'User Access Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$identity_group 	= $this->input->post('identity_group');
			$user_access 		= $encrptopenssl->encrypt($this->input->post('user_access'));
			
			$insertArr = array( 'identity_group_id' => $identity_group, 'user_access_name' => $user_access );			
			$insertQuery = $this->master_model->insertRecord('user_access_type',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','User Access Type successfully created');
				redirect(base_url('xAdmin/useraccesstype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/useraccesstype/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_TYPE_ACCESS';	
    	$data['middle_content']='user_access_type/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$this->db->select('id, identity_group_name');
		$this->db->group_by('identity_group_name'); 
		$identity_group = $this->master_model->getRecords("identity_group",'','',array('id'=>'ASC'));
		
		$res_arr = array();
		$res_data = array();
		
		if(count($identity_group)){	
						
			foreach($identity_group as $row_val){		
				$row_val['id'] = $row_val['id'];		
				$row_val['identity_group_name'] = $encrptopenssl->decrypt($row_val['identity_group_name']);
				$res_arr[] = $row_val;
			}
			
		}
			
		
		$useraccess_data = $this->master_model->getRecords("user_access_type", array('id' => $id));        
		//echo $this->db->last_query();
		if(count($useraccess_data) > 0){
			$row_val['user_access_name'] = $encrptopenssl->decrypt($useraccess_data[0]['user_access_name']);
			$row_val['identity_group_id'] = $useraccess_data[0]['identity_group_id'];
			$res_data[] = $row_val;
		}
		
		$data['identity_data'] = $res_arr;	
		$data['useraccess_data'] = $res_data;
		
		//echo "<pre>";print_r($data['identity_data']);
		//echo "<pre>";print_r($data['useraccess_data']); die(); 
		
		// Check Validation
		$this->form_validation->set_rules('identity_group', 'Idenity Group', 'required');
		$this->form_validation->set_rules('user_access', 'User Access Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$identity_group 	= $this->input->post('identity_group');
			$user_access 		= $encrptopenssl->encrypt($this->input->post('user_access'));
			
			//echo "<pre>";print_r($this->input->post());die();
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'identity_group_id' => $identity_group, 'user_access_name' => $user_access, 'updatedAt' => $updateAt );			
			$updateQuery = $this->master_model->updateRecord('user_access_type',$updateArr,array('id' => $id));
			
			if($updateQuery > 0){
				$this->session->set_flashdata('success','User Access Type successfully updated');
				redirect(base_url('xAdmin/useraccesstype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/useraccesstype/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'User_TYPE_ACCESS';
        $data['middle_content']='user_access_type/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('user_access_type',array('status'=>$value),array('subcat_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/useraccesstype'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('user_access_type',array('status'=>$value),array('subcat_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/useraccesstype'));		
		 }
		 
	 }
	 
	


}