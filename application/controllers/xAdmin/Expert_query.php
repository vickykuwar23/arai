<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class  : Expert Query
Author : Vicky K
*/

class Expert_query extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(18);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		$this->db->select('r1.user_id,r1.email,r1.first_name,r1.middle_name,r1.last_name,expert_connect_query.id,
						expert_connect_query.expert_subject,expert_connect_query.expert_question,expert_connect_query.expert_remark,expert_connect_query.createdAt,r2.user_id as ToUserId,r2.email as ToMail,r2.first_name as ToFname,r2.middle_name as ToMname,r2.last_name as ToLname,connect_to_expert');
		$this->db->join('arai_registration r1','r1.user_id=expert_connect_query.user_id','left');
		$this->db->join('arai_registration r2','r2.user_id=expert_connect_query.connect_to_expert','left');
		$this->db->order_by('expert_connect_query.id','desc');
		$response_data = $this->master_model->getRecords("expert_connect_query");
		// echo "<pre>";print_r($response_data);die;
    	
    	$data['records'] = $response_data; 
		$data['module_name'] = 'Expert_query';
		$data['submodule_name'] = '';	
    	$data['middle_content']='expert-query/index';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	 public function viewDetails(){
	 	$encrptopenssl =  New Opensslencryptdecrypt();	
		 $id=$this->input->post('id'); 
		 					$this->db->select('expert_subject,expert_question,expert_remark,r2.user_id as ToUserId,r2.email as ToMail,r2.first_name as ToFname,r2.middle_name as ToMname,r2.last_name as ToLname,connect_to_expert');
	 						$this->db->join('arai_registration r2','r2.user_id=expert_connect_query.connect_to_expert','left');
		$records = $this->master_model->getRecords("expert_connect_query",array("id" => $id));
		// print_r($response_data);die;
		$expert_subject = $records[0]['expert_subject'];
		$expert_question = $records[0]['expert_question'];
		$expert_remark = $records[0]['expert_remark'];

		$expert_fullname='-';
		if ($records[0]['connect_to_expert']!='') {
			$expert_fullname = ucfirst($encrptopenssl->decrypt($records[0]['ToFname']))." ".ucfirst($encrptopenssl->decrypt($records[0]['ToMname']))." ".ucfirst($encrptopenssl->decrypt($records[0]['ToLname']));
		}
		
		$table = '<table class="table table-bordered">
						<tr>
							<td width="15%"><b>Subject</b></td>
							<td>'.$expert_subject.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Question</b></td>
							<td>'.$expert_question.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Remarks(if any)</b></td>
							<td>'.$expert_remark.'</td>
						</tr>
							<tr>
							<td width="15%"><b>Name Of Expert</b></td>
							<td>'.$expert_fullname.'</td>
						</tr>
				  </table>';
		echo $table;
		 
	 }
	 
}