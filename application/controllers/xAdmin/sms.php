<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : SMS
Author : Vicky K
*/

class Sms extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index($value='')
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
    	$response_data = $this->master_model->getRecords("sms");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['sms_title'] = $encrptopenssl->decrypt($row_val['sms_title']);
				$row_val['sms_details'] = $encrptopenssl->decrypt($row_val['sms_details']);	
				$res_arr[] = $row_val;
			}
			
		}
		
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'SMS';	
    	$data['middle_content']='sms/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('sms_title', 'SMS Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('sms_title', 'SMS Description', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$sms_title = $encrptopenssl->encrypt($this->input->post('sms_title'));
			$sms_desc  = $encrptopenssl->encrypt($this->input->post('sms_desc'));
			
			$insertArr = array( 'sms_title' => $sms_title, 'sms_details' => $sms_desc);			
			$insertQuery = $this->master_model->insertRecord('sms',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','SMS content successfully created');
				redirect(base_url('xAdmin/sms'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/sms/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'SMS';
        $data['middle_content']='sms/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$sms_data = $this->master_model->getRecords("sms", array('id' => $id));
		$data_arr = array();
		if(count($sms_data) > 0){
			
			$row_val['sms_title'] 			= $encrptopenssl->decrypt($sms_data[0]['sms_title']);
			$row_val['sms_details'] 		= $encrptopenssl->decrypt($sms_data[0]['sms_details']);
			$data_arr[] = $row_val;
			
		}
        // Check Validation
		$this->form_validation->set_rules('sms_title', 'SMS Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('sms_desc', 'SMS Description', 'required|xss_clean');
		if($this->form_validation->run())
		{	
			$sms_title = $encrptopenssl->encrypt($this->input->post('sms_title'));
			$sms_desc  = $encrptopenssl->encrypt($this->input->post('sms_desc'));			
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'sms_title' => $sms_title, 'sms_details' => $sms_desc, 'updatedAt' => $updateAt);	
			
			$updateQuery = $this->master_model->updateRecord('sms',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','SMS content successfully updated');
				redirect(base_url('xAdmin/sms'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/sms/edit/'.$id));
			}
		}
		$data['sms_data'] = $data_arr;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'SMS';
        $data['middle_content']='sms/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('sms',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/sms'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('sms',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/sms'));		
		 }
		 
	 }
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('cms',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','CMS page successfully deleted');
		 redirect(base_url('xAdmin/cms'));	
		 
	 }*/


}