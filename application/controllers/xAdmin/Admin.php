<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin for admin basic function
Author : Vicky K
*/

class Admin extends CI_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->helper('captcha');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		$this->load->library("session");	
    }
	
	/*public function index()
	{   
		
		$data['msg'] = '';
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		
		if ($this->form_validation->run() != FALSE)
		{
			$inputCaptcha = $this->input->post('captcha');
            $sessCaptcha  = $this->session->userdata('captchaCode');
			
            if($inputCaptcha === $sessCaptcha || 1){
		
				$user_name 		= mysqli_real_escape_string($this->db->conn_id,$this->input->post('username'));
				$user_password 	= mysqli_real_escape_string($this->db->conn_id,$this->input->post('password')); 
				
				$u_name = $encrptopenssl->encrypt($user_name);
				$p_name = $encrptopenssl->encrypt(md5($user_password));
				
				$where = array("admin_username" =>$u_name,"admin_password" => $p_name);
				$user_exist = $this->master_model->getRecordCount('user_master',$where);
				
					if($user_exist)
					{ 
						$where = array("admin_username" => $u_name,"admin_password" => $p_name );
						$user_all_details = $this->master_model->getRecords("user_master",$where);
						
								if($user_all_details[0]['status']=="Active")
								{
									
								$getPermission   = 	$this->master_model->getRecords("admin_type",array("id" => $user_all_details[0]['admin_roles']));
								$session_data['user_id'] 		= $user_all_details[0]['user_id'];
								$session_data['admin_username'] = $encrptopenssl->decrypt($user_all_details[0]['admin_username']);
								$session_data['admin_name'] 	= $encrptopenssl->decrypt($user_all_details[0]['admin_name']);
								$session_data['admin_email'] 	= $encrptopenssl->decrypt($user_all_details[0]['admin_email']);
								$session_data['is_admin'] 		= $user_all_details[0]['admin_roles'];
								//$session_data['permission']		= $getPermission[0]['permission'];
								
 								$this->session->set_userdata($session_data);
								$this->session->set_userdata($session_data);
								
								redirect(base_url('xAdmin/admin/dashboard'));
								}
								else
								{ 
									$data['msg'] = 'Sorry, your adminstrator has blocked access to this User';
								}
					}
					else
					{
						$data['msg'] = 'Invalid Username or Password!';
						
					}
			
				 }else{
                $data['msg'] = 'Invalid Captcha';
                
             }
		
		}
		
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];

		$data["page_title"] ="Admin Login" ; 
		$this->load->view('admin/login', $data);
	}*/
	
	public function index()
	{   
		if ($this->session->userdata('admin_id')!='')  {
			redirect(base_url('xAdmin/admin/dashboard'));
		}
		$data['msg'] = '';
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('captcha', 'Capcha', 'trim|required|callback_check_captcha_code|xss_clean');
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		if ($this->form_validation->run() != FALSE)
		{		
			$user_name 		= mysqli_real_escape_string($this->db->conn_id,$this->input->post('username'));
			$user_password 	= mysqli_real_escape_string($this->db->conn_id,$this->input->post('password')); 
			$inputCaptcha 	= $this->input->post('captcha');
			
			//$where = array("admin_username" =>$user_name,"admin_password" => md5($user_password));
			//$user_exist = $this->master_model->getRecordCount('user_master',$where);
			
			$u_name = $encrptopenssl->encrypt($user_name);
			$p_name = $encrptopenssl->encrypt(md5($user_password));
				
			$where = array("admin_username" =>$u_name,"admin_password" => $p_name);
			$user_exist = $this->master_model->getRecordCount('user_master',$where);
			
				if($user_exist)
				{ 
					$where = array("admin_username" => $u_name,"admin_password" => $p_name );
					$user_all_details = $this->master_model->getRecords("user_master",$where);
					 
							if($user_all_details[0]['status']=="Active")
							{
								$session_data['admin_id'] = $user_all_details[0]['user_id'];
								$session_data['admin_username'] = $encrptopenssl->decrypt($user_all_details[0]['admin_username']);
								$session_data['admin_name'] 	= $encrptopenssl->decrypt($user_all_details[0]['admin_name']);
								$session_data['admin_email'] 	= $encrptopenssl->decrypt($user_all_details[0]['admin_email']);
								$session_data['is_admin'] = $user_all_details[0]['admin_roles'];
								//$session_data['added_on'] = $user_all_details[0]['added_on'];
								//$session_data['module_perm'] = explode(',',$user_all_details[0]['module_permission']);
								$this->session->set_userdata($session_data);
								$this->session->set_userdata($session_data);								
								redirect(base_url('xAdmin/admin/dashboard'));
							}
							else
							{ 
								$data['msg'] = 'Sorry, your adminstrator has blocked access to this User';
							}
				}
				else
				{
					$data['msg'] = 'Invalid Username or Password!';
					
				}
		
			}
			
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];

		$data["page_title"] ="Admin Login" ; 
		$this->load->view('admin/login', $data);
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }
	
	public function check_captcha_code($code)
	{
      // check if captcha is set -
		if($code == '')
		{
			$this->form_validation->set_message('check_captcha_code', '%s is required.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($code == '' || $_SESSION["captchaCode"] != $code)
		{  
			$this->form_validation->set_message('check_captcha_code', 'Invalid %s.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($_SESSION["captchaCode"] == $code)
		{   
			$this->session->unset_userdata("captchaCode");
			$this->session->set_userdata("captchaCode", rand(1,100000));
		
			return true;
		}
		
	}

	
	//showing dashboard
	public function dashboard()
	{   
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		
		// Total Open Challenge
		$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
		$this->db->where("r.is_deleted","0");
		$open_challenge = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active', "c.challenge_status" => 'Approved'));
		//echo "<br />1==>".$this->db->last_query();
		
		// Total Pending Challenge	
		$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
		$this->db->where("r.is_deleted","0");
		$pending_challenge = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active', "c.challenge_status" => 'Pending'));
		//echo "<br />2==>".$this->db->last_query();
		
		// Total Closed Challenge
		$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
		$this->db->where("r.is_deleted","0");
		$closed_challenge = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active', "c.challenge_status" => 'Closed'));
		//echo "<br />3==>".$this->db->last_query();
		
		// Total Register Users		
		$total_users = $this->master_model->getRecords("registration", array("status" => 'Active'));
		
		$data=array('middle_content' => 'dashboard/index','page_title'=>"Dashboard");
		$data['module_name'] = 'Dashboard';
		$data['submodule_name'] = '';
		$data['open_challenge'] 	= count($open_challenge);
		$data['pending_challenge'] 	= count($pending_challenge);
		$data['closed_challenge'] 	= count($closed_challenge);
		$data['total_users'] 		= count($total_users);
		$this->load->view('admin/admin_combo',$data);
	}
	
	public function change_password()
	{
		$encrptopenssl =  New Opensslencryptdecrypt();
		
	 	$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password','trim|required|min_length[8]|matches[crm_password]');
		$this->form_validation->set_rules('crm_password', 'Confirm Password', 'trim|required|min_length[3]');
		
		if ($this->form_validation->run())
		{
			
			$old_password 	= $this->input->post('old_password');
			$user_password 	= $this->input->post('new_password');	
			$con_password 	= $encrptopenssl->encrypt(md5($this->input->post('crm_password')));	
			//echo $this->session->userdata('admin_username');
			$where = array("admin_username" => $encrptopenssl->encrypt($this->session->userdata('admin_username')),
			"admin_password" => $encrptopenssl->encrypt(md5($old_password)));
			
			$user_all_details = $this->master_model->getRecords("user_master",$where);
			//echo $this->db->last_query();die();
			if($encrptopenssl->encrypt(md5($user_password)) == $encrptopenssl->encrypt(md5($old_password)))
			{	
				$this->session->set_flashdata('error','No change in password.');
				redirect('xAdmin/admin/change_password');		
			}
			else
			{  
			
				if(!empty($user_all_details) && $encrptopenssl->encrypt(md5($old_password)) == $user_all_details[0]['admin_password'])
				{
					if($encrptopenssl->encrypt(md5($user_password)) == $con_password)
					{
							$update_array= array("admin_password"=> $encrptopenssl->encrypt(md5($user_password)));							
							if($admin_details = $this->master_model->updateRecord('user_master',$update_array,
							array('user_id'=>"'".$this->session->userdata('admin_id')."'")))
							{ 
								
								$this->session->set_flashdata('success','Password has been reset successfully');
							  	redirect('xAdmin/admin/change_password');	
							}
							else
							{ 
								$this->session->set_flashdata('error','Error while resetting password.');
								redirect('xAdmin/admin');	
							}
					}
					else
					{	
						$this->session->set_flashdata('error','Wrong Password Confirmation');
						redirect('xAdmin/admin/change_password');	
					}
				}
				else
				{  
					$this->session->set_flashdata('error','Incorrect old password');
					redirect('xAdmin/admin/change_password');	
				}
			}
		}
		
		$data=array('middle_content' => 'change_password');
		$data['module_name'] = 'Change_Password';
		$data['submodule_name'] = '';
		$this->load->view('admin/admin_combo',$data);
	
		//$this->load->view('admin/change_password');
	}
	
	//contact enquiries added from front panel show and details
	public function contact_enq()
	{
		$contact_info=$this->master_model->getRecords("tbl_contact_enq");
		$data = array('middle_content'=>'contact_view','contact_info'=>$contact_info);
		$this->load->view('admin/admin_combo',$data);
	}
	
	//admin logout
	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		redirect(base_url('xAdmin/admin'));	
 		/*$session_data['user_id'] = '';
		$session_data['admin_username'] = '';
		$session_data['admin_email'] = '';
		$session_data['is_admin'] = '';
		$this->session->set_userdata($session_data);
		
		
		
		$admin_data = array( 'user_id','admin_username','admin_email');
	 	$this->session->unset_userdata($admin_data);
		   
			//for security audit
			if (isset($_SERVER['HTTP_COOKIE'])) 
			{
				$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
				foreach($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
			}
		}*/
   		// Finally, destroy the session.
		
		
		//$this->db->where('browser',$_SERVER['HTTP_USER_AGENT']);
		//$this->master_model->deleteRecord("login_master",'ip',$_SERVER['REMOTE_ADDR']);
		//redirect($this->config->item('admin_url').'admin');	
		redirect(base_url('xAdmin/admin'));	
	}
}
