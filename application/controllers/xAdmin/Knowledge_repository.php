<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Knowledge_repository extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(13);
        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {
        error_reporting(0);

        // load excel library
        $this->load->library('excel');

        if (isset($_POST['export']) && !empty($_POST['export'])) {
            /* print_r($_POST); exit; */
            //error_reporting(0);
            $encrptopenssl = new Opensslencryptdecrypt();
            $response      = array();

            $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

            ## Search
            $search_arr  = array();
            $searchQuery = "";
            if ($keyword != '') {
                $string       = $encrptopenssl->encrypt($keyword);
                $search_arr[] = " (	k.title_of_the_content like '%" . $keyword . "%' or k.blog_titkr_descriptionle like '%" . $string . "%' or k.kr_id_disp like '%" . $keyword . "%') ";
                // or arai_challenge.json_str like '%".$keyword."%'
            }

            if (count($search_arr) > 0) {$searchQuery = implode(" and ", $search_arr);}

            ## Total number of records without filtering
            /* $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason, (SELECT GROUP_CONCAT(technology_name SEPARATOR ", ") FROM arai_blog_technology_master WHERE FIND_IN_SET(id, b.technology_ids) AND status = "Active" AND technology_name != "other") AS TechnologyName, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_blog_tags_master WHERE FIND_IN_SET(id, b.tags) AND status = "Active" AND tag_name != "other") AS TagName');
            $records = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
            $totalRecords = count($records); */

            ## Total number of record with filtering
            /* $totalRecordwithFilter = count($records);
            if($searchQuery != '')
            {
            $this->db->where($searchQuery);
            $this->db->select('b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.xOrder, b.is_featured, b.admin_status, b.is_block, b.block_reason');
            $recordsFilter = $this->master_model->getRecords("arai_blog b",array('b.is_deleted' => '0'),'',array('b.blog_id' => 'DESC'));
            $totalRecordwithFilter = count($recordsFilter);
            }
             */

            ## Fetch records
            if ($searchQuery != '') {$this->db->where($searchQuery);}
            $this->db->select('k.technology_other,k.tag_other,k.xOrder,rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_description, k.kr_type, k.kr_type_other, k.tags,  k.author_name, k.xOrder, k.is_featured, k.admin_status, k.is_block, k.block_reason,(SELECT GROUP_CONCAT(technology_name SEPARATOR ",") FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR ", ") FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id, k.tags) AND status = "Active" AND tag_name != "other") AS TagName');
            $this->db->join('arai_knowledge_repository_type_master rt', 'k.kr_type = rt.id', 'LEFT', false);
            $records = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0'), '', array('k.kr_id' => 'DESC'));

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                // set Header
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'KR ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'KR Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'KR Description');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Type');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Other Type');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Tags');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Other Tag');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Technology');
                $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Other Technology');
                $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Author Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'xOrder');
                $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Featured Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Admin Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Block Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Block Reason');

                // set Row
                $rowCount = 2;
                foreach ($records as $row_val) {

                    if ($row_val['is_featured'] == '0') {$isFeatured = 'Not Featured';} else { $isFeatured = 'Featured';}
                    if ($row_val['admin_status'] == '0') {$adminStatus = 'Pending';} else if ($row_val['admin_status'] == '1') {$adminStatus = 'Approved';} else { $adminStatus = 'Rejected';}
                    if ($row_val['is_block'] == '0') {$isBlock = 'Not Block';} else { $isBlock = 'Block';}

                    $i++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['kr_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['kr_id_disp']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['title_of_the_content']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, htmlspecialchars_decode($row_val['kr_description']));
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row_val['type_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row_val['kr_type_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row_val['TagName']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row_val['tag_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row_val['DispTechnology']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row_val['technology_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row_val['author_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row_val['xOrder']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $isFeatured);
                    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $adminStatus);
                    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $isBlock);
                    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row_val['block_reason']);
                    $rowCount++;
                } // Foreach End
            } // Count Check

            // create file name
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="Blog_' . date("YmdHis") . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        $data['records']        = '';
        $data['module_name']    = 'Knowledge repository';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'knowledge_repository/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function get_kr_data()
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $table        = 'arai_knowledge_repository k';
        $column_order = array(
            'k.kr_id',
            'k.kr_id',
            'k.kr_id_disp',
            'k.title_of_the_content',
            'k.created_on',
            'k.admin_status',
            'k.xOrder',
            '(SELECT COUNT(like_id) FROM arai_knowledge_repository_likes WHERE kr_id = k.kr_id) AS TotalLikes',
            '(SELECT COUNT(d_id) FROM arai_knowledge_download_log WHERE kr_id = k.kr_id ) AS TotalDownloads',
            '(SELECT COUNT(reported_id) FROM arai_knowledge_repository_reported WHERE kr_id = k.kr_id) AS TotalReports',
            'k.is_featured',
            'k.is_block',
            'r.title', 'r.first_name', 'r.middle_name', 'r.last_name',
            'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)',
        ); //SET COLUMNS FOR SORT

        $column_search = array(
            'k.kr_id_disp',
            'k.title_of_the_content',
            'k.created_on',
            'IF(k.admin_status = 0, "Pending", IF(k.admin_status = 1, "Approved", IF(k.admin_status = 2, "Rejected", "")))',
            'k.xOrder',
            '(SELECT COUNT(like_id) FROM arai_knowledge_repository_likes WHERE kr_id = k.kr_id)',
            '(SELECT COUNT(d_id) FROM arai_knowledge_download_log WHERE kr_id = k.kr_id )',
            '(SELECT COUNT(reported_id) FROM arai_knowledge_repository_reported WHERE kr_id = k.kr_id)',
            'IF(k.is_featured=1,"Featured","Non Featured")',
            'IF(k.is_block = 0, "Unblock","Block")',
            'CONCAT(r.title_decrypt, " ", r.first_name_decrypt, " ", r.middle_name_decrypt, " ", r.last_name_decrypt)'); //SET COLUMN FOR SEARCH

        $order = array('k.kr_id' => 'DESC'); // DEFAULT ORDER

        $WhereForTotal = "WHERE k.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
        $Where         = "WHERE k.is_deleted = 0 	";

        if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);
            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = k.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_order)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        $Rows        = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));

        $data = array();
        $no   = $_POST['start'];

        foreach ($Rows as $Res) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $Res['kr_id'];
            $row[] = $Res['kr_id_disp'];
            $row[] = $Res['title_of_the_content'];

            $dispName = $encrptopenssl->decrypt($Res['title']) . ' ' . $encrptopenssl->decrypt($Res['first_name']);
            if ($Res['middle_name'] != '') {$dispName .= " " . $encrptopenssl->decrypt($Res['middle_name']);}
            $dispName .= " " . $encrptopenssl->decrypt($Res['last_name']);
            // $row[] = $dispName;
            $row[] = $Res['created_on'];

            $AdminStatus = "";
            if ($Res['admin_status'] == '0') {$chkStatus = "selected";} else { $chkStatus = "";}
            if ($Res['admin_status'] == '1') {$chkStatus1 = "selected";} else { $chkStatus1 = "";}
            if ($Res['admin_status'] == '2') {$chkStatus2 = "selected";} else { $chkStatus2 = "";}
            //if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            //if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            /* $AdminStatus .= "<input type='hidden' name='kr_id' id='kr_id' value='" . $Res['kr_id'] . "' />
            <select name='status' id='status' class='change-status'>
            <option value='0' " . $chkStatus . ">Pending</option>
            <option value='1' " . $chkStatus1 . ">Approved</option>
            <option value='2' " . $chkStatus2 . ">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
            $AdminStatus .= "</select>";
            $row[] = $AdminStatus;*/

            $dropdown = "";

            if ($Res['admin_status'] == '0'): $chkStatus  = "selected";else:$chkStatus  = "";endif;
            if ($Res['admin_status'] == '1'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($Res['admin_status'] == '2'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;
            //if($Res['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            //if($Res['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            $dropdown .= "<input type='hidden' name='kr_id' id='kr_id' value='" . $Res['kr_id'] . "' />
			<select name='status' id='status' class='change-status'>
			<option value='0' " . $chkStatus . ">Pending</option>
			<option value='1' " . $chkStatus1 . ">Approved</option>
			<option value='2' " . $chkStatus2 . ">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
            $dropdown .= "</select>";
            $row[] = $dropdown;

            $xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='" . $Res['xOrder'] . "' name='xOrder' id='xOrder' value='" . $Res['xOrder'] . "' onblur='update_sort_order(this.value, " . $Res['kr_id'] . ")' onkeyup='update_sort_order(this.value, " . $Res['kr_id'] . ")' />";

            $row[] = $xOrder;

            $onclick_like_fun = "show_blog_likes('" . base64_encode($Res['kr_id']) . "')";
            $getTotalLikes    = $Res['TotalLikes'];
            $disp_likes       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';
            $row[]            = $disp_likes;

            $onclick_download_fun = "show_downloads('" . base64_encode($Res['kr_id']) . "')";
            $getTotalDownl        = $Res['TotalDownloads'];
            $disp_downloads       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_download_fun . '">' . $getTotalDownl . '</a></div>';
            $row[]                = $disp_downloads;

            $onclick_report_fun = "show_blog_reports('" . base64_encode($Res['kr_id']) . "')";
            $getTotalReports    = $Res['TotalReports'];
            $disp_reported      = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalReports . '</a></div>';
            $row[]              = $disp_reported;

            /*$onchange_priority_fun = 'change_question_priority("' . base64_encode($Res['q_id']) . '", this.value)';
            if ($Res['priority'] == '0') {$chkPriority0 = "selected";} else { $chkPriority0 = "";}
            if ($Res['priority'] == '1') {$chkPriority1 = "selected";} else { $chkPriority1 = "";}
            if ($Res['priority'] == '2') {$chkPriority2 = "selected";} else { $chkPriority2 = "";}
            $disp_priority = "
            <select name='priority' id='priority' onchange='" . $onchange_priority_fun . "'>
            <option value='0' " . $chkPriority0 . ">Low</option>
            <option value='1' " . $chkPriority1 . ">Medium</option>
            <option value='2' " . $chkPriority2 . ">High</option>
            </select>";
            $row[] = $disp_priority;*/

            $EditBtn = "<a href='" . base_url('xAdmin/knowledge_repository/edit/' . base64_encode($Res['kr_id'])) . "' > <button class='btn btn-success btn-sm'>View/Edit</button> </a>";

            if ($Res['is_featured'] == '1') {$textShowFeatured = "Featured";} else { $textShowFeatured = "Not Featured";}
            $FeaturedBtn = "<a href='javascript:void(0)' data-id='" . $Res['kr_id'] . "' class='btn btn-success btn-sm featured-check' id='change-stac-" . $Res['kr_id'] . "'>" . $textShowFeatured . "</a>";

            /* $onclick_featured_fun = "featured_notfeatured_question('" . base64_encode($Res['kr_id']) . "', '" . $FeaturedFunText . "')";
            $FeaturedBtn          = '<a href="javascript:void(0)" onclick="' . $onclick_featured_fun . '" class="btn btn-success btn-sm featured-check" title="' . $textShowFeatured . '"><i class="fa ' . $FeatureIcon . '" aria-hidden="true"></i></a>';*/

            if ($Res['is_block'] == '0') {$textShowBlockStatus = 'Block';} else if ($Res['is_block'] == '1') {$textShowBlockStatus = 'Unblock';}
            $onclick_block_fun = "block_unblock('" . base64_encode($Res['kr_id']) . "', '" . $textShowBlockStatus . "', '" . $Res['kr_id_disp'] . "', '" . $Res['title_of_the_content'] . "')";
            $BlockBtn          = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="' . $onclick_block_fun . '" >' . $textShowBlockStatus . '</a>';

            /*if ($Res['is_block'] == '0') {
            $textShowBlockStatus = 'Block';
            $blockIcon           = 'fa-ban';} else if ($Res['is_block'] == '1') {
            $textShowBlockStatus = 'Unblock';
            $blockIcon           = 'fa-unlock-alt';}
            $onclick_block_fun = "block_unblock_question('" . base64_encode($Res['kr_id']) . "', '" . $textShowBlockStatus . "', '" . $Res['kr_id_disp'] . "', '" . $this->Common_model_sm->convertValidStringForJs($Res['title_of_the_content']) . "')";
            $BlockBtn          = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="' . $onclick_block_fun . '" alt="' . $textShowBlockStatus . '" title="' . $textShowBlockStatus . '"><i class="fa ' . $blockIcon . '" aria-hidden="true"></i></a>';*/
            $DeleteBtn = "<a href='javascript:void(0)' class='remove' data-id='" . $Res['kr_id'] . "'><button class='btn btn-success btn-sm'>Remove</button></a>";

            /* $onclick_delete_fun = "delete_question('" . base64_encode($Res['kr_id']) . "')";
            $DeleteBtn          = '<a href="javascript:void(0)" class="remove btn btn-success btn-sm" title="Delete" onclick="' . $onclick_delete_fun . '"><i aria-hidden="true" class="fa fa-trash"></i></a>';*/

            $ActionButtons = "<span style='white-space:nowrap;'>" . $EditBtn . " " . $FeaturedBtn . " " . $BlockBtn . " " . $DeleteBtn . "</span>";
            $row[]         = $ActionButtons;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $TotalResult, //All result count
            "recordsFiltered" => $FilteredResult, //Disp result count
            //"Query" => $print_query,
            "data"            => $data,
        );
        //output to json format
        // echo "<pre>"; print_r($output);die;
        echo json_encode($output);
    }

    public function get_kr_data_old()
    {
        //error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $response = array();
        ## Read value
        $draw            = @$this->input->post('draw');
        $start           = @$this->input->post('start');
        $rowperpage      = @$this->input->post('length'); // Rows display per page
        $columnIndex     = @$this->input->post('order')[0]['column']; // Column index
        $columnName      = @$this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = @$this->input->post('order')[0]['dir']; // asc or desc
        $searchValue     = @$this->input->post('search')['value']; // Search value
        $keyword         = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

        ## Search
        $search_arr  = array();
        $searchQuery = "";
        if ($keyword != '') {
            $string       = $encrptopenssl->encrypt($keyword);
            $search_arr[] = " (	arai_challenge.challenge_id like '%" . $keyword . "%' or
				arai_challenge.challenge_title like '%" . $string . "%' or
				arai_challenge.company_name like '%" . $string . "%' or
				arai_challenge.challenge_status like '%" . $keyword . "%' or
				arai_challenge.contact_person_name like '%" . $string . "%' or
				arai_challenge_contact.mobile_no like '%" . $keyword . "%' or
				arai_challenge_contact.office_no like '%" . $keyword . "%' or
				arai_challenge_contact.email_id like '%" . $keyword . "%') ";
            // or arai_challenge.json_str like '%".$keyword."%'
        }

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $this->db->select('k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content,  k.author_name, k.xOrder, k.is_featured, k.admin_status, k.is_deleted, k.is_block, k.created_on');
        $records = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0'), '', array('k.kr_id' => 'DESC'));

        $totalRecords = count($records);

        ## Total number of record with filtering
        $totalRecordwithFilter = count($records);
        /* if($searchQuery != '')
        {
        $this->db->where($searchQuery);
        $this->db->select('arai_registration.title,arai_registration.first_name,arai_registration.middle_name,arai_registration.last_name,arai_registration.email,arai_webinar.w_id,arai_webinar.webinar_id,arai_webinar.webinar_name,arai_webinar.webinar_date,arai_webinar.is_featured,arai_webinar.cost_price,arai_webinar.webinar_cost_type,arai_webinar.admin_status,arai_webinar.xOrder');
        $this->db->join('arai_registration','arai_webinar.user_id = arai_registration.user_id','LEFT',FALSE);
        $recordsFilter = $this->master_model->getRecords("webinar",array('webinar.is_deleted' => '0'),'',array('webinar.w_id' => 'DESC'));
        $totalRecordwithFilter = count($recordsFilter);
        } */

        ## Fetch records
        if ($searchQuery != '') {$this->db->where($searchQuery);}
        $this->db->select('k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.author_name, k.xOrder, k.is_featured, k.admin_status, k.is_deleted, k.is_block, k.created_on');
        $records = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0'), '', array('k.kr_id' => 'DESC'), $start, $rowperpage);

        $data = array();
        $i    = 0;
        foreach ($records as $row_val) {
            $i++;
            $row_val['kr_id']                = $row_val['kr_id'];
            $row_val['kr_id_disp']           = $row_val['kr_id_disp'];
            $row_val['title_of_the_content'] = ucfirst($row_val['title_of_the_content']);
            $row_val['author_name']          = ucfirst($row_val['author_name']);
            $row_val['xOrder']               = $row_val['xOrder'];
            $row_val['is_featured']          = $row_val['is_featured'];
            $row_val['admin_status']         = $row_val['admin_status'];
            $row_val['is_deleted']           = $row_val['is_deleted'];
            $row_val['is_block']             = $row_val['is_block'];
            $row_val['created_on']           = $row_val['created_on'];

            if ($row_val['admin_status'] == '0') {$textShowAdminStatus = "Pending";} else if ($row_val['admin_status'] == '1') {$textShowAdminStatus = "Approved";} else if ($row_val['admin_status'] == '2') {$textShowAdminStatus = "Rejected";}

            $onclick_like_fun = "show_blog_likes('" . base64_encode($row_val['kr_id']) . "')";
            $getTotalLikes    = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $row_val['kr_id']), 'like_id');
            $disp_likes       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_like_fun . '">' . $getTotalLikes . '</a></div>';

            $onclick_download_fun = "show_downloads('" . base64_encode($row_val['kr_id']) . "')";
            $getTotalDownl        = $this->master_model->getRecordCount('arai_knowledge_download_log', array('kr_id' => $row_val['kr_id']), 'd_id');
            $disp_downloads       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Likes" onclick="' . $onclick_download_fun . '">' . $getTotalDownl . '</a></div>';

            $onclick_report_fun = "show_blog_reports('" . base64_encode($row_val['kr_id']) . "')";
            $getTotalReports    = $this->master_model->getRecordCount('arai_knowledge_repository_reported', array('kr_id' => $row_val['kr_id']), 'reported_id');

            $disp_reported = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalReports . '</a></div>';

            $modifiedStatus = "<a href='" . base_url('xAdmin/knowledge_repository/edit/' . base64_encode($row_val['kr_id'])) . "' > <button class='btn btn-success btn-sm'>View/Edit</button> </a>";

            if ($row_val['is_featured'] == '1') {$textShowFeatured = "Featured";} else { $textShowFeatured = "Not Featured";}
            $featuredUser = "<a href='javascript:void(0)' data-id='" . $row_val['kr_id'] . "' class='btn btn-success btn-sm featured-check' id='change-stac-" . $row_val['kr_id'] . "'>" . $textShowFeatured . "</a>";

            if ($row_val['is_block'] == '0') {$textShowBlockStatus = 'Block';} else if ($row_val['is_block'] == '1') {$textShowBlockStatus = 'Unblock';}
            $onclick_block_fun = "block_unblock('" . base64_encode($row_val['kr_id']) . "', '" . $textShowBlockStatus . "', '" . $row_val['kr_id_disp'] . "', '" . $row_val['title_of_the_content'] . "')";
            $blockUser         = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="' . $onclick_block_fun . '" >' . $textShowBlockStatus . '</a>';

            $remove = "<a href='javascript:void(0)' class='remove' data-id='" . $row_val['kr_id'] . "'><button class='btn btn-success btn-sm'>Remove</button></a>";

            $dropdown = "";

            if ($row_val['admin_status'] == '0'): $chkStatus  = "selected";else:$chkStatus  = "";endif;
            if ($row_val['admin_status'] == '1'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($row_val['admin_status'] == '2'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;
            //if($row_val['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            //if($row_val['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            $dropdown .= "<input type='hidden' name='kr_id' id='kr_id' value='" . $row_val['kr_id'] . "' />
				<select name='status' id='status' class='change-status'>
				<option value='0' " . $chkStatus . ">Pending</option>
				<option value='1' " . $chkStatus1 . ">Approved</option>
				<option value='2' " . $chkStatus2 . ">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
            $dropdown .= "</select>";

            $xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='" . $row_val['xOrder'] . "' name='xOrder' id='xOrder' value='" . $row_val['xOrder'] . "' onblur='update_sort_order(this.value, " . $row_val['kr_id'] . ")' onkeyup='update_sort_order(this.value, " . $row_val['kr_id'] . ")' />";

            $showButtons = "<span style='white-space:nowrap;'>" . $modifiedStatus . " " . $featuredUser . " " . $blockUser . " " . $remove . " " . $dropdown . "</span>";

            /* $conclude = $row_val['webinar_date'];
            $dispTime = "<span style='white-space: nowrap;'>".date("h:i A", strtotime($row_val['webinar_start_time']))." - ".date("h:i A", strtotime($row_val['webinar_end_time']))."<span>"; */

            $data[] = array(
                $i,
                $row_val['kr_id'],
                $row_val['kr_id_disp'],
                $row_val['title_of_the_content'],
                // $row_val['author_name'],
                $row_val['created_on'],
                $textShowAdminStatus,
                $xOrder,
                $disp_likes,
                $disp_downloads,
                $disp_reported,
                $showButtons,
            );
        }

        $csrf_test_name = $this->security->get_csrf_hash();
        ## Response
        $response = array(
            "draw"                 => intval($draw),
            "iTotalRecords"        => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData"               => $data,
            "token"                => $csrf_test_name,
        );

        echo json_encode($response);
    }

    public function setOrder()
    {

        $kr_id          = $this->input->post('kr_id');
        $xOrder         = $this->input->post('xOrder');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('knowledge_repository', array('xOrder' => $xOrder), array('kr_id' => $kr_id));

        $status   = "Order Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    /* public function viewDetails(){

    $encrptopenssl =  New Opensslencryptdecrypt();
    $id=$this->input->post('id');
    $this->db->join('arai_registration','arai_webinar_attendence.user_id=arai_registration.user_id','left');
    $records = $this->master_model->getRecords("webinar_attendence",array("webinar_attendence.w_id" => $id, "webinar_attendence.status" => 'Active'));
    //echo $this->db->last_query();

    $table = '';
    if(count($records) > 0){
    $table .= '<table class="table table-bordered">
    <tr>
    <td><b>Fullname</b></td>
    <td><b>Email</b></td>
    </tr>';
    foreach($records as $details){
    $firstname     = $encrptopenssl->decrypt($details['first_name']);
    $lastname     = $encrptopenssl->decrypt($details['last_name']);
    $middlename = $encrptopenssl->decrypt($details['middle_name']);
    $fullname     = $firstname." ".$middlename." ".$lastname;
    $email         = $encrptopenssl->decrypt($details['email']);
    $table .= '<tr>
    <td>'.ucwords($fullname).'</td>
    <td>'.$email.'</td>
    </tr>';
    }
    $table .= '</table>';
    } else {

    $table = "<p class='text-center'><b>No Record Found</b></p>";
    }

    echo $table;

    }
     */

    public function edit($kr_id)
    {
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        $kr_id         = base64_decode($kr_id);

        // Load Library
        $this->load->library('upload');
        $user_id = $this->session->userdata('admin_id');

        $response_data = $this->master_model->getRecords("knowledge_repository", array("kr_id" => $kr_id, "is_deleted" => 0));
        if (count($response_data) == 0) {redirect(site_url('xAdmin/knowledge_repository'));}

        $data['kr_files_data'] = $kr_files_data = $this->master_model->getRecords('knowledge_repository_files', array("kr_id" => $kr_id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));

        $data['kr_banner_error'] = $data['kr_type_error'] = $data['tags_error'] = $error_flag = '';
        $file_upload_flag        = 0;
        if (isset($_POST) && count($_POST) > 0) {
            //$this->form_validation->set_rules('xxx', '', 'required|trim|xss_clean');
            $this->form_validation->set_rules('title_of_the_content', 'knowledge repository title', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('kr_description', 'knowledge repository description', 'required', array('required' => 'Please enter the %s'));

            if (!isset($_POST['kr_type'])) {
                $data['kr_type_error'] = 'Please select the Blog Technology';
                $error_flag            = 1;
            }

            if (!isset($_POST['tags'])) {
                $data['tags_error'] = 'Please select the Tags';
                $error_flag         = 1;
            }

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['kr_banner']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $kr_banner = $this->Common_model_sm->upload_single_file("kr_banner", array('png', 'jpg', 'jpeg', 'gif'), "kr_banner_" . date("YmdHis"), "./uploads/kr_banner", "png|jpeg|jpg|gif");
                    if ($kr_banner['response'] == 'error') {
                        $data['kr_banner_error'] = $kr_banner['message'];
                        $file_upload_flag        = 1;
                    } else if ($kr_banner['response'] == 'success') {
                        $add_data['kr_banner'] = $kr_banner['message'];
                        /* if($mode == 'Update') { @unlink("./uploads/byt/".$team_data[0]['team_banner']); } */
                    }
                }

                if ($file_upload_flag == 0) {
                    //$add_data['user_id'] = $user_id;
                    $add_data['title_of_the_content'] = $this->input->post('title_of_the_content');
                    $add_data['kr_description']       = $this->input->post('kr_description');
                    $add_data['kr_type']              = $this->input->post('kr_type');
                    $add_data['technology_ids']       = implode(",", $this->input->post('technology_ids'));

                    $add_data['terms_condition_check'] = $this->input->post('terms_condition_check');

                    $terms_condition = trim($this->security->xss_clean($this->input->post('terms_condition')));
                    if (isset($terms_condition) && $terms_condition != "") {
                        $add_data['terms_condition'] = trim($this->security->xss_clean($terms_condition));
                    } else { $add_data['terms_condition'] = '';}

                    $kr_type_other = trim($this->security->xss_clean($this->input->post('kr_type_other')));
                    if (isset($kr_type_other) && $kr_type_other != "") {
                        $add_data['kr_type_other'] = trim($this->security->xss_clean($kr_type_other));
                    } else { $add_data['kr_type_other'] = '';}

                    $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                    if (isset($tag_other) && $tag_other != "") {
                        $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
                    } else { $add_data['tag_other'] = '';}

                    $technology_ids_other = trim($this->security->xss_clean($this->input->post('technology_ids_other')));
                    if (isset($technology_ids_other) && $technology_ids_other != "") {
                        $add_data['technology_other'] = trim($this->security->xss_clean($technology_ids_other));
                    } else { $add_data['technology_other'] = '';}

                    $add_data['tags']        = implode(",", $this->input->post('tags'));
                    $add_data['author_name'] = trim($this->security->xss_clean($this->input->post('author_name')));

                    $add_data['updated_on'] = date('Y-m-d H:i:s');

                    $this->master_model->updateRecord('arai_knowledge_repository', $add_data, array('kr_id' => $kr_id));
                    //echo $this->db->last_query(); exit;

                    //START : INSERT  FILES
                    $kr_files = $_FILES['kr_files'];
                    if (count($kr_files) > 0) {
                        for ($i = 0; $i < count($kr_files['name']); $i++) {
                            if ($kr_files['name'][$i] != '') {
                                $kr_file = $this->Common_model_sm->upload_single_file('kr_files', array('png', 'jpg', 'jpeg', 'gif', 'pdf', 'xls', 'xlsx', 'doc', 'docx'), "kr_files_" . $kr_id . "_" . date("YmdHis") . rand(), "./uploads/kr_files", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx", '1', $i);

                                if ($kr_file['response'] == 'error') {} else if ($kr_file['response'] == 'success') {
                                    $add_file_data['user_id']    = $user_id;
                                    $add_file_data['kr_id']      = $kr_id;
                                    $add_file_data['file_name']  = ($kr_file['message']);
                                    $add_file_data['created_on'] = date("Y-m-d H:i:s");
                                    $this->master_model->insertRecord('arai_knowledge_repository_files', $add_file_data, true);
                                    // $kr_files_data[] = $add_file_data; //FOR LOG
                                }
                            }
                        }
                    }
                    //END : INSERT  FILES

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $user_id,
                        'action_name' => "Update KR By Admin",
                        'module_name' => 'KR Admin',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Blog has been successfully updated');
                    redirect(site_url('xAdmin/knowledge_repository'));
                }
            }
        }

        $this->db->order_by('type_name', 'ASC');
        $this->db->order_by("FIELD(type_name, 'other')", "DESC", false);
        $data['type_data'] = $this->master_model->getRecords("knowledge_repository_type_master", array("status" => 'Active'));

        $this->db->order_by('technology_name', 'ASC');
        $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $data['technology_data'] = $this->master_model->getRecords("arai_knowledge_repository_technology_master", array("status" => 'Active'));

        //$this->db->order_by("FIELD(tag_name, 'other')", "DESC", false);
        $data['tag_data'] = $this->master_model->getRecords("knowledge_repository_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['form_data']      = $response_data;
        $data['module_name']    = 'Knowledge Repository';
        $data['submodule_name'] = '';
        $data['mode']           = 'Update';
        $data['middle_content'] = 'knowledge_repository/edit';
        $this->load->view('admin/admin_combo', $data);
    }

    public function delete_kr_file_ajax()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $file_id       = $encrptopenssl->decrypt($this->input->post('file_id', true));

        $csrf_new_token           = $this->security->get_csrf_hash();
        $result['csrf_new_token'] = $csrf_new_token;

        $this->master_model->updateRecord('knowledge_repository_files', array('is_deleted' => '1'), array('file_id' => $file_id));
        $result['flag']    = 'success';
        $result['file_id'] = $file_id;

        //START : INSERT LOG
        $postArr                   = $this->input->post();
        $json_encode_data          = json_encode($postArr);
        $logDetails['user_id']     = $user_id;
        $logDetails['module_name'] = 'KR : Admin delete_team_file_ajax';
        $logDetails['store_data']  = $json_encode_data;
        $logDetails['ip_address']  = $this->get_client_ip();
        $logDetails['createdAt']   = date('Y-m-d H:i:s');
        $logDetails['action_name'] = 'Delete KR File Admin';
        $logData                   = $this->master_model->insertRecord('logs', $logDetails);
        //END : INSERT LOG

        echo json_encode($result);
    }

    public function updateStatus()
    {
        // Create Object
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $status         = $this->input->post('status');
        $reject_reason  = $this->input->post('reject_reason');

        $email_info = $this->get_mail_data($id);

        $updateQuery = $this->master_model->updateRecord('knowledge_repository', array('admin_status' => $status), array('kr_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "KR Status Updated By Admin",
            'module_name' => 'KR',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        if ($status == '2') //Rejected
        {
            $updateQuery1 = $this->master_model->updateRecord('knowledge_repository', array('reject_reason' => $reject_reason), array('kr_id' => $id));

            //START : SEND MAIL TO USER WHEN KR IS REJECTED -
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_admin_rejects_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                    '[reason]',
                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                    $reject_reason,
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['kr_owner_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO USER WHEN kR IS REJECTED

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        } else if ($status == '1') //Approved
        {
            //START : SEND MAIL TO USER WHEN kR IS APPROVED -
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('kr_mail_to_user_after_admin_approve_kr');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[kr_owner_name]',
                    '[kr_title]',
                    '[hyperlink_kr_detail]',
                ];
                $rep_array = [
                    $email_info['kr_owner_name'],
                    $email_info['kr_title'],
                    $email_info['hyperlink_kr_detail'],
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['kr_owner_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            //END : SEND MAIL TO USER WHEN KR IS APPROVED    -

            $updateQuery1 = $this->master_model->updateRecord('knowledge_repository', array('reject_reason' => ''), array('kr_id' => $id));

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        } else {
            $updateQuery1 = $this->master_model->updateRecord('knowledge_repository', array('reject_reason' => ''), array('kr_id' => $id));
            $jsonData     = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        }
    }

    /* public function changeStatus(){

    $id     = $this->uri->segment(4);
    $value = ucfirst($this->uri->segment(5));
    if($value == 'Publish'){

    $updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));

    // Log Data Added
    $user_id = $this->session->userdata('admin_id');
    $arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
    $json_encode_data     = json_encode($arrData);
    $ipAddr                  = $this->get_client_ip();
    $createdAt            = date('Y-m-d H:i:s');
    $logDetails = array(
    'user_id'         => $user_id,
    'action_name'     => "Challenge Status Change - 2",
    'module_name'    => 'Challenge',
    'store_data'    => $json_encode_data,
    'ip_address'    => $ipAddr,
    'createdAt'        => $createdAt
    );
    $logData = $this->master_model->insertRecord('logs',$logDetails);

    $this->session->set_flashdata('success','Publish status successfully changed');
    redirect(base_url('xAdmin/challenge'));

    } else if($value == 'Block'){

    $updateQuery = $this->master_model->updateRecord('challenge',array('is_publish'=>$value),array('c_id' => $id));

    // Log Data Added
    $user_id = $this->session->userdata('admin_id');
    $arrData = array('id' => $id, 'status' => $value, 'admin_id' => $user_id);
    $json_encode_data     = json_encode($arrData);
    $ipAddr                  = $this->get_client_ip();
    $createdAt            = date('Y-m-d H:i:s');
    $logDetails = array(
    'user_id'         => $user_id,
    'action_name'     => "Challenge Status Change",
    'module_name'    => 'Challenge',
    'store_data'    => $json_encode_data,
    'ip_address'    => $ipAddr,
    'createdAt'        => $createdAt
    );
    $logData = $this->master_model->insertRecord('logs',$logDetails);

    $this->session->set_flashdata('success','Publish status successfully changed');
    redirect(base_url('xAdmin/challenge'));
    }

    }
     */

    public function block_unblock() // BLOCK/UNBLOCK BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $kr_id                = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));
            $type                 = trim($this->security->xss_clean($this->input->post('type')));
            $user_id              = $this->session->userdata('admin_id');
            $result['flag']       = "success";

            if ($type == 'Block') {
                //START : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked
                /* $email_info = $this->get_mail_data_blog($kr_id);

                $email_send='';
                $slug = $encrptopenssl->encrypt('blog_mail_to_admin_on_blocked');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content = '';

                if(count($subscriber_mail) > 0)
                {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                '[Blog_Title]',
                '[BLOCKED_REASON]'
                ];
                $rep_array = [
                $email_info['Blog_Title'],
                $popupBlogBlockReason
                ];
                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array=array(
                'to'        =>    $email_info['admin_email'],
                'cc'        =>    '',
                'from'        =>    $fromadmin,
                'subject'    =>     $subject_title,
                'view'        =>  'common-file'
                );

                $other_infoarray    =    array('content' => $sub_content);
                $email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
                }  */
                //END : SEND MAIL TO ADMIN WHEN USER BLOCKED THE BLOG - blog_mail_to_admin_on_blocked

                $up_data['is_block']     = 1;
                $up_data['block_reason'] = $popupBlogBlockReason;
            } else if ($type == 'Unblock') {
                $up_data['is_block']     = 0;
                $up_data['block_reason'] = '';}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('knowledge_repository', $up_data, array('kr_id' => $kr_id));

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "KR By Admin",
                'module_name' => 'KR Admin',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function featured()
    {
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        // $value = ucfirst($this->uri->segment(5));
        //echo $id;
        $c_data = $this->master_model->getRecords("knowledge_repository", array("kr_id" => $id));
        if ($c_data[0]['is_featured'] == "0") {$is_featured = '1';} else { $is_featured = '0';}

        $updateQuery = $this->master_model->updateRecord('knowledge_repository', array('is_featured' => $is_featured), array('kr_id' => $id));
        //echo $this->db->last_query();
        $c_res = $this->master_model->getRecords("knowledge_repository", array("kr_id" => $id));

        if ($c_res[0]['is_featured'] == "0") {$text = 'Featured';} else { $text = 'Not Featured';}

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "KR Featured Status - 3",
            'module_name' => 'KR',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("c_status" => $is_featured, "u_featured" => $text, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function deleted()
    {
        $user_id        = $this->session->userdata('admin_id');
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $updateQuery    = $this->master_model->updateRecord('knowledge_repository', array('is_deleted' => 1), array('kr_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "KR Deleted From Admin",
            'module_name' => 'KR',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function get_mail_data($kr_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,kr_id_disp,title_of_the_content,knowledge_repository.created_on,registration.user_id');
        $this->db->join('registration', 'knowledge_repository.user_id=registration.user_id', 'LEFT');
        $kr_details = $this->master_model->getRecords('knowledge_repository ', array('kr_id' => $kr_id));

        if (count($kr_details)) {
            $user_arr = array();
            if (count($kr_details)) {
                foreach ($kr_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $kr_owner_name       = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $kr_owner_id         = $user_arr[0]['user_id'];
            $kr_owner_email      = $user_arr[0]['email'];
            $kr_title            = $kr_details[0]['title_of_the_content'];
            $kr_id_disp          = $kr_details[0]['kr_id_disp'];
            $kr_date             = date('d/m/Y', strtotime($kr_details[0]['created_on']));
            $kr_detail_link      = site_url('knowledge_repository/details/') . base64_encode($kr_id);
            $hyperlink_kr_detail = '<a href=' . $kr_detail_link . ' target="_blank">here</a>';

            $email_array = array(
                'kr_owner_name'       => $kr_owner_name,
                'kr_owner_email'      => $kr_owner_email,
                'kr_owner_id'         => $kr_owner_id,
                'kr_title'            => $kr_title,
                'kr_id_disp'          => $kr_id_disp,
                'date'                => $kr_date,
                'admin_email'         => 'vishal.phadol@esds.co.in',
                'hyperlink_kr_detail' => $hyperlink_kr_detail,
            );

            $this->db->select('title,first_name,middle_name,last_name,email');
            $loggedin_user_data = $this->master_model->getRecords("registration", array('user_id' => $this->session->userdata('user_id')));

            $loggedin_user_arr = array();
            if (count($loggedin_user_data)) {
                foreach ($loggedin_user_data as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $loggedin_user_arr[]    = $row_val;
                }
            }

            $loggedin_user_name                 = $loggedin_user_arr[0]['title'] . " " . $loggedin_user_arr[0]['first_name'] . " " . $loggedin_user_arr[0]['last_name'];
            $email_array['loggedin_user_name']  = $loggedin_user_name;
            $email_array['loggedin_user_email'] = $loggedin_user_arr[0]['email'];
            return $email_array;
            // echo "<pre>";
            // print_r($email_array);
        }
    }
}
