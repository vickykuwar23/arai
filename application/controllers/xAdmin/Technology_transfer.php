<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Technology_transfer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->library('excel');
        if ($this->session->userdata('admin_id') == "") {
            redirect(base_url('xAdmin/admin'));
        }
        $this->check_permissions->is_authorise_admin(15);
        // ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
    }

    public function index()
    {
        error_reporting(0);

        // load excel library
        $this->load->library('excel');

        if (isset($_POST['export']) && !empty($_POST['export'])) {

            $encrptopenssl = new Opensslencryptdecrypt();
            $response      = array();

            $column_search = array(
                't.id_disp',
                't.technology_title',
                't.created_on',
                'IF(t.admin_status = 0, "Pending", IF(t.admin_status = 1, "Approved", IF(t.admin_status = 2, "Rejected", "")))',
                't.xOrder',
                'IF(t.priority=0,"LOW", IF(t.priority=1,"Medium", "High"))',
                'IF(t.is_featured=1,"Featured","Non Featured")',
                'IF(t.is_block = 0, "Unblock","Block")',

            );

            $keyword = @$this->input->post('keyword') ? $this->input->post('keyword') : '';

            if ($keyword != '') // DATATABLE SEARCH
            {
                $Where .= " (";
                for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($keyword)) . "%' ESCAPE '!' OR ";}

                $Where = substr_replace($Where, "", -3);
                $Where .= ')';
            }

            ## Fetch records
            if ($Where != '') {$this->db->where($Where);}
            $this->db->order_by('tech_id', 'DESC', false);
            $select_f = "t.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR ',') FROM arai_technology_transfer_tags_master WHERE FIND_IN_SET(id,t.tags)) AS DispTags,
            (SELECT trl_name FROM arai_technology_transfer_tlr_master WHERE id=trl) AS DispTrl, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
            $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = t.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = t.user_id', 'LEFT', false);
            $records = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0'), $select_f);
            // echo "<pre>"; print_r($records);die;

            $data = array();
            $i    = 0;

            // Count Check
            if (count($records) > 0) {
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                // set Header
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Tech ID');
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Technology Title');
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Author Name');
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Company Profile');
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Abstract of the Technology available for transfer');
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Value Proposition of the Technology available for transfer');
                $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Important Features/ Specification');
                $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Application');
                $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Intellectual Property');
                $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Beneficiary Industry');
                $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Advantages');
                $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Commercialization Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Technology Readiness Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Tags');
                $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Other Tag');
                $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'xOrder');
                $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Featured Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Admin Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Block Status');
                $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'Block Reason');

                // set Row
                $rowCount = 2;
                foreach ($records as $row_val) {

                    if ($row_val['is_featured'] == '0') {$isFeatured = 'Not Featured';} else { $isFeatured = 'Featured';}
                    if ($row_val['admin_status'] == '0') {$adminStatus = 'Pending';} else if ($row_val['admin_status'] == '1') {$adminStatus = 'Approved';} else { $adminStatus = 'Rejected';}
                    if ($row_val['is_block'] == '0') {$isBlock = 'Not Block';} else { $isBlock = 'Block';}

                    $i++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row_val['tech_id']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row_val['id_disp']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row_val['technology_title']);

                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row_val['author_name']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, strip_tags(html_entity_decode($row_val['company_profile'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, strip_tags(html_entity_decode($row_val['technology_abstract'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, strip_tags(html_entity_decode($row_val['technology_value'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, strip_tags(html_entity_decode($row_val['important_features'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, strip_tags(html_entity_decode($row_val['application'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, strip_tags(html_entity_decode($row_val['intellectual_property'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, strip_tags(html_entity_decode($row_val['beneficiary_industry'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, strip_tags(html_entity_decode($row_val['advantages'])));
                    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, strip_tags(html_entity_decode($row_val['commercialization_status'])));

                    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $row_val['DispTrl']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $row_val['DispTags']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row_val['tag_other']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $row_val['xOrder']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $isFeatured);
                    $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $adminStatus);
                    $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $isBlock);
                    $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $row_val['block_reason']);
                    $rowCount++;
                } // Foreach End
            } // Count Check

            // create file name
            header("Content-Type: application/vnd.ms-excel");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
            header('Content-Disposition: attachment;filename="Technology_transfer_' . date("YmdHis") . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            ob_end_clean();
            $objWriter->save('php://output');
            //$objWriter->save($fileName);
            exit;

        }

        $data['records']        = '';
        $data['module_name']    = 'Technology transfer';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technology_transfer/index';
        $this->load->view('admin/admin_combo', $data);
    }

    public function add($id = 0)
    {

        $data['page_title'] = 'Technology transfer';
        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $admin_id = $this->session->userdata('admin_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("technology_transfer", array('tech_id' => $id, 'is_deleted' => 0));

            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else {
                redirect(site_url('xAdmin/technology_transfer/'));
                exit();
                // $data['mode'] = $mode = "Add";
            }

            $data['files_data'] = $files_data = $this->master_model->getRecords('technology_transfer_files', array("tech_id" => $id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));

            $data['video_data'] = $video_data = $this->master_model->getRecords('technology_transfer_video_details', array("tech_id" => $id, "is_deleted" => '0'), '', array('vid_id' => 'ASC'));

        }

        $data['tile_image_error'] = $data['CustomVAlidationErr'] = $data['company_logo_error'] = $data['tags_error'] = $data['kr_type_error'] = $data['trl_error'] = $error_flag = '';
        $file_upload_flag         = 0;
        if (isset($_POST) && count($_POST) > 0) {
            // echo "<pre>"; print_r($_POST);die;
            $this->form_validation->set_rules('technology_title', 'technology title', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('author_name', 'author name', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));

            $this->form_validation->set_rules('technology_abstract', 'technology abstract', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('technology_value', 'technology value', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('commercialization_status', 'commercialization status', 'required|trim|xss_clean', array('required' => 'Please enter the %s'));

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['tile_image']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $tile_image = $this->Common_model_sm->upload_single_file("tile_image", array('png', 'jpg', 'jpeg', 'gif'), "tile_image_" . date("YmdHis"), "./uploads/technology_transfer/tile_image", "png|jpeg|jpg|gif");
                    if ($tile_image['response'] == 'error') {
                        $data['tile_image_error'] = $tile_image['message'];
                        $file_upload_flag         = 1;
                    } else if ($tile_image['response'] == 'success') {
                        $add_data['tile_image'] = $tile_image['message'];
                    }
                }

                if ($_FILES['company_logo']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $company_logo = $this->Common_model_sm->upload_single_file("company_logo", array('png', 'jpg', 'jpeg', 'gif'), "company_logo_" . date("YmdHis"), "./uploads/technology_transfer/company_logo", "png|jpeg|jpg|gif");
                    if ($company_logo['response'] == 'error') {
                        $data['company_logo_error'] = $company_logo['message'];
                        $file_upload_flag           = 1;
                    } else if ($company_logo['response'] == 'success') {
                        $add_data['company_logo'] = $company_logo['message'];
                    }
                }

                $add_data['technology_title']         = $this->input->post('technology_title');
                $add_data['author_name']              = $this->input->post('author_name');
                $add_data['company_profile']          = $this->input->post('company_profile');
                $add_data['technology_abstract']      = $this->input->post('technology_abstract');
                $add_data['technology_value']         = $this->input->post('technology_value');
                $add_data['important_features']       = $this->input->post('important_features');
                $add_data['application']              = $this->input->post('application');
                $add_data['intellectual_property']    = $this->input->post('intellectual_property');
                $add_data['beneficiary_industry']     = $this->input->post('beneficiary_industry');
                $add_data['advantages']               = $this->input->post('advantages');
                $add_data['commercialization_status'] = $this->input->post('commercialization_status');
                $add_data['include_files']            = $this->input->post('include_files');
                $add_data['video_check']              = $this->input->post('video_check');
                $add_data['trl']                      = $this->input->post('trl');
                $add_data['tags']                     = implode(",", $this->input->post('tags'));

                $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                if (isset($tag_other) && $tag_other != "") {
                    $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
                } else { $add_data['tag_other'] = '';}

                if ($mode == 'Add') {
                    $add_data['is_featured']  = 0;
                    $add_data['admin_status'] = 0;
                    $add_data['is_deleted']   = 0;
                    $add_data['is_block']     = 0;
                    $add_data['created_on']   = date('Y-m-d H:i:s');

                    $last_id = $this->master_model->insertRecord('technology_transfer', $add_data, true);
                    // echo "<pre>";print_r($add_data);echo "<br>";echo $this->db->last_query();die;
                    $up_data['id_disp'] = $disp_id = "TNTTID-" . sprintf("%07d", $last_id);
                    $this->master_model->updateRecord('technology_transfer', $up_data, array('tech_id' => $last_id));

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $admin_id,
                        'action_name' => "Add technology transfer",
                        'module_name' => 'Front technology transfer',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Request sent for admin approval. Your  ' . $disp_id);
                } else if ($mode == 'Update') {
                    // $add_data['admin_status'] = 0;
                    $add_data['updated_on'] = date('Y-m-d H:i:s');

                    $last_id = $id;

                    $this->master_model->updateRecord('technology_transfer', $add_data, array('tech_id' => $last_id));
                    //echo $this->db->last_query(); exit;

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $admin_id,
                        'action_name' => "Update Technology_transfer Admin",
                        'module_name' => 'Technology_transfer Admin',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Record has been successfully updated');
                }

                if ($this->input->post('video_check') == 1) {

                    $this->master_model->updateRecord('technology_transfer_video_details', array('is_deleted' => '1'), array('tech_id' => $id));

                    $caption_arr   = $this->input->post('video_caption');
                    $video_url_arr = $this->input->post('video_url');
                    if (count($caption_arr)) {
                        foreach ($caption_arr as $key => $value) {
                            $add_video_info['tech_id']       = $last_id;
                            $add_video_info['video_caption'] = $caption_arr[$key];
                            $add_video_info['video_url']     = $video_url_arr[$key];
                            $add_video_info['created_on']    = date("Y-m-d H:i:s");

                            $this->master_model->insertRecord('technology_transfer_video_details', $add_video_info, true);
                        }
                    }
                }
                //START : INSERT  FILES
                if ($last_id != "") {
                    $tech_files = $_FILES['tech_files'];
                    if (count($tech_files) > 0) {
                        // echo "<pre>";print_r($tech_files);die;
                        for ($i = 0; $i < count($tech_files['name']); $i++) {
                            if ($tech_files['name'][$i] != '') {
                                $tech_file = $this->Common_model_sm->upload_single_file('tech_files', array('png', 'jpg', 'jpeg', 'gif', 'pdf', 'xls', 'xlsx', 'doc', 'docx', 'pptx', 'ppt'), "admin_file_" . $last_id . "_" . date("YmdHis") . rand(), "./uploads/technology_transfer/tech_files", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx|pptx|ppt", '1', $i);

                                if ($tech_file['response'] == 'error') {} else if ($tech_file['response'] == 'success') {
                                    $add_file_data['user_id']            = $form_data[0]['user_id'];
                                    $add_file_data['tech_id']            = $last_id;
                                    $add_file_data['file_name']          = ($tech_file['message']);
                                    $add_file_data['file_original_name'] = $tech_files['name'][$i];
                                    $add_file_data['created_on']         = date("Y-m-d H:i:s");
                                    $this->master_model->insertRecord('arai_technology_transfer_files', $add_file_data, true);
                                    // $kr_files_data[] = $add_file_data; //FOR LOG
                                }
                            }
                        }
                    }
                }
                //END : INSERT  FILES

                redirect(site_url('xAdmin/technology_transfer/'));

            }
            // echo validation_errors();die;
        }

        $this->db->order_by('xOrder', 'ASC');
        $this->db->order_by("FIELD(trl_name, 'other')", "DESC", false);
        $data['trl_data'] = $this->master_model->getRecords("arai_technology_transfer_tlr_master", array("status" => 'Active'));
        // print_r($data['trl_data']);die;/

        $data['tag_data'] = $this->master_model->getRecords("arai_technology_transfer_tags_master", array("status" => 'Active'), '', array('tag_name' => 'ASC'));

        $data['mode']           = $mode           = "Update";
        $data['module_name']    = 'Knowledge repository';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technology_transfer/add';
        $this->load->view('admin/admin_combo', $data);

    }

    public function get_data_listing()
    {

        $encrptopenssl = new Opensslencryptdecrypt();

        $table        = 'arai_technology_transfer t';
        $column_order = array(
            't.tech_id',
            't.tech_id',
            't.id_disp',
            't.technology_title',
            '(SELECT COUNT(tech_id) FROM arai_technology_transfer_connect_request WHERE tech_id = t.tech_id ) AS TotalRequests',
            '(SELECT COUNT(tech_id) FROM arai_technology_transfer_reported WHERE tech_id = t.tech_id ) AS TotalReports',
            't.created_on',
            't.admin_status',
            't.xOrder',
            't.priority',
            't.is_featured',
            't.is_block',

        ); //SET COLUMNS FOR SORT

        $column_search = array(
            't.id_disp',
            't.technology_title',
            't.created_on',
            'IF(t.admin_status = 0, "Pending", IF(t.admin_status = 1, "Approved", IF(t.admin_status = 2, "Rejected", "")))',
            't.xOrder',
            'IF(t.priority=0,"LOW", IF(t.priority=1,"Medium", "High"))',
            'IF(t.is_featured=1,"Featured","Non Featured")',
            'IF(t.is_block = 0, "Unblock","Block")',

        ); //SET COLUMN FOR SEARCH

        $order = array('t.tech_id' => 'DESC'); // DEFAULT ORDER

        $WhereForTotal = "WHERE t.is_deleted = 0 "; //DEFAULT WHERE CONDITION FOR ALL RECORDS
        $Where         = "WHERE t.is_deleted = 0    ";

        if ($_POST['search']['value']) // DATATABLE SEARCH
        {
            $Where .= " AND (";
            for ($i = 0; $i < count($column_search); $i++) {$Where .= $column_search[$i] . " LIKE '%" . ($this->Common_model_sm->custom_safe_string($_POST['search']['value'])) . "%' ESCAPE '!' OR ";}

            $Where = substr_replace($Where, "", -3);
            $Where .= ')';
        }

        $Order = ""; //DATATABLE SORT
        if (isset($_POST['order'])) {
            $explode_arr = explode("AS", $column_order[$_POST['order']['0']['column']]);
            $Order       = "ORDER BY " . $explode_arr[0] . " " . $_POST['order']['0']['dir'];} else if (isset($order)) {$Order = "ORDER BY " . key($order) . " " . $order[key($order)];}

        $Limit = "";if ($_POST['length'] != '-1') {$Limit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);} // DATATABLE LIMIT

        $join_qry = " ";
        // $join_qry .= " LEFT JOIN arai_registration r ON r.user_id = k.user_id ";
        //$join_qry = " LEFT JOIN tbl_ac_zodiac_master zm ON zm.zodiac_id = mg.guruji_zodiac ";

        $print_query = "SELECT " . str_replace(" , ", " ", implode(", ", $column_order)) . " FROM $table $join_qry $Where $Order $Limit "; //ACTUAL QUERY
        $Result      = $this->db->query($print_query);
        // echo $this->db->last_query();die;
        $Rows = $Result->result_array();

        $TotalResult    = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $WhereForTotal));
        $FilteredResult = count($this->Common_model_sm->getAllRec($column_order[0], $table . " " . $join_qry, $Where));

        $data = array();
        $no   = $_POST['start'];

        foreach ($Rows as $Res) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $Res['tech_id'];
            $row[] = $Res['id_disp'];
            $row[] = $Res['technology_title'];

            $onclick_request_fun = "show_connect_requests('" . base64_encode($Res['tech_id']) . "')";
            $getTotalRequests    = $Res['TotalRequests'];
            $disp_requests       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_request_fun . '">' . $getTotalRequests . '</a></div>';
            $row[]               = $disp_requests;

            $onclick_report_fun = "show_tech_reports('" . base64_encode($Res['tech_id']) . "')";
            $getTotalReports    = $this->master_model->getRecordCount('arai_technology_transfer_reported', array('tech_id' => $Res['tech_id']), 'reported_id');

            $disp_reported = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalReports . '</a></div>';

            $row[]               = $disp_reported;

            // $row[] = $dispName;
            $row[] = $Res['created_on'];

            $dropdown = "";

            if ($Res['admin_status'] == '0'): $chkStatus  = "selected";else:$chkStatus  = "";endif;
            if ($Res['admin_status'] == '1'): $chkStatus1 = "selected";else:$chkStatus1 = "";endif;
            if ($Res['admin_status'] == '2'): $chkStatus2 = "selected";else:$chkStatus2 = "";endif;
            //if($Res['admin_status'] == 'Withdraw'): $chkStatus3="selected"; else: $chkStatus3=""; endif;
            //if($Res['admin_status'] == 'Closed'): $chkStatus4="selected"; else: $chkStatus4=""; endif;

            $dropdown .= "<input type='hidden' name='tech_id' id='tech_id' value='" . $Res['tech_id'] . "' />
            <select name='status' id='status' class='change-status'>
            <option value='0' " . $chkStatus . ">Pending</option>
            <option value='1' " . $chkStatus1 . ">Approved</option>
            <option value='2' " . $chkStatus2 . ">Rejected</option>"; //<option value='Withdraw' ".$chkStatus3.">Withdraw</option>
            $dropdown .= "</select>";
            $row[] = $dropdown;

            $xOrder = "<input type='text' class='order-class allowd_only_numbers' style='width:50px;' data-id='" . $Res['xOrder'] . "' name='xOrder' id='xOrder' value='" . $Res['xOrder'] . "' onblur='update_sort_order(this.value, " . $Res['tech_id'] . ")' onkeyup='update_sort_order(this.value, " . $Res['tech_id'] . ")' />";

            $row[] = $xOrder;

            $onchange_priority_fun = 'change_tech_priority("' . base64_encode($Res['tech_id']) . '", this.value)';
            if ($Res['priority'] == '0') {$chkPriority0 = "selected";} else { $chkPriority0 = "";}
            if ($Res['priority'] == '1') {$chkPriority1 = "selected";} else { $chkPriority1 = "";}
            if ($Res['priority'] == '2') {$chkPriority2 = "selected";} else { $chkPriority2 = "";}
            $disp_priority = "
                    <select name='priority' id='priority' onchange='" . $onchange_priority_fun . "'>
                        <option value='0' " . $chkPriority0 . ">Low</option>
                        <option value='1' " . $chkPriority1 . ">Medium</option>
                        <option value='2' " . $chkPriority2 . ">High</option>
                    </select>";
            $row[] = $disp_priority;

            /*$onchange_priority_fun = 'change_question_priority("' . base64_encode($Res['tech_id']) . '", this.value)';
            if ($Res['priority'] == '0') {$chkPriority0 = "selected";} else { $chkPriority0 = "";}
            if ($Res['priority'] == '1') {$chkPriority1 = "selected";} else { $chkPriority1 = "";}
            if ($Res['priority'] == '2') {$chkPriority2 = "selected";} else { $chkPriority2 = "";}
            $disp_priority = "
            <select name='priority' id='priority' onchange='" . $onchange_priority_fun . "'>
            <option value='0' " . $chkPriority0 . ">Low</option>
            <option value='1' " . $chkPriority1 . ">Medium</option>
            <option value='2' " . $chkPriority2 . ">High</option>
            </select>";
            $row[] = $disp_priority;*/

            $EditBtn = "<a href='" . base_url('xAdmin/technology_transfer/add/' . base64_encode($Res['tech_id'])) . "' > <button class='btn btn-success btn-sm'>View/Edit</button> </a>";

            if ($Res['is_featured'] == '1') {$textShowFeatured = "Featured";} else { $textShowFeatured = "Not Featured";}
            $FeaturedBtn = "<a href='javascript:void(0)' data-id='" . $Res['tech_id'] . "' class='btn btn-success btn-sm featured-check' id='change-stac-" . $Res['tech_id'] . "'>" . $textShowFeatured . "</a>";

            /* $onclick_featured_fun = "featured_notfeatured_question('" . base64_encode($Res['tech_id']) . "', '" . $FeaturedFunText . "')";
            $FeaturedBtn          = '<a href="javascript:void(0)" onclick="' . $onclick_featured_fun . '" class="btn btn-success btn-sm featured-check" title="' . $textShowFeatured . '"><i class="fa ' . $FeatureIcon . '" aria-hidden="true"></i></a>';*/

            if ($Res['is_block'] == '0') {$textShowBlockStatus = 'Block';} else if ($Res['is_block'] == '1') {$textShowBlockStatus = 'Unblock';}
            $onclick_block_fun = "block_unblock('" . base64_encode($Res['tech_id']) . "', '" . $textShowBlockStatus . "', '" . $Res['id_disp'] . "', '" . $Res['technology_title'] . "')";
            $BlockBtn          = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="' . $onclick_block_fun . '" >' . $textShowBlockStatus . '</a>';

            $DeleteBtn = "<a href='javascript:void(0)' class='remove' data-id='" . $Res['tech_id'] . "'><button class='btn btn-success btn-sm'>Remove</button></a>";

            $ActionButtons = "<span style='white-space:nowrap;'>" . $EditBtn . " " . $FeaturedBtn . " " . $BlockBtn . " " . $DeleteBtn . "</span>";
            $row[]         = $ActionButtons;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $TotalResult, //All result count
            "recordsFiltered" => $FilteredResult, //Disp result count
            //"Query" => $print_query,
            "data"            => $data,
        );
        //output to json format
        // echo "<pre>"; print_r($output);die;
        echo json_encode($output);
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public function updateStatus()
    {
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();

        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $status         = $this->input->post('status');
        $reject_reason  = $this->input->post('reject_reason');

        $updateQuery = $this->master_model->updateRecord('arai_technology_transfer', array('admin_status' => $status), array('tech_id' => $id));

        $email_info = $this->get_mail_data($id);

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $status, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Technology transfer Status Updated By Admin",
            'module_name' => 'Technology transfer',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        if ($status == '2') //Rejected
        {
            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_admin_rejection');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[REASON]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $reject_reason,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO USER

            $updateQuery1 = $this->master_model->updateRecord('arai_technology_transfer', array('reject_reason' => $reject_reason), array('tech_id' => $id));

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        } else if ($status == '1') //Approved
        {

            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_admin_approval');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO USER

            $updateQuery1 = $this->master_model->updateRecord('arai_technology_transfer', array('reject_reason' => ''), array('tech_id' => $id));

            $jsonData = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        } else {
            $updateQuery1 = $this->master_model->updateRecord('arai_technology_transfer', array('reject_reason' => ''), array('tech_id' => $id));
            $jsonData     = array("token" => $csrf_test_name, "status" => 'done');
            echo json_encode($jsonData);
        }
    }

    public function block_unblock() // BLOCK/UNBLOCK BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id              = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $popupBlogBlockReason = trim($this->security->xss_clean($this->input->post('popupBlogBlockReason')));
            $type                 = trim($this->security->xss_clean($this->input->post('type')));
            $user_id              = $this->session->userdata('admin_id');
            $result['flag']       = "success";

            if ($type == 'Block') {

                $up_data['is_block']     = 1;
                $up_data['block_reason'] = $popupBlogBlockReason;
            } else if ($type == 'Unblock') {
                $up_data['is_block']     = 0;
                $up_data['block_reason'] = '';}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('technology_transfer', $up_data, array('tech_id' => $tech_id));

            if ($type == 'Block') {
                $email_info = $this->get_mail_data($tech_id);
                // START: MAIL TO USER
                $email_send      = '';
                $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_user_on_content_blocked');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content     = '';

                if (count($subscriber_mail) > 0) {
                    $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                    $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                    $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                    $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                    $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                    $arr_words = [
                        '[OWNER_NAME]',
                        '[TECHNOLOGY_TRANSFER_NAME]',
                        '[ID]',
                        '[REASON]',
                        // '[DD/MM/YYYY]',
                        // '[HH:MM:SS]',

                    ];
                    $rep_array = [
                        $email_info['OWNER_NAME'],
                        $email_info['TECHNOLOGY_TRANSFER_NAME'],
                        $email_info['ID'],
                        $popupBlogBlockReason,
                        // $email_info['DD/MM/YYYY'],
                        // $email_info['HH:MM:SS'],

                    ];

                    $sub_content = str_replace($arr_words, $rep_array, $desc);

                    $info_array = array(
                        'to'      => $email_info['OWNER_EMAIL'],
                        'cc'      => '',
                        'from'    => $fromadmin,
                        'subject' => $subject_title,
                        'view'    => 'common-file',
                    );

                    $other_infoarray = array('content' => $sub_content);
                    $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                }
                // END: MAIL TO USER

                //START MAIL TO MEMBERS
                $this->db->select("r.email,r.first_name,r.last_name,r.title");
                $this->db->join("arai_registration r", "r.user_id = sa.user_id", "INNER", false);
                $applicant_data = $this->master_model->getRecords('arai_technology_transfer_connect_request sa', array("sa.tech_id" => $tech_id));

                if (count($applicant_data) > 0) {

                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_applicant_on_content_blocked');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[OWNER_NAME]',
                            '[TECHNOLOGY_TRANSFER_NAME]',
                            '[ID]',
                            '[REASON]',

                        ];

                        $rep_array = [
                            $email_info['OWNER_NAME'],
                            $email_info['TECHNOLOGY_TRANSFER_NAME'],
                            $email_info['ID'],
                            $popupBlogBlockReason,
                        ];
                        $sub_content = str_replace($arr_words, $rep_array, $desc);
                    }

                    foreach ($applicant_data as $res) {
                        $full_name      = $encrptopenssl->decrypt($res['title']) . " " . $encrptopenssl->decrypt($res['first_name']) . " " . $encrptopenssl->decrypt($res['last_name']);
                        $sub_content    = str_replace("[APPLICANT_NAME]", $full_name, $sub_content);
                        $applicant_mail = $encrptopenssl->decrypt($res['email']);
                        $info_array     = array(
                            // 'to' =>  $applicant_mail,
                            'to'      => $applicant_mail,
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                }
            }

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Block Unblock Technology Transfer Admin",
                'module_name' => 'Technology transfer Admin',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function featured()
    {
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        // $value = ucfirst($this->uri->segment(5));
        //echo $id;
        $c_data = $this->master_model->getRecords("technology_transfer", array("tech_id" => $id));
        if ($c_data[0]['is_featured'] == "0") {$is_featured = '1';} else { $is_featured = '0';}

        $updateQuery = $this->master_model->updateRecord('technology_transfer', array('is_featured' => $is_featured), array('tech_id' => $id));
        //echo $this->db->last_query();
        $c_res = $this->master_model->getRecords("technology_transfer", array("tech_id" => $id));

        if ($c_res[0]['is_featured'] == "0") {$text = 'Featured';} else { $text = 'Not Featured';}

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'status' => $text, 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Technology transfer Featured Status",
            'module_name' => 'Technology transfer',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("c_status" => $is_featured, "u_featured" => $text, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function deleted()
    {
        $user_id        = $this->session->userdata('admin_id');
        $csrf_test_name = $this->security->get_csrf_hash();
        $id             = $this->input->post('id');
        $updateQuery    = $this->master_model->updateRecord('technology_transfer', array('is_deleted' => 1), array('tech_id' => $id));

        // Log Data Added
        $user_id          = $this->session->userdata('admin_id');
        $arrData          = array('id' => $id, 'is_deleted' => '1', 'admin_id' => $user_id);
        $json_encode_data = json_encode($arrData);
        $ipAddr           = $this->get_client_ip();
        $createdAt        = date('Y-m-d H:i:s');
        $logDetails       = array(
            'user_id'     => $user_id,
            'action_name' => "Technology transfer Deleted From Admin",
            'module_name' => 'Technology transfer',
            'store_data'  => $json_encode_data,
            'ip_address'  => $ipAddr,
            'createdAt'   => $createdAt,
        );
        $logData = $this->master_model->insertRecord('logs', $logDetails);

        $jsonData = array("token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function setOrder()
    {

        $tech_id        = $this->input->post('tech_id');
        $xOrder         = $this->input->post('xOrder');
        $csrf_test_name = $this->security->get_csrf_hash();
        $updateQuery    = $this->master_model->updateRecord('technology_transfer', array('xOrder' => $xOrder), array('tech_id' => $tech_id));

        $status   = "Order Successfully Updated.";
        $jsonData = array("msg" => $status, "token" => $csrf_test_name);
        echo json_encode($jsonData);
    }

    public function change_tech_priority_ajax()
    {
        // Create Object
        error_reporting(0);
        $encrptopenssl = new Opensslencryptdecrypt();
        $result        = array();

        $csrf_test_name  = $this->security->get_csrf_hash();
        $result['token'] = $csrf_test_name;

        if (isset($_POST) && count($_POST) > 0) {
            $tech_id  = $this->input->post('tech_id');
            $priority = $this->input->post('priority');

            $updateQuery = $this->master_model->updateRecord('technology_transfer', array('priority' => $priority), array('tech_id' => base64_decode($tech_id)));
            //$result['qry'] = $this->db->last_query();

            // Log Data Added
            $user_id          = $this->session->userdata('admin_id');
            $arrData          = array('id' => $tech_id, 'priority' => $priority, 'admin_id' => $user_id);
            $json_encode_data = json_encode($arrData);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Technology_transfer Priority Updated By Admin",
                'module_name' => 'Technology_transfer Admin',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }

    public function get_mail_data($tech_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,id_disp,technology_title,technology_transfer.created_on,registration.user_id');
        $this->db->join('registration', 'technology_transfer.user_id=registration.user_id', 'LEFT');
        $r_details = $this->master_model->getRecords('technology_transfer', array('tech_id' => $tech_id));

        if (count($r_details)) {
            $user_arr = array();
            if (count($r_details)) {
                foreach ($r_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $email_array                             = array();
            $email_array['OWNER_NAME']               = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $email_array['OWNER_ID']                 = $user_arr[0]['user_id'];
            $email_array['OWNER_EMAIL']              = $user_arr[0]['email'];
            $email_array['TECHNOLOGY_TRANSFER_NAME'] = $r_details[0]['technology_title'];
            $email_array['ID']                       = $r_details[0]['id_disp'];
            $email_array['DD/MM/YYYY']               = date('d/m/Y', strtotime($r_details[0]['created_on']));
            $email_array['HH:MM:SS']                 = date('H:i:s', strtotime($r_details[0]['created_on']));
            $details_page                            = site_url('technology_transfer/details/') . base64_encode($tech_id);
            $email_array['hyperlink_detail']         = '<a href=' . $details_page . ' target="_blank">here</a>';
            $email_array['admin_email']              = 'test@esds.co.in';

            return $email_array;
            // echo "<pre>";
            // print_r($email_array);
        }
    }

}
