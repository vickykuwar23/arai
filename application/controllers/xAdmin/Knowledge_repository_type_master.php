<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Knowledge_repository_type_master extends CI_Controller 
	{
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('Opensslencryptdecrypt');
			if($this->session->userdata('admin_id') == ""){			
				redirect(base_url('xAdmin/admin'));
			}	
			$this->check_permissions->is_authorise_admin(4);
		}
		
    public function index($value='')
    {
			$encrptopenssl =  New Opensslencryptdecrypt();		
			$response_data = $this->master_model->getRecords("arai_knowledge_repository_type_master");
			//echo "<pre>";print_r($response_data);die();
			$res_arr = array();
			if(count($response_data))
			{							
				foreach($response_data as $row_val)
				{						
					$row_val['type_name'] = $row_val['type_name'];
					$res_arr[] = $row_val;
				}			
			}
			$data['records'] = $res_arr; 
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'knowledge_repository_type_master';	
    	$data['middle_content']='knowledge_repository_type_master/index';
			$this->load->view('admin/admin_combo',$data);
		}
		
    public function add()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			// Check Validation
			$this->form_validation->set_rules('type_name', 'Technology Name', 'required|min_length[5]|xss_clean');
			if($this->form_validation->run())
			{	
				$type_name = $this->input->post('type_name');
				$insertArr = array( 'type_name' => $type_name);			
				$insertQuery = $this->master_model->insertRecord('arai_knowledge_repository_type_master',$insertArr);
				if($insertQuery > 0)
				{
					$this->session->set_flashdata('success','Blog Technology successfully created');
					redirect(base_url('xAdmin/knowledge_repository_type_master'));
				} 
				else 
				{
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/knowledge_repository_type_master/add'));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'knowledge_repository_type_master';
			$data['middle_content']='knowledge_repository_type_master/add';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function edit($id)
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$tech_data = $this->master_model->getRecords("arai_knowledge_repository_type_master", array('id' => $id));
			$res_arr = array();
			if(count($tech_data) > 0)
			{ 
				$row_val['type_name'] = $tech_data[0]['type_name'];
				$res_arr[] = $row_val;
			}
			//print_r($res_arr);die();
			$data['technology_data'] = $res_arr;
			// Check Validation
			$this->form_validation->set_rules('type_name', 'Blog Technology Name', 'required|min_length[5]|xss_clean');
			if($this->form_validation->run())
			{	
				$type_name = $this->input->post('type_name');
				$updateAt = date('Y-m-d H:i:s');
				$updateArr = array( 'type_name' => $type_name, 'updatedAt' => $updateAt);			
				$updateQuery = $this->master_model->updateRecord('arai_knowledge_repository_type_master',$updateArr,array('id' => $id));
				if($updateQuery > 0)
				{
					$this->session->set_flashdata('success','Blog Technology Name successfully updated');
					redirect(base_url('xAdmin/knowledge_repository_type_master'));
				} 
				else 
				{
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/knowledge_repository_type_master/edit/'.$id));
				}
			}
			$data['module_name'] = 'Master';
			$data['submodule_name'] = 'knowledge_repository_type_master';
			$data['middle_content']='knowledge_repository_type_master/edit';
			$this->load->view('admin/admin_combo',$data);
		}
		
		public function changeStatus()
		{
			$id 	= $this->uri->segment(4);
			$value = ucfirst($this->uri->segment(5));
			if($value == 'Active')
			{
				$updateQuery = $this->master_model->updateRecord('arai_knowledge_repository_type_master',array('status'=>$value),array('id' => $id));
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/knowledge_repository_type_master'));	
			} 
			else if($value == 'Block')
			{
				$updateQuery = $this->master_model->updateRecord('arai_knowledge_repository_type_master',array('status'=>$value),array('id' => $id)); 
				$this->session->set_flashdata('success','Status successfully changed');
				redirect(base_url('xAdmin/knowledge_repository_type_master'));		
			}
		}
		
		public function delete($id)
		{
			$id 	= $this->uri->segment(4);		 
			$updateQuery = $this->master_model->updateRecord('arai_knowledge_repository_type_master',array('is_deleted'=>1),array('id' => $id));
			$this->session->set_flashdata('success','Blog Technology successfully deleted');
			redirect(base_url('xAdmin/knowledge_repository_type_master'));	
		}
	}	