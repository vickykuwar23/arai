<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expert_emails extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(19);
    }

    public function index(){
		$encrptopenssl =  New Opensslencryptdecrypt();	
		$this->db->select('r1.user_id,r1.email,r1.first_name,r1.middle_name,r1.last_name,em.connect_id,em.created_at,
						em.subject,em.message,r2.user_id as ToUserId,r2.email as ToMail,r2.first_name as ToFname,r2.middle_name as ToMname,r2.last_name as ToLname,expert_id');
		$this->db->join('arai_registration r1','r1.user_id=em.user_id','left',false);
		$this->db->join('arai_registration r2','r2.user_id=em.expert_id','left',false);
		$this->db->order_by('em.connect_id','desc');
		$response_data = $this->master_model->getRecords("arai_expert_connect_emails em");
		// echo "<pre>";print_r($response_data);die;
  	$data['records'] = $response_data; 
		$data['module_name'] = 'Expert Emails';
		$data['submodule_name'] = '';	
  	$data['middle_content']='expert-query/emails';
		$this->load->view('admin/admin_combo',$data);
 	 }
	 
	 public function viewDetails(){
	 	$encrptopenssl =  New Opensslencryptdecrypt();	
		 $id=$this->input->post('id'); 
		 				
		$this->db->select('r1.user_id,r1.email,r1.first_name,r1.middle_name,r1.last_name,em.connect_id,em.created_at,
						em.subject,em.message,r2.user_id as ToUserId,r2.email as ToMail,r2.first_name as ToFname,r2.middle_name as ToMname,r2.last_name as ToLname,expert_id');
		$this->db->join('arai_registration r1','r1.user_id=em.user_id','left',false);
		$this->db->join('arai_registration r2','r2.user_id=em.expert_id','left',false);
		$this->db->order_by('em.connect_id','desc');
		$response_data = $this->master_model->getRecords("arai_expert_connect_emails em",array("connect_id" => $id));
		// print_r($response_data);die;
		$subject = $response_data[0]['subject'];
		$message = $response_data[0]['message'];

		
		$expert_fullname = ucfirst($encrptopenssl->decrypt($response_data[0]['ToFname']))." ".ucfirst($encrptopenssl->decrypt($response_data[0]['ToMname']))." ".ucfirst($encrptopenssl->decrypt($response_data[0]['ToLname']));
		
		$table = '<table class="table table-bordered">
						<tr>
							<td width="15%"><b>Subject</b></td>
							<td>'.$subject.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Question</b></td>
							<td>'.$message.'</td>
						</tr>
							<tr>
							<td width="15%"><b>Name Of Expert</b></td>
							<td>'.$expert_fullname.'</td>
						</tr>
				  </table>';
		echo $table;
		 
	 }
	 
}