<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Contactus extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$response_data = $this->master_model->getRecords("contactus",'','',array('id','DESC'));
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['fullname'] = $encrptopenssl->decrypt($row_val['fullname']);
				$row_val['email_id'] = $encrptopenssl->decrypt($row_val['email_id']);
				$row_val['country_code'] = $encrptopenssl->decrypt($row_val['country_code']);
				$row_val['mobile_no'] = $encrptopenssl->decrypt($row_val['mobile_no']);
				$row_val['subject_details'] = $encrptopenssl->decrypt($row_val['subject_details']);
				$row_val['content_received'] = $encrptopenssl->decrypt($row_val['content_received']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Contactus';	
    	$data['middle_content']='contactus/index';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	 public function viewDetails(){
		 
		$encrptopenssl =  New Opensslencryptdecrypt();
		$id = $this->input->post('id'); 
		$records = $this->master_model->getRecords("contactus",array("id" => $id),'',array('id','DESC'));
		
		$subject = $encrptopenssl->decrypt($records[0]['subject_details']);
		$desc 	 = $encrptopenssl->decrypt($records[0]['content_received']);
		
		$table = '<table class="table table-bordered">
						<tr>
							<td width="15%"><b>Subject</b></td>
							<td>'.$subject.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Content</b></td>
							<td>'.$desc.'</td>
						</tr>
						
				  </table>';
		echo $table;
		 
	 } 
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('usertype',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/usertype'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('usertype',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/usertype'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('usertype',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/usertype'));	
		 
	 }


}