<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : User Type
Author : Vicky K
*/

class Organization_sector extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(4);
    }

    public function index($value='')
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$response_data = $this->master_model->getRecords("organization_sector");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
						
				$row_val['name'] = ucfirst($row_val['name']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Organization_sector';	
    	$data['middle_content']='organization-sector/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
        // Check Validation
		$this->form_validation->set_rules('name', 'Organization Sector Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$name = strtolower($this->input->post('name'));
			
			$insertArr = array( 'name' => $name);			
			$insertQuery = $this->master_model->insertRecord('organization_sector',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Organization Sector successfully created');
				redirect(base_url('xAdmin/organization_sector'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/organization_sector/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Organization_sector';
        $data['middle_content']='organization-sector/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$organization_sector = $this->master_model->getRecords("organization_sector", array('id' => $id));
		$res_arr = array();
		if(count($organization_sector) > 0){ 
			$row_val['name'] = $organization_sector[0]['name'];
			$res_arr[] = $row_val;
		}
		
		$data['sector_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('name', 'Organization Sector Name', 'required|min_length[2]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$name = strtolower($this->input->post('name'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'name' => $name, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('organization_sector',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Organization Sector successfully updated');
				redirect(base_url('xAdmin/organization_sector'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/organization_sector/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Organization_sector';
        $data['middle_content']='organization-sector/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('organization_sector',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/organization_sector'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('organization_sector',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/organization_sector'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('organization_sector',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','User type successfully deleted');
		 redirect(base_url('xAdmin/organization_sector'));	
		 
	 }


}