<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Testimonials
Author : Vicky K
*/

class Testimonials extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(17);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		
		$response_data = $this->master_model->getRecords("testimonials");
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){		
				$row_val['author_name'] = $encrptopenssl->decrypt($row_val['author_name']);		
				$row_val['author_designation'] = $encrptopenssl->decrypt($row_val['author_designation']);
				$row_val['author_content'] = $encrptopenssl->decrypt($row_val['author_content']);
				$row_val['profile_img'] = $encrptopenssl->decrypt($row_val['profile_img']);
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Testimonial';
		$data['submodule_name'] = '';	
    	$data['middle_content']='testimonial/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
        // Check Validation
		$this->form_validation->set_rules('author_name', 'Author Name', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('author_designation', 'Author Designation', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('author_content', 'Content', 'required|min_length[10]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$author_name 		= $encrptopenssl->encrypt($this->input->post('author_name'));
			$author_designation = $encrptopenssl->encrypt($this->input->post('author_designation'));
			$author_content 	= $encrptopenssl->encrypt($this->input->post('author_content'));
			$profile_img  		= $_FILES['profile_img']['name'];
			
			if($_FILES['profile_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/testimonial';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('profile_img', $_FILES, $config, FALSE);
			    $testimonial_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$testimonial_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($testimonial_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/testimonials'));	
				}
						
				$filesize = $_FILES['profile_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // testimonials Image End
			
			$file_names = $encrptopenssl->encrypt($testimonial_image[0]);		
			
			$insertArr = array( 'author_name' => $author_name, 'author_designation' => $author_designation, 'author_content' => $author_content, 'profile_img' => $file_names);			
			
			$insertQuery = $this->master_model->insertRecord('testimonials',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Testimonial successfully created');
				redirect(base_url('xAdmin/testimonials'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/testimonials/add'));
			}
		}
		$data['module_name'] = 'Testimonial';
		$data['submodule_name'] = '';	
        $data['middle_content']='testimonial/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		$testimonial_data = $this->master_model->getRecords("testimonials", array('id' => $id));
		
		$res_arr = array();
		if(count($testimonial_data) > 0){ 
			$row_val['author_name'] 		= $encrptopenssl->decrypt($testimonial_data[0]['author_name']);
			$row_val['author_designation'] 	= $encrptopenssl->decrypt($testimonial_data[0]['author_designation']);
			$row_val['author_content'] 		= $encrptopenssl->decrypt($testimonial_data[0]['author_content']);
			//$row_val['profile_img'] 		= $encrptopenssl->decrypt($testimonial_data[0]['profile_img']);
			$res_arr[] = $row_val;
		}
		
		$data['testimonial_data'] = $res_arr;
		
        // Check Validation
		$this->form_validation->set_rules('author_name', 'Author Name', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('author_designation', 'Author Designation', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('author_content', 'Content', 'required|min_length[10]|xss_clean');
		
		if($this->form_validation->run())
		{	
			$author_name 		= $encrptopenssl->encrypt($this->input->post('author_name'));
			$author_designation = $encrptopenssl->encrypt($this->input->post('author_designation'));
			$author_content 	= $encrptopenssl->encrypt($this->input->post('author_content'));
			$profile_img  		= $_FILES['profile_img']['name'];
			
			if($_FILES['profile_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/testimonial';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('profile_img', $_FILES, $config, FALSE);
			    $testimonials_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$testimonials_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($testimonials_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/banner'));	
				}
						
				$filesize = $_FILES['profile_img']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;				   
			   
			   /*if($filesize > $maxsize) 
			   {
				   $data['imageError'] = "File size is larger than the allowed limit";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('xAdmin/setting'));					 
			   }*/	
			
			} // Banner Image End
			
			
			if($testimonials_image[0] == ""){
				
				$file_names = $testimonial_data[0]['profile_img'];
				
			} else {
				
				$file_names = $encrptopenssl->encrypt($testimonials_image[0]);
			} 
									
			$updateAt = date('Y-m-d H:i:s');
			
			$updateArr = array( 'author_name' => $author_name, 'author_designation' => $author_designation, 'author_content' => $author_content, 'profile_img' => $file_names, 'updatedAt' => $updateAt);			
			$updateQuery = $this->master_model->updateRecord('testimonials',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Testimonial successfully updated');
				redirect(base_url('xAdmin/testimonials'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin//edit/'.$id));
			}
		}
		$data['module_name'] = 'Testimonial';
		$data['submodule_name'] = '';
        $data['middle_content']='testimonial/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('testimonials',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/testimonials'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('testimonials',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/testimonials'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('testimonials',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Testimonial successfully deleted');
		 redirect(base_url('xAdmin/testimonials'));	
		 
	 }


}