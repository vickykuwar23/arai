<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class  : Tool Requirement
Author : Vicky K
*/

class Tool_requirement extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		$this->check_permissions->is_authorise_admin(20);
    }

    public function index()
    {
		
		$encrptopenssl =  New Opensslencryptdecrypt();	
		$this->db->select('registration.user_id,registration.email,registration.first_name,registration.middle_name,registration.last_name,tool_requirement.id,
						tool_requirement.tool_requirements,tool_requirement.tool_purpose,tool_requirement.tools_required,tool_requirement.tools_remark,tool_requirement.createdAt');
		$this->db->join('registration','tool_requirement.user_id=registration.user_id','left');
		$response_data = $this->master_model->getRecords("tool_requirement");
    	//print_r($response_data);
    	$data['records'] = $response_data; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Tool_requirement';	
    	$data['middle_content']='tool-requirement/index';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	  public function viewDetails(){
		$id=$this->input->post('id'); 
		$records = $this->master_model->getRecords("tool_requirement",array("id" => $id));
		$tool_requirements = $records[0]['tool_requirements'];
		$tool_purpose = $records[0]['tool_purpose'];
		$tools_required = $records[0]['tools_required'];
		$tools_remark = $records[0]['tools_remark'];
		
		$table = '<table class="table table-bordered">
						<tr>
							<td width="15%"><b>Your Requirement</b></td>
							<td>'.$tool_requirements.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Purpose</b></td>
							<td>'.$tool_purpose.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Tools/Facilities required</b></td>
							<td>'.$tools_required.'</td>
						</tr>
						<tr>
							<td width="15%"><b>Remarks(if any)</b></td>
							<td>'.$tools_remark.'</td>
						</tr>
				  </table>';
		echo $table;
		 
	 }
}