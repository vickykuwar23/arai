<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Task_setting
Author : Vicky K
*/

class Task_setting extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->helper('url');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
    }
	 
	 public function index()
    {
		
		$data['task_data'] = $this->master_model->getRecords("task_setting");
		
        // Check Validation
		$this->form_validation->set_rules('min_no', 'Minimum Slot', 'required|xss_clean');
		$this->form_validation->set_rules('max_no', 'Maximum Slot', 'required|xss_clean');	
		
		if($this->form_validation->run())
		{	
			
			$min_no 		= $this->input->post('min_no');
			$max_no 		= $this->input->post('max_no');			
			$createdAt		=   date('Y-m-d H:i:s');
			
			$updateArr = array(	
								'min_no' 		=> $min_no, 
								'max_no' 		=> $max_no,
								'updatedAt' 	=> $createdAt								
							);
								
			$updateQuery = $this->master_model->updateRecord('task_setting', $updateArr,array('id' => '1'));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Task setting successfully updated');
				redirect(base_url('xAdmin/task_setting'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/task_setting'));
			}
			
			
		} // Validation End
		
		//$data['task_data'] = $setting_data;	
		$data['module_name'] = 'Task Setting';
		$data['submodule_name'] = '';
        $data['middle_content']='task-setting/index';
        $this->load->view('admin/admin_combo',$data);
     }


}