<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Registration extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }
    public function store_json($value = '')
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        //$this->db->where('domain_industry!=','');
        $this->db->select('user_id,user_category,sub_catname,title,first_name,middle_name,last_name,email,institution_type,institution_full_name,other_institution_type,other_domain_industry,other_public_prvt,domain_industry,mobile');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration");
        $user_arr  = array();
        if (count($user_data)) {
            foreach ($user_data as $row_val) {
                $row_val_new = array();

                if ($encrptopenssl->decrypt($row_val['title']) != "") {$row_val_new['title'] = $encrptopenssl->decrypt($row_val['title']);}
                if ($encrptopenssl->decrypt($row_val['first_name']) != "") {$row_val_new['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);}
                if ($encrptopenssl->decrypt($row_val['middle_name']) != "") {$row_val_new['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);}
                if ($encrptopenssl->decrypt($row_val['last_name']) != "") {$row_val_new['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);}
                if ($encrptopenssl->decrypt($row_val['email']) != "") {$row_val_new['email'] = $encrptopenssl->decrypt($row_val['email']);}

                if ($encrptopenssl->decrypt($row_val['mobile']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['mobile']);}
                if ($encrptopenssl->decrypt($row_val['membership_type']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['membership_type']);}
                if ($encrptopenssl->decrypt($row_val['user_category']) != "") {$row_val_new['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);}
                if ($encrptopenssl->decrypt($row_val['sub_catname']) != "") {$row_val_new['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);}
                if ($encrptopenssl->decrypt($row_val['institution_type']) != "") {$row_val_new['institution_type'] = $encrptopenssl->decrypt($row_val['institution_type']);}
                if ($encrptopenssl->decrypt($row_val['institution_full_name']) != "") {$row_val_new['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);}
                if ($row_val['other_institution_type'] != "") {$row_val_new['other_institution_type'] = $row_val['other_institution_type'];}
                if ($row_val['other_domain_industry'] != "") {$row_val_new['other_domain_industry'] = $row_val['other_domain_industry'];}
                if ($row_val['other_public_prvt'] != "") {$row_val_new['other_public_prvt'] = $row_val['other_public_prvt'];}
                $domain_plain = '';
                if ($row_val['domain_industry'] != '') {
                    $domain = explode(',', $row_val['domain_industry']);
                    foreach ($domain as $key => $value) {
                        $domain_data = $this->master_model->getRecords("domain_industry_master", array('id' => $value), 'domain_name');
                        if (isset($domain_data) && count($domain_data) > 0) {
                            $domain_plain .= $encrptopenssl->decrypt($domain_data[0]['domain_name']) . ',';
                        }
                    }
                }
                $row_val_new['domain_industry'] = rtrim($domain_plain, ',');
                //$user_arr[] = $row_val_new;
                //echo "<pre>"; echo json_encode($user_arr);echo "</pre>";        exit;
                $up_arr['json_str'] = json_encode($row_val_new);
                $this->master_model->updateRecord('arai_registration', $up_arr, array('user_id' => $row_val['user_id']));
            }
            echo "Done";

        }
    }
    public function make_valid()
    {
        die;
        $encrptopenssl     = new Opensslencryptdecrypt();
        $emails_sof_delete = 'joshi.ecl@araiindia.com,tanawade.vel@araiindia.com,shah.tg@araiindia.com,mahagaonkar.sdl@araiindia.com,mishra.edl@araiindia.com,kavathekar.edl@araiindia.com,tembe.edl@araiindia.com,karle.aed@araiindia.com,jog.psl@araiindia.com,jain.nvh@araiindia.com,saravanan.pga@araiindia.com,ghugal.tg@araiindia.com,esdstesters@gmail.com,test1@test.com,sagar.matale@esds.co.inn,vicky.kuwar@esds.co.in,letusicube@gmail.com,esdstesting5@gmail.com,esdstesters@gmail.com,xravi047@gmail.com,ravi047@gmail.com';
        $emails_arr        = explode(',', $emails_sof_delete);
        foreach ($emails_arr as $value) {
            $email_enc = $encrptopenssl->encrypt($value);
            $whr_free  = array(
                'email' => $email_enc,
            );
            $user_data = $this->master_model->getRecords("registration", $whr_free);
            if (count($user_data)) {
                echo $user_data[0]['is_deleted'];
                //    $update_array=array(
                // 'is_deleted'=>'1',
                // );
                // $updateQuery = $this->master_model->updateRecord('registration',$update_array,array('user_id' => $user_data[0]['user_id'] ) );

            }

        }
        echo "Done";
        die;
        $emails     = 'aip@aippistons.net,laxmikant@binixindia.com,sanjeevk@sapl.net,jayesh@millenniumrings.com,arun.ctat@gmail.com,swaroopv@medha.com,ymksolutions@gmail.com,mangesh@rndtechnology.in,prajaktasheth@yahoo.co.in,jambhekar_k@yahoo.com,aniruddh.dubal@ssou.ac.in,srvenkatesan@transvahan.com,aakasjayaraj@gmail.com,somil.jain.17@gmail.com,jitesh@konmos.in,ameen.shaikh@redstonesummerhill.com,baskar.ceri@apnatech.com,girish@primeedge.in,suhas.suryawanshi@kirloskar.com,ashish@atomsalive.com,suyash.singh@evurjaa.com,knnandurkar@kkwagh.edu.in,bhapkar.udaysinh@kitcoek.in,somasekar.d@ashokleyland.com,yogeshkalia@gmail.com,director.cmti@nic.in,renji@cdac.in,hgvinodraj@gmail.com,anand.kharche@cnhind.com,narender.kalsi@isgec.co.in,varsha@tafe.com,joseph.varghese@centromotion.com,winsuninnovation@gmail.com,adityapuppala@speedloopauto.com,sachin.wagle@larsentoubro.com,mmsuelectric@gmail.com,balakrishnan.raju@tcs.com,p.sehgal@revoltmotors.com,awatecm@gmail.com,gopal.raghvendra@gmail.com,kumara@tafe.com,kishor.kjv.jadhav@faurecia.com,pavan.gourai@faurecia.com,ravindranv@tafe.com,connect@oniodesign.com,satish.p@rtfpl.com,ektachirde144@gmail.com,bhanupulibandla143@gmail.com,admin@srauctioneers.co.in,rkshivanand@gmail.com,amod@consonanceacoustics.com,nitin.dhisale@kpt.co.in,john@elangattu.com,manish.khandelwal@maruti.co.in,sjadhav1@msxi.com,ashutosh@quanzen.com,pedapatiprithvi@gmail.com,drpankaj.k@rtfpl.com,sunitapangavhane@gmail.com,shaan.varma@carmate.co.in,alokm@bhel.in,rt.desai@horiba.com,ajayruhela@gmail.com,ceo@viinnovations.com,jitendradhuru@yahoo.in,gaurav.saxena@bhel.in,ashwin.tg@araiindia.com';
        $emails_arr = explode(',', $emails);
        foreach ($emails_arr as $value) {
            $email_enc = $encrptopenssl->encrypt($value);
            $whr_free  = array(
                'email' => $email_enc,
            );
            $user_data = $this->master_model->getRecords("registration", $whr_free);
            if (count($user_data)) {
                // echo $user_data[0]['is_valid'];
                $user_data[0]['is_valid'];
                $update_array = array(
                    'is_valid'       => '1',
                    'payment_status' => 'complete',
                );
                $updateQuery = $this->master_model->updateRecord('registration', $update_array, array('user_id' => $user_data[0]['user_id']));
            }

        }
        echo "Done";
        die;

        $membership_type = 'Free';

        // $membership_type='Paid';
        $membership_free = $encrptopenssl->encrypt($membership_type);
        $whr_free        = array(
            'membership_type' => $membership_free,
        );
        $whr_paid = array(
            'payment_status' => 'complete',
        );
        $response_data = $this->master_model->getRecords("registration", $whr_free, 'user_id,user_category_id,user_sub_category_id,membership_type,email,payment_status,is_valid', array('user_id', 'desc'));
        $res_arr       = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['user_id']         = $row_val['user_id'];
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $res_arr[]                  = $row_val;
            }

        }
        echo "<pre>";
        print_r($res_arr);
        die;

        foreach ($res_arr as $key => $value) {
            $update_array = array(
                'is_valid' => '1',
            );
            $updateQuery = $this->master_model->updateRecord('registration', $update_array, array('user_id' => $value['user_id']));
        }
        echo "Done";

    }
    public function DoNotDeploy($value = '')
    {
        die;
        $encrptopenssl = new Opensslencryptdecrypt();
        $response_data = $this->master_model->getRecords("registration", '', '', array('user_id', 'desc'));
        $res_arr       = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['user_id'] = $row_val['user_id'];

                $row_val['email']  = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
                $res_arr[]         = $row_val;
            }

        }
        echo "<pre>";
        print_r($res_arr);die();
        $data['records'] = $res_arr;
    }

    public function sortBy($field, &$array, $direction = 'asc')
    {
        usort($array, @create_function('$a, $b', '
	        $a = $a["' . $field . '"];
	        $b = $b["' . $field . '"];
	        $a = trim($a);
	        $b = trim($b);
	        if ($a == $b) return 0;
	        @$direction = strtolower(trim($direction));
	        return ($a ' . (@$direction == 'desc' ? '>' : '<') . ' $b) ? -1 : 1;
	    '));
        return true;
    }
    public function index()
    {
        if ($this->session->userdata('user_id') != "") {
            redirect(base_url());
        }
        $encrptopenssl = new Opensslencryptdecrypt();

        $emp_status_data = $this->master_model->array_sorting($this->master_model->getRecords('employement_master', array('status' => "Active")), array('id'), 'name', 1);

        $data['employement_status'] = $emp_status_data;

        $institution_arr = $this->master_model->array_sorting($this->master_model->getRecords('institution_master', array('status' => "Active")), array('id'), 'institution_name');

        $data['institution'] = $institution_arr;

        $domain_arr = $this->master_model->array_sorting($this->master_model->getRecords('domain_industry_master', array('status' => "Active")), array('id'), 'domain_name');

        $data['domain'] = $domain_arr;

        $public_status_arr     = $this->master_model->array_sorting($this->master_model->getRecords('public_status_master', array('status' => "Active")), array('id'), 'public_status_name');
        $data['public_status'] = $public_status_arr;
        // print_r($public_status_arr);die;
        $user_category = $this->master_model->getRecords('registration_usercategory', array('status' => 'Active'), 'id,user_category');
        $category_arr  = array();
        if (count($user_category)) {

            foreach ($user_category as $row_val) {

                $row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
                $category_arr[]           = $row_val;
            }

        }
        if (isset($_POST['submit_button'])) {
            // echo "<pre>";
            // print_r($_POST);
            // die;

            $category = $this->input->post('category');
            $this->form_validation->set_rules('category', 'category', 'trim|required|numeric|max_length[11]|xss_clean');
            $this->form_validation->set_rules('gender', 'Gender', 'trim|required|max_length[15]|xss_clean');
            $this->form_validation->set_rules('subcategory', 'subcategory', 'trim|required|numeric|max_length[11]|xss_clean');
            $this->form_validation->set_rules('country_code', 'country code', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('mobile', 'mobile number', 'trim|required|xss_clean|min_length[9]|max_length[10]|numeric|callback_unique_mobile');
            $this->form_validation->set_rules('email', 'email', 'trim|xss_clean|valid_email|required|callback_unique_email');

            if ($category == 1) {
                //$this->form_validation->set_rules('employement_status', 'Employement Status', 'required');
                //$this->form_validation->set_rules('membership_type', 'membership type', 'trim|required|max_length[10]|xss_clean');
                $this->form_validation->set_rules('title', 'title', 'trim|required|max_length[10]|xss_clean');
                $this->form_validation->set_rules('first_name', 'first Name', 'trim|required|max_length[100]|xss_clean');
                //$this->form_validation->set_rules('middle_name', 'middle name', 'trim|required|max_length[100]|xss_clean');
                $this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[100]|xss_clean');

                //$this->form_validation->set_rules('are_you_student', 'are you student', 'trim|required|max_length[5]|xss_clean');
            } else if ($category == 2) {

                //$this->form_validation->set_rules('institution_type[]', 'institution type', 'trim|required|max_length[11]|xss_clean');
                $this->form_validation->set_rules('domain_industry[]', 'domain industry', 'trim|required|max_length[11]|xss_clean');
                $this->form_validation->set_rules('public_prvt', 'public private status', 'trim|required|max_length[11]|xss_clean');
                $this->form_validation->set_rules('institution_full_name', 'institution full name', 'trim|required|max_length[100]|xss_clean');
                //$this->form_validation->set_rules('name_of_spoc', 'name of spoc', 'trim|required|max_length[255]|xss_clean');
                //$this->form_validation->set_rules('country_code', 'country code', 'trim|required|max_length[10]|xss_clean');
                //$this->form_validation->set_rules('SPOC_Phone_Number', 'SPOC phone number', 'trim|required|numeric|max_length[15]|xss_clean');
                //$this->form_validation->set_rules('SPOC_Email_ID', 'SPOC email', 'trim|valid_email|required|max_length[255]|xss_clean');
                // $this->form_validation->set_rules('User_Bundle_Quanity', 'user bundle quanity', 'trim|required|max_length[10]|xss_clean');
            }

            // if ($this->input->post('membership_type')=='Paid') {
            // $this->form_validation->set_rules('subscription_fee', 'applicable subscription fee', 'trim|required|max_length[10]|xss_clean');
            //    }
            // if ($this->input->post('have_promo_code')=='Yes') {
            // $this->form_validation->set_rules('promo_code', 'promo code', 'trim|required|max_length[15]|xss_clean');
            // }

            //$this->form_validation->set_rules('username', 'username', 'trim|required|max_length[55]|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'trim|required|max_length[55]|xss_clean');
            $this->form_validation->set_rules('verification_code', 'verification code', 'trim|required|max_length[10]|xss_clean');
            //$this->form_validation->set_rules('user_type', 'User Type', 'trim|required|numeric|max_length[11]|xss_clean');
            //$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
            if ($this->form_validation->run()) {

                $category    = $this->input->post('category');
                $subcategory = $this->input->post('subcategory');

                // $membership_type_plain = $this->input->post('membership_type');
                // $membership_type  =$encrptopenssl->encrypt($this->input->post('membership_type'));
                $membership_type_plain = 'Free';
                $membership_type       = $encrptopenssl->encrypt($membership_type_plain);
                $title                 = $encrptopenssl->encrypt($this->input->post('title'));
                $employement_status    = $this->input->post('employement_status');
                $other_emp_status      = $this->input->post('other_emp_status');
                $gender                = $encrptopenssl->encrypt($this->input->post('gender'));
                $first_name            = $encrptopenssl->encrypt($this->input->post('first_name'));
                $middle_name           = $encrptopenssl->encrypt($this->input->post('middle_name'));
                $last_name             = $encrptopenssl->encrypt($this->input->post('last_name'));
                $country_code          = $this->input->post('country_code');
                $mobile                = $encrptopenssl->encrypt($this->input->post('mobile'));
                $email                 = $encrptopenssl->encrypt($this->input->post('email'));
                $institution_full_name = $encrptopenssl->encrypt($this->input->post('institution_full_name'));
                // $are_you_student  =$this->input->post('are_you_student');
                $verification_code = $this->input->post('verification_code');

                $institution_type = '';
                $domain_industry  = '';
                if ($category == 2) {
                    // $institution_type_arr  =$this->input->post('institution_type[]');
                    // $institution_type = implode(',', $institution_type_arr);

                    $domain_industry_arr = $this->input->post('domain_industry[]');
                    $domain_industry     = implode(',', $domain_industry_arr);
                }

                $public_prvt = $this->input->post('public_prvt');
                // $name_of_spoc  =$encrptopenssl->encrypt($this->input->post('name_of_spoc'));
                // $SPOC_Phone_Number  =$encrptopenssl->encrypt($this->input->post('SPOC_Phone_Number'));
                // $SPOC_Email_ID  =$encrptopenssl->encrypt($this->input->post('SPOC_Email_ID'));

                // $User_Bundle_Quanity  =$this->input->post('User_Bundle_Quanity');
                // $want_to_buy_User_Bundle=$this->input->post('want_to_buy_User_Bundle');
                // $other_institution_type  =$this->input->post('other_institution_type');
                $other_subcat_type       = '';
                $other_subcat_type_plain = $this->input->post('other_subcat_type');
                if ($other_subcat_type_plain != '') {
                    $other_subcat_type = $encrptopenssl->encrypt($other_subcat_type_plain);
                }
                $other_domain_industry = $this->input->post('other_domain_industry');
                $other_public_prvt     = $this->input->post('other_public_prvt');

                $username = $encrptopenssl->encrypt($this->input->post('username'));
                $password = $this->input->post('password');
                if ($membership_type_plain == 'Free' && $subcategory == 11) {
                    $this->session->set_flashdata('error_message_register', 'Registration failed.');
                    redirect(base_url() . 'registration');
                    exit;
                }
                if ($category == 1) {
                    $insertArr = array(
                        'user_category_id'     => $category,
                        'user_sub_category_id' => $subcategory,
                        'membership_type'      => $membership_type,

                        // 'employement_status'=>$employement_status,
                        // 'other_emp_status'=>$other_emp_status,
                        'title'                => $title,
                        'gender'               => $gender,
                        'first_name'           => $first_name,
                        'middle_name'          => $middle_name,
                        'last_name'            => $last_name,
                        'country_code'         => $country_code,
                        'mobile'               => $mobile,
                        'email'                => $email,
                        // 'are_you_student'=>$are_you_student,
                        'password'             => $password,
                    );
                } else if ($category == 2) {
                    $insertArr = array(
                        'user_category_id'      => $category,
                        'user_sub_category_id'  => $subcategory,
                        'other_subcat_type'     => $other_subcat_type,
                        // 'institution_type'=>$institution_type,
                        'institution_full_name' => $institution_full_name,
                        'domain_industry'       => $domain_industry,
                        'public_prvt'           => $public_prvt,
                        // 'name_of_spoc'=>$name_of_spoc,
                        'title'                 => $title,
                        'gender'                => $gender,
                        'first_name'            => $first_name,
                        'middle_name'           => $middle_name,
                        'last_name'             => $last_name,
                        'country_code'          => $country_code,
                        'mobile'                => $mobile,
                        'email'                 => $email,
                        // 'other_institution_type'=>$other_institution_type,
                        'other_domain_industry' => $other_domain_industry,
                        'other_public_prvt'     => $other_public_prvt,
                        // 'User_Bundle_Quanity'=>$User_Bundle_Quanity,
                        'password'              => $password,
                    );
                }
                // code added for single verification code for both email and mail
                $email_text             = $this->input->post('email');
                $email                  = $encrptopenssl->encrypt($email_text);
                $mobile                 = $this->input->post('mobile');
                $verification_code_text = $this->input->post('verification_code');
                $verification_code      = $encrptopenssl->encrypt($verification_code_text);

                $whre_code = array(
                    'email_id'          => $email,
                    'verification_code' => $verification_code,
                );
                $whre_otp = array(
                    'mobile_number' => $mobile,
                    'otp'           => $verification_code_text,
                );
                $user_email                     = $this->master_model->getRecords('verification_code', $whre_code);
                $user_mobile                    = $this->master_model->getRecords('otp', $whre_otp);
                $insertArr['verification_code'] = $verification_code_text;

                if ($verification_code_text == '5H13LD') {
                    $insertArr['valid_email'] = '1';
                }
                if (count($user_email) == 1) {
                    $insertArr['valid_email'] = '1';

                }
                if (count($user_mobile) == 1) {
                    $insertArr['valid_mobile'] = '1';

                }
                // code end for single verification code for both email and mail
                if ($category == 1 && $membership_type_plain == 'Free') {
                    $insertArr['is_valid'] = '1';
                }
                // echo "<pre>";
                // print_r($insertArr);die;
                if ($subcategory == 1 || $subcategory == 2) {
                    $insertArr['contactable'] = 'yes';
                }
                $insertArr['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                //ADD FULL NAME
                // if($this->input->post('middle_name') != ''){
                // $user_full_name = $this->input->post('first_name')." ".$this->input->post('middle_name')." ".$this->input->post('last_name');
                // }else{
                // $user_full_name = $this->input->post('first_name')." ".$this->input->post('last_name');
                // }
                // $insertArr['full_name_decrypt']=$user_full_name;
                $insertArr['title_decrypt']       = $this->input->post('title');
                $insertArr['first_name_decrypt']  = $this->input->post('first_name');
                $insertArr['middle_name_decrypt'] = $this->input->post('middle_name');
                $insertArr['last_name_decrypt']   = $this->input->post('last_name');

                $insert_id = $this->master_model->insertRecord('registration', $insertArr);
                // echo $this->db->last_query();
                // Log Data Added
                $postArr          = $this->input->post();
                $arrLog           = $insertArr;
                $arrLog           = array_merge($insertArr, $postArr);
                $json_encode_data = json_encode($arrLog);
                $ipAddr           = $this->get_client_ip();
                $createdAt        = date('Y-m-d H:i:s');
                $logDetails       = array(
                    'user_id'     => $insert_id,
                    'action_name' => "Insert",
                    'module_name' => 'Registration',
                    'store_data'  => $json_encode_data,
                    'ip_address'  => $ipAddr,
                    'createdAt'   => $createdAt,
                );
                $logData = $this->master_model->insertRecord('logs', $logDetails);
                if ($insert_id > 0) {
                    $this->master_model->store_json_for_search($insert_id);

                    if ($subcategory != 2) {
                        $insertArr_profile = array(
                            'user_id'            => $insert_id,
                            'employement_status' => $encrptopenssl->encrypt($employement_status),
                            'other_emp_status'   => $encrptopenssl->encrypt($other_emp_status),
                        );
                        $this->master_model->insertRecord('student_profile', $insertArr_profile);
                    }

                    if ($this->input->post('membership_type') == 'Paid' || $category == 2) {

                        redirect(base_url() . 'registration/payment/' . base64_encode($insert_id));
                        exit();
                    }
                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('registration_successful');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';
                    if (count($subscriber_mail) > 0) {

                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = ['[PHONENO]'];
                        $rep_array = [$phoneNO];

                        $sub_content = str_replace($arr_words, $rep_array, $desc);
                        // 'to'=>$this->input->post('email'),
                        $info_array = array(
                            'to'      => $this->input->post('email'),
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);

                        $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

                    }
                    // if($emailsend || 1)
                    // {
                    $this->session->set_flashdata('success_message_register', 'Registration successful.');

                    redirect(base_url() . 'login');
                    exit;

                    // }

                } else {
                    $this->session->set_flashdata('error_message', 'Something went wrong! Please try again.');
                    redirect(base_url('registration'));
                }
            }

        }
        $data['country_codes'] = $this->master_model->getRecords('country', '', '', array('iso' => 'asc'));

        $data['image']      = $this->create_captcha_reg();
        $data['categories'] = $category_arr;

        $data['page_title']     = 'Register';
        $data['middle_content'] = 'registration/register';
        $this->load->view('front/front_combo', $data);
    }
    public function payment($user_id)
    {
        $encrptopenssl  = new Opensslencryptdecrypt();
        $user_id        = base64_decode($user_id);
        $user_info      = $this->master_model->getRecords('registration', array('user_id' => $user_id));
        $user_category  = $user_info[0]['user_category_id'];
        $user_email_enc = $user_info[0]['email'];
        $mail_to        = $encrptopenssl->decrypt($user_email_enc);

        if (isset($_POST['submit_button'])) {

            $this->form_validation->set_rules('plan', 'plan', 'trim|required|max_length[11]|xss_clean');
            $this->form_validation->set_rules('have_promo_code', 'have promo code', 'trim|required|max_length[11]|xss_clean');
            if ($this->input->post('have_promo_code') == 'Yes') {
                $this->form_validation->set_rules('promo_code', 'promo code', 'trim|required|max_length[15]|xss_clean');
            }

            //$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');
            if ($this->form_validation->run()) {

                $have_promo_code_plain = $this->input->post('have_promo_code');

                $promo_code_plain = $this->input->post('promo_code');
                $plan             = $this->input->post('plan');

                $promo_code_type  = $this->input->post('promo_code_type');
                $have_promo_code  = $this->input->post('have_promo_code');
                $subscription_fee = $this->input->post('subscription_fee');
                $promo_code       = $encrptopenssl->encrypt($this->input->post('promo_code'));
                if ($promo_code_plain == '' || $plan == '') {
                    $this->session->set_flashdata('error_message_register', 'Registration failed.');
                    redirect(base_url() . 'registration/payment/' . base64_encode($user_id));
                    exit;

                }
                $whre = array(
                    'promo_code' => $promo_code,
                    'plan_id'    => $plan,
                );

                $promo_code_info = $this->master_model->getRecords('promo_codes', $whre);
                if (count($promo_code_info) == 0) {
                    $this->session->set_flashdata('error_message_register', 'Registration failed.');
                    redirect(base_url() . 'registration/payment/' . base64_encode($user_id));
                    exit;

                }
                if (count($promo_code_info) == 1) {

                    $promo_id             = $promo_code_info[0]['id'];
                    $promo_used_count     = $promo_code_info[0]['used_count'];
                    $new_promo_used_count = $promo_used_count + 1;
                    $updateArr_promo      = array(
                        'used_count' => $new_promo_used_count,

                    );
                    $updateQuery = $this->master_model->updateRecord('promo_codes', $updateArr_promo, array('id' => $promo_id));
                }

                $orgnization_id = $this->input->post('org_id');
                $promo_code_id  = $this->input->post('promo_code_id');

                $insertArr = array(
                    'user_id'          => $user_id,
                    'plan_id'          => $plan,
                    'have_promo_code'  => $have_promo_code,
                    'promo_code_type'  => $promo_code_type,
                    'promo_code'       => $promo_code,
                    'subscription_fee' => $subscription_fee,
                );
                $insert_id = $this->master_model->insertRecord('subscriptions', $insertArr);

                if ($insert_id > 0) {
                    $whre = array(
                        'plan_id' => $plan,
                    );

                    $plan_info = $this->master_model->getRecords('plan', $whre);

                    $plan_category = $plan_info[0]['plan_category'];

                    if ($plan_category == 2) {
                        $User_Bundle_Quanity = $plan_info[0]['no_of_users'];
                        if ($User_Bundle_Quanity > 0) {
                            for ($i = 0; $i < $User_Bundle_Quanity; $i++) {
                                $insert_array = array(
                                    'admin_id'   => $user_id,
                                    'code_type'  => 'Corporate',
                                    'promo_code' => $encrptopenssl->encrypt(bin2hex(openssl_random_pseudo_bytes(4))),
                                );
                                $this->master_model->insertRecord('promo_codes', $insert_array);
                            }
                        }

                    }
                    $updateArr = array(
                        'payment_status' => 'complete',
                        'is_valid'       => '1',
                    );
                    $updateQuery = $this->master_model->updateRecord('registration', $updateArr, array('user_id' => $user_id));
                    if ($promo_code_type == 'Corporate') {

                        $insertArr_2 = array(
                            'user_id'        => $user_id,
                            'orgnization_id' => $orgnization_id,
                            'promo_code_id'  => $promo_code_id,
                        );
                        $insert_id = $this->master_model->insertRecord('user_org_mapping', $insertArr_2);
                    }
                    if ($updateQuery) {
                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('registration_successful');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';
                        if (count($subscriber_mail) > 0) {

                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = ['[PHONENO]'];
                            $rep_array = [$phoneNO];

                            $sub_content = str_replace($arr_words, $rep_array, $desc);
                            // 'to'=>$this->input->post('email'),
                            $info_array = array(
                                'to'      => $mail_to,
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);

                            $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

                        }
                        $this->session->set_flashdata('success_message_register', 'Registration successful.');

                        redirect(base_url() . 'login');
                        exit;

                    } else {
                        $this->session->set_flashdata('error_message', 'Something went wrong! Please try again.');
                        redirect(base_url('registration/payment'));
                    }

                } else {
                    $this->session->set_flashdata('error_message', 'Something went wrong! Please try again.');
                    redirect(base_url('registration/payment'));
                }
            }

        }
        $plans     = $this->master_model->getRecords('plan', array('plan_category' => $user_category, 'status' => 'Active'));
        $plans_arr = array();
        if (count($plans)) {

            foreach ($plans as $row_val) {

                $row_val['plan_name']     = $encrptopenssl->decrypt($row_val['plan_name']);
                $row_val['plan_amount']   = $encrptopenssl->decrypt($row_val['plan_amount']);
                $row_val['plan_desc']     = $encrptopenssl->decrypt($row_val['plan_desc']);
                $row_val['plan_duration'] = $encrptopenssl->decrypt($row_val['plan_duration']);
                $plans_arr[]              = $row_val;
            }

        }

        // echo "<pre>"; print_r($plans_arr);die;
        $data['image']          = $this->create_captcha_reg();
        $data['plans']          = $plans_arr;
        $data['user_category']  = $user_category;
        $data['page_title']     = 'Register';
        $data['middle_content'] = 'registration/payment';
        $this->load->view('front/front_combo', $data);
    }
    public function get_sub_category()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $str           = '';
        $cat_id        = htmlentities($this->input->post('cat_id'));

       	
       	$sub_category_arr = $this->master_model->array_sorting($this->master_model->getRecords('registration_usersubcategory', array('status' => "Active", 'u_cat_id' => $cat_id)), array('subcat_id'), 'sub_catname');

        $set_select = $this->input->post('setSelect');
        $str        = '<option value=""></option>';

        foreach ($sub_category_arr as $sub_cat) {
            if (isset($set_select) && $set_select == $sub_cat['subcat_id']) {
                $sel = 'selected';

            } else {
                $sel = '';
            }
            $str .= "<option value='" . $sub_cat['subcat_id'] . "' " . $sel . ">" . $sub_cat['sub_catname'] . "</option>";

        }
        $response = array(
            'name'  => $this->security->get_csrf_token_name(),
            'value' => $this->security->get_csrf_hash(),
            'str'   => $str,
        );

        echo json_encode($response);
    }
    public function create_captcha_reg($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }
    public function verify($user_id)
    {
        $u_id = base64_decode($user_id);
        $user = $this->master_model->getRecords('registration', array('user_id' => $u_id));
        if ($user[0]['is_verified'] == 'no') {
            $updateArr = array(
                'is_verified' => 'yes',
            );
            $updateQuery = $this->master_model->updateRecord('registration', $updateArr, array('user_id' => $u_id));
            if ($updateQuery > 0) {
                $this->session->set_flashdata('success', 'Account verified successfully.');
                redirect(base_url('login'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('login'));
            }
        } else {
            //$this->session->set_flashdata('error','Account Already ve.');
            redirect(base_url('login'));
        }

    }
    public function unique_email($email)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $email         = $encrptopenssl->encrypt($email);
        $user          = $this->master_model->getRecords('registration', array('email' => $email, 'is_valid' => '1', 'is_deleted' => '0'));
        if (count($user) > 0) {
            $this->form_validation->set_message('unique_email', '%s already exist.');
            return false;
        } else {
            return true;
        }

    }
    public function unique_email_ajax()
    {
        $email         = $this->input->post('email');
        $encrptopenssl = new Opensslencryptdecrypt();
        $email         = $encrptopenssl->encrypt($email);
        $user          = $this->master_model->getRecords('registration', array('email' => $email, 'is_valid' => '1', 'is_deleted' => '0'));
        if (count($user) > 0) {
            echo "false";
        } else {
            echo "true";
        }

    }
    public function unique_mobile($mobile)
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $mobile        = $encrptopenssl->encrypt($mobile);
        $user          = $this->master_model->getRecords('registration', array('mobile' => $mobile, 'is_valid' => '1', 'is_deleted' => '0'));
        if (count($user) > 0) {
            $this->form_validation->set_message('unique_mobile', '%s already exist.');
            return false;
        } else {
            return true;
        }

    }
    public function unique_mobile_ajax()
    {
        $mobile        = $this->input->post('mobile');
        $encrptopenssl = new Opensslencryptdecrypt();
        $mobile        = $encrptopenssl->encrypt($mobile);
        $user          = $this->master_model->getRecords('registration', array('mobile' => $mobile, 'is_valid' => '1', 'is_deleted' => '0'));
        if (count($user) > 0) {
            echo "false";
        } else {
            echo "true";
        }

    }
    public function check_captcha($code)
    {
        if ($code == '' || $_SESSION["mycaptcha"] != $code) {
            $this->session->unset_userdata("mycaptcha");
            $this->form_validation->set_message('check_captcha', 'Invalid %s.');
            return false;
        }
        if ($_SESSION["mycaptcha"] == $code && $this->session->userdata("used") == 0) {
            $this->session->unset_userdata("mycaptcha");
            return true;
        }

    }
    public function check_captcha_ajax()
    {
        $code = $this->input->post('code');
        if ($code == '' || $_SESSION["mycaptcha"] != $code) {
            //$this->session->unset_userdata("mycaptcha");
            //$this->form_validation->set_message('check_captcha', 'Invalid %s.');
            echo "false";
        }
        if ($_SESSION["mycaptcha"] == $code && $this->session->userdata("used") == 0) {
            //$this->session->unset_userdata("mycaptcha");
            echo "true";
        }

    }

    public function refresh_reg()
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 17,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha', $captcha['word']);

        // Display captcha image
        echo $captcha['image'];
    }
    public function send_otp()
    {
        $mobile = $this->input->post('mobile');
        $otp    = $this->generateNumericOTP(6);
        if ($mobile != '' && $otp != '') {
            $text  = "Your OTP code is" . $otp;
            $msg   = urlencode($text);
            $reply = '';
            // $url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";
            // $string = preg_replace('/\s+/', '', $url);
            // $x = curl_init($string);
            // curl_setopt($x, CURLOPT_HEADER, 0);
            // curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
            // curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);
            // $reply = curl_exec($x);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL            => "http://textlark.com/SMSApi/send",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => "userid=ARAIPU&password=Ara@2020&mobile=" . $mobile . "&msg=" . $msg . "&senderid=ARAIPU&msgType=text&duplicatecheck=true&output=json&sendMethod=quick",
                CURLOPT_HTTPHEADER     => array(
                    "apikey: 05f005b4-7a55-443a-b413-4a989439852c",
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                ),
            ));

            $response = curl_exec($curl);
            $err      = curl_error($curl);
            curl_close($curl);
            $msg_res    = json_decode($response, true);
            $msg_status = $msg_res['status'];

            $insertArr = array(
                'mobile_number' => $mobile,
                'otp'           => $otp,
                'created_at'    => date('Y-m-d H:i:s'),
            );
            if ($msg_status == 'success') {

                $this->master_model->deleteRecord('otp', 'mobile_number', $mobile);
                if ($this->master_model->insertRecord('otp', $insertArr)) {
                    $response = array(
                        'success' => true,
                    );
                    echo json_encode($response);
                    exit();
                } else {
                    $response = array(
                        'success' => false,
                    );
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array(
                    'success' => false,
                );
                echo json_encode($response);
                exit();
            }

        } else {
            $response = array(
                'success' => false,
            );
            echo json_encode($response);
            exit();
        }
    }
    public function verify_otp()
    {
        // echo "true";die;
        $mobile = $this->input->post('mobile');
        $otp    = $this->input->post('otp');

        $whre = array(
            'mobile_number' => $mobile,
            'otp'           => $otp,
        );
        // $encrptopenssl =  New Opensslencryptdecrypt();
        // $mobile=$encrptopenssl->encrypt($mobile);
        if ($mobile != '') {
            $user = $this->master_model->getRecords('otp', $whre);

            if (count($user) == 1) {
                echo "true";
            } else {
                echo "false";
            }
        }

    }
    public function send_codes()
    {

        $encrptopenssl = new Opensslencryptdecrypt();

        $email_text = $this->input->post('email');
        $email      = $encrptopenssl->encrypt($email_text);

        $verification_code_text = $this->generateNumericOTP(6);
        $verification_code      = $encrptopenssl->encrypt($verification_code_text);
        $mobile                 = intval($this->input->post('mobile'));
        $otp                    = $this->generateNumericOTP(6);
        if ($mobile != '' && $otp != '') {
            // $text = "Your OTP code is".$otp;
            $text = "Dear applicant, Your TechNovuus account registration OTP is : " . $otp . "  It is valid for 15 mins. Please do not share it with anyone. Keep innovating..!";
            //$msg = urlencode($text);
            $data        = array("sender_id" => "ARAIPU", "to" => [$mobile], "message" => $text, "route" => "otp");
            $data_string = json_encode($data);
            $ch          = curl_init('https://api.trustsignal.io/v1/sms?api_key=05f005b4-7a55-443a-b413-4a989439852c');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $response = curl_exec($ch);
            // print_r($response);die;
            // $reply='';
            // $curl = curl_init();
            // curl_setopt_array($curl, array(
            //   CURLOPT_URL => "http://textlark.com/SMSApi/send",
            //   CURLOPT_RETURNTRANSFER => true,
            //   CURLOPT_ENCODING => "",
            //   CURLOPT_MAXREDIRS => 10,
            //   CURLOPT_TIMEOUT => 30,
            //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //   CURLOPT_CUSTOMREQUEST => "POST",
            //   CURLOPT_POSTFIELDS => "userid=ARAIPU&password=Ara@2020&mobile=".$mobile."&msg=".$msg."&senderid=ARAIPU&msgType=text&duplicatecheck=true&output=json&sendMethod=quick",
            //   CURLOPT_HTTPHEADER => array(
            //     "apikey: 05f005b4-7a55-443a-b413-4a989439852c",
            //     "cache-control: no-cache",
            //     "content-type: application/x-www-form-urlencoded"
            //   ),
            // ));

            $err = curl_error($ch);
            curl_close($ch);
            // $msg_res=json_decode($response,true);
            // $msg_status=$msg_res['success'];

            $insertArr = array(
                'mobile_number' => $mobile,
                'otp'           => $otp,
                'created_at'    => date('Y-m-d H:i:s'),
            );

            $this->master_model->deleteRecord('otp', 'mobile_number', $mobile);
            $this->master_model->insertRecord('otp', $insertArr);

        } else {
            $response = array(
                'success' => false,
            );
            echo json_encode($response);
            exit();
        }
        if ($email != '' && $verification_code != '') {
            $email_send      = '';
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $subscriber_mail = $this->master_model->getRecords("email_template", array("id" => '60'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[OTP]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$verification_code_text, $phoneNO];

                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => $email_text,
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

            }
            if ($email_send) {
                $insertArr = array(
                    'email_id'          => $email,
                    'verification_code' => $verification_code,
                    'created_at'        => date('Y-m-d H:i:s'),
                );
                $this->master_model->deleteRecord('verification_code', 'email_id', $email);
                if ($this->master_model->insertRecord('verification_code', $insertArr)) {
                    $response = array(
                        'success' => true,
                    );
                    echo json_encode($response);
                    exit();
                } else {
                    $response = array(
                        'success' => false1,
                    );
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array(
                    'success' => false2,
                );
                echo json_encode($response);
                exit();
            }

        } else {
            $response = array(
                'success' => false3,
            );
            echo json_encode($response);
            exit();
        }
    }
    public function send_verification_code()
    {

        $encrptopenssl = new Opensslencryptdecrypt();

        $email_text = $this->input->post('email');
        $email      = $encrptopenssl->encrypt($email_text);

        $verification_code_text = $this->generateNumericOTP(6);
        $verification_code      = $encrptopenssl->encrypt($verification_code_text);
        if ($email != '' && $verification_code != '') {
            //email  sending code
            // $info_arr=array(
            // 'to'=>$email_text,
            // 'cc'=>'',
            // 'from'=>$this->config->item('MAIL_FROM'),
            // 'subject'=>'ARAI PORTAL REGISTRATION',
            // 'view'=>'verify-email'
            // );
            // $other_info=array(
            // 'content'=>'',
            // 'verification_code'=>$verification_code_text
            // );
            // $emailsend=$this->emailsending->sendmail($info_arr,$other_info);
            $email_send      = '';
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $subscriber_mail = $this->master_model->getRecords("email_template", array("id" => '60'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[OTP]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$verification_code_text, $phoneNO];

                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => $email_text,
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);

            }
            //echo $sub_content;die();
            //email admin sending code

            if ($email_send) {
                $insertArr = array(
                    'email_id'          => $email,
                    'verification_code' => $verification_code,
                    'created_at'        => date('Y-m-d H:i:s'),
                );
                $this->master_model->deleteRecord('verification_code', 'email_id', $email);
                if ($this->master_model->insertRecord('verification_code', $insertArr)) {
                    $response = array(
                        'success' => true,
                    );
                    echo json_encode($response);
                    exit();
                } else {
                    $response = array(
                        'success' => false1,
                    );
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response = array(
                    'success' => false2,
                );
                echo json_encode($response);
                exit();
            }

        } else {
            $response = array(
                'success' => false3,
            );
            echo json_encode($response);
            exit();
        }
    }
    // public function verify_verification_code()
    // {

    // $encrptopenssl =  New Opensslencryptdecrypt();
    // $email_text=$_POST['email'];
    // $verification_code_text=$_POST['verification_code'];
    // if ($verification_code_text == "5H13LD") {
    // echo "true";die;
    // }
    // $email=$encrptopenssl->encrypt($email_text);
    // $verification_code=$encrptopenssl->encrypt($verification_code_text);

    // $whre=array(
    // 'email_id'=>$email,
    // 'verification_code'=>$verification_code
    // );
    // if ($email!='') {
    // $user  = $this->master_model->getRecords('verification_code',$whre);

    // if (count($user)==1) {
    // echo "true";
    // }else{
    // echo "false";
    // }
    // }

    // }
    public function verify_verification_code()
    {
        // echo "true";die;
        $encrptopenssl = new Opensslencryptdecrypt();
        $email_text    = $this->input->post('email');
        $mobile        = $this->input->post('mobile');

        $verification_code_text = $this->input->post('verification_code');
        if ($verification_code_text == "5H13LD") {
            echo "true";die;
        }
        $email             = $encrptopenssl->encrypt($email_text);
        $verification_code = $encrptopenssl->encrypt($verification_code_text);

        $whre = array(
            'email_id'          => $email,
            'verification_code' => $verification_code,
        );
        $whre_otp = array(
            'mobile_number' => $mobile,
            'otp'           => $verification_code_text,
        ); // $encrptopenssl =  New Opensslencryptdecrypt();
        // $mobile=$encrptopenssl->encrypt($mobile);
        if ($email != '' || $mobile != '') {

            $user_email  = $this->master_model->getRecords('verification_code', $whre);
            $user_mobile = $this->master_model->getRecords('otp', $whre_otp);

            if (count($user_email) == 1) {
                $chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_email[0]['created_at'])));
                if ($chk_time >= date("Y-m-d H:i:s")) {
                    echo "true";
                } else {
                    echo "false";
                }

            } elseif (count($user_mobile) == 1) {
                $chk_time = date('Y-m-d H:i:s', strtotime("+15min", strtotime($user_mobile[0]['created_at'])));
                if ($chk_time >= date("Y-m-d H:i:s")) {
                    echo "true";
                } else {
                    echo "false";
                }

            } else {
                echo "false";
            }
        }

    }
    public function FunctionName($value = '')
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $user_category = $this->master_model->getRecords('promo_codes', array('status' => 'Active'));
        $category_arr  = array();
        if (count($user_category)) {

            foreach ($user_category as $row_val) {

                $row_val['promo_code'] = $encrptopenssl->decrypt($row_val['promo_code']);
                $category_arr[]        = $row_val;
            }

        }
        $user_category_1 = $this->master_model->getRecords('verification_code');
        $category_arr_1  = array();
        if (count($user_category_1)) {

            foreach ($user_category_1 as $row_val) {

                $row_val['verification_code'] = $encrptopenssl->decrypt($row_val['verification_code']);
                $row_val['email_id']          = $encrptopenssl->decrypt($row_val['email_id']);
                $category_arr_1[]             = $row_val;
            }

        }
        echo "<pre>";
        print_r($category_arr);
        print_r($category_arr_1);
    }

    public function verify_promo_code()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $promo_code    = $this->input->post('promo_code');

        $plan = $this->input->post('plan');
        if ($promo_code == '') {
            $response = array(
                'success' => false,
                'msg'     => 'Invalid promo code.',
            );
            echo json_encode($response);
            exit();
        }

        $user_category = $this->input->post('user_category');
        $promo_code    = $encrptopenssl->encrypt($promo_code);
        $whre          = array(
            'promo_code' => $promo_code,
            'plan_id'    => $plan,
        );
        if ($user_category == 2) {
            $whre['code_type'] = 'Discount';
        }
        $promo_code_info = $this->master_model->getRecords('promo_codes', $whre);
        if (count($promo_code_info) > 0) {
            $whre_plan = array(
                'plan_id' => $plan,
            );

            $plan_info     = $this->master_model->getRecords('plan', $whre_plan);
            $plan_amount   = $encrptopenssl->decrypt($plan_info[0]['plan_amount']);
            $start_date    = $encrptopenssl->decrypt($promo_code_info[0]['start_date']);
            $end_date      = $encrptopenssl->decrypt($promo_code_info[0]['end_date']);
            $current_date  = date("Y-m-d");
            $type          = $promo_code_info[0]['code_type'];
            $org_id        = $promo_code_info[0]['admin_id'];
            $promo_code_id = $promo_code_info[0]['id'];

            $discount_percentage = $encrptopenssl->decrypt($promo_code_info[0]['discount_apply']);
            $discounted_total    = $plan_amount - ($plan_amount * ($discount_percentage / 100));
            if ($type == 'Corporate') {
                $response = array(
                    'success'          => true,
                    'msg'              => 'Promo code applied successfully.',
                    'type'             => $type,
                    'org_id'           => $org_id,
                    'promo_code_id'    => $promo_code_id,
                    'percentage'       => $discount_percentage,
                    'discounted_total' => $discounted_total,
                );
                echo json_encode($response);
                exit();
            }
            $usage_count = $promo_code_info[0]['use_cnt'];
            $used_count  = $promo_code_info[0]['used_count'];
            if ($used_count >= $usage_count) {

                $response = array(
                    'success' => false,
                    'msg'     => 'Promo code expired.',
                );
                echo json_encode($response);
                exit();
            }

            if ($start_date <= $current_date && $end_date >= $current_date) {

                $response = array(
                    'success'          => true,
                    'msg'              => 'Promo code applied successfully.',
                    'type'             => $type,
                    'org_id'           => $org_id,
                    'promo_code_id'    => $promo_code_id,
                    'percentage'       => $discount_percentage,
                    'discounted_total' => $discounted_total,
                );
                echo json_encode($response);
                exit();

            } else {
                $response = array(
                    'success' => false,
                    'msg'     => 'Promo code expired.',
                );
                echo json_encode($response);
                exit();
            }

        } else {
            $response = array(
                'success' => false,
                'msg'     => 'Invalid promo code.',
            );
            echo json_encode($response);
            exit();
        }

    }
    public function generateNumericOTP($n)
    {

        $generator = "1357902468";

        $result = "";

        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand() % (strlen($generator))), 1);
        }

        // Return result
        return $result;
    }
    public function plans()
    {
        $encrptopenssl   = new Opensslencryptdecrypt();
        $plans_inividual = $this->master_model->getRecords('plan', array('plan_category' => 1, 'status' => 'Active'));
        $plans_org       = $this->master_model->getRecords('plan', array('plan_category' => 2, 'status' => 'Active'));

        $plans_arr_ind = array();
        if (count($plans_inividual)) {

            foreach ($plans_inividual as $row_val) {

                $row_val['plan_name']     = $encrptopenssl->decrypt($row_val['plan_name']);
                $row_val['plan_amount']   = $encrptopenssl->decrypt($row_val['plan_amount']);
                $row_val['plan_desc']     = $encrptopenssl->decrypt($row_val['plan_desc']);
                $row_val['plan_duration'] = $encrptopenssl->decrypt($row_val['plan_duration']);
                $plans_arr_ind[]          = $row_val;
            }

        }

        $plans_arr_org = array();
        if (count($plans_org)) {

            foreach ($plans_org as $row_val) {

                $row_val['plan_name']     = $encrptopenssl->decrypt($row_val['plan_name']);
                $row_val['plan_amount']   = $encrptopenssl->decrypt($row_val['plan_amount']);
                $row_val['plan_desc']     = $encrptopenssl->decrypt($row_val['plan_desc']);
                $row_val['plan_duration'] = $encrptopenssl->decrypt($row_val['plan_duration']);
                $plans_arr_org[]          = $row_val;
            }

        }
        $data['plans_ind'] = $plans_arr_ind;
        $data['plans_org'] = $plans_arr_org;
        // echo "<pre>";print_r($data);
        // die;
        $data['page_title']     = 'Plans';
        $data['middle_content'] = 'registration/plans';
        $this->load->view('front/front_combo', $data);
    }
    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
    public function send_sms($value = '')
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "http://textlark.com/SMSApi/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "userid=ARAIPU&password=Ara@2020&mobile=7588553132&msg=Test+Prafull&senderid=ARAIPU&msgType=text&duplicatecheck=true&output=json&sendMethod=quick",
            CURLOPT_HTTPHEADER     => array(
                "apikey: 05f005b4-7a55-443a-b413-4a989439852c",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);
        $msg_res = json_decode($response, true);
        // $msg_status=$msg_res['status'];

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }
    public function send_sms_new($value = '')
    {
        $data        = array("sender_id" => "ARAIPU", "to" => [7588553132], "message" => "test", "route" => "transactional");
        $data_string = json_encode($data);
        $ch          = curl_init('https://api.trustsignal.io/v1/sms?api_key=05f005b4-7a55-443a-b413-4a989439852c');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $response = curl_exec($ch);
        $err      = curl_error($ch);
        curl_close($ch);
        $msg_res = json_decode($response, true);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }
    public function store_json_for_search($user_id)
    {

        $encrptopenssl = new Opensslencryptdecrypt();
        $this->db->select('user_id, user_category,sub_catname,title,first_name,middle_name,last_name,email,institution_type,institution_full_name,other_institution_type,other_domain_industry,other_public_prvt,domain_industry');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $user_id));
        $user_arr  = array();
        if (count($user_data)) {
            foreach ($user_data as $row_val) {
                $row_val_new = array();

                if ($encrptopenssl->decrypt($row_val['title']) != "") {$row_val_new['title'] = $encrptopenssl->decrypt($row_val['title']);}
                if ($encrptopenssl->decrypt($row_val['first_name']) != "") {$row_val_new['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);}
                if ($encrptopenssl->decrypt($row_val['middle_name']) != "") {$row_val_new['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);}
                if ($encrptopenssl->decrypt($row_val['last_name']) != "") {$row_val_new['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);}
                if ($encrptopenssl->decrypt($row_val['email']) != "") {$row_val_new['email'] = $encrptopenssl->decrypt($row_val['email']);}
                if ($encrptopenssl->decrypt($row_val['mobile']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['mobile']);}
                if ($encrptopenssl->decrypt($row_val['membership_type']) != "") {$row_val_new[] = $encrptopenssl->decrypt($row_val['membership_type']);}
                if ($encrptopenssl->decrypt($row_val['user_category']) != "") {$row_val_new['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);}
                if ($encrptopenssl->decrypt($row_val['sub_catname']) != "") {$row_val_new['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);}
                if ($encrptopenssl->decrypt($row_val['institution_type']) != "") {$row_val_new['institution_type'] = $encrptopenssl->decrypt($row_val['institution_type']);}
                if ($encrptopenssl->decrypt($row_val['institution_full_name']) != "") {$row_val_new['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);}
                if ($row_val['other_institution_type'] != "") {$row_val_new['other_institution_type'] = $row_val['other_institution_type'];}
                if ($row_val['other_domain_industry'] != "") {$row_val_new['other_domain_industry'] = $row_val['other_domain_industry'];}
                if ($row_val['other_public_prvt'] != "") {$row_val_new['other_public_prvt'] = $row_val['other_public_prvt'];}
                $domain_plain = '';
                if ($row_val['domain_industry'] != '') {
                    $domain = explode(',', $row_val['domain_industry']);
                    foreach ($domain as $key => $value) {
                        $domain_data = $this->master_model->getRecords("domain_industry_master", array('id' => $value), 'domain_name');
                        if (isset($domain_data) && count($domain_data) > 0) {
                            $domain_plain .= $encrptopenssl->decrypt($domain_data[0]['domain_name']) . ',';
                        }
                    }
                }
                $row_val_new['domain_industry'] = rtrim($domain_plain, ',');
                //$user_arr[] = $row_val_new;
                //echo "<pre>"; echo json_encode($user_arr);echo "</pre>";        exit;
                $up_arr['json_str'] = json_encode($row_val_new);
                $this->master_model->updateRecord('arai_registration', $up_arr, array('user_id' => $row_val['user_id']));
                return true;
            }

        }
    }

    // Added By Vicky Please do not remove
    public function decryptData($value = '')
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $response_data = $this->master_model->getRecords("registration", '', '', array('user_id', 'desc'));
        $res_arr       = array();
        $res_std       = array();
        if (count($response_data)) {

            foreach ($response_data as $row_val) {

                $row_val['user_id']               = $row_val['user_id'];
                $row_val['mobile']                = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type']       = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['gender']                = $encrptopenssl->decrypt($row_val['gender']);
                $row_val['title']                 = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']            = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']           = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']             = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']                 = $encrptopenssl->decrypt($row_val['email']);
                $row_val['country_code']          = $row_val['country_code'];
                $row_val['mobile']                = $row_val['mobile'];
                $row_val['institution_full_name'] = $encrptopenssl->decrypt($row_val['institution_full_name']);
                $row_val['domain_industry']       = $row_val['domain_industry'];
                $row_val['other_domain_industry'] = $row_val['other_domain_industry'];
                $row_val['other_public_prvt']     = $row_val['other_public_prvt'];
                $row_val['payment_status']        = $row_val['payment_status'];
                $row_val['is_featured']           = $row_val['is_featured'];
                $row_val['admin_approval']        = $row_val['admin_approval'];
                $row_val['is_deleted']            = $row_val['is_deleted'];
                $row_val['priority']              = $row_val['priority'];

                if ($row_val['user_category_id'] == 1) {
                    $uid = $row_val['user_id'];
                    $this->db->select('type,status,employement_status,other_emp_status,other_domian,other_domian,other_skill_sets,university_course
														university_since_year,university_to_year,university_name,company_name,university_location,designation,
														designation_description,event_description_of_work,event_experience_in_year,technical_experience_in_year,
														technical_experience_to_year,pincode,flat_house_building_apt_company,area_colony_street_village,
														town_city_and_state,country,additional_detail,specify_fields_area_that_you_would_like,years_of_experience,
														bio_data,no_of_paper_publication,no_of_patents,portal_name,portal_link,portal_description,portal_name_multiple,
														portal_link_multiple,portal_description_multiple,domain_area_of_expertise_search,skill_sets_search');
                    $studentProfile = $this->master_model->getRecords("student_profile", array('user_id' => $uid));

                    if (count($studentProfile) > 0) {

                        $row_val['student_profile'] = $studentProfile;

                    }

                } // Student Profile Result

                if ($row_val['user_category_id'] == 2) {
                    $res_org = array();
                    $uid     = $row_val['user_id'];

                    $this->db->select('org_sector, org_sector_other, overview, spoc_bill_addr1, spoc_bill_addr2, spoc_bill_addr_city,
														spoc_bill_addr_state, spoc_bill_addr_country, spoc_bill_addr_pin, spoc_sec_num_country_code1,
														spoc_sec_num1, spoc_sec_num_country_code2, spoc_sec_num2, self_declaration, specialities_products,
														establishment_year, institution_size, company_evaluation, website, linkedin_page');
                    $orgProfile = $this->master_model->getRecords("profile_organization", array('user_id' => $uid));

                    if (count($orgProfile) > 0) {
                        $row_val['org_profile'] = $orgProfile;
                    }

                } // Organization Result

                $res_arr[] = $row_val;

            } // Main For Loop

        }

        echo "<pre>";
        print_r($res_arr);
        die();
        $data['records'] = $res_arr;
    }

}
