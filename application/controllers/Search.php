<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Search extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('common');
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
    }

    public function index()
    {
        $data['page_title']     = '';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'search/search';
        $data['posted_keyword'] = '';

        if (count($_POST)) {
            $data['posted_keyword'] = $this->input->post('search_keyword');           
        }
        $this->load->view('front/front_combo', $data);
    }

    public function get_search_data_ajax()
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $start                = $this->input->post('start', true);
        $data['limit']        = $limit        = $this->input->post('limit', true);
        $data['new_start']    = $new_start    = $start + $limit;
        $data['is_show_more'] = $this->input->post('is_show_more', true);

        $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING);

        $search_str_res     = '';
        $search_str_kr      = '';
        $search_str_blog    = '';
        $search_str_webinar = '';
        $search_str_expert  = '';
        $search_str_admin   = '';
        $search_str_tech    = '';
        $search_str_ch='';
        if ($keyword) {
            $search_str_res = " ( r.id_disp LIKE '%" . $keyword . "%' OR r.resouce_name LIKE '%" . $keyword . "%' OR r.resource_details LIKE '%" . $keyword . "%' OR r.resource_specification LIKE '%" . $keyword . "%' OR r.owner_name LIKE '%" . $keyword . "%' OR c.city_name LIKE '%" . $keyword . "%' ) ";

            $search_str_kr = " ( k.kr_id_disp LIKE '%" . $keyword . "%' OR k.title_of_the_content LIKE '%" . $keyword . "%' OR k.kr_description LIKE '%" . $keyword . "%' OR k.kr_type_other LIKE '%" . $keyword . "%' OR k.author_name LIKE '%" . $keyword . "%' ) ";

            $search_str_blog = " ( b.blog_id_disp LIKE '%" . $keyword . "%' OR b.blog_title LIKE '%" . $keyword . "%' OR b.blog_description LIKE '%" . $keyword . "%' OR b.technology_other LIKE '%" . $keyword . "%' OR b.author_name LIKE '%" . $keyword . "%' OR b.author_professional_status LIKE '%" . $keyword . "%' OR b.author_org_name LIKE '%" . $keyword . "%' OR b.author_description LIKE '%" . $keyword . "%' OR b.author_about LIKE '%" . $keyword . "%') ";

            $search_str_webinar = " ( w.webinar_name LIKE '%" . $keyword . "%' OR w.webinar_technology_other LIKE '%" . $keyword . "%' OR w.webinar_breif_info LIKE '%" . $keyword . "%' OR w.webinar_key_points LIKE '%" . $keyword . "%' OR w.webinar_about_author LIKE '%" . $keyword . "%') ";

            $search_str_tech = " ( t.id_disp LIKE '%" . $keyword . "%' OR t.technology_title LIKE '%" . $keyword . "%' OR t.author_name LIKE '%" . $keyword . "%' OR t.company_profile LIKE '%" . $keyword . "%' OR t.technology_abstract LIKE '%" . $keyword . "%' OR t.technology_value LIKE '%" . $keyword . "%' OR t.important_features LIKE '%" . $keyword . "%' OR t.application LIKE '%" . $keyword . "%' OR t.intellectual_property LIKE '%" . $keyword . "%' OR t.tag_other LIKE '%" . $keyword . "%') ";

            $search_str_admin = " ( au.feed_title LIKE '%" . $keyword . "%' ) ";

            $search_str_ch = " ( c.challenge_title_decrypt LIKE '%" . $keyword . "%' ) ";

            $search_str_expert = "( reg.first_name_decrypt LIKE '%" . $keyword . "%' ||
                    reg.middle_name_decrypt LIKE '%" . $keyword . "%' ||
                    reg.last_name_decrypt LIKE '%" . $keyword . "%' ||
                    CONCAT_WS( ' ', reg.first_name_decrypt,reg.middle_name_decrypt,reg.last_name_decrypt ) LIKE  '%" . $keyword . "%' ||
                    CONCAT_WS( ' ', reg.first_name_decrypt,reg.last_name_decrypt ) LIKE  '%" . $keyword . "%' )";
        }


        //START RESOURCE SHARING
        if ($search_str_res != "") {$this->db->where($search_str_res);}
        $select_res = "r.*,c.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags";
        $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
        $data['resource_sharing'] = $resource_sharing = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_block' => '0'), $select_res, '');

        //END RESOURCE SHARING

        //START KR
        if ($search_str_kr != "") {$this->db->where($search_str_kr);}
        $select_kr = "k.xOrder,k.feeds_module_id,rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags,k.tag_other, k.author_name, k.created_on,k.technology_other,(SELECT GROUP_CONCAT(technology_name SEPARATOR '##') FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture,k.is_featured";
        $this->db->join('arai_knowledge_repository_type_master rt', 'rt.id = k.kr_type', 'LEFT', false);
        $this->db->join('arai_registration r', 'r.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_profile_organization org', 'org.user_id = k.user_id', 'LEFT', false);
        $this->db->join('arai_student_profile sp', 'sp.user_id = k.user_id', 'LEFT', false);
        $data['knowledge_repository'] = $knowledge_repository = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.is_block' => '0'), $select_kr, '');
        //END KR

        //START BLOG
        if ($search_str_blog != "") {$this->db->where($search_str_blog);}
        $select_blog = "b.xOrder,b.is_featured,b.feeds_module_id,b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
        $this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
        $this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
        $this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
        $data['blogs'] = $blogs = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_block' => '0'), $select_blog, '');
        //END KR

        if ($search_str_webinar != "") {$this->db->where($search_str_webinar);}
        $select_w = "w.xOrder,w.is_featured,w.w_id,w.webinar_id,w.webinar_name,w.banner_img,w.createdAt as created_on,w.webinar_start_time,w.feeds_module_id,w.webinar_date";
        $webinars = $this->master_model->getRecords("webinar w", array('w.is_deleted' => '0', 'w.admin_status' => 'Approved'), $select_w, '');
       

        //START CHALLENGE
        if ($search_str_ch != "") {$this->db->where($search_str_ch);}
        $select_ch     = "c.is_featured,c.c_id,c.challenge_title,c.banner_img,c.company_name,c.challenge_details,c.challenge_launch_date,c.challenge_close_date,c.createdAt as created_on,c.feeds_module_id";
        $featuredChall = $this->master_model->getRecords('challenge c', array('challenge_status' => "Approved", 'is_deleted' => "0"), $select_ch, '');

        $chArr = array();
        if (count($featuredChall)) {

            foreach ($featuredChall as $feat_val) {

                $feat_val['challenge_title']       = $encrptopenssl->decrypt($feat_val['challenge_title']);
                $feat_val['banner_img']            = $encrptopenssl->decrypt($feat_val['banner_img']);
                $feat_val['company_name']          = $encrptopenssl->decrypt($feat_val['company_name']);
                $feat_val['challenge_details']     = $encrptopenssl->decrypt($feat_val['challenge_details']);
                $feat_val['challenge_launch_date'] = $feat_val['challenge_launch_date'];
                $feat_val['challenge_close_date']  = $feat_val['challenge_close_date'];
                $chArr[]                           = $feat_val;
            }

        }
        //END CHALLENGE

        // START Expert
        if ($search_str_expert != "") {$this->db->where($search_str_expert);}
        $this->db->select('reg.xOrder,reg.is_featured,reg.user_id, reg.title,reg.first_name, reg.last_name, sp.company_name, sp.designation, sp.years_of_experience, sp.no_of_paper_publication, sp.profile_picture,bio_data,reg.feeds_module_id,reg.createdAt as created_on');
        $this->db->join('arai_student_profile sp', 'reg.user_id = sp.user_id', 'left', false);
        $Featured_Experts = $this->master_model->getRecords('arai_registration reg', array('reg.status' => "Active", 'reg.user_sub_category_id' => '11', 'reg.is_deleted' => '0'), '', '');

        $featuredExpertsArr = array();

        if (count($Featured_Experts) > 0) {
            foreach ($Featured_Experts as $key => $featured_experts_val) {

                $featured_experts_val['title']      = $encrptopenssl->decrypt($featured_experts_val['title']);
                $featured_experts_val['first_name'] = $encrptopenssl->decrypt($featured_experts_val['first_name']);
                $featured_experts_val['last_name']  = $encrptopenssl->decrypt($featured_experts_val['last_name']);

                $featured_experts_val['company_name']            = $encrptopenssl->decrypt($featured_experts_val['company_name']);
                $featured_experts_val['no_of_paper_publication'] = $featured_experts_val['no_of_paper_publication'];
                $featured_experts_val['years_of_experience']     = $encrptopenssl->decrypt($featured_experts_val['years_of_experience']);
                $featured_experts_val['profile_picture']         = $featured_experts_val['profile_picture'];
                $featured_experts_val['designation']             = json_decode($featured_experts_val['designation']);
                $featured_experts_val['bio_data']                = $encrptopenssl->decrypt($featured_experts_val['bio_data']);
                $featuredExpertsArr[] = $featured_experts_val;
            }
        }

        //END EXPERT

        //START ADMIN UPDATES

        if ($search_str_admin != "") {$this->db->where($search_str_admin);}

        $select_admin_updates = "au.*";
        $data['admin_updates'] = $admin_updates = $this->master_model->getRecords("admin_update au", array('au.is_deleted' => '0', 'au.admin_status' => '1', 'au.is_block' => '0'), $select_admin_updates, '');
        //END ADMIN UPDATES

        //START TECHNOLOGY TRANSFER
        if ($search_str_tech != "") {$this->db->where($search_str_tech);}
        $data['technology_transfer'] = $technology_transfer = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_block' => '0'));
        //END TECHNOLOGY TRANSFER

        $feeds_merged = array_merge($resource_sharing, $knowledge_repository, $blogs, $webinars, $chArr, $featuredExpertsArr, $admin_updates, $technology_transfer);

        // Check common helper for below custom array sorting and filter functions
        $feeds = sort_array_by_date($feeds_merged);


        //pagination is done using array slice method
        $nonfeatured_feeds = array_slice($feeds, $start, $limit);
        // echo "<pre>";
        // print_r($Nonfeatured_feeds);die;
        $data['nonfeatured_feeds']           = $nonfeatured_feeds;
        $data['all_nonfeatured_feeds_count'] = count($feeds);


        $data['feeds_count'] = count($feeds_merged);
        $default_image       = $this->master_model->getRecords("arai_feeds_banner", array('banner_id' => '1'));

        // echo "<pre>";    print_r($data);die;
        $data['default_img_url'] = $default_image[0]['banner'];
        $response = $this->load->view('front/search/incSearch', $data, true);
        $result['flag']           = "success";
        // print_r($result['response']);die;
        echo $response;

    }

}