<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
		Class : Challenge Module
		Author : Vicky K
	*/
	
	class Challenge extends CI_Controller 
	{
		function __construct() {
			parent::__construct();
			$this->load->helper('security');
			$this->load->model('Common_model_sm');	
			$this->load->library('Opensslencryptdecrypt'); 
			$this->load->helper('auth');
			$this->load->helper('text');
			//$this->check_permissions->is_logged_in();
			ini_set('upload_max_filesize', '100M');  
			ini_set('post_max_size', '100M');  
		} 
		
		
		public function index(){
			//$module_id = 8;
			// $this->check_permissions->is_authorise($module_id);   
			
			$this->check_permissions->is_logged_in();
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			$curr_date = date('Y-m-d');
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$this->db->where("c.challenge_close_date >= ", $curr_date);
			$challenge_data = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "c.challenge_status" => 'Approved'),'',array("c.c_id" => 'DESC'),0,6);		
			//echo $this->db->last_query();
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$this->db->where("c.challenge_close_date >= ", $curr_date);
			$data['total'] = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active', "c.challenge_status" => 'Approved'));
			
			$res_arr = array();
			if(count($challenge_data) > 0){
				
				foreach($challenge_data as $row_val){
					
					$audience_pref_id 	= $row_val['audience_pref_id'];
					if($audience_pref_id != '0'){
						$explode = explode(",",$audience_pref_id);
						$valArr = array();	
						foreach($explode as $val){
							
							$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
							$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
							array_push($valArr, $audienceName);					
						}
						$implode = implode(",", $valArr);
						} else {
						$implode = '';
					}
					
					
					$row_val['challenge_title'] 	= $encrptopenssl->decrypt($row_val['challenge_title']);	
					$row_val['banner_img'] 			= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['challenge_details'] 	= $encrptopenssl->decrypt($row_val['challenge_details']);	
					$row_val['company_name'] 		= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['challenge_launch_date'] = $row_val['challenge_launch_date'];
					$row_val['challenge_close_date'] = $row_val['challenge_close_date'];
					$row_val['c_id'] 				= $row_val['c_id'];
					$row_val['audience_pref'] 		= $implode;	
					$res_arr[] = $row_val;
				}	
				
			}
			
			
			//echo "<pre>";print_r($res_arr);die();
			// Technology Data
			$res_tech = array();
			$technology_data = $this->master_model->getRecords("technology_master", array("status" => 'Active'));
			foreach($technology_data as $techval){
				
				$techval['technology_name'] 	= $encrptopenssl->decrypt($techval['technology_name']);
				$techval['id'] 					= $techval['id'];
				$res_tech[] = $techval;
			}
			
			// Tags Data
			$res_tag = array();
			$tags_data = $this->master_model->getRecords("tags", array("status" => 'Active'));
			foreach($tags_data as $tagval){
				
				$tagval['tag_name'] 	= $encrptopenssl->decrypt($tagval['tag_name']);
				$tagval['id'] 			= $tagval['id'];
				$res_tag[] = $tagval;
			}
			
			// Audience Data
			$res_audi = array();
			$audience_data = $this->master_model->getRecords("audience_pref", array("status" => 'Active'));
			foreach($audience_data as $audival){
				
				$audival['preference_name'] 	= $encrptopenssl->decrypt($audival['preference_name']);
				$audival['id'] 			= $audival['id'];
				$res_audi[] = $audival;
			}
			
			// TRL Data
			$res_trl = array();
			$trl_data = $this->master_model->getRecords("trl", array("status" => 'Active'));
			foreach($trl_data as $trlval){
				
				$trlval['trl_name'] 	= $encrptopenssl->decrypt($trlval['trl_name']);
				$trlval['id'] 			= $trlval['id'];
				$res_trl[] = $trlval;
			}
			
			//echo "<pre>";print_r($res_trl);die();
			
			$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
			// Consortium Partners 
			$consortArr = array();
			if(count($consortium_mgt)){				
				
				foreach($consortium_mgt as $con_val){
					
					$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
					$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
					$consortArr[] = $con_val;
					
				}
				
			}
			$data['limit']=6;
			$data['consortium_list']= $consortArr;
			$data['trl_data'] 	= $res_trl;
			$data['audience_data'] 	= $res_audi;
			$data['tag_data'] 		 = $res_tag;
			$data['technology_data'] = $res_tech;
			$data['challenge_data']  = $res_arr; 	
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/index';
			$this->load->view('front/front_combo',$data);
		}
		

		function getChalengeDataAjax(){
			
			ini_set('display_errors', '1');
			ini_set('display_startup_errors', '1');
			error_reporting(E_ALL);
        $encrptopenssl                                = new Opensslencryptdecrypt();
        $csrf_new_token                               = $this->security->get_csrf_hash();
        $result['csrf_new_token']                     = $csrf_new_token;
       
        $search_str                                   = '';

        if (isset($_POST) && count($_POST) > 0) {
         
            $nf_start     = $this->input->post('nf_start', true);
            $data['nf_limit']=$nf_limit     = $this->input->post('nf_limit', true);
            $is_show_more = $this->input->post('is_show_more', true);
            $challenge_status = $this->input->post('challenge_status', true);
            
            if ($nf_start != "" && $nf_limit != "" && $is_show_more != "") {
                $result['flag'] = "success";
                

                //START : FOR SEARCH KEYWORD
                $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword'));
                if ($keyword) {
                    $search_str .= " ( c.challenge_id LIKE '%" . $keyword . "%' OR c.challenge_title_decrypt LIKE '%" . $keyword . "%') ";
                }
                //END : FOR SEARCH KEYWORD
								$close_date = trim($this->input->post('close_date'));
								if($close_date){
									$old_date = explode('-', $close_date); 
									$close_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];		
									$old_search_str = $search_str;
								  if ($old_search_str != '') {$search_str .= " AND ";}	
									$search_str .= " challenge_close_date = '".$close_date ."'";
								}
								
								$category_id = trim($this->input->post('category_id'));
								if($category_id){
									$this->db->where('FIND_IN_SET('. $category_id . ', c.audience_pref_id)');
								}

								$challenage_type = trim($this->input->post('challenage_type'));
								if($challenage_type){
									$old_search_str = $search_str;
								  if ($old_search_str != '') {$search_str .= " AND ";}	
									$search_str .= " challenge_visibility = '".$challenage_type ."'";
								}

								$funding_type = trim($this->input->post('funding_type'));
								if($funding_type){
									$old_search_str = $search_str;
								  if ($old_search_str != '') {$search_str .= " AND ";}	
									$search_str .= " is_external_funding = '".$funding_type ."'";
								}

								$is_exclusive = trim($this->input->post('is_exclusive'));
								if($is_exclusive){
									$old_search_str = $search_str;
								  if ($old_search_str != '') {$search_str .= " AND ";}	
									$search_str .= " is_exclusive_challenge = '".$is_exclusive ."'";
								}


                $technology = trim($this->input->post('technology'));
                if ($technology != '') {
                    $technology_arr = explode(",", $technology);
                    if (count($technology_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($technology_arr as $res) {
                            $search_str .= " c.technology_id =  " . $res . "  ";
                            if ($i != (count($technology_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }

                //START : FOR SEARCH TAG
                $tag = trim($this->input->post('tag'));

                if ($tag != '') {
                    $tag_arr = explode(",", $tag);
                    if (count($tag_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($tag_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',c.tags_id)";
                            if ($i != (count($tag_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
            	
                //END : FOR SEARCH TAG

                $trl = trim($this->input->post('trl'));
                $trl=$encrptopenssl->encrypt($trl);
                if ($trl != '') {

                    $trl_arr = explode(",", $trl);
                    if (count($trl_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($trl_arr as $res) {
                            $search_str .= " c.trl_solution =  '$res'   ";
                            if ($i != (count($trl_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }

                }


                // START : FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
                // $this->db->order_by('tech_id', 'DESC', false);
               	$curr_date = date('Y-m-d');
								$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
								$this->db->where("r.is_deleted","0");
								if($challenge_status=="open"){
								$this->db->where("c.challenge_close_date >= ", $curr_date);
								}
								if($challenge_status=="closed"){
								$this->db->where("c.challenge_close_date < ", $curr_date);
								}

								$challenge_data = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "c.challenge_status" => 'Approved'),'',array("c.c_id" => 'DESC'),$nf_start,$nf_limit);		
								// echo $this->db->last_query();

								$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
								$this->db->where("r.is_deleted","0");
								if($challenge_status=="open"){
								$this->db->where("c.challenge_close_date >= ", $curr_date);
								}
								if($challenge_status=="closed"){
								$this->db->where("c.challenge_close_date < ", $curr_date);
								}

						    if ($search_str != "") {$this->db->where($search_str);}
								$data['total'] = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active', "c.challenge_status" => 'Approved'));
								
								$res_arr = array();
								if(count($challenge_data) > 0){
									
									foreach($challenge_data as $row_val){
										
										$audience_pref_id 	= $row_val['audience_pref_id'];
										if($audience_pref_id != '0'){
											$explode = explode(",",$audience_pref_id);
											$valArr = array();	
											foreach($explode as $val){
												
												$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
												$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
												array_push($valArr, $audienceName);					
											}
											$implode = implode(",", $valArr);
											} else {
											$implode = '';
										}
										
										
										$row_val['challenge_title'] 	= $encrptopenssl->decrypt($row_val['challenge_title']);	
										$row_val['banner_img'] 			= $encrptopenssl->decrypt($row_val['banner_img']);
										$row_val['challenge_details'] 	= $encrptopenssl->decrypt($row_val['challenge_details']);	
										$row_val['company_name'] 		= $encrptopenssl->decrypt($row_val['company_name']);
										$row_val['challenge_launch_date'] = $row_val['challenge_launch_date'];
										$row_val['challenge_close_date'] = $row_val['challenge_close_date'];
										$row_val['c_id'] 				= $row_val['c_id'];
										$row_val['audience_pref'] 		= $implode;	
										$res_arr[] = $row_val;
									}	
									
								}
								$data['challenge_data']  = $res_arr; 

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
              
             
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG xOrder

              
                $data['new_start']               = $new_start               = $nf_start + $nf_limit;
          
                $result['challenge_html']               = $this->load->view('front/challenge/inc_challenge_listing', $data, true);
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }
        echo json_encode($result);
    
		}
		
		public function closed(){
			//$module_id = 8;
			// $this->check_permissions->is_authorise($module_id);   
			
			$this->check_permissions->is_logged_in();
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			$curr_date = date('Y-m-d');
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$this->db->where("c.challenge_close_date < ", $curr_date);
			$challenge_data = $this->master_model->getRecords("challenge c", array("c.status" => 'Active'),'',array("c.c_id" => 'DESC'),0,6);		
			//echo $this->db->last_query();
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$this->db->where("c.challenge_close_date < ", $curr_date);
			$data['total'] = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "r.status" => 'Active'));
			
			$res_arr = array();
			if(count($challenge_data) > 0){
				
				foreach($challenge_data as $row_val){
					
					$audience_pref_id 	= $row_val['audience_pref_id'];
					if($audience_pref_id != '0'){
						$explode = explode(",",$audience_pref_id);
						$valArr = array();	
						foreach($explode as $val){
							
							$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
							$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
							array_push($valArr, $audienceName);					
						}
						$implode = implode(",", $valArr);
						} else {
						$implode = '';
					}
					
					
					$row_val['challenge_title'] 	= $encrptopenssl->decrypt($row_val['challenge_title']);	
					$row_val['banner_img'] 			= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['challenge_details'] 	= $encrptopenssl->decrypt($row_val['challenge_details']);	
					$row_val['company_name'] 		= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['challenge_launch_date'] = $row_val['challenge_launch_date'];
					$row_val['challenge_close_date'] = $row_val['challenge_close_date'];
					$row_val['c_id'] 				= $row_val['c_id'];
					$row_val['audience_pref'] 		= $implode;	
					$res_arr[] = $row_val;
				}	
				
			}
			
			
			//echo "<pre>";print_r($res_arr);die();
			// Technology Data
			$res_tech = array();
			$technology_data = $this->master_model->getRecords("technology_master", array("status" => 'Active'));
			foreach($technology_data as $techval){
				
				$techval['technology_name'] 	= $encrptopenssl->decrypt($techval['technology_name']);
				$techval['id'] 					= $techval['id'];
				$res_tech[] = $techval;
			}
			
			// Tags Data
			$res_tag = array();
			$tags_data = $this->master_model->getRecords("tags", array("status" => 'Active'));
			foreach($tags_data as $tagval){
				
				$tagval['tag_name'] 	= $encrptopenssl->decrypt($tagval['tag_name']);
				$tagval['id'] 			= $tagval['id'];
				$res_tag[] = $tagval;
			}
			
			// Audience Data
			$res_audi = array();
			$audience_data = $this->master_model->getRecords("audience_pref", array("status" => 'Active'));
			foreach($audience_data as $audival){
				
				$audival['preference_name'] 	= $encrptopenssl->decrypt($audival['preference_name']);
				$audival['id'] 			= $audival['id'];
				$res_audi[] = $audival;
			}
			
			// TRL Data
			$res_trl = array();
			$trl_data = $this->master_model->getRecords("trl", array("status" => 'Active'));
			foreach($trl_data as $trlval){
				
				$trlval['trl_name'] 	= $encrptopenssl->decrypt($trlval['trl_name']);
				$trlval['id'] 			= $trlval['id'];
				$res_trl[] = $trlval;
			}
			
			$this->sortBy('trl_name',   $res_trl);
			//echo "<pre>";print_r($res_trl);die();
			
			$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
			// Consortium Partners 
			$consortArr = array();
			if(count($consortium_mgt)){				
				
				foreach($consortium_mgt as $con_val){
					
					$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
					$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
					$consortArr[] = $con_val;
					
				}
				
			}
			$data['limit']=6;
			$data['consortium_list']= $consortArr;
			$data['trl_data'] 	= $res_trl;
			$data['audience_data'] 	= $res_audi;
			$data['tag_data'] 		 = $res_tag;
			$data['technology_data'] = $res_tech;
			$data['challenge_data']  = $res_arr; 	
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/closed';
			$this->load->view('front/front_combo',$data);
		}
		
		public function filter_more(){
			error_reporting(0);
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Post Values
			$challengeDate 	= $this->input->post('ch_date');
			$technologys 	= $this->input->post('technologys');
			$tags 			= $this->input->post('tags');
			$audience 		= $this->input->post('audience');
			$visibility 	= $this->input->post('visibility');
			$if_funding 	= $this->input->post('if_funding');
			$if_exclusive 	= $this->input->post('if_exclusive');
			$trls 			= $this->input->post('trls');			
			$mode 			= $this->input->post('mode');
			
			//$close_date 		= date('Y-m-d', strtotime($challengeDate));
			
			$c_data = "";		
			if($challengeDate){
				$old_date = explode('-', $challengeDate); 
				$close_date = $old_date[2].'-'.$old_date[1].'-'.$old_date[0];			
				$c_data .= " AND challenge_close_date = '".$close_date ."'";
			}
			
			
			if(count((array)$technologys) > 0){
				
				$c_data .= " AND (";
				$i = 0;
				foreach($technologys as $t){
					
					if($i!=(count($technologys) - 1)){
						$c_data .= " FIND_IN_SET('".$t."',`technology_id`) OR ";
						} else {
						$c_data .= " FIND_IN_SET('".$t."',`technology_id`))";
					}				
					$i++;
				}
				
			} 
			
			if(count((array)$tags) > 0){
				
				$c_data .= " AND (";
				$s = 0;
				foreach($tags as $d){
					
					if($s!=(count($tags) - 1)){
						$c_data .= " FIND_IN_SET('".$d."',`tags_id`) OR ";
						} else {
						$c_data .= " FIND_IN_SET('".$d."',`tags_id`))";
					}				
					$s++;
				}
				
			} 
			
			if(count((array)$audience) > 0){
				$c_data .= " AND (";
				$a =0;
				foreach($audience as $y){
					
					if($a!=(count($audience) - 1)){
						$c_data .= " FIND_IN_SET('".$y."',`audience_pref_id`) OR ";
						} else {
						$c_data .= " FIND_IN_SET('".$y."',`audience_pref_id`))";
					}				
					$a++;
				}
				
			} 			
			
			if($if_funding)
			{
				$c_data	.= " AND is_external_funding = '1'";				
			} 
			
			if($visibility)
			{
				$c_data	.= " AND challenge_visibility = '".$visibility."'";
			}
			
			if($if_exclusive)
			{
				$c_data	.= " AND is_exclusive_challenge = '1'";
			}
			
			if($trls!=""){
				$trl_ids = $encrptopenssl->encrypt($trls);
				$c_data	.= " AND trl_solution = '".$trl_ids."'";
			}
			
			$curr_date = date('Y-m-d');
			$c_data .= " AND c.challenge_close_date >= '".$curr_date."'";
			
			$id = $this->input->post('id');
			if(empty($id)){
				$id = 0;
			}
			//$id = $this->input->post('id');		
			//$total_cnt = $this->master_model->getRecords("challenge", array("status" => 'Active', "challenge_status" => 'Approved'));
			
			//$showLimit = 6;
			
			if($mode == 'open'){
				
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Approved' ".$c_data." ORDER BY c.c_id DESC";
			} else {
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Closed' ".$c_data." ORDER BY c.c_id DESC";
			}
			
			
			$result 	= $this->db->query($sql)->result();
			
			$showData = '';
			if(count($result) > 0){
				
				foreach($result as $c_data){
					
					
					$audience_pref_id 	= $c_data->audience_pref_id;
					if($audience_pref_id!=""){
						$explode = explode(",",$audience_pref_id);
						$valArr = array();	
						foreach($explode as $val){
							
							$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
							$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
							array_push($valArr, $audienceName);					
						}
						$implode = implode(",", $valArr);
					}
					
					
					
					$nextid = $c_data->c_id;
					
					$str = $encrptopenssl->decrypt($c_data->challenge_details);
					$limit = 200;
					if (strlen($str ) > $limit){
						$str = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...'; 
					}
					
					$str2 = $encrptopenssl->decrypt($c_data->company_name);
					$limit2 = 48;
					if (strlen($str2 ) > $limit2){
						$str2 = substr($str2 , 0, strrpos(substr($str2 , 0, $limit2), ' ')) . '...'; 
					}
					
					
					$datestr = $c_data->challenge_close_date;//Your date
					//$date	 = strtotime($datestr);//Converted to a PHP date (a second count)
					
					if($c_data->challenge_launch_date <= date("Y-m-d") && date("Y-m-d") <= $c_data->challenge_close_date)
					{
					//if($date3 >= $date){{
						//$removeMinus = str_replace("-","", $days);							
						$date1=date_create(date("Y-m-d"));
						$date2=date_create($c_data->challenge_close_date);
						$diff=date_diff($date1,$date2);
						$showCnt = "Days left : ".($diff->format("%a")+1);							
					} 
					else 
					{
						$showCnt = "Closed Participations";
					}
					
					
					
					$fileExist = base_url('assets/challenge/'.$encrptopenssl->decrypt($c_data->banner_img));
					/*if(file_exists($fileExist)) {
						$image = $fileExist;
						} else {
						$image = base_url('assets/news-14.jpg');
					}*/
					
					if ($c_data->banner_img!="") {
						$image = $fileExist;
						} else {
						$image = base_url('assets/news-14.jpg');
					}
					
					$showData .= '<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3"  onclick="document.location.href='.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'">
					<div class="desc-comp-offer-cont">
					<a href="'.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'"><div class="thumbnail-blogs">
					<div class="caption">
					<i class="fa fa-chain"></i>
					</div>
					<img src="'.$image.'" class="img-fluid" alt="'.$encrptopenssl->decrypt($c_data->challenge_title).'" style="height:224px;">
					</div></a>
					<div class="card-content">
					<h5>'.$str2.'</h5>
					<h3>'.$encrptopenssl->decrypt($c_data->challenge_title).'</h3>
					<p class="desc">'.$str.' <a href="javascript:void(0);" class="show_hide_new">Read More</a></p>
					<div class="cardd-footer content-new">';
					
					$showData .= '<div>';
					if($audience_pref_id!=""){		
						$exp = explode(",", $implode);
						foreach($exp as $newname)
						{
							$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">'.$newname.'</button>';
						}
						} else {
						$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>';
					}			
					$showData .= '</div>';
					
					$showData .= '<div class="daysleft"><i class="fa fa-clock-o"></i> '.$showCnt.'</div>
					</div>
					</div>
					</div>
					</div>';				
					
					
				} // Foreach End
				
				/*if($total_cnt > $showLimit){
					
					$showData .= '<div class="col-md-12 text-center show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}*/
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name, "sql" => '');
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b>No Challenges Found</b></div>', "token" => $csrf_test_name, "sql" => '');
				echo json_encode($jsonData);
				
			}
		}
		
		
		public function category_filter(){
			error_reporting(0);
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Post Values
			$cat_id 	= $this->input->post('cat_id');
			$mode 		= $this->input->post('mode');
			if($cat_id != 0){
				
				$c_data = " AND FIND_IN_SET('".$cat_id."',`audience_pref_id`)";
				
				} else {
				$c_data = '';
			}
			
			/*if(count((array)$audience) > 0){
				$c_data .= " AND (";
				$a =0;
				foreach($audience as $y){
				
				if($a!=(count($audience) - 1)){
				$c_data .= " FIND_IN_SET('".$y."',`audience_pref_id`) OR ";
				} else {
				$c_data .= " FIND_IN_SET('".$y."',`audience_pref_id`))";
				}				
				$a++;
				}
				
			} */			
			
			
			$id = $this->input->post('id');
			if(empty($id)){
				$id = 0;
			}
			//$id = $this->input->post('id');		
			//$total_cnt = $this->master_model->getRecords("challenge", array("status" => 'Active', "challenge_status" => 'Approved'));
			
			$curr_date = date('Y-m-d');
			$c_data .= " AND c.challenge_close_date >= '".$curr_date."'";
			
			//$showLimit = 6;
			
			if($mode == 'open'){
				
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Approved' ".$c_data." ORDER BY c.c_id DESC";
			
			} else {
				
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Closed' ".$c_data." ORDER BY c.c_id DESC";
			
			}
			
			//$sql = "SELECT * FROM arai_challenge WHERE status='Active' AND challenge_status = 'Approved' ".$c_data." ORDER BY c_id DESC LIMIT ".$id.",".$showLimit."";
			$result 	= $this->db->query($sql)->result();
			
			$showData = '';
			if(count($result) > 0){
				
				foreach($result as $c_data){
					
					
					$audience_pref_id 	= $c_data->audience_pref_id;
					if($audience_pref_id!=""){
						$explode = explode(",",$audience_pref_id);
						$valArr = array();	
						foreach($explode as $val){
							
							$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
							$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
							array_push($valArr, $audienceName);					
						}
						$implode = implode(",", $valArr);
					}
					
					
					
					$nextid = $c_data->c_id;
					
					$str = $encrptopenssl->decrypt($c_data->challenge_details);
					$limit = 200;
					if (strlen($str ) > $limit){
						$str = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...'; 
					}
					
					$str2 = $encrptopenssl->decrypt($c_data->company_name);
					$limit2 = 48;
					if (strlen($str2 ) > $limit2){
						$str2 = substr($str2 , 0, strrpos(substr($str2 , 0, $limit2), ' ')) . '...'; 
					}
					
					
					$datestr = $c_data->challenge_close_date;//Your date
					//$date	 = strtotime($datestr);//Converted to a PHP date (a second count)
					
					if($c_data->challenge_launch_date <= date("Y-m-d") && date("Y-m-d") <= $c_data->challenge_close_date)
					{
					//if($date3 >= $date){{
						//$removeMinus = str_replace("-","", $days);							
						$date1=date_create(date("Y-m-d"));
						$date2=date_create($c_data->challenge_close_date);
						$diff=date_diff($date1,$date2);
						$showCnt = "Days left : ".($diff->format("%a")+1);							
					} 
					else 
					{
						$showCnt = "Closed Participations";
					}
					
					$fileExist = base_url('assets/challenge/'.$encrptopenssl->decrypt($c_data->banner_img));
					if($c_data->banner_img!=""){
						$image = $fileExist;
						} else {
						$image = base_url('assets/news-14.jpg');
					}
					
					
					
					$showData .= '<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" onclick="document.location.href='.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'">
					<div class="desc-comp-offer-cont">
					<a href="'.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'"><div class="thumbnail-blogs">
					<div class="caption">
					<i class="fa fa-chain"></i>
					</div>
					<img src="'.$image.'" class="img-fluid" alt="'.$encrptopenssl->decrypt($c_data->challenge_title).'" style="height:224px;">
					</div></a>
					<div class="card-content">
					<h5>'.$str2.'</h5>
					<h3>'.$encrptopenssl->decrypt($c_data->challenge_title).'</h3>
					<p class="desc">'.$str.'
					<a href="javascript:void(0);" class="show_hide_new">Read More</a>
					</p>
					<div class="cardd-footer content-new">';
					
					$showData .= '<div>';
					if($audience_pref_id!=""){	
						$exp = explode(",", $implode);
						foreach($exp as $newname)
						{
							$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">'.$newname.'</button>';
						}
						} else {
						$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>';
					}		
					$showData .= '</div>';
					
					$showData .= '<div class="daysleft"><i class="fa fa-clock-o"></i> '.$showCnt.'</div>
					</div>
					</div>
					</div>
					</div>';				
					
					
				} // Foreach End
				
				/*if($total_cnt > $showLimit){
					
					$showData .= '<div class="col-md-12 text-center show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}*/
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b>No Challenges Found</b></div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
		}
		
		public function getmore(){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$id = $this->input->post('id');
			$mode 		= $this->input->post('mode');
			$curr_date = date('Y-m-d');
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$this->db->where("c.challenge_close_date >= ", $curr_date);
			$showLimit = 6;
			
			if($mode == 'open'){
				
				$total_cnt = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "c.challenge_status" => 'Approved'));
				
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Approved' AND c.challenge_close_date >= '".$curr_date."' ORDER BY c.c_id DESC LIMIT ".$id.",".$showLimit."";
				
			} else {
				
				$total_cnt = $this->master_model->getRecords("challenge c", array("c.status" => 'Active', "c.challenge_status" => 'Closed'));
				
				$sql = "SELECT c.* FROM arai_challenge c INNER JOIN arai_registration r ON r.user_id = c.u_id WHERE r.is_deleted = '0' AND c.status='Active' AND c.challenge_status = 'Closed' AND c.challenge_close_date >= '".$curr_date."' ORDER BY c.c_id DESC LIMIT ".$id.",".$showLimit."";
				
			}
			
			$result 	= $this->db->query($sql)->result();	
			
			$showData = '';
			if(count($result) > 0){
				
				foreach($result as $c_data){
					
					
					$audience_pref_id 	= $c_data->audience_pref_id;
					
					$explode = explode(",",$audience_pref_id);
					$valArr = array();	
					foreach($explode as $val){
						
						$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
						$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
						array_push($valArr, $audienceName);					
					}
					$implode = implode(",", $valArr);
					
					
					$nextid = $c_data->c_id;
					
					$str = $encrptopenssl->decrypt($c_data->challenge_details);
					$limit = 200;
					if (strlen($str ) > $limit){
						$str = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...'; 
					}
					
					$str2 = $encrptopenssl->decrypt($c_data->company_name);
					$limit2 = 48;
					if (strlen($str2 ) > $limit2){
						$str2 = substr($str2 , 0, strrpos(substr($str2 , 0, $limit2), ' ')) . '...'; 
					}
					
					
					$datestr = $c_data->challenge_close_date;//Your date
					//$date	 = strtotime($datestr);//Converted to a PHP date (a second count)
					
					if($c_data->challenge_launch_date <= date("Y-m-d") && date("Y-m-d") <= $c_data->challenge_close_date)
					{
					//if($date3 >= $date){{
						//$removeMinus = str_replace("-","", $days);							
						$date1=date_create(date("Y-m-d"));
						$date2=date_create($c_data->challenge_close_date);
						$diff=date_diff($date1,$date2);
						$showCnt = "Days left : ".($diff->format("%a")+1);							
					} 
					else 
					{
						$showCnt = "Closed Participations";
					}					
					
					$fileExist = base_url('assets/challenge/'.$encrptopenssl->decrypt($c_data->banner_img));
					if($c_data->banner_img!=""){
						$image = $fileExist;
						} else {
						$image = base_url('assets/news-14.jpg');
					}
					
					$showData .= '<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" onclick="document.location.href='.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'">
					<div class="desc-comp-offer-cont">
					<a href="'.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'"><div class="thumbnail-blogs">
					<div class="caption">
					<i class="fa fa-chain"></i>
					</div>
					<img src="'.$image.'" class="img-fluid" alt="'.$encrptopenssl->decrypt($c_data->challenge_title).'" style="height:224px;">
					</div></a>
					<div class="card-content">
					<h5>'.$str2.'</h5>
					<h3>'.$encrptopenssl->decrypt($c_data->challenge_title).'</h3>
					<p class="desc">'.$str.'
					<a href="javascript:void(0);" class="show_hide_new">Read More</a>
					</p>
					
					<div class="cardd-footer content-new">';
					
					$showData .= '<div>';
					if($audience_pref_id!=""){		
						$exp = explode(",", $implode);
						foreach($exp as $newname)
						{
							$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">'.$newname.'</button>';
						}
						} else {
						$showData .= '<button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>';
					}		
					$showData .= '</div>';
					
					$showData .= '<div class="daysleft"><i class="fa fa-clock-o"></i> '.$showCnt.'</div>
					</div>
					</div>
					</div>
					</div>';				
					
					
				} // Foreach End
				
				if($total_cnt > $showLimit){
					
					$showData .= '<div class="col-md-12 text-center show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b>No Challenges Found</b></div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
			
		}
		
    public function add()
    {
    	$this->check_permissions->is_admin_approved();
    	$module_id = 1;
    	$this->check_permissions->is_authorise($module_id);
    	$this->check_permissions->is_profile_complete();
			
    	// Load Library
			$this->load->library('upload');
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');
			
			$getDetails = $this->master_model->getRecords("registration", array("user_id" => $user_id));
			if($getDetails[0]['user_category_id'] == '2'){
				
				$data['orgName'] 	= $encrptopenssl->decrypt($getDetails[0]['institution_full_name']);
				$data['email'] 	 	= $encrptopenssl->decrypt($getDetails[0]['email']);
				$data['fullname'] 	= "";
				
				} else {
				
				$data['orgName'] 	= "";
				$data['email'] 	 	= $encrptopenssl->decrypt($getDetails[0]['email']);
				$data['fullname'] 	= $encrptopenssl->decrypt($getDetails[0]['first_name'])." ".$encrptopenssl->decrypt($getDetails[0]['middle_name'])." ".$encrptopenssl->decrypt($getDetails[0]['last_name']);
			}
			
			// Select Draft Challenge Of Login User
			$res_draft = array();
			$getDraftChallenge = $this->master_model->getRecords("challenge", array("u_id" => $user_id, 'challenge_status' => 'Draft'),'',array('c_id' => 'DESC'),0,1);
			//echo $this->db->last_query();
			if(count($getDraftChallenge) > 0){
				
				foreach($getDraftChallenge as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					//echo $this->db->last_query();
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$res_draft[] = $row_val;
					
				}
				
				
			} // For Draft Count End	
			
			
			$data['draft_data'] = $res_draft;
			
			// Technology Master
			$technology_data= $this->master_model->array_sorting($this->master_model->getRecords('technology_master',array('status'=>"Active")), array('id'),'technology_name');			
			
			// Tags Master
			$tag_data= $this->master_model->array_sorting($this->master_model->getRecords('tags',array('status'=>"Active")), array('id'),'tag_name');			
			
			// Audience Preference Master
			$audi_data= $this->master_model->array_sorting($this->master_model->getRecords('audience_pref',array('status'=>"Active")), array('id'),'preference_name');			
			
			// Domain Master
			$domain_data= $this->master_model->array_sorting($this->master_model->getRecords('domain_master',array('status'=>"Active")), array('id'),'domain_name');			
			
			// TRL Master
			$trl_data= $this->master_model->array_sorting($this->master_model->getRecords('trl',array('status'=>"Active")), array('id'),'trl_name');			
			
			// IP Clause Master
			$data['ip_clause_data'] = $this->master_model->getRecords("ip_clause", array("status" => 'Active'));
			
			// State Master
			$data['state'] = $this->master_model->getRecords("states", array("status" => 'Active'),'',array('name' => 'ASC'));
			
			// Check Validation
			$this->form_validation->set_rules('challenge_title', 'Challenge Title', 'required|xss_clean');
			$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('challenge_details', 'Breif Info About Challenge', 'required|xss_clean');	
			$this->form_validation->set_rules('company_details', 'Company Details', 'required|xss_clean');
			$this->form_validation->set_rules('launch_date', 'Launch Date', 'required|xss_clean');
			$this->form_validation->set_rules('close_date', 'Close Date', 'required|xss_clean');
			$this->form_validation->set_rules('is_agree', 'Agree terms & conditions', 'required|xss_clean');
			$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|xss_clean');
			$this->form_validation->set_rules('contact_email', 'Contact Email', 'required|xss_clean');
			$this->form_validation->set_rules('mobile', 'Contact Mobile', 'required|min_length[10]|max_length[10]|xss_clean');
			$this->form_validation->set_rules('office_no', 'Office Number', 'required|xss_clean');
			$this->form_validation->set_rules('visibility', 'Visibility', 'required|xss_clean');
			$this->form_validation->set_rules('ip_cls', 'IP Clause', 'required|xss_clean');
			$this->form_validation->set_rules('is_exclusive_challenge', 'Challenge exclusively listed on TechNovuus', 'required|xss_clean');
			
			// Draft Functionality Start
			if(isset($_POST['draft_btn'])){
				
				$encrptopenssl =  New Opensslencryptdecrypt();
				
				// Challenge ID 
				$update_cid = $this->input->post('update_id');
				
				$pref = $this->input->post('audi_id');
				
				if($pref){				
					$au_arr = implode(",",$pref);
					} else {
					$au_arr = '';
				}
				
				$techID = $this->input->post('techonogy_id');
				
				if($techID){				
					$tech_arr = implode(",",$techID);
					} else {
					$tech_arr = '';
				}
				
				
				$tagID = $this->input->post('tags_id');
				
				if($tagID){				
					$tegs_arr = implode(",",$tagID);
					} else {
					$tegs_arr = '';
				}
				
				
				$domanID = $this->input->post('domain');
				
				if($domanID){				
					$domain_arr = implode(",",$domanID);
					} else {
					$domain_arr = '';
				}
				
				$geographicalID = $this->input->post('geographical');
				if($geographicalID != "") { $geogra_arr = implode(",",$geographicalID); } else { $geogra_arr = ""; }
				$reward = 'No';
				if($this->input->post('if_reward')){
					$reward = $this->input->post('if_reward');
				} 
				
				$fund_if = 'No';
				if($this->input->post('if_funding')){
					$fund_if = $this->input->post('if_funding');
				} 
				
				$ex_fund_if = '0';
				if($this->input->post('is_external_funding')){
					$ex_fund_if = $this->input->post('is_external_funding');
				} 
				
				$is_agree = '0';
				if($this->input->post('is_agree')){
					$is_agree = $this->input->post('is_agree');
				}
				
				$visibility = '0';
				if($this->input->post('visibility')){
					$visibility = $this->input->post('visibility');
				}
				
				$ip_cls = '0';
				if($this->input->post('ip_cls')){
					$ip_cls = $this->input->post('ip_cls');
				} 
				
				$otherTagname       = $this->input->post('other_tag');			
				if($otherTagname){
					$otherTagNames = $encrptopenssl->encrypt($otherTagname);
					} else {
					$otherTagNames = '';
				}		
				
				$share_details = '0';
				if($this->input->post('details_share')){
					$share_details  = $this->input->post('details_share');
				}
				
				$exclusiveDetails = '0';
				if($this->input->post('is_exclusive_challenge')){
					$exclusiveDetails  = $this->input->post('is_exclusive_challenge');
				}
				
				
				
				$userTechnolgy		= $this->input->post('other_technology');
				
				if($this->input->post('launch_date')!=""){
					$l_date = date('Y-m-d',strtotime($this->input->post('launch_date')));
					} else{
					$l_date = '';
				}
				
				if($this->input->post('close_date')!=""){
					$c_date = date('Y-m-d',strtotime($this->input->post('close_date')));
					} else{
					$c_date = '';
				}
				
				$fileArr			= $_FILES;
				$postArr			= $this->input->post();
				$c_title 			= $this->input->post('challenge_title');
				$challenge_title 	= $encrptopenssl->encrypt($this->input->post('challenge_title'));
				$company_name 		= $encrptopenssl->encrypt($this->input->post('company_name'));
				$company_profile  	= $encrptopenssl->encrypt($this->input->post('company_profile'));
				$breif_info  		= $encrptopenssl->encrypt($this->input->post('challenge_details'));
				$abstract_details  	= $encrptopenssl->encrypt($this->input->post('company_details'));
				$launch_date  		= $l_date;
				$close_date  		= $c_date;
				$techonogy_id  		= $tech_arr;
				$tags_id  			= $tegs_arr;
				$audi_id  			= $au_arr;
				$other_techonology  = $encrptopenssl->encrypt($this->input->post('other_technology'));
				$other_audience  	= $encrptopenssl->encrypt($this->input->post('other_audience'));
				$if_funding  		= $fund_if;
				$if_reward  		= $reward;
				$is_amount  		= $encrptopenssl->encrypt($this->input->post('is_amount'));
				$fund_reward  		= $encrptopenssl->encrypt($this->input->post('fund_reward'));
				$trl_solution  		= $encrptopenssl->encrypt($this->input->post('trl_solution'));
				$min_team  			= $encrptopenssl->encrypt($this->input->post('min_team'));
				$max_team  			= $encrptopenssl->encrypt($this->input->post('max_team'));
				$terms_txt  		= $encrptopenssl->encrypt($this->input->post('challenge_terms'));
				$educational  		= $encrptopenssl->encrypt($this->input->post('educational'));
				$from_age			= $encrptopenssl->encrypt($this->input->post('from_age'));
				$to_age				= $encrptopenssl->encrypt($this->input->post('to_age'));
				$domain  			= $domain_arr;
				$geographical  		= $geogra_arr;			
				//$is_agree  			= $this->input->post('is_agree');
				$contact_name  		= $encrptopenssl->encrypt($this->input->post('contact_name'));
				$contact_email  	= $encrptopenssl->encrypt($this->input->post('contact_email'));
				$mobile  			= $encrptopenssl->encrypt($this->input->post('mobile'));
				$office_no  		= $encrptopenssl->encrypt($this->input->post('office_no'));
				//$visibility  		= $this->input->post('visibility');
				$future_opp  		= $encrptopenssl->encrypt($this->input->post('future_opportunities'));			
				$is_external_fund	= $ex_fund_if;
				$funding_amt		= $encrptopenssl->encrypt($this->input->post('funding_amt'));
				$is_exclusive		= $exclusiveDetails;
				$challenge_ex		= $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
				//$ip_cls				= $this->input->post('ip_cls');
				//$share_details		= $this->input->post('details_share');	
				$banner_img  		= $_FILES['banner_img']['name'];
				$comp_logo   		= $_FILES['comp_logo']['name'];				
				$json_store			= $this->input->post();
				
				if($banner_img!=""){			
					
					$config['upload_path']      = 'assets/challenge';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '1000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$b_image = $upload_files[0];
						
					}
					
				} // Banner Image End
				
				// Get Banner Image Name
				if($banner_img == ""){
					$banner_names = '';
					} else {
					$banner_names = $encrptopenssl->encrypt($b_image[0]);
				}	
				
				if($comp_logo!=""){			
					
					$config['upload_path']      = 'assets/challenge';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('comp_logo', $_FILES, $config, FALSE);
			    $l_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$l_image = $upload_files[0];
						
					}
					
				} // Logo Image End
				
				// Get Logo Image Name
				if($comp_logo == ""){
					$logo_names = '';
					} else {
					$logo_names = $encrptopenssl->encrypt($l_image[0]);
				}
				
				$imgArrStore = array('banner_name' => $banner_names, 'logo_names' => $logo_names);	
				$challenge_code	=	'';
				// Insert Array Built
				

				$insertArr = array( 
				'u_id' 						=> $user_id,
				'challenge_id'				=> $challenge_code,	
				'challenge_title' 			=> $challenge_title, 
				'challenge_title_decrypt'   => $this->input->post('challenge_title'),
				'banner_img' 				=> $banner_names,
				'company_name' 				=> $company_name,
				'company_profile' 			=> $company_profile,
				'company_logo' 				=> $logo_names,
				'challenge_details' 		=> $breif_info,
				'challenge_abstract' 		=> $abstract_details,
				'challenge_launch_date' 	=> $launch_date,
				'challenge_close_date' 		=> $close_date,
				'technology_id' 			=> $techonogy_id,
				'other_techonology'			=> $other_techonology,
				'tags_id' 					=> $tags_id,
				'added_tag_name'			=> $otherTagNames,
				'last_updated_date' 		=> null,
				'audience_pref_id' 			=> $audi_id,
				'other_audience'			=> $other_audience,
				'if_funding' 				=> $fund_if,
				'if_reward' 				=> $reward,
				'is_amount' 				=> $is_amount,
				'fund_reward' 				=> $fund_reward,
				'trl_solution' 				=> $trl_solution,
				'is_agree' 					=> $is_agree,
				'terms_txt' 				=> $terms_txt,
				'contact_person_name' 		=> $contact_name,
				'share_details' 			=> $share_details,
				'challenge_visibility' 		=> $visibility,
				'future_opportunities' 		=> $future_opp,
				'ip_clause' 				=> $ip_cls,
				'is_external_funding' 		=> $is_external_fund,
				'external_fund_details' 	=> $funding_amt,
				'is_exclusive_challenge' 	=> $is_exclusive,
				'exclusive_challenge_details' => $challenge_ex,
				'json_str'					=> json_encode($json_store)
				);
				
				
				// Insert SQL For Challenge
				if($update_cid){
					$this->master_model->updateRecord('challenge',$insertArr,array('c_id' => $update_cid));
					$insertQuery = $update_cid;
					} else {
					$insertQuery = $this->master_model->insertRecord('challenge',$insertArr);
				}	
				
				
				
				if($insertQuery > 0){
					
					//Update Challenge Code/ID
					$generateId = '';
					$upArray = array('challenge_id' => $generateId, 'challenge_status' => 'Draft');
					$updateChallenge = $this->master_model->updateRecord('challenge',$upArray,array('c_id' => $insertQuery));
					
					
					
					// Insert Tag Name into Tag Table
					if($otherTagname){
						
						$tags_delete = $this->master_model->deleteRecord('tags','c_id',$insertQuery);
						
						$newTagExp = explode(",",$otherTagname);
						foreach($newTagExp as $createTag){
							$tagOtherContent = $encrptopenssl->encrypt($createTag);
							$tagDataInsert = array( 'tag_name' => $tagOtherContent, 'c_id' => $insertQuery, 'status' => 'Block');						
							$insertTagsQuery = $this->master_model->insertRecord('tags',$tagDataInsert);
							
						}
						
					}
					
					if($userTechnolgy){
						
						$tech_delete = $this->master_model->deleteRecord('technology_master','c_id',$insertQuery);
						
						// Insert Technology Name into Tag Table
						$newTechnologyExp = explode(",",$userTechnolgy);
						foreach($newTechnologyExp as $createTechnology){
							$techOtherContent = $encrptopenssl->encrypt($createTechnology);
							$techDataInsert = array( 'technology_name' => $techOtherContent, 'c_id' => $insertQuery, 'status' => 'Block');						
							$insertTechQuery = $this->master_model->insertRecord('technology_master',$techDataInsert);
							
						}
						
					}
					
					// Insert SQL For Contact Details
					$con_delete = $this->master_model->deleteRecord('challenge_contact','c_id',$insertQuery);	
					$contactArr = array(	'c_id' 		=> $insertQuery,
					'email_id' 	=> $contact_email,
					'mobile_no' => $mobile,
					'office_no' => $office_no
					);
					$this->master_model->insertRecord('challenge_contact',$contactArr);	
					
					
					// Insert SQL For Elegibility Expectation data
					$elegibility_delete = $this->master_model->deleteRecord('eligibility_expectation','c_id',$insertQuery);	
					$expecArr = array(	'c_id' 				=> $insertQuery,
					'education' 		=> $educational,
					'from_age' 			=> $from_age,
					'to_age' 			=> $to_age,
					'domain' 			=> $domain,
					'geographical_states' => $geographical,
					'min_team' 			=> $min_team,
					'max_team' 			=> $max_team
					);
					$this->master_model->insertRecord('eligibility_expectation',$expecArr);		
					
					$fileArr			= $_FILES;
					$postArr			= $this->input->post();
					// Log Data Added
					$arrLog 			= array_merge($fileArr,$postArr);
					$arrLogImg 			= array_merge($arrLog,$imgArrStore);	
					$json_encode_data 	= json_encode($arrLogImg);
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails = array(
					'user_id' 		=> $user_id,
					'action_name' 	=> "ADD Draft",
					'module_name'	=> 'Challenge',
					'store_data'	=> $json_encode_data,
					'ip_address'	=> $ipAddr,
					'createdAt'		=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					$this->session->set_flashdata('success','Challenge successfully save to draft.');
					
					// Redirect
					redirect(base_url('challenge/myChallenges'));	
					
				} // Insert ID End			
				
			} // Draft Button End
			
			if($this->form_validation->run())
			{	
				// Challenge ID 
				$update_cid = $this->input->post('update_id');
				
				$pref = $this->input->post('audi_id');
				$au_arr = implode(",",$pref);
				
				$techID = $this->input->post('techonogy_id');
				$tech_arr = implode(",",$techID);
				
				$tagID = $this->input->post('tags_id');
				$tegs_arr = implode(",",$tagID);
				
				$domanID = $this->input->post('domain');
				$domain_arr = implode(",",$domanID);
				
				$geographicalID = $this->input->post('geographical');
				if($geographicalID != "") { $geogra_arr = implode(",",$geographicalID); } else { $geogra_arr = ""; }
				$reward = 'No';
				if($this->input->post('if_reward')){
					$reward = $this->input->post('if_reward');
				} 
				
				$fund_if = 'No';
				if($this->input->post('if_funding')){
					$fund_if = $this->input->post('if_funding');
				} 
				
				$ex_fund_if = '0';
				if($this->input->post('is_external_funding')){
					$ex_fund_if = $this->input->post('is_external_funding');
				} 
				
				$share_details = '0';
				if($this->input->post('details_share')){
					$share_details  = $this->input->post('details_share');
				}
				
				$otherTagname       = $this->input->post('other_tag');			
				if($otherTagname){
					$otherTagNames = $encrptopenssl->encrypt($otherTagname);
					} else {
					$otherTagNames = '';
				}
				
				$userTechnolgy		= $this->input->post('other_technology');
				
				$fileArr			= $_FILES;
				$postArr			= $this->input->post();
				$c_title 			= $this->input->post('challenge_title');
				$challenge_title 	= $encrptopenssl->encrypt($this->input->post('challenge_title'));
				$company_name 		= $encrptopenssl->encrypt($this->input->post('company_name'));
				$company_profile  	= $encrptopenssl->encrypt($this->input->post('company_profile'));
				$breif_info  		= $encrptopenssl->encrypt($this->input->post('challenge_details'));
				$abstract_details  	= $encrptopenssl->encrypt($this->input->post('company_details'));
				$launch_date  		= date('Y-m-d',strtotime($this->input->post('launch_date')));
				$close_date  		= date('Y-m-d',strtotime($this->input->post('close_date')));
				$techonogy_id  		= $tech_arr;
				$tags_id  			= $tegs_arr;
				$audi_id  			= $au_arr;
				$other_techonology  = $encrptopenssl->encrypt($this->input->post('other_technology'));
				$other_audience  	= $encrptopenssl->encrypt($this->input->post('other_audience'));
				$if_funding  		= $fund_if;
				$if_reward  		= $reward;
				$is_amount  		= $encrptopenssl->encrypt($this->input->post('is_amount'));
				$fund_reward  		= $encrptopenssl->encrypt($this->input->post('fund_reward'));
				$trl_solution  		= $encrptopenssl->encrypt($this->input->post('trl_solution'));
				$min_team  			= $encrptopenssl->encrypt($this->input->post('min_team'));
				$max_team  			= $encrptopenssl->encrypt($this->input->post('max_team'));
				$terms_txt  		= $encrptopenssl->encrypt($this->input->post('challenge_terms'));
				$educational  		= $encrptopenssl->encrypt($this->input->post('educational'));
				$from_age			= $encrptopenssl->encrypt($this->input->post('from_age'));
				$to_age				= $encrptopenssl->encrypt($this->input->post('to_age'));
				$domain  			= $domain_arr;
				$geographical  		= $geogra_arr;			
				$is_agree  			= $this->input->post('is_agree');
				$contact_name  		= $encrptopenssl->encrypt($this->input->post('contact_name'));
				$contact_email  	= $encrptopenssl->encrypt($this->input->post('contact_email'));
				$mobile  			= $encrptopenssl->encrypt($this->input->post('mobile'));
				$office_no  		= $encrptopenssl->encrypt($this->input->post('office_no'));
				$visibility  		= $this->input->post('visibility');
				$future_opp  		= $encrptopenssl->encrypt($this->input->post('future_opportunities'));			
				$is_external_fund	= $ex_fund_if;
				$funding_amt		= $encrptopenssl->encrypt($this->input->post('funding_amt'));
				$is_exclusive		= $this->input->post('is_exclusive_challenge');
				$challenge_ex		= $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
				$ip_cls				= $this->input->post('ip_cls');
				//$share_details		= $this->input->post('details_share');	
				$banner_img  		= $_FILES['banner_img']['name'];
				$comp_logo   		= $_FILES['comp_logo']['name'];	
				$json_store			= $this->input->post();
				
				if($banner_img!=""){			
					
					$config['upload_path']      = 'assets/challenge';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '1000000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$b_image = $upload_files[0];
						
					}
					
				} // Banner Image End
				
				// Get Banner Image Name
				if($banner_img == ""){
					$banner_names = '';
					} else {
					$banner_names = $encrptopenssl->encrypt($b_image[0]);
				}	
				
				
				//echo $_FILES['banner_img']['name']."==".$banner_names."==".$b_image[0];
				if($comp_logo!=""){			
					
					$config['upload_path']      = 'assets/challenge';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000';         
					$config['encrypt_name']     = TRUE;
					
					$upload_files  = @$this->master_model->upload_file('comp_logo', $_FILES, $config, FALSE);
			    $l_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$l_image = $upload_files[0];
						
					}				
					
				} // Logo Image End
				
				// Get Logo Image Name
				if($comp_logo == ""){
					$logo_names = '';
					} else {
					$logo_names = $encrptopenssl->encrypt($l_image[0]);
				}
				
				$imgArrStore = array('banner_name' => $banner_names, 'logo_names' => $logo_names);
				
				$ch_id = '';	
				
				// Insert Array Built
				$insertArr = array( 
				'u_id' 						=> $user_id,
				'challenge_id' 				=> $ch_id,	
				'challenge_title' 			=> $challenge_title, 
				'banner_img' 				=> $banner_names,
				'company_name' 				=> $company_name,
				'company_profile' 			=> $company_profile,
				'company_logo' 				=> $logo_names,
				'challenge_details' 		=> $breif_info,
				'challenge_abstract' 		=> $abstract_details,
				'challenge_launch_date' 	=> $launch_date,
				'challenge_close_date' 		=> $close_date,
				'technology_id' 			=> $techonogy_id,
				'other_techonology'			=> $other_techonology,
				'tags_id' 					=> $tags_id,
				'added_tag_name'			=> $otherTagNames,
				'last_updated_date' 		=> null,
				'audience_pref_id' 			=> $audi_id,
				'other_audience'			=> $other_audience,
				'if_funding' 				=> $fund_if,
				'if_reward' 				=> $reward,
				'is_amount' 				=> $is_amount,
				'fund_reward' 				=> $fund_reward,
				'trl_solution' 				=> $trl_solution,
				'is_agree' 					=> $is_agree,
				'terms_txt' 				=> $terms_txt,
				'contact_person_name' 		=> $contact_name,
				'share_details'				=> $share_details,
				'challenge_visibility' 		=> $visibility,
				'future_opportunities' 		=> $future_opp,
				'ip_clause' 				=> $ip_cls,
				'is_external_funding' 		=> $is_external_fund,
				'external_fund_details' 	=> $funding_amt,
				'is_exclusive_challenge' 	=> $is_exclusive,
				'exclusive_challenge_details' => $challenge_ex,
				'json_str'					=> json_encode($json_store)
				);		
				
				// Insert SQL For Challenge	
				//$insertQuery = $this->master_model->insertRecord('challenge',$insertArr);
				//echo $this->db->last_query();die();
				
				// Insert SQL For Challenge
				if($update_cid){
					$this->master_model->updateRecord('challenge',$insertArr,array('c_id' => $update_cid));
					$insertQuery = $update_cid;
					} else {
					$insertQuery = $this->master_model->insertRecord('challenge',$insertArr);
				}	
				
				
				if($insertQuery > 0){			
					
					
					// Update Challene Code
					$getCode = getLastInsertID($insertQuery);
					$challenge_code = 'TNCID-'.$getCode;
					$updateArrID = array('challenge_id' => $challenge_code, 'challenge_status' => 'Pending');
					$updateQuery = $this->master_model->updateRecord('challenge',$updateArrID,array('c_id' => $insertQuery));
					
					// Insert Tag Name into Tag Table
					if($otherTagname){
						$tags_delete = $this->master_model->deleteRecord('tags','c_id',$insertQuery);
						$newTagExp = explode(",",$otherTagname);
						foreach($newTagExp as $createTag){
							$tagOtherContent = $encrptopenssl->encrypt($createTag);
							$tagDataInsert = array( 'tag_name' => $tagOtherContent, 'c_id' => $insertQuery, 'status' => 'Block');						
							$insertTagsQuery = $this->master_model->insertRecord('tags',$tagDataInsert);
							
						}
						
					}
					
					if($userTechnolgy){
						
						$tech_delete = $this->master_model->deleteRecord('technology_master','c_id',$insertQuery);					
						// Insert Technology Name into Tag Table
						$newTechnologyExp = explode(",",$userTechnolgy);
						foreach($newTechnologyExp as $createTechnology){
							$techOtherContent = $encrptopenssl->encrypt($createTechnology);
							$techDataInsert = array( 'technology_name' => $techOtherContent, 'c_id' => $insertQuery, 'status' => 'Block');						
							$insertTechQuery = $this->master_model->insertRecord('technology_master',$techDataInsert);
							
						}
						
					}
					
					
					
					// Insert SQL For Contact Details
					$con_delete = $this->master_model->deleteRecord('challenge_contact','c_id',$insertQuery);
					$contactArr = array(	'c_id' 		=> $insertQuery,
					'email_id' 	=> $contact_email,
					'mobile_no' => $mobile,
					'office_no' => $office_no
					);
					$this->master_model->insertRecord('challenge_contact',$contactArr);	
					
					// Insert SQL For Elegibility Expectation data
					$elegibility_delete = $this->master_model->deleteRecord('eligibility_expectation','c_id',$insertQuery);	
					if($this->input->post('min_team') != ""){
						
						$expecArr = array(	'c_id' 				=> $insertQuery,
						'education' 		=> $educational,
						'from_age' 			=> $from_age,
						'to_age' 			=> $to_age,
						'domain' 			=> $domain,
						'geographical_states' => $geographical,
						'min_team' 			=> $min_team,
						'max_team' 			=> $max_team
						);
						$this->master_model->insertRecord('eligibility_expectation',$expecArr);	
						
					}	
					
					$fileArr			= $_FILES;
					$postArr			= $this->input->post();
					// Log Data Added
					$arrLog 			= array_merge($fileArr,$postArr);
					$arrLogImg 			= array_merge($arrLog,$imgArrStore);	
					$json_encode_data 	= json_encode($arrLogImg);
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails = array(
					'user_id' 		=> $user_id,
					'action_name' 	=> "ADD",
					'module_name'	=> 'Challenge',
					'store_data'	=> $json_encode_data,
					'ip_address'	=> $ipAddr,
					'createdAt'		=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					$this->session->set_flashdata('success','Challenge successfully Created.');	
					
					
					
					
					// Admin Email Functionality
					$slug = $encrptopenssl->encrypt('new_challenge_alert');
					$challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
					$content = '';
					
					$pendingChallenge 	= $this->master_model->getRecords("challenge", array("challenge_status" => 'Pending'));
					$totalChallenge		= count($pendingChallenge);
					if(count($challenge_mail) > 0){
						
						$userId 		= $this->session->userdata('user_id');
						$sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
						$fullname 		= $encrptopenssl->decrypt($sender_details[0]['first_name'])." ".$encrptopenssl->decrypt($sender_details[0]['last_name']);
						$login_email 	= $encrptopenssl->decrypt($sender_details[0]['email']);
						$registerType 	= $sender_details[0]['user_category_id'];
						$registeSubType = $sender_details[0]['user_sub_category_id'];
						$membershipType = $encrptopenssl->decrypt($sender_details[0]['membership_type']);
						$registerCat 	= $this->master_model->getRecords("registration_usercategory", array("id" => $registerType));
						$categoryName 	= $encrptopenssl->decrypt($registerCat[0]['user_category']);
						
						$registerSubType = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $registeSubType));
						$subcategoryName = $encrptopenssl->decrypt($registerSubType[0]['sub_catname']);
						
						$subject 	 	= $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
						$description 	= $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
						$from_admin 	= $encrptopenssl->decrypt($challenge_mail[0]['from_email']);
						//$from_admin 	= 'kamlesh.chaube@esds.co.in';	
						$sendername  	= $setting_table[0]['field_1'];					
						
						$arr_words = ['[ACCOUNT]', '[SUBCATEGORY]', '[SUBSCRIPTIONTYPE]', '[TOTAL]'];
						$rep_array = [$categoryName,  $subcategoryName, $membershipType, $totalChallenge];
						
						$content = str_replace($arr_words, $rep_array, $description);
						
						//email admin sending code 
						$info_arr=array(
						'to'		=>	$from_admin,					
						'cc'		=>	'',
						'from'		=>	$login_email,
						'subject'	=> 	$subject,
						'view'		=>  'common-file'
						);
						
						$other_info=array('content'=>$content); 
						
						$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					}	
					
					
					// Subscriber Email
					/*$slug_1 = $encrptopenssl->encrypt('challenge_register');	
					$subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug_1));
					$setting_table_1 = $this->master_model->getRecords("setting", array("id" => '1'));
					$content_Subscriber = '';			
					
					if(count($subscriber_mail) > 0){
						
						$usersId 			= $this->session->userdata('user_id');
						$senders_details 	= $this->master_model->getRecords("registration", array("user_id" => $usersId));					
						$receiver_email 	= $encrptopenssl->decrypt($senders_details[0]['email']);
						//$receiver_email		= "vicky.kuwar@esds.co.in";
						$register_Type		= $senders_details[0]['user_category_id'];
						$register_SubType 	= $senders_details[0]['user_sub_category_id'];
						$membership_Type 	= $encrptopenssl->decrypt($senders_details[0]['membership_type']);
						
						$registerCat = $this->master_model->getRecords("registration_usercategory", array("id" => $register_Type));
						$categoryName = $encrptopenssl->decrypt($registerCat[0]['user_category']);
						
						$registerSub_Type = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $register_SubType));
						$sub_categoryName = $encrptopenssl->decrypt($registerSub_Type[0]['sub_catname']);
						
						$subject_1 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
						$description_1 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
						$from_admin_1 	= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);					
						$sendername1  	= $setting_table_1[0]['field_1'];
						$contact_no1	= $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);
						$modifi_date    = date('d-m-Y', strtotime($close_date));
						$link  = base_url('challenge/challengeDetails/'.base64_encode($insertQuery));
						$linkName = '<a href="'.$link.'" target="_blank">View</a>';	
						
						$arr_words = ['[CHALLENGENAME]', '[ACCOUNT]', '[CHALLENGELINK]', '[CHALLENGECLOSEDATE]', '[PHONENO]'];
						$rep_array = [$c_title,  $categoryName, $linkName, $modifi_date, $contact_no1];
						
						$content_Subscriber = str_replace($arr_words, $rep_array, $description_1);
						
						$info_arr_c=array(
						'to'		=>	$receiver_email,					
						'cc'		=>	'',
						'from'		=>	$from_admin_1,
						'subject'	=> 	$subject_1,
						'view'		=>  'common-file'
						);
						
						$other_info_c=array('content'=>$content_Subscriber); 					
						
						$emailsend_1 = $this->emailsending->sendmail($info_arr_c,$other_info_c);
						
					}*/
					
					// Redirect
					redirect(base_url('challenge/myChallenges'));
					
					} else {
					
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('challenge'));
					
				}
				
				
			} // Validation If End
			
			$data['trl_data'] 	 	 = $trl_data;
			$data['domain_data'] 	 = $domain_data;
			$data['technology_data'] = $technology_data; 
			$data['tag_data'] 		 = $tag_data;
			$data['audi_data'] 		 = $audi_data;	
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/add';
			$this->load->view('front/front_combo',$data);
			
		} // Add End
		
		
		public function edit($id){
			error_reporting(0);
			$this->check_permissions->is_admin_approved();		
			$module_id = 3;
			$this->check_permissions->is_authorise($module_id);
			$this->check_permissions->is_profile_complete();
			
			$challenge_id = base64_decode($id);	
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Load Library
			$this->load->library('upload');
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');	
			
			$this->db->join('eligibility_expectation','challenge.c_id=eligibility_expectation.c_id','left');
			$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');
			$this->db->join('ip_clause','challenge.ip_clause=ip_clause.id','left');
			$this->db->join('arai_registration r','r.user_id = arai_challenge.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$response_data = $this->master_model->getRecords("challenge", array("challenge.c_id" => $challenge_id));
			
			$data['eligibility'] 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $challenge_id));
			$data['challenge_contact'] = $this->master_model->getRecords("challenge_contact", array("c_id" => $challenge_id));
			
			if($response_data[0]['u_id']!= $user_id){
				
				redirect(base_url('challenge/myChallenges'));die();
				
			}
			
			$res_challenges = array();
			if(count($response_data)){	
				
				foreach($response_data as $row_val){
					
					
					$trid = $encrptopenssl->decrypt($row_val['trl_solution']);
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $challenge_id));
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $challenge_id));
					$tr_detail 			= $this->master_model->getRecords("trl", array("id" => $trid));
					$trl_fullname		= $tr_detail[0]['trl_name'];
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					
					$domainDetails = explode(",",$domainIds);				
					$res_domains = array();
					foreach($domainDetails as $dNames){					
						$d_view_data = $this->master_model->getRecords("domain_master", array("id" => $dNames));									
						$domain_name = $encrptopenssl->decrypt($d_view_data[0]['domain_name']);							
						array_push($res_domains, $domain_name);	
					}				
					$implodeD = implode(",", $res_domains);				
					
					
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];
					
					$geoDetails = explode(",",$geographyMaster);				
					$res_geo = array();
					foreach($geoDetails as $games){					
						$state_data = $this->master_model->getRecords("states", array("id" => $games));									
						$state_name = $state_data[0]['name'];							
						array_push($res_geo, $state_name);	
					}				
					$implodeState = implode(",", $res_geo);				
					
					
					
					$row_val['education'] 			= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 			= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 				= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['domain'] 				= $eligibility[0]['domain'];
					$row_val['geographical_states'] = $eligibility[0]['geographical_states'];
					$row_val['min_team'] 			= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 			= $encrptopenssl->decrypt($eligibility[0]['max_team']);				
					$row_val['email_id'] 			= @$encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 			= @$encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 			= @$encrptopenssl->decrypt($challenge_contact[0]['office_no']);	
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['company_name']     		= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 	    = $encrptopenssl->decrypt($row_val['company_profile']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);				
					$row_val['challenge_launch_date'] 	= $row_val['challenge_launch_date'];
					$row_val['challenge_close_date'] 	= $row_val['challenge_close_date'];
					$row_val['technology_id'] 	    	= $row_val['technology_id'];
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['other_techonology'] 	    = $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tag_name'] 	    		= $row_val['tags_id'];
					$row_val['added_tag_name'] 	    	= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id'] 	    = $row_val['audience_pref_id'];
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['domainlist'] 	    		= $implodeD;
					$row_val['stateData'] 	    		= $implodeState;
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['tr_name']					= $encrptopenssl->decrypt($trl_fullname);
					$row_val['other_audience'] 	    	= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['status'] 					= $row_val['status']; 
					$row_val['ip_name'] 				= $row_val['ip_name']; 
					$row_val['is_publish'] 				= $row_val['is_publish'];
					$row_val['is_featured'] 			= $row_val['is_featured'];
					$row_val['is_external_funding'] 	= $row_val['is_external_funding'];				
					$row_val['external_fund_details'] 	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['is_exclusive_challenge'] 	= $row_val['is_exclusive_challenge'];
					$row_val['exclusive_challenge_details'] 	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$res_challenges[] = $row_val;
				}
				
			}
			
			
			$getDetails = $this->master_model->getRecords("registration", array("user_id" => $user_id));
			if($getDetails[0]['user_category_id'] == '2'){
				
				$data['orgName'] = $encrptopenssl->decrypt($getDetails[0]['institution_full_name']);
				
				} else {
				
				$data['orgName'] = "";
				
			}	
			
			// Technology Master
			$technology_data= $this->master_model->array_sorting($this->master_model->getRecords('technology_master',array('status'=>"Active")), array('id'),'technology_name');			
			
			// Tags Master
			$tag_data= $this->master_model->array_sorting($this->master_model->getRecords('tags',array('status'=>"Active")), array('id'),'tag_name');			
			
			// Audience Preference Master
			$audi_data= $this->master_model->array_sorting($this->master_model->getRecords('audience_pref',array('status'=>"Active")), array('id'),'preference_name');			
			
			// Domain Master
			$domain_data= $this->master_model->array_sorting($this->master_model->getRecords('domain_master',array('status'=>"Active")), array('id'),'domain_name');			
			
			// TRL Master
			$trl_data= $this->master_model->array_sorting($this->master_model->getRecords('trl',array('status'=>"Active")), array('id'),'trl_name');			
			
			// IP Clause Master
			$data['ip_clause_data'] = $this->master_model->getRecords("ip_clause", array("status" => 'Active'));
			
			// State Master
			$data['state'] = $this->master_model->getRecords("states", array("status" => 'Active'),'',array('name' => 'ASC'));
			
			
			if(isset($_POST) && count($_POST) > 0)
			{	
				// Check Validation
				$this->form_validation->set_rules('company_profile', 'Company Profile', 'xss_clean');		
				$this->form_validation->set_rules('launch_date', 'Launch Date', 'required|xss_clean');
				$this->form_validation->set_rules('close_date', 'Close Date', 'required|xss_clean');
				$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|xss_clean');
				$this->form_validation->set_rules('contact_email', 'Contact Email', 'required|xss_clean');
				$this->form_validation->set_rules('mobile', 'Contact Mobile', 'required|min_length[10]|max_length[10]|xss_clean');
				$this->form_validation->set_rules('office_no', 'Office Number', 'required|xss_clean');
				$this->form_validation->set_rules('visibility', 'Visibility', 'required|xss_clean');		
				$this->form_validation->set_rules('is_exclusive_challenge', 'Challenge exclusively listed on TechNovuus', 'required|xss_clean');
				
				
				if($this->form_validation->run())
				{
					
					$pref = $this->input->post('audi_id');
					$au_arr = implode(",",$pref);
					
					$techID = $this->input->post('techonogy_id');
					$tech_arr = implode(",",$techID);
					
					$tagsID = $this->input->post('tags_id');
					$tgse_arr = implode(",",$tagsID);		
					
					
					if($this->input->post('if_reward')==""){
						$reward = '';
						} else {
						$reward = $this->input->post('if_reward');
					}
					
					$ex_fund_if = '0';
					if($this->input->post('is_external_funding')){
						$ex_fund_if = $this->input->post('is_external_funding');
					} 
					
					$otherTagname       = $this->input->post('other_tag');
					
					if($otherTagname){
						$otherTagNames = $encrptopenssl->encrypt($otherTagname);
						} else {
						$otherTagNames = '';
					}
					
					$userTechnolgy 		= $this->input->post('other_technology');
					if($userTechnolgy){
						$otherTechstore = $encrptopenssl->encrypt($userTechnolgy);
						} else {
						$otherTechstore = '';
					}			
					
					$challenge_title  	= $encrptopenssl->encrypt($this->input->post('challenge_title'));
					$company_name  		= $encrptopenssl->encrypt($this->input->post('company_name'));
					$challenge_details	= $encrptopenssl->encrypt($this->input->post('challenge_details'));
					$company_details	= $encrptopenssl->encrypt($this->input->post('company_details'));
					$company_profile  	= $encrptopenssl->encrypt($this->input->post('company_profile'));
					$launch_date  		= date('Y-m-d',strtotime($this->input->post('launch_date')));
					$close_date  		= date('Y-m-d',strtotime($this->input->post('close_date')));
					$techonogy_id  		= $tech_arr;
					$tags_id  			= $tgse_arr;
					$otherExtraTag		= $otherTagNames;
					$audi_id  			= $au_arr;
					$other_techonology  = $otherTechstore;
					$if_funding  		= $this->input->post('if_funding');
					$if_reward  		= $reward;
					$is_amount  		= $encrptopenssl->encrypt($this->input->post('is_amount'));
					$fund_reward  		= $encrptopenssl->encrypt($this->input->post('fund_reward'));
					$trl_solution  		= $encrptopenssl->encrypt($this->input->post('trl_solution'));
					
					// Eductional Eligibility Fields
					$educational  		= $encrptopenssl->encrypt($this->input->post('educational'));
					$from_age  			= $encrptopenssl->encrypt($this->input->post('from_age'));
					$to_age  			= $encrptopenssl->encrypt($this->input->post('to_age'));
					$domainPost			= $this->input->post('domain');
					$domain_arr 		= implode(",",$domainPost);
					$geographicalPost	= $this->input->post('geographical');			
					if($geographicalPost != "") { $geogra_arr = implode(",",$geographicalPost); } else { $geogra_arr = ""; }
					$min_team  			= $encrptopenssl->encrypt($this->input->post('min_team'));
					$max_team  			= $encrptopenssl->encrypt($this->input->post('max_team'));
					
					
					// Contact Fields
					$contact_name  		= $encrptopenssl->encrypt($this->input->post('contact_name'));
					$contact_email  	= $encrptopenssl->encrypt($this->input->post('contact_email'));
					$mobile  			= $encrptopenssl->encrypt($this->input->post('mobile'));
					$office_no  		= $encrptopenssl->encrypt($this->input->post('office_no'));
					
					$challenge_terms	= $encrptopenssl->encrypt($this->input->post('challenge_terms'));
					$details_share  	= $this->input->post('details_share');
					$visibility  		= $this->input->post('visibility');	
					$future_oppor 		= $encrptopenssl->encrypt($this->input->post('future_opportunities'));	
					$ip_cls  			= $this->input->post('ip_cls');
					$is_external_fund	= $this->input->post('is_external_funding');
					$funding_amt		= $encrptopenssl->encrypt($this->input->post('funding_amt'));
					$challenge_ex_det	= $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
					$is_exclusive		= $this->input->post('is_exclusive_challenge');					
					$banner_img  		= $_FILES['banner_img']['name'];		
					$json_store			= $this->input->post();
					//echo "<pre>";print_r($this->input->post());
					
					if($banner_img!=""){			
						
						$config['upload_path']      = 'assets/challenge';
						$config['allowed_types']    = '*';  
						$config['max_size']         = '1000000';         
						$config['encrypt_name']     = TRUE;
						
						$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
						$b_image = "";
						if(isset($upload_files[0]) && !empty($upload_files[0])){
							
							$b_image = $upload_files[0];
							
						}				
						
					} // Banner Image End
					
					if($is_exclusive == 1){
						$challenge_ex		= "";
						} else {
						$challenge_ex		= $encrptopenssl->encrypt($this->input->post('challenge_ex_details'));
					}
					$shareDetails = '0';
					if($details_share){
						$shareDetails = '1';
					} 
					$challengeStatusEdit =  $response_data[0]['challenge_status'];
					// Get Banner Image Name
					if($banner_img == ""){
						$bnImg  = '';
						if($challengeStatusEdit == 'Pending'){
							
							$updateArr = array(	
							'challenge_title' 			=> $challenge_title,
							'challenge_title_decrypt'   => $this->input->post('challenge_title'),
							'company_name' 				=> $company_name,
							'challenge_details' 		=> $challenge_details,
							'company_profile' 			=> $company_profile,
							'challenge_abstract' 		=> $company_details,	
							'challenge_launch_date' 	=> $launch_date,
							'challenge_close_date' 		=> $close_date,
							'technology_id' 			=> $techonogy_id,
							'other_techonology'			=> $otherTechstore,
							'tags_id' 					=> $tags_id,
							'added_tag_name'			=> $otherExtraTag,
							'last_updated_date' 		=> date('Y-m-d'),
							'audience_pref_id' 			=> $audi_id,
							'if_funding' 				=> $if_funding,
							'if_reward' 				=> $if_reward,
							'is_amount' 				=> $is_amount,
							'fund_reward' 				=> $fund_reward,
							'trl_solution'				=> $trl_solution,
							'terms_txt'					=> $challenge_terms,
							'contact_person_name' 		=> $contact_name,
							'share_details' 			=> $shareDetails,
							'challenge_visibility' 		=> $visibility,
							'future_opportunities' 		=> $future_oppor,
							'ip_clause'					=> $ip_cls,	
							'is_external_funding' 		=> $ex_fund_if,								
							'external_fund_details' 	=> $funding_amt,
							'is_exclusive_challenge' 	=> $is_exclusive,
							'exclusive_challenge_details' => $challenge_ex,
							'json_str'					=> json_encode($json_store)
							);
							
							} else {
							
							$updateArr = array(						
							'company_profile' 			=> $company_profile,								
							'challenge_launch_date' 	=> $launch_date,
							'challenge_close_date' 		=> $close_date,
							'technology_id' 			=> $techonogy_id,
							'other_techonology'			=> $other_techonology,
							'tags_id' 					=> $tags_id,
							'added_tag_name'			=> $otherExtraTag,
							'last_updated_date' 		=> date('Y-m-d'),
							'audience_pref_id' 			=> $audi_id,								
							'if_funding' 				=> $if_funding,
							'if_reward' 				=> $if_reward,
							'is_amount' 				=> $is_amount,
							'fund_reward' 				=> $fund_reward,								
							'contact_person_name' 		=> $contact_name,
							'challenge_visibility' 		=> $visibility,															
							'is_external_funding' 		=> $ex_fund_if,								
							'external_fund_details' 	=> $funding_amt,
							'share_details' 			=> $shareDetails,
							'ip_clause'					=> $ip_cls,
							'is_exclusive_challenge' 	=> $is_exclusive,
							'exclusive_challenge_details' => $challenge_ex,
							'json_str'					=> json_encode($json_store)
							);
							
						}			
						
						
						} else {
						
						$bnImg  = $b_image[0];
						$banner_names = $encrptopenssl->encrypt($b_image[0]);
						// Update Array Built
						
						if($challengeStatusEdit == 'Pending'){
							
							$updateArr = array(								
							'banner_img' 				=> $banner_names,								
							'challenge_title' 			=> $challenge_title,
							'company_name' 				=> $company_name,
							'challenge_details' 		=> $challenge_details,
							'company_profile' 			=> $company_profile,
							'challenge_abstract' 		=> $company_details,	
							'challenge_launch_date' 	=> $launch_date,
							'challenge_close_date' 		=> $close_date,
							'technology_id' 			=> $techonogy_id,
							'other_techonology'			=> $otherTechstore,
							'tags_id' 					=> $tags_id,
							'added_tag_name'			=> $otherExtraTag,
							'last_updated_date' 		=> date('Y-m-d'),
							'audience_pref_id' 			=> $audi_id,
							'if_funding' 				=> $if_funding,
							'if_reward' 				=> $if_reward,
							'is_amount' 				=> $is_amount,
							'fund_reward' 				=> $fund_reward,
							'trl_solution'				=> $trl_solution,
							'terms_txt'					=> $challenge_terms,
							'contact_person_name' 		=> $contact_name,
							'share_details' 			=> $shareDetails,
							'challenge_visibility' 		=> $visibility,
							'future_opportunities' 		=> $future_oppor,
							'ip_clause'					=> $ip_cls,	
							'is_external_funding' 		=> $ex_fund_if,								
							'external_fund_details' 	=> $funding_amt,
							'is_exclusive_challenge' 	=> $is_exclusive,
							'exclusive_challenge_details' => $challenge_ex,
							'json_str'					=> json_encode($json_store)
							);	
							
							} else {
							
							$updateArr = array(								
							'banner_img' 				=> $banner_names,								
							'company_profile' 			=> $company_profile,								
							'challenge_launch_date' 	=> $launch_date,
							'challenge_close_date' 		=> $close_date,
							'technology_id' 			=> $techonogy_id,
							'other_techonology'			=> $other_techonology,
							'tags_id' 					=> $tags_id,
							'added_tag_name'			=> $otherExtraTag,
							'last_updated_date' 		=> date('Y-m-d'),
							'audience_pref_id' 			=> $audi_id,								
							'if_funding' 				=> $if_funding,
							'if_reward' 				=> $if_reward,
							'is_amount' 				=> $is_amount,
							'fund_reward' 				=> $fund_reward,
							'share_details' 			=> $shareDetails,	
							'contact_person_name' 		=> $contact_name,
							'challenge_visibility' 		=> $visibility,
							'ip_clause'					=> $ip_cls,	
							'is_external_funding' 		=> $ex_fund_if,
							'external_fund_details' 	=> $funding_amt,
							'is_exclusive_challenge' 	=> $is_exclusive,
							'exclusive_challenge_details' => $challenge_ex,
							'json_str'					=> json_encode($json_store)
							);
							
						}	
						
					}				
					
					// Insert SQL For Tags
					
					// Get Previous Data
					$this->db->join('eligibility_expectation','challenge.c_id=eligibility_expectation.c_id','left');
					$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');
					$this->db->join('ip_clause','challenge.ip_clause=ip_clause.id','left');			
					$previousData = $this->master_model->getRecords("challenge", array("challenge.c_id" => $challenge_id));
					//echo "<pre>";print_r($previousData);
					$prev_res = array();
					foreach($previousData as $prevData){
						$prevData['challenge_title'] 				= $encrptopenssl->decrypt($prevData['challenge_title']);
						$prevData['company_name'] 	 				= $encrptopenssl->decrypt($prevData['company_name']);
						$prevData['banner_img'] 	 				= $encrptopenssl->decrypt($prevData['banner_img']);
						$prevData['company_profile'] 	 			= $encrptopenssl->decrypt($prevData['company_profile']);
						$prevData['challenge_details'] 	 			= $encrptopenssl->decrypt($prevData['challenge_details']);
						$prevData['challenge_abstract'] 	 		= $encrptopenssl->decrypt($prevData['challenge_abstract']);
						$prevData['challenge_launch_date'] 	 		= $prevData['challenge_launch_date'];
						$prevData['challenge_close_date'] 	 		= $prevData['challenge_close_date'];
						$prevData['other_techonology'] 	 			= $encrptopenssl->decrypt($prevData['other_techonology']);
						$prevData['technology_id'] 	 				= $prevData['technology_id'];
						$prevData['tags_id'] 	 					= $prevData['tags_id'];
						$prevData['added_tag_name'] 	 			= $encrptopenssl->decrypt($prevData['added_tag_name']);
						$prevData['audience_pref_id'] 	 			= $prevData['audience_pref_id'];
						$prevData['if_funding'] 	 				= $prevData['if_funding'];
						$prevData['if_reward'] 	 					= $prevData['if_reward'];
						$prevData['is_amount'] 	 					= $encrptopenssl->decrypt($prevData['is_amount']);
						$prevData['fund_reward'] 	 				= $encrptopenssl->decrypt($prevData['fund_reward']);
						$prevData['trl_solution'] 	 				= $encrptopenssl->decrypt($prevData['trl_solution']);
						$prevData['terms_txt'] 	 					= $encrptopenssl->decrypt($prevData['terms_txt']);
						$prevData['contact_person_name'] 	 		= $encrptopenssl->decrypt($prevData['contact_person_name']);
						$prevData['share_details'] 	 				= $prevData['share_details'];
						$prevData['challenge_visibility'] 	 		= $prevData['challenge_visibility'];
						$prevData['future_opportunities'] 	 		= $encrptopenssl->decrypt($prevData['future_opportunities']);
						$prevData['ip_clause'] 	 					= $prevData['ip_clause'];
						$prevData['is_external_funding'] 	 		= $prevData['is_external_funding'];
						$prevData['external_fund_details'] 	 		= $encrptopenssl->decrypt($prevData['external_fund_details']);
						$prevData['is_exclusive_challenge'] 	 	= $prevData['is_exclusive_challenge'];
						$prevData['exclusive_challenge_details'] 	= $encrptopenssl->decrypt($prevData['exclusive_challenge_details']);
						$prevData['education'] 	 					= $encrptopenssl->decrypt($prevData['education']);
						$prevData['from_age'] 	 					= $encrptopenssl->decrypt($prevData['from_age']);
						$prevData['to_age'] 	 					= $encrptopenssl->decrypt($prevData['to_age']);
						$prevData['domain'] 	 					= $prevData['domain'];
						$prevData['geographical_states'] 			= $prevData['geographical_states'];
						$prevData['min_team'] 	 					= $encrptopenssl->decrypt($prevData['min_team']);
						$prevData['max_team'] 	 					= $encrptopenssl->decrypt($prevData['max_team']);
						$prevData['email_id'] 	 					= $encrptopenssl->decrypt($prevData['email_id']);
						$prevData['mobile_no'] 	 					= $encrptopenssl->decrypt($prevData['mobile_no']);
						$prevData['office_no'] 	 					= $encrptopenssl->decrypt($prevData['office_no']);
						$prevData['ip_name'] 	 					= $prevData['ip_name'];
						$prev_res[]									= $prevData; 
						
					} // End Previous data 
					
					$mailBody = '<style></style><table width="100%" border="1">';			
					
					
					
					if($prev_res[0]['challenge_title']!= $this->input->post('challenge_title')){
						$mailBody .= $this->returnBody('Challenge Title',  $prev_res[0]['challenge_title'], $this->input->post('challenge_title'));
					}
					
					if($banner_img!=""){
						$mailBody .= $this->returnBody('Challenge Banner', $prev_res[0]['banner_img'], $banner_img);
					}	
					
					if($prev_res[0]['company_name']!= $this->input->post('company_name')){
						$mailBody .= $this->returnBody('Company Name', $prev_res[0]['company_name'], $this->input->post('company_name'));
					}
					
					if($prev_res[0]['company_profile']!= $this->input->post('company_profile')){
						$mailBody .= $this->returnBody('Company Profile', $prev_res[0]['company_profile'], $this->input->post('company_profile'));
					}
					
					if($prev_res[0]['challenge_details']!= $this->input->post('challenge_details')){
						$mailBody .= $this->returnBody('Breif Info About Challenge', $prev_res[0]['challenge_details'], $this->input->post('challenge_details'));
					}
					
					if($prev_res[0]['challenge_abstract']!= $this->input->post('company_details')){
						$mailBody .= $this->returnBody('Challenge Abstract', $prev_res[0]['challenge_abstract'], $this->input->post('company_details'));
					}
					
					if($prev_res[0]['challenge_launch_date']!= $launch_date){
						$mailBody .= $this->returnBody('Challenge Launch Date', $prev_res[0]['challenge_launch_date'], $launch_date);
					}
					
					if($prev_res[0]['challenge_close_date']!= $close_date){
						$mailBody .= $this->returnBody('Challenge Launch Date', $prev_res[0]['challenge_close_date'], $close_date);
					}
					
					
					// Technologies
					$ex_get_arr_1 = explode(',', $prev_res[0]['technology_id']);
					$postTech_list = $this->input->post('techonogy_id');
					
					if(count($ex_get_arr_1) >= count($techID)){ 
						//$diffVal = array_diff($ex_get_arr_1, $techID);
						$diffVal = $techID;	
						}else{			
						$diffVal = array_diff($techID, $ex_get_arr_1);	
					}
					
					if(count($diffVal) > 0){
						$techupdateArr = array();				
						foreach($techID as $t_id){
							
							$getTechName 	= $this->master_model->getRecords("technology_master", array("id" => $t_id));
							$sTechname		= $encrptopenssl->decrypt($getTechName[0]['technology_name']);
							array_push($techupdateArr, $sTechname);
						}	
						$implodeTechArr = implode(', ', $techupdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Technology</b></td><td>'.$implodeTechArr.'</td></tr>';
					} // Technologies End
					
					// Other Technology
					$other_tech_1 = explode(',', trim($prev_res[0]['other_techonology']));
					$postTech_1 = $this->input->post('other_technology');
					$exp_other_techonolgoyes = explode(',',trim($postTech_1));
					
					
					if(count($other_tech_1) >= count($exp_other_techonolgoyes)){ 
						$diffTechVal = array_diff($other_tech_1, $exp_other_techonolgoyes);				
						}else{			 
						$diffTechVal = array_diff($exp_other_techonolgoyes, $other_tech_1);	
					}
					
					
					if($postTech_1!="" && count($diffTechVal) > 0){
						
						$othertechupdateArr = array();
						foreach($exp_other_techonolgoyes as $o_id){					
							array_push($othertechupdateArr, $o_id);
						}	
						$implodeOtherTechArr = implode(', ', $othertechupdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Other Technology</b></td><td>'.$implodeOtherTechArr.'</td></tr>';
						
					}  // OTher End
					
					
					// Tags
					$ex_get_arr_2 = explode(',', $prev_res[0]['tags_id']);
					
					if(count($ex_get_arr_2) >= count($tagsID)){ 
						//$diffTagVal = array_diff($ex_get_arr_2, $tagsID);
						$diffTagVal = $tagsID;	
						}else{			
						$diffTagVal = array_diff($tagsID, $ex_get_arr_2);	
					}
					
					if(count($diffTagVal) > 0){
						$tagsupdateArr = array();
						foreach($tagsID as $tags_id){
							
							$getTagName 	= $this->master_model->getRecords("tags", array("id" => $tags_id));
							$sTagname		= $encrptopenssl->decrypt($getTagName[0]['tag_name']);
							array_push($tagsupdateArr, $sTagname);
						}	
						$implodeTagArr = implode(', ', $tagsupdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Tag</b></td><td>'.$implodeTagArr.'</td></tr>';
					}			
					
					// Other Tags
					$other_tags_1 = explode(',', $prev_res[0]['added_tag_name']);
					$checkOtherTag = explode(',', $otherTagname);			
					if(count($other_tags_1) >= count($checkOtherTag)){ 
						//$diffTagVal = array_diff($other_tags_1, $checkOtherTag);
						$diffTagVal = $checkOtherTag;	
						}else{			
						$diffTagVal = array_diff($checkOtherTag, $other_tags_1);	
					}
					
					if($otherTagname!="" && count($diffTagVal) > 0){
						$othertagupdateArr = array();
						foreach($diffTagVal as $g_id){					
							array_push($othertagupdateArr, $g_id);
						}	
						$implodeOtherTagArr = implode(', ', $othertagupdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Other Tag</b></td><td>'.$implodeOtherTagArr.'</td></tr>';
					} // OTher End
					
					// Audience Preference 
					$audiencePref_prev = explode(',', $prev_res[0]['audience_pref_id']);
					
					if(count($audiencePref_prev) >= count($pref)){ 
						$diffAudiVal = array_diff($pref, $audiencePref_prev);				
						}else{			
						$diffAudiVal = array_diff($pref, $audiencePref_prev);	
					}
					
					if(count($diffAudiVal) > 0){
						$audiUpdateArr = array();
						foreach($diffAudiVal as $a_id){
							$getAudiName 	= $this->master_model->getRecords("audience_pref", array("id" => $a_id));					
							$aTagname		= $encrptopenssl->decrypt($getAudiName[0]['preference_name']);
							array_push($audiUpdateArr, $aTagname);
						}	
						$implodeAudienceArr = implode(',', $audiUpdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Audience Preference</b></td><td>'.$implodeAudienceArr.'</td></tr>';
					} 
					
					if($this->input->post('if_funding')!=""){
						if($prev_res[0]['if_funding']!= $this->input->post('if_funding')){
							$mailBody .= $this->returnBody('Funding', $prev_res[0]['if_funding'], $this->input->post('if_funding'));
						}
					}
					
					if($prev_res[0]['is_amount']!= $this->input->post('is_amount')){
						$mailBody .= $this->returnBody('Funding Amount', $prev_res[0]['is_amount'], $this->input->post('is_amount'));
					}
					
					if($this->input->post('if_reward')!=""){
						if($prev_res[0]['if_reward']!= $this->input->post('if_reward')){
							$mailBody .= $this->returnBody('Reward', $prev_res[0]['if_reward'], $this->input->post('if_reward'));
						}
					}
					
					if($prev_res[0]['fund_reward']!= $this->input->post('fund_reward')){
						$mailBody .= $this->returnBody('Reward Details', $prev_res[0]['fund_reward'], $this->input->post('fund_reward'));
					}
					
					
					// TRL Solution
					if($prev_res[0]['trl_solution']!= $this->input->post('trl_solution')){
						$trlid = $this->input->post('trl_solution');
						$getTrlName 	= $this->master_model->getRecords("trl", array("id" => $trlid));
						$aTrlname		= $encrptopenssl->decrypt($getTrlName[0]['trl_name']);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Expected TRL </b></td><td>'.$aTrlname.'</td></tr>';
					}
					
					if($prev_res[0]['education']!= $this->input->post('educational')){
						$mailBody .= $this->returnBody('Educational', $prev_res[0]['education'], $this->input->post('educational'));
					}
					
					if($prev_res[0]['from_age']!= $this->input->post('from_age')){
						$mailBody .= $this->returnBody('From Age', $prev_res[0]['from_age'], $this->input->post('from_age'));
					}
					
					if($prev_res[0]['to_age']!= $this->input->post('to_age')){
						$mailBody .= $this->returnBody('To Age', $prev_res[0]['to_age'], $this->input->post('to_age'));
					}
					
					
					// Domain List 
					$domain_prev = explode(',', $prev_res[0]['domain']);
					
					if(count($domain_prev) >= count($domainPost)){
						$diffDomainVal = array_diff($domain_prev, $domainPost);				
						}else{			
						$diffDomainVal = array_diff($domainPost, $domain_prev);	
					}
					
					if(count($diffDomainVal) > 0){
						$domainUpdateArr = array();
						foreach($diffDomainVal as $d_id){
							$getDoaminName 	= $this->master_model->getRecords("domain_master", array("id" => $d_id));
							$aDname		= $encrptopenssl->decrypt($getDoaminName[0]['domain_name']);
							array_push($domainUpdateArr, $aDname);
						}	
						$implodeDomainArr = implode(', ', $domainUpdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Domain</b></td><td>'.$implodeDomainArr.'</td></tr>';
						} else {
						$mailBody .= '';
					} // Domain End
					
					// Geographycal List 
					$geo_prev = explode(',', $prev_res[0]['geographical_states']);
					$s_geo = $this->input->post('geographical');			
					if(count($geo_prev) >= count($s_geo)){ 
						//$diffGeoVal = array_diff($s_geo, $s_geo);
						$diffGeoVal = $s_geo;	
						}else{			
						$diffGeoVal = array_diff($s_geo, $geo_prev);	
					}
					
					if(count($diffGeoVal) > 0){
						$geoUpdateArr = array();
						foreach($diffGeoVal as $stat_id){
							$getStateName 	= $this->master_model->getRecords("states", array("id" => $stat_id));
							$states_nm		= $getStateName[0]['name'];
							array_push($geoUpdateArr, $states_nm);
						}	
						$implodeStateArr = implode(', ', $geoUpdateArr);
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Geographycal State</b></td><td>'.$implodeStateArr.'</td></tr>';
					} // Geographycal States End
					
					
					if($this->input->post('challenge_terms')!=""){
						
						if($prev_res[0]['terms_txt']!= $this->input->post('challenge_terms')){
							$mailBody .= $this->returnBody('Additional Terms & Conditions', $prev_res[0]['terms_txt'], $this->input->post('challenge_terms'));
						}
						
					}
					
					if($prev_res[0]['contact_person_name']!= $this->input->post('contact_name')){
						$mailBody .= $this->returnBody('Contact Person Name', $prev_res[0]['contact_person_name'], $this->input->post('contact_name'));
					}
					
					if($prev_res[0]['email_id']!= $this->input->post('contact_email')){
						$mailBody .= $this->returnBody('Contact Email', $prev_res[0]['email_id'], $this->input->post('contact_email'));
					}
					
					if($prev_res[0]['mobile_no']!= $this->input->post('mobile')){
						$mailBody .= $this->returnBody('Mobile', $prev_res[0]['mobile_no'], $this->input->post('mobile'));
					}
					
					if($prev_res[0]['office_no']!= $this->input->post('office_no')){
						$mailBody .= $this->returnBody('Office No', $prev_res[0]['office_no'], $this->input->post('office_no'));
					}
						
					if($this->input->post('details_share')!=""){	
					
						if($prev_res[0]['share_details']!= $this->input->post('details_share')){
							$showDetails = $this->input->post('details_share');
							$showT = 'No';
							if($showDetails == 1){
								$showT = 'Yes';
							}	
							$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>Share Contact Details</b></td><td>'.$showT.'</td></tr>';
							
							} else {
							$mailBody .= '';
						}
					}
					
					if($prev_res[0]['challenge_visibility']!= $this->input->post('visibility')){
						$mailBody .= $this->returnBody('Challenge Visibility', $prev_res[0]['challenge_visibility'], $this->input->post('visibility'));
					}
					
					if($prev_res[0]['future_opportunities']!= $this->input->post('future_opportunities')){
						$mailBody .= $this->returnBody('Future Opportunities', $prev_res[0]['future_opportunities'], $this->input->post('future_opportunities'));
					}
					
					
					
					// IP Clause 
					if($prev_res[0]['ip_clause']!= $this->input->post('ip_cls')){
						$clauses = $this->input->post('ip_cls');
						$getIPName 	= $this->master_model->getRecords("ip_clause", array("id" => $clauses));
						$ip_nm		= $getIPName[0]['ip_name'];	
						$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>IP Clause</b></td><td>'.$ip_nm.'</td></tr>';
						
					} 
					
					// External Funding Amount
					if($this->input->post('is_external_funding')){
						if($prev_res[0]['external_fund_details']!= $this->input->post('funding_amt')){
							$fund_post_amt = $this->input->post('funding_amt');
							$mailBody .= $this->returnBody('Percentage Of Funding (%)', $prev_res[0]['external_fund_details'], $fund_post_amt);
						}
					}
					
					// External Funding Amount
					if($this->input->post('is_exclusive_challenge') == 0){
						$ex_details_ch = $this->input->post('challenge_ex_details');
						if($prev_res[0]['exclusive_challenge_details']!= $ex_details_ch){	
						$mailBody .= $this->returnBody('Challenge Exclusively Content', $prev_res[0]['exclusive_challenge_details'], $ex_details_ch);
						}
					}
					
					$link  = base_url('challenge/challengeDetails/'.base64_encode($challenge_id));
					$linkName = '<a href="'.$link.'" target="_blank">View</a>';	
					$mailBody .= '<tr><td style="vertical-align: top;" nowrap><b>View Challenge</b></td><td>'.$linkName.'</td></tr>';	
					
					$mailBody .= '</table>';
					
					if($otherTagname){
						
						$tags_delete = $this->master_model->deleteRecord('tags','c_id',$challenge_id);
						
						$newTagExp = explode(",",$otherTagname);
						foreach($newTagExp as $createTag){
							$tagOtherContent = $encrptopenssl->encrypt($createTag);
							$tagDataInsert = array( 'tag_name' => $tagOtherContent, 'c_id' => $challenge_id, 'status' => 'Block');						
							$insertTagsQuery = $this->master_model->insertRecord('tags',$tagDataInsert);
							
						}
						
					}
					
					if($userTechnolgy){
						
						$tech_delete = $this->master_model->deleteRecord('technology_master','c_id',$challenge_id);
						
						// Insert Technology Name into Tag Table
						$newTechnologyExp = explode(",",$userTechnolgy);
						foreach($newTechnologyExp as $createTechnology){
							$techOtherContent = $encrptopenssl->encrypt($createTechnology);
							$techDataInsert = array( 'technology_name' => $techOtherContent, 'c_id' => $challenge_id, 'status' => 'Block');						
							$insertTechQuery = $this->master_model->insertRecord('technology_master',$techDataInsert);
							
						}
						
					}
					
					$updateQuery = $this->master_model->updateRecord('challenge',$updateArr,array('c_id' => $challenge_id));
					
					// Email Triggered to Challenge Owner 			
					/*$slug = $encrptopenssl->encrypt('edit_pending_challenge');	
					$challenge_edit_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table_1 = $this->master_model->getRecords("setting", array("id" => '1'));
					$content_edit = '';			
					
					if(count($challenge_edit_mail) > 0){
						
						$usersId 			= $this->session->userdata('user_id');
						$senders_details 	= $this->master_model->getRecords("registration", array("user_id" => $usersId));					
						$receiver_email 	= $encrptopenssl->decrypt($senders_details[0]['email']);
						//$receiver_email		= "vicky.kuwar@esds.co.in";
						$register_Type		= $senders_details[0]['user_category_id'];
						$register_SubType 	= $senders_details[0]['user_sub_category_id'];
						$membership_Type 	= $encrptopenssl->decrypt($senders_details[0]['membership_type']);
						
						$registerCat = $this->master_model->getRecords("registration_usercategory", array("id" => $register_Type));
						$categoryName = $encrptopenssl->decrypt($registerCat[0]['user_category']);
						
						$registerSub_Type = $this->master_model->getRecords("registration_usersubcategory", array("subcat_id" => $register_SubType));
						$sub_categoryName = $encrptopenssl->decrypt($registerSub_Type[0]['sub_catname']);
						
						$subject_1 	 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['email_title']);
						$description_1 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['email_description']);
						$from_admin_1 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['from_email']);					
						$sendername1  	= $setting_table_1[0]['field_1'];
						$contact_no1	= $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);
						$modifi_date    = date('d-m-Y', strtotime($close_date));
						
						//echo $mailBody;die();
						
						$arr_words = ['[BODY]', '[PHONENO]'];
						$rep_array = [$mailBody,  $contact_no1];
						
						$content_edit = str_replace($arr_words, $rep_array, $description_1);
						
						$info_arr_c=array(
						'to'		=>	$receiver_email,					
						'cc'		=>	'',
						'from'		=>	$from_admin_1,
						'subject'	=> 	$subject_1,
						'view'		=>  'common-file'
						);
						
						$other_info_c=array('content'=>$content_edit); 					
						
						$emailsend_1 = $this->emailsending->sendmail($info_arr_c,$other_info_c);
						
					}*/ // End Mail Triggered
					
					// Email Triggered to Challenge Owner New Content
					$slug = $encrptopenssl->encrypt('challenge_update_alert_to_owner');	
					$challenge_edit_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
					$content = '';	
					
					if(count($challenge_edit_mail) > 0){
						
						$usersId 			= $this->session->userdata('user_id');
						$senders_details 	= $this->master_model->getRecords("registration", array("user_id" => $usersId));					
						$receiver_email 	= $encrptopenssl->decrypt($senders_details[0]['email']);
						//$receiver_email		= "vicky.kuwar@esds.co.in";
						$firstnm			= $encrptopenssl->decrypt($senders_details[0]['first_name']);
						$middlenm			= $encrptopenssl->decrypt($senders_details[0]['middle_name']);
						$lastnm				= $encrptopenssl->decrypt($senders_details[0]['last_name']);
						$ch_user_name		= ucwords($firstnm)." ".ucwords($middlenm)." ".$lastnm;
						
						
						// Challenge Detail Get
						$challenge_last_details 	= $this->master_model->getRecords("challenge", array("c_id" => $challenge_id));
						$ch_full_name 	= $encrptopenssl->decrypt($challenge_last_details[0]['challenge_title']);
						$custom_ch_id 	= $challenge_last_details[0]['challenge_id'];
						$base_links		= base_url('challenge/challengeDetails/'.base64_encode($challenge_id));
						$view_ch		= '<a href="'.$base_links.'">View Detail</a>';
						
						$subject 	 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['email_title']);
						$description 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['email_description']);
						$from_admin 	= $encrptopenssl->decrypt($challenge_edit_mail[0]['from_email']);					
						$sendername1  	= $setting_table_1[0]['field_1'];
						$contact_no1	= $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);
						$modifi_date    = date('d-m-Y', strtotime($close_date));
						
						//echo $mailBody;die();
						
						$arr_words = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[UPDATED_FIELDS]', '[CHALLENGEDETAILLINK]'];
						$rep_array = [$ch_user_name,  $ch_full_name,  $custom_ch_id,  $mailBody,  $view_ch];
						
						$content = str_replace($arr_words, $rep_array, $description);
						
						$arr_sb = ['[CHALLENGENAME]', '[CHALLENGEID]'];
						$rep_sb = [$ch_full_name,  $custom_ch_id];
						$subject_change = str_replace($arr_sb, $rep_sb, $subject);
						
						$info_arr_c=array(
						'to'		=>	$receiver_email, //$receiver_email					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject_change,
						'view'		=>  'common-file'
						);
						
						$other_info_c=array('content'=>$content); 					
						
						$emailsend = $this->emailsending->sendmail($info_arr_c,$other_info_c);
						
					}
					
					// Email Triggered to Challenge Update Alert To Admin New Content
					$slug_2 = $encrptopenssl->encrypt('challenge_update_alert_to_admin');	
					$email_admin = $this->master_model->getRecords("email_template", array("slug" => $slug_2));
					$setting_table_2 = $this->master_model->getRecords("setting", array("id" => '1'));
					$content_2 = '';	
					
					if(count($email_admin) > 0){
						
						// Challenge Detail Get
						$challenge_last_details 	= $this->master_model->getRecords("challenge", array("c_id" => $challenge_id));
						$ch_full_name 	= $encrptopenssl->decrypt($challenge_last_details[0]['challenge_title']);
						$custom_ch_id 	= $challenge_last_details[0]['challenge_id'];
						$base_links		= base_url('challenge/challengeDetails/'.base64_encode($challenge_id));
						$view_ch		= '<a href="'.$base_links.'">View Detail</a>';
						
						$subject 	 	= $encrptopenssl->decrypt($email_admin[0]['email_title']);
						$description 	= $encrptopenssl->decrypt($email_admin[0]['email_description']);
						$from_admin 	= $encrptopenssl->decrypt($email_admin[0]['from_email']);					
						$sendername1  	= $setting_table_2[0]['field_1'];
						$admin_email  	= $encrptopenssl->decrypt($setting_table_2[0]['field_2']);
						
						$contact_no1	= $encrptopenssl->decrypt($setting_table_2[0]['contact_no']);
						$modifi_date    = date('d-m-Y', strtotime($close_date));
						
						//echo $mailBody;die();
						
						$arr_words = ['[CHALLENGENAME]', '[CHALLENGEID]', '[UPDATED_FIELDS]', '[CHALLENGEDETAILLINK]'];
						$rep_array = [$ch_full_name,  $custom_ch_id,  $mailBody,  $view_ch];
						
						//$arr_words = ['[BODY]', '[PHONENO]'];
						//$rep_array = [$mailBody,  $contact_no1];
						
						$content = str_replace($arr_words, $rep_array, $description);
						
						$arr_sb = ['[CHALLENGENAME]', '[CHALLENGEID]'];
						$rep_sb = [$ch_full_name,  $custom_ch_id];
						$subject_change = str_replace($arr_sb, $rep_sb, $subject);
						
						$info_arr_c=array(
						'to'		=>	$admin_email, //$admin_email					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject_change,
						'view'		=>  'common-file'
						);
						
						$other_info_c=array('content'=>$content); 					
						
						$emailsend = $this->emailsending->sendmail($info_arr_c,$other_info_c);
						
					} // Email To Admin
					
					
					// Associated Tead Email 
					$slug_3 = $encrptopenssl->encrypt('challenge_update_alert_to_team');	
					$team_email = $this->master_model->getRecords("email_template", array("slug" => $slug_3));
					$setting_table_3 = $this->master_model->getRecords("setting", array("id" => '1'));
					$content_3 = '';	
					if(count($team_email) > 0){
						
						// Challenge Detail Get
						$challenge_last_details 	= $this->master_model->getRecords("challenge", array("c_id" => $challenge_id));
						$ch_full_name 	= $encrptopenssl->decrypt($challenge_last_details[0]['challenge_title']);
						$custom_ch_id 	= $challenge_last_details[0]['challenge_id'];
						$base_links		= base_url('challenge/challengeDetails/'.base64_encode($challenge_id));
						$view_ch		= '<a href="'.$base_links.'">View Detail</a>';
						
						// Email Body
						$subject 	 	= $encrptopenssl->decrypt($email_admin[0]['email_title']);
						$description 	= $encrptopenssl->decrypt($email_admin[0]['email_description']);
						$from_admin 	= $encrptopenssl->decrypt($email_admin[0]['from_email']);					
						$sendername1  	= $setting_table_2[0]['field_1'];
						$admin_email  	= $encrptopenssl->decrypt($setting_table_2[0]['field_2']);
						
						$contact_no1	= $encrptopenssl->decrypt($setting_table_2[0]['contact_no']);
						$modifi_date    = date('d-m-Y', strtotime($close_date));
						
						// Teams Deatils
						$getTeams = $this->master_model->getRecords("byt_teams", array("c_id" => $challenge_id, "status" => 'Active', "is_deleted" => '0'));//"apply_status" => 'Applied', 
						
						foreach($getTeams as $team){
							
							$receiver_details 	= $this->master_model->getRecords("registration", array("user_id" => $team['user_id']));					
							$receiver_email 	= $encrptopenssl->decrypt($receiver_details[0]['email']);
							//$receiver_email		= "vicky.kuwar@esds.co.in";
							$firstnm			= $encrptopenssl->decrypt($receiver_details[0]['first_name']);
							$middlenm			= $encrptopenssl->decrypt($receiver_details[0]['middle_name']);
							$lastnm				= $encrptopenssl->decrypt($receiver_details[0]['last_name']);
							$ch_user_name		= ucwords($firstnm)." ".ucwords($middlenm)." ".$lastnm;
							
							$arr_words = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[UPDATED_FIELDS]', '[CHALLENGEDETAILLINK]'];
							$rep_array = [$ch_user_name,  $ch_full_name,  $custom_ch_id,  $mailBody,  $view_ch];
							
							$content_3 = str_replace($arr_words, $rep_array, $description);
							
							$arr_sb = ['[CHALLENGENAME]', '[CHALLENGEID]'];
							$rep_sb = [$ch_full_name,  $custom_ch_id];
							$subject_change = str_replace($arr_sb, $rep_sb, $subject);
							
							$info_arr_3=array(
							'to'		=>	$receiver_email, //$admin_email					
							'cc'		=>	'',
							'from'		=>	$from_admin,
							'subject'	=> 	$subject_change,
							'view'		=>  'common-file'
							);
							
							$other_info_3=array('content'=>$content_3); 					
							
							$emailsend = $this->emailsending->sendmail($info_arr_3,$other_info_3);
							
						} // Team Foreach End						
						
					}  //Count Exist End 
					
					// Insert SQL For Contact Details			
					$contactArr = array(	'c_id' 		=> $challenge_id,
					'email_id' 	=> $contact_email,
					'mobile_no' => $mobile,
					'office_no' => $office_no,
					'updatedAt' => date('Y-m-d H:i:s')
					);			
					$this->master_model->updateRecord('challenge_contact',$contactArr,array('c_id' => $challenge_id));
					
					
					// Eligibility Expectation 
					
					if($challengeStatusEdit == 'Pending'){
						
						$elegibilityArr = array(	'c_id' 					=> $challenge_id,
						'education' 			=> $educational,
						'from_age' 				=> $from_age,
						'to_age' 				=> $to_age,
						'domain' 				=> $domain_arr,
						'geographical_states' 	=> $geogra_arr,
						'min_team' 				=> $min_team,
						'max_team' 				=> $max_team,
						'updatedAt' 			=> date('Y-m-d H:i:s'));			
						$this->master_model->updateRecord('eligibility_expectation',$elegibilityArr,array('c_id' => $challenge_id));
					} // End If
					
					// Log Data Added
					$fileArr			= array('banner_img' => $bnImg);
					$postArr			= $this->input->post();				
					$arrLog 			= array_merge($fileArr,$postArr);					
					$json_encode_data 	= json_encode($arrLog);
					$ipAddr			  	= $this->get_client_ip();
					$createdAt			= date('Y-m-d H:i:s');
					$logDetails 		= array(
					'user_id' 		=> $user_id,
					'action_name' 	=> "Update",
					'module_name'	=> 'Challenge',
					'store_data'	=> $json_encode_data,
					'ip_address'	=> $ipAddr,
					'createdAt'		=> $createdAt
					);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					
					$this->session->set_flashdata('success','Challenge successfully updated.');		
					redirect(base_url('challenge/myChallenges'));
					
				} // Validation End
				
				
			} // Post Array END
			//echo "<pre>";print_r($res_challenges);die();
			$data['trl_data'] 	 	 = $trl_data;
			$data['domain_data'] 	 = $domain_data;
			$data['technology_data'] = $technology_data; 
			$data['tag_data'] 		 = $tag_data;
			$data['audi_data'] 		 = $audi_data;
			$data['challenge_data']  = $res_challenges;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/edit';
			$this->load->view('front/front_combo',$data);
			
		} // End Edit 
		
		
		public function returnBody($label, $old, $new){
			
			if($old !== $new){
				
				$content = '<tr><td nowrap><b>'.$label.'</b></td><td>'.$new.'</td></tr>';
				
				} else {
				
				$content = ''; 
			}
			
			return $content;
			
		}
		
		
		
		public function challengeDetails($id){
			error_reporting(0);
			$this->check_permissions->is_logged_in();
			$challenge_id = base64_decode($id);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->db->join('eligibility_expectation','challenge.c_id=eligibility_expectation.c_id','left');
			$this->db->join('challenge_contact','challenge.c_id=challenge_contact.c_id','left');
			$this->db->join('ip_clause','challenge.ip_clause=ip_clause.id','left');			
			//$this->db->join('trl','challenge.trl_solution=ip_clause.id','left');	
			$this->db->join('arai_registration r','r.user_id = u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$response_data = $this->master_model->getRecords("challenge", array("challenge.c_id" => $challenge_id));
			$user_id = $this->session->userdata('user_id');
			$data['request_user'] = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challenge_id, "sender_id" => $user_id));
			
			$data['total_apply_team'] = $this->master_model->getRecords("byt_teams", array("c_id" => $challenge_id, "is_deleted" => '0'));
			
			$data['total_received'] = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challenge_id));
			$data['pending_cnt'] = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challenge_id, "status" => 'Pending'));
			$data['approved_cnt'] = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challenge_id, "status" => 'Approved'));
			$data['rejected_cnt'] = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challenge_id, "status" => 'Rejected'));
			
			$res_arr = array();
			if(count($response_data)){	
				
				foreach($response_data as $row_val){	
					
					// Technology Master
					$tech = $row_val['technology_id'];			
					
					
					$explodeTech = explode(",",$tech);				
					$res_tech = array();
					$res_tech_id = array();
					foreach($explodeTech as $exp){					
						$technology_data = $this->master_model->getRecords("technology_master", array("id" => $exp));									
						if(count($technology_data) > 0){						
							$technology_name = $encrptopenssl->decrypt($technology_data[0]['technology_name']);							
							array_push($res_tech, $technology_name);						
						}
						array_push($res_tech_id, $exp);	
					}				
					
					$implodeTech = implode(",&nbsp;&nbsp;", $res_tech);
					
					
					$otherTechnologyName = '';
					
					if(in_array('0', $res_tech_id))
					{  
						$otherTechnologyName =  $encrptopenssl->decrypt($row_val['other_techonology']);
						
					} 
					
					// Tags Master								
					$tagsID = $row_val['tags_id'];				
					$explodeTag = explode(",",$tagsID);				
					$res_tags = array();
					$res_tag_id = array();
					foreach($explodeTag as $expT){					
						$tags_data = $this->master_model->getRecords("tags", array("id" => $expT));									
						if(count($tags_data) > 0){						
							$tag_name = $encrptopenssl->decrypt($tags_data[0]['tag_name']);							
							array_push($res_tags, $tag_name);	
						}
						array_push($res_tag_id, $expT);					
					}				
					$implodeTag = implode(",&nbsp;&nbsp;", $res_tags);
					
					$otherTagName = '';
					
					if(in_array("0", $res_tag_id))
					{
						$otherTagName =  $encrptopenssl->decrypt($row_val['added_tag_name']);
						
					} 				
					
					// Audience Preference Master
					$audiID = $row_val['audience_pref_id'];
					if($audiID!='0'){
						$explodeAudi = explode(",",$audiID);				
						$res_audi = array();
						foreach($explodeAudi as $expA){					
							$audience_data = $this->master_model->getRecords("audience_pref", array("id" => $expA));									
							if(count($audience_data) > 0){						
								$pref_name = $encrptopenssl->decrypt($audience_data[0]['preference_name']);							
								array_push($res_audi, $pref_name);	
							}	
						}					
						$implodeAudi = implode(",", $res_audi);	
						} else {
						
						$implodeAudi = $encrptopenssl->decrypt($row_val['other_audience']);
					}
					
					
					
					// Domain Master
					$domainID = $row_val['domain'];
					
					$explodeDomain = explode(",",$domainID);				
					$res_domain = array();
					foreach($explodeDomain as $expD){					
						$domains_data = $this->master_model->getRecords("domain_master", array("id" => $expD));									
						if(count($domains_data) > 0){						
							$dm_name = $encrptopenssl->decrypt($domains_data[0]['domain_name']);							
							array_push($res_domain, $dm_name);	
						}	
					}				
					$implodeDm = implode(",", $res_domain);
					
					/*********/
					
					$trlId = $encrptopenssl->decrypt($row_val['trl_solution']);
					$trl_data = $this->master_model->getRecords("trl", array("id" =>$trlId ));
					$trlName = $encrptopenssl->decrypt($trl_data[0]['trl_name']);
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['company_name']     		= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 	    = $encrptopenssl->decrypt($row_val['company_profile']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($row_val['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($row_val['max_team']);				
					$row_val['challenge_launch_date'] 	= $row_val['challenge_launch_date'];
					$row_val['challenge_close_date'] 	= $row_val['challenge_close_date'];
					$row_val['challenge_id'] 			= $row_val['challenge_id'];
					$row_val['technology_name'] 	    = $implodeTech;
					$row_val['tag_name'] 	    		= $implodeTag;
					$row_val['preference_name'] 	    = $implodeAudi;
					$row_val['domain_name'] 	    	= $implodeDm;
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_solution'] 	    	= $trlName;
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['status'] 					= $row_val['status']; 
					$row_val['ip_name'] 				= $row_val['ip_name']; 
					$row_val['is_publish'] 				= $row_val['is_publish'];
					$row_val['is_featured'] 			= $row_val['is_featured'];
					$row_val['external_fund_details'] 	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['is_exclusive_challenge'] 	= $row_val['is_exclusive_challenge'];
					$row_val['exclusive_challenge_details'] 	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($row_val['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($row_val['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($row_val['office_no']);
					$row_val['other_tag_name']			= $otherTagName;	
					$row_val['other_technology_name']	= $otherTechnologyName;
					$row_val['res_tech_id']				= $res_tech_id;
					$row_val['res_tag_id']				= $res_tag_id;
					$row_val['education'] 				= $encrptopenssl->decrypt($row_val['education']);	
					$row_val['from_age'] 				= $encrptopenssl->decrypt($row_val['from_age']);	
					$row_val['to_age'] 					= $encrptopenssl->decrypt($row_val['to_age']);	
					
					$res_arr[] = $row_val;
				}
				
			}
			
			// Featured Challenge Management
			$date = date('Y-m-d');
			
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$featuredChall 	= $this->master_model->getRecords('challenge c',array('c.challenge_status'=>"Approved", 'c.is_featured' => "Y", 'c.challenge_close_date >=' => $date),'',array('c.c_id' => "DESC"),0,9);
			//echo $this->db->last_query();
			$featureArr = array();		
			if(count($featuredChall)){				
				
				foreach($featuredChall as $feat_val){
					
					$valArr = array();
					$audience_pref_id 	= $feat_val['audience_pref_id'];
					
					error_reporting(0);
					if($audience_pref_id!= 0){
						
						$explode = explode(",",$audience_pref_id);
						$valArr = array();	
						foreach($explode as $val){
							
							$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
							$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
							array_push($valArr, $audienceName);					
						}
						$implode = implode(",", $valArr);
						
						} else {
						
						$implode = 'Other';
					}
					
					$feat_val['challenge_title'] 		= $encrptopenssl->decrypt($feat_val['challenge_title']);
					$feat_val['banner_img'] 			= $encrptopenssl->decrypt($feat_val['banner_img']);
					$feat_val['company_name'] 			= $encrptopenssl->decrypt($feat_val['company_name']);
					$feat_val['challenge_details'] 		= $encrptopenssl->decrypt($feat_val['challenge_details']);
					$feat_val['challenge_close_date'] 	= $feat_val['challenge_close_date'];
					$feat_val['audience_pref'] 			= $implode;	
					$featureArr[] = $feat_val;
				}
				
			} 
			
			// Government Partners 
			$government_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "G"));
			
			$goverArr = array();
			if(count($government_mgt)){				
				
				foreach($government_mgt as $gove_val){
					
					$gove_val['partner_name'] 	= $encrptopenssl->decrypt($gove_val['partner_name']);
					$gove_val['partner_img'] 	= $encrptopenssl->decrypt($gove_val['partner_img']);
					$goverArr[] = $gove_val;
					
				}
				
			}
			
			
			// Consortium Partners 
			$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"));
			
			$consortArr = array();
			if(count($consortium_mgt)){				
				
				foreach($consortium_mgt as $con_val){
					
					$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
					$con_val['img_link'] 		= $con_val['img_link'];
					$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
					$consortArr[] = $con_val;
					
				}
				
			}
			
			// Get Apply Teams Details Listing 
			$teamDetails  = $this->master_model->getRecords('byt_teams',array('c_id'=> $challenge_id, 'is_deleted'=>"0",  'status'=>"Active", 'team_type'=>"challenge" ),'',array('team_id', 'ASC'),0,6);//'apply_status' => 'Withdrawn',
			
			$setting_table_check 	= $this->master_model->getRecords("setting", array("id" => '1'));		
			$data['quoteTitle'] = $encrptopenssl->decrypt($setting_table_check[0]['field_3']);
			$data['partneTitle'] = $encrptopenssl->decrypt($setting_table_check[0]['field_4']);
			
			/*********************Challenge Updates Start*********************/
			$data['closureUpdates'] = $this->master_model->getRecords("challenge_closure_update", array("c_id" => $challenge_id),'',array("id" => 'DESC'));
			/*******************Challenge Updates End***********************/
			
			
			$data['team_limit']=6;
			$data['team_details']    = $teamDetails;
			$data['consortium_list'] = $consortArr;
			$data['government_list'] = $goverArr;
			$data['featured_list']	 = $featureArr;
			$data['challenge_data']  = $res_arr;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/details';
			$this->load->view('front/front_combo',$data);
		}
	
	public function load_team_listing_ajax()
	{	
		// ini_set('display_errors', '1');
		// ini_set('display_startup_errors', '1');
		// error_reporting(E_ALL);
		
			$encrptopenssl =  New Opensslencryptdecrypt();
			$data=array();
			$data['jobs_count']=0;
			
			if (isset($_POST) && count($_POST)>0 ) {
				
				$start=$this->input->post('start');
				$limit=$this->input->post('limit');
				$searchText =$this->input->post('searchText');
				$is_show_more =$this->input->post('is_show_more');
				$challenge_id =base64_decode($this->input->post('challenge_id'));
				
				$search_str = "";
				if ($searchText!='') {$search_str .= "(team_name LIKE '%" . $searchText . "%' || brief_team_info LIKE '%" . $searchText . "%')";}
				if ($search_str != "") {$this->db->where($search_str);}
				
				$data['team_details']= $team_details  = $this->master_model->getRecords('byt_teams',array('c_id'=> $challenge_id, 'is_deleted'=>"0",  'status'=>"Active", 'team_type'=>"challenge" ),'',array('team_id', 'ASC'),$start,$limit);

				
				if ($search_str != "") {$this->db->where($search_str);}
				$this->db->select('team_id');
				$data['team_details_all']= $team_details_all  = $this->master_model->getRecords('byt_teams',array('c_id'=> $challenge_id, 'is_deleted'=>"0",  'status'=>"Active", 'team_type'=>"challenge" ),'',array('team_id', 'ASC'));

				// echo "<pre>";print_r($res_arr);die;
				
				$data['teams_count']=count($team_details);
				$data['teams_all_count']=count($team_details_all);
				$data['limit'] = $limit;
				$data['new_start']=$start + $limit;
				
		}
		$json_data['teams_listing_html']= $this->load->view('/front/challenge/inc_team_listing',$data,true);
		// $json_data['jobs_all_count']=$jobs_all;
		echo json_encode($json_data);
	}
		
		public function getNumData($query){
			//echo $query;
			return $rowCount = $this->db->query($query)->num_rows();
		}
		
		
		public function my_clist(){
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$userId = $this->session->userdata('user_id');
			
			$techonogy_id 			= @$this->input->post('techonogy_id')?$this->input->post('techonogy_id'):'';
			$change_visibility 		= @$this->input->post('change_visibility')?$this->input->post('change_visibility'):'';
			$c_status 				= @$this->input->post('c_status')?$this->input->post('c_status'):'';
			$audience_pref 			= @$this->input->post('audience_pref')?$this->input->post('audience_pref'):'';
			
			//echo "<pre>";print_r($this->input->post());
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;
			
			
			
			$c_data = "";
			if(!empty($techonogy_id)){
				
				if(count((array)$techonogy_id) > 0){
					$c_data .= " AND (";
					$a =0;
					foreach($techonogy_id as $y){
						
						if($a!=(count($techonogy_id) - 1)){
							$c_data .= " FIND_IN_SET('".$y."',`technology_id`) OR ";
							} else {
							$c_data .= " FIND_IN_SET('".$y."',`technology_id`))";
						}				
						$a++;
					}
					
				}
				
			}
			
			
			if(!empty($audience_pref)){
				
				if(count((array)$audience_pref) > 0){
					$c_data .= " AND (";
					$p =0;
					foreach($audience_pref as $d){
						
						if($p!=(count($audience_pref) - 1)){
							$c_data .= " FIND_IN_SET('".$d."',`audience_pref_id`) OR ";
							} else {
							$c_data .= " FIND_IN_SET('".$d."',`audience_pref_id`))";
						}				
						$p++;
					}
					
				}
				
			}
			
			
			if($change_visibility){
				
				$c_data .= " AND arai_challenge.challenge_visibility = '".$change_visibility."'";
			}
			
			if($c_status){
				
				$c_data .= " AND arai_challenge.challenge_status = '".$c_status."'";
				} else {
				$c_data .= " AND arai_challenge.challenge_status != 'Draft'";
			}		
			
			
			$condition = "SELECT arai_challenge.c_id, arai_challenge.challenge_id, arai_challenge.challenge_title, arai_challenge.company_name, arai_challenge.challenge_launch_date, arai_challenge.challenge_close_date, arai_challenge.challenge_status, arai_challenge.challenge_visibility, arai_ip_clause.ip_name FROM arai_challenge 
			JOIN arai_ip_clause ON arai_ip_clause.id = arai_challenge.ip_clause
			INNER JOIN arai_registration r ON r.user_id = arai_challenge.u_id
			WHERE r.is_deleted = '0' AND arai_challenge.u_id = '".$userId."'  ".$c_data."";
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			//echo $query;die();
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_challenge_details){
					
					$challenge_title 	= $encrptopenssl->decrypt($get_challenge_details->challenge_title);
					$company_name 		= $encrptopenssl->decrypt($get_challenge_details->company_name);				
					$close_date			= date('d-m-Y', strtotime($get_challenge_details->challenge_close_date));
					$launch_date		= date('d-m-Y', strtotime($get_challenge_details->challenge_launch_date));
					$challenge_status	= $get_challenge_details->challenge_status;
					$c_visibility		= $get_challenge_details->challenge_visibility;	
					$challenge_id		= $get_challenge_details->challenge_id;	 	
					$e_link				= base_url('challenge/edit/'.base64_encode($get_challenge_details->c_id));
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_challenge_details->c_id));
					$v_teams			= base_url('challenge/teamListing/'.base64_encode($get_challenge_details->c_id));
					
					$appReceived		= $this->master_model->getRecords("challenge_details_request", array("c_id" => $get_challenge_details->c_id));
					
					$appApproved		= $this->master_model->getRecords("challenge_details_request", array("status" => 'Approved', "c_id" => $get_challenge_details->c_id));
					
					//$edit				= '<a href="'.$e_link.'"><i class="fa fa-edit" aria-hidden="true"></i></a>'; 
					//$view				= '<a href="'.$v_link.'"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					//$conCat				= $view."&nbsp;".$edit;				
					//$edit				= '<a href="'.$e_link.'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
					/*$challenge_status   = 'Withdrawn';*/
					if($challenge_status == 'Withdrawn' || $challenge_status == 'Closed' || $challenge_status == 'Rejected'){
						//$challenge_status == '';
						$withdraw   = '';
						
						} else {
						//$withdraw   = '';
						$withdraw			= '<span id="remove-'.$get_challenge_details->c_id.'"><a href="javascript:void(0);" alt="Withdrawn" title="Withdrawn" class="withdrawn-challenge btn btn-info btn-green-fresh ch-text" data-id="'.$get_challenge_details->c_id.'" id="ch-status-'.$get_challenge_details->c_id.'" onClick="return updateChallengeStatus('.$get_challenge_details->c_id.');"><i class="fa fa-ban" aria-hidden="true"></i></a></span>'; 
					}
					$teams =	'<a href="'.$v_teams.'" class="btn btn-success btn-green-fresh" alt="View Team" title="View Team"><i class="fa fa-users" aria-hidden="true"></i></a>';
					
					//$withdraw			= '<a href="javascript:void(0);" class="withraw-challenge ch-text" data-id="'.$get_challenge_details->c_id.'" id="ch-status-'.$get_challenge_details->c_id.'">Withdrawn</a>'; 
					$edit				= '<a href="'.$e_link.'" class="btn btn-info btn-green-fresh" alt="Edit" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>'; 
					$view				= '<a href="'.$v_link.'" class="btn btn-success btn-green-fresh" alt="View" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					//$conCat				= $edit."&nbsp;".$view."&nbsp;".$teams."&nbsp;".$withdraw;
					$closeCh = '';
					/*if($challenge_status == "Closed" || $challenge_status == "Approved" ){
						
						$closeCh = '<a href="'.base_url('challenge/closeChallenge/'.base64_encode($get_challenge_details->c_id)).'" class="btn btn-success btn-green-fresh" alt="Close Challenge" title="Close Challenge"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
						
					}*/
					
					$closeCh = '<a href="'.base_url('challenge/closeChallenge/'.base64_encode($get_challenge_details->c_id)).'" class="btn btn-success btn-green-fresh" alt="Close Challenge" title="Challenge Updates"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>';
					
					
					
					
					$conCat				= "<span style='white-space: nowrap;' >".$edit."&nbsp;".$view."&nbsp;".$teams."&nbsp;".$withdraw."&nbsp;".$closeCh."</span>";
					$funding			= "Yes";
					$apppli_received	= count($appReceived);
					$apppli_arrpoved	= count($appApproved);
					$update_status_text = '<span id="add-'.$get_challenge_details->c_id.'">'.$challenge_status.'</span>';
					
					$i++;
					$dataArr[] = array(
					$challenge_id,
					$challenge_title,
					$launch_date,
					$close_date,						  
					$funding,
					$apppli_received,
					$apppli_arrpoved,
					$update_status_text,
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
			}
			
			echo json_encode($response);	
			
			
		} 
		
		public function myChallenges(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');
			
			// Technology Master
			/*$technology_data = $this->master_model->getRecords("technology_master", array("status" => 'Active'));
			$res_tech = array();
			if(count($technology_data)){						
				foreach($technology_data as $row_val){							
					$row_val['technology_name'] = $encrptopenssl->decrypt($row_val['technology_name']);	
					$row_val['id'] = $row_val['id'];	
					$res_tech[] = $row_val;
				}			
			}	
			
			
			// Audience Preference Master
			$audi_data = $this->master_model->getRecords("audience_pref", array("status" => 'Active'));
			$res_audi = array();
			if(count($audi_data)){						
				foreach($audi_data as $row_val){							
					$row_val['preference_name'] = $encrptopenssl->decrypt($row_val['preference_name']);	
					$row_val['id'] = $row_val['id'];	
					$res_audi[] = $row_val;
				}			
			}*/
			$technology_data= $this->master_model->array_sorting($this->master_model->getRecords('technology_master',array('status'=>"Active")), array('id'),'technology_name');

			$audi_data= $this->master_model->array_sorting($this->master_model->getRecords('audience_pref',array('status'=>"Active")), array('id'),'preference_name');	
			
			// Challenge Visibility Master
			$challengetype = $this->master_model->getRecords("challengetype", array("status" => 'Active'));
			$res_v = array();
			if(count($challengetype)){						
				foreach($challengetype as $row_val){							
					$row_val['challenge_type'] = $encrptopenssl->decrypt($row_val['challenge_type']);	
					$row_val['id'] = $row_val['id'];	
					$res_v[] = $row_val;
				}			
			}
			
			// TRL Master
			$trl_data = $this->master_model->getRecords("trl", array("status" => 'Active'));
			$res_trl = array();
			if(count($trl_data)){						
				foreach($trl_data as $row_val){							
					$row_val['trl_name'] = $encrptopenssl->decrypt($row_val['trl_name']);	
					$row_val['id'] = $row_val['id'];	
					$res_trl[] = $row_val;
				}			
			}
			
			// IP Clause Master
			$data['ip_clause_data'] = $this->master_model->getRecords("ip_clause", array("status" => 'Active'));
			
			
			
			$data['audience_data'] 	= $audi_data; 
			$data['challengetype'] 	= $res_v; 
			$data['technology_data'] = $technology_data;  
			$data['trl_data'] 		= $res_trl; 
			//$data['challenge_data'] = $res_arr;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/my-challenges';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function withdrawChallenge(){
			$csrf_test_name = $this->security->get_csrf_hash();
			$userId = $this->session->userdata('user_id');
			$id = $this->input->post('id');
			$date = date('Y-m-d');
			$updateArr = array("challenge_status" => 'Withdrawn', "last_updated_date" =>$date, "updated_by_id" => $userId);
			$this->master_model->updateRecord('challenge',$updateArr,array('c_id' => $id, 'u_id' => $userId));
			$text = "Done";
			$jsonData = array("text" => $text,"token" => $csrf_test_name);
			
			$json_encode_data 	= json_encode($updateArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$logDetails 		= array(
			'user_id' 		=> $userId,
			'action_name' 	=> "Withdrawn Challenge",
			'module_name'	=> 'Challenge',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			echo json_encode($jsonData);
		}
		
		public function applied_clist(){
			error_reporting(0);
			$encrptopenssl =  New Opensslencryptdecrypt();
			//echo ">>>".$userId = $this->session->userdata('user_id');die();
			$userId =  $this->input->post('user_id');
			
			$techonogy_id 			= @$this->input->post('techonogy_id')?$this->input->post('techonogy_id'):'';
			$change_visibility 		= @$this->input->post('change_visibility')?$this->input->post('change_visibility'):'';
			$ip_clause 				= @$this->input->post('ip_clause')?$this->input->post('ip_clause'):'';
			$fund_sel 				= @$this->input->post('fund_sel')?$this->input->post('fund_sel'):'';
			$reward_sel 			= @$this->input->post('reward_sel')?$this->input->post('reward_sel'):'';
			$trl_id 				= @$this->input->post('trl_id')?$this->input->post('trl_id'):'';
			
			//echo "<pre>";print_r($this->input->post());die();
			
			//echo "<pre>";print_r($this->input->post());die();
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;
			
			
			
			$c_data = "";
			if(!empty($techonogy_id)){
				
				if(count((array)$techonogy_id) > 0){
					$c_data .= " AND (";
					$a =0;
					foreach($techonogy_id as $y){
						
						if($a!=(count($techonogy_id) - 1)){
							$c_data .= " FIND_IN_SET('".$y."',`technology_id`) OR ";
							} else {
							$c_data .= " FIND_IN_SET('".$y."',`technology_id`))";
						}				
						$a++;
					}
					
				}
				
			}
			
			
			if($change_visibility){
				
				$c_data .= " AND arai_challenge.challenge_visibility = '".$change_visibility."'";
			}
			
			if($ip_clause){
				
				$c_data .= " AND arai_challenge.ip_clause = '".$ip_clause."'";
				
			}
			
			if($fund_sel!=""){
				
				if($fund_sel == '1'){
					$c_data .= " AND arai_challenge.if_funding = 'Funding'";
					} else {
					$c_data .= "";
				}
				
			}
			
			if($reward_sel!=""){
				
				if($reward_sel == '1'){
					$c_data .= " AND arai_challenge.if_reward = 'Reward'";
					} else {
					$c_data .= "";
				}
				
				
			}
			
			if($trl_id){
				$trl_ids = $encrptopenssl->encrypt($trl_id);
				$c_data .= " AND arai_challenge.trl_solution = '".$trl_ids."'";
				
			}
			
			/*$condition = "SELECT arai_challenge_apply.a_id, arai_challenge_apply.req_sender_id, arai_challenge_apply.req_receiver_id, arai_challenge_apply.invitation_status, arai_challenge_apply.team_id, 
				arai_team.team_id, arai_team.team_name, arai_challenge.challenge_title, arai_challenge.company_name FROM arai_challenge_apply  
				JOIN arai_challenge ON arai_challenge_apply.c_id = arai_challenge.c_id
			WHERE arai_challenge_apply.req_sender_id = '".$userId."'  ".$c_data."";*/
			
			/*$condition = "SELECT arai_challenge_apply.a_id, arai_challenge_apply.req_sender_id, arai_challenge_apply.req_receiver_id, arai_challenge_apply.invitation_status, 
				arai_challenge.challenge_title, arai_challenge.company_name FROM arai_challenge_apply  
				JOIN arai_challenge ON arai_challenge_apply.c_id = arai_challenge.c_id					 
			WHERE arai_challenge_apply.req_sender_id = '".$userId."'  ".$c_data."";*/
			
			$condition = "SELECT arai_challenge.c_id, arai_challenge.u_id, arai_challenge.challenge_id, arai_challenge.challenge_title,
			arai_challenge.company_name, arai_challenge.challenge_close_date, arai_challenge.technology_id, arai_challenge.if_funding,
			arai_challenge.c_id, arai_challenge.if_reward, arai_challenge.challenge_status, arai_challenge.ip_clause, arai_challenge.challenge_visibility, 
			arai_byt_slot_applications.team_id, arai_byt_slot_applications.slot_id, arai_byt_slot_applications.app_id, arai_byt_slot_applications.status AS mystatus, arai_byt_slot_applications.team_id  
			FROM arai_challenge 
			JOIN arai_byt_slot_applications ON arai_challenge.c_id = arai_byt_slot_applications.c_id					 
			WHERE arai_challenge.is_deleted = '0' AND (arai_challenge.challenge_status = 'Pending' OR arai_challenge.challenge_status = 'Approved') AND arai_byt_slot_applications.is_deleted = '0' AND arai_byt_slot_applications.apply_user_id = '".$userId."'  ".$c_data."";					
			 
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			//echo $query;die();
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_challenge_details){
					
					$challenge_title 	= $encrptopenssl->decrypt($get_challenge_details->challenge_title);
					$company_name 		= $encrptopenssl->decrypt($get_challenge_details->company_name);				
					$close_date			= date('d-m-Y', strtotime($get_challenge_details->challenge_close_date));
					$challenge_status	= $get_challenge_details->challenge_status;
					$team_id	= $get_challenge_details->team_id;
					$invitation_status	= $get_challenge_details->mystatus;
					
					$getTeamname = $this->master_model->getRecords("byt_teams", array("team_id" => $team_id));
					$team_name			= $getTeamname[0]['team_name'];
					
					//$e_link			= base_url('challenge/edit/'.base64_encode($get_challenge_details->c_id));
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_challenge_details->c_id));
					//$edit				= '<a href="'.$e_link.'"><i class="fa fa-edit" aria-hidden="true"></i></a>'; 
					$view				= '<a class="btn btn-success btn-green-fresh" href="'.$v_link.'"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					//$withdraw			= '<a class="btn btn-success btn-green-fresh"  href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>';
					$withdraw = '';
					$conCat				= $view."&nbsp;".$withdraw;
					
					$i++;
					$dataArr[] = array(
					$i,
					$challenge_title,
					$company_name,						  
					$close_date,
					$challenge_status,
					$invitation_status,
					$team_name,
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
			}
			
			echo json_encode($response);	
			
			
		} 
		
		public function applyChallengelist(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');
			
			// Technology Master		
			$technology_data= $this->master_model->array_sorting($this->master_model->getRecords('technology_master',array('status'=>"Active")), array('id'),'technology_name');			
			
			/*
				$technology_data = $this->master_model->getRecords("technology_master", array("status" => 'Active'));
				$res_tech = array();
				if(count($technology_data)){						
				foreach($technology_data as $row_val){							
				$row_val['technology_name'] = $encrptopenssl->decrypt($row_val['technology_name']);	
				$row_val['id'] = $row_val['id'];	
				$res_tech[] = $row_val;
				}			
			}*/		
			
			// Challenge Visibility Master
			$challengetype= $this->master_model->array_sorting($this->master_model->getRecords('challengetype',array('status'=>"Active")), array('id'),'challenge_type');			
			//echo "<pre>";print_r($challengetype);die();
			/*$challengetype = $this->master_model->getRecords("challengetype", array("status" => 'Active'));
				$res_v = array();
				if(count($challengetype)){						
				foreach($challengetype as $row_val){							
				$row_val['challenge_type'] = $encrptopenssl->decrypt($row_val['challenge_type']);	
				$row_val['id'] = $row_val['id'];	
				$res_v[] = $row_val;
				}			
			}*/
			
			// TRL Master
			$trl_data= $this->master_model->array_sorting($this->master_model->getRecords('trl',array('status'=>"Active")), array('id'),'trl_name');			
			/*$trl_data = $this->master_model->getRecords("trl", array("status" => 'Active'));
				$res_trl = array();
				if(count($trl_data)){						
				foreach($trl_data as $row_val){							
				$row_val['trl_name'] = $encrptopenssl->decrypt($row_val['trl_name']);	
				$row_val['id'] = $row_val['id'];	
				$res_trl[] = $row_val;
				}			
			}*/
			
			// IP Clause Master
			$data['ip_clause_data'] = $this->master_model->getRecords("ip_clause", array("status" => 'Active'),'',array("ip_name" => 'ASC'));	
			//echo $this->db->last_query();die();
			
			$data['challengetype'] 	= $challengetype; 
			$data['technology_data'] = $technology_data;  
			$data['trl_data'] 		= $trl_data; 
			//$data['challenge_data'] = $res_arr;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/apply-challenges';
			$this->load->view('front/front_combo',$data);
			
		}	
		
		
		public function askfordetails(){
			error_reporting(0);
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$challengeId 		= $this->input->post('c_id');
			$challengeOwnerID 	= $this->input->post('o_id');
			$user_id 			= $this->session->userdata('user_id');	
			
			$mergeArr			= $this->input->post();	
			
			
			
			$checkRes = $this->master_model->getRecords("challenge_details_request", array("c_id" => $challengeId, "sender_id" => $user_id));
			if(count($checkRes) > 0) {
				
				if($checkRes[0]['status'] == 'Rejected'){
					$text = 'Your request rejected by challenge owner.';
				} else {
					$text = "You alredy requested for owner details.";
				}
				
				$exist = 'Y';
				
				} else {
				
				$insertArr = array(
				'c_id' 			=> $challengeId,
				'sender_id' 	=> $user_id,
				'receiver_id' 	=> $challengeOwnerID,
				'status' 		=> 'Pending'
				
				);
				
				$insert_id = $this->master_model->insertRecord('challenge_details_request',$insertArr);
				$text = "Waiting For Owner Approval";
				$exist = 'N';
				
				
				$json_data 			= array_merge($insertArr,$mergeArr);
				$json_encode_data 	= json_encode($json_data);
				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
											'user_id' 		=> $user_id,
											'action_name' 	=> "Ask To Owner Challenge Details",
											'module_name'	=> 'Challenge',
											'store_data'	=> $json_encode_data,
											'ip_address'	=> $ipAddr,
											'createdAt'		=> $createdAt
										);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				
				// Send Email TO Challenge Owner New
				$slug = $encrptopenssl->encrypt('challenge_owner_private_request');		
				$slugExist = $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table_1 = $this->master_model->getRecords("setting", array("id" => '1'));
				$content_owner = '';
				
				$challenge_owner 	= $this->master_model->getRecords("registration", array("user_id" => $challengeOwnerID, 'is_deleted' => '0'));
				
				$usersId 			= $this->session->userdata('user_id');
				$senders_details 	= $this->master_model->getRecords("registration", array("user_id" => $usersId, 'is_deleted' => '0'));
				//echo $this->db->last_query();
				
				if(count($challenge_owner) > 0){
					
					// Sender Details
					if($senders_details[0]['user_category_id'] == '1'){
						$challenge_sender_profile 	= $this->master_model->getRecords("student_profile", array("user_id" => $usersId)); 
					} else {
						$challenge_sender_profile 	= $this->master_model->getRecords("profile_organization", array("user_id" => $usersId)); 
					}
					
					// Challenge Owner Details
					$o_full_name = $encrptopenssl->decrypt($challenge_owner[0]['title'])." ".$encrptopenssl->decrypt($challenge_owner[0]['first_name'])." ".$encrptopenssl->decrypt($challenge_owner[0]['middle_name'])." ".$encrptopenssl->decrypt($challenge_owner[0]['last_name']);
					$o_email_id = $encrptopenssl->decrypt($challenge_owner[0]['email']);
					//$o_email_id = 'vicky.kuwar@esds.co.in';
					$owner_fullname = ucwords($o_full_name);
					
					// Challenge Details
					$challenge_desc 	= $this->master_model->getRecords("challenge", array("c_id" => $challengeId));
					$challengeNames = $encrptopenssl->decrypt($challenge_desc[0]['challenge_title']);
					$challenge_new_id = $challenge_desc[0]['challenge_id'];
					
					if(count($slugExist) > 0){
						
						// Email Body Content
						$subject_1 	 	= $encrptopenssl->decrypt($slugExist[0]['email_title']);
						$description_1 	= $encrptopenssl->decrypt($slugExist[0]['email_description']);
						$from_admin_1 	= $encrptopenssl->decrypt($slugExist[0]['from_email']);					
						$sendername1  	= $setting_table_1[0]['field_1'];
						$contact_no1	= $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);
						
						
						$sender_email 	= $encrptopenssl->decrypt($senders_details[0]['email']);
						$sender_ids 	= base64_encode($senders_details[0]['user_id']);						
						$fullname 		= $encrptopenssl->decrypt($senders_details[0]['first_name'])." ".$encrptopenssl->decrypt($senders_details[0]['middle_name'])." ".$encrptopenssl->decrypt($senders_details[0]['last_name']);
						$user_fullname  = ucwords($fullname);
						
						// Get Team Name
						$challengeId 		= $this->input->post('c_id');
						$challengeOwnerID 	= $this->input->post('o_id');
						$cid 				= base64_encode($challengeId);
						$team_desc 			= $this->master_model->getRecords("byt_teams", array("c_id" => $challengeId, "user_id" => $challengeOwnerID));
						
						$teamName 			= ucwords($team_desc[0]['team_name']);
						$teamId				= $team_desc[0]['team_id'];
						$link 				= base_url('myteams/team_details_member/'.base64_encode($teamId));
						$viewLink			= '<a href="'.$link.'" target="_blank">view detail</a>';	
						
						// Challenge Details
						$challengeProfile = base_url('challenge/challengeDetails/'.$cid);
						$view			= '<a href="'.$challengeProfile.'" target="_blank">View Profile</a>';
						
						$arr_words = ['[USERNAME]', '[TEAMNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[TEAMDETAILLINK]', '[CHALLENGEDETAILLINK]'];
						$rep_array = [$owner_fullname,  $teamName, $challengeNames, $challenge_new_id, $viewLink, $view];
						
						$content_owner = str_replace($arr_words, $rep_array, $description_1);
						
						
						//Subject Custom 
						$sub_arr = ['[CHALLENGENAME]'];
						$rep_arr = [$challengeNames];
						$sub_content = str_replace($sub_arr, $rep_arr, $subject_1);
						
						$info_arr_c=array(
							'to'		=>	$o_email_id,				
							'cc'		=>	'',
							'from'		=>	$sender_email,
							'subject'	=> 	$sub_content,
							'view'		=>  'common-file'
						);
						
						$other_info_c=array('content'=>$content_owner); 					
						
						$emailsend_1 = $this->emailsending->sendmail($info_arr_c,$other_info_c);
						
					} // Slug Exist
					
				} // Owner Exist Mail Sent	
				
				
				$usersId 			= $this->session->userdata('user_id');
				$challengeId 		= $this->input->post('c_id');
				$team_exist 		= $this->master_model->getRecords("byt_teams", array("c_id" => $challengeId, "user_id" => $usersId));
				//echo $this->db->last_query();
				//echo ">>".count($team_exist);		
				
				// Request Sender Details New
				if(count($team_exist) > 0){
					
					$slug_1 		= $encrptopenssl->encrypt('team_owner_challenge_private_request');		
					$sender_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug_1));
					$setting_table_2 = $this->master_model->getRecords("setting", array("id" => '1'));
					$content_sender = '';
					
					$subject_2 	 	= $encrptopenssl->decrypt($sender_mail[0]['email_title']);
					$description_2 	= $encrptopenssl->decrypt($sender_mail[0]['email_description']);
					$from_admin_2 	= $encrptopenssl->decrypt($sender_mail[0]['from_email']);					
					$sendername2  	= $setting_table_2[0]['field_1'];
					$contact_no2	= $encrptopenssl->decrypt($setting_table_2[0]['contact_no']);
					
					$challenge_desc 	= $this->master_model->getRecords("challenge", array("c_id" => $challengeId));
					$challengeNames 	= ucwords($encrptopenssl->decrypt($challenge_desc[0]['challenge_title']));
					$challenge_new_id 	= $challenge_desc[0]['challenge_id'];
					
					// Sender Details
					$s_full_name = $encrptopenssl->decrypt($senders_details[0]['title'])." ".$encrptopenssl->decrypt($senders_details[0]['first_name'])." ".$encrptopenssl->decrypt($senders_details[0]['middle_name'])." ".$encrptopenssl->decrypt($senders_details[0]['last_name']);
					$s_email_id = $encrptopenssl->decrypt($senders_details[0]['email']);
					//$s_email_id = 'vicky.kuwar@esds.co.in';
					$sender_fullname = ucwords($s_full_name);
					
					// Team Details 
					$teamName = ucwords($team_exist[0]['team_name']);
					
					//Link 
					$challengeProfile = base_url('challenge/challengeDetails/'.base64_encode($challengeId));
					$view			= '<a href="'.$challengeProfile.'" target="_blank">View Profile</a>';	
					
					$arr_words1 = ['[USERNAME]', '[TEAMNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[REQUESTSTATUSLINK]'];
					$rep_array1 = [$s_full_name,  $teamName,  $challengeNames,  $challenge_new_id,  $view];					
					$content_sender = str_replace($arr_words1, $rep_array1, $description_2);
					
					//Replace Subject
					$subArr = ['[CHALLENGENAME]'];
					$arrView = [$challengeNames];
					$subRep = str_replace($subArr, $arrView, $subject_2);
					
					$info_arr_1=array(
					'to'		=>	$s_email_id,//$o_email_id					
					'cc'		=>	'',
					'from'		=>	$from_admin_2,
					'subject'	=> 	$subRep."Sender mail",
					'view'		=>  'common-file'
					);
					
					$other_info_1=array('content'=>$content_sender); 					
					
					$emailsend_2 = $this->emailsending->sendmail($info_arr_1,$other_info_1);
					
				}
				
			}				
			
			$jsonData = array("token" => $csrf_test_name, "alert_text" => $text, "status" => $exist);
			echo json_encode($jsonData);
			
		}
		
		
		// Request Details
		public function requestDetails($id, $status){
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$challenge_id = base64_decode($id);
			$status 		= base64_decode($status);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			error_reporting(0);
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');
			
			
			// echo "<pre>";print_r($res_apply);die();
			$data['status_get']		= $status;
			$data['challenge_id']	= $challenge_id;
			$data['applicant_data'] = $res_apply;   
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/request';
			$this->load->view('front/front_combo',$data);
			
		}
		
		
		public function request_owner_details(){
			error_reporting(0);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$challenge_id = $this->input->post('challenge_id');		
			$check_status 			= @$this->input->post('check_status')?$this->input->post('check_status'):'';
			/*$techonogy_id 			= @$this->input->post('techonogy_id')?$this->input->post('techonogy_id'):'';
				$change_visibility 		= @$this->input->post('change_visibility')?$this->input->post('change_visibility'):'';
				$ip_clause 				= @$this->input->post('ip_clause')?$this->input->post('ip_clause'):'';
				$fund_sel 				= @$this->input->post('fund_sel')?$this->input->post('fund_sel'):'';
				$reward_sel 			= @$this->input->post('reward_sel')?$this->input->post('reward_sel'):'';
			$trl_id 				= @$this->input->post('trl_id')?$this->input->post('trl_id'):'';*/
			
			
			
			$user_id = $this->session->userdata('user_id');	
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			/*if(!empty($techonogy_id)){
				
				if(count((array)$techonogy_id) > 0){
				$c_data .= " AND (";
				$a =0;
				foreach($techonogy_id as $y){
				
				if($a!=(count($techonogy_id) - 1)){
				$c_data .= " FIND_IN_SET('".$y."',`technology_id`) OR ";
				} else {
				$c_data .= " FIND_IN_SET('".$y."',`technology_id`))";
				}				
				$a++;
				}
				
				}
				
				}
			*/
			
			if($check_status){
				
				if($check_status!="all"){
					$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
				} else {
					$c_data .= '';
				}	
				//$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}		
			
			$condition = "SELECT arai_registration.institution_type,arai_registration.institution_full_name,
			arai_registration.title, arai_registration.user_category_id, arai_registration.first_name,arai_registration.middle_name,
			arai_registration.last_name, arai_registration.email,arai_registration.user_id, 
			arai_challenge.c_id,arai_challenge_details_request.sender_id,arai_challenge_details_request.status,
			arai_challenge_details_request.request_id, arai_challenge_details_request.receiver_id,
			arai_registration_usercategory.user_category FROM arai_challenge_details_request 
			JOIN arai_registration ON arai_challenge_details_request.sender_id=arai_registration.user_id 
			JOIN arai_challenge ON arai_challenge_details_request.c_id=arai_challenge.c_id 
			JOIN arai_registration_usercategory ON arai_registration.user_category_id=arai_registration_usercategory.id 
			WHERE arai_registration.is_deleted = '0' AND arai_challenge_details_request.c_id = '".$challenge_id."'  ".$c_data."";			
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$institution_type 		= $encrptopenssl->decrypt($get_request->institution_type);
					$institution_full_name 	= $encrptopenssl->decrypt($get_request->institution_full_name);				
					//$close_date			= date('d-m-Y', strtotime($get_request->challenge_close_date));
					$title					= $encrptopenssl->decrypt($get_request->title);
					$first_name				= $encrptopenssl->decrypt($get_request->first_name);
					$middle_name			= $encrptopenssl->decrypt($get_request->middle_name);
					$last_name				= $encrptopenssl->decrypt($get_request->last_name);
					$email					= $encrptopenssl->decrypt($get_request->email);
					$user_category			= $encrptopenssl->decrypt($get_request->user_category);
					$u_id					= $get_request->user_id;
					$c_id					= $get_request->c_id;
					$request_id				= $get_request->request_id;
					$sender_id				= $get_request->sender_id;
					$status					= $get_request->status;
					$user_category_id		= $get_request->user_category_id;
					$fullname				= $title."&nbsp;".$first_name."&nbsp;".$middle_name."&nbsp;".$last_name;
					$receiver_id 			= $get_request->receiver_id;
					
					
					
					/*$contents = 'Pending'; 
						if($status == 'Approve'):
						$contents1 = 'Approve'; 
						//$status_text = "<i class='fa fa-thumbs-o-down' aria-hidden='true'></i>";
						endif;
						
						if($status == 'Reject'):
						$contents2 = 'Reject'; 
						//$status_text = "<i class='fa fa-thumbs-o-up' aria-hidden='true'></i>";
						endif;
						
						if($status == 'Withdrawn'):
						$contents3 = 'Withdrawn'; 
						//$status_text = "<i class='fa fa-thumbs-o-up' aria-hidden='true'></i>";
					endif;*/
					
					
					
					//$org_name 			= if($institution_full_name)?$institution_full_name:'';
					//$insti_type			= if($institution_type)?$institution_type:'';
					$withdraw = '';	
					//$e_link			= base_url('challenge/edit/'.base64_encode($get_request->c_id));
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					//$edit				= '<a href="'.$e_link.'"><i class="fa fa-edit" aria-hidden="true"></i></a>'; 
					//$view				= '<a href="'.$v_link.'"><i class="fa fa-eye" aria-hidden="true"></i></a>'; 
					$view				= '<a href="'.base_url('home/viewProfile/'.base64_encode($get_request->sender_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					/*if($user_id == $receiver_id){
						$withdraw			= '| <a href="javascript:void(0);" class="check-status" data-status="'.$contents.'" data-id="'.$get_request->request_id.'" data-toggle="tooltip" title="'.$status.'">'.$status_text.'</a>';
						} else if($user_id == $sender_id){
						$withdraw			= '| <a href="javascript:void(0);" class="check-status" data-status="Reject" data-id="'.$get_request->request_id.'" data-toggle="tooltip" title="Reject">'.$status_text.'</a>';
					}*/
					
					if($user_id == $receiver_id){
						if($status == 'Pending'){
							
							if($status == 'Pending'): $contents = "selected='selected'";endif;
							if($status == 'Approved'): $contents1 = "selected='selected'";endif;
							if($status == 'Rejected'): $contents2 = "selected='selected'";endif;
							
							$dropdown = '<span id="remove-'.$get_request->request_id.'"><select name="req_status" id="req_status" data-id="'.$get_request->request_id.'" class="check-status" onChange="return setRequest('.$get_request->request_id.', this.value);">
							<option value="Pending" '.$contents.'>Pending</option>
							<option value="Approved" '.$contents1.'>Approved</option>
							<option value="Rejected" '.$contents2.'>Rejected</option>
							</select></span><span id="add-'.$get_request->request_id.'"></span>';
							} else {
							
							$dropdown = $status;
						}
						
						} else {
						
						$dropdown = $status;
					}			
					
					$conCat				= $view."&nbsp;".$withdraw;
					
					
					
					// Receiver Details 
					$receiver_detail 	= $this->master_model->getRecords("registration", array("user_id" => $get_request->receiver_id));
					$r_fn = $encrptopenssl->decrypt($receiver_detail[0]['first_name']);
					$r_mn = $encrptopenssl->decrypt($receiver_detail[0]['middle_name']);
					$r_ln = $encrptopenssl->decrypt($receiver_detail[0]['last_name']);
					$e_ln = $encrptopenssl->decrypt($receiver_detail[0]['email']);
					$r_name = ucfirst($r_fn)." ".ucfirst($r_mn)." ".ucfirst($r_ln);
					
					
					
					// Sender Details 
					$sender_detail 	= $this->master_model->getRecords("registration", array("user_id" => $get_request->sender_id));
					$fn = $encrptopenssl->decrypt($sender_detail[0]['first_name']);
					$mn = $encrptopenssl->decrypt($sender_detail[0]['middle_name']);
					$ln = $encrptopenssl->decrypt($sender_detail[0]['last_name']);
					$email_n = $encrptopenssl->decrypt($sender_detail[0]['email']);
					
					$sender_name = ucfirst($fn)." ".ucfirst($mn)." ".ucfirst($ln);	
					
					if($user_category_id == 1){
						//$f_name = ucfirst($fn)." ".ucfirst($mn)." ".ucfirst($ln);
						$f_name = '-';
					} else {
						$f_name = ucwords($institution_full_name);
					}
					
					$i++;
					
					$dataArr[] = array(
					$i,
					$sender_name,
					$email_n,
					$user_category,	
					$f_name,
					//$institution_type,
					$dropdown
					);
					
					//,$conCat
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);		
			
		}
		
		// Request Details
		public function applicationReceived(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$user_id = $this->session->userdata('user_id');		 
		  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/application-received';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function getRejectionlist(){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$rejectionlist = $this->master_model->getRecords("challenge_rejection", array('status' => 'Active'));
			
			if(count($rejectionlist) > 0){
				$rejectionArr = '<select name="rejection_id" id="rejection_id" class="rejection-card form-control">';
				
				foreach($rejectionlist as $rejection){
					
					$printOptin = $encrptopenssl->decrypt($rejection['reject_reason']);	
					
					$rejectionArr .= '<option value="'.stripslashes($rejection['id']).'">'.stripslashes($printOptin).'</option>';
					
				}
				
				$rejectionArr .= '<option value="other">Other</option>
				</select>';
				
				} else {
				
				$rejectionArr .= '<select name="rejection_id" id="rejection_id" class="rejection-card form-control">
				<option value=""> -- Select --</option>
				</select>';
			}
			
			$jsonData = array("token" => $csrf_test_name, "select_box" => $rejectionArr);
			echo json_encode($jsonData, JSON_UNESCAPED_SLASHES);
		}
		
		// Withdrawn Reason 
		public function getWithdrawnlist(){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$rejectionlist = $this->master_model->getRecords("challenge_withdrawal", array('status' => 'Active'));
			
			if(count($rejectionlist) > 0){
				$rejectionArr = '<select name="withdraw_id" id="withdraw_id" class="withdraw-card form-control">';
				
				foreach($rejectionlist as $rejection){
					
					$printOptin = $encrptopenssl->decrypt($rejection['reason']);	
					
					$rejectionArr .= '<option value="'.stripslashes($rejection['id']).'">'.stripslashes($printOptin).'</option>';
					
				}
				
				$rejectionArr .= '<option value="other">Other</option>
				</select>';
				
				} else {
				
				$rejectionArr .= '<select name="withdraw_id" id="withdraw_id" class="withdraw-card form-control">
				<option value=""> -- Select --</option>
				</select>';
			}
			
			$jsonData = array("token" => $csrf_test_name, "select_box" => $rejectionArr);
			echo json_encode($jsonData, JSON_UNESCAPED_SLASHES);
		}
		
		
		public function changeRequestStatus(){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			//$csrf_test_name = null;
			$id 		= $this->input->post('id');
			$status 	= $this->input->post('status');
			$r_id 		= $this->input->post('r_id');
			$r_reason 	= $this->input->post('r_reason');
			$user_id 	= $this->session->userdata('user_id');	
			$updateAt	= date('Y-m-d H:i:s');		
			
			$updateArr = array(
			'updated_by_id' => $user_id,
			'updatedAt' => $updateAt,
			'status' 	=> $status
			);	
			
			$this->db->join('arai_registration r','r.user_id = c.u_id','INNER',FALSE);
			$this->db->where("r.is_deleted","0");
			$checkUser = $this->master_model->getRecords("challenge c", array("c.u_id" => $user_id));
			$ownerID = $checkUser[0]['u_id'];
			
			
			if($ownerID == $user_id){
				
				$updateQuery = $this->master_model->updateRecord('challenge_details_request',$updateArr,array('request_id' => $id));
				
				$postArr			= $this->input->post();
				$insertLog 			= array_merge($postArr,$updateArr);
				$json_encode_data 	= json_encode($insertLog);
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Challenge Owner Request Status Approve/Reject",
				'module_name'	=> 'Challenge',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$checkRes = $this->master_model->getRecords("challenge_details_request", array("request_id" => $id));
				
				// Rejected Status
				if($status == 'Rejected'){
					
					// Challenge ID
					$challengeId 	= $checkRes[0]['c_id'];
					$sender_id 		= $checkRes[0]['sender_id'];
					$insertArr 		= array('c_id' => $challengeId, 'user_id' => $user_id, 'rejection_id' => $r_id, 'owner_reason' => $r_reason);
					
					$checkExist = $this->master_model->getRecords("owner_challenge_rejection", array("c_id" => $challengeId, 'user_id' => $user_id));
					if(count($checkExist) > 0){
						
						$updatedAt = date('Y-m-d H:i:s');
						$updatedArr 		= array('rejection_id' => $r_id, 'owner_reason' => $r_reason, 'updatedAt' => $updatedAt);
						$updateQuery = $this->master_model->updateRecord('owner_challenge_rejection',$updatedArr,array('c_id' => $challengeId, 'user_id' => $user_id));
						
								$postArr			= $this->input->post();
								$insertLog 			= array_merge($postArr,$updatedArr);
								$json_encode_data 	= json_encode($insertLog);
								$ipAddr			  	= $this->get_client_ip();
								$createdAt			= date('Y-m-d H:i:s');
								$logDetails 		= array(
														'user_id' 		=> $user_id,
														'action_name' 	=> "Challenge Owner Request Status Approve/Reject",
														'module_name'	=> 'Challenge',
														'store_data'	=> $json_encode_data,
														'ip_address'	=> $ipAddr,
														'createdAt'		=> $createdAt
													);
								$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						} else {
						
						$insertArr 		= array('c_id' => $challengeId, 'user_id' => $user_id, 'rejection_id' => $r_id, 'owner_reason' => $r_reason);
						$this->master_model->insertRecord('owner_challenge_rejection',$insertArr);
						
						$postArr			= $this->input->post();
						$insertLog 			= array_merge($postArr,$insertArr);
						$json_encode_data 	= json_encode($insertLog);
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
													'user_id' 		=> $user_id,
													'action_name' 	=> "Challenge Owner Request Status Approve/Reject",
													'module_name'	=> 'Challenge',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr,
													'createdAt'		=> $createdAt
											);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						
						
						// Request Rejected Email 						
						$slug = $encrptopenssl->encrypt('challenge_owner_declined_private_request');
						$challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
						$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
						$contact_no1	= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						$content = '';
						
						if(count($challenge_mail) > 0){
							
							$subject 	 	= $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
							$description 	= $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
							$from_admin 	= $encrptopenssl->decrypt($challenge_mail[0]['from_email']);				
							$from_names 	= 'Admin';
							$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
							$emailAdmin  	= $encrptopenssl->decrypt($setting_table[0]['field_2']);
							
							$updatedStatus = $this->master_model->getRecords("challenge", array('c_id' => $challengeId));
							$chnames = $encrptopenssl->decrypt($updatedStatus[0]['challenge_title']);
							$challenge_id_get = $updatedStatus[0]['challenge_id'];
							
							$senderDetails = $this->master_model->getRecords("registration", array('user_id' => $sender_id));								
							$fullname 	= $encrptopenssl->decrypt($senderDetails[0]['first_name'])." ".$encrptopenssl->decrypt($senderDetails[0]['middle_name'])." ".$encrptopenssl->decrypt($senderDetails[0]['last_name']);
							$full_cap 	= ucwords($fullname);
							$senderEmail = $encrptopenssl->decrypt($senderDetails[0]['email']);
							//$senderEmail = 'vicky.kuwar@esds.co.in';
							// Challenge Owner Details
							$user_id 	= $this->session->userdata('user_id');	
							$ownerDetails = $this->master_model->getRecords("registration", array('user_id' => $user_id));
							$ownerEmail = $encrptopenssl->decrypt($ownerDetails[0]['email']);
							// Team Details 
							$teamDetails = $this->master_model->getRecords("byt_teams", array('user_id' => $sender_id, 'c_id' => $challengeId));	
							$teamName = ucwords($teamDetails[0]['team_name']);
							
							// Status Link 
							$urlBuild = base_url('challenge/requestDetails/'.base64_encode($challengeId).'/'.base64_encode('Rejected'));
							$buildLink = '<a href="'.$urlBuild.'">View</a>';
							$arr_words = ['[USERNAME]', '[TEAMNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[REQUESTSTATUSLINK]'];
							$rep_array = [$full_cap, $teamName, $chnames,  $challenge_id_get, $buildLink];
							
							$content = str_replace($arr_words, $rep_array, $description);
							
							$admin_data = $this->master_model->getRecords("user_master", array("user_id" => '38'));
							//$adminEmail = 'sys.tip@Technovuus.araiindia.com';
							//$adminEamil = $encrptopenssl->decrypt($admin_data[0]['admin_email']);
							$adminEmail = $emailAdmin;
							
							
							// Replace Subject 
							$arr_sub = ['[CHALLENGENAME]'];
							$rep_sub = [$chnames];
							$sub_rep = str_replace($arr_sub, $rep_sub, $subject);
							
							//email admin sending code 
							$info_arr=array(
									'to'		=>	$senderEmail, //'vicky.kuwar@esds.co.in',	
									'cc'		=>	'',
									'from'		=>	$ownerEmail,
									'subject'	=> 	$sub_rep,
									'view'		=>  'common-file'
									);
							
							$other_info=array('content'=>$content); 
							
							$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
							
						} // Slug End
						
					} // Else Part
					
				} // Rejected Status End
				
					if($checkRes[0]['status'] == 'Approved')
					{
					
						$changeText = 'Approved';	
					
						// Request Sender Details
						if(count($checkUser) > 0){
							
							// Request Accepted Email To Challenger From Owner
							$slug 		 		= $encrptopenssl->encrypt('challenge_owner_accepted_private_request');		
							$sender_mail 		= $this->master_model->getRecords("email_template", array("slug" => $slug));
							$setting_table_1 	= $this->master_model->getRecords("setting", array("id" => '1'));
							$content_sender 	= '';

							$subject 	 	= $encrptopenssl->decrypt($sender_mail[0]['email_title']);
							$description 	= $encrptopenssl->decrypt($sender_mail[0]['email_description']);
							$from_admin 	= $encrptopenssl->decrypt($sender_mail[0]['from_email']);					
							$sendername  	= $setting_table_1[0]['field_1'];
							$contact_no		= $encrptopenssl->decrypt($setting_table_1[0]['contact_no']);
								
							
							// Challenge ID
							$challengeId 		= $checkRes[0]['c_id'];					
							$challenge_desc 	= $this->master_model->getRecords("challenge", array("c_id" => $challengeId));
							$challengeNames 	= $encrptopenssl->decrypt($challenge_desc[0]['challenge_title']);
							$challenge_next_id 	= $challenge_desc[0]['challenge_id'];
							
							// Sender Details
							$receiverId			= $checkRes[0]['sender_id'];
							$receiver_detail 	= $this->master_model->getRecords("registration", array("user_id" => $receiverId));						
							$r_full_name = $encrptopenssl->decrypt($receiver_detail[0]['title'])." ".$encrptopenssl->decrypt($receiver_detail[0]['first_name'])." ".$encrptopenssl->decrypt($receiver_detail[0]['middle_name'])." ".$encrptopenssl->decrypt($receiver_detail[0]['last_name']);
							$s_email_id = $encrptopenssl->decrypt($checkRes[0]['email']);
							//$s_email_id = 'vicky.kuwar@esds.co.in';
							$receiver_fullname = ucwords($r_full_name);
							
							// Challenge Owner Details
							$user_id 	= $this->session->userdata('user_id');	
							$ownerDetails = $this->master_model->getRecords("registration", array('user_id' => $user_id));
							$ownerEmail = $encrptopenssl->decrypt($ownerDetails[0]['email']);
							
							$urlBuild = base_url('challenge/challengeDetails/'.base64_encode($challengeId));
							$buildLink = '<a href="'.$urlBuild.'">View</a>';
							
							
							$arr_words = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[CHALLENGEDETAILLINK]'];
							$rep_array = [$receiver_fullname, $challengeNames,  $challenge_next_id, $buildLink];						
							$content_sender = str_replace($arr_words, $rep_array, $description);
							
							// Replace Subject 
							$arr_sub = ['[CHALLENGENAME]'];
							$rep_sub = [$challengeNames];
							$sub_rep = str_replace($arr_sub, $rep_sub, $subject);
							
							
							$info_arr = array(
							'to'		=>	$s_email_id,					
							'cc'		=>	'',
							'from'		=>	$ownerEmail,
							'subject'	=> 	$sub_rep,
							'view'		=>  'common-file'
							);
							
							$other_info	=	array('content'=>$content_sender); 					
							
							$emailsend = $this->emailsending->sendmail($info_arr,$other_info);
							
						} // Email End
					
					} else if($checkRes[0]['status'] == 'Rejected'){
					
						$changeText = 'Rejected';
					
					} else {
					
						$changeText = 'Pending';
					}
				
					$jsonData = array("token" => $csrf_test_name, "change_text" => $changeText);	
				
				} else {
					
				$changeText = 'Sorry, You have not access for this action';
				$jsonData = array("token" => $csrf_test_name, "change_text" => $changeText);
			}
			
			echo json_encode($jsonData);
			
		}
		
		// Add Withdrawn Reason
		public function addWithdrawnReason(){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			//$csrf_test_name = null;
			$cid 		= $this->input->post('c_id');
			$w_id 		= $this->input->post('w_id');
			$w_reason 	= $this->input->post('w_reason');
			$user_id 	= $this->session->userdata('user_id');	
			$updateAt	= date('Y-m-d H:i:s');
			
			// Get Challenge Status 
			$getStats = $this->master_model->getRecords("challenge", array('c_id' => $cid));
			$getOldStatus = $getStats[0]['challenge_status'];
			
			// Update Status
			$updateArr = array('challenge_status' => 'Withdrawn', 'updatedAt' => $updateAt, 'updated_by_id' => $user_id, 'last_updated_date' => date('Y-m-d'));
			$updateQuery = $this->master_model->updateRecord('challenge',$updateArr,array('c_id' => $cid));
			
			// Add Withdrawn Reason
			$insertArr 		= array(
			'user_id' 			=> $user_id,
			'c_id' 				=> $cid,
			'w_reason_id'		=> $w_id,
			'reason_details'	=> $w_reason
			);
			$this->master_model->insertRecord('challenge_owner_withdrawn',$insertArr);
			
			//Challenge Withdrawn mail to ADMIN Updated
			$slug = $encrptopenssl->encrypt('challenge_withdrawn_mail_to_admin');
			$challenge_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table  = $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_no1	= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
			$content = '';
			
			if(count($challenge_mail) > 0){	
				$chID = $this->input->post('c_id');
				$reasonStore = $this->input->post('w_reason');
				$w_id 		= $this->input->post('w_id');
				
				$withdrawReason 	= $this->master_model->getRecords("challenge_withdrawal", array('id' => $w_id));
				$withdraw_content 	= ucfirst($encrptopenssl->decrypt($withdrawReason[0]['reason']));
				
				if($reasonStore){
					$addedContent = "<p><b>".$withdraw_content."</b></p><p>".$reasonStore."</p>";
				} else {
					$addedContent = "<p><b>".$withdraw_content."</b></p>";
				}
				
				$subject 	 	= $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($challenge_mail[0]['from_email']);				
				$from_names 	= 'Admin';
				$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$emailAdmin  	= $encrptopenssl->decrypt($setting_table[0]['field_2']);
				
				$updatedStatus = $this->master_model->getRecords("challenge", array('c_id' => $chID));
				$newStatus = $updatedStatus[0]['challenge_status'];
				$u_idget = $updatedStatus[0]['u_id'];
				$chnames = $encrptopenssl->decrypt($updatedStatus[0]['challenge_title']);
				$challenge_id_get = $updatedStatus[0]['challenge_id'];
				
				$ownerDetails = $this->master_model->getRecords("registration", array('user_id' => $u_idget));
				
				$fullname 	= $encrptopenssl->decrypt($ownerDetails[0]['first_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['middle_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['last_name']);
				$full_cap 	= ucwords($fullname);
				$ownerEmail = $encrptopenssl->decrypt($ownerDetails[0]['email']);				
				
				$getActiveCount = $this->master_model->getRecords("challenge", array('u_id' => $user_id, 'challenge_status' => 'Approved'));
				$activeCount = count($getActiveCount);
				$arr_words = ['[CHALLENGENAME]', '[CHALLENGEID]', '[WITHDRAWNREASON]'];
				$rep_array = [$chnames,  $challenge_id_get, $addedContent];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
				$admin_data = $this->master_model->getRecords("user_master", array("user_id" => '38'));
				//$adminEmail = 'sys.tip@Technovuus.araiindia.com';
				//$adminEamil = $encrptopenssl->decrypt($admin_data[0]['admin_email']);
				$adminEmail = $emailAdmin;
				
				
				// Replace Subject 
				$arr_sub = ['[CHALLENGEID]'];
				$rep_sub = [$challenge_id_get];
				$sub_rep = str_replace($arr_sub, $rep_sub, $subject);
				
				//email admin sending code 
				$info_arr=array(
						'to'		=>	$adminEmail, //$adminEmail,		//'vicky.kuwar@esds.co.in',	//		
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$sub_rep,
						'view'		=>  'common-file'
						);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
			}				

			//Challenge Withdrawn mail to Applicant Updated
			$slug2 = $encrptopenssl->encrypt('email_team_owner_withdrawn_challenge');
			$applicant_mail = $this->master_model->getRecords("email_template", array("slug" => $slug2));
			
			$setting_table2  = $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_no2	= $encrptopenssl->decrypt($setting_table2[0]['contact_no']);
			$content2 = '';
			
			if(count($applicant_mail) > 0){	
				$chID = $this->input->post('c_id');	
				$subject2 	 	= $encrptopenssl->decrypt($applicant_mail[0]['email_title']);
				$description2 	= $encrptopenssl->decrypt($applicant_mail[0]['email_description']);
				$from_admin2 	= $encrptopenssl->decrypt($applicant_mail[0]['from_email']);		
				
				$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				
				$generatelink = base_url('challenge/challengeDetails/'.base64_encode($chID));
				
				$link = '<a href="'.$generatelink.'" target="_blank">View Challenge</a>';
				
				$ChallengeData = $this->master_model->getRecords("challenge", array('c_id' => $chID));
				$u_id_get = $ChallengeData[0]['u_id'];
				$chnames = $encrptopenssl->decrypt($ChallengeData[0]['challenge_title']);
				$challenge_id_get = $ChallengeData[0]['challenge_id'];
				
				$ownerDetails = $this->master_model->getRecords("registration", array('user_id' => $u_id_get));
				$fullname 	= $encrptopenssl->decrypt($ownerDetails[0]['first_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['middle_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['last_name']);
				$full_cap 	= ucwords($fullname);
				$ownerEmail = $encrptopenssl->decrypt($ownerDetails[0]['email']);
				
				$reasonStore = $this->input->post('w_reason');
				$w_id 		= $this->input->post('w_id');
				
				$withdrawReason 	= $this->master_model->getRecords("challenge_withdrawal", array('id' => $w_id));
				$withdraw_content 	= ucfirst($encrptopenssl->decrypt($withdrawReason[0]['reason']));
				
				if($reasonStore){
					$addedContent = "<p><b>".$withdraw_content."</b></p><p>".$reasonStore."</p>";
				} else {
					$addedContent = "<p><b>".$withdraw_content."</b></p>";
				}
				
				
				$arr_words2 = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[WITHDRAWNREASON]'];
				$rep_array2 = [$full_cap,  $chnames,  $challenge_id_get,  $addedContent];
				
				$content2 = str_replace($arr_words2, $rep_array2, $description2);
				
				//email admin sending code 
				$info_arr2=array(
						'to'		=>	$ownerEmail, //$ownerEmail,					
						'cc'		=>	'',
						'from'		=>	$from_admin2,
						'subject'	=> 	$subject2,
						'view'		=>  'common-file'
						);
				
				$other_info2=array('content'=>$content2); 
				
				$emailsend=$this->emailsending->sendmail($info_arr2,$other_info2);
				
			}
			
			
			// Team Deactivated
			$chID = $this->input->post('c_id');
			$team_details = $this->master_model->getRecords("byt_teams", array("c_id" => $chID));
			
			$updateAt	= date('Y-m-d H:i:s');
			$updateArr2 = array('status' => 'Block', 'modified_on' => $updateAt);
			$updateQuery2 = $this->master_model->updateRecord('byt_teams',$updateArr2,array('c_id' => $chID));
			
			if(count($team_details) > 0){
				
				foreach($team_details as $teamDeatils){
					
					// Challenge Data
					$challenge_Data = $this->master_model->getRecords("challenge", array('c_id' => $chID));
					$u_idget 		= $challenge_Data[0]['u_id'];
					$challenge_id 	= $challenge_Data[0]['challenge_id'];
					$challenge_title = $encrptopenssl->decrypt($challenge_Data[0]['challenge_title']);
					
					// Team Data
					$teamName 		= $teamDeatils['team_name'];
					$team_owner_id 	= $teamDeatils['user_id'];
					
					// Registration  Data
					$ownerDetails = $this->master_model->getRecords("registration", array('user_id' => $team_owner_id));				
					if($ownerDetails[0]['user_category_id'] == 1){
						
						$fullname 	= $encrptopenssl->decrypt($ownerDetails[0]['first_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['middle_name'])." ".$encrptopenssl->decrypt($ownerDetails[0]['last_name']);
						$full_cap 	= ucwords($fullname);
						
						
					} else {
						
						$fullname 	= ucwords($encrptopenssl->decrypt($ownerDetails[0]['institution_full_name']));
						
					}
					
					$ownerEmail = $encrptopenssl->decrypt($ownerDetails[0]['email']);
					
					
					$slug3 = $encrptopenssl->encrypt('email_to_team_owner_withdrawn_challenge');
					$team_mail = $this->master_model->getRecords("email_template", array("slug" => $slug3));
					
					$setting_table3  = $this->master_model->getRecords("setting", array("id" => '1'));
					$contact_no3	= $encrptopenssl->decrypt($setting_table3[0]['contact_no']);
					$content3 = '';
					
					if(count($team_mail) > 0){	
					
							$chID = $this->input->post('c_id');	
							$subject3 	 	= $encrptopenssl->decrypt($team_mail[0]['email_title']);
							$description3 	= $encrptopenssl->decrypt($team_mail[0]['email_description']);
							$from_admin3 	= $encrptopenssl->decrypt($team_mail[0]['from_email']);		
							
							$sendername3  	= $encrptopenssl->decrypt($setting_table3[0]['field_1']);
							
							
							$w_id 		= $this->input->post('w_id');
							$w_reason 	= $this->input->post('w_reason');
							
							// challenge_owner_withdrawn
							if($w_id == 'other'){
								
								$resonContent = 'Other';
								$reasonsetails = $w_reason;
								
							} else {
								
								$getReason  = $this->master_model->getRecords("challenge_withdrawal", array("id" => $w_id));
								$reasonsetails = $encrptopenssl->decrypt($getReason[0]['reason']);
								
							}
							
							$arr_words3 = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[WITHDRAWNREASON]'];
							$rep_array3 = [$fullname,  $challenge_title,  $challenge_id,  $reasonsetails];
							
							$content3 = str_replace($arr_words3, $rep_array3, $description3);
							
							
							$subArr = ['[CHALLENGEID]'];
							$replArr = [$challenge_id];
							$subCon = str_replace($subArr, $replArr, $subject3);
							
							//email admin sending code 
							$info_arr3=array(
									'to'		=>	$ownerEmail, //$ownerEmail,					
									'cc'		=>	'',
									'from'		=>	$from_admin3,
									'subject'	=> 	$subCon,
									'view'		=>  'common-file'
									);
							
							$other_info3=array('content'=>$content3); 
							
							$emailsend=$this->emailsending->sendmail($info_arr3,$other_info3);
							
						}
					
				}
			}
			
			
			////////////////////////////////////
			
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Log Details
			$logDetails 		= array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Challenge Withdrawn Reason",
			'module_name'	=> 'Challenge',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			$changeText = 'Withdrawn';
			$jsonData = array("token" => $csrf_test_name, "change_text" => $changeText);
			echo json_encode($jsonData);
			
		}
		
		public function open_modal_ajax()
		{
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			$cid = $this->input->post('cid');
			$rejectionlist = $this->master_model->getRecords("challenge_withdrawal", array('status' => 'Active'));
			$arrayReason = array();
			foreach($rejectionlist as $reason){
				$reason['reason'] = $encrptopenssl->decrypt($reason['reason']);
				$arrayReason[] = $reason;
			}
			$data['cid'] = $cid;
			$data['withdraw_reason'] = 	$arrayReason;
			$this->load->view('front/challenge/modal_form', $data);
		}
		
		// BYT Application Listing 
		public function applicationListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			if(count($challengeDetails)){
				
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/application-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Application Listing 
		public function application_listing(){
			
			error_reporting(0);
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			$condition = "	SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status, 
			arai_registration.institution_full_name, arai_registration.user_category_id, arai_registration.title, arai_registration.first_name,arai_registration.middle_name, arai_registration.last_name FROM arai_byt_teams
			JOIN arai_registration ON arai_byt_teams.user_id=arai_registration.user_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."'  ".$c_data."";			
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$title					= $encrptopenssl->decrypt($get_request->title);
					$first_name				= $encrptopenssl->decrypt($get_request->first_name);
					$middle_name			= $encrptopenssl->decrypt($get_request->middle_name);
					$last_name				= $encrptopenssl->decrypt($get_request->last_name);
					$fullnames				= ucwords($title)." ".ucwords($first_name)." ".ucwords($middle_name)." ".ucwords($last_name);
					$categoryID				= $get_request->user_category_id;
					$cmp_name				= $encrptopenssl->decrypt($get_request->institution_full_name);
					
					$getCatSubCat = $this->master_model->getRecords("registration_usercategory", array('id' => $categoryID));
					$categoryName = $encrptopenssl->decrypt($getCatSubCat[0]['user_category']);
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('challenge/show_team_profile/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name		= $team_id." - ".$team_name;	
					
					$conCat				= $view."&nbsp;".$withdraw;
					
					/*if($categoryID == 1){
						$getCompDetails = $this->master_model->getRecords("student_profile", array('user_id' => $creator_user_id));
						$companyName = ucwords($encrptopenssl->decrypt($getCompDetails[0]['company_name']));
					}*/
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$i,
					$categoryName,
					$companyName,
					$teamid_name,	
					$fullnames,
					$team_status,
					$team_size,
					$challenge_owner_status,
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		} // Team Listing ENd
		
		// BYT My Teams Listing 
		public function myTeams(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/my-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// My Created Team Listing
		public function myTeamsListing(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			// Login User
			$user_id = $this->session->userdata('user_id');	
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.skills, arai_byt_team_slots.role_name, arai_byt_team_slots.slot_id 
			FROM arai_byt_teams 
			JOIN arai_byt_team_slots ON arai_byt_teams.c_id=arai_byt_team_slots.c_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.user_id = '".$user_id."' GROUP BY arai_byt_teams.team_id ".$c_data."";
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('challenge/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					$chllenge_code_name		= $category_id." - ".$chName;	
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,	
					$available_Slot,
					$hiddenDiv,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		
		// BYT My Teams Listing 
		public function appliedTeams(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/applied-team-listing';
			$this->load->view('front/front_combo',$data);
			
		}
		
		public function appliedChallengeListing(){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();	
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,arai_byt_slot_applications.status as applicationStatus,
			arai_byt_slot_applications.apply_user_id, arai_byt_slot_applications.if_agree, arai_byt_slot_applications.introduce_urself, arai_byt_slot_applications.app_id 
			FROM arai_byt_slot_applications 
			JOIN arai_byt_teams ON arai_byt_slot_applications.apply_user_id=arai_byt_teams.user_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_slot_applications.is_deleted = '0' AND arai_byt_slot_applications.apply_user_id = '".$user_id."' ".$c_data."";
			
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			//echo "<pre>";print_r($result);die();
			$rowCount = $this->getNumData($condition);	
			
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					$challenge_owner_status	= $get_request->challenge_owner_status;
					$team_status			= $get_request->team_status;
					$myAppstatus			= $get_request->applicationStatus;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = $encrptopenssl->decrypt($getChallenge[0]['challenge_title']);
					$category_id  = $getChallenge[0]['category_id'];
					
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved'));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('challenge/team_owner_view/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $team_id." - ".$team_name;
					$chllenge_code_name		= $category_id." - ".$chName;
					$refId = "";
					
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$refId,
					$myAppstatus,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,
					$team_status,
					$challenge_owner_status,
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}
		
		
		public function teamListing($challengeID){
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			if($challengeID == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Login User
			$user_id = $this->session->userdata('user_id');		 
			
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();		
			
			// Decode ID
			$c_id = base64_decode($challengeID);
			
			$ch_details = array();
			
			// GET Challenge Details
			$challengeDetails = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
			
			if(count($challengeDetails))
			{
				if($user_id != $challengeDetails[0]['u_id']){
			 	redirect(base_url('challenge/myChallenges'));
				}
				foreach($challengeDetails as $row_val){
					
					$getCID = $row_val['c_id'];
					
					$eligibility 		= $this->master_model->getRecords("eligibility_expectation", array("c_id" => $getCID));
					
					$challenge_contact  = $this->master_model->getRecords("challenge_contact", array("c_id" => $getCID));
					
					
					// Domain Master
					$domainIds = $eligibility[0]['domain'];
					// Geography Master
					$geographyMaster = $eligibility[0]['geographical_states'];				
					
					$row_val['challenge_title'] 		= $encrptopenssl->decrypt($row_val['challenge_title']);
					$row_val['banner_img'] 				= $encrptopenssl->decrypt($row_val['banner_img']);
					$row_val['company_name'] 			= $encrptopenssl->decrypt($row_val['company_name']);
					$row_val['company_profile'] 		= $encrptopenssl->decrypt($row_val['company_profile']);	
					$row_val['company_logo'] 			= $encrptopenssl->decrypt($row_val['company_logo']);
					$row_val['challenge_details'] 		= $encrptopenssl->decrypt($row_val['challenge_details']);
					$row_val['challenge_abstract'] 		= $encrptopenssl->decrypt($row_val['challenge_abstract']);
					$row_val['challenge_launch_date'] 	= date('d-m-Y', strtotime($row_val['challenge_launch_date']));
					$row_val['challenge_close_date'] 	= date('d-m-Y', strtotime($row_val['challenge_close_date']));
					$row_val['technology_id'] 			= $row_val['technology_id'];
					$row_val['other_techonology'] 		= $encrptopenssl->decrypt($row_val['other_techonology']);
					$row_val['tags_id'] 				= $row_val['tags_id'];
					$row_val['added_tag_name'] 			= $encrptopenssl->decrypt($row_val['added_tag_name']);
					$row_val['audience_pref_id']		= $row_val['audience_pref_id'];
					$row_val['other_audience'] 			= $encrptopenssl->decrypt($row_val['other_audience']);
					$row_val['if_funding'] 	    		= $row_val['if_funding'];
					$row_val['is_amount'] 	    		= $encrptopenssl->decrypt($row_val['is_amount']);
					$row_val['if_reward'] 	    		= $row_val['if_reward'];
					$row_val['fund_reward'] 	    	= $encrptopenssl->decrypt($row_val['fund_reward']);
					$row_val['terms_txt'] 	    		= $encrptopenssl->decrypt($row_val['terms_txt']);
					$row_val['trl_id'] 	    			= $encrptopenssl->decrypt($row_val['trl_solution']);
					$row_val['is_agree'] 	    		= $row_val['is_agree'];
					$row_val['contact_person_name'] 	= $encrptopenssl->decrypt($row_val['contact_person_name']);
					$row_val['email_id'] 				= $encrptopenssl->decrypt($challenge_contact[0]['email_id']);
					$row_val['mobile_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['mobile_no']);
					$row_val['office_no'] 				= $encrptopenssl->decrypt($challenge_contact[0]['office_no']);
					$row_val['share_details'] 	    	= $row_val['share_details'];
					$row_val['challenge_visibility'] 	= $row_val['challenge_visibility'];
					$row_val['future_opportunities'] 	= $encrptopenssl->decrypt($row_val['future_opportunities']);
					$row_val['domain_id'] 	    		= $domainIds;
					$row_val['geographical_id'] 		= $geographyMaster;
					$row_val['education'] 				= $encrptopenssl->decrypt($eligibility[0]['education']);
					$row_val['from_age'] 				= $encrptopenssl->decrypt($eligibility[0]['from_age']);
					$row_val['to_age'] 					= $encrptopenssl->decrypt($eligibility[0]['to_age']);
					$row_val['min_team'] 				= $encrptopenssl->decrypt($eligibility[0]['min_team']);
					$row_val['max_team'] 				= $encrptopenssl->decrypt($eligibility[0]['max_team']);
					$row_val['ip_clause'] 				= $row_val['ip_clause'];
					$row_val['external_fund_details']	= $encrptopenssl->decrypt($row_val['external_fund_details']);
					$row_val['exclusive_challenge_details']	= $encrptopenssl->decrypt($row_val['exclusive_challenge_details']);
					
					$ch_details[] = $row_val;
					
				}
				
				} else {
				redirect(base_url('challenge/myChallenges'));
			}
			
			$data['challange_details'] 	 = $ch_details;
			$data['c_id'] 	     	 = $c_id;  
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/team-listing';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Filter Data Team Listing 
		public function team_listing(){
			
			error_reporting(E_ALL);
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Challenge ID
			$c_id = $this->input->post('c_id');		
			
			$user_id = $this->session->userdata('user_id');	
			
			
			$draw = $_POST['draw'];
			$row = $_POST['start'];
			$rowperpage = $_POST['length']; // Rows display per page
			$columnIndex = $_POST['order'][0]['column']; // Column index
			$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
			$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
			$searchValue = $_POST['search']['value']; // Search value
			$pageNo = ($_POST['start']/$_POST['length'])+1;		
			
			$c_data = "";
			
			if($check_status){
				
				$c_data .= " AND arai_challenge_details_request.status = '".$check_status."'";
			}	
			
			
			$condition = "SELECT arai_byt_teams.team_id, arai_byt_teams.custom_team_id, arai_byt_teams.c_id, arai_byt_teams.user_id as team_creater_user_id, arai_byt_teams.team_name, arai_byt_teams.apply_status,
			arai_byt_teams.team_banner, arai_byt_teams.team_size, arai_byt_teams.brief_team_info, arai_byt_teams.proposed_approach, 
			arai_byt_teams.challenge_owner_status, arai_byt_teams.post_challenge_user_id, arai_byt_teams.team_status,
			arai_byt_team_slots.slot_type, arai_byt_team_slots.skills, arai_byt_team_slots.role_name, arai_byt_team_slots.slot_id 
			FROM arai_byt_teams 
			JOIN arai_byt_team_slots ON arai_byt_teams.c_id=arai_byt_team_slots.c_id 
			WHERE arai_byt_teams.is_deleted = '0' AND arai_byt_teams.c_id = '".$c_id."'  GROUP BY arai_byt_teams.team_id ".$c_data."";//AND arai_byt_teams.apply_status = 'Applied'
			$query = $condition.' LIMIT  '.$this->input->post('length').' OFFSET '.$this->input->post('start');
			
			$result 	= $this->db->query($query)->result();			
			
			$rowCount = $this->getNumData($condition);	
			
			
			$rowCnt = 0;
			$dataArr = array();
			
			if($rowCount > 0){
				
				$no    = $_POST['start'];
				$i = 0;
				foreach($result as $get_request){
					
					$team_id				= $get_request->team_id;
					$custom_team_id				= $get_request->custom_team_id;
					$c_id					= $get_request->c_id;
					$u_id					= $get_request->user_id;
					$creator_user_id		= $get_request->team_creater_user_id;
					$team_name				= ucwords(trim($get_request->team_name));
					$team_banner			= $get_request->team_banner;
					$team_size				= $get_request->team_size;
					
					$TeamConditions = $this->master_model->checkApplyTeamCondtions($get_request->apply_status, $get_request->challenge_owner_status);
					$dispStatus = $TeamConditions['dispStatus'];
					
					$team_status			= $get_request->team_status;
					
					$getChallenge = $this->master_model->getRecords("challenge", array('c_id' => $c_id));
					$chName 	  = ucwords($encrptopenssl->decrypt($getChallenge[0]['challenge_title']));
					$category_id  = $getChallenge[0]['challenge_id'];
					$challenge_status	= $getChallenge[0]['challenge_status'];
					// Total Slot Count
					$getTotalSlot = $this->master_model->getRecords("byt_team_slots", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0'));
					
					$getApprovedSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Approved', 'apply_user_id != ' => $creator_user_id));
					
					// All Request Count
					$getSlotCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'apply_user_id != ' => $creator_user_id));
					$showCnt	  = count($getSlotCount);
					
					// REJECTED COUNT
					$getRejectedCount = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Rejected'));
					$rejectCnt	  = count($getRejectedCount);
					
					// Pending Slot COunt
					$getPendingSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Pending'));
					
					// Pending Slot COunt
					$getWithdrawSlot = $this->master_model->getRecords("byt_slot_applications", array('c_id' => $c_id, 'team_id' => $team_id, 'is_deleted' => '0', 'status' => 'Withdrawn'));
					
					$withdraw = '';					
					$v_link				= base_url('challenge/challengeDetails/'.base64_encode($get_request->c_id));
					$view				= '<a href="'.base_url('myteams/team_details_member/'.base64_encode($get_request->team_id)).'" data-toggle="tooltip" title="View" class="btn btn-success btn-green-fresh"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
					
					//$companyName		= 'No';
					$teamid_name			= $custom_team_id." - ".$team_name;
					//$chllenge_code_name		= $category_id." - ".$chName;	
					$chllenge_code_name		= '<a href="'.$v_link.'" target="_blank" class="code-anchor">'.$category_id." - ".$chName.'</a>';	
					$totalSlot				= count($getTotalSlot);
					$availableSlot			= count($getApprovedSlot);
					$pendingSlots			= count($getPendingSlot);
					$withDranSLot			= count($getWithdrawSlot);
					$hiddenDiv				= 'Count Of Application Pending<div class="guideline-tooltip"><i class="fa fa-exclamation-circle fa-lg"></i>
					<p>
					<span>Application Received : '.$showCnt.'</span><br />
					<span>Application Approved : '.$availableSlot.'</span> <br />
					<span>Application Rejected : '.$rejectCnt.'</span><br />
					<span>Application Pending  : '.$pendingSlots.'</span><br />
					<span>Application Withdraw  : '.$withDranSLot.'</span><br />	
					</p>											
					</div>';
					// Available Slot Count
					$available_Slot 		= ($totalSlot)-($availableSlot);
					
					// Action Buttons
					$conCat					= $view."&nbsp;".$withdraw;
					
					$teamForAnychallenge = 'Yes';
					
					$companyName = ucwords($cmp_name);
					
					$i++;
					
					$dataArr[] = array(
					$teamid_name,
					$teamForAnychallenge,
					$chllenge_code_name,
					$totalSlot,	
					$available_Slot,
					$hiddenDiv,
					$team_status,
					$challenge_status,
					$dispStatus, 
					$conCat
					);
					
					
					
			    $rowCnt = $rowCount;
					$response = array(
				  "draw" => $draw,
				  "recordsTotal" => $rowCnt?$rowCnt:0,
				  "recordsFiltered" => $rowCnt?$rowCnt:0,
				  "data" => $dataArr
					);
					$response['token'] = $csrf_test_name;
				}
				
				} else {
				
				$rowCnt = $rowCount;
				$response = array(
				"draw" => $draw,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => $dataArr
				);
				
				$response['token'] = $csrf_test_name;
			}
			
			echo json_encode($response);
			
		}  // Team Listing ENd
		
		// BYT Team Profile View
		public function show_team_profile($tid){
			//error_reporting(0);
			
			$teamId = base64_decode($tid);		
			
			if($this->session->userdata('user_id') == ""){			
				redirect(base_url('login'));
			}
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->join('arai_challenge','arai_byt_teams.c_id = arai_challenge.c_id');
			$getTeamDetails 	= $this->master_model->getRecords("byt_teams", array('team_id' => $teamId));
			
			// Show Team Members
			$getTeamSlot 	= $this->master_model->getRecords("byt_team_slots", array('team_id' => $teamId));
			
			$data['show_details']	 = $getTeamDetails;
			$data['show_slotlist'] = $getTeamSlot; 	
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/team-profile';
			$this->load->view('front/front_combo',$data);
		}
		
		// BYT Team Final Selection By Challenge Owner
		public function team_selection(){
			
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Post Values
			$c_id 		= $this->input->post('c_id');	
			$t_status 	= $this->input->post('t_status');
			$t_id 		= $this->input->post('t_id');
			$updateAt	= date('Y-m-d H:i:s');
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			if($t_status == 'Accept'){
				$teamFinalStatus = 'Approved';
				} else {
				$teamFinalStatus = 'Rejected';
			}
			
			
			// Update Status
			$updateArr = array('challenge_owner_status' => $teamFinalStatus, 'modified_on' => $updateAt);
			$updateQuery = $this->master_model->updateRecord('byt_teams',$updateArr,array('team_id' => $t_id, 'user_id' => $user_id));	
			
			if($updateQuery){
				
				$getStatus 	= $this->master_model->getRecords("byt_teams", array('team_id' => $t_id));
				
				if($getStatus[0]['challenge_owner_status'] == 'Approved'){
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully selected');
					} else {
					$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'Team successfully rejected');
				}
				
				} else {
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'warning', "content_text" => 'Somethign Wrong, Tray Again');
			}
			
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Log Details
			$logDetails 		= array(
			'user_id' 		=> $user_id,
			'action_name' 	=> "Team Approved/Rejected Selection",
			'module_name'	=> 'BYT Module',
			'store_data'	=> $json_encode_data,
			'ip_address'	=> $ipAddr,
			'createdAt'		=> $createdAt
			);
			$logData = $this->master_model->insertRecord('logs',$logDetails);	
			
			echo json_encode($jsonData);
		}
		
		// BYT Apply Slot By Applicant For Challenge
		public function applySlot($id){
			
			if($id == ""){
				redirect(base_url('challenge/myChallenges'));
			}
			
			// Decode ID
			$slotid = base64_decode($id);		
			$slotDetails = $this->master_model->getRecords("byt_team_slots", array('slot_id' => $slotid, 'is_deleted' => '0'));			
			//echo "<pre>";print_r($slotDetails);
			$data['slot_details']	 = $slotDetails;
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'challenge/apply-slot';
			$this->load->view('front/front_combo',$data);
			
		}
		
		// Slot Application Received Ajax Functionality
		public function slot_app_received(){
			
			// Generate CSRF
			$csrf_test_name = $this->security->get_csrf_hash();
			
			// Login User ID
			$user_id = $this->session->userdata('user_id');
			
			// Post Values
			$intro_urself 		= $this->input->post('intro_urself');	
			$is_agree 			= $this->input->post('is_agree');
			$team_id 			= $this->input->post('team_id');
			$c_id 				= $this->input->post('ch_id');
			$slot_id 			= $this->input->post('slot_id');
			$updateAt			= date('Y-m-d H:i:s');	
			
			// Log Captured		
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			
			// Fetch Record
			$fetchRecord = $this->master_model->getRecords("byt_slot_applications", array('slot_id' => $slot_id, 'team_id' => $team_id, 'c_id' => $c_id, 'apply_user_id' => $user_id, 'is_deleted' => '0'));			
			
			if(count($fetchRecord) > 0){
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - Already Exist",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'error', "content_text" => 'You already apply for this position.', "t_id" => $team_id);
				
				} else {
				
				// Insert SQL 
				$insertArr 	= array(
				'c_id' 				=> $c_id,
				'team_id' 			=> $team_id,
				'slot_id'			=> $slot_id,
				'apply_user_id'		=> $user_id,
				'introduce_urself'	=> $intro_urself,
				'if_agree'			=> $is_agree
				);
				$this->master_model->insertRecord('byt_slot_applications',$insertArr);	
				
				$logDetails 	= array(
				'user_id' 		=> $user_id,
				'action_name' 	=> "Slot Application Received - New Application",
				'module_name'	=> 'BYT Module',
				'store_data'	=> $json_encode_data,
				'ip_address'	=> $ipAddr,
				'createdAt'		=> $createdAt
				);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				$jsonData = array("token" => $csrf_test_name, "success" => 'success', "content_text" => 'You successfully applied for the position.', "t_id" => $team_id);
			}
			
			echo json_encode($jsonData);
			
		}
		
		/******************Team Load More*****************/
		
		public function team_more()
		{			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			sleep(1);
			$csrf_test_name = $this->security->get_csrf_hash();
			
			$id = $this->input->post('id');
			$c_id = $this->input->post('ch_id');
			
			// Get Teams Total Count 
			$total_cnt  = $this->master_model->getRecords('byt_teams',array('c_id'=> $c_id, 'is_deleted'=>"0", 'status'=>"Active", 'team_type'=>"challenge"),'',array('team_id', 'ASC'));
			
			$showLimit = 6;
			
			$sql = "SELECT * FROM arai_byt_teams WHERE is_deleted = '0' AND status='Active' AND c_id = '".$c_id."' AND team_type = 'challenge' ORDER BY team_id ASC LIMIT ".$id.",".$showLimit."";//AND apply_status!= 'Withdrawn' 
			$result 	= $this->db->query($sql)->result();	
			
			$showData = '';		
			if(count($result) > 0)
			{				
				foreach($result as $c_data)
				{
					$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $c_data->c_id));
					//print_r($getSlotMaxMember);
					// Team Slot Details
					$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $c_data->c_id, 'team_id'=> $c_data->team_id, 'is_deleted' => '0'));
					$maxTeam = $encrptopenssl->decrypt($getSlotMaxMember[0]['max_team']);
					$minTeamCnt = count($slotDetails);
					$availableCnt = $maxTeam - $minTeamCnt;				
					
					$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $c_data->c_id, 'team_id' => $c_data->team_id, 'is_deleted' => '0'));
					//echo $this->db->last_query(); exit;
					
					$totalSlotCnt = $totalPendingCnt = 0;
					foreach($getAllSlotTeam as $res)
					{
						if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
						$totalSlotCnt++;
					}
					
					
					
					$array_skills = array();
					foreach($slotDetails as $slot_list)
					{
						if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }
					}
					
					$combine = implode(",",$array_skills);
					$xp = explode(',',$combine);
					$arrayUnique = array_unique($xp);
					
					$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $c_data->c_id));
					$dCha = $encrptopenssl->decrypt($chDetails[0]['challenge_details']);
					
					if($c_data->team_banner!=""){
						$banner_team = base_url('uploads/byt/'.$c_data->team_banner);
						} else {
						$banner_team = base_url('assets/no-img.png');
					}	
					//<a href="" class="btn btn-primary w-100 mt-02">View Details</a>
					//<a href="javascript:void(0);" class="'.$styles_hide.' apply-popup">Apply</a>
					$showData .= '
						<div class="col-sm-12 justify-content-center mt-4 team-box">
							<div class="boxSection25">
								<ul class="list-group list-group-horizontal">
									<li class="list-group-item">
										<div class="inner-box"  style="border-bottom:none">										
											<img src="'.$banner_team.'" alt="Banner" class="img-fluid">
											<h3 class="minheight" style="font-size:16px;">'.character_limiter($c_data->team_name,60).'</h3>
											<ul>
												<li>'.$totalSlotCnt.' Members </li> 
												<li>-</li>
												<li>'.$totalPendingCnt.' Available Slots</li>
											</ul>
										</div>
									</li>
									
									<li class="list-group-item">
										<div class="inner-box">											
											<strong style="display:block; text-align:left; margin:15px 0px 0px 0px;">Brief Info about Team </strong>
											<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0">'.substr($c_data->brief_team_info,0,90).'...<a href="javascript:void(0)" class="click-more" data-id="'.$c_data->team_id.'">View More</a></p>
											<h4>Looking For</h4>
											<div class="Skillset text-center">';
												$s = 0;	
												foreach($arrayUnique as $commonSkill)
												{
													if($s < 9)
													{
														$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$teamslist['user_id']."')",NULL, false);
														$skill_now = $this->master_model->getRecords('skill_sets');
														$skill_names = $encrptopenssl->decrypt($skill_now[0]['name']);												
														$showData .='<span class="badge badge-pill badge-info">'.$skill_names.'</span>';
													}
													$s++;
												}
			$showData .='		</div>
										</div>
									</li>
							
									<li class="list-group-item">
										<div class="team_listing owl-carousel owl-theme">';	
											// Slot Deatils Foreach	
											$TeamSr = 1;
											foreach($slotDetails as $slotD)
											{
												$skills = array();
												$skillTitle = 'Skills';
												$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
														
												if(count($getSlotDetail) > 0)
												{														
												$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
												$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
						
												$catID = $getUserDetail[0]['user_category_id'];	
													$getNames = $encrptopenssl->decrypt($getUserDetail[0]['first_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['middle_name'])." ".$encrptopenssl->decrypt($getUserDetail[0]['last_name']); //." ".$getUserDetail[0]['user_id']
												$fullname = ucwords($getNames);
						
													if($catID == 1)
												{
														$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
														$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
														
														$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
														if ($getProfileDetail[0]['profile_picture']!="") 
													{
															$imageUser = $imgPath;
															}
														else 
														{
															$imageUser = base_url('assets/no-img.png');
														}
														
														if($getProfileDetail[0]['skill_sets'] != "")
														{
															$skill_set_ids = $encrptopenssl->decrypt($getProfileDetail[0]['skill_sets']);
															
															if($skill_set_ids != "")
															{
																$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
																$skills = $this->master_model->getRecords("arai_skill_sets");														
													}
												} 
						
														if($getProfileDetail[0]['other_skill_sets'] != "") 
												{
															$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
														}
												} 
													else if($catID == 2)
												{
													$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
							
													if(count($getOrgDetail) > 0)
													{								
														$profilePic = $encrptopenssl->decrypt($getOrgDetail[0]['org_logo']);
														$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
															if ($profilePic!="") {
																$imageUser = $imgPath;
																} else {
																$imageUser = base_url('assets/no-img.png');
															}																	
														}	
														
														$skillTitle = 'Sector';
														if($getOrgDetail[0]['org_sector'] != "")
														{
															$skill_set_ids = $getOrgDetail[0]['org_sector'];
															
															if($skill_set_ids != "")
															{
																$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
																$skills = $this->master_model->getRecords("arai_organization_sector");														
													}
												}									
						
														if($getOrgDetail[0]['org_sector_other'] != "") 
														{ 
															$skills[]['name'] = $encrptopenssl->decrypt($getOrgDetail[0]['org_sector_other']); 
														}
													}								
															
													if($getSlotDetail[0]['apply_user_id'] == $teamslist['user_id'])
													{
														$styles_hide = "hide-class";
													} 
													else 
													{
														$styles_hide = "d-inline-block";
													}															
												} 
												else 
												{
													$imageUser = base_url('assets/no-img.png');
													$styles_hide = "d-inline-block";
													$fullname = "Open Slot";											
													
													$skill = $slotD['skills'];											
													$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
													$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
												} 
						
												$showData .='
													<div class="item text-center">
														<img src="'.$imageUser.'" alt="'.$fullname.'" title="'.$fullname.'" class="check-'.$getSlotDetail[0]['apply_user_id'].'" />
														<h4>'.$fullname.'</h4>
														<div class="Skillset text-center">';
															if(count($skills)) 
															{
																$i=0;
																if($i < 6)
																{
																	foreach ($skills as $key => $value) 
																	{ 
																		$showData .='<span class="badge badge-pill badge-info">'; if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { $showData .= $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { $showData .= $value['name']; } } $showData .='</span>';
																		$i++;
																	}
																}
															}
							$showData .='	</div>
														
														<h4>Team Member '.$TeamSr.'</h4>
													</div>';
												
													$m++; $TeamSr++;
											}	
			$showData .= '</div> 
								</li>
								
								<div class="container" style="clear:both;" >
									<div class="row buttonView mb-3">
										<div class="col-md-3">
										<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
										</div>
										<div class="col-md-6">
											<a href="'.base_url('myteams/team_details_member/'.base64_encode($c_data->team_id)).'" class="btn btn-primary w-100">View Details</a>
										</div>
										<div class="col-md-3">
											<a href="'.base_url('myteams/applySlot/').base64_encode($c_data->team_id)."/".base64_encode($c_data->c_id).'" class="btn btn-primary w-100">Apply</a>
										</div>
									</div>
								</div>
							</ul>
						</div>
					</div>';			
					
					$nextid = $c_data->team_id;
					
				} // Foreach End
				
				if($total_cnt > $showLimit){
					
					$showData .= '<div class="col-md-12 text-center show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>';
					
				}
				
				$jsonData = array("html" => $showData, "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
				} else {
				
				$jsonData = array("html" => '<div align="Center" style="width: 100%;"><b>No Record Found</b></div>', "token" => $csrf_test_name);
				echo json_encode($jsonData);
				
			}
			
		}
		
		/*******************End Team More*****************/
		
		public function get_client_ip() {
			$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
			else
			$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}
		
		public function participate_link($c_id=0)
		{
			if($c_id == "" || $c_id == "0")
			{
				redirect(site_url());
			}
			
			$c_id = base64_decode($c_id);
			$data['challenge_data'] = $challenge_data = $this->master_model->getRecords("challenge", array('c_id'=>$c_id));
			
			$data['link'] = $challenge_data[0]['temp_participate_link'];
			
			$data['page_title'] 	 = '';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/participate_link';
			$this->load->view('front/front_combo',$data);
		}
		
		public function viewDetails(){
		 
		//error_reporting(0);
		$encrptopenssl =  New Opensslencryptdecrypt();
		 
		$id=$this->input->post('id'); 
		$records = $this->master_model->getRecords("byt_teams",array("team_id" => $id));
		$challenge_details = $records[0]['brief_team_info'];
		
		
		$table = '<p>'.$challenge_details.'</p>';
		echo $table;
		 
	 }
	 
	public function closeChallenge($challengeId){
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			$this->load->library('upload');
			$c_id = base64_decode($challengeId);
			$user_id = $this->session->userdata('user_id');
			
			
			$challengeData = $this->master_model->getRecords("challenge", array("c_id" => $c_id));	
			
			$this->db->select('title, first_name, middle_name, last_name, email');
			$registeruser_details = $this->master_model->getRecords("registration", array("user_id" => $user_id));
			$profileUpdate = array();
			foreach($registeruser_details as $profileData){
				$profileData['fullname'] 			= ucfirst($encrptopenssl->decrypt($profileData['title']))." ".ucfirst($encrptopenssl->decrypt($profileData['first_name']))." ".ucfirst($encrptopenssl->decrypt($profileData['middle_name']))." ".ucfirst($encrptopenssl->decrypt($profileData['last_name']));
				$profileData['email'] 				= $encrptopenssl->decrypt($profileData['email']);
				$profileUpdate[] = $profileData;
			}		
			
			$countData = $this->master_model->getRecords("challenge_closure_update", array("c_id" => $c_id));
			
			// Condition Match
			if($user_id != $challengeData[0]['u_id']){
				$this->session->set_flashdata('error','Sorry, you have no permission to view this page.');
				redirect(base_url('challenge/myChallenges'));die();
			}

			/*if(count($countData) > 0){
				$this->session->set_flashdata('error','You already closed this challenge.');	
				redirect(base_url('challenge/myChallenges'));die();
			}*/
			
			if(isset($_POST) && count($_POST) > 0)
			{	
				$postValues = $this->input->post();
				
				$planeArr = array(
								"c_id" 			=> $c_id,
								"user_id" 		=> $user_id,
								"closure_title" => $this->input->post('closure_title'),
								"closure_desc" 	=> $this->input->post('closure_desc')
							);
			$insertID = $this->master_model->insertRecord('challenge_closure_update',$planeArr);
				
				// If Upload Type is Image				
				if($this->input->post('upload_type') == 'Image'){					
					
					$closer_file = $_FILES['closer_file'];
					
					if(count($closer_file) > 0)
					{	
						for($i=0; $i < count($closer_file['name']); $i++)
						{
							
							if($closer_file['name'][$i] != '')
							{	
								$challenge_file = $this->Common_model_sm->upload_single_file('closer_file', array('png','jpg','jpeg','gif'), "closer_file_".$c_id."_".date("YmdHis").rand(), "./assets/challenge", "png|jpeg|jpg|gif",'1',$i);
								
								if($challenge_file['response'] == 'error'){ }
								else if($challenge_file['response'] == 'success')
								{
									
									$imgArr = array(
														"cu_id" 		=> $insertID,
														"upload_type" 	=> 'Image',
														"upload_name" 	=> $challenge_file['message']
													);
												
									$insertData = $this->master_model->insertRecord('closure_update_files',$imgArr);
								}
							}
						}
					} // End Image
					
				} else if($this->input->post('upload_type') == 'Video'){
					
					$video_file = $this->input->post('youtube_link');
					
					if(count($video_file) > 0){
						
						foreach($video_file as $videoName){
							
							$videoArr = array(
												"cu_id" 		=> $insertID,
												"upload_type" 	=> 'Video',
												"upload_name" 	=> $videoName
											);
							$insertData = $this->master_model->insertRecord('closure_update_files',$videoArr);
						}
						
					}
					
				} // End Video Loop	
				
				// Document Upload
				$document_file = $_FILES['doc_file'];
				
				if(count($document_file) > 0)
				{	
					for($i=0; $i < count($document_file['name']); $i++)
					{
						
						if($document_file['name'][$i] != '')
						{	
							$document_file_res = $this->Common_model_sm->upload_single_file('doc_file', array('pdf','xls','xlsx','doc','docx','txt','ppt'), "closer_doc_".$c_id."_".date("YmdHis").rand(), "./assets/challenge", "pdf|xls|xlsx|doc|docx|txt|ppt",'1',$i);
							
							if($document_file_res['response'] == 'error'){ }
							else if($document_file_res['response'] == 'success')
							{
								$docArr = array(
													"cu_id" 		=> $insertID,
													"upload_type" 	=> 'Document',
													"upload_name" 	=> $document_file_res['message']
												);
								$insertData = $this->master_model->insertRecord('closure_update_files',$docArr);
							}
						}
					}
				} // End Doc
				
				
				// Challenge Status Update 
				/*$updateArr = array('challenge_status' => "Closed", 'updated_by_id' => $user_id, "updatedAt" => date('Y-m-d H:i:s'));
				$this->master_model->updateRecord('challenge',$updateArr,array('c_id' => $c_id));*/
				
				
				// Log Data Added 
				$filesData 			= $_FILES;
				$json_data 			= array_merge($postValues,$filesData);
				$json_encode_data 	= json_encode($json_data);				
				$ipAddr			  	= $this->get_client_ip();
				$createdAt			= date('Y-m-d H:i:s');
				$logDetails 		= array(
										'user_id' 			=> $user_id,
										'action_name' 		=> "Challenge Closure By Challenge Owner Front End",
										'module_name'		=> 'Challenge',
										'store_data'		=> $json_encode_data,
										'ip_address'		=> $ipAddr,
										'createdAt'			=> $createdAt
										);
				$logData = $this->master_model->insertRecord('logs',$logDetails);
				
				//Email Functionality To Challenge Owner
				$this->challenge_updates_email_to_owner($profileData, $c_id);
				// Email Functionality to approve applicants
				$this->challenge_update_email_to_applicants($c_id);
				
				// Successfully Redirect
				$this->session->set_flashdata('success','Challenge updates successfully posted.');	
				redirect(site_url('challenge/myChallenges'));
				
				
				
				
				
			} // POST ARRRAY END	
			
			$data['c_id']	=	$c_id;
			$data['challengeDetails'] = $challengeData;			
			$data['page_title'] 	 = 'Challenge';
			$data['submodule_name']  = '';	
			$data['middle_content']	 = 'challenge/closure';
			$this->load->view('front/front_combo',$data);
		}
		
		public function feedupdate(){
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			echo $slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_applicants_on_Challenge_Feed_Update');
			echo "<br />";
			echo $slug1 = $encrptopenssl->encrypt('byt_mail_to_all_challenge_applicants_on_applicants_on_Challenge_Feed_Update');
		}
		
		// Challenge update email to applicants
		public function challenge_update_email_to_applicants($c_id){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			$slug = $encrptopenssl->encrypt('byt_mail_to_all_challenge_applicants_on_applicants_on_Challenge_Feed_Update');
			$challenge_update_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			$setting_table3  = $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_no3	= $encrptopenssl->decrypt($setting_table3[0]['contact_no']);
			$content3 = '';
			
			if(count($challenge_update_mail) > 0){	
			
				$subject3 	 	= $encrptopenssl->decrypt($challenge_update_mail[0]['email_title']);
				$description3 	= $encrptopenssl->decrypt($challenge_update_mail[0]['email_description']);
				$from_admin3 	= $encrptopenssl->decrypt($challenge_update_mail[0]['from_email']);					
				$sendername3  	= $encrptopenssl->decrypt($setting_table3[0]['field_1']);
				
				/*$slot_details = $this->master_model->getRecords("byt_slot_applications", array("c_id" => $c_id, 'status' => "Approved")); */
				
				$this->db->join('arai_byt_teams b','s.team_id = b.team_id','INNER',FALSE);
				$this->db->where("b.is_deleted","0");
				$slot_details = $this->master_model->getRecords("byt_slot_applications as s", array("s.c_id" => $c_id, 's.status' => "Approved")); //, 'b.challenge_owner_status' => 'Approved'
				
				
				
				// Challenge Details
				$this->db->select('challenge_id, challenge_title');
				$challenge_details = $this->master_model->getRecords("challenge", array("c_id" => $c_id));
				$challenge_id	   = $challenge_details[0]['challenge_id'];
				$challengeTitle   = $encrptopenssl->decrypt($challenge_details[0]['challenge_title']);
				
				if(count($slot_details) > 0){
					
					foreach($slot_details as $slotUser){
					
						// Register User in Slot
						$this->db->select('title, first_name, middle_name, last_name, email');
						$registeruser_details = $this->master_model->getRecords("registration", array("user_id" => $slotUser['apply_user_id']));
						$fullname 	= ucfirst($encrptopenssl->decrypt($registeruser_details[0]['title']))." ".ucfirst($encrptopenssl->decrypt($registeruser_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($registeruser_details[0]['middle_name']))." ".ucfirst($encrptopenssl->decrypt($registeruser_details[0]['last_name']));
						$email 	= $encrptopenssl->decrypt($registeruser_details[0]['email']);
						
						$link = base_url('challenge/challengeDetails/'.base64_encode($c_id));
						$click = '<a href="'.$link.'" target="_blank">click</a>';
						$arr_words3 = ['[USERNAME]', '[CHALLENGENAME]', '[CLICK]'];
						$rep_array3 = [$fullname,  $challengeTitle,  $click];
						
						$content3 = str_replace($arr_words3, $rep_array3, $description3);
						
						//email admin sending code 
						$info_arr3 = array(
									'to'		=>	$email, 				
									'cc'		=>	'',
									'from'		=>	$from_admin3,
									'subject'	=> 	$subject3,
									'view'		=>  'common-file'
								);
						
						$other_info3=array('content'=>$content3); 
						
						$emailsend=$this->emailsending->sendmail($info_arr3,$other_info3);
						
					}
					
				}				
					
			}
			
		}
		
		// Challenge update email to challenge owner
		public function challenge_updates_email_to_owner($profileData, $c_id){
			
			// Create Object
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$slug = $encrptopenssl->encrypt('byt_mail_to_challenge_owner_on_applicants_on_Challenge_Feed_Update');
			$challenge_update_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
			
			$setting_table3  = $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_no3	= $encrptopenssl->decrypt($setting_table3[0]['contact_no']);
			$content3 = '';
			
			if(count($challenge_update_mail) > 0){	
			
					$subject3 	 	= $encrptopenssl->decrypt($challenge_update_mail[0]['email_title']);
					$description3 	= $encrptopenssl->decrypt($challenge_update_mail[0]['email_description']);
					$from_admin3 	= $encrptopenssl->decrypt($challenge_update_mail[0]['from_email']);					
					$sendername3  	= $encrptopenssl->decrypt($setting_table3[0]['field_1']);
					
					$this->db->select('challenge_id, challenge_title');
					$challenge_details = $this->master_model->getRecords("challenge", array("c_id" => $c_id));
					$challenge_id	   = $challenge_details[0]['challenge_id'];
					$challenge_title   = $encrptopenssl->decrypt($challenge_details[0]['challenge_title']);
					
					$fullname 			= $profileData['fullname'];
					$ownerEmail 		= $profileData['email'];
					$challengeId 		= $challenge_id;
					$challengeTitle 	= ucfirst($challenge_title);
					
					$link = base_url('challenge/challengeDetails/'.base64_encode($c_id));
					$click = '<a href="'.$link.'" target="_blank">click</a>';
					$arr_words3 = ['[USERNAME]', '[CHALLENGENAME]', '[CHALLENGEID]', '[CLICK]'];
					$rep_array3 = [$fullname,  $challengeTitle,  $challengeId, $click];
					
					$content3 = str_replace($arr_words3, $rep_array3, $description3);
					
					//email admin sending code 
					$info_arr3 = array(
								'to'		=>	$ownerEmail, //$ownerEmail,					
								'cc'		=>	'',
								'from'		=>	$from_admin3,
								'subject'	=> 	$subject3,
								'view'		=>  'common-file'
							);
					
					$other_info3=array('content'=>$content3); 
					
					$emailsend=$this->emailsending->sendmail($info_arr3,$other_info3);
					
				} // Slug Exist			
			
		} // Function End

		public function documentDownload($file){
			
			$decodeName = base64_decode($file);
			$root = 'assets/challenge/';					
			$file_name = $root.$decodeName;		
			if($file_name){ 				
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_name));
				flush(); // Flush system output buffer
				readfile($file_name);
				die();
			} else { 
				http_response_code(404);
			}
			
		}

		function sortBy($field, &$array, $direction = 'asc')
		{
		    usort($array, @create_function('$a, $b', '
		        $a = $a["' . $field . '"];
		        $b = $b["' . $field . '"];
		        $a = strtolower(trim($a));
		        $b = strtolower(trim($b));

		        if ($a == $b) return 0;

		        @$direction = strtolower(trim($direction));
		        return ($a ' . (@$direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
		    '));

		    return true;
		}
		
	
		
}	