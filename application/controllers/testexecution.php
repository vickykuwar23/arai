<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Cron Execution
Author : Vicky K
*/

class Execute extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		date_default_timezone_set('Asia/Kolkata');		
    }

	public function index(){	
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$settingData 	= $this->master_model->getRecords("setting", array("id" => '1'));
		$newsAlert 		= $settingData[0]['news_alert'];
		$challengeAlert = $settingData[0]['challenge_alert'];
		
		// News Alert Email Send
		if($newsAlert == 'Y'){
			
			$newsletter = $this->master_model->getRecords("newsletter", array("status" => 'Active', "news_notification" => '1'));
			
			$blogData = $this->master_model->getRecords("news_blog", array("status" => 'Active', "send_alert" => '0'));	
			
			$content = '';			
			//$this->testMail();
			// Newsletter Foreach	
			foreach($newsletter as $newsletter_data){
				
				// Blog Foreach
				foreach($blogData as $blogDetails){	
					
					$email_template = $this->master_model->getRecords("email_template", array("status" => 'Active', "id" => '62'));					
					$subject 	 	= $encrptopenssl->decrypt($email_template[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($email_template[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($email_template[0]['from_email']);					
					$to_user		= 'vicky.kuwar@esds.co.in';
					//$to_user		= $encrptopenssl->decrypt($newsletter_data['email_id']);
					$contact_no  	= $encrptopenssl->decrypt($settingData[0]['contact_no']);				
					
					$arr_words = ['[PHONENO]'];
					$rep_array = [$contact_no];
					
					$content = str_replace($arr_words, $rep_array, $description);
									
					$info_array=array(
						'to'		=>	$to_user,					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject,
						'view'		=>  'common-file'
						);
				
					$other_info	=	array('content' => $content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_info);
					
					if($email_send)
					{
						$insertLog 			= array_merge($info_array,$other_info);
						$json_encode_data 	= json_encode($insertLog);
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
													'user_id' 		=> "0",
													'action_name' 	=> "CRON EXECUTION",
													'module_name'	=> 'News and Updates Cron Success',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr,
													'createdAt'		=> $createdAt
												);
						$logData = $this->master_model->insertRecord('logs',$logDetails);	
						
						$msg =   "Success";
						
					} else {
						
						$msg =   "Fail";
						
						$insertLog 			= array_merge($info_array,$other_info);
						$json_encode_data 	= json_encode($insertLog);
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails 		= array(
													'user_id' 		=> "0",
													'action_name' 	=> "CRON EXECUTION",
													'module_name'	=> 'News and Updates Cron Fail',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr,
													'createdAt'		=> $createdAt
												);
						$logData = $this->master_model->insertRecord('logs',$logDetails);	
						
					}
					
					// array updated values
					$updateArr = array('send_alert' => '1');
					
					//Update SQL
					//$updateQuery = $this->master_model->updateRecord('news_blog',$updateArr,array('id' => $blogDetails['id']));
					
					
				} // Newsletter Foreach	End
				
			} // Blog Foreach End
			
			
		} // News Aler If End	


		if($challengeAlert == 'Y'){			
			
			$newsletter = $this->master_model->getRecords("newsletter", array("status" => 'Active', "challenge_notification" => '1'));
			
			$challengeData = $this->master_model->getRecords("challenge", array("challenge_status" => 'Approved', "status" => 'Active', "send_alert" => '0'));	
			
			// Newsletter Foreach Start
			foreach($newsletter as $newsletter_data){
				
				// Challenge Foreach	
				foreach($challengeData as $challengeDetails){
					
					$email_template = $this->master_model->getRecords("email_template", array("status" => 'Active', "id" => '63'));					
					$subject 	 	= $encrptopenssl->decrypt($email_template[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($email_template[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($email_template[0]['from_email']);
					//$to_user		= $encrptopenssl->decrypt($newsletter_data['email_id']);
					$to_user		= 'vicky.kuwar@esds.co.in';
					$contact_no  	= $encrptopenssl->decrypt($settingData[0]['contact_no']);				
					
					$arr_words = ['[PHONENO]'];
					$rep_array = [$contact_no];
					
					$content = str_replace($arr_words, $rep_array, $description);
									
					$info_array=array(
						'to'		=>	$to_user,					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject,
						'view'		=>  'common-file'
						);
				
					$other_info	=	array('content' => $content); 
					
					$email_send = $this->emailsending->sendmail($info_array,$other_info);
					
					if($email_send)
					{
						$insertLog 			= array_merge($info_array,$other_info);
						$json_encode_data 	= json_encode($insertLog);
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails = array(
											'user_id' 		=> "0",
											'action_name' 	=> "CRON EXECUTION",
											'module_name'	=> 'Challenge Cron Success',
											'store_data'	=> $json_encode_data,
											'ip_address'	=> $ipAddr,
											'createdAt'		=> $createdAt
											);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
						$msg =  "Success";
						
					} else {
						
						$msg =   "Fail";
						$insertLog 			= array_merge($info_array,$other_info);
						$json_encode_data 	= json_encode($insertLog);
						$ipAddr			  	= $this->get_client_ip();
						$createdAt			= date('Y-m-d H:i:s');
						$logDetails = array(
											'user_id' 		=> "0",
											'action_name' 	=> "CRON EXECUTION",
											'module_name'	=> 'Challenge Cron Fail',
											'store_data'	=> $json_encode_data,
											'ip_address'	=> $ipAddr,
											'createdAt'		=> $createdAt
											);
						$logData = $this->master_model->insertRecord('logs',$logDetails);
						
					}
					
					// array updated values
					$updateArr = array('send_alert' => '1');
					
					//Update SQL
					//$updateQuery = $this->master_model->updateRecord('challenge',$updateArr,array('c_id' => $challengeDetails['c_id']));
				
					
				} // Newsletter Foreach	End
				
					
				
			}  // Challenge Foreach End
		
		} // Challenge If End	
	   
  
	 } // Index Function End 
	
	public function testMail(){
		
	    $fromemail = "vicky.kuwar@esds.co.in";
		$fromadmin = "vicky.kuwar@esds.co.in";
		$sub_content =" Testing Mail Cron CRONEXECUTION FILE";	
			$info_array=array(
					'to'		=>	$fromemail,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	"Tesat Mail",
					'view'		=>  'subscription'
					);
			
			$other_infoarray	=	array('content' => $sub_content); 
			
			$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			
			if($email_send)
			{
				echo "Success";die();
			} else {
				echo "Fail";die();
			}
	   
   }	
	 
	 public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	

} 