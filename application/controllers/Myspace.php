<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Myspace extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        $this->login_user_id = $this->session->userdata('user_id');
        if($this->login_user_id == "") { redirect(site_url()); }

        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
    }

    function index(){
        die;
        $user_id = $this->session->userdata('user_id');
       
        $this->db->order_by('r.r_id','DESC',FALSE);
        $data['resource_sharing'] = $resource_sharing = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'user_id'=>$user_id) );

        $this->db->order_by('k.kr_id','DESC',FALSE);
        $data['knowledge_repository'] = $knowledge_repository = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'user_id'=>$user_id));

        
         
        
        // Most commneted blog
        $sql="SELECT arai_blog.user_id,blog_banner,arai_blog.blog_id,`blog_title`, max(totalcomments) as highest_comments FROM ( SELECT `blog_id` , count(`blog_id`) as totalcomments FROM arai_blog_comments where arai_blog_comments.status=1 GROUP BY `blog_id` ) as t LEFT JOIN arai_blog ON arai_blog.blog_id = t.blog_id WHERE  arai_blog.admin_status=1 AND arai_blog.is_deleted=0 AND arai_blog.is_block=0 AND arai_blog.user_id=".$user_id." ";
        $query = $this->db->query($sql);

        $data['most_commented_blog'] = $most_commented_blog= $query->result_array();
         // End most commneted blog

        $sql_like="SELECT arai_blog.user_id,blog_banner,arai_blog.blog_id,`blog_title`, max(total_likes) as highest_likes FROM ( SELECT `blog_id` , count(`blog_id`) as total_likes FROM arai_blogs_likes GROUP BY `blog_id` ) as t LEFT JOIN arai_blog ON arai_blog.blog_id = t.blog_id WHERE  arai_blog.admin_status=1 AND arai_blog.is_deleted=0 AND arai_blog.is_block=0 AND  arai_blog.user_id=".$user_id." ";
        $query_like = $this->db->query($sql_like);

        $data['most_likeed_blog'] = $most_likeed_blog= $query_like->result_array();
        // print_r($most_likeed_blog);die;
       

        $data['technology_transfer'] = $technology_transfer = $this->master_model->getRecords("arai_technology_transfer t",array('t.is_deleted' => '0','user_id'=>$user_id) );

        $this->db->order_by('qa.q_id','DESC',FALSE);
        $data['fqa_forum_data'] = $fqa_forum_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.is_deleted' => '0','user_id'=>$user_id) );

        $this->db->order_by('qc.comment_id','DESC',FALSE);
        $data['fqa_forum_answers_data'] = $fqa_forum_answers_data = $this->master_model->getRecords("arai_qa_forum_comments qc",array('user_id'=>$user_id) );

        $data['webinars'] = $webinars = $this->master_model->getRecords('arai_webinar w',array('user_id'=>$user_id));


        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/blogs';
        $this->load->view('front/front_combo', $data);
    }

    public function blogs()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/blogs';
        $data['module_title'] = 'blogs';
        $this->load->view('front/front_combo', $data);
    }

    public function load_blogs_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $keyword = $this->input->post('keyword');
        if ($is_search==1 && $inner_tab=='myblogs' && $keyword!='') {
            $this->db->where( "(b.blog_id_disp LIKE '%" . $keyword . "%' OR b.blog_title LIKE '%" . $keyword . "%' OR b.blog_description LIKE '%" . $keyword . "%' OR b.technology_other LIKE '%" . $keyword . "%' OR b.author_name LIKE '%" . $keyword . "%' OR b.author_professional_status LIKE '%" . $keyword . "%' OR b.author_org_name LIKE '%" . $keyword . "%' OR b.author_description LIKE '%" . $keyword . "%' OR b.author_about LIKE '%" . $keyword . "%' )") ;
        }    
        $data['blogs'] = $blogs = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0',  'user_id'=>$user_id )); 
        
        if ($is_search==1 && $inner_tab=='myactiveblogs' && $keyword!='') {
            $this->db->where( "(b.blog_id_disp LIKE '%" . $keyword . "%' OR b.blog_title LIKE '%" . $keyword . "%' OR b.blog_description LIKE '%" . $keyword . "%' OR b.technology_other LIKE '%" . $keyword . "%' OR b.author_name LIKE '%" . $keyword . "%' OR b.author_professional_status LIKE '%" . $keyword . "%' OR b.author_org_name LIKE '%" . $keyword . "%' OR b.author_description LIKE '%" . $keyword . "%' OR b.author_about LIKE '%" . $keyword . "%' )") ;
        }

        $data['blogs_active'] = $blogs_active = $this->master_model->getRecords("arai_blog b", array('b.admin_status' => '1','b.is_deleted' => '0','b.is_block'=>'0'));  
        // echo $this->db->last_query();die;
        $temp_active_blogs=array();
        $reply_info=array();
        $reply_info_temp=array();
        foreach ($blogs_active as $key => $blog) {
                            $this->db->order_by('comment_id','desc');
              $comment_info = $this->master_model->getRecords("arai_blog_comments bc", array('bc.deleted_on' => NULL,  'bc.user_id'=>$user_id ,'bc.blog_id' => $blog['blog_id']),'bc.blog_id,bc.comment,bc.comment_id,bc.created_on');

              if (count($comment_info)>0) {
                foreach ($comment_info as $key2 => $value) {
                    $reply_info_arr = $this->master_model->getRecords("arai_blog_comments bc", array('bc.deleted_on' => NULL,  'bc.user_id !='=>$user_id ,'bc.blog_id' => $blog['blog_id'],'bc.parent_comment_id'=>$value['comment_id']),'bc.blog_id,bc.comment,bc.created_on');
                    if (count($reply_info_arr)>0) {
                        $reply_info_temp[$key2]=$reply_info_arr;
                    }

                }
                $reply_info=$reply_info_temp;
                
                                  
              }

              $like_info = $this->master_model->getRecords("arai_blogs_likes bl", array('bl.user_id'=>$user_id ,'bl.blog_id' => $blog['blog_id']),'bl.created_on');

                
          if (count($comment_info)>0) {
                $temp_active_blogs[$key]['blog_title']=$blog['blog_title'];
                $temp_active_blogs[$key]['blog_id']=$blog['blog_id'];
                $temp_active_blogs[$key]['comments']=$comment_info;
                $temp_active_blogs[$key]['reply']=$reply_info;
          }
         
         if (count($like_info)>0 ) {
            $like_date_time=$like_info[0]['created_on'];
            $temp_active_blogs[$key]['blog_title']=$blog['blog_title'];
            $temp_active_blogs[$key]['blog_id']=$blog['blog_id'];
            $temp_active_blogs[$key]['like_info']=$like_date_time;
          }
        }  

        $data['blogs_active_comment']=$temp_active_blogs;
        $response['myspace_blogs']=$view_res = $this->load->view('front/myspace/inc_blogs', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }

    public function qa()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/qa';
        $data['module_title'] = 'qa';
        $this->load->view('front/front_combo', $data);
    }

    public function load_qa_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $keyword = $this->input->post('keyword');
        if ($is_search==1 && $inner_tab=='myqa' && $keyword!='') {
             $this->db->where("( qa.custum_question_id LIKE '%".$keyword."%' OR qa.question LIKE '%".$keyword."%' OR qa.tags_other LIKE '%".$keyword."%')");
        }    
        $this->db->order_by('qa.q_id','DESC',FALSE);
        $data['fqa_forum_data'] = $fqa_forum_data = $this->master_model->getRecords("arai_qa_forum qa",array('qa.is_deleted' => '0','user_id'=>$user_id) );

        if ($is_search==1 && $inner_tab=='myactiveqa' && $keyword!='') {
            $this->db->where("( qa.custum_question_id LIKE '%".$keyword."%' OR qa.question LIKE '%".$keyword."%' OR qa.tags_other LIKE '%".$keyword."%')");
        }

        $data['qa_active'] = $qa_active = $this->master_model->getRecords("arai_qa_forum qa", array('qa.admin_status' => '1','qa.is_deleted' => '0','qa.is_block'=>'0'));  

        $temp_active_qa=array();
        $reply_info=array();
        $reply_info_temp=array();
        if(count($qa_active)>0){
        foreach ($qa_active as $key => $question) {
                            $this->db->order_by('comment_id','desc');
              $comment_info = $this->master_model->getRecords("arai_qa_forum_comments qac", array('qac.deleted_on' => NULL,  'qac.user_id'=>$user_id ,'qac.q_id' => $question['q_id']),'qac.q_id,qac.comment,qac.created_on,comment_id');
              
              if (count($comment_info)>0) {
                foreach ($comment_info as $key2 => $value) {
                    $reply_info_arr = $this->master_model->getRecords("arai_qa_forum_comments qac", array('qac.deleted_on' => NULL,  'qac.user_id !='=>$user_id ,'qac.q_id' => $question['q_id'],'qac.parent_comment_id'=>$value['comment_id']),'qac.q_id,qac.comment,qac.created_on');
                    if (count($reply_info_arr)>0) {
                        $reply_info_temp[$key2]=$reply_info_arr;
                    }

                }
                $reply_info=$reply_info_temp;
                
                                  
              }

              $like_info = $this->master_model->getRecords("arai_qa_forum_likes qal", array('qal.user_id'=>$user_id ,'qal.q_id' => $question['q_id']),'qal.created_on');

                
          if (count($comment_info)>0) {
                $temp_active_qa[$key]['question']=$question['question'];
                $temp_active_qa[$key]['q_id']=$question['q_id'];
                $temp_active_qa[$key]['comments']=$comment_info;
                $temp_active_qa[$key]['reply']=$reply_info;
                
          }
         
         if (count($like_info)>0 ) {
            $like_date_time=$like_info[0]['created_on'];
            $temp_active_qa[$key]['question']=$question['question'];
            $temp_active_qa[$key]['q_id']=$question['q_id'];
            $temp_active_qa[$key]['like_info']=$like_date_time;
          }
        }  
        }
        // echo "<pre>";
        // print_r($temp_active_qa);die;
        $data['qa_active']=$temp_active_qa;
        $response['myspace_qa']=$view_res = $this->load->view('front/myspace/inc_qa', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }

    public function kr()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/kr';
        $data['module_title'] = 'kr';
        $this->load->view('front/front_combo', $data);
    }

    public function load_kr_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $keyword = $this->input->post('keyword');
        if ($is_search==1 && $inner_tab=='mykr' && $keyword!='') {
             
             $this->db->where(" ( k.kr_id_disp LIKE '%" . $keyword . "%' OR k.title_of_the_content LIKE '%" . $keyword . "%' OR k.kr_description LIKE '%" . $keyword . "%' OR k.kr_type_other LIKE '%" . $keyword . "%' OR k.author_name LIKE '%" . $keyword . "%' ) ");
        }    
        $this->db->order_by('k.kr_id','DESC',FALSE);
        $data['knowledge_repository'] = $knowledge_repository = $this->master_model->getRecords("knowledge_repository k", array('k.is_deleted' => '0', 'user_id'=>$user_id));

        if ($is_search==1 && $inner_tab=='myactivekr' && $keyword!='') {
            $this->db->where(" ( k.kr_id_disp LIKE '%" . $keyword . "%' OR k.title_of_the_content LIKE '%" . $keyword . "%' OR k.kr_description LIKE '%" . $keyword . "%' OR k.kr_type_other LIKE '%" . $keyword . "%' OR k.author_name LIKE '%" . $keyword . "%' ) ");
        }
            $this->db->select('k.*,kt.type_name');
            $this->db->join('arai_knowledge_repository_type_master kt','kt.id=k.kr_type','LEFT',FALSE);
        $data['kr_active'] = $kr_active = $this->master_model->getRecords("knowledge_repository k", array('k.admin_status' => '1','k.is_deleted' => '0','k.is_block'=>'0'));  

        $temp_active_kr=array();
        if(count($kr_active)>0){
        foreach ($kr_active as $key => $kr) {

                            $this->db->order_by('d_id','desc');
              $download_info = $this->master_model->getRecords("arai_knowledge_download_log dw", array('dw.user_id'=>$user_id ,'dw.kr_id' => $kr['kr_id']),'dw.d_id,dw.createdAt','',0,1);

              $like_info = $this->master_model->getRecords("arai_knowledge_repository_likes kl", array('kl.user_id'=>$user_id ,'kl.kr_id' => $kr['kr_id']),'kl.created_on');

                
          if (count($download_info)>0) {
                $temp_active_kr[$key]['title_of_the_content']=$kr['title_of_the_content'];
                $temp_active_kr[$key]['type']=$kr['type_name'];
                $temp_active_kr[$key]['kr_id']=$kr['kr_id'];
                $temp_active_kr[$key]['downloads']=$download_info;
          }
         
         if (count($like_info)>0) {
            $like_date_time=$like_info[0]['created_on'];
            $temp_active_kr[$key]['title_of_the_content']=$kr['title_of_the_content'];
            $temp_active_kr[$key]['type']=$kr['type_name'];
            $temp_active_kr[$key]['kr_id']=$kr['kr_id'];
            $temp_active_kr[$key]['like_info']=$like_date_time;
          }
        }  
        }
        // echo "<pre>"; print_r($temp_active_kr);die;
        $data['kr_active']=$temp_active_kr;
        $response['myspace_qa']=$view_res = $this->load->view('front/myspace/inc_kr', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }

    public function resource_sharing()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/resource_sharing';
        $data['module_title'] = 'resource_sharing';
        $this->load->view('front/front_combo', $data);
    }

    public function load_rs_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $keyword = $this->input->post('keyword');
        if ($is_search==1 && $inner_tab=='myrs' && $keyword!='') {
          
          
         $this->db->where(" ( r.id_disp LIKE '%" . $keyword . "%' OR r.resouce_name LIKE '%" . $keyword . "%' OR r.resource_details LIKE '%" . $keyword . "%' OR r.resource_specification LIKE '%" . $keyword . "%' OR r.owner_name LIKE '%" . $keyword . "%' OR c.city_name LIKE '%" . $keyword . "%' ) ");
        }    
        $this->db->select('r.*,c.city_name');   
        $this->db->order_by('r.r_id','DESC',FALSE);
        $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
        $data['resource_sharing'] = $resource_sharing = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'user_id'=>$user_id) );

        if ($is_search==1 && $inner_tab=='myactivers' && $keyword!='') {
                
            $this->db->where(" ( r.id_disp LIKE '%" . $keyword . "%' OR r.resouce_name LIKE '%" . $keyword . "%' OR r.resource_details LIKE '%" . $keyword . "%' OR r.resource_specification LIKE '%" . $keyword . "%' OR r.owner_name LIKE '%" . $keyword . "%' OR c.city_name LIKE '%" . $keyword . "%' ) ");
        }
            $this->db->select('r.*,c.city_name');
            $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
        $data['res_active'] = $res_active = $this->master_model->getRecords("arai_resource_sharing r");  
        // $data['res_active'] = $res_active = $this->master_model->getRecords("arai_resource_sharing r", array('r.admin_status' => '1','r.is_deleted' => '0','r.is_block'=>'0'));  


        $temp_active_rs=array();
        if(count($res_active)>0){
        foreach ($res_active as $key => $res) {

                            $this->db->order_by('connect_id','desc');
              $connect_req_info = $this->master_model->getRecords("arai_resource_sharing_connect_request cr", array('cr.user_id'=>$user_id ,'cr.r_id' => $res['r_id']),'cr.app_id,cr.connect_id,cr.created_on,cr.comments','',0,1);

              $like_info = $this->master_model->getRecords("arai_resource_sharing_likes rl", array('rl.user_id'=>$user_id ,'rl.r_id' => $res['r_id']),'rl.created_on');

                
          if (count($connect_req_info)>0) {
                $temp_active_rs[$key]['resouce_name']=$res['resouce_name'];
                $temp_active_rs[$key]['res_id']=$res['id_disp'];
                $temp_active_rs[$key]['r_id']=$res['r_id'];
                $temp_active_rs[$key]['connect_req']=$connect_req_info;
          }
         
         if (count($like_info)>0) {
            $like_date_time=$like_info[0]['created_on'];
            $temp_active_rs[$key]['resouce_name']=$res['resouce_name'];
            $temp_active_rs[$key]['res_id']=$res['id_disp'];
            $temp_active_rs[$key]['r_id']=$res['r_id'];
            $temp_active_rs[$key]['like_info']=$like_date_time;
          }
        }  
        }
        // echo "<pre>"; print_r($temp_active_rs);die;
        $data['res_active']=$temp_active_rs;
        $response['myspace_rs']=$view_res = $this->load->view('front/myspace/inc_resource_sharing', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }

     public function technology_transfer()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/technology_transfer';
        $data['module_title'] = 'technology_transfer';
        $this->load->view('front/front_combo', $data);
    }

    public function load_tt_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $keyword = $this->input->post('keyword');
        if ($is_search==1 && $inner_tab=='mytt' && $keyword!='') {
         $this->db->where(" ( t.id_disp LIKE '%" . $keyword . "%' OR t.technology_title LIKE '%" . $keyword . "%' OR t.author_name LIKE '%" . $keyword . "%' OR t.company_profile LIKE '%" . $keyword . "%' OR t.technology_abstract LIKE '%" . $keyword . "%' OR t.technology_value LIKE '%" . $keyword . "%' OR t.important_features LIKE '%" . $keyword . "%' OR t.application LIKE '%" . $keyword . "%' OR t.intellectual_property LIKE '%" . $keyword . "%' OR t.tag_other LIKE '%" . $keyword . "%') ");
        }    
        $this->db->order_by('t.tech_id','DESC',FALSE);
        $data['technology_transfer'] = $technology_transfer = $this->master_model->getRecords("arai_technology_transfer t",array('t.is_deleted' => '0','user_id'=>$user_id) );

        if ($is_search==1 && $inner_tab=='myactivett' && $keyword!='') {
                
            $this->db->where(" ( t.id_disp LIKE '%" . $keyword . "%' OR t.technology_title LIKE '%" . $keyword . "%' OR t.author_name LIKE '%" . $keyword . "%' OR t.company_profile LIKE '%" . $keyword . "%' OR t.technology_abstract LIKE '%" . $keyword . "%' OR t.technology_value LIKE '%" . $keyword . "%' OR t.important_features LIKE '%" . $keyword . "%' OR t.application LIKE '%" . $keyword . "%' OR t.intellectual_property LIKE '%" . $keyword . "%' OR t.tag_other LIKE '%" . $keyword . "%') ");
        }
            
      
        $data['tech_active'] = $tech_active = $this->master_model->getRecords("arai_technology_transfer t", array('t.admin_status' => '1','t.is_deleted' => '0','t.is_block'=>'0'));  


        $temp_active_tt=array();
        if(count($tech_active)>0){
        foreach ($tech_active as $key => $res) {

                            $this->db->order_by('connect_id','desc');
              $connect_req_info = $this->master_model->getRecords("arai_technology_transfer_connect_request cr", array('cr.user_id'=>$user_id ,'cr.tech_id' => $res['tech_id']),'cr.app_id,cr.connect_id,cr.created_on,cr.comments','',0,1);
                
          if (count($connect_req_info)>0) {
                $temp_active_tt[$key]['technology_title']=$res['technology_title'];
                $temp_active_tt[$key]['id_disp']=$res['id_disp'];
                $temp_active_tt[$key]['tech_id']=$res['tech_id'];
                $temp_active_tt[$key]['connect_req']=$connect_req_info;
          }
        
        }  
        }
        // echo "<pre>"; print_r($temp_active_tt);die;
        $data['tech_trans_active']=$temp_active_tt;
        $response['myspace_tt']=$view_res = $this->load->view('front/myspace/inc_technology_transfer', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }
    
    
    public function webinar()
    {
        $data['page_title']='My space';
        $data['middle_content'] = 'myspace/webinar';
        $data['module_title'] = 'webinar';
        $this->load->view('front/front_combo', $data);
    }

    public function load_webinar_ajax()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($_POST) && count($_POST) > 0) {
        $is_search = $this->input->post('is_search');
        $inner_tab = $this->input->post('inner_tab');
        $nested_tab = $this->input->post('nested_tab');
        $keyword = $this->input->post('keyword');
        
        $this->db->order_by('w.w_id','DESC',FALSE);
        $data['webinars'] = $webinars = $this->master_model->getRecords('arai_webinar w',array('user_id'=>$user_id));

        if ($is_search==1 && $inner_tab=='myweb' && $nested_tab=='my_exclusive' && $keyword!='') {
         $this->db->where(" ( w.webinar_name LIKE '%" . $keyword . "%' ) ");
        }    
        $this->db->order_by('w.w_id','DESC',FALSE);
        $data['exclusive_webinars'] = $exclusive_webinars = $this->master_model->getRecords('arai_webinar w',array('w.user_id'=>$user_id,'w.exclusive_technovuus_event'=>'Yes','w.is_deleted'=>0));

        if ($is_search==1 && $inner_tab=='myweb' && $nested_tab=='my_nonexclusive' && $keyword!='') {
            $this->db->where(" ( w.webinar_name LIKE '%" . $keyword . "%' ) ");
        }  
        $this->db->order_by('w.w_id','DESC',FALSE);
        $data['non_exclusive_webinars'] = $non_exclusive_webinars = $this->master_model->getRecords('arai_webinar w',array('w.user_id'=>$user_id,'w.exclusive_technovuus_event'=>'No','w.is_deleted'=>0));


        //ACTIVE WEBINARS-EXCLUSIVE
        if ($is_search==1 && $inner_tab=='myactiveweb' && $nested_tab=='my_exclusive' && $keyword!='') {
            $this->db->where(" ( w.webinar_name LIKE '%" . $keyword . "%' ) ");
        }
        $data['webinar_active_exclusive'] = $webinar_active_exclusive = $this->master_model->getRecords("arai_webinar w", array('w.is_deleted' => '0','admin_status' => 'Approved','w.exclusive_technovuus_event'=>'Yes'));  
        $temp_exclusive_active_web=array();
        if(count($webinar_active_exclusive)>0){
        foreach ($webinar_active_exclusive as $key => $res) {

                            $this->db->order_by('id','desc');
              $connect_req_info = $this->master_model->getRecords("arai_webinar_attendence wa", array('wa.user_id'=>$user_id ,'wa.w_id' => $res['w_id']),'wa.id,wa.createdAt','',0,1);
                
          if (count($connect_req_info)>0) {
                $temp_exclusive_active_web[$key]['webinar_name']=$res['webinar_name'];
                $temp_exclusive_active_web[$key]['webinar_id']=$res['webinar_id'];
                $temp_exclusive_active_web[$key]['w_id']=$res['w_id'];
                $temp_exclusive_active_web[$key]['webinar_date']=$res['webinar_date'];
                $temp_exclusive_active_web[$key]['webinar_start_time']=$res['webinar_start_time'];
                $temp_exclusive_active_web[$key]['webinar_end_time']=$res['webinar_end_time'];
                $temp_exclusive_active_web[$key]['connect_req']=$connect_req_info;
                $temp_exclusive_active_web[$key]['hosting_link']=$res['hosting_link'];
            }
        
            }  
        }
    
    //END-ACTIVE WEBINARS-EXCLUSIVE

     if ($is_search==1 && $inner_tab=='myactiveweb' && $nested_tab=='my_nonexclusive' && $keyword!='') {
            $this->db->where(" ( w.webinar_name LIKE '%" . $keyword . "%' ) ");
        }
        $data['webinar_active_nonexclusive'] = $webinar_active_nonexclusive = $this->master_model->getRecords("arai_webinar w", array('w.is_deleted' => '0','admin_status' => 'Approved','w.exclusive_technovuus_event'=>'No'));  
        $temp_nonexclusive_active_web=array();
        if(count($webinar_active_nonexclusive)>0){
        foreach ($webinar_active_nonexclusive as $key => $res) {

                            $this->db->order_by('id','desc');
              $connect_req_info = $this->master_model->getRecords("arai_webinar_attendence wa", array('wa.user_id'=>$user_id ,'wa.w_id' => $res['w_id']),'wa.id,wa.createdAt','',0,1);
                
          if (count($connect_req_info)>0) {
                $temp_nonexclusive_active_web[$key]['webinar_name']=$res['webinar_name'];
                $temp_nonexclusive_active_web[$key]['webinar_id']=$res['webinar_id'];
                $temp_nonexclusive_active_web[$key]['w_id']=$res['w_id'];
                $temp_nonexclusive_active_web[$key]['webinar_date']=$res['webinar_date'];
                $temp_nonexclusive_active_web[$key]['webinar_start_time']=$res['webinar_start_time'];
                $temp_nonexclusive_active_web[$key]['webinar_end_time']=$res['webinar_end_time'];
                $temp_nonexclusive_active_web[$key]['hosting_link']=$res['hosting_link'];
                $temp_nonexclusive_active_web[$key]['registration_link']=$res['registration_link'];
                $temp_nonexclusive_active_web[$key]['connect_req']=$connect_req_info;

            }
        
            }  
        }
        // echo "<pre>"; print_r($temp_nonexclusive_active_web);die;
        $data['webinar_exclusive_active']=$temp_exclusive_active_web;
        $data['webinar_nonexclusive_active']=$temp_nonexclusive_active_web;
        $response['myspace_tt']=$view_res = $this->load->view('front/myspace/inc_webinar', $data,true);
        $response['flag']='success';
        $response['keyword']=$keyword;
        $response['inner_tab']=$inner_tab;
        $response['is_search']=$is_search;
    }else{
        $response['flag']='error';
    }
        echo json_encode($response);

    }


}