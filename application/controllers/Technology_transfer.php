<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Technology_transfer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model_sm');
        $this->load->library('Opensslencryptdecrypt');
        $this->load->helper('auth');
        $this->load->helper('text');
        // $this->check_permissions->is_logged_in();
        ini_set('upload_max_filesize', '100M');
        ini_set('post_max_size', '100M');

        $this->login_user_id = $this->session->userdata('user_id');
        // if($this->login_user_id == "") { redirect(site_url()); }

        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
    }

    public function index()
    {
        //$module_id = 8;
        //$this->check_permissions->is_authorise($module_id);
        $this->check_permissions->is_logged_in();
        $this->check_permissions->is_authorise(39);     
        $encrptopenssl                   = new Opensslencryptdecrypt();
        $data['featured_tech_limit']     = $featured_tech_limit     = 15;
        $data['non_featured_tech_limit'] = $non_featured_tech_limit = 10;
        $data['captcha']                 = $this->create_captcha();

        /*$this->db->order_by('technology_name', 'ASC');
        $this->db->order_by("FIELD(technology_name, 'other')", "DESC", false);
        $data['technology_data'] = $this->master_model->getRecords("arai_blog_technology_master", array("status" => 'Active'));*/

        $this->db->order_by('tag_name', 'ASC');
        $this->db->order_by("FIELD(tag_name, 'Other')", "DESC", false);
        $data['tag_data'] = $this->master_model->getRecords("arai_technology_transfer_tags_master", array("status" => 'Active'));

        $this->db->order_by('xOrder', 'ASC');
        $this->db->order_by("FIELD(trl_name, 'other')", "DESC", false);
        $data['trl_data'] = $this->master_model->getRecords("arai_technology_transfer_tlr_master", array("status" => 'Active'));

        $data['page_title']     = 'Blogs';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technology_transfer/index';
        $this->load->view('front/front_combo', $data);
    }

    public function getTechDataAjax()
    {

        $encrptopenssl                                = new Opensslencryptdecrypt();
        $csrf_new_token                               = $this->security->get_csrf_hash();
        $result['csrf_new_token']                     = $csrf_new_token;
        $data_non_featured['featured_tech_limit']     = $featured_tech_limit     = 15;
        $data_non_featured['non_featured_tech_limit'] = $non_featured_tech_limit = 10;
        $search_str                                   = '';

        if (isset($_POST) && count($_POST) > 0) {
            $f_start      = $this->input->post('f_start', true);
            $f_limit      = $this->input->post('f_limit', true);
            $nf_start     = $this->input->post('nf_start', true);
            $nf_limit     = $this->input->post('nf_limit', true);
            $is_show_more = $this->input->post('is_show_more', true);
            if ($f_start != "" && $f_limit != "" && $nf_start != "" && $nf_limit != "" && $is_show_more != "") {
                $result['flag'] = "success";
                $user_id        = $this->session->userdata('user_id');
                $TechActiondata = $this->getTechActiondata($user_id);

                //START : FOR SEARCH KEYWORD
                $keyword = $this->security->xss_clean(filter_var(trim($this->input->post('keyword'))), FILTER_SANITIZE_STRING); //trim($this->input->post('keyword'));
                if ($keyword) {
                    $search_str .= " ( t.id_disp LIKE '%" . $keyword . "%' OR t.technology_title LIKE '%" . $keyword . "%' OR t.author_name LIKE '%" . $keyword . "%' OR t.company_profile LIKE '%" . $keyword . "%' OR t.technology_abstract LIKE '%" . $keyword . "%' OR t.technology_value LIKE '%" . $keyword . "%' OR t.important_features LIKE '%" . $keyword . "%' OR t.application LIKE '%" . $keyword . "%' OR t.intellectual_property LIKE '%" . $keyword . "%' OR t.tag_other LIKE '%" . $keyword . "%') ";
                }
                //END : FOR SEARCH KEYWORD

                //START : FOR SEARCH TAG
                $tag = trim($this->input->post('tag'));
                if ($tag != '') {
                    $tag_arr = explode(",", $tag);
                    if (count($tag_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($tag_arr as $res) {
                            $search_str .= " FIND_IN_SET('" . $res . "',t.tags)";
                            if ($i != (count($tag_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }
                }
                //END : FOR SEARCH TAG

                $trl = trim($this->input->post('trl'));
                if ($trl != '') {

                    $trl_arr = explode(",", $trl);
                    if (count($trl_arr) > 0) {
                        $old_search_str = $search_str;
                        if ($old_search_str != '') {$search_str .= " AND ";}

                        $search_str .= " (";
                        $i = 0;
                        foreach ($trl_arr as $res) {
                            $search_str .= " t.trl =  " . $res . "  ";
                            if ($i != (count($trl_arr) - 1)) {$search_str .= " OR ";}
                            $i++;
                        }
                        $search_str .= " )";
                    }

                }

                // START : FOR FEATURED BLOG & TRENDING BLOGS
                $featured_tech_total_cnt = 0;
                if ($is_show_more == 0) {
                    // START : FOR FEATURED BLOG
                    if ($search_str != "") {$this->db->where($search_str);}
                    // $this->db->order_by('tech_id', 'DESC', false);
                    $this->db->order_by('ISNULL(t.xOrder), t.xOrder = 0, t.xOrder, t.created_on DESC', '', false);
                    $select_f = "t.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_technology_transfer_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags,
                    (SELECT trl_name FROM arai_technology_transfer_tlr_master WHERE id=trl) AS DispTrl, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                    $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
                    $this->db->join('arai_profile_organization org', 'org.user_id = t.user_id', 'LEFT', false);
                    $this->db->join('arai_student_profile sp', 'sp.user_id = t.user_id', 'LEFT', false);
                    $data_featured['featured_tech'] = $featured_tech = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_featured' => '1', 't.is_block' => '0'), $select_f, '', $f_start, $f_limit);
                    // echo $this->db->last_query();die;
                    $data_featured['qry']              = $this->db->last_query();
                    $featured_tech_total_cnt           = count($featured_tech);
                    $data_featured['TechActiondata']   = $TechActiondata;
                    $result['featured_tech_total_cnt'] = count($featured_tech);
                    //$result['featured_tech_qry'] = $this->db->last_query();
                    $result['Featured_response'] = $this->load->view('front/technology_transfer/incFeatureTechCommon', $data_featured, true);
                    // END : FOR FEATURED BLOG

                }
                // START : FOR FEATURED BLOG & TRENDING BLOGS

                // START : FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
                // $this->db->order_by('tech_id', 'DESC', false);
                $this->db->order_by('ISNULL(t.xOrder), t.xOrder = 0, t.xOrder, t.created_on DESC', '', false);
                $select_nf = "t.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_technology_transfer_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags,(SELECT trl_name FROM arai_technology_transfer_tlr_master WHERE id=trl) AS DispTrl,r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
                $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
                $this->db->join('arai_profile_organization org', 'org.user_id = t.user_id', 'LEFT', false);
                $this->db->join('arai_student_profile sp', 'sp.user_id = t.user_id', 'LEFT', false);
                $data_non_featured['non_featured_tech'] = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_featured' => '0', 't.is_block' => '0'), $select_nf, '', $nf_start, $nf_limit);
                $data_non_featured['TechActiondata']    = $TechActiondata;
                // echo "<pre>";
                // print_r($data_non_featured);die;
                //$result['non_featured_tech_qry'] = $this->db->last_query();

                //START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG
                if ($search_str != "") {$this->db->where($search_str);}
                $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
                $data_non_featured['total_non_featured_tech_count'] = $total_non_featured_tech_count = $this->master_model->getRecordCount('arai_technology_transfer t', array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_featured' => '0', 't.is_block' => '0'), 't.tech_id');
                //END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT FOR NON FEATURED BLOG xOrder

                $data_non_featured['featured_tech_total_cnt'] = $featured_tech_total_cnt;
                $data_non_featured['new_start']               = $new_start               = $nf_start + $nf_limit;
                $result['non_featured_tech_total_cnt']        = $total_non_featured_tech_count;
                $result['NonFeatured_response']               = $this->load->view('front/technology_transfer/incNonFeatureTechCommon', $data_non_featured, true);
                // END : FOR NON FEATURED BLOG
            } else {
                $result['flag'] = "error";
            }
        } else {
            $result['flag'] = "error";
        }

        echo json_encode($result);
    }

    public function details($tech_id = 0, $comment_flag = 0) //BLOG DETAILS PAGE

    {
        $this->check_permissions->is_logged_in();
        $this->check_permissions->is_authorise(39);
        $encrptopenssl   = new Opensslencryptdecrypt();
        $data['user_id'] = $user_id = $this->session->userdata('user_id');

        if ($tech_id == '0') {redirect(site_url('technology_transfer'));} else {
            $tech_id = base64_decode($tech_id);

            $data['tech_id'] = $tech_id;

            $select = "t.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_technology_transfer_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags,(SELECT trl_name FROM arai_technology_transfer_tlr_master WHERE id=trl) AS DispTrl, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
            $this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
            $this->db->join('arai_profile_organization org', 'org.user_id = t.user_id', 'LEFT', false);
            $this->db->join('arai_student_profile sp', 'sp.user_id = t.user_id', 'LEFT', false);
            $data['form_data'] = $form_data = $this->master_model->getRecords("arai_technology_transfer t", array('t.tech_id' => $tech_id, 't.is_deleted' => '0'), $select);

            if (count($form_data)==0 ) {
                redirect(base_url('technology_transfer'));
            }

            //, 'b.admin_status' => '1', 'b.is_block' => '0'

            // echo "<pre>"; print_r($form_data); echo "</pre>";die;

            $redirect_flag = 0;
            if (count($form_data) == 0) {$redirect_flag = 1;} else if ($form_data[0]['admin_status'] != 1 && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;} else if ($form_data[0]['is_block'] == '1' && $form_data[0]['user_id'] != $user_id) {$redirect_flag = 1;}

            if ($redirect_flag == 1) {redirect(site_url('technology_transfer'));}
        }
        $data['files'] = $files = $this->master_model->getRecords('technology_transfer_files', array("tech_id" => $tech_id, "is_deleted" => '0'), 'file_name,file_id');

        $totalSize = 0;
        foreach ($files as $file) {
            $path = "uploads/kr_files/" . $file['file_name'];
            if (file_exists($path)) {
                $totalSize += filesize($path);
            }

        }

        $data['video_data'] = $video_data = $this->master_model->getRecords('technology_transfer_video_details', array("tech_id" => $tech_id, "is_deleted" => '0'), '', array('vid_id' => 'ASC'));

        $data['files_size']     = $this->formatSizeUnits($totalSize);
        $data['files_count']    = count($files);
        $data['TechActiondata'] = $this->getTechActiondata($user_id);

        $data['captcha']        = $this->create_captcha();
        $data['page_title']     = 'Technology Transfer Details';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technology_transfer/details';

        $this->load->view('front/front_combo', $data);
    }


    public function create_captcha($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('ttcaptcha');
        $this->session->set_userdata('ttcaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }

    public function check_captcha_ajax()
    {
        $code = $_POST['code'];

        if ($code == '' || $_SESSION["ttcaptcha"] != $code) {
            //$this->session->unset_userdata("ttcaptcha");
            //$this->form_validation->set_message('check_captcha', 'Invalid %s.');
            echo "false";
        }
        if ($_SESSION["ttcaptcha"] == $code && $this->session->userdata("used") == 0) {
            //$this->session->unset_userdata("ttcaptcha");
            echo "true";
        }

    }

    public function refresh()
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 17,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('ttcaptcha');
        $this->session->set_userdata('ttcaptcha', $captcha['word']);

        // Display captcha image
        echo $captcha['image'];
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function getTechActiondata($user_id = 0)
    {
        $like_arr = $reported_arr = $request_arr = $return_arr = array();
        

        $user_reported_tech = $this->master_model->getRecords("arai_technology_transfer_reported", array('user_id' => $user_id), 'tech_id');
        if (count($user_reported_tech) > 0) {
            foreach ($user_reported_tech as $res) {
                $reported_arr[$res['tech_id']] = $res['tech_id'];
            }
        }

        $user_request_tech = $this->master_model->getRecords("arai_technology_transfer_connect_request", array('user_id' => $user_id), 'tech_id');
        if (count($user_request_tech) > 0) {
            foreach ($user_request_tech as $res) {
                $request_arr[$res['tech_id']] = $res['tech_id'];
            }
        }

        //$return_arr['like_blog'] = $like_arr;
        //$return_arr['self_blog'] = $self_blog_arr;
        $return_arr['reported_tech'] = $reported_arr;
        $return_arr['requested_tech'] = $request_arr;
        return $return_arr;
    }

    public function add($id = 0)
    {
        $this->check_permissions->is_logged_in();
        $this->check_permissions->is_authorise(38);
        $data['page_title'] = 'Technology transfer';
        // Create Object
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');

        if ($id == '0') {$data['mode'] = $mode = "Add";} else {
            $id = base64_decode($id);

            $data['form_data'] = $form_data = $this->master_model->getRecords("technology_transfer", array('tech_id' => $id,'user_id' => $user_id, 'is_deleted' => 0));

            if (count($form_data) > 0) {
                $data['mode'] = $mode = "Update";
            } else { $data['mode'] = $mode = "Add";}

            $data['files_data'] = $files_data = $this->master_model->getRecords('technology_transfer_files', array("tech_id" => $id, "is_deleted" => '0'), '', array('file_id' => 'ASC'));

            $data['video_data'] = $video_data = $this->master_model->getRecords('technology_transfer_video_details', array("tech_id" => $id, "is_deleted" => '0'), '', array('vid_id' => 'ASC'));
            // echo "<pre>";
            // print_r($data);die;

        }

        $data['tile_image_error'] = $data['CustomVAlidationErr'] = $data['company_logo_error'] = $data['tags_error'] = $data['kr_type_error'] = $data['trl_error'] = $error_flag = '';
        $file_upload_flag         = 0;
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('technology_title', 'technology title', 'required|trim', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('author_name', 'author name', 'required|trim', array('required' => 'Please enter the %s'));

            $this->form_validation->set_rules('technology_abstract', 'technology abstract', 'required|trim', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('technology_value', 'technology value', 'required|trim', array('required' => 'Please enter the %s'));
            $this->form_validation->set_rules('commercialization_status', 'commercialization status', 'required|trim', array('required' => 'Please enter the %s'));

            if ($this->form_validation->run() && $error_flag == '') {
                $postArr = $this->input->post();

                if ($_FILES['tile_image']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $tile_image = $this->Common_model_sm->upload_single_file("tile_image", array('png', 'jpg', 'jpeg', 'gif'), "tile_image_" . date("YmdHis"), "./uploads/technology_transfer/tile_image", "png|jpeg|jpg|gif");
                    if ($tile_image['response'] == 'error') {
                        $data['tile_image_error'] = $tile_image['message'];
                        $file_upload_flag         = 1;
                    } else if ($tile_image['response'] == 'success') {
                        $add_data['tile_image'] = $tile_image['message'];
                    }
                }

                if ($_FILES['company_logo']['name'] != "") //UPLOAD BLOG BANNER IMAGE
                {
                    $company_logo = $this->Common_model_sm->upload_single_file("company_logo", array('png', 'jpg', 'jpeg', 'gif'), "company_logo_" . date("YmdHis"), "./uploads/technology_transfer/company_logo", "png|jpeg|jpg|gif");
                    if ($company_logo['response'] == 'error') {
                        $data['company_logo_error'] = $company_logo['message'];
                        $file_upload_flag           = 1;
                    } else if ($company_logo['response'] == 'success') {
                        $add_data['company_logo'] = $company_logo['message'];
                    }
                }

                $add_data['user_id']             = $user_id;
                $add_data['technology_title']    = $this->input->post('technology_title');
                $add_data['author_name']         = $this->input->post('author_name');
                $add_data['company_profile']     = stripslashes($this->input->post('company_profile'));
                $add_data['technology_abstract'] = stripslashes($this->input->post('technology_abstract'));

                $add_data['technology_value']         = stripslashes($this->input->post('technology_value'));
                $add_data['important_features']       = stripslashes($this->input->post('important_features'));
                $add_data['application']              = stripslashes($this->input->post('application'));
                $add_data['intellectual_property']    = stripslashes($this->input->post('intellectual_property'));
                $add_data['beneficiary_industry']     = stripslashes($this->input->post('beneficiary_industry'));
                $add_data['advantages']               = stripslashes($this->input->post('advantages'));
                $add_data['commercialization_status'] = stripslashes($this->input->post('commercialization_status'));
                $add_data['include_files']            = $this->input->post('include_files');
                $add_data['video_check']              = $this->input->post('video_check');
                $add_data['trl']                      = $this->input->post('trl');
                $add_data['tags']                     = implode(",", $this->input->post('tags'));

                $tag_other = trim($this->security->xss_clean($this->input->post('tag_other')));
                if (isset($tag_other) && $tag_other != "") {
                    $add_data['tag_other'] = trim($this->security->xss_clean($tag_other));
                    if ($mode=='Add') {
                        $this->check_permissions->StoreOtherData($tag_other, 'arai_technology_transfer_tags_master','tag_name');
                        }
                } else { $add_data['tag_other'] = '';}

                if ($mode == 'Add') {
                    $add_data['is_featured']  = 0;
                    $add_data['admin_status'] = 0;
                    $add_data['is_deleted']   = 0;
                    $add_data['is_block']     = 0;
                    $add_data['created_on']   = date('Y-m-d H:i:s');

                    $last_id            = $this->master_model->insertRecord('technology_transfer', $add_data, true);
                    $up_data['id_disp'] = $disp_id = "TNTTID-" . sprintf("%07d", $last_id);
                    $this->master_model->updateRecord('technology_transfer', $up_data, array('tech_id' => $last_id));

                    $email_info = $this->get_mail_data($last_id);

                    // START: MAIL TO USER
                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_submit');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[OWNER_NAME]',
                            '[TECHNOLOGY_TRANSFER_NAME]',
                            '[ID]',
                            '[DD/MM/YYYY]',
                            '[HH:MM:SS]',

                        ];
                        $rep_array = [
                            $email_info['OWNER_NAME'],
                            $email_info['TECHNOLOGY_TRANSFER_NAME'],
                            $email_info['ID'],
                            $email_info['DD/MM/YYYY'],
                            $email_info['HH:MM:SS'],

                        ];

                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['OWNER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    // END: MAIL TO USER

                    // START: MAIL TO ADMIN
                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_admin_on_submit');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[OWNER_NAME]',
                            '[TECHNOLOGY_TRANSFER_NAME]',
                            '[ID]',
                            '[DD/MM/YYYY]',
                            '[HH:MM:SS]',

                        ];
                        $rep_array = [
                            $email_info['OWNER_NAME'],
                            $email_info['TECHNOLOGY_TRANSFER_NAME'],
                            $email_info['ID'],
                            $email_info['DD/MM/YYYY'],
                            $email_info['HH:MM:SS'],

                        ];

                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['admin_email'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    // END: MAIL TO ADMIN

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $user_id,
                        'action_name' => "Add technology transfer",
                        'module_name' => 'Front technology transfer',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Content is sent for Admin approval. Your ' . $disp_id);
                } else if ($mode == 'Update') {

                    $add_data['admin_status'] = 0;
                    $add_data['updated_on']   = date('Y-m-d H:i:s');

                    $last_id = $id;

                    $this->master_model->updateRecord('technology_transfer', $add_data, array('tech_id' => $last_id));

                    $email_info = $this->get_mail_data($last_id);

                    // START: MAIL TO USER ON UPDATE
                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_update_content');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[OWNER_NAME]',
                            '[TECHNOLOGY_TRANSFER_NAME]',
                            '[ID]',
                            '[DD/MM/YYYY]',
                            '[HH:MM:SS]',

                        ];
                        $rep_array = [
                            $email_info['OWNER_NAME'],
                            $email_info['TECHNOLOGY_TRANSFER_NAME'],
                            $email_info['ID'],
                            $email_info['DD/MM/YYYY'],
                            $email_info['HH:MM:SS'],

                        ];

                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['OWNER_EMAIL'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    // END: MAIL TO USER ON UPDATE

                    // START: MAIL TO ADMIN ON UPDATE
                    $email_send      = '';
                    $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_admin_on_update_content');
                    $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                    $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                    $sub_content     = '';

                    if (count($subscriber_mail) > 0) {
                        $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                        $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                        $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                        $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                        $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                        $arr_words = [
                            '[OWNER_NAME]',
                            '[TECHNOLOGY_TRANSFER_NAME]',
                            '[ID]',
                            '[DD/MM/YYYY]',
                            '[HH:MM:SS]',

                        ];
                        $rep_array = [
                            $email_info['OWNER_NAME'],
                            $email_info['TECHNOLOGY_TRANSFER_NAME'],
                            $email_info['ID'],
                            $email_info['DD/MM/YYYY'],
                            $email_info['HH:MM:SS'],

                        ];

                        $sub_content = str_replace($arr_words, $rep_array, $desc);

                        $info_array = array(
                            'to'      => $email_info['admin_email'],
                            'cc'      => '',
                            'from'    => $fromadmin,
                            'subject' => $subject_title,
                            'view'    => 'common-file',
                        );

                        $other_infoarray = array('content' => $sub_content);
                        $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                    }
                    // END: MAIL TO ADMIN  ON UPDATE

                    //START MAIL TO MEMBERS
                    $this->db->select("r.email,r.first_name,r.last_name,r.title");
                    $this->db->join("arai_registration r", "r.user_id = sa.user_id", "INNER", false);
                    $applicant_data = $this->master_model->getRecords('arai_technology_transfer_connect_request sa', array("sa.tech_id" => $last_id));

                    if (count($applicant_data) > 0) {

                        $email_send      = '';
                        $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_applicant_on_update_content');
                        $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                        $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                        $sub_content     = '';

                        if (count($subscriber_mail) > 0) {
                            $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                            $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                            $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                            $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                            $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                            $arr_words = [
                                '[OWNER_NAME]',
                                '[TECHNOLOGY_TRANSFER_NAME]',
                                '[ID]',
                                '[DD/MM/YYYY]',
                                '[HH:MM:SS]',

                            ];

                            $rep_array = [
                                $email_info['OWNER_NAME'],
                                $email_info['TECHNOLOGY_TRANSFER_NAME'],
                                $email_info['ID'],
                                $email_info['DD/MM/YYYY'],
                                $email_info['HH:MM:SS'],

                            ];
                            $sub_content = str_replace($arr_words, $rep_array, $desc);
                        }

                        foreach ($applicant_data as $res) {
                            $full_name      = $encrptopenssl->decrypt($res['title']) . " " . $encrptopenssl->decrypt($res['first_name']) . " " . $encrptopenssl->decrypt($res['last_name']);
                            $sub_content    = str_replace("[APPLICANT_NAME]", $full_name, $sub_content);
                            $applicant_mail = $encrptopenssl->decrypt($res['email']);
                            $info_array     = array(
                                // 'to' =>  $applicant_mail,
                                'to'      => $applicant_mail,
                                'cc'      => '',
                                'from'    => $fromadmin,
                                'subject' => $subject_title,
                                'view'    => 'common-file',
                            );

                            $other_infoarray = array('content' => $sub_content);
                            $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                        }
                    }

                    //END MAIL TO MEMBERS

                    // Log Data Added
                    $filesData        = $_FILES;
                    $json_data        = array_merge($postArr, $filesData);
                    $json_encode_data = json_encode($json_data);
                    $ipAddr           = $this->get_client_ip();
                    $createdAt        = date('Y-m-d H:i:s');
                    $logDetails       = array(
                        'user_id'     => $user_id,
                        'action_name' => "Update Resource",
                        'module_name' => 'Resource Sharing Front',
                        'store_data'  => $json_encode_data,
                        'ip_address'  => $ipAddr,
                        'createdAt'   => $createdAt,
                    );
                    $logData = $this->master_model->insertRecord('logs', $logDetails);

                    $this->session->set_flashdata('success', 'Content has been successfully updated');
                }

                if ($this->input->post('video_check') == 1) {

                    $this->master_model->updateRecord('technology_transfer_video_details', array('is_deleted' => '1'), array('tech_id' => $id));

                    $caption_arr   = $this->input->post('video_caption');
                    $video_url_arr = $this->input->post('video_url');
                    if (count($caption_arr)) {
                        foreach ($caption_arr as $key => $value) {
                            $add_video_info['tech_id']       = $last_id;
                            $add_video_info['video_caption'] = $caption_arr[$key];
                            $add_video_info['video_url']     = $video_url_arr[$key];
                            $add_video_info['created_on']    = date("Y-m-d H:i:s");

                            $this->master_model->insertRecord('technology_transfer_video_details', $add_video_info, true);
                        }
                    }
                }
                //START : INSERT  FILES
                if ($last_id != "") {
                    $tech_files = $_FILES['tech_files'];
                    if (count($tech_files) > 0) {
                        // echo "<pre>";print_r($tech_files);die;
                        for ($i = 0; $i < count($tech_files['name']); $i++) {
                            if ($tech_files['name'][$i] != '') {
                                $tech_file = $this->Common_model_sm->upload_single_file('tech_files', array('png', 'jpg', 'jpeg', 'gif', 'pdf', 'xls', 'xlsx', 'doc', 'docx', 'pptx', 'ppt'), "admin_file_" . $last_id . "_" . date("YmdHis") . rand(), "./uploads/technology_transfer/tech_files", "png|jpeg|jpg|gif|pdf|xls|xlsx|doc|docx|pptx|ppt", '1', $i);

                                if ($tech_file['response'] == 'error') {} else if ($tech_file['response'] == 'success') {
                                    $add_file_data['user_id']            = $user_id;
                                    $add_file_data['tech_id']            = $last_id;
                                    $add_file_data['file_name']          = ($tech_file['message']);
                                    $add_file_data['file_original_name'] = $tech_files['name'][$i];
                                    $add_file_data['created_on']         = date("Y-m-d H:i:s");
                                    $this->master_model->insertRecord('arai_technology_transfer_files', $add_file_data, true);
                                    // $kr_files_data[] = $add_file_data; //FOR LOG
                                }
                            }
                        }
                    }
                }
                //END : INSERT  FILES

                redirect(site_url('technology_transfer/my_technology_transfer'));

            }
            // echo validation_errors();die;
        }

        // $this->db->order_by('xOrder', 'ASC');
        // $this->db->order_by("FIELD(trl_name, 'other')", "DESC", false);
        $this->db->order_by('FIELD(trl_name, "Other") ASC, trl_name ASC');
        $data['trl_data'] = $this->master_model->getRecords("arai_technology_transfer_tlr_master", array("status" => 'Active'));
        // print_r($data['trl_data']);die;/
         $this->db->order_by('FIELD(tag_name, "Other") ASC, tag_name ASC');
        $data['tag_data'] = $this->master_model->getRecords("arai_technology_transfer_tags_master", array("status" => 'Active'));

        $data['middle_content'] = 'technology_transfer/add';
        $this->load->view('front/front_combo', $data);
    }

    public function my_technology_transfer() // MY BLOGS LIST

    {
        $this->check_permissions->is_logged_in();
        $user_id                = $this->session->userdata('user_id');
        $data['page_title']     = 'Technology transfer';
        $data['submodule_name'] = '';
        $data['middle_content'] = 'technology_transfer/listing';
        $this->load->view('front/front_combo', $data);
    }

    public function my_listing() // MY BLOGS LIST : GET SERVER SIDE DATATABLE DATA USING AJAX

    {
        $this->check_permissions->is_logged_in();
        $csrf_test_name  = $this->security->get_csrf_hash();
        $user_id         = $this->session->userdata('user_id');
        $draw            = $_POST['draw'];
        $row             = $_POST['start'];
        $rowperpage      = $_POST['length']; // Rows display per page
        $columnIndex     = @$_POST['order'][0]['column']; // Column index
        $columnName      = @$_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = @$_POST['order'][0]['dir']; // asc or desc
        $searchValue     = @$_POST['search']['value']; // Search value
        $pageNo          = ($_POST['start'] / $_POST['length']) + 1;

        $condition = "SELECT * FROM arai_technology_transfer WHERE is_deleted = '0' AND user_id = '" . $user_id . "' ORDER BY tech_id DESC";
        $query     = $condition . ' LIMIT  ' . $this->input->post('length') . ' OFFSET ' . $this->input->post('start');
        $result    = $this->db->query($query)->result();
        $rowCount  = $this->getNumData($condition);
        $rowCnt    = 0;
        $dataArr   = array();

        if ($rowCount > 0) {
            $no = $_POST['start'];
            $i  = 0;
            foreach ($result as $showResult) {
                $tech_id          = $showResult->tech_id;
                $id_disp          = '<a href="' . base_url('technology_transfer/details/' . base64_encode($tech_id)) . '" class="applicant-listing">' . $showResult->id_disp . '</a>';
                $technology_title = ucwords($showResult->technology_title);
                $author_name      = $showResult->author_name;
                $posted_on        = date("d M, Y h:i a", strtotime($showResult->created_on));

                $admin_status = $action = '';

                if ($showResult->admin_status == 0) {$admin_status = 'Pending';} else if ($showResult->admin_status == 1) {$admin_status = 'Approved';} else if ($showResult->admin_status == 2) {$admin_status = 'Rejected';}

                $action .= '<a href="' . base_url('technology_transfer/add/' . base64_encode($tech_id)) . '" data-toggle="tooltip" title="Edit" class="btn btn-info btn-green-fresh"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                if ($showResult->is_block == '0') {
                    $b_title = 'block';
                    $b_icon  = 'fa fa-ban';} else if ($showResult->is_block == '1') {
                    $b_title = 'unblock';
                    $b_icon  = 'fa fa-unlock-alt';}
                $onclick_block_fun = "block_unblock_tt('" . base64_encode($tech_id) . "', '" . $b_title . "')";

                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="' . $b_title . '" class="btn btn-info btn-green-fresh" onclick="' . $onclick_block_fun . '"><i class="' . $b_icon . '" aria-hidden="true"></i></a>&nbsp;&nbsp;';

                $onclick_delete_fun = "delete_tt('" . base64_encode($tech_id) . "')";
                $action .= '<a href="javascript:void(0);" data-toggle="tooltip" title="Remove" class="btn btn-info btn-green-fresh" onclick="' . $onclick_delete_fun . '"><i class="fa fa-remove" aria-hidden="true"></i></a> ';

                $onclick_request_fun = "show_connect_requests('" . base64_encode($tech_id) . "')";
                $getTotalRequests    = $this->master_model->getRecordCount('arai_technology_transfer_connect_request', array('tech_id' => $tech_id), 'tech_id');
                $disp_requests       = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_request_fun . '">' . $getTotalRequests . '</a></div>';

                $onclick_report_fun = "show_tech_reports('" . base64_encode($tech_id) . "')";
                $getTotalReports    = $this->master_model->getRecordCount('arai_technology_transfer_reported', array('tech_id' => $tech_id), 'reported_id');

                $disp_reported = '<div class="text-center"><a href="javascript:void(0);" data-toggle="tooltip" title="Reports" onclick="' . $onclick_report_fun . '">' . $getTotalReports . '</a></div>';

                $i++;
                $dataArr[] = array(
                    $id_disp,
                    $technology_title,
                    // $author_name,
                    $disp_requests,
                    $disp_reported,
                    $posted_on,
                    $admin_status,
                    "<span style='white-space:nowrap;'>" . $action . "</span>",
                );
                $rowCnt   = $rowCount;
                $response = array(
                    "draw"            => $draw,
                    "recordsTotal"    => $rowCnt ? $rowCnt : 0,
                    "recordsFiltered" => $rowCnt ? $rowCnt : 0,
                    "data"            => $dataArr,
                );
                $response['token'] = $csrf_test_name;
            }
        } else {
            $rowCnt   = $rowCount;
            $response = array(
                "draw"            => $draw,
                "recordsTotal"    => 0,
                "recordsFiltered" => 0,
                "data"            => $dataArr,
            );
            $response['token'] = $csrf_test_name;
        }
        echo json_encode($response);

    } // End Function

    public function show_connect_requests_ajax() // SHOW REPORTED COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));

            $result['flag']    = "success";
            $result['tech_id'] = $tech_id;

            $this->db->order_by('tech_id', 'DESC', false);
            $select = "br.connect_id,br.app_id, br.tech_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = br.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_technology_transfer_connect_request br", array('br.tech_id' => $tech_id), $select);
            $result['response'] = $this->load->view('front/technology_transfer/incTechRequestedModal', $data, true);

            $tech_data            = $this->master_model->getRecords("arai_technology_transfer", array('tech_id' => $tech_id), "technology_title,id_disp");
            $result['blog_title'] = "Technology transfer title : " . $tech_data[0]['technology_title'];
            $result['id_disp']    = "Technology transfer ID : " . $tech_data[0]['id_disp'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

     public function show_tech_reported_ajax() // SHOW REPORTED COUNT

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id = trim($this->security->xss_clean(base64_decode($this->input->post('tech_id'))));

            $result['flag']  = "success";
            $result['tech_id'] = $tech_id;

            $this->db->order_by('tech_id', 'DESC', false);
            $select = "br.reported_id, br.tech_id, br.user_id, br.comments, br.created_on, r.user_category_id, r.title, r.first_name, r.middle_name, r.last_name, r.email";
            $this->db->join('arai_registration r', 'r.user_id = br.user_id', 'INNER', false);
            $data['likes_data'] = $likes_data = $this->master_model->getRecords("arai_technology_transfer_reported br", array('br.tech_id' => $tech_id), $select);
            $result['response'] = $this->load->view('front/technology_transfer/incTechReportedModal', $data, true);

            $blog_data            = $this->master_model->getRecords("technology_transfer", array('tech_id' => $tech_id), "technology_title");
            $result['blog_title'] = "Technology Transfer Reports : " . $blog_data[0]['technology_title'];
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function delete_tech_file_ajax()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $file_id       = $encrptopenssl->decrypt($this->input->post('file_id', true));

        $csrf_new_token           = $this->security->get_csrf_hash();
        $result['csrf_new_token'] = $csrf_new_token;

        $this->master_model->updateRecord('technology_transfer_files', array('is_deleted' => '1'), array('file_id' => $file_id));
        $result['flag']    = 'success';
        $result['file_id'] = $file_id;

        //START : INSERT LOG
        $postArr                   = $this->input->post();
        $json_encode_data          = json_encode($postArr);
        $logDetails['user_id']     = $this->login_user_id;
        $logDetails['module_name'] = 'Technology transfer : delete_tech_file_ajax';
        $logDetails['store_data']  = $json_encode_data;
        $logDetails['ip_address']  = $this->get_client_ip();
        $logDetails['createdAt']   = date('Y-m-d H:i:s');
        $logDetails['action_name'] = 'Delete Technology transfer File';
        $logData                   = $this->master_model->insertRecord('logs', $logDetails);
        //END : INSERT LOG

        echo json_encode($result);
    }

    public function block_unblock_tt() // BLOCK/UNBLOCK BLOG
    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id        = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $type           = trim($this->security->xss_clean($this->input->post('type')));
            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";

            if ($type == 'block') {

                $up_data['is_block'] = 1;

            } else if ($type == 'unblock') {$up_data['is_block'] = 0;}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('technology_transfer', $up_data, array('tech_id' => $tech_id));

            // echo $this->db->last_query();die;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Block technology transfer",
                'module_name' => 'technology transfer front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }


    public function close_tt() // BLOCK/UNBLOCK BLOG
    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id        = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $type           = trim($this->security->xss_clean($this->input->post('type')));
            $user_id        = $this->session->userdata('user_id');
            $result['flag'] = "success";

            if ($type == 'close') {

                $up_data['is_closed'] = '1';

            } else if ($type == 'open') {$up_data['is_closed'] = '0';}
            $up_data['updated_on'] = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('technology_transfer', $up_data, array('tech_id' => $tech_id));

            // echo $this->db->last_query();die;

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "close technology transfer",
                'module_name' => 'technology transfer front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function connect_request_ajax() // REPORT BLOG

    {
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id                 = trim($this->security->xss_clean(base64_decode($this->input->post('tech_id'))));
            $popupTechRequestComment = trim($this->security->xss_clean($this->input->post('popupTechRequestComment')));
            $user_id                 = $this->session->userdata('user_id');
            $result['flag']          = "success";
            $result['tech_id']       = $tech_id;

            $app_id = '';
            for ($i = 0; $i < 9; $i++) {
                $app_id .= mt_rand(0, 9);
            }

            $action_name                = "Connect Technology";
            $add_reported['tech_id']    = $tech_id;
            $add_reported['app_id']     = $app_id;
            $add_reported['comments']   = $popupTechRequestComment;
            $add_reported['user_id']    = $user_id;
            $add_reported['created_on'] = date('Y-m-d H:i:s');
            $this->master_model->insertRecord('arai_technology_transfer_connect_request', $add_reported);

            $result['app_id'] = $app_id;

            if ($popupTechRequestComment == '') {
                $reason = 'NA';
            } else {
                $reason = $popupTechRequestComment;
            }

            $email_info = $this->get_mail_data($tech_id);

            // START: MAIL TO APPLICANT ON SUBMIT REQUEST
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_applicant_on_request_submit');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[APPLICANT_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[APPLICATION_ID]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['APPLICANT_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $app_id,
                    date('d/m/Y'),
                    date('H:i:s'),

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['APPLICANT_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: START: MAIL TO APPLICANT ON SUBMIT REQUEST

            // START: MAIL TO ADMIN ON SUBMIT REQUEST
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_admin_on_request_submit');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[APPLICANT_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[APPLICATION_ID]',
                    '[APPLICANT_EMAIL_ID]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['APPLICANT_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $app_id,
                    $email_info['APPLICANT_EMAIL'],
                    date('d/m/Y'),
                    date('H:i:s'),

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['admin_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: START: MAIL TO ADMIN ON SUBMIT REQUEST

            // START: MAIL TO OWNER ON SUBMIT REQUEST
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_request_submit');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[APPLICANT_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[APPLICATION_ID]',
                    '[APPLICANT_EMAIL_ID]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',
                    '[PURPOSE]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['APPLICANT_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $app_id,
                    $email_info['APPLICANT_EMAIL'],
                    date('d/m/Y'),
                    date('H:i:s'),
                    $reason,

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: START: MAIL TO ADMIN ON SUBMIT REQUEST

            $Reported_label = 'Request Already Sent';
            $ReportedFlag   = '0';

            $onclick_fun        = "connect_request_technology_tarnsfer('" . base64_encode($tech_id) . "','" . $ReportedFlag . "', '', '')";
            $result['response'] = '<button class="btn btn-primary float-right" href="javascript:void(0)" title="' . $Reported_label . '" onclick="' . $onclick_fun . '">Connect</button>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'Resource Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }


    public function report_tech_ajax() // REPORT BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id                  = trim($this->security->xss_clean(base64_decode($this->input->post('tech_id'))));
            $popupTechReportComment = trim($this->security->xss_clean($this->input->post('popupTechReportComment')));
            $user_id                = $this->session->userdata('user_id');
            $result['flag']         = "success";
            $result['tech_id']        = $tech_id;

            $action_name                = "Reported technology transfer";
            $add_reported['tech_id']      = $tech_id;
            $add_reported['comments']   = $popupTechReportComment;
            $add_reported['user_id']    = $user_id;
            $add_reported['created_on'] = date('Y-m-d H:i:s');
            $this->master_model->insertRecord('arai_technology_transfer_reported', $add_reported);


            $email_info = $this->get_mail_data($tech_id);
            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_content_reported');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[REASON]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $popupTechReportComment,
                    $email_info['DD/MM/YYYY'],
                    $email_info['HH:MM:SS'],

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO USER

             // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_admin_on_content_reported');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[USER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[REASON]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['APPLICANT_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $popupTechReportComment,
                    $email_info['DD/MM/YYYY'],
                    $email_info['HH:MM:SS'],

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['admin_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO ADMIN

           

            $Reported_label = 'Knowledge Repository Already Reported';
            $ReportedFlag   = '0';

            $onclick_fun        = "report_tech('" . base64_encode($tech_id) . "','" . $ReportedFlag . "', '', '')";
       
            $result['response'] = '<a class="btn btn-sm btn-danger btn_report" href="javascript:void(0)"  title="'.$Reported_label.'" onclick="'.$onclick_fun.'"> Report</a>';

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => $action_name,
                'module_name' => 'knowledge repository Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function my_tech_applications() // SHOW REPORTED COUNT

    {

        $user_id = $this->session->userdata('user_id');

        $this->db->select('tr.*,t.technology_title,t.id_disp');
        $this->db->order_by('tr.connect_id', 'DESC', false);

        $this->db->join('arai_technology_transfer t', 't.tech_id = tr.tech_id', 'INNER', false);
        $data['applications'] = $applications = $this->master_model->getRecords("arai_technology_transfer_connect_request tr", array('tr.user_id' => $user_id));
        // echo "<pre>";print_r($applications);die;
        $data['page_title']     = 'Resource sharing';
        $data['middle_content'] = 'technology_transfer/my_applications_listing';
        $this->load->view('front/front_combo', $data);

    }

    public function delete_tt() // DELETE BLOG

    {
        $this->check_permissions->is_logged_in();
        $encrptopenssl = new Opensslencryptdecrypt();
        if (isset($_POST) && count($_POST) > 0) {
            $tech_id              = trim($this->security->xss_clean(base64_decode($this->input->post('id'))));
            $popupKrDeleteComment = trim($this->security->xss_clean($this->input->post('popupKrDeleteComment')));
            $user_id              = $this->session->userdata('user_id');
            $result['flag']       = "success";
            $result['tech_id']    = $tech_id;

            $del_data['is_deleted']    = 1;
            $del_data['delete_reason'] = $popupKrDeleteComment;
            $del_data['deleted_on']    = date('Y-m-d H:i:s');
            $this->master_model->updateRecord('technology_transfer', $del_data, array('tech_id' => $tech_id));

            $email_info = $this->get_mail_data($tech_id);

            // START: MAIL TO USER
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_owner_on_delete_content');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[REASON]',
                    // '[DD/MM/YYYY]',
                    // '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $popupKrDeleteComment,
                    // $email_info['DD/MM/YYYY'],
                    // $email_info['HH:MM:SS'],

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['OWNER_EMAIL'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO USER

            // START: MAIL TO ADMIN
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_admin_on_delete_content');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';

            if (count($subscriber_mail) > 0) {
                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = [
                    '[OWNER_NAME]',
                    '[TECHNOLOGY_TRANSFER_NAME]',
                    '[ID]',
                    '[REASON]',
                    '[DD/MM/YYYY]',
                    '[HH:MM:SS]',

                ];
                $rep_array = [
                    $email_info['OWNER_NAME'],
                    $email_info['TECHNOLOGY_TRANSFER_NAME'],
                    $email_info['ID'],
                    $popupKrDeleteComment,
                    date('d/m/Y'),
                    date('H:i:s'),

                ];

                $sub_content = str_replace($arr_words, $rep_array, $desc);

                $info_array = array(
                    'to'      => $email_info['admin_email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);
                $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
            }
            // END: MAIL TO ADMIN

            //START MAIL TO MEMBERS
            $this->db->select("r.email,r.first_name,r.last_name,r.title");
            $this->db->join("arai_registration r", "r.user_id = sa.user_id", "INNER", false);
            $applicant_data = $this->master_model->getRecords('arai_technology_transfer_connect_request sa', array("sa.tech_id" => $tech_id));

            if (count($applicant_data) > 0) {

                $email_send      = '';
                $slug            = $encrptopenssl->encrypt('tech_trans_mail_to_applicant_on_delete_content');
                $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
                $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
                $sub_content     = '';

                if (count($subscriber_mail) > 0) {
                    $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                    $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                    $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                    $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                    $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                    $arr_words = [
                        '[OWNER_NAME]',
                        '[TECHNOLOGY_TRANSFER_NAME]',
                        '[ID]',
                        '[REASON]',

                    ];

                    $rep_array = [
                        $email_info['OWNER_NAME'],
                        $email_info['TECHNOLOGY_TRANSFER_NAME'],
                        $email_info['ID'],
                        $popupKrDeleteComment,
                    ];
                    $sub_content = str_replace($arr_words, $rep_array, $desc);
                }

                foreach ($applicant_data as $res) {
                    $full_name      = $encrptopenssl->decrypt($res['title']) . " " . $encrptopenssl->decrypt($res['first_name']) . " " . $encrptopenssl->decrypt($res['last_name']);
                    $sub_content    = str_replace("[APPLICANT_NAME]", $full_name, $sub_content);
                    $applicant_mail = $encrptopenssl->decrypt($res['email']);
                    $info_array     = array(
                        // 'to' =>  $applicant_mail,
                        'to'      => $applicant_mail,
                        'cc'      => '',
                        'from'    => $fromadmin,
                        'subject' => $subject_title,
                        'view'    => 'common-file',
                    );

                    $other_infoarray = array('content' => $sub_content);
                    $email_send      = $this->emailsending->sendmail($info_array, $other_infoarray);
                }
            }

            $postArr = $this->input->post();
            // Log Data Added
            $json_encode_data = json_encode($postArr);
            $ipAddr           = $this->get_client_ip();
            $createdAt        = date('Y-m-d H:i:s');
            $logDetails       = array(
                'user_id'     => $user_id,
                'action_name' => "Delete technology transfer",
                'module_name' => 'Technology transfer Front',
                'store_data'  => $json_encode_data,
                'ip_address'  => $ipAddr,
                'createdAt'   => $createdAt,
            );
            $logData = $this->master_model->insertRecord('logs', $logDetails);

            $this->session->set_flashdata('success_tech_del_msg', 'Content has been successfully deleted');
        } else {
            $result['flag'] = "error";
        }

        $result['token'] = $this->security->get_csrf_hash();
        echo json_encode($result);
    }

    public function getNumData($query) // CALCULATE NUM ROWS

    {
        //echo $query;
        return $rowCount = $this->db->query($query)->num_rows();
    }

    public function get_mail_data($tech_id)
    {
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('title,first_name,middle_name,last_name,email,id_disp,technology_title,technology_transfer.created_on,registration.user_id');
        $this->db->join('registration', 'technology_transfer.user_id=registration.user_id', 'LEFT');
        $r_details = $this->master_model->getRecords('technology_transfer', array('tech_id' => $tech_id));

        if (count($r_details)) {
            $user_arr = array();
            if (count($r_details)) {
                foreach ($r_details as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $row_val['user_id']     = $row_val['user_id'];
                    $user_arr[]             = $row_val;
                }
            }
            $email_array                             = array();
            $email_array['OWNER_NAME']               = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];
            $email_array['OWNER_ID']                 = $user_arr[0]['user_id'];
            $email_array['OWNER_EMAIL']              = $user_arr[0]['email'];
            $email_array['TECHNOLOGY_TRANSFER_NAME'] = $r_details[0]['technology_title'];
            $email_array['ID']                       = $r_details[0]['id_disp'];
            $email_array['DD/MM/YYYY']               = date('d/m/Y', strtotime($r_details[0]['created_on']));
            $email_array['HH:MM:SS']                 = date('H:i:s', strtotime($r_details[0]['created_on']));
            $details_page                            = site_url('technology_transfer/details/') . base64_encode($tech_id);
            $email_array['hyperlink_detail']         = '<a href=' . $details_page . ' target="_blank">here</a>';
            $email_array['admin_email']              = 'test@esds.co.in';

            $this->db->select('user_id,title,first_name,middle_name,last_name,email');
            $loggedin_user_data = $this->master_model->getRecords("registration", array('user_id' => $this->session->userdata('user_id')));

            $loggedin_user_arr = array();
            if (count($loggedin_user_data)) {
                foreach ($loggedin_user_data as $row_val) {
                    $row_val['title']       = $encrptopenssl->decrypt($row_val['title']);
                    $row_val['first_name']  = $encrptopenssl->decrypt($row_val['first_name']);
                    $row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
                    $row_val['last_name']   = $encrptopenssl->decrypt($row_val['last_name']);
                    $row_val['email']       = $encrptopenssl->decrypt($row_val['email']);
                    $loggedin_user_arr[]    = $row_val;
                }

                $loggedin_user_name             = $loggedin_user_arr[0]['title'] . " " . $loggedin_user_arr[0]['first_name'] . " " . $loggedin_user_arr[0]['last_name'];
                $email_array['APPLICANT_NAME']  = $loggedin_user_name;
                $email_array['APPLICANT_EMAIL'] = $loggedin_user_arr[0]['email'];
                $email_array['APPLICANT_ID']    = $loggedin_user_arr[0]['user_id'];
            }
            return $email_array;
            // echo "<pre>";
            // print_r($email_array);
        }
    }
    public function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
