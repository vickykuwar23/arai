<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Profile_organization extends CI_Controller 
	{
		public $login_user_id;
		
		function __construct() 
		{
			parent::__construct();
			$this->load->helper('security');
			$this->load->library('upload');
			$this->load->library('Opensslencryptdecrypt');
			$this->load->model('Common_model_sm');	
			$this->check_permissions->is_logged_in();
			
			$this->login_user_id = $this->session->userdata('user_id');
		}
		
		public function index()
		{
			$encrptopenssl =  New Opensslencryptdecrypt();
			
			$this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,mobile,membership_type');
			$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
			$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
			$user_data = $this->master_model->getRecords("registration",array('user_id'=>$this->login_user_id));
			
			$personal_info = array();
			if(!empty($user_data))
			{ 
				//********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************	
				$personal_info['title'] = $encrptopenssl->decrypt($user_data[0]['title']);
				$personal_info['first_name'] = $encrptopenssl->decrypt($user_data[0]['first_name']);
				$personal_info['middle_name'] = $encrptopenssl->decrypt($user_data[0]['middle_name']);
				$personal_info['last_name'] = $encrptopenssl->decrypt($user_data[0]['last_name']);
				$personal_info['email'] = $encrptopenssl->decrypt($user_data[0]['email']);
				$personal_info['mobile'] = $encrptopenssl->decrypt($user_data[0]['mobile']);
				$personal_info['membership_type'] = $encrptopenssl->decrypt($user_data[0]['membership_type']);
				$personal_info['user_category'] = $encrptopenssl->decrypt($user_data[0]['user_category']);
				$personal_info['sub_catname'] = $encrptopenssl->decrypt($user_data[0]['sub_catname']);
			}

			$this->db->join("arai_country c1","c1.id = o.spoc_sec_num_country_code1","LEFT",false);
			$this->db->join("arai_country c2","c2.id = o.spoc_sec_num_country_code2","LEFT",false);
			$org_profile_data = $this->master_model->getRecords('profile_organization o',array("o.user_id"=>$this->login_user_id), 'o.*, c1.iso, c1.phonecode, c2.iso AS iso2, c2.phonecode AS phonecode2');
			
			$profile_info = $bod_info = array();
			if(!empty($org_profile_data))
			{ 
				//********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY ************				
				$profile_info['overview'] = $encrptopenssl->decrypt($org_profile_data[0]['overview']);
				$profile_info['spoc_bill_addr1'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr1']);
				$profile_info['spoc_bill_addr2'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr2']);
				$profile_info['spoc_bill_addr_city'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_city']);
				$profile_info['spoc_bill_addr_state'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_state']);
				$profile_info['spoc_bill_addr_country'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_country']);
				$profile_info['spoc_bill_addr_pin'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_bill_addr_pin']);
				$profile_info['spoc_sec_num_country_code1'] = $org_profile_data[0]['iso']." ".$org_profile_data[0]['phonecode'];
				$profile_info['spoc_sec_num1'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num1']);
				$profile_info['spoc_sec_num_country_code2'] = $org_profile_data[0]['iso2']." ".$org_profile_data[0]['phonecode2'];
				$profile_info['spoc_sec_num2'] = $encrptopenssl->decrypt($org_profile_data[0]['spoc_sec_num2']);
				$profile_info['specialities_products'] = $encrptopenssl->decrypt($org_profile_data[0]['specialities_products']);
				$profile_info['establishment_year'] = $encrptopenssl->decrypt($org_profile_data[0]['establishment_year']);
				$profile_info['institution_size'] = $encrptopenssl->decrypt($org_profile_data[0]['institution_size']);
				$profile_info['company_evaluation'] = $encrptopenssl->decrypt($org_profile_data[0]['company_evaluation']);
				$profile_info['website'] = $encrptopenssl->decrypt($org_profile_data[0]['website']);
				$profile_info['linkedin_page'] = $encrptopenssl->decrypt($org_profile_data[0]['linkedin_page']);
				$profile_info['pan_card'] = $encrptopenssl->decrypt($org_profile_data[0]['pan_card']);
				$profile_info['institution_reg_certificate'] = $encrptopenssl->decrypt($org_profile_data[0]['institution_reg_certificate']);
				$profile_info['self_declaration'] = $encrptopenssl->decrypt($org_profile_data[0]['self_declaration']);
				$profile_info['address_proof'] = $encrptopenssl->decrypt($org_profile_data[0]['address_proof']);
				$profile_info['gst_reg_certificate'] = $encrptopenssl->decrypt($org_profile_data[0]['gst_reg_certificate']);
				
				$bod_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_data[0]['org_profile_id'], 'user_id'=>$this->login_user_id),'*');
				if(!empty($bod_data))
				{
					foreach($bod_data as $bod_key => $bod)
					{
						$bod_info[$bod_key]['bod_id'] = $bod['bod_id'];
						$bod_info[$bod_key]['org_profile_id'] = $bod['org_profile_id'];
						$bod_info[$bod_key]['user_id'] = $bod['user_id'];
						$bod_info[$bod_key]['bod_name'] = $encrptopenssl->decrypt($bod['bod_name']);
						$bod_info[$bod_key]['bod_designation'] = $encrptopenssl->decrypt($bod['bod_designation']);
						$bod_info[$bod_key]['bod_since'] = $encrptopenssl->decrypt($bod['bod_since']);
						}
				}
			}
			
			$data['personal_info'] = $personal_info;
			$data['profile_info'] = $profile_info;
			$data['bod_info'] = $bod_info;
			$data['page_title'] = 'Organization Profile';
			$data['middle_content'] = 'organization_profile/manage_profile';
			$this->load->view('front/front_combo',$data);
		}
		
		public function update_profile()
    {
			$encrptopenssl =  New Opensslencryptdecrypt();
			$form_data = array();
			
			$organization_data = $this->master_model->getRecords('profile_organization',array("user_id"=>$this->login_user_id));
			if(empty($organization_data)) { $mode = "Add"; $org_profile_id = 0; }
			else 
			{ 
				$mode = "Update"; 
				$org_profile_id = $organization_data[0]['org_profile_id'];
				
				/********** START : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
				$form_data['org_profile_id'] = $organization_data[0]['org_profile_id'];
				$form_data['user_id'] = $organization_data[0]['user_id'];
				$form_data['overview'] = $encrptopenssl->decrypt($organization_data[0]['overview']);
				$form_data['spoc_bill_addr1'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr1']);
				$form_data['spoc_bill_addr2'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr2']);
				$form_data['spoc_bill_addr_city'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_city']);
				$form_data['spoc_bill_addr_state'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_state']);
				$form_data['spoc_bill_addr_country'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_country']);
				$form_data['spoc_bill_addr_pin'] = $encrptopenssl->decrypt($organization_data[0]['spoc_bill_addr_pin']);
				$form_data['spoc_sec_num_country_code1'] = $organization_data[0]['spoc_sec_num_country_code1'];
				$form_data['spoc_sec_num1'] = $encrptopenssl->decrypt($organization_data[0]['spoc_sec_num1']);
				$form_data['spoc_sec_num_country_code2'] = $organization_data[0]['spoc_sec_num_country_code2'];
				$form_data['spoc_sec_num2'] = $encrptopenssl->decrypt($organization_data[0]['spoc_sec_num2']);
				$form_data['specialities_products'] = $encrptopenssl->decrypt($organization_data[0]['specialities_products']);
				$form_data['establishment_year'] = $encrptopenssl->decrypt($organization_data[0]['establishment_year']);
				$form_data['institution_size'] = $encrptopenssl->decrypt($organization_data[0]['institution_size']);
				$form_data['company_evaluation'] = $encrptopenssl->decrypt($organization_data[0]['company_evaluation']);
				$form_data['website'] = $encrptopenssl->decrypt($organization_data[0]['website']);
				$form_data['linkedin_page'] = $encrptopenssl->decrypt($organization_data[0]['linkedin_page']);
				$form_data['pan_card'] = $encrptopenssl->decrypt($organization_data[0]['pan_card']);
				$form_data['institution_reg_certificate'] = $encrptopenssl->decrypt($organization_data[0]['institution_reg_certificate']);
				$form_data['self_declaration'] = $encrptopenssl->decrypt($organization_data[0]['self_declaration']);
				$form_data['address_proof'] = $encrptopenssl->decrypt($organization_data[0]['address_proof']);
				$form_data['gst_reg_certificate'] = $encrptopenssl->decrypt($organization_data[0]['gst_reg_certificate']);
				$form_data['created_on'] = $organization_data[0]['created_on'];
				$form_data['created_by'] = $organization_data[0]['created_by'];
				$form_data['updated_on'] = $organization_data[0]['updated_on'];
				$form_data['updated_by'] = $organization_data[0]['updated_by'];
				/********** END : PREPARE NEW DECRYPTED ARRAY TO DISPLAY IN FORM ************/				
			}
			
			$data['pan_card_error'] = $data['institution_reg_certificate_error'] =  $data['self_declaration_error'] = $data['address_proof_error'] =  $data['gst_reg_certificate_error'] = '';
			$file_upload_flag = 0;
			
			if(isset($_POST) && count($_POST) > 0)
			{ 
				$this->form_validation->set_rules('overview', 'Overview', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr1', 'Flat/House No/Building/Apt/Company', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr2', 'Area/Colony/Street/Village', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_city', 'Town/City', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_state', 'State', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_country', 'Country', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_bill_addr_pin', 'Pincode', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('spoc_sec_num_country_code1', 'Country Code', 'trim');
				$this->form_validation->set_rules('spoc_sec_num1', 'Number', 'trim');
				$this->form_validation->set_rules('otp1', 'OTP', 'trim');
				$this->form_validation->set_rules('spoc_sec_num_country_code2', 'Country Code', 'trim');
				$this->form_validation->set_rules('spoc_sec_num2', 'Number', 'trim');
				$this->form_validation->set_rules('otp2', 'OTP', 'trim');
				$this->form_validation->set_rules('specialities_products', 'Specialities & products', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('establishment_year', 'Year of Establishment', 'trim|required',array('required' => 'Please select the %s'));		
				$this->form_validation->set_rules('institution_size', 'Institution Size', 'trim');		
				$this->form_validation->set_rules('company_evaluation', 'Company Evaluation', 'trim');		
				$this->form_validation->set_rules('website', 'Website', 'trim|required',array('required' => 'Please enter the %s'));		
				$this->form_validation->set_rules('linkedin_page', 'Linked-In Page', 'trim|required',array('required' => 'Please enter the %s'));						
				$this->form_validation->set_rules('bod_id[]', '', 'trim');						
				$this->form_validation->set_rules('bod_name[]', '', 'trim');						
				$this->form_validation->set_rules('bod_designation[]', '', 'trim');						
				$this->form_validation->set_rules('bod_since[]', '', 'trim');						
				
				if($mode == "Add" || (isset($organization_data[0]['pan_card']) && $organization_data[0]['pan_card'] == ''))
				{
					if (empty($_FILES['pan_card']['name']))
					{
						$this->form_validation->set_rules('pan_card', 'Pan Card', 'required', array('required' => 'Please upload the %s'));
					}
				}
				
				if($mode == "Add" || (isset($organization_data[0]['institution_reg_certificate']) && $organization_data[0]['institution_reg_certificate'] == ''))
				{
					if (empty($_FILES['institution_reg_certificate']['name']))
					{
						$this->form_validation->set_rules('institution_reg_certificate', 'Institution Registration Certificate', 'required', array('required' => 'Please upload the %s'));
					}
				}
				
				if($mode == "Add" || (isset($organization_data[0]['self_declaration']) && $organization_data[0]['self_declaration'] == ''))
				{
					if (empty($_FILES['self_declaration']['name']))
					{
						$this->form_validation->set_rules('self_declaration', 'Self Declaration for all of the above data', 'required', array('required' => 'Please upload the %s'));
					}
				}
				/* $this->form_validation->set_rules('team_size', 'Team Size', 'trim|numeric|callback_check_team_size['.$challenge_id.']|required',array('required' => 'Please enter the %s'));	*/
				
				if($this->form_validation->run())
				{	
					/* echo "<pre>"; print_r($_POST); echo "</pre>"; exit;  */
					
					if($_FILES['pan_card']['name'] != "")
					{
						$pan_card = $this->Common_model_sm->upload_single_file("pan_card", array('png','jpg','jpeg','gif','pdf'), "pan_card_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($pan_card['response'] == 'error')
						{
							$data['pan_card_error'] = $pan_card['message'];
							$file_upload_flag = 1;
						}
						else if($pan_card['response'] == 'success')
						{
							$add_data['pan_card'] = $encrptopenssl->encrypt($pan_card['message']);	
							@unlink("./uploads/organization_profile/".$organization_data[0]['pan_card']);
						}
					}
					
					if($_FILES['institution_reg_certificate']['name'] != "")
					{
						$institution_reg_certificate = $this->Common_model_sm->upload_single_file("institution_reg_certificate", array('png','jpg','jpeg','gif','pdf'), "institution_reg_certificate_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($institution_reg_certificate['response'] == 'error')
						{
							$data['institution_reg_certificate_error'] = $institution_reg_certificate['message'];
							$file_upload_flag = 1;
						}
						else if($institution_reg_certificate['response'] == 'success')
						{
							$add_data['institution_reg_certificate'] = $encrptopenssl->encrypt($institution_reg_certificate['message']);	
							@unlink("./uploads/organization_profile/".$organization_data[0]['institution_reg_certificate']);
						}
					}
					
					if($_FILES['self_declaration']['name'] != "")
					{
						$self_declaration = $this->Common_model_sm->upload_single_file("self_declaration", array('png','jpg','jpeg','gif','pdf'), "self_declaration_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($self_declaration['response'] == 'error')
						{
							$data['self_declaration_error'] = $self_declaration['message'];
							$file_upload_flag = 1;
						}
						else if($self_declaration['response'] == 'success')
						{
							$add_data['self_declaration'] = $encrptopenssl->encrypt($self_declaration['message']);	
							@unlink("./uploads/organization_profile/".$organization_data[0]['self_declaration']);
						}
					}
					
					if($_FILES['address_proof']['name'] != "")
					{
						$address_proof = $this->Common_model_sm->upload_single_file("address_proof", array('png','jpg','jpeg','gif','pdf'), "address_proof_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($address_proof['response'] == 'error')
						{
							$data['address_proof_error'] = $address_proof['message'];
							$file_upload_flag = 1;
						}
						else if($address_proof['response'] == 'success')
						{
							$add_data['address_proof'] = $encrptopenssl->encrypt($address_proof['message']);	
							@unlink("./uploads/organization_profile/".$organization_data[0]['address_proof']);
						}
					}
					
					if($_FILES['gst_reg_certificate']['name'] != "")
					{
						$gst_reg_certificate = $this->Common_model_sm->upload_single_file("gst_reg_certificate", array('png','jpg','jpeg','gif','pdf'), "gst_reg_certificate_".date("YmdHis"), "./uploads/organization_profile", "png|jpeg|jpg|gif|pdf");
						if($gst_reg_certificate['response'] == 'error')
						{
							$data['gst_reg_certificate_error'] = $gst_reg_certificate['message'];
							$file_upload_flag = 1;
						}
						else if($gst_reg_certificate['response'] == 'success')
						{
							$add_data['gst_reg_certificate'] = $encrptopenssl->encrypt($gst_reg_certificate['message']);	
							@unlink("./uploads/organization_profile/".$organization_data[0]['gst_reg_certificate']);
						}
					}
				
					if($file_upload_flag == 0)
					{
						$add_data['user_id'] = $this->login_user_id;								
						$add_data['overview'] = $encrptopenssl->encrypt($this->input->post('overview', TRUE));								
						$add_data['spoc_bill_addr1'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr1', TRUE));	
						$add_data['spoc_bill_addr2'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr2', TRUE));	
						$add_data['spoc_bill_addr_city'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_city', TRUE));	
						$add_data['spoc_bill_addr_state'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_state', TRUE));	
						$add_data['spoc_bill_addr_country'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_country', TRUE));	
						$add_data['spoc_bill_addr_pin'] = $encrptopenssl->encrypt($this->input->post('spoc_bill_addr_pin', TRUE));	
						$add_data['spoc_sec_num_country_code1'] = $this->input->post('spoc_sec_num_country_code1', TRUE);	
						$add_data['spoc_sec_num1'] = $encrptopenssl->encrypt($this->input->post('spoc_sec_num1', TRUE));	
						$add_data['spoc_sec_num_country_code2'] = $this->input->post('spoc_sec_num_country_code2', TRUE);	
						$add_data['spoc_sec_num2'] = $encrptopenssl->encrypt($this->input->post('spoc_sec_num2', TRUE));	
						$add_data['specialities_products'] = $encrptopenssl->encrypt($this->input->post('specialities_products', TRUE));	
						$add_data['establishment_year'] = $encrptopenssl->encrypt($this->input->post('establishment_year', TRUE));	
						$add_data['institution_size'] = $encrptopenssl->encrypt($this->input->post('institution_size', TRUE));	
						$add_data['company_evaluation'] = $encrptopenssl->encrypt($this->input->post('company_evaluation', TRUE));	
						$add_data['website'] = $encrptopenssl->encrypt($this->input->post('website', TRUE));	
						$add_data['linkedin_page'] = $encrptopenssl->encrypt($this->input->post('linkedin_page', TRUE));	
						
						if($mode == "Add")
						{
							$add_data['created_on'] = date("Y-m-d H:i:s");
							$add_data['created_by'] = $this->login_user_id;
							$org_profile_id = $this->master_model->insertRecord('profile_organization',$add_data,TRUE);
						}
						else if($mode == "Update")
						{ 
							$add_data['updated_on'] = date("Y-m-d H:i:s");
							$add_data['updated_by'] = $this->login_user_id;
							$this->master_model->updateRecord('profile_organization',$add_data,array("org_profile_id"=>$org_profile_id));
						}
						
						$bod_id = $this->input->post('bod_id', TRUE);
						$bod_name = $this->input->post('bod_name', TRUE);
						$bod_designation = $this->input->post('bod_designation', TRUE);
						$bod_since = $this->input->post('bod_since', TRUE);
						
						if($mode == "Update")
						{
							$bod_old_data_arr = array();
							$bod_old_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_id, 'user_id'=>$this->login_user_id),'bod_id');
							if(!empty($bod_old_data))
							{
								foreach($bod_old_data as $bod_old)
								{
									$bod_old_data_arr[] = $bod_old['bod_id'];
								}
							}
							
							$bod_new_data = $bod_id;
							$delete_bod_arr = array_diff($bod_old_data_arr, $bod_new_data);
							
							if(!empty($delete_bod_arr))
							{
								foreach($delete_bod_arr as $delete_bod)
								{
									$this->master_model->deleteRecord('arai_organization_bod','bod_id',$delete_bod);
								}
							}
						}
						
						if(count($bod_id) > 0)
						{
							for($i=0; $i<count($bod_id); $i++)
							{
								if($bod_name[$i] != '' && $bod_designation[$i] != '' && $bod_since[$i] != '')
								{
									$add_bod = array();
									$add_bod['org_profile_id'] = $org_profile_id;
									$add_bod['user_id'] = $this->login_user_id;
									$add_bod['bod_name'] = $encrptopenssl->encrypt($bod_name[$i]);
									$add_bod['bod_designation'] = $encrptopenssl->encrypt($bod_designation[$i]);
									$add_bod['bod_since'] = $encrptopenssl->encrypt($bod_since[$i]);
									
									if($bod_id[$i] == 0)//ADD
									{
										$add_bod['created_on'] = date("Y-m-d H:i:s");
										$add_bod['created_by'] = $this->login_user_id;
										$this->master_model->insertRecord('organization_bod',$add_bod,TRUE);
									}
									else//UPDATE
									{
										$add_bod['updated_on'] = date("Y-m-d H:i:s");
										$add_bod['updated_by'] = $this->login_user_id;
										$this->master_model->updateRecord('organization_bod',$add_bod,array("bod_id"=>$bod_id[$i]));
									}
								}
							}
						}
						
						$this->session->set_flashdata('success','Profile updated successfully');
						redirect(site_url('profile_organization'),'refresh');
					}
				}
			}
						
			$year_arr = array();
			for($i = date("Y"); $i >= 1981; $i--) { $year_arr[] = $i; }
			$data['year_arr'] = $year_arr;
			
			$bod_data = $this->master_model->getRecords('organization_bod',array('org_profile_id'=>$org_profile_id, 'user_id'=>$this->login_user_id),'*');
			$bod_form_data = array();
			if(!empty($bod_data))
			{
				foreach($bod_data as $bod_key => $bod)
				{
					$bod_form_data[$bod_key]['bod_id'] = $bod['bod_id'];
					$bod_form_data[$bod_key]['org_profile_id'] = $bod['org_profile_id'];
					$bod_form_data[$bod_key]['user_id'] = $bod['user_id'];
					$bod_form_data[$bod_key]['bod_name'] = $encrptopenssl->decrypt($bod['bod_name']);
					$bod_form_data[$bod_key]['bod_designation'] = $encrptopenssl->decrypt($bod['bod_designation']);
					$bod_form_data[$bod_key]['bod_since'] = $encrptopenssl->decrypt($bod['bod_since']);
				}
			}
			
			$data['mode'] = $mode;			
			$data['user_id'] = $this->login_user_id;
			$data['organization_data'] = $organization_data;
			$data['form_data'] = $form_data;
			$data['bod_form_data'] = $bod_form_data;
			$data['country_codes'] = $this->master_model->getRecords('country',array(),'id, iso, name, phonecode');
			$data['page_title']='Organization Profile';
			$data['middle_content']='organization_profile/update_profile';
			$this->load->view('front/front_combo',$data);
		}
		
		public function send_otp()
		{
			$mobile = $this->input->post('mobile', TRUE);
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			$otp = $this->generateNumericOTP(6);
			if($mobile!='' && $otp!='')
			{
				$text = "Your OTP code is".$otp;
				$msg = urlencode($text);
				$reply='';
				// $url="https://smsgw.sms.gov.in/failsafe/HttpLink?username=isarita.auth&pin=K*4$56qw&message=".$msg."&mnumber=".$mobile."&signature=IGRMAH";
				
				// $string = preg_replace('/\s+/', '', $url);
				// $x = curl_init($string);
				// curl_setopt($x, CURLOPT_HEADER, 0);    
				// curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
				// curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);    
				// curl_setopt($x, CURLOPT_SSL_VERIFYPEER, FALSE);        
				// $reply = curl_exec($x);
				
				$add_data['mobile_number']=$mobile;
				$add_data['otp']=$otp;
				$add_data['created_at']=date('Y-m-d H:i:s');			
				
				if ($reply || 1) 
				{
					$this->master_model->deleteRecord('otp','mobile_number',$mobile);
					if($this->master_model->insertRecord('otp',$add_data))
					{
						$result['flag'] = "success";
					}
					else
					{
						$result['flag'] = "error";
					}
				}
				else
				{
					$result['flag'] = "error";
				}
			}
			else
			{
				$result['flag'] = "error";				
			}
			echo json_encode($result);
		}
		
		function generateNumericOTP($n) 
		{ 			
			$generator = "1357902468"; 
			$result = ""; 
			
			for ($i = 1; $i <= $n; $i++) 
			{ 
        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
			} 
			return $result; 
		} 
				
		public function check_spoc_sec_num_exist() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['spoc_num']) && $_POST['spoc_num'] != "" && isset($_POST['col_name']) && $_POST['col_name'] != "")
			{
				$spoc_num = $this->input->post('spoc_num');
				$col_name = $this->input->post('col_name');
				
				$whre[$col_name]= $encrptopenssl->encrypt($spoc_num);				
				$whre['user_id !=']= $this->login_user_id;
				
				$check_cnt = $this->master_model->getRecordCount('profile_organization',$whre);
				if($check_cnt == 0) 
				{
					echo "true";
					$result['flag'] = "success";
				}
				else
				{
					echo "false";
					$result['flag'] = "error";
				}
			}
			else
			{
				echo "true";
				$result['flag'] = "success";
			}
			
			//echo json_encode($result);
		}		
		
		public function verify_otp() 
    { 
			$encrptopenssl =  New Opensslencryptdecrypt();
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			
			if(isset($_POST) && isset($_POST['spoc_num']) && $_POST['spoc_num'] != "")
			{
				$spoc_num = $this->input->post('spoc_num');
				$otp = $this->input->post('otp');
				$mode = $this->input->post('mode');
				$input = $this->input->post('input');
				$check_otp_flag = 0;
				
				if($mode == 'Add') { $check_otp_flag = 1; }
				else if($mode == 'Update')
				{
					$whre_con['user_id'] = $this->login_user_id;
					$whre_con[$input] = $encrptopenssl->encrypt($spoc_num);
					$check_cnt = $this->master_model->getRecordCount('profile_organization',$whre_con);
					if($check_cnt > 0) { $result['flag'] = "success"; }
					else { $check_otp_flag = 1; }
				}
								
				if($check_otp_flag == 1)
				{
					if($otp == "")
					{
						$result['flag'] = "error";
						$result['response'] = "Please enter the OTP";
					}
					else
					{
						$whre['mobile_number']= $spoc_num;
						$whre['otp']= $otp;
					
						// $encrptopenssl =  New Opensslencryptdecrypt();
						// $mobile=$encrptopenssl->encrypt($mobile);
						$user = $this->master_model->getRecords('otp',$whre);
						if (count($user)==1) 
						{
							$result['flag'] = "success";
						}
						else
						{
							$result['flag'] = "error";
							$result['response'] = "Please enter valid OTP";
						}
					}
				}
				else { $result['flag'] = "success"; }
			}
			else { $result['flag'] = "success"; }			
			echo json_encode($result);
		}
		
		function delete_org_profile_files()
		{
			$org_profile_id = $this->input->post('org_profile_id', TRUE);	
			$input_name = $this->input->post('input_name', TRUE);	
						
			$csrf_new_token = $this->security->get_csrf_hash();
			$result['csrf_new_token'] = $csrf_new_token;
			$result['flag'] = "error";
			
			if($org_profile_id == 0) { $result['flag'] = "error"; $this->session->set_flashdata('error','Error occurred. Please try after sometime.'); }
			else 
			{
				$res_data = $this->master_model->getRecords('arai_profile_organization',array("org_profile_id"=>$org_profile_id));
				if(count($res_data) == 0) { $result['flag'] = "error"; $this->session->set_flashdata('error','Error occurred. Please try after sometime.'); }
				else 
				{ 
					@unlink("./uploads/organization_profile/".$res_data[0][$input_name]);
					
					$up_data[$input_name] = "";
					$up_data['updated_on'] = date("Y-m-d H:i:s");
					$up_data['updated_by'] = $this->login_user_id;
					$this->master_model->updateRecord('arai_profile_organization',$up_data,array("org_profile_id"=>$org_profile_id));			
					
					//$this->session->set_flashdata('success','File successfully deleted');
					$result['flag'] = "success";
				}
			}
			
			echo json_encode($result);
		}
	
}				