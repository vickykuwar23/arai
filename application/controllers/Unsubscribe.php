<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unsubscribe extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
    }
	
	public function unsubscribe_user($str)
	{
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		if($str == "")
		{			
			redirect(base_url());			
		}
		else 
		{			
			$newletterData  	= $this->master_model->getRecords('newsletter',array('email_id'=>$str));
						
			$get_email = $encrptopenssl->decrypt($str);
			if(count($newletterData) > 0)
			{
				$is_deleted = 1;				
				foreach($newletterData as $res)
				{
					if($res['is_deleted'] == '0') { $is_deleted = 0; }
				}
				
				if($is_deleted == 0)
				{
					$upArray = array('is_deleted' => '1', 'status' => 'Block');
					$updateChallenge = $this->master_model->updateRecord('newsletter',$upArray,array('email_id' => "'$str'"));
					
					$this->session->set_flashdata('success','You successfully unsubscribe from newslsetter');
					
					// Subscription Functionality
					$slug 			= $encrptopenssl->encrypt('discontinue_subscription');					
					$contact_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
					$setting_table 	= $this->master_model->getRecords("setting", array("id" => '1'));
					$content = '';
					if(count($contact_mail) > 0)
					{							
						$subject 	 	= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
						$description 	= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
						$from_admin 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);				
						$from_names 	= 'Admin';
						$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
						$phoneNO  		= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
						$arr_words = ['[PHONENO]'];
						$rep_array = [$phoneNO];
						
						$content = str_replace($arr_words, $rep_array, $description);
						
					}			
					
					//email admin sending code 
					$info_arr=array(
							'to'		=>	$get_email,					
							'cc'		=>	'',
							'from'		=>	$from_admin,
							'subject'	=> 	$subject,
							'view'		=>  'common-file'
							);
					
					$other_info=array('content'=>$content); 
					//print_r($other_info);die();
					$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					
					$data['page_title']='Unsubscribe';
					$this->load->view('front/unsubscribe',$data);
					
				}
				else
				{
					
					$this->session->set_flashdata('success','You have successfully unsubscribed from Newsletter');
					$data['page_title']='Unsubscribe';	
					$this->load->view('front/unsubscribe',$data);
				}
			} 
			else 
			{
				
					$this->session->set_flashdata('error','No any user found regarding your request');
					$data['page_title']='Unsubscribe';	
					$this->load->view('front/unsubscribe',$data);
				
			}
			
		} // IF End 
		
	} // Function End
	 
}
