<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : News & Blogs Module
Author : Vicky K
*/

class Blogs extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		/*if($this->session->userdata('user_id') == ""){			
			redirect(base_url('login'));
		}*/
    }

	public function index_old(){	
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		$blog_data = $this->master_model->getRecords("news_blog", array("status" => 'Active'),'',array("id" => 'DESC'),0,12);		
		$featured_blog = $this->master_model->getRecords("news_blog", array("status" => 'Active', "is_featured" =>'Y' ),'',array("id" => 'DESC'),0,6);		
		
		$data['total'] = $this->master_model->getRecords("news_blog", array("status" => 'Active'));
		$res_arr = array();
		if(count($blog_data) > 0){
			
			foreach($blog_data as $row_val){
				
				$row_val['blog_name'] 	= $encrptopenssl->decrypt($row_val['blog_name']);	
				$row_val['blog_desc'] 	= $encrptopenssl->decrypt($row_val['blog_desc']);
				$row_val['blog_url'] 	= $encrptopenssl->decrypt($row_val['blog_url']);	
				$row_val['blog_img']= $encrptopenssl->decrypt($row_val['blog_img']);
				$row_val['createdAt'] 	= $row_val['createdAt'];
				$row_val['id'] 			= $row_val['id'];
				
				$res_arr[] = $row_val;
			}	
			
		}
		
		// Featured Blogs		
		$res_blog = array();
		if(count($featured_blog) > 0){
			
			foreach($featured_blog as $data_val){
				
				$data_val['blog_name'] 	= $encrptopenssl->decrypt($data_val['blog_name']);	
				$data_val['blog_desc'] 	= $encrptopenssl->decrypt($data_val['blog_desc']);
				$data_val['blog_url'] 	= $encrptopenssl->decrypt($data_val['blog_url']);	
				$data_val['blog_img']= $encrptopenssl->decrypt($data_val['blog_img']);
				$data_val['createdAt'] 	= $data_val['createdAt'];
				$data_val['is_featured'] 	= $data_val['is_featured'];
				$data_val['id'] 			= $data_val['id'];
				
				$res_blog[] = $data_val;
			}	
			
		}
		
		//echo "<pre>";print_r($res_arr);die();
		$data['featured_blog'] = $res_blog; 	
		$data['blog_data'] = $res_arr; 	
		$data['page_title'] 	 = 'Blog';
		$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'blogs/index';
		$this->load->view('front/front_combo',$data);
	}

	public function index(){	
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		$data['blog_data'] = $res_arr; 	
		$data['page_title'] 	 = 'Blog';
		$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'blogs/index_new';
		$this->load->view('front/front_combo',$data);
	}

	public function load_news_ajax()
	{	
		// ini_set('display_errors', '1');
		// ini_set('display_startup_errors', '1');
		// error_reporting(E_ALL);
			$encrptopenssl =  New Opensslencryptdecrypt();
			$data=array();
			$data['jobs_count']=0;
			
			if (isset($_POST) && count($_POST)>0 ) {
				
				$start=$this->input->post('start');
				$limit=$this->input->post('limit');
				$searchText =$this->input->post('searchText');
				$is_show_more =$this->input->post('is_show_more');

				$search_str = "";
				if ($searchText!='') {$search_str .= "(blog_name_decrypt LIKE '%" . $searchText . "%' || blog_desc_decrypt LIKE '%" . $searchText . "%')";}
				if ($search_str != "") {$this->db->where($search_str);}
				$blog_data = $this->master_model->getRecords("news_blog", array("status" => 'Active'),'',array("id" => 'DESC'),$start,$limit);		
					$res_arr = array();
					if(count($blog_data) > 0){
						
						foreach($blog_data as $row_val){
							
							$row_val['blog_name'] 	= $encrptopenssl->decrypt($row_val['blog_name']);	
							$row_val['blog_desc'] 	= $encrptopenssl->decrypt($row_val['blog_desc']);
							$row_val['blog_url'] 	= $encrptopenssl->decrypt($row_val['blog_url']);	
							$row_val['blog_img']= $encrptopenssl->decrypt($row_val['blog_img']);
							$row_val['createdAt'] 	= $row_val['createdAt'];
							$row_val['id'] 			= $row_val['id'];
							
							$res_arr[] = $row_val;
						}	
						
					}
					
					// Featured Blogs		
					if ($is_show_more==0) {
					
					if ($search_str != "") {$this->db->where($search_str);}
					$featured_blog = $this->master_model->getRecords("news_blog", array("status" => 'Active', "is_featured" =>'Y' ),'',array("id" => 'DESC'),0,6);

					$res_blog = array();
					if(count($featured_blog) > 0){

						foreach($featured_blog as $data_val){

							$data_val['blog_name'] 	= $encrptopenssl->decrypt($data_val['blog_name']);	
							$data_val['blog_desc'] 	= $encrptopenssl->decrypt($data_val['blog_desc']);
							$data_val['blog_url'] 	= $encrptopenssl->decrypt($data_val['blog_url']);	
							$data_val['blog_img']= $encrptopenssl->decrypt($data_val['blog_img']);
							$data_val['createdAt'] 	= $data_val['createdAt'];
							$data_val['is_featured'] 	= $data_val['is_featured'];
							$data_val['id'] 			= $data_val['id'];

							$res_blog[] = $data_val;
						}	

						}
						$data_feartured['featured_blog'] = $res_blog; 	
						$json_data['featured_news_html']= $this->load->view('/front/blogs/inc_featured_news',$data_feartured,true);
						$json_data['featured_blog_count']=count($featured_blog);

					}
				
				if ($search_str != "") {$this->db->where($search_str);}
				$this->db->select('id');
				$news_all = $this->master_model->getRecordCount("news_blog", array("status" => 'Active'),'',array("id" => 'DESC'));	

				// echo "<pre>";print_r($res_arr);die;
				$data['blog_data'] = $res_arr;
				$data['news_count']=count($res_arr);
				$data['news_all_count']=$news_all;
				$data['limit'] = $limit;
				$data['new_start']=$start + $limit;
				
		}
		$json_data['news_html']= $this->load->view('/front/blogs/inc_news',$data,true);
		// $json_data['jobs_all_count']=$jobs_all;
		echo json_encode($json_data);
	}
		
    public function get_more(){
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		sleep(1);
		$csrf_test_name = $this->security->get_csrf_hash();
		
		$id = $this->input->post('id');
		
		$total_cnt = $this->master_model->getRecords("news_blog", array("status" => 'Active'), '', array("id" => 'DESC'));
		
		$showLimit = 6;
		
		$sql = "SELECT * FROM arai_news_blog WHERE status = 'Active' ORDER BY id DESC LIMIT ".$id.",".$showLimit."";
		$result 	= $this->db->query($sql)->result();	
		
		$showData = '';
		if(count($result) > 0){
			
			foreach($result as $b_data){
			
				$nextid = $b_data->id;
				
				$str = $encrptopenssl->decrypt($b_data->blog_desc);
				$limit = 160;
				$str1 = strip_tags($str);
				$chk_add  = substr($str1 , 0, strrpos(substr($str1 , 0, $limit), ' ')) . '...';	
				 
				 
				 $limit_1 = 40;
				 $str_1 = $encrptopenssl->decrypt($b_data->blog_name);
				 if (strlen($str_1 ) > $limit_1){
					$str_1 = substr($str_1, 0,40)."...";
					//substr($str_1 , 0, strrpos(substr($str , 0, $limit_1), ' ')) . '...'; 
				 }
				 
				 
				 
				
				$createdAt = date('d M, Y', strtotime($b_data->createdAt));//Your date
				
				$blog_url = base_url('blogs/blogDetails/'.base64_encode($b_data->id));
				
				$fileExist = base_url('assets/blog/'.$encrptopenssl->decrypt($b_data->blog_img));
				
				if($b_data->blog_img!=""){
					$fileExist = base_url('assets/blog/'.$encrptopenssl->decrypt($b_data->blog_img));
					$image = $fileExist;
					/*$fileExist = base_url('assets/blog/'.$b_data->blog_img);
					 if (@GetImageSize($fileExist)) {
						$image = $fileExist;
					} else {
						$image = base_url('assets/news-14.jpg');
					} */
					
				} else {
					
					$image = base_url('assets/News_default.png');
					
				}

				$showData .= '<article class="single-news">
								<div class="row">				
									<div class="col-md-4 d-flex" style="position:relative">
										<img src="'.$image.'" class="img-fluid" alt="...">
										
										<div class="text">
											<a href="'.$blog_url.'"><i class="fa fa-bars" aria-hidden="true"></i></a>
										</div>
									</div>
									<div class="col-md-8 d-flex pl-0">
										<div class="p-3">
											<div class="" style="margin: 0;font-size: 20px;font-weight: 500;line-height:22px;">
												<a href="'.$blog_url.'">'.ucwords($str_1).'</a>
											</div>
											<ul class="list-inline" style="color:#c80032;">
												<li>Posted: <span class="text-theme-colored2" style="color:#333;"> '.$b_data->createdAt.'</span></li>
											</ul>
											<p>'.$chk_add.'</p>
											<a href="'.$blog_url.'" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
										</div>					
									</div>
								</div>								
							</article>';
				
			} // Foreach End
			
			if($total_cnt > $showLimit){
			
			$showData .= '<div class="show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>';
			
			}
			
			$jsonData = array("html" => $showData, "token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		} else {
			
			$jsonData = array("html" => '', "token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		}
		
	}	
	 
	 public function blogDetails($id){
		 
		$bid = base64_decode($id);
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$blog_data = $this->master_model->getRecords("news_blog", array("id" => $bid));
		$res_arr = array();
		if(count($blog_data) > 0){
			
			foreach($blog_data as $row_val){
				
				$row_val['blog_name'] 	= $encrptopenssl->decrypt($row_val['blog_name']);	
				$row_val['blog_desc'] 	= $encrptopenssl->decrypt($row_val['blog_desc']);
				$row_val['blog_url'] 	= $encrptopenssl->decrypt($row_val['blog_url']);	
				$row_val['blog_img']	= $encrptopenssl->decrypt($row_val['blog_img']);
				$row_val['createdAt'] 	= $row_val['createdAt'];
				$row_val['id'] 			= $row_val['id'];
				
				$res_arr[] = $row_val;
			}	
			
		}	

		$featured_blog = $this->master_model->getRecords("news_blog", array("status" => 'Active', "is_featured" =>'Y' ),'',array("id" => 'DESC'),0,6);			
		 
		// Featured Blogs		
		$res_blog = array();
		if(count($featured_blog) > 0){
			
			foreach($featured_blog as $data_val){
				
				$data_val['blog_name'] 	= $encrptopenssl->decrypt($data_val['blog_name']);	
				$data_val['blog_desc'] 	= $encrptopenssl->decrypt($data_val['blog_desc']);
				$data_val['blog_url'] 	= $encrptopenssl->decrypt($data_val['blog_url']);	
				$data_val['blog_img']= $encrptopenssl->decrypt($data_val['blog_img']);
				$data_val['createdAt'] 	= $data_val['createdAt'];
				$data_val['id'] 			= $data_val['id'];
				
				$res_blog[] = $data_val;
			}	
			
		}
		
		//echo "<pre>";print_r($res_arr);die();
		$data['featured_blog'] 	= $res_blog; 		 
		$data['blog_data'] 		= $res_arr;  
		$data['page_title'] 	 = 'Blog';
		$data['submodule_name']  = '';	
    	$data['middle_content']	 = 'blogs/details';
		$this->load->view('front/front_combo',$data);
	 }
	 
	

}