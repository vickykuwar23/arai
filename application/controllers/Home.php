<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper('captcha');
		$this->load->helper('security');	
		$this->load->library('Opensslencryptdecrypt'); 
		$this->load->library("session");
    }
	
	public function index()
	{
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$date = date('Y-m-d');
		$banner_mgt  	= $this->master_model->getRecords('banners',array('status'=>"Active"),'',array('xOrder' => 'ASC'));
		$service_mgt  	= $this->master_model->getRecords('services',array('status'=>"Active"));
		$featuredChall 	= $this->master_model->getRecords('challenge',array('challenge_status'=>"Approved", 'is_featured' => "Y", 'is_deleted' => "0", 'challenge_close_date >=' => $date),'',array('c_id' => "DESC"),0,12);
		
		$testimonial_mgt= $this->master_model->getRecords('testimonials',array('status'=>"Active"));
		$government_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "G"));
		//$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		$featuredBlog 	= $this->master_model->getRecords('news_blog',array('is_featured' => "Y"),'',array('id' => "DESC"),0,6);
		
		//Count Users
		$data['no_register_user']  		= $this->master_model->getRecords('registration',array('status'=>"Active"));
		$data['no_experts_user']  		= $this->master_model->getRecords('registration',array('status'=>"Active", "user_sub_category_id" => '11'));
		//$data['no_of_challenge']  		= $this->master_model->getRecords('challenge',array('challenge_status' => "Approved", 'status'=>"Active"));
		// $data['no_of_challenge_closed'] = $this->master_model->getRecords('challenge',array('challenge_status' => "Closed",  'is_deleted' => "0", 'status'=>"Active"));

		$curr_date = date('Y-m-d');
		$this->db->where("challenge_close_date < ", $curr_date);
		$data['no_of_challenge_closed'] = $this->master_model->getRecords('challenge',array('challenge_status' => "Approved",'is_deleted' => "0", 'status'=>"Active"));
		
		$this->db->join('registration', 'registration.user_id = challenge.u_id');
		$data['no_of_challenge'] = $this->master_model->getRecords("challenge", array("registration.is_deleted" => '0', "registration.status" => 'Active', "challenge.is_deleted" => '0', "challenge.challenge_status" => 'Approved'));		
		
		
		$How_It_Works  = $this->master_model->getRecords('how_it_works',array('status'=>"Active"));
		
		// Featured Expert
		$this->db->select('arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
		$Featured_Experts = $this->master_model->getRecords('registration',array('registration.status'=>"Active",'registration.is_featured'=>'Y','registration.user_sub_category_id' => '11','registration.is_deleted' => '0'),'',array('xOrder' => 'ASC'));
		//echo $this->db->last_query();
		
		$res_arr = array();
		if(count($banner_mgt)){	
						
			foreach($banner_mgt as $row_val){		
						
				$row_val['banner_name'] = $encrptopenssl->decrypt($row_val['banner_name']);
				$row_val['banner_sub'] = $encrptopenssl->decrypt($row_val['banner_sub']);
				$row_val['banner_desc'] = $encrptopenssl->decrypt($row_val['banner_desc']);
				$row_val['banner_url'] = $encrptopenssl->decrypt($row_val['banner_url']);
				$row_val['banner_img'] = $encrptopenssl->decrypt($row_val['banner_img']);
				$res_arr[] = $row_val;
			}
			
		}
		
		// Services Management
		$ser_arr = array();		
		if(count($service_mgt)){	
						
			foreach($service_mgt as $res_val){		
						
				$res_val['service_icon'] = $encrptopenssl->decrypt($res_val['service_icon']);
				$res_val['service_title'] = $encrptopenssl->decrypt($res_val['service_title']);
				$res_val['service_desc'] = $encrptopenssl->decrypt($res_val['service_desc']);
				$res_val['service_url'] = $encrptopenssl->decrypt($res_val['service_url']);
				$ser_arr[] = $res_val;
			}
			
		}

		
		// Featured Challenge Management
		$featureArr = array();		
		if(count($featuredChall)){				
					
			foreach($featuredChall as $feat_val){
				
				$valArr = array();
				$audience_pref_id 	= $feat_val['audience_pref_id'];
				
				error_reporting(0);
				if($audience_pref_id!=""){
					
					$explode = explode(",",$audience_pref_id);
					$valArr = array();	
					foreach($explode as $val){
						
						$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
						$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
						array_push($valArr, $audienceName);					
					}
					$implode = implode(",", $valArr);
				
				}
				
				$feat_val['challenge_title'] 		= $encrptopenssl->decrypt($feat_val['challenge_title']);
				$feat_val['banner_img'] 			= $encrptopenssl->decrypt($feat_val['banner_img']);
				$feat_val['company_name'] 			= $encrptopenssl->decrypt($feat_val['company_name']);
				$feat_val['challenge_details'] 		= $encrptopenssl->decrypt($feat_val['challenge_details']);
				$feat_val['challenge_launch_date'] 	= $feat_val['challenge_launch_date'];
				$feat_val['challenge_close_date'] 	= $feat_val['challenge_close_date'];
				$feat_val['audience_pref'] 			= $implode;	
				$featureArr[] = $feat_val;
			}
			
		} 
		
		// Testimonial Management
		$testimonialArr = array();		
		if(count($testimonial_mgt)){				
					
			foreach($testimonial_mgt as $test_val){
				
				$test_val['author_name'] 		= $encrptopenssl->decrypt($test_val['author_name']);
				$test_val['author_designation'] = $encrptopenssl->decrypt($test_val['author_designation']);
				$test_val['profile_img'] 		= $encrptopenssl->decrypt($test_val['profile_img']);
				$test_val['author_content'] 	= $encrptopenssl->decrypt($test_val['author_content']);
				$testimonialArr[] = $test_val;
				
			}
			
		}
		
		// Government Partners 
		$goverArr = array();
		if(count($government_mgt)){				
					
			foreach($government_mgt as $gove_val){
				
				$gove_val['partner_name'] 	= $encrptopenssl->decrypt($gove_val['partner_name']);
				$gove_val['partner_img'] 	= $encrptopenssl->decrypt($gove_val['partner_img']);
				$goverArr[] = $gove_val;
				
			}
			
		}
		
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['img_link'] 		= $con_val['img_link'];
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}

		// How It Works 
		$howItWorksArr = array();
		if(count($How_It_Works)){				
					
			foreach($How_It_Works as $how_it_work_val){
				
				$how_it_work_val['subject_title'] 	= $encrptopenssl->decrypt($how_it_work_val['subject_title']);
				$how_it_work_val['first_title'] 	= $encrptopenssl->decrypt($how_it_work_val['first_title']);
				$how_it_work_val['first_description'] 	= $encrptopenssl->decrypt($how_it_work_val['first_description']);
				$how_it_work_val['first_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['first_icon_image']);
				$how_it_work_val['second_title'] 	= $encrptopenssl->decrypt($how_it_work_val['second_title']);
				$how_it_work_val['second_description'] 	= $encrptopenssl->decrypt($how_it_work_val['second_description']);
				$how_it_work_val['second_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['second_icon_image']);
				$how_it_work_val['third_title'] 	= $encrptopenssl->decrypt($how_it_work_val['third_title']);
				$how_it_work_val['third_description'] 	= $encrptopenssl->decrypt($how_it_work_val['third_description']);
				$how_it_work_val['third_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['third_icon_image']);
				$how_it_work_val['background_image'] 	= $encrptopenssl->decrypt($how_it_work_val['background_image']);
				$howItWorksArr[] = $how_it_work_val;
				
			}
			
		}

		// Featured Experts 
		$featuredExpertsArr = array();
		
		if(count($Featured_Experts) > 0){
			foreach($Featured_Experts as $key => $featured_experts_val){
					
					$featured_experts_val['title'] 	= $encrptopenssl->decrypt($featured_experts_val['title']);
					$featured_experts_val['first_name'] 	= $encrptopenssl->decrypt($featured_experts_val['first_name']);
					$featured_experts_val['last_name'] 	= $encrptopenssl->decrypt($featured_experts_val['last_name']);
					$featured_experts_val['institution_full_name'] 	= $encrptopenssl->decrypt($featured_experts_val['institution_full_name']);
					$featured_experts_val['designation_description'] 	= $encrptopenssl->decrypt($featured_experts_val['designation_description']);
					$featured_experts_val['skill_sets'] = $encrptopenssl->decrypt($featured_experts_val['skill_sets']);
					$featured_experts_val['no_of_patents'] = $encrptopenssl->decrypt($featured_experts_val['no_of_patents']);
					$featured_experts_val['company_name'] 	= $encrptopenssl->decrypt($featured_experts_val['company_name']);
					$featured_experts_val['domain_area_of_expertise'] 	= $encrptopenssl->decrypt($featured_experts_val['domain_area_of_expertise']);
					$featured_experts_val['no_of_paper_publication'] = $featured_experts_val['no_of_paper_publication'];
					$featured_experts_val['years_of_experience'] = $encrptopenssl->decrypt($featured_experts_val['years_of_experience']);
					$featured_experts_val['profile_picture'] = $featured_experts_val['profile_picture'];
					$featured_experts_val['designation'] = json_decode($featured_experts_val['designation']);
					
					$featuredExpertsArr[] = $featured_experts_val;
				
			}
		}
		
		 //echo "<pre>";print_r($featuredExpertsArr);die();
		
		// Featured Experts 
		$featuredNewsArr = array();
		if(count($featuredBlog) > 0){
			foreach($featuredBlog as $featured_Blog){
				
					$featured_Blog['blog_name'] = $encrptopenssl->decrypt($featured_Blog['blog_name']);
					$featured_Blog['blog_desc'] = $encrptopenssl->decrypt($featured_Blog['blog_desc']);
					$featured_Blog['blog_url'] 	= $encrptopenssl->decrypt($featured_Blog['blog_url']);
					$featured_Blog['blog_img'] 	= $encrptopenssl->decrypt($featured_Blog['blog_img']);
					$featured_Blog['createdAt'] = $featured_Blog['createdAt'];
					$featuredNewsArr[] = $featured_Blog;
				
			}
		}

		// START FEATURED TECH TRANSFER

		$this->db->order_by('ISNULL(t.xOrder), t.xOrder = 0, t.xOrder, t.created_on DESC', '', false);
		$select_tech = "t.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_technology_transfer_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags,
		(SELECT trl_name FROM arai_technology_transfer_tlr_master WHERE id=trl) AS DispTrl, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
		$this->db->join('arai_registration r', 'r.user_id = t.user_id', 'LEFT', false);
		$this->db->join('arai_profile_organization org', 'org.user_id = t.user_id', 'LEFT', false);
		$this->db->join('arai_student_profile sp', 'sp.user_id = t.user_id', 'LEFT', false);
		$data['featured_tech'] = $featured_tech = $this->master_model->getRecords("arai_technology_transfer t", array('t.is_deleted' => '0', 't.admin_status' => '1', 't.is_featured' => '1', 't.is_block' => '0'), $select_tech, '');
		// END FEATURED TECH TRANSFER

		// START FEATURED RESOURCE SHARING		
		$this->db->order_by('r.xOrder', 'ASC', false);
    $select_rs = "r.*,c.*, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_resource_sharing_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other' ) AS DispTags";

    $this->db->join('arai_cities c', 'c.city_id = r.location', 'LEFT', false);
    $data['featured_resource']    = $featured_resource    = $this->master_model->getRecords("arai_resource_sharing r", array('r.is_deleted' => '0', 'r.admin_status' => '1', 'r.is_featured' => '1', 'r.is_block' => '0'), $select_rs, '');
		// END FEATURED RESOURCE SHARING


		$select_blog = "b.blog_id, b.user_id, b.blog_id_disp, b.blog_title, b.blog_banner, b.blog_description, b.technology_ids, b.technology_other, b.tags, b.is_technical, b.author_name, b.author_professional_status, b.author_org_name, b.author_description, b.author_about, b.created_on, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_blog_tags_master WHERE FIND_IN_SET(id,tags)) AS DispTags, r.user_category_id, r.user_sub_category_id, org.org_logo, sp.profile_picture";
		$this->db->join('arai_registration r', 'r.user_id = b.user_id', 'LEFT', false);
		$this->db->join('arai_profile_organization org', 'org.user_id = b.user_id', 'LEFT', false);
		$this->db->join('arai_student_profile sp', 'sp.user_id = b.user_id', 'LEFT', false);
		$data['featured_blog']  = $featured_blog  = $this->master_model->getRecords("arai_blog b", array('b.is_deleted' => '0', 'b.admin_status' => '1', 'b.is_featured' => '1', 'b.is_block' => '0'), $select_blog, '');

		$data['featured_webinar'] = $this->master_model->getRecords("webinar",array('is_deleted' => '0', 'admin_status' => 'Approved', 'is_featured' => 'Yes'),'','');
		

		$setting_table 	= $this->master_model->getRecords("setting", array("id" => '1'));
		
		$data['quoteTitle'] = $encrptopenssl->decrypt($setting_table[0]['field_3']);
		$data['partneTitle'] = $encrptopenssl->decrypt($setting_table[0]['field_4']);
		
		$data['featuredNews']= $featuredNewsArr;
		$data['consortium_list']= $consortArr;
		$data['government_list']= $goverArr;
		$data['testimonial_list']= $testimonialArr;
		$data['featured_list']= $featureArr;
		$data['service_mgt']= $ser_arr;
		$data['banner_mgt']= $res_arr;
		$data['how_it_works']= $howItWorksArr;
		$data['featured_experts']= $featuredExpertsArr;
		$data['page_title']='Home';
		$data['middle_content']='index';
		$data['captcha']        = $this->create_captcha_tt();
		$this->load->view('front/front_combo',$data);
	}

	 public function create_captcha_tt($value = '')
    {
        // Captcha configuration
        $config = array(
            'img_path'    => 'assets/captcha_images/',
            'img_url'     => base_url() . 'assets/captcha_images/',
            'img_width'   => '170',
            'img_height'  => 30,
            'word_length' => 4,
            'font_size'   => 18,
            'pool'        => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors'      => array(
                'background' => array(186, 212, 237),
                'border'     => array(162, 171, 186),
                'text'       => array(5, 32, 77),
                'grid'       => array(151, 173, 196),
            ),
        );
        $captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('ttcaptcha');
        $this->session->set_userdata('ttcaptcha', $captcha['word']);

        // Send captcha image to view
        return $captcha['image'];
    }
	
	public function privacyPolicy()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"3"));

		$dataArr = array();		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		
		$data['privacy_data']= $dataArr;
		$data['page_title']='Privacy';
		$data['middle_content']='privacy';
		$this->load->view('front/front_combo',$data);
	}

	public function cts()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$cts_data = $this->master_model->getRecords('cms',array('id'=>"5"));

		$dataArr = array();		
		if(count($cts_data)){
				
				$cts_data['page_title'] 		= $encrptopenssl->decrypt($cts_data[0]['page_title']);
				$cts_data['page_description'] 	= $encrptopenssl->decrypt($cts_data[0]['page_description']);
				$dataArr[] = $cts_data;			
		}
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['cts_data']= $dataArr;
		$data['page_title']='CTS';
		$data['middle_content']='cts';
		$this->load->view('front/front_combo',$data);
	}

	public function build_your_team()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$build_data = $this->master_model->getRecords('cms',array('id'=>"6"));

		$dataArr = array();		
		if(count($build_data)){
				
				$build_data['page_title'] 		= $encrptopenssl->decrypt($build_data[0]['page_title']);
				$build_data['page_description'] 	= $encrptopenssl->decrypt($build_data[0]['page_description']);
				$dataArr[] = $build_data;			
		}
		
		$data['build_data']= $dataArr;
		$data['page_title']='Build_YOUR_TEAM';
		$data['middle_content']='byt';
		$this->load->view('front/front_combo',$data);
	}	
	
	public function expert_connect()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$expert_data = $this->master_model->getRecords('cms',array('id'=>"7"));
		//print_r($expert_data);die();
		$dataArr = array();		
		if(count($expert_data)){
				
				$expert_data['page_title'] 			= $encrptopenssl->decrypt($expert_data[0]['page_title']);
				$expert_data['page_description'] 	= $encrptopenssl->decrypt($expert_data[0]['page_description']);
				$dataArr[] = $expert_data;			
		}
		//print_r($dataArr);die();
		//print_r($dataArr);die();
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['expert_data']= $dataArr;
		$data['page_title']='EXPERT_CONNECT';
		$data['middle_content']='expert_connect';
		$this->load->view('front/front_combo',$data);
	}	

	public function about_technology_transfer()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$cms_data = $this->master_model->getRecords('cms',array('id'=>"8"));
		//print_r($cms_data);die();
		$dataArr = array();		
		if(count($cms_data)){
				
				$cms_data['page_title'] 			= $encrptopenssl->decrypt($cms_data[0]['page_title']);
				$cms_data['page_description'] 	= $encrptopenssl->decrypt($cms_data[0]['page_description']);
				$dataArr[] = $cms_data;			
		}
		//print_r($dataArr);die();
		//print_r($dataArr);die();
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['cms_data']= $dataArr;
		$data['page_title']='EXPERT_CONNECT';
		$data['middle_content']='cms/about_technology_transfer';
		$this->load->view('front/front_combo',$data);
	}

	public function about_technology_wall()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$cms_data = $this->master_model->getRecords('cms',array('id'=>"9"));
		//print_r($cms_data);die();
		$dataArr = array();		
		if(count($cms_data)){
				
				$cms_data['page_title'] 			= $encrptopenssl->decrypt($cms_data[0]['page_title']);
				$cms_data['page_description'] 	= $encrptopenssl->decrypt($cms_data[0]['page_description']);
				$dataArr[] = $cms_data;			
		}
		//print_r($dataArr);die();
		//print_r($dataArr);die();
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['cms_data']= $dataArr;
		$data['page_title']='EXPERT_CONNECT';
		$data['middle_content']='cms/about_technology_wall';
		$this->load->view('front/front_combo',$data);
	}

	public function about_resource_sharing()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$cms_data = $this->master_model->getRecords('cms',array('id'=>"10"));
		//print_r($cms_data);die();
		$dataArr = array();		
		if(count($cms_data)){
				
				$cms_data['page_title'] 			= $encrptopenssl->decrypt($cms_data[0]['page_title']);
				$cms_data['page_description'] 	= $encrptopenssl->decrypt($cms_data[0]['page_description']);
				$dataArr[] = $cms_data;			
		}
		//print_r($dataArr);die();
		//print_r($dataArr);die();
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['cms_data']= $dataArr;
		$data['page_title']='EXPERT_CONNECT';
		$data['middle_content']='cms/about_resource_sharing';
		$this->load->view('front/front_combo',$data);
	}

	 
	
	public function termsOfuse()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"4"));

		$dataArr = array();		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		
		$data['privacy_data']= $dataArr;
		$data['page_title']='Terms';
		$data['middle_content']='terms';
		$this->load->view('front/front_combo',$data);
	}	
	
	public function aboutUs()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"1"));

		$dataArr = array();		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['privacy_data']= $dataArr;
		$data['page_title']='About';
		$data['middle_content']='about';
		$this->load->view('front/front_combo',$data);
	}
	
	
	public function contactUs()
	{	
		error_reporting(E_ALL);
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"2"));
		$data['country_codes']  = $this->master_model->getRecords('country','','',array('iso'=>'asc'));
		$dataArr = array();	
		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		
		// Check Validation
		$this->form_validation->set_rules('cname', 'Fullname', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');	
		$this->form_validation->set_rules('contents', 'Description', 'required|xss_clean');
		$this->form_validation->set_rules('mobile', 'Mobile No', 'required|xss_clean');
		$this->form_validation->set_rules('country_code', 'Country Code', 'required|xss_clean');
		$this->form_validation->set_rules('captcha', 'Capcha', 'trim|required|callback_check_captcha_code|xss_clean');
		if($this->form_validation->run())
		{
			$cname 			= $encrptopenssl->encrypt($this->input->post('cname'));
			$email_id 		= $encrptopenssl->encrypt($this->input->post('email_id'));
			$subject  		= $encrptopenssl->encrypt($this->input->post('subject'));
			$contents  		= $encrptopenssl->encrypt($this->input->post('contents'));
			$country_code	= $encrptopenssl->encrypt($this->input->post('country_code'));
			$mobile  		= $encrptopenssl->encrypt($this->input->post('mobile'));
			$combineMo      = $encrptopenssl->encrypt($this->input->post('country_code')."-".$this->input->post('mobile'));
			$c_c 			= $this->input->post('country_code');
			$inputCaptcha 	= $this->input->post('captcha');
			$countryCodeValue = $this->master_model->getRecords('country',array('id'=>$c_c));
			$ccVal			= $countryCodeValue[0]['phonecode'];
			
			$contactCode	= $this->input->post('country_code');
			$contactName	= ucwords($this->input->post('cname'));
			$contactEmail	= $this->input->post('email_id');
			$contactSubject	= $this->input->post('subject');
			$contactDesc	= $this->input->post('contents');
			$contactPhone1	= $this->input->post('mobile');
			$contactPhone	= $ccVal."-".$contactPhone1;
			
			$insertArr = array( 
								'fullname' 			=> $cname, 
								'email_id' 			=> $email_id,
								'country_code'		=> $country_code,
								'mobile_no' 		=> $mobile,
								'subject_details' 	=> $subject,
								'content_received' 	=> $contents
								
								);		
			
			// Insert SQL For Challenge	
			$insertQuery = $this->master_model->insertRecord('contactus',$insertArr);
			
			if($insertQuery){
				
				// Log Capture
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$user_id 			= $this->session->userdata('user_id');
			$logDetails 	= array(
									'user_id' 		=> $user_id,
									'action_name' 	=> "Add",
									'module_name'	=> 'ContactUs',
									'store_data'	=> $json_encode_data,
									'ip_address'	=> $ipAddr,
									'createdAt'		=> $createdAt
								);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			
			// Contact User Email Functionality
				$slug 			= $encrptopenssl->encrypt('contact_us_subscriber');
				$contact_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table 	= $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				if(count($contact_mail) > 0){	
					
					$subject 	 	= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);				
					$from_names 	= 'Admin';
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					
					$arr_words = ['[USERNAME]', '[SIGNATURE]'];
					$rep_array = [$contactName,  $sendername];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
				}			
				
				//email admin sending code 
				$info_arr=array(
						'to'		=>	$contactEmail,					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject,
						'view'		=>  'common-file'
						);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				/********************************/
				
				// Admin Email Notification
				$slug_1 			= $encrptopenssl->encrypt('admin_alert_contact');
				$contact_admin_mail = $this->master_model->getRecords("email_template", array("slug" => $slug_1));
				$setting_table_1 	= $this->master_model->getRecords("setting", array("id" => '1'));
				$content_1 = '';
				if(count($contact_admin_mail) > 0){	
					
					$subject_1 	 	= $encrptopenssl->decrypt($contact_admin_mail[0]['email_title']);
					$description_1 	= $encrptopenssl->decrypt($contact_admin_mail[0]['email_description']);
					$from_admin_1 	= $encrptopenssl->decrypt($contact_admin_mail[0]['from_email']);
					$from_admin_2 	= $encrptopenssl->decrypt($contact_admin_mail[0]['cc_email_id']);	
					$from_names_1 	= 'Admin';
					$sendername_1  	= $encrptopenssl->decrypt($setting_table_1[0]['field_1']);					
					
					$arr_words_1 = ['[APPLICANTNAME]', '[NAME]', '[EMAIL]', '[SUBJECT]', '[DETAILS]', '[MOBILE]', '[SIGNATURE]'];
					$rep_array_1 = [$contactName,  $contactName, $contactEmail, $contactSubject, $contactDesc, $contactPhone, $sendername_1];
					
					$content_1 = str_replace($arr_words_1, $rep_array_1, $description_1);
					
				}			
				
				//email admin sending code 
				$info_arr_1 =array(
						'to'		=>	$from_admin_1, //$from_admin_1					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject_1,
						'view'		=>  'common-file'
						);
				
				$other_info_1=array('content'=>$content_1); 
				
				$emailsend=$this->emailsending->sendmail($info_arr_1,$other_info_1);
				
			} // Insert Query END			
			
			
			$this->session->set_flashdata('success','Contact form successfully submitted.');
			
			redirect(base_url('home/contactUs'));
		}
		
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];
		
		$data['contact_data']= $dataArr;
		$data['page_title']='Contact';
		$data['middle_content']='contactus';
		$this->load->view('front/front_combo',$data);
	}
	
	public function check_captcha_code($code)
	{
		
	  // check if captcha is set -
		if($code == '')
		{
			$this->form_validation->set_message('check_captcha_code', '%s is required.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($code == '' || $_SESSION["captchaCode"] != $code)
		{  
			$this->form_validation->set_message('check_captcha_code', 'Invalid %s.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($_SESSION["captchaCode"] == $code)
		{   
			$this->session->unset_userdata("captchaCode");
			$this->session->set_userdata("captchaCode", rand(1,100000));
		
			return true;
		}
		
	}
	
	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	public function faq($file_name = NULL)
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->order_by('FIELD(xOrder, "0") ASC, xOrder ASC, updatedAt DESC', FALSE);	
		$faq_data = $this->master_model->getRecords('faq',array('status'=>"Active"));

		$dataArr = array();		
		if(count($faq_data)){
			
				foreach($faq_data as $row_val){
					
				$row_val['faq_title'] 	= $encrptopenssl->decrypt($row_val['faq_title']);
				$row_val['faq_desc'] 	= $encrptopenssl->decrypt($row_val['faq_desc']);
				$row_val['faq_img'] 	= $encrptopenssl->decrypt($row_val['faq_img']);
				$dataArr[] = $row_val;

				}	
		}
		
		if(isset($_POST['faq_btn']) && !empty($_POST['faq_btn'])){			
			$file = $this->input->post('faq_file');
			$file_name = base_url('assets/faq/'.$file);
			//Tippo Server
			//$root = $_SERVER['DOCUMENT_ROOT']."/arai_testing/assets/faq/";
			// Live Envirnment	
			$root = 'assets/faq/';					
			$file_name = $root.$file;		
			if($file_name){ 
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_name));
				flush(); // Flush system output buffer
				readfile($file_name);
				die();
			} else { 
				http_response_code(404);
			}
		}
		
		
		
		$data['faq_data']= $dataArr;
		$data['page_title']='FAQ';
		$data['middle_content']='faq';
		$this->load->view('front/front_combo',$data);
	}	
	

	public function featuredChallenges(){
		error_reporting(0);
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		$date_now = date('Y-m-d');
		$challenge_data = $this->master_model->getRecords("challenge", array("challenge_status" => 'Approved', "is_deleted" => '0', "is_featured" => 'Y', "challenge_close_date >= " => $date_now),'','',0,6);		
		//echo $this->db->last_query();die();
		$data['total'] = $this->master_model->getRecords("challenge", array("challenge_status" => 'Approved', "is_featured" => 'Y', "is_deleted" => '0', "challenge_close_date >= " => $date_now));
		
		$res_arr = array();
		if(count($challenge_data) > 0){
			
			foreach($challenge_data as $row_val){
				
				$audience_pref_id 	= $row_val['audience_pref_id'];
				$implode ="";
				if($audience_pref_id!=""){
					
					$explode = explode(",",$audience_pref_id);
					$valArr = array();	
					foreach($explode as $val){
						
						$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
						$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
						array_push($valArr, $audienceName);					
					}
					$implode = implode(",", $valArr);
					
				} 
				
				
				$row_val['challenge_title'] 	= $encrptopenssl->decrypt($row_val['challenge_title']);	
				$row_val['banner_img'] 			= $encrptopenssl->decrypt($row_val['banner_img']);
				$row_val['challenge_details'] 	= $encrptopenssl->decrypt($row_val['challenge_details']);	
				$row_val['company_name'] 		= $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['challenge_close_date'] = $row_val['challenge_close_date'];
				$row_val['c_id'] 				= $row_val['c_id'];
				$row_val['audience_pref'] 		= $implode;	
				$res_arr[] = $row_val;
			}	
			
		}
	
		$data['challenge_data']= $res_arr;
		$data['page_title']='Featured Challenge';
		$data['middle_content']='featured_challenge';
		$this->load->view('front/front_combo',$data);
	}


	public function featuredlist(){
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		sleep(1);
		$csrf_test_name = $this->security->get_csrf_hash();
		$date_now = date('Y-m-d');
		$id = $this->input->post('id');
		
		$total_cnt = $this->master_model->getRecords("challenge", array("challenge_status" => 'Approved', "is_deleted" => '0', "is_featured" => 'Y', "challenge_close_date >= " => $date_now));
		
		$showLimit = 6;
		
		$sql = "SELECT * FROM `arai_challenge` WHERE `challenge_status` = 'Approved' AND `is_featured` = 'Y' AND `is_deleted` = '0' AND `challenge_close_date` >= '".$date_now."' LIMIT ".$id.",".$showLimit."";
		$result 	= $this->db->query($sql)->result();	
		
		$showData = '';
		if(count($result) > 0){
			
			foreach($result as $c_data){


				$audience_pref_id 	= $c_data->audience_pref_id;
				
				$explode = explode(",",$audience_pref_id);
				$valArr = array();	
				foreach($explode as $val){
					
					$audioPref 		= $this->master_model->getRecords("audience_pref", array("id" => $val));
					$audienceName	= $encrptopenssl->decrypt($audioPref[0]['preference_name']);
					array_push($valArr, $audienceName);					
				}
				$implode = implode(",", $valArr);

			
				$nextid = $c_data->c_id;
				
				$str = $encrptopenssl->decrypt($c_data->challenge_details);
				$limit = 200;
				 if (strlen($str ) > $limit){
					$str = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...'; 
				 }
				 
				 $str2 = $encrptopenssl->decrypt($c_data->company_name);
				 $limit2 = 48;
				 if (strlen($str2 ) > $limit2){
					$str2 = substr($str2 , 0, strrpos(substr($str2 , 0, $limit2), ' ')) . '...'; 
				 }
					
				
				$datestr = $c_data->challenge_close_date;//Your date
				$date	 = strtotime($datestr);//Converted to a PHP date (a second count)

				//Calculate difference
				$diff	= 	$date-time();//time returns current time in seconds
				$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
				//$hours	=	round(($diff-$days*60*60*24)/(60*60));

				if($days > 0){
					$showCnt = "Days left : ".$days;
				} else {
					$showCnt = "Closed Paticipations";
				}
				
				$fileExist = base_url('assets/challenge/'.$encrptopenssl->decrypt($c_data->banner_img));
				/*if(file_exists($fileExist)) {
					$image = $fileExist;
				} else {
					$image = base_url('assets/news-14.jpg');
				}*/
				
				 if ($c_data->banner_img!="") {
					$image = $fileExist;
				} else {
					$image = base_url('assets/news-14.jpg');
				}
				
				$showData .= '<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" onclick="document.location.href='.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'">
								<a href="'.base_url('challenge/challengeDetails/'.base64_encode($c_data->c_id)).'"><div class="desc-comp-offer-cont">
									<div class="thumbnail-blogs">
										<div class="caption">
											<i class="fa fa-chain"></i>
										</div>
											<img src="'.$image.'" class="img-fluid" alt="'.$encrptopenssl->decrypt($c_data->challenge_title).'" style="height:224px;">
										</div>
										<div class="card-content">
											<h5>'.$str2.'</h5>
											<h3>'.$encrptopenssl->decrypt($c_data->challenge_title).'</h3>
											<p class="desc">'.$str.'</p>
											<div class="cardd-footer">
											<div>';
											$exp = explode(",", $implode);
												foreach($exp as $newname)
												{
													$showData .= '<button class="btn btn-general btn-green-fresh" role="button">'.$newname.'</button>';
												}	
								$showData .= '</div>
												<div class="daysleft"><i class="fa fa-clock-o"></i> '.$showCnt.'</div>
											</div>
										</div>
									</div></a>
								</div>';				
				
				
			} // Foreach End
			
			if($total_cnt > $showLimit){
			
			$showData .= '<div class="show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>';
			
			}
			
			$jsonData = array("html" => $showData, "token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		} else {
			
			$jsonData = array("html" => '', "token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		}
		
	}	
	
	public function newsletter()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();
    	$user_types  = $this->master_model->getRecords('usertype');
		
		$res_arr = array();
		if(count($user_types)){	
						
			foreach($user_types as $row_val){		
						
				$row_val['user_type'] = $encrptopenssl->decrypt($row_val['user_type']);
				$res_arr[] = $row_val;
			}
			
		}

		if (isset($_POST['submit'])) {

			$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email|required|callback_unique_email');
		
			$this->form_validation->set_rules('code','Captcha Code','trim|required|xss_clean|callback_check_captcha');

			if($this->form_validation->run())
			{	
				$insertArr=array(
				 'email' => $encrptopenssl->encrypt($this->input->post('email')),
				);
						
				$insert_id = $this->master_model->insertRecord('newsletter',$insertArr);
				if($insert_id > 0){

					//email  sending code 
					$info_arr=array(
						'to'=>$this->input->post('email'),
						'cc'=>'',
						'from'=>$this->config->item('MAIL_FROM'),
						'subject'=>'ARAI PORTAL REGISTRATION',
						'view'=>'newsletter-email'
					);

					$other_info=array(
						'user_id'=>base64_encode($insert_id) 
					);
 
					//$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
					$this->session->set_flashdata('success_message','You have successfully subscribed to our newsletter.');
					
			    	redirect(base_url().'home');
					 
				
				} else {
					$this->session->set_flashdata('error_message','Something went wrong! Please try again.');
					redirect(base_url('home'));
				}
			}

			
		}

		$data['image'] = $this->create_captcha_code();
		$data['page_title']='Home';
		$data['middle_content']='home';
		$this->load->view('front/front_combo',$data);
	}

	public function create_captcha_code($value='')
	{
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Send captcha image to view
		return $captcha['image'];
	}


	public function unique_email($email) 
	{		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$email=$encrptopenssl->encrypt($email);
			$user  = $this->master_model->getRecords('newsletter',array('email'=>$email));
			if (count($user)>0) {
				$this->form_validation->set_message('unique_email', '%s already exist.'); 
				return false;
			}else{
				return true;
			}
		
	}



	public function check_captcha($code) 
	{

				if($code == '' || $_SESSION["mycaptcha"] != $code    )
				{
					$this->session->unset_userdata("mycaptcha");
					$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
					return false;
				}
				if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
				{
					$this->session->unset_userdata("mycaptcha");
					return true;
				}
		
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
      		'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
		
	
	public function verify_subscription_email_ajax()
	{
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		$emailId = $this->input->post('email');
		
		$em = $encrptopenssl->encrypt($emailId);
		//$user = $this->master_model->getRecords('newsletter',array('email_id'=>$em));
		$user = $this->master_model->getRecords('newsletter',array('email_id'=>$em),'',array('id' => 'DESC'),0,1);
		$result['result'] = $this->db->last_query();
		if(count($user) > 0 && $user[0]['is_deleted'] == 0)
		{
			if($user[0]['status'] == 'Active')
			{
				$result['flag'] = "error";
				$result['response'] = "This Email Address already exists";
			}
			else if($user[0]['status'] == 'Block')
			{
				$result['flag'] = "error";
				$result['response'] = "This email address is blocked. Please contact Admin on info@technovuus.araiindia.com.";
			}
		}
		else
		{
			$result['flag'] = "success";
		}
		
		echo json_encode($result);
 }
	 
	public function validateEmail()
	{
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
   		$emailId = $this->input->post('email');
		
		$em = $encrptopenssl->encrypt($emailId);
   		$user = $this->master_model->getRecords('newsletter',array('email_id'=>$em));

		if(!$user)
		{
			echo "true";
		}
		else
		{
			echo "false";
		}
   }

   /*public function InsertEmail()	
   {
   		$emailId = $this->input->post('email');
   		$insertArr = array( 'email_id' => $emailId);			
		$insertQuery = $this->master_model->insertRecord('newsletter',$insertArr);
		if($insertQuery)
		{
			$result['flag'] = 'success';
			$result['message'] = 'Your Email has been Subscibe';
		}
		else
		{
			$result['flag'] = 'error';
		}
		echo json_encode($result);
   }*/
   
     public function InsertEmail()	
   {
	    // Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		//echo "<pre>";print_r($this->input->post());die();
   		$emailId = $this->input->post('email');
		$all_check = $this->input->post('all_check');
		$ch_challenge = $this->input->post('ch_challenge');
		$ch_news = $this->input->post('ch_news');
		
		$allChk 		= 0;
		$challengeChk 	= 0;
		$newsChk 		= 0;
		if($all_check){
			$allChk = $all_check;
		}
		
		if($ch_challenge){
			$challengeChk = $ch_challenge;
		}
		
		if($ch_news){
			$newsChk = $ch_news;
		}
		
		$insert_email = $encrptopenssl->encrypt($emailId);
		
   		$insertArr = array( 'email_id' => $insert_email, 'all_notification' => $allChk, 'challenge_notification' => $challengeChk, 'news_notification' => $newsChk);
		
		$user = $this->master_model->getRecords('newsletter',array('email_id'=>$emailId));
		
		if(count($user) > 0 && $user[0]['is_deleted'] == 1){
			
			$upArray = array('is_deleted' => 0, 'status' => 'Active');
			$updateChallenge = $this->master_model->updateRecord('challenge',$upArray,array('email_id' => $insert_email));
			$result['flag'] = 'Success';
			$result['message'] = 'Confirmation: You have successfully subscribed to our Newsletter';
			$insertQuery = 1;
		} else { 
			$insertQuery = $this->master_model->insertRecord('newsletter',$insertArr);
			//echo $this->db->last_query();die();
		}
		
		
		
		if($insertQuery)
		{
			// Admin Email Functionality
			/*$challenge_mail = $this->master_model->getRecords("email_template", array("id" => '36'));
			$setting_table = $this->master_model->getRecords("setting", array("id" => '1'));
			$content = '';
			if(count($challenge_mail) > 0){
				
				$userId = $this->session->userdata('user_id');
				if($userId > 0){
					
					$sender_details = $this->master_model->getRecords("registration", array("user_id" => $userId));
					$fullname = $encrptopenssl->decrypt($sender_details[0]['first_name'])." ".$encrptopenssl->decrypt($sender_details[0]['last_name']);
					
				} else {
					
					$fullname = 'Guest User';
				}
				
				
				$subject 	 	= $encrptopenssl->decrypt($challenge_mail[0]['email_title']);
				$description 	= $encrptopenssl->decrypt($challenge_mail[0]['email_description']);
				$from_admin 	= $encrptopenssl->decrypt($challenge_mail[0]['from_email']);				
				$from_email 	= $emailId;
				$sendername  	= $setting_table[0]['field_1'];
				
				$arr_words = ['[EMAIL]', '[SIGNATURE]'];
				$rep_array = [$emailId, $sendername];
				
				$content = str_replace($arr_words, $rep_array, $description);
				
			}			
			
			//email admin sending code 
			$info_arr=array(
					'to'		=>	$from_admin,					
					'cc'		=>	'',
					'from'		=>	$from_email,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
			
			$other_info=array('content'=>$content); 
			
			$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
			
			if($emailsend)
			{
				$result['flag'] = 'success';
				$result['message'] = 'Confirmation: You have successfully subscribed to our Newsletter';
			}*/
			
			// Subscriber Email Functionality 
			$slug 				= $encrptopenssl->encrypt('successfull_subscription');
			$setting_table 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$subscriber_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			if(count($subscriber_mail) > 0){			
				
				
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$to_email 			= $emailId;
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				$base_url			= base_url('unsubscribe/unsubscribe_user/'.$insert_email);
				$unsubscribe		= "If you do not want to receive this mailer,<a href='".$base_url."'>Unsubscribe</a>";
				
				$arr_words = ['[PHONENO]', '[UNSUBSCRIBE]']; //'[SIGNATURE]'
				$rep_array = [$phoneNO, $unsubscribe];
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				
			}			
			//echo $sub_content;die();
			//email admin sending code 
			$info_array=array(
					'to'		=>	$to_email,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	$subject_title,
					'view'		=>  'common-file'
					);
			
			$other_infoarray	=	array('content' => $sub_content); 
			
			$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			
			if($email_send)
			{
				$result['flag'] = 'Success';
				$result['message'] = 'Confirmation: You have successfully subscribed to our Newsletter';
			}			
			
		}
		else
		{
			$result['flag'] = 'error';
		}
		echo json_encode($result);
   }
   
      public function testMail(){
	   $fromemail = "vicky.kuwar@esds.co.in";
		$fromadmin = "abc@gmail.com";
		$sub_content =" Testin Mail";	
			$info_array=array(
					'to'		=>	$fromemail,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	"Tesat Mail",
					'view'		=>  'subscription'
					);
			
			$other_infoarray	=	array('content' => $sub_content); 
			
			$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			
			if($email_send)
			{
				echo "Success";die();
			} else {
				echo "Fail";die();
			}
	   
   }
   
    // Tool Connect Popup Window 
	public function open_tool_connect()
	{
		$this->load->view('front/open_tool_connect_form');
	}

	// Tool Connect Insert Data
	public function open_tool_form_insert()	
    {
		//echo "<pre>";print_r($this->input->post());die();
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$requirements 		= $this->input->post('requirements');
		$purpose 			= $this->input->post('purpose');
		$tools_required 	= $this->input->post('tools_required');
		$tools_remark 		= $this->input->post('tools_remark');
		$user_id 			= $this->session->userdata('user_id');
		
		// Login User Details
		$userInfo = $this->master_model->getRecords('registration',array('user_id'=>$user_id));
			
		$user_email = $encrptopenssl->decrypt($userInfo[0]['email']);
		$primary_ID = $userInfo[0]['user_id'];
		$first_name = $encrptopenssl->decrypt($userInfo[0]['first_name']);
		$middle_name = $encrptopenssl->decrypt($userInfo[0]['middle_name']);
		$last_name = $encrptopenssl->decrypt($userInfo[0]['last_name']);
		$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);
		
		$insertArr = array( 'user_id' => $user_id, 'tool_requirements' => $requirements, 'tool_purpose' => $purpose, 'tools_required' => $tools_required, 'tools_remark' => $tools_remark);
		$insertQuery = $this->master_model->insertRecord('tool_requirement',$insertArr);
	    
		if($insertQuery)
		{
			error_reporting(E_ALL);
			// Tool Requirement Admin Email Functionality 
			$slug 				= $encrptopenssl->encrypt('tool_requirement_to_admin');
			$setting_table 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$subscriber_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			
			if(count($subscriber_mail) > 0){	
			
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$to_email 			= 'Office.tip@araiindia.com';
				//$to_email 			= 'jitendra.battise@esds.co.in';
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				// Receiver Name 
				$receiverName = 'Admin';
				if($tools_required!=""){
					$toolContent = $tools_required;
				} else {
					$toolContent = 'No';
				}
				
				if($tools_remark!=""){
					$toolRemark = $tools_remark;
				} else {
					$toolRemark = 'No';
				}
				
				$arr_words = ['[APPLICANT]', '[FULLNAME]', '[EMAIL]', '[ID]', '[REQUIREMENT]', '[PURPOSE]', '[FACILITIES]', '[REMARKS]']; //'[SIGNATURE]'
				$rep_array = [$receiverName, $fullname, $user_email, $primary_ID, $requirements, $purpose, $toolContent, $toolRemark];
			
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				
				//email admin sending code 
				$info_array=array(
						'to'		=>	$to_email,					
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
				
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
								
			}
			
			// Email Receiver Email Functionality
			$slug_1 				= $encrptopenssl->encrypt('tool_requiremnt_reply_to_user');
			$setting_table_1 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_mail 			= $this->master_model->getRecords("email_template", array("slug" => $slug_1));
			$email_content 			= '';
			if(count($contact_mail) > 0){	
				
					$title 	 			= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
					$description 		= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
					$from_admin_mail 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);
					//$new_email	= "vicky.kuwar@esds.co.in";	
					$new_email 			= $user_email;
					$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);
					
					$arr_words_1 	= ['[APPLICANT]']; //'[SIGNATURE]'
					$rep_array_1 	= [$fullname];
					$email_content  = str_replace($arr_words_1, $rep_array_1, $description);
					
					//email admin sending code 
					$info_array_1 = array(
											'to'		=>	$new_email,					
											'cc'		=>	'',
											'from'		=>	$from_admin_mail,
											'subject'	=> 	$title,
											'view'		=>  'common-file'
											);
					
					$other_infoarray_1	=	array('content' => $email_content); 
					
					$email_send_1 = $this->emailsending->sendmail($info_array_1,$other_infoarray_1);
					
				}
			
			$result['flag'] = 'Success';
			$result['message'] = 'Your request successfully submitted.';
			
		}  else {
			
			$result['flag'] = 'error';
			$result['message'] = 'Sorry, Try Again.';
		}	 // Insert End	
	
		echo json_encode($result);
    } // Function End	
	 
	// Expert Query Popup Window 
	public function open_connect_expert()
	{
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->db->select('arai_registration.priority, arai_registration.xOrder, arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
		$this->db->order_by('(CASE priority WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 3 THEN 2 WHEN 0 THEN 3 END) ASC, years_of_experience_search DESC');
		
		$Featured_Experts = $this->master_model->getRecords('registration',array('registration.is_deleted'=>"0",'registration.admin_approval'=>"yes",'registration.status'=>"Active",'registration.user_sub_category_id' => '11','registration.contactable' => 'yes'));


			$featuredExpertsArr = array();
		if(count($Featured_Experts) > 0){
			foreach($Featured_Experts as $key => $featured_experts_val){
					
					$featured_experts_val['title'] 	= $encrptopenssl->decrypt($featured_experts_val['title']);
					$featured_experts_val['first_name'] 	= $encrptopenssl->decrypt($featured_experts_val['first_name']);
					$featured_experts_val['last_name'] 	= $encrptopenssl->decrypt($featured_experts_val['last_name']);
					$featured_experts_val['institution_full_name'] 	= @$encrptopenssl->decrypt($featured_experts_val['institution_full_name']);
					$featured_experts_val['designation_description'] 	= @$encrptopenssl->decrypt($featured_experts_val['designation_description']);
					$featured_experts_val['no_of_patents'] = @$encrptopenssl->decrypt($featured_experts_val['no_of_patents']);
					$featured_experts_val['no_of_paper_publication'] = @$featured_experts_val['no_of_paper_publication'];
					$featured_experts_val['years_of_experience'] = @$encrptopenssl->decrypt($featured_experts_val['years_of_experience']);
					$featured_experts_val['profile_picture'] = @$featured_experts_val['profile_picture'];
					$featured_experts_val['company_name'] 	= $encrptopenssl->decrypt($featured_experts_val['company_name']);
					$featured_experts_val['domain_area_of_expertise'] 	= @$encrptopenssl->decrypt($featured_experts_val['domain_area_of_expertise']);
					$featured_experts_val['designation'] = json_decode($featured_experts_val['designation']);		
					$featuredExpertsArr[] = $featured_experts_val;
				
			}
		}
		// echo "<pre>"; print_r($featuredExpertsArr);die;
		$data['all_experts']	= $featuredExpertsArr;

		$this->load->view('front/open_expert_form',$data);
	}// Expert Connect Insert Data

	// Expert Query Popup Window 
	

	
	public function open_expert_form_insert()	
    {
		// echo "<pre>";print_r($this->input->post());die();
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$expert_subject 	= $this->input->post('expert_subject');
		$expert_question 	= $this->input->post('expert_question');
		$expert_remark 		= $this->input->post('expert_remark');
		$user_id 			= $this->session->userdata('user_id');
		$connect_to_expert 			= $this->input->post('connect_to_expert');

		
		// Login User Details
		$userInfo = $this->master_model->getRecords('registration',array('user_id'=>$user_id));
			
		$user_email = $encrptopenssl->decrypt($userInfo[0]['email']);
		$primary_ID = $userInfo[0]['user_id'];
		$first_name = $encrptopenssl->decrypt($userInfo[0]['first_name']);
		$middle_name = $encrptopenssl->decrypt($userInfo[0]['middle_name']);
		$last_name = $encrptopenssl->decrypt($userInfo[0]['last_name']);
		$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);
		
		$insertArr = array( 'user_id' => $user_id, 'expert_subject' => $expert_subject, 'expert_question' => $expert_question, 'expert_remark' => $expert_remark,'connect_to_expert'=>$connect_to_expert);
		$insertQuery = $this->master_model->insertRecord('expert_connect_query',$insertArr);
	    
		if($insertQuery)
		{
			error_reporting(E_ALL);
			// Expert Connect Query Admin Email Functionality 
			$slug 				= $encrptopenssl->encrypt('expert_connect_query');
			$setting_table 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$subscriber_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			
			if(count($subscriber_mail) > 0){	
			
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$to_email 			= 'Office.tip@araiindia.com';
				//$to_email 			= 'jitendra.battise@esds.co.in';
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				// Receiver Name 
				$receiverName = 'Admin';
				if($expert_remark!=""){
					$arr_words = ['[RECEIVER]', '[USERNAME]', '[EMAIL]', '[USERID]', '[SUBJECT]', '[QUESTION]', '[REMARK]']; //'[SIGNATURE]'
					$rep_array = [$receiverName, $fullname, $user_email, $primary_ID, $expert_subject, $expert_question, $expert_remark];
				} else {
					$expert_remark = 'No';
					$arr_words = ['[RECEIVER]', '[USERNAME]', '[EMAIL]', '[USERID]', '[SUBJECT]', '[QUESTION]', '[REMARK]']; //'[SIGNATURE]'
					$rep_array = [$receiverName, $fullname, $user_email, $primary_ID, $expert_subject, $expert_question, $expert_remark];
				}
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				
				//email admin sending code 
				$info_array=array(
						'to'		=>	$to_email,					
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
				
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
								
			}
			
			// Email Receiver Email Functionality
			$slug_1 				= $encrptopenssl->encrypt('expert_connect_query_reply');
			$setting_table_1 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_mail 			= $this->master_model->getRecords("email_template", array("slug" => $slug_1));
			$email_content 			= '';
			if(count($contact_mail) > 0){	
				
					$title 	 			= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
					$description 		= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
					$from_admin_mail 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);
					//$new_email	= "vicky.kuwar@esds.co.in";	
					$new_email 			= $user_email;
					$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);
					
					$arr_words_1 	= ['[APPLICANT]']; //'[SIGNATURE]'
					$rep_array_1 	= [$fullname];
					$email_content  = str_replace($arr_words_1, $rep_array_1, $description);
					
					//email admin sending code 
					$info_array_1 = array(
											'to'		=>	$new_email,					
											'cc'		=>	'',
											'from'		=>	$from_admin_mail,
											'subject'	=> 	$title,
											'view'		=>  'common-file'
											);
					
					$other_infoarray_1	=	array('content' => $email_content); 
					
					$email_send_1 = $this->emailsending->sendmail($info_array_1,$other_infoarray_1);
					
				}
			
			$result['flag'] = 'Success';
			$result['message'] = 'Your request successfully submitted.';
			
		}  else {
			
			$result['flag'] = 'error';
			$result['message'] = 'Sorry, Try Again.';
		}	 // Insert End	
	
		echo json_encode($result);
  } // Function End


  public function open_expert_email_popup()
	{
		
		$data['expert_id'] 	= $this->input->post('expert_id');
		$this->load->view('front/open_expert_email_form',$data);
	}

	public function submit_expert_email_popup()	
    {
		// echo "<pre>";print_r($this->input->post());die();
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		$subject 	= $this->input->post('expert_subject');
		$message 	= $this->input->post('expert_msg');
		$user_id 			= $this->session->userdata('user_id');
		$expert_id 			= $this->input->post('expert_id');

		$insertArr = array( 'user_id' => $user_id, 'subject' => $subject, 'message' => $message, 'expert_id' => $expert_id);
		$insertQuery = $this->master_model->insertRecord('expert_connect_emails',$insertArr);
		// echo $this->db->last_query();
		// die;
	    
		if($insertQuery)
		{
			/*
			$userInfo = $this->master_model->getRecords('registration',array('user_id'=>$user_id));
			$user_email = $encrptopenssl->decrypt($userInfo[0]['email']);
			$primary_ID = $userInfo[0]['user_id'];
			$first_name = $encrptopenssl->decrypt($userInfo[0]['first_name']);
			$middle_name = $encrptopenssl->decrypt($userInfo[0]['middle_name']);
			$last_name = $encrptopenssl->decrypt($userInfo[0]['last_name']);
			$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);

			error_reporting(E_ALL);
			// Expert Connect Query Admin Email Functionality 
			$slug 				= $encrptopenssl->encrypt('expert_connect_query');
			$setting_table 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$subscriber_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
			$sub_content = '';
			
			if(count($subscriber_mail) > 0){	
			
				$subject_title 	 	= $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
				$desc 				= $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
				$fromadmin 			= $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);				
				$to_email 			= 'Office.tip@araiindia.com';
				//$to_email 			= 'jitendra.battise@esds.co.in';
				$sender_name  		= $encrptopenssl->decrypt($setting_table[0]['field_1']);
				$phoneNO  			= $encrptopenssl->decrypt($setting_table[0]['contact_no']);
				
				// Receiver Name 
				$receiverName = 'Admin';
				if($expert_remark!=""){
					$arr_words = ['[RECEIVER]', '[USERNAME]', '[EMAIL]', '[USERID]', '[SUBJECT]', '[QUESTION]', '[REMARK]']; //'[SIGNATURE]'
					$rep_array = [$receiverName, $fullname, $user_email, $primary_ID, $expert_subject, $expert_question, $expert_remark];
				} else {
					$expert_remark = 'No';
					$arr_words = ['[RECEIVER]', '[USERNAME]', '[EMAIL]', '[USERID]', '[SUBJECT]', '[QUESTION]', '[REMARK]']; //'[SIGNATURE]'
					$rep_array = [$receiverName, $fullname, $user_email, $primary_ID, $expert_subject, $expert_question, $expert_remark];
				}
				
				$sub_content = str_replace($arr_words, $rep_array, $desc);
				
				//email admin sending code 
				$info_array=array(
						'to'		=>	$to_email,					
						'cc'		=>	'',
						'from'		=>	$fromadmin,
						'subject'	=> 	$subject_title,
						'view'		=>  'common-file'
						);
				
				$other_infoarray	=	array('content' => $sub_content); 
				
				$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
				
								
			}
			
			// Email Receiver Email Functionality
			$slug_1 				= $encrptopenssl->encrypt('expert_connect_query_reply');
			$setting_table_1 		= $this->master_model->getRecords("setting", array("id" => '1'));
			$contact_mail 			= $this->master_model->getRecords("email_template", array("slug" => $slug_1));
			$email_content 			= '';
			if(count($contact_mail) > 0){	
				
					$title 	 			= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
					$description 		= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
					$from_admin_mail 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);
					//$new_email	= "vicky.kuwar@esds.co.in";	
					$new_email 			= $user_email;
					$fullname = ucfirst($first_name)." ".ucfirst($middle_name)." ".ucfirst($last_name);
					
					$arr_words_1 	= ['[APPLICANT]']; //'[SIGNATURE]'
					$rep_array_1 	= [$fullname];
					$email_content  = str_replace($arr_words_1, $rep_array_1, $description);
					
					//email admin sending code 
					$info_array_1 = array(
											'to'		=>	$new_email,					
											'cc'		=>	'',
											'from'		=>	$from_admin_mail,
											'subject'	=> 	$title,
											'view'		=>  'common-file'
											);
					
					$other_infoarray_1	=	array('content' => $email_content); 
					
					$email_send_1 = $this->emailsending->sendmail($info_array_1,$other_infoarray_1);
					
				}*/
			
			$result['flag'] = 'Success';
			$result['message'] = 'Your request successfully submitted.';
			
		}  else {
			
			$result['flag'] = 'error';
			$result['message'] = 'Sorry, Try Again.';
		}	 // Insert End	
	
		echo json_encode($result);
  } //


	 
	 public function open_subscription_modal_ajax()
	 {
			$this->load->view('front/modal_subscription_form');
	 }
	 
	public function expert_filter(){
		
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		sleep(1);		
		
		// Post Values
		$skillset 		= $this->input->post('skillset');
		$domain_exp 	= $this->input->post('domain_exp');
		$emp_status 	= $this->input->post('emp_status');
		$year_of_exp 	= $this->input->post('year_of_exp');
		$no_of_pub 		= $this->input->post('no_of_pub');
		$no_of_patents 	= $this->input->post('no_of_patents');
		$keyword 		= rtrim($this->input->post('search_txt'));
		//echo "<pre>";print_r($this->input->post());
		$c_data = "";
		
		//skill set
		if(count((array)$skillset) > 0){
			
			$c_data .= " AND (";
			$i = 0;
			foreach($skillset as $t){
				
				if($i!=(count($skillset) - 1)){
					$c_data .= " FIND_IN_SET('".$t."',`skill_sets_search`) OR ";
				} else {
					$c_data .= " FIND_IN_SET('".$t."',`skill_sets_search`))";
				}				
				$i++;
			}
			
		} // End Skill Set IF
		
		//domain set
		if(count((array)$domain_exp) > 0){
			
			$c_data .= " AND (";
			$i = 0;
			foreach($domain_exp as $t){
				
				if($i!=(count($domain_exp) - 1)){
					$c_data .= " FIND_IN_SET('".$t."',`domain_area_of_expertise_search`) OR ";
				} else {
					$c_data .= " FIND_IN_SET('".$t."',`domain_area_of_expertise_search`))";
				}				
				$i++;
			}
			
		} // End domain If 
		
		// Employement Status
		if($emp_status)
		{
			$c_data	.= " AND arai_student_profile.employement_status = '".$encrptopenssl->encrypt($emp_status)."'";
		}
		
		// Year Of Experience
		if($year_of_exp)
		{
			$c_data	.= " AND years_of_experience_search >= '".$year_of_exp."'";
		}
		
		// No Of Publications
		if($no_of_pub)
		{
			$c_data	.= " AND no_of_paper_publication >= '".$no_of_pub."'";
		}
		
		// No Of Patents Here
		if($no_of_patents)
		{
			$c_data	.= " AND no_of_patents >= '".$no_of_patents."'";
		}
		
		if($keyword)
		{
			$enryptString = $encrptopenssl->encrypt($keyword);
			$designation = "$keyword";
			
			//$str1 = '"%"first_name":"'.$designation.'%"';
			//$c_data	.= " AND (`arai_student_profile`.`designation` LIKE '%".$designation."%' || `arai_student_profile`.`company_name` = '".$enryptString."' || `arai_registration`.`first_name` = '".$enryptString."' || `arai_registration`.`middle_name` = '".$enryptString."' || `arai_registration`.`last_name` = '".$enryptString."')";
			//$c_data	.= " AND (`arai_registration`.`json_str` LIKE ".$str1.")";
			$str1 = '"%'.$designation.'%"';
			$c_data	.= " AND (`arai_student_profile`.`designation` LIKE '%".$designation."%' || `arai_student_profile`.`company_name` = '".$enryptString."' || `arai_registration`.`json_str` LIKE ".$str1.")";
		}
		
		 $sql = "SELECT arai_registration.priority, arai_registration.xOrder, arai_registration.user_id, arai_registration.json_str,  
				arai_registration.title,arai_registration.first_name, arai_registration.last_name, 
				arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience,
				arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture FROM `arai_registration` 
				LEFT JOIN `arai_student_profile` ON `arai_student_profile`.`user_id`=`arai_registration`.`user_id` 
				WHERE `arai_registration`.`status` = 'Active' AND `arai_registration`.`admin_approval` = 'yes' AND `arai_registration`.`is_deleted` = '0'
				AND `arai_registration`.`user_sub_category_id` = '11' ".$c_data." ORDER BY (CASE priority WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 3 THEN 2 WHEN 0 THEN 3 END) ASC, years_of_experience_search DESC";
		//echo $sql;
		$result 	= $this->db->query($sql)->result();	
		
		$csrf_test_name = $this->security->get_csrf_hash();
		
		$showData = '';
		
		if(count($result) > 0){
			
			foreach($result as $ex_data){
				
				$prof_pic = $ex_data->profile_picture;
				
				if($prof_pic!=""){
					$image = base_url('assets/profile_picture/'.$prof_pic);
					
					$profile_picture = $image;
					
				} else {
					$profile_picture = base_url('assets/front/img/testimonial-1.jpg');
				}	

				$designation = json_decode($ex_data->designation);
				$yearExp = $ex_data->years_of_experience;
				$no_pa = $ex_data->no_of_paper_publication;
				if($yearExp){
					$ye = $encrptopenssl->decrypt($yearExp);
				} else {
					$ye = 0;
				}

				if($no_pa){
					$ap = $no_pa;
				} else {
					$ap = 0;
				}		
				
				$nextid = $ex_data->user_id;
				
				$fullname = $encrptopenssl->decrypt($ex_data->first_name).' '.$encrptopenssl->decrypt($ex_data->last_name);
				$company_name = $encrptopenssl->decrypt($ex_data->company_name);
				 $showData .= '<div class="col-md-4 p3">
									<div class="expertsListBox allExperts">
									  
										   <img src="'.$profile_picture.'" alt="experts">
									   
									   <div class="experts">
										   <span class="read"><a href="'.base_url('home/viewProfile/'.base64_encode($ex_data->user_id)).'">View Profile</a></span>
										  
											 <h3>'.$fullname.'</h3>	
											<p class="c_name">'.$company_name.'</p>
											<p class="d_name">'.$designation[0].'</p>
									   </div>						   
									   <div class="expertsBox">
										   <ul>                                  
											   <li><span>'.$ye.'</span>Years of Experience</li>                                       
											   <li><span>'.$ap.'</span> Academic Publications</li>                                   
										   </ul>
									   </div>
									</div>
								 </div>';
				
			} // Foreach End			
			
			$jsonData = array("html" => $showData, "token" => $csrf_test_name,'count_of_experts'=>count($result));
			echo json_encode($jsonData);
			
		} else {
			
			$jsonData = array("html" => '<div class="text-center" style="width:100%;">No Record Found</div>', "token" => $csrf_test_name,'count_of_experts'=>count($result));
			echo json_encode($jsonData);
			
		}	
		
		
	}	// Filter End
	
	
	
	public function getmore(){
		
		// Create Object
		$expert_encrypt =  New Opensslencryptdecrypt();
		sleep(1);
		$csrf_test_name = $this->security->get_csrf_hash();
		
		$id = $this->input->post('id');		
		
		$this->db->select('arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
		$total_cnt = $this->master_model->getRecords('registration',array('registration.admin_approval'=>"yes",'registration.status'=>"Active",'registration.user_sub_category_id' => '11'));
	
		
		
		/* $sql = "SELECT * FROM `arai_registration` LEFT JOIN `arai_student_profile` ON `arai_student_profile`.`user_id`=`arai_registration`.`user_id` 
				WHERE `arai_registration`.`status` = 'Active' AND `arai_registration`.`admin_approval` = 'yes' 
				AND `arai_registration`.`user_sub_category_id` = '11' ORDER BY `arai_registration`.`xOrder` ASC LIMIT ".$id.",".$showLimit."";
		
		$result 	= $this->db->query($sql)->result(); */	
		
		$showLimit = 30;
		$result = $this->getAllExpertData($id, $showLimit);
		
		$showData = '';
		
		if(count($result) > 0){
			
			foreach($result as $ex_data){
				
				$prof_pic = $ex_data['profile_picture'];
				
				if($prof_pic!=""){
					$image = base_url('assets/profile_picture/'.$prof_pic);
					
					$profile_picture = $image;
					
				} else {
					$profile_picture = base_url('assets/front/img/testimonial-1.jpg');
				}	

				$designation = json_decode($ex_data['designation']);
				$yearExp = $ex_data['years_of_experience'];
				$no_pa = $ex_data['no_of_paper_publication'];
				if($yearExp){
					$ye = $expert_encrypt->decrypt($yearExp);
				} else {
					$ye = 0;
				}

				if($no_pa){
					$ap = $no_pa;
				} else {
					$ap = 0;
				}		
				
				$nextid = $ex_data['user_id'];
				
				$fullname = $expert_encrypt->decrypt($ex_data['first_name']).' '.$expert_encrypt->decrypt($ex_data['last_name']);
				$company_name = $expert_encrypt->decrypt($ex_data['company_name']);
				 $showData .= '<div class="col-md-4 p3">
									<div class="expertsListBox allExperts">
									  
										   <img src="'.$profile_picture.'" alt="experts">
									   
									   <div class="experts">
										   <span class="read"><a href="'.base_url('home/viewProfile/'.base64_encode($ex_data['user_id'])).'">View Profile</a></span>
										  
											 <h3>'.$fullname.'</h3>	
											<p class="c_name">'.$company_name.'</p>
											<p class="d_name">'.$designation[0].'</p>
									   </div>						   
									   <div class="expertsBox">
										   <ul>                                  
											   <li><span>'.$ye.'</span>Years of Experience</li>                                       
											   <li><span>'.$ap.'</span> Academic Publications</li>                                   
										   </ul>
									   </div>
									</div>
								 </div>';
				
				
			} // Foreach End
			
			if($total_cnt > $showLimit){
			
			$showData .= '<div class="col-md-12 text-center show_more_main" id="show_more_main'.$nextid.'">
					<span id="'.$nextid.'" class="show_more btn btn-general btn-white" title="Load more">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>';
			
			}
			//print_r($showData);
			$jsonData = array("html" => $showData, "token" => $csrf_test_name);
			
			echo json_encode($jsonData);			
		} else {
			
			$jsonData = array("html" => '', "token" => $csrf_test_name);
			echo json_encode($jsonData);
			
		}
		
	}
	
	public function viewAllExperts()
   	{	
		//error_reporting(0);
		$this->check_permissions->is_logged_in();
   		$encrptopenssl =  New Opensslencryptdecrypt();
		
		//Sill-Set Management			
		$skillset_mgt= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');			
				
		//Sill-Set Management More			
		$skillset_mgt2= $this->master_model->array_sorting($this->master_model->getRecords('skill_sets',array('status'=>"Active")), array('id'),'name');		
		
		
		//Domain Area Expertise Management
		$this->db->select('id, domain_name');	
		$domain_mgt= $this->master_model->array_sorting($this->master_model->getRecords('domain_master',array('status'=>"Active")), array('id'),'domain_name');		
		
		$domain_mgt2= $this->master_model->array_sorting($this->master_model->getRecords('domain_master',array('status'=>"Active")), array('id'),'domain_name');		
		$Featured_Experts = $this->getAllExpertData(0, 6);
   		
		//echo $this->db->last_query();
   		$featuredExpertsArr = array();
		if(count($Featured_Experts) > 0){
			foreach($Featured_Experts as $key => $featured_experts_val){
					
					$featured_experts_val['title'] 	= $encrptopenssl->decrypt($featured_experts_val['title']);
					$featured_experts_val['first_name'] 	= $encrptopenssl->decrypt($featured_experts_val['first_name']);
					$featured_experts_val['last_name'] 	= $encrptopenssl->decrypt($featured_experts_val['last_name']);
					$featured_experts_val['institution_full_name'] 	= @$encrptopenssl->decrypt($featured_experts_val['institution_full_name']);
					$featured_experts_val['designation_description'] 	= @$encrptopenssl->decrypt($featured_experts_val['designation_description']);
					$featured_experts_val['no_of_patents'] = @$encrptopenssl->decrypt($featured_experts_val['no_of_patents']);
					$featured_experts_val['no_of_paper_publication'] = @$featured_experts_val['no_of_paper_publication'];
					$featured_experts_val['years_of_experience'] = @$encrptopenssl->decrypt($featured_experts_val['years_of_experience']);
					$featured_experts_val['profile_picture'] = @$featured_experts_val['profile_picture'];
					$featured_experts_val['company_name'] 	= $encrptopenssl->decrypt($featured_experts_val['company_name']);
					$featured_experts_val['domain_area_of_expertise'] 	= @$encrptopenssl->decrypt($featured_experts_val['domain_area_of_expertise']);
					$featured_experts_val['designation'] = json_decode($featured_experts_val['designation']);		
					$featuredExpertsArr[] = $featured_experts_val;
				
			}
		}
		
		$count_experts = $this->master_model->getRecords('registration',array('registration.admin_approval'=>"yes",'registration.status'=>"Active",'registration.user_sub_category_id' => '11','is_deleted'=>"0"),'',array('registration.user_id' => "DESC"));
		
		$data['year_of_exp']			= $this->master_model->getRecords('year_of_exp',array('status'=>"Active"));
		$data['year_of_exp_more']			= $this->master_model->getRecords('year_of_exp',array('status'=>"Active"));
		$data['no_of_patents']			= $this->master_model->getRecords('no_of_patents',array('status'=>"Active"));
		$data['no_of_publication']		= $this->master_model->getRecords('no_paper_publications',array('status'=>"Active"));
		$data['employement_status']		= $this->master_model->getRecords('employement_master',array('status'=>"Active"));
   		$data['employement_more']		= $this->master_model->getRecords('employement_master',array('status'=>"Active"));
		$data['all_featured_experts']	= $featuredExpertsArr;
		$data['count_featured']			= $count_experts;
		$data['skill_set']				= $skillset_mgt;
		$data['skillmore']				= $skillset_mgt2;
		$data['domain_list']			= $domain_mgt; 
		$data['domain_more']			= $domain_mgt2; 	
   		
   		$profileInformation = $this->master_model->getRecords("student_profile");
		$data['page_title']='Expert';
		$data['middle_content']='view_all_experts';
		$this->load->view('front/front_combo',$data);		
   		
   	}

	public function getAllExpertData($start_limit=0,$end_limit=0)
	{
		/*$this->db->select('arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
		$this->db->order_by('FIELD(arai_registration.xOrder, "0") ASC, arai_registration.xOrder ASC', FALSE);
		*/
		$this->db->select('arai_registration.priority, arai_registration.xOrder, arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
		$this->db->order_by('(CASE priority WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 3 THEN 2 WHEN 0 THEN 3 END) ASC, years_of_experience_search DESC');
		
		$Featured_Experts = $this->master_model->getRecords('registration',array('registration.is_deleted'=>"0",'registration.admin_approval'=>"yes",'registration.status'=>"Active",'registration.user_sub_category_id' => '11'),'','',$start_limit,$end_limit);
		//echo $this->db->last_query();
		return $Featured_Experts;
	}
		
	/*public function getAllExpertData($start_limit=0,$end_limit=0)
		{
			$this->db->select('arai_registration.user_id, arai_registration.title,arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
   		$this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
			$this->db->order_by('FIELD(arai_registration.xOrder, "0") ASC, arai_registration.xOrder ASC', FALSE);
   		$Featured_Experts = $this->master_model->getRecords('registration',array('registration.is_deleted'=>"0",'registration.admin_approval'=>"yes",'registration.status'=>"Active",'registration.user_sub_category_id' => '11'),'','',$start_limit,$end_limit);
			return $Featured_Experts;
		}*/
	
	
	public function viewProfile($id)
   	{
		error_reporting(0);
		$this->check_permissions->is_logged_in();
		$profileId = base64_decode($id);
   		$encrptopenssl =  New Opensslencryptdecrypt();

		
		
		//$this->db->select('arai_registration.user_id, arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
		
		//$userD = $this->master_model->getRecords("registration",array('user_id' => $profileId));
		/*if($userD[0]['user_category_id'] == 1)
		{
				$this->db->join('registration','student_profile.user_id=registration.user_id','left');
				$profileInformation = $this->master_model->getRecords("student_profile",array('student_profile.user_id' => $profileId));
		
		} else {
			
			$this->db->join('registration','student_profile.user_id=registration.user_id','left');
			$profileInformation = $this->master_model->getRecords("profile_organization",array('profile_organization.user_id' => $profileId));
		
		}*/
		
		$this->db->join('registration','student_profile.user_id=registration.user_id','left');
		$profileInformation = $this->master_model->getRecords("student_profile",array('student_profile.user_id' => $profileId));
		
		//echo $this->db->last_query();die();	
   		$profile_information_arr = array();
		if(count($profileInformation)){			
			foreach($profileInformation as $row_val){	
				$row_val['status'] = $encrptopenssl->decrypt($row_val['status']);
				$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
				$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);				
				$row_val['company_name'] = $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['domain_area_of_expertise'] = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));
				
				$row_val['other_domian'] = $encrptopenssl->decrypt($row_val['other_domian']);
				$row_val['other_skill_sets'] = $encrptopenssl->decrypt($row_val['other_skill_sets']);

				$row_val['areas_of_interest'] = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
				$row_val['skill_sets'] = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));
				$row_val['event_experience_in_year'] = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
				$row_val['event_description_of_work'] = $encrptopenssl->decrypt($row_val['event_description_of_work']);
				$row_val['technical_experience_in_year'] = $encrptopenssl->decrypt($row_val['technical_experience_in_year']);
				$row_val['technical_description_of_work'] = $encrptopenssl->decrypt($row_val['technical_description_of_work']);
				//$row_val['technical_description_of_work'] = $row_val['technical_description_of_work'];
				$row_val['bio_data'] = $encrptopenssl->decrypt($row_val['bio_data']);
				$row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);
				$row_val['no_of_paper_publication'] = $row_val['no_of_paper_publication'];
				$row_val['no_of_patents'] = $row_val['no_of_patents'];
				
				$row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);
				$row_val['university_course'] = $encrptopenssl->decrypt($row_val['university_course']);
				$row_val['university_since_year'] = $encrptopenssl->decrypt($row_val['university_since_year']);
				$row_val['university_to_year'] = $encrptopenssl->decrypt($row_val['university_to_year']);
				$row_val['university_name'] = $encrptopenssl->decrypt($row_val['university_name']);
				$row_val['university_location'] = $encrptopenssl->decrypt($row_val['university_location']);
				$row_val['current_study_course'] = $encrptopenssl->decrypt($row_val['current_study_course']);
				$row_val['current_study_since_year'] = $encrptopenssl->decrypt($row_val['current_study_since_year']);
				$row_val['current_study_to_year'] = $encrptopenssl->decrypt($row_val['current_study_to_year']);
				$row_val['current_study_description'] = $encrptopenssl->decrypt($row_val['current_study_description']);
				$row_val['designation'] = json_decode($row_val['designation']);		
				$row_val['designation_since_year'] = $encrptopenssl->decrypt($row_val['designation_since_year']);
				$row_val['designation_description'] = $encrptopenssl->decrypt($row_val['designation_description']);
				$row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
				
				$row_val['corporate_linkage_code'] = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
				
				$row_val['pincode'] = $encrptopenssl->decrypt($row_val['pincode']);		
				$row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
				$row_val['area_colony_street_village'] = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
				$row_val['town_city_and_state'] = $encrptopenssl->decrypt($row_val['town_city_and_state']);
				$row_val['country'] = $encrptopenssl->decrypt($row_val['country']);
				$row_val['student_id_proof'] = $encrptopenssl->decrypt($row_val['student_id_proof']);
				$row_val['specify_fields_area_that_you_would_like'] = $encrptopenssl->decrypt($row_val['specify_fields_area_that_you_would_like']);
				
				$row_val['paper_year'] = $encrptopenssl->decrypt($row_val['paper_year']);
				$row_val['paper_conf_name'] = $encrptopenssl->decrypt($row_val['paper_conf_name']);
				$row_val['paper_title'] = $encrptopenssl->decrypt($row_val['paper_title']);		
				
				$row_val['patent_year'] = $encrptopenssl->decrypt($row_val['patent_year']);
				$row_val['patent_number'] = $encrptopenssl->decrypt($row_val['patent_number']);
				$row_val['patent_title'] = $encrptopenssl->decrypt($row_val['patent_title']);
				$row_val['portal_name'] = $encrptopenssl->decrypt($row_val['portal_name']);
				$row_val['portal_link'] = $encrptopenssl->decrypt($row_val['portal_link']);
				$row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);
				 $row_val['profile_picture'] = $row_val['profile_picture'];		
				 $row_val['contactable'] = $row_val['contactable'];					
				 
				$profile_information_arr[] = $row_val;		
			}
		}
		$data['profile_information'] = $profile_information_arr;
		// echo "<pre>";print_r($data['profile_information']);die();
		foreach ($profile_information_arr[0]['domain_area_of_expertise'] as $key => $value) {
			$domain_data[] = $this->master_model->getRecords("domain_master",array('id' => $value));
		}

		foreach ($profile_information_arr[0]['skill_sets'] as $key => $value) {
			$skill_sets[] = $this->master_model->getRecords("skill_sets",array('id' => $value));
		}

		foreach ($profile_information_arr[0]['areas_of_interest'] as $key => $value) {
			$area_of_interest[] = $this->master_model->getRecords("area_of_interest",array('id' => $value));
		}

		$domain_arr = array();
		if(count($domain_data)){			
			foreach($domain_data as $row_val){			
				$row_val[0]['domain_name'] = $encrptopenssl->decrypt($row_val[0]['domain_name']);
				$domain_arr[] = $row_val;
			}
			
		}
		$data['domain'] = $domain_arr;

		$skill_sets_arr = array();
		if(count($skill_sets)){	
						
			foreach($skill_sets as $row_val){		
						
				$row_val[0]['name'] = $encrptopenssl->decrypt($row_val[0]['name']);
				$skill_sets_arr[] = $row_val;
			}
			
		}
		$data['skill_sets'] = $skill_sets_arr;

		$area_of_interest_arr = array();
		if(count($area_of_interest)){	
						
			foreach($area_of_interest as $row_val){		
						
				$row_val[0]['area_of_interest'] = $encrptopenssl->decrypt($row_val[0]['area_of_interest']);
				$area_of_interest_arr[] = $row_val;
			}
			
		}
		$data['area_of_interest'] = $area_of_interest_arr;
		// echo "<pre>";
		// print_r($data);die;
		$setting_data = $this->master_model->getRecords("setting");
		$data['admin_expert_connect']=$setting_data[0]['expert_connect'];
 		$data['page_title']='Expert';
		$data['middle_content']='expertDetail';
		$this->load->view('front/front_combo',$data);
   	}
	
	public function get_expert_more()
       {
         $encrptopenssl =  New Opensslencryptdecrypt();
         sleep(1);
        $csrf_test_name = $this->security->get_csrf_hash();
        $id = $this->input->post('id');
        $start = $id;
        $end = $start;

 
		$this->db->select('arai_registration.user_id, arai_registration.first_name, arai_registration.last_name, arai_student_profile.company_name, arai_student_profile.designation, arai_student_profile.years_of_experience, arai_student_profile.no_of_paper_publication, arai_student_profile.profile_picture');
        $this->db->join('student_profile','student_profile.user_id=registration.user_id','left');
        $Featured_Experts = $this->master_model->getRecords('registration',array('registration.status'=>"Active",'registration.is_featured'=>'Y','registration.user_sub_category_id' => '11'),'','',$start,$end);

 

           $featuredExpertsArr = array();
        if(count($Featured_Experts)){
            foreach($Featured_Experts as $key => $featured_experts_val){
                if($featured_experts_val['user_id'])
                {
                    $featured_experts_val['first_name']     = $encrptopenssl->decrypt($featured_experts_val['first_name']);
                    $featured_experts_val['last_name']     = $encrptopenssl->decrypt($featured_experts_val['last_name']);
                    $featured_experts_val['institution_full_name']     = $encrptopenssl->decrypt($featured_experts_val['institution_full_name']);
                    $featured_experts_val['designation_description']     = $encrptopenssl->decrypt($featured_experts_val['designation_description']);
                    $featured_experts_val['no_of_patents'] = $encrptopenssl->decrypt($featured_experts_val['no_of_patents']);
                    $featured_experts_val['no_of_paper_publication'] = $encrptopenssl->decrypt($featured_experts_val['no_of_paper_publication']);
                    $featured_experts_val['years_of_experience'] = $encrptopenssl->decrypt($featured_experts_val['years_of_experience']);
                    $featured_experts_val['profile_picture'] = $featured_experts_val['profile_picture'];
                    $row_val['designation'] = json_decode($featured_experts_val['designation']);
                    $featuredExpertsArr[] = $featured_experts_val;
                } 
            }
        } 
        $data['all_featured_experts']= $featuredExpertsArr;
        $this->load->view('load_more',$data);
        
    }
	
	
	// About BYT Page
	public function aboutByt(){
	
		//error_reporting(0);
		// Create Object
		$encrptopenssl =  New Opensslencryptdecrypt();
		
		
		
		$How_It_Works  		= $this->master_model->getRecords('how_it_works',array('status'=>"Active"));
		$consortium_mgt  	= $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		$about_data 		= $this->master_model->getRecords('byt_landing_page',array('a_id' => '1'));
	
		// How It Works 
		$howItWorksArr = array();
		if(count($How_It_Works) > 0){				
					
			foreach($How_It_Works as $how_it_work_val){
				
				$how_it_work_val['subject_title'] 	= $encrptopenssl->decrypt($how_it_work_val['subject_title']);
				$how_it_work_val['first_title'] 	= $encrptopenssl->decrypt($how_it_work_val['first_title']);
				$how_it_work_val['first_description'] 	= $encrptopenssl->decrypt($how_it_work_val['first_description']);
				$how_it_work_val['first_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['first_icon_image']);
				$how_it_work_val['second_title'] 	= $encrptopenssl->decrypt($how_it_work_val['second_title']);
				$how_it_work_val['second_description'] 	= $encrptopenssl->decrypt($how_it_work_val['second_description']);
				$how_it_work_val['second_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['second_icon_image']);
				$how_it_work_val['third_title'] 	= $encrptopenssl->decrypt($how_it_work_val['third_title']);
				$how_it_work_val['third_description'] 	= $encrptopenssl->decrypt($how_it_work_val['third_description']);
				$how_it_work_val['third_icon_image'] 	= $encrptopenssl->decrypt($how_it_work_val['third_icon_image']);
				$how_it_work_val['background_image'] 	= $encrptopenssl->decrypt($how_it_work_val['background_image']);
				$howItWorksArr[] = $how_it_work_val;
				
			}
			
		}


		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['img_link'] 		= $con_val['img_link'];
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}	
		
		$data['about_byt'] = $about_data;
		$data['consortium_list']= $consortArr;
		$data['how_it_works']= $howItWorksArr;
		//$data['about_data']= $res_arr;
		$data['page_title']='About BYT';
		$data['middle_content']='about-byt-cms';
		$this->load->view('front/front_combo',$data);
	}
	
  public function participate($cid=0)
	{	
		if($cid == '0') { $this->session->set_flashdata('error','Invalid challenge selected'); redirect(site_url('challenge')); }
		else
		{
			$cid = base64_decode($cid);
			$data['challenge_data'] = $challenge_data = $this->master_model->getRecords("challenge", array('c_id'=>$cid));
			//echo $this->db->last_query(); exit;
			if(count($challenge_data) == 0) //Check challenge exist or not
			{ 
				$this->session->set_flashdata('error','Challenge not exist');
				redirect(site_url('challenge')); 
			}
			else 
			{
				if($challenge_data[0]['challenge_status'] != 'Approved') //Check challenge approves or not
				{ 
					$this->session->set_flashdata('error','Challenge is not approved');
					redirect(site_url('challenge')); 
				}
				else if($challenge_data[0]['status'] != 'Active') //Check challenge Active or not
				{ 
					$this->session->set_flashdata('error','Challenge is not Active');
					redirect(site_url('challenge'));
				}
				else if($challenge_data[0]['challenge_launch_date'] > date("Y-m-d") && $challenge_data[0]['challenge_close_date'] < date("Y-m-d")) //Check challenge valid date
				{
					$this->session->set_flashdata('error','Challenge launch date or close date is invalid');
					redirect(site_url('challenge/challengeDetails/'.base64_encode($challenge_data[0]['c_id']))); 
				}
			}
		}
		
		if(isset($_POST) && Count($_POST) > 0)
		{
			$i_agree = $this->input->post('i_agree', TRUE);
			if(isset($i_agree) && $i_agree == '1')
			{
				$add_data['c_id'] = $cid;
				$add_data['user_id'] = $this->session->userdata('user_id');
				$add_data['created_on'] = date("Y-m-d H:i:s");
				$log_id = $this->master_model->insertRecord('arai_participation_log',$add_data,TRUE);
				
				if($log_id != "" && $log_id > 0)
				{
					$logDetails 	= array(
							'user_id' 		=> $this->session->userdata('user_id'),
							'action_name' 	=> "Challenge Participation log - google form link - success",
							'module_name'	=> 'Challenge Participation Success',
							'store_data'	=> json_encode($add_data),
							'ip_address'	=> $this->get_client_ip(),
							'createdAt'		=> date("Y-m-d H:i:s")
						);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					$c_id = $challenge_data[0]['c_id'];
					redirect(site_url('challenge/participate_link/'.base64_encode($c_id))); 
				}
				else
				{
					$logDetails 	= array(
							'user_id' 		=> $this->session->userdata('user_id'),
							'action_name' 	=> "Challenge Participation log - google form link - error",
							'module_name'	=> 'Challenge Participation Error',
							'store_data'	=> json_encode($add_data),
							'ip_address'	=> $this->get_client_ip(),
							'createdAt'		=> date("Y-m-d H:i:s")
						);
					$logData = $this->master_model->insertRecord('logs',$logDetails);
					
					$this->session->set_flashdata('error','Error occurred. Please try after sometime.');
					redirect(site_url('challenge/challengeDetails/'.base64_encode($challenge_data[0]['c_id']))); 
				}
			}
			else
			{
				$this->session->set_flashdata('error','Error occurred. Please try after sometime.');
				redirect(site_url('challenge/challengeDetails/'.base64_encode($challenge_data[0]['c_id']))); 
			}
		}
		
		$data['page_title'] 	 = 'Home';
		$data['submodule_name']  = '';	
		$data['middle_content']	 = 'challenge/participate';
		$this->load->view('front/front_combo',$data);
	}
	
	public function comingSoon()
	{
		$data['page_title'] 	 = 'Home';
		$data['submodule_name']  = '';	
		$data['middle_content']	 = 'coming_soon';
		$this->load->view('front/front_combo',$data);
	}
	
	public function deleteAccount(){
		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$csrf_test_name = $this->security->get_csrf_hash();
		$userID = $this->session->userdata('user_id');
		if($userID == ""){
			$this->session->set_flashdata('error','Please login, to perform this action');
			redirect(site_url()); 
		} 
		
		// Fetch Register Details		
		$user_details 	= $this->master_model->getRecords("registration", array("user_id" => $userID));
		$fullName     	= ucfirst($encrptopenssl->decrypt($user_details[0]['first_name']))." ".ucfirst($encrptopenssl->decrypt($user_details[0]['last_name']));
		$email_id	 	= $encrptopenssl->decrypt($user_details[0]['email']);
		
		if($user_details[0]['user_category_id'] == 1){
			$accountType = 'Individual';
		} else {
			$accountType = 'Organizational';
		}
		
		
		// Registration Table
		$upArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateRegisteration = $this->master_model->updateRecord('registration',$upArray,array('user_id' => $userID));

		// Reference Challenge Data Removed
		$challengeArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateChallenge = $this->master_model->updateRecord('challenge',$challengeArray,array('u_id' => $userID));
	
		// Reference Teams Data Removed
		$teamArray = array('is_deleted' => '1', 'modified_on' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateTeam = $this->master_model->updateRecord('byt_teams',$teamArray,array('user_id' => $userID));
	
		// Reference Slot Data Removed
		$slotArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateSlot = $this->master_model->updateRecord('byt_slot_applications',$slotArray,array('apply_user_id' => $userID));
		
		// Remove entry from Notifications
		$newsletterArray = array('is_deleted' => '1', 'challenge_notification' => '0', 'news_notification' => '0', 'updatedAt' => date('Y-m-d H:i:s'));
		$updateNewsletter = $this->master_model->updateRecord('newsletter',$newsletterArray,array('email_id' => $email_id));


		// Delete Account Email TO User
		$slug 			= $encrptopenssl->encrypt('delete_account');
		$delete_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
		$setting_table 	= $this->master_model->getRecords("setting", array("id" => '1'));
		$content = '';
		if(count($delete_mail) > 0){	
			
			$subject 	 	= $encrptopenssl->decrypt($delete_mail[0]['email_title']);
			$description 	= $encrptopenssl->decrypt($delete_mail[0]['email_description']);
			$from_admin 	= $encrptopenssl->decrypt($delete_mail[0]['from_email']);				
			$from_names 	= 'Admin';
			$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);			
			
			$base_url			= base_url('unsubscribe/unsubscribe_user/'.$email_id);
			$unsubscribe		= "If you do not want to receive this mailer,<a href='".$base_url."'>click here</a>";		
			
			$arr_words = ['[USERNAME]', '[ACCOUNTTYPE]', '[EMAIL]', '[UNSUBSCRIBE]'];
			$rep_array = [$fullName,  $accountType, $email_id, $unsubscribe];
			
			$content = str_replace($arr_words, $rep_array, $description);
			
			//email admin sending code 
			$info_arr=array(
					'to'		=>	$email_id,	//$email_id				
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject,
					'view'		=>  'common-file'
					);
			
			$other_info=array('content'=>$content); 
			
			$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
			
		}
		
		// End Email TO User
		
		// Email TO Admin 
		$slug_1 			= $encrptopenssl->encrypt('delete_account_email_to_admin');
		$delete_mail_admin 	= $this->master_model->getRecords("email_template", array("slug" => $slug_1));
		$setting_table_1 	= $this->master_model->getRecords("setting", array("id" => '1'));
		$content_1 = '';
		if(count($delete_mail_admin) > 0){	
			
			$subject1 	 	= $encrptopenssl->decrypt($delete_mail_admin[0]['email_title']);
			$description1 	= $encrptopenssl->decrypt($delete_mail_admin[0]['email_description']);
			$from_admin 	= $encrptopenssl->decrypt($delete_mail_admin[0]['from_email']);				
			$from_names 	= 'Admin';
			$sendername  	= $encrptopenssl->decrypt($setting_table_1[0]['field_1']);
			$admin_email  	= $encrptopenssl->decrypt($setting_table_1[0]['field_2']);
			
			$arr_words = ['[ACCOUNTTYPE]', '[EMAIL]'];
			$rep_array = [$accountType, $email_id];
			
			$content_1 = str_replace($arr_words, $rep_array, $description1);
			
			//email admin sending code 
			$info_arr=array(
					'to'		=>	$admin_email,//$admin_email					
					'cc'		=>	'',
					'from'		=>	$from_admin,
					'subject'	=> 	$subject1,
					'view'		=>  'common-file'
					);
			
			$other_info=array('content'=>$content_1); 
			
			$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
			
		}	
		
		session_destroy();
		$this->session->sess_destroy();
		$returnArr = array("message" => 'Your account successfully deleted from Technovuus', "token" => $csrf_test_name);		
		echo json_encode($returnArr);
		
	} // Delete Account
	
	/*public function deleteAccount(){
		
		$csrf_test_name = $this->security->get_csrf_hash();
		$userID = $this->session->userdata('user_id');
		if($userID == ""){
			$this->session->set_flashdata('error','Please login, to perform this action');
			redirect(site_url()); 
		}
		
		// Registration Table
		$upArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateRegisteration = $this->master_model->updateRecord('registration',$upArray,array('user_id' => $userID));

		// Reference Challenge Data Removed
		$challengeArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateChallenge = $this->master_model->updateRecord('challenge',$challengeArray,array('u_id' => $userID));
	
		// Reference Teams Data Removed
		$teamArray = array('is_deleted' => '1', 'modified_on' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateTeam = $this->master_model->updateRecord('byt_teams',$teamArray,array('user_id' => $userID));
	
		// Reference Slot Data Removed
		$slotArray = array('is_deleted' => '1', 'updatedAt' => date('Y-m-d H:i:s'));//'is_deleted' => '1', 
		$updateSlot = $this->master_model->updateRecord('byt_slot_applications',$slotArray,array('user_id' => $userID));	
		
		session_destroy();
		$this->session->sess_destroy();
		$returnArr = array("message" => 'Your account successfully deleted from Technovuus', "token" => $csrf_test_name);		
		echo json_encode($returnArr);
		
	}*/ // Delete Account

	public function unset_lang_cookie()
	{
		 if(!isset($_COOKIE['googtrans'])) {
				setcookie("googtrans","",time()-3600);
		}
	}
	
} // Class End
