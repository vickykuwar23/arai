<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Class : Student_profile
Author : Suraj M
 */

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');

        $this->load->model('Common_model_sm');

        $this->load->library('Opensslencryptdecrypt');
        if ($this->session->userdata('user_id') == "") {
            redirect(base_url('login'));
        }
        if ($this->session->userdata('user_category') != 1) {redirect(site_url());}
    }

    public function abc()
    {
        die;
        $encrptopenssl = new Opensslencryptdecrypt();

        $user_data = $this->master_model->getRecords("student_profile", '', 'id,user_id,domain_area_of_expertise,skill_sets');
        $user_arr  = array();
        foreach ($user_data as $key => $row_val) {
            $row_val['domain_area_of_expertise'] = $encrptopenssl->decrypt($row_val['domain_area_of_expertise']);

            $row_val['skill_sets'] = $encrptopenssl->decrypt($row_val['skill_sets']);

            $user_arr[] = $row_val;
        }

        foreach ($user_arr as $key => $value) {
            $update_array = array(
                'domain_area_of_expertise_search' => $value['domain_area_of_expertise'],
                'skill_sets_search'               => $value['skill_sets'],
            );
            $updateQuery = $this->master_model->updateRecord('student_profile', $update_array, array('id' => $value['id']));
        }
        echo "Done";
    }

    public function index($value = '')
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');
        $this->db->select('user_category,user_sub_category_id,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type,discoverable,contactable');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $user_id));
        // print_r($user_data);

        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        $profileInfo = $this->master_model->getRecords("student_profile", array('user_id' => $user_id));

        $student_arr = array();
        if (count($profileInfo)) {
            foreach ($profileInfo as $row_val) {
                $row_val['status']                = $encrptopenssl->decrypt($row_val['status']);
                $row_val['university_course']     = json_decode($row_val['university_course']);
                $row_val['university_since_year'] = json_decode($row_val['university_since_year']);
                $row_val['university_to_year']    = json_decode($row_val['university_to_year']);
                $row_val['university_name']       = json_decode($row_val['university_name']);
                $row_val['university_location']   = json_decode($row_val['university_location']);

                $row_val['other_domian']     = json_decode($row_val['other_domian']);
                $row_val['other_area_int']   = json_decode($row_val['other_area_int']);
                $row_val['other_skill_sets'] = json_decode($row_val['other_skill_sets']);

                $row_val['current_study_course']                  = json_decode($row_val['current_study_course']);
                $row_val['current_study_since_year']              = json_decode($row_val['current_study_since_year']);
                $row_val['current_study_to_year']                 = json_decode($row_val['current_study_to_year']);
                $row_val['current_study_description']             = json_decode($row_val['current_study_description']);
                $row_val['domain_and_area_of_training_and_study'] = explode(',', $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']));
                $row_val['areas_of_interest']                     = $encrptopenssl->decrypt($row_val['areas_of_interest']);
                $row_val['skill_sets']                            = $encrptopenssl->decrypt($row_val['skill_sets']);
                $row_val['pincode']                               = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['flat_house_building_apt_company']       = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']            = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']                   = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                               = $encrptopenssl->decrypt($row_val['country']);
                $row_val['student_id_proof']                      = $encrptopenssl->decrypt($row_val['student_id_proof']);
                $student_arr[]                                    = $row_val;
            }
        }

        $data['student_profile'] = $student_arr;

        $skill_sets     = $this->master_model->getRecords("arai_skill_sets", array('status' => 'Active'));
        $skill_sets_arr = array();
        if (count($skill_sets)) {

            foreach ($skill_sets as $row_val) {

                $row_val['name']  = $encrptopenssl->decrypt($row_val['name']);
                $skill_sets_arr[] = $row_val;
            }

        }
        $data['skill_sets'] = $skill_sets_arr;

        if (count($profileInfo)) {
            $domain_info = explode(',', $encrptopenssl->decrypt($profileInfo[0]['domain_area_of_expertise']));
            foreach ($domain_info as $key) {
                $this->db->or_where('id', $key);
            }
            $domain_data = $this->master_model->getRecords("arai_domain_master", array('status' => 'Active'), 'domain_name');
            $domain_arr  = array();
            if (count($domain_data)) {

                foreach ($domain_data as $row_val) {

                    $row_val['domain_name'] = $encrptopenssl->decrypt($row_val['domain_name']);
                    $domain_arr[]           = $row_val;
                }

            }

            $data['domain_info'] = $domain_arr;
        } else {

            $data['domain_info'] = array();
        }

        if (count($profileInfo)) {
            $intrest_info = explode(',', $encrptopenssl->decrypt($profileInfo[0]['areas_of_interest']));
            foreach ($intrest_info as $key) {
                $this->db->or_where('id', $key);
            }
            $intrest_data = $this->master_model->getRecords("area_of_interest", array('status' => 'Active'), 'area_of_interest');
            $intrest_arr  = array();
            if (count($intrest_data)) {

                foreach ($intrest_data as $row_val) {

                    $row_val['area_of_interest'] = $encrptopenssl->decrypt($row_val['area_of_interest']);
                    $intrest_arr[]               = $row_val;
                }

            }

            $data['intrest_info'] = $intrest_arr;
        } else {

            $data['intrest_info'] = array();
        }

        if (count($profileInfo)) {
            $skill_info = explode(',', $encrptopenssl->decrypt($profileInfo[0]['skill_sets']));
            foreach ($skill_info as $key) {
                $this->db->or_where('id', $key);
            }
            $skill_data = $this->master_model->getRecords("skill_sets", '', 'name');
            $skill_arr  = array();
            if (count($skill_data)) {

                foreach ($skill_data as $row_val) {

                    $row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
                    $skill_arr[]     = $row_val;
                }

            }

            $data['skill_info'] = $skill_arr;
        } else {

            $data['skill_info'] = array();
        }

        if (count($profileInfo)) {
            $org_sector_info = explode(',', $profileInfo[0]['org_sector']);

            foreach ($org_sector_info as $key) {
                $this->db->or_where('id', $key);
            }
            $sector_data = $this->master_model->getRecords("organization_sector", '', 'name');

            $sector = array();

            if (count($sector_data)) {

                foreach ($sector_data as $row_val) {

                    $row_val['name'] = $row_val['name'];
                    $sector[]        = $row_val;
                }

            }

            $data['org_sector_info'] = $sector;
        } else {

            $data['org_sector_info'] = array();
        }

        $user_sub_cat = $this->session->userdata('user_sub_category');

        if ($user_sub_cat == 1) {

            $mandatory_arr = array('employement_status', 'designation', 'domain_area_of_expertise', 'skill_sets', 'years_of_experience');

            $optional_arr = array('company_name', 'designation_description', 'corporate_linkage_code', 'profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country');
        }

        if ($user_sub_cat == 2) {

            $mandatory_arr = array('status', 'university_course', 'university_since_year', 'university_to_year', 'university_name', 'university_location', 'domain_area_of_expertise', 'skill_sets', 'student_id_proof');

            $optional_arr = array('profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country', 'event_experience_in_year', 'event_description_of_work', 'technical_experience_in_year', 'technical_experience_to_year', 'technical_description_of_work', 'additional_detail');

        }

        if ($user_sub_cat == 11) {

            $mandatory_arr = array('employement_status', 'designation', 'domain_area_of_expertise', 'skill_sets', 'years_of_experience', 'bio_data', 'no_of_paper_publication', 'no_of_patents');

            $optional_arr = array('company_name', 'designation_description', 'corporate_linkage_code', 'profile_picture', 'pincode', 'flat_house_building_apt_company', 'area_colony_street_village', 'town_city_and_state', 'country', 'portal_name_multiple', 'portal_link_multiple', 'portal_description_multiple');
        }

        $data['profile_complete_per'] = $this->get_completeness_bar_per(40, 40, 20, $mandatory_arr, $optional_arr, $profileInfo);

        $data['profile_complete_per_man'] = $this->get_completeness_bar_per_mandatory(40, 40, 20, $mandatory_arr, $optional_arr, $profileInfo);

        $data['profile_complete_per_opt'] = $this->get_completeness_bar_per_optional(40, 40, 20, $mandatory_arr, $optional_arr, $profileInfo);

        $data['profile_information'] = $this->profile_data($user_id);
        // echo "<pre>";print_r($data['profile_information']);die;
        $data['user']           = $user_arr;
        $data['page_title']     = 'Individual_profile';
        $data['middle_content'] = 'individual_profile/index';
        $this->load->view('front/front_combo', $data);

    }

    public function get_completeness_bar_per($reg_per = 0, $man_per = 0, $opt_per = 0, $mandatory_arr = '', $optional_arr = '', $result_arr = '')
    {
        $man_complete_per = $opt_complete_per = 0;
        if (count($mandatory_arr) > 0 && count($optional_arr) > 0 && count($result_arr) > 0) {
            if (!empty($result_arr)) {
                $total_man_cnt = $complete_man_cnt = 0;
                foreach ($mandatory_arr as $man_res) {
                    if ($result_arr[0][$man_res] != "") {$complete_man_cnt++;}
                    $total_man_cnt++;
                }

                $man_complete_per = round((($complete_man_cnt * $man_per) / $total_man_cnt), 2);

                $total_opt_cnt = $complete_opt_cnt = 0;
                foreach ($optional_arr as $opt_res) {
                    if ($result_arr[0][$opt_res] != "") {$complete_opt_cnt++;}
                    $total_opt_cnt++;
                }

                $opt_complete_per = round((($complete_opt_cnt * $opt_per) / $total_opt_cnt), 2);
            }
        }

        return $profile_complete_per = $reg_per + $man_complete_per + $opt_complete_per;
    }

    public function get_completeness_bar_per_mandatory($reg_per = 0, $man_per = 0, $opt_per = 0, $mandatory_arr = '', $optional_arr = '', $result_arr = '')
    {
        $man_complete_per = $opt_complete_per = 0;
        if (count($mandatory_arr) > 0 && count($optional_arr) > 0 && count($result_arr) > 0) {
            if (!empty($result_arr)) {
                $total_man_cnt = $complete_man_cnt = 0;
                foreach ($mandatory_arr as $man_res) {
                    if ($result_arr[0][$man_res] != "") {$complete_man_cnt++;}
                    $total_man_cnt++;
                }

                $man_complete_per = round((($complete_man_cnt * $man_per) / $total_man_cnt), 2);

                $total_opt_cnt = $complete_opt_cnt = 0;
                foreach ($optional_arr as $opt_res) {
                    if ($result_arr[0][$opt_res] != "") {$complete_opt_cnt++;}
                    $total_opt_cnt++;
                }

                $opt_complete_per = round((($complete_opt_cnt * $opt_per) / $total_opt_cnt), 2);
            }
        }

        return $man_complete_per;
    }

    public function get_completeness_bar_per_optional($reg_per = 0, $man_per = 0, $opt_per = 0, $mandatory_arr = '', $optional_arr = '', $result_arr = '')
    {
        $man_complete_per = $opt_complete_per = 0;
        if (count($mandatory_arr) > 0 && count($optional_arr) > 0 && count($result_arr) > 0) {
            if (!empty($result_arr)) {
                $total_man_cnt = $complete_man_cnt = 0;
                foreach ($mandatory_arr as $man_res) {
                    if ($result_arr[0][$man_res] != "") {$complete_man_cnt++;}
                    $total_man_cnt++;
                }

                $man_complete_per = round((($complete_man_cnt * $man_per) / $total_man_cnt), 2);

                $total_opt_cnt = $complete_opt_cnt = 0;
                foreach ($optional_arr as $opt_res) {
                    if ($result_arr[0][$opt_res] != "") {$complete_opt_cnt++;}
                    $total_opt_cnt++;
                }

                $opt_complete_per = round((($complete_opt_cnt * $opt_per) / $total_opt_cnt), 2);
            }
        }

        return $opt_complete_per;
    }

    public function edit($value = '')
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');
        $this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $user_id));
        // print_r($user_data);
        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['country_code']    = $encrptopenssl->decrypt($row_val['country_code']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        $data['country_codes'] = $this->master_model->getRecords('country', '', '', array('iso' => 'asc'));
        $data['user']          = $user_arr;

        $data['page_title']     = 'Individual_profile';
        $data['middle_content'] = 'individual_profile/edit_personal';
        $this->load->view('front/front_combo', $data);

    }

    public function profile_data($userId)
    {
        $encrptopenssl    = new Opensslencryptdecrypt();
        $profileInfo      = $this->master_model->getRecords("student_profile", array('user_id' => $userId));
        $profile_info_arr = array();
        if (count($profileInfo)) {
            foreach ($profileInfo as $row_val) {
                $row_val['status']             = $encrptopenssl->decrypt($row_val['status']);
                $row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);
                $row_val['other_emp_status']   = $encrptopenssl->decrypt($row_val['other_emp_status']);

                $row_val['org_sector']       = $row_val['org_sector'];
                $row_val['org_sector_other'] = $row_val['org_sector_other'];

                $row_val['other_domian']     = $encrptopenssl->decrypt($row_val['other_domian']);
                $row_val['other_skill_sets'] = $encrptopenssl->decrypt($row_val['other_skill_sets']);
                $row_val['other_area_int']   = $encrptopenssl->decrypt($row_val['other_area_int']);

                $row_val['university_course']         = $encrptopenssl->decrypt($row_val['university_course']);
                $row_val['university_since_year']     = $encrptopenssl->decrypt($row_val['university_since_year']);
                $row_val['university_to_year']        = $encrptopenssl->decrypt($row_val['university_to_year']);
                $row_val['university_name']           = $encrptopenssl->decrypt($row_val['university_name']);
                $row_val['company_name']              = $encrptopenssl->decrypt($row_val['company_name']);
                $row_val['university_location']       = $encrptopenssl->decrypt($row_val['university_location']);
                $row_val['current_study_course']      = $encrptopenssl->decrypt($row_val['current_study_course']);
                $row_val['current_study_since_year']  = $encrptopenssl->decrypt($row_val['current_study_since_year']);
                $row_val['current_study_to_year']     = $encrptopenssl->decrypt($row_val['current_study_to_year']);
                $row_val['current_study_description'] = $encrptopenssl->decrypt($row_val['current_study_description']);

                $row_val['designation']             = json_decode($row_val['designation']);
                $row_val['designation_since_year']  = json_decode($row_val['designation_since_year']);
                $row_val['designation_description'] = json_decode($row_val['designation_description']);

                $row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
                $row_val['domain_area_of_expertise']              = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));
                $row_val['areas_of_interest']                     = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
                $row_val['corporate_linkage_code']                = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
                $row_val['skill_sets']                            = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));

                $row_val['event_experience_in_year']  = json_decode($row_val['event_experience_in_year']);
                $row_val['event_description_of_work'] = json_decode($row_val['event_description_of_work']);

                $row_val['technical_experience_in_year'] = json_decode($row_val['technical_experience_in_year']);
                $row_val['technical_experience_to_year'] = json_decode($row_val['technical_experience_to_year']);

                $row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);

                $row_val['pincode']                         = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']      = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']             = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                         = $encrptopenssl->decrypt($row_val['country']);
                $row_val['student_id_proof']                = $encrptopenssl->decrypt($row_val['student_id_proof']);

                $row_val['specify_fields_area_that_you_would_like'] = json_decode($row_val['specify_fields_area_that_you_would_like']);

                $row_val['bio_data']            = $encrptopenssl->decrypt($row_val['bio_data']);
                $row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);

                $row_val['no_of_paper_publication'] = $row_val['no_of_paper_publication'];
                $row_val['paper_year']              = json_decode($row_val['paper_year']);
                $row_val['paper_conf_name']         = json_decode($row_val['paper_conf_name']);
                $row_val['paper_title']             = json_decode($row_val['paper_title']);

                $row_val['no_of_patents'] = $row_val['no_of_patents'];
                $row_val['patent_year']   = json_decode($row_val['patent_year']);
                $row_val['patent_number'] = json_decode($row_val['patent_number']);
                $row_val['patent_title']  = json_decode($row_val['patent_title']);

                $row_val['portal_name_multiple']        = json_decode($row_val['portal_name_multiple']);
                $row_val['portal_link_multiple']        = json_decode($row_val['portal_link_multiple']);
                $row_val['portal_description_multiple'] = json_decode($row_val['portal_description_multiple']);

                $row_val['portal_name']        = $encrptopenssl->decrypt($row_val['portal_name']);
                $row_val['portal_link']        = $encrptopenssl->decrypt($row_val['portal_link']);
                $row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);

                if ($row_val['portal_name_multiple'] == '' && $row_val['portal_name'] != '') {
                    $row_val['portal_name_multiple'] = array();
                    array_unshift($row_val['portal_name_multiple'], $row_val['portal_name']);
                }

                if ($row_val['portal_link_multiple'] == '' && $row_val['portal_link'] != '') {
                    $row_val['portal_link_multiple'] = array();
                    array_unshift($row_val['portal_link_multiple'], $row_val['portal_link']);
                }

                if ($row_val['portal_description_multiple'] == '' && $row_val['portal_description'] != '') {
                    $row_val['portal_description_multiple'] = array();
                    array_unshift($row_val['portal_description_multiple'], $row_val['portal_description']);

                }

                $row_val['profile_picture'] = $row_val['profile_picture'];
                $profile_info_arr[]         = $row_val;
            }

        }

        return $profile_info_arr;

    }
    public function individual($value = '')
    {
        $userId        = $this->session->userdata('user_id');
        $encrptopenssl = new Opensslencryptdecrypt();

        $this->db->select('user_category,user_sub_category_id,sub_catname,title,first_name,middle_name,last_name,email,mobile,membership_type');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $userId));

        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        $this->load->library('upload');

        $data['employement_status'] = $emp_status_data = $this->master_model->array_sorting($this->master_model->getRecords('employement_master', array('status' => "Active")), array('id'), 'name', 1);

        $data['domain'] = $domain_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_domain_master', array('status' => "Active")), array('id'), 'domain_name');

        $data['area_of_interest'] = $area_of_interest_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_area_of_interest', array('status' => "Active")), array('id'), 'area_of_interest');

        $data['skill_sets'] = $skill_sets_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_skill_sets', array('status' => "Active")), array('id'), 'name');

        $profileInfo      = $this->master_model->getRecords("student_profile", array('user_id' => $userId));
        $profile_info_arr = array();
        if (count($profileInfo)) {
            foreach ($profileInfo as $row_val) {
                $row_val['status']             = $encrptopenssl->decrypt($row_val['status']);
                $row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);

                $row_val['other_emp_status'] = $encrptopenssl->decrypt($row_val['other_emp_status']);

                $row_val['org_sector']       = $row_val['org_sector'];
                $row_val['org_sector_other'] = $row_val['org_sector_other'];

                $row_val['other_domian']     = $encrptopenssl->decrypt($row_val['other_domian']);
                $row_val['other_skill_sets'] = $encrptopenssl->decrypt($row_val['other_skill_sets']);

                $row_val['university_course']         = $encrptopenssl->decrypt($row_val['university_course']);
                $row_val['university_since_year']     = $encrptopenssl->decrypt($row_val['university_since_year']);
                $row_val['university_to_year']        = $encrptopenssl->decrypt($row_val['university_to_year']);
                $row_val['university_name']           = $encrptopenssl->decrypt($row_val['university_name']);
                $row_val['company_name']              = $encrptopenssl->decrypt($row_val['company_name']);
                $row_val['university_location']       = $encrptopenssl->decrypt($row_val['university_location']);
                $row_val['current_study_course']      = $encrptopenssl->decrypt($row_val['current_study_course']);
                $row_val['current_study_since_year']  = $encrptopenssl->decrypt($row_val['current_study_since_year']);
                $row_val['current_study_to_year']     = $encrptopenssl->decrypt($row_val['current_study_to_year']);
                $row_val['current_study_description'] = $encrptopenssl->decrypt($row_val['current_study_description']);

                $row_val['designation']             = json_decode($row_val['designation']);
                $row_val['designation_since_year']  = json_decode($row_val['designation_since_year']);
                $row_val['designation_description'] = json_decode($row_val['designation_description']);

                $row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
                $row_val['domain_area_of_expertise']              = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));
                $row_val['areas_of_interest']                     = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
                $row_val['corporate_linkage_code']                = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
                $row_val['skill_sets']                            = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));
                $row_val['event_experience_in_year']              = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
                $row_val['event_description_of_work']             = $encrptopenssl->decrypt($row_val['event_description_of_work']);

                $row_val['technical_experience_in_year']  = json_decode($row_val['technical_experience_in_year']);
                $row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);

                $row_val['pincode']                         = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']      = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']             = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                         = $encrptopenssl->decrypt($row_val['country']);
                $row_val['student_id_proof']                = $encrptopenssl->decrypt($row_val['student_id_proof']);

                $row_val['specify_fields_area_that_you_would_like'] = json_decode($row_val['specify_fields_area_that_you_would_like']);

                $row_val['bio_data']            = $encrptopenssl->decrypt($row_val['bio_data']);
                $row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);

                $row_val['no_of_paper_publication'] = $row_val['no_of_paper_publication'];
                $row_val['paper_year']              = json_decode($row_val['paper_year']);
                $row_val['paper_conf_name']         = json_decode($row_val['paper_conf_name']);
                $row_val['paper_title']             = json_decode($row_val['paper_title']);

                $row_val['no_of_patents'] = $row_val['no_of_patents'];

                $row_val['patent_year']   = json_decode($row_val['patent_year']);
                $row_val['patent_number'] = json_decode($row_val['patent_number']);
                $row_val['patent_title']  = json_decode($row_val['patent_title']);

                $row_val['portal_name_multiple']        = json_decode($row_val['portal_name_multiple']);
                $row_val['portal_link_multiple']        = json_decode($row_val['portal_link_multiple']);
                $row_val['portal_description_multiple'] = json_decode($row_val['portal_description_multiple']);

                $row_val['portal_name']        = $encrptopenssl->decrypt($row_val['portal_name']);
                $row_val['portal_link']        = $encrptopenssl->decrypt($row_val['portal_link']);
                $row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);

                if ($row_val['portal_name_multiple'] == '' && $row_val['portal_name'] != '') {
                    $row_val['portal_name_multiple'] = array();
                    array_unshift($row_val['portal_name_multiple'], $row_val['portal_name']);
                }

                if ($row_val['portal_link_multiple'] == '' && $row_val['portal_link'] != '') {
                    $row_val['portal_link_multiple'] = array();
                    array_unshift($row_val['portal_link_multiple'], $row_val['portal_link']);
                }

                if ($row_val['portal_description_multiple'] == '' && $row_val['portal_description'] != '') {
                    $row_val['portal_description_multiple'] = array();
                    array_unshift($row_val['portal_description_multiple'], $row_val['portal_description']);

                }

                $row_val['profile_picture'] = $row_val['profile_picture'];
                $profile_info_arr[]         = $row_val;
            }

        }

        $data['profile_information'] = $profile_info_arr;
        // echo "<pre>";
        // print_r($data['profile_information']);die;

        // if (isset($_POST['btn_submit'])) {
        // Check Validation
        $this->form_validation->set_rules('employement_status', 'Employement Status', 'required');
        if ($this->input->post('employement_status') == 'Company') {
            $this->form_validation->set_rules('company_name', 'Company Name', 'required');
        }

        $this->form_validation->set_rules('designation[]', 'Designation', 'required');
        // $this->form_validation->set_rules('designation_since_year[]', 'Designation Since Year', 'required');
        // $this->form_validation->set_rules('designation_description[]', 'Description', 'required');
        //$this->form_validation->set_rules('corporate_linkage_code', 'Corporate Linkage Code', 'required');

        // $this->form_validation->set_rules('technical_experience_in_year[]', 'Technical Experience', 'required');
        // $this->form_validation->set_rules('technical_description_of_work[]', 'Description of Work', 'required');

        // $this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
        // $this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
        // $this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
        // $this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
        // $this->form_validation->set_rules('country', 'Country', 'required');

        if ($this->session->userdata('user_sub_category') == 11) {
            // $this->form_validation->set_rules('specify_fields_area_that_you_would_like[]', 'specify fields area that you would like to be an expert', 'required');
            $this->form_validation->set_rules('bio_data', 'Bio Data', 'required');
            $this->form_validation->set_rules('years_of_experience', 'Years of Experience', 'required');
            $this->form_validation->set_rules('no_of_paper_publication', 'No of Paper Publication', 'required');
            // $this->form_validation->set_rules('paper_year', 'Paper Year', 'required');
            // $this->form_validation->set_rules('paper_conf_name', 'Paper Conf. Name', 'required');
            // $this->form_validation->set_rules('paper_title', 'Paper Title', 'required');
            $this->form_validation->set_rules('no_of_patents', 'No of Patents', 'required');
            // $this->form_validation->set_rules('patent_year', 'Patent Year', 'required');
            // $this->form_validation->set_rules('patent_number', 'Patent Number', 'required');
            // $this->form_validation->set_rules('patent_title', 'Patent Title', 'required');
            // $this->form_validation->set_rules('portal_name', 'Portal Name', 'required');
            // $this->form_validation->set_rules('portal_link', 'Portal Link', 'required');
            // $this->form_validation->set_rules('portal_description', 'Portal Description', 'required');
        }

        if ($this->form_validation->run()) {

            $type               = $this->input->post('type');
            $employement_status = $encrptopenssl->encrypt($this->input->post('employement_status'));
            $company_name       = $encrptopenssl->encrypt($this->input->post('company_name'));

            $designation            = json_encode($this->input->post('designation[]'));
            $designation_since_year = json_encode($this->input->post('designation_since_year[]'));

            $designation_description = '';
            if (array_filter($this->input->post('designation_description[]'))) {
                $designation_description = json_encode($this->input->post('designation_description[]'));

            }

            $technical_experience_in_year     = '';
            $technical_description_of_work    = '';
            $technical_experience_in_year_arr = $this->input->post('technical_experience_in_year[]');

            $technical_experience_in_year_arr = '';
            if (count($technical_experience_in_year_arr) > 0) {
                $technical_experience_in_year  = json_encode($technical_experience_in_year_arr);
                $technical_description_of_work = json_encode($this->input->post('technical_description_of_work[]'));
            }

            $corporate_linkage_code = $encrptopenssl->encrypt($this->input->post('corporate_linkage_code'));

            $pincode                         = $encrptopenssl->encrypt($this->input->post('pincode'));
            $flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
            $area_colony_street_village      = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
            $town_city_and_state             = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
            $country                         = $encrptopenssl->encrypt($this->input->post('country'));
            $years_of_experience             = $encrptopenssl->encrypt($this->input->post('years_of_experience'));
            $years_of_experience_search      = $this->input->post('years_of_experience');

            $org_sector       = implode(",", $this->input->post('org_sector', true));
            $org_sector_other = $this->input->post('org_sector_other');

            if ($this->session->userdata('user_sub_category') == 11) {

                $specify_fields_area_that_you_would_like = json_encode($this->input->post('specify_fields_area_that_you_would_like'));
                $bio_data                                = $encrptopenssl->encrypt($this->input->post('bio_data'));

                $no_of_paper_publication = $this->input->post('no_of_paper_publication');

                $paper_year = '';
                $py         = $this->input->post('paper_year[]');
                if (isset($py) && array_filter($py)) {
                    $paper_year = json_encode($this->input->post('paper_year[]'));

                }

                $pcn             = $this->input->post('paper_conf_name[]');
                $paper_conf_name = '';
                if (isset($pcn) && array_filter($pcn)) {
                    $paper_conf_name = json_encode($this->input->post('paper_conf_name[]'));

                }

                $paper_title = '';
                $pt          = $this->input->post('paper_title[]');

                if (isset($pt) && array_filter($pt)) {
                    $paper_title = json_encode($this->input->post('paper_title[]'));

                }

                $no_of_patents = $this->input->post('no_of_patents');

                $patent_year = '';
                $pat_y       = $this->input->post('patent_year[]');
                if (isset($pat_y) && array_filter($pat_y)) {
                    $patent_year = json_encode($this->input->post('patent_year'));

                }

                $patent_number = '';
                $pat_n         = $this->input->post('patent_number[]');
                if (isset($pat_n) && array_filter($pat_n)) {
                    $patent_number = json_encode($this->input->post('patent_number'));

                }

                $patent_title = '';
                $pat_t        = $this->input->post('patent_title[]');
                if (isset($pat_t) && array_filter($pat_t)) {
                    $patent_title = json_encode($this->input->post('patent_title'));

                }

                // $portal_name = $encrptopenssl->encrypt($this->input->post('portal_name'));
                // $portal_link = $encrptopenssl->encrypt($this->input->post('portal_link'));
                // $portal_description = $encrptopenssl->encrypt($this->input->post('portal_description'));

                $portal_name_multiple = '';
                if (array_filter($this->input->post('portal_name[]'))) {
                    $portal_name_multiple = json_encode($this->input->post('portal_name'));

                }

                $portal_link_multiple = '';
                if (array_filter($this->input->post('portal_link[]'))) {
                    $portal_link_multiple = json_encode($this->input->post('portal_link'));

                }

                $portal_description_multiple = '';
                if (array_filter($this->input->post('portal_description[]'))) {
                    $portal_description_multiple = json_encode($this->input->post('portal_description'));

                }

            }

            $other_emp_status_plain = $this->input->post('other_emp_status');

            $other_emp_status = '';
            if ($other_emp_status_plain != '') {
                $other_emp_status = $encrptopenssl->encrypt($other_emp_status_plain);
            }

            $other_domian       = '';
            $other_domian_plain = $this->input->post('other_domian');
            if ($other_domian_plain != '') {
                $other_domian = $encrptopenssl->encrypt($other_domian_plain);
            }

            $other_skill_sets_plain = $this->input->post('other_skill_sets');

            $other_skill_sets = '';
            if ($other_skill_sets_plain != '') {
                $other_skill_sets = $encrptopenssl->encrypt($other_skill_sets_plain);
            }

            $domain = $this->input->post('domain_and_area_of_expertise');

            $domain_search = implode(',', $domain);

            $domainAreaOfExpertise = $encrptopenssl->encrypt(implode(',', $domain));

            $AreaOfInterest = $this->input->post('areas_of_interest');
            $AreaOfInterest = '0';
            // $AreaOfInterest = $encrptopenssl->encrypt(implode(',', $areaofInterest));

            $skillSets        = $this->input->post('skill_sets');
            $skillSets_search = implode(',', $skillSets);
            $SkillSets        = $encrptopenssl->encrypt(implode(',', $skillSets));

            // if($_FILES['profile_picture']['name']!=""){

            //     $config['upload_path']      = 'assets/profile_picture';
            //     $config['allowed_types']    = '*';
            //     $config['max_size']         = '5000';
            //     $config['encrypt_name']     = TRUE;

            //     $upload_files  = @$this->master_model->upload_file('profile_picture', $_FILES, $config, FALSE);
            //     $b_image = "";
            //     if(isset($upload_files[0]) && !empty($upload_files[0])){

            //         $b_image = $upload_files[0];

            //     }

            //     $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");

            //     $ext = pathinfo($b_image[0], PATHINFO_EXTENSION);

            //     if(!array_key_exists($ext, $allowed)){

            //         $data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
            //         $this->session->set_flashdata('error',$data['imageError']);
            //         redirect(base_url('profile/individual'));
            //     }

            //     $filesize = $_FILES['profile_picture']['size'];
            //     // Verify file size - 5MB maximum
            //     $maxsize = 5 * 1024 * 1024;
            //     $file_names = $b_image[0];
            // } // Student ID Proof Image End
            // else {
            //     $file_names = $data['profile_information'][0]['profile_picture'];
            // }
            if ($this->session->userdata('user_sub_category') == 11) {
                $insertArr = array(
                    'type'                                    => $type,
                    'employement_status'                      => $employement_status,
                    'company_name'                            => $company_name,
                    'designation'                             => $designation,
                    'designation_since_year'                  => $designation_since_year,
                    'designation_description'                 => $designation_description,
                    'corporate_linkage_code'                  => $corporate_linkage_code,
                    'technical_experience_in_year'            => $technical_experience_in_year,
                    'technical_description_of_work'           => $technical_description_of_work,
                    'pincode'                                 => $pincode,
                    'flat_house_building_apt_company'         => $flat_house_building_apt_company,
                    'area_colony_street_village'              => $area_colony_street_village,
                    'town_city_and_state'                     => $town_city_and_state, 'country' => $country,

                    'specify_fields_area_that_you_would_like' => $specify_fields_area_that_you_would_like,
                    'bio_data'                                => $bio_data,
                    'years_of_experience'                     => $years_of_experience,
                    'years_of_experience_search'              => $years_of_experience_search,
                    'no_of_paper_publication'                 => $no_of_paper_publication,
                    'paper_year'                              => $paper_year,
                    'paper_conf_name'                         => $paper_conf_name,
                    'paper_title'                             => $paper_title,
                    'no_of_patents'                           => $no_of_patents,
                    'patent_year'                             => $patent_year,
                    'patent_number'                           => $patent_number,
                    'patent_title'                            => $patent_title,

                    // 'portal_name' => $portal_name,
                    // 'portal_link' => $portal_link,
                    // 'portal_description' => $portal_description,

                    'portal_name_multiple'                    => $portal_name_multiple,
                    'portal_link_multiple'                    => $portal_link_multiple,
                    'portal_description_multiple'             => $portal_description_multiple,

                    // 'profile_picture' => $file_names,

                    'domain_area_of_expertise'                => $domainAreaOfExpertise,
                    'areas_of_interest'                       => $AreaOfInterest,
                    'skill_sets'                              => $SkillSets,
                    'user_id'                                 => $userId,
                    'profile_completion_status'               => 'complete',

                    'domain_area_of_expertise_search'         => $domain_search,
                    'skill_sets_search'                       => $skillSets_search,

                    'updatedAt'                               => date('Y-m-d H:i:s'),
                );
            } else {
                $insertArr = array(
                    'type'                            => $type,
                    'employement_status'              => $employement_status,
                    'company_name'                    => $company_name,
                    'years_of_experience'             => $years_of_experience,
                    'years_of_experience_search'      => $years_of_experience_search,
                    'designation'                     => $designation,
                    'designation_since_year'          => $designation_since_year,
                    'designation_description'         => $designation_description,
                    'corporate_linkage_code'          => $corporate_linkage_code,
                    'technical_experience_in_year'    => $technical_experience_in_year,
                    'technical_description_of_work'   => $technical_description_of_work,
                    'pincode'                         => $pincode,
                    'flat_house_building_apt_company' => $flat_house_building_apt_company,
                    'area_colony_street_village'      => $area_colony_street_village,
                    'town_city_and_state'             => $town_city_and_state, 'country' => $country,
                    // 'profile_picture' => $file_names,
                    'domain_area_of_expertise'        => $domainAreaOfExpertise,
                    'areas_of_interest'               => $AreaOfInterest,
                    'skill_sets'                      => $SkillSets,
                    'user_id'                         => $userId,
                    'profile_completion_status'       => 'complete',

                    'domain_area_of_expertise_search' => $domain_search,
                    'skill_sets_search'               => $skillSets_search,

                    'updatedAt'                       => date('Y-m-d H:i:s'),
                );
            }

            $insertArr['other_emp_status'] = $other_emp_status;
            $insertArr['other_domian']     = $other_domian;
            $insertArr['other_skill_sets'] = $other_skill_sets;

            $insertArr['org_sector']       = $org_sector;
            $insertArr['org_sector_other'] = $org_sector_other;

            // echo "<pre>";
            // print_r($insertArr);die;

            $file_upload_flag = 0;
            if ($_FILES['profile_picture']['name'] != "") {
                $profile_res = $this->Common_model_sm->upload_single_file("profile_picture", array('png', 'jpg', 'jpeg'), "profile_picture_" . date("YmdHis"), "./assets/profile_picture", "png|jpeg|jpg");
                if ($profile_res['response'] == 'error') {
                    $data['profile_picture_error'] = $profile_res['message'];
                    $file_upload_flag              = 1;
                } else if ($profile_res['response'] == 'success') {
                    $file_names                   = $profile_res['message'];
                    $insertArr['profile_picture'] = $file_names;
                    //@unlink("./uploads/organization_profile/".$organization_data[0]['pan_card']);
                }
            }

            $id          = $this->input->post('profileId');
            $insertQuery = '';
            if (!empty($id)) {
                $updateQuery = $this->master_model->updateRecord('student_profile', $insertArr, array('id' => $id));
            } else {
                $insertQuery = $this->master_model->insertRecord('student_profile', $insertArr);
            }

            $promo_code_plain = $this->input->post('promo_code');

            if ($promo_code_plain != '') {

                $whre = array(
                    'promo_code' => $promo_code,
                    'code_type'  => 'Corporate',
                );
                $promo_code_info = $this->master_model->getRecords('promo_codes', $whre);

                if (count($promo_code_info) > 0) {

                    $promo_code_type  = $this->input->post('promo_code_type');
                    $subscription_fee = '00';
                    $promo_code       = $encrptopenssl->encrypt($this->input->post('promo_code'));

                    $orgnization_id = $this->input->post('org_id');
                    $promo_code_id  = $this->input->post('promo_code_id');

                    $insertArr = array(
                        'user_id'          => $userId,
                        'have_promo_code'  => $have_linkage_code,
                        'promo_code_type'  => $promo_code_type,
                        'promo_code'       => $promo_code,
                        'subscription_fee' => $subscription_fee,
                        'created_at'       => date('Y-m-d H:i:s'),

                    );

                    $insert_id_sub = $this->master_model->insertRecord('subscriptions', $insertArr);

                    $updateArr = array(
                        'payment_status' => 'complete',
                    );

                    $updateQuery = $this->master_model->updateRecord('registration', $updateArr, array('user_id' => $userId));

                    $insertArr_2 = array(
                        'user_id'        => $userId,
                        'orgnization_id' => $orgnization_id,
                        'promo_code_id'  => $promo_code_id,

                    );
                    $insert_id_mapping = $this->master_model->insertRecord('user_org_mapping', $insertArr_2);

                }

            }

            if ($insertQuery > 0) {

                if ($other_domian_plain) {

                    $newDomian = explode(",", $other_domian_plain);
                    foreach ($newDomian as $createdomain) {
                        $other_domain = $encrptopenssl->encrypt($createdomain);
                        $DataInsert   = array('domain_name' => $other_domain, 'profile_id' => $insertQuery, 'status' => 'Block');
                        $insertQuery  = $this->master_model->insertRecord('domain_master', $DataInsert);

                    }

                }

                if ($other_skill_sets_plain) {

                    $newTagExp = explode(",", $other_skill_sets_plain);
                    foreach ($newTagExp as $createTag) {
                        $tagOtherContent = $encrptopenssl->encrypt($createTag);
                        $tagDataInsert   = array('name' => $tagOtherContent, 'profile_id' => $insertQuery, 'status' => 'Block');
                        $insertTagsQuery = $this->master_model->insertRecord('skill_sets', $tagDataInsert);

                    }

                }

                $this->session->set_flashdata('success_profile', 'Profile Updated Successfully.');
                redirect(base_url('profile'));
            } else if ($updateQuery > 0) {

                if ($other_domian_plain) {

                    $newDomian = explode(",", $other_domian_plain);
                    foreach ($newDomian as $createdomain) {
                        $other_domain = $encrptopenssl->encrypt($createdomain);
                        $DataInsert   = array('domain_name' => $other_domain, 'profile_id' => $updateQuery, 'status' => 'Block');
                        $insertQuery  = $this->master_model->insertRecord('domain_master', $DataInsert);

                    }

                }

                if ($other_skill_sets_plain) {

                    $newTagExp = explode(",", $other_skill_sets_plain);
                    foreach ($newTagExp as $createTag) {
                        $tagOtherContent = $encrptopenssl->encrypt($createTag);
                        $tagDataInsert   = array('name' => $tagOtherContent, 'profile_id' => $updateQuery, 'status' => 'Block');
                        $insertTagsQuery = $this->master_model->insertRecord('skill_sets', $tagDataInsert);

                    }

                }
                $this->session->set_flashdata('success_profile', 'Profile Updated Successfully.');
                redirect(base_url('profile'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('profile/individual'));
            }
        } else {
            echo validation_errors();
        }

        $data['org_sector_data'] = $sector_array = $this->master_model->array_sorting($this->master_model->getRecords('arai_organization_sector', array('status' => "Active")), array('id'), 'name', 1);

        $data['page_title']     = 'Individual_profile';
        $data['user']           = $user_arr;
        $data['middle_content'] = 'individual_profile/add';
        $this->load->view('front/front_combo', $data);
    }

    public function student($value = '')
    {
        $userId        = $this->session->userdata('user_id');
        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');

        $this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');

        $user_data = $this->master_model->getRecords("registration", array('user_id' => $userId));
        // print_r($user_data);

        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        $data['status'] = $status_arr = $this->master_model->array_sorting($this->master_model->getRecords('student_master', array('status' => "Active")), array('id'), 'degree_name');

        $data['domain'] = $domain_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_domain_master', array('status' => "Active")), array('id'), 'domain_name');

        $data['area_of_interest'] = $area_of_interest_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_area_of_interest', array('status' => "Active")), array('id'), 'area_of_interest');

        $data['skill_sets'] = $skill_sets_arr = $this->master_model->array_sorting($this->master_model->getRecords('arai_skill_sets', array('status' => "Active")), array('id'), 'name');

        $profileInfo      = $this->master_model->getRecords("student_profile", array('user_id' => $userId));
        $profile_info_arr = array();
        if (count($profileInfo)) {
            foreach ($profileInfo as $row_val) {
                $row_val['status']             = $encrptopenssl->decrypt($row_val['status']);
                $row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);

                $row_val['other_domian']     = $encrptopenssl->decrypt($row_val['other_domian']);
                $row_val['other_area_int']   = $encrptopenssl->decrypt($row_val['other_area_int']);
                $row_val['other_skill_sets'] = $encrptopenssl->decrypt($row_val['other_skill_sets']);

                $row_val['university_course']         = json_decode($row_val['university_course']);
                $row_val['university_since_year']     = json_decode($row_val['university_since_year']);
                $row_val['university_to_year']        = json_decode($row_val['university_to_year']);
                $row_val['university_name']           = json_decode($row_val['university_name']);
                $row_val['university_location']       = json_decode($row_val['university_location']);
                $row_val['company_name']              = $encrptopenssl->decrypt($row_val['company_name']);
                $row_val['current_study_course']      = json_decode($row_val['current_study_course']);
                $row_val['current_study_since_year']  = json_decode($row_val['current_study_since_year']);
                $row_val['current_study_to_year']     = json_decode($row_val['current_study_to_year']);
                $row_val['current_study_description'] = json_decode($row_val['current_study_description']);
                $row_val['designation']               = $encrptopenssl->decrypt($row_val['designation']);
                $row_val['designation_since_year']    = $encrptopenssl->decrypt($row_val['designation_since_year']);
                $row_val['designation_description']   = $encrptopenssl->decrypt($row_val['designation_description']);

                $row_val['domain_area_of_expertise'] = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));

                $row_val['domain_and_area_of_training_and_study'] = explode(',', $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']));

                $row_val['areas_of_interest']      = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
                $row_val['corporate_linkage_code'] = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
                $row_val['skill_sets']             = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));

                $row_val['event_experience_in_year']     = json_decode($row_val['event_experience_in_year']);
                $row_val['technical_experience_to_year'] = json_decode($row_val['technical_experience_to_year']);
                $row_val['event_description_of_work']    = json_decode($row_val['event_description_of_work']);

                $row_val['technical_experience_in_year']  = json_decode($row_val['technical_experience_in_year']);
                $row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);

                $row_val['pincode']                                 = $encrptopenssl->decrypt($row_val['pincode']);
                $row_val['flat_house_building_apt_company']         = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
                $row_val['area_colony_street_village']              = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
                $row_val['town_city_and_state']                     = $encrptopenssl->decrypt($row_val['town_city_and_state']);
                $row_val['country']                                 = $encrptopenssl->decrypt($row_val['country']);
                $row_val['student_id_proof']                        = $encrptopenssl->decrypt($row_val['student_id_proof']);
                $row_val['specify_fields_area_that_you_would_like'] = $encrptopenssl->decrypt($row_val['specify_fields_area_that_you_would_like']);
                $row_val['bio_data']                                = $encrptopenssl->decrypt($row_val['bio_data']);
                $row_val['years_of_experience']                     = $encrptopenssl->decrypt($row_val['years_of_experience']);
                $row_val['no_of_paper_publication']                 = $encrptopenssl->decrypt($row_val['no_of_paper_publication']);
                $row_val['paper_year']                              = $encrptopenssl->decrypt($row_val['paper_year']);
                $row_val['paper_conf_name']                         = $encrptopenssl->decrypt($row_val['paper_conf_name']);
                $row_val['paper_title']                             = $encrptopenssl->decrypt($row_val['paper_title']);
                $row_val['no_of_patents']                           = $encrptopenssl->decrypt($row_val['no_of_patents']);
                $row_val['patent_year']                             = $encrptopenssl->decrypt($row_val['patent_year']);
                $row_val['patent_number']                           = $encrptopenssl->decrypt($row_val['patent_number']);
                $row_val['patent_title']                            = $encrptopenssl->decrypt($row_val['patent_title']);
                $row_val['portal_name']                             = $encrptopenssl->decrypt($row_val['portal_name']);
                $row_val['portal_link']                             = $encrptopenssl->decrypt($row_val['portal_link']);
                $row_val['portal_description']                      = $encrptopenssl->decrypt($row_val['portal_description']);
                $row_val['profile_picture']                         = $row_val['profile_picture'];
                $profile_info_arr[]                                 = $row_val;
            }

        }

        $data['profile_information'] = $profile_info_arr;

        // echo "<pre>";
        // print_r($data);die;

        if (isset($_POST) && count($_POST) > 0) {
            // echo "<pre>";
            // print_r($this->input->post());die;
            //Check Validation
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('university_course[]', 'Course', 'required');
            $this->form_validation->set_rules('university_since_year[]', 'Since Year', 'required');
            $this->form_validation->set_rules('university_to_year[]', 'To Year', 'required');
            $this->form_validation->set_rules('university_name[]', 'Name', 'required');
            $this->form_validation->set_rules('university_location[]', 'Location', 'required');
            // $this->form_validation->set_rules('current_study_course[]', 'Study Course', 'required');
            // $this->form_validation->set_rules('current_study_since_year[]', 'Location', 'required');
            // $this->form_validation->set_rules('current_study_to_year[]', 'Location', 'required');
            // $this->form_validation->set_rules('current_study_description[]', 'Location', 'required');

            // $this->form_validation->set_rules('event_experience_in_year', 'Event Experience', 'required');
            // $this->form_validation->set_rules('event_description_of_work', 'Description of Work', 'required');
            // $this->form_validation->set_rules('technical_experience_in_year', 'Technical Experience', 'required');
            // $this->form_validation->set_rules('technical_description_of_work', 'Description of Work', 'required');
            // $this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
            // $this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
            // $this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
            // $this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
            // $this->form_validation->set_rules('country', 'Country', 'required');

            if ($this->form_validation->run()) {
                // echo "<pre>";
                // print_r($this->input->post());die;
                $type   = $this->input->post('type');
                $status = $encrptopenssl->encrypt($this->input->post('status'));

                $university_course         = $encrptopenssl->encrypt($this->input->post('university_course'));
                $university_course         = json_encode($this->input->post('university_course[]'));
                $university_since_year     = json_encode($this->input->post('university_since_year[]'));
                $university_to_year        = json_encode($this->input->post('university_to_year[]'));
                $university_name           = json_encode($this->input->post('university_name[]'));
                $university_location       = json_encode($this->input->post('university_location[]'));
                $current_study_course      = json_encode($this->input->post('current_study_course'));
                $current_study_since_year  = json_encode($this->input->post('current_study_since_year'));
                $current_study_to_year     = json_encode($this->input->post('current_study_to_year'));
                $current_study_description = json_encode($this->input->post('current_study_description'));
                $domain                    = $interestArea                    = $skillSets                    = [];

                $domain       = $this->input->post('domain_and_area_of_expertise');
                $domainString = $encrptopenssl->encrypt(implode(',', $domain));

                $additional_detail = $this->input->post('additional_detail');

                // $interestArea = $this->input->post('areas_of_interest');
                // $AreaOfInterest = $encrptopenssl->encrypt(implode(',', $interestArea));

                $skillSets = $this->input->post('skill_sets');
                $SkillSets = $encrptopenssl->encrypt(implode(',', $skillSets));

                $domain_search    = implode(',', $domain);
                $skillSets_search = implode(',', $skillSets);

                // $event_experience_in_year = json_encode(array_filter($this->input->post('event_experience_in_year')));

                $event_experience_in_year = '';
                if (array_filter($this->input->post('event_experience_in_year[]'))) {
                    $event_experience_in_year = json_encode($this->input->post('event_experience_in_year[]'));

                }

                // $event_description_of_work = json_encode(array_filter($this->input->post('event_description_of_work')));

                $event_description_of_work = '';
                if (array_filter($this->input->post('event_description_of_work[]'))) {
                    $event_description_of_work = json_encode($this->input->post('event_description_of_work[]'));

                }

                // $technical_experience_to_year = json_encode(array_filter($this->input->post('technical_experience_to_year')));

                $technical_experience_to_year = '';
                if (array_filter($this->input->post('technical_experience_to_year[]'))) {
                    $technical_experience_to_year = json_encode($this->input->post('technical_experience_to_year[]'));

                }

                // $technical_experience_in_year = json_encode(array_filter($this->input->post('technical_experience_in_year')));

                $technical_experience_in_year = '';
                if (array_filter($this->input->post('technical_experience_in_year[]'))) {
                    $technical_experience_in_year = json_encode($this->input->post('technical_experience_in_year[]'));

                }

                // $technical_description_of_work = json_encode(array_filter($this->input->post('technical_description_of_work')));

                $technical_description_of_work = '';
                if (array_filter($this->input->post('technical_description_of_work[]'))) {
                    $technical_description_of_work = json_encode($this->input->post('technical_description_of_work[]'));

                }

                $pincode                         = $encrptopenssl->encrypt($this->input->post('pincode'));
                $flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
                $area_colony_street_village      = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
                $town_city_and_state             = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
                $country                         = $encrptopenssl->encrypt($this->input->post('country'));

                $student_id_proof = $_FILES['student_id_proof']['name'];

                $other_domian       = '';
                $other_domian_plain = $this->input->post('other_domian');
                if ($other_domian_plain != '') {
                    $other_domian = $encrptopenssl->encrypt($other_domian_plain);
                }

                $other_area_int       = '';
                $other_area_int_plain = $this->input->post('other_area_int');
                if ($other_area_int_plain != '') {
                    $other_area_int = $encrptopenssl->encrypt($other_area_int_plain);
                }

                $other_skill_sets_plain = $this->input->post('other_skill_sets');

                $other_skill_sets = '';
                if ($other_skill_sets_plain != '') {
                    $other_skill_sets = $encrptopenssl->encrypt($other_skill_sets_plain);
                }

                $insertArr = array(
                    'type'                            => $type,
                    'status'                          => $status,
                    'university_course'               => $university_course,
                    'university_since_year'           => $university_since_year,
                    'university_to_year'              => $university_to_year,
                    'university_name'                 => $university_name,
                    'university_location'             => $university_location,
                    'current_study_course'            => $current_study_course,
                    'current_study_since_year'        => $current_study_since_year,
                    'current_study_to_year'           => $current_study_to_year,
                    'current_study_description'       => $current_study_description,
                    'domain_area_of_expertise'        => $domainString,
                    // 'areas_of_interest' => $AreaOfInterest,
                    'skill_sets'                      => $SkillSets,
                    'event_experience_in_year'        => $event_experience_in_year,
                    'event_description_of_work'       => $event_description_of_work,
                    'technical_experience_in_year'    => $technical_experience_in_year,
                    'technical_experience_to_year'    => $technical_experience_to_year,
                    'technical_description_of_work'   => $technical_description_of_work,
                    'pincode'                         => $pincode,
                    'flat_house_building_apt_company' => $flat_house_building_apt_company,
                    'area_colony_street_village'      => $area_colony_street_village,
                    'town_city_and_state'             => $town_city_and_state,
                    'country'                         => $country,
                    'user_id'                         => $userId,
                    'profile_completion_status'       => 'complete',

                    'domain_area_of_expertise_search' => $domain_search,
                    'skill_sets_search'               => $skillSets_search,
                    'additional_detail'               => $additional_detail,

                    'updatedAt'                       => date('Y-m-d H:i:s'),
                );
                $file_upload_flag = 0;
                if ($_FILES['profile_picture']['name'] != "") {
                    $profile_res = $this->Common_model_sm->upload_single_file("profile_picture", array('png', 'jpg', 'jpeg'), "profile_picture_" . date("YmdHis"), "./assets/profile_picture", "png|jpeg|jpg");
                    if ($profile_res['response'] == 'error') {
                        $data['profile_picture_error'] = $profile_res['message'];
                        $file_upload_flag              = 1;
                    } else if ($profile_res['response'] == 'success') {
                        $file_names                   = $profile_res['message'];
                        $insertArr['profile_picture'] = $file_names;
                        //@unlink("./uploads/organization_profile/".$organization_data[0]['pan_card']);
                    }
                }

                if ($_FILES['student_id_proof']['name'] != "") {
                    $profile_res = $this->Common_model_sm->upload_single_file("student_id_proof", array('png', 'jpg', 'jpeg', 'pdf'), "student_id_proof" . date("YmdHis"), "./assets/id_proof", "png|jpeg|jpg|pdf|PDF");
                    if ($profile_res['response'] == 'error') {
                        $data['student_id_proof_error'] = $profile_res['message'];
                        $file_upload_flag               = 1;
                    } else if ($profile_res['response'] == 'success') {
                        $file_name_id                  = $profile_res['message'];
                        $insertArr['student_id_proof'] = $encrptopenssl->encrypt($file_name_id);
                        //@unlink("./uploads/organization_profile/".$organization_data[0]['pan_card']);
                    }
                }

                //             if($_FILES['student_id_proof']['name']!=""){

                //     $config['upload_path']      = 'assets/id_proof';
                //     $config['allowed_types']    = '*';
                //     $config['max_size']         = '5000';
                //     $config['encrypt_name']     = TRUE;

                //     $upload_files  = @$this->master_model->upload_file('student_id_proof', $_FILES, $config, FALSE);
                //     $b_image = "";
                //     if(isset($upload_files[0]) && !empty($upload_files[0])){

                //         $b_image = $upload_files[0];

                //     }

                //     // $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");

                //     // $ext = pathinfo($b_image[0], PATHINFO_EXTENSION);

                //     // if(!array_key_exists($ext, $allowed)){

                //     //     $data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
                //     //     $this->session->set_flashdata('error',$data['imageError']);
                //     //     redirect(base_url('profile/student'));
                //     // }

                //     $filesize = $_FILES['student_id_proof']['size'];
                //     // Verify file size - 5MB maximum
                //     $maxsize = 5 * 1024 * 1024;
                // } else {
                //     $file_names = $data['profile_information'][0]['student_id_proof'];
                // }

                // $file_names = $encrptopenssl->encrypt($b_image[0]);

                $insertArr['other_domian']   = $other_domian;
                $insertArr['other_area_int'] = $other_area_int;

                $insertArr['other_skill_sets'] = $other_skill_sets;

                $id          = $this->input->post('profileId');
                $insertQuery = '';
                if (!empty($id)) {
                    $updateQuery = $this->master_model->updateRecord('student_profile', $insertArr, array('id' => $id));
                } else {
                    $insertQuery = $this->master_model->insertRecord('student_profile', $insertArr);
                }

                if ($insertQuery > 0) {
                    $this->session->set_flashdata('success_profile', 'Profile Updated Successfully.');
                    redirect(base_url('profile'));
                } else if ($updateQuery > 0) {
                    $this->session->set_flashdata('success_profile', 'Profile Updated Successfully.');
                    redirect(base_url('profile'));
                } else {
                    $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                    redirect(base_url('profile/student'));
                }

            }
        }

        $data['page_title']     = 'Student_profile';
        $data['user']           = $user_arr;
        $data['middle_content'] = 'student_profile/add';
        $this->load->view('front/front_combo', $data);
    }

    public function claim_as_expert($value = '')
    {

        $this->check_permissions->is_profile_complete();

        $encrptopenssl = new Opensslencryptdecrypt();
        $this->load->library('upload');
        $user_id = $this->session->userdata('user_id');

        $data['profile_information'] = $this->profile_data($user_id);

        if (
            $data['profile_information'][0]['aplied_for_expert'] == 'yes' &&
            $data['profile_information'][0]['expert_claim_admin_approval'] == 'Pending'
        ) {
            $this->session->set_flashdata('alreday_claim_expert', 'Application submitted successfully created');
            redirect(base_url('profile'));
        }

        $this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
        $this->db->join('registration_usersubcategory', 'registration.user_sub_category_id=registration_usersubcategory.subcat_id');
        $this->db->join('registration_usercategory', 'registration.user_category_id=registration_usercategory.id');
        $user_data = $this->master_model->getRecords("registration", array('user_id' => $user_id));
        // print_r($user_data);

        $user_arr = array();
        if (count($user_data)) {

            foreach ($user_data as $row_val) {

                $row_val['title']           = $encrptopenssl->decrypt($row_val['title']);
                $row_val['first_name']      = $encrptopenssl->decrypt($row_val['first_name']);
                $row_val['middle_name']     = $encrptopenssl->decrypt($row_val['middle_name']);
                $row_val['last_name']       = $encrptopenssl->decrypt($row_val['last_name']);
                $row_val['email']           = $encrptopenssl->decrypt($row_val['email']);
                $row_val['mobile']          = $encrptopenssl->decrypt($row_val['mobile']);
                $row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
                $row_val['user_category']   = $encrptopenssl->decrypt($row_val['user_category']);
                $row_val['sub_catname']     = $encrptopenssl->decrypt($row_val['sub_catname']);
                $user_arr[]                 = $row_val;
            }

        }

        $name = $user_arr[0]['title'] . " " . $user_arr[0]['first_name'] . " " . $user_arr[0]['last_name'];

        $this->load->library('upload');

        $this->form_validation->set_rules('bio_data', 'Bio Data', 'required');

        $this->form_validation->set_rules('no_of_paper_publication', 'No of Paper Publication', 'required');

        $this->form_validation->set_rules('no_of_patents', 'No of Patents', 'required');

        if ($this->form_validation->run()) {

            $type = $this->input->post('type');

            // $specify_fields_area_that_you_would_like = json_encode($this->input->post('specify_fields_area_that_you_would_like'));

            $bio_data = $encrptopenssl->encrypt($this->input->post('bio_data'));

            $no_of_paper_publication = $this->input->post('no_of_paper_publication');

            $paper_year = '';
            $py         = $this->input->post('paper_year[]');
            if (isset($py) && array_filter($py)) {
                $paper_year = json_encode($this->input->post('paper_year[]'));

            }

            $pcn             = $this->input->post('paper_conf_name[]');
            $paper_conf_name = '';
            if (isset($pcn) && array_filter($pcn)) {
                $paper_conf_name = json_encode($this->input->post('paper_conf_name[]'));

            }

            $paper_title = '';
            $pt          = $this->input->post('paper_title[]');

            if (isset($pt) && array_filter($pt)) {
                $paper_title = json_encode($this->input->post('paper_title[]'));

            }

            $no_of_patents = $this->input->post('no_of_patents');

            $patent_year = '';
            $pat_y       = $this->input->post('patent_year[]');
            if (isset($pat_y) && array_filter($pat_y)) {
                $patent_year = json_encode($this->input->post('patent_year'));

            }

            $patent_number = '';
            $pat_n         = $this->input->post('patent_number[]');
            if (isset($pat_n) && array_filter($pat_n)) {
                $patent_number = json_encode($this->input->post('patent_number'));

            }

            $patent_title = '';
            $pat_t        = $this->input->post('patent_title[]');
            if (isset($pat_t) && array_filter($pat_t)) {
                $patent_title = json_encode($this->input->post('patent_title'));

            }

            $portal_name_multiple = '';
            if (array_filter($this->input->post('portal_name[]'))) {
                $portal_name_multiple = json_encode($this->input->post('portal_name'));

            }

            $portal_link_multiple = '';
            if (array_filter($this->input->post('portal_link[]'))) {
                $portal_link_multiple = json_encode($this->input->post('portal_link'));

            }

            $portal_description_multiple = '';
            if (array_filter($this->input->post('portal_description[]'))) {
                $portal_description_multiple = json_encode($this->input->post('portal_description'));

            }

            $insertArr = array(
                'user_id'                     => $user_id,
                // 'specify_fields_area_that_you_would_like' => $specify_fields_area_that_you_would_like,
                'bio_data'                    => $bio_data,
                'no_of_paper_publication'     => $no_of_paper_publication,

                'paper_year'                  => $paper_year,
                'paper_conf_name'             => $paper_conf_name,
                'paper_title'                 => $paper_title,

                'no_of_patents'               => $no_of_patents,

                'patent_year'                 => $patent_year,
                'patent_number'               => $patent_number,
                'patent_title'                => $patent_title,

                // 'portal_name' => $portal_name,
                // 'portal_link' => $portal_link,
                // 'portal_description' => $portal_description,

                'portal_name_multiple'        => $portal_name_multiple,
                'portal_link_multiple'        => $portal_link_multiple,
                'portal_description_multiple' => $portal_description_multiple,

                'aplied_for_expert'           => 'yes',

                'expert_claim_admin_approval' => 'Pending',

                'updatedAt'                   => date('Y-m-d H:i:s'),
            );

            // echo "<pre>";
            // print_r($insertArr);
            // die;

            $id = $this->input->post('profileId');

            if (!empty($id)) {
                $updateQuery = $this->master_model->updateRecord('student_profile', $insertArr, array('id' => $id));
            } else {
                $insertQuery = $this->master_model->insertRecord('student_profile', $insertArr);
            }

            // if ($data['profile_information'][0]['aplied_for_expert']=='no') {

            //email to admin

            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('individual_to_expert_admin');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[NAME]', '[USERID]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$name, $user_id, $phoneNO];
                // sys.tip@technovuus.araiindia.com
                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => 'info@Technovuus.araiindia.com',
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            // email to user
            $email_send      = '';
            $slug            = $encrptopenssl->encrypt('expert_request_user_notification');
            $subscriber_mail = $this->master_model->getRecords("email_template", array("slug" => $slug));
            $setting_table   = $this->master_model->getRecords("setting", array("id" => '1'));
            $sub_content     = '';
            if (count($subscriber_mail) > 0) {

                $subject_title = $encrptopenssl->decrypt($subscriber_mail[0]['email_title']);
                $desc          = $encrptopenssl->decrypt($subscriber_mail[0]['email_description']);
                $fromadmin     = $encrptopenssl->decrypt($subscriber_mail[0]['from_email']);
                $sender_name   = $encrptopenssl->decrypt($setting_table[0]['field_1']);
                $phoneNO       = $encrptopenssl->decrypt($setting_table[0]['contact_no']);

                $arr_words = ['[username]', '[PHONENO]']; //'[SIGNATURE]'
                $rep_array = [$name, $phoneNO];
                // sys.tip@technovuus.araiindia.com
                $sub_content = str_replace($arr_words, $rep_array, $desc);
                $info_array  = array(
                    'to'      => $user_arr[0]['email'],
                    'cc'      => '',
                    'from'    => $fromadmin,
                    'subject' => $subject_title,
                    'view'    => 'common-file',
                );

                $other_infoarray = array('content' => $sub_content);

                $email_send = $this->emailsending->sendmail($info_array, $other_infoarray);
            }

            // }

            if ($updateQuery > 0) {

                $this->session->set_flashdata('success_claim_expert', 'Application submitted successfully created');
                redirect(base_url('profile'));
            } else if ($updateQuery > 0) {
                $this->session->set_flashdata('success_claim_expert', 'Application submitted successfully created');
                redirect(base_url('profile'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong! Please try again.');
                redirect(base_url('profile/claim_as_expert'));
            }
        }

        $data['page_title']     = 'Individual_profile';
        $data['user']           = $user_arr;
        $data['middle_content'] = 'individual_profile/claim_as_expert';
        $this->load->view('front/front_combo', $data);

    }

    public function changePassword()
    {
        $data['page_title']     = 'change_password';
        $data['middle_content'] = 'change_password/index';
        $this->load->view('front/front_combo', $data);
    }

    //      public function checkCurrentPassword()
    //      {
    //          $currentPassword = $this->input->post('current_password');
    //          $user = $this->master_model->getRecords('registration',array('password'=>$currentPassword));
    // if($user)
    // {
    //     echo "true";
    // }
    // else
    // {
    //     echo "false";
    // }
    //      }

    public function storeNewPassword()
    {
        // echo "<pre>";
        // print_r($_POST);die;
        $currentPassword = $this->input->post('current_password');
        $userId          = $this->session->userdata('user_id');
        $newPassword     = $this->input->post('new_password');
        if ($currentPassword == $newPassword) {

            $this->session->set_flashdata('error', 'Old and new password should not be same');
            redirect(base_url('profile/changePassword'));
        }

        $user = $this->master_model->getRecords('registration', array('user_id' => $userId, 'password' => $currentPassword));
        // echo $this->db->last_query();
        // die;

        if (count($user)) {
            $confirmPassword = $this->input->post('confirm_new_password');

            $insertArr   = array('password' => $newPassword);
            $updateQuery = $this->master_model->updateRecord('registration', $insertArr, array('user_id' => $userId));
            // echo $this->db->last_query();die;
            if ($updateQuery > 0) {
                $this->session->set_flashdata('success', 'Password Updated Successfully');
                redirect(base_url('profile/changePassword'));
            } else {
                $this->session->set_flashdata('error', 'Something went wrong');
                redirect(base_url('profile/changePassword'));
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Old Password');
            redirect(base_url('profile/changePassword'));
        }
    }

    public function check_pass()
    {
        $currentPassword = sha1($this->input->post('current_password'));
        $userId          = $this->session->userdata('user_id');

        $user = $this->master_model->getRecords('registration', array('user_id' => $userId, 'password' => $currentPassword));
        if (count($user)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function sortBy($field, &$array, $direction = 'asc')
    {
        usort($array, create_function('$a, $b', '
		        $a = $a["' . $field . '"];
		        $b = $b["' . $field . '"];
		        $a = strtolower(trim($a));
		        $b = strtolower(trim($b));

		        if ($a == $b) return 0;

		        @$direction = strtolower(trim($direction));
		        return ($a ' . (@$direction == 'desc' ? '>' : '<') . ' $b) ? -1 : 1;
		    '));

        return true;
    }

    public function verify_linkage_code()
    {
        $encrptopenssl = new Opensslencryptdecrypt();
        $promo_code    = $_POST['promo_code'];

        if ($promo_code == '') {
            $response = array(
                'success' => false,
                'msg'     => 'Invalid promo code.',

            );
            echo json_encode($response);
            exit();
        }

        $promo_code = $encrptopenssl->encrypt($promo_code);

        $whre_is_used = array(
            'promo_code'      => $promo_code,
            'promo_code_type' => 'Corporate',
        );

        $is_promo_code_used = $this->master_model->getRecords('subscriptions', $whre_is_used);

        if (count($is_promo_code_used) > 0) {
            $response = array(
                'success' => false,
                'msg'     => 'Linakge code already used.',

            );
            echo json_encode($response);
            exit();
        }

        $whre = array(
            'promo_code' => $promo_code,
            'code_type'  => 'Corporate',
        );
        $promo_code_info = $this->master_model->getRecords('promo_codes', $whre);
        if (count($promo_code_info) > 0) {

            $type          = $promo_code_info[0]['code_type'];
            $org_id        = $promo_code_info[0]['admin_id'];
            $promo_code_id = $promo_code_info[0]['id'];

            // $percentage=$promo_code_info[0]['percentage'];

            $response = array(
                'success'       => true,
                'msg'           => 'Linkage code applied successfully.',
                'type'          => $type,
                'org_id'        => $org_id,
                'promo_code_id' => $promo_code_id,
            );
            echo json_encode($response);
            exit();

        } else {
            $response = array(
                'success' => false,
                'msg'     => 'Invalid linakge code.',

            );
            echo json_encode($response);
            exit();
        }

    }

    public function is_discoverable()
    {
        $discoverable = $_POST['discoverable'];
        $user_id      = $_POST['user_id'];

        if ($user_id != '') {

            if ($discoverable == 'no') {
                $this->master_model->updateRecord('registration', array('contactable' => 'no'), array('user_id' => $user_id));
            }

            $query = $this->master_model->updateRecord('registration', array('discoverable' => $discoverable), array('user_id' => $user_id));
            if ($query) {
                echo true;

            } else {
                echo false;
            }

        } else {
            echo false;
        }

    }

    public function is_contactable()
    {
        $contactable = $_POST['contactable'];
        $user_id     = $_POST['user_id'];

        if ($user_id != '') {

            $query = $this->master_model->updateRecord('registration', array('contactable' => $contactable), array('user_id' => $user_id));
            if ($query) {
                echo true;

            } else {
                echo false;
            }

        } else {
            echo false;
        }

    }

}
