<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

function sort_array_by_date($array)
{
	usort($array, 'date_compare');
	return $array;
}

function date_compare($a, $b)
{
    $t1 = strtotime($a['created_on']);
    $t2 = strtotime($b['created_on']);
    return $t2 - $t1;
}  


function filterArrayByKeyValue($array, $key, $keyValue)
{
    return array_filter($array, function($value) use ($key, $keyValue) {
       return $value[$key] == $keyValue; 
    });
}

function filertNonFeaturedFeeds($array){
	return array_filter($array, function ($var) {
    	return (($var['is_featured'] == '0' || $var['is_featured'] == 'N' || $var['is_featured'] == 'No') && $var['xOrder']==0);
	});
}
	

function filertFeaturedFeeds($array){
	return array_filter($array, function ($var) {
    	return ($var['is_featured'] == '1' || $var['is_featured'] == 'Y' || $var['is_featured'] == 'Yes');
	});
}


function filertTrendingFeeds($array){
	return array_filter($array, function ($var) {
    	return ( ( $var['is_featured'] == '0' || $var['is_featured'] == 'No' ) && $var['xOrder'] != 0 );
	});
}