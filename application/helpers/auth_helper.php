<?php
/*
File_name 			: my_helper.php
Developer  			: Vicky Kuwar
Created on 			: 15th July 2020
Description 		: to manage frequently used authentication related activities.
Last Modified on 	:
Last Modified By 	:
Description 		:
 */
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/*function isLogin($return = '') {
	if (!isset($_SESSION['user_id'])) {
		if ($return != '') {
			return true;
		} else {
			redirect(base_url());
		}
	}
}*/
function isAccess($module) {
	//modules : PR: Projects
	$CI = &get_instance();
	$user_role = $CI->user_role;
	$role_array = array('AD' => array('PR', 'MR'),
		'SD' => array('PR'),
	);
	$module_array = $role_array[$user_role];
	if (!in_array($module, $module_array)) {
		redirect('dashboard');
	}
}

function dateDiffInDays($date1, $date2 = '') {
	$date2 = $date2 != '' ? $date2 : date('Y-m-d');
	$diff = strtotime($date2) - strtotime($date1);
	return abs(round($diff / 86400));
}

/*below function used to convert date from unix timestamp to date*/
function format_date($date) {
	return date('d-m-Y H:i:s', $date);
}
function createFromFormat($date) {
	$date_obj = DateTime::createFromFormat('d-m-Y', $date);
	$date = $date_obj->format('Y-m-d');
	return $date;
}
function get_years() {
	$year = date('Y');
	for ($i = 2018; $i <= $year; $i++) {
		$year_array[] = $i;
	}
	return $year_array;
}

function getLastInsertID($cid){
	
	$CI = &get_instance();
	$insertArr = array('c_id' => $cid);
	$insertQuery = $CI->master_model->insertRecord('track_challenge',$insertArr);
	$updateArrID = array('challenge_code' => $insertQuery);
	
	// Update Challenge Code
	$CI->master_model->updateRecord('track_challenge',$updateArrID,array('c_id' => $cid));	
	$CI->db->select('challenge_code');
	
	// Get Last Challenge COde
	$getRecord = $CI->master_model->getRecords("track_challenge",'','',array('id' => 'DESC'));
	return $getRecord[0]['challenge_code'];
	
}

function getLastWebinarID($wid){
	
	$CI = &get_instance();
	$insertArr = array('w_id' => $wid);
	$insertQuery = $CI->master_model->insertRecord('track_webinar',$insertArr);
	$updateArrID = array('webinar_code' => $insertQuery);
	
	// Update Challenge Code
	$CI->master_model->updateRecord('track_webinar',$updateArrID,array('w_id' => $wid));	
	$CI->db->select('webinar_code');
	
	// Get Last Challenge COde
	$getRecord = $CI->master_model->getRecords("track_webinar",'','',array('id' => 'DESC'));
	return $getRecord[0]['webinar_code'];
	
}

function getQestionID($qid){
	
	$CI = &get_instance();
	$insertArr = array('q_id' => $qid);
	$insertQuery = $CI->master_model->insertRecord('track_question',$insertArr);
	$updateArrID = array('question_code' => $insertQuery);
	
	// Update Challenge Code
	$CI->master_model->updateRecord('track_question',$updateArrID,array('q_id' => $qid));	
	$CI->db->select('question_code');
	
	// Get Last Challenge COde
	$getRecord = $CI->master_model->getRecords("track_question",'','',array('id' => 'DESC'));
	return $getRecord[0]['question_code'];
	
}