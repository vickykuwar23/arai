<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 *
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 * @copyright    Copyright (c) 2018 ESDS Software Solution Private.
 * @author       Vicky K
 * @package      Libraries
 * @version      2.0
 * @updated      2020-07-14
 */
class Opensslencryptdecrypt {
	
	public $encrypt_method = "AES-256-CBC";
	//these are test keys, you can be generate one at https://asecuritysite.com/encryption/keygen
	public $secret_key = 'EF61FE21EB90047086DED9A5386A8808'; //This is my secret key	
	public $secret_iv = 'F148EB0B9C6A3E5B16A03FCBE949BAE8'; //This is my secret iv
	
	public $output = false;
	function encrypt($string) {	

		$simple_string = $string;
		if(trim($string) != ''){
		$ciphering = "AES-128-CTR"; 
		// Use OpenSSl Encryption method 
		$iv_length = @openssl_cipher_iv_length($ciphering); 
		$options = 0; 
		// Non-NULL Initialization Vector for encryption 
		$encryption_iv = 'F148EB0B9C6A3E5B16A03FCBE949BAE8';
		// Store the encryption key 
		$encryption_key = "EF61FE21EB90047086DED9A5386A8808"; 
		// Use openssl_encrypt() function to encrypt the data 
		$encryption1 = @openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv); // Display the encrypted string 
		//echo "Encrypted String: " . $encryption1 . "\n"; 
		return $encryption = base64_encode($encryption1);
		// Non-NULL Initialization Vector for decryption 
	}else{

		return '';
	}

	}

	function decrypt($string) {

		
		if(trim($string) != ''){
			$simple_string = base64_decode($string);
			$ciphering = "AES-128-CTR";
			// Use OpenSSl Encryption method 
			$iv_length = @openssl_cipher_iv_length($ciphering); 
			$options = 0;
			$decryption_iv = 'F148EB0B9C6A3E5B16A03FCBE949BAE8'; 
			// Store the decryption key 
			$decryption_key = "EF61FE21EB90047086DED9A5386A8808"; 
	 		// Use openssl_decrypt() function to decrypt the data 
	        $decryption1= @openssl_decrypt($simple_string, $ciphering,$decryption_key, $options, $decryption_iv); 
			// Display the decrypted string 
			return $decryption1; 
			
			}else{
				return '';
			}
	}
	
	public function test() {
		$plain_txt = "This is my plain text";
		echo "Plain Text =" . $plain_txt . "\n";
		$encrypted_txt = $this -> encrypt($plain_txt);
		echo "Encrypted Text = " . $encrypted_txt . "\n";
		$decrypted_txt = $this -> decrypt($encrypted_txt);
		echo "Decrypted Text =" . $decrypted_txt . "\n";
		echo $plain_txt === $decrypted_txt ? "SUCCESS" : "FAILED";
		echo "\n";
	}
}

?>