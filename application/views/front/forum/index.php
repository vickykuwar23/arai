<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
    border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
.select2-container--default .select2-selection--single{height:35px!important; border:none}
#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
#WebinarWallSlider.owl-carousel .owl-dot,#WebinarWallSlider.owl-carousel .owl-nav .owl-next,#WebinarWallSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
#WebinarWallSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
#WebinarWallSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
#WebinarWallSlider .owl-nav.disabled{display:block}
#WebinarWallSlider .owl-nav{position:absolute; top:40%; width:100%}
#WebinarWallSlider.owl-theme .owl-dots .owl-dot.active span,#WebinarWallSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
#WebinarWallSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
#WebinarWallSlider .owl-nav .owl-prev{left:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
#WebinarWallSlider .owl-nav .owl-next{right:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
#WebinarWallSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}
#WebinarWallSlider .owl-item{ padding: 0 5px;}
#WebinarWallSlider .owl-item img{width:100%; }
#WebinarWallSlider .owl-dots{position:absolute;width:100%;bottom:25px}
#WebinarWallSlider .owl-item{ padding: 0;}
.WebinarsSlider.owl-carousel .owl-dot,.WebinarsSlider.owl-carousel .owl-nav .owl-next,.WebinarsSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
.WebinarsSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
.WebinarsSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
.WebinarsSlider .owl-nav.disabled{display:block}
.WebinarsSlider .owl-nav{position:absolute; top:40%; width:100%}
.WebinarsSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #000;color:#32f0ff}
.WebinarsSlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
.WebinarsSlider .owl-nav .owl-next{right:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
.WebinarsSlider .owl-item{ padding: 0 15px;}
.WebinarsSlider .owl-item img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2);}
.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
/* .WebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
.product_details{padding: 20px 0 0 0;}
/** Blog**/
.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag ul li a:hover{ background: #000; color: #32f0ff; }
.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.shareList ul li a:hover{ background: #000; color: #32f0ff; }
.userPhoto-info h3{ font-size: 19px !important; }
.userPhoto-info img{ border-radius: 50%;}
.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
.descriptionBox45 p{color: #FFF;}
.descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
.bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
.bs-example45 h3{color:#fff;font-size:19px;font-weight:800}
.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
/** Color Change**/
.nav-tabs{border-bottom:1px solid #32f0ff}
.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#32f0ff;background-color:#000 ;border-color:#000}
.product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {
	border-color: #32f0ff #32f0ff #32f0ff;
}
.title-bar .heading-border{background-color:#32f0ff}
.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
.owner-teammember img{height:100%!important; width:100%!important;border-radius: 0%!important;}
.owner-teammember a{background:none;}
.owner-teammember a:hover{background:none;}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Technology Wall</h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">List of challenges</li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<!--====================================================
                  FEATURED CHALLENGES
======================================================-->
  <section id="story" data-aos="fade-up">   
      <div class="container">
        <div class="row">
            <div class="col-md-12 product_details">
                <ul class="nav nav-tabs align-item-center  justify-content-center"  id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#TechNovuusFeed">TechNovuus Feed</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#AnswersForum">Questions And Answers Forum</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#Blogs">Blogs</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#KnowledgeRepository
                      ">Knowledge Repository</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#WebinarWall">Webinar Wall</a>
                    </li>
                  </ul>
            </div>
            </div>
            </div>
        <img src="<?php echo base_url('assets/img/serachBox.png') ?>" alt="serach" class="img-fluid">
            <div class="tab-content active" id="myTabContent">
                <div class="tab-pane active show fade" id="TechNovuusFeed">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-8">
                       <div class="formInfo65">
                       <h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
                       <div class="row userPhoto-info">
                        <div class="col-md-2">
                          <img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
                        </div>
                        <div class="col-md-10 pl-0">
                      <h3>A. P. J. Abdul Kalam</h3>
                      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
                      <div class="shareList">
                          <ul>
                              <li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
                              <li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
                              <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                          </ul>
                      </div>
                      </div>
                    </div>
                       </div>
                        <div class="formInfo65 mt-3">
                            <h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
                            <div class="row userPhoto-info">
                             <div class="col-md-2">
                               <img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
                             </div>
                             <div class="col-md-10 pl-0">
                           <h3>A. P. J. Abdul Kalam</h3>
                           <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
                           <div class="shareList">
                               <ul>
                                   <li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
                                   <li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
                                   <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                               </ul>
                           </div>
                           </div>
                         </div>
                            </div>
                            <div class="formInfo65 mt-3">
                                <h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
                                <div class="row userPhoto-info">
                                 <div class="col-md-2">
                                   <img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
                                 </div>
                                 <div class="col-md-10 pl-0">
                               <h3>A. P. J. Abdul Kalam</h3>
                               <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
                               <div class="shareList">
                                   <ul>
                                       <li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
                                       <li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
                                       <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                                   </ul>
                               </div>
                               </div>
                             </div>
                                </div>
                      </div>
                      <div class="col-md-4">
                     <div class="bs-example45">
                        <h3>Trending on TechNovuus </h3>
                        <div class="descriptionBox45 text-justify">
                            <ul>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                            </ul>
                        </div>
                     </div>
                     <div class="bs-example45 mt-4">
                        <h3>Featured on TechNovuus</h3>
                        <div class="descriptionBox45 text-justify">
                            <ul>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                                <li>Blog: Tesla AutoPilot</li>
                                <li>Webinar: Shaping Mobility</li>
                                <li>Questions and Answers Forum: How to develop Air Battery</li>
                            </ul>
                        </div>
                     </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="AnswersForum">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-12">
                            AnswersForum
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="tab-pane fade " id="Blogs">
                  <div class="container">
                    <div class="row">
                        <div class="col-md-9" data-aos="fade-left">
                            <div class="row">
                     <div class="col-md-6">
                        <img src="<?php echo base_url('assets/img/aboutInnerPage.jpg') ?>" alt="About Inner Page" class="img-fluid">
                        <div class="textInfoAbout3">
                          <div class="row userPhoto-info">
                              <div class="col-md-3">
                                <img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
                              </div>
                              <div class="col-md-9 pl-0">
                            <h3>A. P. J. Abdul Kalam</h3>
                            <div class="postInfo">
                                <ul>
                                    <li><strong>Posted</strong> on <i class="fa fa-calendar" aria-hidden="true"></i> 11-11-2020  at <i class="fa fa-clock-o" aria-hidden="true"></i> 10:30am </li>
                                </ul>
                            </div>
                            </div>
                          </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <h4>About Us</h4>
                        <div class="tag">
                            <ul>
                               <li><strong>Tags:</strong></li> 
                               <li><a href="#">EV</a></li> 
                               <li><a href="#">HEV</a></li> 
                               <li><a href="#">IC Engine</a></li> 
                               <li><a href="#">ETC</a></li>
                            </ul>
                        </div>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
                     </div>
                     <div class="col-md-12">
                        <p class="mt-3">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <p class="mt-4">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <div class="shareList mt-4">
                            <ul>
                               <li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li> 
                               <li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li> 
                                <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li> 
                                <li><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></li> 
                            </ul>
                        </div>
                     </div>
                    </div>
                            </div>
                            <div class="col-md-3" data-aos="fade-right">
                                <div class="bs-example2">
                                 <h3>Filters</h3> 
                                <div class="FiltersList">
                                <form>
                                    <div class="form-check">
                                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                      <label class="form-check-label" for="exampleCheck1">Technical</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                      </div>
                                      <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Technical</label>
                                      </div>
                                      <div class="form-check">
                                          <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                          <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            <label class="form-check-label" for="exampleCheck1">Technical</label>
                                          </div>
                                          <div class="form-check">
                                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                              <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                            </div>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                <label class="form-check-label" for="exampleCheck1">Technical</label>
                                              </div>
                                              <div class="form-check">
                                                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                  <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                                </div>
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                    <label class="form-check-label" for="exampleCheck1">Technical</label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                      <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="form-check-label" for="exampleCheck1">Technical</label>
                                                      </div>
                                                      <div class="form-check">
                                                          <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                          <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                            <label class="form-check-label" for="exampleCheck1">Technical</label>
                                                          </div>
                                                          <div class="form-check">
                                                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                              <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                                <label class="form-check-label" for="exampleCheck1">Technical</label>
                                                              </div>
                                                              <div class="form-check">
                                                                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                                  <label class="form-check-label" for="exampleCheck1">Non Technical</label>
                                                                </div>
                                  </form>
                                </div>
                            </div>
                                   </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="KnowledgeRepository">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12">
                        KnowledgeRepository
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade  pt-0" id="WebinarWall">
				<?php 
				$encrptopenssl =  New Opensslencryptdecrypt();	
				if(count($webinar_banner) > 0){
				?>
                    <div id="WebinarWallSlider" class="owl-carousel mb-2 owl-theme">
						<?php 
						foreach($webinar_banner as $wbanner){
						?>
                        <div class="owner-teammember">
                         <img src="<?php echo base_url('assets/webinar_banner/'.$encrptopenssl->decrypt($wbanner['banner_img'])); ?>" alt="<?php $encrptopenssl->decrypt($wbanner['banner_name']) ?>" title="<?php echo $wbanner['id'] ?>">
                          </div>
						  <?php 
							} // End Foreach
						?>                        
                    </div>
				<?php 					
				} // End Slider Count IF
				?>		
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12">
						<div class="title-bar">
                            <h2>Featured Webinars </h2>
                            <div class="heading-border"></div>
                        </div>
						<?php 
						if(count($featured_webinar) > 0){
						?>
                        
                                <div class="formInfoNew">
                                    <div  class="owl-carousel mb-2 owl-theme WebinarsSlider">
										<?php
											foreach($featured_webinar as $featuredSlide){	
										?>
                  <div class="owner-teammember inner-img">
											<div class="boxShadow">
												<a href="<?php echo base_url('webinar/viewDetail/'.base64_encode($featuredSlide['w_id'])); ?>"><img src="<?php echo base_url('assets/webinar/'.$featuredSlide['banner_img']); ?>" alt="experts" style="height: 196px!important;"></a>
												<div class="borderBoxNewClass">
													<h3><?php echo character_limiter($featuredSlide['webinar_name'], 25); ?></h3>
													<ul>
														<li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("jS F Y", strtotime($featuredSlide['webinar_date'])) ?></li>
														<li>|</li>
														<li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php
														$changeDate = $featuredSlide['webinar_time']." ".$featuredSlide['webinar_date']; 
														echo date('h:ia', strtotime($changeDate));
														//echo $featuredSlide['webinar_time'] ?> </li>
													</ul>
												</div>
											</div>
                                        </div>
										<?php } // Foreach End ?>	
                                    </div>
                                </div>
							<?php } else {  // Count End ?>	
								<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;">No Featured Webinar Available</p></div>
							<?php } // Foreach End ?>	
							<div class="title-bar">
								<h2>Upcoming Webinars</h2>
								<div class="heading-border"></div>
                            </div>
							<?php if(count($upcoming_webinar) > 0){ ?>
                                <div class="formInfoNew">
                                    <div class="row upcoming-list">
										<?php foreach($upcoming_webinar as $commingWebinar){ ?>
										<div class="col-md-4 upcoming-webinar">
											<div class="boxShadow">
											<a href="<?php echo base_url('webinar/viewDetail/'.base64_encode($commingWebinar['w_id'])); ?>"><img src="<?php echo base_url('assets/webinar/'.$commingWebinar['banner_img']); ?>" class="img-fluid" alt="experts" style="height: 196px!important;"></a>
											<div class="borderBoxNewClass">
												<h3><?php echo character_limiter($commingWebinar['webinar_name'], 25); ?></h3>
												<ul>
													<li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("jS F Y", strtotime($commingWebinar['webinar_date'])) ?></li>
													<li>|</li>
													<li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
													$changeDate2 = $commingWebinar['webinar_time']." ".$commingWebinar['webinar_date']; 
														echo date('h:ia', strtotime($changeDate2));	
													//echo $commingWebinar['webinar_time'] ?></li>
												</ul>
											</div>
										   </div>
										</div>
									   <?php } // Foreach End ?>	
									</div>
									<style>
										.ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
										.ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}
									</style>
									<?php if($total_cnt > $limit){ ?>
									<div class="col-md-12 ButtonAllWebinars text-center" id="show_more_main<?php echo $commingWebinar['w_id']; ?>">
										<a href="javascript:void(0)" class="click-more" id="<?php echo $commingWebinar['w_id']; ?>">Show More</a>
										<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
									</div>
									<?php } ?>
								</div>
								<?php } else { ?>
								<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;">No Upcoming Webinar Available</p></div>
								<?php } ?>									
						</div>
                    </div>
                  </div>
                </div>
              </div>

              
  </section>
  <style>
#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
#flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px;border:solid 1px #32f0ff;text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
#flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #000;text-decoration:none}
  </style>
  <div id="flotingButton2">
    <a href="<?php echo base_url('webinar/add'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post Webinar</a>
 </div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
 <script>
	(function($){
		$(window).on("load",function(){
			$(".descriptionBox45").mCustomScrollbar();
		});
	})(jQuery);
</script>
<script>
$( document ).ready(function() {
	// owl crousel testimonial section
	$('#WebinarWallSlider').owlCarousel({
		items: 1,
		autoplay: true,
		smartSpeed: 700,
		autoHeight: true,
		loop: true,
		nav: true,
		dots: true,
		navText: ["", ""],
		autoplayHoverPause: true
	});


   $('.WebinarsSlider').owlCarousel({ 
    autoplay: true,
    smartSpeed: 700,
    loop: false,
    nav: true,
    dots: false,
    navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
        0: {
            items: 1
        },
        480: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 3
        }
    }
  }); 
	
	// Load More Code 
	$(document).on('click','.click-more',function(){
		// $(".token").remove();
		var base_url = '<?php echo base_url(); ?>'; 
		var ID = $(this).attr('id');
		var length = $('.upcoming-webinar').length;
		var cs_t = 	$('.token').val();
		$('.click-more').hide();
		$('.loding').show();
		$.ajax({
			type:'POST',
			url: base_url+'webinar/load_more',
			data:'id='+length+'&csrf_test_name='+cs_t,
			dataType:"text",
			success:function(data){
				//console.log(data);
				var output = JSON.parse(data);
				if(output.html == ""){
					$(".show_more_main").remove();
				}	
				$(".token").val(output.token);								
				$('#show_more_main'+ID).remove();				
				$('.upcoming-list').append(output.html); 
			}
		});
	});
});	
$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>