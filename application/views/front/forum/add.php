<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
    cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
/**
 * Tooltip Styles
 */

/* Add this attribute to the element that needs a tooltip */
[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
}

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
}

.form-control-placeholder{top: -14px !important;}
textarea.error {color: #000000!important;}
input.error{color:#000000!important;}

</style>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/css/') ?>jquery.datetimepicker.min.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('assets/front/js/') ?>jquery.datetimepicker.js"></script>
<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Add Question</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Question</a></li>
				<li class="breadcrumb-item active" aria-current="page">Add Question </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-8">               
				<div class="formInfo">	
					
					<form method="POST" id="webinarform" name="webinarform" enctype="multipart/form-data">			
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="auestion_id" id="auestion_id" value="<?php echo $webinar_code; ?>"  readonly >
									<label class="form-control-placeholder floatinglabel">Question ID <em>*</em></label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="question" id="question" value=""  >
									<label class="form-control-placeholder floatinglabel">Question <em>*</em></label>
								</div>
							</div>

              <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-placeholder" for="tags">Tags <em>*</em></label>
                        <select class="form-control select2_common" name="tags[]" id="tags" multiple="multiple">
                            <?php if(!empty($tags))
                            {
                              foreach($tags as $res) 
                              { ?>
                              <option value="<?php echo $res['id'] ?>" > <?php echo ucfirst($res['tag_name']); ?></option>
                              <?php }
                            } ?>                        
                        </select>
                        <?php if(form_error('tags')!=""){ ?><span class="error"><?php echo form_error('tags'); ?></span> <?php } ?>
                      </div>
                    </div>
							
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Image </button>
									<input type="file" class="form-control" name="banner_img" id="banner_img"  value="">
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>											
							</div>	

							<?php
								$dynamic_cls = 'd-none';
								$full_img = $onclick_fun = '';
							
								?>
									
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="banner_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
											<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
										</div>
									</div>
								</div>
							
							
							<div class="col-md-12" ><br />
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary add_button">Cancel</a> 
								<button type="submit" class="btn btn-primary add_button">Submit</button> 
							</div>	

						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>	
// Word Count Validation
function getWordCount(wordString) {
	var words = wordString.split(" ");
	words = words.filter(function(words) { 
		return words.length > 0
	}).length;
	return words;
}
function show_hide_div()
{
	
	$('#paid-amt').hide();
	
	var values = $("input[name='cost_type']:checked").val();
	if(values == "PAID")
	{
		$('#paid-amt').show();
	}
	else 
	{			
		$('#paid-amt').hide();
		$("#cost_price").val(null);			
	}
}
show_hide_div();
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		
		  $('.select2_common').select2();
		/****New DateTimepicker**/
		
		//TimePicke Example
		$('.datetimepicker1').datetimepicker({
			datepicker:false,
			formatTime:"h:i a",
			format:"h:i a",
			step:15
		});
		
		
		//DatePicker Example
			/*$('#datetimepicker').datetimepicker();
			
			//TimePicke Example
			$('.datetimepicker1').datetimepicker({
				datepicker:false,
				formatTime:"h:i a",
				format:"h:i a",
				step:60
			});
			
			//Inline DateTimePicker Example
			$('#datetimepicker2').datetimepicker({
				format:'Y-m-d H:i',
				inline:true
			});
			
			//minDate and maxDate Example
			$('#datetimepicker3').datetimepicker({
				 format:'Y-m-d',
				 timepicker:false,
				 minDate:'-1970/01/02', //yesterday is minimum date
				 maxDate:'+1970/01/02' //tomorrow is maximum date
			});
			
			//allowTimes options TimePicker Example
			$('#datetimepicker4').datetimepicker({
				datepicker:false,
				allowTimes:[
				  '11:00', '13:00', '15:00', 
				  '16:00', '18:00', '19:00', '20:00'
				]
			});*/
		
		
		
		$("a").tooltip();
		var date = new Date();
		$('#start_date').datepicker({
			format: 'dd-mm-yyyy',
			startDate: date,
			autoclose: true,
			ignoreReadonly: true,
			minDate:new Date()	
		});
		
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		$.validator.addMethod("valid_url", function(value, element) {
			return this.optional(element) || /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i.test(value);
		}, "Please enter valid url");
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		// Using Class Add Validation To array Image upload
		$.validator.addMethod("image_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		$.validator.addClassRules("image_file", { image_required: true, valid_img_format:true, filesize:2000000 });
		
		// Textbox Validation required 
		$.validator.addMethod("youtube_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		// Youtube URL Validation
		$.validator.addMethod("youtube_input_valid", function(value, element) 
		{
			var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Please enter valid youtube url.");	
		
		$.validator.addClassRules("youtube_input", { youtube_required: true, youtube_input_valid: true });
		
		
		// ALPHANUMERIC Validation
		/*$.validator.addMethod("alphanumeric", function(value, element) 
		{
			var p = /^[A-Za-z0-9 _.-]+$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Only Alphabets and Numbers allowed");*/
		
		
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		// Doc Validation .png, jpeg, jpg etc.
		$.validator.addMethod("doc_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".txt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		}, 'Please upload only .pdf, .xls, .xlsx, .doc, .docx, .ppt, .txt file format');
		
		$.validator.addClassRules("doc_upload", { doc_format:true, filesize:2000000 });
		
		//******* JQUERY VALIDATION *********
		$("#webinarform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			rules:
			{
				question: { required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
        'tags[]':{required:true},
				banner_img: { required: true, valid_img_format: true,filesize:2000000},
			},
			messages:
			{
				question: { required: "This field is required", nowhitespace: "Please enter the title" },
			
				banner_img:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
									
			},
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "cost_type") 
				{
					error.insertAfter("#upload_type_err");
				}
				else { element.closest('.form-group').append(error); }
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BannerPreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#banner_img_outer").removeClass('d-none');
					//$("#banner_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#banner_img_outer .previous_img_outer img").attr("src", e.target.result);
					$("#banner_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#banner_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#banner_img").change(function() { BannerPreview(this); });	
</script>