<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Applicant Listing</h1>        
    </div>
    <!--/end container-->
</div><?php //echo "<pre>";print_r($webinar_applicant); ?>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">					
                    <!--<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									
									<select id="c_status" name="c_status"  class="form-control select2">                           
										<option value=""></option>
										<option value="Approved">Approved</option>								
										<option value="Closed">Closed</option>
										<option value="Pending">Pending</option>								
										<option value="Rejected">Rejected</option>
										<option value="Withdrawn">Withdrawn</option>
									</select>	
									<label class="form-control-placeholder leftLable2" for="technology">Challenge Status</label>					
								</div> 
							</div> 
						
							
							<div class="col-md-4 mt-3">
								<button type="submit" id="btnReset" class="btn btn-secondary">Clear All </button>
								<button type="submit" id="btn-click" class="btn btn-primary btn-click">Apply</button>
							</div>
							
						 </div>-->
				
                 <div class="row mt-12">
                    <div class="col-md-12">					
                        <div class="table-responsive">
                        <table id="applicant-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">Webinar ID</th>
								<th scope="col">Title</th>
								<th scope="col">Applicant Name</th>
                                <th scope="col">Date & Time</th>
                                <th scope="col">Costing</th>
								<th scope="col">Host Link</th>
                                <th scope="col">Webinar Status</th>
                              </tr>
                            </thead>
                            <tbody>							
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
	<input type="hidden" name="webinarId" id="webinarId" value="<?php echo $web_id; ?>" />
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
  $(document).ready( function () {
	var getID  = '<?php echo $web_id; ?>';  
	var base_path = '<?php echo base_url() ?>';
	var table = $('#applicant-list').DataTable({
		"ordering":false,
		"searching": false,
		"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"webinar/applicant_listing",
		"type":"POST",
		"data":function(data) {	
				data.webinarId 			= $("#webinarId").val();
				/*data.techonogy_id 			= $("#techonogy_id").val();
				data.change_visibility 		= $("#change_visibility").val();
				data.c_status 				= $("#c_status").val();
				data.audience_pref 			= $("#audience_pref").val();*/
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});

});
$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>