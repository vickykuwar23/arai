<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
.select2-container--default .select2-selection--single{height:35px!important; border:none}
#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
.body-background{background-color:#6698FF;}
.web-color{color:#fff;}


.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);padding:85px 15px 15px 15px;min-height:400px}
.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}

.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}

.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#FFF;background:#47a44b;margin-top:10px}

.bs-example2 .buttonNew2:hover{color:#FFF;background:#39843c;text-decoration:none}
.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff; }
.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
.borderBoxNewClass p{ font-size: 14px;color: #FFF; text-align: center; text-transform: uppercase; font-weight: 700; margin:15px 0 ; }
h5 {text-transform:uppercase;}
p {font-family: "Montserrat", sans-serif; color: #64707b; font-size: 15px; font-weight: 300;}
</style>
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p CollaborativeTechnologySolutions text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Webinar Details</h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url('webinar'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Webinar Details</li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<section id="registration-form" class="inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-aos="fade-right">
                <div class="bs-example2">
					<h3 class="text-center"><?php echo $webinar_details[0]['webinar_name'] ?></h3> 
					<div class="borderBoxNewClass">
						<ul>
						   <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date('d M Y', strtotime($webinar_details[0]['webinar_date'])) ?></li>
						   <li>|</li>
						   <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
						   $changeDate = $webinar_details[0]['webinar_time']." ".$webinar_details[0]['webinar_date']; 
						   echo date('h:ia', strtotime($changeDate)); ?></li>
						</ul>
						<p><i class="fa fa-inr" aria-hidden="true"></i> <?php if($webinar_details[0]['webinar_cost_type'] == "FREE"){echo "0"; }else{echo $webinar_details[0]['cost_price'];}echo "&nbsp;&nbsp;".$webinar_details[0]['webinar_cost_type'] ?></p>
					</div>
					<?php
					if($this->session->userdata('user_id') != $webinar_details[0]['user_id']){
					if(count($check_entry) == 0){ ?>
					 <a href="javascript:void(0)<?php //echo $webinar_details[0]['registration_link'] ?>" class="buttonNew register-link" id="<?php echo $webinar_details[0]['w_id'] ?>">Register Now</a>
					 <?php } } ?>
					 <a href="javascript:void(0)" class="buttonNew2 share-url" id="<?php echo $webinar_details[0]['w_id'] ?>"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a>   
                </div>   
            </div>
            <div class="col-md-8" data-aos="fade-left">
				<div class="aboutInnerPage">
					<img src="<?php echo base_url('assets/webinar/'.$webinar_details[0]['banner_img']); ?>" alt="About Inner Page" class="img-fluid">
				 </div>
				<!-- <div class="textInfoAbout2">
				<h2>About Us</h2>
				<p>In the report, BNEF outlines that electric vehicles (EVs) will hit 10% of global passenger vehicle sales in 2025, with that number rising to 28% in 2030 and 58% in 2040. According to the study, EVs currently make up 3% of global car sales.
				</p>
				</div> -->
            </div>
            <div class="col-md-12 mt-4">
              <h5>About Us</h5>
                <p><?php echo $webinar_details[0]['webinar_breif_info'] ?></p>
				<hr/>
                <h5>Key Takeaway</h5>
				<p><?php echo $webinar_details[0]['webinar_key_points'] ?></p>
				<!--<ul class="detail-list">
				<li>Through their entire lifetime, electric cars are better for the climate.</li>
				<li>Published in the journal Nature Climate Change yesterday.</li>
				</ul>-->
				<hr/>
				<h5>About Author</h5>
				<p><?php echo $webinar_details[0]['webinar_about_author'] ?> </p>
			</div>
        </div>
    </div>
</section>



 <div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents">
				<p><b>Copy below url to share</b></p>
				<p><?php echo $webinar_details[0]['registration_link'] ?></p>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script type="text/javascript">
   $(document).ready(function () { 		 
		// Load More Code 
		$(document).on('click','.register-link',function(){			
			var base_url = '<?php echo base_url(); ?>'; 
			var id = $(this).attr('id');
			var cs_t	= $('.token').val();
				
			$.ajax({
				type:'POST',
				url: base_url+'webinar/addAttendee',
				data:'id='+id+'&csrf_test_name='+cs_t,
				dataType:"text",
				success:function(data){
					//console.log(data);
					var output = JSON.parse(data);					
					$(".token").val(output.token);
					var titles = output.success;	
					var msg = output.msg;
					swal({
						title: titles,
						text: msg,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				 
				 var linkHref = '<?php echo $webinar_details[0]['registration_link'] ?>';
				 window.open(linkHref, '_blank');
				}
			});
		});
	 
   });
   
   $(document).on('click','.share-url',function(){	  
		$('#share-popup').modal('show');
   });
 
$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });	
</script>


