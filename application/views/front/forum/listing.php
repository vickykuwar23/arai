<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">My Webinar</h1>        
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">
					
                    
				
                 <div class="row mt-12">
                    <div class="col-md-12">					
                        <div class="table-responsive">
                        <table id="question-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">Question ID</th>
								<th scope="col">Question</th>
                                <th scope="col">Tags</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>							
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
<div class="modal fade" id="withdrawnModal" tabindex="-1" role="dialog" aria-labelledby="withdrawnModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_withdrawn">
	</div>
</div>	
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
  $(document).ready( function () {
	  
	var base_path = '<?php echo base_url() ?>';
	var table = $('#question-list').DataTable({
		"ordering":false,
		"searching": false,
		"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"webinar/question_listing",
		"type":"POST",
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	});
	
	
	
	
	

});

// Withdrawn Challenge 
$(document).on('click','.webinar-withdraw',function(){	
	var id = $(this).attr('data-id');
	var base_path = '<?php echo base_url() ?>';
	var cs_t = 	$('.token').val();
	//alert(cid);
	$.ajax({
		type:'POST',
		url: base_path+'webinar/processWithrawn',
		data:'id='+id+'&csrf_test_name='+cs_t,				
		dataType:"text",
		success:function(data){ //console.log(data);
			$("#preloader").css("display", "none");
			var output = JSON.parse(data);						
			$(".token").val(output.token);
			$("#web-status-"+id).html('Withdraw');
			$('#webinar-list').DataTable().ajax.reload();	
			swal({
					title: 'Success!',
					text: "Webinar successfully withdraw.",
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
		}
	});
});  

// Withdrawn Challenge 
$(document).on('click','.webinar-remove',function(){	
	var id = $(this).attr('data-id');
	var base_path = '<?php echo base_url() ?>';
	var cs_t = 	$('.token').val();
	//alert(cid);
	$.ajax({
		type:'POST',
		url: base_path+'webinar/processRemoved',
		data:'id='+id+'&csrf_test_name='+cs_t,				
		dataType:"text",
		success:function(data){ //console.log(data);
			$("#preloader").css("display", "none");
			var output = JSON.parse(data);						
			$(".token").val(output.token);
			$('#webinar-list').DataTable().ajax.reload();	
			swal({
					title: 'Success!',
					text: "Webinar successfully removed.",
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
		}
	});
});



// $(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
// $(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>