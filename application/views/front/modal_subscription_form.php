<?php 
// Create Object
$encrptopen_ssl =  New Opensslencryptdecrypt();
$subcription_data = $this->master_model->getRecords("newsletter", array("email_id" => $this->session->userdata('user_email')));
$subscrEmail = "";
if(count($subcription_data) > 0){
	
	$subscrEmail = $encrptopen_ssl->decrypt($this->session->userdata('user_email'));
	
} else {
	
	$subscrEmail = $encrptopen_ssl->decrypt($this->session->userdata('user_email'));
}

?>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="SubscriptionModalLabel">Subscription for Newsletter</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="subscriptionForm" name="subscriptionForm" role="form">
		
		<div class="p-3">
			<div class="form-group">
				<input type="email" name="email" id="email_sub" class="form-control" value="<?php echo $subscrEmail; ?>"  required="" />
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Please enter your email ID <em>*</em></label>
			</div>
			<!--<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<input type="checkbox" name="all_check" id="all_check" value="1" class="" checked="checked" />
					</div>
					<div class="col-md-10">
						<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">New Challenge Posts & News Updates</label>
					</div>
				</div>
			</div>-->
			<div class="form-group">
				<div class="row">
					<div class="col-md-1">
						<input type="checkbox" name="ch_challenge" id="ch_challenge" value="1" class="" checked="checked" />
					</div>
					<div class="col-md-11">
						<label for="email" class="form-control-placeholder mt-3 floatinglabel" style="position: absolute;left: 0;">Only New Challenges Posts</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-1">
						<input type="checkbox" name="ch_news" id="ch_news" value="1" class="" checked="checked" />
					</div>
					<div class="col-md-11">
						<label for="email" class="form-control-placeholder mt-3  floatinglabel" style="position: absolute;left: 0;">Only News Updates</label>
					</div>
				</div>
			</div>

		<div class="modal-footer" style="border-top:none; padding:0; justify-content:center">
			<button type="submit" class="btn btn-primary" id="email_subscription_btn_submit">Subscribe</button>
		</div>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<script>
	$(document).ready(function () {
		/*$.validator.setDefaults({
			
		});*/
		
		$.validator.addMethod("valid_email", function(value, element) 
		{ 
			var email = value;
			var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			var result = pattern.test(email);	
			if(result){return true;}else {return false;}
		});
		
		$.validator.addMethod("custom_validate_subscription_email_header", function(value, element)
		{
			if($.trim(value).length == 0)
			{
				return true;
			}
			else
			{
				$("#preloader-loader").css("display", "block");
				var isSuccess = false;
				var parameter = { "email":encodeURIComponent($.trim(value)) }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('home/verify_subscription_email_ajax') ?>",
					data: parameter,
					async: false,
					cache : false,
					dataType: 'JSON',
					success: function(data)
					{
						if($.trim(data.flag) == 'success')
						{
							isSuccess = true;
						}
						
						$.validator.messages.custom_validate_subscription_email_header = data.response;
						$("#preloader-loader").css("display", "none");
					}
				});
				
				return isSuccess;
			}
		});
		
		
		$( "#subscriptionForm" ).validate({
			onfocusout: false,
			onkeyup: false,
			onclick: false,
			/* debug: false, */
			rules: {
				email: {
					required: true,
					email: true,
					valid_email:true,
					custom_validate_subscription_email_header:true
					/* remote: {
						url: "<?php echo base_url('home/validateEmail'); ?>",
						type: "post"        
					} */
				}
			},
			messages: {
				email: {
					required: "Please Enter Email Address",
					valid_email:"Please enter valid email address.",
					remote : "This Email Address already exists."
				},				
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				$("#preloader-loader").css("display", "block");
				var email = $('#email_sub').val();
				$.ajax({
					url: "<?php echo base_url('home/InsertEmail'); ?>", 
					type: "POST",             
					//data: { "email" : email },
					data:$('#subscriptionForm').serialize(),
					dataType: 'JSON',
					cache: false,
					success: function(response) 
					{
						$("#SubscriptionModal").modal('hide');
						$("#preloader-loader").css("display", "none");
						swal({
							title: response.flag,
							text: response.message,
							icon: response.flag,
							}).then(function() {
							//location.reload();
						});
					}
				});
				return false;
			}
		});
	
	}); 
</script>