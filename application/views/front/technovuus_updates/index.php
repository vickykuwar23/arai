<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<style>
	

	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	
	.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
	
	
	.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
	.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
	
	.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	
	.product_details{padding: 20px 0 0 0;}
	
	
	/** Blog**/
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	
	.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
	
	.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
	.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
	
	.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
	
	
	.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; } 
	.tag ul li { float: none; padding: 0; margin: 5px 5px 5px 0; display: inline-block; }
	.tag ul li span{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px; cursor:auto; }
	.tag ul li span:hover{ background: #000; color: #32f0ff; }
	
	
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.shareList ul li span{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px; cursor:pointer; }
	.shareList ul li span:hover{ background: #000; color: #32f0ff; }
	
	.userPhoto-info h3{ font-size: 19px !important; }
	.userPhoto-info img{ border-radius: 50%;}
	.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
	.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
	.descriptionBox45 p{color: #FFF;}
	.descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
	.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
	
	.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
	.bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
	.bs-example45 h3{color:#fff;font-size:19px;font-weight:800}
	
	.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	
	.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	.formInfo65 p a{ text-decoration: none; font-weight: 800; color: #000;}
	.formInfo65 p a:hover{ text-decoration: none; font-weight: 800; color: #32f0ff;}
	
	/** Color Change**/
	.nav-tabs{border-bottom:1px solid #c80032}
	.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#fff;background-color:#c80032 ;border-color:#c80032}
	.product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {border-color: #c80032 #c80032 #c80032;}

	.title-bar .heading-border{background-color:#c80032}
	.search-sec2{padding:1rem;background:#c80032;width:100%;margin-bottom:25px}
	.filterBox2 .btn-primary{color:#c80032;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
	.filterBox2 .btn-primary:hover{color:#000;background-color:#c80032;border-color:#000}
	
	.search-sec { padding: 1rem; background: #c80032; width: 100%; margin-bottom: 0px; }
	.filterBox .btn-primary { color: #FFF; background-color: #333; border-color: #333; width: 100%; display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px; }
	.filterBox .btn-primary:hover { color: #FFF; background-color: #333; border-color: #333; }
	.clearAll { background: #333 !important; color: #FFF !important; border-color: #333 !important; margin-right: 15px; }
	.clearAll:hover { background: #333 !important; color: #FFF !important; border-color: #333 !important; }

	/**Questions And Answers Forum**/
	.userPhoto-info h3 span {font-size: 14px !important; color: #a3a3a3; text-transform: initial; }
	#AnswersForum h2{font-size:1.75rem}
	.bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}
	
	.listViewinfo h3 {font-size: 24px !important;}
	.listViewinfo h3 span {float: right;}
	.listViewinfo h3 span a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
	.listViewinfo h3 span a:hover {background: #000; color: #32f0ff; text-align: center; }	
	
	.listViewinfo img {border-radius: 50%;}
	.imgBoxView img {border-radius: 0%;}
	.listViewinfo ul{ list-style: none; padding: 0; margin: 0; font-size: 15px;}
	.listViewinfo ul li{ list-style: none; padding: 0; margin: 0; color: #333; font-weight: 800;}
	.listViewinfo ul li span{font-weight: normal; color: #000;}
	
	.likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
	.likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
	.likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
	.likeShare ul li:last-child{ float: right; margin: 0;}
	.likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
	.likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
	.dropdown-menu{ padding: 0px !important;}
	.dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
	.dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }	
	
	.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}	
	
	.imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}
	
	.blogSlider.owl-carousel .owl-dot,.blogSlider.owl-carousel .owl-nav .owl-next,.blogSlider.owl-carousel .owl-nav .owl-prev, .TrendingblogSlider.owl-carousel .owl-dot,.TrendingblogSlider.owl-carousel .owl-nav .owl-next,.TrendingblogSlider.owl-carousel .owl-nav .owl-prev {font-family:fontAwesome}
	.blogSlider.owl-carousel .owl-nav .owl-prev:before, .TrendingblogSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	.blogSlider.owl-carousel .owl-nav .owl-next:after, .TrendingblogSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	.blogSlider .owl-nav.disabled, .TrendingblogSlider .owl-nav.disabled{display:block}
	.blogSlider .owl-nav, .TrendingblogSlider .owl-nav{position:relative; bottom:0px; width:100%}
	.blogSlider.owl-theme .owl-dots .owl-dot.active span,.blogSlider.owl-theme .owl-dots .owl-dot:hover span,
	.TrendingblogSlider.owl-theme .owl-dots .owl-dot.active span,.TrendingblogSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
	.blogSlider.owl-theme .owl-nav, .TrendingblogSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
	.blogSlider .owl-nav .owl-prev, .TrendingblogSlider .owl-nav .owl-prev{background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
	.blogSlider .owl-nav .owl-next, .TrendingblogSlider .owl-nav .owl-next{  background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
	.blogSlider .owl-nav [class*=owl-]:hover, .TrendingblogSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}
	
	.blogSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0 5px;}
	.blogSlider .owl-item img, .TrendingblogSlider .owl-item img{width:100%; }
	.blogSlider .owl-dots, .TrendingblogSlider .owl-dots{position:absolute;width:100%;bottom:25px}
	.blogSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0;}
	
	a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;}  
	
	#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
	#flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
	#flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
	
.ButtonAllWebinars a {
    background: #c80032;
    color: #FFF;
    border: solid 1px #c80032;
    text-decoration: none;
    padding: 10px;
    border-radius: .25rem;
}

 

.ButtonAllWebinars a:hover {
    background: #FFF;
    color: #c80032;
    text-decoration: none;
}
	
	#UserDetailModal .modal_custom_content { padding:0 15px; }
	p a:hover { text-decoration:underline !important; }
	
	.blog_type_highlighted_text { font-size: 12px; background: #32f0ff; padding: 1px 6px 2px; border-radius: 3px; font-weight: 500; color: #000; line-height: 16px; position: absolute; top: 0; right: 0; }
	
	.blogProfileicon {
	position: absolute;
	top: 8px;
	right: 25px;
	width: 45px;
	height: 45px;
	border-radius: 5px;
	background: #32f0ff;
	padding: 5px;
	text-align: center;


}
</style>
<!-- New Css 01-02-2021 -->
<style>
/* technovuusAdmin Start */

.technovuusAdmin{background: url(http://design.esdsdev.com/arai-mockup/Technology-Discussion-Forum/img/abstractbg.png) top right no-repeat #fff; background-size: 100% 100%; position: relative; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); transition: all .5s  ease-in-out; -webkit-transition: all .3s ease-in-out; border:2px solid #c80032;  padding: 25px 25px 0px 25px;  width: 100%; margin: 0 auto; z-index: 99; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; margin-top: 25px; position: relative;}
.technovuusAdmin:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}

.technovuusAdmin::after {width: 0; height: 0; border-left: 0px solid transparent; border-right: 30px solid transparent;content: ""; position: absolute; left: 0px; top: 0px; border-top: 30px solid #c80032;}
.technovuusAdmin::before {width: 0; height: 0; border-left: 30px solid transparent; border-right: 0px solid transparent;content: ""; position: absolute; right: 0px; bottom: 0px; border-bottom: 30px solid #c80032; }

.technovuusAdminListNew ul{ list-style: none; padding: 0; margin:20px 0; display: flex; }
.technovuusAdminListNew ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.technovuusAdminListNew ul li a{  background: #c80032; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.technovuusAdminListNew ul li a:hover{ background: #000; color: #c80032; }

.technovuusAdminListNew a.link{ color: #000; text-decoration: none; font-weight: bold;}
.technovuusAdminListNew a.link:hover{ color: #c80032;}


.paddingRight{ padding-right: 155px;}
.descriptionLinkSection{ overflow-y: scroll; height: 250px;}
.descriptionLinkSection ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
.descriptionLinkSection ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}

.descriptionLinkSection li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f101";color:#FFF}
.descriptionLinkSection ul li a{ color: #FFF; text-decoration: none;}
.descriptionLinkSection ul li a:hover{ color: #CCC; text-decoration: none;}



.technovuusAdmin .btn-primary {color: #000; background: #c80032; padding: .375rem 5.75rem; border:none; text-decoration: none; text-align: center; font-size: 14px; margin: 0 auto;  display: block; position: relative;}
.technovuusAdmin .btn-primary:hover {color: #c80032; background: #000; border: none; text-decoration: none;}
.technovuusAdmin .btn{border-radius:0px;}


.technovuusAdmin .btn-primary::after {width: 0; height: 0; border-left:0px solid transparent; border-right:10px solid #c80032;content: ""; position: absolute; left: 0px; top: 0px; border-top: 35px solid #FFF;}
.technovuusAdmin .btn-primary::before {width: 0; height: 0; border-left: 9px solid #c80032; border-right: 0px solid #c80032; content: ""; position: absolute; right: 0px; bottom: 0px; border-bottom: 0px solid #c80032; border-top: 35px solid #FFF; }

.technovuusAdmin .btn-primary:hover:after{width: 0; height: 0; border-left:0px solid #FFF; border-right:10px solid #000;content: ""; position: absolute; left: 0px; top: -3px; border-top: 35px solid #FFF;}
.technovuusAdmin .btn-primary:hover:before {width: 0; height: 0; border-left: 10px solid #000; border-right: 0px solid #c80032; content: ""; position: absolute; right: 0px; bottom: 0px; border-bottom: 0px solid #000; border-top: 35px solid #FFF; }

/* technovuusAdmin End */
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">TechNovuus Feed</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">TechNovuus Feed</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php $this->load->view('front/webinar/inc_technology_wall_tabs'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane fade tab-pane active show fade pt-0" id="Blogs">
			
			<!------------ START : BLOG SEARCH --------->
			<div class="filterBox">
				<section class="search-sec">
					<div id="filterData" name="filterData">
						<div class="container">   
							<div class="row">
								<div class="col-lg-12">
									<div id="mySidenav" class="sidenav">
										<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
										<div class="scroll">	
											
											<ul>
												<li> <strong> Type Of Feed</strong> </li>		

												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="1" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Challenges</label>
												</li>

												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="2" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Experts</label>
												</li>

												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="6" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Blog</label>
												</li>										
												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="4" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Webinar</label>
												</li>	
												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="5" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Knowledge Repositories</label>
												</li>	
												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="3" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Resource Sharing</label>
												</li>	
																		
												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="7" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Technovuus Updates</label>
												</li>

												<li class="">
													<input class="form-check-input search_feed_type" type="checkbox" value="8" id="" name="search_feed_type[]" onchange="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)">
													<label class="form-check-label">Technology Transfers</label>
												</li>



											</ul>
										</div>								
									</div>
									
									<div class="row d-flex justify-content-center search_Box">								
										<div class="col-md-6">
											<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />	
											<button type="button" class="btn btn-primary searchButton" onclick="getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
										<div class="clearAll">
											<a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_blog_search_form()">Clear All</a>
										</div>
										<?php if(count($technology_data) > 0) { ?>
											<div class="filter d-nonex">
												<button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
											</div>
										<?php } ?>																		
										<div class="col-md-12"></div>								
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!------------ END : BLOG SEARCH --------->
			
			<div class="container" style="word-break: break-all;">	
		
				<div id="TrendingBlogSliderOuter"></div>
				
				<div class="title-bar" id="OuterTitle">
					<h2>All Feeds </h2>
					<div class="heading-border"></div>
				</div>
				<div id="FeedsOuter"></div>	
			</div>

			
	</div>
</section>


<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

<div class="modal fade" id="BlogDescriptionDetail" tabindex="-1" aria-labelledby="BlogDescriptionDetailLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogDescriptionDetailLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogDescriptionDetailContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
		<div class="modal-content" id="UserDetailModalContentOuter">			
		</div>
	</div>
</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php if($this->session->flashdata('success_blog_del_msg')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success_blog_del_msg'); ?>"); </script><?php } ?>

<script>
	function show_more_technology(val)
	{
		if(val == 'show')
		{
			$( ".filter_technology" ).removeClass( "d-none" )
			$( ".filter_load_less" ).removeClass( "d-none" )
			$( ".filter_load_more" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_technology" ).addClass( "d-none" )
			$( ".filter_load_less" ).addClass( "d-none" )
			$( ".filter_load_more" ).removeClass( "d-none" )
		}
	}
	
	function show_more_tag(val)
	{
		if(val == 'show')
		{
			$( ".filter_tag" ).removeClass( "d-none" )
			$( ".filter_load_less_tag" ).removeClass( "d-none" )
			$( ".filter_load_more_tag" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_tag" ).addClass( "d-none" )
			$( ".filter_load_less_tag" ).addClass( "d-none" )
			$( ".filter_load_more_tag" ).removeClass( "d-none" )
		}
	}
	
	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0);
		} 
	});
	
	function clear_blog_search_form() 
	{ 
		$("#search_txt").val('');  
		$('.search_technology:checkbox').prop('checked',false); 
		$('.search_tag:checkbox').prop('checked',false); 
		$('.search_blog_type:checkbox').prop('checked',false); 
		$('.search_feed_type:checkbox').prop('checked',false); 
		
		getFeedDataAjax(0, '<?php echo $limit; ?>',  1, 0);
	}
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
			
		//alert("Contents copied to clipboard.");
		return false;
	}
	

	
	//START : FEATURED & NON-FEATURED BLOG DATA
	function getFeedDataAjax(start, limit, is_search, is_show_more)
	{
		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		
		
		var selected_feed_type2 = [];
		$.each($("input.search_feed_type:checked"), function() { selected_feed_type2.push($(this).val()); });
		
		var keyword = encodeURIComponent($('#search_txt').val());

		var feed_type = encodeURIComponent(selected_feed_type2);
		
		parameters= { 'start':start, 'limit':limit,  'is_show_more':is_show_more, 'keyword':keyword, 'feed_type':feed_type, 'cs_t':cs_t }
		// $("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('technovuus_updates/get_feeds_ajax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					if(is_search == '1') { $("#FeedsOuter").html(''); }
					
					$(".token").val(data.csrf_new_token)
					if(is_show_more == 0) 
					{ 
						
					}
					
					if(data.feeds_response.trim() == "") { $("#OuterTitle").css("display", "none"); } 
					else { $("#OuterTitle").css("display", "block"); }
					$("#FeedsOuter").append(data.feeds_response);
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getFeedDataAjax(0, '<?php echo $limit; ?>',  0, 0);
	//END : FEATURED & NON-FEATURED BLOG DATA
	
	function read_more_description(blog_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'blog_id':blog_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/getread_more_descriptionAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$("#BlogDescriptionDetailLabel").html(data.blog_title);
					$("#BlogDescriptionDetailContent").html(data.blog_description);
					$("#BlogDescriptionDetail").modal('show');
				}else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	
	function openNav() 
	{		
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
		
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";		
	}
</script>

<script>
	function like_unlike_resource(id,flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("resource_sharing/like_unlike_ajax"); ?>',
			data:{ 'id':id, 'flag':flag,'is_feed':1, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#res_like_unlike_btn_outer_"+data.r_id).html(data.response);
				$("#like_count_outer_"+data.r_id).html(data.total_likes_html);
			}
		});
	}

	function like_unlike_kr(id,flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("knowledge_repository/like_unlike_blog_ajax"); ?>',
			data:{ 'id':id, 'flag':flag,'is_feed':1, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#kr_like_unlike_btn_outer_"+data.kr_id).html(data.response);
				$("#like_count_outer_"+data.kr_id).html(data.total_likes_html);
			}
		});
	}

	function like_unlike_blog(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("blogs_technology_wall/like_unlike_blog_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#blog_like_unlike_btn_outer_"+data.blog_id).html(data.response);
			}
		});
	}


	function like_unlike_feed(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("feeds/like_unlike_feed_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#feed_like_unlike_btn_outer_"+data.feed_id).html(data.response);
			}
		});
	}

	function share_feed(url)
	{
		$("#BlogShareLink").html(url);
		$("#share-popup").modal('show');
	}
</script>