<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>

<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<style>

.customBtnBlog { background: #c80032; color: #FFF; border:solid 1px #c80032; text-decoration: none; padding: 5px 15px; border-radius: .25rem; border:none; }
.customBtnBlog:hover { background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; }
.tag2 ul li:first-child {display: block; margin-bottom:5px !important;}
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}

.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}

.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */


.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag ul li a:hover{ background: #000; color: #32f0ff; }


.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.shareList ul li a:hover{ background: #000; color: #32f0ff; }

/** Color Change**/
.nav-tabs{border-bottom:1px solid #32f0ff}
.title-bar .heading-border{background-color:#32f0ff}
.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}

/**Questions And Answers Forum**/
#AnswersForum h2{font-size:1.75rem}
.bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}

.imgBoxView img {border-radius: 0%;}

.likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
.likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
.likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
.likeShare ul li:last-child{ float: right; margin: 0;}
.likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
.likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
.dropdown-menu{ padding: 0px !important;}
.dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
.dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }


.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
.imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}

/**Blog Icon Added Css**/
.blogProfileicon{ position: absolute; top:8px; right:25px; width: 45px; height: 45px; border-radius:5px; background:#32f0ff;  padding: 5px; text-align: center;}
.blogProfileicon img{ width: 50%;}

.boxShadow75{margin:0 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px;}
.boxShadow75:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
.boxShadow75 ul{ padding: 0; margin:13px 0; list-style: none;}
.boxShadow75 ul li{ padding: 0; margin: 0; display: inline-block; color: #64707b; font-size: 15px; position: relative;}
.boxShadow75 ul li span{ background: #32f0ff; color: #000; padding: 5px; border-radius: 5px;}
.boxShadow75 ul li a {background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.boxShadow75 ul li a:hover { background: #000; color: #32f0ff;}

.tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
.tag2 ul li{ padding: 0; margin:0 5px 10px 0 !important;}
.tag2 ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag2 ul li a:hover{ background: #000; color: #32f0ff; }

.boxShadow75 .btn-primary2{color: #32f0ff; background: #000; padding: .375rem .75rem; border: solid 1px #000; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary2:hover {color: #000; background: #32f0ff; border: solid 1px #32f0ff; text-decoration: none; }

.boxShadow75 .btn-primary{color: #000; background: #32f0ff; padding: .375rem .75rem; border: solid 1px #32f0ff; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary:hover {color: #32f0ff; background: #000; border: solid 1px #000; text-decoration: none; }

#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
#flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
#flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 

	
.shareKR ul li span{  background: #32f0ff; color: #000; text-decoration: none;  cursor:pointer; }
.shareKR ul li span:hover{ background: #000; color: #32f0ff; }
.ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
.ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}

.comment_block_common { border-top: 1px solid #ccc; padding: 15px 0 15px 0; }	
.comment_block_common .comment_img { width: 40px; height: 40px; overflow: hidden; border-radius: 50%; float: left; }
.comment_block_common .comment_img img { max-width: 100%; max-height: 100%; }
.comment_block_common .comment_inner { float: left; width: calc(100% - 50px); margin-left: 10px; }
.comment_block_common .comment_inner p.comment_name { font-weight: 600; margin: 0 0 0 0; }
.comment_block_common .comment_inner p.comment_time { margin: 0; font-size: 12px; font-weight: 500; color: #787878; }
.comment_block_common .comment_inner p.comment_content { margin: 5px 0 5px 0; }
.comment_block_common .comment_inner p.comment_like_btn { cursor: pointer; margin: 0; display: inline-block; }
.comment_block_common_reply { margin: 10px 0 0 50px; background: #f3f3f3; padding: 8px 10px 10px 10px; }
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down"
style="background: url(<?php if($form_data[0]['banner']!=''){echo base_url('uploads/admin_update_banner/').$form_data[0]['banner'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size:100% 100% !important;" 
>
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Technovuus Feeds </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
					<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Technovuus  Feeds Details </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="story" data-aos="fade-up">	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade" id="KnowledgeRepository">

			<div class="container">
                   <div class="col-md-12">
                                <div class="boxShadow75 shareKR">
                   				
                                	<div class="row">
                                	<div class="col-md-12">
									
									<h4><?php echo ucfirst($form_data[0]['feed_title']); ?></h4>

									<p class="mb-0"><strong>TNFID:</strong> <?php echo $form_data[0]['id_disp']; ?></p>
										
									<div class="desc pt-3">
									<strong>Brief about the Content</strong>

									<p><?php echo $form_data[0]['feed_description']; ?></p>
									</div>
                                	
									<?php if ($files_count  && $form_data[0]['files_check']==1 ) { ?>
										
									
                                		<div class="mt-3 mb-4 mt-2">
                                    	<a  class="btn btn-primary mb-2" href="javascript:void(0)" onclick="open_download_modal('<?php echo $form_data[0]['feed_id'] ?>')" >
                                    		<i class="fa fa-download"></i></a>
                                      	

                                      	<span class="pl-2"><i class="fa fa-file fa-lg">&nbsp </i><?php echo $files_count." files "  ?>
                                      	<i class="fa fa-database fa-lg pl-2">&nbsp</i><?php echo $files_size ?>
                                      	</span>
                                      	</div>

                                      	<?php } ?>

                                      <?php if (count($video_data) && $form_data[0]['video_check']==1) { ?>
				                        <div class="title-bar mt-5">
				                               <h2>Videos</h2>
				                                  <div class="heading-border"></div>
				                        </div>
				                        <div class="row">
				                          <!-- <div class="owl-carousel owl-theme tech_video_slider"> -->
				                           
				                           <?php foreach ($video_data as $key => $video): 
				                              $embed_video_url=str_replace('watch?v=', 'embed/', $video['video_url']);
				                              ?>
				                            
				                          <div class="col-md-4">
				                           <iframe width="200" height="200" src="<?php echo $embed_video_url ?>" frameborder="0" allowfullscreen></iframe>
				                           <p><?php echo $video['caption']; ?></p>

				                            <!-- <div class="item-video" data-merge="1"><a class="owl-video" href="<?php echo $video['video_url'] ?>"></a> -->
				                              <!-- <div><p class="video_caption"><?php echo $video['video_caption'] ?></p>
				                            </div> -->
				                            </div>
				                            
				                           <?php endforeach ?>
				                        </div>
				                        <?php } ?>	

                                    </div>

                                    <div class="col-md-12">
					                        <div class="mt-2 mb-3">
												
					                          
												<p class="mb-0"><strong>Published Date:</strong> <?php echo date("d-m-Y", strtotime($form_data[0]['created_on'])); ?></p>

													
												<div class="tag2 mt-3">
												<ul>
															
												<br>
												<li style="cursor:auto" id="like_count_outer_<?php echo $form_data[0]['feed_id']; ?>"><span style="cursor:auto; background: #32f0ff;color: #000;" style="cursor:auto"><?php echo $getTotalLikes ?> </span>
												</li>	
												<?php if(in_array($form_data[0]['feed_id'], $Actiondata['like_feed'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
												else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
												<li id="like_unlike_btn_outer_<?php echo $form_data[0]['feed_id']; ?>">
													<span onclick="like_unlike_feed('<?php echo base64_encode($form_data[0]['feed_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
												</li>
															
												<li>
												<span onclick="share_blog('<?php echo site_url('feeds/details/'.base64_encode($form_data[0]['feed_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
												</li> 


											
												</ul>

												</div>
					                        </div>
									 </div>
                            	</div>
                            	<?php if ($form_data[0]['comment']==1) { ?>
                            	
                            	
                            	<div class="row">
								<div class="col-md-12">
									<div class="comment_section_outer" style="margin-top:25px">
										<p style="border-bottom: 1px solid #ccc;padding-bottom: 5px;" id="total_comment_outer">Comments (4 Comments)</p>
										
										<div class="row">
											<div class="col-md-11">
												<input type="text" class="form-control" name="send_comment" id="send_comment" value="" placeholder="What do you think?" onkeyup="check_comment_validation()">
												<span id="comment_error" class='error'></span>
											</div>
											<div class="col-md-1">
												<button type="button" class="customBtnBlog" onclick="post_comment()">Post</button>
											</div>
										</div>
										
										<div class="form-group" id="sort_outer">
											Sort By : 
											<select class="" name="sort_by_comment" id="sort_by_comment" onchange="sort_comments(this.value)">
												<option value="Newest" selected>Newest</option>
												<option value="Oldest">Oldest</option>
												<option value="Best">Best</option>
											</select>
										</div>
										
										<div class="comment_block_outer">
											
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
                    
                                </div>


                            </div>	
								
					</div> 
			
			</div>

		</div>
	</div>
</section>

<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlogReportpopupLabel">Knowledge Repository Report</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div class="bgNewBox65">
				<p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
				<p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
				</div>
					
					<div class="form-group mt-4">
						
						<textarea class="form-control" id="popupBlogReportComment" name="popupBlogReportComment" required onkeyup="check_report_validation()"></textarea>
						<label for="popupBlogReportComment" class="form-control-placeholder">Comment <em>*</em></label>
						<span id="popupBlogReportComment_err" class='error'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submit_blog_report('<?php echo base64_encode($form_data[0]['feed_id']); ?>')">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="download_model_content_outer">
	</div>
</div>
<!-- Modal -->

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<?php
if($form_data[0]['admin_status'] == 1 && $form_data[0]['is_block'] == 0) { $action_flag = 0; }
else { $action_flag = 1; } ?>

<script>
	

$(".toggle_btn").click(function(event) {
	$(".btn_report").toggleClass('d-none')	
});
	function like_unlike_feed(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("feeds/like_unlike_feed_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#like_unlike_btn_outer_"+data.feed_id).html(data.response);
				$("#like_count_outer_"+data.feed_id).html(data.total_likes_html);
				
			}
		});
	}
</script>

<script>
	function open_download_modal(feed_id)
	{
		// module_id=31;
		// if(check_permissions_ajax(module_id) == false){
  //        	swal( 'Warning!','You don\'t have permission to access this page !','warning');
  //        	return false;
		// }
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('feeds/open_download_modal_ajax'); ?>",
			data: {'feed_id':feed_id},
			cache: false,
			success:function(data)
			{
				//alert(data)
				if(data == "error")
				{
					location.reload();
				}
				else
				{
					$("#download_model_content_outer").html(data);
					$("#DownloadModal").modal('show');
				}
			}
		});
	}
	function delete_blog_from_user_listing(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected blog?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("knowledge_repository/delete_blog"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{ 
						window.location.replace("<?php echo site_url('knowledge_repository'); ?>");
					}
				});
			} 
		});		
	}	
	
	function report_blog(feed_id, ReportedFlag, blog_disp_id, blog_title)
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not Report the knowledge repository as its status is Pending or Blocked",
				type: 'warning'				
			});
		<?php }else { ?>
			if(ReportedFlag == "1")
			{
				$("#popupBlogId").html(blog_disp_id);
				$("#popupBlogTitle").html(blog_title);
				$("#popupBlogReportComment_err").html('');
				$("#BlogReportpopup").modal('show');
				//$("#popupBlogReportComment").val('');
				$("#popupBlogReportComment").focus();
			}
			else
			{
				swal(
				{
					title: 'Alert!',
					text: "You have already reported the knowledge repository",
					type: 'alert',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
			}
		<?php } ?>
	}	
	
	function check_report_validation()
	{
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		if(popupBlogReportComment.trim() == "") { $("#popupBlogReportComment_err").html('Please enter the report comment'); $("#popupBlogReportComment").focus(); }
		else { $("#popupBlogReportComment_err").html(''); }
	}
	
	function submit_blog_report(feed_id)
	{	
		check_report_validation();
		
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		if(popupBlogReportComment.trim() != "")		
		{
			$("#popupBlogReportComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to report the selected knowledge repository?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("knowledge_repository/report_blog_ajax"); ?>',
						data:{ 'feed_id':feed_id, 'popupBlogReportComment':popupBlogReportComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#report_blog_btn_outer_"+data.feed_id).html(data.response);						
							$("#BlogReportpopup").modal('hide');
							
							swal(
							{
								title: 'Success!',
								text: "Knowledge Repository Reported successfully",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	

	

	
	function share_blog(url) 
	{

			$("#BlogShareLink").html(url);
			$("#share-popup").modal('show');
		
	}
	
	
	
	
	
	
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
			
		//alert("Contents copied to clipboard.");
		return false;
	}
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>

<script>

	function like_dislike_comment(id, flag)
	{		
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not like the comment as the blog status is Pending or Blocked",
				type: 'warning'
			});
		<?php }else { ?>
			var cs_t = 	$('.token').val();
			$.ajax(
			{
				type:'POST',
				url: '<?php echo site_url("feeds/like_unlike_comment_ajax"); ?>',
				data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
				dataType:"JSON",
				success:function(data)
				{
					$("#preloader").css("display", "none");
					$(".token").val(data.token);
					
					$("#like_unlike_comment_btn_outer_"+data.comment_id).html(data.response);
				}
			});
		<?php } ?>
	}
	
	function delete_comment(id, comment_type)
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not delete the comment as the blog status is Pending or Blocked",
				type: 'warning'
			});
		<?php }else { ?>
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to delete the selected comment?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("feeds/delete_comment_ajax"); ?>',
						data:{ 'id':id, 'comment_type':comment_type, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							
							$("#preloader").css("display", "none");
							$(".token").val(data.token);
							
							if(comment_type == 'reply')
							{
								$("#comment_block_"+data.comment_id).remove();
								$("#reply_comment_btn_outer_"+data.parent_comment_id).html(data.new_comment_reply_cnt);
							}
							else
							{				

								get_comment_data('<?php echo $feed_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', $("#sort_by_comment").val(), 0, 0, 1);
							}
							
							swal(
							{
								title: 'Success!',
								text: "Comment successfully deleted.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		<?php } ?>
	}

	function check_comment_validation()
	{
		var comment_content = $("#send_comment").val();
		if(comment_content.trim() == "") { $("#comment_error").html('Please enter your comment'); }
		else { $("#comment_error").html(''); }
	}
	
	$('#send_comment').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			post_comment();
		} 
	});
	
	function post_comment()
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not post the comment as the blog status is Pending or Blocked",
				type: 'warning'
			});
		<?php }else { ?>		
			check_comment_validation();
			
			var comment_content = $("#send_comment").val();
			if(comment_content.trim() != "") 
			{ 
				var cs_t = 	$('.token').val();
				var user_id = "<?php echo $user_id; ?>";
				var feed_id = "<?php echo $feed_id; ?>";
				var comment_id = 	0;
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("feeds/post_comment_ajax"); ?>',
					data:{ 'user_id':user_id, 'feed_id':feed_id, 'comment_id':comment_id, 'comment_content':comment_content, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$("#send_comment").val("");
						get_comment_data(feed_id, 0, '<?php echo $comment_limit; ?>', user_id, $("#sort_by_comment").val(), 0, data.new_comment_id, 1);
					}
				});
			}
		<?php } ?>
	}
	
	function sort_comments(sort_val)
	{
		get_comment_data('<?php echo $feed_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', sort_val, 0, 0, 1);
	}
	
	function get_comment_data(feed_id, start, limit, user_id, sort_order, is_show_more, new_comment_id, is_search)
	{
		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("feeds/get_comment_data_ajax"); ?>',
			data:{ 'feed_id':feed_id, 'start':start, 'limit':limit, 'user_id':user_id, 'sort_order':sort_order, 'is_show_more':is_show_more, 'new_comment_id':new_comment_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				if(data.flag == "success")
				{
					if(is_search == '1') { $(".comment_block_outer").html(''); }
					
					$(".token").val(data.csrf_new_token)
					$(".comment_block_outer").append(data.response);
					if (data.total_comment_cnt>1) {
						var comment_string=' Comments )';
					}else{
						var comment_string=' Comment )';
					}
					$("#total_comment_outer").html('Comments ('+data.total_comment_cnt+' '+comment_string+' ');
					if(data.total_comment_cnt == 0) { $("#sort_outer").css("display", "none"); } else { $("#sort_outer").css("display", "block"); }
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	var show_comments = '<?php echo  $form_data[0]["comment"] ?>';
	if (show_comments==1) {
	get_comment_data('<?php echo $feed_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', $("#sort_by_comment").val(), 0, 0, 0);
	}
	
	function show_hide_reply(comment_id)
	{
		if($("#comment_reply_"+comment_id).hasClass( "d-none" ))
		{
			$("#send_reply_comment_"+comment_id).val("");
			$("#send_reply_comment_"+comment_id).focus();
			$("#comment_reply_"+comment_id).removeClass( "d-none" );
		}
		else
		{
			$("#comment_reply_"+comment_id).addClass( "d-none" );
		}
	}
</script>

