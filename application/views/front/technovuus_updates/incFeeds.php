<div class="row">
   <div class="col-md-8">
      <?php foreach ($nonfeatured_feeds as $key => $feed) { ?>
      
      <?php if ($feed['is_featured']==0 ) { ?>
      <div class="technovuusAdmin">
         <h3 class="mb-4">Technovuus Feed</h3>
         <div class="row technovuusAdminListNew">
            <div class="col-md-3">
               <?php if($feed['banner']!=''){ ?>
               <img src="<?php echo base_url('uploads/admin_update_banner/').$feed['banner'] ?>"  class="img-fluid">
               <?php } else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>
            </div>
            <div class="col-md-9 pl-0">
               <h4>
              
               
               <a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>" title="<?php echo ucfirst($feed['feed_title']);  ?>"><?php if(strlen($feed['feed_title']) > 35 ){ echo ucfirst(substr($feed['feed_title'], 0,35) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a>
                </h4>
               <p><?php $feed_desc=strip_tags(html_entity_decode($feed['feed_description'])); 
                  
                  if(strlen($feed_desc) > 250 ){ echo ucfirst(substr($feed_desc, 0,250) )."..."; } else {echo ucfirst($feed_desc);} 
                  ?>
               </p>
               <div class="technovuusAdminListNew">
                  <ul class="mb-5">
                     <?php if(in_array($feed['feed_id'], $Actiondata['like_feed'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                        else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                     <li id="feed_like_unlike_btn_outer_<?php echo $feed['feed_id']; ?>">
                        <span style="cursor:pointer;" onclick="like_unlike_feed('<?php echo base64_encode($feed['feed_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                     </li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo site_url('feeds/details/'.base64_encode($feed['feed_id'])); ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                     <li><a href="<?php echo site_url('feeds/details/'.base64_encode($feed['feed_id']).'/1'); ?>"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>" type="submit" class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>


      <?php  } ?>
      
      <?php } ?>   
      <?php if($new_start < $all_nonfeatured_feeds_count)
         { ?><br>
      <div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
         <a href="javascript:void(0)" class="click-more" onclick="getFeedDataAjax('<?php echo $new_start; ?>', '<?php echo $limit; ?>', 0,1 )">Show More</a>       
      </div>
      <?php 
         } 
         
         ?>  
      <?php if ($feeds_count ==0) { ?>
      <div class="col-md-12 text-center">
         <p style="font-weight:bold; text-align: center;"><br>No Records Available</p>
      </div>
      <?php } ?>  
   </div>
   <?php if ($is_show_more==0) { ?>
   <div class="col-md-4">
      <div class="bs-example45 mt-4">
         <h3>Featured on TechNovuus</h3>
         <div class="descriptionLinkSection text-justify">
            <ul class="pr-3">
               <?php foreach ($featured_feeds as $key => $feed) { ?>
               

               <?php if ( $feed['is_featured']==1 ) { ?>
              
               <li><a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>">Technovuus Feed: <?php if(strlen($feed['feed_title']) > 65 ){ echo ucfirst(substr($feed['feed_title'], 0,65) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a></li>

              
               <?php } ?>
               <?php } ?>
            </ul>
         </div>
      </div>
   </div>
   <?php }   ?>
</div>