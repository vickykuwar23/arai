        <div class="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
               <li id="inner_tab_myrs">My Shared Resource</li>
               <li id="inner_tab_myactivers">Resources I applied for

            </li>
            </ul>
            <div class="resp-tabs-container hor_1">
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
               
                    <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myrs" name="search_txt" id="search_txt_myrs" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceRSDataAjax(1,'myrs','rs')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myrs','rs')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php  if(count($resource_sharing)>0){ ?>
                     <ul class="resp-tabs-list ver_1 content light ul_of_qa" id="content-id1">
                        <?php foreach ($resource_sharing as $key => $value1) { ?>
                        <li id="inner_li_<?php echo $value1['r_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $value1['resouce_name']; ?>
                        </li>
                        <?php } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php 
                        foreach ($resource_sharing as $key => $value) { 
                       $getTotalLikes = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $value['r_id']), 'like_id');

                        $getTotalRequest = $this->master_model->getRecordCount('arai_resource_sharing_connect_request', array('r_id' => $value['r_id']), 'connect_id');
                      
                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <h3><a onclick="show_resource_likes('<?php echo base64_encode($value['r_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalLikes ?></a></h3>
                                    <span>Likes</span>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a  href="javascript:void(0)" onclick="show_resource_requests('<?php echo base64_encode($value['r_id'])  ?>')"><?php echo $getTotalRequest ?></a>
                                       </h3>
                                    <span>Application Received</span>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <span class="last-text">
                                    <?php if ($value['admin_status']=='0'){  echo '<span title="Pending"><i class="fa fa-flag fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='1') { echo '<span title="Active"><i class="fa fa-check-circle fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='2') { echo '<span title="Rejected"><i class="fa fa-ban fa-2x" aria-hidden="true"  ></i></span>';
                                     } ?>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('resource_sharing/details/').base64_encode($value['r_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('resource_sharing/add/').base64_encode($value['r_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $r_id=$value['r_id'];
                                  if ($value['is_block'] == '0') {
                                      $b_title = 'block';
                                      $b_icon  = 'fa fa-ban';} else if ($value['is_block'] == '1') {
                                      $b_title = 'unblock';
                                      $b_icon  = 'fa fa-unlock-alt';}
                                  $onclick_block_fun = "block_unblock_kr('" . base64_encode($r_id) . "', '" . $b_title . "')";
                                 ?>
                                 <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_block_fun ?>">
                                 <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                 </a>
                                 

                                 <a href="javascript:void(0)" class="btn btn-primary37 mt-4" onclick="delete_resource('<?php echo base64_encode($r_id)  ?>')">
                                 <i class="fa fa-trash-o" aria-hidden="true"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                           <?php } ?>

                     </div>
                  <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No resource found</strong>
                     </div>
                  <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('resource_sharing') ?>" class="btn btn-primary36">
                  Explore Resource Sharing
                  </a>
               </div>
               <!-- My Answered Question -->
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
                     <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myactivers" name="search_txt" id="search_txt_myactivers" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceRSDataAjax(1,'myactivers','rs')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactivers','rs')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php if(count($res_active)>0){ ?>
              
                     <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                        <?php 
                        foreach ($res_active as $key => $res) { ?>
                        <li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $res['resouce_name']; ?>
                        </li>
                      <?php  } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php foreach ($res_active as $key => $res) { ?>
                        <div>
                           <div class="row">
                              <div class="col-md-12">
                           
                                    <?php 
                                    if( array_key_exists('like_info', $res) ){ ?>
                                       <p>Your liked this resource on <?php echo $res['like_info'] ?></p>
                                       <hr>
                                    <?php } 
                                    if(array_key_exists('connect_req',$res)){?>
                                     <div class="row">
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Resource ID :<?php echo $res['res_id'];  ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Application ID :<?php echo $res['connect_req'][0]['app_id'];  ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Purpose: <?php echo date('d-m-Y',strtotime($res['connect_req'][0]['created_on']))  ?>
                                          <br>
                                          <?php echo $res['connect_req'][0]['comments'];  ?> </span>
                                       </div>
                                    </div>
                                   
                                     </div>
                                     <?php   }?>
                                
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <a target="_blank" href="<?php echo base_url('resource_sharing/details/').base64_encode($res['r_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                           </div>
                        </div>

                        <?php  } ?>
                     </div>
                      <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No resource found</strong>
                     </div>
                        <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('resource_sharing') ?>" class="btn btn-primary36">
                  Explore Resource Sharing
                  </a>
               </div>
            </div>
         </div>

