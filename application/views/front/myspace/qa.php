<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="MySpaceOuter"></div>
         </div>
         
      </div>
   </div>

<!---------- MODAL POP UP FOR BLOCK QUESTION --------------------->
<div class="modal fade" id="QuestionBlockpopup" tabindex="-1" role="dialog" aria-labelledby="BlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlockpopupLabel"><b>Block Question?</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div>
                  <label><b>Question ID : </b><span id="popupQuerstionId"></span></label>
               </div>
               <div>
                  <label><b>Question : </b><span id="popupQuestionTitle"></span></label>
               </div>
               
               <div>
                  <label for="popupQuestionBlockReason"><b>Block reason <em>*</em></b></label>
                  <textarea class="form-control" id="popupQuestionBlockReason" name="popupQuestionBlockReason" required onkeyup="check_block_validation()"></textarea>
                  <span id="popupQuestionBlockReason_err" class='error'></span>
               </div>
            </div>   
            
            <div class="modal-footer">
               <button id="modal_submit_btn" type="button">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>

<!---------- MODAL POP UP FOR SHOW LIKES USER DATA FOR QUESTION --------------------->
<div class="modal fade" id="LikesPopUp" tabindex="-1" aria-labelledby="LikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="LikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>
   function block_unblock_question(id, type, custum_question_id, question)
   {
      if(type == "Block")
      {
         $("#popupQuerstionId").html(custum_question_id);
         $("#popupQuestionTitle").html(question);
         $("#popupQuestionBlockReason_err").html('');
         
         var onclick_fun = "submit_question_block('"+id+"', '"+type+"')";
         $("#modal_submit_btn").replaceWith('<button id="modal_submit_btn" type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
         
         $("#QuestionBlockpopup").modal('show');
         $("#popupQuestionBlockReason").val('');
         $("#popupQuestionBlockReason").focus();         
      }
      else
      {
         submit_question_block(id, type)
      }     
   }
   
   function check_block_validation()
   {
      var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
      if(popupQuestionBlockReason.trim() == "") { $("#popupQuestionBlockReason_err").html('Please enter the block reason'); $("#popupQuestionBlockReason").focus(); }
      else { $("#popupQuestionBlockReason_err").html(''); }
   }
   
   function submit_question_block(q_id, type)
   {  
      check_block_validation();
      
      var flag = 0;
      if(type == 'Block')
      {
         var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
         if(popupQuestionBlockReason.trim() == "") { flag = 1;}
      }
            
      if(flag == 0)
      {
         swal(
         {  
            title:"Confirm?",
            text: "Are you sure you want to "+type+" the selected question?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
         }).then(function (result) 
         { 
            if (result.value) 
            {
               var cs_t =  $('.token').val();
               $.ajax(
               {
                  type:'POST',
                  url: '<?php echo site_url("questions_answers_forum/block_unblock_question"); ?>',
                  data: {'id':q_id, 'type':type, 'popupQuestionBlockReason':popupQuestionBlockReason, 'csrf_test_name':cs_t },
                  dataType:"JSON",
                  success:function(data)
                  {
                     $(".token").val(data.token);
                     $("#QuestionBlockpopup").modal('hide');
                     
                     // $('#MyQuestionListing').DataTable().ajax.reload();

                     swal(
                     {
                        title: 'Success!',
                        text: "Question successfully "+type+"ed.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok'
                     });

                     var dec_q_id = atob(q_id);
                     getMySpaceQADataAjax(0,'myqa','qa',dec_q_id);
                  }
               });
            } 
         });
      }
   }  

   function delete_question(id)
   {
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to delete the selected question?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("questions_answers_forum/delete_question"); ?>',
               data:{ 'id':id, 'csrf_test_name':cs_t },
               dataType:"JSON",
               success:function(data)
               {
                  $(".token").val(data.token);
                  
                  swal(
                  {
                     title: 'Success!',
                     text: "Question successfully deleted.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });
                  getMySpaceQADataAjax(0,'myqa','qa');
               }
            });
         } 
      });
   }

   function show_question_likes(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("questions_answers_forum/show_likes_user_data_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#LikesLabel").html(data.question);           
               $("#LikesContent").html(data.response);            
               $("#LikesPopUp").modal('show');           
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }

   function show_question_reported(q_id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("questions_answers_forum/show_reported_user_ajax"); ?>',
         data:{ 'q_id':q_id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#LikesLabel").html(data.question);           
               $("#LikesContent").html(data.response);            
               $("#LikesPopUp").modal('show');           
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }
</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceQADataAjax(0,'myblogs','blog');
      // initializeTabs()
   });


   function getMySpaceQADataAjax(is_search=0,inner_tab,outer_tab,inner_li_id='')
   {
      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_qa_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#MySpaceOuter").html(data.myspace_qa);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()
               

               $("#inner_tab_"+outer_tab).trigger("click");
               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_qa').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,outer_tab){
      getMySpaceQADataAjax(0,inner_tab,outer_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>