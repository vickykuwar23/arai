        <div class="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
               <li id="inner_tab_mytt">My Shared Technologies</li>
               <li id="inner_tab_myactivett">Technologies I applied for
            </li>
            </ul>
            <div class="resp-tabs-container hor_1">
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
               
                    <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_mytt" name="search_txt" id="search_txt_mytt" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceTTDataAjax(1,'mytt','tt')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myrs','rs')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php  if(count($technology_transfer)>0){ ?>
                     <ul class="resp-tabs-list ver_1 content light ul_of_qa" id="content-id1">
                        <?php foreach ($technology_transfer as $key => $value1) { ?>
                        <li id="inner_li_<?php echo $value1['tech_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $value1['technology_title']; ?>
                        </li>
                        <?php } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php 
                        foreach ($technology_transfer as $key => $value) { 

                        $getTotalRequest = $this->master_model->getRecordCount('arai_technology_transfer_connect_request', array('tech_id' => $value['tech_id']), 'connect_id');
                      
                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <span><?php echo $value['id_disp'] ?></span>
                                    <br>
                                    <span>TT ID</span>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a  href="javascript:void(0)" onclick="show_connect_requests('<?php echo base64_encode($value['tech_id'])  ?>')"><?php echo $getTotalRequest ?></a>
                                       </h3>
                                    <span>Application Received</span>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="boxNewSection">
                                    <span class="last-text">
                                    <?php if ($value['admin_status']=='0'){  echo '<span title="Pending"><i class="fa fa-flag fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='1') { echo '<span title="Active"><i class="fa fa-check-circle fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='2') { echo '<span title="Rejected"><i class="fa fa-ban fa-2x" aria-hidden="true"  ></i></span>';
                                     } ?>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('technology_transfer/details/').base64_encode($value['tech_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('technology_transfer/add/').base64_encode($value['tech_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $tech_id=$value['tech_id'];
                                  if ($value['is_block'] == '0') {
                                      $b_title = 'block';
                                      $b_icon  = 'fa fa-ban';} else if ($value['is_block'] == '1') {
                                      $b_title = 'unblock';
                                      $b_icon  = 'fa fa-unlock-alt';}
                                  $onclick_block_fun = "block_unblock_tt('" . base64_encode($tech_id) . "', '" . $b_title . "')";
                                 ?>
                                 <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_block_fun ?>">
                                 <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                 </a>

                                 <?php 
                                 $tech_id=$value['tech_id'];
                                  if ($value['is_closed'] == '0') {
                                      $b_title = 'close';
                                      $b_icon  = 'fa fa-times-circle';
                                } 
                                else if ($value['is_closed'] == '1') {
                                      $b_title = 'open';
                                      $b_icon  = 'fa fa-check-circle';
                                }
                                  $onclick_close_fun = "close_tt('" . base64_encode($tech_id) . "', '" . $b_title . "')";
                                 ?>
                                 <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_close_fun ?>">
                                 <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                 </a>
                                 

                                 <a href="javascript:void(0)" class="btn btn-primary37 mt-4" onclick="delete_tt('<?php echo base64_encode($tech_id)  ?>')">
                                 <i class="fa fa-trash-o" aria-hidden="true"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                           <?php } ?>

                     </div>
                  <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No technologies found</strong>
                     </div>
                  <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('technology_transfer') ?>" class="btn btn-primary36">
                 Explore Technologies for Transfer
                  </a>
               </div>
               <!-- My Answered Question -->
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
                     <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myactivett" name="search_txt" id="search_txt_myactivett" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceTTDataAjax(1,'myactivett','tt')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactivett','tt')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php if(count($tech_trans_active)>0){ ?>
              
                     <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                        <?php 
                        foreach ($tech_trans_active as $key => $res) { ?>
                        <li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $res['technology_title']; ?>
                        </li>
                      <?php  } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php foreach ($tech_trans_active as $key => $res) { ?>
                        <div>
                           <div class="row">
                              <div class="col-md-12">
                                 
                                    <?php 
                                    if(array_key_exists('connect_req',$res)){?>
                                     <div class="row">
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Technology ID :<?php echo $res['id_disp'];  ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Application ID :<?php echo $res['connect_req'][0]['app_id'];  ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="boxNewSection">
                                          <span>Purpose: <?php echo date('d-m-Y',strtotime($res['connect_req'][0]['created_on']))  ?>
                                          <br>
                                          <?php echo $res['connect_req'][0]['comments'];  ?> </span>
                                       </div>
                                    </div>
                                   
                                     </div>
                                     <?php   }?>
                                 
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <a target="_blank" href="<?php echo base_url('technology_transfer/details/').base64_encode($res['tech_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                           </div>
                        </div>

                        <?php  } ?>
                     </div>
                      <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No technologies found</strong>
                     </div>
                        <?php } ?>
                  </div>
                  <p>&nbsp</p>
                   <a target="_blank" href="<?php echo base_url('technology_transfer') ?>" class="btn btn-primary36">
                 Explore Technologies for Transfer
                  </a>
               </div>
            </div>
         </div>

