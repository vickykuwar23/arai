<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo base_url('assets/front/js/easyResponsiveTabs.js') ?>"></script>

<script>
 function initializeScroll(){
   
           $.mCustomScrollbar.defaults.scrollButtons.enable = true; //enable scrolling buttons by default
           $.mCustomScrollbar.defaults.axis = "y"; //enable 2 axis scrollbars by default
   
           $("#content-id1, #content-id2, #content-id3, #content-id4, #content-id2").mCustomScrollbar({
               theme: "inset-2-dark"
           });
   
           $(".all-themes-switch a").click(function (e) {
               e.preventDefault();
               var $this = $(this),
                   rel = $this.attr("rel"),
                   el = $(".content");
               switch (rel) {
                   case "toggle-content":
                       el.toggleClass("expanded-content");
                       break;
               }
           });
   
 }

 function initializeTabs(){
    //Horizontal Tab
       $('.parentHorizontalTab').easyResponsiveTabs({
           type: 'default', //Types: default, vertical, accordion
           width: 'auto', //auto or any width like 600px
           fit: true, // 100% fit in a container
           tabidentify: 'hor_1', // The tab groups identifier
           activate: function (event) { // Callback function if tab is switched
               var $tab = $(this);
               var $info = $('#nested-tabInfo');
               var $name = $('span', $info);
               $name.text($tab.text());
               $info.show();
           }
       });

    //Horizontal Tab2
        $('.parentHorizontalTab2').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_2', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

   
       // Child Tab
       $('.ChildVerticalTab_1').easyResponsiveTabs({
           type: 'vertical',
           width: 'auto',
           fit: true,
           tabidentify: 'ver_1', // The tab groups identifier
           activetab_bg: '#fff', // background color for active tabs in this group
           inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
           active_border_color: '#c1c1c1', // border color for active tabs heads in this group
           active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
       });

   
       //Vertical Tab
       $('#parentVerticalTab').easyResponsiveTabs({
           type: 'vertical', //Types: default, vertical, accordion
           width: 'auto', //auto or any width like 600px
           fit: true, // 100% fit in a container
           closed: 'accordion', // Start closed if in accordion view
           tabidentify: 'hor_1', // The tab groups identifier
           activate: function (event) { // Callback function if tab is switched
               var $tab = $(this);
               var $info = $('#nested-tabInfo2');
               var $name = $('span', $info);
               $name.text($tab.text());
               $info.show();
           }
       });
}

</script>