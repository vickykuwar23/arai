<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="MySpaceOuter"></div>
         </div>
         
      </div>
   </div>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>
   function show_resource_requests(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("resource_sharing/show_resource_requests_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }
   
   function show_resource_likes(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("resource_sharing/show_resource_likes_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }

   function block_unblock_kr(id, type)
   {
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to "+type+" the selected resource sharing?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("resource_sharing/block_unblock_kr"); ?>',
               data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
               dataType:"JSON",
               success:function(data)
               {
                  $(".token").val(data.token);
                  swal(
                  {
                     title: 'Success!',
                     text: "Resource sharing successfully "+type+"ed.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });
                  var dec_r_id = atob(id);
                  getMySpaceRSDataAjax(0,'myrs','rs',dec_r_id);
               }
            });
         } 
      });
   }

   function delete_resource(id)
   {
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to delete the selected resource sharing?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("resource_sharing/delete_resource"); ?>',
               data:{ 'id':id, 'csrf_test_name':cs_t },
               dataType:"JSON",
               success:function(data)
               {
                  $(".token").val(data.token);
                  swal(
                  {
                     title: 'Success!',
                     text: "Resource sharing successfully deleted.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });
                  getMySpaceRSDataAjax(0,'myrs','rs');
               }
            });
         } 
      });
   }
</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceRSDataAjax(0,'myrs','rs');
      // initializeTabs()
   });


   function getMySpaceRSDataAjax(is_search=0,inner_tab,outer_tab,inner_li_id='')
   {
      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_rs_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#MySpaceOuter").html(data.myspace_rs);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()
               

               $("#inner_tab_"+outer_tab).trigger("click");
               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_qa').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,outer_tab){
      getMySpaceRSDataAjax(0,inner_tab,outer_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>