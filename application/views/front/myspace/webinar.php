<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="MySpaceOuter"></div>
         </div>
         
      </div>
   </div>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogReportpopupLabel">Technology transfer delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
            
            <!-- <div class="bgNewBox65">
            <p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
            <p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
            </div> -->
               
               <div class="form-group mt-4">
                  
                  <label for="popupKrDeleteComment" class="">Reason <em style="color: red">*</em></label>
                  <textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
                  <span id="popupKrDeleteComment_err" class='error'></span>
               </div>
            </div>   
            <input type="hidden" id="popupBlogIdHidden" name="">
            <div class="text-center pb-3">
               <button type="button" class="btn btn-primary" onclick="submit_delete_request()">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>

$(document).on('click','.webinar-withdraw',function(){   
      var id = $(this).attr('data-id');
      var tab = $(this).attr('data-tab');
      var base_path = '<?php echo base_url() ?>';
      var cs_t =  $('.token').val();
      //alert(cid);
      $.ajax({
         type:'POST',
         url: base_path+'webinar/processWithrawn',
         data:'id='+id+'&csrf_test_name='+cs_t,          
         dataType:"text",
         success:function(data){ //console.log(data);
            $("#preloader").css("display", "none");
            var output = JSON.parse(data);                  
            $(".token").val(output.token);
            $("#web-status-"+id).html('Withdraw');
 
            swal({
               title: 'Success!',
               text: "Webinar successfully withdraw.",
               type: 'success',
               showCancelButton: false,
               confirmButtonText: 'Ok'
            });

            getMySpaceWebinarDataAjax(0,'myweb',tab,id);
         }
      });
   }); 

   $(document).on('click','.webinar-remove',function(){  
      var id = $(this).attr('data-id');
      var tab = $(this).attr('data-tab');
      var base_path = '<?php echo base_url() ?>';
      var cs_t =  $('.token').val();
      //alert(cid);
      $.ajax({
         type:'POST',
         url: base_path+'webinar/processRemoved',
         data:'id='+id+'&csrf_test_name='+cs_t,          
         dataType:"text",
         success:function(data){ //console.log(data);
            $("#preloader").css("display", "none");
            var output = JSON.parse(data);                  
            $(".token").val(output.token);
            swal({
               title: 'Success!',
               text: "Webinar successfully removed.",
               type: 'success',
               showCancelButton: false,
               confirmButtonText: 'Ok'
            });
            getMySpaceWebinarDataAjax(0,'myweb',tab);
         }
      });
   });

</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceWebinarDataAjax(0,'myweb','my_exclusive');
      // initializeTabs()
   });


   function getMySpaceWebinarDataAjax(is_search=0,inner_tab,nested_tab='',inner_li_id='')
   {
      if (is_search==1) {
         if (inner_tab=='myweb') {
            nested_tab =$("#nested_my_activwebinar li.resp-tab-active").data('tab');
         }else if(inner_tab=='myactiveweb'){
            nested_tab =$("#nested_my_activwebinar li.resp-tab-active").data('tab');
         }
      }

      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t ,'nested_tab':nested_tab}
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_webinar_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#MySpaceOuter").html(data.myspace_tt);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()

                if (is_search==1) {
                  if (inner_tab=='myweb') {
                     $("#nested_tab_"+nested_tab).trigger("click");
                  }else if(inner_tab=='myactiveweb'){
                     $("#active_nested_tab_"+nested_tab).trigger("click");
                  }
               }else{
                  $("#nested_tab_"+nested_tab).trigger("click");
               }



               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_qa').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,nested_acive=0){
      nested_tab='';
      if (nested_acive==0) {
         nested_tab =$("#nested_my_webinar li.resp-tab-active").data('tab');
      }else{
         nested_tab =$("#nested_my_activwebinar li.resp-tab-active").data('tab');
      }
      getMySpaceWebinarDataAjax(0,inner_tab,nested_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>