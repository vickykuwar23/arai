        <div class="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
               <li id="inner_tab_mykr">My Shared Repositories</li>
               <li id="inner_tab_myactivekr">Repositories I am Interested in
            </li>
            </ul>
            <div class="resp-tabs-container hor_1">
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
               
                    <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_mykr" name="search_txt" id="search_txt_mykr" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceKRDataAjax(1,'mykr','kr')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('mykr','kr')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php  if(count($knowledge_repository)>0){ ?>
                     <ul class="resp-tabs-list ver_1 content light ul_of_qa" id="content-id1">
                        <?php foreach ($knowledge_repository as $key => $value1) { ?>
                        <li id="inner_li_<?php echo $value1['kr_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $value1['title_of_the_content']; ?>
                        </li>
                        <?php } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php 
                        foreach ($knowledge_repository as $key => $value) { 
                       $getTotalLikes = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $value['kr_id']), 'like_id');

                        $getTotalDownloads = $this->master_model->getRecordCount('arai_knowledge_download_log', array('kr_id' => $value['kr_id']), 'd_id');
                      
                        $getTotalReported     = $this->master_model->getRecordCount('arai_knowledge_repository_reported', array('kr_id' => $value['kr_id']), 'reported_id');
                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3><a onclick="show_blog_likes('<?php echo base64_encode($value['kr_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalLikes ?></a></h3>
                                    <span>Likes</span>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a  href="javascript:void(0)" onclick="show_downloads('<?php echo base64_encode($value['kr_id'])  ?>')"><?php echo $getTotalDownloads ?></a>
                                       </h3>
                                    <span>Downloads</span>
                                 </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3><a onclick="show_blog_reports('<?php echo base64_encode($value['kr_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalReported ?></a></h3>
                                    <span>Reported</span>
                                 </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span class="last-text">
                                    <?php if ($value['admin_status']=='0'){  echo '<span title="Pending"><i class="fa fa-flag fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='1') { echo '<span title="Active"><i class="fa fa-check-circle fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($value['admin_status']=='2') { echo '<span title="Rejected"><i class="fa fa-ban fa-2x" aria-hidden="true"  ></i></span>';
                                     } ?>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('knowledge_repository/details/').base64_encode($value['kr_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('Knowledge_repository/add/').base64_encode($value['kr_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $kr_id=$value['kr_id'];
                                  if ($value['is_block'] == '0') {
                                      $b_title = 'Delete';
                                      $b_icon  = 'fa fa-ban';} else if ($value['is_block'] == '1') {
                                      $b_title = 'Restore';
                                      $b_icon  = 'fa fa-unlock-alt';}
                                  $onclick_block_fun = "block_unblock_kr('" . base64_encode($kr_id) . "', '" . $b_title . "', '" . $value['kr_id_disp'] . "', '" . $value['title_of_the_content'] . "')";
                                 ?>
                                 <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_block_fun ?>">
                                 <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                 </a>
                                 

                                 <a href="javascript:void(0)" class="btn btn-primary37 mt-4" onclick="delete_kr_new('<?php echo base64_encode($kr_id)  ?>','<?php echo base64_encode($value['kr_id_disp'])  ?>','<?php echo base64_encode($value['title_of_the_content'])  ?>')">
                                 <i class="fa fa-trash-o" aria-hidden="true"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                           <?php } ?>

                     </div>
                  <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No Repositories found</strong>
                     </div>
                  <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('knowledge_repository') ?>" class="btn btn-primary36">
                  Explore Knowledge Repository
                  </a>
               </div>
               <!-- My Answered Question -->
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
                     <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myactivekr" name="search_txt" id="search_txt_myactivekr" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceKRDataAjax(1,'myactivekr','kr')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactivekr','kr')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php if(count($kr_active)>0){ ?>
              
                     <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                        <?php 
                        foreach ($kr_active as $key => $krepo) { ?>
                        <li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $krepo['title_of_the_content']; ?>
                        </li>
                      <?php  } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php foreach ($kr_active as $key => $krepo) { ?>
                        <div>
                           <div class="row">
                              <div class="col-md-12">
                                 <div id="content-id2" class="content2 light">
                                    <?php 
                                    if( array_key_exists('like_info', $krepo) ){ ?>
                                       <p>Your liked this repository on <?php echo $krepo['like_info'] ?></p>
                                       <hr>
                                    <?php } 
                                    if(array_key_exists('downloads',$krepo)){?>
                                     <div class="row">
                                    <div class="col-md-3">
                                       <div class="boxNewSection">
                                          <span><?php echo $krepo['type'] ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                       <div class="boxNewSection">
                                          <span>Downloaded On <?php echo date('d-m-Y',strtotime($krepo['downloads'][0]['createdAt']))  ?></span>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                       <div class="boxNewSection">
                                        <a class="text-white" onclick="open_download_modal('<?php echo $krepo['kr_id'] ?>')" href="javascript:void(0)">Download Again</a>
                                       </div>
                                    </div>
                                     </div>
                                     <?php   }?>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <a target="_blank" href="<?php echo base_url('knowledge_repository/details/').base64_encode($krepo['kr_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the page
                                 </a>
                              </div>
                           </div>
                        </div>

                        <?php  } ?>
                     </div>
                      <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No Repositories found</strong>
                     </div>
                        <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('knowledge_repository') ?>" class="btn btn-primary36">
                  Explore Knowledge Repository
                  </a>
               </div>
            </div>
         </div>

