<style>
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	.MyTeamsListingTab ul li:before { display:none; }
</style>
 <style>
        .boxNewSection {
            padding: 25px 15px;
            color: #fff;
            background: #333;
            text-align: center;
            box-shadow: .3s ease, border .3s;
            box-shadow: 0 0 5px 0 rgba(46, 61, 73, .15);
        }

        .boxNewSection h3 {
            color: #FFF;
            font-size: 28px;
            margin: 0;
            padding: 0;
        }

        .boxNewSection span {
            color: #FFF;
            font-size: 15px;
            font-weight: bold;
            margin: 0;
            padding: 0;
        }

        .bgColorSection {
            background: #c80032;
            padding: 15px;
            color: #fff;
            margin-top: 25px;
            text-align: center;

        }

        .formInfoNew {
            background: #fff;
            position: relative;
            box-shadow: .3s ease, border .3s;
            box-shadow: 0 0 5px 0 rgba(46, 61, 73, .15);
            padding: 25px;
            width: 100%;
            margin: 0 auto;
            z-index: 99;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .tab-pane {
            padding: 0 !important;
        }

        #verticalTabs.nav-tabs,
        #verticalTabsAnswers.nav-tabs,
        #verticalTabsBlogs.nav-tabs,
        #verticalTabsKnowRep.nav-tabs {
            border: none;
            padding: 0;
        }

        #verticalTabs .nav-item,
        #verticalTabsAnswers .nav-item,
        #verticalTabsBlogs .nav-item,
        #verticalTabsKnowRep .nav-item {
            width: 100%;
            font-size: 15px;
        }


        #verticalTabs.nav-tabs .nav-link.active,
        #verticalTabsAnswers.nav-tabs .nav-link.active,
        #verticalTabsBlogs.nav-tabs .nav-link.active,
        #verticalTabsKnowRep.nav-tabs .nav-link.active {
            background: #c80032;
            color: #FFF;
            border: solid 1px #c80032;
            border-radius: 0;
        }

        #verticalTabs .nav-link,
        #verticalTabsAnswers .nav-link,
        #verticalTabsBlogs .nav-link,
        #verticalTabsKnowRep .nav-link {
            display: block;
            padding: 10px;
            border: 1px solid #CCC;
            margin-bottom: 5px;
            border-radius: 0;
            line-height: initial;
        }

        #verticalTabs.nav-tabs .nav-link:hover,
        #verticalTabsAnswers.nav-tabs .nav-link:hover,
        #verticalTabsBlogs.nav-tabs .nav-link:hover,
        #verticalTabsKnowRep.nav-tabs .nav-link:hover {
            border: solid 1px #c80032;
            border-radius: 0;
        }


        .roundImgUser {
            border-radius: 50%;
            width: 120px;
            height: 120px;
            margin: 0 auto;
            /* justify-content: center; */
            display: block;
        }

        .myDocImgUser {

            width: 120px;
            height: 120px;
            margin: 5px 15px 15px 15px;
            /* justify-content: center; */
            display: block;
        }

        .myDoc .col-md-2 {
            border-right: solid 1px #f8f8f8;
        }

        .blogsUser h6,
        .myDoc h6 {
            font-size: 19px;
            padding: 0;
            margin: 0;
        }

        .blogsUser h6 a,
        .myDoc h6 a {
            text-decoration: none;
            color: #000;
            font-weight: normal;
        }

        .blogsUser h6 a:hover,
        .myDoc h6 a:hover {
            text-decoration: none;
            color: #c80032;
        }

        .blogsUser h6 span,
        .myDoc h6 span {
            font-size: 19px;
            float: right;
        }

        .blogsUser {
            border-top: solid 1px #f8f8f8;
            border-bottom: solid 1px #f8f8f8;
            padding-top: 15px;
            margin: 0 0;
            padding-bottom: 15px;
        }

        .myDoc {
            border-top: solid 1px #f8f8f8;
            border-bottom: solid 1px #f8f8f8;
            margin: 0;
            padding: 0;
        }

        .boldText {
            text-align: center;
            margin: 5px 0 0 0;
            font-weight: bold;
        }

        .linkAll a {
            color: #c80032;
            text-decoration: none;
            font-weight: bold;
        }

        .linkAll a:hover {
            color: #333;
            text-decoration: none;
        }

        .content {
            overflow: auto;
            position: relative;
            height: 190px;
        }

        .mCS-minimal.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
	background-color: #fff;
	background-color: rgba(255,255,255,0.8);
	filter: "alpha(opacity=20)";
	-ms-filter: "alpha(opacity=20)";
}

.mCS-minimal.mCSB_scrollTools .mCSB_draggerRail, .mCS-minimal-dark.mCSB_scrollTools .mCSB_draggerRail {
	background-color: #000;
}
    </style>


<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Space</h1>        
	</div>
</div>

<section id="registration-form" class="inner-page">
        <div class="container">
            <!-- Questions Section Start -->
            <div class="row">
                
                <div class="col-md-12">
                    <div class="title-bar pt-0" style="display: block;">
                        <h2>Questions/Answers Forum</h2>
                        <div class="heading-border"></div>
                    </div>
                </div>
                <div class="formInfoNew">
                    <?php if ($fqa_forum_data) { ?>
                    <div class="row">
                        <div class="col-md-5">
                            <h5>All Questions</h5>
                            <div id="content-1" class="content mCustomScrollbar">

                                <ul class="nav nav-tabs" id="verticalTabs" role="tablist">
                                	<?php foreach ($fqa_forum_data as $key => $question) { ?>
                        			 <li class="nav-item">
                                        <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="questions-tab" data-toggle="tab"
                                            href="#questions<?php echo $key+1 ?>" role="tab" aria-selected="true"><i
                                                class="fa fa-caret-right" aria-hidden="true"></i><?php echo $question['question'] ?></a>
                                    </li>
                                    <?php }
                                	 ?>
                                   
                                    
                                </ul>

                            </div>
                        </div>
                    	<div class="col-md-7 pl-0 ">
                          
                            <div class="tab-content mt-4" id="myTabContent">
                            	<?php foreach ($fqa_forum_data as $key => $question) {
                    				$getTotalLikes = $this->master_model->getRecordCount('arai_qa_forum_likes', array('q_id' => $question['q_id']), 'like_id');

                    				$getTotalComments = $this->master_model->getRecordCount('arai_qa_forum_comments', array('q_id' => $question['q_id']), 'comment_id');

                    				 ?>
                                	<div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="questions<?php echo $key+1 ?>" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalLikes; ?></h3>
                                                <span>Likes</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalComments; ?></h3>
                                                <span>Answers</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 d-none">
                                            <div class="boxNewSection">
                                                <h3>15</h3>
                                                <span>Shares</span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="bgColorSection">
                                    <a style="color: #fff" href="<?php echo base_url('questions_answers_forum/QaDetails/').base64_encode($question['q_id']);  ?>">Take me to the question</a>	
                                    </div>
                                </div>
                                 <?php }
                            	 ?>
                                
                                
                            </div>
                        	
                        	
                        	</div>
                    </div>
                <?php } ?>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="title-bar pt-0" style="display: block;">
                                <h2>Answers</h2>
                                <div class="heading-border"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h5>All Answers</h5>
                            </div>
                            <div class="col-md-5">

                                <div id="content-2" class="content mCustomScrollbar">

                                    <ul class="nav nav-tabs" id="verticalTabsAnswers" role="tablist">
                                        <?php foreach ($fqa_forum_answers_data as $key => $ans) { ?>
                                            
                                        <li class="nav-item">
                                            <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="answers-tab" data-toggle="tab"
                                                href="#answers<?php echo $key+1 ?>" role="tab" aria-selected="true"><i
                                                    class="fa fa-caret-right" aria-hidden="true"></i><?php echo $ans['comment']; ?></a>
                                        </li>
                                        <?php } ?>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-7 pl-0">

                                <div class="tab-content" id="myTabContent">
                                    <?php foreach ($fqa_forum_answers_data as $key => $ans) { 

                                    $getTotalLikes_ans = $this->master_model->getRecords('arai_qa_forum_comments', array('comment_id' => $ans['comment_id']), 'total_likes');

                                    $getTotalReply = $this->master_model->getRecordCount('arai_qa_forum_comments', array('parent_comment_id' => $ans['comment_id']), 'comment_id');
                                    

                                    //$getTotalComments = $this->master_model->getRecordCount('arai_qa_forum_comments', array('q_id' => $ans['q_id']), 'comment_id');

                                    ?>
                                    <div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="answers<?php echo $key+1 ?>" role="tabpanel">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="boxNewSection">
                                                    <h3><?php echo $getTotalLikes_ans[0]['total_likes']; ?></h3>
                                                    <span>Likes</span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="boxNewSection">
                                                    <h3><?php echo $getTotalReply; ?></h3>
                                                    <span>Replies</span>
                                                </div>
                                            </div>
                                            <div class="col-md-4 d-none">
                                                <div class="boxNewSection">
                                                    <h3>15</h3>
                                                    <span>Shares</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="bgColorSection">
                                        <a style="color: #fff" href="<?php echo base_url('questions_answers_forum/QaDetails/').base64_encode($ans['q_id']);  ?>">Take me to the Answer</a>    
                                        </div>
                                        </div>

                                    <?php } ?>
                                   
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <!-- Questions Section End -->
            <!-- Blog Section Start -->
            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="title-bar pt-0" style="display: block;">
                        <h2>Blogs</h2>
                        <div class="heading-border"></div>
                    </div>
                </div>
                <div class="formInfoNew">
                    <?php if(count($blogs)) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>All Blogs</h5>
                        </div>
                        <div class="col-md-5">
                            <div id="content-3" class="content mCustomScrollbar">

                                <ul class="nav nav-tabs" id="verticalTabsBlogs" role="tablist">
                                    <?php foreach ($blogs as $key => $blog) { ?>
                                        
                                    <li class="nav-item">
                                        <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="Blogs-tab" data-toggle="tab" href="#Blogs<?php echo $key+1 ?>"
                                            role="tab" aria-selected="true"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i> <?php echo $blog['blog_title'] ?></a>
                                    </li>
                                    <?php } ?>
                                   
                                </ul>

                            </div>
                        </div>
                        <div class="col-md-7 pl-0">
                            <div class="tab-content" id="myTabContent">
                                <?php foreach ($blogs as $key => $blog) { 
                                $getTotalLikes_blog    = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $blog['blog_id']), 'like_id');

                                $getTotalComments_blog    = $this->master_model->getRecordCount('arai_blog_comments', array('blog_id' => $blog['blog_id']), 'comment_id');

                                ?>
                                
                                <div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="Blogs<?php echo $key+1 ?>" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalLikes_blog ?></h3>
                                                <span>Likes</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalComments_blog ?></h3>
                                                <span>Comments</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 d-none">
                                            <div class="boxNewSection">
                                                <h3>15</h3>
                                                <span>Shares</span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="bgColorSection"> <a style="color: #fff" href="<?php echo base_url('blogs_technology_wall/blogDetails//').base64_encode($blog['blog_id']);  ?>">Take me to the Blog</a> </div>
                                </div>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                <?php } ?>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h5>Blogs you are active on</h5>
                        </div>
                    </div>
                <?php if (count($most_commented_blog)>0) { ?>
                    <div class="row d-flex align-items-center mt-2 blogsUser">
                        <div class="col-md-2">
                            <img src="<?php  echo base_url('uploads/blog_banner/').$most_commented_blog[0]['blog_banner'] ?>"  class="roundImgUser">
                        </div>
                        <div class="col-md-10">
                            <h6><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($most_commented_blog[0]['blog_id']) ?>"><?php echo $most_commented_blog[0]['blog_title'] ?> </a> <span><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($most_commented_blog[0]['blog_id']) ?>"><i
                                            class="fa fa-angle-double-right" aria-hidden="true"></i></a></span></h6>
                            <strong class="d-none"><i class="fa fa-user-circle" aria-hidden="true"></i> Admin</strong>
                            <strong ><i class="fa fa-envelope" aria-hidden="true"></i> Comments: <?php echo $most_commented_blog[0]['highest_comments']; ?></strong>
                        </div>
                    </div>
                <?php } ?>
                <?php if (count($most_likeed_blog)>0) { ?>
                    
                    <div class="row d-flex align-items-center mt-2 blogsUser">
                        <div class="col-md-2">
                            <img src="<?php echo base_url('uploads/blog_banner/').$most_likeed_blog[0]['blog_banner'] ?>"  class="roundImgUser">
                        </div>
                        <div class="col-md-10">
                            <h6><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($most_likeed_blog[0]['blog_id']) ?>"> <?php echo $most_likeed_blog[0]['blog_title'] ?></a> <span><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($most_likeed_blog[0]['blog_id']) ?>"><i
                                            class="fa fa-angle-double-right" aria-hidden="true"></i></a></span></h6>
                            <strong class="d-none"><i class="fa fa-user-circle" aria-hidden="true"></i> Admin</strong>
                            <strong ><i class="fa fa-envelope" aria-hidden="true"></i> Likes: <?php echo $most_likeed_blog[0]['highest_likes']; ?></strong>
                        </div>
                    </div>

                 <?php } ?>



                </div>

            </div>
            <!-- Blog Section End -->

             <?php if(count($knowledge_repository)) { ?>
            <!-- Knowledge Repository Section Start -->
            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="title-bar pt-0" style="display: block;">
                        <h2>Knowledge Repository</h2>
                        <div class="heading-border"></div>
                    </div>
                </div>
                <div class="formInfoNew">

                    <div class="row">
                        <div class="col-md-12">
                            <h5>My Uploads</h5>
                        </div>
                        <div class="col-md-5">

                            <div id="content-4" class="content mCustomScrollbar">

                                <ul class="nav nav-tabs" id="verticalTabsKnowRep" role="tablist">
                                      <?php foreach ($knowledge_repository as $key => $kr) {

                                        ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="KnowRep-tab" data-toggle="tab" href="#KnowRep<?php echo $key+1 ?>"
                                            role="tab" aria-selected="true"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i><?php echo $kr['title_of_the_content'] ?> </a>
                                    </li>
                                <?php } ?>
                                    
                                </ul>

                            </div>


                        </div>
                        <div class="col-md-7 pl-0">
                            <div class="tab-content" id="myTabContent">
                              <?php foreach ($knowledge_repository as $key => $kr) {

                                $getTotalDownl        = $this->master_model->getRecordCount('arai_knowledge_download_log', array('kr_id' => $kr['kr_id']), 'd_id');
                                ?>  
                                <div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="KnowRep<?php echo $key+1 ?>" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalDownl ?></h3>
                                                <span>Downloads</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bgColorSection">
                                         <a style="color: #fff" href="<?php echo base_url('knowledge_repository/details//').base64_encode($kr['kr_id']);  ?>">Take me to the page</a>
                                    </div>
                                </div>
                            <?php } ?>
                              
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h5>My Downloads</h5>
                        </div>
                    </div>

                    <div class="row d-flex align-items-center mt-2 myDoc">
                        <div class="col-md-2">
                            <p class="boldText">Today</p>
                            <img src="img/fileImg.jpg" alt="abdul-kalam" class="myDocImgUser">
                        </div>
                        <div class="col-md-10 linkAll">
                            <h6><a href="#"> The basic nav from above and adds the?</a> <span><a href="#"><i
                                            class="fa fa-times" aria-hidden="true"></i></a></span></h6>
                            <p>Select your favorite social network and share our icons with your contacts or
                                friends, if
                                you do not have these social
                                networks copy the link and paste it in the one you use. For more information</p>
                            <a href="#">Show in Folder</a>

                        </div>
                    </div>
                    <div class="row d-flex align-items-center mt-2 myDoc">
                        <div class="col-md-2">
                            <p class="boldText">Yesterday</p>
                            <img src="img/fileImg.jpg" alt="abdul-kalam" class="myDocImgUser">
                        </div>
                        <div class="col-md-10 linkAll">
                            <h6><a href="#"> The basic nav from above and adds the?</a> <span><a href="#"><i
                                            class="fa fa-times" aria-hidden="true"></i></a></span></h6>
                            <p>Select your favorite social network and share our icons with your contacts or
                                friends, if
                                you do not have these social
                                networks copy the link and paste it in the one you use. For more information</p>
                            <a href="#">Show in Folder</a>
                        </div>
                    </div>



                </div>

            </div>
        <?php } ?>
            <!-- Knowledge Repository Section End -->

        <!-- Resource sharing Section Start -->
          <?php if(count($resource_sharing)) { ?>
            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="title-bar pt-0" style="display: block;">
                        <h2>Resource Sharing</h2>
                        <div class="heading-border"></div>
                    </div>
                </div>
                <div class="formInfoNew">

                    <div class="row">
                        <div class="col-md-12">
                            <h5>My Resources</h5>
                        </div>
                        <div class="col-md-5">

                            <div id="content-4" class="content mCustomScrollbar">

                                <ul class="nav nav-tabs" id="verticalTabsKnowRep" role="tablist">
                                      <?php foreach ($resource_sharing as $key => $res) {

                                        ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="Resource-tab" data-toggle="tab" href="#res_sharing<?php echo $key+1 ?>"
                                            role="tab" aria-selected="true"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i><?php echo $res['resouce_name'] ?> </a>
                                    </li>
                                <?php } ?>
                                    
                                </ul>

                            </div>


                        </div>
                        <div class="col-md-7 pl-0">
                            <div class="tab-content" id="myTabContent">
                              <?php foreach ($resource_sharing as $key => $res) {

                                $getTotalLikes_res        = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $res['r_id']), 'like_id');

                                $getTotalRequest_res   = $this->master_model->getRecordCount('arai_resource_sharing_connect_request', array('r_id' => $res['r_id']), 'connect_id');
                                ?>  
                                    <div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="res_sharing<?php echo $key+1 ?>" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalLikes_res ?></h3>
                                                <span>Likes</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalRequest_res ?></h3>
                                                <span>Connect Requests</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bgColorSection">
                                         <a style="color: #fff" href="<?php echo base_url('resource_sharing/details/').base64_encode($res['r_id']);  ?>">Take me to the page</a>
                                    </div>
                                </div>
                            <?php } ?>
                              
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        <?php } ?>
        <!-- Resource sharing Section End -->


        <!-- Technolgoy transfer Section Start -->
          <?php if(count($technology_transfer)) { ?>
            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="title-bar pt-0" style="display: block;">
                        <h2>Technology Transfers</h2>
                        <div class="heading-border"></div>
                    </div>
                </div>
                <div class="formInfoNew">

                    <div class="row">
                        <div class="col-md-12">
                            <h5>My Technology Transfers</h5>
                        </div>
                        <div class="col-md-5">

                            <div id="content-4" class="content mCustomScrollbar">

                                <ul class="nav nav-tabs" id="verticalTabsKnowRep" role="tablist">
                                      <?php foreach ($technology_transfer as $key => $tech) {

                                        ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php if($key==0){echo 'active';} ?>" id="tech-tab" data-toggle="tab" href="#tech_trans<?php echo $key+1 ?>"
                                            role="tab" aria-selected="true"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i><?php echo $tech['technology_title'] ?> </a>
                                    </li>
                                <?php } ?>
                                    
                                </ul>

                            </div>
                        </div>
                        <div class="col-md-7 pl-0">
                            <div class="tab-content" id="myTabContent">
                              <?php foreach ($technology_transfer as $key => $tech) {

                             

                                $getTotalRequest_tech   = $this->master_model->getRecordCount('arai_technology_transfer_connect_request', array('tech_id' => $tech['tech_id']), 'connect_id');
                                ?>  
                                    <div class="tab-pane fade show <?php if($key==0){echo 'active';} ?>" id="tech_trans<?php echo $key+1 ?>" role="tabpanel">
                                    <div class="row">
                                       
                                        <div class="col-md-4">
                                            <div class="boxNewSection">
                                                <h3><?php echo $getTotalRequest_tech ?></h3>
                                                <span>Connect Requests</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bgColorSection">
                                         <a style="color: #fff" href="<?php echo base_url('technology_transfer/details/').base64_encode($tech['tech_id']);  ?>">Take me to the page</a>
                                    </div>
                                </div>
                            <?php } ?>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- Technolgoy transfer Section End -->

        </div>
    </section>


<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">

				<div class="col-md-8">

			</div> 

				<h5 class="modal-title" id="BlogReportpopupLabel">Technology transfer delete</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				
					<div class="form-group mt-4">
						
						<label for="popupKrDeleteComment" class="">Reason <em style="color: red">*</em></label>
						<textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
						<span id="popupKrDeleteComment_err" class='error'></span>
					</div>
				</div>	
				<input type="hidden" id="popupBlogIdHidden" name="">
				<div class="text-center pb-3">
					<button type="button" class="btn btn-primary" onclick="submit_delete_request()">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
      	<div>
        <h5  id="BlogLikesLabel"></h5>
        </div>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="col-md-12">
        <h5 id="IdDisp"></h5>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>


        (function ($) {
            $(window).on("load", function () {

                $("#content-1, #content-2, #content-3, #content-4").mCustomScrollbar({
                    theme: "minimal"
                });

            });
        })(jQuery);
 

	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
 
	function block_unblock_tt(id, type)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to "+type+" the selected content?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("technology_transfer/block_unblock_tt"); ?>',
					data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Resource sharing successfully "+type+"ed.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}



  


	function delete_tt(kr_id)
	{
				// $("#popupBlogId").html(kr_disp_id);
				$("#popupBlogIdHidden").val(kr_id);
				// $("#popupBlogTitle").html(kr_title);
				$("#popupKrDeleteComment").val('');
				$("#popupKrDeleteComment_err").html('');
				$("#BlogReportpopup").modal('show');
				$("#popupKrDeleteComment").focus();
			
	}

	function check_report_validation()
	{
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		if(popupKrDeleteComment.trim() == "") { $("#popupKrDeleteComment_err").html('Please enter the delete reason'); $("#popupKrDeleteComment").focus(); }
		else { $("#popupKrDeleteComment_err").html(''); }
	}

	function submit_delete_request(kr_id)
	{	
		check_report_validation();
		
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		var id = $("#popupBlogIdHidden").val();
		if(popupKrDeleteComment.trim() != "")		
		{
			$("#popupKrDeleteComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to delete the selected conent?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("technology_transfer/delete_tt"); ?>',
						data:{ 'id':id, 'popupKrDeleteComment':popupKrDeleteComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$("#BlogReportpopup").modal('hide');
							$(".token").val(data.token);
						
							$('#blog-list').DataTable().ajax.reload();	
							swal(
							{
								title: 'Success!',
								text: "Content successfully deleted.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}
	
	function delete_tt_old(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected conent?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("technology_transfer/delete_tt"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Resource sharing successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function show_connect_requests(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("technology_transfer/show_connect_requests_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);
					$('#IdDisp').html(data.id_disp)				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function show_resource_likes(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("resource_sharing/show_resource_likes_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>