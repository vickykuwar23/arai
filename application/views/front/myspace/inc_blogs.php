
           <div class="parentHorizontalTab">
               <ul class="resp-tabs-list hor_1">
                  <li id="inner_tab_myblogs">My Blogs</li>
                  <li id="inner_tab_myactiveblogs">Blogs I am Active on</li>
               </ul>
               <div class="resp-tabs-container hor_1">
                  <div>
                     <!--vertical Tabs-->
                     <div class="ChildVerticalTab_1">
                  
                       <div class="filterBox">
                        <section class="search-sec">
                           <div id="filterData" name="filterData">
                              <div class="container">   
                                 <div class="row">
                                    <div class="col-lg-12">
                                       <div class="row d-flex justify-content-center search_Box">                       
                                          <div class="col-md-6">
                                             <input type="text" class="form-control search-slt search_myblogs" name="search_txt" id="search_txt_myblogs" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                             <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceDataAjax(1,'myblogs','blog')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                          </div>
                                          <div class="clearAll">
                                             <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myblogs','blog')">Clear All</a>
                                          </div>
                                          <div class="col-md-12"></div>                      
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                     </div>
                        <?php  if(count($blogs)>0){ ?>
                        <ul class="resp-tabs-list ver_1 content light ul_of_blogs" id="content-id1">
                           <?php foreach ($blogs as $key => $blog) { ?>
                           <li id="inner_li_<?php echo $blog['blog_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $blog['blog_title']; ?>
                           </li>
                           <?php } ?>
                        </ul>
                        <div class="resp-tabs-container ver_1">
                           <?php 
                           foreach ($blogs as $key => $blog) { 
                           $getTotalLikes_blog    = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $blog['blog_id']), 'like_id');

                           $getTotalComments_blog    = $this->master_model->getRecordCount('arai_blog_comments', array('blog_id' => $blog['blog_id'],'deleted_on'=>NULL), 'comment_id');

                           $getTotalReported     = $this->master_model->getRecordCount('arai_blogs_reported', array('blog_id' => $blog['blog_id']), 'reported_id');
                           ?>
                           <div>
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="boxNewSection">
                                       <h3><a onclick="show_blog_likes('<?php echo base64_encode($blog['blog_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalLikes_blog ?></a></h3>
                                       <span>Likes</span>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="boxNewSection">
                                       <h3>
                                          <a target="_blank" href="<?php echo site_url('blogs_technology_wall/blogDetails/' . base64_encode($blog['blog_id']) . '/1') ?>"><?php echo $getTotalComments_blog ?></a>
                                          </h3>
                                       <span>Comments</span>
                                    </div>
                                 </div>

                                 <div class="col-md-3">
                                    <div class="boxNewSection">
                                       <h3><a onclick="show_blog_reported('<?php echo base64_encode($blog['blog_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalReported ?></a></h3>
                                       <span>Reported</span>
                                    </div>
                                 </div>

                                 <div class="col-md-3">
                                    <div class="boxNewSection">
                                    <span class="last-text">
                                    <?php if ($blog['admin_status']=='0'){  echo '<span title="Pending"><i class="fa fa-flag fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($blog['admin_status']=='1') { echo '<span title="Active"><i class="fa fa-check-circle fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($blog['admin_status']=='2') { echo '<span title="Rejected"><i class="fa fa-ban fa-2x" aria-hidden="true"  ></i></span>';
                                     } ?>
                                    </span>
                                 </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-9">
                                    <a target="_blank" href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($blog['blog_id']);  ?>" class="btn btn-primary35 mt-4">
                                    Take Me to the blog
                                    </a>
                                 </div>
                                 <div class="col-md-3 text-center">
                                    <a href="<?php echo base_url('blogs_technology_wall/add_blog//').base64_encode($blog['blog_id']);  ?>" class="btn btn-primary37 mt-4">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
                                    <?php 
                                    $blog_id=$blog['blog_id'];
                                     if ($blog['is_block'] == '0') {
                                         $b_title = 'Block';
                                         $b_icon  = 'fa fa-ban';} else if ($blog['is_block'] == '1') {
                                         $b_title = 'Unblock';
                                         $b_icon  = 'fa fa-unlock-alt';}
                                     $onclick_block_fun = "block_unblock_blog('" . base64_encode($blog_id) . "', '" . $b_title . "', '" . $blog['blog_id_disp'] . "', '" . $blog['blog_title'] . "')";
                                    ?>
                                    <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_block_fun ?>">
                                    <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                    </a>
                                    
                                    <a href="javascript:void(0)" class="btn btn-primary37 mt-4" onclick="delete_blog('<?php echo base64_encode($blog_id)  ?>')">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                              <?php } ?>

                        </div>
                     <?php }else{ ?>
                        <div class="text-center p-3">
                           <strong>No blogs found</strong>
                        </div>
                     <?php } ?>
                     </div>
                     <p>&nbsp</p>
                     <a target="_blank" href="<?php echo base_url('blogs_technology_wall') ?>" class="btn btn-primary36">
                     Explore Blogs
                     </a>
                  </div>
                  <!-- My Answered Question -->
                  <div>
                     <!--vertical Tabs-->
                     <div class="ChildVerticalTab_1">
                        <div class="filterBox">
                        <section class="search-sec">
                           <div id="filterData" name="filterData">
                              <div class="container">   
                                 <div class="row">
                                    <div class="col-lg-12">
                                       <div class="row d-flex justify-content-center search_Box">                       
                                          <div class="col-md-6">
                                             <input type="text" class="form-control search-slt search_myactiveblogs" name="search_txt" id="search_txt_myactiveblogs" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                             <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceDataAjax(1,'myactiveblogs','blog')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                          </div>
                                          <div class="clearAll">
                                             <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactiveblogs','blog')">Clear All</a>
                                          </div>
                                          <div class="col-md-12"></div>                      
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                     </div>
                        <?php if(count($blogs_active_comment)>0){ ?>
                 
                        <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                           <?php 
                           foreach ($blogs_active_comment as $key => $comment_active) { ?>
                           <li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $comment_active['blog_title']; ?>
                           </li>
                         <?php  } ?>
                        </ul>
                        <div class="resp-tabs-container ver_1">
                           <?php foreach ($blogs_active_comment as $key => $comment_active) { ?>
                           <div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div id="content-id2" class="content2 light">
                                       <?php 
                                       if( array_key_exists('like_info', $comment_active) ){ ?>
                                          <p>Your liked this blog on <?php echo $comment_active['like_info'] ?></p>
                                          <hr>
                                       <?php } 
                                       if(array_key_exists('comments',$comment_active)){
                                       foreach ($comment_active['comments'] as $key => $comment) { ?>
                                       <p>You commented on this blog as below</p>
                                       <p class="mt-2 mb-2"><?php echo $comment['comment'] ?>
                                       </p>
                                       <em><i class="fa fa-calendar" aria-hidden="true"></i>
                                       <?php echo $comment['created_on'] ?></em>
                                       <hr>
                                        <?php  } }?>

                                        <?php  
                                    if(array_key_exists('reply',$comment_active)){
                                    if(count($comment_active['reply'])>0){  
                                    foreach ($comment_active['reply'] as $key => $reply_arr) { 
                                       foreach($reply_arr as $rep){
                                       ?>
                                    
                                    <p>You got reply on your comment as below</p>
                                    <p class="mt-2 mb-2"><?php echo $rep['comment'] ?>
                                    </p>
                                    <em><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <?php echo $rep['created_on'] ?></em>
                                    <hr>

                                     <?php  } } } }?>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <a target="_blank" href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($comment_active['blog_id']);  ?>" class="btn btn-primary35 mt-4">
                                    Take Me to the blog
                                    </a>
                                 </div>
                              </div>
                           </div>

                           <?php  } ?>
                        </div>
                         <?php }else{ ?>
                        <div class="text-center p-3">
                           <strong>No blogs found</strong>
                        </div>
                           <?php } ?>
                     </div>
                     <p>&nbsp</p>
                     <a target="_blank" href="<?php echo base_url('blogs_technology_wall') ?>" class="btn btn-primary36">
                     Explore Blogs
                     </a>
                  </div>
               </div>
            </div>

