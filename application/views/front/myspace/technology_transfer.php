<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="MySpaceOuter"></div>
         </div>
         
      </div>
   </div>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogReportpopupLabel">Technology transfer delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
            
            <!-- <div class="bgNewBox65">
            <p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
            <p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
            </div> -->
               
               <div class="form-group mt-4">
                  
                  <label for="popupKrDeleteComment" class="">Reason <em style="color: red">*</em></label>
                  <textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
                  <span id="popupKrDeleteComment_err" class='error'></span>
               </div>
            </div>   
            <input type="hidden" id="popupBlogIdHidden" name="">
            <div class="text-center pb-3">
               <button type="button" class="btn btn-primary" onclick="submit_delete_request()">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>
  function show_connect_requests(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("technology_transfer/show_connect_requests_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);
               $('#IdDisp').html(data.id_disp)           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }

     function close_tt(id, type)
    {
        swal(
        {  
            title:"Confirm?",
            text: "Are you sure you want to close the selected content?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        { 
            if (result.value) 
            {
                var cs_t =  $('.token').val();
                $.ajax(
                {
                    type:'POST',
                    url: '<?php echo site_url("technology_transfer/close_tt"); ?>',
                    data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
                    dataType:"JSON",
                    success:function(data)
                    {
                        $(".token").val(data.token);
                        //$('#blog-list').DataTable().ajax.reload();  
                        var type_new  = type;
                        if (type=='close') {type_new='clos';}
                        swal(
                        {
                            title: 'Success!',
                            text: "Resource sharing successfully "+type_new+"ed.",
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        var dec_t_id = atob(id);
                        getMySpaceTTDataAjax(0,'mytt','tt',dec_t_id);
                    }
                });
            } 
        });
    }

   function block_unblock_tt(id, type)
   {
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to "+type+" the selected content?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("technology_transfer/block_unblock_tt"); ?>',
               data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
               dataType:"JSON",
               success:function(data)
               {
                  $(".token").val(data.token);
                  swal(
                  {
                     title: 'Success!',
                     text: "Content successfully "+type+"ed.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });
                  var dec_t_id = atob(id);
                  getMySpaceTTDataAjax(0,'mytt','tt',dec_t_id);
               }
            });
         } 
      });
   }

   function delete_tt(kr_id)
   {
            // $("#popupBlogId").html(kr_disp_id);
            $("#popupBlogIdHidden").val(kr_id);
            // $("#popupBlogTitle").html(kr_title);
            $("#popupKrDeleteComment").val('');
            $("#popupKrDeleteComment_err").html('');
            $("#BlogReportpopup").modal('show');
            $("#popupKrDeleteComment").focus();
         
   }

   function check_report_validation()
   {
      var popupKrDeleteComment = $("#popupKrDeleteComment").val();
      if(popupKrDeleteComment.trim() == "") { $("#popupKrDeleteComment_err").html('Please enter the delete reason'); $("#popupKrDeleteComment").focus(); }
      else { $("#popupKrDeleteComment_err").html(''); }
   }

   function submit_delete_request(kr_id)
   {  
      check_report_validation();
      
      var popupKrDeleteComment = $("#popupKrDeleteComment").val();
      var id = $("#popupBlogIdHidden").val();
      if(popupKrDeleteComment.trim() != "")     
      {
         $("#popupKrDeleteComment_err").html('');
         swal(
         {  
            title:"Confirm?",
            text: "Are you sure you want to delete the selected conent?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
         }).then(function (result) 
         { 
            if (result.value) 
            {
               var cs_t =  $('.token').val();
               $.ajax(
               {
                  type:'POST',
                  url: '<?php echo site_url("technology_transfer/delete_tt"); ?>',
                  data:{ 'id':id, 'popupKrDeleteComment':popupKrDeleteComment, 'csrf_test_name':cs_t },
                  dataType:"JSON",
                  success:function(data)
                  {
                     $("#BlogReportpopup").modal('hide');
                     $(".token").val(data.token);
                      
                     swal(
                     {
                        title: 'Success!',
                        text: "Content successfully deleted.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok'
                     });
                      getMySpaceQADataAjax(0,'mytt','tt');
                     
                  }
               });
            } 
         });
      }
   }
</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceTTDataAjax(0,'mytt','tt');
      // initializeTabs()
   });


   function getMySpaceTTDataAjax(is_search=0,inner_tab,outer_tab,inner_li_id='')
   {
      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_tt_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#MySpaceOuter").html(data.myspace_tt);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()
               

               $("#inner_tab_"+outer_tab).trigger("click");
               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_qa').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,outer_tab){
      getMySpaceTTDataAjax(0,inner_tab,outer_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>