        <div class="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
               <li id="inner_tab_myqa">My Questions</li>
               <li id="inner_tab_myactiveqa">My Answered Question
            </li>
            </ul>
            <div class="resp-tabs-container hor_1">
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
               
                    <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myqa" name="search_txt" id="search_txt_myqa" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceQADataAjax(1,'myqa','qa')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myqa','qa')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php  if(count($fqa_forum_data)>0){ ?>
                     <ul class="resp-tabs-list ver_1 content light ul_of_qa" id="content-id1">
                        <?php foreach ($fqa_forum_data as $key => $question) { ?>
                        <li id="inner_li_<?php echo $question['q_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $question['question']; ?>
                        </li>
                        <?php } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php 
                        foreach ($fqa_forum_data as $key => $question) { 
                       $getTotalLikes = $this->master_model->getRecordCount('arai_qa_forum_likes', array('q_id' => $question['q_id']), 'like_id');

                        $getTotalComments = $this->master_model->getRecordCount('arai_qa_forum_comments', array('q_id' => $question['q_id']), 'comment_id');
                      
                        $getTotalReported     = $this->master_model->getRecordCount('arai_qa_forum_reported', array('q_id' => $question['q_id']), 'reported_id');

                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3><a onclick="show_question_likes('<?php echo base64_encode($question['q_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalLikes ?></a></h3>
                                    <span>Likes</span>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a target="_blank" href="<?php echo site_url('questions_answers_forum/QaDetails/' . base64_encode($question['q_id']) . '/1') ?>"><?php echo $getTotalComments ?></a>
                                       </h3>
                                    <span>Answers</span>
                                 </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3><a onclick="show_question_reported('<?php echo base64_encode($question['q_id'])  ?>')" href="javascript:void(0)"><?php echo $getTotalReported ?></a></h3>
                                    <span>Reported</span>
                                 </div>
                              </div>

                              <div class="col-md-3">
                                    <div class="boxNewSection">
                                    <span class="last-text">
                                    <?php if ($question['admin_status']=='0'){  echo '<span title="Pending"><i class="fa fa-flag fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($question['admin_status']=='1') { echo '<span title="Active"><i class="fa fa-check-circle fa-2x" aria-hidden="true"  ></i></span>';
                                     }elseif ($question['admin_status']=='2') { echo '<span title="Rejected"><i class="fa fa-ban fa-2x" aria-hidden="true"  ></i></span>';
                                     } ?>
                                    </span>
                                 </div>
                                 </div>
                                 
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('questions_answers_forum/QaDetails/').base64_encode($question['q_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take Me to the question
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('questions_answers_forum/add_qa/').base64_encode($question['q_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $question_id=$question['q_id'];
                                  if ($question['is_block'] == '0') {
                                      $b_title = 'Block';
                                      $b_icon  = 'fa fa-ban';} else if ($question['is_block'] == '1') {
                                      $b_title = 'Unblock';
                                      $b_icon  = 'fa fa-unlock-alt';}
                                  $onclick_block_fun = "block_unblock_question('" . base64_encode($question_id) . "', '" . $b_title . "', '" . $question['custum_question_id'] . "', '" . $question['question'] . "')";
                                 ?>
                                 <a href="javascript:void(0)" title="<?php echo $b_title ?>" class="btn btn-primary37 mt-4" onclick="<?php echo $onclick_block_fun ?>">
                                 <i class="<?php echo $b_icon ?>" aria-hidden="true"></i>
                                 </a>
                                 
                                 <a href="javascript:void(0)" class="btn btn-primary37 mt-4" onclick="delete_question('<?php echo base64_encode($question_id)  ?>')">
                                 <i class="fa fa-trash-o" aria-hidden="true"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                           <?php } ?>

                     </div>
                  <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No questions found</strong>
                     </div>
                  <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('questions_answers_forum') ?>" class="btn btn-primary36">
                  Explore Q&A Forum
                  </a>
               </div>
               <!-- My Answered Question -->
               <div>
                  <!--vertical Tabs-->
                  <div class="ChildVerticalTab_1">
                     <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myactiveqa" name="search_txt" id="search_txt_myactiveqa" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceQADataAjax(1,'myactiveqa','qa')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactiveqa','qa')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                     <?php if(count($qa_active)>0){ ?>
              
                     <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                        <?php 
                        foreach ($qa_active as $key => $question) { ?>
                        <li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $question['question']; ?>
                        </li>
                      <?php  } ?>
                     </ul>
                     <div class="resp-tabs-container ver_1">
                        <?php foreach ($qa_active as $key => $question) { ?>
                        <div>
                           <div class="row">
                              <div class="col-md-12">
                                 <div id="content-id2" class="content2 light">
                                    <?php 
                                    if( array_key_exists('like_info', $question) ){ ?>
                                       <p>Your liked this question on <?php echo $question['like_info'] ?></p>
                                       <hr>
                                    <?php } 
                                    if(array_key_exists('comments',$question)){
                                    foreach ($question['comments'] as $key => $comment) { ?>
                                    <p>You commented on this question as below</p>
                                    <p class="mt-2 mb-2"><?php echo $comment['comment'] ?>
                                    </p>
                                    <em><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <?php echo $comment['created_on'] ?></em>
                                    <hr>
                                     <?php  } }?>

                                  <?php  
                                    if(array_key_exists('reply',$question)){
                                    if(count($question['reply'])>0){  
                                    foreach ($question['reply'] as $key => $reply_arr) { 
                                       foreach($reply_arr as $rep){
                                       ?>
                                    
                                    <p>You got reply on your answer as below</p>
                                    <p class="mt-2 mb-2"><?php echo $rep['comment'] ?>
                                    </p>
                                    <em><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <?php echo $rep['created_on'] ?></em>
                                    <hr>

                                     <?php  } } } }?>

                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <a target="_blank" href="<?php echo base_url('questions_answers_forum/QaDetails/').base64_encode($question['q_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take me to the answer
                                 </a>
                              </div>
                           </div>
                        </div>

                        <?php  } ?>
                     </div>
                      <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No questions found</strong>
                     </div>
                        <?php } ?>
                  </div>
                  <p>&nbsp</p>
                  <a target="_blank" href="<?php echo base_url('questions_answers_forum') ?>" class="btn btn-primary36">
                  Explore Q&A Forum
                  </a>
               </div>
            </div>
         </div>

