<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <!-- BLOGS STARTS HERE -->
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="BlogOuter"></div>
         </div>
      </div>
   </div>

<div class="modal fade" id="BlogBlockpopup" tabindex="-1" role="dialog" aria-labelledby="BlogBlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogBlockpopupLabel"><b>Block Blog?</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div>
                  <label><b>Blog ID : </b><span id="popupBlogId"></span></label>
               </div>
               <div>
                  <label><b>Blog Title : </b><span id="popupBlogTitle"></span></label>
               </div>
               
               <div>
                  <label for="popupBlogBlockReason"><b>Block reason <em>*</em></b></label>
                  <textarea class="form-control" id="popupBlogBlockReason" name="popupBlogBlockReason" required onkeyup="check_block_validation()"></textarea>
                  <span id="popupBlogBlockReason_err" class='error'></span>
               </div>
            </div>   
            
            <div class="modal-footer">
               <button id="modal_submit_btn" type="button">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>
<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>



   function block_unblock_blog(id, type, blog_disp_id, blog_title)
   {
      if(type == "Block")
      {
         $("#popupBlogId").html(blog_disp_id);
         $("#popupBlogTitle").html(blog_title);
         $("#popupBlogBlockReason_err").html('');
         
         var onclick_fun = "submit_blog_block('"+id+"', '"+type+"')";
         $("#modal_submit_btn").replaceWith('<button id="modal_submit_btn" type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
         
         $("#BlogBlockpopup").modal('show');
         $("#popupBlogBlockReason").val('');
         $("#popupBlogBlockReason").focus();       
      }
      else
      {
         submit_blog_block(id, type)
      }     
   }
   
   function check_block_validation()
   {
      var popupBlogBlockReason = $("#popupBlogBlockReason").val();
      if(popupBlogBlockReason.trim() == "") { $("#popupBlogBlockReason_err").html('Please enter the block reason'); $("#popupBlogBlockReason").focus(); }
      else { $("#popupBlogBlockReason_err").html(''); }
   }
   
   function submit_blog_block(blog_id, type)
   {  
      check_block_validation();
      
      var flag = 0;
      if(type == 'Block')
      {
         var popupBlogBlockReason = $("#popupBlogBlockReason").val();
         if(popupBlogBlockReason.trim() == "")  { flag = 1;}
      }
            
      if(flag == 0)
      {
         swal(
         {  
            title:"Confirm?",
            text: "Are you sure you want to "+type+" the selected blog?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
         }).then(function (result) 
         { 
            if (result.value) 
            {
               var cs_t =  $('.token').val();
               $.ajax(
               {
                  type:'POST',
                  url: '<?php echo site_url("blogs_technology_wall/block_unblock_blog"); ?>',
                  data: {'id':blog_id, 'type':type, 'popupBlogBlockReason':popupBlogBlockReason, 'csrf_test_name':cs_t },
                  dataType:"JSON",
                  async:false,
                  success:function(data)
                  {
                     $(".token").val(data.token);
                     $("#BlogBlockpopup").modal('hide');
                     
                     //$('#blog-list').DataTable().ajax.reload();   
                     swal(
                     {
                        title: 'Success!',
                        text: "Blog successfully "+type+"ed.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok'
                     });
                     var dec_bloc_id = atob(blog_id);
                     getMySpaceDataAjax(0,'myblogs','blog',dec_bloc_id);
                     
                  }
               });
            } 
         });
      }
   }

   function delete_blog(id)
   {
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to delete the selected blog?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("blogs_technology_wall/delete_blog"); ?>',
               data:{ 'id':id, 'csrf_test_name':cs_t },
               dataType:"JSON",
               async:false,
               success:function(data)
               {
                  $(".token").val(data.token);
                  
                  //$('#blog-list').DataTable().ajax.reload();   
                  swal(
                  {
                     title: 'Success!',
                     text: "Blog successfully deleted.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });
                 getMySpaceDataAjax(0,'myblogs','blog');
               }
              
            });
         } 
      });
   }

   function show_blog_likes(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("blogs_technology_wall/show_blog_likes_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }  

   function show_blog_reported(blog_id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("blogs_technology_wall/show_blog_reported_ajax"); ?>',
         data:{ 'blog_id':blog_id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }
</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceDataAjax(0,'myblogs','blog');
      // initializeTabs()

        $('#search_txt_myblogs').keypress(function (e)
         { 
            if (e.which == 13) 
            { 
               getMySpaceDataAjax(1,'myblogs','blog')
            } 
         });

         $('#search_txt_myactiveblogs').keypress(function (e)
         { 
            if (e.which == 13) 
            { 
               getMySpaceDataAjax(1,'myactiveblogs','blog')
            } 
         });
   
   });



   //START : FEATURED & NON-FEATURED BLOG DATA
   function getMySpaceDataAjax(is_search=0,inner_tab,outer_tab,inner_li_id='')
   {
      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_blogs_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#BlogOuter").html(data.myspace_blogs);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()
               

               $("#inner_tab_"+outer_tab).trigger("click");
               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_blogs').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,outer_tab){
      getMySpaceDataAjax(0,inner_tab,outer_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>