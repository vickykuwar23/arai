<link rel="stylesheet" type="text/css" href="https://phpsamurai.esdsdev.com/arai-mockup25/css/easy-responsive-tabs.css" />
<style>

    .boxNewSection {​​​​​
    padding: 25px 15px;
    color: #fff;
    background: #333;
    text-align: center;
    min-height: 165px;   
    box-shadow: 0 0 5px 0 rgba(46, 61, 73, .15);
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
}​​​​​

   .boxNewSection h3 {
   color: #FFF;
   font-size: 28px;
   margin: 0;
   padding: 0;
   }
   .boxNewSection span {
   color: #FFF;
   font-size: 14px;
   font-weight: 400;
   margin: 0;
   padding: 0;
   }
 

   .boxNewSection span a {
	color: #FFF; 
   font-weight: bold;
}


   .boxNewSection span a:hover {
	color: #C80032;
   text-decoration: none;
}
   .boxNewSection span.last-text {
   /* padding: 15px 0 0 0 !important; */
   display: block;
   text-align: center;
   }
   .bgColorSection {
   background: #c80032;
   padding: 10px;
   color: #fff;
   text-align: center;
   }
   .formInfoNew {
   background: #fff;
   position: relative;
   box-shadow: .3s ease, border .3s;
   box-shadow: 0 0 5px 0 rgba(46, 61, 73, .15);
   padding: 25px;
   width: 100%;
   margin: 0 auto;
   z-index: 99;
   -webkit-border-radius: 5px;
   -moz-border-radius: 5px;
   border-radius: 5px;
   }
   .roundImgUser {
   border-radius: 50%;
   width: 120px;
   height: 120px;
   margin: 0 auto;
   /* justify-content: center; */
   display: block;
   }
   .myDocImgUser {
   width: 120px;
   height: 120px;
   margin: 5px 15px 15px 15px;
   /* justify-content: center; */
   display: block;
   }
   .myDoc .col-md-2 {
   border-right: solid 1px #f8f8f8;
   }
   .blogsUser h6,
   .myDoc h6 {
   font-size: 19px;
   padding: 0;
   margin: 0;
   }
   .blogsUser h6 a,
   .myDoc h6 a {
   text-decoration: none;
   color: #000;
   font-weight: normal;
   }
   .blogsUser h6 a:hover,
   .myDoc h6 a:hover {
   text-decoration: none;
   color: #c80032;
   }
   .blogsUser h6 span,
   .myDoc h6 span {
   font-size: 19px;
   float: right;
   }
   .blogsUser {
   border-top: solid 1px #f8f8f8;
   border-bottom: solid 1px #f8f8f8;
   padding-top: 15px;
   margin: 0 0;
   padding-bottom: 15px;
   }
   .myDoc {
   border-top: solid 1px #f8f8f8;
   border-bottom: solid 1px #f8f8f8;
   margin: 0;
   padding: 0;
   }
   .boldText {
   text-align: center;
   margin: 5px 0 0 0;
   font-weight: bold;
   }
   .linkAll a {
   color: #b49aa1;
   text-decoration: none;
   font-weight: bold;
   }
   .linkAll a:hover {
   color: #333;
   text-decoration: none;
   }
   .content {
   overflow: auto;
   position: relative;
   height: 300px;
   }
   .content2 {
   overflow: auto;
   position: relative;
   height: 270px;
   }
   .mCSB_scrollTools {
   position: absolute;
   width: 16px;
   height: auto;
   left: auto;
   top: 0;
   right: 8px;
   bottom: 0;
   }
   .btn-primary35 {
   color: #fff;
   background-color: #c80032;
   border-color: #c80032;
   text-align: center;
   display: block;
   padding: 10px;
   }
   .btn-primary35:hover {
   color: #fff;
   background-color: #333;
   border-color: #333;
   }
   .btn-primary36 {
   color: #fff;
   background-color: #c80032;
   border-color: #c80032;
   text-align: center;
   padding: 10px;
   margin: 0 auto;
   display: table;
   }
   .btn-primary36:hover {
   color: #fff;
   background-color: #333;
   border-color: #333;
   }
   .ChildVerticalTab_1 .img-fluid {
   max-width: 100%;
   height: auto;
   width: 100%;
   }
   /*****/
   .nav-tabs {
   border: none;
   }
   .nav-tabs .nav-item {
   /* margin-bottom: -1px; */
   margin: 0 2px;
   }
   .tab-pane {
   padding: 0;
   }
   .resp-tab-content .row {
   margin: 0;
   }
   .tab-pane h5 {
   color: #41464b;
   font-weight: 800;
   text-transform: none;
   }
   .parentHorizontalTab {
   border: solid 1px #c80032;
   padding: 15px;
   }
   .product_details .nav-link {
   display: block;
   padding: .5rem 1rem;
   border: solid 1px #c80032;
   }
   .nav-tabs .nav-link {
   border-radius: 35px;
   }
   .nav-tabs .nav-link:focus,
   .nav-tabs .nav-link:hover {
   border-color: #c80032;
   color: #fff;
   background-color: #c80032;
   }
   .product_details .nav-tabs .nav-item.show .nav-link,
   .product_details .nav-tabs .nav-link.active {
   color: #fff;
   background-color: #c80032;
   border-color: #c80032;
   }
   .btn-primary37 {
   color: #fff;
   background-color: #c80032;
   border-color: #c80032;
   text-align: center;
   padding: 10px;
   }
   .btn-primary37:hover {
   color: #fff;
   background-color: #333;
   border-color: #333;
   }
   .borderTOP {
   border-top: solid 1px #eee;
   }
.search-sec {background: #c80032;}
.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block;
font-weight: bold; padding: 10px 8px; border-radius: 0px;}
   
.mCSB_inside>.mCSB_container { padding:0 !important;}

   
.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important;}

.boxNewSection h3 a {text-decoration: none; color: #c80032;}
.boxNewSection h3 a:hover {text-decoration: none; color: #FFF;}

</style>