<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<section id="registration-form" class="inner-page borderTOP">
   <div class="container">
      <div class="row">
         <div class="col-md-12 product_details">
            <ul class="nav nav-tabs align-item-center  justify-content-center" id="myTab" role="tablist">
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='blogs'){ echo 'active';} ?>" id="tab_outer_blog"  href="<?php echo base_url('myspace/blogs') ?>">Blogs</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='qa'){ echo 'active';} ?>"  href="<?php echo base_url('myspace/qa') ?>" id="tab_outer_qa">Question and
                  Answer Forum</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='kr'){ echo 'active';} ?>"  href="<?php echo base_url('myspace/kr') ?>">Knowledge
                  Repository</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='webinar'){ echo 'active';} ?>"  href="<?php echo base_url('myspace/webinar') ?>" >Webinar</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='resource_sharing'){ echo 'active';} ?>" href="<?php echo base_url('myspace/resource_sharing') ?>">Resource Sharing</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link <?php if($module_title=='technology_transfer'){ echo 'active';} ?>" href="<?php echo base_url('myspace/technology_transfer') ?>" >Technology Transfer</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" style="padding:.5rem 0.8rem" href="<?php echo base_url() ?>"><i class="fa fa-times"
                     aria-hidden="true"></i>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>