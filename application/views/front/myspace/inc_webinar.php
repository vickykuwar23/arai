<div class="parentHorizontalTab">
  <ul class="resp-tabs-list hor_1">
      <li id="inner_tab_myweb">My Webinar</li>
      <li id="inner_tab_myactiveweb">Webinar I applied to</li>
  </ul>

  <div class="resp-tabs-container hor_1">
      <div>
          <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myweb" name="search_txt" id="search_txt_myweb" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceWebinarDataAjax(1,'myweb')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myweb')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
          <!-- New Tabs Start -->
          <div class="parentHorizontalTab2 mt-4">
              <ul class="resp-tabs-list hor_2" id="nested_my_webinar">

                  <li data-tab="my_exclusive" id="nested_tab_my_exclusive">Exclusive (<?php echo count($exclusive_webinars); ?>)</li>
                  <li data-tab="my_nonexclusive" id="nested_tab_my_nonexclusive">Non Exclusive (<?php echo count($non_exclusive_webinars); ?>)</li>
              </ul>

              <div class="resp-tabs-container hor_2">

                  <div>
                      <!--vertical Tabs-->
                      <?php if(count($exclusive_webinars)>0){ ?>
                      <div class="ChildVerticalTab_1">

                          <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                          	<?php foreach ($exclusive_webinars as $key => $e_web) { ?>
                          		<li id="inner_li_<?php echo $e_web['w_id'] ?>"> <i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $e_web['webinar_name']; ?></li>
                          		<?php } ?>
                          </ul>

                        <div class="resp-tabs-container ver_1">
                        <?php 
                        foreach ($exclusive_webinars as $key => $value) { 

                        $getTotalRequest = $this->master_model->getRecordCount('arai_webinar_attendence', array('w_id' => $value['w_id']), 'id');
                      
                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span>Webinar ID</span>
                                    <br>
                                    <span><?php echo $value['webinar_id'] ?></span>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a target="_blank" href="<?php echo base_url('webinar/applicantListing/').base64_encode($value['w_id']) ?>"><?php echo $getTotalRequest ?></a>
                                       </h3>
                                    <span>Application Received</span>
                                 </div>
                              </div>
                               <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span>Date & Time</span>
                                    <br>
                                    <span><?php echo date('d-m-y',strtotime($value['webinar_date']));  ?></span>
                                    <br>
                                    <span><?php echo date('H:i',strtotime($value['webinar_start_time']))." to ".date('H:i',strtotime($value['webinar_end_time']));  ?></span>
                                    
                                 </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span class="last-text">

                                    <?php echo $value['admin_status']; ?>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('webinar/viewDetail/').base64_encode($value['w_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take me to this webinar
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('webinar/edit/').base64_encode($value['w_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $action='';
                                 $w_id=$value['w_id'];
                                  if($value['admin_status'] != 'Withdraw'){					
																		$action = '<a href="javascript:void(0);" data-toggle="tooltip" title="Withdraw" data-id="'.$w_id.'" data-tab="my_exclusive" class="webinar-withdraw btn btn-info btn-green-fresh btn btn-primary37 mt-4"><i class="fa fa-ban" aria-hidden="true"></i></a> ';					
																	}else if($value['admin_status'] == 'Withdraw' || $value['admin_status'] == 'Rejected'){

																		$action				= '<a href="javascript:void(0);" data-toggle="tooltip" data-tab="exclusive" data-tab="my_exclusive"  title="Remove" data-id="'.$w_id.'" class="webinar-remove btn btn-info btn-green-fresh btn btn-primary37 mt-4"><i class="fa fa-remove" aria-hidden="true"></i></a> ';
																	}	
                                 echo $action;
                                 ?>

                              </div>
                           </div>
                        </div>
                           <?php } ?>
                          </div>
                      </div>
                       <?php }else{ ?>
                     <div class="text-center p-3">
                        <strong>No webinars found</strong>
                     </div>
                  		<?php } ?>
                      <a target="_blank" href="<?php echo base_url('webinar') ?>" class="btn btn-primary36">
                          Explore Webinars

                      </a>
                  </div>
                  <!-- My Answered Question -->
                  <div>
                      <!--vertical Tabs-->
                      <?php if(count($non_exclusive_webinars)>0){ ?>
                      <div class="ChildVerticalTab_1">
                          <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                          	<?php foreach ($non_exclusive_webinars as $key => $ne_web) { ?>
                              <li> <i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $ne_web['webinar_name']; ?></li>
                          		<?php } ?>
                              
                          </ul>

                      <div class="resp-tabs-container ver_1">
                              <?php 
                        foreach ($non_exclusive_webinars as $key => $ne_value) { 
                        $getTotalRequest = $this->master_model->getRecordCount('arai_webinar_attendence', array('w_id' => $ne_value['w_id']), 'id');
                      
                        ?>
                        <div>
                           <div class="row">
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span>Webinar ID</span>
                                    <br>
                                    <span><?php echo $ne_value['webinar_id'] ?></span>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <h3>
                                       <a target="_blank" href="<?php echo base_url('webinar/applicantListing/').base64_encode($ne_value['w_id']) ?>"><?php echo $getTotalRequest ?></a>
                                       </h3>
                                    <span>Application Received</span>
                                 </div>
                              </div>
                               <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span>Date & Time</span>
                                    <br>
                                    <span><?php echo date('d-m-y',strtotime($ne_value['webinar_date']));  ?></span>
                                    <br>
                                    <span><?php echo date('H:i',strtotime($ne_value['webinar_start_time']))." to ".date('H:i',strtotime($ne_value['webinar_end_time']));  ?></span>
                                    
                                 </div>
                              </div>

                              <div class="col-md-3">
                                 <div class="boxNewSection">
                                    <span class="last-text">

                                    <?php echo $ne_value['admin_status']; ?>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                 <a target="_blank" href="<?php echo base_url('webinar/viewDetail/').base64_encode($ne_value['w_id']);  ?>" class="btn btn-primary35 mt-4">
                                 Take me to this webinar
                                 </a>
                              </div>
                              <div class="col-md-3 text-center">
                                 <a href="<?php echo base_url('webinar/edit/').base64_encode($ne_value['w_id']);  ?>" class="btn btn-primary37 mt-4">
                                 <i class="fa fa-edit" aria-hidden="true"></i>
                                 </a>
                                 <?php 
                                 $action='';
                                 $w_id=$ne_value['w_id'];
                                  if($ne_value['admin_status'] != 'Withdraw'){					
																		$action = '<a href="javascript:void(0);" data-toggle="tooltip" title="Withdraw" data-id="'.$w_id.'" data-tab="my_nonexclusive" class="webinar-withdraw btn btn-info btn-green-fresh btn btn-primary37 mt-4"><i class="fa fa-ban" aria-hidden="true"></i></a> ';					
																	}else if($ne_value['admin_status'] == 'Withdraw' || $ne_value['admin_status'] == 'Rejected'){

																		$action				= '<a href="javascript:void(0);" data-toggle="tooltip" data-tab="my_nonexclusive" title="Remove" data-id="'.$w_id.'" class="webinar-remove btn btn-info btn-green-fresh btn btn-primary37 mt-4"><i class="fa fa-remove" aria-hidden="true"></i></a> ';
																	}	
                                 echo $action;
                                 ?>
                                
                              </div>
                           </div>
                        </div>
                           <?php } ?>
                          </div>
                      </div>
                    <?php }else{ ?>
                    	<div class="text-center p-3">
                        <strong>No webinars found</strong>
                     </div>
                    <?php } ?>
                      <p>&nbsp</p>
                      <a target="_blank" href="<?php echo base_url('webinar') ?>" class="btn btn-primary36">
                          Explore Webinars

                      </a>
                  </div>

              </div>
          </div>
      </div>
      <div>
          <div class="filterBox">
                     <section class="search-sec">
                        <div id="filterData" name="filterData">
                           <div class="container">   
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="row d-flex justify-content-center search_Box">                       
                                       <div class="col-md-6">
                                          <input type="text" class="form-control search-slt search_myactiveweb" name="search_txt" id="search_txt_myactiveweb" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                          <button type="button" class="btn btn-primary searchButton" onclick="getMySpaceWebinarDataAjax(1,'myactiveweb')"><i class="fa fa-search" aria-hidden="true"></i></button>
                                       </div>
                                       <div class="clearAll">
                                          <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form('myactiveweb')">Clear All</a>
                                       </div>
                                       <div class="col-md-12"></div>                      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
          
          <div class="parentHorizontalTab2 mt-4">
              <ul class="resp-tabs-list hor_2" id="nested_my_activwebinar">
                 <li data-tab="my_exclusive" id="active_nested_tab_my_exclusive">Exclusive (<?php echo count($webinar_exclusive_active); ?>)</li>
                  <li data-tab="my_nonexclusive" id="active_nested_tab_my_nonexclusive">Non Exclusive (<?php echo count($webinar_nonexclusive_active); ?>)</li>

              </ul>

              <div class="resp-tabs-container hor_2">
                  <div>
                  	<?php if(count($webinar_exclusive_active)>0){ ?>
                      <div class="ChildVerticalTab_1">
                          <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                          	<?php foreach ($webinar_exclusive_active as $key => $value1) { ?>
                          		<li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $value1['webinar_name'] ?></li>
                          	<?php } ?>
                              
                          </ul>

                          <div class="resp-tabs-container ver_1">
                          	<?php foreach ($webinar_exclusive_active as $key => $value) { ?>
                              <div>
                                  <div class="row">
                                      <div class="col-md-4">
                                          <div class="boxNewSection">
                                              <span>Webinar ID: </span>
                                              <br>
                                              <span><?php echo $value['webinar_id'] ?></span>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="boxNewSection">
				                                    <span>Date & Time</span>
				                                    <br>
				                                    <span><?php echo date('d-m-y',strtotime($value['webinar_date']));  ?></span>
				                                    <br>
				                                    <span><?php echo date('H:i',strtotime($value['webinar_start_time']))." to ".date('H:i',strtotime($value['webinar_end_time']));  ?></span>
				                                    
				                                 </div>
                                      </div>
                                      <?php if($value['hosting_link']!=''){ ?>
                                      <div class="col-md-4">
                                          <div class="boxNewSection">
                                              <span>
                                                <a target="_blank" href="<?php echo $value['hosting_link']; ?>">Attend the Webinar</a>
                                               </span>
                                              <br>
                                          </div>
                                      </div>
                                    <?php } ?>

                                  </div>
                                  <div class="row">
                                      <div class="col-md-12">
                                         <a target="_blank" href="<?php echo base_url('webinar/viewDetail/').base64_encode($value['w_id']);  ?>" class="btn btn-primary35 mt-4">
			                                 Take me to this webinar details
			                                 </a>
                                      </div>
                                  </div>
                              </div>
                              <?php } ?>
                          </div>
                      </div>
                    <?php }else{ ?>
                    	<div class="text-center p-3">
                        <strong>No webinars found</strong>
                     </div>
                    <?php } ?>

                      <p>&nbsp</p>
                      <a target="_blank" href="<?php echo base_url('webinar') ?>" class="btn btn-primary36">
                          Explore Webinars
                      </a>
                  </div>
                  <!-- My Answered Question -->
                  <div>
                      <!--vertical Tabs-->
                      <?php if(count($webinar_nonexclusive_active)>0){ ?>
                      <div class="ChildVerticalTab_1">
                          <ul class="resp-tabs-list ver_1 content light" id="content-id1">
                          	<?php foreach ($webinar_nonexclusive_active as $key => $value1) { ?>
                          		<li> <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $value1['webinar_name'] ?></li>
                          	<?php } ?>
                              
                          </ul>

                          <div class="resp-tabs-container ver_1">
                          	<?php foreach ($webinar_nonexclusive_active as $key => $value) { ?>
                              <div>
                                  <div class="row">
                                      <div class="col-md-4">
                                          <div class="boxNewSection">
                                              <span>Webinar ID: </span>
                                              <br>
                                              <span><?php echo $value['webinar_id'] ?></span>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="boxNewSection">
				                                    <span>Date & Time</span>
				                                    <br>
				                                    <span><?php echo date('d-m-y',strtotime($value['webinar_date']));  ?></span>
				                                    <br>
				                                    <span><?php echo date('H:i',strtotime($value['webinar_start_time']))." to ".date('H:i',strtotime($value['webinar_end_time']));  ?></span>
				                                    
				                                 </div>
                                      </div>

                                      <div class="col-md-4">
                                          <div class="boxNewSection">
                                            <?php if($value['hosting_link']!=''){ ?>
                                             <span>
                                                <a target="_blank" href="<?php echo $value['hosting_link']; ?>">Register for this Webinar</a>
                                               </span>
                                               <?php } ?>
                                               <br>
                                             <?php if($value['hosting_link']!=''){ ?>
                                              <span class="mt-3">
                                                <a target="_blank" href="<?php echo $value['hosting_link']; ?>">Attend the Webinar</a>
                                               </span>
                                              <?php } ?>
                                          </div>
                                      </div>

                                  </div>
                                  <div class="row">
                                      <div class="col-md-12">
                                         <a target="_blank" href="<?php echo base_url('webinar/viewDetail/').base64_encode($value['w_id']);  ?>" class="btn btn-primary35 mt-4">
			                                 Take me to this webinar details
			                                 </a>
                                      </div>
                                  </div>
                              </div>
                              <?php } ?>
                          </div>
                      </div>
                    <?php }else{ ?>
                    	<div class="text-center p-3">
                        <strong>No webinars found</strong>
                     </div>
                    <?php } ?>
                      <p>&nbsp</p>
                      <a target="_blank" href="<?php echo base_url('webinar') ?>" class="btn btn-primary36">
                          Explore Webinars
                      </a>
                  </div>

              </div>
          </div>

          <!-- New Tabs End -->
      </div>

  </div>
</div>