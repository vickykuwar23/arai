<?php $this->load->view('front/myspace/myspace_css') ?>
<?php $this->load->view('front/myspace/myspace_menu') ?>
<div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="Blogs">
         <div class="container" >
         <div id="MySpaceOuter"></div>
         </div>
         
      </div>
   </div>

<!---------- MODAL POP UP FOR BLOCK QUESTION --------------------->
<div class="modal fade" id="QuestionBlockpopup" tabindex="-1" role="dialog" aria-labelledby="BlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlockpopupLabel"><b>Block Question?</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div>
                  <label><b>Question ID : </b><span id="popupQuerstionId"></span></label>
               </div>
               <div>
                  <label><b>Question : </b><span id="popupQuestionTitle"></span></label>
               </div>
               
               <div>
                  <label for="popupQuestionBlockReason"><b>Block reason <em>*</em></b></label>
                  <textarea class="form-control" id="popupQuestionBlockReason" name="popupQuestionBlockReason" required onkeyup="check_block_validation()"></textarea>
                  <span id="popupQuestionBlockReason_err" class='error'></span>
               </div>
            </div>   
            
            <div class="modal-footer">
               <button id="modal_submit_btn" type="button">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>

<!---------- MODAL POP UP FOR SHOW LIKES USER DATA FOR QUESTION --------------------->
<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogReportpopupLabel">Knowledge Repository Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
            <div class="bgNewBox65">
            <p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
            <p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
            </div>
               
               <div class="form-group mt-4">
                  
                  <label for="popupKrDeleteComment" class="">Reason <em>*</em></label>
                  <textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
                  <span id="popupKrDeleteComment_err" class='error'></span>
               </div>
            </div>   
            <input type="hidden" id="popupBlogIdHidden" name="">
            <div class="modal-footer">
               <button type="button" class="btn btn-primary" onclick="submit_kr_delete()">Submit</button>       
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
            </div>
         </form>        
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document" id="download_model_content_outer">
   </div>
</div>
<!-- Modal -->
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>  
<script>
   function block_unblock_kr(id, type)
   {
      var text2='from';
      if (type=='Restore') {text2='in';}
      swal(
      {  
         title:"Confirm?",
         text: "Are you sure you want to "+type+" the selected knowledge repository "+text2+" front listing?",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes!'
      }).then(function (result) 
      { 
         if (result.value) 
         {
            var cs_t =  $('.token').val();
            $.ajax(
            {
               type:'POST',
               url: '<?php echo site_url("knowledge_repository/block_unblock_kr"); ?>',
               data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
               dataType:"JSON",
               success:function(data)
               {
                  $(".token").val(data.token);
                  swal(
                  {
                     title: 'Success!',
                     text: "Knowledge repository successfully "+type+"d.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonText: 'Ok'
                  });

                  var dec_kr_id = atob(id);
                  getMySpaceKRDataAjax(0,'mykr','kr',dec_kr_id);
               }
            });
         } 
      });
   }

   function delete_kr_new(kr_id,  kr_disp_id, kr_title)
   {
            $("#popupBlogId").html(kr_disp_id);
            $("#popupBlogIdHidden").val(kr_id);
            $("#popupBlogTitle").html(kr_title);
            $("#popupKrDeleteComment").val('');
            $("#popupKrDeleteComment_err").html('');
            $("#BlogReportpopup").modal('show');
            $("#popupKrDeleteComment").focus();
         
   }  

   function submit_kr_delete(kr_id)
   {  
      check_delete_validation();
      
      var popupKrDeleteComment = $("#popupKrDeleteComment").val();
      var kr_id = $("#popupBlogIdHidden").val();
      if(popupKrDeleteComment.trim() != "")     
      {
         $("#popupKrDeleteComment_err").html('');
         swal(
         {  
            title:"Confirm?",
            text: "Are you sure you want to remove the selected knowledge repository? This action will remove this entry from your listing, and will no longer be available for the reference.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
         }).then(function (result) 
         { 
            if (result.value) 
            {
               var cs_t =  $('.token').val();
               $.ajax(
               {
                  type:'POST',
                  url: '<?php echo site_url("knowledge_repository/delete_kr"); ?>',
                  data:{ 'kr_id':kr_id, 'popupKrDeleteComment':popupKrDeleteComment, 'csrf_test_name':cs_t },
                  dataType:"JSON",
                  success:function(data)
                  {
                     $("#BlogReportpopup").modal('hide');
                     $(".token").val(data.token);
                     swal(
                     {
                        title: 'Success!',
                        text: "knowledge repository successfully deleted.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok'
                     });
                     getMySpaceKRDataAjax(0,'mykr','kr');
                  }
               });
            } 
         });
      }
   }
   function check_delete_validation()
   {
      var popupKrDeleteComment = $("#popupKrDeleteComment").val();
      if(popupKrDeleteComment.trim() == "") { $("#popupKrDeleteComment_err").html('Please enter the delete reason'); $("#popupKrDeleteComment").focus(); }
      else { $("#popupKrDeleteComment_err").html(''); }
   }


   function show_blog_likes(id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("knowledge_repository/show_blog_likes_ajax"); ?>',
         data:{ 'id':id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }
   
   function show_blog_reports(kr_id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("knowledge_repository/show_blog_reported_ajax"); ?>',
         data:{ 'kr_id':kr_id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }

   function show_downloads(kr_id)
   {
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("knowledge_repository/show_kr_downloads_ajax"); ?>',
         data:{ 'id':kr_id, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $(".token").val(data.token);
            if(data.flag == 'success')
            {
               $("#BlogLikesLabel").html(data.blog_title);           
               $("#BlogLikesContent").html(data.response);           
               $("#BlogLikes").modal('show');            
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
         }
      });
   }

   function open_download_modal(kr_id)
   {
      module_id=31;
      if(check_permissions_ajax(module_id) == false){
            swal( 'Warning!','You don\'t have permission to access this page !','warning');
            return false;
      }
      $.ajax({
         type: "POST",
         url: "<?php echo site_url('knowledge_repository/open_download_modal_ajax'); ?>",
         data: {'kr_id':kr_id},
         cache: false,
         success:function(data)
         {
            //alert(data)
            if(data == "error")
            {
               location.reload();
            }
            else
            {
               $("#download_model_content_outer").html(data);
               $("#DownloadModal").modal('show');
            }
         }
      });
   }
</script>

<!--Plug-in Initialisation-->
<script type="text/javascript">
   $(document).ready(function () {
      getMySpaceKRDataAjax(0,'myblogs','blog');
      // initializeTabs()
   });
   
   function getMySpaceKRDataAjax(is_search=0,inner_tab,outer_tab,inner_li_id='')
   {
      var cs_t =  $('.token').val();
      var keyword = encodeURIComponent($('#search_txt_'+inner_tab).val());
      if (keyword=='undefined') {keyword='';}
      parameters= {  'keyword':keyword,'is_search':is_search,'inner_tab':inner_tab,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('myspace/load_kr_ajax'); ?>",
         data: parameters,
         cache: false,
         async:false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               $("#MySpaceOuter").html(data.myspace_qa);
               if (data.is_search==1 && data.inner_tab!='') {
                  $(".search_"+data.inner_tab).val(data.keyword);
               }
               
               $(".token").val(data.csrf_new_token)
               initializeTabs()
               initializeScroll()
               

               $("#inner_tab_"+outer_tab).trigger("click");
               $("#inner_tab_"+inner_tab).trigger("click");
               if (inner_li_id!='') {
                  $("#inner_li_"+inner_li_id).trigger("click");
                  $('.ul_of_qa').scrollTop($("#inner_li_"+inner_li_id).position().top);

               }

            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });     
   }

   function clear_search_form(inner_tab,outer_tab){
      getMySpaceKRDataAjax(0,inner_tab,outer_tab);
   }
   

</script>
<?php $this->load->view('front/myspace/myspace_common_js') ?>