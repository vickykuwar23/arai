<div class="col-6">
              <?php if ($this->session->flashdata('error_message') != "") 
            { ?>
               <div class="alert alert-danger alert-dismissable">
               <i class="fa fa-ban"></i>
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
            <?php } 
            ?>
            <?php 
            if ( validation_errors()!="" )
              { ?> 
               <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo validation_errors();  ?>
               </div>
               <?php }
               ?>
</div>
<div class="container row">
<div class="col-5">
<form method="POST" id="contact_form">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
  <div class="form-group">
    <label for="fullname">First Name</label>
    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="First Name" value="<?php echo set_value('fullname')?>">
  </div>
 
  <div class="form-group">
    <label for="last_name">Mobile</label>
    <input type="text" class="form-control" name="mobile_no" id="mobile" placeholder="Mobile" value="<?php echo set_value('mobile')?>">
  </div>

  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" name="email_id" id="email" placeholder="Email address" value="<?php echo set_value('email')?>">
  </div>
  <div class="form-group">
    <label for="password">Message</label>
    <textarea class="form-control"name="subject_details"></textarea>
  </div>
   
   <div class="form-group row captcha">
                <label class="col-lg-4 control-label text-left cptcha_img">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-6">
                  <input type="text" name="code" id="captcha" placeholder="" class="form-control" > 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
                  
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
        </div>

  <div class="form-group">
    <button type="submit" class="form-control" name="submit" value="Submit" id="submit" >Submit</button> 
  </div>

  
</form>
</div>
</div>


<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
    form.submit();
    }
  });

  $('#contact_form').validate({
    rules: {
         
          fullname: {
            required: true
          },
          mobile_no: {
            required: true,
            number: true,
            minlength:10,
            maxlength:11
          },
          email_id: {
            required: true,
            email: true
          },
          code: {
            required: true
          },

          
        },
    messages: {
          fullname: {
            required: "This field is required",
          },
         
          mobile_no: {
            required: "This field is required",
          },
          email_id: {
            required: "This field is required",
          },
          
          code: {
            required: "This field is required",
          },

    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'contact/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>




