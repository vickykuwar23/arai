<?php
$encObj =  New Opensslencryptdecrypt();

	if(count($featured_tech) > 0)
	{ ?>								
	
	<div class="owl-carousel mb-2 mt-4 owl-theme resourceSharingSlider">
		
			<?php foreach($featured_tech as $res)
				{	 
					$data['res'] = $res; 
					$data['blog_type_flag'] = $blog_type_flag = 'featured';	?>
				
				<div>							

					<?php 
					$this->load->view('front/technology_transfer/inc_tech_listing_inner', $data); ?>
				</div>				
			<?php } ?>	
			
		</div>
	
	
	<script>
	 $('.resourceSharingSlider').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: false,
        nav: true,
        dots: false,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });
	</script>
<?php } ?>