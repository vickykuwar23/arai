<?php  $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script>



  // CKEDITOR.replace('body', {height: 50});

	

	function append_video_row(){
		var count = $("#video_warapper_count").val();
		var id = parseInt(count)+1;
		console.log(id)
		if (id>6) {
			swal( 'Warning!','Maximum 6 videos are allowed','warning');
			return false;
		}

		var append_sring = '';
		append_sring =  '<div class="row appended_div_'+id+'">'
		append_sring +=  '<div class="col-md-4">'
		append_sring +=   '<div class="form-group">';
		append_sring +=    '<input type="text" class="form-control video_caption" name="video_caption[]" id="caption'+id+'" value="">';
		append_sring += 	'<label class="form-control-placeholder floatinglabel">Caption</label></div></div>'
		
		append_sring +=		'<div class="col-md-6"><div class="form-group">';
									
		append_sring +=		'<input type="text" class="form-control video_url" name="video_url[]" id="video_url'+id+'" value="">';
		append_sring +=		'<label class="form-control-placeholder floatinglabel">Link</label></div></div>';
		append_sring +=		'<div class="col-md-2"><div class="form-group">';
								
		append_sring +=	'<button type="button" class="btn btn-danger btn-sm mt-4" onclick="remove_video_row('+id+')">-</button>';
		append_sring +=	 '</div></div>';
		append_sring +=	 '</div>';
		
		var wrapper =  $(".video_wrapper");
		$(wrapper).append(append_sring); 

		$("#video_warapper_count").val(parseInt(count)+1);


	}

	function remove_video_row(div_id){
		// alert(div_id)
		$(".appended_div_"+div_id).remove();
		var count = $("#video_warapper_count").val();
		$("#video_warapper_count").val(parseInt(count)-1);
		// $(this).closest('.row').remove();
	}
	//VALIDATION SCROLLING TO TOP
	$('.select2_common').select2();
	function show_hide_tag_other()
	{
		$("#tag_other_outer").hide();

		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++)
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}

		if(disp_other_flg == 1)
		{
			$("#tag_other_outer").show();
			$("#tag_other").prop("required", true);
		}
		else
		{
			$("#tag_other_outer").hide();
			$("#tag_other").prop("required", false);
			$("#tag_other").val('');
		}
	}
	show_hide_tag_other();

	$(document).ready(function()
	{
	$("#include_files").change(function () {
	    if ($('#include_files').is(':checked')) {
	    	$(".files_div").removeClass('d-none');
	    }else{
	    	
	    	$(".files_div").addClass('d-none');
	    }

	});

	$("#video_check").change(function () {
		
	    if ($('#video_check').is(':checked')) {
	    	
	    	$(".video_wrapper").removeClass('d-none');
	    }else{
	    	$(".video_wrapper").addClass('d-none');
	    }

	});


	});



	function scroll_to_top(div_id)
	{
		if(div_id == '' || div_id==undefined) { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); }
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();

	//REMOVE BANNER PREVIEW / IMAGE
	function remove_tile_img(flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the tile image?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result)
		{
			if (result.value)
			{
				$("#preloader").css("display", "block");
				if(flag == 0)
				{
					$('#tile_image').val('');
					$("#tile_image").parent().find('input').next("span").remove();
				}
				else
				{
					<?php if ($mode == 'Update') {?>
					var csrf_test_name = $("#csrf_token").val();
					var data = {
						'tbl_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('arai_technology_transfer'); ?>")),
						'pk_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('tech_id') ?>")),
						'del_id': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt($form_data[0]['tech_id']); ?>")),
						'input_nm': encodeURIComponent($.trim('tile_image')),
						'file_path': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('./uploads/byt/'); ?>")),
						'csrf_test_name': encodeURIComponent($.trim(csrf_test_name))
					};
					$.ajax(
					{
						type: "POST",
						url: '<?php echo site_url("delete_single_file") ?>',
						data: data,
						dataType: 'JSON',
						success:function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							/* $("#"+input_nm+"_outer").remove();

							swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" }); */
						}
					});
					<?php }?>
				}

				$("#tile_image_outer").addClass('d-none');
				$("#tile_image_outer .previous_img_outer img").attr("src", "");
				$("#preloader").css("display", "none");
			}
		});
	}

	function remove_logo_img(flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the logo image?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result)
		{
			if (result.value)
			{
				$("#preloader").css("display", "block");
				if(flag == 0)
				{
					$('#company_logo').val('');
					$("#company_logo").parent().find('input').next("span").remove();
				}
				else
				{
					<?php if ($mode == 'Update') {?>
					var csrf_test_name = $("#csrf_token").val();
					var data = {
						'tbl_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('arai_technology_transfer'); ?>")),
						'pk_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('tech_id') ?>")),
						'del_id': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt($form_data[0]['tech_id']); ?>")),
						'input_nm': encodeURIComponent($.trim('company_logo')),
						'file_path': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('./uploads/byt/'); ?>")),
						'csrf_test_name': encodeURIComponent($.trim(csrf_test_name))
					};
					$.ajax(
					{
						type: "POST",
						url: '<?php echo site_url("delete_single_file") ?>',
						data: data,
						dataType: 'JSON',
						success:function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							/* $("#"+input_nm+"_outer").remove();

							swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" }); */
						}
					});
					<?php }?>
				}

				$("#company_logo_outer").addClass('d-none');
				$("#company_logo_outer .previous_img_outer_logo img").attr("src", "");
				$("#preloader").css("display", "none");
			}
		});
	}




	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function TileBannerPreview(input)
	{
		if (input.files && input.files[0])
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e)
				{
					$("#tile_image_outer").removeClass('d-none');
					//$("#tile_image_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#tile_image_outer .previous_img_outer img").attr("src", e.target.result);
					$("#tile_image_outer .previous_img_outer a.btn").attr("onclick", "remove_tile_img(0)");
				}
				reader.readAsDataURL(input.files[0]); // convert to base64 string
			}
			else
			{
				$("#tile_image_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#tile_image").change(function() { TileBannerPreview(this); });


	function LogoPreview(input)
	{
		if (input.files && input.files[0])
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e)
				{
					$("#company_logo_outer").removeClass('d-none');
					//$("#company_logo_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#company_logo_outer .previous_img_outer_logo img").attr("src", e.target.result);
					$("#company_logo_outer .previous_img_outer_logo a.btn").attr("onclick", "remove_logo_img(0)");
				}
				reader.readAsDataURL(input.files[0]); // convert to base64 string
			}
			else
			{
				$("#company_logo_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#company_logo").change(function() { LogoPreview(this); });

	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_tech_file(div_id, file_id, public_private_flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result)
		{
			if (result.value)
			{
				$("#preloader").css("display", "block");
				var customFileCount = $("#customFileCount").val();
				$("#customFileCount").val(parseInt(customFileCount)-1);
				if(file_id!= '' && file_id != '0')
				{
					<?php if ($mode == 'Update') {?>
					var csrf_test_name = $("#csrf_token").val();
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };
					$.ajax(
					{
							type: "POST",
							url: '<?php echo site_url("technology_transfer/delete_tech_file_ajax") ?>',
							data: data,
							dataType: 'JSON',
							success:function(data)
							{
									$("#csrf_token").val(data.csrf_new_token);
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php }?>
				}

				if(public_private_flag == 'public')
				{
				$("#tech_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				else if(public_private_flag == 'private')
				{
					$("#tech_files_outer_private"+div_id).remove();
					$(".btnOuterForDelPrivate"+div_id).remove();
				}

				$("#preloader").css("display", "none");
			}
		});
	}

	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function TeamFilesPreview(input, public_private_flag)
	{
		$(".tech_files_err_new").html('');

		console.log(public_private_flag);
		if (input.files && input.files[0])
		{
			$("#preloader").css("display", "block");

			if(public_private_flag == 'public')
			{
			var customFileCount = $("#customFileCount").val();
			var disp_filename_final = '';
			var disp_filename = input.files[0].name;
			var reader = new FileReader();
			var j = customFileCount;
			}
			else if(public_private_flag == 'private')
			{
				var customFileCount = $("#customFileCountPrivate").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}

			var upload_file_size = input.files[0].size;

			if(upload_file_size < 5000000)
			{

			var disp_img_name = "";
			var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
			disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>";

			var id_name = '';
			var btn_id_name = '';
			var input_cls_name = '';
			var input_type_name = '';
			var fun_var_parameter = "";
			var file_upload_common = '';
			if(public_private_flag == 'public')
			{
				id_name = 'tech_files_outer'+j;
				btn_id_name = 'addFileBtnOuter';
				input_cls_name = 'fileUploadTeams';
				input_type_name = 'tech_files[]';
				fun_var_parameter = "'public'";
				file_upload_common = 'file-upload';
			}
			else if(public_private_flag == 'private')
			{
				id_name = 'tech_files_outer_private'+j;
				btn_id_name = 'addFileBtnOuterPrivate';
				input_cls_name = 'fileUploadTeamsPrivate';
				input_type_name = 'tech_files_private[]';
				fun_var_parameter = "'private'";
				file_upload_common = 'file-upload-private';
			}

			var append_str = '';
			append_str += '	<div class="col-md-2 tech_files_outer_common" id="'+id_name+'">';
			append_str += '		<div class="file-list">';
			append_str += '			<a href="javascript:void(0)" onclick="remove_tech_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
			//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
			append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
			append_str += '		</div>';
			append_str += '	</div>';

			var btn_str = '';
			btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
			btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
			btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="TeamFilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
			btn_str +=	'	</div>';
			btn_str +=	'	</div> <div class="tech_files_err"></div>';

			if(public_private_flag == 'public')
			{
			$("#addFileBtnOuter").addClass('d-none');
			$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
			$("#addFileBtnOuter").removeAttr( "id" );

			$(append_str).insertBefore("#last_team_file_id");
			$(btn_str).insertAfter("#last_team_file_id");
			$("#customFileCount").val(parseInt(customFileCount)+1);
			}
			else if(public_private_flag == 'private')
			{
				$("#addFileBtnOuterPrivate").addClass('d-none');
				$("#addFileBtnOuterPrivate").addClass('btnOuterForDelPrivate'+j+'');
				$("#addFileBtnOuterPrivate").removeAttr( "id" );

				$(append_str).insertBefore("#last_team_file_id_private");
				$(btn_str).insertAfter("#last_team_file_id_private");
				$("#customFileCountPrivate").val(parseInt(customFileCount)+1);
			}

			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 5 MB','error');
			}

				$("#preloader").css("display", "none");
			}
	}
	$(".fileUploadTeams").change(function() { TeamFilesPreview(this,'public'); });
	$(".fileUploadTeamsPrivate").change(function() { TeamFilesPreview(this,'private'); });


	function append_invite_row()
	{
		if($(".invitation_email_input").valid()==false) { return false; }

		var total_div_cnt = $('input[name*="invite_email[]"]').length;
		if(total_div_cnt >= 25)
		{
			swal({ title: "Alert", text: "You can not invite more than "+total_div_cnt+" members at a time", type: "warning" });
		}
		else
		{
			var row_cnt = $("#invite_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;

			var append_html = '';
			append_html += '	<div class="row" id="byt_invite_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-11">';
			append_html += '			<div class="form-group upload-btn-wrapper" style="margin-top:0">';
			append_html += '				<input type="text" class="form-control invitation_email_input" name="invite_email[]" id="invite_email'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group" style="margin-top:0">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';

			$("#invite_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_invite_last");
		}
	}

	function remove_current_div_invite(div_no)
	{
		$("#byt_invite_row_"+div_no).remove();
	}

	$('body').on('change', '.cls_team_skillset', function ()
	{
		var selected = $(this).find('option:selected', this);
		var results = [];

		var element_key=$(this).data('key');
		//console.log(element_key)

		selected.each(function() { results.push($(this).data('id')); });
		var display_flag=0;
		$.each(results,function(i) { if(results[i]=='Other') { display_flag = 1; } });

		if (display_flag==1)
		{
			//$("#other_skill_div"+element_key).removeClass('d-none')
		}
		else
		{
			$("#other_skill"+element_key).val('');
			$("#other_skill_div"+element_key).addClass('d-none')
		}
	});



	function swal_confirm_popup()
	{

		swal(
		{
			title:"Confirm?" ,
			text: "Are you sure you want to cancel?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result)
		{
			if (result.value)
			{
				var mode_check = '<?php echo $mode ?>';
				if(mode_check=='Update'){
					window.location.href ='<?php echo site_url('technology_transfer/my_technology_transfer') ?>';
				}else{
					window.location.href = '<?php echo site_url('technology_transfer/my_technology_transfer') ?>';
				}

			}
		});
	}

	//STEP WIZARD
	$(document).ready(function()
	{

		//******* STEP SHOW EVENT *********
		$("#smartwizard_apply_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition)
		{
			console.log("You are on step "+stepPosition+" now");
			if (stepPosition === 'first') { 
				$(".sw-btn-prev").hide(); 
				$(".sw-btn-next").show(); 
				$(".btnfinish").hide(); 
				$(".btnCancelCustom").show();  
				$(".btnCancelCustom").addClass( "btnCancelCustomStep1" ); 
				$(".sw-btn-group-extra").addClass( "sw-btn-group-extra-step1" );
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons_add_tech'); 
			}
			else if (stepPosition === 'final') { $(".sw-btn-prev").show(); $(".sw-btn-next").hide(); $(".btnfinish").show(); $(".btnCancelCustom").show(); $(".btnCancelCustom").removeClass( "btnCancelCustomStep1" ); $(".sw-btn-group-extra").removeClass( "sw-btn-group-extra-step1" ); $(".sw-btn-group-extra").addClass('step_wiz_buttons');  $(".sw-btn-group-extra").addClass('step_wiz_buttons_add_tech');}
			else { $(".sw-btn-prev").show(); $(".sw-btn-next").show(); $(".btnfinish").hide(); $(".btnCancelCustom").hide(); $(".btnCancelCustom").removeClass( "btnCancelCustomStep1" ); $(".sw-btn-group-extra").removeClass( "sw-btn-group-extra-step1" ); 
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons_add_tech'); 
		}
		});

		var cancel_onclick_fun = "swal_confirm_popup()";
		//******* STEP WIZARD *********
		$('#smartwizard_apply_challenge').smartWizard(
		{
			/*selected: 3,*/
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			keyNavigation: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */

			  anchorSettings: {
			      anchorClickable: true, // Enable/Disable anchor navigation
			      enableAllAnchors: true, // Activates all anchors clickable all times
			      markDoneStep: true, // Add done state on navigation
			      markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
			      removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
			      enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
			  },


			toolbarSettings:
			{
				toolbarExtraButtons:
				[
				$('<button></button>').text(<?php if ($mode == 'Add') {?>'Submit'<?php } else {?> 'Update' <?php }?>).addClass('btn btn-primary btnfinish').on('click', function(e)
				{

					e.preventDefault();
					var submit_flag = true;


					if ($("input[name=include_files]").is(":checked")==true) {
							if( $(".fileUploadTeams").valid()==false ) { 
								submit_flag = false; 
							}
					}
						
					if ($("input[name=video_check]").is(":checked")==true) {
						
						if($(".video_caption").valid()==false ) { 
							submit_flag = false; 
						}	

						if($(".video_url").valid()==false ) { 
							submit_flag = false; 
						}
					
					}
						


						
						


					if($(".accept_terms").valid()==false ) { 
							submit_flag = false; 
						}
					
					
					if(submit_flag == true)
					{
						swal(
						{
							title:"Confirm",
							<?php if ($mode == 'Add') {?> text: "Are you sure you want to submit your technology transfer details?", <?php } else {?>text: "Are you sure you want to update your technology transfer details?", <?php }?>
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Yes!'
						}).then(function (result)
						{
							if (result.value)
							{
								$("#ApplyChallengeForm").submit();
							}
						});
					}else{
						return submit_flag;
					}
				}),
				$('<a style="border-radius: 4px;" href="javascript:void(0)" onclick="'+cancel_onclick_fun+'" ></a>').text('Cancel').addClass('btn btn-primary btnCancelCustom')
				]
			}
		});

		$("#smartwizard_apply_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection)
		{

			if(stepNumber==2 && stepDirection=="forward"){
				$(".sw-btn-group-extra").addClass('step_wiz_buttons');
			}else{
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons');
			}

			var mode_check_new = '<?php echo $mode ?>';
			if(mode_check_new=='Update'){
			if( stepNumber==1 && stepDirection=="forward" ){
				$(".sw-btn-group-extra").addClass('step_wiz_buttons');
			}else{
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons');
			}
				}
			var isValidate = true;

			if(stepNumber==0 && stepDirection=="forward")
			{

				if($("#company_profile").valid()==false) { isValidate= false; $("#company_profile").focus(); }
				if($("#technology_abstract").valid()==false) { isValidate= false; $("#technology_abstract").focus(); }
				if($("#technology_value").valid()==false) { isValidate= false; $("#technology_value").focus(); }


				//if($("#team_size").valid()==false) { isValidate= false; $("#team_size").focus(); }
				if($("#tile_image").valid()==false) { isValidate= false; $("#tile_image").focus(); }
				if($("#company_logo").valid()==false) { isValidate= false; $("#company_logo").focus(); }

				
				if($("#technology_title").valid()==false) { isValidate= false; $("#technology_title").focus(); }
				if($("#author_name").valid()==false) { isValidate= false; $("#author_name").focus(); }



				// if($("#technology_title").valid()==false) { scroll_to_top('technology_title'); }
				// else if($("#author_name").valid()==false) { scroll_to_top('author_name'); }
				// else if($("#tile_image").valid()==false) { scroll_to_top('tile_image'); }
				//else if($("#team_size").valid()==false) { scroll_to_top('team_size'); }
				// else if($("#company_profile").valid()==false) { scroll_to_top('company_profile'); }

				else { scroll_to_top('smartwizard_apply_challenge'); }

				//if(isValidate == true) { append_team_member_blocks(); }
			}

			if(stepNumber==1 && stepDirection=="forward")
			{
				if($("#important_features").valid()==false) { isValidate= false; $("#important_features").focus(); }
				if($("#application").valid()==false) { isValidate= false; $("#application").focus(); }
				if($("#intellectual_property").valid()==false) { isValidate= false; $("#intellectual_property").focus(); }
				if($("#beneficiary_industry").valid()==false) { isValidate= false; $("#beneficiary_industry").focus(); }

				// if($(".cls_team_type").valid()==false) { isValidate = false; }
				// if($(".cls_team_skillset").valid()==false) { isValidate = false; }
				// if($(".cls_team_role").valid()==false) { isValidate = false; }
				scroll_to_top('smartwizard_apply_challenge');
			}
			if(stepNumber==2 && stepDirection=="forward")
			{
				
				if($("#advantages").valid()==false) { isValidate= false; $("#advantages").focus(); }
				if($("#commercialization_status").valid()==false) { isValidate= false; $("#commercialization_status").focus(); }
				if($("#tags").valid()==false) { isValidate= false; $("#tags").focus(); }
				if($("#trl").valid()==false) { isValidate= false; $("#trl").focus(); }
				
				scroll_to_top('smartwizard_apply_challenge');
			}
			/* if(stepNumber==2 && stepDirection=="forward")
				{
				if($("#from_age").valid()==false) { isValidate= false; $("#from_age").focus(); }
				if($("#educational").valid()==false) { isValidate= false; $("#educational").focus(); }

				if($("#educational").valid()==false) { scroll_to_top('educational'); }
				else if($("#from_age").valid()==false) { scroll_to_top('from_age'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				}
			*/

			if(stepDirection=="backward") { scroll_to_top('smartwizard_apply_challenge') }
			return isValidate;
		})
	});

	$(document).ready(function()
	{
		$("input.form-control").keydown(function(event)
		{
			if(event.keyCode == 13)
			{
				event.preventDefault();
				return false;
			}
		});
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all');?>
<?php
$current_date       = date('d-m-Y');
$date_after_90_days = date('d-m-Y', strtotime("+3 months", strtotime($current_date)));
?>

<script type="text/javascript">
	$('.select2_common').select2(
	{
    tags: true,
    tokenSeparators: [',','#']
	});

	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }

	function getWordCount(wordString)
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}

	

	//JQUERY VALIDATIONS
	$(document).ready(function ()
	{
		$("input.numbers").keypress(function(event) { return /\d/.test(String.fromCharCode(event.keyCode)); });

		$.validator.addMethod("type_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Type of Member');
		$.validator.addClassRules("cls_team_type", { type_required: true });

		$.validator.addMethod("skillset_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Skill Sets');
		$.validator.addClassRules("cls_team_skillset", { skillset_required: true });

		/* $.validator.addMethod("team_other_skill_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Other Skill');
		$.validator.addClassRules("cls_other_skill", { team_other_skill_required: true, letters_number_space: true }); */

		$.validator.addMethod("team_role_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Role in Team');

		$.validator.addMethod("CustomMaxlength", function(value, element, param) { if($.trim(value).length <= param ) { return true; } else { return false; } },'Please enter maximum 30 character');
		$.validator.addClassRules("cls_team_role", { team_role_required: true, letters_number_space: true, CustomMaxlength:30 });

		$.validator.addMethod("invitation_email_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Email');
		$.validator.addMethod("invitation_email_valid", function(value, element) { if ($.trim(value) != '') { var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/); return pattern.test(value); } else { return true; } },'Please enter a valid email address');

		$.validator.addMethod("invitation_email_unique", function(value, element)
		{
			var valid_flag = 0;
		 	var current_email = value;
		 	var current_id = element.id;

		    $('.invitation_email_input').each(function()
		    {
		        if ($(this).val() == current_email && $(this).attr('id') != current_id && current_email != '')
		        {

		           valid_flag = 1;
		        }

		    });

		    if(valid_flag == 0) { return true; }
		    else { return false; }


		},'Please enter a unique email address');

		$.validator.addClassRules("invitation_email_input", { /* invitation_email_required: true, */ invitation_email_valid: true,
			 invitation_email_unique:true,
		});

		$.validator.addMethod("minCount", function(value, element, params)
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));

		$.validator.addMethod("maxCount", function(value, element, params)
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));

		$.validator.addMethod("letters_number_space", function(value, element)
		{
			/* return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value); */
			return true;
		}, 'Special characters are not allowed');

		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\'\- \ ]+$/i.test(value);
		}, "Name must contain only letters, numbers, or dashes.");

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');

		jQuery.validator.addMethod('ck_max_word_count', function (value, element, params) {
		    var idname = jQuery(element).attr('id');
		    var messageLength =  jQuery.trim ( CKEDITOR.instances[idname].getData() );
		   	var value = $(messageLength).text();
		    var count = getWordCount(value);
		    if(count<401){
				return true;
			}else{
				return false;
			}
		    
		}, "Maximum 400 words are allowed.");




		//******* JQUERY VALIDATION *********
		$("#ApplyChallengeForm").validate(
		{
			/* onkeyup: false, */
			// ignore: ":disabled",
		    ignore: [],

			rules:
			{
				// company_profile:{
				// 	maxCount:['100']
				// },
				company_profile:{
					ck_max_word_count:true,
				},
				important_features:{
					ck_max_word_count:true,
				},
				application:{
					ck_max_word_count:true,
				},
				intellectual_property:{
					ck_max_word_count:true,
				},
				beneficiary_industry:{
					ck_max_word_count:true,
				},
				advantages:{
					ck_max_word_count:true,
				},


				technology_abstract: 
				{
					required: function() 
					{
						CKEDITOR.instances.technology_abstract.updateElement();
					},
					ck_max_word_count:true,							
					minlength:1,
				},
				technology_value: 
				{
					required: function() 
					{
						CKEDITOR.instances.technology_value.updateElement();
					},	
					ck_max_word_count:true,						
					minlength:1
				},
				commercialization_status: 
				{
					required: function() 
					{
						CKEDITOR.instances.commercialization_status.updateElement();
					},	
					ck_max_word_count:true,						
					minlength:1
				},

				technology_title:{required: true, nowhitespace: true, valid_value: true},
				author_name: { required: true, nowhitespace: true, valid_value: true/* ,
				maxCount:['20'] */ },
				tile_image: { valid_img_format: true, maxsize: 2000000 },
				company_logo: { valid_img_format: true, maxsize: 2000000 },

				accept_terms: { required: true},

				// company_profile: { required: true, nowhitespace: true, letters_number_space: true, minCount:['5'],  maxCount:['80'] },
				trl:{required:true},
				"tags[]":{required:true},
				
				// technology_abstract:{required:true},
				// technology_value:{required:true},
				
				
				"video_caption[]": {
	               required: function() {
					return $("input[name=video_check]").is(":checked") ;
	               }
               
              	},



              	"video_url[]": {
	               required: function() {
           			return $("input[name=video_check]").is(":checked") ;
	               },
	               url:function () {
	               	return $("input[name=video_check]").is(":checked") ;
	               }
               
              	},
              	"tech_files[]": { 
              		custum_required: function(){
             	
              		if( $("input[name=include_files]").is(":checked")==false ){
              			 return false;
              		}else{
              			 return true;
              		}	
              		// return $("input[name=include_files]").is(":checked") ;	
              		},
              		valid_img_format: function(){
             	
              		if( $("input[name=include_files]").is(":checked")==false ){
              			 return false;
              		}else{
              			 return true;
              		}	
              		// return $("input[name=include_files]").is(":checked") ;	
              		},
              		
              		// maxsize: 2000000

              	},


			},
			messages:
			{
				// technology_title: { required: "Please enter the technology title", nowhitespace: "Please enter the technology title" },
				// author_name: { required: "Please enter the name of the author", nowhitespace: "Please enter the name of the author" },
				tile_image: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				company_logo: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				"tech_files[]":{
					valid_img_format: "Please upload only image file",
				}
				
			},
			errorElement: 'span',
			errorPlacement: function (error, element)
			{
				$("#tech_files_err_new").html('');
				if(element.hasClass('select2_common') && element.next('.select2-container').length)
				{
					error.insertAfter(element.parent());
				}
				else if (element.attr("name") == "accept_terms") 
				{
					error.insertAfter("#accept_terms_err");
				}
				else if (element.attr("name") == "tech_files[]"){
					$("span#file-upload-error").remove();
					$(".tech_files_err_new").text('');
					error.insertAfter("#tech_files_err_new");
					
				}
				else { element.closest('.form-group').append(error);  }
			},
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); },
			invalidHandler: function(e,validator)
			{
				//validator.errorList contains an array of objects, where each object has properties "element" and "message".  element is the actual HTML Input.
				for (var i=0;i<validator.errorList.length;i++){
					console.log(validator.errorList[i]);
				}
				//validator.errorMap is an object mapping input names -> error messages
				for (var i in validator.errorMap) {
				  console.log(i, ":", validator.errorMap[i]);
				}
			}
		});

		$(document).on("change", ".select2_common", function()
		{
			var isValidate = true;
			var form = $( "#ApplyChallengeForm" );
			if(form.valid()==false) { isValidate= false; }
			return isValidate;
		});

	

	});


	CKEDITOR.replace('company_profile',{removeButtons: 'Source',});
	CKEDITOR.replace('technology_abstract',{removeButtons: 'Source',});
	CKEDITOR.replace('technology_value',{removeButtons: 'Source',});
	CKEDITOR.replace('important_features',{removeButtons: 'Source',});
	CKEDITOR.replace('application',{removeButtons: 'Source',});
	CKEDITOR.replace('intellectual_property',{removeButtons: 'Source',});
	CKEDITOR.replace('beneficiary_industry',{removeButtons: 'Source',});
	CKEDITOR.replace('advantages',{removeButtons: 'Source',});
	CKEDITOR.replace('commercialization_status',{removeButtons: 'Source',});

 	var company_profile = CKEDITOR.instances['company_profile'];
 	var technology_abstract = CKEDITOR.instances['technology_abstract'];
 	var technology_value = CKEDITOR.instances['technology_value'];
 	var important_features = CKEDITOR.instances['important_features'];
 	var application = CKEDITOR.instances['application'];
 	var intellectual_property = CKEDITOR.instances['intellectual_property'];
 	var beneficiary_industry = CKEDITOR.instances['beneficiary_industry'];
 	var advantages = CKEDITOR.instances['advantages'];
 	var commercialization_status = CKEDITOR.instances['commercialization_status'];

	company_profile.on('save', function(event) {return false;});
	technology_abstract.on('save', function(event) {return false;});
	technology_value.on('save', function(event) {return false;});
	important_features.on('save', function(event) {return false;});
	application.on('save', function(event) {return false;});
	intellectual_property.on('save', function(event) {return false;});
	beneficiary_industry.on('save', function(event) {return false;});
	advantages.on('save', function(event) {return false;});
	commercialization_status.on('save', function(event) {return false;});



</script>


<?php if ($CustomVAlidationErr != '') {?><script>swal({ title: "Alert", text: '<?php echo $CustomVAlidationErr; ?>', type: "warning" });</script><?php }?>