<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	.MyTeamsListingTab ul li:before { display:none; }
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Technology Transfer</h1>        
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">			
			<div class="col-md-12">
				<div class="formInfo">
					
					<section>
						<div class="product_details MyTeamsListingTab">

							<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
								<li class="nav-item"><a  href="javascript:void(0)" class="nav-link active" >My Technology Transfer</a></li>
								 <li class="nav-item"><a href="<?php echo base_url('technology_transfer/my_tech_applications'); //#TeamTab1 ?>" class="nav-link " >My Technology Transfer Applications</a></li>
							</ul>
							<!-- <ul class="nav nav-tabs align-item-center justify-content-center d-none" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link active" href="javascript:void(0)">My Webinar's</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo base_url('webinar/appliedWebinar'); ?>">Webinar's I have Applied To</a></li>
							</ul> -->
							
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="TeamTab1">
									
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
												<div class="table-responsive">
													<table id="blog-list" class="table table-bordered table-hover challenges-listing-page">
														<thead>
															<tr>
																<th scope="col">Technology Transfer ID</th>
																<th scope="col">Technology Transfer Name</th>
																<!-- <th scope="col">Author</th> -->
																<th scope="col">Application received (numbers)</th>
																<th scope="col">Reports</th>
																
																<th scope="col">Posted ON</th>
																<th scope="col">Status</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>							
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlogReportpopupLabel">Technology transfer delete</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				
				<!-- <div class="bgNewBox65">
				<p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
				<p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
				</div> -->
					
					<div class="form-group mt-4">
						
						<label for="popupKrDeleteComment" class="">Reason <em style="color: red">*</em></label>
						<textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
						<span id="popupKrDeleteComment_err" class='error'></span>
					</div>
				</div>	
				<input type="hidden" id="popupBlogIdHidden" name="">
				<div class="text-center pb-3">
					<button type="button" class="btn btn-primary" onclick="submit_delete_request()">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
      	<div>
        <h5  id="BlogLikesLabel"></h5>
        </div>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="col-md-12">
        <h5 id="IdDisp"></h5>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
  $(document).ready( function () 
	{	  
		var base_path = '<?php echo base_url() ?>';
		var table = $('#blog-list').DataTable({
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": base_path+"technology_transfer/my_listing",
				"type":"POST",
				"data":function(data) 
				{						
					/* data.techonogy_id = $("#techonogy_id").val();
					data.change_visibility = $("#change_visibility").val();
					data.c_status = $("#c_status").val();
					data.audience_pref = $("#audience_pref").val(); */
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": {
					401:function(responseObject, textStatus, jqXHR) {			
					},
				},
			}			
		});
	});
	
	function block_unblock_tt(id, type)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to "+type+" the selected content?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("technology_transfer/block_unblock_tt"); ?>',
					data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Resource sharing successfully "+type+"ed.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}


	function delete_tt(kr_id)
	{
				// $("#popupBlogId").html(kr_disp_id);
				$("#popupBlogIdHidden").val(kr_id);
				// $("#popupBlogTitle").html(kr_title);
				$("#popupKrDeleteComment").val('');
				$("#popupKrDeleteComment_err").html('');
				$("#BlogReportpopup").modal('show');
				$("#popupKrDeleteComment").focus();
			
	}

	function check_report_validation()
	{
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		if(popupKrDeleteComment.trim() == "") { $("#popupKrDeleteComment_err").html('Please enter the delete reason'); $("#popupKrDeleteComment").focus(); }
		else { $("#popupKrDeleteComment_err").html(''); }
	}

	function submit_delete_request(kr_id)
	{	
		check_report_validation();
		
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		var id = $("#popupBlogIdHidden").val();
		if(popupKrDeleteComment.trim() != "")		
		{
			$("#popupKrDeleteComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to delete the selected conent?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("technology_transfer/delete_tt"); ?>',
						data:{ 'id':id, 'popupKrDeleteComment':popupKrDeleteComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$("#BlogReportpopup").modal('hide');
							$(".token").val(data.token);
						
							$('#blog-list').DataTable().ajax.reload();	
							swal(
							{
								title: 'Success!',
								text: "Content successfully deleted.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}
	
	function delete_tt_old(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected conent?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("technology_transfer/delete_tt"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Resource sharing successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function show_connect_requests(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("technology_transfer/show_connect_requests_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);
					$('#IdDisp').html(data.id_disp)				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	


	function show_tech_reports(tech_id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("technology_transfer/show_tech_reported_ajax"); ?>',
			data:{ 'tech_id':tech_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>