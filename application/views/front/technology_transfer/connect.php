<style>
#popupTechTitle {font-size: 18px; color: #000;}
.modal-footer {justify-content: left; position: relative;}
.modal-open .modal .modal-dialog {top: 13%;}
.rightButton {position: absolute; right: 20px; top: auto;}

.flotingCss {-webkit-transform: translateY(16px) scale(1); transform: translateY(16px) scale(1);
cursor: pointer; color: #333; font-weight: 500; font-size: 14px;}

.form-group .floatinglabel {-webkit-transform: translateY(14px) scale(1); transform: translateY(14px) scale(1);cursor: pointer;}
textarea::placeholder {font-size: 15px; color:#CCC !important;}

</style>
<div class="modal fade" id="TechRequestpopup" tabindex="-1" role="dialog" aria-labelledby="TechRequestpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TechRequestpopupLabel">Technology Transfer Connect Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="downloadForm" name="downloadForm" role="form">
                <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
                <div class="">
                <p><strong>TNTTID :</strong> <span id="popupTechIdDisp"></span> 
                <h5> <span id="popupTechTitle"></span></h5>

                 <p>
                Thank you for contacting us. <br>
                We welcome enquiries from potential investors, entrepreneurs,
                potential licensees and other interested parties.

                </p>  
                </div>
                    
                    <div class="form-group mt-2">
                       <label for="popupTechRequestComment" class="flotingCss leftLable2" >Remarks </label>
                        <textarea class="form-control" id="popupTechRequestComment" name="popupTechRequestComment"  placeholder="Please let us know in brief about your plan / interest for this technology" rows="6"></textarea>
                  

                         <p>More information related to this topic will be sent to your registered Email ID</p>

                        <span id="popupTechRequestComment_err" class='error'></span>

                    </div>
                    <input type="hidden" id="tech_id">
                    <div class="form-group row captcha"  style=" margin-top:0px;">
                      <label class="col-lg-3 pr-0 control-label text-left cptcha_img" style="transform:translateY(-0px) scale(1)">
                        <span id="captImg"><?php echo $captcha; ?></span>
                          
                        </label>
                      <div class="col-lg-3">
                        <input type="text"  style="margin-top:10px;" name="code" id="captcha" placeholder="" class="form-control" > 
                        
                      </div>
                      <div class="col-lg-2">
                        <a style="margin-left:5px; margin-top:10px; color: #fff;" class="btn btn-primary refreshCaptcha_dw"><i class="fa fa-refresh"></i></a> 
                      </div>
                    </div>
                </div>  

               <!--  <div class="bgNewBox65">
                </div> -->
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>          
                    <button type="submit" class="btn btn-primary rightButton" >Connect</button>           
                </div>
            </form>         
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script>
   function connect_request_technology_tarnsfer(tect_id, ReportedFlag, disp_id, title,owner_name,date,is_closed=0)
   {
        var user_id = '<?php echo $this->session->userdata('user_id'); ?>';
        if(user_id == ''){
           swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
            return false;
        }

      module_id=40;
      if(check_permissions_ajax(module_id) == false){
              swal( 'Warning!','You don\'t have permission to access this page !','warning');
              return false;
      }
       // module_id=35;
       // if(check_permissions_ajax(module_id) == false){
       //     swal( 'Warning!','You don\'t have permission to access this page !','warning');
       //     return false;
       // }
       if (is_closed==1) {
        swal({
                   title: 'Alert!',
                   text: "This content is closed",
                   type: 'alert',
                   showCancelButton: false,
                   confirmButtonText: 'Ok'
               });
        return false;
       }
   
           if(ReportedFlag == "1")
           {
               $("#popupTechIdDisp").html(disp_id);
               $("#popupTechTitle").html(title);
               $("#tech_id").val(tect_id)
               $("#popupTechRequestComment_err").html('');
               $("#popupTechRequestComment").focus();
               $("#TechRequestpopup").modal('show');
           }
           else
           {
               swal(
               {
                   title: 'Alert!',
                   text: "You have already sent a connect request",
                   type: 'alert',
                   showCancelButton: false,
                   confirmButtonText: 'Ok'
               });
           }
       
   }   
   
   function check_tt_request_validation()
   {
       var popupTechRequestComment = $("#popupTechRequestComment").val();
       if(popupTechRequestComment.trim() == "") { $("#popupTechRequestComment_err").html('Please enter the comment'); $("#popupTechRequestComment").focus(); }
       else { $("#popupTechRequestComment_err").html(''); }
   }
   
   function submit_tt_request()
   {   
       // check_report_validation();
       // alert()
          var popupTechRequestComment = $("#popupTechRequestComment").val();
          var tech_id = $("#tech_id").val();
           $("#popupTechRequestComment_err").html('');
           swal(
           {  
               title:"Confirm?",
               // text: "Are you sure you want to send the connect request for this technology?",
               text:"Are you sure you want to submit?",
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Yes!'
           }).then(function (result) 
           { 
               if (result.value) 
               {
                   var cs_t =  $('.token').val();
                   $.ajax(
                   {
                       type:'POST',
                       url: '<?php echo site_url("technology_transfer/connect_request_ajax"); ?>',
                       data:{ 'tech_id':tech_id, 'popupTechRequestComment':popupTechRequestComment, 'csrf_test_name':cs_t },
                       dataType:"JSON",
                       success:function(data)
                       {
                          $("#report_blog_btn_outer_"+data.tech_id).html(data.response);
                          var app_id= data.app_id;
                           $(".token").val(data.token);
                           $("#TechRequestpopup").modal('hide');
                           
                           swal(
                           {
                               title: 'Success!',
                               text: "Your successfully submitted your request. Application ID: "+app_id,
                               type: 'success',
                               showCancelButton: false,
                               confirmButtonText: 'Ok'
                           });
                       }
                   });
               } 
           });
   }   

$(document).ready(function(){
             $('.refreshCaptcha_dw').on('click', function(){
                 $.get('<?php echo base_url().'technology_transfer/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
 });
$( "#downloadForm" ).validate({
      onfocusout: false,
      onkeyup: false,
      onclick: false,
      /* debug: false, */
      rules: {
      
       code: {
                required: true,
                  remote: {
                         url: "<?php echo base_url();?>technology_transfer/check_captcha_ajax",
                         type: "post",
                         async:false,

                  },
              },
             "files_to_download[]":{required:true}, 
      },
      messages: {
        code: {
          required: "Please Enter Captcha",
          remote : "Invalid Captcha."
        },        
      },
      errorElement: 'span',
         errorPlacement: function (error, element) {

          if (element.attr("name") == "files_to_download[]") 
        {
          error.insertAfter("#file_chek_err");
        }else{
           error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
        }
        
        
         },
         highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
         },
         unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
         },
        submitHandler: function () {
        
        submit_tt_request()

      }
    });

</script>