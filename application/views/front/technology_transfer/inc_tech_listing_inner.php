<?php $encObj =  New Opensslencryptdecrypt(); 
$read_more_url=base_url('technology_transfer/details/').base64_encode($res['tech_id']) ;
$read_more='<a href='.$read_more_url.' >read more</a>';

?>


<div class="boxShadow75 custom_box_class">
  

   <?php 

   if ($res['tile_image']!=''){
      $img_src=  base_url('uploads/technology_transfer/tile_image/').$res['tile_image'];
   }else{
    $img_src=  base_url('uploads/technology_transfer/defaltImg.jpg');

   }  
   ?>
   <img src="<?php echo $img_src; ?>" alt=" img" class="img-fluid" style="cursor: pointer;" onclick="window.open('<?php echo $read_more_url ?>','_self')">
   <ul style="text-align: center;">

    <li><span href="javascript:void(0)" style="cursor:auto; background: #FFF;
color: #c80032;"><i class="fa fa-ticket" aria-hidden="true"></i> <?php echo $res['id_disp']; ?></span> 
      </li>
  
     
     
   </ul>

   <ul>

      <li><span style="background: #FFF; color: #c80032;"><?php echo $res['DispTrl'] ?></span> </li>
      <!-- <li id="like_unlike_btn_outer_<?php echo $res['tech_id']; ?>">
         <span style="cursor:pointer;" ><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</span>
      </li> -->
      
      <li class="share_tech float-right">
         <span style="cursor:pointer;" onclick="share_blog('<?php echo site_url('technology_transfer/details/'.base64_encode($res['tech_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
      </li>
     
     
   </ul>
   <strong style="cursor: pointer;" onclick="window.open('<?php echo $read_more_url ?>','_self')" class="text-center tech_name"><?php if(strlen($res['technology_title']) > 30 ){ echo ucfirst(substr($res['technology_title'], 0,20) )."..."; } else {echo ucfirst($res['technology_title']);} ?>
      
   </strong>
   <p class="mt-2"> <strong>Offered By:</strong> <?php if(strlen($res['author_name']) > 25 ){ echo ucfirst(substr($res['author_name'], 0,25) )."..."; } else {echo ucfirst($res['author_name']);} ?></p>
   <div class="mt-3 mb-3 tech_details">
      <?php 
      $technology_abstract= strip_tags(htmlspecialchars_decode( $res['technology_abstract']));
      $technology_abstract = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $technology_abstract);
      $technology_abstract=trim($technology_abstract," ");
       $technology_abstract= str_replace('&nbsp;','',$technology_abstract);
      

      if(strlen($technology_abstract) > 79 ){ echo ucfirst(substr($technology_abstract, 0,80) )."...".$read_more; } else{ echo ucfirst($technology_abstract); } ?>
   </div>
   <?php if($res['DispTags'] != "" || $res['tag_other']!='') 
                                        { ?>
                                        <div >
                                        <strong>Tags:</strong> 
                                        <div class="owl-carousel mb-2 owl-theme tagSlider">
                                        <?php if($res['DispTags'] != "") 
                                        { 
                                            $tags_arr = explode("##",$res['DispTags']); 
                                             foreach($tags_arr as $tag)
                                                    {   ?>
                                                    <div><a style="cursor:auto;"><?php if(strlen($tag) > 20 ) {echo substr($tag,0, 20)."..." ; }else{ echo $tag; }; ?></a></div> 
                                                <?php } }   ?>
                                                
                                                <?php if ($res['DispTags'] == "" && $res['tag_other']!='') {
                                                    $style='cursor:auto;min-width:250px ';
                                                }else{
                                                    $style='cursor:auto;  ';
                                                } ?>

                                                <?php if ($res['tag_other']!=''){ 
                                                    $tag_other_arr=explode(',', $res['tag_other']);
                                                    foreach ($tag_other_arr as $key => $tag_other) {
                                                      ?>
                                                                <div><a style="<?php echo $style ?>">
                                                                    <?php 
                                                                    if(strlen($tag_other) > 20 ) {echo substr($tag_other,0, 20)."..." ; }else{ echo $tag_other; }; ?>

                                                                        
                                                                    </a></div>
                                                <?php  } } ?>
                                        </div>  
                                        </div>
                                        <?php } ?>
   <a href="<?php echo base_url('technology_transfer/details/').base64_encode($res['tech_id'])  ?>" class="btn btn-primary2">View Detail</a>
   <?php
      $date=date("d M, Y", strtotime( $res['created_on']));
      ?>
   <span id="report_blog_btn_outer_<?php echo $res['tech_id']; ?>"> 
   <?php if( $this->session->userdata('user_id')!= $res['user_id'] || 1){ ?>  

     <?php 
       $date=date("d M, Y", strtotime( $res['created_on']));
       if(in_array($res['tech_id'], $TechActiondata['requested_tech'])) { $Reported_label = "Request Already Sent"; $ReportedFlag = '0';  }
      else { $Reported_label = "Connect"; $ReportedFlag = '1'; } 
   ?>
  
   <?php if( $this->session->userdata('user_id')!= $res['user_id'] ){ ?> 
      <!-- connect_request_technology_tarnsfer() is defined in footer -->
       <button id="report_blog_btn_outer_<?php echo $res['tech_id']; ?>" type="submit" class="btn btn-primary float-right" onclick="connect_request_technology_tarnsfer('<?php echo base64_encode($res['tech_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $res['id_disp']; ?>', '<?php echo $res['technology_title']; ?>' , '<?php echo $res['author_name']; ?>','<?php echo $date; ?>','<?php echo $res['is_closed']; ?>')"  title="<?php echo $Reported_label; ?>">Connect</button>
       <?php } ?>
    
   
   <!-- <button type="submit" class="btn btn-primary float-right" onclick="connect_resource('<?php echo base64_encode($res['tech_id']); ?>', '<?php echo $res['id_disp']; ?>', '<?php echo $res['technology_title']; ?>' ,'<?php echo $date; ?>')" >Connect</button> -->
   
   <?php } ?>
   </span>
</div>
