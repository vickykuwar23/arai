<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<?php  $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
   .step_wiz_buttons_add_tech {
    position: absolute !important;
    top: 30px;
   }
   .previous_img_outer { position: relative; }
   .previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
   .previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
   .previous_img_outer_logo { position: relative; }
   .previous_img_outer_logo img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
   .previous_img_outer_logo a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
   p.team_member_display_cnt { text-align:center; font-weight:500; }
   /* .formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
   .formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; } */
   .sw-theme-arrows .sw-toolbar{ overflow:hidden; display:block;}
   /* .formInfo .btn-group.sw-btn-group-extra {
   position: absolute;
   display: block;
   right: 0;
   } */
   .btn-group.mr-2.sw-btn-group-extra {
   float: left;
   position: inherit;
   }
   /* .formInfo .btn-group.sw-btn-group-extra{position:absolute;left:15px} */
   .custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
   .BtnAsLabel, .BtnAsLabel:hover, .BtnAsLabel:active, .BtnAsLabel:focus, .BtnAsLabel:disabled { background-color: #333 !important; opacity: 1; border-color: #333 !important;
   }
   .datepicker table tr td, .datepicker table tr th 
   {
   border-radius: 0;
   border: 1px solid rgba(0,0,0,0.1);
   }
   .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover 
   {
   background: rgba(0,0,0,0.1);
   color: rgba(0,0,0,0.4);
   cursor: not-allowed;
   border: 1px solid #fff;
   }
   th.datepicker-switch, th.next, th.prev 
   {
   border: none !important;
   }
   .sw-btn-group{ display:block}
   .sw-btn-next {
   position: absolute !important;
   right: 0 !important;
   /* width: 100%; */
   }
   .step_wiz_buttons {
   position: absolute !important;
   right: 0 !important;
   }
   .step_wiz_buttons .btnfinish{ margin-right:5px}
</style>
<div id="home-p" class="home-p pages-head3 text-center">
   <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">Technology Transfer</h1>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb wow fadeInUp">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Technology Transfer </li>
         </ol>
      </nav>
   </div>
</div>
<section id="registration-form" class="inner-page">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="formInfo">
               <?php //echo validation_errors(); ?>
               <form method="POST" action="" id="ApplyChallengeForm" name="ApplyChallengeForm" enctype="multipart/form-data">
                  <div id="smartwizard_apply_challenge">
                     <ul>
                        <li><a href="#step-1">Step 1<br /><small></small></a></li>
                        <li><a href="#step-2">Step 2<br /><small></small></a></li>
                        <li><a href="#step-3">Step 3<br /><small></small></a></li>
                        <li><a href="#step-4">Step 4<br /><small></small></a></li>
                     </ul>
                     <div>
                        <div id="step-1" class="mt-4">
                           <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" value="<?php if($mode == 'Add') { echo set_value('technology_title'); } else { echo $form_data[0]['technology_title']; } ?>" name="technology_title" id="technology_title" maxlength="255" autofocus>
                                    <label class="form-control-placeholder floatinglabel">Technology Transfer Title<em>*</em></label>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="author_name" id="author_name" value="<?php if($mode == 'Add') { echo set_value('author_name'); } else { echo $form_data[0]['author_name']; } ?>" required  maxlength="250">
                                    <label class="form-control-placeholder floatinglabel" for="author_name">Proposed By/Offered By<em>*</em></label>
                                    <span class="error"><?php echo form_error('author_name'); ?></span>
                                 </div>
                              </div>
                              <?php
                                 $dynamic_cls = 'd-none';
                                 $full_img = $onclick_fun = '';
                                 if($mode == 'Update' && $form_data[0]['tile_image'] != '' && file_exists('./uploads/technology_transfer/tile_image/'.$form_data[0]['tile_image']))
                                 {	
                                 	$dynamic_cls = '';
                                 	$full_img = base_url().'uploads/technology_transfer/tile_image/'.$form_data[0]['tile_image'];
                                 	$onclick_fun = "remove_tile_img('1','".$encrypt_obj->encrypt('arai_technology_transfer')."', '".$encrypt_obj->encrypt('tech_id')."', '".$encrypt_obj->encrypt($form_data[0]['tech_id'])."', 'tile_image', '".$encrypt_obj->encrypt('./uploads/technology_transfer/tile_image/')."', 'Team Banner')";
                                 } ?>
                              <div class="col-md-6">
                                 <div class="col-sm-12 <?php echo $dynamic_cls; ?>" id="tile_image_outer">
                                    <div class="form-group">
                                       <div class="previous_img_outer">
                                          <img src="<?php echo $full_img; ?>">
                                          <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group upload-btn-wrapper">
                                       <button class="btn btn-upload"><i class="fa fa-plus"> </i> Tile Image</button>	
                                       <input type="file" class="form-control" name="tile_image" id="tile_image" />
                                       <div class="clearfix"></div>
                                       <span class="small">Only .jpg, .jpeg, .png image formats below 2MB are accepted</span>
                                       <div class="clearfix"></div>
                                       <?php if(form_error('tile_image')!=""){ ?>
                                       <div class="clearfix"></div>
                                       <span class="error"><?php echo str_replace("filetype","file type",form_error('tile_image')); ?></span> <?php } ?>
                                       <?php if($tile_image_error!=""){ ?>
                                       <div class="clearfix"></div>
                                       <span class="error"><?php echo str_replace("filetype","file type",$tile_image_error); ?></span> <?php } ?>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 $dynamic_cls = 'd-none';
                                 $full_img = $onclick_fun = '';
                                 if($mode == 'Update' && $form_data[0]['company_logo'] != '' && file_exists('./uploads/technology_transfer/company_logo/'.$form_data[0]['company_logo']))
                                 {	
                                 	$dynamic_cls = '';
                                 	$full_img = base_url().'uploads/technology_transfer/company_logo/'.$form_data[0]['company_logo'];
                                 	$onclick_fun = "remove_logo_img('1','".$encrypt_obj->encrypt('arai_technology_transfer')."', '".$encrypt_obj->encrypt('tech_id')."', '".$encrypt_obj->encrypt($form_data[0]['tech_id'])."', 'company_logo', '".$encrypt_obj->encrypt('./uploads/technology_transfer/company_logo/')."', 'Team Banner')";
                                 } ?>
                              <div class="col-md-6">
                                 <div class="col-sm-12 <?php echo $dynamic_cls; ?>" id="company_logo_outer">
                                    <div class="form-group">
                                       <div class="previous_img_outer_logo">
                                          <img src="<?php echo $full_img; ?>">
                                          <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group upload-btn-wrapper">
                                       <button class="btn btn-upload"><i class="fa fa-plus"> </i>Company Logo</button>	
                                       <input type="file" class="form-control" name="company_logo" id="company_logo" />
                                       <div class="clearfix"></div>
                                       <span class="small">Only .jpg, .jpeg, .png image formats below 2MB are accepted</span>
                                       <div class="clearfix"></div>
                                       <?php if(form_error('company_logo')!=""){ ?>
                                       <div class="clearfix"></div>
                                       <span class="error"><?php echo str_replace("filetype","file type",form_error('company_logo')); ?></span> <?php } ?>
                                       <?php if($company_logo_error!=""){ ?>
                                       <div class="clearfix"></div>
                                       <span class="error"><?php echo str_replace("filetype","file type",$company_logo_error); ?></span> <?php } ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-12">
                                 <div class="form-group">
                                    <textarea  class="form-control" name="company_profile" id="company_profile"><?php if($mode == 'Add') { echo set_value('company_profile'); } else { echo $form_data[0]['company_profile']; } ?></textarea>
                                    <label class="form-control-placeholder" for="company_profile" style="top:-25px !important">Company Profile </label>
                                    <span class="error"><?php echo form_error('company_profile'); ?></span>
                                 </div>
                              </div>
                              <div class="col-sm-12">
                                 <div class="form-group"> 
                                    <textarea  class="form-control" name="technology_abstract" id="technology_abstract"><?php if($mode == 'Add') { echo set_value('technology_abstract'); } else { echo htmlspecialchars_decode($form_data[0]['technology_abstract']); } ?></textarea>
                                    <label style="top:-25px !important" class="form-control-placeholder" for=""> Abstract of the Technology available for transfer  <em style="color:red;">*</em></label>
                                    <span><?php echo form_error('technology_abstract'); ?></span>
                                 </div>
                              </div>
                              <div class="col-sm-12">
                                 <div class="form-group">
                                    <textarea  class="form-control" name="technology_value" id="technology_value"><?php if($mode == 'Add') { echo set_value('technology_value'); } else { echo $form_data[0]['technology_value']; } ?></textarea>
                                    <label style="top:-25px !important" class="form-control-placeholder" for=""> Value Proposition of the Technology available for transfer <em style="color:red;">*</em></label>
                                    <span><?php echo form_error('close_date'); ?></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="step-2" class="mt-4">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="important_features" id="important_features"><?php if($mode == 'Add') { echo set_value('important_features'); } else { echo $form_data[0]['important_features']; } ?></textarea>
                                    <label style="top:-25px !important" class="form-control-placeholder floatinglabel" for="important_features" style="margin-top: -25px;">Important Features/ Specification  <em></em></label>
                                    <span class="error"><?php echo form_error('important_features'); ?></span>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="application" id="application"><?php if($mode == 'Add') { echo set_value('application'); } else { echo $form_data[0]['application']; } ?></textarea>
                                    <label class="form-control-placeholder floatinglabel" for="application" style="margin-top: -25px;">Application <em></em></label>
                                    <span class="error"><?php echo form_error('application'); ?></span>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="intellectual_property" id="intellectual_property"><?php if($mode == 'Add') { echo set_value('intellectual_property'); } else { echo $form_data[0]['intellectual_property']; } ?></textarea>
                                    <label class="form-control-placeholder floatinglabel" for="intellectual_property" style="margin-top: -25px;">Intellectual Property <em></em></label>
                                    <span class="error"><?php echo form_error('intellectual_property'); ?></span>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="beneficiary_industry" id="beneficiary_industry"><?php if($mode == 'Add') { echo set_value('beneficiary_industry'); } else { echo $form_data[0]['beneficiary_industry']; } ?></textarea>
                                    <label class="form-control-placeholder floatinglabel" for="beneficiary_industry" style="margin-top: -25px;">Beneficiary Industry <em></em></label>
                                    <span class="error"><?php echo form_error('beneficiary_industry'); ?></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="step-3" class="mt-4">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="advantages" id="advantages"><?php if($mode == 'Add') { echo set_value('advantages'); } else { echo $form_data[0]['advantages']; } ?></textarea>
                                    <label class="form-control-placeholder floatinglabel" for="advantages" style="margin-top: -25px;">Advantages <em></em></label>
                                    <span class="error"><?php echo form_error('advantages'); ?></span>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <textarea  type="text" class="form-control" name="commercialization_status" id="commercialization_status"><?php if($mode == 'Add') { echo set_value('commercialization_status'); } else { echo $form_data[0]['commercialization_status']; } ?></textarea>
                                    <label class="form-control-placeholder floatinglabel" for="commercialization_status" style="margin-top: -25px;">Commercialization Status <em>*</em></label>
                                    <span class="error"><?php echo form_error('commercialization_status'); ?></span>
                                 </div>
                              </div>
                           
                              <div class="col-md-7">
                                 <div class="form-group">
                                    <label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Technology Readiness Status <em>*</em></label>
                                    <select class="form-control select2_common" name="trl" id="trl" data-placeholder="Technology Readiness Status"  >
                                       <?php if(count($trl_data) > 0)
                                          {	
                                          	foreach($trl_data as $res)
                                          	{	
                                          		$trl_arr = array();
                                          		if($mode == 'Add') { if(set_value('trl[]') != "") { $trl_arr = set_value('trl[]'); } }
                                          	else { $trl_arr = explode(",",$form_data[0]['trl']); } ?>
                                       <option data-id='<?php echo $res['trl_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$trl_arr)) { echo 'selected'; } ?> ><?php echo $res['trl_name']; ?></option>
                                       <?php }
                                          } ?>
                                    </select>
                                    <?php if($trl_error!=""){ ?> 
                                    <div class="clearfix"></div>
                                    <label class="error"><?php echo $trl_error; ?></label> <?php } ?>
                                 </div>
                              </div>
                              
                              <div class="col-md-7">
                                 <div class="form-group">
                                    <label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Tags <em>*</em> </label>
                                    <select class="form-control select2_common" name="tags[]" id="tags" data-placeholder="Tags" multiple  onchange="show_hide_tag_other()">
                                       <?php if(count($tag_data) > 0)
                                          {	
                                          	foreach($tag_data as $res)
                                          	{	
                                          		$tags_arr = array();
                                          		if($mode == 'Add') { if(set_value('tags[]') != "") { $tags_arr = set_value('tags[]'); } }
                                          	else { $tags_arr = explode(",",$form_data[0]['tags']); } ?>
                                       <option data-id='<?php echo $res['tag_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
                                       <?php }
                                          } ?>
                                    </select>
                                    <?php if($tags_error!=""){ ?> 
                                    <div class="clearfix"></div>
                                    <label class="error"><?php echo $tags_error; ?></label> <?php } ?>
                                 </div>
                              </div>
                              <div class="col-md-5" id="tag_other_outer" style="margin-top:26px;">
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php if($mode == 'Add') { echo set_value('tag_other'); } else { echo $form_data[0]['tag_other']; } ?>">
                                    <label class="form-control-placeholder floatinglabel">Other Tag <em>*</em></label>
                                    <?php if(form_error('tag_other')!=""){ ?> 
                                    <div class="clearfix"></div>
                                    <label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="step-4" class="mt-4">
                           
                        <div class="row mt-4">
                                 <div class="col-md-6">
                                    <label class="form-control-placeholder floatinglabel">Do you want to add Images related to the content</label>
                                    <label class="switch" style="margin-top:25px;">
                                    <input type="hidden" name="include_files" value="0">   
                                    <input type="checkbox" value="1" id="include_files" name="include_files" <?php if($mode == 'Update') { if($form_data[0]['include_files']=='1'){echo 'checked';} } ?>> 
                                    <span class="slider round"></span>
                                    </label>
                                 </div>
                           </div>
                           

                     <?php if($mode == 'Update' && $form_data[0]['include_files']==1){
                           $display_files= '';
                        }else{
                           $display_files= 'd-none';
                        } ?>
                           <div class="files_div <?php echo $display_files ?>">
                           <div class="file-details mt-2 " style="min-height:100px;">
                              <div class="row" ><div class="col-md-12 mb-2 mt-2"><h4 class="titleBox">Files to be Published  (Total files  data size limited to 5 MB Limited) </h4></div></div>
                              <?php /* <div class="form-group"><label class="form-control-placeholder"></label></div><br> */ ?>
                              <?php if($mode == 'Update' && count($files_data) > 0)
                                 {	
                                 	echo '<div class="row">';
                                 	foreach($files_data as $res)
                                 	{	?>
                              <div class="col-md-2" id="tech_files_outer<?php echo $res['tech_id']."_".$res['file_id']; ?>">
                                 <div class="file-list">
                                    <a href="javascript:void(0)" onclick="remove_tech_file('<?php echo $res['tech_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close"><i class="fa fa-remove"></i></a>
                                    <a class="file_ext_title" href="<?php echo base_url().'uploads/technology_transfer/tech_files/'.$res['file_name']; ?>" target="_blank">
                                       <?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
                                          if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/technology_transfer/tech_files/'.$res['file_name']; }
                                          else { $disp_img_name = ''; } ?>
                                       <?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
                                          else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
                                       <!--span><i class="fa fa-file"></i></span> View-->
                                    </a>
                                 </div>
                              </div>
                              <?php	}
                                 echo '</div>';
                                 }	?>
                              <input type="hidden" name="customFileCount" id="customFileCount" 
                              value="<?php if($mode == 'Add') { echo 0; } else { echo count($files_data); } ?>">
                              <div class="row">
                                 <div id="last_team_file_id"></div>
                                 <div class="col-md-2 custom-file" id="addFileBtnOuter">
                                    <label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
                                    <input type="file" class="form-control fileUploadTeams" name="tech_files[]" id="file-upload" />
                                    <div class="tech_files_err"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="tech_files_err_new" id="tech_files_err_new"></div>
                           </div>

                           <div class="row mt-3">
                                 <div class="col-md-6">
                                    <label class="form-control-placeholder floatinglabel">Do you want to add Video Links related to the content</label>
                                    <label class="switch" style="margin-top:25px;">
                                    <input type="hidden" name="video_check" value="0">   
                                    <input type="checkbox" id="video_check" value="1" name="video_check" <?php if($mode == 'Update') { if($form_data[0]['video_check']=='1'){echo 'checked';} } ?>> 
                                    <span class="slider round"></span>
                                    </label>
                                 </div>
                           </div>


            <?php if($mode == 'Update' && $form_data[0]['video_check']==1){ ?>
            <div class="row mb-2 mt-2" ><div class="col-md-12"><h4 class="titleBox">Video Embed Links ( upto 6 Videos)</h4></div></div>
            <div class="video_wrapper "> 
             <?php  foreach ($video_data as $key => $video) { ?>
               
               <div class="appended_div_<?php echo $key ?>">
                  <div class="row video_row ">
                     <div class="col-md-4">
                        <div class="form-group">
                           <input type="text" class="form-control video_caption" name="video_caption[]" id="caption<?php echo $key+1 ?>" value="<?php if($mode == 'Add') { echo set_value('video_caption'); } else { echo $video['video_caption']; } ?>">
                           <label class="form-control-placeholder floatinglabel">Caption</label>
                           <?php if(form_error('video_caption')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('video_caption'); ?></label> <?php } ?>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <input type="text" class="form-control video_url" name="video_url[]" id="video_url<?php echo $key+1 ?>" value="<?php if($mode == 'Add') { echo set_value('video_url'); } else { echo $video['video_url']; } ?>">
                           <label class="form-control-placeholder floatinglabel">Link</label>
                           <?php if(form_error('video_url')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('video_url'); ?></label> <?php } ?>
                        </div>
                     </div>
                     <?php if ($key==0){ ?>
                        
                     <div class="col-md-2">
                        <div class="form-group">
                        <button type="button" class="btn btn-success btn-sm mt-4" onclick="append_video_row()">+</button>
                        </div>
                     </div>
                     <?php } else{ ?>
                     <div class="col-md-2">
                        <div class="form-group">
                        <button type="button" class="btn btn-danger btn-sm mt-4" onclick="remove_video_row('<?php echo $key ?>')">-</button>
                        </div>
                     </div>
                     <?php } ?>  

                  <input type="hidden" value="<?php if($mode == 'Add') { echo '1'; } else { echo count($video_data); } ?>" name="video_warapper_count" id="video_warapper_count"> 
                  </div>
                  </div>
                     
                  <?php } ?>
                  </div>
                 <?php }else{ ?>
                  <div class="video_wrapper d-none">
                    <div class="row mb-2 mt-2" ><div class="col-md-12"><h4 class="titleBox">Video Embed Links ( upto 6 Videos)</h4></div></div>  
                  <div class="row video_row ">
                     <div class="col-md-4">
                        <div class="form-group">
                           <input type="text" class="form-control video_caption" name="video_caption[]" id="caption1" value="">
                           <label class="form-control-placeholder floatinglabel">Caption</label>
                           <?php if(form_error('caption')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('caption'); ?></label> <?php } ?>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <input type="text" class="form-control video_url" name="video_url[]" id="video_url1" value="">
                           <label class="form-control-placeholder floatinglabel">Link</label>
                           <?php if(form_error('video_url')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('video_url'); ?></label> <?php } ?>
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="form-group">
                        <button type="button" class="btn btn-success btn-sm mt-4" onclick="append_video_row()">+</button>
                        </div>
                     </div>

                  <input type="hidden" value="1" name="video_warapper_count" id="video_warapper_count"> 
                  </div>
                  </div>
                  <?php } ?>

                  <div class="row">
                     
                     <div class="col-md-12">
                        <div class="">
                          <input type="checkbox" class="accept_terms" id="accept_terms" name="accept_terms"> 
                           <label class="" for="accept_terms">I hereby agree to Technovuus Terms and Conditions for Publishing this content <em style="color: red">*</em></label>
                        </div>
                           <div id="accept_terms_err"></div>
                     </div>
                  </div>


                           

                        </div>
                        <br>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<?php $this->load->view('front/technology_transfer/add_script'); ?>

