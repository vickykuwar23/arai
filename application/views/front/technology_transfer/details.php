<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<style>
   .owl-carousel .item-video{height:300px}
   /* .btn_outer {margin-top: -35px;} */

   #connect_btn_outer_5, #connect_btn_outer_4, #connect_btn_outer_6, #connect_btn_outer_3, #connect_btn_outer_2, #connect_btn_outer_1, #connect_btn_outer_7 , #connect_btn_outer_8, #connect_btn_outer_9, #connect_btn_outer_10, #connect_btn_outer_11, #connect_btn_outer_12, #connect_btn_outer_13, #connect_btn_outer_14, #connect_btn_outer_15 {
	margin-top: -35px;
}

   .tag2 ul li:first-child {display: block; margin-bottom:5px !important;}
   #preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
   #preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
   border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
   .boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
   .boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
   /* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
   .tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag ul li a:hover{ background: #000; color: #32f0ff; }
   .shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .shareList ul li a:hover{ background: #000; color: #32f0ff; }
   /** Color Change**/
   .nav-tabs{border-bottom:1px solid #32f0ff}
   .title-bar .heading-border{background-color:#c80032}
   .search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
   .filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
   .filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
   /**Questions And Answers Forum**/
   #AnswersForum h2{font-size:1.75rem}
   .bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}
   .imgBoxView img {border-radius: 0%;}
   .likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
   .likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
   .likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
   .likeShare ul li:last-child{ float: right; margin: 0;}
   .likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
   .likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
   .dropdown-menu{ padding: 0px !important;}
   .dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
  
   .bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
   .imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}
   /**Blog Icon Added Css**/
   .blogProfileicon{ position: absolute; top:8px; right:25px; width: 45px; height: 45px; border-radius:5px; background:#32f0ff;  padding: 5px; text-align: center;}
   .blogProfileicon img{ width: 50%;}
   .boxShadow75{margin:0 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px;}
   .boxShadow75:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
   .boxShadow75 .scroll_tags{ padding: 0; margin:13px 0; list-style: none;}
   .boxShadow75 .scroll_tags li{ padding: 0; margin: 0; display: inline-block; font-size: 15px; position: relative;}
   .boxShadow75 .scroll_tags li span{ background: #FFF; color:#c80032; border:solid 1px #c80032; padding: 5px; border-radius: 5px;}
   .boxShadow75 .scroll_tags li a {background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .boxShadow75 .scroll_tags li a:hover { background: #000; color: #32f0ff;}
   .tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
   .tag2 ul li{ padding: 0; margin:0 5px 10px 0 !important;}
   .tag2 ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag2 ul li a:hover{ background: #000; color: #32f0ff; }
   .boxShadow75 .btn-primary2{color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary2:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none; }
   .boxShadow75 .btn-primary{color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none; }
   #flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
   #flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
   #flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
   a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 
   
   /*.shareKR ul li span{  background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none;  cursor:pointer; }*/
   /* .shareKR ul li span:hover{ background: #FFF; color: #c80032; } */
   .ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
   .ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}
   .gallerySlider.owl-carousel .owl-dot,.gallerySlider.owl-carousel .owl-nav .owl-next,.gallerySlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
   .gallerySlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
   .gallerySlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
   .gallerySlider .owl-nav.disabled{display:block}
   .gallerySlider .owl-nav{position:absolute; bottom:-15px; width:100%}


   .gallerySlider .owl-nav{position:absolute; bottom:-15px; width:100%}

   .gallerySlider .owl-nav [class*=owl-]:hover{background:#c80032!important;border:solid 1px #c80032;color:#FFF}

   .gallerySlider .owl-nav .owl-prev{background:#fff;color:#c80032; border: solid 1px #c80032;}
   .gallerySlider .owl-nav .owl-next{background:#fff;color:#c80032; border: solid 1px #c80032;}

   /* .gallerySlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #96c832;color:#96c832} */

   .gallerySlider .owl-item{ padding:10px;}
   .gallerySlider .owl-item img{width:100%;height: 240px; }
   .gallerySlider .owl-dots{position:absolute;width:100%;bottom:25px}

   .tech_video_slider.owl-carousel .owl-dot,.tech_video_slider.owl-carousel .owl-nav .owl-next,.tech_video_slider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
   .tech_video_slider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
   .tech_video_slider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
   .tech_video_slider .owl-nav.disabled{display:block}
   .tech_video_slider .owl-nav{position:absolute; bottom:-15px; width:100%}


   .tech_video_slider .owl-nav{position:absolute; bottom:-15px; width:100%}

   .tech_video_slider .owl-nav [class*=owl-]:hover{background:#c80032!important;border:solid 1px #c80032;color:#FFF}

   .tech_video_slider .owl-nav .owl-prev{background:#fff;color:#c80032; border: solid 1px #c80032;}
   .tech_video_slider .owl-nav .owl-next{background:#fff;color:#c80032; border: solid 1px #c80032;}

   /* .tech_video_slider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #96c832;color:#96c832} */

   .tech_video_slider .owl-item{ padding:10px;}
   .tech_video_slider .owl-item img{width:100%; }
   .tech_video_slider .owl-dots{position:absolute;width:100%;bottom:25px}

.detailsPage h4 {font-size: 18px; color: #000;}
.detailsPage ul {list-style-type: none; padding: 0; font-size: 15px;}
.detailsPage li {position: relative; padding-left: 15px; margin: 0 0 5px; color: #000;}
.detailsPage li::before {position: absolute; left: 0; content: '-'; font-weight: bold; color: #000;width: 14px;height: 27px;}


 .dropdown-menu a:hover {background:#f0f0f0 !important; color: #000 !important;  }

#popupTechTitle {font-size: 15px; color: #000;}
.modal-footer {justify-content: left; position: relative;}
.modal-open .modal .modal-dialog {top: 13%;}
.rightButton {position: absolute; right: 20px; top: auto;}

.company_logo {width: 100%; min-height: 240px;}
.tile_image {width: 100%; min-height: 240px;}
.home-p.pages-head4{min-height:30vh !important; height:30vh;}

.btn_outer {margin-top: -35px;}
.btn_outer1 {margin-bottom: 40px;}

.tech_video_slider iframe {
    width: 100%;
    min-height: auto !important;
}

.dynamicContent strong {
	display: block;
	margin: 10px  0;
}

.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown_div {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  right: 0px;
  top:30px;

}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown_div a:hover {background-color: #ddd;}

.show_dropdown {display: block !important;}

.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}


.btn_report {background: #FFF !important; color: #c80032 !important;}
.btn_report:hover {background: #c80032 !important; color: #FFF !important;}

.report_menu ul{
  margin-top: 15px !important;
}
.report_menu ul li span{  background: #FFF; border:solid 1px #c80032; color: #c80032; text-decoration: none;  cursor:pointer; padding: 5px;border-radius: 5px; }
.report_menu ul li span:hover{ background: #c80032; color: #FFF; }
.report_menu ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
 .report_menu ul li{ padding: 0; margin:0 5px 10px 0 !important;}
 .report_menu ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
 .report_menu ul li a:hover{ background: #000; color: #32f0ff; }
</style>

 <style>
                .techReadinessStatus {
                    height: 150px;
                }

                .techReadinessStatus ul {
                    list-style: none;
                    font-size: 15px;
                    padding: 0;
                    margin: 0 0 15px 0;
                    display: flex;
                    align-items: flex-end;
                }

                .techReadinessStatus ul li {
                    padding: 0px;
                    margin: 0 0 0 -1px;
                    float: left;
                    width: 120px;

                }

                .techReadinessStatus ul li p {
                    height: 45px;
                    text-align: center;
                    color: #000;
                    font-size: 11px;
                    font-weight: bold;
                    margin: 0;
                    padding: 4px 0 0 0;
                    /* vertical-align: middle; */
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }

                .techReadinessStatus ul li:nth-child(odd) {
                    /* background: #000; */
                }

                .techReadinessStatus ul li:nth-child(even) p {
                    /* background: red; */
                    position: relative;
                    top: 85px;
                }

                .techReadinessStatus ul li span {
                    padding: 10px 10px 8px 10px;
                    margin: 0 2px 0 0;
                    display: block;
                    text-align: center;

                    border-top: solid 1px #FFF;
                    border-left: solid 1px #FFF;
                    border-bottom: solid 1px #FFF;
                }


                .bg1 {
                    position: relative;
                    background: #ffc000;
                }

                .bg1::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #ffc000;
                    border-bottom: 20px solid transparent;
                }

                .bg1::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }


                .bg2 {
                    position: relative;
                    background: #e3f90c;
                }

                .bg2::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #e3f90c;
                    border-bottom: 20px solid transparent;
                }

                .bg2::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }



                .bg3 {
                    position: relative;
                    background: #94f319;
                }

                .bg3::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #94f319;
                    border-bottom: 20px solid transparent;
                }

                .bg3::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }



                .bg4 {
                    position: relative;
                    background: #52ed24;
                }

                .bg4::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #52ed24;
                    border-bottom: 20px solid transparent;
                }

                .bg4::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }

                .bg5 {
                    position: relative;
                    background: #30e845;
                }

                .bg5::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #30e845;
                    border-bottom: 20px solid transparent;
                }

                .bg5::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }

                .bg6 {
                    position: relative;
                    background: #3be287;
                }

                .bg6::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #3be287;
                    border-bottom: 20px solid transparent;
                }

                .bg6::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }


                .bg7 {
                    position: relative;
                    background: #46debf;
                }

                .bg7::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #46debf;
                    border-bottom: 20px solid transparent;
                }

                .bg7::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }


                .bg8 {
                    position: relative;
                    background: #51c7d9;
                }

                .bg8::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #51c7d9;
                    border-bottom: 20px solid transparent;
                }

                .bg8::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }


                .bg9 {
                    position: relative;
                    background: #5b9bd5;
                }

                .bg9::after {
                    position: absolute;
                    right: -30px;
                    top: 0;
                    z-index: 99;
                    content: "";
                    border-top: 20px solid transparent;
                    border-left: 30px solid #5b9bd5;
                    border-bottom: 20px solid transparent;
                }

                .bg9::before {
                    position: absolute;
                    right: -32px;
                    top: 0;
                    content: "";
                    z-index: 99;
                    border-top: 20px solid transparent;
                    border-left: 30px solid #FFF;
                    border-bottom: 20px solid transparent;
                }
                .active_trl{
                  color:#000; font-weight: bold; 
                  animation: blinker 1s linear infinite;

                }
              



@keyframes blinker {
  50% {
    opacity: 0;
  }
}
</style>

<div id="preloader-loader" style="display:none;"></div>
<?php /* 
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down"
   style="background: url(<?php if($form_data[0]['tile_image']!=''){echo base_url('uploads/technology_transfer/tile_image').$form_data[0]['tile_image'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size:100% 100% !important;" >
    */ ?>

   <div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down"
   style="background: url(<?php echo base_url('assets/img/aboutus.jpg'); ?>) no-repeat center top; background-size:cover !important;" 
   >
   <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Technology Transfer </h1>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb wow fadeInUp">
            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Technology Transfer Details </li>
         </ol>
      </nav>
   </div>
</div>
<section id="story" data-aos="fade-up">
   <div class="tab-content active" id="myTabContent">
      <div class="tab-pane active show fade" id="KnowledgeRepository">
         <div class="container">
            <div class="col-md-12">
               <div class="boxShadow75 shareKR" style="word-break: break-all;">
                  <div class="row">
                     <div class="col-md-3">
                        <?php 
                           $img_src=  base_url('uploads/technology_transfer/defaltImg.jpg');

                           if ($form_data[0]['tile_image']!=''){
                              $img_src=  base_url('uploads/technology_transfer/tile_image/').$form_data[0]['tile_image'];
                           }
                           ?>
                           <img src="<?php echo $img_src; ?>" alt=" img" class="img-fluid tile_image">

                     </div>  
                     <div class="col-md-6">
                        <div class="top_details_tt mt-5">
                        <strong><?php echo ucfirst($form_data[0]['technology_title']); ?></strong>
                        <p class="mb-0"><strong>Proposed/Offered by:</strong> <?php echo $form_data[0]['author_name']; ?></p>
                        <p class="mb-0"><strong>TRL:</strong> <?php echo $form_data[0]['DispTrl']; ?></p>
                        <p class="mb-0"><strong>TNTTID:</strong> <?php echo $form_data[0]['id_disp']; ?></p>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <?php 
                           $img_src=  base_url('uploads/technology_transfer/tile_image/11.jpg');

                           if ($form_data[0]['company_logo']!=''){
                              $img_src=  base_url('uploads/technology_transfer/company_logo/').$form_data[0]['company_logo'];
                           }
                           ?>
                           <img src="<?php echo $img_src; ?>" alt=" img" class="img-fluid company_logo">

                     </div>
                  </div>

                    <div class="report_menu mt-3">
                            <ul class="float-right">
                              
                            <?php  if(in_array($form_data[0]['tech_id'], $TechActiondata['reported_tech'])) { $Reported_label = "Knowledge Repository Already Reported"; $ReportedFlag = '0';  }
                            else { $Reported_label = "Report Knowledge Repository"; $ReportedFlag = '1'; } ?>
                            
                            <?php if($user_id!=$form_data[0]['user_id']){ ?>

                            <li>
                              <div class="dropdown_div">
                                    <span style="cursor: pointer;" onclick="ShowDropdown()" class="dropbtn pl-2 pr-2"><i   class="fa fa-ellipsis-v" ></i></span>
                                    <div id="myDropdown" class="dropdown-content">
                                      <a id="report_btn_outer_<?php echo $form_data[0]['tech_id']; ?>" class="btn btn-sm btn-danger btn_report" href="javascript:void(0)" title="<?php echo $Reported_label; ?>" onclick="report_tech('<?php echo base64_encode($form_data[0]['tech_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['id_disp']; ?>', '<?php echo $form_data[0]['technology_title']; ?>')">Report</a>
                                      
                                    </div>
                                  </div>
                            </li>
                        <?php } ?> 
                        </ul>

                        

                        </div>


                  <div class="row">
                     <div class="col-md-12 mt-4">
                        
                        <div class="desc pt-3">

                        <!-- <div class="detailsPage">
                        <h4>ABSTRACT:</h4>
                        <p>The Automotive Research Association India is working on internal R&D program titled “SwayamGO”. The main objective of this program is to work on improvement of safety making use of latest electronic and software technologies. As part of this program, ARAI has made prototype of Autonomous vehicle using standard electric passenger car. The actuators of the vehicle (accelerator pedal, brake and steering) have been automated.</p>
                        <h4>VALUE PROPOSITION:</h4>
                        <p>This ARAIs autonomous vehicle deployment platform offers complete packaged solution for customers to jump start their ADAS/AV functionality development. It offers options for sensor selection as per customer’s specific requirement. The platform can be used for on road deployment, development and validation of ADAS/AV functionality.
This complete packaged vehicle prototype can be offered as solution to interested parties. It will help customers to jump start their ADAS/AV functionality development.</p>

                        <h4>FEATURES: </h4>
                        <ul>
                        <li>For a typical electric 4-wheeler (Car, LCV, SUV segment) with full drive by control capability</li>
                        <li>CAN / TCP-IP option for communication with vehicle controller</li>
                        <li>Actuation Systems for Steering, Brake, Accelerator</li>
                        </ul> 
                        </div>
                        -->
                         <div class="dynamicContent">
                         <div>
                           <?php if ($form_data[0]['company_profile']!=''): ?>
                             <strong>Company Profile : </strong><?php echo htmlspecialchars_decode($form_data[0]['company_profile']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['technology_abstract']!=''): ?>
                             <strong>Abstract: </strong><?php echo htmlspecialchars_decode($form_data[0]['technology_abstract']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['technology_value']!=''): ?>
                             <strong>Value Proposition : </strong> 
                              <?php echo htmlspecialchars_decode($form_data[0]['technology_value']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['important_features']!=''): ?>
                              <strong>Features: </strong>
                              <?php echo htmlspecialchars_decode($form_data[0]['important_features']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['application']!=''): ?>
                              <strong>Application : </strong>
                              <?php echo htmlspecialchars_decode($form_data[0]['application']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['intellectual_property']!=''): ?>
                              <strong>Intellectual Property: </strong>
                              <?php echo htmlspecialchars_decode($form_data[0]['intellectual_property']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['beneficiary_industry']!=''): ?>
                              <strong>Target Industry: </strong>
                              <?php echo htmlspecialchars_decode($form_data[0]['beneficiary_industry']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['advantages']!=''): ?>
                              <strong>Advantages: </strong>
                             <?php echo htmlspecialchars_decode($form_data[0]['advantages']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                            <?php if ($form_data[0]['commercialization_status']!=''): ?>
                              <strong>Commercialization Status: </strong>
                              <?php echo htmlspecialchars_decode($form_data[0]['commercialization_status']); ?>
                           <?php endif ?>
                           </div>
                           <div>
                           <?php if ($form_data[0]['DispTrl']!=''): ?>
                              <strong>Technology Readiness status : </strong>
                              <?php //echo htmlspecialchars_decode($form_data[0]['DispTrl']); ?>

                            <div class="techReadinessStatus col-md-12">
                            <ul>
                                <li>
                                    <p>IDEA</p><span class="bg1"> <div class="<?php if ($form_data[0]['trl']==1){ echo 'active_trl';}?> ">TRL 1</div> </span>
                                </li>
                                <li>
                                    <p>CONCEPT <br> DEFINATION</p><span class="bg2"> <div class="<?php if ($form_data[0]['trl']==2){ echo 'active_trl';}?> ">TRL 2</div> </span>
                                </li>
                                <li >
                                    <p >PROOF OF<br> CONCEPT</p><span class="bg3"> <div class=""><div class="<?php if ($form_data[0]['trl']==3){ echo 'active_trl';}?> ">TRL 3</div></span>
                                </li>
                                <li>
                                    <p>PROTOTYPE</p><span class="bg4"><div class="<?php if ($form_data[0]['trl']==4){ echo 'active_trl';}?> ">TRL 4</div></span>
                                </li>
                                <li>
                                    <p>LAB VALIDATION</p><span class="bg5"><div class="<?php if ($form_data[0]['trl']==5){ echo 'active_trl';}?> ">TRL 5</div></span>
                                </li>
                                <li>
                                    <P>TECHNOLOGY <br> DEVELOPEMENT</P><span class="bg6"><div class="<?php if ($form_data[0]['trl']==6){ echo 'active_trl';}?> ">TRL 6</div></span>
                                </li>
                                <li>
                                    <p>TECHNOLOGY <br> DEMONSTRATION</p><span class="bg7"><div class="<?php if ($form_data[0]['trl']==7){ echo 'active_trl';}?> ">TRL 7</div></span>
                                </li>
                                <li>
                                    <p>TECHNOLOGY <br> INTEGRATION</p><span class="bg8"><div class="<?php if ($form_data[0]['trl']==8){ echo 'active_trl';}?> ">TRL 8</div></span>
                                </li>
                                <li>
                                    <p>MARKET LAUNCH</p><span class="bg9"><div class="<?php if ($form_data[0]['trl']==9){ echo 'active_trl';}?> "> TRL 9</div></span>
                                </li>
                            </ul>
                           </div>

                           <?php endif ?>
                           </div>

                   
                           
                          
                           <div>

                           <?php if ($form_data[0]['DispTags']!=''): ?>
                                <?php if($form_data[0]['DispTags'] != "") 
                                    { 
                                    $tags_arr = explode("##",$form_data[0]['DispTags']); ?>
                                   
                                 <ul class="scroll_tags">
                                    <li class="technology_areas"> <strong>Technology Areas : </strong></li>
                                    <?php foreach($tags_arr as $tag)
                                       {   ?>
                                    <li><span style="cursor:auto"><?php echo $tag; ?></span></li>
                                    <?php } ?>
                                    <?php if ($form_data[0]['tag_other']!=''){ 
                                       $tag_other_arr=explode(',', $form_data[0]['tag_other']);
                                        foreach ($tag_other_arr as $key => $tag_other) {
                                       ?>
                                       <li><span style="cursor:auto"><?php echo $tag_other; ?></span></li>

                                    <?php } } ?>
                                 </ul>
                                 <?php } ?>
                           <?php endif ?>

                           </div>

                           


                        </div>
                           </div>
                           <?php if (count($files) && $form_data[0]['include_files']==1 ) { ?> 

                                       <div class="title-bar pt-0">
                                           <h2>Gallery</h2>
                                           <div class="heading-border"></div>
                                       </div>
                                       <div class="owl-carousel mb-2 mt-4 owl-theme gallerySlider">
                                       <?php 
                                       foreach ($files as $key => $value) { ?>
                                       <div>
                                               <div class="boxShadow75">
                                                   <img src="<?php echo base_url('uploads/technology_transfer/tech_files/').$value['file_name']; ?>" alt=" img">
                                               </div>

                                           </div>
                                       <?php } ?>
                                         
                                      
                                       </div>
                           <?php } ?>

                        
                        <?php if (count($video_data) && $form_data[0]['video_check']==1) { ?>
                        <div class="title-bar mt-5">
                               <h2>Videos</h2>
                                  <div class="heading-border"></div>
                        </div>
                        <div class="row">
                          <!-- <div class="owl-carousel owl-theme tech_video_slider"> -->
                           
                           <?php foreach ($video_data as $key => $video): 
                              $embed_video_url=str_replace('watch?v=', 'embed/', $video['video_url']);
                              ?>
                            
                          <div class="col-md-4">
                           <iframe width="200" height="200" src="<?php echo $embed_video_url ?>" frameborder="0" allowfullscreen></iframe>

                            <!-- <div class="item-video" data-merge="1"><a class="owl-video" href="<?php echo $video['video_url'] ?>"></a> -->
                              <!-- <div><p class="video_caption"><?php echo $video['video_caption'] ?></p>
                            </div> -->
                            </div>
                            
                           <?php endforeach ?>
                        </div>
                        <?php } ?>


                         
                            
                       
                     </div>
                     <div class="col-md-12">
                        <div class="mt-5 mb-3">
                           <!-- <p class="mb-0"><strong>Published Date:</strong> <?php echo date("d-m-Y", strtotime($form_data[0]['created_on'])); ?></p> -->
                           <div class="tag2 mt-3">
                              
                           </div>
                        </div>


                      <?php if( $this->session->userdata('user_id')== $form_data[0]['user_id']){ 
                        $btn_class='btn_outer1';
                      }else{
                        $btn_class='';
                      }
                     ?>


                     <button type="submit" class="btn btn-primary2 <?php echo $btn_class ?>" onclick="window.history.go(-1); return false;">Back</button>
                         
                         <?php 
                         $date=date("d M, Y", strtotime( $form_data[0]['created_on']));
                         if(in_array($form_data[0]['tech_id'], $TechActiondata['requested_tech'])) { $Request_label = "Request Already Sent"; $ReportedFlag = '0';  }
                        else { $Request_label = "Connect"; $ReportedFlag = '1'; } 
                     ?>
                     <div id="connect_btn_outer_<?php echo $form_data[0]['tech_id']; ?>" class="btn_outer">
                     <?php if( $this->session->userdata('user_id')!= $form_data[0]['user_id'] ){ ?> 
                        <!-- connect_request_technology_tarnsfer() is defined in footer -->
                         <button type="submit" class="btn btn-primary float-right" onclick="connect_request_technology_tarnsfer('<?php echo base64_encode($form_data[0]['tech_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['id_disp']; ?>', '<?php echo $form_data[0]['technology_title']; ?>' , '<?php echo $form_data[0]['author_name']; ?>','<?php echo $date; ?>', '<?php echo $form_data[0]['is_closed']; ?>')"  title="<?php echo $Request_label; ?>">Connect</button>
                         <?php } ?>
                      </div>

                     </div>


                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
   </div>
    
</section>
<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Share</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
            <div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>
            <span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="TechReportpopup" tabindex="-1" role="dialog" aria-labelledby="TechReportpopupLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="TechReportpopupLabel">Technology Transfer Report</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div class="">
                  <p>Technology Transfer ID : <span id="popupTechId"></span></p>
                  <p>Technology Transfer Title : <span id="popupTechTitle"></span></p>
               </div>
               <div class="form-group mt-4">
                <label for="popupTechReportComment" class="">Comment <em>*</em></label>
                  <textarea class="form-control" id="popupTechReportComment" name="popupTechReportComment" required onkeyup="check_report_validation()"></textarea>
                  
                  <span id="popupTechReportComment_err" class='error'></span>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary" onclick="submit_tech_report('<?php echo base64_encode($form_data[0]['tech_id']); ?>')">Submit</button>			
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document" id="download_model_content_outer">
   </div>
</div>
<!-- Modal -->
<?php $this->load->view('front/technology_transfer/connect',$captcha); ?>
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php
   if($form_data[0]['admin_status'] == 1 && $form_data[0]['is_block'] == 0) { $action_flag = 0; }
   else { $action_flag = 1; } ?>
<script>

function ShowDropdown() {
  document.getElementById("myDropdown").classList.toggle("show_dropdown");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show_dropdown')) {
        openDropdown.classList.remove('show_dropdown');
      }
    }
  }
} 


   $(function() {
     $(".tech_video_slider").owlCarousel({
       items: 3,
       merge: false,
       loop: false,
       margin: 10,
       video: true,
       lazyLoad: true,
       center: false,
       nav:true,
       dots:false,
       navText: ["", ""],
       responsive: {
         320: {
           items: 1
         },
         560: {
           items: 2
         },
         992: {
           items: 3
         }
       }
     });
   });

   $(".toggle_btn").click(function(event) {
   	$(".btn_report").toggleClass('d-none')	
   
   });
   	function like_unlike_blog(id, flag)
   	{		
   		var cs_t = 	$('.token').val();
   		$.ajax(
   		{
   			type:'POST',
   			url: '<?php echo site_url("knowledge_repository/like_unlike_blog_ajax"); ?>',
   			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
   			dataType:"JSON",
   			success:function(data)
   			{
   				$("#preloader").css("display", "none");
   				$(".token").val(data.token);
   				
   				$("#like_unlike_btn_outer_"+data.tech_id).html(data.response);
   				$("#like_count_outer_"+data.tech_id).html(data.total_likes_html);
   				
   			}
   		});
   	}
</script>
<script>
   function open_download_modal(tech_id)
   {
   	module_id=31;
   	if(check_permissions_ajax(module_id) == false){
           	swal( 'Warning!','You don\'t have permission to access this page !','warning');
           	return false;
   	}
   	$.ajax({
   		type: "POST",
   		url: "<?php echo site_url('knowledge_repository/open_download_modal_ajax'); ?>",
   		data: {'tech_id':tech_id},
   		cache: false,
   		success:function(data)
   		{
   			//alert(data)
   			if(data == "error")
   			{
   				location.reload();
   			}
   			else
   			{
   				$("#download_model_content_outer").html(data);
   				$("#DownloadModal").modal('show');
   			}
   		}
   	});
   }
   function delete_blog_from_user_listing(id)
   {
   	swal(
   	{  
   		title:"Confirm?",
   		text: "Are you sure you want to delete the selected blog?",
   		type: 'warning',
   		showCancelButton: true,
   		confirmButtonColor: '#3085d6',
   		cancelButtonColor: '#d33',
   		confirmButtonText: 'Yes!'
   	}).then(function (result) 
   	{ 
   		if (result.value) 
   		{
   			var cs_t = 	$('.token').val();
   			$.ajax(
   			{
   				type:'POST',
   				url: '<?php echo site_url("knowledge_repository/delete_blog"); ?>',
   				data:{ 'id':id, 'csrf_test_name':cs_t },
   				dataType:"JSON",
   				success:function(data)
   				{ 
   					window.location.replace("<?php echo site_url('knowledge_repository'); ?>");
   				}
   			});
   		} 
   	});		
   }	
   
   function report_tech(tech_id, ReportedFlag, blog_disp_id, blog_title)
   {
   	<?php if($action_flag == 1) { ?>
   		swal(
   		{
   			title: 'Alert!',
   			html: "You can not Report the content as its status is Pending or Blocked",
   			type: 'warning'				
   		});
   	<?php }else { ?>
   		if(ReportedFlag == "1")
   		{
   			$("#popupTechId").html(blog_disp_id);
   			$("#popupTechTitle").html(blog_title);
   			$("#popupTechReportComment_err").html('');
   			$("#TechReportpopup").modal('show');
   			//$("#popupTechReportComment").val('');
   			$("#popupTechReportComment").focus();
   		}
   		else
   		{
   			swal(
   			{
   				title: 'Alert!',
   				text: "You have already reported this content",
   				type: 'alert',
   				showCancelButton: false,
   				confirmButtonText: 'Ok'
   			});
   		}
   	<?php } ?>
   }	
   
   function check_report_validation()
   {
   	var popupTechReportComment = $("#popupTechReportComment").val();
   	if(popupTechReportComment.trim() == "") { $("#popupTechReportComment_err").html('Please enter the report comment'); $("#popupTechReportComment").focus(); }
   	else { $("#popupTechReportComment_err").html(''); }
   }
   
   function submit_tech_report(tech_id)
   {	
   	check_report_validation();
   	
   	var popupTechReportComment = $("#popupTechReportComment").val();
   	if(popupTechReportComment.trim() != "")		
   	{
   		$("#popupTechReportComment_err").html('');
   		swal(
   		{  
   			title:"Confirm?",
   			text: "Are you sure you want to report the selected content ?",
   			type: 'warning',
   			showCancelButton: true,
   			confirmButtonColor: '#3085d6',
   			cancelButtonColor: '#d33',
   			confirmButtonText: 'Yes!'
   		}).then(function (result) 
   		{ 
   			if (result.value) 
   			{
   				var cs_t = 	$('.token').val();
   				$.ajax(
   				{
   					type:'POST',
   					url: '<?php echo site_url("technology_transfer/report_tech_ajax"); ?>',
   					data:{ 'tech_id':tech_id, 'popupTechReportComment':popupTechReportComment, 'csrf_test_name':cs_t },
   					dataType:"JSON",
   					success:function(data)
   					{
   						$(".token").val(data.token);
   						$("#report_btn_outer_"+data.tech_id).replaceWith(data.response);						
   						$("#TechReportpopup").modal('hide');
   						
   						swal(
   						{
   							title: 'Success!',
   							text: "Content Reported successfully",
   							type: 'success',
   							showCancelButton: false,
   							confirmButtonText: 'Ok'
   						});
   					}
   				});
   			} 
   		});
   	}
   }	
   
   
   
   
   function share_blog(url) 
   {
   	<?php if($action_flag == 1) { ?>
   		swal(
   		{
   			title: 'Alert!',
   			html: "You can not share the blog as its status is Pending or Blocked", 
   			type: 'warning'
   		});
   	<?php }else { ?>
   		$("#BlogShareLink").html(url);
   		$("#share-popup").modal('show');
   	<?php } ?>
   }
   
   
   function copyToClipboardLink(containerid) 
   {
   	var id = containerid;
   	var el = document.getElementById(id);
   	var range = document.createRange();
   	range.selectNodeContents(el);
   	var sel = window.getSelection();
   	sel.removeAllRanges();
   	sel.addRange(range);
   	document.execCommand('copy');
   	
   	$("#link_copy_msg").html('Link copied');
   	
   	$("#link_copy_msg").slideDown(function() {
   		setTimeout(function() 
   		{
   			$("#link_copy_msg").slideUp();
   			sel.removeRange(range);
   		}, 1500);
   	});
   		
   	//alert("Contents copied to clipboard.");
   	return false;
   }
   
   $(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
   $(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>