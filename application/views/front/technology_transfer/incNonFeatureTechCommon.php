<?php if(count($non_featured_tech) > 0)
	{ ?>
		<div class="row">
		<?php foreach($non_featured_tech as $res)
		{ 
			$data['res'] = $res; $data['blog_type_flag'] = $blog_type_flag = 'nonfeatured'; ?>
		
				<div class="col-md-4 mt-3">
            		
				
				<?php 
				$this->load->view('front/technology_transfer/inc_tech_listing_inner', $data); ?>
					
				</div>
		
		<?php } ?>
		
		</div>
		
		<?php if($new_start < $total_non_featured_tech_count)
		{ ?><br>
		<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
			<a href="javascript:void(0)" class="click-more" onclick="getTechDataAjax(0, '<?php echo $featured_tech_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $non_featured_tech_limit; ?>', 0, 1)">Show More</a>				
		</div>
		<?php }
	} 
	else 
	{ 
		if($featured_tech_total_cnt == 0 ) 
		{	?>
	<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;"><br>No Records Available</p></div>
<?php }
	}	?>	


	<script>
   
    $('.tagSlider').owlCarousel({
        items: 1,
        autoplay: false,
        smartSpeed: 700,
        autoWidth:true,
        loop: true,
        nav: true,
        dots: false,
        margin:12,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });

</script>