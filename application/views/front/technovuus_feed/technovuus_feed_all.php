<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
	.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
	.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
	.select2-container--default .select2-selection--single{height:35px!important; border:none}
	#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
	#WebinarWallSlider.owl-carousel .owl-dot,#WebinarWallSlider.owl-carousel .owl-nav .owl-next,#WebinarWallSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
	#WebinarWallSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	#WebinarWallSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	#WebinarWallSlider .owl-nav.disabled{display:block}
	#WebinarWallSlider .owl-nav{position:absolute; top:40%; width:100%}
	#WebinarWallSlider.owl-theme .owl-dots .owl-dot.active span,#WebinarWallSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
	#WebinarWallSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
	#WebinarWallSlider .owl-nav .owl-prev{left:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
	#WebinarWallSlider .owl-nav .owl-next{right:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
	#WebinarWallSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}
	#WebinarWallSlider .owl-item{ padding: 0 5px;}
	#WebinarWallSlider .owl-item img{width:100%; }
	#WebinarWallSlider .owl-dots{position:absolute;width:100%;bottom:25px}
	#WebinarWallSlider .owl-item{ padding: 0;}
	#FeaturedWebinarsSlider.owl-carousel .owl-dot,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	#FeaturedWebinarsSlider .owl-nav.disabled{display:block}
	#FeaturedWebinarsSlider .owl-nav{position:absolute; top:40%; width:100%}
	#FeaturedWebinarsSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #000;color:#32f0ff}
	#FeaturedWebinarsSlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
	#FeaturedWebinarsSlider .owl-nav .owl-next{right:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
	#FeaturedWebinarsSlider .owl-item{ padding: 0 15px;}
	#FeaturedWebinarsSlider .owl-item img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2);}
	.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
	/* #FeaturedWebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
	.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
	.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
	.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
	.product_details{padding: 20px 0 0 0;}
	/** Blog**/
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
	.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
	.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
	.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.tag ul li a:hover{ background: #000; color: #32f0ff; }
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.shareList ul li a:hover{ background: #000; color: #32f0ff; }
	.userPhoto-info h3{ font-size: 19px !important; }
	.userPhoto-info img{ border-radius: 50%;}
	.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
	.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.descriptionBox45{padding:15px;border:1px solid #CCC; height: 330px; }
	.descriptionBox45 p{color: #333;}
	.descriptionBox45 ul{color: #333; list-style: none; padding: 0; margin:0;  font-size: 14px;}
	.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
	.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#333}
	.bs-example45{border-radius:6px;background:#FFF;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
	.bs-example45 h3{color:#333;font-size:19px;font-weight:800}
	.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/** Color Change**/
	.nav-tabs{border-bottom:1px solid #32f0ff}
	.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#32f0ff;background-color:#000 ;border-color:#000}
	.product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {
	border-color: #32f0ff #32f0ff #32f0ff;
	}
	.title-bar .heading-border{background-color:#32f0ff}
	.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
	.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
	.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
	.owner-teammember img{height:100%!important; width:100%!important;border-radius: 0%!important;}
	.owner-teammember a{background:none;}
	.owner-teammember a:hover{background:none;}
	
	/**overwrite css**/
	.search-sec {padding: 1rem;background: #32f0ff; width: 100%; margin-bottom: 0px;}
	
	.filterBox .btn-primary {color: #32f0ff;	background-color: #020001; border-color: #020001;	width: 100%;display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}
	.filterBox .btn-primary:hover {color: #000; background-color: #32f0ff; border-color: #000}
	
	.clearAll{ background:#000 !important; color:#32f0ff !important; border-color: #32f0ff !important; margin-right:15px}
	.clearAll:hover{ background:#32f0ff !important; color:#000 !important; border-color: #000 !important;}
	
	.form-check-input {position: absolute; margin-top: .3rem !important; /* margin-left: -1.25rem !important; */ }
	#AnswersForum h2{font-size:1.75rem}
	
	a.filter_load_more, a.filter_load_less {background:none; font-size: 14px; float: right;}  
</style>

<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">TechNovuus Feed</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">List of TechNovuus Feed</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php $this->load->view('front/webinar/inc_technology_wall_tabs.php'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade" id="TechNovuusFeed">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="formInfo65">
							<h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
							<div class="row userPhoto-info">
								<div class="col-md-2">
									<img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
								</div>
								<div class="col-md-10 pl-0">
									<h3>A. P. J. Abdul Kalam</h3>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
									<div class="shareList">
										<ul>
											<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
											<li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
											<li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="formInfo65 mt-3">
							<h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
							<div class="row userPhoto-info">
								<div class="col-md-2">
									<img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
								</div>
								<div class="col-md-10 pl-0">
									<h3>A. P. J. Abdul Kalam</h3>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
									<div class="shareList">
										<ul>
											<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
											<li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
											<li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="formInfo65 mt-3">
							<h3 class="mb-4">A New Expert joined on TechNovuus!!</h3>
							<div class="row userPhoto-info">
								<div class="col-md-2">
									<img src="<?php echo base_url('assets/img/abdul-kalam.jpg') ?>" alt="abdul kalam" class="img-fluid">
								</div>
								<div class="col-md-10 pl-0">
									<h3>A. P. J. Abdul Kalam</h3>
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo...</p>
									<div class="shareList">
										<ul>
											<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
											<li><a href="#"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</a></li>
											<li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bs-example45">
							<h3>Trending on TechNovuus </h3>
							<div class="descriptionBox45 text-justify">
								<ul>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
								</ul>
							</div>
						</div>
						<div class="bs-example45 mt-4">
							<h3>Featured on TechNovuus</h3>
							<div class="descriptionBox45 text-justify">
								<ul>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
									<li>Blog: Tesla AutoPilot</li>
									<li>Webinar: Shaping Mobility</li>
									<li>Questions and Answers Forum: How to develop Air Battery</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
	#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
	#flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
	#flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
</style>

<div id="flotingButton2">
	<a href="<?php echo base_url('webinar/add'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post Webinar</a>
</div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script>	
	function show_more_technology(val)
	{
		if(val == 'show')
		{
			$( ".filter_technology" ).removeClass( "d-none" )
			$( ".filter_load_less" ).removeClass( "d-none" )
			$( ".filter_load_more" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_technology" ).addClass( "d-none" )
			$( ".filter_load_less" ).addClass( "d-none" )
			$( ".filter_load_more" ).removeClass( "d-none" )
		}
	}
	
	(function($)
	{
		$(window).on("load",function()
		{
			$(".descriptionBox45").mCustomScrollbar({
        theme: "rounded-dark"
			});
		});
	})(jQuery);

	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			getFeaturedUpcomingWebibarDataAjax(0, 15, 0, 3, 1, 0);
		} 
	});
	
	function clear_webinar_search_form() 
	{ 
		$("#search_txt").val('');  
		$('.search_technology:checkbox').prop('checked',false); 
		$('.search_pay_type:checkbox').prop('checked',false); 
		
		getFeaturedUpcomingWebibarDataAjax(0, 15, 0, 3, 1, 0);
	}
	
	//START : FEATURED & UPCOMING WEBINARS DATA
	function getFeaturedUpcomingWebibarDataAjax(f_start, f_limit, u_start, u_limit, is_search, is_show_more)
	{
		$("#showMoreBtnUpcoming").remove();
		var cs_t = 	$('.token').val();
		
		var selected_technology = [];
		$.each($("input.search_technology:checked"), function() { selected_technology.push($(this).val()); });
		
		var selected_pay_type = [];
		$.each($("input.search_pay_type:checked"), function() { selected_pay_type.push($(this).val()); });
				
		var keyword = encodeURIComponent($('#search_txt').val());
		var technology = encodeURIComponent(selected_technology);	
		var pay_type = encodeURIComponent(selected_pay_type);
		
		parameters= { 'f_start':f_start, 'f_limit':f_limit, 'u_start':u_start, 'u_limit':u_limit, 'is_show_more':is_show_more, 'keyword':keyword, 'technology':technology, 'pay_type':pay_type, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('webinar/getFeaturedUpcomingWebibarDataAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					if(is_search == '1') { $("#UpcomingWebibarDataOuter").html(''); }
					
					$(".token").val(data.csrf_new_token)
					if(is_show_more == 0) { $("#FeaturedWebinarDataOuter").html(data.Featured_response); }
					$("#UpcomingWebibarDataOuter").append(data.Upcoming_response);
				}else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getFeaturedUpcomingWebibarDataAjax(0, 15, 0, 3, 0, 0);
	//END : FEATURED & UPCOMING WEBINARS DATA
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	
	function openNav() 
	{		
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
		
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";		
	}
</script>