<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
.guideline-tooltip{
	position: relative;
	display: inline-block;
	margin-left: 10px;
}
.guideline-tooltip:hover p {
  opacity: 1;
  -webkit-transform: translate(-35%, 0);
          transform: translate(-35%, 0);
    margin-top: 10px;
	visibility: visible;
}
.guideline-tooltip p {
    position: absolute;
    left: 50%;
    top: 100%;
    opacity: 0;
    padding: 1em;
    background-color: #e7f0ff;
    font-size: 14px;
    line-height: 1.6;
    text-align: left;
    white-space: nowrap;
    -webkit-transform: translate(-35%, 1em);
    transform: translate(-35%, 1em);
    -webkit-transition: all 0.15s ease-in-out;
    transition: all 0.15s ease-in-out;
    color: #000;
	z-index: 99;
    font-weight: 400;
	visibility: hidden;
}
.guideline-tooltip p::before {
    content: '';
    position: absolute;
    top: -16px;
    left: 33%;
    width: 0;
    height: 0;
    border: 0.6em solid transparent;
    border-top-color: #e7f0ff;
    transform: rotate(180deg);
}

</style>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Listing</h1>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav> -->
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">			
               <div class="col-md-12">
			   
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">
				 <p><b>Challenge Name :</b> <?php echo $challange_details[0]['challenge_title']; ?> </p>
                    <!--<div class="row">
                    <div class="col-md-4">                            
						<div class="form-group">
						
							<select class="form-control select2 " id="techonogy_id" name="techonogy_id[]" multiple="multiple" required>
							  <option value=""></option>
								<?php foreach($technology_data as $techname){ ?>
								<option value="<?php echo $techname['id'] ?>" ><?php echo $techname['technology_name'] ?></option>								  
								<?php  } ?>								
							</select>	
							<label class="form-control-placeholder leftLable2" for="techonogy_id">Technology Field </label>						
						</div>
                     </div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Challenge Visibility</label>
							<select id="change_visibility" name="change_visibility"  class="form-control select2">                           
									<option value=""></option>
								<?php 
									foreach($challengetype as $visibility){
								?>
									<option value="<?php echo $visibility['challenge_type'] ?>"><?php echo $visibility['challenge_type'] ?></option>
								<?php } ?>
							</select>						
						</div> 
					</div>                 
					
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Funding Available</label>
							<select id="fund_sel" name="fund_sel" class="form-control select2" required="">
								<option value=""></option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Reward Available</label>
							<select id="reward_sel" name="reward_sel" class="form-control select2" required="">
								<option value=""></option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">                            
						<div class="form-group">
							<label class="form-control-placeholder">TRL </label>
							<select class="form-control select2" id="trl_id" name="trl_id">
							  <option value=""></option>
								<?php foreach($trl_data as $trl){ ?>
								<option value="<?php echo $trl['id'] ?>" ><?php echo $trl['trl_name'] ?></option>								  
								<?php  } ?>								
							</select>							
						</div>
                     </div>
					 <div class="col-md-4">

					 
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">IP Clause </label>
							<select id="ip_clause" name="ip_clause" class="form-control select2" required="">
								<option value=""></option>
								<?php 
								foreach($ip_clause_data as $ip_data){
								?>
								<option value="<?php echo $ip_data['id'] ?>"><?php echo $ip_data['ip_name'] ?></option>
								<?php } ?>
							  
							</select>						
						</div> 
					</div>
					<div class="col-md-4">
						<button type="submit" id="btnReset" class="btn btn-secondary">Clear All </button>
						<button type="submit" id="btn-click" class="btn btn-primary btn-click">Apply</button>
					</div>

                 </div>-->
                 <div class="row mt-12 mt-5">
                    <div class="col-md-12">
					
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
						 <?php echo $this->session->flashdata('success'); ?>
						</div>
					   <?php } //echo "<pre>";print_r($challange_details); ?>
                        <div class="table-responsive">
                        <table id="team-listing" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">Team ID</th>
								<th scope="col">Team for any Challenge?</th>
                                <th scope="col">Challenge Name</th>
                                <th scope="col">Total No. Of Slots</th>
                                <th scope="col">Open Slots</th>
                                <th scope="col">New Application Received</th>
								<th scope="col">Team Status</th>
                                <th scope="col">Challenge Application Status</th>
								<th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>							
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
	
	<input type="hidden" name="c_id" id="c_id" value="<?php echo $c_id ?>" />
<script>
  $(document).ready( function () {	

		//$(".open-details").on("click", function () {	
		$("body").on("mouseover", "#team-listing tbody tr .open-details", function (e) { 
			var totalCnt = $(this).attr('data-total');
			var acceptedCnt = $(this).attr('data-accepted');
			$(".popup").show();
			$('#GFG_Span').text(totalCnt); 
		
		});
		
		$("body").on("mouseout", "#team-listing tbody tr .open-details", function (e) { 			
			$(".popup").hide();		
		});
	
		var base_path = '<?php echo base_url() ?>';
		var table = $('#team-listing').DataTable({
			
			"ordering":false,
			"searching": false,
			"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"myTeams/team_listing",
			"type":"POST",
			"data":function(data) {	
				
					data.c_id 				= $("#c_id").val();
					/*data.change_visibility 		= $("#change_visibility").val();
					data.ip_clause 				= $("#ip_clause").val();
					data.fund_sel 				= $("#fund_sel").val();
					data.reward_sel 			= $("#reward_sel").val();
					data.trl_id 				= $("#trl_id").val();*/
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
		});

		$('.btn-click').click(function(){
			var techonogy_id 		= $('#techonogy_id').val();
			var change_visibility 	= $('#change_visibility').val();	
			var ip_clause 			= $('#ip_clause').val();
			var fund_sel 			= $('#fund_sel').val();
			var reward_sel 			= $('#reward_sel').val();
			table.ajax.reload();		
		});
		
		$("#btnReset").on("click", function () {			
			$("#techonogy_id").val([]).change();
			//$("#audience_pref").val([]).change();
			//$("#c_status").val([]).change();
			$("#ip_clause").val([]).change();
			$("#fund_sel").val([]).change();
			$("#reward_sel").val([]).change();
			$("#trl_id").val([]).change();
			$("#change_visibility").val([]).change();					
			table.ajax.reload();	
		});
		
		
	
	$('.select2').select2({});

});
$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>