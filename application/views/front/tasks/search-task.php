﻿<style>	
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	.team_search.owl-carousel .owl-dot,
	.team_search.owl-carousel .owl-nav .owl-next,
	.team_search.owl-carousel .owl-nav .owl-prev {
	font-family: fontAwesome
	}
	
	.team_search.owl-carousel .owl-nav .owl-prev:before {
	content: "\f177"
	}
	
	.team_search.owl-carousel .owl-nav .owl-next:after {
	content: "\f178"
	}
	
	.team_search .owl-nav.disabled {
	display: block
	}
	
	.team_search .owl-nav {
	position: absolute;
	top: 35px;
	width: 100%
	}
	
	.team_search .owl-nav .owl-prev {
	left: 10px;
	position: absolute
	}
	
	.team_search .owl-nav .owl-next {
	right: 10px;
	position: absolute
	}
	
	/* .team_search h4, .inner-box h4 {
	color: #000;
	padding: 10px 0;
	margin: 15px 0 15px 0;
	border-top: solid 1px #eee;
	border-bottom: solid 1px #eee;
	font-size: 22px;
	} */
	
	
	
	
	.team_search .item {
	margin: 0;
	padding: 16px 0 0 0;
	
	}
	
	.team_search .item a {
	text-decoration: none;
	padding: 5px 15px;
	color: #fff;
	background: #e23751;
	text-align: center;
	border-radius: .25rem;
	display: block;
	}
	
	.team_search .item a:hover {
	text-decoration: none;
	color: #FFF;
	background: #333;
	}
	
	.boxSection25 {
	padding: 0;
	margin: 0;
	}
	
	.formInfo ul.list-group {
	display: inline-block
	}
	
	.boxSection25 .list-group li.list-group-item {
	width: 33.33%;
	float: left;
	min-height: 438px;
	padding: 8px;
	}
	
	/* .boxSection25 h3 {
	color: #000;
	padding: 15px 0;
	margin: 0 0 15px 0;
	border-bottom: solid 1px #eee;
	font-size: 22px;
	}
	
	.boxSection25 ul {
	list-style: none;
	text-align: center;
	overflow: hidden;
	padding: 0;
	margin: 0;
	display: inline-block;
	width: 100%;
	}
	.boxSection25 ul li {
	padding: 0 5px;
	float: left;
	margin: 0;
	} */
	
	.list-group-item:before, .formInfo li::before{ display:none}
	
	
	.chatButton {
	text-decoration: none;
	padding: 5px 15px;
	color: #fff;
	background: #333 !important;
	text-align: center;
	border-radius: .25rem;
	display: block;
	}
	
	.chatButton:hover {
	text-decoration: none;
	color: #FFF;
	background: #e23751 !important;
	}
	
	.Skillset {
	display: block;
	overflow: hidden
	}
	
	.Skillset ul {
	list-style: none;
	text-align: center;
	padding: 0;
	margin: 0 !important;
	overflow: auto; height: 96px;
	}
	
	.Skillset ul li {
	padding: 5px;
	float: left;
	margin: 0 5px 5px 0;
	border: solid 1px #eee;
	font-size: 14px;
	}
	
	.team_search img {
	width: 100px!important;
	height: 100px;
	border-radius: 50%;
	margin: 0 auto
	}
	
	.form-control-placeholder {
	left: 0px
	}
	
	.border_top_bottom {
	border-top: solid 1px #eee;
	width: 100%;
	padding: 15px 0;
	display: block;
	margin: 20px 0 0;
	clear: both
	}
	
	.form-group .form-control {
	height: 100px !important;
	resize: none;
	}
	
	/* .inner-box {
	width: 100%;
	min-height: 308px;
	float: left;
	} */
	
	#more_details {display: none;}
	.inner-box .img-fluid {
	height: 255px;
	width: 100%;
	}
	
	.mt-02{margin-top:47px}
	
	a.filter_load_less_skill{background:none; font-size: 14px; float: right;}
	a.filter_load_more_skill{background:none;font-size: 14px; float: right;}
	
	.btn-primary25 {
	color: #fff;
	background-color: #e23751;
	border-color: #e23751;
	}
	
	
	.btn-primary25:hover {
	color: #333;
	background-color: #FFF;
	border-color: #333;
	}
	
	.blink_me {
  animation: blinker 1s linear infinite; font-weight:bold;
	}
	
	@keyframes blinker {  
  50% { opacity: 0; }
	}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Search For Existing Teams </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Search for Existing Teams</li>
			</ol>
		</nav>
		<a href="<?php echo base_url('tasks/create'); ?>" class="btn btn-primary25"><span class="blink_me">Create Task Team</span></a>
	</div>
	<!--/end container-->
</div>
<div class="filterBox">
	<section class="search-sec">
		<form method="post" id="filterData" name="filterData" onsubmit="return false;">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="mySidenav" class="sidenav">
							` <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>×</strong></a>
							<div class="scroll">
								<ul>
									<li> <strong> Select Skillset </strong> </li>
									<?php $i=0; 
										foreach($skill_set as $skills) 
										{ ?>
											<li class="<?php if($i >= 5) { ?>filter_skills d-none<?php	} ?>">
												<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills['id'] ?>" id="skillset" name="skillset[]" onchange="show_more_teams(0, '<?php echo $limit; ?>', 1, 0, 0)">
												<label class="form-check-label" for="defaultCheck1">
													<?php echo $skills['name'] ?>
												</label>
											</li>
							<?php 	$i++;
										} ?>
										
										<?php if(count($skillmore) > 5): ?>
										<a href="javascript:void(0);" class="filter_load_more_skill" onclick="show_more_skills('show')">Show More >> </a>
										<a href="javascript:void(0);" class="filter_load_less_skill d-none"  id="skill-readless" onclick="show_more_skills('hide')">Show Less >></a>
										<?php endif; ?>
								</ul>                                     
							</div>
						</div>
						<div class="row d-flex justify-content-center search_Box">
							
							<div class="col-md-6">
								<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />
								<button type="button" class="btn btn-primary searchButton" onclick="show_more_teams(0, '<?php echo $limit; ?>', 1, 0, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							<div class="clearAll"><a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form()">Clear All</a>
							</div>
							<div class="filter"><button type="button" onclick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
							</div>
							
							<div class="col-md-12"></div>
							
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
</div>


<section id="registration-form" class="inner-page p-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 justify-content-center mt-4 team-box">
				<div class="boxSection25">
					
				</div>
				<input type="hidden" id="hidden_keyword" name="hidden_keyword">
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="briefInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Brief Info about Team</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents">
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>	
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<script type="text/javascript">	
	$(document).on('click','.apply-popup',function()
	{		
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
		
	});
	
	$(document).on('click','.chat-popup',function()
	{
		
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});		
	});
	
	function show_more_skills(val)
	{
		if(val == 'show')
		{
			$( ".filter_skills" ).removeClass( "d-none" )
			$( ".filter_load_less_skill" ).removeClass( "d-none" )
			$( ".filter_load_more_skill" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_skills" ).addClass( "d-none" )
			$( ".filter_load_less_skill" ).addClass( "d-none" )
			$( ".filter_load_more_skill" ).removeClass( "d-none" )
		}
	}
	
	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			show_more_teams(0, '<?php echo $limit; ?>', 1, 0, 0);
		} 
	});
	
	function clear_search_form() 
	{ 
		$("#search_txt").val('');  
		$('.search-value:checkbox').prop('checked',false); 
		
		show_more_teams(0, '<?php echo $limit; ?>', 1, 0, 0);
	}
	
	$(document).ready(function () 
	{
		$(document).on('click','.click-more',function()
		{		
			//var priorityVal = $(this).val();			
			var cid	=	$(this).attr('data-id');
			//alert(cid);return false;
			$('#briefInf').modal('show');		
			
			var base_url = '<?php echo base_url('challenge/viewDetails'); ?>';
			$.ajax({
				url: base_url,
				type: "post",
				data: {id:cid},				
				success: function (response) {					
					$("#contents").html(response);				
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
			
		});	
		
		$('#myModal').modal('show');
	});
	$('.select2').select2({});
	
	
	// Load More Code 
	function show_more_teams(start, limit, is_search, is_show_more, first_flag)
	{
		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		var selected_skillset = [];
		$.each($("input.search-value:checked"), function() { selected_skillset.push($(this).val()); });
		
		var keyword = encodeURIComponent($('#search_txt').val());
		var skillset = encodeURIComponent(selected_skillset);	
		
		parameters= { 'start':start, 'limit':limit,  'is_show_more':is_show_more, 'keyword':keyword, 'skillset':skillset, 'cs_t':cs_t, 'first_flag':first_flag }
		$("#preloader-loader").show();
		
		$('#preloader-loader').css('display', 'block');	
		var base_url = '<?php echo base_url(); ?>'; 
		var ID = $(this).attr('id');
		var length = $('.counts').length;
		
		var cs_t = 	$('.token').val();
		$('.show_more').hide();
		
		$.ajax({
			type:'POST',
			url: base_url+'tasks/load_teams_new',
			data:parameters,
			dataType: 'JSON',
			async:false,
			success:function(response)
			{ 
				$('#preloader-loader').css('display', 'none');			
				$("#show_more_main").remove();
				
				if(is_search == '1') { $(".boxSection25").html(''); }
				
				$(".token").val(response.token);											
				$('.boxSection25').append(response.html);				
			},
			error: function (jqXHR, exception) 
			{ 
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
					msg = 'Time out error.';
					} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
					} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				console.log(msg);
				//$('#post').html(msg);
			},
		});
	};
	show_more_teams(0, '<?php echo $limit; ?>', 0, 0, 1);
	
	function openNav() 
	{	
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		} 
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";
	}
	
	(function($) 
	{
    $(window).scroll(function() {
			
			if ($(this).scrollTop() < 61) {
				// hide nav
				$("nav").removeClass("vesco-top-nav");
				$("#flotingButton").fadeOut();
				
        } else {
				// show nav
				$("nav").addClass("vesco-top-nav");
				$("#flotingButton").fadeIn();
			}
		});
	})(jQuery); // End of use strict
</script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>          