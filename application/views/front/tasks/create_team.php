<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<?php  $encrypt_obj =  New Opensslencryptdecrypt(); ?>

<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	/* .formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; } */

.sw-theme-arrows .sw-toolbar{ overflow:hidden; display:block;}
/* .formInfo .btn-group.sw-btn-group-extra {
	position: absolute;
	display: block;
	right: 0;
} */


.btn-group.mr-2.sw-btn-group-extra {
	float: left;
	position: inherit;
}
/* .formInfo .btn-group.sw-btn-group-extra{position:absolute;left:15px} */
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	.BtnAsLabel, .BtnAsLabel:hover, .BtnAsLabel:active, .BtnAsLabel:focus, .BtnAsLabel:disabled { background-color: #333 !important; opacity: 1; border-color: #333 !important;
	}
	.datepicker table tr td, .datepicker table tr th 
	{
	    border-radius: 0;
	    border: 1px solid rgba(0,0,0,0.1);
	}

	.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover 
	{
	    background: rgba(0,0,0,0.1);
	    color: rgba(0,0,0,0.4);
	    cursor: not-allowed;
	    border: 1px solid #fff;
	}

	th.datepicker-switch, th.next, th.prev 
	{
	    border: none !important;
	}


	.sw-btn-group{ display:block}

	.sw-btn-next {
	position: absolute !important;
	right: 0 !important;
	/* width: 100%; */
}


.step_wiz_buttons {
	position: absolute !important;
	right: 0 !important;
}

.step_wiz_buttons .btnfinish{ margin-right:5px}



</style>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Create a Task Team</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt') ?>">BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Create a Task Team </li>
			</ol>
		</nav>
	</div>
</div> 
	
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					<?php //echo validation_errors(); ?>
					
					<form method="POST" action="<?php echo site_url('tasks/create/'.$encoded_team_id ); ?>" id="ApplyChallengeForm" name="ApplyChallengeForm" enctype="multipart/form-data">
						<div id="smartwizard_apply_challenge">
							<ul>
								<li><a href="#step-1">Team Details<br /><small></small></a></li>
								<li><a href="#step-2">Member Details<br /><small></small></a></li>
								<li><a href="#step-3">Task details<br /><small></small></a></li>
								<?php if($mode == 'Add') { ?><li><a href="#step-4">Send Invitation<br /><small></small></a></li> <?php } ?>
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control" value="<?php if($mode == 'Add') { echo set_value('task_name'); } else { echo $team_data[0]['task_name']; } ?>" name="task_name" id="task_name">
												<label class="form-control-placeholder floatinglabel">Task Name<em>*</em></label>
											</div>
										</div>	
									
										
										<div class="col-sm-12">
											<div class="form-group">
												<input type="text" class="form-control" name="team_name" id="team_name" value="<?php if($mode == 'Add') { echo set_value('team_name'); } else { echo $team_data[0]['team_name']; } ?>" required autofocus maxlength="250">
												<label class="form-control-placeholder floatinglabel" for="team_name">Name of Team <em>*</em></label>
												<span class="error"><?php echo form_error('team_name'); ?></span>
											</div>
										</div>
										
										<?php
										$dynamic_cls = 'd-none';
										$full_img = $onclick_fun = '';
										if($mode == 'Update' && $team_data[0]['team_banner'] != '' && file_exists('./uploads/byt/'.$team_data[0]['team_banner']))
										{	
											$dynamic_cls = '';
											$full_img = base_url().'uploads/byt/'.$team_data[0]['team_banner'];
											$onclick_fun = "remove_banner_img('1','".$encrypt_obj->encrypt('arai_byt_teams')."', '".$encrypt_obj->encrypt('team_id')."', '".$encrypt_obj->encrypt($team_data[0]['team_id'])."', 'team_banner', '".$encrypt_obj->encrypt('./uploads/byt/')."', 'Team Banner')";
										} ?>
									
										<div class="col-sm-12 <?php echo $dynamic_cls; ?>" id="team_banner_outer">
											<div class="form-group">
												<div class="previous_img_outer">
													<img src="<?php echo $full_img; ?>">
													<a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
										
										<div class="col-md-12">											
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Banner</button>	
												<input type="file" class="form-control" name="team_banner" id="team_banner" />
												<div class="clearfix"></div>
												<span class="small">Note : Files should be images less than 2MB</span><div class="clearfix"></div>
												<?php if(form_error('team_banner')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo str_replace("filetype","file type",form_error('team_banner')); ?></span> <?php } ?>
												<?php if($team_banner_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo str_replace("filetype","file type",$team_banner_error); ?></span> <?php } ?>
											</div>
										</div>
										
									
										
										<div class="col-sm-12">
											<div class="form-group">
												<textarea class="form-control" name="team_details" id="team_details"><?php if($mode == 'Add') { echo set_value('team_details'); } else { echo $team_data[0]['brief_team_info']; } ?></textarea>
												<label class="form-control-placeholder" for="team_details" style="margin-top: -11px;">Brief About Team <em style="color:red;">*</em></label>
												<span class="error"><?php echo form_error('team_details'); ?></span>
											</div>										
										</div>
										<div class="col-sm-6">
											<div class="form-group"> 
												<input type="text" class="form-control datepicker" name="launch_date" id="launch_date" value="<?php if($mode == 'Add') { echo set_value('launch_date'); } else { echo date('d-m-Y',strtotime($team_data[0]['task_start_date'])); } ?>" readonly />
												<label class="form-control-placeholder" for=""> Start Date <em style="color:red;">*</em></label>
												<span><?php echo form_error('launch_date'); ?></span>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" name="close_date" id="close_date" value="<?php if($mode == 'Add') { echo set_value('launch_date'); } elseif($mode=='Update' && $team_data[0]['task_end_date']!=NULL) { echo date('d-m-Y',strtotime($team_data[0]['task_end_date'])); } ?>" readonly />
												<label class="form-control-placeholder" for=""> End Date  </label>
												<span><?php echo form_error('close_date'); ?></span>
											</div>
										</div>										
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<input type="hidden" name="byt_member_slot_cnt" id="byt_member_slot_cnt" value="<?php if($mode == 'Add') { if(set_value('byt_member_slot_cnt') != "") { echo set_value('byt_member_slot_cnt'); } else { echo $min_team; } } else { echo count($team_slot_new_arr); } ?>">
									
									<div class="row myRow">										
										<div class="col-md-10 h-100">
											<div id="CreateTeamSlider" class="owl-carousel owl-theme">
												
												<?php 
												if($mode == 'Add') 
												{
													if(set_value('byt_member_slot_cnt') != "") { $min_team_limit = set_value('byt_member_slot_cnt'); }
													else { $min_team_limit = $min_team; }
												} else { $min_team_limit = count($team_slot_new_arr); }
												
												$k=0;
												for ($i=0; $i < $min_team_limit ; $i++) 
												{  
													$disp_flag = 1;
													$member_final_flag = 0;
													if($mode == 'Add')
													{
														if(set_value('byt_member_slot_cnt') != "") 
														{
															$mem_slot_type = set_value('team_type[team_type'.$i.']');
															if(!isset($mem_slot_type) || $mem_slot_type == '') { $disp_flag = 0; }
														}
													}
													
													if($disp_flag == 1)
													{	?>
														<div class="teammember byt_member_blocks_outer_cls" id="byt_member_blocks_outer<?php echo $i; ?>">
															<input type="hidden" name="team_slot_id[team_slot_id<?php echo $i; ?>]" value="<?php if($mode == 'Add') { if(set_value('team_slot_id[team_slot_id'.$i.']') != "") { echo set_value('team_slot_id[team_slot_id'.$i.']'); } else { echo "0"; } } else { echo $team_slot_new_arr[$i]['slot_id']; } ?>" class="form-control">
															
															<?php if($mode == 'Add')
															{	?>
																<img src="<?php if($i == 0) { echo $disp_photo; } else { echo $default_disp_photo; } ?>" alt="">
															<?php } 
															else
															{	?>
																<img src="<?php echo $team_slot_new_arr[$i]['profile_img']; ?>" alt="">
												<?php }	?>
															<h3>
																<?php if($i==0) 
																	{ 
																		echo $encrypt_obj->decrypt($login_user_data['0']['title'])." ".$encrypt_obj->decrypt($login_user_data['0']['first_name'])." ".$encrypt_obj->decrypt($login_user_data['0']['last_name']); 
																		$member_final_flag = 1; 
																	} 
																	else 
																	{ 
																		if($mode == 'Update') 
																		{ 
																			$displayName = trim($team_slot_new_arr[$i]['DispUserName']);
																			echo $displayName; 
																			if($displayName != "&nbsp;") { $member_final_flag = 1; } 
																		} 
																		else { echo "&nbsp;"; } 
																	} ?>
															</h3>
															
															<div class="form-group mt-4">
																<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;-webkit-transform: translateY(0px) scale(1); transform: translateY(0px) scale(1); ">Type of Member <em>*</em></label>
																<select class="form-control cls_team_type" name="team_type[team_type<?php echo $i; ?>]" id="team_type<?php echo $i; ?>" data-placeholder="Select" <?php if($mode == 'Update') { echo 'disabled'; } ?>>
																	<option value="">Select</option>
																	<?php
																		if(count($type_data) > 0)
																		{ 
																			foreach ($type_data as $key => $type) 
																			{ 																				
																				$team_type_sel = '';
																				if($mode == 'Add') 
																				{	
																					if(set_value('team_type[team_type'.$i.']') != "") { $team_type_sel = set_value('team_type[team_type'.$i.']'); }
																				}
																				else { $team_type_sel = $team_slot_new_arr[$i]['slot_type']; }
																				?>
																				<option value="<?php echo $type['tid'] ?>" <?php if($type['tid'] == $team_type_sel) { echo 'selected'; } ?>><?php echo $type['name'] ?></option>
																<?php } 
																		} ?>
																</select>																
															</div>
															
															<div class="boderBox">
																<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Select Skill Sets <em>*</em></label>
																<div class="boderBox65">
																	<select data-key="<?php echo $i ?>" class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset<?php echo $i; ?>][]" id="team_skillset<?php echo $i; ?>" data-placeholder="" multiple >
																		<?php if(count($skill_sets_data) > 0)
																			{	
																				foreach($skill_sets_data as $skill_sets)
																				{	
																					if(strtolower($skill_sets['name']) != 'other' && strtolower($skill_sets['name']) != 'others')
																					{
																						if($mode == 'Add') 
																						{
																							if(set_value('team_skillset[team_skillset'.$i.'][]') != "") { $team_skillset_arr = set_value('team_skillset[team_skillset'.$i.'][]'); }
																							else { if($i == 0) { $team_skillset_arr = $disp_skill_sets; } else { $team_skillset_arr = array(); } }
																						} else { $team_skillset_arr = explode(",",$team_slot_new_arr[$i]['skills']); }?>
																						<option data-id='<?php echo $skill_sets['name'] ?>' value="<?php echo $skill_sets['id']; ?>" <?php if(in_array($skill_sets['id'],$team_skillset_arr)) { echo 'selected'; } ?> ><?php echo $skill_sets['name']; ?></option>
																		<?php }	
																				}
																			}	?>
																	</select> 																	
																</div>

																<p style="margin:10px 0 0 0;font-size: 10px;line-height: 14px;text-align: justify;"><strong>Note:</strong> If the requisite field is not listed, type the required text and press #.</p>
															</div>
															
															<?php if($mode == 'Add') { $sel_other_skill = set_value('other_skill[other_skill'.$i.']'); } else { $sel_other_skill = $team_slot_new_arr[$i]['other_skill']; }  ?>
															<div class="other_div <?php if($sel_other_skill == "") { echo 'd-none'; } ?>" id="other_skill_div<?php echo $i ?>">															
																<div class="form-group">
																	<input type="text" name="other_skill[other_skill<?php echo $i; ?>]" value="<?php if($sel_other_skill != "") { if($mode == 'Add') { echo set_value('other_skill[other_skill'.$i.']'); } else { echo $team_slot_new_arr[$i]['other_skill']; }} ?>" id="other_skill<?php echo $i; ?>" class="form-control cls_other_skill" requiredxx="">
																	<label for="other_skill[other_skill<?php echo $i; ?>]" class="form-control-placeholder floatinglabel">Other Skillset  <em class="mandatory">*</em></label>
																</div>															
															</div>
															
															<div class="form-group mt-4">
																<input type="text" name="team_role[team_role<?php echo $i; ?>]" value="<?php if($mode == 'Add') { if(set_value('team_role[team_role'.$i.']') != "") { echo set_value('team_role[team_role'.$i.']'); } } else { echo $team_slot_new_arr[$i]['role_name']; } ?>" id="team_role<?php echo $i; ?>" class="form-control cls_team_role" maxlength="30">
																<label class="form-control-placeholder" for="cname">Role in Team <em>*</em></label>
																<div class="error" style="color:#F00"><?php echo form_error('team_role'); ?> </div>
															</div>
															
															<?php 
																$team_slot_id = 0;
																if($mode == 'Update') { $team_slot_id = $team_slot_new_arr[$i]['slot_id']; }  ?>
																
															<div class="delete_btn_slot_outer" data-id="<?php echo $i; ?>" data-slot_id="<?php echo $team_slot_id; ?>" data-member-final-flag="<?php echo $member_final_flag; ?>">
															<?php																
																if($i == 0)
																{	?>
																	<button type="button" name="remove" class="btn btn-primary w-100 mb-2 BtnAsLabel" disabled>Team Admin</button>
													<?php }
																else
																{	
																	if(count($type_data) >= $min_team) 
																	{	?>
																		<button type="button" name="remove" class="btn btn-primary w-100 mb-2" onclick="remove_slot('<?php echo $i; ?>', '<?php echo $team_slot_id; ?>', '<?php echo $member_final_flag; ?>')">Remove</button>
														<?php }
																	else
																	{	?>
																		<button type="button" name="remove" class="btn btn-primary w-100 mb-2" disabled style="visibility:hidden">Remove</button>
														<?php	}
														
																} ?>															
															</div>
															<p class="team_member_display_cnt">Team Member <?php echo $k+1; ?></p>
														</div>
												<?php $k++;
													}
												}	?>
											</div>
										</div>
										<div class="col-md-2 teammember d-flex align-items-center addSlot">
											<button class="add_slot_button" type="button" onclick="append_new_slot()" <?php if($max_team <= $min_team_limit) { echo 'disabled'; } ?>><span><i class="fa fa-plus-circle"></i></span>Add Slot</button>
										</div>
									</div>								
								</div>							
								
								<div id="step-3" class="mt-4">
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<textarea type="text" class="form-control" name="proposed_approach" id="proposed_approach"><?php if($mode == 'Add') { echo set_value('proposed_approach'); } else { echo $team_data[0]['proposed_approach']; } ?></textarea>
												<label class="form-control-placeholder floatinglabel" for="proposed_approach" style="margin-top: -11px;">Proposed Approach <em></em></label>
												<span class="error"><?php echo form_error('proposed_approach'); ?></span>
											</div>											
										</div>
									</div>
									
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<textarea type="text" class="form-control" name="additional_information" id="additional_information"><?php if($mode == 'Add') { echo set_value('additional_information'); } else { echo $team_data[0]['additional_information']; } ?></textarea>
												<label class="form-control-placeholder floatinglabel" for="additional_information" style="margin-top: -11px;">Additional Information <em></em></label>
												<span class="error"><?php echo form_error('additional_information'); ?></span>
											</div>											
										</div>
									</div>
									
									<div class="row"><div class="col-md-12"><h4 class="titleBox">Public Files</h4></div></div>
									<div class="file-details" style="min-height:100px;">
										<?php /* <div class="form-group"><label class="form-control-placeholder"></label></div><br> */ ?>
										<?php if($mode == 'Update' && count($team_files_data) > 0)
										{	
											echo '<div class="row">';
											foreach($team_files_data as $res)
											{	?>
												<div class="col-md-2" id="team_files_outer<?php echo $res['team_id']."_".$res['file_id']; ?>">
													<div class="file-list">
															<a href="javascript:void(0)" onclick="remove_team_file('<?php echo $res['team_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close"><i class="fa fa-remove"></i></a>
															<a class="file_ext_title" href="<?php echo base_url().'uploads/byt/'.$res['file_name']; ?>" target="_blank">
																<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
																if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$res['file_name']; }
																else { $disp_img_name = ''; } ?>
																
																<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
																else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
																<!--span><i class="fa fa-file"></i></span> View-->
															</a>
													</div>
												</div>
								<?php	}
											echo '</div>';
										}	?>
										
										<input type="hidden" name="customFileCount" id="customFileCount" value="0">
										<div class="row">											
											<div id="last_team_file_id"></div>
											<div class="col-md-2 custom-file" id="addFileBtnOuter">
												<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
												<input type="file" class="form-control fileUploadTeams" name="team_files[]" id="file-upload" />
											</div>
										</div>
									</div>
									
									<div class="row" style="margin-top:20px;"><div class="col-md-12"><h4 class="titleBox">Private Files</h4></div></div>
									<div class="file-details" style="min-height:80px;">
										<?php /* <div class="form-group"><label class="form-control-placeholder">Private Files</label></div><br> */ ?>
										<?php if($mode == 'Update' && count($team_files_private_data) > 0)
										{	
											echo '<div class="row">';
											foreach($team_files_private_data as $res)
											{	?>
												<div class="col-md-2" id="team_files_outer_private<?php echo $res['team_id']."_".$res['file_id']; ?>">
													<div class="file-list">
															<a href="javascript:void(0)" onclick="remove_team_file('<?php echo $res['team_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'private')" class="file-close"><i class="fa fa-remove"></i></a>
															<a class="file_ext_title" href="<?php echo base_url().'uploads/byt/'.$res['file_name']; ?>" target="_blank">
																<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
																if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$res['file_name']; }
																else { $disp_img_name = ''; } ?>
																
																<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
																else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
																<!--span><i class="fa fa-file"></i></span> View-->
															</a>
													</div>
												</div>
								<?php	}
											echo '</div>';
										} ?>
										
										<input type="hidden" name="customFileCountPrivate" id="customFileCountPrivate" value="0">
										<div class="row">											
											<div id="last_team_file_id_private"></div>
											<div class="col-md-2 custom-file" id="addFileBtnOuterPrivate">
												<label for="file-upload-private" class="custom-file-upload-private mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
												<input type="file" class="form-control fileUploadTeamsPrivate" name="team_files_private[]" id="file-upload-private" />
											</div>
										</div>
										
										<div class="row" style="margin-top:30px;">	
											<div class="col-md-6">
												<label class="form-control-placeholder floatinglabel">Share files with Team Members</label>
												<label class="switch" style="margin-top:25px;">
													<input type="checkbox" id="share_files" name="share_files" <?php if($mode == 'Update') { if($team_data[0]['share_files']=='yes'){echo 'checked';} } ?>> 
													<span class="slider round"></span>
												</label>
											</div>
										</div>
									</div>
								</div><br>
								
								<?php if($mode == 'Add') { ?>
								<div id="step-4" class="">
									<input type="hidden" name="invite_row_cnt" id="invite_row_cnt" value="1">
									<div class="row" id="byt_invite_row_0">
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control invitation_email_input" name="invite_email[]" id="invite_email1" value="" >
												<label class="form-control-placeholder floatinglabel" for="invite_email">Please enter the email address of the candidate you would like to invite </label>
												<span class="error"><?php echo form_error('invite_email'); ?></span>
											</div>
										</div>
									</div>									
									<div id="byt_invite_last"></div>									
									<button type="button" class="btn btn-primary btn-sm" onclick="append_invite_row()"><i class="fa fa-plus"></i> Add More</button>
								</div>	
								<?php } ?>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	//VALIDATION SCROLLING TO TOP
	function scroll_to_top(div_id)
	{
		if(div_id == '' || div_id==undefined) { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
		
	//REMOVE BANNER PREVIEW / IMAGE
	function remove_banner_img(flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the banner image?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				if(flag == 0)
				{
					$('#team_banner').val('');
					$("#team_banner").parent().find('input').next("span").remove();
				}
				else
				{
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();
					var data = { 
						'tbl_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('arai_byt_teams'); ?>")), 
						'pk_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('team_id') ?>")), 
						'del_id': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt($team_data[0]['team_id']); ?>")), 
						'input_nm': encodeURIComponent($.trim('team_banner')), 
						'file_path': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('./uploads/byt/'); ?>")), 
						'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
					};          
					$.ajax(
					{ 
						type: "POST", 
						url: '<?php echo site_url("delete_single_file") ?>', 
						data: data, 
						dataType: 'JSON',
						success:function(data) 
						{ 
							$("#csrf_token").val(data.csrf_new_token);
							/* $("#"+input_nm+"_outer").remove();
							
							swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" }); */
						}
					});
					<?php } ?>
				}
				
				$("#team_banner_outer").addClass('d-none');
				$("#team_banner_outer .previous_img_outer img").attr("src", "");
				$("#preloader").css("display", "none");
			}
		});
	}
	
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function TeamBannerPreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#team_banner_outer").removeClass('d-none');
					//$("#team_banner_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#team_banner_outer .previous_img_outer img").attr("src", e.target.result);
					$("#team_banner_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#team_banner_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#team_banner").change(function() { TeamBannerPreview(this); });	
	
	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_team_file(div_id, file_id, public_private_flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				
				if(file_id!= '' && file_id != '0')
				{
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("team/delete_team_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				
				if(public_private_flag == 'public')
				{
				$("#team_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				else if(public_private_flag == 'private')
				{
					$("#team_files_outer_private"+div_id).remove();
					$(".btnOuterForDelPrivate"+div_id).remove();
				}
				
				$("#preloader").css("display", "none");
			}
		});
	}
	
	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function TeamFilesPreview(input, public_private_flag) 
	{
		console.log(public_private_flag);
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
			var customFileCount = $("#customFileCount").val();
			var disp_filename_final = '';
			var disp_filename = input.files[0].name;
			var reader = new FileReader();
			var j = customFileCount;
			}
			else if(public_private_flag == 'private')
			{			
				var customFileCount = $("#customFileCountPrivate").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}

			var upload_file_size = input.files[0].size; 

			if(upload_file_size < 10000000)
			{
							
			var disp_img_name = "";
			var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
			disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
				
			var id_name = '';
			var btn_id_name = '';
			var input_cls_name = '';
			var input_type_name = '';
			var fun_var_parameter = "";
			var file_upload_common = '';
			if(public_private_flag == 'public')
			{
				id_name = 'team_files_outer'+j;
				btn_id_name = 'addFileBtnOuter';
				input_cls_name = 'fileUploadTeams';
				input_type_name = 'team_files[]';
				fun_var_parameter = "'public'";
				file_upload_common = 'file-upload';
			}
			else if(public_private_flag == 'private')
			{
				id_name = 'team_files_outer_private'+j;
				btn_id_name = 'addFileBtnOuterPrivate';
				input_cls_name = 'fileUploadTeamsPrivate';
				input_type_name = 'team_files_private[]';
				fun_var_parameter = "'private'";
				file_upload_common = 'file-upload-private';
			}
			
			var append_str = '';
			append_str += '	<div class="col-md-2 team_files_outer_common" id="'+id_name+'">';
			append_str += '		<div class="file-list">';
			append_str += '			<a href="javascript:void(0)" onclick="remove_team_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
			//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
			append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
			append_str += '		</div>';
			append_str += '	</div>';				
				
			var btn_str = '';
			btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
			btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
			btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="TeamFilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
			btn_str +=	'	</div>';
				
			if(public_private_flag == 'public')
			{
			$("#addFileBtnOuter").addClass('d-none');
			$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
			$("#addFileBtnOuter").removeAttr( "id" );
				
			$(append_str).insertBefore("#last_team_file_id");
			$(btn_str).insertAfter("#last_team_file_id");
			$("#customFileCount").val(parseInt(customFileCount)+1);
			}
			else if(public_private_flag == 'private')
			{
				$("#addFileBtnOuterPrivate").addClass('d-none');
				$("#addFileBtnOuterPrivate").addClass('btnOuterForDelPrivate'+j+'');
				$("#addFileBtnOuterPrivate").removeAttr( "id" );
				
				$(append_str).insertBefore("#last_team_file_id_private");
				$(btn_str).insertAfter("#last_team_file_id_private");
				$("#customFileCountPrivate").val(parseInt(customFileCount)+1);
			}

			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 10 MB','error');
			}
				
				$("#preloader").css("display", "none");
			}
	}
	$(".fileUploadTeams").change(function() { TeamFilesPreview(this,'public'); });	
	$(".fileUploadTeamsPrivate").change(function() { TeamFilesPreview(this,'private'); });
	
	//ENABLE-DISABLE HIDE ADD SLOT BUTTON
	function show_hide_add_slot_btn()
	{
		var min_team_size = '<?php echo $min_team; ?>';
		var max_team_size = '<?php echo $max_team; ?>';
		var current_slot_cnt = $('.byt_member_blocks_outer_cls').length;
		
		if(current_slot_cnt == max_team_size) { /* $(".add_slot_button").addClass('d-none'); */ $(".add_slot_button").attr("disabled",true); }
		else { /* $(".add_slot_button").removeClass('d-none'); */ $(".add_slot_button").attr("disabled",false); }
	}
	show_hide_add_slot_btn();
	
	//APPEND NEW SLOT
	function append_new_slot()
	{
		<?php /* if($mode == "Add") { ?> 
			if($(".cls_team_type").valid()==false || $(".cls_team_skillset").valid()==false || $(".cls_team_role").valid()==false) { return false; } 
		<?php 
			}
			else 
			{	?>
				if($(".cls_team_skillset").valid()==false || $(".cls_team_role").valid()==false) { return false; }
<?php }  */?>		

		var min_team_size = '<?php echo $min_team; ?>';
		var max_team_size = '<?php echo $max_team; ?>';
		var current_slot_cnt = $('.byt_member_blocks_outer_cls').length;
		var byt_member_slot_cnt = $("#byt_member_slot_cnt").val();
		
		var append_flag = 0;
		if(current_slot_cnt < max_team_size) { append_flag = 1; }			
			
		if(append_flag == 1)
		{
			var appent_str = '';
			appent_str += '	<div class="teammember byt_member_blocks_outer_cls" id="byt_member_blocks_outer'+byt_member_slot_cnt+'">';
			appent_str += '		<input type="hidden" name="team_slot_id[team_slot_id'+byt_member_slot_cnt+']" value="0" class="form-control">';
			appent_str += '		<img src="<?php echo $default_disp_photo; ?>" alt="experts">';
			appent_str += '		<h3><?php echo "&nbsp;"; ?></h3>';
			appent_str += '		<div class="form-group mt-4">';
			appent_str += '			<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;-webkit-transform: translateY(0px) scale(1); transform: translateY(0px) scale(1); ">Type of Member <em>*</em></label>';
			appent_str += '			<select class="form-control cls_team_type" name="team_type[team_type'+byt_member_slot_cnt+']" id="team_type'+byt_member_slot_cnt+'" data-placeholder="">';
			appent_str += '				<option value="">Select</option>';
														<?php
														if(count($type_data) > 0)
														{ 
															foreach ($type_data as $key => $type) 
															{ ?>
																appent_str += '	<option value="<?php echo $type["tid"] ?>"><?php echo $type["name"] ?></option>';
												<?php } 
														} ?>
			appent_str += '			</select>';
			appent_str += '		</div>';
			appent_str += '		<div class="boderBox">';
			appent_str += '			<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Select Skill Sets <em>*</em></label>';
			appent_str += '			<div class="boderBox65">';
			appent_str += '				<select data-key="'+byt_member_slot_cnt+'" class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset'+byt_member_slot_cnt+'][]" id="team_skillset'+byt_member_slot_cnt+'" data-placeholder="" multiple >';
															<?php if(count($skill_sets_data) > 0)
															{	
																foreach($skill_sets_data as $skill_sets)
																{ 
																	if(strtolower($skill_sets['name']) != 'other' && strtolower($skill_sets['name']) != 'others')
																	{	?>
																		appent_str += '<option data-id="<?php echo $skill_sets["name"] ?>" value="<?php echo $skill_sets["id"]; ?>"><?php echo $skill_sets["name"]; ?></option>';
														<?php }
																}
															}	?>
			appent_str += '				</select>';
			appent_str += '			</div>';
			appent_str += "			<p style='margin:10px 0 0 0;font-size: 10px;line-height: 14px;text-align: justify;'><strong>Note:</strong> If the requisite field is not listed, type the required text and press #.</p>";
			appent_str += '		</div>';
			
			appent_str += '		<div class="other_div d-none" id="other_skill_div'+byt_member_slot_cnt+'">';	
			appent_str += '			<div class="form-group">';
			appent_str += '				<input type="text" name="other_skill[other_skill'+byt_member_slot_cnt+']" value="" id="other_skill'+byt_member_slot_cnt+'" class="form-control cls_other_skill" requiredxx="">';
			appent_str += '				<label for="other_skill[other_skill'+byt_member_slot_cnt+']" class="form-control-placeholder floatinglabel">Other Skillset  <em class="mandatory">*</em></label>';
			appent_str += '			</div>';
			appent_str += '		</div>';
			
			appent_str += '		<div class="form-group mt-4">';
			appent_str += '			<input type="text" name="team_role[team_role'+byt_member_slot_cnt+']" value="" id="team_role'+byt_member_slot_cnt+'" class="form-control cls_team_role" maxlength="30">';
			appent_str += '			<label class="form-control-placeholder" for="cname">Role in Team <em>*</em></label>';
			appent_str += '		</div>';			
			appent_str += '		<div class="delete_btn_slot_outer" data-id="'+byt_member_slot_cnt+'" data-slot_id="0" data-member-final-flag="0">';			
			appent_str += '		<button class="remove_slot_button btn btn-primary w-100 mb-2" type="button" onclick="remove_slot('+byt_member_slot_cnt+',0,0)">Remove</button>';
			appent_str += '		</div>';
			appent_str += '		<p class="team_member_display_cnt">Team Member </p>';
			appent_str += '	</div>';
			
			//$(appent_str).insertBefore("#byt_member_blocks_last");
			$('#CreateTeamSlider').trigger('add.owl.carousel', [appent_str]).trigger('to.owl.carousel', $('.byt_member_blocks_outer_cls').length).owlCarousel('update');
			//$('#CreateTeamSlider');
					
			applyFloatingCls();
			$('.select2_common').select2(
			{
				tags: true,
				tokenSeparators: [',','#']
			});
			
			$("input[type='text']").change( function() 
			{
				if ($(this).val() != '') { $(this).parent().find('label').addClass('floatinglabel'); }
				else { $(this).parent().find('label').removeClass('floatinglabel'); }
			});
			
			$(".Skillset, .boderBox, .boderBox65").mCustomScrollbar();
			
			$("input.form-control").keydown(function(event)
			{
				if(event.keyCode == 13) { event.preventDefault(); return false; }
			});
			
			$("#byt_member_slot_cnt").val(parseInt(byt_member_slot_cnt)+1);
			show_hide_add_slot_btn();
			reset_team_member_cnt();
			display_delete_btn_slot();
		}	
	}
	
	function display_delete_btn_slot()
	{ 
		var min_team_size = '<?php echo $min_team; ?>';
		var current_slot_cnt = $('.byt_member_blocks_outer_cls').length;
		
		var i=1;
		$('.delete_btn_slot_outer').each(function()
		{	 
			if(i > 1)
			{
				if(min_team_size < current_slot_cnt)
				{
					var ival = $(this).attr("data-id");
					var slot_val = $(this).attr("data-slot_id");
					var member_final_flag = $(this).attr("data-member-final-flag");
					var onclick_fun = "remove_slot('"+ival+"', '"+slot_val+"', '"+member_final_flag+"')";
					$(this).html('<button type="button" name="remove" class="btn btn-primary w-100 mb-2" onclick="'+onclick_fun+'">Remove</button>');
				}
				else
				{
					$(this).html('<button type="button" name="remove" class="btn btn-primary w-100 mb-2" disabled style="visibility:hidden">Remove</button>');
				}
			}
			i++;
		});
	}
	
	function reset_team_member_cnt()
	{
		var i=1;
		$('.team_member_display_cnt').each(function()
		{	 
			$(this).html('Team Member '+i);
			i++;
		});
	}
	
	//REMOVE SLOT
	function remove_slot(div_no, team_slot_id, member_final_flag)
	{
		if(member_final_flag == 0)
		{
			swal(
			{			
				title:"DELETE?" ,
				text: "Are you sure you want to delete this team slot?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'			
			}).then(function (result) 
			{
				if (result.value) 
				{
					if(team_slot_id != '0') 
					{ 
						$("#preloader").css("display", "block");
						var csrf_test_name = $("#csrf_token").val();
						var data = { 'team_slot_id': encodeURIComponent($.trim(team_slot_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
						$.ajax(
						{ 
							type: "POST", 
							url: '<?php echo site_url("team/delete_team_slot_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							async: false,
							cache : false, 
							success:function(data) 
							{ 
								$("#csrf_token").val(data.csrf_new_token);																										
							}
						});
					}
					
					var x= 0;
					$('.byt_member_blocks_outer_cls').each(function() 
					{
						if(this.id == "byt_member_blocks_outer"+div_no)
						{
							$("#CreateTeamSlider").trigger('remove.owl.carousel', [x]).owlCarousel('update');
						}						
						x++;
						display_delete_btn_slot();
					});

					//$("#byt_member_blocks_outer"+div_no).remove();
					
					reset_team_member_cnt();
					$("#preloader").css("display", "none");
					swal({ title: "Success", text: 'Slot successfully deleted', type: "success" });
					show_hide_add_slot_btn();
				}
			});	
		}
		else
		{
			swal({ title: "Alert", text: 'Slot cannot be removed as it is already filled. Please remove the member first and try again.', type: "warning" });
		}
	}	
	
	//CURRENTLY NOT IN USE
	/* function xxappend_byt_files_row()
	{
		var total_div_cnt = $('input[name*="team_files[]"]').length;
		if(total_div_cnt >= 15)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#files_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_file_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-10">';
			append_html += '			<div class="form-group upload-btn-wrapper">';
			append_html += '				<button class="btn btn-upload"><i class="fa fa-plus"> </i> Browse</button>';
			append_html += '				<input type="file" class="form-control" name="team_files[]" id="team_files'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_byt_files_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#files_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_team_files_last");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	function xxremove_byt_files_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_file_row_"+div_no).remove();
			}
		});	
	} */
	
	//CURRENTLY NOT IN USE
	function append_invite_row()
	{
		if($(".invitation_email_input").valid()==false) { return false; }
		
		var total_div_cnt = $('input[name*="invite_email[]"]').length;
		if(total_div_cnt >= 25)
		{
			swal({ title: "Alert", text: "You can not invite more than "+total_div_cnt+" members at a time", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#invite_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_invite_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-11">';
			append_html += '			<div class="form-group upload-btn-wrapper" style="margin-top:0">';			
			append_html += '				<input type="text" class="form-control invitation_email_input" name="invite_email[]" id="invite_email'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group" style="margin-top:0">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#invite_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_invite_last");
		}
	}
		
	function remove_current_div_invite(div_no)
	{
		$("#byt_invite_row_"+div_no).remove();
		// swal(
		// {
		// 	title:"DELETE?" ,
		// 	text: "Are you sure you want to delete this entry?",
		// 	type: 'warning',
		// 	showCancelButton: true,
		// 	confirmButtonColor: '#3085d6',
		// 	cancelButtonColor: '#d33',
		// 	confirmButtonText: 'Yes!'
		// }).then(function (result) 
		// {
		// 	if (result.value) 
		// 	{
		// 		$("#byt_invite_row_"+div_no).remove();
		// 	}
		// });	
	}
		
	$('body').on('change', '.cls_team_skillset', function () 
	{
		var selected = $(this).find('option:selected', this);
		var results = [];
		
		var element_key=$(this).data('key');
		//console.log(element_key)	
		
		selected.each(function() { results.push($(this).data('id')); });
		var display_flag=0;
		$.each(results,function(i) { if(results[i]=='Other') { display_flag = 1; } });
		
		if (display_flag==1) 
		{
			//$("#other_skill_div"+element_key).removeClass('d-none')
		}
		else
		{			
			$("#other_skill"+element_key).val('');
			$("#other_skill_div"+element_key).addClass('d-none')			
		}		
	});
	
	//CURRENTLY NOT IN USE
	/* function delete_team_file(file_id)
	{ 
			swal(
			{
					title:"Confirm?" ,
					text: "Are you confirm to delete the File?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
			}).then(function (result) 
			{
					if (result.value) 
					{
						$("#preloader").css("display", "block");
						var csrf_test_name = $("#csrf_token").val();
						var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
						$.ajax(
						{ 
								type: "POST", 
								url: '<?php echo site_url("team/delete_team_file_ajax") ?>', 
								data: data, 
								dataType: 'JSON',
								success:function(data) 
								{ 
										$("#csrf_token").val(data.csrf_new_token);
										$("#team_file_"+data.file_id).remove();
										$("#preloader").css("display", "none");
										swal({ title: "Success", text: 'File successfully deleted', type: "success" });
								}
						});
					}
			});
	} */ 	
	
	function swal_confirm_popup()
	{

		swal(
		{
			title:"Confirm?" ,
			text: "Are you sure you want to cancel?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				var mode_check = '<?php echo $mode ?>';
				if(mode_check=='Update'){
					window.location.href ='<?php echo site_url('myteams/myCreatedteams') ?>'; 
				}else{
					window.location.href = '<?php echo site_url('tasks/taskSearch') ?>'; 
				}
				
			}
		});
	}
	
	//STEP WIZARD
	$(document).ready(function() 
	{
	
		//******* STEP SHOW EVENT *********
		$("#smartwizard_apply_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') { $(".sw-btn-prev").hide(); $(".sw-btn-next").show(); $(".btnfinish").hide(); $(".btnCancelCustom").show();  $(".btnCancelCustom").addClass( "btnCancelCustomStep1" ); $(".sw-btn-group-extra").addClass( "sw-btn-group-extra-step1" ); } 
			else if (stepPosition === 'final') { $(".sw-btn-prev").show(); $(".sw-btn-next").hide(); $(".btnfinish").show(); $(".btnCancelCustom").show(); $(".btnCancelCustom").removeClass( "btnCancelCustomStep1" ); $(".sw-btn-group-extra").removeClass( "sw-btn-group-extra-step1" ); } 
			else { $(".sw-btn-prev").show(); $(".sw-btn-next").show(); $(".btnfinish").hide(); $(".btnCancelCustom").hide(); $(".btnCancelCustom").removeClass( "btnCancelCustomStep1" ); $(".sw-btn-group-extra").removeClass( "sw-btn-group-extra-step1" ); }
		});
		
		var cancel_onclick_fun = "swal_confirm_popup()";
		//******* STEP WIZARD *********
		$('#smartwizard_apply_challenge').smartWizard(
		{
			/*selected: 3,*/   
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			keyNavigation: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */	

			  anchorSettings: {
			      anchorClickable: true, // Enable/Disable anchor navigation
			      enableAllAnchors: true, // Activates all anchors clickable all times
			      markDoneStep: true, // Add done state on navigation
			      markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
			      removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
			      enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
			  },	

			  
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
				$('<button></button>').text(<?php if($mode == 'Add'){ ?>'Create Team'<?php }else { ?> 'Update Team' <?php } ?>).addClass('btn btn-primary btnfinish').on('click', function(e)
				{ 
					e.preventDefault();						
					var submit_flag = 0;
					
					/* if($("#proposed_approach").valid()==false) { submit_flag = 1; $("#proposed_approach").focus(); }					
					if($("#proposed_approach").valid()==false) { scroll_to_top('proposed_approach'); } */
					
					<?php if($mode == "Add"){ ?>if($(".invitation_email_input").valid()==false) { submit_flag = 1; }	<?php } ?>
					
					if(submit_flag == 0)
					{
						swal(
						{
							title:"Confirm",
							<?php if($mode == 'Add'){ ?> text: "Save your details and build your Team?", <?php } else { ?>text: "Are you sure you want to update the team details?", <?php } ?>
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Yes!'
						}).then(function (result) 
						{ 
							if (result.value) 
							{ 
								$("#ApplyChallengeForm").submit();
							} 
						});
					}
				}),
				$('<a style="border-radius: 4px;" href="javascript:void(0)" onclick="'+cancel_onclick_fun+'" ></a>').text('Cancel').addClass('btn btn-primary btnCancelCustom')
				]
			}
		});
		
		$("#smartwizard_apply_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{

			if(stepNumber==2 && stepDirection=="forward"){
				$(".sw-btn-group-extra").addClass('step_wiz_buttons');
			}else{
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons');
			}

			var mode_check_new = '<?php echo $mode ?>';
			if(mode_check_new=='Update'){	
			if( stepNumber==1 && stepDirection=="forward" ){
				$(".sw-btn-group-extra").addClass('step_wiz_buttons');
			}else{
				$(".sw-btn-group-extra").removeClass('step_wiz_buttons');
			}
				}
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{		
		
				if($("#team_details").valid()==false) { isValidate= false; $("#team_details").focus(); }
				//if($("#team_size").valid()==false) { isValidate= false; $("#team_size").focus(); }
				if($("#team_banner").valid()==false) { isValidate= false; $("#team_banner").focus(); }
				if($("#team_name").valid()==false) { isValidate= false; $("#team_name").focus(); }
				if($("#task_name").valid()==false) { isValidate= false; $("#task_name").focus(); }
				if($("#launch_date").valid()==false) { isValidate= false;  }
				

				if($("#task_name").valid()==false) { scroll_to_top('task_name'); }
				else if($("#team_name").valid()==false) { scroll_to_top('team_name'); }
				else if($("#team_banner").valid()==false) { scroll_to_top('team_banner'); }
				//else if($("#team_size").valid()==false) { scroll_to_top('team_size'); }
				else if($("#team_details").valid()==false) { scroll_to_top('team_details'); }
				else if($("#launch_date").valid()==false) { scroll_to_top('launch_date'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				
				//if(isValidate == true) { append_team_member_blocks(); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				if($(".cls_team_type").valid()==false) { isValidate = false; }
				if($(".cls_team_skillset").valid()==false) { isValidate = false; }
				if($(".cls_team_role").valid()==false) { isValidate = false; }			
				scroll_to_top('smartwizard_apply_challenge');
			}
			
			/* if(stepNumber==2 && stepDirection=="forward")
				{				
				if($("#from_age").valid()==false) { isValidate= false; $("#from_age").focus(); }
				if($("#educational").valid()==false) { isValidate= false; $("#educational").focus(); }
				
				if($("#educational").valid()==false) { scroll_to_top('educational'); }
				else if($("#from_age").valid()==false) { scroll_to_top('from_age'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				}		
			*/
			
			if(stepDirection=="backward") { scroll_to_top('smartwizard_apply_challenge') }
			return isValidate;
		})
	});
	
	$(document).ready(function() 
	{
		$("input.form-control").keydown(function(event)
		{
			if(event.keyCode == 13) 
			{
				event.preventDefault();
				return false;
			}
		});
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>
<?php
	$current_date = date('d-m-Y');
	$date_after_90_days = date('d-m-Y', strtotime("+3 months", strtotime($current_date)));
?>
<script>
	// Datepicker
	var FromDateFun = $('#launch_date').datepicker(
	{               
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy",              
		startDate:"<?php echo $current_date; ?>",
		endDate:"<?php echo $date_after_90_days; ?>"
	}).
	on('changeDate', function(ev)
	{             
		var FromDateVal = $("#launch_date").val();
		var ToDateVal = $("#close_date").val();
	 
		var newToDateStart = FromDateVal;
	 
		var temp1 = FromDateVal.split('-');
		var convertFromDateVal = temp1[2] + '-' + temp1[1] + '-' + temp1[0].slice(-2);         
		var d = new Date(convertFromDateVal);
		d.setMonth(d.getMonth() + 3);
		var yyyy = (d.getFullYear());
		var mm = (d.getMonth());
		var dd = (d.getDate());             
		//var newToDateEnd = (yyyy+"-"+("0" + (mm + 1)).slice(-2)+"-"+("0" + (dd + 1)).slice(-2));
		var newToDateEnd = (("0" + (dd + 1)).slice(-2)+"-"+("0" + (mm + 1)).slice(-2)+"-"+yyyy);
	 
		/* console.log(newToDateStart);
		console.log(newToDateEnd); */
	 
		var temp2 = ToDateVal.split('-');
		var convertToDateVal = temp2[2] + '-' + temp2[1] + '-' + temp2[0].slice(-2);
	 
		var temp3 = newToDateStart.split('-');
		var convertnewToDateStart = temp3[2] + '-' + temp3[1] + '-' + temp3[0].slice(-2);
	 
		var temp4 = newToDateEnd.split('-');
		var convertnewToDateEnd = temp4[2] + '-' + temp4[1] + '-' + temp4[0].slice(-2);
	 
		var update_flag = 1;
		if(convertToDateVal == "") { update_flag = 1; }
		else if(new Date(convertToDateVal) >= new Date(convertnewToDateStart) && new Date(convertToDateVal) <= new Date(convertnewToDateEnd)) { update_flag = 0; }
	 
		$('#close_date').datepicker('setStartDate', newToDateStart);
		$('#close_date').datepicker('setEndDate', newToDateEnd);
	 
		if(update_flag == 1)
		{
			$('#close_date').datepicker('update', newToDateStart);
		}             
	 
	  
	}).data('datepicker');
 
	var ToDateFun = $('#close_date').datepicker(
	{
	  
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy",               
		startDate:"<?php echo $current_date; ?>",
		endDate:"<?php echo $date_after_90_days; ?>"
	});
</script>
<script type="text/javascript">
	$('.select2_common').select2(
	{
    tags: true,
    tokenSeparators: [',','#']
	});
	
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		$("input.numbers").keypress(function(event) { return /\d/.test(String.fromCharCode(event.keyCode)); });
		
		$.validator.addMethod("type_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Type of Member');		
		$.validator.addClassRules("cls_team_type", { type_required: true });
		
		$.validator.addMethod("skillset_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Skill Sets');		
		$.validator.addClassRules("cls_team_skillset", { skillset_required: true });
		
		/* $.validator.addMethod("team_other_skill_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Other Skill');		
		$.validator.addClassRules("cls_other_skill", { team_other_skill_required: true, letters_number_space: true }); */
		
		$.validator.addMethod("team_role_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Role in Team');
		
		$.validator.addMethod("CustomMaxlength", function(value, element, param) { if($.trim(value).length <= param ) { return true; } else { return false; } },'Please enter maximum 30 character');		
		$.validator.addClassRules("cls_team_role", { team_role_required: true, letters_number_space: true, CustomMaxlength:30 });
		
		$.validator.addMethod("invitation_email_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Email');
		$.validator.addMethod("invitation_email_valid", function(value, element) { if ($.trim(value) != '') { var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/); return pattern.test(value); } else { return true; } },'Please enter a valid email address');
		
		$.validator.addMethod("invitation_email_unique", function(value, element) 
		{
			var valid_flag = 0;
		 	var current_email = value;
		 	var current_id = element.id;

		    $('.invitation_email_input').each(function() 
		    {
		        if ($(this).val() == current_email && $(this).attr('id') != current_id && current_email != '')
		        {
		          
		           valid_flag = 1;
		        }

		    });

		    if(valid_flag == 0) { return true; }
		    else { return false; }
			
			
		},'Please enter a unique email address');

		$.validator.addClassRules("invitation_email_input", { /* invitation_email_required: true, */ invitation_email_valid: true,
			 invitation_email_unique:true, 
		});
		
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		$.validator.addMethod("letters_number_space", function(value, element) 
		{ 
			/* return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value); */ 
			return true;
		}, 'Special characters are not allowed');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\'\- \ ]+$/i.test(value);
		}, "Name must contain only letters, numbers, or dashes.");



		
		
		
		
		
		//******* JQUERY VALIDATION *********
		$("#ApplyChallengeForm").validate( 
		{
			/* onkeyup: false, */
			ignore: ":disabled",
			rules:
			{
				task_name:{required: true, nowhitespace: true, valid_value: true},
				team_name: { required: true, nowhitespace: true, valid_value: true/* , 
				maxCount:['20'] */ },
				team_banner: { valid_img_format: true, maxsize: 2000000 },
				//team_size: { required: true, nowhitespace: true, digits:true, min: parseInt("<?php echo $min_team; ?>"), max: parseInt("<?php echo $max_team; ?>") },
				team_details: { required: true, nowhitespace: true, letters_number_space: true, minCount:['5'],  maxCount:['80'] /* , minCount:['20'], maxCount:['80'] */ },
				launch_date : { required: true},
			},
			messages:
			{
				task_name: { required: "Please enter the Task Name", nowhitespace: "Please enter the Task Name" },
				team_name: { required: "Please enter the Name of the Team", nowhitespace: "Please enter the Name of the Team" },
				team_banner: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				//team_size: { required: "Please enter the Team Size", nowhitespace: "Please enter the Team Size", digits: "Please enter only numbers in Team Size", min:"Please enter a value greater than or equal to <?php echo $min_team; ?>", max:"Please enter a value less than or equal to <?php echo $max_team; ?>" },
				team_details: { required: "Please enter some information about your team", nowhitespace: "Please enter some information about your team" },
			},
			errorElement: 'span',
			errorPlacement: function (error, element) 
			{
				if(element.hasClass('select2_common') && element.next('.select2-container').length) 
				{
					error.insertAfter(element.parent());
				}
				else { element.closest('.form-group').append(error);  }
			},
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); },
			invalidHandler: function(e,validator) 
			{
				//validator.errorList contains an array of objects, where each object has properties "element" and "message".  element is the actual HTML Input.
				for (var i=0;i<validator.errorList.length;i++){
					console.log(validator.errorList[i]);
				}
				//validator.errorMap is an object mapping input names -> error messages
				for (var i in validator.errorMap) {
				  console.log(i, ":", validator.errorMap[i]);
				}
			}
		});		
		
		$(document).on("change", ".select2_common", function() 
		{
			var isValidate = true;
			var form = $( "#ApplyChallengeForm" );
			if(form.valid()==false) { isValidate= false; }
			return isValidate;
		});
	});
</script>
<script>
// $(document).ready(function() 
// 	{
// 		$(".sw-btn-group-extra").addClass('step_wiz_buttons');
// 	});	
</script>

<?php if($CustomVAlidationErr != '') { ?><script>swal({ title: "Alert", text: '<?php echo $CustomVAlidationErr; ?>', type: "warning" });</script><?php } ?>