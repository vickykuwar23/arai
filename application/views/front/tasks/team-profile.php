<?php 
error_reporting(0);
// Create Object
$encrptopenssl_team =  New Opensslencryptdecrypt();
$team_ID  			= $show_details[0]['team_id'];
$team_name   		= ucwords($show_details[0]['team_name']);
$team_banner   		= $show_details[0]['team_banner'];
$team_size   		= $show_details[0]['team_size'];
$brief_team_info   	= ucfirst(trim($show_details[0]['brief_team_info']));
$proposed_approach	= ucfirst(trim($show_details[0]['proposed_approach']));
$additional_info	= ucfirst(trim($show_details[0]['additional_information']));
$ch_owner_status	= $show_details[0]['challenge_owner_status'];
$team_status   		= $show_details[0]['team_status'];
$challenge_title	= $encrptopenssl_team->decrypt($show_details[0]['challenge_title']);
$challenge_id  		= $show_details[0]['challenge_id'];
$c_id  				= $show_details[0]['c_id'];

if($team_banner!=""){
	$fileExist = base_url('uploads/byt/'.$team_banner);
	 if (@GetImageSize($fileExist)) {
		$image = $fileExist;
	} else {
		$image = base_url('assets/default-banner.jpg');
	}
} else {
	$image = base_url('assets/default-banner.jpg');
}	



?>

<style>

.home-p.pages-head4 {
	background: url(<?php echo $image; ?>) no-repeat center top;
	background-size: 100%; min-height: 100%;
}
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}

table#show-hide{display:none;}

</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down">
    <div class="container">
      <!-- <h1 class="wow fadeInUp" data-wow-delay="0.1s">Challenge Details</h1>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb wow fadeInUp">
          <li class="breadcrumb-item"><a href="#">Challenge</a></li>
          <li class="breadcrumb-item active" aria-current="page">Challenge Details </li>
        </ol>
      </nav> -->
    </div>
</div>
<section>
    <div class="challenges-title">
      <div class="row">
        <div class="col-sm-12">
          <h3><a href="javascript:void(0);">Team Details</a></h3>
        </div>       
      </div>
    </div>
	<div class="container">
	  <div class="row">
		<div class="col">
			<div class="col-md-6" style="width:100%;">
				<p><b>Team ID : </b><?php echo $team_ID; ?></p>
				<p><b>Team Name : </b><?php echo $team_name; ?></p>
			</div>
		</div>
		<div class="col">
			<div class="col-md-6"  style="width:100%;">
				<p><b>Challenge ID : </b><?php echo $challenge_id; ?></p>
				<p><b>Challenge Name : </b><a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($c_id)); ?>"><?php echo $challenge_title; ?></a></p>
			</div>
		</div>		
	  </div>
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><b>Brief Information About Team : </b><?php echo $brief_team_info; ?></p>
			</div>
	  </div>
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><b>Proposed Approach : </b><?php echo $proposed_approach; ?></p>
			</div>
	  </div>
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><b>Additional Information :</b> <?php echo $additional_info; ?></p>
			</div>
	  </div>
	  <?php 
		$getFiles = $this->master_model->getRecords("byt_team_files", array('team_id' => $team_ID));
		if(count($getFiles) > 0){
		foreach($getFiles as $chkfile){
	  ?>
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><a href="<?php echo base_url('uploads/byt/'.$chkfile['file_name']); ?>" target="_blank">View</a></p>
			</div>
	  </div>
	  <?php }

	  } ?>
	  
	  <?php
				
		foreach($show_slotlist as $slot){
			
			$this->db->join('registration','byt_slot_applications.apply_user_id = registration.user_id');
			$memberList = $this->master_model->getRecords("byt_slot_applications", array('byt_slot_applications.slot_id' => $slot['slot_id']));
			//echo "<pre>";print_r($memberList);
			
			$tile 			= ucfirst($encrptopenssl_team->decrypt($memberList[0]['title']));
			$first_name 	= ucfirst($encrptopenssl_team->decrypt($memberList[0]['first_name']));
			$middle_name 	= ucfirst($encrptopenssl_team->decrypt($memberList[0]['middle_name']));
			$last_name 		= ucfirst($encrptopenssl_team->decrypt($memberList[0]['last_name']));
			$fullN			= $tile." ".$first_name." ".$middle_name." ".$last_name;
			$roll_ids		= $slot['role_name'];
			
			$user_cat		= $memberList[0]['user_category_id'];
			if($user_cat == 1){
				$studProfile 	= $this->master_model->getRecords("student_profile", array('user_id' => $memberList[0]['user_id']));
				$profilePic 	= $studProfile[0]['profile_picture'];
				
				//$pic_exist = base_url('assets/profile_picture/'.$encrptopenssl_team->decrypt($profilePic));
				$pic_exist = base_url('assets/profile_picture/'.$profilePic);
				 if ($profilePic!="") {
					$pro_pic = $pic_exist;
				} else {
					$pro_pic = base_url('assets/not_available.png');
				}
				
			} else {
				
				$orgProfile 	= $this->master_model->getRecords("profile_organization", array('user_id' => $memberList[0]['user_id']));
				$profilePic 	= $orgProfile[0]['org_logo'];
				
				$pic_exist = base_url('uploads/organization_profile/'.$encrptopenssl_team->decrypt($profilePic));
				 if ($profilePic!="") {
					$pro_pic = $pic_exist;
				} else {
					$pro_pic = base_url('assets/not_available.png');
				}
			}
			
			$expRole = explode(",",$roll_ids);
			
			$combineRole = array();
			foreach($expRole as $getrole){
				
				$roleDetails = $this->master_model->getRecords("byt_role_master", array('id' => $getrole));
				$rolename = ucfirst($encrptopenssl_team->decrypt($roleDetails[0]['name']));
				array_push($combineRole, $rolename);
			}
			$impRole = implode(", ", $combineRole);
		?>

			<div class="">
				<div><a href="javascript:void(0);"><img src="<?php echo $pro_pic; ?>" height="50px" width="50px" class="img-view <?php echo $user_cat; ?>" ></a></div>
				<div><?php echo $fullN; ?></div>
				<div><?php echo $impRole; ?></div>
			</div>
			
			
		<?php	
			
		}
	  ?>
	  <div class="col-md-12">
			<?php 
			$getTeamStatus = $this->master_model->getRecords("byt_teams", array('team_id' => $team_ID, 'challenge_owner_status' => 'Approved'));
			if(count($getTeamStatus) == 0){
			?>
			<div id="remove-div">
				<input type="submit" name="accept_btn" id="accept_btn" class="btn btn-success team-selector" data-id="Accept" data-tid="<?php echo $team_ID ?>" data-cid="<?php echo $c_id ?>" value="Accept" />
				<input type="submit" name="reject_btn" id="reject_btn" class="btn btn-danger team-selector" data-id="Reject" data-tid="<?php echo $team_ID ?>" data-cid="<?php echo $c_id ?>" value="Reject" />
			</div>
			<?php 
			}
			?>
	  </div>
	</div>

 </section>
 <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
$(document).ready( function () {
	$(".team-selector").on("click", function () {	
		var team_status = $(this).attr('data-id');
		var team_id = $(this).attr('data-tid');
		var ch_id = $(this).attr('data-cid');
		//alert(team_status+"=="+team_id);
		
		swal(
		{
			title:"Confirmation:",
			text: "Are you sure?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
			//html:'You are requesting access to the owner details. '+'<input type="hidden" name="c_id" id="c_id" value="<?php echo $challenge_data[0]['c_id']  ?>" /><input type="hidden" name="o_id" id="o_id" value="<?php echo $challenge_data[0]['u_id']  ?>" /><input type="hidden" class="token" id="csrf_test_name" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash(); ?>" />'
		}).then(function (result) 
		{
			if (result.value) 
			{	
				//alert(team_status+"=="+team_id);return false;
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();							
				 $.ajax({
					type:'POST',
					url: base_url+'myTeams/team_selection',
					data:'c_id='+ch_id+'&t_status='+team_status+'&t_id='+team_id+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);								
						$(".token").val(output.token);
						//console.log(output);	
						var storetype = output.success;
						var textAlert = output.content_text;
						if(storetype == "success"){
							var alertContent = "Success!";
						} else {
							var alertContent = "Error!";
						}
						$('#remove-div').remove();
						swal({
								title: alertContent,
								text: textAlert,
								type: storetype,
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						
					}
				});	// Ajax End
				
			} // IF SWAL SUCCESS
		
		}); // TRUE
	}); // Click Event 
}); // Document Event
</script> 