<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<?php  if(count($invitation_users) > 0)
	{	
		$i=0;
		foreach($invitation_users as $res)
		{	
			$profile_img_icon = '<img src="'.base_url('assets/profile_picture/user_dummy.png').'" alt="experts" alt="" class="img-fluid">';
			$skillset_str = $skillTitle = '';
			if($res['user_category_id'] == 1)
			{
				if($res['profile_picture'] !='' ) 
				{ 
					$profile_img_icon = '<img src="'.base_url('assets/profile_picture/'.$res['profile_picture']).'" alt="" class="img-fluid">';
				}
				
				if($res['skill_sets'] != "")
				{
					$skill_set_ids = $encrptopenssl->decrypt($res['skill_sets']);
					
					if($skill_set_ids != "")
					{
						$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
						$SkillSetData = $this->master_model->getRecords("arai_skill_sets");
						if(count($SkillSetData) > 0)
						{
							foreach($SkillSetData as $SkillRes)
							{
								$skillSetName = $encrptopenssl->decrypt($SkillRes['name']);
								if(strtolower($skillSetName) != 'other')
								{
									$skillset_str .= $skillSetName.", ";
								}
							}
						}
					}
				}
				
				if($res['other_skill_sets'] != "") { $skillset_str .= $encrptopenssl->decrypt($res['other_skill_sets']); }
				
				$skillTitle = 'Skillset';
			}
			else
			{
				if($res['org_logo'] !='' ) 
				{ 
					$profile_img_icon = '<img src="'.base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($res['org_logo'])).'" alt="" class="img-fluid">';
				}
				
				if($res['org_sector'] != "")
				{
					$skill_set_ids = $res['org_sector'];
					
					if($skill_set_ids != "")
					{
						$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
						$SkillSetData = $this->master_model->getRecords("arai_organization_sector");
						if(count($SkillSetData) > 0)
						{
							foreach($SkillSetData as $SkillRes)
							{
								$skillSetName = $SkillRes['name'];
								if(strtolower($skillSetName) != 'other')
								{
									$skillset_str .= $skillSetName.", ";
								}
							}
						}
					}
				}
				
				if($res['org_sector_other'] != "") { $skillset_str .= $encrptopenssl->decrypt($res['org_sector_other']); }
				$skillTitle = 'Sector';
			} 
			
			$display_name = $encrptopenssl->decrypt($res['first_name']);  
			if($encrptopenssl->decrypt($res['middle_name']) != "") { $display_name .= " ".$encrptopenssl->decrypt($res['middle_name']); } 
			$display_name .= " ".$encrptopenssl->decrypt($res['last_name']); ?>
		
		<div class="col-md-6 <?php echo $i; ?>">
			<div class="hiringBox">
				<div class="row">
					<div class="col-md-4 d-flex align-items-center"><a class="userImgLink" href="javascript:void(0)" onclick="get_user_details('<?php echo base64_encode($res['user_id']); ?>')"><?php echo $profile_img_icon; ?></a></div>
					<div class="col-md-8">
						<ul class="member_inner_listing">
							<li class="text-capitalize"><strong><i class="fa fa-user-circle-o" aria-hidden="true"></i> Name: </strong><?php echo $display_name; ?></li>
							<li><strong><i class="fa fa-graduation-cap" aria-hidden="true"></i> Type: </strong><?php echo $encrptopenssl->decrypt($res['sub_catname']); ?></li>
							<li><strong><i class="fa fa-language" aria-hidden="true"></i> <?php echo $skillTitle; ?>: </strong><?php echo character_limiter(rtrim($skillset_str,", "), 20); ?></li>
						</ul>												
					</div>
					
					<div class="col-md-12 text-center pt-3" style="border-top: solid 1px #eee;">
						<a href="javascript:void(0)" class="btn btn-primary" onclick="coming_soon_popup()">Chat </a>
						
						<span id="InviteBtnOuter<?php echo $res['user_id']; ?>">
						<?php if($res['invitation_id'] == "") { ?>
							<a href="javascript:void(0)" class="btn btn-primary" onclick="open_invite_modal('<?php echo $res['user_id']; ?>','<?php echo base64_encode($team_id); ?>','<?php echo base64_encode($slot_id); ?>','<?php echo $display_name; ?>','<?php echo base64_encode($res['email']); ?>')">Invite </a>	
						<?php } 
						else 
						{ ?> 
							<button class="btn btn-primary" style="cursor:not-allowed">Invited </button>
						<?php } ?>						
						</span>
					</div>
					
				</div>
			</div>
		</div>
		<?php $i++; }
		
		if($new_start < $total_user_count){ ?>
		<div class="col-md-12 text-center" id="showMoreBtn">
			<span class="btn btn-general btn-white" onclick="getInvitationDataAjax('<?php echo $new_start; ?>','6')">Show more </span>
		</div>
		<?php } ?>
<?php
	}
	else
	{	?>
	<h4 class="text-center" style="text-decoration:none">No Record Found</h4>
<?php } ?>