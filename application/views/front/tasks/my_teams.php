
<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Teams</h1>        
	</div>
</div>
<style>#datatable-list_wrapper { max-width:99%; } </style>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<div class="formInfo">					
					<div class="row mt-12">
						<div class="col-md-12">							
							<div class="table-responsive">
								<table id="datatable-list" class="table table-bordered table-hover challenges-listing-page">
									<thead>
										<tr>
											<th scope="col">ID</th>
											<th scope="col">Challenge Title</th>
											<th scope="col">Team Name</th>
											<th scope="col">Team Size</th>
											<th scope="col">Team Status</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>							
									</tbody>
								</table>
							</div>
						</div>
					</div>    
				</div>    
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
  $(document).ready( function () 
	{	 
		var base_path = '<?php echo base_url() ?>';
		var table = $('#datatable-list').DataTable(
		{			
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": base_path+"myteams/get_my_team_list_ajax",
				"type":"POST",
				/* "data":function(data) {	
					data.techonogy_id 			= $("#techonogy_id").val();
					data.change_visibility 		= $("#change_visibility").val();
					data.c_status 				= $("#c_status").val();
					data.audience_pref 			= $("#audience_pref").val();
				}, */
				"error":function(x, status, error) {
					
				},
				"statusCode": {
					401:function(responseObject, textStatus, jqXHR) {			
					},
				},
			}
			
		});
	});
	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>		

<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>