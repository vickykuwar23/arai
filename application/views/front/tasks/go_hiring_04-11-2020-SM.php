<style>
	.form-control-placeholder{left:0px}
	.chatButton{text-decoration:none; padding:10px 15px; color:#fff; background:#333 !important; text-align:center; border-radius:.25rem; display: block;}
	.chatButton:hover{ text-decoration: none; color: #FFF; background: #e23751 !important; }
	.hiring-page img{width:auto;height:auto;margin:0 auto;box-shadow:0 0 5px 0 rgba(46,61,73,.15);max-height:100px}
	
	.hiring-page ul{ list-style: none; padding: 0; margin: 0 !implements;}
	.hiring-page ul li{ padding:5px 15px; margin: 5px; background: #f8f8f8;}
	.hiring-page ul li::before{ display:none;}
	.hiringBox{border:solid 1px #eee; display: flex; width:100%;padding:15px}
	.hiringBox a{text-decoration:none; padding:10px 15px; color:#fff; background:#e23751 !important; text-align:center; border-radius:.25rem; margin:0 5px}
	.hiringBox a:hover{ text-decoration: none; color: #FFF; background: #333 !important; }
	ul.hiring_box_content_listing li:before { display:none; }
	#GoHiringModal .go_hiring_content { padding:0 15px; }
	.byt_terms_outer{margin-top:-5px}
</style>
<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>

<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">View Applications / Go Hiring Page</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Go Hiring Page</li>
			</ol>
		</nav>
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row hiring-page">
						<div class="col-md-12 text-center mb-2"><strong>Skill Sets You are looking for - <?php echo rtrim($TeamSkillStr,", "); ?></strong></div>
						<?php if(count($team_slot_application_data) > 0)
							{	
								foreach($team_slot_application_data as $res)
								{	
									$profile_img_icon = '<img src="'.base_url('assets/profile_picture/user_dummy.png').'" alt="experts" alt="" class="img-fluid">';
									$skillset_str = '';
									if($res['user_category_id'] == 1)
									{
										$this->db->select('profile_picture, skill_sets, other_skill_sets');
										$profileData = $this->master_model->getRecords("arai_student_profile",array('user_id'=>$res['apply_user_id']));
										
										if (isset($profileData[0]['profile_picture']) &&  $profileData[0]['profile_picture'] !='' ) 
										{ 
											$profile_img_icon = '<img src="'.base_url('assets/profile_picture/'.$profileData[0]['profile_picture']).'" alt="" class="img-fluid">';
										}
										
										if($profileData[0]['skill_sets'] != "")
										{
											$skill_set_ids = $encrptopenssl->decrypt($profileData[0]['skill_sets']);
											
											if($skill_set_ids != "")
											{
												$this->db->where("id IN (".$skill_set_ids.")");
												$SkillSetData = $this->master_model->getRecords("arai_skill_sets");
												if(count($SkillSetData) > 0)
												{
													foreach($SkillSetData as $SkillRes)
													{
														$skillSetName = $encrptopenssl->decrypt($SkillRes['name']);
														if(strtolower($skillSetName) != 'other')
														{
															$skillset_str .= $skillSetName.", ";
														}
													}
												}
											}
										}
										
										if($profileData[0]['other_skill_sets'] != "") { $skillset_str .= $encrptopenssl->decrypt($profileData[0]['other_skill_sets']); }
									}
									else
									{
										$this->db->select('org_logo');
										$profileImgData = $this->master_model->getRecords("arai_profile_organization",array('user_id'=>$res['apply_user_id']));
										
										if (isset($profileImgData[0]['org_logo']) &&  $profileImgData[0]['org_logo'] !='' ) 
										{ 
											$profile_img_icon = '<img src="'.base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profileImgData[0]['org_logo'])).'" alt="" class="img-fluid">';
										}
									} 
									
									$display_name = $encrptopenssl->decrypt($res['first_name']);  
									if($encrptopenssl->decrypt($res['middle_name']) != "") { $display_name .= " ".$encrptopenssl->decrypt($res['middle_name']); } 
								$display_name .= " ".$encrptopenssl->decrypt($res['last_name']); ?>
								
								<div class="col-md-6">
									<div class="hiringBox">
										<div class="row">
											<div class="col-md-4 d-flex align-items-center"><?php echo $profile_img_icon; ?></div>
											<div class="col-md-8">
												<ul>
													<li><strong><i class="fa fa-user-circle-o" aria-hidden="true"></i> Name: </strong><?php echo $display_name; ?></li>
													<li><strong><i class="fa fa-graduation-cap" aria-hidden="true"></i> Type: </strong><?php echo $encrptopenssl->decrypt($res['sub_catname']); ?></li>
													<li><strong><i class="fa fa-language" aria-hidden="true"></i> Skillset: </strong><?php echo rtrim($skillset_str,", "); ?></li>
													<li>
														<strong><i class="fa fa-flag" aria-hidden="true"></i> Status: </strong>
														<?php /* if($res['status'] =='Pending') { echo "On-Hold"; }
															else if($res['status'] =='Approved') { echo "Accepted"; }
														else { echo $res['status']; } */
														
														echo $res['status']; ?>
													</li>
												</ul>												
											</div>
											
											<div class="col-md-12 text-center pt-3" style="border-top: solid 1px #eee;">
												<?php 
													$showBtnFlag = 0;
													if($accetpted_application_id == "") { $showBtnFlag = 1; }
													else if($accetpted_application_id != "" && $accetpted_application_id == $res['app_id']){ $showBtnFlag = 1; }
													
													if($showBtnFlag == 1 && ($res['status'] == 'Approved' || $res['status'] == 'Pending'))
													{	?>													
														<a href="javascript:void(0)" onclick="coming_soon_popup()">Chat </a>
														<a href="javascript:void(0)" onclick="open_go_hiring_modal('<?php echo base64_encode($res['app_id']); ?>','<?php echo $display_name; ?>','<?php echo $res['status']; ?>')">Action </a>
												<?php }
												else
												{	?>
													<a href="javascript:void(0)" style="visibility:hidden">Chat </a>
													<a href="javascript:void(0)" style="visibility:hidden">Action </a>
									<?php	}	?>
											</div>
											
										</div>
									</div>
								</div>
								<?php } 
							}	?>
					</div>					
				</div>			
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="GoHiringModal" tabindex="-1" role="dialog" aria-labelledby="GoHiringModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_content_outer">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="GoHiringModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			
			<div class="go_hiring_content">
				<div class="go_hiring_inner">
					<p class="byt_terms_outer">What would you like to do with this application?</p>
				</div>
				
				<form method="post" action="<?php echo site_url('myteams/goHiring/'.base64_encode($team_id).'/'.base64_encode($slot_id)); ?>" id="GoHiringForm" enctype="multipart/form-data">
					<input type="hidden" name="app_id" id="app_id" value="">
					<div style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<div class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" id="go_hiring_radio1" name="go_hiring_radio" required value="Approved">
							<label class="custom-control-label" for="go_hiring_radio1">Accept</label>
						</div>
						<div class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" id="go_hiring_radio2" name="go_hiring_radio" required value="Rejected">
							<label class="custom-control-label" for="go_hiring_radio2">Reject</label>
						</div>
						<div class="custom-control custom-radio">
							<input type="radio" class="custom-control-input" id="go_hiring_radio3" name="go_hiring_radio" required value="Pending">
							<label class="custom-control-label" for="go_hiring_radio3">Pending</label>
						</div>
						<div id="custom_radio_err"></div>
					</div>
					<div class="modal-footer p-2">
						<input type="submit" class="btn btn-primary" value="Save">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>
<script type="text/javascript">
	function coming_soon_popup() 
	{ 
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
	}
	
	function open_go_hiring_modal(app_id, disp_name, status)
	{
		$("#app_id").val(app_id);
		$("#GoHiringModalLabel").html("Name : "+disp_name);
		
		if(status == 'Approved') { $("#go_hiring_radio1").attr('checked',true); }
		else if(status == 'Rejected') { $("#go_hiring_radio2").attr('checked',true); }
		else { $("#go_hiring_radio3").attr('checked',true); }
		
		$("#GoHiringModal").modal('show');
	}
	
	//******* JQUERY VALIDATION *********
	$("#GoHiringForm").validate( 
	{
		rules: { go_hiring_radio: { required: true } },
		messages: { go_hiring_radio: { required: "Please select at least one option" }, },
		errorPlacement: function(error, element) // For replace error 
		{
			if (element.attr("name") == "go_hiring_radio") { error.insertAfter("#custom_radio_err"); }
			else { error.insertAfter(element); }
		},
		submitHandler: function(form) 
		{ 
			swal(
			{
				title:"Confirm",
				text: "Are you sure you want to change the application status of this user?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!',
				html:''
			}).then(function (result) 
			{
				if (result.value) 
				{
					$("#GoHiringModal").modal('hide');
					form.submit();		
				}
				
			});
		}
	});
	</script>	