<style>
	ul#myTab li:before { display:none; }
	
	.form-control-placeholder{left:0px}
	.chatButton{text-decoration:none; padding:10px 15px; color:#fff; background:#333 !important; text-align:center; border-radius:.25rem; display: block;}
	.chatButton:hover{ text-decoration: none; color: #FFF; background: #e23751 !important; }
	.hiring-page img{width:auto;height:auto;margin:0 auto;box-shadow:0 0 5px 0 rgba(46,61,73,.15);max-height:100px}
	
	.hiring-page ul.member_inner_listing { list-style: none; padding: 0; margin: 0 !implements;}
	.hiring-page ul.member_inner_listing li{ padding:5px 15px; margin: 5px; background: #f8f8f8;}
	.hiring-page ul li::before{ display:none;}
	.hiringBox { border: solid 1px #eee; display: flex; width: 100%; padding: 15px; background: rgba(0,0,0,0.015); margin-bottom: 20px; min-height:240px;}
	/* .hiringBox a{text-decoration:none; padding:10px 15px; color:#fff; background:#e23751 !important; text-align:center; border-radius:.25rem; margin:0 5px}
	.hiringBox a:hover{ text-decoration: none; color: #FFF; background: #333 !important; } */
	ul.hiring_box_content_listing li:before { display:none; }
	#GoHiringModal .go_hiring_content { padding:0 15px; }
	.byt_terms_outer{margin-top:-5px}
	
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	.technical-queryText{display:block;margin-bottom:10px;font-size:14px; padding-left:10px;}
	#more_details {display: none;}
	a.skill-more{background:none; font-size: 14px; float: right;}
	a.skill-less{background:none;font-size: 14px; float: right;}
	#more_results {display: none;}
	a.domain-more{background:none; font-size: 14px; float: right;}
	a.domain-less{background:none;font-size: 14px; float: right;}
	#more_emp {display: none;}
	a.employement-more{background:none; font-size: 14px; float: right;}
	a.employement-less{background:none;font-size: 14px; float: right;}
	#more_exp {display: none;}
	a.exp-more{background:none; font-size: 14px; float: right;}
	a.exp-less{background:none;font-size: 14px; float: right;}
	#flotingButton { position: fixed; top:45%; left:8px; z-index:999; transition: all 0.5s ease; } 
	.expertsList a { color: #fff; background: #e23751; padding: 10px 15px; border: solid 1px #e23751; text-decoration: none; display: block; text-align: center; border-radius: 15px; font-size: 14px; }
	
	.filterBox { width:100%; }
	.scroll ul li { padding-left:0; }
	.skillSetMore, .DomainMore, .EmploymentMore, .YearExperienceMore { display:none; }
	
	#InvitationModal .modal_custom_content, #UserDetailModal .modal_custom_content { padding:0 15px; }
	
	a.userImgLink { background: none !important; padding: 0; margin: 0 auto; }
	a.userImgLink img { opacity:0.9; }
	a.userImgLink img:hover { opacity:1; }
</style>
<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>

<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Go Hunting</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Go Hunting</li>
			</ol>
		</nav>
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">
						<div class="col-md-12 text-center mb-2"><strong>Skill Sets You are looking for - <?php echo rtrim($TeamSkillStr,", "); ?></strong></div>
						
						<div class="product_details MyTeamsListingTab" style="width:100%">
							<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('myteams/goHiring/'.base64_encode($team_id).'/'.base64_encode($slot_id)); ?>">Received Applications</a></li>
								<li class="nav-item"><a class="nav-link active" href="javascript:void(0)">Go Hunting</a></li>
							</ul>
							
							<div class="tab-content hiring-page" id="myTabContent">
								<div class="tab-pane active" id="TeamTab2">
									<div class="container">
										<div class="row justify-content-center d-none"> 
											<div class="filterBox">												
												<section class="search-sec">
													<form method="post" id="filterData" name="filterData">		
														<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
														<div class="row d-flex justify-content-center search_Box">																		
															<div class="col-md-6">																	
																<input type="text" class="form-control search-slt" name="search_txt" id="search_txt"  value="" placeholder="Enter Search Keyword" />	
																<button type="button" class="btn btn-primary searchButton" ><i class="fa fa-search" aria-hidden="true"></i></button>
															</div>
															<div class="clearAll"><a class="btn btn-primary clearAll" id="reset-val">Clear All</a></div>
															<div class="filter">
																<button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
															</div>																		
															<div class="col-md-12"></div>																		
														</div>
														
														<div id="mySidenav" class="sidenav">
															<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
															<div class="scroll">					
																<ul>
																	<li><strong> Select Skillset </strong></li>
																	<?php $i=0; 
																		foreach($skill_set as $skills)
																		{ ?>
																		<li <?php if($i >= 5) { echo 'class="skillSetMore"'; } ?>>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills['id'] ?>" id="skillset" name="skillset[]" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $skills['name'] ?>
																			</label>
																		</li>
																		<?php	$i++;
																		}
																		
																		if(count($skill_set) > 5)
																		{	?>
																		<a href="javascript:void(0);" class="skill-more" id="skill-readmore" onclick="skill_show_hide('show')">Show More >> </a>
																		<a href="javascript:void(0);" class="skill-less" style="display:none;" id="skill-readless" onclick="skill_show_hide('hide')">Show Less >></a>
																	<?php }  ?>
																</ul> 
																
																<ul>
																	<li> <strong> Select Domain Expertise </strong> </li>
																	<?php $d=0;  
																		foreach($domain_list as $domain_val)
																		{ ?>
																		<li <?php if($d >= 5) { echo 'class="DomainMore"'; } ?>>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $domain_val['id'] ?>" id="domain_exp" name="domain_exp[]" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $domain_val['domain_name'] ?>
																			</label>
																		</li>
																		<?php $d++; 
																		} ?>
																		<?php if(count($domain_list) > 5) { ?>
																			<a href="javascript:void(0);" class="domain-more" id="domain-readmore" onclick="domain_show_hide('show')">Show More >> </a>
																			<a href="javascript:void(0);" class="domain-less" style="display:none;"  id="domain-readless" onclick="domain_show_hide('hide')">Show Less >></a>
																		<?php } ?>
																</ul> 
																
																<ul>
																	<li> <strong> Employment Status </strong> </li>
																	<?php $e=0; 
																		foreach($employement_status as $emp_status)
																		{ ?>	
																		<li <?php if($e >= 5) { echo 'class="EmploymentMore"'; } ?>>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $emp_status['name'] ?>" id="emp_status" name="emp_status" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $emp_status['name'] ?>
																			</label>
																		</li>
																		<?php $e++; 
																		} ?>	
																		
																		<?php if(count($employement_status) > 5) { ?>
																			<a href="javascript:void(0);" class="employement-more" id="skill-readmore" onclick="employement_show_hide('show')">Show More >> </a>
																			<a href="javascript:void(0);" class="employement-less" style="display:none;" id="employement-readless" onclick="employement_show_hide('hide')">Show Less >></a>
																		<?php } ?>
																</ul> 
																
																<ul>
																	<li> <strong> Year Of Experience </strong> </li>
																	<?php $y=0; 
																		foreach($year_of_exp as $exp_no)
																		{ ?>	
																		<li <?php if($y >= 5) { echo 'class="YearExperienceMore"'; } ?>>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $exp_no['year_exp'] ?>" id="year_of_exp" name="year_of_exp" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $exp_no['year_exp']."+" ?>
																			</label>
																		</li>
																		<?php $y++;  
																		}
																		
																		if(count($year_of_exp) > 5)	{ ?>
																		<a href="javascript:void(0);" class="exp-more" id="exp-readmore" onclick="year_experience_show_hide('show')">Show More >> </a>
																		<a href="javascript:void(0);" class="exp-less" style="display:none;"  id="exp-readless" onclick="year_experience_show_hide('hide')">Show Less >></a>
																	<?php } ?>
																</ul> 
																
																<ul>
																	<li> <strong> No. Of Papers Publications </strong> </li>
																	<?php foreach($no_of_publication as $publication_no)
																		{ ?>	
																		<li>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $publication_no['paper_no'] ?>" id="no_of_pub" name="no_of_pub" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $publication_no['paper_no']."+" ?>
																			</label>
																		</li>
																	<?php } ?>
																</ul> 
																
																<ul>
																	<li> <strong> No. Of Patents</strong> </li>
																	<?php foreach($no_of_patents as $patent_no){ ?>	
																		<li>
																			<input class="form-check-input search-value" type="checkbox" value="<?php echo $patent_no['patent_no'] ?>" id="no_of_patents" name="no_of_patents" >
																			<label class="form-check-label" for="defaultCheck1">
																				<?php echo $patent_no['patent_no']."+" ?>
																			</label>
																		</li>
																	<?php } ?>
																</ul> 
															</div>
														</div>
													</form>
												</section>
											</div>
										</div>
										
										<div class="row" id="InvitationDataOuter"></div>
									</div>
								</div>    
							</div>
						</div>
					</div>					
				</div>			
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="InvitationModal" tabindex="-1" role="dialog" aria-labelledby="InvitationModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_content_outer">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="InvitationModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			
			<div class="modal_custom_content">
				<form method="post" id="InviteForm" enctype="multipart/form-data">
					<input type="hidden" name="user_id" id="user_id" value="">
					<input type="hidden" name="team_id" id="team_id" value="">
					<input type="hidden" name="slot_id" id="slot_id" value="">
					<input type="hidden" name="user_name" id="user_name" value="">
					<input type="hidden" name="user_email" id="user_email" value="">
					
					<div style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<div class="form-group">
							<textarea class="form-control" name="invitation_msg" id="invitation_msg" required></textarea>
							<label class="form-control-placeholder floatinglabel" for="invitation_msg" style="display:block">Invitation Message <em>*</em></label>
						</div>
					</div>
					<div class="modal-footer p-2">
						<input type="submit" class="btn btn-primary" value="Send Invitation">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
		<div class="modal-content" id="UserDetailModalContentOuter">			
		</div>
	</div>
</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>
<script type="text/javascript">
	function coming_soon_popup() 
	{ 
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
	}
</script>	

<script type="text/javascript">  
	function skill_show_hide(type)
	{
		if(type == 'show') { $('.skillSetMore').css('display', 'block'); $('.skill-less').css('display', 'block'); $('.skill-more').css('display', 'none'); }
		else { $('.skillSetMore').css('display', 'none'); $('.skill-less').css('display', 'none'); $('.skill-more').css('display', 'block'); }
	}
	
	function domain_show_hide(type)
	{
		if(type == 'show') { $('.DomainMore').css('display', 'block'); $('.domain-less').css('display', 'block'); $('.domain-more').css('display', 'none'); }
		else { $('.DomainMore').css('display', 'none'); $('.domain-less').css('display', 'none'); $('.domain-more').css('display', 'block'); }
	}
	
	function employement_show_hide(type)
	{
		if(type == 'show') { $('.EmploymentMore').css('display', 'block'); $('.employement-less').css('display', 'block'); $('.employement-more').css('display', 'none'); }
		else { $('.EmploymentMore').css('display', 'none'); $('.employement-less').css('display', 'none'); $('.employement-more').css('display', 'block'); }
	}
	
	function year_experience_show_hide(type)
	{
		if(type == 'show') { $('.YearExperienceMore').css('display', 'block'); $('.exp-less').css('display', 'block'); $('.exp-more').css('display', 'none'); }
		else { $('.YearExperienceMore').css('display', 'none'); $('.exp-less').css('display', 'none'); $('.exp-more').css('display', 'block'); }
	}
	
	$(document).ready(function () 
	{ 	
		$(document).on('click','.searchButton',function()
		{	
			getInvitationDataAjax(0,6);
			/* $('#preloader-loader').css('display', 'block');	
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html); 
					$('#preloader-loader').css('display', 'none');	
					
				},
				error: function (jqXHR, exception) { 
				  $('#preloader-loader').css('display', 'none');
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					console.log(msg);
					//$('#post').html(msg);
				},
			}); */
		});
		
		
		// Filter Apply		
		/* $(document).on('click','.searchButton',function()
		{
			
			$('#preloader-loader').css('display', 'block');	
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html); 
					$('#preloader-loader').css('display', 'none');	
					
				},
				error: function (jqXHR, exception) { 
				  $('#preloader-loader').css('display', 'none');
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		
		$(document).on('click','.search-value',function()
		{
			$('#preloader-loader').css('display', 'block');
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html);
					$('#preloader-loader').css('display', 'none');	
					
				},
				error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		
		$(document).on('keydown','.search-slt',function()
		{
			$('#preloader-loader').css('display', 'block');
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html);
					$('#preloader-loader').css('display', 'none');	
					
				},
				error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		$(document).on('click','a#reset-val',function()
		{
			$('#preloader-loader').css('display', 'block');
			//alert();
			$("#filterData").trigger("reset");
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html); 
					$('#preloader-loader').css('display', 'none');	
					
				},
				error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		 */
		$('#myModal').modal('show');
	});
		
	function openNav() 
	{	
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
		}
		else 
		{
			document.getElementById("mySidenav").style.width = "25%";
		} 
	}
	
	function closeNav() { document.getElementById("mySidenav").style.width = "0"; }	
	
	//==============BACK TO TOP====================================
	(function($) 
	{
    $(window).scroll(function() 
		{
			if ($(this).scrollTop() < 61) 
			{
				// hide nav
				$("nav").removeClass("vesco-top-nav");
				$("#flotingButton").fadeOut();
				
			} 
			else 
			{
				// show nav
				$("nav").addClass("vesco-top-nav");
				$("#flotingButton").fadeIn();
			}
		});
	})(jQuery); // End of use strict
</script>

<script>
	//GET USER DATA ON PAGE LOAD AND SHOW MORE
	function getInvitationDataAjax(start, limit)
	{
		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		var keyword = encodeURIComponent($('#search_txt').val());
		var skillset = encodeURIComponent($('#skillset').val());
		var domain_exp = encodeURIComponent($('#domain_exp').val());
		var emp_status = encodeURIComponent($('#emp_status').val());
		var year_of_exp = encodeURIComponent($('#year_of_exp').val());
		var no_of_pub = encodeURIComponent($('#no_of_pub').val());
		var no_of_patents = encodeURIComponent($('#no_of_patents').val());
		
		parameters= { 'start':start, 'limit':limit, 'keyword':keyword, 'skillset':skillset, 'domain_exp':domain_exp, 'emp_status':emp_status, 'year_of_exp':year_of_exp, 'no_of_pub':no_of_pub, 'no_of_patents':no_of_patents, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('Myteams/getInvitationDataAjax/'.base64_encode($team_id).'/'.base64_encode($slot_id)); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)
					$("#InvitationDataOuter").append(data.response);
				}else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getInvitationDataAjax(0,6);
	
	//OPEN INVITATION FORM MODAL
	function open_invite_modal(user_id, team_id, slot_id, display_name, email)	
	{
		$("#invitation_msg").val("");
		$("#user_id").val(user_id);
		$("#team_id").val(team_id);
		$("#slot_id").val(slot_id);
		$("#user_name").val(display_name);
		$("#user_email").val(email);
		$("#InvitationModalLabel").html(display_name);
		$("#InvitationModal").modal('show');
	}
	
	//******* JQUERY VALIDATION *********
	$("#InviteForm").validate( 
	{
		rules: { invitation_msg: { required: true, nowhitespace:true } },
		messages: { invitation_msg: { required: "Please enter the Invitation Message", nowhitespace: "Please enter the Invitation Message" } },
		errorElement: 'span',
		submitHandler: function(form) 
		{ 
			swal(
			{
				title:"Confirm",
				text: "Are you sure you want to invite this member?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!',
				html:''
			}).then(function (result) 
			{
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					parameters= { 'invitation_msg':$("#invitation_msg").val(), 'user_id':$("#user_id").val(), 'team_id':$("#team_id").val(), 'slot_id':$("#slot_id").val(), 'user_name':$("#user_name").val(), 'user_email':$("#user_email").val(), 'cs_t':cs_t }
					$("#preloader-loader").show();
					$.ajax(
					{
						type: "POST",
						url: "<?php echo site_url('myteams/sendInvitationAjax'); ?>",
						data: parameters,
						cache: false,
						dataType: 'JSON',
						success:function(data)
						{
							if(data.flag == "success")
							{
								userId = $("#user_id").val();
								$("#InvitationModal").modal('hide');
								$(".token").val(data.csrf_new_token)
								$("#InviteBtnOuter"+userId).html('<button class="btn btn-primary" style="cursor:not-allowed">Invited </button>');
								$("#preloader-loader").hide();
								sweet_alert_success("Your invitation is successfully send to this user");
							}
							else { sweet_alert_error("Error Occurred. Please try again."); }
						}
					});	
				}
				
			});
		}
	});	
	
	//ON CLICK ON USER IMAGE, OPEN USER DETAIL POP UP
	function get_user_details(user_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'user_id':user_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('myteams/CommonUserDetailsAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)					
					$("#UserDetailModalContentOuter").html(data.response)
					$("#UserDetailModal").modal('show')
					$("#preloader-loader").hide();
				}
				else 
				{ 
					$("#preloader-loader").hide();
					sweet_alert_error("Error Occurred. Please try again."); 
				}
			}
		});	
	}
</script>

