<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Coming Soon</h1>		
	</div>
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">				
				<div class="formInfo">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h3>This facility is coming soon - Stay Tuned</h3>
							<img src="https://technovuus.araiindia.com/assets/front/img/coming-soon.png" style="width:50%;" alt="Coming Soon...">
						</div>
					</div>
					
					<div class="row mt-12">
						<div class="col-md-12">
							
						</div>
					</div>    
				</div>    
			</div>
		</div>
	</div>
</section>
