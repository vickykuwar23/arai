<?php if(count($fqa_forum_data) > 0)
	{	?> 
		<div class="col-md-12">
			<div class="formInfo65 mb-4">
				<div  class="owl-carousel mb-2 owl-theme QuestionSlider">
					<?php foreach($fqa_forum_data as $res)
					{  $data['res'] = $res;	?>	
						<div class="">
							<?php $this->load->view('front/questions_answers_forum/inc_qa_listing_inner', $data); ?>
						</div>
			<?php } ?>
				</div>
			</div>
		</div>
		
		<script>
		$('.QuestionSlider').owlCarousel(
		{
			items: 1,
			autoplay: true,
			smartSpeed: 700,			
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				768: {
					items: 3
				},
				992: {
					items: 1
				}
			}		
		});
	</script>
<?php } ?>	