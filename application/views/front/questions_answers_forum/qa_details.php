<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>

<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<style>	
	.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
	/* .WebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
	.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
	.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
	.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
	.product_details{padding: 20px 0 0 0;}
	/** Blog**/
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
	.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
	.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
	.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; } 
	.tag ul li { float: none; padding: 0; margin: 5px 5px 5px 0; display: inline-block; }
	.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.tag ul li a:hover{ background: #000; color: #32f0ff; }
	.shareList { margin-top:15px; }
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.shareList ul li a:hover{ background: #000; color: #32f0ff; }
	
	.userPhoto-info h3{ font-size: 19px !important; }
	.userPhoto-info img{ border-radius: 50%;}
	.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
	.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
	.descriptionBox45 p{color: #FFF;}
	.descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
	.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
	.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
	.bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
	.bs-example45 h3{color:#fff;font-size:19px;font-weight:800}
	.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:99;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	.formInfo65 p a{ text-decoration: none; font-weight: 800; color: #000;}
	.formInfo65 p a:hover{ text-decoration: none; font-weight: 800; color: #32f0ff;}
	/** Color Change**/
	.nav-tabs{border-bottom:1px solid #32f0ff}
	.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#32f0ff;background-color:#000 ;border-color:#000}
	.product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {border-color: #32f0ff #32f0ff #32f0ff;}
	.title-bar .heading-border{background-color:#32f0ff}
	.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
	.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
	.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
	/**Questions And Answers Forum**/
	.userPhoto-info h3 span {font-size: 14px !important; color: #a3a3a3; text-transform: initial; }
	#AnswersForum h2{font-size:1.75rem}
	/* .bgNewBox{ background: #f8f8f8; padding: 15px 15px 0 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;} */
	.listViewinfo h3 {font-size: 24px !important;}
	.listViewinfo h3 span a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
	.listViewinfo h3 span a:hover {background: #000; color: #32f0ff; text-align: center; }
	.listViewinfo img {border-radius: 50%;}
	.imgBoxView img {border-radius: 0%;}
	.listViewinfo ul{ list-style: none; padding: 0; margin: 0; font-size: 15px;}
	.listViewinfo ul li{ list-style: none; padding: 0; margin: 0; color: #333; font-weight: 800;}
	.listViewinfo ul li span{font-weight: normal; color: #000;}
	.likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
	.likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
	.likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
	.likeShare ul li:last-child{ float: right; margin: 0;}
	.likeShare ul li a, .likeShare ul li span {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold; cursor:pointer; }
	.likeShare ul li a:hover, .likeShare ul li span:hover {background: #000; color: #32f0ff; text-align: center; text-decoration:none; }
	.dropdown-menu{ padding: 0px !important;}
	.dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
	.dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }
	.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
	.imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}
	
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	
	.customBtnBlog { background: #FFF; color: #c80032;  border:solid #c80032 1px !important; text-decoration: none; padding: 5px 15px; border-radius: .25rem; border:none; }
	.customBtnBlog:hover { background: #c80032; color: #FFF; text-decoration: none; }
	
	.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
	.tag ul li { display: inline-block; padding: 0; margin: 5px 0px 5px 2px; }
	.tag ul li span { background: #FFF; color: #c80032; border:solid #c80032 1px; text-decoration: none; border-radius: 5px; padding: 5px; }
	/* .tag ul li span:hover{ background: #c80032; color: #FFF; } */
	
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; text-align:right; }
	.shareList ul li{ padding: 0; margin:0; display:inline-block; float:none; }
	.shareList ul li.report_btn_listing { margin:0; }
	.shareList ul li span{  background: #FFF; color: #c80032; border:solid #c80032 1px; text-decoration: none; border-radius: 5px; padding: 5px; cursor:pointer; }
	.shareList ul li span:hover{ background: #c80032; color: #FFF; }
	.shareList ul li span.invert_color { color: #c80032; background: #FFF; border:solid #c80032 1px; }
	.shareList ul li span.invert_color:hover{ color: #FFF; background: #c80032; }
	
	.comment_block_common { border-top: 1px solid #ccc; padding: 15px 0 15px 0; }	
	.comment_block_common .comment_img { width: 40px; height: 40px; overflow: hidden; border-radius: 50%; float: left; }
	.comment_block_common .comment_img img { max-width: 100%; max-height: 100%; }
	.comment_block_common .comment_inner { float: left; width: calc(100% - 50px); margin-left: 10px; }
	.comment_block_common .comment_inner p.comment_name { font-weight: 600; margin: 0 0 0 0; }
	.comment_block_common .comment_inner p.comment_name a:hover { color:#000; }
	.comment_block_common .comment_inner p.comment_time { margin: 0; font-size: 12px; font-weight: 500; color: #787878; }
	.comment_block_common .comment_inner p.comment_content { margin: 5px 0 5px 0; }
	.comment_block_common .comment_inner p.comment_like_btn { cursor: pointer; margin: 0; display: inline-block; }
	.comment_block_common_reply { margin: 10px 0 0 50px; background: #f3f3f3; padding: 8px 10px 10px 10px; }
	
	.custom_question_title { font-size: 18px; font-weight: 600; color: #41464b; white-space: break-spaces; display: block; line-height: 24px; margin:0; min-height: 100px; } 
	
	.ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
	.ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}
	
	.postDate span { font-size:13px; }
	.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  right:0px; top:-40px;
}

.dropdown-content a {
  color: black;
  text-decoration: none;
  display: block; color:#FFF;
   background-color: #FFF; color:#c80032;

}

.dropdown a:hover {background-color: #c80032; color:#FFF;}

.show_dropdown {display:block;}
.btn_report {background: #FFF !important; color: #c80032 !important;}
.btn_report:hover {background: #c80032 !important; color: #FFF !important;}

.postDate {margin-bottom: 15px;}
.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Question & Answer Forum</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url('questions_answers_forum') ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Question & Answer Forum Details </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="story" data-aos="fade-up">	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade" id="Blogs">
			<div class="container">	
				<div class="row mb-4">
					<div class="col-md-12">
						<div class="formInfo65 listViewinfo">
							
							<?php 
							$encrptopenssl =  New Opensslencryptdecrypt();
							$disp_name = $encrptopenssl->decrypt($form_data[0]['title'])." ".$encrptopenssl->decrypt($form_data[0]['first_name']);
							if($form_data[0]['middle_name'] != '') { $disp_name .= " ".$encrptopenssl->decrypt($form_data[0]['middle_name'])." "; }
							$disp_name .= " ".$encrptopenssl->decrypt($form_data[0]['last_name']); ?>
									
							<div class="row userPhoto-info">

								<div class="col-md-2">  
									<?php $iconImg = base_url('assets/no-img.png');
										if($form_data[0]['user_category_id'] == 2)
										{
											if($form_data[0]['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encrypt_obj->decrypt($form_data[0]['org_logo'])); }
										}
										else 
										{
											if($form_data[0]['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$form_data[0]['profile_picture']); }
										} ?>
										<img src="<?php echo $iconImg; ?>" alt="<?php echo $disp_name; ?>" title="<?php echo $disp_name; ?>" class="img-fluid" style='max-width: 80px; max-height: 80px; height: 80px; border: 1px solid #ccc; display: block; margin: 0 auto 5px auto; '>
										<a href="javascript:void(0)" style="color: #41464b; font-size: 12px; text-align: center; display: block; font-weight: 500; text-transform: capitalize;" onclick="get_user_details('<?php echo base64_encode($form_data[0]['user_id']); ?>')">
											<?php echo $disp_name; ?> 
										</a>
								</div>
								
								
								<div class="<?php if($form_data[0]["qa_image"] != "") { echo 'col-md-10'; } else { echo 'col-md-10'; } ?>">
									<h4 class='custom_question_title'><?php echo $form_data[0]['question'] ?></h4>

									<?php if($form_data[0]["qa_image"] != "") { ?>
									<div class="col-md-12 float-left">
										<a href="<?php echo base_url('uploads/qa_image/'.$form_data[0]['qa_image']); ?>" data-fancybox data-caption="<?php echo $this->Common_model_sm->convertValidStringForJs($form_data[0]['question']); ?>">
											<img src="<?php echo base_url('uploads/qa_image/'.$form_data[0]["qa_image"]); ?>" alt="<?php echo $this->Common_model_sm->convertValidStringForJs($form_data[0]['question']); ?>" title="<?php echo $this->Common_model_sm->convertValidStringForJs($form_data[0]['question']); ?>" class="img-fluid" style='max-width: 200px; width: 200px; border-radius: 0; display: block;'>
										</a>
									</div>
									<?php } ?>

									<div class="postDate float-right"><span>Posted on <?php echo date("jS F Y", strtotime($form_data[0]['created_on'])); ?>  at <?php echo date("h:i a", strtotime($form_data[0]['created_on'])); ?></span></div>
								</div>
							</div>
							
							<div class="row">									
								<div class="col-md-2"></div>
							
								<div class="col-md-10 pl-0">
									<div class="bgNewBox">												
										<div class="tag">
											<?php if($form_data[0]['DispTags'] != "") 
												{ 
												$tags_arr = explode("##",$form_data[0]['DispTags']); ?>
												<div class="tag">
													<ul>
														<li style='font-weight:600'>Tags:</li> 
														<?php foreach($tags_arr as $tag)
															{	
																if(strtolower(trim($tag)) != 'other')
																{	?>
																	<li><span><?php echo $tag; ?></span></li> 
														<?php }
															}
														
															if($form_data[0]['tags_other'] != "") 
															{
															$tag_other_arr=explode(',', $form_data[0]['tags_other']);
                                                   			 foreach ($tag_other_arr as $key => $tag_other) {	

															?>
																<li><span><?php echo $tag_other; ?></span></li> 
												<?php } }	?>
													</ul>
												</div>
											<?php } ?>
										</div>
									</div>
										
									<div class="shareList">
										<ul>
											<?php if(in_array($form_data[0]['q_id'], $QaActiondata['like_arr'])) 
											{ 
												$Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down'; $LikeClass = 'invert_color';
											}
											else 
											{ 
												$Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa fa-thumbs-o-up'; $LikeClass = '';
											} ?>
											
											<li id="like_unlike_btn_outer_<?php echo $form_data[0]['q_id']; ?>">
												<span><?php echo $form_data[0]['LikeCnt']; ?></span>
												<span class="<?php echo $LikeClass; ?>" onclick="like_unlike_question('<?php echo base64_encode($form_data[0]['q_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
											</li> 
											<li>
												<span onclick="share_question('<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($form_data[0]['q_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
											</li>
											
											<?php if(in_array($form_data[0]['q_id'], $QaActiondata['self_arr'])) { ?>
												<li><span onclick="delete_question_from_listing('<?php echo base64_encode($form_data[0]['q_id']); ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</span></li> 
											<?php } ?>
											
											<?php 
											if(!in_array($form_data[0]['q_id'], $QaActiondata['self_arr']))
											{
												if(in_array($form_data[0]['q_id'], $QaActiondata['reported_arr'])) { $Reported_label = "Question Already Reported"; $ReportedFlag = '0';  }
												else { $Reported_label = "Report Question"; $ReportedFlag = '1'; } ?>
												<?php /*
												<li id="report_qa_btn_outer_<?php echo $form_data[0]['q_id']; ?>" class='report_btn_listing'>
													<span title="<?php echo $Reported_label; ?>" onclick="report_qa('<?php echo base64_encode($form_data[0]['q_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['custum_question_id']; ?>', '<?php echo $this->Common_model_sm->convertValidStringForJs($form_data[0]['question']); ?>')"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span>
												</li>
												*/ ?>
												<li  class='report_btn_listing'>
													<div class="dropdown">
															  <span style="cursor: pointer;" onclick="myFunction('<?php echo $form_data[0]['q_id'];  ?>')" class="dropbtn pl-2 pr-2"><i   class="fa fa-ellipsis-v" ></i></span>
															  <div id="myDropdown_<?php echo $form_data[0]['q_id']; ?>" class="dropdown-content">
															    <a id="report_qa_btn_outer_<?php echo $form_data[0]['q_id']; ?>" class="btn btn-sm btn-danger btn_report" href="javascript:void(0)" title="<?php echo $Reported_label; ?>" onclick="report_qa('<?php echo base64_encode($form_data[0]['q_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['custum_question_id']; ?>', '<?php echo $this->Common_model_sm->convertValidStringForJs($form_data[0]['question']); ?>')">Report</a>
															    
															  </div>
															</div>
									
													</li>
											<?php } ?>  
										</ul>
									</div>                  
								</div>
							</div>
						
							<div class="row">
								<div class="col-md-12">
									<div class="comment_section_outer" style="margin-top:25px">
										<p style="border-bottom: 1px solid #ccc;padding-bottom: 5px;" id="total_comment_outer"></p>
										
										<div class="row">
											<div class="col-md-11">
												<textarea class="form-control" name="send_comment" id="send_comment" value="" placeholder="Your answer" onkeyup="check_comment_validation()"></textarea>

												<span id="comment_error" class='error'></span>
											</div>
											<div class="col-md-1">
												<button type="button" class="customBtnBlog" onclick="post_comment()">Post</button>
											</div>
										</div>
										
										<div class="form-group" id="sort_outer">
											Sort By : 
											<select class="" name="sort_by_comment" id="sort_by_comment" onchange="sort_comments(this.value)">
												<option value="Newest" selected>Newest</option>
												<option value="Oldest">Oldest</option>
												<option value="Best">Best</option>
											</select>
										</div>
										
										<div class="comment_block_outer">
											
										</div>
									</div>
								</div>
							</div>                     
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!------ SHARE QUESTION MODAL ------------->
<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="QuestionShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('QuestionShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<!------ REPORT QUESTION MODAL ------------->
<div class="modal fade" id="QuestionReportpopup" tabindex="-1" role="dialog" aria-labelledby="QuestionReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="QuestionReportpopupLabel">Question Report</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
					<div>
						<label><b>Question ID : </b><span id="popupQuestionId"></span></label>
					</div>
					<div>
						<label><b>Question : </b><span id="popupQuestionTitle"></span></label>
					</div>
					
					<div>
						<label for="popupQuestionReportComment"><b>Comment <em>*</em></b></label>
						<textarea class="form-control" id="popupQuestionReportComment" name="popupQuestionReportComment" required onkeyup="check_report_validation()"></textarea>
						<span id="popupQuestionReportComment_err" class='error'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submit_question_report('<?php echo base64_encode($form_data[0]['q_id']); ?>')">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<!---------- MODAL TO DISPLAY THE USER DATA IN POP UP WHEN CLICK ON NAME ------------------>
<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
		<div class="modal-content" id="UserDetailModalContentOuter">			
		</div>
	</div>
</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<?php
	if($form_data[0]['admin_status'] == 1 && $form_data[0]['is_block'] == 0) { $action_flag = 0; }
else { $action_flag = 1; } ?>

<?php if($comment_flag == 1)
	{	?>
	<script>
		$('html, body').animate(
		{
		scrollTop: $(".comment_section_outer").offset().top  - 180
		}, 2000);
	</script>
<?php }	?>

<script>
	function delete_question_from_listing(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected question?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("questions_answers_forum/delete_question"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{ 
						window.location.replace("<?php echo site_url('questions_answers_forum'); ?>");
					}
				});
			} 
		});		
	}	
	
	function report_qa(q_id, ReportedFlag, custum_question_id, question)
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not Report the question as its status is Pending or Blocked",
				type: 'warning'				
			});
			<?php }else { ?>
			if(ReportedFlag == "1")
			{
				$("#popupQuestionId").html(custum_question_id);
				$("#popupQuestionTitle").html(question);
				$("#popupQuestionReportComment_err").html('');
				$("#QuestionReportpopup").modal('show');
				//$("#popupQuestionReportComment").val('');
				$("#popupQuestionReportComment").focus();
			}
			else
			{
				swal(
				{
					title: 'Alert!',
					text: "You have already reported the question",
					type: 'alert',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
			}
		<?php } ?>
	}	
	
	function check_report_validation()
	{
		var popupQuestionReportComment = $("#popupQuestionReportComment").val();
		if(popupQuestionReportComment.trim() == "") { $("#popupQuestionReportComment_err").html('Please enter the report comment'); $("#popupQuestionReportComment").focus(); }
		else { $("#popupQuestionReportComment_err").html(''); }
	}
	
	function submit_question_report(q_id)
	{	
		check_report_validation();
		
		var popupQuestionReportComment = $("#popupQuestionReportComment").val();
		if(popupQuestionReportComment.trim() != "")		
		{
			$("#popupQuestionReportComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to report the selected question?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("questions_answers_forum/report_qa_ajax"); ?>',
						data:{ 'q_id':q_id, 'popupQuestionReportComment':popupQuestionReportComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#report_qa_btn_outer_"+data.q_id).replaceWith(data.response);						
							$("#QuestionReportpopup").modal('hide');
							
							swal(
							{
								title: 'Success!',
								text: "Question Reported successfully",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	
	
	function like_unlike_question(id, flag)
	{	
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not Like the question as its status is Pending or Blocked",
				type: 'warning'
			});
			<?php }else { ?>
			var cs_t = 	$('.token').val();
			$.ajax(
			{
				type:'POST',
				url: '<?php echo site_url("questions_answers_forum/like_unlike_question_ajax"); ?>',
				data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
				dataType:"JSON",
				success:function(data)
				{
					$(".token").val(data.token);
					
					$("#like_unlike_btn_outer_"+data.q_id).html(data.response);
				}
			});
		<?php } ?>
	}
	
	function share_question(url) 
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not share the question as its status is Pending or Blocked", 
				type: 'warning'
			});
			<?php }else { ?>
			$("#QuestionShareLink").html(url);
			$("#share-popup").modal('show');
		<?php } ?>
	}
	
	//ON CLICK ON USER IMAGE, OPEN USER DETAIL POP UP
	function get_user_details(user_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'user_id':user_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('myteams/CommonUserDetailsAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)					
					$("#UserDetailModalContentOuter").html(data.response)
					$("#UserDetailModal").modal('show')
					$("#preloader-loader").hide();
				}
				else 
				{ 
					$("#preloader-loader").hide();
					sweet_alert_error("Error Occurred. Please try again."); 
				}
			}
		});	
	}
	
	
	function like_dislike_comment(id, flag)
	{		
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not like the comment / answer as the question status is Pending or Blocked",
				type: 'warning'
			});
			<?php }else { ?>
			var cs_t = 	$('.token').val();
			$.ajax(
			{
				type:'POST',
				url: '<?php echo site_url("questions_answers_forum/like_unlike_comment_ajax"); ?>',
				data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
				dataType:"JSON",
				success:function(data)
				{
					$("#preloader").css("display", "none");
					$(".token").val(data.token);
					
					$("#like_unlike_comment_btn_outer_"+data.comment_id).html(data.response);
				}
			});
		<?php } ?>
	}
	
	function delete_comment(id, comment_type)
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not delete the comment / answer as the question status is Pending or Blocked",
				type: 'warning'
			});
			<?php }else { ?>
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to delete the selected comment / answer?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("questions_answers_forum/delete_comment_ajax"); ?>',
						data:{ 'id':id, 'comment_type':comment_type, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$("#preloader").css("display", "none");
							$(".token").val(data.token);
							
							if(comment_type == 'reply')
							{
								$("#comment_block_"+data.comment_id).remove();
								$("#reply_comment_btn_outer_"+data.parent_comment_id).html(data.new_comment_reply_cnt);
							}
							else
							{						
								get_comment_data('<?php echo $q_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', $("#sort_by_comment").val(), 0, 0, 1);
							}
							
							swal(
							{
								title: 'Success!',
								text: "Comment / answer successfully deleted.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		<?php } ?>
	}
	
	
	function check_comment_validation()
	{
		var comment_content = $("#send_comment").val();
		if(comment_content.trim() == "") { $("#comment_error").html('Please enter your comment / answer'); }
		else { $("#comment_error").html(''); }
	}
	
	// $('#send_comment').keypress(function (e)
	// { 
	// 	if (e.which == 13) 
	// 	{ 
	// 		post_comment();
	// 	} 
	// });
	
	function post_comment()
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not post the comment / answer as the question status is Pending or Blocked",
				type: 'warning'
			});
			<?php }else { ?>		
			check_comment_validation();
			
			var comment_content = $("#send_comment").val();
			if(comment_content.trim() != "") 
			{ 
				var cs_t = 	$('.token').val();
				var user_id = "<?php echo $user_id; ?>";
				var q_id = "<?php echo $q_id; ?>";
				var comment_id = 	0;
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("questions_answers_forum/post_comment_ajax"); ?>',
					data:{ 'user_id':user_id, 'q_id':q_id, 'comment_id':comment_id, 'comment_content':comment_content, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$("#send_comment").val("");
						get_comment_data(q_id, 0, '<?php echo $comment_limit; ?>', user_id, $("#sort_by_comment").val(), 0, data.new_comment_id, 1);
					}
				});
			}
		<?php } ?>
	}
	
	function sort_comments(sort_val)
	{
		get_comment_data('<?php echo $q_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', sort_val, 0, 0, 1);
	}
	
	function get_comment_data(q_id, start, limit, user_id, sort_order, is_show_more, new_comment_id, is_search)
	{
		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/get_comment_data_ajax"); ?>',
			data:{ 'q_id':q_id, 'start':start, 'limit':limit, 'user_id':user_id, 'sort_order':sort_order, 'is_show_more':is_show_more, 'new_comment_id':new_comment_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				if(data.flag == "success")
				{
					if(is_search == '1') { $(".comment_block_outer").html(''); }
					
					$(".token").val(data.csrf_new_token)
					$(".comment_block_outer").append(data.response);
					$("#total_comment_outer").html('Comments / Answers ('+data.total_comment_cnt+')');
					if(data.total_comment_cnt == 0) { $("#sort_outer").css("display", "none"); } else { $("#sort_outer").css("display", "block"); }
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	get_comment_data('<?php echo $q_id; ?>', 0, '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', $("#sort_by_comment").val(), 0, 0, 0);
	
	function show_hide_reply(comment_id)
	{
		if($("#comment_reply_"+comment_id).hasClass( "d-none" ))
		{
			$("#send_reply_comment_"+comment_id).val("");
			$("#send_reply_comment_"+comment_id).focus();
			$("#comment_reply_"+comment_id).removeClass( "d-none" );
		}
		else
		{
			$("#comment_reply_"+comment_id).addClass( "d-none" );
		}
	}
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
		
		//alert("Contents copied to clipboard.");
		return false;
	}
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });

function myFunction(id) {
	
  //document.getElementById("myDropdown"+id).classList.toggle("show");
  $("#myDropdown_"+id).toggleClass("show_dropdown");
}
</script>