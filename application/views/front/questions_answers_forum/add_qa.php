<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
	cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
	/**
	* Tooltip Styles
	*/
	
	/* Add this attribute to the element that needs a tooltip */
	[data-tooltip] {
	position: relative;
	z-index: 2;
	cursor: pointer;
	}
	
	/* Hide the tooltip content by default */
	[data-tooltip]:before,
	[data-tooltip]:after {
	visibility: hidden;
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
	opacity: 0;
	pointer-events: none;
	}
	
	/* Position tooltip above the element */
	[data-tooltip]:before {
	position: absolute;
	bottom: 150%;
	left: 50%;
	margin-bottom: 5px;
	margin-left: -80px;
	padding: 7px;
	width: 160px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	background-color: #000;
	background-color: hsla(0, 0%, 20%, 0.9);
	color: #fff;
	content: attr(data-tooltip);
	text-align: center;
	font-size: 14px;
	line-height: 1.2;
	}
	
	/* Triangle hack to make tooltip look like a speech bubble */
	[data-tooltip]:after {
	position: absolute;
	bottom: 150%;
	left: 50%;
	margin-left: -5px;
	width: 0;
	border-top: 5px solid #000;
	border-top: 5px solid hsla(0, 0%, 20%, 0.9);
	border-right: 5px solid transparent;
	border-left: 5px solid transparent;
	content: " ";
	font-size: 0;
	line-height: 0;
	}
	
	/* Show tooltip content on hover */
	[data-tooltip]:hover:before,
	[data-tooltip]:hover:after {
	visibility: visible;
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
	opacity: 1;
	}
	
	.form-control-placeholder{top: -14px !important;}
	textarea.error {color: #000000!important;}
	input.error{color:#000000!important;}
	
	/**overwrite css**/
	.form-check-input {position: absolute; margin-top: .3rem !important; margin-left: -1.25rem !important;}	
	
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	
	.guideline-tooltip{ position: relative;  display: inline-block;  margin-left: 10px; }
	.guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-35%, 0); transform: translate(-35%, 0); margin-top: 10px; visibility: visible; }
	.guideline-tooltip p { position: absolute; left: 50%; top: 100%; opacity: 0; padding: 1em; background-color: #e7f0ff; font-size: 14px; line-height: 1.6; text-align: left; white-space: nowrap; -webkit-transform: translate(-35%, 1em); transform: translate(-35%, 1em); -webkit-transition: all 0.15s ease-in-out; transition: all 0.15s ease-in-out; color: #000; z-index: 99; font-weight: 400; visibility: hidden; width: 500px; white-space: normal; }
	.guideline-tooltip p::before { content: ''; position: absolute; top: -16px; left: 33%; width: 0; height: 0; border: 0.6em solid  transparent; border-top-color: #e7f0ff; transform: rotate(180deg); }
	
	@media screen and (max-width:480px) { .guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-42%, 0); transform: translate(-42%, 0); margin-top: 10px; width: 300px; white-space: pre-line; } }
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Question</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('questions_answers_forum') ?>">Question & Answer Forum</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Question </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="QaForm" name="QaForm" enctype="multipart/form-data">		
						<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea name="question" id="question" class="form-control" cols="12" rows="4"><?php if($mode == 'Add') { echo set_value('question'); } else { echo $form_data[0]['question']; } ?></textarea>
									<label class="form-control-placeholder floatinglabel">Question <em>*</em></label>
									<?php if(form_error('question')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('question'); ?></label> <?php } ?>									
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Tags <em>*</em> </label>
									<?php /* <div class="guideline-tooltip">
										<i class="fa fa-info-circle fa-lg"></i>
										<p>Please select relevant Tag/Tags for the blog being posted. Tags will facilitate better reach & visibility. If the requisite field is not listed, kindly select "Other" and enter the desired tag.</p>
									</div> */ ?>
									<div class="boderBox65x">
										<select class="form-control select2_common" name="tags[]" id="tags" data-placeholder="Tags" multiple onchange="show_hide_tags_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = array();
														if($mode == 'Add') { if(set_value('tags[]') != "") { $tags_arr = set_value('tags[]'); } }
													else { $tags_arr = explode(",",$form_data[0]['tags']); } ?>
													<option value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" id="tags_other_outer">
								<div class="form-group">
									<input type="text" class="form-control" name="tags_other" id="tags_other" value="<?php if($mode == 'Add') { echo set_value('tags_other'); } else { echo $form_data[0]['tags_other']; } ?>">
									<label class="form-control-placeholder floatinglabel">Tags Other <em>*</em></label>
									<?php if(form_error('tags_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('tags_other'); ?></label> <?php } ?>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;"></em> Question Image </button>
									<input type="file" class="form-control" name="qa_image" id="qa_image" <?php /* if($mode == 'Add') { echo 'required'; } */ ?>>
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>		
								<?php if($qa_image_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $qa_image_error; ?></label> <?php } ?>
							</div>	
							
							<?php $dynamic_cls = $full_img = $onclick_fun = '';
								if($mode == 'Add')
								{
									$dynamic_cls = 'd-none';
								}
								else
								{
									if($form_data[0]['qa_image'] != "") { $full_img = base_url().'uploads/qa_image/'.$form_data[0]['qa_image']; }
								}
								
								if($full_img != "") { ?>
									<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="qa_image_outer">
										<div class="form-group">
											<div class="previous_img_outer">
												<img src="<?php echo $full_img; ?>">
												<a class="btn btn-danger btn-sm" onclick="delete_single_file('<?php echo $encrypt_obj->encrypt('arai_qa_forum'); ?>', '<?php echo $encrypt_obj->encrypt('q_id'); ?>', '<?php echo $encrypt_obj->encrypt($form_data[0]['q_id']); ?>', 'qa_image', '<?php echo $encrypt_obj->encrypt('./uploads/qa_image/'); ?>', 'Question Image')" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</div>
										</div>
									</div>
								<?php } ?>
						</div>
						
						<div class="row">
							<div class="col-md-4">
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary btnCancelCustom">Cancel</a> 
							</div>
							<div class="col-md-4 text-center">
								<?php if($mode == 'Add') { ?><a href="javascript:void(0)" onclick="confirm_clear_all()" class="btn btn-primary btnCancelCustom">Clear All</a><?php } ?> 
							</div>	
							<div class="col-md-4 text-right">
								<button type="submit" class="btn btn-primary add_button"><?php if($mode == 'Add') { echo 'Post'; } else { echo 'Update'; } ?></button> 
							</div>
						</div>						
					</form>
					
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">	
	$('.select2_common').select2();
	
	function show_hide_tags_other()
	{
		$("#tags_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#tags_other_outer").show();
			$("#tags_other").prop("required", true);
		}
		else
		{
			$("#tags_other_outer").hide();
			$("#tags_other").prop("required", false);
			$("#tags_other").val('');
		}
	}
	show_hide_tags_other();
	
	function confirm_clear_all()
	{
		swal(
		{  
			title:"Confirm?",
			text: "This action will clear the complete form. Please confirm if you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); location.reload(); } });
	}
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		
		//******* JQUERY VALIDATION *********
		$("#QaForm").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				question: { required: true, nowhitespace: true, minCount: ['3'], maxCount: ['150'] /* valid_value: true */ },
				"tags[]": { required: true},				
				qa_image: { <?php /* if($mode == 'Add') { ?>required: true,<?php } */ ?> valid_img_format: true,filesize:2000000},
			},
			messages:
			{
				question: { required: "This field is required", nowhitespace: "Please enter the title" },
				"tags[]":{required: "This field is required"},
				qa_image:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},									
			},
			<?php /* if($mode == 'Update') { ?>
				submitHandler: function(form)
				{
				swal(
				{  
				title:"Confirm?",
				text: "Are you sure you want to update the blog? as you updated the blog, it needs admin approval to activate the blog again",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
				}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); form.submit(); } });
				},
			<?php } */ ?>
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				element.closest('.form-group').append(error);
				/* if (element.attr("name") == "accept_terms") 
					{
					error.insertAfter("#accept_terms_err");
					}
					else
					{
					element.closest('.form-group').append(error);
				} */
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function ImagePreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#qa_image_outer").removeClass('d-none');
					//$("#qa_image_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#qa_image_outer .previous_img_outer img").attr("src", e.target.result);
					//$("#qa_image_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#qa_image_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#qa_image").change(function() { ImagePreview(this); });	
</script>		