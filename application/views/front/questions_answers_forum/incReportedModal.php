<?php $enc_obj =  New Opensslencryptdecrypt(); ?>
<div class="table-responsive">
	<table id="likes-list" class="table table-bordered table-hover challenges-listing-page">
		<thead>
			<tr>
				<th class="text-center">REP-ID</th>
				<th class="text-center">Comment</th>
				<th class="text-center">Commented By</th>
				<th class="text-center">Commented On</th>
			</tr>
		</thead>
		<tbody>	
			<?php if(count($likes_data) > 0)
			{
				$i=1;
				foreach($likes_data as $res)
				{	?>
					<tr>
						<td class="text-center"><?php echo $res['custom_reported_id']; ?></td>
						<td><?php echo $res['comments']; ?></td>
						<td>
							<?php 
								$title = $enc_obj->decrypt($res['title']);
								$first_name = $enc_obj->decrypt($res['first_name']);
								$middle_name = $enc_obj->decrypt($res['middle_name']);
								$last_name = $enc_obj->decrypt($res['last_name']);
								
								$disp_name = $title." ".$first_name;
								if($middle_name != '') { $disp_name .= " ".$middle_name." "; }
								$disp_name .= $last_name;
								echo $disp_name; ?>
						</td>
						<td class="text-right"><?php echo date("d-m-Y, h:ia", strtotime($res['created_on'])); ?></td>
					</tr>
		<?php	$i++;
				}				
			}	?>
		</tbody>
	</table>
</div>

<script>
$(document).ready( function () 
	{	  
		var table = $('#likes-list').DataTable(
		{
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.					
		});
	});
</script>