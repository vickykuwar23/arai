<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<style>
	.customBtnBlog { background: #FFF; color: #c80032;  border:solid #c80032 1px !important; text-decoration: none; padding: 5px 15px; border-radius: .25rem; border:none; }
	.customBtnBlog:hover { background: #c80032; color: #FFF; text-decoration: none; }
	.question_html {
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   line-height: 16px;     /* fallback */
   max-height: 100px;      /* fallback */
   -webkit-line-clamp: 2; /* number of lines to show */
   -webkit-box-orient: vertical;
}
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
	.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
	.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
	.select2-container--default .select2-selection--single{height:35px!important; border:none}
	#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
	
	.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
	.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
	.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
	.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
	.product_details{padding: 20px 0 0 0;}
	/** Blog**/
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
	.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
	.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
	.tag ul{ list-style: none; padding: 0; margin:0 0 0 0; }
	.tag ul li { display: inline-block; padding: 0; margin: 5px 0px 5px 2px; }
	.tag ul li span { background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; border-radius: 5px; padding: 5px; font-size: 13px !important}
	.tag ul li span:hover{ background: #FFF; color: #c80032; }
	.shareList { margin-top:15px; }
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; float: right; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	/* .shareList ul li.report_btn_listing { margin:0 0 0 8px; float:right; } */
	.shareList ul li span{  background: #FFF; color: #c80032; border: 1px #c80032 solid; text-decoration: none; border-radius: 5px; padding: 5px; cursor:pointer; font-size: 13px !important}
	.shareList ul li span:hover{ background: #c80032; color: #FFF; }
	.shareList ul li span.invert_color { color: #c80032; background: #FFF; }
	.shareList ul li span.invert_color:hover{ color: #FFF; background: #c80032; }
	.userPhoto-info h3{ font-size: 19px !important; }
	.userPhoto-info img{ border-radius: 50%;}
	.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
	.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.descriptionBox45{padding:15px;border:1px solid #CCC; height: 330px; }
	.descriptionBox45 p{color: #333;}
	.descriptionBox45 ul{color: #333; list-style: none; padding: 0; margin:0;  font-size: 14px;}
	.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
	.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#333}
	.bs-example45{border-radius:6px;background:#FFF;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
	.bs-example45 h3{color:#333;font-size:19px;font-weight:800}
	.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:99;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/** Color Change**/
	.nav-tabs{border-bottom:none}
	.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#FFF;background-color:#c80032 ;border-color:#c80032}
	/* .product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {
	border-color: #32f0ff #32f0ff #32f0ff;
	} */
	.title-bar .heading-border{background-color:#c80032}
	.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
	.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
	.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
	.owner-teammember img{height:100%!important; width:100%!important;border-radius: 0%!important;}
	.owner-teammember a{background:none;}
	.owner-teammember a:hover{background:none;}
	
	/**overwrite css**/
	.search-sec {padding: 1rem;background: #c80032; width: 100%; margin-bottom: 0px;}
	
	/* .filterBox .btn-primary {color: #32f0ff;	background-color: #020001; border-color: #020001;	width: 100%;display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}
	.filterBox .btn-primary:hover {color: #000; background-color: #32f0ff; border-color: #000}
	
	.clearAll{ background:#000 !important; color:#32f0ff !important; border-color: #32f0ff !important; margin-right:15px}
	.clearAll:hover{ background:#32f0ff !important; color:#000 !important; border-color: #000 !important;} */

	.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important;}

.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block;
font-weight: bold; padding: 10px 8px; border-radius: 0px;}
	
	.form-check-input {position: absolute; margin-top: .3rem !important; /* margin-left: -1.25rem !important; */ }
	#AnswersForum h2{font-size:1.75rem}
	
	.QuestionSlider.owl-carousel .owl-dot,.QuestionSlider.owl-carousel .owl-nav .owl-next,.QuestionSlider.owl-carousel .owl-nav .owl-prev, .TrendingblogSlider.owl-carousel .owl-dot,.TrendingblogSlider.owl-carousel .owl-nav .owl-next,.TrendingblogSlider.owl-carousel .owl-nav .owl-prev {font-family:fontAwesome}
	.QuestionSlider.owl-carousel .owl-nav .owl-prev:before, .TrendingblogSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	.QuestionSlider.owl-carousel .owl-nav .owl-next:after, .TrendingblogSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	.QuestionSlider .owl-nav.disabled, .TrendingblogSlider .owl-nav.disabled{display:block}
	.QuestionSlider .owl-nav, .TrendingblogSlider .owl-nav{position:relative; bottom:0px; width:100%}
	.QuestionSlider.owl-theme .owl-dots .owl-dot.active span,.QuestionSlider.owl-theme .owl-dots .owl-dot:hover span,
	.TrendingblogSlider.owl-theme .owl-dots .owl-dot.active span,.TrendingblogSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}

	.QuestionSlider.owl-theme .owl-nav, .TrendingblogSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
	.QuestionSlider .owl-nav .owl-prev, .TrendingblogSlider .owl-nav .owl-prev{background:#FFF; border:solid 1px #c80032; color: #c80032;}  
	.QuestionSlider .owl-nav .owl-next, .TrendingblogSlider .owl-nav .owl-next{  background:#FFF; border:solid 1px #c80032;  color: #c80032;}
	.QuestionSlider .owl-nav [class*=owl-]:hover, .TrendingblogSlider .owl-nav [class*=owl-]:hover{background:#c80032 !important;border:solid 1px #c80032;color:#FFF}
	
	.QuestionSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0 5px;}
	.QuestionSlider .owl-item img, .TrendingblogSlider .owl-item img{width:100%; }
	.QuestionSlider .owl-dots, .TrendingblogSlider .owl-dots{position:absolute;width:100%;bottom:25px}
	.QuestionSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0;}
	
.ButtonAllWebinars a {background: #c80032; color: #FFF; border: solid 1px #c80032; text-decoration: none; padding:  10px;border-radius: .25rem;}

.ButtonAllWebinars a:hover {background: #FFF; color: #c80032; text-decoration: none;}
	
	.userPhoto-info h3 span { font-size: 14px !important; color: #a3a3a3; text-transform: initial; } 
	/* .bgNewBox { background: #000; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; } */
	
	a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag { background: none; font-size: 14px;
	float: right; }
	
	/*a.custom_question_title  { font-size: 16px; font-weight: 600; color: #41464b; white-space: break-spaces; display: block; line-height: 22px; } 
	a.custom_question_title:hover { color:#000; text-decoration:none; }*/
	.custom_question_title { font-size: 18px; font-weight: 600; color: #41464b; white-space: pre-line; display: block; line-height: 24px; margin:0; } 
	
	.postDate { margin-bottom:15px; }
	.postDate span { font-size:13px; margin-bottom:15px; }

.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  right:0px; top:-40px;
}

.dropdown-content a {
  color: black;
  text-decoration: none;
  display: block; color:#FFF;
   background-color: #FFF; color:#c80032;

}

.dropdown a:hover {background-color: #c80032; color:#FFF;}

.show_dropdown {display:block;}
/*.btn_report {background: #FFF !important; color: #c80032 !important;}*/
/*.btn_report:hover {background: #c80032 !important; color: #FFF !important;}*/
.comment_block_common { border: 1px solid #ccc;border-radius: 15px;padding: 15px; }	
.comment_block_common .comment_img { width: 40px; height: 40px; overflow: hidden; border-radius: 50%; float: left; }
.comment_block_common .comment_img img { max-width: 100%; max-height: 100%; }
.comment_block_common .comment_inner { float: left; width: calc(100% - 50px); margin-left: 10px; }
.comment_block_common .comment_inner p.comment_name { font-weight: 600; margin: 0 0 0 0; }
.comment_block_common .comment_inner p.comment_name a:hover { color:#000; }
.comment_block_common .comment_inner p.comment_time { margin: 0; font-size: 12px; font-weight: 500; color: #787878; }
.comment_block_common .comment_inner p.comment_content { margin: 5px 0 5px 0; }
.comment_block_common .comment_inner p.comment_like_btn { cursor: pointer; margin: 0; display: inline-block; }
.blog_type_highlighted_text { font-size: 12px; background: #c80032; padding: 1px 6px 2px; border-radius: 3px; font-weight: 500; color: #FFF; line-height: 16px; position: absolute; top: 0; right: 0; }


.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}

 .datepicker {background: #FFF !important; border-radius: 0;}




</style>

<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Technology Wall</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">List of Question & Answer Forum</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php $this->load->view('front/webinar/inc_technology_wall_tabs'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade pt-0" id="AnswersForum">
			
			<!------------ START : QUESTION SEARCH --------->
			<div class="filterBox mb-4">
				<section class="search-sec">
					<div id="filterData" name="filterData">
						<div class="container">   
							<div class="row">
								<div class="col-lg-12">
									<div id="mySidenav" class="sidenav">
										<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
										<div class="scroll">	
											<ul>
												<li> <strong> Select Tags</strong> </li>										
												<?php 
													$i=0;
													if(count($tag_data) > 0) 
													{ 
														foreach($tag_data as $res)
														{ ?>										
														<li class="<?php if($i >= 5) { ?>filter_tag d-none<?php	} ?>">
															<input class="form-check-input search_tag" type="checkbox" value="<?php echo $res['id']; ?>" id="search_tags" name="search_tags[]" onchange="getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0)">
															<label class="form-check-label"><?php echo $res['tag_name']; ?></label>
														</li>
														<?php $i++;															
														}
													}	?>
											</ul>
											<?php if(count($tag_data) > 5){ ?>
												<a href="javascript:void(0);" class="filter_load_more_tag" onclick="show_more_tag('show')">Show More >> </a>
												<a href="javascript:void(0);" class="filter_load_less_tag d-none" onclick="show_more_tag('hide')">Show Less >></a><br>
											<?php } ?>
											
											<ul>
												<li> <strong> Types</strong> </li>										
												<li class="">
													<input class="form-check-input search_type" type="checkbox" value="0" id="search_types" name="search_type[]" onchange="getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0)">
													<label class="form-check-label">Organization</label>
												</li>										
												<li class="">
													<input class="form-check-input search_type" type="checkbox" value="1" id="search_types" name="search_type[]" onchange="getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0)">
													<label class="form-check-label">Individual</label>
												</li>									
												<li class="">
													<input class="form-check-input search_type" type="checkbox" value="11" id="search_types" name="search_type[]" onchange="getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0)">
													<label class="form-check-label">Experts</label>
												</li>
											</ul>
										</div>								
									</div>
									
									<div class="row d-flex justify-content-center search_Box">				
										<div class="col-md-2">
										<input type="text" class="form-control datepicker" name="from_date" id="from_date"  value="" placeholder="From Date" readonly />
			                            </div>	

			                            <div class="col-md-2">
										<input type="text" class="form-control datepicker" name="to_date" id="to_date"  value="" placeholder="To Date" readonly />
			                            </div>				
										<div class="col-md-5">
											<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />	
											<button type="button" class="btn btn-primary searchButton" onclick="getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
										<div class="clearAll">
											<a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form()">Clear All</a>
										</div>
										<div class="filter d-nonex">
											<button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
										</div>																		
										<div class="col-md-12"></div>								
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!------------ END : QUESTION SEARCH --------->
			
			<div class="container">		
				<div class="title-bar" id="FeaturedQuestionsSliderOuterTitle">
					<h2>Featured Questions </h2>
					<div class="heading-border"></div>
				</div>
				<div id="FeaturedQuestionsSliderOuter" class="mt-4"></div>
				
				<div class="title-bar" id="QaForumOuterTitle">
					<h2>All Questions </h2>
					<div class="heading-border"></div>
				</div>
				<div id="QaForumOuter"></div>	
			</div>         
		</div>
	</div>
</section>

<style>
	#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
#flotingButton2 a {color: #FFF; background: #c80032; padding: 10px 15px; border: solid 1px #c80032; text-decoration:  none; display: block; text-align: center;border-radius: 15px; font-size: 14px;}

#flotingButton2 a:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none;}
</style>

<div id="flotingButton2" class="d-none">
	<a href="<?php echo base_url('questions_answers_forum/add_qa'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post Question</a>
</div>
<div id="social-share">
                    <div class="social-open-menu">
                        <a class="btn-share"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    </div>
                    <ul class="social-itens hidden">
                    	<li>
                            <a class="btn-share social-item-2" href="<?php echo base_url('questions_answers_forum/add_qa'); ?>">Ask Question</a>
                        </li>
                       
                        <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('resource_sharing/add'); ?>">Share / Post a Resource</a>
                        </li> 
                         <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('technology_transfer/add'); ?>">Share / Post Technology to Transfer </a>
                        </li>
                        <li>
                            <a class="btn-share social-item-1" href="<?php echo base_url('knowledge_repository/add'); ?>">Share Knowledge </a>
                        </li>
                        
                        <li>
                            <a class="btn-share social-item-3" href="<?php echo base_url('blogs_technology_wall/add_blog'); ?>">Write a Blog</a>
                        </li>

                        <li>
                            <a class="btn-share social-item-5" href="<?php echo base_url('webinar/add'); ?>">Share / Post Webinar</a>
                        </li>
                    </ul>

                </div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

<div class="modal fade" id="BlogDescriptionDetail" tabindex="-1" aria-labelledby="BlogDescriptionDetailLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogDescriptionDetailLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
				</button>
			</div>
      <div class="modal-body" id="BlogDescriptionDetailContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!---------- MODAL TO DISPLAY THE QUESTION SHARE URL ------------------>
<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="CommonShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('CommonShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<!---------- MODAL TO DISPLAY THE USER DATA IN POP UP WHEN CLICK ON NAME ------------------>
<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
		<div class="modal-content" id="UserDetailModalContentOuter">			
		</div>
	</div>
</div>

<!------ REPORT QUESTION MODAL ------------->
<div class="modal fade" id="QuestionReportpopup" tabindex="-1" role="dialog" aria-labelledby="QuestionReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="QuestionReportpopupLabel">Question Report</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
					<div>
						<label><b>Question ID : </b><span id="popupQuestionId"></span></label>
					</div>
					<div>
						<label><b>Question : </b><span id="popupQuestionTitle"></span></label>
					</div>
					
					<div>
						<label for="popupQuestionReportComment"><b>Comment <em>*</em></b></label>
						<textarea class="form-control" id="popupQuestionReportComment" name="popupQuestionReportComment" required onkeyup="check_report_validation()"></textarea>
						<span id="popupQuestionReportComment_err" class='error'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<span id="submit_report_btn"><button type="button" class="btn btn-primary" onclick="submit_question_report('')">Submit</button></span>		
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>


<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php if($this->session->flashdata('success_question_del_msg')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success_question_del_msg'); ?>"); </script><?php } ?>

<script>

function show_hide_reply(comment_id)
	{
		if($("#comment_reply_"+comment_id).hasClass( "d-none" ))
		{
			$("#send_reply_comment_"+comment_id).val("");
			$("#send_reply_comment_"+comment_id).focus();
			$("#comment_reply_"+comment_id).removeClass( "d-none" );
		}
		else
		{
			$("#comment_reply_"+comment_id).addClass( "d-none" );
		}
	}
 
 function like_dislike_comment(id, flag)
	{		
		
			var cs_t = 	$('.token').val();
			$.ajax(
			{
				type:'POST',
				url: '<?php echo site_url("questions_answers_forum/like_unlike_comment_ajax"); ?>',
				data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
				dataType:"JSON",
				success:function(data)
				{
					$("#preloader").css("display", "none");
					$(".token").val(data.token);
					
					$("#like_unlike_comment_btn_outer_"+data.comment_id).html(data.response);
				}
			});
		
	}		


 $(document).ready(function () { 

	$('.datepicker').datepicker({
	   format: 'dd-mm-yyyy',
	   //minDate: '0',
	   //startDate:'+0d',
	   autoclose: true
   });
 });  

function myFunction(id) {
	
  //document.getElementById("myDropdown"+id).classList.toggle("show");
  $("#myDropdown_"+id).toggleClass("show_dropdown");
}
	function show_more_tag(val)
	{
		if(val == 'show')
		{
			$( ".filter_tag" ).removeClass( "d-none" )
			$( ".filter_load_less_tag" ).removeClass( "d-none" )
			$( ".filter_load_more_tag" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_tag" ).addClass( "d-none" )
			$( ".filter_load_less_tag" ).addClass( "d-none" )
			$( ".filter_load_more_tag" ).removeClass( "d-none" )
		}
	}
	
	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0);
		} 
	});
	
	function clear_search_form() 
	{ 
		$("#to_date").attr("placeholder", "To date");
        $("#from_date").attr("placeholder", "From date");
		$("#from_date").val('');
		$("#to_date").val(''); 

		$("#search_txt").val('');  
		$('.search_tag:checkbox').prop('checked',false); 		
		$('.search_type:checkbox').prop('checked',false);
		
		getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0);
	}
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
		
		//alert("Contents copied to clipboard.");
		return false;
	}
	
	//ON CLICK ON USER IMAGE, OPEN USER DETAIL POP UP
	function get_user_details(user_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'user_id':user_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('myteams/CommonUserDetailsAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)					
					$("#UserDetailModalContentOuter").html(data.response)
					$("#UserDetailModal").modal('show')
					$("#preloader-loader").hide();
				}
				else 
				{ 
					$("#preloader-loader").hide();
					sweet_alert_error("Error Occurred. Please try again."); 
				}
			}
		});	
	}
	
	//START : QA FORUM DATA
	function getQaForumDataAjax(fqa_start, fqa_limit, qa_start, qa_limit, is_search, is_show_more)
	{
		$("#to_date").css('border-color','#88888');
		$("#from_date").css('border-color','#88888');
		$("#to_date").attr("placeholder", "To date");
        $("#from_date").attr("placeholder", "From date");

		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		var selected_tag = [];
		$.each($("input.search_tag:checked"), function() { selected_tag.push($(this).val()); });
		
		var selected_type = [];
		$.each($("input.search_type:checked"), function() { selected_type.push($(this).val()); });
		
		var keyword = encodeURIComponent($('#search_txt').val());
		var tag = encodeURIComponent(selected_tag);
		var search_type = encodeURIComponent(selected_type);

		var from_date = $("#from_date").val();
		var to_date  = $("#to_date").val();

		if (from_date!='') {
			if(to_date==''){
			$("#to_date").css('border-color','red');
			 $("#to_date").attr("placeholder", "Select to date");
			return false;
			}
		}

		if (to_date!='') {

			if(from_date==''){
				$("#from_date").css('border-color','red');
				$("#from_date").attr("placeholder", "Select from date");
				return false 
			}

			var startDate = new Date($('#from_date').val().split("-").reverse().join("-") );
			var endDate = new Date($('#to_date').val().split("-").reverse().join("-"));

		

			if (  startDate > endDate  ){
				$("#to_date").css('border-color','red');
				$("#to_date").val('');
				$("#to_date").attr('placeholder','Invalid End Date');
				return false;
			}

		}
		
		parameters= { 'fqa_start':fqa_start, 'fqa_limit':fqa_limit, 'qa_start':qa_start, 'qa_limit':qa_limit, 'featured_question_limit':'<?php echo $featured_question_limit; ?>', 'question_limit':'<?php echo $question_limit; ?>', 'is_show_more':is_show_more, 'keyword':keyword, 'tag':tag, 'search_type':search_type, 'cs_t':cs_t,'from_date':from_date,'to_date':to_date }
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('questions_answers_forum/getQaForumDataAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)
					if(is_search == '1') { $("#QaForumOuter").html(''); }
					
					if(is_show_more == 0) 
					{ 
						if(data.Featured_response.trim() == "") { $("#FeaturedQuestionsSliderOuterTitle").css("display", "none"); } 
						else { $("#FeaturedQuestionsSliderOuterTitle").css("display", "block"); }						
						$("#FeaturedQuestionsSliderOuter").html(data.Featured_response); 
					}
					
					if(data.QA_forum_response.trim() == "" || data.qa_forum_question_total_cnt == 0) { $("#QaForumOuterTitle").css("display", "none"); } 
					else { $("#QaForumOuterTitle").css("display", "block"); }
					$("#QaForumOuter").append(data.QA_forum_response);
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 0, 0);
	//END : QA FORUM DATA
	
	function delete_question_from_listing(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected Question?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("questions_answers_forum/delete_question"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$("#preloader").css("display", "none");
						$(".token").val(data.token);
						
						getQaForumDataAjax(0, '<?php echo $featured_question_limit; ?>', 0, '<?php echo $question_limit; ?>', 1, 0);
						
						swal(
						{
							title: 'Success!',
							text: "Question successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function like_unlike_question(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/like_unlike_question_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#like_unlike_btn_outer_"+data.q_id).html(data.response);
			}
		});
	}
	
	function share_question(url)
	{
		$("#CommonShareLink").html(url);
		$("#share-popup").modal('show');
	}
	
	function report_qa(q_id, ReportedFlag, custum_question_id, question)
	{
		if(ReportedFlag == "1")
		{
			$("#popupQuestionId").html(custum_question_id);
			$("#popupQuestionTitle").html(question);
			
			var onclick_fun = "submit_question_report('"+q_id+"')";
			$("#submit_report_btn").html('<button type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
			$("#popupQuestionReportComment_err").html('');
			$("#popupQuestionReportComment").val('');
			$("#QuestionReportpopup").modal('show');
			$("#popupQuestionReportComment").focus();
		}
		else
		{
			swal(
			{
				title: 'Alert!',
				text: "You have already reported the question",
				type: 'alert',
				showCancelButton: false,
				confirmButtonText: 'Ok'
			});
		}		
	}
	
	function check_report_validation()
	{
		var popupQuestionReportComment = $("#popupQuestionReportComment").val();
		if(popupQuestionReportComment.trim() == "") { $("#popupQuestionReportComment_err").html('Please enter the report comment'); $("#popupQuestionReportComment").focus(); }
		else { $("#popupQuestionReportComment_err").html(''); }
	}
	
	function submit_question_report(q_id)
	{	
		check_report_validation();
		
		var popupQuestionReportComment = $("#popupQuestionReportComment").val();
		if(popupQuestionReportComment.trim() != "")		
		{
			$("#popupQuestionReportComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to report the selected question?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("questions_answers_forum/report_qa_ajax"); ?>',
						data:{ 'q_id':q_id, 'popupQuestionReportComment':popupQuestionReportComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#report_qa_btn_outer_"+data.q_id).replaceWith(data.response);						
							$("#QuestionReportpopup").modal('hide');
							
							swal(
							{
								title: 'Success!',
								text: "Question Reported successfully",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	
	
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	
	function openNav() 
	{		
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
		
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";		
	}

$(document).ready(function(){
$(".social-open-menu").click(function () {
    $(".social-itens").toggleClass("open");
    $(".social-itens").toggleClass("hidden");
});
});
</script>