<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	.MyTeamsListingTab ul li:before { display:none; }
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Posted Questions</h1>        
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">			
			<div class="col-md-12">
				<div class="formInfo">
					
					<section>
						<div class="product_details MyTeamsListingTab">
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="TeamTab1">
									
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
												<div class="table-responsive">
													<table id="MyQuestionListing" class="table table-bordered table-hover challenges-listing-page">
														<thead>
															<tr>
																<th scope="col">Question ID</th>
																<th scope="col">Question</th>																
																<th scope="col">Posted ON</th>
																<th scope="col">Admin Approval</th>
																<th scope="col">Likes</th>
																<th scope="col">Comments</th>
																<th scope="col">Reports</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>							
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

<!---------- MODAL POP UP FOR SHOW LIKES USER DATA FOR QUESTION --------------------->
<div class="modal fade" id="LikesPopUp" tabindex="-1" aria-labelledby="LikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="LikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!---------- MODAL POP UP FOR BLOCK QUESTION --------------------->
<div class="modal fade" id="QuestionBlockpopup" tabindex="-1" role="dialog" aria-labelledby="BlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlockpopupLabel"><b>Block Question?</b></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
					<div>
						<label><b>Question ID : </b><span id="popupQuerstionId"></span></label>
					</div>
					<div>
						<label><b>Question : </b><span id="popupQuestionTitle"></span></label>
					</div>
					
					<div>
						<label for="popupQuestionBlockReason"><b>Block reason <em>*</em></b></label>
						<textarea class="form-control" id="popupQuestionBlockReason" name="popupQuestionBlockReason" required onkeyup="check_block_validation()"></textarea>
						<span id="popupQuestionBlockReason_err" class='error'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button id="modal_submit_btn" type="button">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
  $(document).ready( function () 
	{	  
		var base_path = '<?php echo base_url() ?>';
		var table = $('#MyQuestionListing').DataTable({
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": base_path+"questions_answers_forum/myQuestionsAjax",
				"type":"POST",
				"data":function(data) 
				{						
					/* data.techonogy_id = $("#techonogy_id").val();
					data.change_visibility = $("#change_visibility").val();
					data.c_status = $("#c_status").val();
					data.audience_pref = $("#audience_pref").val(); */
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": {
					401:function(responseObject, textStatus, jqXHR) {			
					},
				},
			}			
		});
	});
	
	function block_unblock_question(id, type, custum_question_id, question)
	{
		if(type == "Block")
		{
			$("#popupQuerstionId").html(custum_question_id);
			$("#popupQuestionTitle").html(question);
			$("#popupQuestionBlockReason_err").html('');
			
			var onclick_fun = "submit_question_block('"+id+"', '"+type+"')";
			$("#modal_submit_btn").replaceWith('<button id="modal_submit_btn" type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
			
			$("#QuestionBlockpopup").modal('show');
			$("#popupQuestionBlockReason").val('');
			$("#popupQuestionBlockReason").focus();			
		}
		else
		{
			submit_question_block(id, type)
		}		
	}
	
	function check_block_validation()
	{
		var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
		if(popupQuestionBlockReason.trim() == "") { $("#popupQuestionBlockReason_err").html('Please enter the block reason'); $("#popupQuestionBlockReason").focus(); }
		else { $("#popupQuestionBlockReason_err").html(''); }
	}
	
	function submit_question_block(q_id, type)
	{	
		check_block_validation();
		
		var flag = 0;
		if(type == 'Block')
		{
			var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
			if(popupQuestionBlockReason.trim() == "")	{ flag = 1;}
		}
				
		if(flag == 0)
		{
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to "+type+" the selected question?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("questions_answers_forum/block_unblock_question"); ?>',
						data: {'id':q_id, 'type':type, 'popupQuestionBlockReason':popupQuestionBlockReason, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#QuestionBlockpopup").modal('hide');
							
							$('#MyQuestionListing').DataTable().ajax.reload();	
							swal(
							{
								title: 'Success!',
								text: "Question successfully "+type+"ed.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	
	
	function delete_question(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected question?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("questions_answers_forum/delete_question"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						
						$('#MyQuestionListing').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Question successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function show_question_reported(q_id)
	{
      var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/show_reported_user_ajax"); ?>',
			data:{ 'q_id':q_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#LikesLabel").html(data.question);				
					$("#LikesContent").html(data.response);				
					$("#LikesPopUp").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function show_question_likes(id)
	{
      var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/show_likes_user_data_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#LikesLabel").html(data.question);				
					$("#LikesContent").html(data.response);				
					$("#LikesPopUp").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>