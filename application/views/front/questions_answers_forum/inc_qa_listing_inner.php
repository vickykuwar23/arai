<?php 
	$encrptopenssl =  New Opensslencryptdecrypt();
	$disp_name = $encrptopenssl->decrypt($res['title'])." ".$encrptopenssl->decrypt($res['first_name']);
	if($res['middle_name'] != '') { $disp_name .= " ".$encrptopenssl->decrypt($res['middle_name'])." "; }
	$disp_name .= " ".$encrptopenssl->decrypt($res['last_name']); 
	
	// $this->db->limit(1);
	//$this->db->group_by('comment_id'); 
	//$this->db->order_by('comment_id', 'asc');
	// $this->db->select_max('total_likes');

	//  $this->db->join('arai_registration r','r.user_id = bc.user_id', 'LEFT', FALSE);
	// $this->db->join('arai_profile_organization org','org.user_id = bc.user_id', 'LEFT', FALSE);
	// $this->db->join('arai_student_profile sp','sp.user_id = bc.user_id', 'LEFT', FALSE);
	// $most_like_answer = $this->master_model->getRecords("arai_qa_forum_comments bc",array('bc.q_id' => $res['q_id'], 'bc.status'=>1, 'bc.is_block'=>0, 'bc.parent_comment_id'=>0 ),'bc.q_id, bc.comment,r.user_category_id,r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture,bc.created_on,bc.user_id,total_likes,(SELECT MAX(`total_likes`) FROM arai_qa_forum_comments) AS MaxLikes');
	// echo $this->db->last_query();die;

	 $sql="SELECT  bc.q_id, bc.comment, r.user_category_id, r.user_sub_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture, bc.created_on, bc.user_id, total_likes,bc.comment_id 
		FROM arai_qa_forum_comments bc 
		LEFT JOIN arai_registration r ON r.user_id = bc.user_id
		LEFT JOIN arai_profile_organization org ON org.user_id = bc.user_id 
		LEFT JOIN arai_student_profile sp ON sp.user_id = bc.user_id 
		WHERE total_likes = (SELECT MAX(total_likes) FROM arai_qa_forum_comments WHERE q_id = ".$res['q_id'].")  AND bc.status = 1 AND bc.is_block = 0 AND bc.parent_comment_id = 0 AND q_id = ".$res['q_id']." GROUP BY comment_id ORDER BY comment_id DESC  LIMIT 1";
    $query = $this->db->query($sql);
    $most_like_answer= $query->result_array();
	
	$dispAnswer = '';
	if(count($most_like_answer) > 0)
	{
		$dispAnswer = $most_like_answer[0]['comment'];
		$replyCount= $this->master_model->getRecordCount("arai_qa_forum_comments", array('status' => '1', 'parent_comment_id' => $most_like_answer[0]['comment_id'], 'is_block' => '0'));
	} ?>
	
	<?php $dispHighlight = '';
	 if($res['user_category_id'] == 2) { $dispHighlight = 'Organization'; } 
	 else { 
	 	if($res['user_sub_category_id'] == 11) { $dispHighlight = 'Expert'; 
	 	} } 
		?>
		<?php if ($dispHighlight!='') { ?>
			<span class="blog_type_highlighted_text"><?php echo $dispHighlight  ?></span>
		<?php } ?>
			
	<div class="row userPhoto-info">
		
			
		<?php /* if($res["qa_image"] != "") { ?>
			<div class="col-md-4">
				<a href="<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id'])); ?>"> 
					<img src="<?php echo base_url('uploads/qa_image/'.$res["qa_image"]); ?>" alt="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" title="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" class="img-fluid" style='max-width: 300px; max-height: 100px; border-radius: 0; margin: 0 auto 20px auto; display: block; '>
				</a>
			</div>
		<?php } */ ?>

		<div class="col-md-2">                     
			<?php $iconImg = base_url('assets/no-img.png');
				if($res['user_category_id'] == 2)
				{
					if($res['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($res['org_logo'])); }
				}
				else 
				{
					if($res['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$res['profile_picture']); }
				} ?>
				<img src="<?php echo $iconImg; ?>" alt="<?php echo $disp_name; ?>" title="<?php echo $disp_name; ?>" class="img-fluid" style='max-width: 80px; max-height: 80px; height: 80px; border: 1px solid #ccc; display: block; margin: 0 auto 5px auto; border-radius:50%; '>
				<a href="javascript:void(0)" style="color: #41464b; font-size: 12px; text-align: center; display: block; font-weight: 500; text-transform: capitalize;" onclick="get_user_details('<?php echo base64_encode($res['user_id']); ?>')">
					<?php echo $disp_name; ?> 
				</a>		
		</div>
				
		<div class="<?php if($res["qa_image"] != "") { echo 'col-md-10'; } else { echo 'col-md-10'; } ?>">
			<div style="min-height: 100px">
			<h4 style="margin:0;line-height:22px;" >
				<a href="<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id'])); ?>" style='font-size: 18px;margin: 0; color: #000;' class="question_html custom_question_title">
					<?php $dispQuestion = htmlspecialchars_decode($res['question']);
					$dispQuestion_plain = strip_tags(htmlspecialchars_decode($res['question']) );

						echo substr($dispQuestion, 0, 150); 

						?>
						<?php  if(strlen($dispQuestion_plain) > 150) { echo '... '; ?> <?php } 
						?>
				</a>
			</h4>
			<?php  if($res["qa_image"] != "") { ?>
			<div class="col-md-12 float-left">
				<?php /* ?>
				<a href="<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id'])); ?>"> 
					<img src="<?php echo base_url('uploads/qa_image/'.$res["qa_image"]); ?>" alt="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" title="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" class="img-fluid p-2 float-left" style='max-width: 200px; max-height: 100px; border-radius: 0; margin: 0 auto 20px auto; display: block; '>
				</a>
				<?php */ ?>
				<a href="<?php echo base_url('uploads/qa_image/'.$res['qa_image']); ?>" data-fancybox data-caption="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>">
					<img height="75" width="75" src="<?php echo base_url('uploads/qa_image/'.$res["qa_image"]); ?>" alt="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" title="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>" class="p-2 float-left" style='max-width: 75px; max-height: 75px; border-radius: 0; display: block;'>
				</a>

			</div>
			<?php }  ?>
			</div>
			<div class="col-md-12 postDate mb-2"><span class="float-right">Posted on <?php echo date("jS F Y", strtotime($res['created_on'])); ?>  at <?php echo date("h:i a", strtotime($res['created_on'])); ?></span></div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-2">      

			<?php /* $iconImg = base_url('assets/no-img.png');
				if($res['user_category_id'] == 2)
				{
					if($res['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($res['org_logo'])); }
				}
				else 
				{
					if($res['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$res['profile_picture']); }
				} ?>
				<img src="<?php echo $iconImg; ?>" alt="<?php echo $disp_name; ?>" title="<?php echo $disp_name; ?>" class="img-fluid" style='max-width: 80px; max-height: 80px; border: 1px solid #ccc; display: block; margin: 0 auto 5px auto; border-radius:50%; '>
				<a href="javascript:void(0)" style="color: #41464b; font-size: 12px; text-align: center; display: block; font-weight: 500; text-transform: capitalize;" onclick="get_user_details('<?php echo base64_encode($res['user_id']); ?>')">
					<?php echo $disp_name; ?> 
				</a>
				<?php */ ?>

		</div>
		
		<div class="col-md-10">
			<div class="bgNewBox">			
				<div class="tag">
					<?php if($res['DispTags'] != "") 
						{ 
						$tags_arr = explode("##",$res['DispTags']); ?>
						<div class="tag">
							<ul>
								<li style='font-weight:600'>Tags:</li> 
								<?php foreach($tags_arr as $tag)
									{	
										if(strtolower(trim($tag)) != 'other')
										{	?>
										<li><span><?php echo $tag; ?></span></li> 
										<?php }
									}
									
									if($res['tags_other'] != "") 
									{	
									$tag_other_arr=explode(',', $res['tags_other']);
									foreach ($tag_other_arr as $key => $tag_other) {	
									?>

									<li><span><?php echo $tag_other; ?></span></li> 

								<?php } }?>
							</ul>
						</div>
					<?php } ?>
				</div>
			</div>

				<div class="shareList">
				<ul class="float-left">
							<li><span onclick="window.location.href='<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id']).'/1'); ?>'"><i class="fa fa-comments" aria-hidden="true"></i> Comments/Answers <small>(<?php echo $res['CommentCnt']; ?>)</small></span></li> 
				</ul>	
				<ul>
					<?php if(in_array($res['q_id'], $QaActiondata['like_arr'])) 
						{ 
							$Like_label = "Like"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-up'; $LikeClass = 'invert_color'; 
						}
						else 
						{ 
							$Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa fa-thumbs-o-up'; $LikeClass = '';
						} ?>
						
						<li id="like_unlike_btn_outer_<?php echo $res['q_id']; ?>">
							<span><?php echo  $res['LikeCnt']; ?></small></span>
							<span class="<?php echo $LikeClass; ?>" onclick="like_unlike_question('<?php echo base64_encode($res['q_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
						</li> 
						
						<li>
							<span onclick="share_question('<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
						</li>
						
				
						
						<?php if(in_array($res['q_id'], $QaActiondata['self_arr'])) { ?>
							<li><span onclick="delete_question_from_listing('<?php echo base64_encode($res['q_id']); ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</span></li> 
						<?php } ?>
						
						<?php 
							if(!in_array($res['q_id'], $QaActiondata['self_arr']))
							{
								if(in_array($res['q_id'], $QaActiondata['reported_arr'])) { $Reported_label = "Question Already Reported"; $ReportedFlag = '0';  }
							else { $Reported_label = "Report Question"; $ReportedFlag = '1'; } ?>
							<?php /*
							<li id="report_qa_btn_outer_<?php echo $res['q_id']; ?>" class='report_btn_listing'>
								<span title="<?php echo $Reported_label; ?>" onclick="report_qa('<?php echo base64_encode($res['q_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $res['custum_question_id']; ?>', '<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>')"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span>
							</li>
							*/?>
					
							<li  class='report_btn_listing'>
							<div class="dropdown">
									  <span style="cursor: pointer;" onclick="myFunction('<?php echo $res['q_id'];  ?>')" class="dropbtn pl-1 pr-1"><i style="padding:0 5px;" class="fa fa-ellipsis-v" ></i></span>
									  <div id="myDropdown_<?php echo $res['q_id']; ?>" class="dropdown-content">
									    <a id="report_qa_btn_outer_<?php echo $res['q_id']; ?>" class="btn btn-sm btn-danger btn_report" href="javascript:void(0)" title="<?php echo $Reported_label; ?>" onclick="report_qa('<?php echo base64_encode($res['q_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $res['custum_question_id']; ?>', '<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>')">Report</a>
									    
							  </div>
							</div>
									
						</li>
						<?php } ?>
				</ul>


			</div> 

			<div class="clear-fix">&nbsp;</div>
			
			<?php if($dispAnswer != "") { 
				
				$iconImg = base_url('assets/no-img.png');
				if($most_like_answer[0]['user_category_id'] == 2)
				{
					if($most_like_answer[0]['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($most_like_answer[0]['org_logo'])); }
				}
				else 
				{
					if($most_like_answer[0]['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$most_like_answer[0]['profile_picture']); }
				} 
				
				$title = $encrptopenssl->decrypt($most_like_answer[0]['title']);
				$first_name = $encrptopenssl->decrypt($most_like_answer[0]['first_name']);
				$middle_name = $encrptopenssl->decrypt($most_like_answer[0]['middle_name']);
				$last_name = $encrptopenssl->decrypt($most_like_answer[0]['last_name']);
				
				$disp_name_commentor = $title." ".$first_name;
				if($middle_name != '') { $disp_name_commentor .= " ".$middle_name." "; }
				$disp_name_commentor .= $last_name;
				
				$dispHighlight_ans = ''; if($most_like_answer[0]['user_category_id'] == 2) { $dispHighlight_ans = 'Organization'; } else { if($most_like_answer[0]['user_sub_category_id'] == 11) { $dispHighlight_ans = 'Expert'; } } 

				?>
				<div class="comment_block_common mt-4">
				<div class="comment_img"><img src="<?php echo $iconImg; ?>"></div>
			<div class="comment_inner">
				<p class="comment_name">
					
					<a href="javascript:void(0)" style="color: #41464b; font-size: 12px;  display: block; font-weight: 500; text-transform: capitalize;" onclick="get_user_details('<?php echo base64_encode($most_like_answer[0]['user_id']); ?>')">
					<?php echo $disp_name_commentor; ?>
					<?php  if($dispHighlight_ans != '') 
						{ ?> 
							<span style='font-weight: 500; font-size: 12px; color: #595959;'>(<?php echo $dispHighlight_ans; ?>)</span>
					<?php }  ?>
						</a>
					
					
				</p>
				<p class="comment_time">
					<?php echo $this->Common_model_sm->time_Ago(strtotime($most_like_answer[0]['created_on']), $most_like_answer[0]['created_on']); ?>
				</p>
				<p class="comment_content">				
						<?php $dispAnswer =nl2br( strip_tags(htmlspecialchars_decode($dispAnswer)));
							echo substr($dispAnswer, 0, 300); 
						if(strlen($dispAnswer) > 300) { echo '... '; ?><span onclick="window.location.href='<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id']).'/1'); ?>'" style="cursor:pointer;"><b>Read More</b></span> <?php } ?>
					</p>


				<?php if(in_array($most_like_answer[0]['comment_id'], $CommentActiondata['like_comments'])) { $Like_label = "Like"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-up'; $bg_color="style='color:#1562a6; font-weight:500;'"; }
				else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-o-up'; $bg_color="style='color:#000'"; } ?>
				
				<span id="like_unlike_comment_btn_outer_<?php echo $most_like_answer[0]['comment_id']; ?>">
					<p class="comment_like_btn" onclick="like_dislike_comment('<?php echo base64_encode($most_like_answer[0]['comment_id']); ?>','<?php echo $LikeFlag; ?>')" <?php echo $bg_color; ?>><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label." (".$most_like_answer[0]['total_likes'].")"; ?></p>
				</span>&nbsp;&nbsp;&nbsp;&nbsp;
				
				<span id="reply_comment_btn_outer_<?php echo $most_like_answer[0]['comment_id']; ?>">
					<p class="comment_like_btn" onclick="window.location.href='<?php echo site_url('questions_answers_forum/QaDetails/'.base64_encode($res['q_id']).'/1'); ?>'"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply <?php echo '('.$replyCount.')' ?></p>
				</span>

			
			</div>
			<div class="clearfix"></div>
			<div id='comment_reply_<?php echo $most_like_answer[0]['comment_id']; ?>' class="d-none">
				<div class='comment_block_common_reply'>
					<div class="row">
						<div class="col-md-10">
							<input type="text" class="form-control comment_reply_input" name="send_reply_comment" id="send_reply_comment_<?php echo $most_like_answer[0]['comment_id']; ?>" data-id="<?php echo $most_like_answer[0]['comment_id']; ?>" placeholder="Add reply to comment / answer" onkeyup="check_reply_comment_validation('<?php echo $most_like_answer[0]['comment_id']; ?>')">
							<span id="reply_comment_error_<?php echo $most_like_answer[0]['comment_id']; ?>" class='error'></span>
						</div>
						<div class="col-md-2">
							<button type="button" class="customBtnBlog" onclick="post_comment_reply('<?php echo $most_like_answer[0]['comment_id']; ?>')">Reply</button>
						</div>
					</div>
				</div>
			</div>

					
			</div>
			<?php } ?>
			
		                    
		</div> 
	</div>