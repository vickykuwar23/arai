<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>

<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Search</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Search</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php //$this->load->view('front/webinar/inc_technology_wall_tabs'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane fade tab-pane active show fade pt-0" id="Blogs">
			<div class="container">	
				<form action="" method="POST">
					<input type="text" name="search_keyword">
					<input type="submit">
				</form>
				<div class="title-bar" id="FeaturedBlogSliderOuterTitle">
					<h2>Search Results</h2>
					<div class="heading-border"></div>
				</div>
				<div id="SearchOuter" class="mt-2"></div>
				<div>
					<input type="hidden" name="posted_keyword" id="posted_keyword" value="<?php echo $posted_keyword; ?>">
				</div>
			</div>			
		</div>
	</div>
</section>

<div id="flotingButton2" class="d-none">
	<a href="<?php echo base_url('technology_transfer/add'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post a Technology for Transfer</a>
</div>

<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />


<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php $this->load->view('front/layouts/site_popups'); ?>


<script>
	//START : FEATURED & NON-FEATURED BLOG DATA
	getSearchDataAjax(1,20,0);
	function getSearchDataAjax(start,limit,is_show_more)
	{
		$("#showMoreBtn").remove();
		var posted_keyword = $("#posted_keyword").val();	
		if (posted_keyword!='') {
				parameters= { 'start':start, 'limit':limit, 'keyword':posted_keyword,'is_show_more':is_show_more}

				$.ajax( 
							{
								type: "POST",
								url: "<?php echo site_url('search/get_search_data_ajax'); ?>",
								data: parameters,
								cache: false,
						    dataType: "html",
								success:function(html)
								{
						
										$("#SearchOuter").append(html);
								}
							});
		}
	}
	//END : FEATURED & NON-FEATURED BLOG DATA
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	


</script>