<div class="row">
   <div class="col-md-10">
      <?php foreach ($nonfeatured_feeds as $key => $feed) { ?>
      <?php if ($feed['feeds_module_id']==1 ){ ?>
      <div class="resourceSharing mt-4">
         <h4 class="mt-3">Challange</h4>
         <div class="row resourceSharingListNew">
            <div class="col-md-12">
               <strong>
                  <a target="_blank" title="<?php echo ucfirst($feed['challenge_title']); ?> " href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>">
                  <?php if(strlen($feed['challenge_title']) > 70 ){ echo ucfirst(substr($feed['challenge_title'], 0,70) )."..."; } else {echo ucfirst($feed['challenge_title']);} ?>
                  </a>
               </strong>
               <p class="d-none">Challenge Brief : <?php 
               $read_more_url=base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               if(strlen($feed['challenge_details']) > 250 ){ echo ucfirst(substr($feed['challenge_details'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['challenge_details']);} ?>
            </div>
            <a class="d-none" href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==2){ ?>
      <div class="resourceSharing mt-4">
         <h4 class="mt-3">Expert</h4>
         <div class="row resourceSharingListNew">
            <div class="col-md-12">
               <strong><?php $name =  $feed['title']." ".$feed['first_name']." ".$feed['last_name'] ?>
               <a target="_blank" title="<?php echo ucfirst($name); ?>" href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>"><?php if(strlen($name) > 70 ){ echo ucfirst(substr($name, 0,70) )."..."; } else {echo ucfirst($name);} ?></strong>
                  
               </h4>
               <?php if ($feed['bio_data']!='') { ?>
               <p class="d-none"><?php 
                $read_more_url=base_url('home/viewProfile/').base64_encode($feed['user_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';
               if(strlen($feed['bio_data']) > 250 ){ echo ucfirst(substr($feed['bio_data'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['bio_data']);} ?>
                  <?php } ?>
               
            </div>
            <a class="d-none" href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==3 ) { 
      $getTotalLikes_res = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $feed['r_id']), 'like_id'); 
      ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Resource Sharing</h4>
         <div class="row resourceSharingListNew">
            <div class="col-md-9">
               <strong>
                  <a target="_blank" title="<?php echo ucfirst($feed['resouce_name']); ?>" href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>"><?php if(strlen($feed['resouce_name']) > 70 ){ echo ucfirst(substr($feed['resouce_name'], 0,70) )."..."; } else {echo ucfirst($feed['resouce_name']);} ?></a>
                  
               </strong>
               <p class="d-none">
                  <?php 
                  $read_more_url=base_url('resource_sharing/details/').base64_encode($feed['r_id']) ;
                  $read_more='<a href='.$read_more_url.' >read more</a>';

                  if(strlen($feed['resource_details']) > 250 ){ echo ucfirst(substr($feed['resource_details'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['resource_details']);} ?>
                  <!-- <a href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>" class="link">Read More</a>  -->
               </p>
               
            </div>
            <a class="d-none" href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==4  ) { ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Webinar</h4>
         <div class="row resourceSharingListNew">
              
            <div class="col-md-9">
               <strong>
                  <a href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>" title="<?php echo ucfirst($feed['webinar_name']); ?>"><?php if(strlen($feed['webinar_name']) > 70 ){ echo ucfirst(substr($feed['webinar_name'], 0,70) )."..."; } else {echo ucfirst($feed['webinar_name']);} ?></a>
                  
               </strong>

               <p class="d-none"><strong>Date:</strong> <?php echo date('jS F Y', strtotime($feed['webinar_date'])); ?></p>
               <p class="d-none"><strong>Time:</strong> <?php 
               $webinar_start_time        = $feed['webinar_start_time'];
               $webinar_end_time       = $feed['webinar_end_time'];
               $dispTime = "<span style='white-space:nowrap;'>".date('h:i A', strtotime($webinar_start_time))." - ".date('h:i A', strtotime($webinar_end_time))."</span>";
               echo $dispTime; 
               ?></p>

              
            </div>
            <a class="d-none" href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==5) { 
    
      ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Knowledge Repository</h4>
         <div class="row resourceSharingListNew">
          
            <div class="col-md-9">
               <strong>
                  <a target="_blank" title="<?php echo ucfirst($feed['title_of_the_content']); ?>" href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>"><?php if(strlen($feed['title_of_the_content']) > 70 ){ echo ucfirst(substr($feed['title_of_the_content'], 0,70) )."..."; } else {echo ucfirst($feed['title_of_the_content']);} ?></a>
                  
               </strong>
               <p class="d-none">
                  <strong>Auther Name: </strong>
                  <?php if(strlen($feed['author_name']) > 250 ){ echo ucfirst(substr($feed['author_name'], 0,250) )."..."; } else {echo ucfirst($feed['author_name']);} ?>
               </p>
               <p class="d-none">
                  <strong>Posted On: </strong>
                  <?php echo date("jS F Y", strtotime($feed['created_on'])); ?>
               </p>
               
            </div>
            <a class="d-none" href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>"   class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==6 ) { 
      ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Blog</h4>
         <div class="row resourceSharingListNew">
            
            <div class="col-md-9">
               <strong>
                  <a target="_blank" title="<?php echo ucfirst($feed['blog_title']);  ?>" href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>"><?php if(strlen($feed['blog_title']) > 70 ){ echo ucfirst(substr($feed['blog_title'], 0,70) )."..."; } else {echo ucfirst($feed['blog_title']);} ?></a>
                  
               </strong>
               <p class="d-none">
                  <?php 
                  $read_more_url=base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ;
                  $read_more='<a href='.$read_more_url.' >read more</a>';

                  $desc=strip_tags(html_entity_decode($feed['blog_description'])); 
                  if(strlen($desc) > 250 ){ echo ucfirst(substr($desc, 0,250) )."...".$read_more; } else {echo ucfirst($desc);}  ?>
               </p>
               <strong class="d-none">Date: </strong class="d-none"><?php // echo date('jS F Y', strtotime($feed['created_on'])); ?>
               <br>
               <strong class="d-none">Time: </strong class="d-none"><?php // echo date('h:i A', strtotime($feed['created_on'])); ?>
            </div>
            <a class="d-none" href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==7 ) { 
         $getTotalLikes_feed = $this->master_model->getRecordCount('arai_admin_update_likes', array('feed_id' => $feed['feed_id']), 'like_id');
      ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Technovuus Feeds</h4>
         <div class="row resourceSharingListNew">
            <div class="col-md-9">
               <strong>
              
               
               <a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>" title="<?php echo ucfirst($feed['feed_title']);  ?>"><?php if(strlen($feed['feed_title']) > 70 ){ echo ucfirst(substr($feed['feed_title'], 0,70) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a>
                </strong>
               <p class="d-none"><?php 
               $read_more_url=base_url('feeds/details/').base64_encode($feed['feed_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               $feed_desc=strip_tags(html_entity_decode($feed['feed_description'])); 
                  if(strlen($feed_desc) > 250 ){ echo ucfirst(substr($feed_desc, 0,250) )."...".$read_more; } else {echo ucfirst($feed_desc);} 
                  ?>
               </p>
            </div>
            <a class="d-none" href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>

     <?php }if ($feed['feeds_module_id']==8 ) { ?>
      <div class="resourceSharing">
         <h4 class="mt-3">Technology Transfer</h4>
         <div class="row resourceSharingListNew">
            <div class="col-md-9">
               <strong>
                  <a target="_blank" title="<?php echo ucfirst($feed['technology_title']);  ?>" href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>"><?php if(strlen($feed['technology_title']) > 70 ){ echo ucfirst(substr($feed['technology_title'], 0,70) )."..."; } else {echo ucfirst($feed['technology_title']);} ?></a>
                  
               </strong>
               <p class="d-none"><?php 
               $read_more_url=base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               $tech_desc=strip_tags(html_entity_decode($feed['technology_abstract'])); 
                  if(strlen($tech_desc) > 250 ){ echo ucfirst(substr($tech_desc, 0,250) )."...".$read_more; } else {echo ucfirst($tech_desc);}  ?>
               </p>
            </div>
            <a class="d-none" href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>

      <?php  } ?>
      
      <?php } ?>   
      <?php if($new_start < $all_nonfeatured_feeds_count)
         { ?><br>
      <div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
         <a href="javascript:void(0)" class="click-more" onclick="getSearchDataAjax('<?php echo $new_start; ?>', '<?php echo $limit; ?>',1 )">Show More</a>       
      </div>
      <?php 
         } 
         
         ?>  
     
   </div>
</div>

  <?php if ($feeds_count ==0) { ?>
      <div class="col-md-12 text-center">
         <p style="font-weight:bold; text-align: center;"><br>No Records Available</p>
      </div>
      <?php } ?> 