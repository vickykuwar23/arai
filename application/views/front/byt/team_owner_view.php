<style>

	.file_logo {
	
	font-size: 42px;
	color: #FFC107;
	height: 50px;
	}
	
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	ul.file_attachment_outer_common {
	list-style: none;
	margin: 0;
	padding: 0;
	}
	
	ul.file_attachment_outer_common li:before { display:none; }
	
	ul.file_attachment_outer_common li {
	padding: 3px;
	margin: 0 5px 10px 0;
	width: 80px;
	height: 60px;
	border: 1px solid #ccc;
	border-radius: 5px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
	}
	
	ul.file_attachment_outer_common li a h4 {
	margin: 0;
	line-height: 55px;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	font-size: 15px !important;
	}
	
	ul.file_attachment_outer_common li a img
	{
	max-width:100%;
	max-height:100%;
	}
	.home-p.pages-head4 {	
	background-size: 100%; min-height: 100%;
	}
	
	.customDetailView
	{
	height: auto;
	margin: 0;
	padding: 10px 0 5px 0 !important;
	text-align:justify;
	}
	
	a.receivedApplicationCntOuter {
	position: absolute;
	z-index: 999;
	width: 30px;
	height: 30px;
	background: #e23751;
	color: #fff;
	font-weight: 600;
	font-size: 11px;
	line-height: 28px;
	text-align: center;
	border-radius: 50%;
	border: 1px solid #000;
	right: 90px;
	margin: 0;
	padding: 0;
}

#RemoveMemberModal .custom_modal_content, #UserDetailModal .modal_custom_content { padding:0 15px; }
	
	a.userImgLink { background: none !important; padding: 0; margin: 0 auto; }
	a.userImgLink img { opacity:0.9; }
	a.userImgLink img:hover { opacity:1; }
</style>
<?php if( count($team_slots) < 5 ){ ?> 
	<style>
	.owl-nav {
    display: none !important;
	}
	</style>
<?php } ?>


<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down" style="background: url(<?php if($team_details[0]['team_banner']!=''){echo base_url('uploads/byt/').$team_details[0]['team_banner'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size: 100% 100% !important;">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Details</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Team Details</li>
			</ol>
		</nav>
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">						
						<?php if ($team_details[0]['team_banner']!='') { ?>
							<div class="col-md-4 d-none">
								<div class="img-box">
            			<img class="img-fluid"  src="<?php echo base_url('uploads/byt/').$team_details[0]['team_banner'] ?>" >
								</div>
							</div>
						<?php } ?>
						
						<div class="col-md-12">
							<div class="form-group">
								<p class="form-control customDetailView"><?php echo $team_details[0]['custom_team_id']." - " .$team_details[0]['team_name'] ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Team ID & Name</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<p class="form-control customDetailView"><?php echo $team_details[0]['challenge_id']." - " .$encrptopenssl->decrypt($team_details[0]['challenge_title'])."" ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Challenge ID & Name</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['brief_team_info']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Brief about Team</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['proposed_approach']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Proposed Approach</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['additional_information']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Additional Information</strong></label>
							</div>
						</div>
						
						<?php if(count($team_files_public) > 0) { ?>							
							<div class="col-md-12">
								<p ><strong>Public Files & Attachments :</strong></p>
								<ul class="file_attachment_outer_common">
									<?php 
										foreach ($team_files_public as $key => $file) 
										{ ?>
										<li>
											<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
												<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
												else { $disp_img_name = ''; } ?>
												
												<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
												else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
											</a>
										</li>
									<?php } ?>
								</ul>
								<br>
							</div>
						<?php } 
						
						if(count($team_files_private) > 0) { ?>							
							<div class="col-md-12">
								<p ><strong>Private Files & Attachments :</strong></p>
								<ul class="file_attachment_outer_common">
									<?php 
										foreach ($team_files_private as $key => $file) 
										{ ?>
										<li>
											<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
												<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
												else { $disp_img_name = ''; } ?>
												
												<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
												else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
											</a>
										</li>
									<?php } ?>
								</ul>
								<br>
							</div>
							
							<div class="col-md-12">
								<strong style="display: block;">Share Files</strong>
								<label class="switch">
									<input type="checkbox" id="share_files" <?php if($team_details[0]['share_files']=='yes'){echo 'checked';} ?>> 
									<span class="slider round" ></span>
								</label>
							</div>
						<?php } ?>          	
						
						<div class="col-md-12">
							<div id="owner-teamSlider" class="owl-carousel mt-4 owl-theme">			
								<?php $i=0;
									foreach ($team_slots as $key => $slot) 
									{ 
										$this->db->select('sa.app_id, sa.apply_user_id, r.user_id, r.title, r.first_name, r.last_name, r.user_category_id');
										$this->db->join('arai_registration r','r.user_id = sa.apply_user_id','INNER', false);
										$aproved_member = $this->master_model->getRecords("arai_byt_slot_applications sa",array('sa.slot_id'=>$slot['slot_id'],'sa.status'=>'Approved'));
										//echo $this->db->last_query(); die;
										
										$DispUserName = '-';
										$skills = array();
										$skillTitle = 'Skills';
										$slot_final_flag = $receivedApplicationCnt = 0;
										$profile_img = '<img src="'.base_url('assets/profile_picture/user_dummy.png').'" alt="experts">';
										if(count($aproved_member) > 0) 
										{
											$DispUserName = $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
											$slot_final_flag = 1;
											
											if($aproved_member[0]['user_category_id'] == 1)
											{
												$this->db->select('profile_picture, skill_sets, other_skill_sets');
												$profileImgdata = $this->master_model->getRecords("student_profile",array('user_id'=>$aproved_member[0]['apply_user_id']));
												
												if (isset($profileImgdata[0]['profile_picture']) &&  $profileImgdata[0]['profile_picture'] !='' ) 
												{ 
													$profile_img = '<img src="'.base_url('assets/profile_picture/'.$profileImgdata[0]['profile_picture']).'" alt="" class="img-fluid bor">';
												}
												
												if($profileImgdata[0]['skill_sets'] != "")
												{
													$skill_set_ids = $encrptopenssl->decrypt($profileImgdata[0]['skill_sets']);
													
													if($skill_set_ids != "")
													{
														$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
														$skills = $this->master_model->getRecords("arai_skill_sets");														
													}
												}
												
												if($profileImgdata[0]['other_skill_sets'] != "") 
												{ 
													$skills[]['name'] = $profileImgdata[0]['other_skill_sets']; 
												}												
											}
											else
											{
												$this->db->select('org_logo, org_sector, org_sector_other');
												$profileImgdata = $this->master_model->getRecords("arai_profile_organization",array('user_id'=>$aproved_member[0]['apply_user_id']));
												
												if(isset($profileImgdata[0]['org_logo']) &&  $profileImgdata[0]['org_logo'] !='' ) 
												{ 
													$profile_img = '<img src="'.base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profileImgdata[0]['org_logo'])).'" alt="" class="img-fluid bor">';
												}
												
												$skillTitle = 'Sector';
												if($profileImgdata[0]['org_sector'] != "")
												{
													$skill_set_ids = $profileImgdata[0]['org_sector'];
													
													if($skill_set_ids != "")
													{
														$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
														$skills = $this->master_model->getRecords("arai_organization_sector");														
													}
												}
												
												if($profileImgdata[0]['org_sector_other'] != "") 
												{ 
													$skills[]['name'] = $encrptopenssl->decrypt($profileImgdata[0]['org_sector_other']); 
												} //echo "<pre>"; print_r($skills); echo "</pre>";
											}
										}
										else
										{
											//$this->db->where("(status = 'Rejected' OR status = 'Pending')");
											$receivedApplicationCnt = $this->master_model->getRecordCount( "arai_byt_slot_applications",array("slot_id"=>$slot['slot_id'], "is_deleted"=>'0', "status"=>'Pending'));
											//echo $this->db->last_query(); exit;
										
										$skill=$slot['skills'];	
											$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slot['user_id']."')",NULL, false);
											$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
										}	 ?>
									
									<div class="owner-teammember boxSection25 text-center">
										<?php if($receivedApplicationCnt > 0) { echo "<a class='receivedApplicationCntOuter' href=".site_url('myteams/goHiring/'.base64_encode($slot['team_id'])."/".base64_encode($slot['slot_id'])).">".$receivedApplicationCnt."</a>"; } ?>
										
										<?php if ($DispUserName != '-') { ?> <a class="userImgLink" href="javascript:void(0)" onclick="get_user_details('<?php echo base64_encode($aproved_member[0]['apply_user_id']); ?>')"> <?php }
											
											echo $profile_img; 
											
											if ($DispUserName != '-') { echo "</a>"; }
										?> 
											 
											<h3><?php echo $DispUserName; ?></h3>
											
											<h3><?php echo $slot['name']; ?></h3>
											<p><strong><?php echo $skillTitle; ?></strong></p>
											<div class="boderBox newheight mb-4" style="text-align:left;">
												<?php 
													foreach ($skills as $key => $value) { ?>													
													<span class="badge badge-pill badge-info"><?php if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { echo $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { echo $value['name']; } } ?></span>
													<?php }  ?>
											</div>
											<div class="form-group mt-4">
												<p class="form-control customDetailView" style="height:65px"><?php echo $slot['role_name']; ?></p>
												<label class="form-control-placeholder floatinglabel" style="left:0px;"><strong>Role in Team</strong></label>
											</div>
											
											<div class="text-center">
												<?php 
													if($i > 0)
													{
														if($slot_final_flag == 1)
														{	?>
														<a href="javascript:void(0)" class="btn-sm" onclick="remove_slot_member('<?php echo base64_encode($aproved_member[0]['app_id']); ?>', '<?php echo $DispUserName; ?>')">Remove Member</a>
														<a href="javascript:void(0)" class="d-none">Chat</a>
														<div class="border_top_bottom2 d-none">
															<a href="javascript:void(0)">View Details</a>
														</div>
														<?php }
														else if($receivedApplicationCnt > 0)
														{	?>
														<a href="<?php echo site_url('myteams/goHiring/'.base64_encode($slot['team_id'])."/".base64_encode($slot['slot_id'])); ?>" class="btn-sm">View Applications</a>
														<?php }
														else
														{	?>
															<a href="<?php echo site_url('myteams/sendInvitation/'.base64_encode($slot['team_id'])."/".base64_encode($slot['slot_id'])); ?>" class="btn-sm">Go Hunting</a>
											<?php		}
													}	?>
											</div>
									</div>
									
									<?php $i++;
									} ?>
									
									
							</div>
						</div>
						
						<?php 
						$total_slots_count=count($team_slots);

						$total_approved_members_arr = $this->master_model->getRecords("arai_byt_slot_applications sa",array('sa.team_id'=>$team_details[0]['team_id'],'sa.status'=>'Approved'));
						 $total_approved_members = count($total_approved_members_arr);



						?>
						<div class="col-md-12 mt-4">
							<strong style="display: block;">Is Your Team Complete?</strong>
							<label class="switch">
								<input type="checkbox" id="is_team_complete" <?php if($team_details[0]['team_status']=='Completed'){echo 'checked';} ?>> 

							
								<span class="slider round"></span>
							</label>
								<input type="hidden" id="total_slots_count" value="<?php echo $total_slots_count ?>">
								<input type="hidden" id="total_approved_members" value="<?php echo $total_approved_members ?>">
						</div>
						
						<div class="col-md-12">
							<div class="team_view_listing2 mt-4">
								<?php if($team_details[0]['apply_status'] == "Withdrawn" || $team_details[0]['challenge_owner_status'] == "Approved" || $team_details[0]['challenge_owner_status'] == "Rejected" || $team_details[0]['challenge_status'] == "Pending" || $team_details[0]['challenge_status'] == "Rejected" || $team_details[0]['challenge_status'] == "Withdrawn" || $team_details[0]['challenge_status'] == "Closed" ) {  }
								else { ?>
								<a href="<?php echo base_url('team/create/').base64_encode($team_details[0]['c_id'])."/".base64_encode($team_details[0]['team_id']); ?>" class="pull-left" style="margin-right: 10px;">Edit Team </a>
								<?php } ?>
								
								<?php if($team_details[0]['apply_status']=='Pending' || ($team_details[0]['apply_status']=='Withdrawn' && $team_details[0]['challenge_owner_status']=='Pending')){ /* $team_details[0]['apply_status']=='Withdrawn' ||  */ ?>									
									<a href="javascript:void(0)" id="apply_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="pull-left chatButton">
										Apply for the Challenge
									</a>
									
									<!-- 	<a href="javascript:void(0)" class="pull-left chatButton" onclick="coming_soon_popup()">
										Apply For A Challenge
									</a> -->
								<?php } ?>
								
								
								<?php if( $team_details[0]['apply_status']=='Applied' || ($team_details[0]['challenge_owner_status']=='Approved' && $team_details[0]['apply_status']=='Applied' )){ ?>
									<a href="javascript:void(0)" id="withdraw_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="pull-left chatButton mobileTop">
										Withdraw Team from Challenge
									</a>
								<?php } ?>
								
                <!-- <a href="#" class="pull-left chatButton">Apply for Challenge</a> -->
							</div>
						</div>
						<!-- <div class="col-md-12 mt-4">
							<div class="chatBg">
							<div class="form-group mt-3">
							<textarea class="form-control" id="exampleFormControlTextarea1" name="contents"></textarea>
							<label class="form-control-placeholder" for="exampleFormControlTextarea1">Chat Description </label>
							</div>
							</div>
						</div> -->
						<!-- <div class="col-md-12 mt-4">
							<div class="chatBg2">
							<div class="form-group mt-3">
							<textarea class="form-control" id="exampleFormControlTextarea1" name="contents"></textarea>
							<label class="form-control-placeholder" for="exampleFormControlTextarea1"> My Chat Description </label>
							</div>
							<button type="submit" class="btn btn-primary">Send</button>
							</div>
						</div> -->
						<div class="col-md-12 mt-4">
							<strong>Team Discussion</strong>
							<div class="row">
								<div class="col-12 chatScrollbar">
									<div id="chats">
										
									</div>
									<?php
									$u_id = $this->session->userdata('user_id');

								
								$this->db->select('title,first_name,last_name,profile_picture,org_logo');						
								$this->db->join('profile_organization','profile_organization.user_id=registration.user_id','LEFT');	
								$this->db->join('student_profile','student_profile.user_id=registration.user_id','LEFT');			
								$user_data= $this->master_model->getRecords('registration',
								array('registration.user_id'=>$u_id ));

									if ($this->session->userdata('user_category') == 1 &&  count($user_data) && $user_data[0]['profile_picture']!='') {
											$pro_pic = base_url('assets/profile_picture/'.$user_data[0]['profile_picture']);
									
									}
										elseif ($this->session->userdata('user_category') == 2 && count($user_data) && $user_data[0]['org_logo']!='') {
											$pro_pic = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($user_data[0]['org_logo']));

									}else{
										$pro_pic = base_url('assets/profile_picture/user_dummy.png');
									} 
									
									 ?>
								<div class="comments">
										<div class="comment-box">
											<span class="commenter-pic">
												<img src="<?php echo $pro_pic ?>" class="img-fluid">
											</span>
											<span class="commenter-name">
												<a href="javascript:void(0)"><?php echo $encrptopenssl->decrypt($user_data[0]['title'])." ".$encrptopenssl->decrypt($user_data[0]['first_name'])." ".$encrptopenssl->decrypt($user_data[0]['last_name']) ?></a> 
											</span>       
											<div class="form-group mt-3">
												<textarea class="form-control" id="msg_content"  name="contents"></textarea>
											</div>
											<button  type="button" id="send_msg" class="btn btn-primary pull-right">Send</button>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="RemoveMemberModal" tabindex="-1" role="dialog" aria-labelledby="RemoveMemberModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_content_outer">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="RemoveMemberModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			
			<div class="custom_modal_content">
				<div class="go_hiring_inner">
					<p class="byt_terms_outer">Please select the reason for removal</p>
				</div>
				
				<form method="post" action="<?php echo site_url('myteams/remove_slot_member_ajax/'.base64_encode($team_id)); ?>" id="RemoveMemberForm" enctype="multipart/form-data">
					<input type="hidden" name="app_id" id="app_id" value="">
					<div style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<div class="form-group">
							<select class="form-control" name="remove_reason" id="remove_reason" data-placeholder="Select Type *" required style="color:#495057 !important;" onchange="check_reason_other()">
								<option value="">Select Reason *</option>
								<?php
									if(count($member_remove_reason_data) > 0)
									{ 
										foreach ($member_remove_reason_data as $key => $res) 
										{ ?>
											<option value="<?php echo $res['id'] ?>"><?php echo $res['reason'] ?></option>
							<?php } 
									} ?>
							</select>
						</div>
						
						<div class="form-group" id="other_reason_outer" style="display:none; margin-top:35px;">
							<textarea class="form-control" name="other_reason" id="other_reason"></textarea>
							<label class="form-control-placeholder floatinglabel" for="other_reason" style="display:block">Other Reason <em>*</em></label>
						</div>
					</div>
					<div class="modal-footer p-2">
						<input type="submit" class="btn btn-primary" value="Submit">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

	<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
			<div class="modal-content" id="UserDetailModalContentOuter">			
			</div>
		</div>
	</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script>
	function coming_soon_popup() 
	{ 
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
	}
	
	function remove_slot_member(app_id, dispName)
	{
		$("#app_id").val(app_id);
		$("#RemoveMemberModalLabel").html(dispName);
		$("#RemoveMemberModal").modal('show');
		/* swal(
		{
			title:"Confirm",
			text: "Are you sure you want to remove this member?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!',
			html:''
		}).then(function (result) 
		{
			if (result.value) 
			{
				var parameters = { "app_id":app_id }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('myteams/remove_slot_member_ajax') ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						//if(data.flag == "success"){ }else {	}
						location.reload(); 
					}
				});
			}				
		}); */
	}
	
	function check_reason_other()
	{
		var selectedReason = $( "#remove_reason option:selected" ).text().toLowerCase();
		if(selectedReason == 'other')
		{
			$("#other_reason").attr('required', true);
			$("#other_reason_outer").show();
		}
		else
		{
			$("#other_reason").attr('required', false);
			$("#other_reason").val("");
			$("#other_reason_outer").hide();
		}
		$("#RemoveMemberForm").validate();
	}
	
	$(document).ready(function() {
		
		$('#share_files').change(function(){
			var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
			var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
			
			var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
			
			var share_files 
			if ($('#share_files:checked').val()=='on') {
				share_files = 'yes';
				}else{
				share_files = 'no';
			}
			
			var datastring='share_files='+ share_files + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
			$.ajax({
				type: 'POST',
				data:datastring,
				url: "<?php echo base_url();?>myteams/share_files",
				beforeSend: function(){
					$('#preloader-loader').css("display", "block");
				},
				success: function(res){ 
					// var json = $.parseJSON(res);
					// $('#subcategory').html(json.str);
					//csrfName=json.name;
					//csrfHash=json.value;
				},
				complete: function(){
					$('#preloader-loader').css("display", "none");
				},
				
			});
			
			
			
		})
		
		$('#is_team_complete').change(function(){
			var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
			var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
			var total_slots_count = $("#total_slots_count").val();
			var total_approved_members = $("#total_approved_members").val();
			if (total_slots_count!=total_approved_members) {
				 $('#is_team_complete').prop('checked', function() {return this.defaultChecked;});
				swal( 'Warning!','Please Fill/Remove vacant slots to mark your team as complete','warning');
				return false;
			}
			
			var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
			
			var team_complete;
			var swal_text;
			if ($('#is_team_complete:checked').val()=='on') {
				team_complete = 'Completed';
				swal_text = 'complete';
				}else{
				team_complete = 'Incomplete';
				swal_text = 'incomplete';
			}
			
			var datastring='team_complete='+ team_complete + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
			
			 swal({
                    title:'Confirm' ,
                    text: 'Are you sure you want to mark your team as '+swal_text+'?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                    }).then(function (result) {
                    if (result.value) {
                      $.ajax({
						type: 'POST',
						data:datastring,
						url: "<?php echo base_url();?>myteams/is_team_complete",
						beforeSend: function(){
							$('#preloader-loader').css("display", "block");
						},
						success: function(res){ 
							// var json = $.parseJSON(res);
							// $('#subcategory').html(json.str);
							//csrfName=json.name;
							//csrfHash=json.value;
						},
						complete: function(){
							$('#preloader-loader').css("display", "none");
						},
						
						});

                    }else{

                    $('#is_team_complete').prop('checked', function() {return this.defaultChecked;});
                    }
            }); 

		})
		
		
		
		
		
		$('#apply_for_challenge').click(function(){
			
			swal(
			{
				title:"Confirm?" ,
				text: "Are you sure you want to apply for this challenge?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{
				if (result.value) 
				{
					
					
					var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
					var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
					
					var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
					
		      
					
					var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
					$.ajax({
						type: 'POST',
						data:datastring,
						url: "<?php echo base_url();?>myteams/apply_for_challenge",
						beforeSend: function(){
							$('#preloader-loader').css("display", "block");
						},
						success: function(res){ 
							var data = $.parseJSON(res);
							
							if(data.success==true){
								
								swal({
									title: "Success!",
									text: "Application Submitted Successfully!",
									type: "success"
									}).then(function() {
									//location.reload(); 
									location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
								});
								
								}else{
								swal( 'Error!','Something went wrong!','error');
							}
						},
						complete: function(){
							$('#preloader-loader').css("display", "none");
						},
						
					});
					
				}
			} ) 	
			
		})
		
		
		$('#withdraw_for_challenge').click(function(){

			var ch_owner_approval =  '<?php echo $team_details[0]['challenge_owner_status'] ;?>';
			var msg;
			if (ch_owner_approval = 'Approved') {

				msg = 'You will not be able to apply to this challenge again.'
			}else{
				msg = 'Are you sure you want to withdraw your team?';
			}
			
			swal(
			{
				title:"Confirm?" ,
				text: msg,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{
				if (result.value) 
				{
					
					
					var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
					var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
					
					var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
					
		      
					
					var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
					$.ajax({
						type: 'POST',
						data:datastring,
						url: "<?php echo base_url();?>myteams/withdraw_for_challenge",
						beforeSend: function(){
							$('#preloader-loader').css("display", "block");
						},
						success: function(res){ 
							var data = $.parseJSON(res);
							
							if(data.success==true){
								
								swal({
									title: "Success!",
									text: "Application Withdrawn Successfully!",
									type: "success"
									}).then(function() {
									//location.reload(); 
									location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
								});
								
								}else{
								swal( 'Error!','Something went wrong!','error');
							}
						},
						complete: function(){
							$('#preloader-loader').css("display", "none");
						},
						
					});
					
				}
			} ) 	
			
		})
		
		
		
	})
</script>


<script>
	$(document).ready(function(){
 		setInterval(
 			get_chats_interval,50000);
	});
	
	$(window).on('load', function () {
	   	get_chats();
 	});


 	function get_chats_interval(){
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			async:true,
			url: "<?php echo base_url();?>team_discussion/get_chats",
		
			success: function(res){ 

				$("#chats").html(res);
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
		
			
		});


    }
 	}

 	function get_chats(){
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			url: "<?php echo base_url();?>team_discussion/get_chats",
			beforeSend: function(){
				$('#preloader-loader').css("display", "block");
			},
			success: function(res){ 

				$("#chats").html(res);
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
			complete: function(){
				$('#preloader-loader').css("display", "none");
			},
			
		});


    }
 	}


	$('#send_msg').on('click', function () {

		
	$("#msg_content").css('border-color','#888')
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
      var  msg_content = $("#msg_content").val();
      if (msg_content=='') {$("#msg_content").css('border-color','red'); return false;}
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='msg_content='+msg_content+'&team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			url: "<?php echo base_url();?>team_discussion/insert_chat",
			beforeSend: function(){
				$('#preloader-loader').css("display", "block");
			},
			success: function(res){ 
				$("#msg_content").val('');
				// $("#chats").html(res);
				get_chats();
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
			complete: function(){
				$('#preloader-loader').css("display", "none");
			},
			
		});


    }
   
 });	
 
 //******* JQUERY VALIDATION *********
	$("#RemoveMemberForm").validate( 
	{
		rules: { remove_reason: { required: true } },
		messages: { remove_reason: { required: "Please select the reason" }, other_reason: { required: "Please enter the other reason" }, },
		errorElement: 'span',
		errorPlacement: function (error, element) 
		{
			if(element.hasClass('select2_common') && element.next('.select2-container').length) 
			{
				error.insertAfter(element.parent());
			}
			else { element.closest('.form-group').append(error);  }
		},
		highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
		unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); },
		invalidHandler: function(e,validator) 
		{
			//validator.errorList contains an array of objects, where each object has properties "element" and "message".  element is the actual HTML Input.
			for (var i=0;i<validator.errorList.length;i++){
				console.log(validator.errorList[i]);
			}
			//validator.errorMap is an object mapping input names -> error messages
			for (var i in validator.errorMap) {
				console.log(i, ":", validator.errorMap[i]);
			}
		},
		submitHandler: function(form) 
		{ 
			swal(
			{
				title:"Confirm",
				text: "Are you sure you want to remove this member? Once a member is removed, they will not be able to join your team again",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!',
				html:''
			}).then(function (result) 
			{
				if (result.value) 
				{
					$("#RemoveMemberModal").modal('hide');
					form.submit();		
				}
				
			});
		}
	});
   
		//ON CLICK ON USER IMAGE, OPEN USER DETAIL POP UP
		function get_user_details(user_id)
		{
			var cs_t = 	$('.token').val();
			parameters= { 'user_id':user_id, 'cs_t':cs_t }
			$("#preloader-loader").show();
			$.ajax(
			{
				type: "POST",
				url: "<?php echo site_url('myteams/CommonUserDetailsAjax'); ?>",
				data: parameters,
				cache: false,
				dataType: 'JSON',
				success:function(data)
				{
					if(data.flag == "success")
					{
						$(".token").val(data.csrf_new_token)					
						$("#UserDetailModalContentOuter").html(data.response)
						$("#UserDetailModal").modal('show')
						$("#preloader-loader").hide();
					}
					else 
					{ 
						$("#preloader-loader").hide();
						sweet_alert_error("Error Occurred. Please try again."); 
					}
				}
			});	
		}
	

</script>














