<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<!--style>
	.byt_member_blocks { border: 1px solid rgba(0,0,0,0.09); overflow: hidden; background: rgba(0,0,0,0.03); padding: 15px 15px; min-height:320px; }
	.byt_member_blocks .byt_member_img { height: 100px; width:100px; border-radius:50%; margin: 0px auto 15px; display: block; border: 1px solid #ccc; overflow:hidden; }
	.byt_member_blocks img { max-width: 100%; max-height: 100px; }
	.byt_member_blocks .byt_member_name { text-align: center; margin: 0 0 8px 0; min-height: 24px; }
	
	.byt_member_blocks .byt_member_skill_set { /* border: 1px solid #ccc !important; min-height:150px; background:#fff; */ margin:0; }  
	.byt_member_blocks .byt_member_roles { /* border: 1px solid #ccc !important;  min-height:100px; background:#fff; */ margin-top:28px; }
  .byt_member_blocks .select2 { max-width:100%; width:100% !important; }
	/* .byt_member_blocks .select2-container--disabled .select2-selection--multiple { background-color: #fff; } */
	
	.byt_member_blocks .select2-container--default .select2-selection--multiple { /* border: none !important; */ font-size:14px; margin:0; padding:5px; }
	.byt_member_blocks_outer_cls { position: relative; }
	.byt_member_blocks_outer_cls .remove_slot_button { position: absolute; right: 0; bottom: 0; }
	
	
	
	ul.existing_byt_files_listing { list-style: none; margin: 0; padding: 0; }
	ul.existing_byt_files_listing li { padding: 0; display: inline-block; margin: 0 5px 10px; }
	ul.existing_byt_files_listing li:before { display:none; }
	
	ul.existing_byt_files_listing li .previous_img_outer a.img_outer { width: 120px; height: 120px; border: 3px solid #ccc; padding: 2px; background: #fff; border-radius: 5px; display: inline-block; line-height: 100px; vertical-align: middle; }
	ul.existing_byt_files_listing li .previous_img_outer a.img_outer:hover { text-decoration:none; }
	ul.existing_byt_files_listing li .previous_img_outer a.img_outer img { max-width: 100%; max-height: 100%; border: none; padding: 0; background: #fff; border-radius:0; }
	ul.existing_byt_files_listing li .previous_img_outer a.img_outer h4, ul.existing_byt_files_listing li .previous_img_outer a.img_outer h4:hover { font-size: 20px !important; text-transform: uppercase; margin: 0; padding: 0; text-align: center; line-height: 100px; text-decoration: none; font-weight: 600; }
</style-->

<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
</style>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Apply For Challenge</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Apply For Challenge </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					<?php //echo validation_errors(); ?>
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php }  ?>
					<?php if( $this->session->flashdata('error')){ ?>
						<div class="alert alert-error alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php }  ?> 
					
					
					<form method="POST" action="<?php echo site_url('team/create/'.base64_encode($challenge_id).'/'.$encoded_team_id); ?>" id="ApplyChallengeForm" name="ApplyChallengeForm" enctype="multipart/form-data">
						<div id="smartwizard_apply_challenge">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
								<!-- <li><a href="#step-4">Step 4<br /><small></small></a></li> -->
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									
									<div class="row">					 
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" value="<?php echo $disp_challenge_title; ?>" readonly disabled>
												<label class="form-control-placeholder floatinglabel">Challenge Name<em></em></label>
											</div>
										</div>	
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" value="<?php if($challenge_data[0]['challenge_id'] != "") { echo $challenge_data[0]['challenge_id']; } else { echo "--"; } ?>" readonly disabled>
												<label class="form-control-placeholder floatinglabel">Challenge Id<em></em></label>
											</div>
										</div>
										
										<div class="col-sm-12">
											<div class="form-group">
												<input type="text" class="form-control" name="team_name" id="team_name" value="<?php if($mode == 'Add') { echo set_value('team_name'); } else { echo $team_data[0]['team_name']; } ?>" required autofocus maxlength="250">
												<label class="form-control-placeholder floatinglabel" for="team_name">Name of Team <em>*</em></label>
												<span class="error"><?php echo form_error('team_name'); ?></span>
											</div>
										</div>
										
										<?php
										$dynamic_cls = 'd-none';
										$full_img = $onclick_fun = '';
										if($mode == 'Update' && $team_data[0]['team_banner'] != '' && file_exists('./uploads/byt/'.$team_data[0]['team_banner']))
										{	
											$dynamic_cls = '';
											$full_img = base_url().'uploads/byt/'.$team_data[0]['team_banner'];
											$onclick_fun = "remove_banner_img('1','".$encrypt_obj->encrypt('arai_byt_teams')."', '".$encrypt_obj->encrypt('team_id')."', '".$encrypt_obj->encrypt($team_data[0]['team_id'])."', 'team_banner', '".$encrypt_obj->encrypt('./uploads/byt/')."', 'Team Banner')";
										} ?>
									
										<div class="col-sm-12 <?php echo $dynamic_cls; ?>" id="team_banner_outer">
											<div class="form-group">
												<div class="previous_img_outer">
													<img src="<?php echo $full_img; ?>">
													<a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
										
										<div class="col-md-12">											
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Banner</button>	
												<input type="file" class="form-control" name="team_banner" id="team_banner" />
												<div class="clearfix"></div>
												<span class="small">Note : Please upload only image having size less than 2MB</span><div class="clearfix"></div>
												<?php if(form_error('team_banner')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo str_replace("filetype","file type",form_error('team_banner')); ?></span> <?php } ?>
												<?php if($team_banner_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo str_replace("filetype","file type",$team_banner_error); ?></span> <?php } ?>
											</div>
										</div>
										
										<?php /*<div class="col-md-12 d-none">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="team_size" id="team_size" value="<?php echo set_value('team_size'); ?>">
												<label class="form-control-placeholder floatinglabel" for="team_size">Team Size <em>*</em></label>
												<span class="error"><?php echo form_error('team_size'); ?></span>
											</div>
										</div>*/?>
										
										<div class="col-sm-12">
											<div class="form-group">
												<textarea class="form-control" style="padding:15px 0;" name="team_details" id="team_details"><?php if($mode == 'Add') { echo set_value('team_details'); } else { echo $team_data[0]['brief_team_info']; } ?></textarea>
												<label class="form-control-placeholder" for="team_details">Brief About Team <em>*</em></label>
												<span class="error"><?php echo form_error('team_details'); ?></span>
											</div>										
										</div>										
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<input type="hidden" name="byt_member_slot_cnt" id="byt_member_slot_cnt" value="<?php if($mode == 'Add') { if(set_value('byt_member_slot_cnt') != "") { echo set_value('byt_member_slot_cnt'); } else { echo $min_team; } } else { echo count($team_slot_data); } ?>">
									
									<div class="row myRow">										
										<div class="col-md-10 h-100">
											<div id="CreateTeamSlider" class="owl-carousel owl-theme">
												
												<?php 
												if($mode == 'Add') 
												{
													if(set_value('byt_member_slot_cnt') != "") { $min_team_limit = set_value('byt_member_slot_cnt'); }
													else { $min_team_limit = $min_team; }
												} else { $min_team_limit = count($team_slot_data); }
												
												$k=0;
												for ($i=0; $i < $min_team_limit ; $i++) 
												{  
													$disp_flag = 1;
													if($mode == 'Add')
													{
														if(set_value('byt_member_slot_cnt') != "") 
														{
															$mem_slot_type = set_value('team_type[team_type'.$i.']');
															if(!isset($mem_slot_type) || $mem_slot_type == '') { $disp_flag = 0; }
														}
													}
													
													if($disp_flag == 1)
													{	?>
														<div class="teammember byt_member_blocks_outer_cls" id="byt_member_blocks_outer<?php echo $i; ?>">
															<input type="hidden" name="team_slot_id[team_slot_id<?php echo $i; ?>]" value="<?php if($mode == 'Add') { if(set_value('team_slot_id[team_slot_id'.$i.']') != "") { echo set_value('team_slot_id[team_slot_id'.$i.']'); } else { echo "0"; } } else { echo $team_slot_data[$i]['slot_id']; } ?>" class="form-control">
															
															<img src="<?php if($i == 0) { echo $disp_photo; } else { echo $default_disp_photo; } ?>" alt="experts">
															<h3>
																<?php if($i==0) { echo $encrypt_obj->decrypt($login_user_data['0']['title'])." ".$encrypt_obj->decrypt($login_user_data['0']['first_name'])." ".$encrypt_obj->decrypt($login_user_data['0']['last_name']); } else { echo "&nbsp;"; } ?>
															</h3>
															
															<div class="form-group mt-4">
																<select class="form-control cls_team_type" name="team_type[team_type<?php echo $i; ?>]" id="team_type<?php echo $i; ?>" data-placeholder="Select Type *" <?php if($mode == 'Update') { echo 'disabled'; } ?>>
																	<option value="">Select Type</option>
																	<?php
																		if(count($type_data) > 0)
																		{ 
																			foreach ($type_data as $key => $type) 
																			{ 																				
																				$team_type_sel = '';
																				if($mode == 'Add') 
																				{	
																					if(set_value('team_type[team_type'.$i.']') != "") { $team_type_sel = set_value('team_type[team_type'.$i.']'); }
																				}
																				else { $team_type_sel = $team_slot_data[$i]['slot_type']; }
																				?>
																				<option value="<?php echo $type['tid'] ?>" <?php if($type['tid'] == $team_type_sel) { echo 'selected'; } ?>><?php echo $type['name'] ?></option>
																<?php } 
																		} ?>
																</select>
															</div>
															
															<div class="boderBox">
																<div class="boderBox65">
																<select data-key="<?php echo $i ?>" class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset<?php echo $i; ?>][]" id="team_skillset<?php echo $i; ?>" data-placeholder="Select Skill Sets *" multiple >
																	<?php if(count($skill_sets_data) > 0)
																		{	
																			foreach($skill_sets_data as $skill_sets)
																			{	
																				if(strtolower($skill_sets['name']) != 'other' && strtolower($skill_sets['name']) != 'others')
																				{
																					if($mode == 'Add') 
																					{
																						if(set_value('team_skillset[team_skillset'.$i.'][]') != "") { $team_skillset_arr = set_value('team_skillset[team_skillset'.$i.'][]'); }
																						else { $team_skillset_arr = $disp_skill_sets; }
																					} else { $team_skillset_arr = explode(",",$team_slot_data[$i]['skills']); }?>
																					<option data-id='<?php echo $skill_sets['name'] ?>' value="<?php echo $skill_sets['id']; ?>" <?php if(in_array($skill_sets['id'],$team_skillset_arr)) { echo 'selected'; } ?> ><?php echo $skill_sets['name']; ?></option>
																	<?php }	
																			}
																		}	?>
																</select>
																	</div>

																<p style="margin:10px 0 0 0;font-size: 10px;line-height: 14px;text-align: justify;"><strong>Note:</strong> If the requisite field is not listed, type the required text and press #.</p>
															</div>
															
															<?php if($mode == 'Add') { $sel_other_skill = set_value('other_skill[other_skill'.$i.']'); } else { $sel_other_skill = $team_slot_data[$i]['other_skill']; }  ?>
															<div class="other_div <?php if($sel_other_skill == "") { echo 'd-none'; } ?>" id="other_skill_div<?php echo $i ?>">															
																<div class="form-group">
																	<input type="text" name="other_skill[other_skill<?php echo $i; ?>]" value="<?php if($sel_other_skill != "") { if($mode == 'Add') { echo set_value('other_skill[other_skill'.$i.']'); } else { echo $team_slot_data[$i]['other_skill']; }} ?>" id="other_skill<?php echo $i; ?>" class="form-control cls_other_skill" requiredxx="">
																	<label for="other_skill[other_skill<?php echo $i; ?>]" class="form-control-placeholder floatinglabel">Other Skillset  <em class="mandatory">*</em></label>
																</div>															
															</div>
															
															<div class="form-group mt-4">
																<input type="text" name="team_role[team_role<?php echo $i; ?>]" value="<?php if($mode == 'Add') { if(set_value('team_role[team_role'.$i.']') != "") { echo set_value('team_role[team_role'.$i.']'); } } else { echo $team_slot_data[$i]['role_name']; } ?>" id="team_role<?php echo $i; ?>" class="form-control cls_team_role">
																<label class="form-control-placeholder" for="cname">Role in Team <em>*</em></label>
																<div class="error" style="color:#F00"><?php echo form_error('team_role'); ?> </div>
															</div>
															
															<?php 
																$team_slot_id = 0;
																if($mode == 'Update') { $team_slot_id = $team_slot_data[$i]['slot_id']; } 
																
																if($i == 0)
																{	?>
																	<button type="button" name="remove" class="btn btn-primary w-100 mb-2" disabled>Team Admin</button>
													<?php }
																else
																{	
																	if($k >= $min_team) 
																	{	?>
																		<button type="button" name="remove" class="btn btn-primary w-100 mb-2" onclick="remove_slot('<?php echo $i; ?>', '<?php echo $encrypt_obj->encrypt($team_slot_id); ?>')">Remove</button>
														<?php }
																	else
																	{	?>
																		<button type="button" name="remove" class="btn btn-primary w-100 mb-2" disabled style="visibility:hidden">Remove</button>
														<?php	}
																} ?>															
															<!--p>Team Member 1</p-->
														</div>
												<?php $k++;
													}
												}	?>
											</div>
										</div>
										<div class="col-md-2 teammember d-flex align-items-center addSlot">
											<button class="add_slot_button" type="button" onclick="append_new_slot()"><span><i class="fa fa-plus-circle"></i></span>Add Slot</button>
										</div>
									</div>								
								</div>							
								
								<div id="step-3" class="mt-4">
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<textarea type="text"  style="padding:15px 0;"
 class="form-control" name="proposed_approach" id="proposed_approach"><?php if($mode == 'Add') { echo set_value('proposed_approach'); } else { echo $team_data[0]['proposed_approach']; } ?></textarea>
												<label class="form-control-placeholder floatinglabel" for="proposed_approach">Proposed approach for solving challenge <em></em></label>
												<span class="error"><?php echo form_error('proposed_approach'); ?></span>
											</div>											
										</div>
									</div>
									
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<textarea type="text" style="padding:15px 0;"
 class="form-control" name="additional_information" id="additional_information"><?php if($mode == 'Add') { echo set_value('additional_information'); } else { echo $team_data[0]['additional_information']; } ?></textarea>
												<label class="form-control-placeholder floatinglabel" for="additional_information">Additional Information <em></em></label>
												<span class="error"><?php echo form_error('additional_information'); ?></span>
											</div>											
										</div>
									</div>
									
									<div class="file-details">										
										<?php if($mode == 'Update' && count($team_files_data) > 0)
										{	
											echo '<div class="row">';
											foreach($team_files_data as $res)
											{	?>
												<div class="col-md-2" id="team_files_outer<?php echo $res['team_id']."_".$res['file_id']; ?>">
													<div class="file-list">
															<a href="javascript:void(0)" onclick="remove_team_file('<?php echo $res['team_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>')" class="file-close"><i class="fa fa-remove"></i></a>
															<a class="file_ext_title" href="<?php echo base_url().'uploads/byt/'.$res['file_name']; ?>" target="_blank">
																<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
																if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$res['file_name']; }
																else { $disp_img_name = ''; } ?>
																
																<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
																else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
																<!--span><i class="fa fa-file"></i></span> View-->
															</a>
													</div>
												</div>
								<?php	}
											echo '</div>';
										}	?>
										
										<input type="hidden" name="customFileCount" id="customFileCount" value="0">
										<div class="row">											
											<div id="last_team_file_id"></div>
											<div class="col-md-2 custom-file" id="addFileBtnOuter">
												<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
												<input type="file" class="form-control fileUploadTeams" name="team_files[]" id="file-upload" />
											</div>
										</div>
									</div>
									<!--p class="mt-3 mb-4">Multiple file additions possible (All File Formats with size limit = TBD)</p-->
								</div><br>
								
								<div id="step-4" class="mt-4 d-none">
									<input type="hidden" name="invite_row_cnt" id="invite_row_cnt" value="1">
									<div class="row" id="byt_invite_row_0">
										<div class="col-md-10">
											<div class="form-group">
												<input type="text" class="form-control" name="invite_email[]" id="invite_email1" value="">
												<label class="form-control-placeholder floatinglabel" for="invite_email">Invite </label>
												<span class="error"><?php echo form_error('invite_email'); ?></span>
											</div>
										</div>
									</div>									
									<div id="byt_invite_last"></div>									
									<button type="button" class="btn btn-primary btn-sm" onclick="append_invite_row()"><i class="fa fa-plus"></i> Add More</button>
								</div>	
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	//VALIDATION SCROLLING TO TOP
	function scroll_to_top(div_id='')
	{
		if(div_id == '') { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
		
	//REMOVE BANNER PREVIEW / IMAGE
	function remove_banner_img(flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the banner image?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				if(flag == 0)
				{
					$('#team_banner').val('');
					$("#team_banner").parent().find('input').next("span").remove();
				}
				else
				{
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();
					var data = { 
						'tbl_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('arai_byt_teams'); ?>")), 
						'pk_nm': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('team_id') ?>")), 
						'del_id': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt($team_data[0]['team_id']); ?>")), 
						'input_nm': encodeURIComponent($.trim('team_banner')), 
						'file_path': encodeURIComponent($.trim("<?php echo $encrypt_obj->encrypt('./uploads/byt/'); ?>")), 
						'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
					};          
					$.ajax(
					{ 
						type: "POST", 
						url: '<?php echo site_url("delete_single_file") ?>', 
						data: data, 
						dataType: 'JSON',
						success:function(data) 
						{ 
							/* $("#csrf_token").val(data.csrf_new_token);
							$("#"+input_nm+"_outer").remove();
							
							swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" }); */
						}
					});
					<?php } ?>
				}
				
				$("#team_banner_outer").addClass('d-none');
				$("#team_banner_outer .previous_img_outer img").attr("src", "");
				$("#preloader").css("display", "none");
			}
		});
	}
	
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function TeamBannerPreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#team_banner_outer").removeClass('d-none');
					//$("#team_banner_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#team_banner_outer .previous_img_outer img").attr("src", e.target.result);
					$("#team_banner_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#team_banner_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#team_banner").change(function() { TeamBannerPreview(this); });	
	
	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_team_file(div_id, file_id)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				
				if(file_id!= '')
				{
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("team/delete_team_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				$("#team_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				
				$("#preloader").css("display", "none");
			}
		});
	}
	
	//DISPLAY TEAM FILE PREVIEW WHEN BROWSE
	function TeamFilesPreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			var customFileCount = $("#customFileCount").val();
			var disp_filename_final = '';
			var disp_filename = input.files[0].name;
			var reader = new FileReader();
			var j = customFileCount;
							
			var disp_img_name = "";
			var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
			disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
				
			var append_str = '';
			append_str += '	<div class="col-md-2 team_files_outer_common" id="team_files_outer'+j+'">';
			append_str += '		<div class="file-list">';
			append_str += '			<a href="javascript:void(0)" onclick="remove_team_file('+j+')" class="file-close"><i class="fa fa-remove"></i></a>';
			//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
			append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
			append_str += '		</div>';
			append_str += '	</div>';				
				
			var btn_str = '';
			btn_str += '	<div class="col-md-2 custom-file" id="addFileBtnOuter">';
			btn_str +=	'		<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
			btn_str +=	'		<input type="file" class="form-control fileUploadTeams" onchange="TeamFilesPreview(this)" name="team_files[]" id="file-upload" />';
			btn_str +=	'	</div>';
				
			$("#addFileBtnOuter").addClass('d-none');
			$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
			$("#addFileBtnOuter").removeAttr( "id" );
				
			$(append_str).insertBefore("#last_team_file_id");
			$(btn_str).insertAfter("#last_team_file_id");
			$("#customFileCount").val(parseInt(customFileCount)+1);
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadTeams").change(function() { TeamFilesPreview(this); });	
	
	//ENABLE-DISABLE HIDE ADD SLOT BUTTON
	function show_hide_add_slot_btn()
	{
		var min_team_size = '<?php echo $min_team; ?>';
		var max_team_size = '<?php echo $max_team; ?>';
		var current_slot_cnt = $('.byt_member_blocks_outer_cls').length;
		
		if(current_slot_cnt == max_team_size) { /* $(".add_slot_button").addClass('d-none'); */ $(".add_slot_button").attr("disabled",true); }
		else { /* $(".add_slot_button").removeClass('d-none'); */ $(".add_slot_button").attr("disabled",false); }
	}
	show_hide_add_slot_btn();
	
	//APPEND NEW SLOT
	function append_new_slot()
	{
		var min_team_size = '<?php echo $min_team; ?>';
		var max_team_size = '<?php echo $max_team; ?>';
		var current_slot_cnt = $('.byt_member_blocks_outer_cls').length;
		var byt_member_slot_cnt = $("#byt_member_slot_cnt").val();
		
		var append_flag = 0;
		if(current_slot_cnt < max_team_size) { append_flag = 1; }			
			
		if(append_flag == 1)
		{
			var appent_str = '';
			appent_str += '	<div class="teammember byt_member_blocks_outer_cls" id="byt_member_blocks_outer'+byt_member_slot_cnt+'">';
			appent_str += '		<input type="hidden" name="team_slot_id[team_slot_id'+byt_member_slot_cnt+']" value="0" class="form-control">';
			appent_str += '		<img src="<?php echo $default_disp_photo; ?>" alt="experts">';
			appent_str += '		<h3><?php echo "&nbsp;"; ?></h3>';
			appent_str += '		<div class="form-group mt-4">';
			appent_str += '			<select class="form-control cls_team_type" name="team_type[team_type'+byt_member_slot_cnt+']" id="team_type'+byt_member_slot_cnt+'" data-placeholder="Select Type *">';
			appent_str += '				<option value="">Select Type</option>';
														<?php
														if(count($type_data) > 0)
														{ 
															foreach ($type_data as $key => $type) 
															{ ?>
																appent_str += '	<option value="<?php echo $type["tid"] ?>"><?php echo $type["name"] ?></option>';
												<?php } 
														} ?>
			appent_str += '			</select>';
			appent_str += '		</div>';
			appent_str += '		<div class="boderBox">';
			appent_str += '			<select data-key="'+byt_member_slot_cnt+'" class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset'+byt_member_slot_cnt+'][]" id="team_skillset'+byt_member_slot_cnt+'" data-placeholder="Select Skill Sets *" multiple >';
														<?php if(count($skill_sets_data) > 0)
														{	
															foreach($skill_sets_data as $skill_sets)
															{ 
																if(strtolower($skill_sets['name']) != 'other' && strtolower($skill_sets['name']) != 'others')
																{	?>
																	appent_str += '<option data-id="<?php echo $skill_sets["name"] ?>" value="<?php echo $skill_sets["id"]; ?>"><?php echo $skill_sets["name"]; ?></option>';
													<?php }
															}
														}	?>
			appent_str += '			</select>';
			appent_str += "			<p style='margin: 2px 0 0 0;font-size: 10px;line-height: 14px;text-align: justify;'><strong>Note:</strong> If the requisite field is not listed, type the required text and press #.</p>";
			appent_str += '		</div>';
			
			appent_str += '		<div class="other_div d-none" id="other_skill_div'+byt_member_slot_cnt+'">';	
			appent_str += '			<div class="form-group">';
			appent_str += '				<input type="text" name="other_skill[other_skill'+byt_member_slot_cnt+']" value="" id="other_skill'+byt_member_slot_cnt+'" class="form-control cls_other_skill" requiredxx="">';
			appent_str += '				<label for="other_skill[other_skill'+byt_member_slot_cnt+']" class="form-control-placeholder floatinglabel">Other Skillset  <em class="mandatory">*</em></label>';
			appent_str += '			</div>';
			appent_str += '		</div>';
			
			appent_str += '		<div class="form-group mt-4">';
			appent_str += '			<input type="text" name="team_role[team_role'+byt_member_slot_cnt+']" value="" id="team_role'+byt_member_slot_cnt+'" class="form-control cls_team_role">';
			appent_str += '			<label class="form-control-placeholder" for="cname">Role in Team <em>*</em></label>';
			appent_str += '		</div>';			
			appent_str += '		<button class="remove_slot_button btn btn-primary w-100 mb-2" type="button" onclick="remove_slot('+byt_member_slot_cnt+',0)">Remove</button>';
			appent_str += '	</div>';
			
			//$(appent_str).insertBefore("#byt_member_blocks_last");
			$('#CreateTeamSlider').trigger('add.owl.carousel', [appent_str]).trigger('to.owl.carousel', $('.byt_member_blocks_outer_cls').length).owlCarousel('update');
			//$('#CreateTeamSlider');
					
			applyFloatingCls();
			$('.select2_common').select2(
			{
				tags: true,
				tokenSeparators: [',','#']
			});
			
			$("input[type='text']").change( function() 
			{
				if ($(this).val() != '') { $(this).parent().find('label').addClass('floatinglabel'); }
				else { $(this).parent().find('label').removeClass('floatinglabel'); }
			});
			
			$("#byt_member_slot_cnt").val(parseInt(byt_member_slot_cnt)+1);
			show_hide_add_slot_btn();
		}	
	}
	
	//REMOVE SLOT
	function remove_slot(div_no, team_slot_id)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the slot?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				if(team_slot_id != '0') 
				{ 
					$("#preloader").css("display", "block");
					var csrf_test_name = $("#csrf_token").val();
					var data = { 'team_slot_id': encodeURIComponent($.trim(team_slot_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
						type: "POST", 
						url: '<?php echo site_url("team/delete_team_slot_ajax") ?>', 
						data: data, 
						dataType: 'JSON',
						success:function(data) 
						{ 
							$("#csrf_token").val(data.csrf_new_token);																										
						}
					});
				}
				
				var x= 0;
				$('.byt_member_blocks_outer_cls').each(function() 
				{
					if(this.id == "byt_member_blocks_outer"+div_no)
					{
						$("#CreateTeamSlider").trigger('remove.owl.carousel', [x]).owlCarousel('update');
					}						
					x++;
				});

				//$("#byt_member_blocks_outer"+div_no).remove();
				
				$("#preloader").css("display", "none");
				//swal({ title: "Success", text: 'Slot successfully deleted', type: "success" });
				show_hide_add_slot_btn();
			}
		});	
	}	
	
	//CURRENTLY NOT IN USE
	/* function xxappend_byt_files_row()
	{
		var total_div_cnt = $('input[name*="team_files[]"]').length;
		if(total_div_cnt >= 15)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#files_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_file_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-10">';
			append_html += '			<div class="form-group upload-btn-wrapper">';
			append_html += '				<button class="btn btn-upload"><i class="fa fa-plus"> </i> Browse</button>';
			append_html += '				<input type="file" class="form-control" name="team_files[]" id="team_files'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_byt_files_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#files_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_team_files_last");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	function xxremove_byt_files_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_file_row_"+div_no).remove();
			}
		});	
	} */
	
	//CURRENTLY NOT IN USE
	/* function XXappend_invite_row()
	{
		var total_div_cnt = $('input[name*="invite_email[]"]').length;
		if(total_div_cnt >= 25)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#invite_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_invite_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-10">';
			append_html += '			<div class="form-group upload-btn-wrapper">';
			
			append_html += '				<input type="text" class="form-control" name="invite_email[]" id="invite_email'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#invite_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_invite_last");
		}
	}
		
	function remove_current_div_invite(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete this row?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_invite_row_"+div_no).remove();
			}
		});	
	} */
		
	$('body').on('change', '.cls_team_skillset', function () 
	{
		var selected = $(this).find('option:selected', this);
		var results = [];
		
		var element_key=$(this).data('key');
		//console.log(element_key)	
		
		selected.each(function() { results.push($(this).data('id')); });
		var display_flag=0;
		$.each(results,function(i) { if(results[i]=='Other') { display_flag = 1; } });
		
		if (display_flag==1) 
		{
			//$("#other_skill_div"+element_key).removeClass('d-none')
		}
		else
		{			
			$("#other_skill"+element_key).val('');
			$("#other_skill_div"+element_key).addClass('d-none')			
		}		
	});
	
	//CURRENTLY NOT IN USE
	/* function delete_team_file(file_id)
	{ 
			swal(
			{
					title:"Confirm?" ,
					text: "Are you confirm to delete the File?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
			}).then(function (result) 
			{
					if (result.value) 
					{
						$("#preloader").css("display", "block");
						var csrf_test_name = $("#csrf_token").val();
						var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
						$.ajax(
						{ 
								type: "POST", 
								url: '<?php echo site_url("team/delete_team_file_ajax") ?>', 
								data: data, 
								dataType: 'JSON',
								success:function(data) 
								{ 
										$("#csrf_token").val(data.csrf_new_token);
										$("#team_file_"+data.file_id).remove();
										$("#preloader").css("display", "none");
										swal({ title: "Success", text: 'File successfully deleted', type: "success" });
								}
						});
					}
			});
	} */ 
	
	
	function swal_confirm_popup(url)
	{
		swal(
		{
			title:"Confirm?" ,
			text: "Are you sure?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				window.location.replace(url);
			}
		});
	}
	//STEP WIZARD
	$(document).ready(function() 
	{
		//******* STEP SHOW EVENT *********
		$("#smartwizard_apply_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') { $(".sw-btn-prev").hide(); $(".sw-btn-next").show(); $(".btnfinish").hide(); $(".btnCancelCustom").hide(); } 
			else if (stepPosition === 'final') { $(".sw-btn-prev").show(); $(".sw-btn-next").hide(); $(".btnfinish").show(); $(".btnCancelCustom").show(); } 
			else { $(".sw-btn-prev").show(); $(".sw-btn-next").show(); $(".btnfinish").hide(); $(".btnCancelCustom").hide(); }
		});
		
		var cancel_onclick_fun = "swal_confirm_popup('<?php echo site_url('myteams/myCreatedteams'); ?>')";
		//******* STEP WIZARD *********
		$('#smartwizard_apply_challenge').smartWizard(
		{
			/*selected: 2,   */
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */		
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
				$('<button></button>').text(<?php if($mode == 'Add'){ ?>'Form a Team'<?php }else { ?> 'Update Team' <?php } ?>).addClass('btn btn-primary btnfinish').on('click', function(e)
				{ 
					e.preventDefault();						
					var submit_flag = 0;
					
					if($("#proposed_approach").valid()==false) { submit_flag = 1; $("#proposed_approach").focus(); }
					
					if($("#proposed_approach").valid()==false) { scroll_to_top('proposed_approach'); }						
					
					if(submit_flag == 0)
					{
						swal(
						{
							title:"Confirm",
							<?php if($mode == 'Add'){ ?> text: "Are you confirm to build your team?", <?php } else { ?>text: "Are you confirm to update your team?", <?php } ?>
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Yes!'
						}).then(function (result) { if (result.value) { $('#ApplyChallengeForm').submit(); } });
					}
				}),
				$('<a style="margin-left: 5px;border-radius: 4px;" href="javascript:void(0)" onclick="'+cancel_onclick_fun+'" ></a>').text('Cancel').addClass('btn btn-primary btnCancelCustom')
				]
			}
		});
		
		$("#smartwizard_apply_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{				
				if($("#team_details").valid()==false) { isValidate= false; $("#team_details").focus(); }
				//if($("#team_size").valid()==false) { isValidate= false; $("#team_size").focus(); }
				if($("#team_banner").valid()==false) { isValidate= false; $("#team_banner").focus(); }
				if($("#team_name").valid()==false) { isValidate= false; $("#team_name").focus(); }
				
				if($("#team_name").valid()==false) { scroll_to_top('team_name'); }
				else if($("#team_banner").valid()==false) { scroll_to_top('team_banner'); }
				//else if($("#team_size").valid()==false) { scroll_to_top('team_size'); }
				else if($("#team_details").valid()==false) { scroll_to_top('team_details'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				
				//if(isValidate == true) { append_team_member_blocks(); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				var form = $( "#ApplyChallengeForm" );
				if(form.valid()==false) { isValidate= false; }
				
				if(form.valid()==false) { scroll_to_top('smartwizard_apply_challenge'); }			
				else { scroll_to_top('smartwizard_apply_challenge'); }
			}
			
			/* if(stepNumber==2 && stepDirection=="forward")
				{				
				if($("#from_age").valid()==false) { isValidate= false; $("#from_age").focus(); }
				if($("#educational").valid()==false) { isValidate= false; $("#educational").focus(); }
				
				if($("#educational").valid()==false) { scroll_to_top('educational'); }
				else if($("#from_age").valid()==false) { scroll_to_top('from_age'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				}		
			*/
			if(stepDirection=="backward") { scroll_to_top('smartwizard_apply_challenge') }
			return isValidate;
		})
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	$('.select2_common').select2(
	{
    tags: true,
    tokenSeparators: [',','#']
	});
	
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		$("input.numbers").keypress(function(event) { return /\d/.test(String.fromCharCode(event.keyCode)); });
		
		$.validator.addMethod("type_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Type');		
		$.validator.addClassRules("cls_team_type", { type_required: true });
		
		$.validator.addMethod("skillset_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Skill Sets');		
		$.validator.addClassRules("cls_team_skillset", { skillset_required: true });
		
		$.validator.addMethod("team_other_skill_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Other Skill');		
		$.validator.addClassRules("cls_other_skill", { team_other_skill_required: true, letters_number_space: true });
		
		$.validator.addMethod("team_role_required", function(value, element) { if($.trim(value) == "") { return false; } else { return true; } },'Please enter the Role in Team');		
		$.validator.addClassRules("cls_team_role", { team_role_required: true, letters_number_space: true });
		
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		$.validator.addMethod("letters_number_space", function(value, element) { return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value); }, 'Please enter only alpha numeric character'),
		
		//******* JQUERY VALIDATION *********
		$("#ApplyChallengeForm").validate( 
		{
			/* onkeyup: false, 
			ignore: [],*/
			rules:
			{
				team_name: { required: true, nowhitespace: true, letters_number_space: true/* , maxCount:['20'] */ },
				team_banner: { valid_img_format: true, maxsize: 2000000 },
				//team_size: { required: true, nowhitespace: true, digits:true, min: parseInt("<?php echo $min_team; ?>"), max: parseInt("<?php echo $max_team; ?>") },
				team_details: { required: true, nowhitespace: true, letters_number_space: true/* , minCount:['20'], maxCount:['80'] */ },
			},
			messages:
			{
				team_name: { required: "Please enter the Name of the Team", nowhitespace: "Please enter the Name of the Team" },
				team_banner: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				//team_size: { required: "Please enter the Team Size", nowhitespace: "Please enter the Team Size", digits: "Please enter only numbers in Team Size", min:"Please enter a value greater than or equal to <?php echo $min_team; ?>", max:"Please enter a value less than or equal to <?php echo $max_team; ?>" },
				team_details: { required: "Please enter the Brief About the Team", nowhitespace: "Please enter the Brief About the Team" },
			},
			errorElement: 'span',
			errorPlacement: function (error, element) 
			{
				if(element.hasClass('select2_common') && element.next('.select2-container').length) 
				{
					error.insertAfter(element.parent());
				}
				else { element.closest('.form-group').append(error);  }
			},
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); }
		});		
		
		$(document).on("change", ".select2_common", function() 
		{
			var form = $( "#ApplyChallengeForm" );
			if(form.valid()==false) { isValidate= false; }
			return isValidate;
		});
	});
</script>