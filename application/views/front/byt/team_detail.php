<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<style>
	.home-p.pages-head3 {
	background: url(<?php echo base_url() ?>assets/img/aboutus.jpg) no-repeat center top !important;
	background-size: cover !important;
	}
	
	ul.file_attachment_outer_common {
	list-style: none;
	margin: 0;
	padding: 0;
	}
	
	ul.file_attachment_outer_common li:before { display:none; }
	
	ul.file_attachment_outer_common li {
	padding: 3px;
	margin: 0 5px 10px 0;
	width: 80px;
	height: 60px;
	border: 1px solid #ccc;
	border-radius: 5px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
	}
	
	ul.file_attachment_outer_common li a h4 {
	margin: 0;
	line-height: 55px;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	font-size: 15px !important;
	}
	
	ul.file_attachment_outer_common li a img
	{
	max-width:100%;
	max-height:100%;
	}
	.home-p.pages-head4 {	
	background-size: 100%; min-height: 100%;
	}
	
	.customDetailView
	{
	height: auto;
	margin: 0;
	padding: 10px 0 5px 0 !important;
	text-align:justify;
	}
	
	span.badge.badge-info { white-space: normal; }
</style>

<?php if( count($team_slots) < 5 ){ ?> 
	<style>
	.owl-nav {
    display: none !important;
	}
	</style>
<?php } ?>

<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down"
style="background: url(<?php if($team_details[0]['team_banner']!=''){echo base_url('uploads/byt/').$team_details[0]['team_banner'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size:100% 100% !important;" 
>
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Details</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Team Details</li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">
						<div class="col-md-12">
							<div class="details-table">
								<table class="table table-bordered">
									<tr>
										<td width="30%"><label>Team ID & Name</label></td>
										<td><?php echo $team_details[0]['custom_team_id']." - " .$team_details[0]['team_name'] ?></td>
									</tr>
									<tr>
										<td><label>Challenge ID & Name</label></td>
										<td><?php echo $team_details[0]['challenge_id']." - " .$encrptopenssl->decrypt($team_details[0]['challenge_title']) ?></td>
									</tr>
									<tr>
										<td><label>Brief About Team</label></td>
										<td><?php echo $team_details[0]['brief_team_info']; ?></td>
									</tr>
									<tr>
										<td><label>Proposed Approach </label></td>
										<td><?php echo $team_details[0]['proposed_approach']; ?></td>
									</tr>
									
									<?php if(count($team_files))
									{ ?>									
									<tr>
										<td><label>Files </label></td>
										<td>
											<div class="file-details">
												<div class="row">
													<div class="col-md-12">
														
														<?php 
																if(count($team_files) > 0)
																{ ?>																
															<ul class="file_attachment_outer_common">
																<?php 
																	foreach ($team_files as $key => $file) 
																		{ 
																			$logged_in_user_id = $this->session->userdata('user_id');
																			$chkApplication = $this->master_model->getRecords("byt_slot_applications", array('team_id' => $team_id, 'apply_user_id' => $logged_in_user_id, 'status' => 'Approved', 'is_deleted' => '0'));
																			$chkChallenge = $this->master_model->getRecords("arai_challenge", array('c_id' => $team_details[0]['c_id'], 'u_id' => $logged_in_user_id, 'is_deleted' => '0'));
																																				
																			$display_flag = 0;
																			if($file['file_type'] == "Public") { $display_flag = 1; }
																			else  
																			{ 
																				/* if($team_details[0]['share_files'] == 'yes' && (count($chkApplication) > 0 || count($chkChallenge) > 0)) */
																				
																				if(count($chkApplication) > 0)
																				{
																					$display_flag = 1; 
																				}
																				else if($team_details[0]['share_files'] == 'yes' && count($chkChallenge) > 0)
																				{
																					$display_flag = 1; 
																				}
																			}
																			
																			if($display_flag == 1)
																	{ ?>
																	<li>
																		<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
																			<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
																				if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
																			else { $disp_img_name = ''; } ?>
																			
																			<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
																			else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
																		</a>
																	</li>
																	<?php }
																		}	?>
															</ul>
														<?php	}?>
														
														<div class="col-md-12 text-center">
															<a href="#" class="btn btn-primary btn-sm mt-3 d-none">Download Files</a>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<?php } ?>
								</table>
							</div>
						</div>
						
						
						<div id="teamSlider" class="owl-carousel owl-theme">
							
							<?php foreach ($team_slots as $key => $slot) 
								{ 
									$this->db->select('sa.app_id, sa.apply_user_id, r.user_id, r.title, r.first_name, r.last_name, r.user_category_id');
									$this->db->join('arai_registration r','r.user_id = sa.apply_user_id','INNER', false);
									$aproved_member = $this->master_model->getRecords("arai_byt_slot_applications sa",array('sa.slot_id'=>$slot['slot_id'],'sa.status'=>'Approved'));
									//echo $this->db->last_query(); die;
									
									$DispUserName = '-';
									$skills = array();
									$skillTitle = 'Skills';
									$slot_final_flag = $receivedApplicationCnt = 0;
									$profile_img = '<img src="'.base_url('assets/profile_picture/user_dummy.png').'" alt="experts">';
									if(count($aproved_member) > 0) 
									{
										$DispUserName = $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
										$slot_final_flag = 1;
										
										if($aproved_member[0]['user_category_id'] == 1)
										{
											$this->db->select('profile_picture, skill_sets, other_skill_sets');
											$profileImgdata = $this->master_model->getRecords("student_profile",array('user_id'=>$aproved_member[0]['apply_user_id']));
											
											if (isset($profileImgdata[0]['profile_picture']) &&  $profileImgdata[0]['profile_picture'] !='' ) 
											{ 
												$profile_img = '<img src="'.base_url('assets/profile_picture/'.$profileImgdata[0]['profile_picture']).'" alt="" class="img-fluid bor">';
											}
											
											if($profileImgdata[0]['skill_sets'] != "")
											{
												$skill_set_ids = $encrptopenssl->decrypt($profileImgdata[0]['skill_sets']);
												
												if($skill_set_ids != "")
												{
													$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
													$skills = $this->master_model->getRecords("arai_skill_sets");														
												}
											}
											
											if($profileImgdata[0]['other_skill_sets'] != "") 
											{ 
												$skills[]['name'] = $profileImgdata[0]['other_skill_sets']; 
											}
										}
										else
										{
											$this->db->select('org_logo, org_sector, org_sector_other');
											$profileImgdata = $this->master_model->getRecords("arai_profile_organization",array('user_id'=>$aproved_member[0]['apply_user_id']));
											
											if(isset($profileImgdata[0]['org_logo']) &&  $profileImgdata[0]['org_logo'] !='' ) 
											{ 
												$profile_img = '<img src="'.base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($profileImgdata[0]['org_logo'])).'" alt="" class="img-fluid bor">';
											}
											
											$skillTitle = 'Sector';
											if($profileImgdata[0]['org_sector'] != "")
											{
												$skill_set_ids = $profileImgdata[0]['org_sector'];
												
												if($skill_set_ids != "")
												{
													$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
													$skills = $this->master_model->getRecords("arai_organization_sector");														
												}
											}
											
											if($profileImgdata[0]['org_sector_other'] != "") 
											{ 
												$skills[]['name'] = $encrptopenssl->decrypt($profileImgdata[0]['org_sector_other']); 
											}
										}
									}
									else
									{
										$receivedApplicationCnt = $this->master_model->getRecordCount( "arai_byt_slot_applications",array("slot_id"=>$slot['slot_id'], "is_deleted"=>'0', "status != "=>'Approved'));
									
									$skill=$slot['skills'];	
										$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slot['user_id']."')",NULL, false);
										$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
									} ?>
								
								<div class="mt-4">
									<div class="boxSection26">										
										<div class="text-center"><?php echo $profile_img; ?></div>
										<h3><?php echo $DispUserName; ?></h3>
										<h3 class="mb-2"><?php echo $slot['name']; ?></h3>
										<div class="Skillset">
											<?php 
												if (count($skills)) {
													foreach ($skills as $key => $value) { ?>													
													<span class="badge badge-pill badge-info"><?php if($skillTitle != 'Sector') { if(strtolower($encrptopenssl->decrypt($value['name'])) != 'other') { echo $encrptopenssl->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { echo $value['name']; } } ?></span>													
												<?php } }
											?>
										</div><br>
										<strong style="font-weight:500;">Role in Team</strong>
										<p class="form-control customDetailView" style="height: 55px;border-left: none;border-top: none;border-right: none;border-radius: 0;padding-top: 0 !important;"><?php echo $slot['role_name']; ?></p>									
									</div>
								</div>							
							<?php } ?>						
						</div>					
						
						<div class="col-md-12">
							<div class="team_view_listing mt-4">
								<a href="#" class="pull-left chatButton d-none">Chat</a>
								<a href="#" class="pull-right d-none">Apply</a>
							</div>
						</div>
					</div>



						<?php 
						
						$u_id = $this->session->userdata('user_id');

						if ( isset($u_id) && $u_id!='') { 

						$is_pproved = $this->master_model->getRecords("byt_slot_applications", array('apply_user_id' => $u_id, 'team_id' => $team_details[0]['team_id'], 'is_deleted' => '0', 'status' => 'Approved'));

						if(count($is_pproved)){
		
						 	
						
						$this->db->select('title,first_name,last_name,profile_picture,org_logo');						
						$this->db->join('profile_organization','profile_organization.user_id=registration.user_id','LEFT');	
						$this->db->join('student_profile','student_profile.user_id=registration.user_id','LEFT');			
						$user_data= $this->master_model->getRecords('registration',
						array('registration.user_id'=>$u_id ));

							if ($this->session->userdata('user_category') == 1 &&  count($user_data) && $user_data[0]['profile_picture']!='') {
									$pro_pic = base_url('assets/profile_picture/'.$user_data[0]['profile_picture']);
							
							}
								elseif ($this->session->userdata('user_category') == 2 && count($user_data) && $user_data[0]['org_logo']!='') {
									$pro_pic = base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($user_data[0]['org_logo']));

							}else{
								$pro_pic = base_url('assets/profile_picture/user_dummy.png');
							} 
						?>
							
						<div class="col-md-12 mt-4">
							<strong>Team Discussion</strong>
							<div class="row">
								<div class="col-12 chatScrollbar">
									<div id="chats">
										
									</div>
								
								<div class="comments">
										<div class="comment-box">
											<span class="commenter-pic">
												<img src="<?php echo $pro_pic ?>" class="img-fluid">
											</span>
											<span class="commenter-name">
												<a href="#"><?php echo $encrptopenssl->decrypt($user_data[0]['title'])." ".$encrptopenssl->decrypt($user_data[0]['first_name'])." ".$encrptopenssl->decrypt($user_data[0]['last_name']) ?></a> 
											</span>       
											<div class="form-group mt-3">
												<textarea class="form-control" id="msg_content"  name="contents"></textarea>
											</div>
											<button  type="button" id="send_msg" class="btn btn-primary pull-right">Send</button>
										</div>
									</div>

								</div>
							</div>
						</div>

						<?php } } ?> 



					<?php if ($team_details[0]['u_id']==$this->session->userdata('user_id') ) { ?>
						<div class="row mt-5">
							<div class="col-md-12">
								<div class="text-center">
									<?php
									
									$TeamConditions = $this->master_model->checkApplyTeamCondtions($team_details[0]['apply_status'], $team_details[0]['challenge_owner_status']);
									$showBtnAccept = $TeamConditions['showBtnAccept'];
									$showBtnReject = $TeamConditions['showBtnReject'];
									
									if($showBtnAccept == 1) 
									{ ?>
										<a href="javascript:void(0)" id="accept_team" data-approval='accept' data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary ch_owner_approval">Accept</a>
						<?php } 
									else if($team_details[0]['challenge_owner_status'] == 'Approved' && $showBtnReject == 1)
									{	?>
										<button disabled class="btn btn-primary">Accepted</button>
						<?php	}
									
									if($showBtnReject == 1){ ?>									
									<a href="javascript:void(0)" id="reject_team" data-approval='reject' data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary ch_owner_approval">Reject</a>								
									<?php }
									else if($team_details[0]['challenge_owner_status'] == 'Rejected') { ?>
										<button disabled class="btn btn-primary">Rejected</button>
									<?php } ?>
									
									<a href="<?php echo site_url('challenge/challengeDetails/'.base64_encode($team_details[0]['c_id'])); ?>" class="btn btn-primary">Back</a>
								</div>
							</div>
						</div>
						
					<?php } ?> 




					<div class="row mt-3">	
						<div class="col-md-6">	
							<div class="form-group mt- d-none" id="reject_reason_div">
								<input type="text" id="reject_reason" class="form-control"  value="" maxlength="250">
								<label class="form-control-placeholder" style="left:0px;" for="reject_reason">Rejection Reason </label>
								<span class="error" id=err_reason></span>
							</div>
						</div>	
					</div>		
					
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('.ch_owner_approval').click(function(){
		var appr=$(this).data('approval');
		var reject_reason = '';
		$("#err_reason").html('');
		if (appr=='reject') {
			$("#reject_reason_div").removeClass('d-none');
			reject_reason = $("#reject_reason").val();
			if (reject_reason=='') {
				$("#err_reason").html('Please enter reason');
				return false;
			}
			
			}else{ 
			$("#reject_reason_div").addClass('d-none')
		}
		
		
		swal(
		{
			title:"Confirm?" ,
			text: "Are you sure you want to "+ appr + " this team?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				
				
				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
				
				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
				
				
				var b_url= "<?php echo base_url();?>";
				var c_id = '<?php echo base64_encode($team_details[0]["c_id"]); ?>';
				var datastring='reject_reason='+ reject_reason + '&appr='+ appr + '&team_id='+ team_id + '&' + csrfName + '='+csrfHash;
				$.ajax({
					type: 'POST',
					data:datastring,
					url: "<?php echo base_url();?>myteams/challenge_owner_approval",
					beforeSend: function(){
						$('#preloader-loader').css("display", "block");
					},
					success: function(res){ 
						
						
						if(res=='success'){
							
							swal({
						    title: "Success!",
						    text: "Team "+appr+"ed Successfully!",
						    type: "success"
								}).then(function() {
						    location.href = b_url+'challenge/teamListing/'+c_id;
							});
							
							}else{
							swal( 'Error!','Something went wrong!','error');
						}
					},
					complete: function(){
						$('#preloader-loader').css("display", "none");
					},
					
				});
				
			}
		} ) 	
		
	})
</script>		


<script>
	$(document).ready(function(){
 		setInterval(
 			get_chats_interval,50000);
	});
	
	$(window).on('load', function () {
	   	get_chats();
 	});


 	function get_chats_interval(){
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			async:true,
			url: "<?php echo base_url();?>team_discussion/get_chats",
		
			success: function(res){ 

				$("#chats").html(res);
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
		
			
		});


    }
 	}

 	function get_chats(){
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			url: "<?php echo base_url();?>team_discussion/get_chats",
			beforeSend: function(){
				$('#preloader-loader').css("display", "block");
			},
			success: function(res){ 

				$("#chats").html(res);
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
			complete: function(){
				$('#preloader-loader').css("display", "none");
			},
			
		});


    }
 	}


	$('#send_msg').on('click', function () {

		
	$("#msg_content").css('border-color','#888')
      var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
      var  msg_content = $("#msg_content").val();
      if (msg_content=='') {$("#msg_content").css('border-color','red'); return false;}
     
      if (team_id != '') {   
      
     	 var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		
		var datastring='msg_content='+msg_content+'&team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		$.ajax({
			type: 'POST',
			data:datastring,
			url: "<?php echo base_url();?>team_discussion/insert_chat",
			beforeSend: function(){
				$('#preloader-loader').css("display", "block");
			},
			success: function(res){ 
				$("#msg_content").val('');
				// $("#chats").html(res);
				get_chats();
				// var data = $.parseJSON(res);
				
				// if(data.success==true){
					
				// 	swal({
				// 		title: "Success!",
				// 		text: "Application Withdrawn Successfully!",
				// 		type: "success"
				// 		}).then(function() {
				// 		//location.reload(); 
				// 		location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
				// 	});
					
				// 	}else{
				// 	swal( 'Error!','Something went wrong!','error');
				// }
			},
			complete: function(){
				$('#preloader-loader').css("display", "none");
			},
			
		});


    }
   
 });	
   
</script>
<script>
	function delete_chat(chat_id)
		{
			swal(
			{
				title:"Confirm",
				text: "Are you sure you want to delete this message ?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!',
				html:''
			}).then(function (result) 
			{
				if (result.value) 
				{
						var cs_t = 	$('.token').val();
						parameters= { 'chat_id':chat_id, 'cs_t':cs_t }
						$("#preloader-loader").show();
						$.ajax(
						{
							type: "POST",
							url: "<?php echo site_url('team_discussion/delete_chat'); ?>",
							data: parameters,
							cache: false,
							dataType: 'JSON',
							success:function(data)
							{
								if(data.flag == "success")
								{
									$(".token").val(data.csrf_new_token)					
									$("#preloader-loader").hide();
									get_chats();
								}
								else 
								{ 
									$("#preloader-loader").hide();
									sweet_alert_error("Error Occurred. Please try again."); 
								}
							}
						});	
				}
				
			});


				
		} 
</script>