<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<style>
	.home-p.pages-head3 {
	background: url(<?php echo base_url() ?>assets/img/aboutus.jpg) no-repeat center top !important;
	background-size: cover !important;
	}
</style>
<div class="profileinfo home-p pages-head3 aos-init aos-animate" data-aos="fade-down">
	<div class="container">
	
		<div class="row mt-4">
			<div class="col-md-2">

				<img class="img-fluid"  src="<?php echo base_url('uploads/byt/').$team_details[0]['team_banner'] ?>" >

			</div>
			<div class="col-md-8">
				<h3 class="card-title mt-5"><?php echo $team_details[0]['team_name'] ?></h3>
			</div>
		</div>

	</div>
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">
						<div class="col-md-12">
							<div class="details-table">
								<table class="table table-bordered">
									<tr>
										<td width="30%"><label>Team Name</label></td>
										<td><?php echo $team_details[0]['team_name'] ?></td>
									</tr>
									<tr>
										<td><label>Challenge Name</label></td>
										<td><?php echo $team_details[0]['challenge_id']." " .$encrptopenssl->decrypt($team_details[0]['challenge_title']) ?></td>
									</tr>
									<tr>
										<td><label>Brief About Team</label></td>
										<td><?php echo $team_details[0]['brief_team_info']; ?></td>
									</tr>
									<tr>
										<td><label>Proposed Approach </label></td>
										<td><?php echo $team_details[0]['proposed_approach']; ?></td>
									</tr>
									<?php if ($team_details[0]['share_files'] == 'yes'): ?>
										
									<tr>
										<td><label>Files </label></td>
										<td>
											<div class="file-details">
												<div class="row">
													
													<?php 
													if(count($team_files) > 0){
													foreach ($team_files as $key => $file) { ?>
													
													<div class="col-md-3">
														<div class="file-list">
															
															
																<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
																	<span>
																<i class="fa fa-file">
																	
																</i>
																</span> 
																</a>

														
														</div>
													</div>

													<?php } }?>

													<div class="col-md-12 text-center">
														<a href="#" class="btn btn-primary btn-sm mt-3 d-none">Download Files</a>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<?php endif ?>
								</table>
							</div>
						</div>

						
						<div id="teamSlider" class="owl-carousel owl-theme">

						<?php foreach ($team_slots as $key => $slot) { 

											$this->db->select('registration.user_id,title,first_name,last_name,profile_picture');
											
											$this->db->join('student_profile','student_profile.user_id=byt_slot_applications.apply_user_id');

											$this->db->join('registration','registration.user_id=byt_slot_applications.apply_user_id');
						$aproved_member   = $this->master_model->getRecords("byt_slot_applications",array('slot_id'=>$slot['slot_id'],'byt_slot_applications.status'=>'Approved' ));	
									
									$skill=$slot['skills'];	
									$this->db->where("id IN (".$skill.")",NULL, false);
						$skills   = $this->master_model->getRecords( "arai_skill_sets",'','name');

							

						?>
							
							<div class="mt-4">
								<div class="boxSection26">
									
									<div class="text-center">

									<?php if (isset($aproved_member[0]['profile_picture']) &&  $aproved_member[0]['profile_picture'] !='' ) { ?>
			                    	<img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $aproved_member[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
			                      
						       		 <?php }else{ ?>
					                    <img src="<?php echo base_url() ?>assets/img/testimonial-2.jpg" alt="experts">
						   		  <?php } ?>


									</div>
									<h3>
									<?php 
								if (count($aproved_member)) {
								echo $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
								} else {echo "N/A";} ?>
									</h3>
									<h3 class="mb-2"><?php echo $slot['name']; ?></h3>
									<div class="Skillset">

										<?php 
										if (count($skills)) {
										foreach ($skills as $key => $value) { ?>

											<span class="badge badge-pill badge-info"><?php echo $encrptopenssl->decrypt( $value['name'] ); ?></span>
											
										<?php } }
										?>
										
									</div>
									<strong>Role : <?php echo $slot['role_name']; ?></strong>

								</div>
							</div>


						<?php } ?>

						</div>

						<div class="col-md-12">
							<div class="team_view_listing mt-4">
								<a href="#" class="pull-left chatButton d-none">Chat</a>
								<a href="#" class="pull-right d-none">Apply</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>