<style>

#preloader-loader {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
z-index: 9999;
overflow: hidden;
background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
content: "";
position: fixed;
top: calc(50% - 30px);
left: calc(50% - 30px);
border: 6px solid #f2f2f2;
border-top: 6px solid #c80032;
border-radius: 50%;
width: 60px;
height: 60px;
-webkit-animation: animate-preloader 1s linear infinite;
animation: animate-preloader 1s linear infinite;
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.owl-item .form-control-placeholder{left:0px;}
</style>

<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Details</h1>        
	</div>
</div>
<style>#datatable-list_wrapper { max-width:99%; } </style>
<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>

<div id="preloader-loader" style="display:none;"></div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<div class="formInfo">					
					<div class="row mt-12">

						<?php  if ($team_details[0]['team_banner']!='') { ?>
						<div class="col-md-5 offset-md-3 pt-5 pb-5">
							<img class="img-fluid"  src="<?php echo base_url('uploads/byt/').$team_details[0]['team_banner'] ?>" >
						</div>
						<?php } ?>
					

						<div class="col-md-6 mb-3">							
							<p><?php echo $team_details[0]['custom_team_id']." <strong>" .$team_details[0]['team_name']."</strong>" ?></p>
								
						</div>
						<div class="col-md-6 mb-3">							
							<p><?php echo $team_details[0]['challenge_id']." <strong>" .$encrptopenssl->decrypt($team_details[0]['challenge_title'])."</strong>" ?></p>
								
						</div>

						<div class="col-md-12 mb-3">
							<p>
								<strong>Brief about Team: </strong> <?php echo $team_details[0]['brief_team_info']; ?>
							</p>
						</div>

						<div class="col-md-12 mb-3">
							<p><strong>Proposed Approach: </strong> <?php echo $team_details[0]['proposed_approach']; ?>
							</p>
						</div>

						<div class="col-md-12 mb-3">
							<p><strong>Additional Information: </strong> <?php echo $team_details[0]['additional_information']; ?>
							</p>
						</div>



						<div class="col-md-4">
							<p ><strong> Files & Attachments :</strong></p>
							
							<?php 
							if(count($team_files) > 0){
							foreach ($team_files as $key => $file) { ?>
								<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>View File</a><br>
							<?php } }?>
							<br>
						</div>

						<div class="col-md-6">
								<label for="">Share Files</label>
								<label class="switch">
								  <input type="checkbox" id="share_files" <?php if($team_details[0]['share_files']=='yes'){echo 'checked';} ?>> 
								  <span class="slider round"></span>
								</label>
						</div>


						<?php  foreach ($team_slots as $key => $slot) { 

											$this->db->select('registration.user_id,title,first_name,last_name,profile_picture');
											
											$this->db->join('student_profile','student_profile.user_id=byt_slot_applications.apply_user_id','LEFT');

											$this->db->join('registration','registration.user_id=byt_slot_applications.apply_user_id');
						$aproved_member   = $this->master_model->getRecords("byt_slot_applications",array('slot_id'=>$slot['slot_id'],'byt_slot_applications.status'=>'Approved' ));

									$skill=$slot['skills'];	
									$this->db->where("id IN (".$skill.")",NULL, false);
						$skills   = $this->master_model->getRecords( "arai_skill_sets",'','name');	
							

						?>
						
							<div class="col-md-3">

								<?php if (isset($aproved_member[0]['profile_picture']) &&  $aproved_member[0]['profile_picture'] !='' ) { ?>
		                    	<img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $aproved_member[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
		                      
					       		 <?php }else{ ?>
				                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
					   		  <?php } ?>

							<p>
								<?php 
								if (count($aproved_member)) {
								echo $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
								} ?>
									
							</p>

							<?php 
							foreach ($skills as $key => $value) { ?>

								<span class="badge badge-pill badge-info"><?php echo $encrptopenssl->decrypt( $value['name'] ); ?></span>
								
							<?php } 
							?>
								
							<p>
								<?php echo $slot['role_name']; ?>
							</p>
							<p>
								<?php echo $slot['name']; ?>
							</p>

							<?php  if ($key!=0 && count($aproved_member) ) {

							 ?>
								<a href="javascript:void(0)" data-id='<?php echo $slot['slot_id'] ?>' class="btn btn-danger">
									Remove Member
								</a>
							<?php } ?>

						</div>


							
						 <?php } ?>

						 <div class="col-md-8 mt-5">
								<label for="">Is Your Team Complete?</label>
								<label class="switch">
								  <input type="checkbox" id="is_team_complete" <?php if($team_details[0]['team_status']=='Completed'){echo 'checked';} ?>> 
								  <span class="slider round"></span>
								</label>
						</div>

						 <?php if($team_details[0]['apply_status']=='Withdrawn' || $team_details[0]['apply_status']=='Pending'){ ?>
						<div class="col-md-8 mt-5" >
								<a href="javascript:void(0)" id="apply_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary">
									Apply For A Challenge
								</a>
						</div>
						<?php } ?>


						 <?php if($team_details[0]['apply_status']=='Applied'){ ?>
						<div class="col-md-6 mt-5" >
								<a href="javascript:void(0)" id="withdraw_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary mobileTop">
									Withdraw Team
								</a>
						</div>
						<?php } ?>

						

					</div>    
				</div>    
			</div>
		</div>
	</div>
</section>

<script>
        $(document).ready(function() {
          
          $('#share_files').change(function(){
          	  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
          	  var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

          	  var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
          	
          	var share_files 
          	if ($('#share_files:checked').val()=='on') {
          		share_files = 'yes';
          	}else{
          		share_files = 'no';
          	}

	  	   var datastring='share_files='+ share_files + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
	         $.ajax({
	             type: 'POST',
	             data:datastring,
	             url: "<?php echo base_url();?>myteams/share_files",
	             beforeSend: function(){
	               $('#preloader-loader').css("display", "block");
	             },
	             success: function(res){ 
	               // var json = $.parseJSON(res);
	               // $('#subcategory').html(json.str);
	               //csrfName=json.name;
	               //csrfHash=json.value;
	             },
	             complete: function(){
	                      $('#preloader-loader').css("display", "none");
	              },

	           });


          	
          })

            $('#is_team_complete').change(function(){
          	  
          	  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
          	  var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

          	  var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
          	
          	var team_complete 
          	if ($('#is_team_complete:checked').val()=='on') {
          		team_complete = 'Completed';
          	}else{
          		team_complete = 'Incomplete';
          	}

	  	   var datastring='team_complete='+ team_complete + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
	         $.ajax({
	             type: 'POST',
	             data:datastring,
	             url: "<?php echo base_url();?>myteams/is_team_complete",
	             beforeSend: function(){
	               $('#preloader-loader').css("display", "block");
	             },
	             success: function(res){ 
	               // var json = $.parseJSON(res);
	               // $('#subcategory').html(json.str);
	               //csrfName=json.name;
	               //csrfHash=json.value;
	             },
	             complete: function(){
	                      $('#preloader-loader').css("display", "none");
	              },

	           });


          	
          })

          

          

            $('#apply_for_challenge').click(function(){

	    	 swal(
	        {
	            title:"Confirm?" ,
	            text: "Are you sure you want to apply for this challenge?",
	            type: 'warning',
	            showCancelButton: true,
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Yes!'
	        }).then(function (result) 
	        {
	            if (result.value) 
	            {


				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';

		      

		  	   var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		         $.ajax({
		             type: 'POST',
		             data:datastring,
		             url: "<?php echo base_url();?>myteams/apply_for_challenge",
		             beforeSend: function(){
		               $('#preloader-loader').css("display", "block");
		             },
		             success: function(res){ 
		               var data = $.parseJSON(res);
		              
		               if(data.success==true){

						swal({
						    title: "Success!",
						    text: "Application Submitted Successfully!",
						    type: "success"
						}).then(function() {
						     location.reload(); 
						});

		               }else{
		               	swal( 'Error!','Something went wrong!','error');
		               }
		             },
		             complete: function(){
		                      $('#preloader-loader').css("display", "none");
		              },

		           });

		            }
		       } ) 	

          })


           $('#withdraw_for_challenge').click(function(){

	    	 swal(
	        {
	            title:"Confirm?" ,
	            text: "Are you sure you want to withdraw your team?",
	            type: 'warning',
	            showCancelButton: true,
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Yes!'
	        }).then(function (result) 
	        {
	            if (result.value) 
	            {


				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';

		      

		  	   var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		         $.ajax({
		             type: 'POST',
		             data:datastring,
		             url: "<?php echo base_url();?>myteams/withdraw_for_challenge",
		             beforeSend: function(){
		               $('#preloader-loader').css("display", "block");
		             },
		             success: function(res){ 
		                var data = $.parseJSON(res);
		              
		               if(data.success==true){

						swal({
						    title: "Success!",
						    text: "Application Withdrawn Successfully!",
						    type: "success"
						}).then(function() {
						     location.reload(); 
						});

		               }else{
		               	swal( 'Error!','Something went wrong!','error');
		               }
		             },
		             complete: function(){
		                      $('#preloader-loader').css("display", "none");
		              },

		           });

		            }
		       } ) 	

          })

            

          })
</script>
<script>
	function delete_chat(chat_id)
		{
			swal(
			{
				title:"Confirm",
				text: "Are you sure you want to delete this message ?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!',
				html:''
			}).then(function (result) 
			{
				if (result.value) 
				{
						var cs_t = 	$('.token').val();
						parameters= { 'chat_id':chat_id, 'cs_t':cs_t }
						$("#preloader-loader").show();
						$.ajax(
						{
							type: "POST",
							url: "<?php echo site_url('team_discussion/delete_chat'); ?>",
							data: parameters,
							cache: false,
							dataType: 'JSON',
							success:function(data)
							{
								if(data.flag == "success")
								{
									$(".token").val(data.csrf_new_token)					
									$("#preloader-loader").hide();
									get_chats();
								}
								else 
								{ 
									$("#preloader-loader").hide();
									sweet_alert_error("Error Occurred. Please try again."); 
								}
							}
						});	
				}
				
			});


				
		} 
</script>