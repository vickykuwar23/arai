﻿<style>	
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
.team_search.owl-carousel .owl-dot,
.team_search.owl-carousel .owl-nav .owl-next,
.team_search.owl-carousel .owl-nav .owl-prev {
	font-family: fontAwesome
}

.team_search.owl-carousel .owl-nav .owl-prev:before {
	content: "\f177"
}

.team_search.owl-carousel .owl-nav .owl-next:after {
	content: "\f178"
}

.team_search .owl-nav.disabled {
	display: block
}

.team_search .owl-nav {
	position: absolute;
	top: 35px;
	width: 100%
}

.team_search .owl-nav .owl-prev {
	left: 10px;
	position: absolute
}

.team_search .owl-nav .owl-next {
	right: 10px;
	position: absolute
}

.team_search h4, .inner-box h4 {
	color: #000;
	padding: 10px 0;
	margin: 15px 0 15px 0;
	border-top: solid 1px #eee;
	border-bottom: solid 1px #eee;
	font-size: 22px;
}




.team_search .item {
	margin: 0;
	padding: 15px 0 0 0;

}

.team_search .item a {
	text-decoration: none;
	padding: 5px 15px;
	color: #fff;
	background: #e23751;
	text-align: center;
	border-radius: .25rem;
	display: block;
}

.team_search .item a:hover {
	text-decoration: none;
	color: #FFF;
	background: #333;
}

.boxSection25 {
	padding: 0;
	margin: 0;
}

.formInfo ul.list-group {
	display: inline-block
}

.boxSection25 .list-group li.list-group-item {
	width: 33.33%;
	float: left;
	min-height: 438px;
	padding: 8px;
}

.boxSection25 h3 {
	color: #000;
	padding: 15px 0;
	margin: 0 0 15px 0;
	border-bottom: solid 1px #eee;
	font-size: 22px;
}

.boxSection25 ul {
	list-style: none;
	text-align: center;
	overflow: hidden;
	padding: 0;
	margin: 0;
	display: inline-block;
	width: 100%;
}

.list-group-item:before, .formInfo li::before{ display:none}
.boxSection25 ul li {
	padding: 0 5px;
	float: left;
	margin: 0;
}

.chatButton {
	text-decoration: none;
	padding: 5px 15px;
	color: #fff;
	background: #333 !important;
	text-align: center;
	border-radius: .25rem;
	display: block;
}

.chatButton:hover {
	text-decoration: none;
	color: #FFF;
	background: #e23751 !important;
}

.Skillset {
	display: block;
	overflow: hidden
}

.Skillset ul {
	list-style: none;
	text-align: center;
	padding: 0;
	margin: 0 !important;
	overflow: auto; height: 96px;
}

.Skillset ul li {
	padding: 5px;
	float: left;
	margin: 0 5px 5px 0;
	border: solid 1px #eee;
	font-size: 14px;
}

.team_search img {
	width: 100px!important;
	height: 100px;
	border-radius: 50%;
	margin: 0 auto
}

.form-control-placeholder {
	left: 0px
}

.border_top_bottom {
	border-top: solid 1px #eee;
	width: 100%;
	padding: 15px 0;
	display: block;
	margin: 20px 0 0;
	clear: both
}

.form-group .form-control {
	height: 100px !important;
	resize: none;
}

.inner-box {
	width: 100%;
	min-height: 308px;
	float: left;
}

#more_details {display: none;}
.inner-box .img-fluid {
	height: 255px;
	width: 100%;
}

.mt-02{margin-top:47px}

a.skill-more{background:none; font-size: 14px; float: right;}
a.skill-less{background:none;font-size: 14px; float: right;}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
        <div class="container">
            <h1 class="wow fadeInUp" data-wow-delay="0.1s">Search For Existing Teams </h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb wow fadeInUp">
                    <li class="breadcrumb-item"><a href="#">About BYT</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Search for Existing Teams</li>
                </ol>
            </nav>
        </div>
        <!--/end container-->
    </div>
    <div class="filterBox">
        <section class="search-sec">
            <form method="post" id="filterData" name="filterData">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="mySidenav" class="sidenav">
                                ` <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>×</strong></a>
                                <div class="scroll">
                                    <ul>
										<li> <strong> Select Skillset </strong> </li>
										<?php $i=0; 
											foreach($skill_set as $skills){
												
													if($i < 5){
														
											?>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills['id'] ?>" id="skillset" name="skillset[]" >
											<label class="form-check-label" for="defaultCheck1">
												<?php echo $skills['name'] ?>
											</label>
										</li>
										<?php }
											$i++;
										} ?>
										<span id="more_details">
										<?php $i=0; foreach($skillmore as $skills_add){
											if($i >= 5){
										?>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills_add['id'] ?>" id="skillset" name="skillset[]" >
											<label class="form-check-label" for="defaultCheck1">
												<?php echo $skills_add['name'] ?>
											</label>
										</li>
											<?php } $i++; } ?>
										</span>
										<?php if(count($skillmore) > 5): ?>
										<a href="javascript:void(0);" class="skill-more" id="skill-readmore">Show More >> </a>
										<a href="javascript:void(0);" class="skill-less" style="display:none;"  id="skill-readless">Show Less >></a>
										<?php endif; ?>
									</ul> 

                                    <!--<ul>
                                        <li> <strong> Select Domain Expertise </strong> </li>
                                        <li>
                                            <input class="form-check-input search-value" type="checkbox" value="159" id="domain_exp" name="domain_exp[]">
                                            <label class="form-check-label floatinglabel" for="defaultCheck1">
                                        ADAS								</label>
                                        </li>                                        
                                        <li>
                                    <input class="form-check-input search-value" type="checkbox" value="92" id="skillset" name="skillset[]">
                                    <label class="form-check-label floatinglabel" for="defaultCheck1">
                                        xEV Sizing &amp; Development								</label>
                                </li>
                                                                <li>
                                    <input class="form-check-input search-value" type="checkbox" value="209" id="skillset" name="skillset[]">
                                    <label class="form-check-label floatinglabel" for="defaultCheck1">
                                        Other								</label>
                                </li>
                                                                </span>
                                        <a href="javascript:void(0);" class="domain-more" id="domain-readmore">Show More &gt;&gt; </a>
                                        <a href="javascript:void(0);" class="domain-less" style="display:none;" id="domain-readless">Show Less &gt;&gt;</a>
                                    </ul>-->

                                    
                                </div>
                            </div>
                            <div class="row d-flex justify-content-center search_Box">

                                <div class="col-md-6">


                                    <input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword">
                                    <button type="button" class="btn btn-primary searchButton"><i class="fa fa-search" aria-hidden="true"></i></button>


                                </div>
                                <div class="clearAll"><a class="btn btn-primary clearAll" id="reset-val">Clear All</a>
                                </div>
                                <div class="filter"><button type="button" onclick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
                                </div>

                                <div class="col-md-12"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>


    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="formInfo">
                        <div class="col-sm-12 justify-content-center mt-4 team-box">
                            <div class="boxSection25 ">
								<?php if(count($team_details) > 0){ ?>
								
								<?php foreach($team_details as $teamslist){ 
								
									$slotDecryption =  New Opensslencryptdecrypt();	
									$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $teamslist['c_id']));
									
									// Team Slot Details
									$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $teamslist['c_id'], 'team_id'=> $teamslist['team_id'], 'is_deleted' => '0'));
									$maxTeam = $slotDecryption->decrypt($getSlotMaxMember[0]['max_team']);
									$minTeamCnt = count($slotDetails);
									$availableCnt = $maxTeam - $minTeamCnt;
									
									$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $teamslist['c_id'], 'team_id' => $teamslist['team_id'], 'is_deleted' => '0'));
									//echo $this->db->last_query(); exit;
									
									$totalSlotCnt = $totalPendingCnt = 0;
									foreach($getAllSlotTeam as $res)
									{
										if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
										$totalSlotCnt++;
									}
									
									$array_skills = array();
									foreach($slotDetails as $slot_list){
										array_push($array_skills, $slot_list['skills']);							
									}
									
									$combine = implode(",",$array_skills);
									$xp = explode(',',$combine);
									$arrayUnique = array_unique($xp);
									$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $teamslist['c_id']));
									$dCha = $slotDecryption->decrypt($chDetails[0]['challenge_details']);
								
									if($teamslist['team_banner']!=""){
										$banner_team = base_url('uploads/byt/'.$teamslist['team_banner']);
									} else {
										$banner_team = base_url('assets/no-img.png');
									}
								?>
                                <ul class="list-group list-group-horizontal counts">
                                    <li class="list-group-item">
                                        <div class="inner-box" style="border-bottom:none">
                                           <img src="<?php echo $banner_team; ?>" alt="Banner" class="img-fluid">
											<h3 class="minheight" style="font-size:16px;"><?php echo ucfirst($teamslist['team_name']) ?></h3>
                                            <ul>
                                                <li> <?php echo $totalSlotCnt ?> Members </li> 
												<li>-</li>
												<li><?php echo $totalPendingCnt; ?> Available Slots</li>
                                            </ul>
                                        </div>
                                        <!--<div class="border_top_bottom">
                                            <a href="javascript:void(0);" class="d-inline-block chatButton chat-popup">Chat</a>
                                        </div>-->
                                    </li>
                                    <li class="list-group-item">
                                        <div class="inner-box">
										<strong style="display:block; text-align:left; margin:15px 0 0px 0px;">Brief Info about Challenge </strong>
										<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0"><?php echo substr($teamslist['brief_team_info'],0,90).'...<a href="javascript:void(0)" class="click-more" data-id="'.$teamslist['team_id'].'">View More</a>'; ?></p>

                                            <h4>Looking For</h4>
                                            <div class="Skillset text-center">
                                                
                                                    <?php
														$s = 0;	
														foreach($arrayUnique as $commonSkill){
															if($s < 9){
															$skill_now = $this->master_model->getRecords('skill_sets',array('id'=> $commonSkill));
															$skill_names = $slotDecryption->decrypt($skill_now[0]['name']);												
															?>
															<span class="badge badge-pill badge-info"><?php echo $skill_names; ?></span>
														<?php }
															$s++;
														}
														?>
                                               
                                            </div>
                                        </div>
                                        <!--<div class="border_top_bottom">
                                            <a href="<?php echo base_url('myteams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>" class="btn btn-primary mt-02 w-100">View Details</a>
                                        </div>-->
                                    </li>
                                    <li class="list-group-item" style="height: 0 !important;">
                                        <div class="team_search owl-carousel owl-theme">
											<?php	
											$TeamSr = 1;
											$m = 1;
									foreach($slotDetails as $slotD){
										
										$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id']));
										
										if(count($getSlotDetail) > 0){
											
											$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
											$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
												
											$catID = $getUserDetail[0]['user_category_id'];	
											$getNames = $slotDecryption->decrypt($getUserDetail[0]['first_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['middle_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['last_name']); //." ".$getUserDetail[0]['user_id']
											$fullname = ucwords($getNames);
											$skillsets = $slotD['skills'];
											$span = '';
											if($skillsets!=""){																
												if($skillsets!="0"){
													
													$expSkillset = explode(",", $skillsets);
													$s=1;
													foreach($expSkillset as $getSkillname){
														if($s <=6)
														{																			
															$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
															$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
															$span .= '<span class="badge badge-pill badge-info">'.$get_skill_name.'</span>';																			
														}
														$s++;																		
													}																	
												}																
											} 
											
											
											if($catID == 1){
												
												$this->db->select('profile_picture, skill_sets_search');
												$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
												
												$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
												if ($getProfileDetail[0]['profile_picture']!="") {
													$imageUser = $imgPath;
												} else {
													$imageUser = base_url('assets/no-img.png');
												}										
												
											} else if($catID == 2){
												//$span = '';
												//echo ">userID>".$getUserDetail[0]['user_id'];
												$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
												
												if(count($getOrgDetail) > 0){
													
													$profilePic = $slotDecryption->decrypt($getOrgDetail[0]['org_logo']);
													$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
													if ($profilePic!="") {
														$imageUser = $imgPath;
													} else {
														$imageUser = base_url('assets/no-img.png');
													}
													
												}
												
											}								
											
											if($getSlotDetail[0]['apply_user_id'] == $teamslist['user_id']){
												$styles_hide = "hide-class";
											} else {
												$styles_hide = "d-inline-block";
											}
											
										} else {
															
											$imageUser = base_url('assets/no-img.png');
											$styles_hide = "d-inline-block";
											$fullname = "Open Slot";											
											$skillsets = $slotD['skills'];
											$span = '';
											if($skillsets!=""){
												
												if($skillsets!="0"){
													
													$expSkillset = explode(",", $skillsets);
													$s=1;
													foreach($expSkillset as $getSkillname){
														if($s <=6){
															
															$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
															$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
															$span .= '<span class="badge badge-pill badge-info">'.$get_skill_name.'</span> ';																		
														}
														$s++;
														
													}
													
												}
												
											} 		
											
										} // Else End
						
									?>
                                           <div class="item text-center">
												<img src="<?php echo $imageUser; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $fullname; ?>"  />
												<h4><?php echo $fullname; ?></h4>
												<div class="Skillset text-center">
													<?php echo $span; ?>
												</div>
												<h4>Team Member <?php echo $TeamSr; ?></h4> 
												<?php /*if(@$getSlotDetail[0]['status']!='Approved'): ?>
												<a href="javascript:void(0);" class="<?php echo $styles_hide; ?> apply-popup">Apply</a>
												<?php endif;*/ ?>
											</div>
								
									<?php $m++; $TeamSr++; } ?>
                                        </div>
                                    </li>
									
									<div class="container" style="clear:both;" >
									<div class="row buttonView mb-3">
										<div class="col-md-3">
										<a href="javascript:void(0);" class="btn btn-primary w-100">Chat</a>
										</div>
										<div class="col-md-6">
										<a href="<?php echo base_url('myteams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>" class="btn btn-primary w-100">View Details</a>
										</div>
										<div class="col-md-3">
										<a href="<?php echo base_url('myteams/applySlot/').base64_encode($teamslist['team_id'])."/".base64_encode($teamslist['c_id']) ?>" class="btn btn-primary w-100">Apply</a>
										</div>
									</div>
									</div>
									
									
									
                                </ul>
								<?php 		} // Foreach End ?>
								
								 <?php if(count($team_cnts) > 6){ ?>
								<div class="row justify-content-center mt-4  show_more_main" id="show_more_main<?php echo $teamslist['team_id']; ?>">
									<span id="<?php echo $teamslist['team_id']; ?>" class="show_more btn btn-general btn-white" title="Show More">Show more</span>
									<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
								</div>
								<?php 		} ?>
								<?php } else {  ?>
								
								<p> No Team Available </p>
								
								<?php } // If End ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="modal fade" id="briefInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Brief Info about Challenge</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>	
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script type="text/javascript">


$(document).on('click','.apply-popup',function(){
		
		swal({
			title:"",
			text: "Comming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
		
	});
	
	$(document).on('click','.chat-popup',function(){
		
		swal({
			title:"",
			text: "Comming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
		
	});
   
$(document).ready(function () { 
   
	$('.team_search').owlCarousel({
		items: 1,
		autoplay: true,
		smartSpeed: 700,
		loop: false,
		nav: true,
		dots: false,
		navText: ["", ""],
		autoplayHoverPause: true
	});
   
   
//$('#preloader-loader').css('display', 'block');		

$(document).on('click','.skill-more',function(){
	$('#more_details').css('display', 'block');
	$('.skill-less').css('display', 'block');
	$('.skill-more').css('display', 'none');
});

$(document).on('click','.skill-less',function(){
	$('#more_details').css('display', 'none');
	$('.skill-less').css('display', 'none');
	$('.skill-more').css('display', 'block');
});	

$(document).on('click','.domain-more',function(){
	$('#more_results').css('display', 'block');
	$('.domain-less').css('display', 'block');
	$('.domain-more').css('display', 'none');
});

$(document).on('click','.domain-less',function(){
	$('#more_results').css('display', 'none');
	$('.domain-less').css('display', 'none');
	$('.domain-more').css('display', 'block');
});

$(document).on('click','.employement-more',function(){
	$('#more_emp').css('display', 'block');
	$('.employement-less').css('display', 'block');
	$('.employement-more').css('display', 'none');
});

$(document).on('click','.employement-less',function(){
	$('#more_emp').css('display', 'none');
	$('.employement-less').css('display', 'none');
	$('.employement-more').css('display', 'block');
});

 

$(document).on('click','.exp-more',function(){
	$('#more_exp').css('display', 'block');
	$('.exp-less').css('display', 'block');
	$('.exp-more').css('display', 'none');
});

$(document).on('click','.exp-less',function(){
	$('#more_exp').css('display', 'none');
	$('.exp-less').css('display', 'none');
	$('.exp-more').css('display', 'block');
});	

// Load More Code 
$(document).on('click','.show_more',function(){
	$('#preloader-loader').css('display', 'block');	
	var base_url = '<?php echo base_url(); ?>'; 
	var ID = $(this).attr('id');
	var length = $('.counts').length;
	
	var cs_t = 	$('.token').val();
	$('.show_more').hide();
	
	$.ajax({
		type:'POST',
		url: base_url+'myteams/load_more_teams',
		data:'id='+length+'&csrf_test_name='+cs_t,
		dataType:"html",
		async:false,
		success:function(response){ 
			$('#preloader-loader').css('display', 'none');	
			var output = JSON.parse(response);
			if(output.html == ""){
				$(".show_more_main").remove();
			}	
			$(".token").val(output.token);								
			$('#show_more_main'+ID).remove();				
			$('.formInfo').append(output.html);
			$(".team_search").owlCarousel({
				items: 1,
				autoplay: true,
				smartSpeed: 700,
				loop: false,
				nav: true,
				dots: false,
				navText: ["", ""],
				autoplayHoverPause: true
			});	

		},
		 error: function (jqXHR, exception) { 
			var msg = '';
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			console.log(msg);
			//$('#post').html(msg);
		},
	});
});
	
$(document).on('click','.click-more',function(){		
	//var priorityVal = $(this).val();			
	var cid	=	$(this).attr('data-id');
	//alert(cid);return false;
	$('#briefInf').modal('show');		
	
	var base_url = '<?php echo base_url('challenge/viewDetails'); ?>';
	 $.ajax({
		url: base_url,
		type: "post",
		data: {id:cid},				
		success: function (response) {					
			$("#contents").html(response);				
		},
		error: function(jqXHR, textStatus, errorThrown) {
		   console.log(textStatus, errorThrown);
		}
	});
	
});	
	
// Filter Apply		
$(document).on('click','.searchButton',function(){

	$('#preloader-loader').css('display', 'block');	
	var cs_t 			=  $('.token').val();
	var base_url 		= '<?php echo base_url(); ?>'; 
	var skillset 		= $('#skillset').val();
	var keyword 		= $('#search_txt').val();
	/*var domain_exp 		= $('#domain_exp').val();
	var emp_status 		= $('#emp_status').val();
	var year_of_exp 	= $('#year_of_exp').val();
	var no_of_pub 		= $('#no_of_pub').val();
	var no_of_patents 	= $('#no_of_patents').val();*/
	
	//console.log(keyword);
	$.ajax({
		type:'POST',
		url: base_url+'myteams/team_filter',
		data: $('#filterData').serialize(),
		dataType:"text",
		//async:false,
		success:function(response){ 
			//console.log(response);					
			var output = JSON.parse(response);					
			$(".token").val(output.token);
			$('.formInfo').html(output.html); 
			$('#preloader-loader').css('display', 'none');
			$(".team_search").owlCarousel({
				items: 1,
				autoplay: true,
				smartSpeed: 700,
				loop: false,
				nav: true,
				dots: false,
				navText: ["", ""],
				autoplayHoverPause: true
			});	

		},
		 error: function (jqXHR, exception) { 
		  $('#preloader-loader').css('display', 'none');
			var msg = '';
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			console.log(msg);
			//$('#post').html(msg);
		},
	});
});
	
	
$(document).on('keydown','.search-slt',function(){
	$('#preloader-loader').css('display', 'block');
	var cs_t 			=  $('.token').val();
	var base_url 		= '<?php echo base_url(); ?>'; 
	var skillset 		= $('#skillset').val();
	var keyword 		= $('#search_txt').val();
	/*var domain_exp 		= $('#domain_exp').val();
	var emp_status 		= $('#emp_status').val();
	var year_of_exp 	= $('#year_of_exp').val();
	var no_of_pub 		= $('#no_of_pub').val();
	var no_of_patents 	= $('#no_of_patents').val();
	var keyword 		= $('#search_txt').val();*/
	//console.log(keyword);
	$.ajax({
		type:'POST',
		url: base_url+'myteams/team_filter',
		data: $('#filterData').serialize(),
		dataType:"text",
		//async:false,
		success:function(response){ 
			//console.log(response);
			
			var output = JSON.parse(response);					
			$(".token").val(output.token);
			$('.boxSection25').html(output.html);
			$('#preloader-loader').css('display', 'none');
			$(".team_search").owlCarousel({
				items: 1,
				autoplay: true,
				smartSpeed: 700,
				loop: false,
				nav: true,
				dots: false,
				navText: ["", ""],
				autoplayHoverPause: true
			});		

		},
		 error: function (jqXHR, exception) { 
			var msg = '';
			$('#preloader-loader').css('display', 'none');
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			//console.log(msg);
			//$('#post').html(msg);
		},
	});
});
	
	
$(document).on('click','.search-value',function(){
	$('#preloader-loader').css('display', 'block');
	var cs_t 			=  $('.token').val();
	var base_url 		= '<?php echo base_url(); ?>'; 
	var skillset 		= $('#skillset').val();
	var keyword 		= $('#search_txt').val();
	/*var domain_exp 		= $('#domain_exp').val();
	var emp_status 		= $('#emp_status').val();
	var year_of_exp 	= $('#year_of_exp').val();
	var no_of_pub 		= $('#no_of_pub').val();
	var no_of_patents 	= $('#no_of_patents').val();*/
	
	//console.log(keyword);
	$.ajax({
		type:'POST',
		url: base_url+'myteams/team_filter',
		data: $('#filterData').serialize(),
		dataType:"text",
		//async:false,
		success:function(response){ 
			//console.log(response);
			
			var output = JSON.parse(response);					
			$(".token").val(output.token);
			$('.boxSection25 ').html(output.html);
			$('#preloader-loader').css('display', 'none');
			$(".team_search").owlCarousel({
				items: 1,
				autoplay: true,
				smartSpeed: 700,
				loop: false,
				nav: true,
				dots: false,
				navText: ["", ""],
				autoplayHoverPause: true
			});		

		},
		 error: function (jqXHR, exception) { 
			var msg = '';
			$('#preloader-loader').css('display', 'none');
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			//console.log(msg);
			//$('#post').html(msg);
		},
	});
});
	
$(document).on('click','a#reset-val',function()
{
	 location.reload(); 
	/* $('#preloader-loader').css('display', 'block');
	//alert();
	$("#filterData").trigger("reset");
	var cs_t 			=  $('.token').val();
	var base_url 		= '<?php echo base_url(); ?>'; 
	$.ajax({
		type:'POST',
		url: base_url+'myteams/teamSearch',
		data: $('#filterData').serialize(),
		dataType:"text",
		//async:false,
		success:function(response){ 
			//console.log(response);
			
			var output = JSON.parse(response);					
			$(".token").val(output.token);
			$('.boxSection25').html(output.html); 
			$('#preloader-loader').css('display', 'none');	

		},
		 error: function (jqXHR, exception) { 
			var msg = '';
			$('#preloader-loader').css('display', 'none');
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			//console.log(msg);
			//$('#post').html(msg);
		},
	}); */
});

$('#myModal').modal('show');

});
$('.select2').select2({});

function openNav() {
	
 var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
		isMobile = true;
		document.getElementById("mySidenav").style.width = "100%";
	} else {
		document.getElementById("mySidenav").style.width = "25%";
	}
 
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";

}


(function($) {

    $(window).scroll(function() {

        if ($(this).scrollTop() < 61) {
            // hide nav
            $("nav").removeClass("vesco-top-nav");
            $("#flotingButton").fadeOut();

        } else {
            // show nav
            $("nav").addClass("vesco-top-nav");
            $("#flotingButton").fadeIn();
        }
    });
})(jQuery); // End of use strict
</script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>          