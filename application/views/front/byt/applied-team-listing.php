<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
	.guideline-tooltip{
	position: relative;
	display: inline-block;
	margin-left: 10px;
	}
	.guideline-tooltip:hover p {
  opacity: 1;
  -webkit-transform: translate(-35%, 0);
	transform: translate(-35%, 0);
	margin-top: 10px;
	visibility: visible;
	}
	.guideline-tooltip p {
	position: absolute;
	left: 50%;
	top: 100%;
	opacity: 0;
	padding: 1em;
	background-color: #e7f0ff;
	font-size: 14px;
	line-height: 1.6;
	text-align: left;
	white-space: nowrap;
	-webkit-transform: translate(-35%, 1em);
	transform: translate(-35%, 1em);
	-webkit-transition: all 0.15s ease-in-out;
	transition: all 0.15s ease-in-out;
	color: #000;
	z-index: 99;
	font-weight: 400;
	visibility: hidden;
	}
	.guideline-tooltip p::before {
	content: '';
	position: absolute;
	top: -16px;
	left: 33%;
	width: 0;
	height: 0;
	border: 0.6em solid transparent;
	border-top-color: #e7f0ff;
	transform: rotate(180deg);
	}
	.formInfo li::before{display:none;}
</style>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Listing</h1>
		<!-- <nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
			<li class="breadcrumb-item"><a href="#">Registration</a></li>
			<li class="breadcrumb-item active" aria-current="page">Registration </li>
			</ol>
		</nav> -->
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">			
			<div class="col-md-12">
				
				<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<div class="formInfo">		
					
					
					
					<!--################## ADD TWO TABS HERE : My Formed Teams, Teams I Applied To ######################-->
					<section>
						<div class="product_details">
							<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link " href="<?php echo base_url('myteams/myCreatedteams'); //#TeamTab1 ?>">Teams I have formed</a></li>
								<li class="nav-item"><a class="nav-link active"  href="javascript:void(0)">Teams I have applied to</a></li>
							</ul>
							
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane" id="TeamTab1">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-md-12">
												Test										
											</div>
										</div>
									</div>
								</div>
								
								<div class="tab-pane active" id="TeamTab2">
									<div class="container">
										<div class="row justify-content-center">             
											<div class="col-md-12">
												<div class="table-responsive">
													<table id="apply-team-listing" class="table table-bordered table-hover challenges-listing-page">
														<thead>
															<tr>
																<th scope="col">Team ID</th>
																<!--<th scope="col">Application Ref Id</th>-->
																<th scope="col">My Application Status</th>
																<th scope="col">Challenge Association</th>
																<th scope="col">Challenge/Task Name</th>
																<th scope="col">Team size</th>
																<th scope="col">Team Status</th>
																<th scope="col">Challenge Application Status</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>							
														</tbody>
													</table>
												</div>      
											</div>
										</div>
									</div>
								</div>    
							</div>
						</div>
					</section>					
					<!--########################################-->	
				</div>    
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="c_id" id="c_id" value="<?php //echo $c_id ?>" />
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<script>
	
	//$("body").on("click", "#apply-team-listing tbody tr a.remove-app", function (e) 
	function withdrawn_application(app_id)
	{
		swal(
		{
			title:"Withdraw?" ,
			text: "You will not be able to apply to this team again. Are you sure you want to withdraw from this Team?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				var id = app_id; //$(this).attr('data-id');
				var csrf_test_name	= $('.token').val();			
				var base_url = '<?php echo base_url('myteams/removeMemberrequest'); ?>';
				$.ajax(
				{
					url: base_url,
					type: "post",
					data: {id:id,csrf_test_name:csrf_test_name},				
					success: function (response) {
						
						var returndata = JSON.parse(response);
						var token = returndata.token;			
						$('.txt_csrfname').val(token);		
						$('#apply-team-listing').DataTable().ajax.reload();
						swal({
							title: 'Success!',
							text: "You have successfully withdrawn from team",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
					}
				});
			}
		})
	}	
	
  $(document).ready( function () {	
		
		//$(".open-details").on("click", function () {	
		$("body").on("mouseover", "#team-listing tbody tr .open-details", function (e) { 
			var totalCnt = $(this).attr('data-total');
			var acceptedCnt = $(this).attr('data-accepted');
			$(".popup").show();
			$('#GFG_Span').text(totalCnt); 
			
		});
		
		$("body").on("mouseout", "#team-listing tbody tr .open-details", function (e) { 			
			$(".popup").hide();		
		});
		
		var base_path = '<?php echo base_url() ?>';
		var table = $('#apply-team-listing').DataTable({
			
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": base_path+"myteams/appliedChallengeListing",
				"type":"POST",
				"data":function(data) {	
					
					data.c_id 				= $("#c_id").val();
					/*data.change_visibility 		= $("#change_visibility").val();
						data.ip_clause 				= $("#ip_clause").val();
						data.fund_sel 				= $("#fund_sel").val();
						data.reward_sel 			= $("#reward_sel").val();
					data.trl_id 				= $("#trl_id").val();*/
					
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": {
					401:function(responseObject, textStatus, jqXHR) {			
					},
				},
			}
			
		});
		
		$('.btn-click').click(function(){
			var techonogy_id 		= $('#techonogy_id').val();
			var change_visibility 	= $('#change_visibility').val();	
			var ip_clause 			= $('#ip_clause').val();
			var fund_sel 			= $('#fund_sel').val();
			var reward_sel 			= $('#reward_sel').val();
			table.ajax.reload();		
		});
		
		$("#btnReset").on("click", function () {			
			$("#techonogy_id").val([]).change();
			//$("#audience_pref").val([]).change();
			//$("#c_status").val([]).change();
			$("#ip_clause").val([]).change();
			$("#fund_sel").val([]).change();
			$("#reward_sel").val([]).change();
			$("#trl_id").val([]).change();
			$("#change_visibility").val([]).change();					
			table.ajax.reload();	
		});
		
		
		
		$('.select2').select2({});
		
	});
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>