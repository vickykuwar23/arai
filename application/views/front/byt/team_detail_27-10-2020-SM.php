<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<style>
	.home-p.pages-head3 {
	background: url(<?php echo base_url() ?>assets/img/aboutus.jpg) no-repeat center top !important;
	background-size: cover !important;
	}
	
	ul.file_attachment_outer_common {
	list-style: none;
	margin: 0;
	padding: 0;
	}
	
	ul.file_attachment_outer_common li:before { display:none; }
	
	ul.file_attachment_outer_common li {
	padding: 3px;
	margin: 0 5px 10px 0;
	width: 80px;
	height: 60px;
	border: 1px solid #ccc;
	border-radius: 5px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
	}
	
	ul.file_attachment_outer_common li a h4 {
	margin: 0;
	line-height: 55px;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	font-size: 15px !important;
	}
	
	ul.file_attachment_outer_common li a img
	{
	max-width:100%;
	max-height:100%;
	}
	.home-p.pages-head4 {	
	background-size: 100%; min-height: 100%;
	}
	
.customDetailView
{
	height: auto;
	margin: 0;
	padding: 10px 0 5px 0 !important;
	text-align:justify;
}

span.badge.badge-info { white-space: normal; }
</style>

<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down"
style="background: url(<?php if($team_details[0]['team_banner']!=''){echo base_url('uploads/byt/').$team_details[0]['team_banner'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size:100% 100% !important;" 
>
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Details</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="#">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Team Details</li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">
						<div class="col-md-12">
							<div class="details-table">
								<table class="table table-bordered">
									<tr>
										<td width="30%"><label>Team ID & Name</label></td>
										<td><?php echo $team_details[0]['custom_team_id']." - " .$team_details[0]['team_name'] ?></td>
									</tr>
									<tr>
										<td><label>Challenge ID & Name</label></td>
										<td><?php echo $team_details[0]['challenge_id']." - " .$encrptopenssl->decrypt($team_details[0]['challenge_title']) ?></td>
									</tr>
									<tr>
										<td><label>Brief About Team</label></td>
										<td><?php echo $team_details[0]['brief_team_info']; ?></td>
									</tr>
									<tr>
										<td><label>Proposed Approach </label></td>
										<td><?php echo $team_details[0]['proposed_approach']; ?></td>
									</tr>
									<?php if ( count($team_files) && $team_details[0]['share_files'] == 'yes'): ?>
									
									<tr>
										<td><label>Files </label></td>
										<td>
											<div class="file-details">
												<div class="row">
													<div class="col-md-12">
														
														<?php 
															if(count($team_files) > 0){ ?>
															
															<ul class="file_attachment_outer_common">
																<?php 
																	foreach ($team_files as $key => $file) 
																	{ ?>
																	<li>
																		<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
																			<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
																				if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
																			else { $disp_img_name = ''; } ?>
																			
																			<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
																			else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
																		</a>
																	</li>
																<?php } ?>
															</ul>
															
														<?php	}?>
														
														<div class="col-md-12 text-center">
															<a href="#" class="btn btn-primary btn-sm mt-3 d-none">Download Files</a>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<?php endif ?>
								</table>
							</div>
						</div>
						
						
						<div id="teamSlider" class="owl-carousel owl-theme">
							
							<?php foreach ($team_slots as $key => $slot) { 
								
								$this->db->select('registration.user_id,title,first_name,last_name');
								
								if($_SESSION['user_category'] == 1)
								{
									$this->db->select('profile_picture');
									$this->db->join('student_profile','student_profile.user_id=byt_slot_applications.apply_user_id','LEFT');
								}
								else
								{
									$this->db->select('org_logo');
									$this->db->join('arai_profile_organization','arai_profile_organization.user_id = byt_slot_applications.apply_user_id','LEFT');
								}
								
								$this->db->join('registration','registration.user_id=byt_slot_applications.apply_user_id');
								$aproved_member   = $this->master_model->getRecords("byt_slot_applications",array('slot_id'=>$slot['slot_id'],'byt_slot_applications.status'=>'Approved' ));	
								
								$skill=$slot['skills'];	
								$this->db->where("id IN (".$skill.")",NULL, false);
								$skills   = $this->master_model->getRecords( "arai_skill_sets",'','name');
								
								
								
							?>
							
							<div class="mt-4">
								<div class="boxSection26">
									
									<div class="text-center">
										
										
										
										
										<?php 
											if($_SESSION['user_category'] == 1)
											{
												if (isset($aproved_member[0]['profile_picture']) &&  $aproved_member[0]['profile_picture'] !='' ) 
												{ ?>
												<img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $aproved_member[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
												<?php }
												else
												{ ?>
												<img src="<?php echo base_url('assets/profile_picture/user_dummy.png'); ?>" alt="experts">
												<?php }
											}
											else if($_SESSION['user_category'] == 2)
											{
												if (isset($aproved_member[0]['org_logo']) &&  $aproved_member[0]['org_logo'] !='' ) 
												{ ?>
												<img src="<?php echo base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($aproved_member[0]['org_logo'])); ?>" alt="" class="img-fluid bor">
												<?php }
												else
												{ ?>
												<img src="<?php echo base_url('assets/profile_picture/user_dummy.png'); ?>" alt="experts">
												<?php }
											} ?>
											
											
									</div>
									<h3>
										<?php 
											if (count($aproved_member)) {
												echo $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
											} else {echo "-";} ?>
									</h3>
									<h3 class="mb-2"><?php echo $slot['name']; ?></h3>
									<div class="Skillset">
										<?php 
											if (count($skills)) {
												foreach ($skills as $key => $value) { ?>
												
												<span class="badge badge-pill badge-info"><?php echo $encrptopenssl->decrypt( $value['name'] ); ?></span>
												
											<?php } }
										?>
									</div><br>
									<strong style="font-weight:500;">Role in Team</strong>
									<p class="form-control customDetailView" style="height: 55px;border-left: none;border-top: none;border-right: none;border-radius: 0;padding-top: 0 !important;"><?php echo $slot['role_name']; ?></p>									
								</div>
							</div>
							
							
							<?php } ?>
							
						</div>
						
						
						
						<div class="col-md-12">
							<div class="team_view_listing mt-4">
								<a href="#" class="pull-left chatButton d-none">Chat</a>
								<a href="#" class="pull-right d-none">Apply</a>
							</div>
						</div>
					</div>
					<?php if ($team_details[0]['u_id']==$this->session->userdata('user_id') ) { ?>
						<div class="row mt-5">
							<div class="col-md-12">
								<div class="text-center">
									<a href="javascript:void(0)" id="accept_team" data-approval='accept' data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary ch_owner_approval">Accept</a>
									
									<a href="javascript:void(0)" id="reject_team" data-approval='reject' data-id='<?php echo $team_details['team_id'] ?>' class="btn btn-primary ch_owner_approval">Reject</a>								
								</div>
							</div>
						</div>
						
					<?php } ?> 
					<div class="row mt-3">	
						<div class="col-md-6">	
							<div class="form-group mt- d-none" id="reject_reason_div">
								<input type="text" id="reject_reason" class="form-control"  value="" maxlength="250">
								<label class="form-control-placeholder" style="left:0px;" for="reject_reason">Rejection Reason </label>
								<span class="error" id=err_reason></span>
							</div>
						</div>	
					</div>		
					
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('.ch_owner_approval').click(function(){
		var appr=$(this).data('approval');
		var reject_reason = '';
		$("#err_reason").html('');
		if (appr=='reject') {
			$("#reject_reason_div").removeClass('d-none');
			reject_reason = $("#reject_reason").val();
			if (reject_reason=='') {
				$("#err_reason").html('Please enter reason');
				return false;
			}
			
			}else{ 
			$("#reject_reason_div").addClass('d-none')
		}
		
		
		swal(
		{
			title:"Confirm?" ,
			text: "Are you sure you want to "+ appr + " this team?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				
				
				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
				
				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
				
				
				var b_url= "<?php echo base_url();?>";
				var c_id = '<?php echo base64_encode($team_details[0]["c_id"]); ?>';
				var datastring='reject_reason='+ reject_reason + '&appr='+ appr + '&team_id='+ team_id + '&' + csrfName + '='+csrfHash;
				$.ajax({
					type: 'POST',
					data:datastring,
					url: "<?php echo base_url();?>myteams/challenge_owner_approval",
					beforeSend: function(){
						$('#preloader-loader').css("display", "block");
					},
					success: function(res){ 
						
						
						if(res=='success'){
							
							swal({
						    title: "Success!",
						    text: "Team "+appr+"ed Successfully!",
						    type: "success"
								}).then(function() {
						    location.href = b_url+'challenge/teamListing/'+c_id;
							});
							
							}else{
							swal( 'Error!','Something went wrong!','error');
						}
					},
					complete: function(){
						$('#preloader-loader').css("display", "none");
					},
					
				});
				
			}
		} ) 	
		
	})
</script>				

