<style>
.file_logo {
   
    font-size: 42px;
    color: #FFC107;
    height: 50px;
}

#preloader-loader {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
z-index: 9999;
overflow: hidden;
background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
content: "";
position: fixed;
top: calc(50% - 30px);
left: calc(50% - 30px);
border: 6px solid #f2f2f2;
border-top: 6px solid #c80032;
border-radius: 50%;
width: 60px;
height: 60px;
-webkit-animation: animate-preloader 1s linear infinite;
animation: animate-preloader 1s linear infinite;
}

ul.file_attachment_outer_common {
	list-style: none;
	margin: 0;
	padding: 0;
}

ul.file_attachment_outer_common li:before { display:none; }

ul.file_attachment_outer_common li {
	padding: 3px;
	margin: 0 5px 10px 0;
	width: 80px;
	height: 60px;
	border: 1px solid #ccc;
	border-radius: 5px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
}

ul.file_attachment_outer_common li a h4 {
	margin: 0;
	line-height: 55px;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	font-size: 15px !important;
}

ul.file_attachment_outer_common li a img
{
	max-width:100%;
	max-height:100%;
}
.home-p.pages-head4 {	
background-size: 100%; min-height: 100%;
}

.customDetailView
{
	height: auto;
	margin: 0;
	padding: 10px 0 5px 0 !important;
	text-align:justify;
}

.receivedApplicationCntOuter 
{
	position: absolute;
	z-index: 999;
	width: 25px;
	height: 25px;
	background: #e23751;
	color: #fff;
	font-weight: 600;
	font-size: 12px;
	line-height: 23px;
	text-align: center;
	border-radius: 50%;
	border: 1px solid #000;
	right:90px;
}
</style>


<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down"
style="background: url(<?php if($team_details[0]['team_banner']!=''){echo base_url('uploads/byt/').$team_details[0]['team_banner'];}else{echo base_url('assets/img/aboutus.jpg');} ?>) no-repeat center top; background-size: 100% 100% !important;" 
>
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Team Details</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('home/aboutByt'); ?>">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Team Details</li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">

						<?php if ($team_details[0]['team_banner']!='') { ?>
						<div class="col-md-4 d-none">
						<div class="img-box">
            			<img class="img-fluid"  src="<?php echo base_url('uploads/byt/').$team_details[0]['team_banner'] ?>" >
          				</div>
          				</div>
          				<?php } ?>

						<div class="col-md-12">
							<div class="form-group">
								<p class="form-control customDetailView"><?php echo $team_details[0]['custom_team_id']." - " .$team_details[0]['team_name'] ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Team ID & Name</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<p class="form-control customDetailView"><?php echo $team_details[0]['challenge_id']." - " .$encrptopenssl->decrypt($team_details[0]['challenge_title'])."" ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Challenge ID & Name</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['brief_team_info']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Brief about Team</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['proposed_approach']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Proposed Approach</strong></label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group mt-3">
								<p class="form-control customDetailView"><?php echo $team_details[0]['additional_information']; ?></p>
								<label class="form-control-placeholder floatinglabel"><strong>Additional Information</strong></label>
							</div>
						</div>

						<?php if(count($team_files) > 0) { ?>
							
						<div class="col-md-12">
							<p ><strong> Files & Attachments :</strong></p>
							<ul class="file_attachment_outer_common">
							<?php 
							foreach ($team_files as $key => $file) 
							{ ?>
								<li>
									<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
										<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
										if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
										else { $disp_img_name = ''; } ?>
										
										<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
										else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
									</a>
								</li>
						<?php } ?>
							</ul>
							<br>
						</div>

            			<div class="col-md-12">
							<strong style="display: block;">Share Files</strong>
							<label class="switch">
								 <input type="checkbox" id="share_files" <?php if($team_details[0]['share_files']=='yes'){echo 'checked';} ?> > 
								<span class="slider round" ></span>
							</label>
						</div>

						<?php } ?>
          	

          				  <div class="col-md-12">
							<div id="owner-teamSlider" class="owl-carousel mt-4 owl-theme">


								<?php $i=0;
								foreach ($team_slots as $key => $slot) 
								{ 
									$UserCategory = $this->master_model->getRecords('arai_registration',array('user_id'=>$slot['user_id']),'user_category_id');
									
									$this->db->select('registration.user_id,title,first_name,last_name');
									if($UserCategory[0]['user_category_id'] == 1)
									{
										$this->db->select('profile_picture');
										$this->db->join('student_profile','student_profile.user_id=byt_slot_applications.apply_user_id','LEFT');
									}
									else
									{
										$this->db->select('org_logo');
										$this->db->join('arai_profile_organization','arai_profile_organization.user_id = byt_slot_applications.apply_user_id','LEFT');
									}

									$this->db->join('registration','registration.user_id=byt_slot_applications.apply_user_id');
						$aproved_member   = $this->master_model->getRecords("byt_slot_applications",array('slot_id'=>$slot['slot_id'],'byt_slot_applications.status'=>'Approved' ));
									//echo $this->db->last_query();
						
									$DispUserName = '-';
									$slot_final_flag = $receivedApplicationCnt = 0;
									if(count($aproved_member) > 0) 
									{
										$DispUserName = $encrptopenssl->decrypt($aproved_member[0]['title'])." ".$encrptopenssl->decrypt($aproved_member[0]['first_name'])." ".$encrptopenssl->decrypt($aproved_member[0]['last_name']);
										$slot_final_flag = 1;
									}
									else
									{
										$receivedApplicationCnt = $this->master_model->getRecordCount( "arai_byt_slot_applications",array("slot_id"=>$slot['slot_id'], "is_deleted"=>'0', "status != "=>'Approved'));
									}

									$skill=$slot['skills'];	
									$this->db->where("id IN (".$skill.")",NULL, false);
						$skills   = $this->master_model->getRecords( "arai_skill_sets",'','name'); 
								?>

								<div class="owner-teammember boxSection25 text-center">
									<?php if($receivedApplicationCnt > 0) { echo "<span class='receivedApplicationCntOuter'>".$receivedApplicationCnt."</span>"; } ?>
								<?php 
								if($UserCategory[0]['user_category_id'] == 1)
								{
									if (isset($aproved_member[0]['profile_picture']) &&  $aproved_member[0]['profile_picture'] !='' ) 
									{ ?>
										<img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $aproved_member[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
						<?php }
									else
									{ ?>
										<img src="<?php echo base_url('assets/profile_picture/user_dummy.png'); ?>" alt="experts">
						<?php }
								}
								else if($UserCategory[0]['user_category_id'] == 2)
								{
									if (isset($aproved_member[0]['org_logo']) &&  $aproved_member[0]['org_logo'] !='' ) 
									{ ?>
										<img src="<?php echo base_url('uploads/organization_profile/'.$encrptopenssl->decrypt($aproved_member[0]['org_logo'])); ?>" alt="" class="img-fluid bor">
						<?php }
									else
									{ ?>
										<img src="<?php echo base_url('assets/profile_picture/user_dummy.png'); ?>" alt="experts">
						<?php }
								} ?>
									
									<h3><?php echo $DispUserName; ?></h3>

									<h3><?php echo $slot['name']; ?></h3>
									<p><strong>Skills</strong></p>
									<div class="boderBox newheight mb-4" style="text-align:left;">
										<?php 
										foreach ($skills as $key => $value) { ?>

											<span class="badge badge-pill badge-info"><?php echo $encrptopenssl->decrypt( $value['name'] ); ?></span>
										<?php } 
										?>
									</div>
									<div class="form-group mt-4">
										<p class="form-control customDetailView" style="height:65px"><?php echo $slot['role_name']; ?></p>
										<label class="form-control-placeholder floatinglabel" style="left:0px;"><strong>Role in Team</strong></label>
									</div>
									
									<div class="text-center">
										<?php 
										if($i > 0)
										{
											if($slot_final_flag == 1)
											{	?>
												<a href="javascript:void(0)" class="btn-sm">Remove Member</a>
												<a href="javascript:void(0)" class="d-none">Chat</a>
									<div class="border_top_bottom2 d-none">
													<a href="javascript:void(0)">View Details</a>
												</div>
											<?php }
											else if($receivedApplicationCnt > 0)
											{	?>
												<a href="<?php echo site_url('myteams/goHiring/'.base64_encode($slot['team_id'])."/".base64_encode($slot['slot_id'])); ?>" class="btn-sm">Go Hiring / View Applications</a>
											<?php }
										}	?>
									</div>
								</div>

							<?php $i++;
							} ?>


							</div>
						</div>
            		
            		<div class="col-md-12 mt-4">
							<strong style="display: block;">Is Your Team Complete?</strong>
							<label class="switch">
								<input type="checkbox" id="is_team_complete" <?php if($team_details[0]['team_status']=='Completed'){echo 'checked';} ?> disabled> 
								<span class="slider round" onclick="coming_soon_popup()"></span>
							</label>
					</div>

					<div class="col-md-12">
							<div class="team_view_listing2 mt-4">
                	<a href="<?php echo base_url('team/create/').base64_encode($team_details[0]['c_id'])."/".base64_encode($team_details[0]['team_id']); ?>" class="pull-left" style="margin-right: 10px;">Edit Team Profile </a>

                	<?php if($team_details[0]['apply_status']=='Withdrawn' || $team_details[0]['apply_status']=='Pending'){ ?>
						
								<a href="javascript:void(0)" id="apply_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="pull-left chatButton">
									Apply For A Challenge
								</a>
								
							<!-- 	<a href="javascript:void(0)" class="pull-left chatButton" onclick="coming_soon_popup()">
									Apply For A Challenge
								</a> -->
						<?php } ?>


						 <?php if($team_details[0]['apply_status']=='Applied'){ ?>
								<a href="javascript:void(0)" id="withdraw_for_challenge" data-id='<?php echo $team_details['team_id'] ?>' class="pull-left chatButton mobileTop">
									Withdraw Team
								</a>
						<?php } ?>

                <!-- <a href="#" class="pull-left chatButton">Apply for Challenge</a> -->
							</div>
						</div>
						<!-- <div class="col-md-12 mt-4">
							<div class="chatBg">
							<div class="form-group mt-3">
							<textarea class="form-control" id="exampleFormControlTextarea1" name="contents"></textarea>
							<label class="form-control-placeholder" for="exampleFormControlTextarea1">Chat Description </label>
							</div>
							</div>
						</div> -->
						<!-- <div class="col-md-12 mt-4">
							<div class="chatBg2">
							<div class="form-group mt-3">
							<textarea class="form-control" id="exampleFormControlTextarea1" name="contents"></textarea>
							<label class="form-control-placeholder" for="exampleFormControlTextarea1"> My Chat Description </label>
							</div>
							<button type="submit" class="btn btn-primary">Send</button>
							</div>
						</div> -->
						<div class="col-md-12 mt-4 d-none">
							<div class="row">
								<div class="col-12">
									<div class="comments">
										<div class="comment-box">
											<span class="commenter-pic">
												<img src="<?php echo base_url() ?>assets/img/user-icon.jpg" class="img-fluid">
											</span>
											<span class="commenter-name">
												<a href="#">Happy uiuxStream</a> <span class="comment-time">2 hours ago</span>
											</span>       
											<p class="comment-txt more">Suspendisse massa enim, condimentum sit amet maximus quis, pulvinar sit amet ante. Fusce eleifend dui mi, blandit vehicula orci iaculis Suspendisse massa enim, condimentum sit amet maximus quis, pulvinar sit amet ante. Fusce eleifend...</p>
											<div class="comment-meta">
												<!-- <button class="comment-like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 99</button>
												<button class="comment-dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> 149</button>  -->
												<!-- <button class="comment-reply reply-popup"><i class="fa fa-reply-all" aria-hidden="true"></i> Reply</button>          -->
											</div>
										</div>
									</div>
									<div class="comments">
										<div class="comment-box">
											<span class="commenter-pic">
												<img src="<?php echo base_url() ?>assets/img/user-icon.jpg" class="img-fluid">
											</span>
											<span class="commenter-name">
												<a href="#">Happy uiuxStream</a> <span class="comment-time">2 hours ago</span>
											</span>       
											<div class="form-group mt-3">
												<textarea class="form-control" id="exampleFormControlTextarea1" placeholder="comment" name="contents"></textarea>
											</div>
											<button  type="submit" class="btn btn-primary pull-right">Send</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
function coming_soon_popup() 
{ 
	swal({
				title:"",
				text: "Coming Soon",
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK!',
				html:''
		});
}
		
        $(document).ready(function() {
          
          $('#share_files').change(function(){
          	  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
          	  var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

          	  var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
          	
          	var share_files 
          	if ($('#share_files:checked').val()=='on') {
          		share_files = 'yes';
          	}else{
          		share_files = 'no';
          	}

	  	   var datastring='share_files='+ share_files + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
	         $.ajax({
	             type: 'POST',
	             data:datastring,
	             url: "<?php echo base_url();?>myteams/share_files",
	             beforeSend: function(){
	               $('#preloader-loader').css("display", "block");
	             },
	             success: function(res){ 
	               // var json = $.parseJSON(res);
	               // $('#subcategory').html(json.str);
	               //csrfName=json.name;
	               //csrfHash=json.value;
	             },
	             complete: function(){
	                      $('#preloader-loader').css("display", "none");
	              },

	           });


          	
          })

            $('#is_team_complete').change(function(){
          	  
          	  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
          	  var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

          	  var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';
          	
          	var team_complete 
          	if ($('#is_team_complete:checked').val()=='on') {
          		team_complete = 'Completed';
          	}else{
          		team_complete = 'Incomplete';
          	}

	  	   var datastring='team_complete='+ team_complete + '&'+'team_id='+ team_id + '&' + csrfName + '='+csrfHash;
	         $.ajax({
	             type: 'POST',
	             data:datastring,
	             url: "<?php echo base_url();?>myteams/is_team_complete",
	             beforeSend: function(){
	               $('#preloader-loader').css("display", "block");
	             },
	             success: function(res){ 
	               // var json = $.parseJSON(res);
	               // $('#subcategory').html(json.str);
	               //csrfName=json.name;
	               //csrfHash=json.value;
	             },
	             complete: function(){
	                      $('#preloader-loader').css("display", "none");
	              },

	           });


          	
          })

          

          

            $('#apply_for_challenge').click(function(){

	    	swal(
	        {
	            title:"Confirm?" ,
	            text: "Are you sure you want to apply for this challenge?",
	            type: 'warning',
	            showCancelButton: true,
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Yes!'
	        }).then(function (result) 
	        {
	            if (result.value) 
	            {


				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';

		      

		  	   var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		         $.ajax({
		             type: 'POST',
		             data:datastring,
		             url: "<?php echo base_url();?>myteams/apply_for_challenge",
		             beforeSend: function(){
		               $('#preloader-loader').css("display", "block");
		             },
		             success: function(res){ 
		               var data = $.parseJSON(res);
		              
		               if(data.success==true){

						swal({
						    title: "Success!",
						    text: "Application Submitted Successfully!",
						    type: "success"
						}).then(function() {
						     //location.reload(); 
								 location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
						});

		               }else{
		               	swal( 'Error!','Something went wrong!','error');
		               }
		             },
		             complete: function(){
		                      $('#preloader-loader').css("display", "none");
		              },

		           });

		            }
		       } ) 	

          })


           $('#withdraw_for_challenge').click(function(){

	    	 swal(
	        {
	            title:"Confirm?" ,
	            text: "Are you sure you want to withdraw your team?",
	            type: 'warning',
	            showCancelButton: true,
	            confirmButtonColor: '#3085d6',
	            cancelButtonColor: '#d33',
	            confirmButtonText: 'Yes!'
	        }).then(function (result) 
	        {
	            if (result.value) 
	            {


				var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

				var  team_id = '<?php echo $team_details[0]["team_id"]; ?>';

		      

		  	   var datastring='team_id='+ team_id + '&' + csrfName + '='+csrfHash;
		         $.ajax({
		             type: 'POST',
		             data:datastring,
		             url: "<?php echo base_url();?>myteams/withdraw_for_challenge",
		             beforeSend: function(){
		               $('#preloader-loader').css("display", "block");
		             },
		             success: function(res){ 
		                var data = $.parseJSON(res);
		              
		               if(data.success==true){

						swal({
						    title: "Success!",
						    text: "Application Withdrawn Successfully!",
						    type: "success"
						}).then(function() {
						     //location.reload(); 
								 location.href ="<?php echo site_url('myteams/myCreatedteams'); ?>";
						});

		               }else{
		               	swal( 'Error!','Something went wrong!','error');
		               }
		             },
		             complete: function(){
		                      $('#preloader-loader').css("display", "none");
		              },

		           });

		            }
		       } ) 	

          })

            

          })
</script>




 


