<?php if(count($non_featured_blog) > 0)
	{
		$i = 1;
		foreach($non_featured_blog as $res)
		{ 
			if($i >= $i_val && $i <= $chk_val)
			{ ?>			
			<div class="formInfo65 mt-4" id="nonFeaturedOuter_<?php echo $res['blog_id']; ?>" style="position:relative">
				<div class="row">					
					<div class="col-md-12">
						<p style='font-weight:600'><?php echo $i.". ".ucfirst($res['blog_title']); ?> : <a href="<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id'])); ?>" style="color:#000;font-weight:500"><?php echo $res['blog_id_disp']; ?></a></p>
					</div>				
				</div>				
			</div>				
<?php  }

			if($i > $chk_val) { break; }
			$i++;
		}// Foreach End 
		
		$new_i_val = $i_val+3;
		$new_chk_val = $chk_val + 3;
		
		if($new_i_val < $total_non_featured_blog_count)
		{ ?><br>
		<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
			<a href="javascript:void(0)" class="click-more" onclick="getBlogDataAjax(0, '<?php echo $featured_blog_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $non_featured_blog_limit; ?>', 0, 1, '<?php echo $new_i_val; ?>', '<?php echo $new_chk_val; ?>')">Show More</a>				
		</div>
		<?php }
	} ?>		