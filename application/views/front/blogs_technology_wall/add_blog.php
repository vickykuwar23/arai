<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<style>
	iframe { min-height:auto ; }	
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
	cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
	/**
	* Tooltip Styles
	*/
	
	/* Add this attribute to the element that needs a tooltip */
	[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
	}
	
	/* Hide the tooltip content by default */
	[data-tooltip]:before,
	[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
	}
	
	/* Position tooltip above the element */
	[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
	}
	
	/* Triangle hack to make tooltip look like a speech bubble */
	[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
	}
	
	/* Show tooltip content on hover */
	[data-tooltip]:hover:before,
	[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
	}
	
	.form-control-placeholder{top: -14px !important;}
	textarea.error {color: #000000!important;}
	input.error{color:#000000!important;}
	
	/**overwrite css**/
	.form-check-input {position: absolute; margin-top: .3rem !important; margin-left: -1.25rem !important;}	
	
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	
	.guideline-tooltip{ position: relative;  display: inline-block;  margin-left: 10px; }
	.guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-35%, 0); transform: translate(-35%, 0); margin-top: 10px; visibility: visible; }
	.guideline-tooltip p { position: absolute; left: 50%; top: 100%; opacity: 0; padding: 1em; background-color: #e7f0ff; font-size: 14px; line-height: 1.6; text-align: left; white-space: nowrap; -webkit-transform: translate(-35%, 1em); transform: translate(-35%, 1em); -webkit-transition: all 0.15s ease-in-out; transition: all 0.15s ease-in-out; color: #000; z-index: 99; font-weight: 400; visibility: hidden; width: 500px; white-space: normal; }
	.guideline-tooltip p::before { content: ''; position: absolute; top: -16px; left: 33%; width: 0; height: 0; border: 0.6em solid  transparent; border-top-color: #e7f0ff; transform: rotate(180deg); }

	@media screen and (max-width:480px) { .guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-42%, 0); transform: translate(-42%, 0); margin-top: 10px; width: 300px; white-space: pre-line; } }
	
	
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Blog</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('blogs_technology_wall') ?>">Blogs</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Blog </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="BlogForm" name="BlogForm" enctype="multipart/form-data">		
						<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="blog_title" id="blog_title" value="<?php if($mode == 'Add') { echo set_value('blog_title'); } else { echo $form_data[0]['blog_title']; } ?>">
									<label class="form-control-placeholder floatinglabel">Blog Title <em>*</em></label>
									<?php if(form_error('blog_title')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('blog_title'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder" style="<?php if($mode == 'Add') { ?>top:-25px !important<?php } ?>">Blog Description <em>*</em></label>
									<textarea name="blog_description" id="blog_description" class="form-control" cols="12" rows="4"><?php if($mode == 'Add') { echo set_value('blog_description'); } else { echo $form_data[0]['blog_description']; } ?></textarea>
									<?php if(form_error('blog_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('blog_description'); ?></label> <?php } ?>									
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Blog Banner </button>
									<input type="file" class="form-control" name="blog_banner" id="blog_banner" <?php if($mode == 'Add') { echo 'required'; } ?>>
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>		
								<?php if($blog_banner_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $blog_banner_error; ?></label> <?php } ?>
							</div>	
							
							<?php $dynamic_cls = $full_img = $onclick_fun = '';
								if($mode == 'Add')
								{
									$dynamic_cls = 'd-none';
								}
								else
								{
									$full_img = base_url().'uploads/blog_banner/'.$form_data[0]['blog_banner'];
								} ?>
								
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="blog_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
											<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
										</div>
									</div>
								</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Blog Technology <em>*</em></label>
									<div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Please select relevant Technology/Technologies for the blog being posted. If the requisite field is not listed, kindly select "Other" and enter the desired Technology.</p>
									</div>
									<div class="boderBox65x">
										<select class="form-control select2_common" name="technology_ids[]" id="technology_ids" data-placeholder="Blog Technology" multiple onchange="show_hide_blog_technology_other()">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														$technology_ids_arr = array();
														if($mode == 'Add') { if(set_value('technology_ids[]') != "") { $technology_ids_arr = set_value('technology_ids[]'); } }
													else { $technology_ids_arr = explode(",",$form_data[0]['technology_ids']); } ?>
													<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$technology_ids_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
													<?php }
												} ?>
										</select>
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small> 	
										<?php if($technology_ids_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $technology_ids_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-6" id="blog_technology_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<input type="text" class="form-control" name="technology_ids_other" id="technology_ids_other" value="<?php if($mode == 'Add') { echo set_value('technology_ids_other'); } else { echo $form_data[0]['technology_other']; } ?>">
									<label class="form-control-placeholder floatinglabel">Blog Technology Other <em>*</em></label>
									<?php if(form_error('technology_ids_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('technology_ids_other'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Tags <em>*</em> </label>
									<div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Please select relevant Tag/Tags for the blog being posted. Tags will facilitate better reach & visibility. If the requisite field is not listed, kindly select "Other" and enter the desired tag.</p>
									</div>
									<div class="boderBox65x">
										<select class="form-control select2_common" name="tags[]" id="tags" data-placeholder="Tags" multiple onchange="show_hide_tag_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = array();
														if($mode == 'Add') { if(set_value('tags[]') != "") { $tags_arr = set_value('tags[]'); } }
													else { $tags_arr = explode(",",$form_data[0]['tags']); } ?>
													<option value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>

							<div class="col-md-6" id="tag_other_outer" style="margin-top:26px;">
							               <div class="form-group">
							                  <input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php if($mode == 'Add') { echo set_value('tag_other'); } else { echo $form_data[0]['tag_other']; } ?>">
							                  <label class="form-control-placeholder floatinglabel">Other Tag <em>*</em></label>
							                  <?php if(form_error('tag_other')!=""){ ?> 
							                  <div class="clearfix"></div>
							                  <label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
							               </div>
							            </div>
							
							<div class="col-md-6" style="margin-bottom:20px">				
								<div class="form-group" style="margin-bottom:0px;">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Blog Type <em>*</em></label>
									<div style="margin-top:20px;">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="is_technical1" name="is_technical" class="custom-control-input get-type" value="1" <?php if($mode == 'Add') { if(set_value('is_technical') == 1) { echo 'checked'; } } else { if($form_data[0]['is_technical'] == 1) { echo 'checked'; } } ?>>
											<label class="custom-control-label" for="is_technical1">Technical</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="is_technical0" name="is_technical" class="custom-control-input get-type" value="0" <?php if($mode == 'Add') { if(set_value('is_technical') == 0) { echo 'checked'; } } else { if($form_data[0]['is_technical'] == 0) { echo 'checked'; } } ?>>
											<label class="custom-control-label" for="is_technical0">Non Technical</label>
										</div>								
									</div>								
									<div id="upload_type_err"></div>
								</div>								
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="margin: 0 0 15px 0;">	
								<label style="margin: 0;display: block;background: #ededed;padding: 10px 10px;font-weight: 500;">Author Section</label>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="author_name" id="author_name" value="<?php if($mode == 'Add') { echo set_value('author_name'); } else { echo $form_data[0]['author_name']; } ?>" required>
									<label class="form-control-placeholder floatinglabel">Author Name <em>*</em></label>
									<?php if(form_error('author_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_name'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" style="margin-bottom:20px;">
									<input type="checkbox" class="custom-control-input" id="author_professional_status_chk" name="author_professional_status_chk" onchange="enable_disable_inputs('author_professional_status_chk','author_professional_status')" <?php if($mode == 'Add') { if(set_value('author_professional_status_chk') == 'on') { echo 'checked'; } } else if($mode == 'Update') { if($form_data[0]['author_professional_status'] != "") { echo 'checked'; } } ?>>
									<label class="custom-control-label" for="author_professional_status_chk">Include My professional Status while posting this blog</label>
								</div>
							</div>
							
							<?php $professional_status_flag = 'readonly disabled'; $professional_status_disp_flag = 'style="display:none;"';
							if($mode == 'Add' && set_value('author_professional_status_chk') == 'on') { $professional_status_flag = ''; $professional_status_disp_flag = ""; }
							else if($mode == 'Update' && $form_data[0]['author_professional_status'] != "") { $professional_status_flag = ''; $professional_status_disp_flag = ""; } ?>
							<div class="col-md-12" id="author_professional_status_outer" <?php echo $professional_status_disp_flag; ?>>
								<div class="form-group">
									<input type="text" class="form-control" name="author_professional_status" id="author_professional_status" value="<?php if($mode == 'Add') { echo set_value('author_professional_status'); } else { echo $form_data[0]['author_professional_status']; } ?>" <?php echo $professional_status_flag; ?>>
									<label class="form-control-placeholder floatinglabel">Professional Status <em>*</em></label>
									<?php if(form_error('author_professional_status')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_professional_status'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" style="margin-bottom:20px;">
									<input type="checkbox" class="custom-control-input" id="author_org_name_chk" name="author_org_name_chk" onchange="enable_disable_inputs('author_org_name_chk','author_org_name')" <?php if($mode == 'Add') { if(set_value('author_org_name_chk') == 'on') { echo 'checked'; } } else if($mode == 'Update') { if($form_data[0]['author_org_name'] != "") { echo 'checked'; } }  ?>>
									<label class="custom-control-label" for="author_org_name_chk">Include My Organization Name while posting this blog</label>
								</div>
							</div>
							
							<?php $org_name_flag = 'readonly disabled'; $org_name_disp_flag = 'style="display:none;"';
							if($mode == 'Add' && set_value('author_org_name_chk') == 'on') { $org_name_flag = ''; $org_name_disp_flag = ''; }
							else if($mode == 'Update' && $form_data[0]['author_org_name'] != "") { $org_name_flag = ''; $org_name_disp_flag = ''; } ?>
							<div class="col-md-12" id="author_org_name_outer" <?php echo $org_name_disp_flag; ?>>
								<div class="form-group">
									<input type="text" class="form-control" name="author_org_name" id="author_org_name" value="<?php if($mode == 'Add') { echo set_value('author_org_name'); } else { echo $form_data[0]['author_org_name']; } ?>" <?php echo $org_name_flag; ?> maxlength="100">
									<label class="form-control-placeholder floatinglabel">Organization Name <em>*</em></label>
									<?php if(form_error('author_org_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_org_name'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" style="margin-bottom:20px;">
									<input type="checkbox" class="custom-control-input" id="author_description_chk" name="author_description_chk" onchange="enable_disable_inputs('author_description_chk','author_description')" <?php if($mode == 'Add') { if(set_value('author_description_chk') == 'on') { echo 'checked'; } } else if($mode == 'Update') { if($form_data[0]['author_description'] != "") { echo 'checked'; } }  ?>>
									<label class="custom-control-label" for="author_description_chk">Include below while posting this blog</label>
								</div>
							</div>
							
							<?php $author_description_flag = 'readonly disabled'; $author_description_disp_flag = 'style="display:none;"';
							if($mode == 'Add' && set_value('author_description_chk') == 'on') { $author_description_flag = ''; $author_description_disp_flag = ''; }
							else if($mode == 'Update' && $form_data[0]['author_description'] != "") { $author_description_flag = ''; $author_description_disp_flag = ''; } ?>
							<div class="col-md-12" id="author_description_outer" <?php echo $author_description_disp_flag; ?>>
								<div class="form-group">
									<textarea class="form-control" name="author_description" id="author_description" <?php echo $author_description_flag; ?> maxlength="50"><?php if($mode == 'Add') { echo set_value('author_description'); } else { echo $form_data[0]['author_description']; } ?></textarea>
									<label class="form-control-placeholder floatinglabel">Additional Personal Details <em>*</em></label>
									<?php if(form_error('author_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_description'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" style="margin-bottom:20px;">
									<input type="checkbox" class="custom-control-input" id="author_about_chk" name="author_about_chk" onchange="enable_disable_inputs('author_about_chk','author_about')" <?php if($mode == 'Add') { if(set_value('author_about_chk') == 'on') { echo 'checked'; } } else if($mode == 'Update') { if($form_data[0]['author_about'] != "") { echo 'checked'; } }  ?>>
									<label class="custom-control-label" for="author_about_chk">About Author</label>
								</div>
							</div>
							
							<?php $author_about_flag = 'readonly disabled'; $author_about_disp_flag = 'style="display:none;"';
							if($mode == 'Add' && set_value('author_about_chk') == 'on') { $author_about_flag = ''; $author_about_disp_flag = ''; }
							else if($mode == 'Update' && $form_data[0]['author_about'] != "") { $author_about_flag = ''; $author_about_disp_flag = ''; } ?>
							<div class="col-md-12" id="author_about_outer" <?php echo $author_about_disp_flag; ?>>
								<div class="form-group">
									<textarea class="form-control" name="author_about" id="author_about" <?php echo $author_about_flag; ?> rows="6"><?php if($mode == 'Add') { echo set_value('author_about'); } else { echo $form_data[0]['author_about']; } ?></textarea>
									<label class="form-control-placeholder floatinglabel">Author Brief <em>*</em></label>
									<?php if(form_error('author_about')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_about'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" id="accept_terms_err">
									<input type="checkbox" class="custom-control-input" id="accept_terms" name="accept_terms" <?php if($mode == 'Add') { if(set_value('accept_terms') == 'on') { echo 'checked'; } } ?> required>
									<label class="custom-control-label form-label" for="accept_terms">I accept the Terms and Condition <em>*</em></label>
								</div>
							</div>
						</div><br>
						
						<div class="row">
							<div class="col-md-4">
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary btnCancelCustom">Cancel</a> 
							</div>
							<div class="col-md-4 text-center">
								<?php if($mode == 'Add') { ?><a href="javascript:void(0)" onclick="confirm_clear_all()" class="btn btn-primary btnCancelCustom">Clear All</a><?php } ?> 
							</div>	
							<div class="col-md-4 text-right">
								<button type="submit" class="btn btn-primary add_button">Post</button> 
							</div>
						</div>						
					</form>
					
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">	
	CKEDITOR.replace('blog_description');
	
	$('.select2_common').select2();
	
	function show_hide_blog_technology_other()
	{
		$("#blog_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#technology_ids').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#blog_technology_other_outer").show();
			$("#technology_ids_other").prop("required", true);
		}
		else
		{
			$("#blog_technology_other_outer").hide();
			$("#technology_ids_other").prop("required", false);
			$("#technology_ids_other").val('');
		}
	}
	show_hide_blog_technology_other();
	
	function enable_disable_inputs(checkbox_id, input_id)
	{
		if($("#"+checkbox_id).prop("checked") == true)
		{
			$("#"+input_id+"_outer").css("display", 'block');
			$("#"+input_id).prop("disabled", false);
			$("#"+input_id).prop("readonly", false);
			$("#"+input_id).prop("required", true);
			$("#"+input_id).focus();
			
			if(input_id == 'author_professional_status' || input_id == 'author_org_name')
			{
				//$("#preloader").css("display", "block");
				parameters = { 'checkbox_id':checkbox_id, 'input_id':input_id, 'csrf_test_name':$('.token').val() }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						if(data.flag == "success")
						{ 
							$(".token").val(data.token);
							if(input_id == 'author_professional_status') { $("#author_professional_status").val(data.professional_status); }
							if(input_id == 'author_org_name') { $("#author_org_name").val(data.org_name); }						
							$("#preloader").css("display", "none");
						}
						else 
						{	}
					}
				});
			}
		}
		else if($("#"+checkbox_id).prop("checked") == false)
		{
			$("#"+input_id+"_outer").css("display", 'none');
			$("#"+input_id).prop("disabled", true);
			$("#"+input_id).prop("readonly", true);
			$("#"+input_id).prop("required", false);
			//$("#"+input_id).val("");
		}
	}
	
	function get_author_name()
	{
		//$("#preloader").css("display", "block");
		parameters = { 'input_id':'author_name', 'csrf_test_name':$('.token').val() }
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{ 
					$(".token").val(data.token);
					$("#author_name").val(data.author_name);					
					$("#preloader").css("display", "none");
				}
				else 
				{	}
			}
		});
	}
	
	function confirm_clear_all()
	{
		swal(
		{  
			title:"Confirm?",
			text: "This action will clear the complete form. Please confirm if you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); location.reload(); } });
	}
</script>

<?php if($mode == 'Add' && set_value('author_name') == '') { ?>
	<script>get_author_name();</script>
<?php } ?>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }

	function show_hide_tag_other()
	{
		$("#tag_other_outer").hide();

		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++)
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}

		if(disp_other_flg == 1)
		{
			$("#tag_other_outer").show();
			$("#tag_other").prop("required", true);
		}
		else
		{
			$("#tag_other_outer").hide();
			$("#tag_other").prop("required", false);
			$("#tag_other").val('');
		}
	}
	show_hide_tag_other();
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		
		//******* JQUERY VALIDATION *********
		$("#BlogForm").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				blog_title: { required: true, nowhitespace: true },
				blog_description: 
				{
					required: function() 
					{
						CKEDITOR.instances.blog_description.updateElement();
					},							
					minlength:1
				},
				blog_banner: { <?php if($mode == 'Add') { ?>required: true,<?php } ?> valid_img_format: true,filesize:2000000},
				"technology_ids[]": { required: true},
				"tags[]": { required: true},
				is_technical: { required: true},
				author_name: { required: true, nowhitespace: true },
				author_about: { maxCount:['400'] },
				accept_terms: { required: true},
				
				/* end_time: { required: true, chk_valid_time:true },
					cost_type: { required: true},
					cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
					"webinar_technology[]": { required: true },
					exclusive_technovuus_event:{required: true },
					registration_link:{required: true,valid_url:true },
					hosting_link:{required: true,valid_url:true },
					breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
					key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] }, */
			},
			messages:
			{
				blog_title: { required: "This field is required", nowhitespace: "Please enter the title" },
				blog_description: { required: "This field is required", nowhitespace: "Please enter the title" },				
				blog_banner:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				is_technical:{required: "This field is required"},
				author_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				accept_terms:{required: "This field is required"},
				
				/* start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}		 */					
			},
			<?php if($mode == 'Update') { ?>
			submitHandler: function(form)
			{
				/* form.submit(); */
				swal(
				{  
					title:"Confirm?",
					text: "Are you sure you want to update the blog? as you updated the blog, it needs admin approval to activate the blog again",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
				}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); form.submit(); } });
			},
			<?php } ?>
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "accept_terms") 
				{
					error.insertAfter("#accept_terms_err");
				}
				else
				{
					element.closest('.form-group').append(error);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BlogImagePreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#blog_img_outer").removeClass('d-none');
					//$("#blog_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#blog_img_outer .previous_img_outer img").attr("src", e.target.result);
					//$("#blog_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#blog_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#blog_banner").change(function() { BlogImagePreview(this); });	
</script>		