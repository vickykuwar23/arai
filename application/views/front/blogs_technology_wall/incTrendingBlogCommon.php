<?php
	if(count($trending_blog) > 0)
	{ ?>								
	<div class="formInfo65" style="padding:0">
		<div class="owl-carousel mb-2 owl-theme TrendingblogSlider">
			<?php foreach($trending_blog as $res)
				{	 $data['res'] = $res; $data['blog_type_flag'] = 'trending';		?>
				
				<div class="" style="padding:25px">							
					<?php $dispHighlight = ''; if($res['user_category_id'] == 2) { $dispHighlight = 'Organization'; } else { if($res['user_sub_category_id'] == 11) { $dispHighlight = 'Expert'; } } 
			
					if($dispHighlight != '') { echo ' <span class="blog_type_highlighted_text">'.$dispHighlight.'</span>'; } ?>			
					<?php $data['dispHighlight'] = $dispHighlight; $this->load->view('front/blogs_technology_wall/inc_blog_listing_inner', $data); ?>
				</div>				
			<?php } ?>	
		</div>
	</div>
	
	<script>
		$('.TrendingblogSlider').owlCarousel(
		{
			items: 1,
			autoplay: true,
			smartSpeed: 700,			
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				768: {
					items: 3
				},
				992: {
					items: 1
				}
			}		
		});
	</script>
<?php } ?>