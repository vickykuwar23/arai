<?php if(count($type_data) > 0)
	{
		$_SESSION['records_avaialbe']=0;
		foreach($type_data as $key=>$res)
		{

			if ($kr_type_id_load_more!="" && $res['id']===$kr_type_id_load_more) {
				$nf_limit = $nf_start + $nf_limit;
			}else{
				$nf_limit =3;
			}	
		if($search_str != "") { 
			$srs=$search_str;
			$this->db->where($search_str); 
		}			
		$this->db->where('kr_type',$res['id']);	
			// $this->db->order_by('kr_id','DESC',FALSE);
		// $this->db->order_by('k.xOrder','ASC',FALSE);
		$this->db->order_by('ISNULL(k.xOrder), k.xOrder = 0, k.xOrder, k.created_on DESC','',FALSE);
		$select_nf = "rt.type_name,k.kr_id, k.user_id, k.kr_id_disp, k.title_of_the_content, k.kr_banner, k.kr_description, k.kr_type, k.kr_type_other, k.tags, k.author_name,k.tag_other, k.created_on,k.technology_other,(SELECT GROUP_CONCAT(technology_name SEPARATOR '##') FROM arai_knowledge_repository_technology_master WHERE FIND_IN_SET(id,technology_ids)) AS DispTechnology, (SELECT GROUP_CONCAT(tag_name SEPARATOR '##') FROM arai_knowledge_repository_tags_master WHERE FIND_IN_SET(id,tags) AND tag_name!='other') AS DispTags";
		$this->db->join('arai_knowledge_repository_type_master rt','rt.id = k.kr_type', 'LEFT', FALSE);
		$non_featured_kr = $this->master_model->getRecords("knowledge_repository k",array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.is_featured' => '0', 'k.is_block' => '0'),$select_nf,'',0,$nf_limit);

		//START : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT
		if($search_str != "") { $this->db->where($search_str); }
		
		$this->db->where('kr_type',$res['id']);	
		$this->db->join('arai_registration r','r.user_id = k.user_id', 'LEFT', FALSE);
		$total_non_featured_kr_count = $this->master_model->getRecordCount('knowledge_repository k', array('k.is_deleted' => '0', 'k.admin_status' => '1', 'k.is_featured' => '0', 'k.is_block' => '0'), 'k.kr_id');
		$srs=$this->db->last_query();
		//END : SAME QUERY AS ABOVE WITHOUT LIMIT TO GET TOTAL ROW COUNT

		 ?>
		<div class="row">
		 <?php if(count($non_featured_kr)) {
 		 $_SESSION['records_avaialbe']=1;
		  ?>
                            <div class="col-md-12">
                                <div class="title-bar">
                                    <h2><?php echo $res['type_name'] ?> </h2>
                                    <div class="heading-border"></div>
                                </div>
                    
                            </div>
           <?php  } ?>                 


		 	<?php 
		 	foreach($non_featured_kr as $nfkr){
		 	?>
	 					<?php $getTotalLikes = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $nfkr['kr_id']), 'like_id'); ?>
	 					
                            <div class="col-md-4">
                                <div class="boxShadow75 shareKR textPart ">
                                    <img src="<?php echo base_url('uploads/kr_banner/').$nfkr['kr_banner'] ?>" alt="img" class="img-fluid">
                                    <ul style="text-align: center">
                                    <li class="id_disp"><a href="javascript:void(0)" style="cursor:auto;">
                                         <span style="cursor:auto; background: #FFF; color: #c80032; "><?php echo $nfkr['kr_id_disp']; ?></span></a></li>
                                         </ul>
                                    
                                    <ul> 	
									<li style="cursor:auto;" id="like_count_outer_<?php echo $nfkr['kr_id']; ?>"><span style="cursor:auto;  background: #FFF; color: #c80032; text-align: center;"><?php echo $getTotalLikes ?> </span></li>
					            	<?php if(in_array($nfkr['kr_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
									else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
									<li id="like_unlike_btn_outer_<?php echo $nfkr['kr_id']; ?>">
										<span onclick="like_unlike_blog('<?php echo base64_encode($nfkr['kr_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
									</li>
                                        
                                        
                                   <li style="float: right;">
									<span onclick="share_blog('<?php echo site_url('knowledge_repository/details/'.base64_encode($nfkr['kr_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
									</li> 
                    
                                    </ul>
                                    <p style="height: 45px" title="<?php echo ucfirst($nfkr['title_of_the_content']); ?>"><strong> 
                                    	<?php //echo ucfirst($nfkr['title_of_the_content']); ?>
                                    	<?php 
                                    	//echo str_word_count($nfkr['title_of_the_content']);
                                    	 if(strlen($nfkr['title_of_the_content']) > 65 ){ 
                                    	 	$srt_to_show= ucfirst(substr($nfkr['title_of_the_content'], 0,65) )."...";
                                    	 	echo wordwrap($srt_to_show, 32, "\n", true);
                                    	 	 } else {echo ucfirst($nfkr['title_of_the_content']);} ?>	
                                    	</strong></p>
                    
		                        <div class="bgNewBox45 mt-5 mb-3">
		                            <p class="mb-0" title="<?php echo $nfkr['author_name']; ?>"><strong>By: </strong>
		                            	<?php if(strlen($nfkr['author_name']) > 35 ){ echo ucfirst(substr($nfkr['author_name'], 0,35) )."..."; } else {echo ucfirst($nfkr['author_name']);} ?>	
		                            </p>
		                            <p class="mb-0"><strong>Type: </strong><?php if(strtolower($nfkr['type_name'])=='other' && $nfkr['kr_type_other']!='') { echo $nfkr['kr_type_other'] ;}else{echo $nfkr['type_name'];} ?></p>
		                            <p class="mb-0"><strong>Published Date : </strong><?php echo date("jS F Y", strtotime($nfkr['created_on'])); ?></p>
		                        </div>

		                        		
										<?php if($nfkr['DispTechnology'] != "" || $nfkr['technology_other']!=''){ ?>

										<strong >Technology Domain:</strong> 
										<div class="owl-carousel mb-2 owl-theme tagSlider">
										<?php $tech_str = ''; 
										if($nfkr['DispTechnology'] != "") 
										{

										$techArr = explode("##",$nfkr['DispTechnology']); 		
										foreach($techArr as $res_td)
											{
												if(strtolower(trim($res_td)) != 'other')
												{ ?>
													<div><a style="cursor:auto;"><?php if(strlen($res_td) > 15 ) {echo substr($res_td,0, 15)."..." ; }else{ echo $res_td; }; ?>
															
														</a></div> 
										<?php } } } ?>

										<?php if (count($techArr) == 1 && $nfkr['technology_other']!='') {
													$style='cursor:auto;min-width:250px ';
												}else{
													$style='cursor:auto;';
										} ?>

										<?php if ($nfkr['technology_other']!=''){ 
                                                    $technology_other_arr=explode(',', $nfkr['technology_other']);
                                                    foreach ($technology_other_arr as $key => $technology_other) {
                                                      ?>
                                                    <div><a style="<?php echo $style ?>">
                                                    <?php 
                                                    if(strlen($technology_other) > 15 ) {echo substr($technology_other,0, 15)."..." ; }else{ echo $technology_other; }; ?>

                                                                        
                                                    </a></div>
                                    <?php  } } ?>
										
										 </div>
										<?php } ?>
                    
				                   		<?php if($nfkr['DispTags'] != "" || $nfkr['tag_other']!='') 
										{ ?>
										<div style="margin: -25px 0 0 0;">
										<strong>Tags:</strong> 
										<div class="owl-carousel mb-2 owl-theme tagSlider">
										<?php if($nfkr['DispTags'] != "") 
										{ 
											$tags_arr = explode("##",$nfkr['DispTags']); 
											 foreach($tags_arr as $tag)
													{	?>
													<div><a style="cursor:auto;"><?php if(strlen($tag) > 15 ) {echo substr($tag,0, 15)."..." ; }else{ echo $tag; }; ?></a></div> 
												<?php } }	?>
												
												<?php if ($nfkr['DispTags'] == "" && $nfkr['tag_other']!='') {
													$style='cursor:auto;min-width:250px ';
												}else{
													$style='cursor:auto;  ';
												} ?>

											  	<?php if ($nfkr['tag_other']!=''){ 
                                                    $tag_other_arr=explode(',', $nfkr['tag_other']);
                                                    foreach ($tag_other_arr as $key => $tag_other) {
                                                      ?>
                                                                <div><a style="<?php echo $style ?>">
                                                                    <?php 
                                                                    if(strlen($tag_other) > 15 ) {echo substr($tag_other,0, 15)."..." ; }else{ echo $tag_other; }; ?>

                                                                        
                                                                    </a></div>
                                                <?php  } } ?>
										</div>	
										</div>
										<?php } ?>	
                    
                                    <a href="<?php echo base_url('knowledge_repository/details/').base64_encode($nfkr['kr_id'])  ?>"  class="btn btn-primary">Read More </a>
                    
                                    <button class="btn btn-primary float-right" onclick="open_download_modal('<?php echo $nfkr['kr_id'] ?>')">Download</button>
                                </div>
                            </div>
                              

        <?php  } ?>
        </div>

           <?php 	
		        if($nf_limit < $total_non_featured_kr_count)
				{ ?> <br>
				<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
					<a href="javascript:void(0)" class="click-more" onclick="getKrDataAjax(0, '<?php echo $featured_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $non_featured_limit; ?>', 0, 1,'<?php echo $res['id'] ?>')">Show More </a>				
				</div>
				<?php } ?>
                          
<?php }
	}	?>	     
	
	
<?php 
// print_r($type_data);
if( $_SESSION['records_avaialbe']==0 && $featured_kr_total_cnt==0 ) 
		{	
			?>
	<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;"><br>No Records Available.</p></div>
<?php }	?>	

<script type="text/javascript">
	$('.tagSlider').owlCarousel({
        items: 1,
        autoplay: false,
        smartSpeed: 700,
        autoWidth:true,
        loop: true,
        nav: true,
        dots: false,
        margin:11,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });
</script>