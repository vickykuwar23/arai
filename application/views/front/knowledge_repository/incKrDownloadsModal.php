<?php $enc_obj =  New Opensslencryptdecrypt(); ?>
<div class="table-responsive">
	<table id="likes-list" class="table table-bordered table-hover challenges-listing-page">
		<thead>
			<tr>
				<th class="text-center">Sr. No</th>
				<th class="text-center">Name</th>
				<th class="text-center">Email</th>
				<th class="text-center">Date</th>
			</tr>
		</thead>
		<tbody>	
			<?php if(count($likes_data) > 0)
			{
				$i=1;
				foreach($likes_data as $res)
				{	?>
					<tr>
						<td class="text-center"><?php echo $i; ?></td>
						<td>
							<?php 
								$title = $enc_obj->decrypt($res['title']);
								$first_name = $enc_obj->decrypt($res['first_name']);
								$middle_name = $enc_obj->decrypt($res['middle_name']);
								$last_name = $enc_obj->decrypt($res['last_name']);
								
								$disp_name = $title." ".$first_name;
								if($middle_name != '') { $disp_name .= " ".$middle_name." "; }
								$disp_name .= $last_name;
								echo $disp_name; ?>
						</td>
						<td><?php echo $enc_obj->decrypt($res['email']); ?></td>
						<td class="text-right"><?php echo date("d-m-Y, h:ia", strtotime($res['createdAt'])); ?></td>
					</tr>
		<?php	$i++;
				}				
			}	?>
		</tbody>
	</table>
</div>

<script>
$(document).ready( function () 
	{	  
		var table = $('#likes-list').DataTable(
		{
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.					
		});
	});
</script>