<?php
	if(count($featured_kr) > 0)
	{ 
	?>								
	 	 <div class="owl-carousel mb-2 owl-theme KnowledgeRepositorySlider">
                    
			<?php foreach($featured_kr as $res)
				{	 $data['res'] = $res	?>
										
					<?php $this->load->view('front/knowledge_repository/inc_kr_listing_inner', $data); ?>
			<?php } ?>	
		</div>
	
	<script>

 $('.KnowledgeRepositorySlider').owlCarousel({
        items: 1,
        smartSpeed: 700,
        loop: false,
        nav: true,
        dots: false,
        navText: ["", ""],
        // autoplayHoverPause: true,
        autoplay:true,
        // autoplayTimeout:4000,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });	

     
 
$('.tagSlider').owlCarousel({
        items: 1,
        autoplay: false,
        smartSpeed: 700,
        autoWidth:true,
        loop: true,
        margin:12,
        nav: true,
        dots: false,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });
	</script>
<?php } ?>