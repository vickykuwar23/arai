<?php if(count($non_featured_blog) > 0)
	{
		foreach($non_featured_blog as $res)
		{ $data['res'] = $res; $data['blog_type_flag'] = $blog_type_flag = 'nonfeatured'; ?>
		
			<div class="formInfo65 mt-4" id="nonFeaturedOuter_<?php echo $res['blog_id']; ?>" style="position:relative">
				
				<?php $dispHighlight = ''; if($res['user_category_id'] == 2) { $dispHighlight = 'Organization'; } else { if($res['user_sub_category_id'] == 11) { $dispHighlight = 'Expert'; } } 
			
				if($dispHighlight != '') { echo ' <span class="blog_type_highlighted_text">'.$dispHighlight.'</span>'; } ?>
				<?php $this->load->view('front/blogs_technology_wall/inc_blog_listing_inner', $data); ?>
			</div>	
		
		<?php } // Foreach End 
		
		if($new_start < $total_non_featured_blog_count)
		{ ?><br>
		<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
			<a href="javascript:void(0)" class="click-more" onclick="getBlogDataAjax(0, '<?php echo $featured_blog_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $non_featured_blog_limit; ?>', 0, 1)">Show More</a>				
		</div>
		<?php }
	} 
	else 
	{ 
		if($featured_blog_total_cnt == 0 && $trending_blog_total_cnt == 0) 
		{	?>
	<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;"><br>No Blog Available</p></div>
<?php }
	}	?>	