<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>

<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<style>

.tag2 ul li:first-child {
    display: block; margin-bottom:5px !important;
}
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	
#WebinarWallSlider.owl-carousel .owl-dot,#WebinarWallSlider.owl-carousel .owl-nav .owl-next,#WebinarWallSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
#WebinarWallSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
#WebinarWallSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
#WebinarWallSlider .owl-nav.disabled{display:block}
#WebinarWallSlider .owl-nav{position:absolute; top:40%; width:100%}
#WebinarWallSlider.owl-theme .owl-dots .owl-dot.active span,#WebinarWallSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
#WebinarWallSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
#WebinarWallSlider .owl-nav .owl-prev{left:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
#WebinarWallSlider .owl-nav .owl-next{right:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
#WebinarWallSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}

#WebinarWallSlider .owl-item{ padding: 0 5px;}
#WebinarWallSlider .owl-item img{width:100%; }
#WebinarWallSlider .owl-dots{position:absolute;width:100%;bottom:25px}
/* #WebinarWallSlider .owl-item{ padding: 0;} */


.WebinarsSlider.owl-carousel .owl-dot,.WebinarsSlider.owl-carousel .owl-nav .owl-next,.WebinarsSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
.WebinarsSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
.WebinarsSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
.WebinarsSlider .owl-nav.disabled{display:block}
.WebinarsSlider .owl-nav{position:absolute; top:40%; width:100%}

.WebinarsSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #000;color:#32f0ff}

.WebinarsSlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
.WebinarsSlider .owl-nav .owl-next{right:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
.WebinarsSlider .owl-item{ padding: 0 15px;}
.WebinarsSlider .owl-item img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2);}


.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}


/* .WebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}

.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}

/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
.product_details{padding: 20px 0 0 0;}


/** Blog**/




.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}

.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}

.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}

.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}

.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}


.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag ul li a:hover{ background: #000; color: #32f0ff; }


.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.shareList ul li a:hover{ background: #000; color: #32f0ff; }

.userPhoto-info h3{ font-size: 19px !important; }
.userPhoto-info img{ border-radius: 50%;}
.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
.descriptionBox45 p{color: #FFF;}
.descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}

.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
.bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
.bs-example45 h3{color:#fff;font-size:19px;font-weight:800}




.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
.formInfo65 p a{ text-decoration: none; font-weight: 800; color: #000;}
.formInfo65 p a:hover{ text-decoration: none; font-weight: 800; color: #32f0ff;}





/** Color Change**/
.nav-tabs{border-bottom:1px solid #32f0ff}
.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#32f0ff;background-color:#000 ;border-color:#000}
.product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {border-color: #32f0ff #32f0ff #32f0ff;}
.title-bar .heading-border{background-color:#32f0ff}
.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}

/**Questions And Answers Forum**/
.userPhoto-info h3 span {font-size: 14px !important; color: #a3a3a3; text-transform: initial; }
#AnswersForum h2{font-size:1.75rem}
.bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}

.listViewinfo h3 {font-size: 24px !important;}
.listViewinfo h3 span {float: right;}
.listViewinfo h3 span a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
.listViewinfo h3 span a:hover {background: #000; color: #32f0ff; text-align: center; }


.listViewinfo img {border-radius: 50%;}
.imgBoxView img {border-radius: 0%;}
.listViewinfo ul{ list-style: none; padding: 0; margin: 0; font-size: 15px;}
.listViewinfo ul li{ list-style: none; padding: 0; margin: 0; color: #333; font-weight: 800;}
.listViewinfo ul li span{font-weight: normal; color: #000;}

.likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
.likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
.likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
.likeShare ul li:last-child{ float: right; margin: 0;}
.likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
.likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
.dropdown-menu{ padding: 0px !important;}
.dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
.dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }


.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}


.imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}

.blogSlider.owl-carousel .owl-dot,.blogSlider.owl-carousel .owl-nav .owl-next,.blogSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
.blogSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
.blogSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
.blogSlider .owl-nav.disabled{display:block}
.blogSlider .owl-nav{position:absolute; top:40%; width:100%}
.blogSlider.owl-theme .owl-dots .owl-dot.active span,.blogSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
.blogSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
.blogSlider .owl-nav .owl-prev{left:-20px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
.blogSlider .owl-nav .owl-next{right:-20px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
.blogSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}

.blogSlider .owl-item{ padding: 0 5px;}
.blogSlider .owl-item img{width:100%; }
.blogSlider .owl-dots{position:absolute;width:100%;bottom:25px}
.blogSlider .owl-item{ padding: 0;}



/**Blog Icon Added Css**/
.blogProfileicon{ position: absolute; top:8px; right:25px; width: 45px; height: 45px; border-radius:5px; background:#32f0ff;  padding: 5px; text-align: center;}
.blogProfileicon img{ width: 50%;}



/** Knowledge Repository Slider **/

.KnowledgeRepositorySlider.owl-carousel .owl-dot,.KnowledgeRepositorySlider.owl-carousel .owl-nav .owl-next,.KnowledgeRepositorySlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
.KnowledgeRepositorySlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
.KnowledgeRepositorySlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
.KnowledgeRepositorySlider .owl-nav.disabled{display:block}
.KnowledgeRepositorySlider .owl-nav{position:absolute; bottom:-15px; width:100%}


.KnowledgeRepositorySlider .owl-nav{position:absolute; top:40%; width:100%}

.KnowledgeRepositorySlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #000;color:#32f0ff}

.KnowledgeRepositorySlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}
.KnowledgeRepositorySlider .owl-nav .owl-next{right:-30px;position:absolute;background:#fff;color:#32f0ff; border: solid 1px #32f0ff;}

.KnowledgeRepositorySlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}

.KnowledgeRepositorySlider .owl-item{ padding:10px;}
.KnowledgeRepositorySlider .owl-item img{width:100%; }
.KnowledgeRepositorySlider .owl-dots{position:absolute;width:100%;bottom:25px}


.boxShadow75{margin:0 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px;}
.boxShadow75:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
.boxShadow75 ul{ padding: 0; margin:13px 0; list-style: none;}
.boxShadow75 ul li{ padding: 0; margin: 0; display: inline-block; color: #64707b; font-size: 15px; position: relative;}
.boxShadow75 ul li span{ background: #32f0ff; color: #000; padding: 5px; border-radius: 5px;}
.boxShadow75 ul li a {background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.boxShadow75 ul li a:hover { background: #000; color: #32f0ff;}

.tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
.tag2 ul li{ padding: 0; margin:0 5px 10px 0 !important;}
.tag2 ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag2 ul li a:hover{ background: #000; color: #32f0ff; }

.boxShadow75 .btn-primary2{color: #32f0ff; background: #000; padding: .375rem .75rem; border: solid 1px #000; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary2:hover {color: #000; background: #32f0ff; border: solid 1px #32f0ff; text-decoration: none; }

.boxShadow75 .btn-primary{color: #c80032; background: #FFF; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary:hover {color: #FFF; background: #c80032; border: solid 1px #c80032; text-decoration: none; }

#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
#flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
#flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 

	
.shareKR ul li span{  background: #FFF; border:solid 1px #c80032; color: #c80032; text-decoration: none;  cursor:pointer; font-size:13px; display: block; }
.shareKR ul li span:hover{ background: #FFF; color: #c80032; }
.ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
.ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}
.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}

.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}


.btn_report {background: #FFF !important; color: #c80032 !important;}
.btn_report:hover {background: #c80032 !important; color: #FFF !important;}
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s"> Knowledge Repository </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
					<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Knowledge Repository Details </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="story" data-aos="fade-up">	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade" id="KnowledgeRepository">

			<div class="container">
                   <div class="col-md-12">
                                <div class="boxShadow75 shareKR">
                   				
                                	<div class="row">
                                	<div class="col-md-8">
									<h3 style="word-break: break-all;"> <?php echo ucfirst($form_data[0]['title_of_the_content']); ?> </h3>
                                    	<img src="<?php echo base_url('uploads/kr_banner/').$form_data[0]['kr_banner'] ?>" alt="img" class="img-fluid" style=" margin-top:5px">
                                   
                                    	<div class="mt-3 mb-4">
                                    	<a  class="btn btn-primary mb-2" href="javascript:void(0)" onclick="open_download_modal('<?php echo $form_data[0]['kr_id'] ?>')" >
                                    		<i class="fa fa-download"></i></a>
                                      	<strong  class="mb-2"><?php echo ucfirst($form_data[0]['title_of_the_content']); ?></strong>
                                      	<br>

                                      	<span class="pl-5"><i class="fa fa-file fa-lg">&nbsp </i><?php echo $files_count." files "  ?>
                                      	<i class="fa fa-database fa-lg pl-2">&nbsp</i><?php echo $files_size ?>
                                      	</span>
                                      	</div>
                                    </div>

                                    <div class="col-md-4">
		                                  

                    
					                        <div class="bgNewBox65 mt-5 mb-3">
												<p class="mb-0"><strong>TNKID:</strong> <?php echo $form_data[0]['kr_id_disp']; ?></p>
										
					                            <p class="mb-0"><strong>By:</strong> <?php echo $form_data[0]['author_name']; ?></p>
					                            <p class="mb-0"><strong>Type:</strong> <?php echo $form_data[0]['type_name']; ?></p>
												<p class="mb-0"><strong>Published Date:</strong> <?php echo date("jS F Y", strtotime($form_data[0]['created_on'])); ?></p>

												
													
													<div class="tag2 mt-3">
														<ul>
														<li style='font-weight:600'>Technology Domain:</li> 
														<?php $tech_str = ''; 
														if($form_data[0]['DispTechnology'] != "") 
														{
														$techArr = explode("##",$form_data[0]['DispTechnology']); 
														
															foreach($techArr as $res)
															{
																if(strtolower(trim($res)) != 'other')
																{ ?>
																	<li><span style="cursor:auto"><?php echo $res; ?></span></li> 
																
															<?php 
															}  } } ?>
															 
															<?php /* if ($form_data[0]['technology_other']!=''): ?>
																<li><span style="cursor:auto"><?php echo $form_data[0]['technology_other']; ?></span></li>
																<?php endif */ ?>

                                 <?php if ($form_data[0]['technology_other']!=''){ 
                                                    $technology_other_arr=explode(',', $form_data[0]['technology_other']);
                                                    foreach ($technology_other_arr as $key => $technology_other) {
                                                      ?>
                                                  <li><span style="cursor:auto">
                                                      <?php 
                                                       echo $technology_other; ; 

                                                          
                                                     ?></span></li>
                                    <?php  } } ?>

														 

													</ul>
												</div>

												
												<div class="tag2 mt-3">
														<ul>
															<li style='font-weight:600'>Tags:</li> 
															<?php if($form_data[0]['DispTags'] != "") 
															{ 
															$tags_arr = explode("##",$form_data[0]['DispTags']); 

															 foreach($tags_arr as $tag)
																{	?>
																<li><span style="cursor:auto"><?php echo $tag; ?></span></li> 
															<?php } }	?>


															<?php /* if ($form_data[0]['tag_other']!=''): ?>
																<li><span style="cursor:auto"><?php echo $form_data[0]['tag_other']; ?></span></li>
															<?php endif */ ?>
                              <?php if ($form_data[0]['tag_other']!=''){ 
                                                    $tag_other_arr=explode(',', $form_data[0]['tag_other']);
                                                    foreach ($tag_other_arr as $key => $tag_other) {
                                                      ?>
                                                  <li><span style="cursor:auto">
                                                      <?php 
                                                       echo $tag_other;  ?>

                                                          
                                                      </span></li>
                                    <?php  } } ?>
													<br>
													<br>
												<li style="cursor:auto" id="like_count_outer_<?php echo $form_data[0]['kr_id']; ?>"><span style="cursor:auto;"><?php echo $getTotalLikes ?> </span></li>	
												<?php if(in_array($form_data[0]['kr_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
												else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
												<li id="like_unlike_btn_outer_<?php echo $form_data[0]['kr_id']; ?>">
													<span onclick="like_unlike_blog('<?php echo base64_encode($form_data[0]['kr_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
												</li>
															
												<li>
												<span onclick="share_blog('<?php echo site_url('knowledge_repository/details/'.base64_encode($form_data[0]['kr_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
												</li> 


												<?php if(in_array($form_data[0]['kr_id'], $BlogActiondata['reported_blog'])) { $Reported_label = "Knowledge Repository Already Reported"; $ReportedFlag = '0';  }
												else { $Reported_label = "Report Knowledge Repository"; $ReportedFlag = '1'; } ?>
												
												<?php if($user_id!=$form_data[0]['user_id']){ /*
												?>

												<li id="report_blog_btn_outer_<?php echo $form_data[0]['kr_id']; ?>">
													<a href="javascript:void(0)" class="toggle_btn mr-1">
													<i style="cursor: pointer;"  class="fa fa-ellipsis-v " aria-hidden="true"></i>
													</a>
													

													<button class="btn btn-sm btn-danger btn_report d-none" href="javascript:void(0)" title="<?php echo $Reported_label; ?>" onclick="report_blog('<?php echo base64_encode($form_data[0]['kr_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['kr_id_disp']; ?>', '<?php echo $form_data[0]['title_of_the_content']; ?>')">Report</button>
												
												</li>	
											<?php */} ?>
                      <?php if($user_id!=$form_data[0]['user_id']){ 
                        ?>
												<li >
													<div class="dropdown">
															  <span style="cursor: pointer;" onclick="ShowDropdown()" class="dropbtn pl-2 pr-2"><i   class="fa fa-ellipsis-v" ></i></span>
															  <div id="myDropdown" class="dropdown-content">
															    <a id="report_blog_btn_outer_<?php echo $form_data[0]['kr_id']; ?>" class="btn btn-sm btn-danger btn_report" href="javascript:void(0)" title="<?php echo $Reported_label; ?>" onclick="report_blog('<?php echo base64_encode($form_data[0]['kr_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['kr_id_disp']; ?>', '<?php echo $form_data[0]['title_of_the_content']; ?>')">Report</a>
															    
															  </div>
															</div>
												</li>	
                      <?php } ?>
												</ul>

												

													</div>
												

												

												
					                        </div>


                    

									 </div>
                            	</div>
                            <div class="row">
                            	<div class="col-md-12">
                            	<h4>Brief about the Content</h4>
                            	</div>
                            	<div class="col-md-12">
                            	<p><?php echo $form_data[0]['kr_description']; ?></p>
                            	</div>
                            </div>
                    
                    
                    
                                  
                                </div>
                            </div>	
								
					</div> 
			
			</div>

		</div>
	</div>
</section>

<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlogReportpopupLabel">Knowledge Repository Report</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div class="bgNewBox65">
				<p>Technology Transfer ID : <span id="popupBlogId"></span></p>
				<p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
				</div>
					
					<div class="form-group mt-4">
						
						<textarea class="form-control" id="popupBlogReportComment" name="popupBlogReportComment" required onkeyup="check_report_validation()"></textarea>
						<label for="popupBlogReportComment" class="form-control-placeholder">Comment <em>*</em></label>
						<span id="popupBlogReportComment_err" class='error'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submit_blog_report('<?php echo base64_encode($form_data[0]['kr_id']); ?>')">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="download_model_content_outer">
	</div>
</div>
<!-- Modal -->

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<?php
if($form_data[0]['admin_status'] == 1 && $form_data[0]['is_block'] == 0) { $action_flag = 0; }
else { $action_flag = 1; } ?>

<script>
function ShowDropdown() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}	

$(".toggle_btn").click(function(event) {
	$(".btn_report").toggleClass('d-none')	
});
	function like_unlike_blog(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("knowledge_repository/like_unlike_blog_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#like_unlike_btn_outer_"+data.kr_id).html(data.response);
				$("#like_count_outer_"+data.kr_id).html(data.total_likes_html);
				
			}
		});
	}
</script>

<script>
	function open_download_modal(kr_id)
	{
		module_id=31;
		if(check_permissions_ajax(module_id) == false){
         	swal( 'Warning!','You don\'t have permission to access this page !','warning');
         	return false;
		}
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('knowledge_repository/open_download_modal_ajax'); ?>",
			data: {'kr_id':kr_id},
			cache: false,
			success:function(data)
			{
				//alert(data)
				if(data == "error")
				{
					location.reload();
				}
				else
				{
					$("#download_model_content_outer").html(data);
					$("#DownloadModal").modal('show');
				}
			}
		});
	}
	function delete_blog_from_user_listing(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected blog?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("knowledge_repository/delete_blog"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{ 
						window.location.replace("<?php echo site_url('knowledge_repository'); ?>");
					}
				});
			} 
		});		
	}	
	
	function report_blog(kr_id, ReportedFlag, blog_disp_id, blog_title)
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not Report the knowledge repository as its status is Pending or Blocked",
				type: 'warning'				
			});
		<?php }else { ?>
			if(ReportedFlag == "1")
			{
				$("#popupBlogId").html(blog_disp_id);
				$("#popupBlogTitle").html(blog_title);
				$("#popupBlogReportComment_err").html('');
				$("#BlogReportpopup").modal('show');
				//$("#popupBlogReportComment").val('');
				$("#popupBlogReportComment").focus();
			}
			else
			{
				swal(
				{
					title: 'Alert!',
					text: "You have already reported the knowledge repository",
					type: 'alert',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
			}
		<?php } ?>
	}	
	
	function check_report_validation()
	{
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		if(popupBlogReportComment.trim() == "") { $("#popupBlogReportComment_err").html('Please enter the report comment'); $("#popupBlogReportComment").focus(); }
		else { $("#popupBlogReportComment_err").html(''); }
	}
	
	function submit_blog_report(kr_id)
	{	
		check_report_validation();
		
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		if(popupBlogReportComment.trim() != "")		
		{
			$("#popupBlogReportComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to report the selected knowledge repository?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("knowledge_repository/report_blog_ajax"); ?>',
						data:{ 'kr_id':kr_id, 'popupBlogReportComment':popupBlogReportComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#report_blog_btn_outer_"+data.kr_id).replaceWith(data.response);						
							$("#BlogReportpopup").modal('hide');
							
							swal(
							{
								title: 'Success!',
								text: "Knowledge Repository Reported successfully",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	

	

	
	function share_blog(url) 
	{
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not share the blog as its status is Pending or Blocked", 
				type: 'warning'
			});
		<?php }else { ?>
			$("#BlogShareLink").html(url);
			$("#share-popup").modal('show');
		<?php } ?>
	}
	
	
	
	
	
	
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
			
		//alert("Contents copied to clipboard.");
		return false;
	}
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>

