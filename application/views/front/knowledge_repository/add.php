<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
	cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
	/**
	* Tooltip Styles
	*/
	
	/* Add this attribute to the element that needs a tooltip */
	[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
	}
	
	/* Hide the tooltip content by default */
	[data-tooltip]:before,
	[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
	}
	
	/* Position tooltip above the element */
	[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
	}
	
	/* Triangle hack to make tooltip look like a speech bubble */
	[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
	}
	
	/* Show tooltip content on hover */
	[data-tooltip]:hover:before,
	[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
	}
	
	.form-control-placeholder{top: -14px !important;}
	textarea.error {color: #000000!important;}
	input.error{color:#000000!important;}
	
	/**overwrite css**/
	.form-check-input {position: absolute; margin-top: .3rem !important; margin-left: -1.25rem !important;}	
	
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	
	.guideline-tooltip{ position: relative;  display: inline-block;  margin-left: 10px; }
	.guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-35%, 0); transform: translate(-35%, 0); margin-top: 10px; visibility: visible; }
	.guideline-tooltip p { position: absolute; left: 50%; top: 100%; opacity: 0; padding: 1em; background-color: #e7f0ff; font-size: 14px; line-height: 1.6; text-align: left; white-space: nowrap; -webkit-transform: translate(-35%, 1em); transform: translate(-35%, 1em); -webkit-transition: all 0.15s ease-in-out; transition: all 0.15s ease-in-out; color: #000; z-index: 99; font-weight: 400; visibility: hidden; width: 500px; white-space: normal; }
	.guideline-tooltip p::before { content: ''; position: absolute; top: -16px; left: 33%; width: 0; height: 0; border: 0.6em solid  transparent; border-top-color: #e7f0ff; transform: rotate(180deg); }

	@media screen and (max-width:480px) { .guideline-tooltip:hover p { opacity: 1; -webkit-transform: translate(-42%, 0); transform: translate(-42%, 0); margin-top: 10px; width: 300px; white-space: pre-line; } }

	.form_check_custum {
    padding-left: 0px !important;
}
</style>
<div id="preloader-loader" style="display:none;"></div>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Knowledge Repository</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('blogs_technology_wall') ?>">Blogs</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php if($mode == 'Add') { echo 'Add'; } else { echo "Edit"; } ?> Knowledge Repository </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="BlogForm" name="BlogForm" enctype="multipart/form-data">		
						<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

						<?php if ($mode == 'Update') { ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control"  value="<?php echo $form_data[0]['kr_id_disp']  ?>" readonly>
									<label class="form-control-placeholder floatinglabel">Knowledge Repository ID  </label>
									
								</div>
							</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="title_of_the_content" id="title_of_the_content" value="<?php if($mode == 'Add') { echo set_value('title_of_the_content'); } else { echo $form_data[0]['title_of_the_content']; } ?>">
									<label class="form-control-placeholder floatinglabel">Title of the Content  <em>*</em></label>
									<?php if(form_error('title_of_the_content')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('title_of_the_content'); ?></label> <?php } ?>
								</div>
							</div>
						
							
							
						</div>
						
						<div class="row">
						    	<div class="col-md-12">

								<div class="form-group">
									<input type="text" class="form-control" name="author_name" id="author_name" value="<?php if($mode == 'Add') { echo set_value('author_name'); } else { echo $form_data[0]['author_name']; } ?>" required>
									<label class="form-control-placeholder floatinglabel">Content Provider Name / Organization Name​<em>*</em></label>
									<?php if(form_error('author_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_name'); ?></label> <?php } ?>
								</div>
							</div>
							
						</div> 
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Banner Image </button>
									<input type="file" class="form-control" name="kr_banner" id="kr_banner" <?php if($mode == 'Add') { echo 'required'; } ?>>
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>		
								<?php if($kr_banner_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $kr_banner_error; ?></label> <?php } ?>
							</div>	
							
							<?php $dynamic_cls = $full_img = $onclick_fun = '';
								if($mode == 'Add')
								{
									$dynamic_cls = 'd-none';
								}
								else
								{
									$full_img = base_url().'uploads/kr_banner/'.$form_data[0]['kr_banner'];
								} ?>
								
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="blog_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
											<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
										</div>
									</div>
								</div>
						</div>


						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;"> Technology Domain <em>*</em></label>
									<!-- <div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dignissim enim sed nunc dapibus iaculis. Phasellus a massa at massa aliquam lacinia. Sed iaculis vulputate eros quis porttitor.</p>
									</div> -->
									<div class="boderBox65x">
										<select class="form-control select2_common" name="technology_ids[]" id="technology_ids" data-placeholder="Technology Domain" multiple onchange="show_hide_technology_domain_other()">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														$technology_ids_arr = array();
														if($mode == 'Add') { if(set_value('technology_ids[]') != "") { $technology_ids_arr = set_value('technology_ids[]'); } }
													else { $technology_ids_arr = explode(",",$form_data[0]['technology_ids']); } ?>
													<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$technology_ids_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
										<?php if($technology_ids_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $technology_ids_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-6" id="blog_technology_other_outer" style="margin-top:25px;">
								<div class="form-group">
									<input type="text" class="form-control" name="technology_ids_other" id="technology_ids_other" value="<?php if($mode == 'Add') { echo set_value('technology_ids_other'); } else { echo $form_data[0]['technology_other']; } ?>">
									<label class="form-control-placeholder floatinglabel">Technology Domain Other <em>*</em></label>
									<?php if(form_error('technology_ids_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('technology_ids_other'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

							<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Repository Type <em>*</em></label>
									<!-- <div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dignissim enim sed nunc dapibus iaculis. Phasellus a massa at massa aliquam lacinia. Sed iaculis vulputate eros quis porttitor.</p>
									</div> -->
									<div class="boderBox65x">
										<select class="form-control select2_common" name="kr_type" id="kr_type" data-placeholder="Knowledge Repository Type"  onchange="show_hide_kr_type_other()">
											<?php if(count($type_data) > 0)
												{	
													foreach($type_data as $res)
													{	
														$kr_type_arr = array();
														if($mode == 'Add') { if(set_value('kr_type[]') != "") { $kr_type_arr = set_value('kr_type[]'); } }
													else { $kr_type_arr = explode(",",$form_data[0]['kr_type']); } ?>
													<option data-id='<?php echo $res['type_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$kr_type_arr)) { echo 'selected'; } ?> ><?php echo $res['type_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
										<?php if($kr_type_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $kr_type_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-6" id="kr_type_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<input type="text" class="form-control" name="kr_type_other" id="kr_type_other" value="<?php if($mode == 'Add') { echo set_value('kr_type_other'); } else { echo $form_data[0]['kr_type_other']; } ?>">
									<label class="form-control-placeholder floatinglabel">Other Repository Type <em>*</em></label>
									<?php if(form_error('kr_type_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('kr_type_other'); ?></label> <?php } ?>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Tags <em>*</em> </label>
									<!-- <div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dignissim enim sed nunc dapibus iaculis. Phasellus a massa at massa aliquam lacinia. Sed iaculis vulputate eros quis porttitor.</p>
									</div> -->
									<div class="boderBox65x">
										<select class="form-control select2_common" name="tags[]" id="tags" data-placeholder="Tags" multiple  onchange="show_hide_tag_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = array();
														if($mode == 'Add') { if(set_value('tags[]') != "") { $tags_arr = set_value('tags[]'); } }
													else { $tags_arr = explode(",",$form_data[0]['tags']); } ?>
													<option data-id='<?php echo $res['tag_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>	
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>

							<div class="col-md-6" id="tag_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php if($mode == 'Add') { echo set_value('tag_other'); } else { echo $form_data[0]['tag_other']; } ?>">
									<label class="form-control-placeholder floatinglabel">Other Tag <em>*</em></label>
									<?php if(form_error('tag_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
								</div>
							</div>


						</div>

						<div class="row">
							<div class="col-md-12 mt-4">
								<div class="form-group">
									<label class="form-control-placeholder" style="<?php if($mode == 'Add') { ?>top:-25px !important<?php } ?>">Brief about the content up to 400 words​ <em>*</em></label>
									<textarea name="kr_description" id="kr_description" class="form-control" cols="12" rows="4"><?php if($mode == 'Add') { echo set_value('kr_description'); } else { echo $form_data[0]['kr_description']; } ?></textarea>
									<?php if(form_error('kr_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('kr_description'); ?></label> <?php } ?>									
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum">
								<input type="hidden" name="terms_condition_check" value="0">
							    <input type="checkbox" name="terms_condition_check" id="terms_condition_check" class="terms_condition_check"  value="1" <?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){echo 'checked';} ?> />
							    <label class="form-check-label" for="exampleCheck1">Content Providers Terms and Condition</label>
							  </div>
							  </div>
							
							</div>
						</div>
						<?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){
							$terms_condition_display= '';
						}else{
							$terms_condition_display= 'd-none';
						} ?>

						<div class="row terms_condition_row <?php echo $terms_condition_display ?>">
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control" name="terms_condition" id="terms_condition" ><?php if($mode == 'Add') { echo set_value('terms_condition'); } else { echo $form_data[0]['terms_condition']; } ?></textarea>
									<label class="form-control-placeholder floatinglabel">Terms & Conditions<em>*</em></label>
									<?php if(form_error('terms_condition')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('terms_condition'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row"><div class="col-md-12"><h4 class="titleBox">Files to be Published</h4></div></div>
						<div class="file-details" style="min-height:100px;">
							<?php /* <div class="form-group"><label class="form-control-placeholder"></label></div><br> */ ?>
							<?php if($mode == 'Update' && count($kr_files_data) > 0)
							{	
								echo '<div class="row">';
								foreach($kr_files_data as $res)
								{	?>
									<div class="col-md-2" id="kr_files_outer<?php echo $res['kr_id']."_".$res['file_id']; ?>">
										<div class="file-list">
												<a href="javascript:void(0)" onclick="remove_kr_file('<?php echo $res['kr_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close"><i class="fa fa-remove"></i></a>
												<a class="file_ext_title" href="<?php echo base_url().'uploads/kr_files/'.$res['file_name']; ?>" target="_blank">
													<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/kr_files/'.$res['file_name']; }
													else { $disp_img_name = ''; } ?>
													
													<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
													else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
													<!--span><i class="fa fa-file"></i></span> View-->
												</a>
										</div>
									</div>
					<?php	}
								echo '</div>';
							}	?>
							
							<input type="hidden" name="customFileCount" id="customFileCount"  value="<?php if($mode=='Update'){echo count($kr_files_data); }else{ echo 0;} ?>">
							<div class="row">											
								<div id="last_team_file_id"></div>
								<div class="col-md-2 custom-file" id="addFileBtnOuter">
									<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
									<input type="file" class="form-control fileUploadKr" name="kr_files[]" id="file-upload" />
									<div class="kr_files_err"></div>
								</div>
							</div>
						</div>
					
						
						
						
						<div class="row">
							
							<div class="col-md-12">
								<div class="custom-control custom-checkbox" id="accept_terms_err">
									<input type="checkbox" class="custom-control-input" id="accept_terms" name="accept_terms" <?php if($mode == 'Add') { if(set_value('accept_terms') == 'on') { echo 'checked'; } } ?> required>
									<label class="custom-control-label form-label" for="accept_terms">I hereby agree to Technovuus Terms and Conditions for Publishing this content <em>*</em></label>
								</div>
							</div>
						</div><br>
						
						<div class="row">
							<div class="col-md-4">
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary btnCancelCustom">Back</a> 
							</div>
							<div class="col-md-4 text-center">
								<?php if($mode == 'Add') { ?><a href="javascript:void(0)" onclick="confirm_clear_all()" class="btn btn-primary btnCancelCustom">Clear Form</a><?php } ?> 
							</div>	
							<div class="col-md-4 text-right">
								<button type="submit" class="btn btn-primary add_button"><?php if($mode=='Update') {echo 'Update';}else{echo 'Upload';} ?></button> 
							</div>
						</div>						
					</form>
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php if($mode == 'Add' && set_value('author_name') == '') { ?>
	<script>get_author_name();</script>
<?php } ?>

<script type="text/javascript">	

	$("#terms_condition_check").change(function () {

	    if ($('input.terms_condition_check').is(':checked')) {
	    	$(".terms_condition_row").removeClass('d-none');
	    }else{
	    	$("#terms_condition").val("");
	    	$(".terms_condition_row").addClass('d-none');
	    }

	});
	// CKEDITOR.replace('blog_description');
	
	$('.select2_common').select2();


	function show_hide_technology_domain_other()
	{
		$("#blog_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#technology_ids').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#blog_technology_other_outer").show();
			$("#technology_ids_other").prop("required", true);
		}
		else
		{
			$("#blog_technology_other_outer").hide();
			$("#technology_ids_other").prop("required", false);
			$("#technology_ids_other").val('');
		}
	}
	show_hide_technology_domain_other();
	
	function show_hide_kr_type_other()
	{
		$("#kr_type_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#kr_type').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#kr_type_other_outer").show();
			$("#kr_type_other").prop("required", true);
		}
		else
		{
			$("#kr_type_other_outer").hide();
			$("#kr_type_other").prop("required", false);
			$("#kr_type_other").val('');
		}
	}
	show_hide_kr_type_other();






	function show_hide_tag_other()
	{
		$("#tag_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#tag_other_outer").show();
			$("#tag_other").prop("required", true);
		}
		else
		{
			$("#tag_other_outer").hide();
			$("#tag_other").prop("required", false);
			$("#tag_other").val('');
		}
	}
	show_hide_tag_other();
	
	function enable_disable_inputs(checkbox_id, input_id)
	{
		if($("#"+checkbox_id).prop("checked") == true)
		{
			$("#"+input_id+"_outer").css("display", 'block');
			$("#"+input_id).prop("disabled", false);
			$("#"+input_id).prop("readonly", false);
			$("#"+input_id).prop("required", true);
			$("#"+input_id).focus();
			
			if(input_id == 'author_professional_status' || input_id == 'author_org_name')
			{
				//$("#preloader").css("display", "block");
				parameters = { 'checkbox_id':checkbox_id, 'input_id':input_id, 'csrf_test_name':$('.token').val() }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						if(data.flag == "success")
						{ 
							$(".token").val(data.token);
							if(input_id == 'author_professional_status') { $("#author_professional_status").val(data.professional_status); }
							if(input_id == 'author_org_name') { $("#author_org_name").val(data.org_name); }						
							$("#preloader").css("display", "none");
						}
						else 
						{	}
					}
				});
			}
		}
		else if($("#"+checkbox_id).prop("checked") == false)
		{
			$("#"+input_id+"_outer").css("display", 'none');
			$("#"+input_id).prop("disabled", true);
			$("#"+input_id).prop("readonly", true);
			$("#"+input_id).prop("required", false);
			//$("#"+input_id).val("");
		}
	}
	
	function get_author_name()
	{
		//$("#preloader").css("display", "block");
		parameters = { 'input_id':'author_name', 'csrf_test_name':$('.token').val() }
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('knowledge_repository/get_autor_details'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{ 
					$(".token").val(data.token);
					$("#author_name").val(data.author_name);					
					$("#preloader").css("display", "none");
				}
				else 
				{	}
			}
		});
	}
	
	function confirm_clear_all()
	{
		swal(
		{  
			title:"Confirm?",
			text: "This action will clear the complete form. Please confirm if you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); location.reload(); } });
	}
</script>

<?php if($mode == 'Add' && set_value('author_name') == '') { ?>
	<script>get_author_name();</script>
<?php } ?>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod("valid_files_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".pdf",".xls",".doc",".csv",".xlsx",".pptx",".ppt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');
		
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		
		//******* JQUERY VALIDATION *********
		$("#BlogForm").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				title_of_the_content: { required: true, nowhitespace: true, valid_value: true },
				kr_description: 
				{
					required: true,
					minCount:['5'],
					maxCount:['400']
				},
				kr_banner: { <?php if($mode == 'Add') { ?>required: true,<?php } ?> valid_img_format: true,filesize:2000000},
				"technology_ids[]": { required: true},
				"tags[]": { required: true},
				"kr_files[]": { custum_required: true},
				author_name: { required: true, nowhitespace: true },
				accept_terms: { required: true},
				terms_condition: {
	               required: function() {
	               return $("input[name=terms_condition_check]:checked").val() == 1; 
	               }
               
              	},
				
				/* end_time: { required: true, chk_valid_time:true },
					cost_type: { required: true},
					cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
					"webinar_technology[]": { required: true },
					exclusive_technovuus_event:{required: true },
					registration_link:{required: true,valid_url:true },
					hosting_link:{required: true,valid_url:true },
					breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
					key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] }, */
			},
			messages:
			{
				title_of_the_content: { required: "This field is required", nowhitespace: "Please enter the title" },
				kr_description: { required: "This field is required", nowhitespace: "Please enter the description" },				
				kr_banner:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				"kr_files[]":{required: "This field is required"},
				author_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				accept_terms:{required: "This field is required"},
				"kr_files[]":{required:"This field is required"}
				
				/* start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}		 */					
			},
			<?php if($mode == 'Update') { ?>
			submitHandler: function(form)
			{

				/* form.submit(); */
				swal(
				{  
					title:"Confirm?",
					text: "Are you sure you want to update the knowledge repository? as you updated the knowledge repository, it needs admin approval to activate the knowledge repository again",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
				}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); form.submit(); } });
			},
			<?php } ?>
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "accept_terms") 
				{
					error.insertAfter("#accept_terms_err");
				}
				else if (element.attr("name") == "kr_files[]"){
					error.insertAfter(".kr_files_err");
					
				}
				else
				{
					element.closest('.form-group').append(error);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BlogImagePreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#blog_img_outer").removeClass('d-none');
					//$("#blog_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#blog_img_outer .previous_img_outer img").attr("src", e.target.result);
					//$("#blog_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#blog_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#kr_banner").change(function() { BlogImagePreview(this); });	



	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function FilesPreview(input, public_private_flag) 
	{
		
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			var upload_file_size = input.files[0].size; //1000000 Bytes = 1 Mb 
			if(upload_file_size < 5000000)
			{
				var disp_img_name = "";
				var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
				if(extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf' ||
					extension == 'xls' || extension == 'doc' || extension == 'docx' || extension == 'csv' || extension == 'xlsx' ||
					extension == 'pptx' || extension == 'ppt'  )
				{

				
				disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
					
				var id_name = '';
				var btn_id_name = '';
				var input_cls_name = '';
				var input_type_name = '';
				var fun_var_parameter = "";
				var file_upload_common = '';
				if(public_private_flag == 'public')
				{
					id_name = 'kr_files_outer'+j;
					btn_id_name = 'addFileBtnOuter';
					input_cls_name = 'fileUploadKr';
					input_type_name = 'kr_files[]';
					fun_var_parameter = "'public'";
					file_upload_common = 'file-upload';
				}
			
				
				var append_str = '';
				append_str += '	<div class="col-md-2 kr_files_outer_common" id="'+id_name+'">';
				append_str += '		<div class="file-list">';
				append_str += '			<a href="javascript:void(0)" onclick="remove_kr_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
				//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
				append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
				append_str += '		</div>';
				append_str += '	</div>';				
					
				var btn_str = '';
				btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
				btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
				btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="FilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
				btn_str +=	'	</div> <div class="kr_files_err"></div';
					
				if(public_private_flag == 'public')
				{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
					
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
				}
				}else{
					swal( 'Error!','Invalid File Extension','error');
				}
			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 5 MB','error');
			}
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadKr").change(function() { FilesPreview(this,'public'); });	



	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_kr_file(div_id, file_id, public_private_flag)
	{
		var FileCount = $("#customFileCount").val();
		if (FileCount==1) {
			swal("Warning","You can not remove all files","warning");
			return false;
		}

		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				var customFileCount = $("#customFileCount").val();
					$("#customFileCount").val(parseInt(customFileCount)-1);
				
				if(file_id!= '' && file_id != '0')
				{
					
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("knowledge_repository/delete_kr_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				
				if(public_private_flag == 'public')
				{
				$("#kr_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				
				
				$("#preloader").css("display", "none");
			}
		});
	}
	
</script>		