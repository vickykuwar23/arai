<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	.MyTeamsListingTab ul li:before { display:none; }
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Knowledge Repository</h1>        
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">			
			<div class="col-md-12">
				<div class="formInfo">
					
					<section>
						<div class="product_details MyTeamsListingTab">
							<!-- <ul class="nav nav-tabs align-item-center justify-content-center d-none" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link active" href="javascript:void(0)">My Webinar's</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo base_url('webinar/appliedWebinar'); ?>">Webinar's I have Applied To</a></li>
							</ul> -->
							
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="TeamTab1">
									
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
												<div class="table-responsive">
													<table id="blog-list" class="table table-bordered table-hover challenges-listing-page">
														<thead>
															<tr>
																<th scope="col">TNKID</th>
																<th scope="col">Content Title</th>
																<!-- <th scope="col">Author</th> -->
																<th scope="col">Posted ON</th>
																<th scope="col">Status</th>
																<th scope="col">Type</th>
																<th scope="col">Likes</th>
																<th scope="col">Number of Downloads</th>
																<th scope="col">Reports</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>							
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlogReportpopupLabel">Knowledge Repository Delete</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div class="bgNewBox65">
				<p>Knowledge Repository ID : <span id="popupBlogId"></span></p>
				<p>Knowledge Repository Title : <span id="popupBlogTitle"></span></p>
				</div>
					
					<div class="form-group mt-4">
						
						<label for="popupKrDeleteComment" class="">Reason <em>*</em></label>
						<textarea class="form-control" id="popupKrDeleteComment" name="popupKrDeleteComment" ></textarea>
						<span id="popupKrDeleteComment_err" class='error'></span>
					</div>
				</div>	
				<input type="hidden" id="popupBlogIdHidden" name="">
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submit_blog_report()">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	

<script type="text/javascript">
	
	function delete_kr_new(kr_id,  kr_disp_id, kr_title)
	{
				$("#popupBlogId").html(kr_disp_id);
				$("#popupBlogIdHidden").val(kr_id);
				$("#popupBlogTitle").html(kr_title);
				$("#popupKrDeleteComment").val('');
				$("#popupKrDeleteComment_err").html('');
				$("#BlogReportpopup").modal('show');
				$("#popupKrDeleteComment").focus();
			
	}	

	function check_report_validation()
	{
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		if(popupKrDeleteComment.trim() == "") { $("#popupKrDeleteComment_err").html('Please enter the delete reason'); $("#popupKrDeleteComment").focus(); }
		else { $("#popupKrDeleteComment_err").html(''); }
	}

	function submit_blog_report(kr_id)
	{	
		check_report_validation();
		
		var popupKrDeleteComment = $("#popupKrDeleteComment").val();
		var kr_id = $("#popupBlogIdHidden").val();
		if(popupKrDeleteComment.trim() != "")		
		{
			$("#popupKrDeleteComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to remove the selected knowledge repository? This action will remove this entry from your listing, and will no longer be available for the reference.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("knowledge_repository/delete_kr"); ?>',
						data:{ 'kr_id':kr_id, 'popupKrDeleteComment':popupKrDeleteComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$("#BlogReportpopup").modal('hide');
							$(".token").val(data.token);
						
							$('#blog-list').DataTable().ajax.reload();	
							swal(
							{
								title: 'Success!',
								text: "knowledge repository successfully deleted.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}
</script>
<script>
  $(document).ready( function () 
	{	  
		var base_path = '<?php echo base_url() ?>';
		var table = $('#blog-list').DataTable({
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": base_path+"knowledge_repository/my_kr_listing",
				"type":"POST",
				"data":function(data) 
				{						
					/* data.techonogy_id = $("#techonogy_id").val();
					data.change_visibility = $("#change_visibility").val();
					data.c_status = $("#c_status").val();
					data.audience_pref = $("#audience_pref").val(); */
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": {
					401:function(responseObject, textStatus, jqXHR) {			
					},
				},
			}			
		});
	});
	
	function block_unblock_kr(id, type)
	{
		var text2='from';
		if (type=='Restore') {text2='in';}
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to "+type+" the selected knowledge repository "+text2+" front listing?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("knowledge_repository/block_unblock_kr"); ?>',
					data: {'id':id, 'type':type, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Knowledge repository successfully "+type+"d.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function delete_kr(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected knowledge repository?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("knowledge_repository/delete_kr"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "knowledge repository successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}


	function preventDelete(){
		swal({
				title: 'Warning!',
				text: "Please delete knowledge repository from front list.",
				type: 'warning',
				showCancelButton: false,
				confirmButtonText: 'Ok'
			});
	}
	
	function show_blog_reported(blog_id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("blogs_technology_wall/show_blog_reported_ajax"); ?>',
			data:{ 'blog_id':blog_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	

	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });


		function show_blog_likes(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("knowledge_repository/show_blog_likes_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function show_blog_reports(kr_id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("knowledge_repository/show_blog_reported_ajax"); ?>',
			data:{ 'kr_id':kr_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}

	function show_downloads(kr_id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("knowledge_repository/show_kr_downloads_ajax"); ?>',
			data:{ 'id':kr_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#BlogLikesLabel").html(data.blog_title);				
					$("#BlogLikesContent").html(data.response);				
					$("#BlogLikes").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
</script>