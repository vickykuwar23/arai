<?php $encObj =  New Opensslencryptdecrypt();

if(count($comment_data) > 0)
{
	foreach($comment_data as $res)
	{
		$iconImg = base_url('assets/no-img.png');
		if($res['user_category_id'] == 2)
		{
			if($res['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encObj->decrypt($res['org_logo'])); }
		}
		else 
		{
			if($res['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$res['profile_picture']); }
		} 
		
		$title = $encObj->decrypt($res['title']);
		$first_name = $encObj->decrypt($res['first_name']);
		$middle_name = $encObj->decrypt($res['middle_name']);
		$last_name = $encObj->decrypt($res['last_name']);
		
		$disp_name = $title." ".$first_name;
		if($middle_name != '') { $disp_name .= " ".$middle_name." "; }
		$disp_name .= $last_name; ?>
		
		<div class="comment_block_common" id="comment_block_<?php echo $res['comment_id']; ?>">
			<div class="comment_img"><img src="<?php echo $iconImg; ?>"></div>
			<div class="comment_inner">
				<p class="comment_name"><?php echo $disp_name; ?></p>
				<p class="comment_time">
					<?php echo $this->Common_model_sm->time_Ago(strtotime($res['created_on']), $res['created_on']); ?>
				</p>
				<p class="comment_content"><?php echo $res['comment']; ?></p>	
				
				<?php if(in_array($res['comment_id'], $CommentActiondata['like_comments'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down'; }
				else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
				
				<span id="like_unlike_comment_btn_outer_<?php echo $res['comment_id']; ?>">
					<p class="comment_like_btn" onclick="like_dislike_comment('<?php echo base64_encode($res['comment_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label." (".$res['total_likes'].")"; ?></p>
				</span>&nbsp;&nbsp;|&nbsp;&nbsp;
				
				<span id="reply_comment_btn_outer_<?php echo $res['comment_id']; ?>">
					<p class="comment_like_btn" onclick="show_hide_reply('<?php echo $res['comment_id']; ?>')"><i class="fa fa fa-reply" aria-hidden="true"></i> Reply <?php echo '('.$res['TotalReply'].')' ?></p>
				</span>
				
				<?php if(in_array($res['comment_id'], $CommentActiondata['self_comments']) || $user_id == $blog_data[0]['user_id']) 
				{	?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<span id="delete_comment_btn_outer_<?php echo $res['comment_id']; ?>">
						<p class="comment_like_btn" onclick="delete_comment('<?php echo base64_encode($res['comment_id']); ?>', '')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
					</span>
				<?php } ?>
			</div><div class="clearfix"></div>
			
			<div id='comment_reply_<?php echo $res['comment_id']; ?>' class="d-none">
				<div class='comment_block_common_reply'>
					<div class="row">
						<div class="col-md-10">
							<input type="text" class="form-control comment_reply_input" name="send_reply_comment" id="send_reply_comment_<?php echo $res['comment_id']; ?>" data-id="<?php echo $res['comment_id']; ?>" placeholder="Add reply to comment" onkeyup="check_reply_comment_validation('<?php echo $res['comment_id']; ?>')">
							<span id="reply_comment_error_<?php echo $res['comment_id']; ?>" class='error'></span>
						</div>
						<div class="col-md-2">
							<button type="button" class="customBtnBlog" onclick="post_comment_reply('<?php echo $res['comment_id']; ?>')">Reply</button>
						</div>
					</div>
				</div>
				
				<div id="append_reply_div_<?php echo $res['comment_id']; ?>"></div>
				
				<?php if($res['TotalReply'] > 0)
				{
					$select = "bc.comment_id, bc.blog_id, bc.user_id, bc.parent_comment_id, bc.comment, bc.total_likes, bc.created_on, r.user_category_id, r.title, r.gender, r.first_name, r.middle_name, r.last_name, r.email, r.mobile, r.institution_full_name, org.org_logo, sp.profile_picture, (SELECT COUNT(comment_id) FROM arai_blog_comments WHERE parent_comment_id = bc.comment_id AND status = 1) AS TotalReply";
					$this->db->join('arai_registration r','r.user_id = bc.user_id', 'LEFT', FALSE);
					$this->db->join('arai_profile_organization org','org.user_id = bc.user_id', 'LEFT', FALSE);
					$this->db->join('arai_student_profile sp','sp.user_id = bc.user_id', 'LEFT', FALSE);
					$get_reply_comments = $this->master_model->getRecords("arai_blog_comments bc",array('bc.status' => '1', 'bc.parent_comment_id' => $res['comment_id'], 'bc.blog_id' => $blog_id, 'r.is_verified'=>"yes", 'r.status'=>"Active", 'r.is_valid'=>"1", 'r.is_deleted'=>"0"),$select, array('bc.created_on','DESC'));
					
					foreach($get_reply_comments as $subCmt)
					{	
						$iconImg = base_url('assets/no-img.png');
						if($subCmt['user_category_id'] == 2)
						{
							if($subCmt['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encObj->decrypt($subCmt['org_logo'])); }
						}
						else 
						{
							if($subCmt['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$subCmt['profile_picture']); }
						} 
			
						$title = $encObj->decrypt($subCmt['title']);
						$first_name = $encObj->decrypt($subCmt['first_name']);
						$middle_name = $encObj->decrypt($subCmt['middle_name']);
						$last_name = $encObj->decrypt($subCmt['last_name']);
			
						$disp_name = $title." ".$first_name;
						if($middle_name != '') { $disp_name .= " ".$middle_name." "; }
						$disp_name .= $last_name; ?>			
						<div class="comment_block_common comment_block_common_reply" id="comment_block_<?php echo $subCmt['comment_id']; ?>">
							<div class="comment_img"><img src="<?php echo $iconImg; ?>"></div>
							<div class="comment_inner">
								<p class="comment_name"><?php echo $disp_name; ?></p>
								<p class="comment_time">
									<?php echo $this->Common_model_sm->time_Ago(strtotime($subCmt['created_on']), $subCmt['created_on']); ?>
								</p>
								<p class="comment_content"><?php echo $subCmt['comment']; ?></p>	
								
								<?php if(in_array($subCmt['comment_id'], $CommentActiondata['like_comments'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down'; }
								else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
								
								<span id="like_unlike_comment_btn_outer_<?php echo $subCmt['comment_id']; ?>">
									<p class="comment_like_btn" onclick="like_dislike_comment('<?php echo base64_encode($subCmt['comment_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label." (".$subCmt['total_likes'].")"; ?></p>
								</span>
								
								<?php if(in_array($subCmt['comment_id'], $CommentActiondata['self_comments']) || $user_id == $blog_data[0]['user_id']) 
								{	?>&nbsp;&nbsp;|&nbsp;&nbsp;
									<span id="delete_comment_btn_outer_<?php echo $subCmt['comment_id']; ?>">
										<p class="comment_like_btn" onclick="delete_comment('<?php echo base64_encode($subCmt['comment_id']); ?>', 'reply')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</p>
									</span>
								<?php } ?>
							</div><div class="clearfix"></div>						
						</div>
	<?php		}
				}	?>
				</div>
		</div>
<?php	}

		if($new_start < $comment_data_count)
		{ ?>
		<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
			<a href="javascript:void(0)" class="click-more" onclick="get_comment_data('<?php echo $blog_id; ?>', '<?php echo $new_start; ?>', '<?php echo $comment_limit; ?>', '<?php echo $user_id; ?>', '<?php echo $sort_order; ?>', 0, '<?php echo $new_comment_id; ?>', 0);">Show More</a>				
		</div>
		<?php }
} ?>

<?php
if($blog_data[0]['admin_status'] == 1 && $blog_data[0]['is_block'] == 0) { $action_flag = 0; }
else { $action_flag = 1; } ?>

<script>
	function check_reply_comment_validation(comment_id)
	{
		var reply_comment_content = $("#send_reply_comment_"+comment_id).val();
		if(reply_comment_content.trim() == "") { $("#reply_comment_error_"+comment_id).html('Please enter your reply'); }
		else { $("#reply_comment_error_"+comment_id).html(''); }
	}
	
	$('.comment_reply_input').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			post_comment_reply($(this).data("id"))
		} 
	}); 
	
	function post_comment_reply(comment_id)
	{
		<?php if($action_flag == 1) { ?>
			$("#send_reply_comment_"+comment_id).val("");	
			swal(
			{
				title: 'Alert!',
				html: "You can not reply to this comment as the blog status is Pending or Blocked",
				type: 'warning'				
			});
		<?php }else { ?>
			check_reply_comment_validation(comment_id);
			
			var comment_content = $("#send_reply_comment_"+comment_id).val();
			if(comment_content.trim() != "") 
			{ 
				var cs_t = 	$('.token').val();
				var user_id = "<?php echo $user_id; ?>";
				var blog_id = "<?php echo $blog_id; ?>";
				var comment_id = 	comment_id;
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("blogs_technology_wall/post_comment_ajax"); ?>',
					data:{ 'user_id':user_id, 'blog_id':blog_id, 'comment_id':comment_id, 'comment_content':comment_content, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						$("#send_reply_comment_"+comment_id).val("");	
						$("#reply_comment_btn_outer_"+comment_id).html(data.new_comment_reply_cnt)
						$("#append_reply_div_"+comment_id).replaceWith(data.response);
					}
				});
			}
		<?php } ?>
	}	
</script>