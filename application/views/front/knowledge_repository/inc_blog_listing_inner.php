<?php $encObj =  New Opensslencryptdecrypt(); ?>
<div class="row">
	<div class="col-md-5">
		<a href="<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id'])); ?>" style="display: block; text-align: center; background: #f0f0f0; ">
			<img src="<?php echo base_url('uploads/blog_banner/'.$res['blog_banner']); ?>" title="<?php echo ucfirst($res['blog_title']); ?>" class="img-fluid" style="max-height:240px;">		
		</a> 
		
		<div class="textInfoAbout3">
			<div class="row userPhoto-info">
				<div class="col-md-3">
					<?php $iconImg = base_url('assets/no-img.png');
					if($res['user_category_id'] == 2)
					{
						if($res['org_logo'] != "") { $iconImg = base_url('uploads/organization_profile/'.$encObj->decrypt($res['org_logo'])); }
					}
					else 
					{
						if($res['profile_picture'] != "") { $iconImg = base_url('assets/profile_picture/'.$res['profile_picture']); }
					} ?>
					
					<a href="javascript:void(0)" onclick="get_user_details('<?php echo base64_encode($res['user_id']); ?>')">
						<img src="<?php echo $iconImg; ?>" alt="" title="" class="img-fluid">
					</a>
				</div>
				<div class="col-md-9 pl-0">
					<h3 class="mt-2"><a href="javascript:void(0)" style="color:#41464b" onclick="get_user_details('<?php echo base64_encode($res['user_id']); ?>')"><?php echo $res['author_name']; ?></a></h3>
					<div class="postInfo">
						<ul>
							<li><strong>Posted</strong> on <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d-m-Y", strtotime($res['created_on'])); ?>  at <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date("h:i a", strtotime($res['created_on'])); ?> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<h4 style='font-size: 20px;line-height: 20px;font-weight: 500;'>
			<a href="<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id'])); ?>" style="color:#41464b;"><?php echo ucfirst($res['blog_title']); ?></a>			
		</h4>
		<p style='font-weight:600'>Blog ID : <a href="<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id'])); ?>" style="color:#000;font-weight:500"><?php echo $res['blog_id_disp']; ?></a></p>
		<?php if($res['DispTags'] != "") 
			{ 
			$tags_arr = explode("##",$res['DispTags']); ?>
			<div class="tag">
				<ul>
					<li style='font-weight:600'>Tags:</li> 
					<?php foreach($tags_arr as $tag)
						{	?>
						<li><span><?php echo $tag; ?></span></li> 
					<?php }	?>
				</ul>
			</div>
		<?php } ?>
		
		<p>
			<?php $blogDesc = strip_tags(htmlspecialchars_decode($res['blog_description']));
				echo substr($blogDesc, 0, 450); 
			if(strlen($blogDesc) > 450) { echo '... '; ?><span onclick="read_more_description('<?php echo base64_encode($res['blog_id']); ?>')" style="cursor:pointer;"><b>Read More</b></span> <?php } ?>
		</p>
		
		<div class="shareList mt-4">
			<ul>
				<?php if(in_array($res['blog_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
				else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
				<li id="like_unlike_btn_outer_<?php echo $res['blog_id']; ?>">
					<span onclick="like_unlike_blog('<?php echo base64_encode($res['blog_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
				</li> 
				
				<li>
					<span onclick="share_blog('<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
				</li> 
				
				<li><span onclick="window.location.href='<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($res['blog_id']).'/1'); ?>'"><i class="fa fa-comments" aria-hidden="true"></i> Comments</span></li> 
				
				<?php if(in_array($res['blog_id'], $BlogActiondata['self_blog'])) { ?>
					<li><span onclick="delete_blog_from_user_listing('<?php echo base64_encode($res['blog_id']); ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</span></li> 
				<?php } ?>
			</ul>
		</div>
	</div>				
</div>
