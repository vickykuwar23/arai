<?php $encObj =  New Opensslencryptdecrypt(); ?>

		<div class="">
                    <div class="boxShadow75 shareKR textPart">
                        <img src="<?php echo base_url('uploads/kr_banner/').$res['kr_banner'] ?>" alt=" img">

                        <ul style="text-align: center">
                        	<li class="id_disp"><a href="javascript:void(0)" style="cursor:auto; ">
                                        	
                                        <span style="cursor:auto; background: #FFF; color: #c80032;"><?php echo $res['kr_id_disp']; ?></span></a> </li>
                                    </ul>
                                    <ul>
                        	<?php $getTotalLikes = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $res['kr_id']), 'like_id'); ?>
									<li style="cursor:auto; " id="like_count_outer_<?php echo $res['kr_id']; ?>"><span style="cursor:auto;  text-align: center; background: #FFF; color: #c80032; "><?php echo $getTotalLikes ?> </span></li>
                                        
					            	<?php if(in_array($res['kr_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
									else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
									<li id="like_unlike_btn_outer_<?php echo $res['kr_id']; ?>">
										<span onclick="like_unlike_blog('<?php echo base64_encode($res['kr_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?> </span> 
									</li>
									<li style="float: right;">
									<span onclick="share_blog('<?php echo site_url('knowledge_repository/details/'.base64_encode($res['kr_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
									</li> 
                    
                                    </ul>
                        <p style="height: 45px"><strong title="<?php echo ucfirst($res['title_of_the_content']); ?>">
                        	<?php ///echo ucfirst($res['title_of_the_content']); ?>

                        	<?php  if(strlen($res['title_of_the_content']) > 65 ){ 
                        		$srt_to_show= ucfirst(substr($res['title_of_the_content'], 0,65) )."..."; 
                        		echo wordwrap($srt_to_show, 32, "\n", true);
                        		} else {echo ucfirst($res['title_of_the_content']);} ?>

                        	
                        		
                        	</strong></p>

                        <div class="bgNewBox45 mt-5 mb-3">
                           
                           <p class="mb-0" title="<?php echo $res['author_name']; ?>"><strong>By: </strong>
                            	<?php if(strlen($res['author_name']) > 35 ){ echo ucfirst(substr($res['author_name'], 0,35) )."..."; } else {echo ucfirst($res['author_name']);} ?>	
                            </p>

                            <p class="mb-0"><strong>Type:</strong> <?php if(strtolower($res['type_name'])=='other' && $res['kr_type_other']!='') { echo $res['kr_type_other'] ;}else{echo $res['type_name'];} ?></p>
                            <p class="mb-0"><strong>Published Date :</strong> <?php echo date("jS F Y", strtotime($res['created_on'])); ?></p>
                        </div>


                         
							<?php if($res['DispTechnology'] != "" || $res['technology_other']!=''){ ?>
								<strong>Technology Domain:</strong> 
								<div class="owl-carousel mb-2 owl-theme tagSlider">
								 <?php $tech_str = ''; 
								if($res['DispTechnology'] != "") 
								{
								$techArr = explode("##",$res['DispTechnology']); 	
										foreach($techArr as $res_a)
									{
										if(strtolower(trim($res_a)) != 'other')
										{ ?>
											<div><a style="cursor:auto; ">
												<?php //echo $res_a; ?>
												<?php if(strlen($res_a) > 15 ) {echo substr($res_a,0, 15)."..." ; }else{ echo $res_a; }; ?>
												
											</a></div> 
									<?php }} } ?>
									<?php if (count($techArr) == 1 && $res['technology_other']!='') {
													$style='cursor:auto; min-width:250px ';
												}else{
													$style='cursor:auto; ';
									} ?>

									<?php /* if ($res['technology_other']!=''): ?>
										<div><a style="<?php echo $style ?>">
											<?php //echo $res['technology_other']; ?>
											<?php if(strlen($res['technology_other']) > 20 ) {echo substr($res['technology_other'],0, 20)."..." ; }else{ echo $res['technology_other']; }; ?>

										</span></div>
									<?php endif */ ?>

									<?php if ($res['technology_other']!=''){ 
                                                    $technology_other_arr=explode(',', $res['technology_other']);
                                                    foreach ($technology_other_arr as $key => $technology_other) {
                                                      ?>
                                                    <div><a style="<?php echo $style ?>">
                                                    <?php 
                                                    if(strlen($technology_other) > 15 ) {echo substr($technology_other,0, 15)."..." ; }else{ echo $technology_other; }; ?>

                                                                        
                                                    </a></div>
                                    <?php  } } ?>
									</div>
								<?php } ?>


			        <?php if($res['DispTags'] != "" || $res['tag_other']!='') 
										{ ?>
							<div style="margin: -25px 0 0 0;">
							
								<strong>Tags:</strong> 
								<div class="owl-carousel mb-2 owl-theme tagSlider">
								<?php if($res['DispTags'] != "") 
								{ 
								$tags_arr = explode("##",$res['DispTags']);	

								foreach($tags_arr as $tag)
									{	?>
									<div><a style="cursor:auto; ">
										<?php if(strlen($tag) > 15 ) {echo substr($tag,0, 15)."..." ; }else{ echo $tag; }; ?>
											
										</a></div> 
								<?php }	}?>
								<?php if ($res['DispTags'] == "" && $res['tag_other']!='') {
													$style='cursor:auto; min-width:250px ';
									}else{
													$style='cursor:auto;  min-width:100px ';
									} ?>
									
								  <?php if ($res['tag_other']!=''){ 
                                                    $tag_other_arr=explode(',', $res['tag_other']);
                                                    foreach ($tag_other_arr as $key => $tag_other) {
                                                      ?>
                                                                <div><a style="<?php echo $style ?>">
                                                                    <?php 
                                                                    if(strlen($tag_other) > 15 ) {echo substr($tag_other,0, 15)."..." ; }else{ echo $tag_other; }; ?>

                                                                        
                                                                    </a></div>
                                    <?php  } } ?>
						
								</div>
							</div>
						<?php } ?>


                          <a href="<?php echo base_url('knowledge_repository/details/').base64_encode($res['kr_id'])  ?>"  class="btn btn-primary">Read More </a>

                        <button  class="btn btn-primary float-right" onclick="open_download_modal('<?php echo $res['kr_id'] ?>')">Download</button>
                    </div>
             </div>
