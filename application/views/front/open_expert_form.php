<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
textarea {
        resize: none; padding:10px 0;
    }
	#connectExpertModal .modal-dialog {
    top: 10%;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header" style="text-align: center;display: block;">
		<img src="<?php echo base_url('assets/front/img/logo1.png'); ?>" alt="logo" style="width: 40%;">
		<h5 style="display: block;clear: both;width: 100%;background: #e23751;color: #FFF; margin-top:10px; font-size:16px; border-radius: 5px;" class="modal-title" id="SubscriptionModalLabel">Ask your technical question to our Experts</h5>
		<button style="margin-top: -100px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="expertForm" name="expertForm" role="form">
		
		<div class="pl-3 pr-3 pb-3">
			<div class="form-group">
				<!--<input type="text" name="expert_subject" id="expert_subject" class="form-control" value=""  required="" />-->
				<textarea rows="2" cols="12" name="expert_subject" id="expert_subject" class="form-control valid-new"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Subject <em>*</em></label>
			</div>			
			<div class="form-group">
				<!--<input type="text" name="expert_question" id="expert_question" class="form-control" value=""  required="" />-->
				<textarea rows="2" cols="12" name="expert_question" id="expert_question" class="form-control valid-new"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Question <em>*</em></label>
			</div>
			<div class="form-group">				
				<textarea rows="3" cols="12" name="expert_remark" id="expert_remark" class="form-control valid-new" ></textarea>					
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Remarks (If any)</label>
			</div>
			<div class="form-group">	
			<label>Tag the Expert</label>	
			<select name="connect_to_expert" id="connect_to_expert" class=" form-control" autocomplete="nope" onchange="show_designation(this.value)">
			<option></option>
        <?php foreach ($all_experts as $key => $expert) { ?>
    
          <option value="<?php echo $expert['user_id'] ?>"><?php echo $expert['title']." ".$expert['first_name']." ".$expert['last_name'] ?></option>
       
        <?php } ?>
      </select>
    </div>
    <?php foreach ($all_experts as $key => $expert) { 
    	if(array_key_exists('designation', $expert)){
    ?>

    <p id="expert_designation_<?php echo$expert['user_id'] ?>" class="expert_designation d-none"><?php echo ucfirst($expert['designation'][0]); ?></p>
    <?php } } ?>

		<div class="modal-footer" style="border-top:none; padding:0;">
			<div class="text-center" ><button type="submit" class="btn btn-primary" id="email_subscription_btn_submit">Submit</button></div>
		</div>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>



<script>

	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	


		function show_designation(user_id){
			$('.expert_designation').addClass('d-none');
			if (user_id!='') {
				if($("#expert_designation_"+user_id).length > 0) {
				$('#expert_designation_'+user_id).removeClass('d-none');
				}
			}
		}
	
	
	$(document).ready(function () {

		
$('#connect_to_expert').select2({
	    allowClear: true,
      placeholder : "Please select the expert",
      closeOnSelect : true,
      width: '100%',
  });



		// Word Count Validation
		/*function getWordCount(wordString) {
			var words = wordString.split(" ");
			words = words.filter(function(words) { 
				return words.length > 0
			}).length;
			return words;
		}*/
	
		jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);			
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));	
	
		$("#expertForm" ).validate({			
			/*debug: true,
			 onkeyup: true, */
			load: true,
			blur: true,
			change: true,
			keypress: true,
			keyup: true,
			keydown: true,
			onclick: true,
			rules: {
				expert_subject: {
					required: true,
					minCount:['1'],
					maxCount:['50']	
				},
				expert_question: {
					required: true,
					minCount:['1'],
					maxCount:['100']	
				},
				expert_remark: {	
					maxCount:['100']	
				}
			},
			messages: {
				expert_subject: {
					required: "This field is required"
				},
				expert_question: {
					required: "This field is required"
				},
				expert_remark: {
					maxCount: "Maximum 100 words are allowed."
				}
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				
				$("#preloader-loader").css("display", "block");
				var expert_subject  = $('#expert_subject').val();
				var expert_question = $('#expert_question').val();
				var expert_remark 	= $('#expert_remark').val();
				var connect_to_expert = $('#connect_to_expert').val();
				$.ajax({
					url: "<?php echo base_url('home/open_expert_form_insert'); ?>", 
					type: "POST",             
					//data: { "email" : email },
					data:$('#expertForm').serialize(),
					dataType: 'JSON',
					cache: false,
					success: function(response) 
					{
						$("#connectExpertModal").modal('hide');
						$("#preloader-loader").css("display", "none");
						swal({
							title: response.flag,
							text: response.message,
							icon: response.flag,
							}).then(function() {
							//location.reload();
						});
					}
				});
				return false;
			}
		});
	
	}); 
</script>