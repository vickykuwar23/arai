<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
textarea {
        resize: none; padding:10px 0;
    }
	#connectExpertModal .modal-dialog {
    top: 10%;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header" style="text-align: center;display: block;">
		<img src="<?php echo base_url('assets/front/img/logo1.png'); ?>" alt="logo" style="width: 40%;">
		<h5 style="display: block;clear: both;width: 100%;background: #e23751;color: #FFF; margin-top:10px; font-size:16px; border-radius: 5px;" class="modal-title" id="SubscriptionModalLabel">Type Your Message Below</h5>
		<button style="margin-top: -100px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="expertEmailForm" name="expertEmailForm" role="form">
		
		<div class="pl-3 pr-3 pb-3">
			<div class="form-group">
				<textarea rows="2" cols="12" name="expert_subject" id="expert_subject" class="form-control valid-new"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Subject <em>*</em></label>
			</div>			
			<div class="form-group">
				<textarea rows="8" cols="12" name="expert_msg" id="expert_msg" class="form-control valid-new"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Message <em>*</em></label>
			</div>
		
		<div class="modal-footer" style="border-top:none; padding:0;">
			<div class="text-center" ><button type="submit" class="btn btn-primary" >Send</button></div>
		</div>
		<input type="hidden" name="expert_id" id="expert_id" value="<?php echo $expert_id ?>">
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>


<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<script>

	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	

	
	
	$(document).ready(function () {

		jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);			
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));	
	
		$("#expertEmailForm" ).validate({			
			/*debug: true,
			 onkeyup: true, */
			load: true,
			blur: true,
			change: true,
			keypress: true,
			keyup: true,
			keydown: true,
			onclick: true,
			rules: {
				expert_subject: {
					required: true,
					minCount:['1'],
					maxCount:['50']	
				},
				expert_msg: {
					required: true,
					minCount:['1'],
					maxCount:['100']	
				},
				expert_remark: {	
					maxCount:['100']	
				}
			},
			messages: {
				expert_subject: {
					required: "This field is required"
				},
				expert_msg: {
					required: "This field is required"
				},
				expert_remark: {
					maxCount: "Maximum 100 words are allowed."
				}
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				
				    swal({
              title:'Confirm' ,
              text: 'Are you sure you want to send email to this expert?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
              }).then(function (result) {
              if (result.value) {
              	$("#preloader-loader").css("display", "block");
								$.ajax({
									url: "<?php echo base_url('home/submit_expert_email_popup'); ?>", 
									type: "POST",             
									data:$('#expertEmailForm').serialize(),
									dataType: 'JSON',
									cache: false,
									success: function(response) 
									{
										$("#connectExpertModal").modal('hide');
										$("#preloader-loader").css("display", "none");
										swal({
											title: response.flag,
											text: response.message,
											icon: response.flag,
											}).then(function() {
											//location.reload();
										});
									}
									});
            }

          }); 
				

				return false;
			}
		});
	
	}); 
</script>