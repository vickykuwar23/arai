<style>
.agreement_content { max-width: 800px; margin: 0 auto; text-align: left; }
.agreement_content .agreement_content_inner { width: 100%; border: 1px solid #d3d3d3; background: #f9f9f9; border-radius: 5px; padding: 20px; }
.agreement_content .agreement_content_inner h4 { margin: 0 0 8px 0; text-align: center; color: #000; }
.agreement_content .agreement_content_inner ul { margin: 0; padding: 0 0 0 19px; text-align: justify; }
.agreement_content .agreement_content_inner ul li:before { display:none; } 
.agreement_content .agreement_content_inner ul li { list-style: disc; padding: 0; margin-bottom: 10px; }
.agreement_content .agreement_content_inner ul li p { float: left; width: calc(100% - 190px); font-size: 14px; margin:5px 0 0 0; } 
.agreement_content .agreement_content_inner ul li .whats_app_img { float: right; max-width: 180px; } </style>
  
<?php $en_obj =  New Opensslencryptdecrypt(); ?>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php if($challenge_data[0]['temp_participate_link'] == ""){ echo "Coming Soon"; } else { echo "Participate"; } ?></h1>
		<!-- <nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
			<li class="breadcrumb-item"><a href="#">Registration</a></li>
			<li class="breadcrumb-item active" aria-current="page">Registration </li>
			</ol>
		</nav> -->
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<div class="formInfo">
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php if($challenge_data[0]['temp_participate_link'] == "") 
								{ ?>
								<h3>This facility is coming soon - Stay Tuned</h3>
								<img src="https://technovuus.araiindia.com/assets/front/img/coming-soon.png" alt="Coming Soon..." style="max-width:500px;">
								<?php }
								else
								{	?>	
								<h3><?php echo $en_obj->decrypt($challenge_data[0]['challenge_title']); ?></h3>
								
								<div class="agreement_content">
                  <div class="agreement_content_inner">
                    <h4>Terms of Reference-Participation in student challenge</h4>
                    <ul>
                      <li>A team shall consist of a Team Leader, at least 1 faculty guide, & not more than 5 team members (including team leader). Individual students can also take part in the hackathon, though a faculty guide is necessary.</li>
                      <li>It is mandatory that the team leader and all his/her team mates and faculty guide(s) be registered at Technovuus as users.</li>
                      <li>The Institutions from which students and faculty are participating also need to be registered as academia at Technovuus to enable them to participate in Technovuus Hackathon.</li>
                      <li>The team leader alone has to register the team with details of all team members &faculty advisor by clicking on 'participate' under the challenge page.</li>
                      <li>In case of inter college team, the faculty guide should be from the same college of the team leader.</li>
                      <li>The adjudication of teams for the Final Evaluation will be prerogative of the expert team at Technovuus. No queries shall be entertained.</li>
                      <li>The adjudication of the winner of problem statement in the Final Evaluation will be done by the Technovuus Jury. No queries/requests shall be entertained.</li>
                      <li>The resources required (laptop, hardware, software, sensors etc.)for solving the problem statements shall be either the college resources or the student's own resources. Technovuus shall not provide any resources to any participating teams unless otherwise specifically mentioned.</li>
                    </ul><br>
                    
                    <h4>Timeline</h4>
                    <ul>
                      <li>Participation and submission of problem statement abstract by team leader should be on or before 23rd November 2020, 17:00hrs</li>
                      <li>The Team leader should submit a solution abstract (400-word limit) detailing the how they intend to solve the challenge giving specific technical methodology</li>
                      <li>A preliminary shortlisting will take place based on the abstract submitted.</li>
                      <li>Shortlisted teams will be notified and will have to submit a detailed solution description by 31st December 2020</li>
                      <li>The solution ideas that are submitted shall be evaluated by our team of experts at Technovuus.</li>
                      <li>The teams that are selected for the final evaluation shall be intimated via Technovuus website.</li>
                      <li>
                        The final evaluation will be held virtually within 2 months after the notification of short listed teams.
                        <div class="clearfix"></div>
                        <p>
                          Any queries may be addressed to info@technovuus.araiindia.com, or to our whatsapp helpdesk by clicking on the link or scanning the QR Code. <br>
                          <a href="https://wa.me/message/BWX6SYO2JAXDH1">https://wa.me/message/BWX6SYO2JAXDH1</a>
                        </p>
                        <img class="whats_app_img" src="<?php echo base_url(); ?>assets/whats_app_img.png">
                        <div class="clearfix"></div>
                      </li>
                    </ul>
                  </div>
									<!--img src="<?php echo base_url(); ?>assets/sample_logo.png" style="width: 100%;border: 5px solid #eaeaea; padding: 2px; margin: 10px auto;"-->
									
									<form method="post" action="<?php echo site_url('home/participate/'.base64_encode(($challenge_data[0]['c_id']))); ?>" id="participate_form" enctype="multipart/form-data">
										<div class="col-auto my-1" style="margin: 15px 0 !important;padding-left:0;">
											<div class="custom-control custom-checkbox mr-sm-2">
												<input type="checkbox" class="custom-control-input" id="i_agree" name="i_agree" required value="1">
												<label class="custom-control-label" for="i_agree">I Agree, the Terms of Reference</label>
											</div>
											<div id="i_agree_err"></div>
										</div>
										
										<button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Confirm Participation</button>
									</form>
								</div>
							<?php } ?>
						</div>
					</div>
					
					<div class="row mt-12">
						<div class="col-md-12">
							
						</div>
					</div>    
				</div>    
			</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>
<script>
	/******* JQUERY VALIDATION *********/
		$("#participate_form").validate( 
		{
			rules:
			{
				i_agree: { required: true }				
			},
			messages:
			{
				i_agree: { required: "Please accept the Terms of Reference" },				
			},
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "i_agree") 
				{
					error.insertAfter("#i_agree_err");
				}
				else 
				{
					error.insertAfter(element);
				}
			}
		});		
	</script>
