<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.byt_member_blocks { border: 1px solid rgba(0,0,0,0.09); overflow: hidden; background: rgba(0,0,0,0.03); padding: 15px 15px; min-height:320px; }
	.byt_member_blocks .byt_member_img { height: 100px; width:100px; border-radius:50%; margin: 0px auto 15px; display: block; border: 1px solid #ccc; overflow:hidden; }
	.byt_member_blocks img { max-width: 100%; max-height: 100px; }
	.byt_member_blocks .byt_member_name { text-align: center; margin: 0 0 8px 0; min-height: 24px; }
	
	.byt_member_blocks .byt_member_skill_set { /* border: 1px solid #ccc !important; min-height:150px; background:#fff; */ margin:0; }  
	.byt_member_blocks .byt_member_roles { /* border: 1px solid #ccc !important;  min-height:100px; background:#fff; */ margin-top:20px; }  
	.byt_member_blocks .select2 { max-width:100%; width:100% !important; }
	/* .byt_member_blocks .select2-container--disabled .select2-selection--multiple { background-color: #fff; } */
	
	.byt_member_blocks .select2-container--default .select2-selection--multiple { /* border: none !important; */ font-size:14px; margin:0; padding:5px; }
</style>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Apply For Challenge </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Apply For Challenge </li>
			</ol>
		</nav>
	</div>
</div> 


<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					<?php //echo validation_errors(); ?>
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php }  ?>
					<?php if( $this->session->flashdata('error')){ ?>
						<div class="alert alert-error alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php }  ?> 
					
					
					<form method="POST" action="<?php echo site_url('applyChallenge/index/'.base64_encode($challenge_id)); ?>" id="ApplyChallengeForm" name="ApplyChallengeForm" enctype="multipart/form-data">
						<div id="smartwizard_apply_challenge">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control" name="team_name" id="team_name" value="<?php echo set_value('team_name'); ?>" required autofocus maxlength="250">
												<label class="form-control-placeholder" for="team_name">Name of the Team <em>*</em></label>
												<span><?php echo form_error('team_name'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Banner</button>
												<input type="file" class="form-control" name="team_banner" id="team_banner" />
												<div class="clearfix"></div>
												<span class="small">Note : Please upload only image having size less than 2MB</span><div class="clearfix"></div>
												<?php if(form_error('team_banner')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('team_banner'); ?></span> <?php } ?>
												<?php /* if($org_logo_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $org_logo_error; ?></span> <?php } */ ?>
											</div>
										</div>
										
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="team_size" id="team_size" value="<?php echo set_value('team_size'); ?>">
												<label class="form-control-placeholder" for="team_size">Team Size <em>*</em></label>
												<span><?php echo form_error('team_size'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12">
											<div class="form-group">
												<textarea class="form-control" name="team_details" id="team_details"><?php echo set_value('team_details'); ?></textarea>
												<label class="form-control-placeholder" for="team_details">Brief About the Team <em>*</em></label>
												<span><?php echo form_error('team_details'); ?></span>
											</div>
										</div>										
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<input type="hidden" name="byt_member_block_flag" id="byt_member_block_flag" value="<?php if(set_value('byt_member_block_flag') != "") { echo set_value('byt_member_block_flag'); } else { echo "0"; } ?>">
									<input type="hidden" name="byt_member_total_block" id="byt_member_total_block" value="<?php if(set_value('byt_member_total_block') != "") { echo set_value('byt_member_total_block'); } else { echo "1"; } ?>">
									<div class="row">					 
										<div class="col-md-3" id="byt_member_blocks_outer1">
											<div class="form-group">
												<div class="byt_member_blocks">
													<div class="byt_member_img">
														<img src="<?php echo $disp_photo; ?>" class="img-fluid bor">
													</div>
													<div class="byt_member_name">
														<?php echo $encrypt_obj->decrypt($login_user_data['0']['title'])." ".$encrypt_obj->decrypt($login_user_data['0']['first_name'])." ".$encrypt_obj->decrypt($login_user_data['0']['last_name']); ?>
													</div>
													<div class="byt_member_skill_set">
														<select class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset0][]" id="team_skillset0" data-placeholder="Select Skill Sets *" multiple readonly disabled><!---->
															<option></option>
															<?php if(count($skill_sets_data) > 0)
																{	
																	foreach($skill_sets_data as $skill_sets)
																	{	
																		if(set_value('team_skillset[team_skillset1][]') != "") { $team_skillset_arr = set_value('team_skillset[team_skillset1][]'); }
																	else { $team_skillset_arr = $disp_skill_sets; } ?>
																	<option value="<?php echo $skill_sets['id']; ?>" <?php if(in_array($skill_sets['id'],$team_skillset_arr)) { echo 'selected'; } ?> ><?php echo $skill_sets['name']; ?></option>
																	<?php }	
																}	?>
														</select>
														<input type="hidden" class="form-control" name="team_skillset[team_skillset1][]" id="team_skillset1" value="<?php echo implode(",",$team_skillset_arr); ?>">
													</div>
													
													<div class="byt_member_roles">
														<select class="form-control select2_common cls_team_role" name="team_role[team_role1][]" id="team_role1" data-placeholder="Select Role in the Team *" multiple>
															<option></option>
															<?php if(count($role_data) > 0)
																{	
																	foreach($role_data as $role)
																	{	
																		if(set_value('team_role[team_role1][]') != "") { $team_role_arr = set_value('team_role[team_role1][]'); }
																	else { $team_role_arr = array(); } ?>
																	<option value="<?php echo $role['id']; ?>" <?php if(in_array($role['id'],$team_role_arr)) { echo 'selected'; } ?>><?php echo ($role['name']); ?></option>
																	<?php }	
																}	?>
														</select>
													</div>
												</div>
											</div>							
										</div>
										
										<?php if(set_value('byt_member_total_block') != "") 
											{	
												for($i=2; $i <= set_value('byt_member_total_block'); $i++)
												{	?>										
												<div class="col-md-3" id="byt_member_blocks_outer<?php echo $i; ?>">
													<div class="form-group">
														<div class="byt_member_blocks">
															<div class="byt_member_img"><img src="<?php echo $default_disp_photo; ?>"></div>
															<div class="byt_member_name"></div>
															<div class="byt_member_skill_set">
																<select class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset<?php echo $i; ?>][]" id="team_skillset<?php echo $i; ?>" data-placeholder="Select Skill Sets *" multiple>
																	<option></option>
																	<?php if(count($skill_sets_data) > 0)
																		{	
																			foreach($skill_sets_data as $skill_sets)
																			{	
																				if(set_value('team_skillset[team_skillset'.$i.'][]') != "") { $team_skillset_arr = set_value('team_skillset[team_skillset'.$i.'][]'); }
																				else { $team_skillset_arr = array(); }
																			?>
																			<option value="<?php echo $skill_sets['id']; ?>" <?php if(in_array($skill_sets['id'],$team_skillset_arr)) { echo 'selected'; } ?> ><?php echo $skill_sets['name']; ?></option>
																			<?php }	
																		}	?>
																</select>
															</div>
															
															<div class="byt_member_roles">
																<select class="form-control select2_common cls_team_role" name="team_role[team_role<?php echo $i; ?>][]" id="team_role<?php echo $i; ?>" data-placeholder="Select Role in the Team *" multiple>
																	<option></option>
																	<?php if(count($role_data) > 0)
																		{	
																			foreach($role_data as $role)
																			{	
																				if(set_value('team_role[team_role'.$i.'][]') != "") { $team_role_arr = set_value('team_role[team_role'.$i.'][]'); }
																			else { $team_role_arr = array(); } ?>
																			<option value="<?php echo $role['id']; ?>" <?php if(in_array($role['id'],$team_role_arr)) { echo 'selected'; } ?>><?php echo ($role['name']); ?></option>
																			<?php }		
																		}	?>
																</select>
															</div>
														</div>
													</div>							
												</div>
												<?php }
											}	?>
											
											<div id="byt_member_blocks_last"></div>
									</div>							
								</div>							
								
								<div id="step-3" class="mt-4">
									<div class="row">					 
										<div class="col-md-12">
											<div class="form-group">
												<textarea type="text" class="form-control" name="proposed_approach" id="proposed_approach"><?php echo set_value('proposed_approach'); ?></textarea>
												<label class="form-control-placeholder" for="proposed_approach">Proposed Approach for solving challenge <em></em></label>
												<span><?php echo form_error('proposed_approach'); ?></span>
											</div>											
										</div>
									</div>
									
									<input type="hidden" name="files_row_cnt" id="files_row_cnt" value="1">
									<div class="row" id="byt_file_row_0">
										<div class="col-md-12">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Browse</button>
												<input type="file" class="form-control" name="team_files[]" id="team_files1" />
												<div class="clearfix"></div>
												<!--span class="small">Note : Please upload only image having size less than 2MB</span><div class="clearfix"></div-->
												<?php if(form_error('team_files')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('team_files'); ?></span> <?php } ?>
												<?php /* if($org_logo_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $org_logo_error; ?></span> <?php } */ ?>
											</div>
										</div>
									</div>
									
									<div id="byt_team_files_last"></div>
									<button type="button" class="btn btn-primary btn-sm" onclick="append_byt_files_row()"><i class="fa fa-plus"></i> Add More</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function scroll_to_top(div_id='')
	{
		if(div_id == '') { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
	
	function append_team_member_blocks()
	{
		var team_size = $("#team_size").val();
		var byt_member_block_flag = $("#byt_member_block_flag").val();
		var byt_member_total_block = $("#byt_member_total_block").val();
		
		var append_flag = 0;
		var delete_flag = 0;
		var final_team_size = 1;
		var start = 2;
		
		if(team_size != "" && team_size > 0)
		{
			if(byt_member_block_flag == 0)//APPEND MEMBER 
			{
				final_team_size = team_size;
				append_flag = 1;
			}
			else
			{
				if(byt_member_total_block == team_size)
				{
					final_team_size = team_size;
				}
				else if(byt_member_total_block < team_size)
				{
					start = parseInt(byt_member_total_block) + 1;
					final_team_size = team_size;
					append_flag = 1;
				}
				else if(byt_member_total_block > team_size)
				{
					delete_flag = 1;
					start = parseInt(team_size) + 1;
					final_team_size = team_size;
				}
			}
			
			/* alert("append flag : "+append_flag);
				alert("delete flag : "+delete_flag);
				alert("start : "+start);
			alert("final_team_size : "+final_team_size); */
			
			if(append_flag == 1)
			{
				var appent_str = '';
				for(var i=start; i <= final_team_size; i++)
				{				
					appent_str += '<div class="col-md-3" id="byt_member_blocks_outer'+i+'">';
					appent_str += '	<div class="form-group">';
					appent_str += '		<div class="byt_member_blocks">';
					appent_str += '			<div class="byt_member_img"><img src="<?php echo $default_disp_photo; ?>"></div>';
					appent_str += '			<div class="byt_member_name"></div>';
					appent_str += '			<div class="byt_member_skill_set">';
					appent_str += '				<select class="form-control select2_common cls_team_skillset" name="team_skillset[team_skillset'+i+'][]" id="team_skillset'+i+'" data-placeholder="Select Skill Sets *" multiple>';
					appent_str += '					<option></option>';
					
					<?php if(count($skill_sets_data) > 0)
						{	
							foreach($skill_sets_data as $skill_sets)
							{	?>
							appent_str += '<option value="<?php echo $skill_sets["id"]; ?>"><?php echo ($skill_sets["name"]); ?></option>';
							<?php }	
						}	?>
						appent_str += '				</select>';
						appent_str += '			</div>';
						
						appent_str += '			<div class="byt_member_roles">';
						appent_str += '				<select class="form-control select2_common cls_team_role" name="team_role[team_role'+i+'][]" id="team_role'+i+'" data-placeholder="Select Role in the Team *" multiple>';
						appent_str += '					<option></option>';
						<?php if(count($role_data) > 0)
							{	
								foreach($role_data as $role)
								{	?>
								appent_str += '<option value="<?php echo $role["id"]; ?>"><?php echo ($role["name"]); ?></option>';
								<?php }	
							}	?>
							appent_str += '				</select>';
							appent_str += '			</div>';
							appent_str += '		</div>';
							appent_str += '	</div>';
							appent_str += '</div>';
				}
				
				$(appent_str).insertBefore("#byt_member_blocks_last");
				$('.select2_common').select2();
				
				$("#byt_member_block_flag").val('1');
				$("#byt_member_total_block").val(team_size);
			}
			
			if(delete_flag == 1)
			{
				for(var i=start; i <= byt_member_total_block; i++)
				{			
					$("#byt_member_blocks_outer"+i).remove();
				}
				
				$('.select2_common').select2();
				$("#byt_member_total_block").val(team_size);
			}
		}
	}
	
	function append_byt_files_row()
	{
		var total_div_cnt = $('input[name*="team_files[]"]').length;
		if(total_div_cnt >= 15)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#files_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_file_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-10">';
			append_html += '			<div class="form-group upload-btn-wrapper">';
			append_html += '				<button class="btn btn-upload"><i class="fa fa-plus"> </i> Browse</button>';
			append_html += '				<input type="file" class="form-control" name="team_files[]" id="team_files'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#files_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_team_files_last");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
			});
		}
	}
	
	function remove_current_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_file_row_"+div_no).remove();
			}
		});	
	}
	
	$(document).ready(function() 
	{
		//******* STEP SHOW EVENT *********
		$("#smartwizard_apply_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') { $(".sw-btn-prev").hide(); $(".sw-btn-next").show(); $(".btnfinish").hide(); } 
			else if (stepPosition === 'final') { $(".sw-btn-prev").show(); $(".sw-btn-next").hide(); $(".btnfinish").show(); } 
			else { $(".sw-btn-prev").show(); $(".sw-btn-next").show(); $(".btnfinish").hide(); }
		});
		
		//******* STEP WIZARD *********
		$('#smartwizard_apply_challenge').smartWizard(
		{
			/* selected: 2, */
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */		
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
				$('<button></button>').text('Form A Team').addClass('btn btn-primary btnfinish').on('click', function(e)
				{ 
					e.preventDefault();						
					var submit_flag = 0;
					
					if($("#proposed_approach").valid()==false) { submit_flag = 1; $("#proposed_approach").focus(); }
					
					if($("#proposed_approach").valid()==false) { scroll_to_top('proposed_approach'); }						
					
					if(submit_flag == 0)
					{
						swal(
						{
							title:"Confirm",
							text: "Are you sure you want to build your team?",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Yes!'
						}).then(function (result) { if (result.value) { $('#ApplyChallengeForm').submit(); } });
					}
				}),
				]
			}
		});
		
		$("#smartwizard_apply_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{				
				if($("#team_details").valid()==false) { isValidate= false; $("#team_details").focus(); }
				if($("#team_size").valid()==false) { isValidate= false; $("#team_size").focus(); }
				if($("#team_banner").valid()==false) { isValidate= false; $("#team_banner").focus(); }
				if($("#team_name").valid()==false) { isValidate= false; $("#team_name").focus(); }
				
				if($("#team_name").valid()==false) { scroll_to_top('team_name'); }
				else if($("#team_banner").valid()==false) { scroll_to_top('team_banner'); }
				else if($("#team_size").valid()==false) { scroll_to_top('team_size'); }
				else if($("#team_details").valid()==false) { scroll_to_top('team_details'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				
				if(isValidate == true) { append_team_member_blocks(); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				var form = $( "#ApplyChallengeForm" );
				if(form.valid()==false) { isValidate= false; }
				
				if(form.valid()==false) { scroll_to_top('smartwizard_apply_challenge'); }			
				else { scroll_to_top('smartwizard_apply_challenge'); }
			}
			
			/* if(stepNumber==2 && stepDirection=="forward")
				{				
				if($("#from_age").valid()==false) { isValidate= false; $("#from_age").focus(); }
				if($("#educational").valid()==false) { isValidate= false; $("#educational").focus(); }
				
				if($("#educational").valid()==false) { scroll_to_top('educational'); }
				else if($("#from_age").valid()==false) { scroll_to_top('from_age'); }
				else { scroll_to_top('smartwizard_apply_challenge'); }
				}		
			*/
			if(stepDirection=="backward") { scroll_to_top('smartwizard_apply_challenge') }
			return isValidate;
		})
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	$('.select2_common').select2();
	
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	$(document).ready(function () 
	{
		$("input.numbers").keypress(function(event) { return /\d/.test(String.fromCharCode(event.keyCode)); });
		
		$.validator.addMethod("skillset_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Skill Sets');		
		$.validator.addClassRules("cls_team_skillset", { skillset_required: true });
		
		$.validator.addMethod("team_role_required", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please select the Team Role');		
		$.validator.addClassRules("cls_team_role", { team_role_required: true });
		
		//******* JQUERY VALIDATION *********
		$("#ApplyChallengeForm").validate( 
		{
			onkeyup: false,
			rules:
			{
				team_name: { required: true, nowhitespace: true },
				team_banner: { valid_img_format: true, maxsize: 2000000 },
				team_size: { required: true, nowhitespace: true, digits:true, min: parseInt("<?php echo $min_team; ?>"), max: parseInt("<?php echo $max_team; ?>") },
				team_details: { required: true, nowhitespace: true },
			},
			messages:
			{
				team_name: { required: "Please enter the Name of the Team", nowhitespace: "Please enter the Name of the Team" },
				team_banner: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				team_size: { required: "Please enter the Team Size", nowhitespace: "Please enter the Team Size", digits: "Please enter only numbers in Team Size", min:"Please enter a value greater than or equal to <?php echo $min_team; ?>", max:"Please enter a value less than or equal to <?php echo $max_team; ?>" },
				team_details: { required: "Please enter the Brief About the Team", nowhitespace: "Please enter the Brief About the Team" },
			},
			errorElement: 'span',
			errorPlacement: function (error, element) 
			{
				if(element.hasClass('select2_common') && element.next('.select2-container').length) 
				{
					error.insertAfter(element.parent());
				}
				else { element.closest('.form-group').append(error);  }
			},
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); }
		});	
		
		$('input[type="file"]').change(function(e)
		{              
			var fileName = e.target.files[0].name;
			$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
		});          
	});
</script>					