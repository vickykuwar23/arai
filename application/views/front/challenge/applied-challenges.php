
<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- jQuery -->
	<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

	
	
	<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Applied Challenges Listing</h1>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav> -->
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               
                 <div class="formInfo">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
							
                            <select id="technology" name="technology" class="form-control" required="">
								<option value=""></option>
								<?php 
								foreach($technology_data as $techname){
								?>
                                <option value="<?php echo $techname['id'] ?>"><?php echo $techname['technology_name'] ?></option>
                                <?php } ?>
                            </select>
                            <label class="form-control-placeholder" for="technology">Technology Field
                                <em>*</em></label>
                        </div> 
                     </div>
					<div class="col-md-4">
						<div class="form-group">
						<select id="change_visibility" name="change_visibility"  class="form-control" required="">                           
							<option value=""></option>
							<?php 
								foreach($challengetype as $visibility){
							?>
							<option value="<?php echo $visibility['id'] ?>"><?php echo $visibility['challenge_type'] ?></option>
							<?php } ?>
						</select>
						<label class="form-control-placeholder" for="technology">Challenge Visibility<em>*</em></label>
						</div> 
					</div>                 
					<div class="col-md-4">
						<div class="form-group">
						<select id="ip_clause" name="ip_clause" class="form-control" required="">
							<option value=""></option>
							<?php 
							foreach($ip_clause_data as $ip_data){
							?>
							<option value="<?php echo $ip_data['id'] ?>"><?php echo $ip_data['ip_name'] ?></option>
							<?php } ?>
						  
						</select>
						<label class="form-control-placeholder" for="technology">IP Clause <em>*</em></label>
						</div> 
					</div>
					<div class="col-md-4">
						<button type="button" id="btnReset" class="btn btn-secondary">Clear All </button>
						<button type="submit" class="btn btn-primary">Apply</button>
					</div>

                 </div>
                 <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="table-responsive">
                        <table id="challenge-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">ID</th>
                                <th scope="col">Title</th>
                                <th scope="col">Company Name</th>
                                <th scope="col">Close Date</th>
                                <th scope="col">Challenge Status</th>
                                <th scope="col">Challenge Visibility</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
							<?php
							if(count($challenge_data) > 0){	
							$i=1;
							foreach($challenge_data as $cdata){
							?>
                              <tr>
									<td><?php echo $i ?></td>
									<td><?php echo $cdata['challenge_title'] ?></td>
									<td><?php echo $cdata['company_name'] ?></td>
									<td><?php echo date('d-m-Y', strtotime($cdata['challenge_close_date'])) ?></td>
									<td><?php echo $cdata['challenge_status'] ?></td>
									<td><?php echo $cdata['challenge_visibility'] ?></td>
									<td><a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($cdata['c_id'])); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a> <a href="<?php echo base_url('challenge/edit/'.base64_encode($cdata['c_id'])); ?>"><i class="fa fa-edit" aria-hidden="true"></i><!--<i class="fa fa-times-circle" aria-hidden="true"></i>--></a></td>
                              </tr>
                              <?php $i++;
									}
								}
							  ?>
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
	<script>
  $(document).ready( function () {
	  
		$('.datepicker').datepicker();
		//var table = $('#challenge-list').DataTable({}); 
		 $("#challenge-list").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$("#btnReset").on("click", function () {
				alert();
			/*$("#techonogy_id").val([]).change();
			$("#audience_pref").val([]).change();
			$("#c_status").val([]).change();
			$("#change_visibility").val([]).change();	
			table.ajax.reload();*/	
			window.location.reload();
		});
		
	
	});
	$('.select2').select2({
	});
</script>