
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Applied Challenges</h1>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav> -->
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">
                    <div class="row">
                    <div class="col-md-4">                            
						<div class="form-group">
						
							<select class="form-control select2 " id="techonogy_id" name="techonogy_id[]" multiple="multiple" required>
							  <option value=""></option>
								<?php foreach($technology_data as $techname){ ?>
								<option value="<?php echo $techname['id'] ?>" ><?php echo $techname['technology_name'] ?></option>								  
								<?php  } ?>								
							</select>	
							<label class="form-control-placeholder leftLable2" for="techonogy_id">Technology Field </label>						
						</div>
                     </div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Challenge Visibility</label>
							<select id="change_visibility" name="change_visibility"  class="form-control select2">                           
									<option value=""></option>
								<?php 
									foreach($challengetype as $visibility){
								?>
									<option value="<?php echo $visibility['challenge_type'] ?>"><?php echo $visibility['challenge_type'] ?></option>
								<?php } ?>
							</select>						
						</div> 
					</div>                 
					
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Funding Available</label>
							<select id="fund_sel" name="fund_sel" class="form-control select2" required="">
								<option value=""></option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Reward Available</label>
							<select id="reward_sel" name="reward_sel" class="form-control select2" required="">
								<option value=""></option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">                            
						<div class="form-group">
							<label class="form-control-placeholder">TRL </label>
							<select class="form-control select2" id="trl_id" name="trl_id">
							  <option value=""></option>
								<?php foreach($trl_data as $trl){ ?>
								<option value="<?php echo $trl['id'] ?>" ><?php echo $trl['trl_name'] ?></option>								  
								<?php  } ?>								
							</select>							
						</div>
                     </div>
					 <div class="col-md-4">

					 
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">IP Clause </label>
							<select id="ip_clause" name="ip_clause" class="form-control select2" required="">
								<option value=""></option>
								<?php 
								foreach($ip_clause_data as $ip_data){
								?>
								<option value="<?php echo $ip_data['id'] ?>"><?php echo $ip_data['ip_name'] ?></option>
								<?php } ?>
							  
							</select>						
						</div> 
					</div>
					<div class="col-md-4">
						<button type="submit" id="btnReset" class="btn btn-secondary">Clear All </button>
						<button type="submit" id="btn-click" class="btn btn-primary btn-click">Apply</button>
					</div>

                 </div>
                 <div class="row mt-12 mt-5">
                    <div class="col-md-12">
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
						 <?php echo $this->session->flashdata('success'); ?>
						</div>
					   <?php } $userId = $this->session->userdata('user_id'); ?>
                        <div class="table-responsive">
                        <table id="apply-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">ID</th>
								<th scope="col">Title</th>
                                <th scope="col">Company Name</th>
                                <th scope="col">Close Date</th>
                                <th scope="col">Challenge Status</th>
                                <th scope="col">My Application Status</th>
								<th scope="col">Team Name</th>
                                <th scope="col">Options</th>
                              </tr>
                            </thead>
                            <tbody>							
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
	
<script>
  $(document).ready( function () {	  
	
		var base_path = '<?php echo base_url() ?>';
		var table = $('#apply-list').DataTable({
			
			"ordering":false,
			"searching": false,
			"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"challenge/applied_clist",
			"type":"POST",
			"data":function(data) {	
					data.user_id				= '<?php echo $userId; ?>';
					data.techonogy_id 			= $("#techonogy_id").val();
					data.change_visibility 		= $("#change_visibility").val();
					data.ip_clause 				= $("#ip_clause").val();
					data.fund_sel 				= $("#fund_sel").val();
					data.reward_sel 			= $("#reward_sel").val();
					data.trl_id 				= $("#trl_id").val();
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
		});

		$('.btn-click').click(function(){
			var user_id				= '<?php echo $userId; ?>';
			var techonogy_id 		= $('#techonogy_id').val();
			var change_visibility 	= $('#change_visibility').val();	
			var ip_clause 			= $('#ip_clause').val();
			var fund_sel 			= $('#fund_sel').val();
			var reward_sel 			= $('#reward_sel').val();
			table.ajax.reload();		
		});
		
		$("#btnReset").on("click", function () {			
			$("#techonogy_id").val([]).change();
			//$("#audience_pref").val([]).change();
			//$("#c_status").val([]).change();
			$("#ip_clause").val([]).change();
			$("#fund_sel").val([]).change();
			$("#reward_sel").val([]).change();
			$("#trl_id").val([]).change();
			$("#change_visibility").val([]).change();					
			table.ajax.reload();	
		});
	
	$('.select2').select2({});

});
$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>