<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
    cursor: pointer;}
</style>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Close Challenge</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Close Challenge </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="closeChallenge" name="closeChallenge" enctype="multipart/form-data">			
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="closure_title" id="closure_title" value=""  >
									<label class="form-control-placeholder floatinglabel">Title <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type1" name="upload_type" class="custom-control-input get-type" value="Image" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type1">Image</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type2" name="upload_type" class="custom-control-input get-type" value="Video" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type2">Video</label>
								</div>
								<div id="upload_type_err"></div>
							</div>							
							
							<div class="col-md-12" id="uploadImg">	
								<div class="file-details" style="min-height:100px;">
									<input type="hidden" name="customFileCount" id="customFileCount" value="0">
									<div class="form-group" id="row_files_0">
										<div class="row">										
											<div class="col-md-12">
												<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload0" />
											</div>
										</div>
									</div>
										
									<div id="last_team_file_id"></div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="custom-file-upload mt-3 btn btn-primary" onclick="append_image_files_row()"><i class="fa fa-plus-circle"></i> Add File</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" id="uploadVideo">
								<input type="hidden" name="invite_row_cnt" id="invite_row_cnt" value="1">
								<div class="row" id="byt_invite_row_0">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control youtube_input video_link" name="youtube_link[]" id="youtube_link1" value="">
											<label class="form-control-placeholder floatinglabel" for="youtube_link">Video </label>
											<span class="error"><?php echo form_error('youtube_link'); ?></span>
										</div>
									</div>
								</div>									
								<div id="byt_invite_last"></div>									
								<button type="button" class="btn btn-primary btn-sm" onclick="append_invite_row()"><i class="fa fa-plus"></i> Add More</button>
							</div>
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12">
										<div class="form-group">
											<textarea type="text" class="form-control" name="closure_desc" id="closure_desc"></textarea>
											<label class="form-control-placeholder floatinglabel" for="closure_desc">Brief Information <em>*</em></label>
											<span class="error"><?php echo form_error('closure_desc'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							
							<div class="col-md-12">	
								<div class="file-details" style="min-height:100px;">
									<input type="hidden" name="docFileCount" id="docFileCount" value="0">
									<div class="form-group" id="row_doc_0">
										<div class="row">										
											<div class="col-md-12">
												<input type="file" class="form-control doc_file doc_upload" name="doc_file[]" id="doc-upload0" />
												<label class="form-control-placeholder floatingfile" for="youtube_link">Document </label>
											</div>
										</div>
									</div>
										
									<div id="last_doc_file_id"></div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="custom-file-upload mt-3 btn btn-primary" onclick="append_doc_files_row()"><i class="fa fa-plus-circle"></i> Add File</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" ><br />
								<button type="submit" class="btn btn-primary add_button">Submit</button> 
							</div>	
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	
	//CURRENTLY NOT IN USE
	function append_invite_row()
	{
		var total_div_cnt = $('input[name*="youtube_link[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" youtube link", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#invite_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row" id="byt_invite_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-11">';
			append_html += '			<div class="form-group upload-btn-wrapper" style="margin-top:0">';			
			append_html += '				<input type="text" class="form-control youtube_input video_link" name="youtube_link[]" id="youtube_link'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group" style="margin-top:0">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#invite_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_invite_last");
		}
	}
	
	function remove_current_div_invite(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete this row?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_invite_row_"+div_no).remove();
			}
		});	
	}
	
	
	// DOCUMENT UPLOAD
	function append_doc_files_row()
	{
		var total_div_cnt = $('input[name*="doc_file[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#customFileCount").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="form-group" id="row_doc_'+row_cnt_new+'">';
			append_html += '		<div class="row">';
			append_html += '			<div class="col-md-11">';
			append_html += '				<input type="file" class="form-control doc_file doc_upload" name="doc_file[]" id="doc-upload'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '			<div class="col-md-1">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_doc_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
						
			$("#docFileCount").val(row_cnt_new);
			$(append_html).insertBefore("#last_doc_file_id");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	function remove_doc_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#row_doc_"+div_no).remove();
			}
		});	
	}
	/************************ Document Upload Close *********************************/
	
	function append_image_files_row()
	{
		var total_div_cnt = $('input[name*="closer_file[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#customFileCount").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="form-group" id="row_files_'+row_cnt_new+'">';
			append_html += '		<div class="row">';
			append_html += '			<div class="col-md-11">';
			append_html += '				<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '			<div class="col-md-1">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_files_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
						
			$("#customFileCount").val(row_cnt_new);
			$(append_html).insertBefore("#last_team_file_id");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	
	function remove_files_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#row_files_"+div_no).remove();
			}
		});	
	}
	
	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function TeamFilesPreview(input, public_private_flag) 
	{
		console.log(public_private_flag);
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			
			var disp_img_name = "";
			var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
			disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
			
			var id_name = '';
			var btn_id_name = '';
			var input_cls_name = '';
			var input_type_name = '';
			var fun_var_parameter = "";
			var file_upload_common = '';
			if(public_private_flag == 'public')
			{
				id_name = 'team_files_outer'+j;
				btn_id_name = 'addFileBtnOuter';
				input_cls_name = 'fileUploadTeams';
				input_type_name = 'closer_file[]';
				fun_var_parameter = "'public'";
				file_upload_common = 'file-upload';
			}			
			
			var append_str = '';
			append_str += '	<div class="col-md-2 team_files_outer_common" id="'+id_name+'">';
			append_str += '		<div class="file-list">';
			append_str += '			<a href="javascript:void(0)" onclick="remove_team_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
			//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
			append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
			append_str += '		</div>';
			append_str += '	</div>';				
			
			var btn_str = '';
			btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
			btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
			btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="TeamFilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
			btn_str +=	'	</div>';
			
			if(public_private_flag == 'public')
			{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
				
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
			}
			
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadTeams").change(function() { TeamFilesPreview(this,'public'); });	
	
	
	function show_hide_div()
	{
		$('#uploadImg').hide();
		$('#uploadVideo').hide();
		
		var values = $("input[name='upload_type']:checked").val();
		if(values == "Image")
		{
			$('#uploadImg').show();
			$('#uploadVideo').hide();
		}
		else if(values == "Video")
		{
			$('#uploadVideo').show();
			$('#uploadImg').hide();
		}
	}
	show_hide_div();
	
	$(document).ready(function() 
	{
		/* $('#uploadImg').hide();
		$('#uploadVideo').hide();
		
		$( ".get-type" ).change(function() 
		{	
			var values = $(this).val();
			if(values == "Image"){
				$('#uploadImg').show();
				$('#uploadVideo').hide();
				}else if(values == "Video"){
				$('#uploadVideo').show();
				$('#uploadImg').hide();
			}
		}); */
		
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		}, 'Please upload only .png, .jpeg, .jpg, .gif file format');
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) {
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		// Using Class Add Validation To array Image upload
		$.validator.addMethod("image_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'Please select the image');
		
		$.validator.addClassRules("image_file", { image_required: true, valid_img_format:true, filesize:2000000 });
		
		// Textbox Validation required 
		$.validator.addMethod("youtube_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'Please enter the youtube link');
		
		// Youtube URL Validation
		$.validator.addMethod("youtube_input_valid", function(value, element) 
		{
			var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Please enter valid youtube url.");	
		
		$.validator.addClassRules("youtube_input", { youtube_required: true, youtube_input_valid: true });
		
		
		// Doc Validation .png, jpeg, jpg etc.
		$.validator.addMethod("doc_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".pdf", ".xls", ".xlsx", ".doc", ".docx");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		}, 'Please upload only .pdf, .xls, .xlsx, .doc, .docx file format');
		
		$.validator.addClassRules("doc_upload", { doc_format:true, filesize:true });
		
		$('.add_button').click(function() 
		{ 
			$("#closeChallenge").validate();
			
			var values = $("input[name='upload_type']:checked").val();
			if(values == "Image")
			{
				if($(".image_file").valid()==false) { return false; } 
			}
			else if(values == "Video")
			{
				if($(".youtube_input").valid()==false) { return false; } 
			}
		});
		
		//******* JQUERY VALIDATION *********
		$("#closeChallenge").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			rules:
			{
				closure_title: { required: true, nowhitespace: true },
				upload_type:{required: true, nowhitespace: true },
				closure_desc:{required: true, nowhitespace: true },		
				//"youtube_link[]":{ youtube_required: true, youtube_input_valid: true }
				//team_banner: { valid_img_format: true, maxsize: 2000000 }
			},
			messages:
			{
				closure_title: { required: "This field is required", nowhitespace: "Please enter the title" },
				upload_type:{required: "This field is required", nowhitespace: "This field is required"},
				closure_desc:{required: "This field is required", nowhitespace: "This field is required"}
				//team_banner: { valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" }				
			},
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "upload_type") 
				{
					error.insertAfter("#upload_type_err");
				}
				else { element.closest('.form-group').append(error); }
			}
		});
	});
</script>