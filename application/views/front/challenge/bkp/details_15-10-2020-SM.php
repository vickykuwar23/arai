<?php 
error_reporting(0);
$datestr = $challenge_data[0]['challenge_close_date'];//Your date
$date	 = strtotime($datestr);//Converted to a PHP date (a second count)

//Calculate difference
$diff	= 	$date-time();//time returns current time in seconds
$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
//$hours	=	round(($diff-$days*60*60*24)/(60*60));
$style="";
if($days > 0){
	$style="display:none";
	$showCnt = $days. " Days left";
} else {
	$showCnt = "Closed Paticipations";
}


$fileExist = base_url('assets/challenge/'.$challenge_data[0]['company_logo']);
 if (@GetImageSize($fileExist)) {
	$image = $fileExist;
} else {
	$image = base_url('assets/not_available.png');
}
//echo "<pre>";print_r($challenge_data);die();

$bannerImg = base_url('assets/challenge/'.$challenge_data[0]['banner_img']);

/*$challenge_sender_profile 	= $this->master_model->getRecords("student_profile", array("user_id" => '6'));
//echo "<pre>";print_r($challenge_sender_profile);
print_r($challenge_sender_profile[0]['designation']);
echo json_decode($challenge_sender_profile[0]['designation'],true)."<br />";
echo $challenge_sender_profile[0]['domain_area_of_expertise_search'];*/
?>

<style>

.home-p.pages-head4 {
	background: url(<?php echo $bannerImg; ?>) no-repeat center top;
	background-size: 100%; min-height: 100%;
}
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}

table#show-hide{display:none;}


/********* Teams CSS *********/
.super-card {
    position: relative;
    border-radius: 3px;
    border: 1px solid #dee2e4;
	margin-top: 10px;
}

.super-card .thumbnail {
    height: 200px;
    position: relative;
    text-align: center;
    overflow: hidden;
    border: 0;
    background-color: #cacaca;
    border-radius: 3px 3px 0 0;
    z-index: 0;
    padding: 0;
}

.super-card .btn-player {
    display: none;
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 10;
    opacity: .7;
    margin: -30px 0 0 -30px;
}
.super-card .thumbnail .filter {
    background: -webkit-gradient(linear,left bottom,left top,from(rgba(0,0,0,.8)),to(hsla(0,0%,100%,0)));
    background: linear-gradient(0deg,rgba(0,0,0,.8),hsla(0,0%,100%,0));
    border-radius: 3px 3px 0 0;
    opacity: .8;
    z-index: 1;
}
.super-card .thumbnail .bg, .super-card .thumbnail .filter, .super-card .thumbnail .over-link, .super-card .thumbnail .title {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
}
.super-card .thumbnail .title {
    width: auto;
    height: auto;
    top: auto;
    bottom: 0;
    z-index: 1;
}
.m-l-md {
    margin-left: 20px;
}
.super-card .content-sm {
    padding: 10px;
}

.bg-primary, .live-chat .chat-footer .action, ul.list-vote li .slider .complete, ul.list-vote li .slider span {
    color: #fff;
}

.bg-primary, .live-chat .chat-footer .action, ul.list-vote li .slider span, ul.list-vote li .slider .complete {
    color: #fff;
}

.super-card .block-radius {
    border-radius: 3px;
    overflow: hidden;
}

.super-card .content-sm {
    padding: 10px;
}

.super-card .block-radius {
    border-radius: 3px;
    overflow: hidden;
}
.bg-light, .live-chat .chat-body li.typing-users .typing-animation .circle {
    background-color: #f5f5f5;
}

.super-card .content {
    padding: 20px;
}

.bg-white, .live-chat, .menu-static, .navbar-challenges .nav-challenges, .popover.white, .preview-doc .dashboard-container .header, .white.collapse-popover {
    background-color: #fff;
}

.bg-white, .navbar-challenges .nav-challenges, .popover.white, .white.collapse-popover, .live-chat, .menu-static, .preview-doc .dashboard-container .header {
    background-color: white;
}

#hero-wrapper .key-information .slogan-challenge h1, .label, .m-b-xs, .nowrap-label .show-more, .selectize-input .item {
    margin-bottom: 5px;
}

.header-profile .media-profile .localisation i.fa, .label, .m-r-xs, .nowrap-label .show-more, .selectize-input .item, .state-success .bullet, .state-warning .bullet {
    margin-right: 5px;
}

.label-ghost.primary, .nowrap-label .show-more, .selectize-input .item {
    color: #4da3f8;
    border: 1px solid #4da3f8;
}

.label-ghost, .nowrap-label .show-more, .selectize-input .item {
    color: #0f2031;
    background: transparent;
    border: 1px solid #0f2031;
}

.label, .nowrap-label .show-more, .selectize-input .item {
    width: auto;
    display: inline-block;
    border-radius: 4px;
    padding: 5px 10px;
}

ul {
    padding: 0;
    margin: 0;
}
ul li {
    list-style-position: outside;
}

#enrollment-nav .challenge-detail, #enrollment-nav .challenge-detail .slogan-challenge, #enrollment-nav .enrollment-down, #enrollment-nav .enrollment-up, #enrollment-nav .friend-list, #enrollment-nav .friend-list .display-group, .live-chat .conversation-filter .select-style, .media, .media-body, ul.list-messages .message-author, ul.list-messages .message-body, ul.list-messages li.message {
    overflow: hidden;
    zoom: 1;
}
#enrollment-nav .challenge-detail, #enrollment-nav .challenge-detail .slogan-challenge, #enrollment-nav .friend-list, #enrollment-nav .friend-list .display-group, .live-chat .conversation-filter .select-style, .media-body, ul.list-messages .message-author, ul.list-messages .message-body {
    width: 10000px;
}

#enrollment-nav .challenge-detail, #enrollment-nav .challenge-detail .area-logo, #enrollment-nav .challenge-detail .slogan-challenge, #enrollment-nav .friend-list, #enrollment-nav .friend-list .display-group, #enrollment-nav .friend-list li, #enrollment-nav .friend-list ul.list-group, #enrollment-nav .remaining-time, #enrollment-nav .share, .live-chat .conversation-filter .select-style, .live-chat .conversation-filter label, .media-body, .media-left, .media-right, .preview-doc .media-image, .prize-featurette .gift-presentation .gift .media-gift, ul.list-messages .avatar, ul.list-messages .message-author, ul.list-messages .message-body, ul.list-messages .post-label, ul.list-messages .timestamp {
    display: table-cell;
    vertical-align: top;
}
#hero-wrapper .key-information .remaining-time, .custom-agorize-hacker_challenges .footer-home .section-footer ul.list-footer li+li, .custom-agorize-hacker_challenges section.section-challenge .block-desc ul.details-category li+li, .custom-agorize-home .footer-home .section-footer ul.list-footer li+li, .custom-agorize-home section.section-challenge .block-desc ul.details-category li+li, .custom-agorize-startup_challenges .footer-home .section-footer ul.list-footer li+li, .custom-agorize-startup_challenges section.section-challenge .block-desc ul.details-category li+li, .custom-agorize-student_challenges .footer-home .section-footer ul.list-footer li+li, .custom-agorize-student_challenges section.section-challenge .block-desc ul.details-category li+li, .header-profile .media-profile .list-label, .header-profile .media-stats, .intl-tel-input .country-list, .m-t-sm, .panel-group .panel+.panel {
    margin-top: 10px;
}

.m-t-sm, .panel-group .panel+.panel, .header-profile .media-profile .list-label, .header-profile .media-stats, .intl-tel-input .country-list {
    margin-top: 10px;
}

.super-card .section+.section {
    border-top: 1px solid #dee2e4;
}

.super-card .content {
    padding: 20px;
}

.bg-white, .live-chat, .menu-static, .navbar-challenges .nav-challenges, .popover.white, .preview-doc .dashboard-container .header, .white.collapse-popover {
    background-color: #fff;
}

.super-card .hiring-button {
    text-align: right;
}

.super-card .hiring-button {
    text-align: center;
}
*, :after, :before {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

*, *:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.btn-ghost.primary, .nowrap-label .btn-ghost.show-more, .selectize-input .btn-ghost.item {
    color: #4da3f8;
    background-color: transparent;
    border-color: #4da3f8;
}

.btn-group-xs>.btn, .btn-xs, .navbar-top .navbar-collapse .open .dropdown-menu li .notification-action .btn-xs {
    padding: 5px 24px;
    font-size: 14px;
    line-height: 20px;
    border-radius: 50px;
}
.super-card .hiring-button+.chat-link, .super-card .hiring-button .btn, .super-card .hiring-button .chat-link, .super-card .hiring-button p {
    float: right;
}
.super-card .hiring-button p, .super-card .hiring-button .btn, .super-card .hiring-button .chat-link, .super-card .hiring-button+.chat-link {
    float: right;
}

ul.list-team .media-photo {
    width: 48px;
    height: 48px;
    display: inline-block;
    vertical-align: top;
    border: 2px solid transparent;
    border-radius: 48px;
}

.user-avatar.media-photo {
    width: 40px;
    height: 40px;
    display: inline-block;
    vertical-align: top;
    border: 0 solid transparent;
    border-radius: 40px;
}
ul.list-team li {
    display: block;
    padding: 0;
    margin: 10px;
}

ul li {
    list-style-position: outside;
}
ul.list-team .media-photo .media-round img {
    width: 100%;
    min-width: 100%;
    min-height: 100%;
    vertical-align: top;
}

ul.list-team .media-photo {
    width: 48px;
    height: 48px;
    display: inline-block;
    vertical-align: top;
    border: 2px solid transparent;
    border-radius: 48px;
}
.user-avatar.media-photo.user-online {
    border: 0 solid #0cb87d;
}

.user-avatar.media-photo.user-online {
    border: 0 solid #0cb87d;
}

ul.list-team .media-photo.user-online {
    border: 2px solid #0cb87d;
}
ul.list-team .media-photo .media-round img {
    width: 100%;
    min-width: 100%;
    min-height: 100%;
    vertical-align: top;
}

.user-avatar.media-photo .media-round img {
    width: 100%;
    min-width: 100%;
    min-height: 100%;
    vertical-align: top;
}

img {
    vertical-align: middle;
}

img {
    border: 0;
}

.btn-primary, .navbar-default .navbar-nav>li .btn-primary, .navbar-panel .navbar-nav>li .btn-primary {
    color: #fff;
    background-color: #4da3f8;
    border-color: #4da3f8;
}
.btn-ghost {
    color: #0f2031;
    background-color: transparent;
    border-color: #0f2031;
}
ul.list-team .media-photo .media-round {
    width: 44px;
    height: 44px;
    border: 2px solid transparent;
    border-radius: 44px;
    overflow: hidden;
    position: relative;
}

.user-avatar.media-photo .media-round {
    width: 40px;
    height: 40px;
    border: 0 solid transparent;
    border-radius: 40px;
    overflow: hidden;
    position: relative;
}

.text-center, .modal-body .submit, .header-description, .preview-doc .category {
    text-align: center;
}

p {
    margin: 0 0 10.5px;
}

#hero-wrapper .key-information .slogan-challenge h1, .label, .m-b-xs, .nowrap-label .show-more, .selectize-input .item {
    margin-bottom: 5px;
}

.text-ellipsis, .text-ellipsis-wrap {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.super-card .thumbnail .bg {
    background-size: cover;
    background-position: 50%;
    border-radius: 3px 3px 0 0;
    z-index: 0;
}

#enrollment-nav .challenge-detail .area-logo, #enrollment-nav .enrollment-down>.pull-left, #enrollment-nav .enrollment-up>.pull-left, #enrollment-nav .friend-list li, #enrollment-nav .friend-list ul.list-group, #enrollment-nav .post-discussion .enrollment-down>.post-avatar, #enrollment-nav .post-discussion .enrollment-up>.post-avatar, .live-chat .conversation-filter label, .media-left, .media>.pull-left, .post-discussion #enrollment-nav .enrollment-down>.post-avatar, .post-discussion #enrollment-nav .enrollment-up>.post-avatar, .post-discussion .media>.post-avatar, .post-discussion ul.list-messages li.message>.post-avatar, .preview-doc .media-image, .prize-featurette .gift-presentation .gift .media-gift, ul.list-messages .avatar, ul.list-messages .post-discussion li.message>.post-avatar, ul.list-messages li.message>.pull-left {
    padding-right: 10px;
}

p {
    margin: 0 0 10.5px;
}

.bg-primary, .live-chat .chat-footer .action, ul.list-vote li .slider span, ul.list-vote li .slider .complete {
    color: #fff;
}

.bg-primary, .live-chat .chat-footer .action, ul.list-vote li .slider .complete, ul.list-vote li .slider span {
    color: #fff;
}
div#teams {
    background-color: #fff;
}
.teams-listing{float:left;}



/***********************/
</style>
<div id="preloader-loader" style="display:none;"></div>
 <div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down">
    <div class="container">
      <!-- <h1 class="wow fadeInUp" data-wow-delay="0.1s">Challenge Details</h1>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb wow fadeInUp">
          <li class="breadcrumb-item"><a href="#">Challenge</a></li>
          <li class="breadcrumb-item active" aria-current="page">Challenge Details </li>
        </ol>
      </nav> -->
    </div>
    <!--/end container-->
  </div>

<!--====================================================
        CHALLENGE DETAILS WITH TAB STRUCTURE
======================================================-->
<section id="challenge-intro">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="img-box">
            <img src="<?php echo $image //base_url('assets/challenge/'.$challenge_data[0]['company_logo']); ?>" class="img-fluid" alt="img">
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-6">
              <div class="title-bar">
                <div class="title">Proposed by ARAI</div>
                <h3><?php echo ucwords($challenge_data[0]['challenge_title']) ?></h3>
                <div class="heading-border-light"></div>
              </div>
			   <?php if($challenge_data[0]['challenge_id']): ?>
				<div class="challenge-code">
					
					<?php echo $challenge_data[0]['challenge_id']; ?>
				
				</div>
				<?php endif; ?>
              <ul class="list-group list-group-horizontal">
               <!-- <li class="list-group-item">
					<?php //$part_link = 'https://docs.google.com/forms/d/e/1FAIpQLSd1dXXfSbzpsFvX5ElrwkO6t7tyOvCrcI5q36fhpX8qY4q7yw/viewform?embedded=true'; ?>
                  <a href="<?php echo site_url('challenge/participate_link/'.base64_encode($part_link)); ?><?php //echo base_url('home/participate'); ?>" class="btn btn-general btn-green participate-clickx" role="button">Participate</a>
                </li>-->
                <!--<li class="list-group-item"><a href="" class="chshare"><i class="fa fa-share-alt"></i> Share</a></li>-->
              
				<?php if($challenge_data[0]['challenge_id'] == 'TNCID-000018'):
					$part_link = 'https://docs.google.com/forms/d/e/1FAIpQLSfKzVrZZQtMYf-fy-6d6K9LPj4Gw0b5qvN1LsmJ5v8GC8D3RQ/viewform?embedded=true';
				 ?>	
					<a href="<?php echo site_url('challenge/participate_link/'.base64_encode($part_link)); ?>" class="btn btn-general btn-green"  role="button">Participate</a>
				 <?php else: ?>
					<a href="<?php echo base_url('home/participate'); ?>" class="btn btn-general btn-green" role="button">Participate</a>
				<?php endif; ?>	
			  </ul>
            </div>
            <div class="col-md-6">
              <ul class="list-unstyled product_info">
                <li>
                  <label>Teams:</label>
                  <span> <a href="javascript:voiod(0);"><?php echo $challenge_data[0]['min_team'] ?> - <?php echo $challenge_data[0]['max_team'] ?> People</a></span></li>
                <li>
                  <i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></li>
              </ul>
              <p><?php 
			  
					$limit = 250;
					$str = $challenge_data[0]['company_profile'];
					 if (strlen($challenge_data[0]['company_profile'] ) > $limit){
						$str  = substr($challenge_data[0]['company_profile'] , 0, strrpos(substr($challenge_data[0]['company_profile'] , 0, $limit), ' ')) . '...'; 
					 }
			  
					echo $str; ?></p>
            </div>

          </div>

        </div>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="product_details">
      <ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#technology">Brief Company Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " data-toggle="tab" href="#features">Brief Info About Challenge</a>
        </li>
        <!--<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#application">Abstract About Challenge</a>
        </li>-->
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#validation_achive">Other Information</a>
        </li>
		
		<?php 
		if($this->session->userdata('user_id') != $challenge_data[0]['u_id']){
			
		if($challenge_data[0]['share_details'] == '1'): ?>     
		<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#intellectual_Property">Challenge Owner Details</a>
        </li>
		<?php endif;

			}
		?>  
		<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#teams">Teams</a>
        </li>
				
		
        <!--<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#intellectual_Property">Teams</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#abstract">Rules</a>
        </li>-->
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="technology">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <?php echo $challenge_data[0]['company_profile'] ?>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade " id="features">
          <div class="container">
            <div class="row justify-content-center">             
              <div class="col-md-12">
                 <?php echo $challenge_data[0]['challenge_details'] ?>                
              </div>
            </div>
          </div>
        </div>
        <!--<div class="tab-pane fade" id="application">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <p> <?php echo $challenge_data[0]['challenge_abstract'] ?>   </p>
               
              </div>

            </div>
          </div>
        </div>-->
        <div class="tab-pane fade" id="validation_achive">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
				<?php //echo "<pre>";print_r($challenge_data); ?>
				<table class="table">
					<?php if($challenge_data[0]['challenge_id']): ?>
					<tr>
						<td width="20%"><b>Challenge ID :</b></td>
						<td width="80%"><?php echo $challenge_data[0]['challenge_id']; ?></td>
					</tr>
					<?php endif; ?>
										
					<tr>
						<td><b>Launch Date :</b></td>
						<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_launch_date'])); ?></td>
					</tr>
					<tr>
						<td><b>Close Date :</b></td>
						<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_close_date'])); ?></td>
					</tr>
					<tr>
						<td><b>Technology :</b></td>
						<td><?php echo $challenge_data[0]['technology_name']; 
						if(in_array("0", $challenge_data[0]['res_tech_id'])){ echo ', Other'; }
						?></td>
					</tr>
					<?php 
					
					if(in_array("0", $challenge_data[0]['res_tech_id']))
					{ 
					?>
					<tr>
						<td><b>Other Technology :</b></td>
						<td><?php echo $challenge_data[0]['other_technology_name']; ?></td>
					</tr>
					<?php } ?>
					
					<tr>
						<td><b>Audience Preference :</b></td>
						<td><?php echo $challenge_data[0]['preference_name']; ?></td>
					</tr>					
					<tr>
						<td><b>Domain:</b></td>
						<td><?php echo $challenge_data[0]['domain_name']; ?></td>
					</tr>
					</table>
					
					<table class="table" id="show-hide">
					<tr>
						<td width="20%"><b>Contact Person Name :</b></td>
						<td width="80%"><?php echo $challenge_data[0]['contact_person_name']; ?></td>
					</tr>
					<tr>
						<td width="20%"><b>Abstract About Challenge :</b></td>
						<td width="80%"> <p> <?php echo $challenge_data[0]['challenge_abstract'] ?>   </p></td>
					</tr>
					<tr>
						<td><b>Tags :</b></td>
						<td><?php echo $challenge_data[0]['tag_name']; 
						if(in_array("0", $challenge_data[0]['res_tag_id'])){ echo ', Other'; } ?></td>
					</tr>
					<?php 
					if(in_array("0", $challenge_data[0]['res_tag_id']))
					{
					?>
					<tr>
						<td><b>Other Tags :</b></td>
						<td><?php echo $challenge_data[0]['other_tag_name']; ?></td>
					</tr>
					<?php } ?>
					<?php if($challenge_data[0]['if_funding'] =='Funding' ): ?>
					<tr>
						<td><b>Amount :</b></td>
						<td><?php echo $challenge_data[0]['is_amount']; ?></td>
					</tr>
					<?php endif; ?>
					
					<?php if($challenge_data[0]['if_reward'] =='Reward' ): ?>
					<tr>
						<td><b>Rewards :</b></td>
						<td><?php echo $challenge_data[0]['fund_reward']; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><b>Expected TRL Level:</b></td>
						<td><?php echo $challenge_data[0]['trl_solution']; ?></td>
					</tr>
					
					<tr>
						<td><b>Challenge Visibility:</b></td>
						<td><?php echo $challenge_data[0]['challenge_visibility']; ?></td>
					</tr>
					
					<?php if($challenge_data[0]['terms_txt'] !="" ): ?>
					<tr>
						<td><b>Terms & Conditions: </b></td>
						<td><?php echo $challenge_data[0]['terms_txt']; ?></td>
					</tr>
					<?php endif;  ?>
					<?php if($challenge_data[0]['future_opportunities'] !="" ): ?>
					<tr>
						<td><b>Future Opportunities: </b></td>
						<td><?php echo $challenge_data[0]['future_opportunities']; ?></td>
					</tr>
					<?php endif;  ?>
					<tr>
						<td><b>IP Clause: </b></td>
						<td><?php echo $challenge_data[0]['ip_name']; ?></td>
					</tr>
					
					<?php if($challenge_data[0]['is_external_funding'] == '1' ): ?>
					<tr>
						<td><b>Required Funding Amount(in %) :</b></td>
						<td><?php echo $challenge_data[0]['external_fund_details']; ?></td>
					</tr>
					<?php endif;  ?>
					<tr>
						<td><b>Challenge Exclusivity: </b></td>
						<td><?php  if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Exclusively listed on Technovuus'; else: echo 'Also listed on other platforms'; endif; ?></td>
					</tr>	
					<?php if($challenge_data[0]['is_exclusive_challenge'] == '0' ): ?>
					<tr>
						<td><b>Details :</b></td>
						<td><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></td>
					</tr>
					<?php endif; ?>
					
				</table>
					<div class="row">
						<a href="javascript:void(0);" class="btn-toggle-show btn btn-warning">Show Advanced Details</a>
						<a href="javascript:void(0);" class="btn-toggle-hide btn btn-warning" style="display:none;">Hide Advanced Details</a>
					</div>
			  </div>

            </div>
          </div>
        </div>
		<div class="tab-pane fade" id="intellectual_Property">		
			<div class="container">	
		  
			<?php		
				if($this->session->userdata('user_id') == ""){ 
			?>
		 
				<div class="row justify-content-center">			
				  <div class="row"><h4>Ask challenge owner details.?</h4></div>
				  <p>This challenge is marked as "PRIVATE" by the challenge owner. <br />
				  To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
				</div>
				 <div class="row justify-content-center" align="center">				
				 <a href="<?php echo base_url('login'); ?>" class="btn btn-general btn-green" role="button">REQUEST ACCESS</a>
				</div>
			<?php		
				} else {  // Login Users
				
				$style = 'display:none';
				 if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
					 $style = 'display:block'; 
				 }
				 
				 $anchor = 'display:block'; 	
				 if(count($request_user) > 0){
					 $status_check = $request_user[0]['status']; 
					 if($status_check == 'Approve'){
						$style = 'display:block'; 
						$anchor = 'display:none'; 
					 } else {
						$style = 'display:none'; 
						$anchor = 'display:block';
					 }
					 
				 } else { 
					$status_check = 'REQUEST ACCESS'; 
				 }
				 
				if($challenge_data[0]['challenge_visibility'] == 'Public'){ // Public Details
					
				?>					
				<div class="col-md-6">
					<table class="table table-bordered">
						<tr>
							<td><b>Email ID </b></td>
							<td><?php echo $challenge_data[0]['email_id']; ?></td>
						</tr>
						<tr>
							<td><b>Mobile Number </b></td>
							<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
						</tr>
						<tr>
							<td><b>Office Number</b></td>
							<td><?php echo $challenge_data[0]['office_no']; ?></td>
						</tr>
					</table>
				</div> 
			
				<?php	} else { 

					
				?>
					
				<div class="row justify-content-center">			
				  <h4 style="<?php echo $anchor; ?>">Ask challenge owner details?</h4>
				  <p>This challenge is marked as "PRIVATE" by the challenge owner. To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
				</div>
				 <div class="row justify-content-center" align="center">				
				 <a href="javascript:void(0);" data-cid="<?php echo $challenge_data[0]['c_id']  ?>" data-oid="<?php echo $challenge_data[0]['u_id']  ?>" style="<?php echo $anchor; ?>" class="btn btn-general btn-green ask-details" onClick="return get_challenge_details(<?php echo $challenge_data[0]['c_id']  ?>, <?php echo $challenge_data[0]['u_id']  ?>)" role="button"><?php echo $status_check ?></a>
				</div>
				<?php if(($this->session->userdata('user_id') == $request_user[0]['sender_id']) && $request_user[0]['status'] == 'Approve'){ ?>	
				<div class="col-md-6">
					<table class="table table-bordered">
						<tr>
							<td><b>Email ID </b></td>
							<td><?php echo $challenge_data[0]['email_id']; ?></td>
						</tr>
						<tr>
							<td><b>Mobile Number </b></td>
							<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
						</tr>
						<tr>
							<td><b>Office Number</b></td>
							<td><?php echo $challenge_data[0]['office_no']; ?></td>
						</tr>
					</table>
				</div>	
				<?php } ?>	
			<?php
					} // Private Challenge
					
				} // Else End
				
			?>
		 
		   </div>        
      </div>
		<div class="tab-pane fade" id="teams">		
			<div class="container">
				<div class="col-md-12">
				<div class="row teams-listing">
				<?php if(count($team_details) > 0): 
						foreach($team_details as $teamslist):
						$slotDecryption =  New Opensslencryptdecrypt();	
						$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $teamslist['c_id']));
						//print_r($getSlotMaxMember);
						// Team Slot Details
						$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $teamslist['c_id'], 'team_id'=> $teamslist['team_id'], 'is_deleted' => '0'));
						$maxTeam = $slotDecryption->decrypt($getSlotMaxMember[0]['max_team']);
						$minTeamCnt = count($slotDetails);
						$availableCnt = $maxTeam - $minTeamCnt;
						$array_skills = array();
						foreach($slotDetails as $slot_list){
							array_push($array_skills, $slot_list['skills']);							
						}
						
						$combine = implode(",",$array_skills);
						$xp = explode(',',$combine);
						$arrayUnique = array_unique($xp);
						
				?>
					<div class="col-md-4">
						<div class="super-card team-12509 picture-preview m-b-md" data-environment="team" data-video-url="" data-avatar-url="" data-id="">
							<div class="thumbnail m-b-none">
							  <a href="#" class="btn-player">
								<i class="fa fa-play-circle text-white"></i>
							  </a>
							  <div class="container-iframe"></div>
							  <div class="filter full hidden">
								<div class="bg-hiring title-vertical-center text-center"></div>
							  </div>
							  <div class="bg" style="background-image: url(../../uploads/byt/<?php echo $teamslist['team_banner'] ?>)">
								<div class="filter"></div>
							  </div>
							  <div class="title text-left m-l-md m-b-md">
								<h3 class="text-white m-b-xs h4"><?php echo $teamslist['team_name'] ?></h3>
								<p class="text-sm text-white m-t-xs text-ellipsis m-b-none"></p>
							  </div>
							</div>
								<div class="text-center bg-primary content-sm">
								  <p class="m-b-none" style="text-align: center; color: #fff;">
								  <?php if($availableCnt != 0){ ?>
								  <?php echo $maxTeam ?> members - <?php echo $availableCnt; ?> Available slots
								   <?php } else { ?>
								   <?php echo $maxTeam ?> members
								   <?php } ?>
								  </p>
								</div>
								  <div class="section">
							  <div class="content bg-white">
								<div class="content-sm bg-light m-b-sm block-radius">
								  <p class="text-base m-t-md m-b-xs"><b>Brief Info about Challenge</b></p>
								  <p class="m-b-none"><?php echo substr($teamslist['proposed_approach'],0,90).'...'; ?> </p>
									<div class="nowrap-label" style=" border-top: 1px solid #000;">
										<div class="list" style=" margin-top: 10px;">
											<p class="text-base m-t-md m-b-xs"><b>Looking for (max 3 Lines)</b></p>
											<?php
											$s = 0;	
											foreach($arrayUnique as $commonSkill){
												if($s < 9){
												$skill_now = $this->master_model->getRecords('skill_sets',array('id'=> $commonSkill));
												$skill_names = $slotDecryption->decrypt($skill_now[0]['name']);												
												?>
												<span class="btn btn-info" style="margin: 5px 5px 5px 5px;"><?php echo $skill_names; ?></span>
											<?php }
												$a++;
											}
											?>
											
										</div>
									</div>
								</div>
							<ul class="list-team m-t-sm">
								<?php
								
								
								foreach($slotDetails as $slotD){
										
										$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id']));
										$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
										$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
										
										$catID = $getUserDetail[0]['user_category_id'];	
										$getNames = $slotDecryption->decrypt($getUserDetail[0]['first_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['middle_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['last_name']); //." ".$getUserDetail[0]['user_id']
										$fullname = ucwords($getNames);
										if($catID == 1){
											$this->db->select('profile_picture, skill_sets_search');
											$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
											
											$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
											if ($getProfileDetail[0]['profile_picture']!="") {
												$imageUser = $imgPath;
											} else {
												$imageUser = base_url('assets/no-img.png');
											}
											
											$skillsets = $getProfileDetail[0]['skill_sets_search'];
											$span = '';
											if($skillsets!=""){
												
												if($skillsets!="0"){
												
												$expSkillset = explode(",", $skillsets);
												
													foreach($expSkillset as $getSkillname){
														$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
														$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
														$span .= '<span class="btn btn-info" style="margin: 5px 5px 5px 5px;">'.$get_skill_name.'</span>';
													}
												
												}
												
											} 											
											
										} else {
											$span = '';
											
											$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
											
											if(count($getOrgDetail) > 0){
												
												$profilePic = $slotDecryption->decrypt($getOrgDetail[0]['org_logo']);
												$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
												if ($profilePic!="") {
													$imageUser = $imgPath;
												} else {
													$imageUser = base_url('assets/no-img.png');
												}
												
											}
											
											
										}
										
											
								?>
								<li>
								  <div class="media-left">
									<a href="#">
									  <div class="user-avatar media-photo user-online [object Object]">
											<div class="media-round">
												<a href="<?php echo base_url('challenge/applySlot/'.base64_encode($slotD['slot_id'])); ?>"><img src="<?php echo $imageUser; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $fullname; ?>"></a>
											</div>
										</div>
									</a>
								  </div>
								  <div class="media-body">
									<h4 class="h6 m-t-none m-b-xs"><?php echo $fullname; ?></h4>									
								  </div>
								  <div ><?php echo $span; ?></div>
								</li>
								<?php } ?>
							</ul>
							  </div>
							</div>
						  <div class="section">
							<div class="content bg-white">
							  <div class="action"><div class="hiring-button"><a class="btn btn-primary btn-xs" href="#">Apply</a></div><a class="btn btn-ghost primary btn-xs" href="#">Chat</a></div>
							  <div class="clearfix"></div>
							</div>
						  </div>
						  <div class="section">
							<div class="content bg-white">
							  <div class="action"><a class="btn btn-primary btn-xs" href="<?php echo base_url('myTeams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>">View Details</a></div>
							  <div class="clearfix"></div>
							</div>
						  </div>
						</div>
					</div>
						<?php 
							endforeach;
						else:

						echo "<p class='text-center'>No Teams Found</p>";	
						endif; ?>
						
				</div> <!---- End Team Listing ---->
			</div>  
			</div>	
		</div>
  </section>
  <?php if($this->session->userdata('user_id') == $challenge_data[0]['u_id']): ?>
   <section>
    <div class="challenges-title">
      <div class="row">
        <div class="col-sm-9">
          <h3><a href="javascript:void(0);" class="show-hide1">Additional Statistics to be portrayed view options</a></h3>
        </div>       
      </div>
    </div>
	<div class="portrayed-content" >
		<div class="conatiner-fluid">
		  <div class="row">		  
			<div class="col-md-12" align="center">
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-1">
				<div class="content">
				  <span><?php echo count($total_received) ?></span>
				  <p>Access Request</p>
				</div>
				  <i class="lni lni-enter"></i>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-2">
				<div class="content">
					<span>0</span>
					<p> Total Applicant</p>
				 </div>
				  <i class="lni lni-pencil-alt"></i>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-3">
				<div class="content">
					<span><?php echo count($approved_cnt) ?></span>
				   <p>Total Approved</p>
				</div>
				  <i class="lni lni-checkmark-circle"></i>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-4">
				<div class="content">
					<span><?php echo count($pending_cnt) ?></span>
					<p> Total Pending</p>
				</div>
				  <i class="lni lni-timer"></i>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-5">
				<div class="content">
					<span><?php echo count($rejected_cnt) ?></span>
					<p> Total Rejected</p>
				 </div>
				  <i class="lni lni-cross-circle"></i>
			  </div>
			  </a>
			</div>
		  </div>
		</div>			
   </div>
  </section>  
  <hr>
  <?php endif; ?>
 

  <!--====================================================
                      OUR PARTNERS
======================================================-->
  <!--Client Logo Section-->
 	<?php
	if(count($consortium_list) > 0){
	?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Technology Platform Partners</h2>
                    <div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
				<?php foreach($consortium_list as $consrotium): ?>
                <div class="col-sm-12"><a href="<?php echo $consrotium['img_link']; ?>" target="_blank"><img src="<?php echo base_url('assets/partners/'.$consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
                <?php endforeach; ?>
			</div>
			</div>
        </div>
    </section>
	<?php } ?>

	
  <section class="ministrylogo">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center title-bar">
          <h4>Under Aegis of</h4>
          <div class="heading-border"></div>
           <a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
        </div>
      </div>
    </div>
  </section>
  <!--<a href="" onClick="return popitup('https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u')">Click Me</a>
  <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />-->
<script>
function popitup(url) 
{
	newwindow=window.open(url,'name','height=600,width=1200,screenX=400,screenY=350');
	if (window.focus) {newwindow.focus()}
	return false;
}
function get_challenge_details(c_id, o_id){

	swal(
		{
			title:"Confirmation:",
			text: "You are requesting access to the challenge details",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!',
			html:'You are requesting access to the owner details. '+'<input type="hidden" name="c_id" id="c_id" value="<?php echo $challenge_data[0]['c_id']  ?>" /><input type="hidden" name="o_id" id="o_id" value="<?php echo $challenge_data[0]['u_id']  ?>" /><input type="hidden" class="token" id="csrf_test_name" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash(); ?>" />'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();
				var c_id = $("#c_id").val();
				var o_id = $("#o_id").val();	
				//alert(c_id+"=="+o_id);return false;
				/*var c_id = $(this).attr('data-cid');
				var o_id = $(this).attr('data-oid');
				var cs_t = 	$('.token').val();
				var base_url = '<?php echo base_url() ?>';*/
				 $.ajax({
					type:'POST',
					url: base_url+'challenge/askfordetails',
					data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);								
						$(".token").val(output.token); 	
						var textAlert = output.alert_text;
						var stat = output.status;
						if(stat == 'N'){
							$(".ask-details").text('Waiting For Owner Approval');
						}
						swal({
								title: 'Success!',
								text: textAlert,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						
					}
				});	// Ajax End
				
			}
		
		});
}


jQuery(document).ready(function(){
	
		
		$(document).on('click','.participate-click',function(){
			window.open("https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u");
		});
		
		$(document).on('click','.btn-toggle-show',function(){
			$('#show-hide').css('display', 'block');
			$('.btn-toggle-hide').css('display', 'block');
			$('.btn-toggle-show').css('display', 'none');
		});

		$(document).on('click','.btn-toggle-hide',function(){
			$('#show-hide').css('display', 'none');
			$('.btn-toggle-hide').css('display', 'none');
			$('.btn-toggle-show').css('display', 'block');
		});
	
	// Read More/Less
	    $(".content-new").show();
		  $(".show_hide_new").on("click", function() {	
			//console.log('HELLO');
			$(this).parent().next(".content-new").toggle();			
			if ($(this).text().trim() == "Read More") {
			  $(this).text("Read Less");
			} else {
			  $(this).text("Read More");
			}
		  });
	
	/*$('.ask-details').on('click', function(){	
		var c_id = $(this).attr('data-cid');
		var o_id = $(this).attr('data-oid');
		var base_url = '<?php echo base_url() ?>';
		var cs_t = 	$('.token').val();	
		 $.ajax({
			type:'POST',
			url: base_url+'challenge/askfordetails',
			data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
			success:function(data){
				var output = JSON.parse(data);								
				$(".token").val(output.token); 			
				
				var textAlert = output.alert_text;
				var stat = output.status;
				if(stat == 'N'){
					$(".ask-details").text('Waiting For Owner Approval');
				}
				swal({
						title: 'Success!',
						text: textAlert,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				
			}
		});	
		
	});*/	
});
</script>


