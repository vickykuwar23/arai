<?php 
	error_reporting(0);
	$datestr = $challenge_data[0]['challenge_close_date'];//Your date
	$date	 = strtotime($datestr);//Converted to a PHP date (a second count)
	
	//Calculate difference
	$diff	= 	$date-time();//time returns current time in seconds
	$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
	//$hours	=	round(($diff-$days*60*60*24)/(60*60));
	$style="";
	
	
	if($days > 0){
		$style="display:none";
		$showCnt = $days. " Days left";
		} else {
		$showCnt = "Closed Paticipations";
	}
	
	
	
	/*if ($challenge_data[0]['company_logo']!="") {
		$image = $fileExist;
		} else {
		$image = base_url('assets/not_available.png');
	}*/
	
	$bannerImg = base_url('assets/challenge/'.$challenge_data[0]['banner_img']);
	
	
	
	if($challenge_data[0]['company_logo']!=""){
		$fileExist = base_url('assets/challenge/'.$challenge_data[0]['company_logo']);
		$image = $fileExist;
		
	}else {
		
		if($challenge_data[0]['challenge_id'] == 'TNCID-000018'){
			$image = base_url('assets/arai_site.jpg');
		}else {
			$image = base_url('assets/not_available.png');
		}
		
	}
		
	arai_site
?>

<style>
	
	.home-p.pages-head4 {
	background: url(<?php echo $bannerImg; ?>) no-repeat center top;
	background-size: 100%; min-height: 100%;
	}
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	table#show-hide{display:none;}
	
	
	
	
	
	/***********************/
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down">
	<div class="container">     
	</div>
</div>

<!--====================================================
	CHALLENGE DETAILS WITH TAB STRUCTURE
======================================================-->
<section id="challenge-intro">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="img-box">
					<img src="<?php echo $image; ?>" class="img-fluid" alt="img">
				</div>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-6">
						<div class="title-bar">
							<div class="title">Proposed by ARAI</div>
							<h3><?php echo ucwords($challenge_data[0]['challenge_title']) ?></h3>
							<div class="heading-border-light"></div>
						</div>
						<?php if($challenge_data[0]['challenge_id']): ?>
						<div class="challenge-code">
							
							<?php echo $challenge_data[0]['challenge_id']; ?>
							
						</div>
						<?php endif; ?>
						<ul class="list-group list-group-horizontal">				
							<a href="<?php echo base_url('team/create/'.base64_encode($challenge_data[0]['c_id'])); ?>" class="btn btn-general btn-green" role="button">Participate</a>
							
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="list-unstyled product_info">
							<li>
								<label>Teams:</label>
							<span> <a href="javascript:voiod(0);"><?php echo $challenge_data[0]['min_team'] ?> - <?php echo $challenge_data[0]['max_team'] ?> People</a></span></li>
							<li>
							<i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></li>
						</ul>
						<p><?php 
							
							$limit = 250;
							$str = $challenge_data[0]['company_profile'];
							if (strlen($challenge_data[0]['company_profile'] ) > $limit){
								$str  = substr($challenge_data[0]['company_profile'] , 0, strrpos(substr($challenge_data[0]['company_profile'] , 0, $limit), ' ')) . '...'; 
							}
							
						echo $str; ?></p>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</div>
</section>
<section>
	<div class="product_details">
		<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#technology">Brief Company Profile</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " data-toggle="tab" href="#features">Brief Info About Challenge</a>
			</li>        
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#validation_achive">Other Information</a>
			</li>		
			<?php 
				if($this->session->userdata('user_id') != $challenge_data[0]['u_id']){
					
				if($challenge_data[0]['share_details'] == '1'): ?>     
				<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#intellectual_Property">Challenge Owner Details</a>
				</li>
				<?php endif;
					
				}
			?>  
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#teams">Teams</a>
			</li>			
		</ul>
		
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="technology">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<?php echo $challenge_data[0]['company_profile'] ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="tab-pane fade " id="features">
				<div class="container">
					<div class="row justify-content-center">             
						<div class="col-md-12">
							<?php echo $challenge_data[0]['challenge_details'] ?>                
						</div>
					</div>
				</div>
			</div>    
			
			<div class="tab-pane fade" id="validation_achive">
				<div class="container">
					<div class="row">
						<div class="col-md-12">				
							<table class="table">
								<?php if($challenge_data[0]['challenge_id']): ?>
								<tr>
									<td width="20%"><b>Challenge ID :</b></td>
									<td width="80%"><?php echo $challenge_data[0]['challenge_id']; ?></td>
								</tr>
								<?php endif; ?>
								
								<tr>
									<td><b>Launch Date :</b></td>
									<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_launch_date'])); ?></td>
								</tr>
								<tr>
									<td><b>Close Date :</b></td>
									<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_close_date'])); ?></td>
								</tr>
								<tr>
									<td><b>Technology :</b></td>
									<td><?php echo $challenge_data[0]['technology_name']; 
										if(in_array("0", $challenge_data[0]['res_tech_id'])){ echo ', Other'; }
									?></td>
								</tr>
								<?php 
									
									if(in_array("0", $challenge_data[0]['res_tech_id']))
									{ 
									?>
									<tr>
										<td><b>Other Technology :</b></td>
										<td><?php echo $challenge_data[0]['other_technology_name']; ?></td>
									</tr>
								<?php } ?>
								
								<tr>
									<td><b>Audience Preference :</b></td>
									<td><?php echo $challenge_data[0]['preference_name']; ?></td>
								</tr>					
								<tr>
									<td><b>Domain:</b></td>
									<td><?php echo $challenge_data[0]['domain_name']; ?></td>
								</tr>
							</table>
							
							<table class="table" id="show-hide">
								<tr>
									<td width="20%"><b>Contact Person Name :</b></td>
									<td width="80%"><?php echo $challenge_data[0]['contact_person_name']; ?></td>
								</tr>
								<tr>
									<td width="20%"><b>Abstract About Challenge :</b></td>
									<td width="80%"> <p> <?php echo $challenge_data[0]['challenge_abstract'] ?>   </p></td>
								</tr>
								<tr>
									<td><b>Tags :</b></td>
									<td><?php echo $challenge_data[0]['tag_name']; 
									if(in_array("0", $challenge_data[0]['res_tag_id'])){ echo ', Other'; } ?></td>
								</tr>
								<?php 
									if(in_array("0", $challenge_data[0]['res_tag_id']))
									{
									?>
									<tr>
										<td><b>Other Tags :</b></td>
										<td><?php echo $challenge_data[0]['other_tag_name']; ?></td>
									</tr>
								<?php } ?>
								<?php if($challenge_data[0]['if_funding'] =='Funding' ): ?>
								<tr>
									<td><b>Amount :</b></td>
									<td><?php echo $challenge_data[0]['is_amount']; ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if($challenge_data[0]['if_reward'] =='Reward' ): ?>
								<tr>
									<td><b>Rewards :</b></td>
									<td><?php echo $challenge_data[0]['fund_reward']; ?></td>
								</tr>
								<?php endif; ?>
								<tr>
									<td><b>Expected TRL Level:</b></td>
									<td><?php echo $challenge_data[0]['trl_solution']; ?></td>
								</tr>
								
								<tr>
									<td><b>Challenge Visibility:</b></td>
									<td><?php echo $challenge_data[0]['challenge_visibility']; ?></td>
								</tr>
								
								<?php if($challenge_data[0]['terms_txt'] !="" ): ?>
								<tr>
									<td><b>Terms & Conditions: </b></td>
									<td><?php echo $challenge_data[0]['terms_txt']; ?></td>
								</tr>
								<?php endif;  ?>
								<?php if($challenge_data[0]['future_opportunities'] !="" ): ?>
								<tr>
									<td><b>Future Opportunities: </b></td>
									<td><?php echo $challenge_data[0]['future_opportunities']; ?></td>
								</tr>
								<?php endif;  ?>
								<tr>
									<td><b>IP Clause: </b></td>
									<td><?php echo $challenge_data[0]['ip_name']; ?></td>
								</tr>
								
								<?php if($challenge_data[0]['is_external_funding'] == '1' ): ?>
								<tr>
									<td><b>Required Funding Amount(in %) :</b></td>
									<td><?php echo $challenge_data[0]['external_fund_details']; ?></td>
								</tr>
								<?php endif;  ?>
								<tr>
									<td><b>Challenge Exclusivity: </b></td>
									<td><?php  if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Exclusively listed on Technovuus'; else: echo 'Also listed on other platforms'; endif; ?></td>
								</tr>	
								<?php if($challenge_data[0]['is_exclusive_challenge'] == '0' ): ?>
								<tr>
									<td><b>Details :</b></td>
									<td><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></td>
								</tr>
								<?php endif; ?>
								
							</table>
							<div class="row">
								<a href="javascript:void(0);" class="btn-toggle-show btn btn-warning">Show More</a>
								<a href="javascript:void(0);" class="btn-toggle-hide btn btn-warning" style="display:none;">Show Less</a>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="tab-pane fade" id="intellectual_Property">		
				<div class="container">	
					
					<?php		
						if($this->session->userdata('user_id') == ""){ 
						?>
						
						<div class="row justify-content-center">			
							<div class="row"><h4>Ask challenge owner details.?</h4></div>
							<p>This challenge is marked as "PRIVATE" by the challenge owner. <br />
							To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
						</div>
						<div class="row justify-content-center" align="center">				
							<a href="<?php echo base_url('login'); ?>" class="btn btn-general btn-green" role="button">REQUEST ACCESS</a>
						</div>
						<?php		
							} else {  // Login Users
							
							$style = 'display:none';
							if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
								$style = 'display:block'; 
							}
							
							$anchor = 'display:block'; 	
							if(count($request_user) > 0){
								$status_check = $request_user[0]['status']; 
								if($status_check == 'Approve'){
									$style = 'display:block'; 
									$anchor = 'display:none'; 
									} else {
									$style = 'display:none'; 
									$anchor = 'display:block';
								}
								
								} else { 
								$status_check = 'REQUEST ACCESS'; 
							}
							
							if($challenge_data[0]['challenge_visibility'] == 'Public'){ // Public Details
								
							?>					
							<div class="col-md-6">
								<table class="table table-bordered">
									<tr>
										<td><b>Email ID </b></td>
										<td><?php echo $challenge_data[0]['email_id']; ?></td>
									</tr>
									<tr>
										<td><b>Mobile Number </b></td>
										<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
									</tr>
									<tr>
										<td><b>Office Number</b></td>
										<td><?php echo $challenge_data[0]['office_no']; ?></td>
									</tr>
								</table>
							</div> 
							
							<?php	} else { 
								
								
							?>
							
							<div class="row justify-content-center">			
								<h4 style="<?php echo $anchor; ?>">Ask challenge owner details?</h4>
								<p>This challenge is marked as "PRIVATE" by the challenge owner. To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
							</div>
							<div class="row justify-content-center" align="center">				
								<a href="javascript:void(0);" data-cid="<?php echo $challenge_data[0]['c_id']  ?>" data-oid="<?php echo $challenge_data[0]['u_id']  ?>" style="<?php echo $anchor; ?>" class="btn btn-general btn-green ask-details" onClick="return get_challenge_details(<?php echo $challenge_data[0]['c_id']  ?>, <?php echo $challenge_data[0]['u_id']  ?>)" role="button"><?php echo $status_check ?></a>
							</div>
							<?php if(($this->session->userdata('user_id') == $request_user[0]['sender_id']) && $request_user[0]['status'] == 'Approve'){ ?>	
								<div class="col-md-6">
									<table class="table table-bordered">
										<tr>
											<td><b>Email ID </b></td>
											<td><?php echo $challenge_data[0]['email_id']; ?></td>
										</tr>
										<tr>
											<td><b>Mobile Number </b></td>
											<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
										</tr>
										<tr>
											<td><b>Office Number</b></td>
											<td><?php echo $challenge_data[0]['office_no']; ?></td>
										</tr>
									</table>
								</div>	
							<?php } ?>	
							<?php
							} // Private Challenge
							
						} // Else End
						
					?>
					
				</div>        
			</div>
			
			<div class="tab-pane fade show" id="teams">
				<div class="container">
					<div class="row teams-listing">
						<?php if(count($team_details) > 0): 
							$m=1;
							foreach($team_details as $teamslist):
							$slotDecryption =  New Opensslencryptdecrypt();	
							$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $teamslist['c_id']));
							//print_r($getSlotMaxMember);
							// Team Slot Details
							$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $teamslist['c_id'], 'team_id'=> $teamslist['team_id'], 'is_deleted' => '0'));
							$maxTeam = $slotDecryption->decrypt($getSlotMaxMember[0]['max_team']);
							$minTeamCnt = count($slotDetails);
							$availableCnt = $maxTeam - $minTeamCnt;
							$array_skills = array();
							foreach($slotDetails as $slot_list){
								array_push($array_skills, $slot_list['skills']);							
							}
							
							$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $teamslist['c_id']));
							$dCha = $slotDecryption->decrypt($chDetails[0]['challenge_details']);
							$combine = implode(",",$array_skills);
							$xp = explode(',',$combine);
							$arrayUnique = array_unique($xp); 
							if($teamslist['team_banner']!=""){
								$banner_team = base_url('uploads/byt/'.$teamslist['team_banner']);
							} else {
								$banner_team = base_url('assets/no-img.png');
							}
							?>
						
						<div class="col-sm-12 justify-content-center mt-4 team-box">
							<div class="boxSection25">
								<ul class="list-group list-group-horizontal">
									<li class="list-group-item">
										<div class="inner-box">
										
											<img src="<?php echo $banner_team; ?>" alt="Banner" class="img-fluid">
											<h3 class="minheight"><?php echo $teamslist['team_name'] ?></h3>
											<ul>
												<li> <?php echo $maxTeam ?> Members </li> 
												<li>-</li>
												<li><?php echo $availableCnt; ?> Available Slots</li>
											</ul>
										</div>
										<div class="border_top_bottom">
											<a href="javascript:void(0);" class="d-inline-block chatButton apply-popup">Chat</a>
										</div>
									</li>
									
									<li class="list-group-item">
										<div class="inner-box">											
											<strong style="display:block; text-align:left; margin:15px 0px 0px 0px;">Brief Info about Challenge </strong>
											<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0"><?php echo substr($dCha,0,90).'...'; ?></p>
											<h4>Looking For</h4>
											<div class="Skillset">
												<?php
													$s = 0;	
													foreach($arrayUnique as $commonSkill){
														if($s < 9){
															$skill_now = $this->master_model->getRecords('skill_sets',array('id'=> $commonSkill));
															$skill_names = $slotDecryption->decrypt($skill_now[0]['name']);												
														?>
														
														<span class="badge badge-pill badge-info"><?php echo $skill_names; ?></span>
														<?php }
														$a++;
													}
												?>												
											</div>
										</div>
										<div class="border_top_bottom">
											<a href="<?php echo base_url('myteams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>" class="btn btn-primary w-100 mt-02" >View Details</a>
										</div>
									</li>
									
									<li class="list-group-item">
										<div class="team_listing owl-carousel owl-theme">
											<?php		
												$TeamSr = 1;
												foreach($slotDetails as $slotD)
												{	
													$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id']));
													
													if(count($getSlotDetail) > 0)
													{														
														$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
														$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
														
														$catID = $getUserDetail[0]['user_category_id'];	
														$getNames = $slotDecryption->decrypt($getUserDetail[0]['first_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['middle_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['last_name']); //." ".$getUserDetail[0]['user_id']
														$fullname = ucwords($getNames);
														
														$skillsets = $slotD['skills'];
															//$skillsets = $getProfileDetail[0]['skill_sets_search'];
															$span = '';
															if($skillsets!=""){																
																if($skillsets!="0"){
																	
																	$expSkillset = explode(",", $skillsets);
																	$s=1;
																	foreach($expSkillset as $getSkillname){
																		if($s <=6)
																		{																			
																			$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
																			$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
																			$span .= '<span class="badge badge-pill badge-info">'.$get_skill_name.'</span>';																			
																		}
																		$s++;																		
																	}																	
																}																
															} 		
														
														
														if($catID == 1)
														{															
															$this->db->select('profile_picture, skill_sets_search');
															$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
															
															$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
															if ($getProfileDetail[0]['profile_picture']!="") {
																$imageUser = $imgPath;
																} else {
																$imageUser = base_url('assets/no-img.png');
															}
															
															/*$skillsets = $getProfileDetail[0]['skill_sets_search'];
															$span = '';
															if($skillsets!=""){																
																if($skillsets!="0"){
																	
																	$expSkillset = explode(",", $skillsets);
																	$s=1;
																	foreach($expSkillset as $getSkillname){
																		if($s <=6)
																		{																			
																			$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
																			$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
																			$span .= '<span class="badge badge-pill badge-info">'.$get_skill_name.'</span>';																			
																		}
																		$s++;																		
																	}																	
																}																
															} */											
															
															} else if($catID == 2){
															//$span = '';
															//echo ">userID>".$getUserDetail[0]['user_id'];
															$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
															
															if(count($getOrgDetail) > 0){
																
																$profilePic = $slotDecryption->decrypt($getOrgDetail[0]['org_logo']);
																$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
																if ($profilePic!="") {
																	$imageUser = $imgPath;
																	} else {
																	$imageUser = base_url('assets/no-img.png');
																}
																
															}
															
														}								
														
														if($getSlotDetail[0]['apply_user_id'] == $teamslist['user_id']){
															$styles_hide = "hide-class";
															} else {
															$styles_hide = "d-inline-block";
														}
														
														} else {
														
														$imageUser = base_url('assets/no-img.png');
														$styles_hide = "d-inline-block";
														$fullname = "Open Slot";											
														$skillsets = $slotD['skills'];
														$span = '';
														if($skillsets!=""){
															
															if($skillsets!="0"){
																
																$expSkillset = explode(",", $skillsets);
																$s=1;
																foreach($expSkillset as $getSkillname){
																	if($s <=6){
																		
																		$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
																		$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
																		$span .= '<span class="badge badge-pill badge-info">'.$get_skill_name.'</span> ';																		
																	}
																	$s++;
																	
																}
																
															}
															
														} 		
														
													} // Else End
												?>
												
												<div class="item text-center">
													<img src="<?php echo $imageUser; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $fullname; ?>" class="check-<?php echo $getSlotDetail[0]['apply_user_id'] ?>" />
													<h4><?php echo $fullname; ?></h4>
													<div class="Skillset">
														<?php echo $span; ?>														
													</div>
													<h4 class="teamMember">Team Member <?php echo $TeamSr; ?></h4>
													<?php if($getSlotDetail[0]['status']!='Approved'): ?>
													<a href="javascript:void(0);" class="<?php echo $styles_hide; ?> apply-popup">Apply</a>
													<?php endif; ?>
												</div>
												
											<?php $m++; $TeamSr++; } ?>									
										</div> <!-- Slider End -->
										
									</li>
								</ul>
							</div>
						</div>
						<?php 
							endforeach;
							else:
							
							echo "<p style='width: 100%;margin-top: 20px;font-size: 18px !important;font-weight: 500;text-align:center'>No Teams Found</p>";	
						endif; ?>
						
					</div> <!---- End Team Listing ---->
					<!--<div class="row justify-content-center mt-4">
						<div class="col-md-12 text-center">
						<a href="javascript:void(0);" class="btn btn-primary">Show More</a>
						</div>
					</div>-->
					<?php 
						if(count($team_details) > 6):
					?>
					<div class="row justify-content-center mt-4  show_more_main" id="show_more_main<?php echo $teamslist['team_id']; ?>">
						<span id="<?php echo $teamslist['team_id']; ?>" class="show_more btn btn-general btn-white" title="Show More">Show more</span>
						<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if($this->session->userdata('user_id') == $challenge_data[0]['u_id']): ?>
<section>
	<div class="challenges-title">
		<div class="row">
			<div class="col-sm-9">
				<h3><a href="javascript:void(0);" class="show-hide1">Additional Statistics to be portrayed view options</a></h3>
			</div>       
		</div>
	</div>
	<div class="portrayed-content" >
		<div class="conatiner-fluid">
		  <div class="row">		  
				<div class="col-md-12" align="center">
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
						<div class="circle circle-1">
							<div class="content">
								<span><?php echo count($total_received) ?></span>
								<p>Access Request</p>
							</div>
							<i class="lni lni-enter"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
						<div class="circle circle-2">
							<div class="content">
								<span>0</span>
								<p> Total Applicant</p>
							</div>
							<i class="lni lni-pencil-alt"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
						<div class="circle circle-3">
							<div class="content">
								<span><?php echo count($approved_cnt) ?></span>
								<p>Total Approved</p>
							</div>
							<i class="lni lni-checkmark-circle"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
						<div class="circle circle-4">
							<div class="content">
								<span><?php echo count($pending_cnt) ?></span>
								<p> Total Pending</p>
							</div>
							<i class="lni lni-timer"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
						<div class="circle circle-5">
							<div class="content">
								<span><?php echo count($rejected_cnt) ?></span>
								<p> Total Rejected</p>
							</div>
							<i class="lni lni-cross-circle"></i>
						</div>
					</a>
				</div>
			</div>
		</div>			
	</div>
</section>  
<hr>
<?php endif; ?>


<!--====================================================
	OUR PARTNERS
======================================================-->
<!--Client Logo Section-->
<?php
	if(count($consortium_list) > 0){
	?>
	<!--Client Logo Section-->
	<section id="partners" data-aos="fade-down">
		<div class="container clientLogo2">
			<div class="row title-bar">
				<div class="col-md-12">
					<h2>Technology Platform Partners</h2>
					<div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
					<?php foreach($consortium_list as $consrotium): ?>
					<div class="col-sm-12"><a href="<?php echo $consrotium['img_link']; ?>" target="_blank"><img src="<?php echo base_url('assets/partners/'.$consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>


<section class="ministrylogo">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center title-bar">
				<h4>Under Aegis of</h4>
				<div class="heading-border"></div>
				<a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
			</div>
		</div>
	</div>
</section>
<!--<a href="" onClick="return popitup('https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u')">Click Me</a>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />-->

<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
	function popitup(url) 
	{
		newwindow=window.open(url,'name','height=600,width=1200,screenX=400,screenY=350');
		if (window.focus) {newwindow.focus()}
		return false;
	}
	function get_challenge_details(c_id, o_id){
		
		swal(
		{
			title:"Confirmation:",
			text: "You are requesting access to the challenge details",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!',
			html:'You are requesting access to the owner details. '+'<input type="hidden" name="c_id" id="c_id" value="<?php echo $challenge_data[0]['c_id']  ?>" /><input type="hidden" name="o_id" id="o_id" value="<?php echo $challenge_data[0]['u_id']  ?>" /><input type="hidden" class="token" id="csrf_test_name" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash(); ?>" />'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();
				var c_id = $("#c_id").val();
				var o_id = $("#o_id").val();	
				//alert(c_id+"=="+o_id);return false;
				/*var c_id = $(this).attr('data-cid');
					var o_id = $(this).attr('data-oid');
					var cs_t = 	$('.token').val();
				var base_url = '<?php echo base_url() ?>';*/
				$.ajax({
					type:'POST',
					url: base_url+'challenge/askfordetails',
					data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);								
						$(".token").val(output.token); 	
						var textAlert = output.alert_text;
						var stat = output.status;
						if(stat == 'N'){
							$(".ask-details").text('Waiting For Owner Approval');
						}
						swal({
							title: 'Success!',
							text: textAlert,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
						
					}
				});	// Ajax End
				
			}
			
		});
	}
	
	$(document).on('click','.apply-popup',function(){
		
		swal({
				title:"",
				text: "Coming Soon",
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK!',
				html:''
		});
		
	});
	
	jQuery(document).ready(function(){
		
		
		$('.team_listing').owlCarousel({
			items: 1,
			autoplay: true,
			smartSpeed: 700,
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true
		});
		
		
		
		/***************** Load More ***********************/
			
			// Load More Code 
			$(document).on('click','.show_more',function(){
				// $(".token").remove();
				var base_url = '<?php echo base_url(); ?>'; 
				var ID = $(this).attr('id');
				var length = $('.team-box').length;
				var cs_t  = 	$('.token').val();
				var ch_id = 	'<?php echo $challenge_data[0]['c_id']  ?>';
				$('#preloader-loader').css('display', 'block');	
				$('.show_more').hide();
				$('.loding').show();
				$.ajax({
					type:'POST',
					url: base_url+'challenge/team_more',
					data:'id='+length+'&csrf_test_name='+cs_t+'&ch_id='+ch_id,
					dataType:"html",
					async:false,
					success:function(response){ 
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(response);
						if(output.html == ""){
							$(".show_more_main").remove();
						}	
						$(".token").val(output.token);								
						$('#show_more_main'+ID).remove();
						$('.teams-listing').append(output.html); 					
						
						$(".team_listing").owlCarousel({
							items: 1,
							autoplay: true,
							smartSpeed: 700,
							loop: false,
							nav: true,
							dots: false,
							navText: ["", ""],
							autoplayHoverPause: true
						});
						
						
					},
					error: function (jqXHR, exception) { 
						$('#preloader-loader').css('display', 'none');	
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
							msg = 'Time out error.';
							} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
							} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
						console.log(msg);
						//$('#post').html(msg);
					},
				});
			});
			
			
			
			/******************End Load More ********************/
				
				
				
				
				$(document).on('click','.participate-click',function(){
					window.open("https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u");
				});
				
				$(document).on('click','.btn-toggle-show',function(){
					$('#show-hide').css('display', 'block');
					$('.btn-toggle-hide').css('display', 'block');
					$('.btn-toggle-show').css('display', 'none');
				});
				
				$(document).on('click','.btn-toggle-hide',function(){
					$('#show-hide').css('display', 'none');
					$('.btn-toggle-hide').css('display', 'none');
					$('.btn-toggle-show').css('display', 'block');
				});
				
				// Read More/Less
				$(".content-new").show();
				$(".show_hide_new").on("click", function() {	
					//console.log('HELLO');
					$(this).parent().next(".content-new").toggle();			
					if ($(this).text().trim() == "Read More") {
						$(this).text("Read Less");
						} else {
						$(this).text("Read More");
					}
				});
				
				
				
				/*$('.ask-details').on('click', function(){	
					var c_id = $(this).attr('data-cid');
					var o_id = $(this).attr('data-oid');
					var base_url = '<?php echo base_url() ?>';
					var cs_t = 	$('.token').val();	
					$.ajax({
					type:'POST',
					url: base_url+'challenge/askfordetails',
					data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
					success:function(data){
					var output = JSON.parse(data);								
					$(".token").val(output.token); 			
					
					var textAlert = output.alert_text;
					var stat = output.status;
					if(stat == 'N'){
					$(".ask-details").text('Waiting For Owner Approval');
					}
					swal({
					title: 'Success!',
					text: textAlert,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
					});
					
					}
					});	
					
				});*/	
			});
		</script>
		
		<?php $this->load->view('front/inc_sweet_alert_common'); ?>
		