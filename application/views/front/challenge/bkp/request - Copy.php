
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Ask Details Request Received</h1>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav> -->
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">
                    <div class="row">
                    <div class="col-md-4">                            
						<div class="form-group">
							<label  for="form-control-placeholder">Status </label>
							<select class="form-control select2" id="check_status" name="check_status">
							  <option value=""></option>
							  <option value="Pending">Pending</option>
							  <option value="Approve">Approved</option>
							  <option value="Reject">Reject</option>
																
							</select>							
						</div>
                     </div>
					<!--<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Challenge Visibility</label>
							<select id="change_visibility" name="change_visibility"  class="form-control">                           
									<option value=""></option>
								
							</select>						
						</div> 
					</div>                 
					
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Funding Available</label>
							<select id="fund_sel" name="fund_sel" class="form-control" required="">
								<option value="">-- Select --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">Reward Available</label>
							<select id="reward_sel" name="reward_sel" class="form-control" required="">
								<option value="">-- Select --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>	
						</div>
					</div>
					<div class="col-md-4">                            
						<div class="form-group">
							<label  for="form-control-placeholder">TRL </label>
							<select class="form-control select2" id="trl_id" name="trl_id">
							  <option value=""></option>
														
							</select>							
						</div>
                     </div>
					 <div class="col-md-4">
						<div class="form-group">
							<label class="form-control-placeholder" for="technology">IP Clause </label>
							<select id="ip_clause" name="ip_clause" class="form-control" required="">
								<option value=""></option>								
							</select>						
						</div> 
					</div>-->
					<div class="col-md-4">
						<button type="submit" id="" class="btn btn-secondary">Clear All </button>
						<button type="submit" id="btn-click" class="btn btn-primary btn-click">Apply</button>
					</div>

                 </div>
				 <input type="hidden" value="<?php echo $challenge_id; ?>" id="challenge_id" name="challenge_id" />
                 <div class="row mt-12">
                    <div class="col-md-12">
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
						 <?php echo $this->session->flashdata('success'); ?>
						</div>
					   <?php }  ?>
                        <div class="table-responsive">
                        <table id="request-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
                                <th scope="col">Spoc Email</th>
                                <th scope="col">Category</th>
								<th scope="col">Organization Name</th>
								<th scope="col">Institute Type</th>
								<th scope="col">Status</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>									
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
  $(document).ready( function () {	  
	
		var base_path = '<?php echo base_url() ?>';
		var table = $('#request-list').DataTable({
			
			"ordering":false,
			"searching": false,
			"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"challenge/request_owner_details",
			"type":"POST",
			"data":function(data) {	
					data.challenge_id 			= $("#challenge_id").val();
					data.check_status 			= $("#check_status").val();
					//data.change_visibility 		= $("#change_visibility").val();
					//data.ip_clause 				= $("#ip_clause").val();
					//data.fund_sel 				= $("#fund_sel").val();
					//data.reward_sel 			= $("#reward_sel").val();
					//data.trl_id 				= $("#trl_id").val();
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
		});

		$('.btn-click').click(function(){
			var check_status 		= $('#check_status').val();
			var challenge_id 	= $('#challenge_id').val();	
			//var ip_clause 			= $('#ip_clause').val();
			//var fund_sel 			= $('#fund_sel').val();
			//var reward_sel 			= $('#reward_sel').val();
			table.ajax.reload();		
		});
		
		$(document).on('click','.check-status',function(){	
			var request_id = $(this).attr('data-id');
			var request_status = $(this).attr('data-status');
			var base_path = '<?php echo base_url() ?>';
			var cs_t = 	$('.token').val();
			//alert(request_id);return false;
			$.ajax({
				type:'POST',
				url: base_path+'challenge/changeRequestStatus',
				data:'id='+request_id+'&csrf_test_name='+cs_t+'&status='+request_status,				
				dataType:"text",
				success:function(data){ //console.log(data);
					$("#preloader").css("display", "none");
					var output = JSON.parse(data);						
					$(".token").val(output.token);
					var replaceText = output.change_text;
					console.log(replaceText);
					//$(this).attr("data-status", replaceText);
					//$(this).find("a#data-status").text(replaceText);
					//$(this).find("#data-status").html(replaceText);
					window.location.reload();
					swal({
							title: 'Success!',
							text: "Request Status successfully updated.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				}
			});
			
			
		});
		
		
	$('.select2').select2({});

});
$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>