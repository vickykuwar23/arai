<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Closure Challenge</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Closure Challenge</li>
			</ol>
		</nav>
	</div>
</div> 
<?php
$challengeDecryption =  New Opensslencryptdecrypt();
 ?>
<div id="preloader-loader" style="display:none;"></div>
<section>
<form name="closureForm" id="closureForm" method="post" role="form" enctype="multipart/form-data">
   
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-6"> 
				<div class="formInfo">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group mt-4">
								<input type="text" id="closure_title" name="closure_title" class="form-control" required="">
								<label class="form-control-placeholder" for="cname">Title<em>*</em></label>
							</div>
							<div class="form-group mt-4">
								<input class="form-check-input get-type" type="radio" value="Image" id="upload_type" name="upload_type" value="Image">
								<label class="form-check-label" for="defaultCheck1">
									Image
								</label>
								
								<input class="form-check-input get-type" type="radio" value="Video" id="upload_type" name="upload_type" value="Video">
								<label class="form-check-label" for="defaultCheck1">
								   Video
								</label>
							</div>							
							<div class="form-group mt-4" id="uploadImg">
								<div class="field_wrapper_img">
									<div>
										<div class="form-group upload-btn-wrapper">
											<button class="btn btn-upload"><i class="fa fa-plus"> </i> Upload Image</button>
											<input type="file" class="form-control image_link" name="closer_file[]" id="closer_file">
											<div class="clearfix"></div>									
											<span class="small">Note : Files should be images less than 2MB</span>
											<div class="clearfix"></div>
										</div>
										<a href="javascript:void(0);" class="add_img" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>					 
							<div class="form-group mt-4" id="uploadVideo">
								<div class="field_wrapper">
									<div>
										<input type="text" class="form-control video_link" id="youtube_link" name="youtube_link[]" value=""/>
										<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus" aria-hidden="true"></i></a>
										 <label class="form-control-placeholder" for="cname">Video<em>*</em></label>
									</div>
								</div>
							</div>
							<div class="form-group mt-4">
								<textarea class="form-control" id="exampleFormControlTextarea1" id="desc_info" name="desc_info"></textarea>
								<label class="form-control-placeholder" for="exampleFormControlTextarea1">Info Box<em>*</em></label>
							</div>
							<div class="form-group mt-4" >
								<div class="field_wrapper_doc">
									<div>
										<div class="form-group upload-btn-wrapper">
											<button class="btn btn-upload"><i class="fa fa-plus"> </i> Upload Document</button>
											<input type="file" class="form-control valid_doc" name="closer_doc[]" id="closer_doc">
											<div class="clearfix"></div>									
											<span class="small">Note : Files should be images less than 2MB</span>
											<div class="clearfix"></div>
										</div>
										<a href="javascript:void(0);" class="add_doc" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>	
							
							<button type="submit" class="btn btn-primary">Submit</button> 
						</div> 
					</div> 
				</div> 
			</div> 
		</div>
	</div>	
</form>
 </section>
 <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
 <script>
 
 // Word Count Validation
function getWordCount(wordString) {
	var words = wordString.split(" ");
	
	words = words.filter(function(words) { 
		return words.length > 0
	}).length;
	return words;
}



$(document).ready( function () {
	
	$('#uploadImg').hide();
	$('#uploadVideo').hide();
	$( ".get-type" ).change(function() {	
		var values = $(this).val();
		if(values == "Image"){
			$('#uploadImg').show();
			$('#uploadVideo').hide();
		}else if(values == "Video"){
			$('#uploadVideo').show();
			$('#uploadImg').hide();
		}
	});
	
	// Multiple Input Fields For Youtube Link
	var maxField = 50; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="text" class="form-control video_link" id="youtube_link" name="youtube_link[]" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-remove" aria-hidden="true"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
	
	// End For Video Link
	
	// Multiple Input Fields For Image Upload
	var addButtonImg = $('.add_img'); //Add button selector
    var wrapper_img = $('.field_wrapper_img'); //Input field wrapper
    var fieldHTML_img = '<div><div class="form-group upload-btn-wrapper"><button class="btn btn-upload"><i class="fa fa-plus"> </i> Upload Image</button><input type="file" class="form-control image_link" name="closer_file[]" id="closer_file"><div class="clearfix"></div><span class="small">Note : Files should be images less than 2MB</span><div class="clearfix"></div></div><a href="javascript:void(0);" class="remove_button_img"><i class="fa fa-remove" aria-hidden="true"></i></a></div>'; 
	
	//New File Uupload
    var f = 1; //Initial field counter is 1
	
	//Once add button is clicked
    $(addButtonImg).click(function(){
        //Check maximum number of input fields
        if(f < maxField){ 
            f++; //Increment field counter
            $(wrapper_img).append(fieldHTML_img); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper_img).on('click', '.remove_button_img', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        f--; //Decrement field counter
    });
	
	//End Image Multiple Upload
	
	
	
	
	
	var addButtondoc = $('.add_doc'); //Add button selector
    var wrapper_doc = $('.field_wrapper_doc'); //Input field wrapper
    var fieldHTML_doc = '<div><div class="form-group upload-btn-wrapper"><button class="btn btn-upload"><i class="fa fa-plus"> </i> Upload Image</button><input type="file" class="form-control valid_doc" name="closer_doc[]" id="closer_doc"><div class="clearfix"></div><span class="small">Note : Files should be images less than 2MB</span><div class="clearfix"></div></div><a href="javascript:void(0);" class="remove_button_doc"><i class="fa fa-remove" aria-hidden="true"></i></a></div>'; 
	
	//New File Uupload
    var d = 1; //Initial field counter is 1
	
	//Once add button is clicked
    $(addButtondoc).click(function(){
        //Check maximum number of input fields
        if(d < maxField){ 
            d++; //Increment field counter
            $(wrapper_doc).append(fieldHTML_doc); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper_doc).on('click', '.remove_button_doc', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        f--; //Decrement field counter
    });
	
	jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);			
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));	
		
		
		$.validator.addMethod("youtube", function(value, element) {
		 var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
		 return (value.match(p)) ? RegExp.$1 : false;
		}, "Please enter valid youtube url.");
		
		$.validator.addMethod("youtube_link", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please enter youtube link.');		
		$.validator.addClassRules("video_link", { youtube_link: true, youtube: true });
		
		
		$.validator.addMethod("image_link", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'This field is required');		
		$.validator.addClassRules("valid_img", { image_link: true, youtube: true });
		
	
	
	
		
	$("#closureForm" ).validate({			
			/*debug: true,
			 onkeyup: true,
			ignore: ':hidden:not("#resultId")',
			debug: false, 
			load: true,
			blur: true,
			change: true,
			keypress: true,
			keyup: true,
			keydown: true,
			onclick: true,*/
			debug: false,
			onclick: true,	
			rules: {				
				closure_title: {
					required: true,
					minCount:['3'],
					maxCount:['100']	
				},
				upload_type: {
					required: true	
				}
			},
			messages: {
				closure_title: {
					required: "This field is required"
				},
				upload_type: {
					required: "This field is required"
				}
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				//alert("INNN");
				form.submit();
				return true;
				/*var valid = true;
				var video_link = $('.is-invalid').length;
				console.log(video_link);return false;
				//var video_link = $('form[name="closureForm"]').find('video_link');
				
				var video_link = $('.video_link').length;
				//alert($('.video_link').length);
				if(video_link > 0) 
				{
					for(var i = 0; i < video_link; i++) 
					{	console.log("===>>"+i);
						if($(video_link[i]).closest("input.form-control").addClass("is-invalid")) 
						{  console.log("INN In IF");
							if(!$(video_link[i]).val()) 
							{
								$(video_link[i]).closest(".form-control").addClass("is-invalid");
								$(video_link[i]).after('<span id="' + Math.random() + '-error" class="help-block pull-right">This field is required.</span>');
								valid = false;
							} else {
								console.log('OUT');
							}
						}
					}
				}
				return false;
				if(valid === true) {
					form.submit();
				}*/
			
			}
		});	

}); // Document Event
</script> 