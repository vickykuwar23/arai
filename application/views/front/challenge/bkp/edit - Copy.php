<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Edit Challenge </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Edit Challenge </li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer a.img_outer img { max-width: 300px; max-height: 100px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
</style>
<style>
.datepicker table tr td, .datepicker table tr th 
{
    border-radius: 0;
    border: 1px solid rgba(0,0,0,0.1);
}

.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover 
{
    background: rgba(0,0,0,0.1);
    color: rgba(0,0,0,0.4);
    cursor: not-allowed;
    border: 1px solid #fff;
}

th.datepicker-switch, th.next, th.prev 
{
    border: none !important;
}

#challengeform .btn-group.sw-btn-group .sw-btn-next { position: absolute; right: 100px; }
</style>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php }  ?>
					<?php if( $this->session->flashdata('error')){ ?>
						<div class="alert alert-error alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } 
						
						//Technology
						$explodes = explode(",",$challenge_data[0]['technology_id']);
						$arrTech = array();
						foreach($explodes as $ids){
							array_push($arrTech, $ids);
						}
						
						
						// TagsDetail
						$exp = explode(",",$challenge_data[0]['tags_id']);
						$arrTags = array();
						foreach($exp as $tid){
							array_push($arrTags, $tid);
						}
						
						
						// Audience
						$prefID = explode(",",$challenge_data[0]['audience_pref_id']);
						$arrPref = array();
						foreach($prefID as $t_id){
							array_push($arrPref, $t_id);
						}
						
						// Domain
						$domain = explode(",",$challenge_data[0]['domain']);
						$arrdomain = array();
						foreach($domain as $d_id){
							array_push($arrdomain, $d_id);
						}
						
						//State Arr
						$gArr = array();
						$geoArr = explode(",",$challenge_data[0]['geographical_states']);
						foreach($geoArr as $g_id){
							array_push($gArr, $g_id);
						}
						
						echo validation_errors(); 
					?> 				
					
					<form method="POST" id="challengeform" name="challengeform" enctype="multipart/form-data">
						<div id="smartwizard_add_challenge">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
								<li><a href="#step-4">Step 4<br /><small></small></a></li>
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<div class="row">					 
							
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="challenge_title" id="challenge_title" placeholder="Challenge Title" value="<?php echo $challenge_data[0]['challenge_title']; ?>" readonly >
												<label class="form-control-placeholder" for="cname">Challenge Title </label>
												<span><?php //echo form_error('challenge_title'); ?></span>
											</div>
										</div>
							
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="<?php echo $challenge_data[0]['company_name']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Company Name </label>
												<span><?php //echo form_error('company_name'); ?></span>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Company Banner </button>
												<input type="file" class="form-control" name="banner_img" id="banner_img" placeholder="" value="">
												<!--<img src="<?php echo base_url('assets/challenge/'.$challenge_data[0]['banner_img']); ?>" height="100" width="100" />-->
												<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted </p>
											</div>
										</div>
										
										<?php
											if($challenge_data[0]['banner_img'] != '')
											{	?>
											<div class="col-md-6" id="address_proof_outer">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'assets/challenge/'.$challenge_data[0]['banner_img']; ?>" target="_blank">
															<img src="<?php echo base_url().'assets/challenge/'.$challenge_data[0]['banner_img']; ?>">
														</a>
													</div>
												</div>
											</div>
										<?php } ?>
										
							
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="company_profile" id="company_profile" class="form-control" cols="12" rows="4"><?php echo $challenge_data[0]['company_profile']; ?></textarea>
												<label class="form-control-placeholder" for="cname">Company Profile <em></em></label>
												<span><?php echo form_error('company_profile'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="challenge_details" id="challenge_details" class="form-control" cols="12" rows="4" readonly><?php echo $challenge_data[0]['challenge_details']; ?></textarea>
												<label class="form-control-placeholder" for="cname">Brief Info About Challenge </label>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="company_details" id="company_details" class="form-control" cols="12" rows="4" readonly><?php echo $challenge_data[0]['challenge_abstract']; ?></textarea>
												<label class="form-control-placeholder" for="cname">Challenge Abstract </label>
												
											</div>
										</div>
									</div>
								</div>
								
								
								<div id="step-2" class="mt-4">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group"> 
												<input type="text" class="form-control datepicker" name="launch_date" id="launch_date" placeholder="Launch Date" value="<?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_launch_date'])); ?>" readonly />
												<label class="form-control-placeholder" for="cname">Launch Date <em>*</em></label>
												<span><?php echo form_error('launch_date'); ?></span>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="close_date" id="close_date" placeholder="Close Date" value="<?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_close_date'])); ?>" readonly />
												<label class="form-control-placeholder" for="cname">Closing Date <em>*</em></label>
												<span><?php echo form_error('close_date'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<label  for="cname">Technology <em>*</em></label>
												
												<select class="form-control choose-tech select2" id="techonogy_id" name="techonogy_id[]" multiple="multiple">
													
													<?php foreach($technology_data as $tech){ ?>
														<option value="<?php echo $tech['id'] ?>" <?php if(in_array($tech['id'], $arrTech)): ?> selected="selected" <?php endif; ?>><?php echo $tech['technology_name'] ?></option>								  
													<?php  } ?>
													<option value="0" <?php if(in_array(0, $arrTech)): ?> selected="selected" <?php endif; ?>>Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('techonogy_id'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12" id="otherTech">
											<div class="form-group">
												<input type="text" class="form-control" name="other_technology" id="other_technology" placeholder="Other Option" value="<?php  echo $challenge_data[0]['other_techonology'];  ?>">
												<label class="form-control-placeholder" for="cname"></label>
												<span><?php //echo form_error('other_technology'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<label  for="cname">Tags <em>*</em></label>
												<select class="form-control choose-tags select2" id="tags_id" name="tags_id[]" multiple="multiple">
													
													<?php foreach($tag_data as $tag){ ?>
														<option value="<?php echo $tag['id'] ?>" <?php if(in_array($tag['id'], $arrTags)): ?> selected="selected" <?php endif; ?>><?php echo $tag['tag_name'] ?></option>								  
													<?php  } ?>
													<option value="0" <?php if(in_array(0, $arrTags)): ?> selected="selected" <?php endif; ?>>Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('tags_id'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12" id="otherTags"><br />
											<div class="form-group">
												<input type="text" class="form-control" name="other_tag" id="other_tag" placeholder="" value="<?php echo $challenge_data[0]['added_tag_name']; ?>">
												<label class="form-control-placeholder" for="cname">Other <em>*</em></label>
												
												<span><?php echo form_error('other_tag'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12">
											<label class="form-control-placeholder" for="cname">Audience Preference  <em>*</em></label><br />
											<div class="form-group">
												
												<?php foreach($audi_data as $audience){ ?>
													<div style="float: left;">
														<div class="form-check form-check-inline">
															<input type="checkbox" class="chk" name="audi_id[]" id="audi_id" value="<?php echo $audience['id'] ?>" <?php if(in_array($audience['id'], $arrPref)): ?> checked="checked" <?php endif; ?>>
															&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> <?php echo $audience['preference_name'] ?></label>
														</div>									  
													</div>
												<?php  } ?>
												<!--<div style="float: left;">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="audi-chk" name="audi_id[]" id="audi_id" value="0" <?php if(in_array(0, $arrPref)): ?> checked="checked" <?php endif; ?>>
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> Other</label>
													</div>									  
												</div>-->
												<div style="float: left;">
													<div class="form-check form-check-inline">
														<input type="checkbox" class=""  id="checkAll"  >
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> All</label>
													</div>									  
												</div>	
												<span><?php echo form_error('audi_id'); ?></span>
											</div>								
										</div>
										<br />
										<div class="col-md-12" id="otherAudience">
											<div class="form-group">
												<input type="text" class="form-control" name="other_audience" id="other_audience" placeholder="Other Option" value="<?php echo $challenge_data[0]['other_audience']; ?>">
												<label class="form-control-placeholder" for="cname"></label>
												<span><?php echo form_error('other_audience'); ?></span>
											</div>
										</div>							
										<br />
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-label" for="ip_clause">Funding & Rewards</label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-chk" value="Funding" name="if_funding" id="if_funding" <?php if($challenge_data[0]['if_funding'] == 'Funding'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1"> Funding</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-reward" value="Reward" name="if_reward" id="if_reward" <?php if($challenge_data[0]['if_reward'] == 'Reward'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2"> Reward</label>
													</div>
												</div>
												
											</div>
										</div>
							
										<div class="col-md-6" id="fund_agree">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="is_amount" id="is_amount" placeholder="Enter Amount / Reward" value="<?php echo $challenge_data[0]['is_amount']; ?>">
												<label class="form-control-placeholder" for="cname">Funding Amount(in INR) <em>*</em></label>
											</div>
										</div>
							
										<div class="col-md-6" id="fund_reward_div">
											<div class="form-group">
												<input type="text" class="form-control" name="fund_reward" id="fund_reward" placeholder="Enter Reward" value="<?php echo $challenge_data[0]['fund_reward']; ?>">
												<label class="form-control-placeholder" for="cname">Reward Details  <em>*</em></label>
												
											</div>
										</div>
							
										<div class="col-md-6" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['tr_name']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Expected TRL Solution  </label>
												
											</div>
											<!--<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['tr_name']; ?>">
												<label for="form-control-placeholder">Expected TRL Solution </label>
												
												<select class="form-control select2" id="trl_solution" name="trl_solution" autocomplete="nope">
												<option value="">Please Select</option>
												<?php foreach($trl_data as $trl){ ?>
													<option value="<?php echo $trl['id'] ?>" <?php if($challenge_data[0]['trl_id'] == $trl['id']): ?> selected="selected" <?php endif; ?>><?php echo $trl['trl_name'] ?></option>								  
												<?php  } ?>
												</select>
												<span><?php echo form_error('trl_solution'); ?></span>
											</div>-->
										</div>							
										
									</div>
								</div>
								
								<div id="step-3" class="mt-4">
									<div class="row">
										<div class="col-md-12" >	
											<div class="title-box">
												<div class="title"><b>Eligibility Expectations</b></div>                  
											</div>
										</div>
										<hr />
										<div class="col-md-12" >
											<div class="form-group">
												<input type="text" class="form-control" name="educational" id="educational" placeholder="" value="<?php echo $challenge_data[0]['education']; ?>" readonly >
												<label class="form-control-placeholder" for="cname">Educational  <em></em></label>
											</div>
										</div>
							
										<div class="col-md-3" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['from_age']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">From Age  </label>
												
											</div>								
										</div>
										<div class="col-md-3" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['to_age']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">To Age  </label>									
											</div>
											<!--<div class="form-group">		
												<label  for="cname">To Age <em></em></label>
												
												<select class="form-control select2" id="to_age" name="to_age" autocomplete="nope">
												<option value="">Please Select</option>
												<?php
													for($f=10;$f<=80;$f++){
													?>
													<option value="<?php echo $f; ?>" <?php if($challenge_data[0]['to_age'] == $f){ ?> selected="selected" <?php } ?>><?php echo $f; ?></option>		
													<?php
													}
												?>
												</select>
											</div>-->
										</div>
							
										<div class="col-md-6" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['domainlist']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Domain  </label>
												
											</div>
											<!--<div class="form-group">
												<label  for="cname">Select Domain <em>*</em></label>
												
												<select class="form-control select2" id="domain" name="domain[]" autocomplete="nope" multiple="multiple">
												<option value="">Please Select</option>
												<?php foreach($domain_data as $domain_detail){ ?>
													<option value="<?php echo $domain_detail['id'] ?>" <?php  if(in_array($domain_detail['id'], $arrdomain)): ?> selected="selected" <?php endif; ?>><?php echo $domain_detail['domain_name'] ?></option>								  
												<?php  } ?>
												</select>
												<span><?php //echo form_error('trl_solution'); ?></span>									
											</div>-->
										</div>
										<div class="col-md-6" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['stateData']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Geographical (States)  </label>
												
											</div>	
											<!--<div class="form-group">
												<label for="cname">Geographical (States) <em></em></label>
												
												<select class="form-control select2" id="geographical" name="geographical[]" autocomplete="nope" multiple="multiple">
												<option value="">Please Select</option>
												<?php foreach($state as $state_detail){ ?>
													<option value="<?php echo $state_detail['id'] ?>" <?php if(in_array($state_detail['id'],$gArr)): ?> selected="selected" <?php endif; ?>><?php echo $state_detail['name'] ?></option>								  
												<?php  } ?>
												</select>
												<span><?php //echo form_error('trl_solution'); ?></span>									
											</div>-->
											
										</div>
										<div class="col-md-3" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['min_team']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Minimum Team  </label>
												
											</div>
											<!--<div class="form-group">
												<label for="cname">Minimum Team <em>*</em></label>
												
												<select class="form-control select2" id="min_team" name="min_team" autocomplete="nope">
												<option value="">Please Select</option>
												<?php
													for($t=0;$t<=10;$t++){
													?>
													<option value="<?php echo $t; ?>" <?php if($challenge_data[0]['min_team'] == $t): ?> selected="selected" <?php endif; ?>><?php echo $t; ?></option>		
													<?php
													}
												?>
												</select>
												<span></span>
												
											</div>-->
										</div>
							
										<div class="col-md-3" >
											<div class="form-group">
												<input type="text" class="form-control"  placeholder="" value="<?php echo $challenge_data[0]['max_team']; ?>" readonly>
												<label class="form-control-placeholder" for="cname">Maximum Team  </label>
												
											</div>
											<!--<div class="form-group">
												<label  for="cname">Maximum Team <em>*</em></label>
												
												<select class="form-control select2" id="max_team" name="max_team" autocomplete="nope">
												<option value="">Please Select</option>
												<?php
													for($m=1;$m<=10;$m++){
													?>
													<option value="<?php echo $m; ?>" <?php if($challenge_data[0]['max_team'] == $m): ?> selected="selected" <?php endif; ?>><?php echo $m; ?></option>		
													<?php
													}
												?>
												</select>
												<span><?php //echo form_error('max_team'); ?></span>
												
											</div>-->
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="challenge_terms" id="challenge_terms" class="form-control" cols="12" rows="4" readonly ><?php echo $challenge_data[0]['terms_txt']; ?></textarea>
												<label class="form-control-placeholder" for="cname">Challenge Terms & Conditions <em>*</em></label>
												
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">									
												<div class="">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="" name="is_agree" id="is_agree" value="1" <?php if($challenge_data[0]['is_agree'] == '1'){ ?> checked="checked" <?php } ?> readonly >
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> <a href="<?php echo base_url('home/termsOfuse'); ?>" target="_blank">Agree terms and conditions</a></label>
													</div>									  
												</div>									
											</div>
										</div>
									</div>
								</div>
								
								<div id="step-4" class="mt-4">
									<div class="row">
										<div class="col-md-12" >
											<div class="form-group">
												<input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name" value="<?php echo $challenge_data[0]['contact_person_name']; ?>">
												<label class="form-control-placeholder" for="cname"> Contact Person Name <em>*</em></label>
												<span><?php echo form_error('contact_name'); ?></span>
											</div>
										</div>
										<div class="col-md-12" >	
											<div class="title-box">
												<div class="title"><b>Contact Details </b></div>                  
											</div>
										</div>
										<hr />
										<div class="col-md-4" >
											<div class="form-group">
												<input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="Email address" value="<?php echo $challenge_data[0]['email_id']; ?>">
												<label class="form-control-placeholder" for="cname"> Email address  <em>*</em></label>
												<span><?php echo form_error('contact_email'); ?></span>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="form-group">
												<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $challenge_data[0]['mobile_no']; ?>">
												<label class="form-control-placeholder" for="cname"> Mobile Number <em>*</em></label>
												<span><?php echo form_error('mobile'); ?></span>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="form-group">
												<input type="text" class="form-control" name="office_no" id="office_no" placeholder="Office Number" value="<?php echo $challenge_data[0]['office_no']; ?>" maxlength="11">
												<label class="form-control-placeholder" for="cname"> Office Number <em>*</em></label>
												<span><?php echo form_error('office_no'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">									
												<div class="">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="" name="details_share" id="details_share" value="1" <?php if($challenge_data[0]['share_details'] == '1'){ ?> checked="checked" <?php } ?>  >
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> Share Contact Details with Participants </label>
													</div>									  
												</div>
												
											</div>
										</div>
							
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label" for="ip_clause">Challenge Visibility<em>*</em></label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input" value="Public" name="visibility" id="visibility" <?php if($challenge_data[0]['challenge_visibility'] == 'Public'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1"> Public</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input" value="Private" name="visibility" id="visibility" <?php if($challenge_data[0]['challenge_visibility'] == 'Private'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2"> Private</label>
													</div>
												</div>
												<span><?php echo form_error('visibility'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="future_opportunities" id="future_opportunities" class="form-control" cols="12" rows="4" readonly ><?php echo $challenge_data[0]['future_opportunities']; ?></textarea>
												<label class="form-control-placeholder" for="cname"> Future opportunities <em></em></label>
												
											</div>
										</div>
							
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label" for="ip_clause">IP Clause</label>
												<div class="d-block">
													<?php foreach($ip_clause_data as $clause){ ?>
														<div class="form-check form-check-inline">
															<input type="radio" class="form-check-input" value="<?php echo $clause['id'] ?>" name="ip_cls" id="ip_cls" <?php if($challenge_data[0]['ip_clause'] == $clause['id']){ ?> checked="checked" <?php } ?> readonly >
															<label class="form-check-label" for="techNovuus1"><?php echo $clause['ip_name'] ?></label>
														</div>
													<?php } ?>	
												</div>
												
											</div>
										</div>
							
										<div class="col-md-12">
											<div class="form-group form-check form-check-inline">
												<input type="checkbox" class="" name="is_external_funding" id="is_external_funding" value="1" <?php if($challenge_data[0]['is_external_funding'] == '1'){ ?> checked="checked" <?php } ?>>
												&nbsp;&nbsp;<label class="textName" style="margin:5px 0" for="cname"> External funding required?  </label>
												<span><?php echo form_error('is_external_funding'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12" id="external_fund">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="funding_amt" id="funding_amt" placeholder="" value="<?php echo $challenge_data[0]['external_fund_details']; ?>" maxlength="2">
												<label class="form-control-placeholder" for="cname"> Percentage Of Funding (%) <em>*</em></label>
												<span><?php echo form_error('funding_amt'); ?></span>
											</div>
										</div>
							
										<div class="col-md-12">								
											<div class="form-group">
												<label class="form-label" for="ip_clause">Is this challenge exclusively listed on TechNovuus?<em>*</em></label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input chk-exclusive" value="1" name="is_exclusive_challenge" id="is_exclusive_challenge" <?php if($challenge_data[0]['is_exclusive_challenge'] == '1'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1"> Yes</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input chk-exclusive" value="0" name="is_exclusive_challenge" id="is_exclusive_challenge" <?php if($challenge_data[0]['is_exclusive_challenge'] == '0'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2"> No</label>
													</div>
												</div>
												<span><?php echo form_error('is_exclusive_challenge'); ?></span>
											</div>								
									</div>
							
										<div class="col-md-12" id="excl_challenge">
											<div class="form-group">
												<textarea name="challenge_ex_details" id="challenge_ex_details" class="form-control" cols="12" rows="4"><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></textarea>
												<label class="form-control-placeholder" for="cname"> Challenge Exclusively Content <em>*</em></label>
												<span><?php echo form_error('challenge_ex_details'); ?></span>
											</div>
										</div>							
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function scroll_to_top(div_id='')
	{
		if(div_id == '') { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
	
	$(document).ready(function() 
	{
		//**** PREVENT FORM SUBMITTING WHEN PRESS ENTER ****
		$(window).keydown(function(event)
		{
			if(event.keyCode == 13) 
			{
				event.preventDefault();
				return false;
			}
		});
		
		//******* STEP SHOW EVENT *********
		$("#smartwizard_add_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");//sw-btn-next $(".btnfinish").addClass('d-none');
			if(stepNumber == 0 || stepNumber == 1 || stepNumber == 2){
				$(".sw-btn-next").show();
			}
			
			if (stepPosition === 'first') { $("#prev-btn").addClass('disabled'); $("#sw-btn-next").show(); $(".btnfinish").removeClass('d-none'); } 
			else if (stepPosition === 'final') { $(".sw-btn-next").hide(); $("#next-btn").addClass('disabled'); $(".btnfinish").removeClass('d-none'); } 
			else { $("#prev-btn").removeClass('disabled'); $("#next-btn").removeClass('disabled');   }
		});
		
		//******* STEP WIZARD *********
		$('#smartwizard_add_challenge').smartWizard(
		{
			/* selected: 2, */
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */	
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
					$('<button></button>').text('Submit')
					.addClass('btn btn-primary btnfinish')
					.on('click', function(e)
					{ 
						e.preventDefault();						
						var submit_flag = 0;
						
						if($("#challenge_ex_details").valid()==false) { submit_flag = 1; $("#challenge_ex_details").focus(); }
						if($("#is_exclusive_challenge").valid()==false) { submit_flag = 1; $("#is_exclusive_challenge").focus(); }
						if($("#funding_amt").valid()==false) { submit_flag = 1; $("#funding_amt").focus(); }
						if($("#is_external_funding").valid()==false) { submit_flag = 1; $("#is_external_funding").focus(); }
						if($("#visibility").valid()==false) { submit_flag = 1; $("#visibility").focus(); }
						if($("#office_no").valid()==false) { submit_flag = 1; $("#office_no").focus(); }
						if($("#mobile").valid()==false) { submit_flag = 1; $("#mobile").focus(); }
						if($("#contact_email").valid()==false) { submit_flag = 1; $("#contact_email").focus(); }
						if($("#contact_name").valid()==false) { submit_flag = 1; $("#contact_name").focus(); }	
						
						if($("#contact_name").valid()==false) { scroll_to_top('contact_name'); }
						else if($("#contact_email").valid()==false) { scroll_to_top('contact_email'); }
						else if($("#mobile").valid()==false) { scroll_to_top('mobile'); }
						else if($("#office_no").valid()==false) { scroll_to_top('office_no'); }
						else if($("#visibility").valid()==false) { scroll_to_top('visibility'); }
						else if($("#is_external_funding").valid()==false) { scroll_to_top('is_external_funding'); }
						else if($("#funding_amt").valid()==false) { scroll_to_top('funding_amt'); }
						else if($("#is_exclusive_challenge").valid()==false) { scroll_to_top('is_exclusive_challenge'); }
						else if($("#challenge_ex_details").valid()==false) { scroll_to_top('challenge_ex_details'); }
												
						if(submit_flag == 0)
						{
							swal(
							{
								title:"Update?",
								text: "Are you confirm to update challenge?",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes!'
							}).then(function (result) { if (result.value) { $('#challengeform').submit(); } });
						}
					}),
				]
			}
		});
		
		$("#smartwizard_add_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{				
				if($("#company_profile").valid()==false) { isValidate= false; $("#company_profile").focus(); }
				if($("#banner_img").valid()==false) { isValidate= false; $("#banner_img").focus(); }
				
				if($("#banner_img").valid()==false) { scroll_to_top('banner_img'); }
				else if($("#company_profile").valid()==false) { scroll_to_top('company_profile'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				if($("#fund_reward").valid()==false) { isValidate= false; $("#fund_reward").focus(); }
				if($("#is_amount").valid()==false) { isValidate= false; $("#is_amount").focus(); }
				if($("#if_funding").valid()==false) { isValidate= false; $("#if_funding").focus(); }
				if($("#other_audience").valid()==false) { isValidate= false; $("#other_audience").focus(); }
				if($("#audi_id").valid()==false) { isValidate= false; $("#audi_id").focus(); }
				if($("#tags_id").valid()==false) { isValidate= false; $("#tags_id").focus(); }
				if($("#other_tag").valid()==false) { isValidate= false; $("#other_tag").focus(); }
				if($("#techonogy_id").valid()==false) { isValidate= false; $("#techonogy_id").focus(); }
				if($("#other_technology").valid()==false) { isValidate= false; $("#other_technology").focus(); }
				if($("#close_date").valid()==false) { isValidate= false; $("#close_date").focus(); }
				if($("#launch_date").valid()==false) { isValidate= false; $("#launch_date").focus(); }
				
				if($("#launch_date").valid()==false) { scroll_to_top('launch_date'); }
				else if($("#close_date").valid()==false) { scroll_to_top('close_date'); }
				else if($("#techonogy_id").valid()==false) { scroll_to_top('techonogy_id'); }
				else if($("#tags_id").valid()==false) { scroll_to_top('tags_id'); }
				else if($("#other_tag").valid()==false) { scroll_to_top('other_tag'); }
				else if($("#other_technology").valid()==false) { scroll_to_top('other_technology'); }
				else if($("#audi_id").valid()==false) { scroll_to_top('audi_id'); }
				else if($("#other_audience").valid()==false) { scroll_to_top('other_audience'); }
				else if($("#if_funding").valid()==false) { scroll_to_top('if_funding'); }
				else if($("#is_amount").valid()==false) { scroll_to_top('is_amount'); }
				else if($("#fund_reward").valid()==false) { scroll_to_top('fund_reward'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==2 && stepDirection=="forward")
			{				
				scroll_to_top('smartwizard_add_challenge');
			}
			
			if(stepDirection=="backward") { scroll_to_top('smartwizard_add_challenge') }
			return isValidate;
		})
	});
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
		
	$("#checkAll").change(function () {
		$(".chk").prop('checked', $(this).prop("checked"));
	});
	
	$("#fund_agree").hide();
	$("#external_fund").hide();
	$("#fund_reward_div").hide();
	$("#otherTech").hide();
	$("#otherAudience").hide();
	$("#show-text").hide();
	$("#excl_challenge").hide();
	
	var get_tag_ward = '<?php echo $challenge_data[0]['tags_id'] ?>';
	var get_tech_ward = '<?php echo $challenge_data[0]['technology_id'] ?>';
	//console.log(get_tag_ward);
	
	if(jQuery.inArray("0", get_tag_ward) !== -1){
		//	console.log('Yes');
		$("#otherTags").show();
	} else {
		$("#otherTags").hide();
	}
	
	if(jQuery.inArray("0", get_tech_ward) !== -1){
		//	console.log('Yes');
		$("#otherTech").show();
	} else {
		$("#otherTech").hide();
	}
	
	var re_ward = $('input[name="if_reward"]:checked').val();
	if(re_ward == 'Reward'){ 			
	$("#fund_reward_div").show();
	} else {
	$("#fund_reward_div").hide();
	}
	
	var funds_ward = $('input[name="if_funding"]:checked').val();
	if(funds_ward == 'Funding'){			
	$("#fund_agree").show();
	} else {
	$("#fund_agree").hide();
	}
	
	var myfund = $('input[name="is_external_funding"]:checked').val();
	if(myfund == 1){
	$("#external_fund").show();
	} else {
	$("#external_fund").hide();
	}


	var chals = $('input[name="is_exclusive_challenge"]:checked').val();
	if(chals == '0'){
		$("#excl_challenge").show();
		} else {
		$("#excl_challenge").hide();
	}


<?php
	"<br>Current date : ".$current_date = date('d-m-Y');
	"<br>After 90 days : ".$date_after_90_days = date('d-m-Y', strtotime("+3 months", strtotime($current_date)));
?>
var FromDateFun = $('#launch_date').datepicker(
            {               
                keyboardNavigation: true,
                forceParse: true,
                calendarWeeks: false,
                autoclose: true,
                format: "dd-mm-yyyy",              
                startDate:"<?php echo $current_date; ?>",
                endDate:"<?php echo $date_after_90_days; ?>"
            }).
            on('changeDate', function(ev)
            {             
                var FromDateVal = $("#launch_date").val();
                var ToDateVal = $("#close_date").val();
             
                var newToDateStart = FromDateVal;
             
                var temp1 = FromDateVal.split('-');
                var convertFromDateVal = temp1[2] + '-' + temp1[1] + '-' + temp1[0].slice(-2);         
                var d = new Date(convertFromDateVal);
                d.setMonth(d.getMonth() + 3);
                var yyyy = (d.getFullYear());
                var mm = (d.getMonth());
                var dd = (d.getDate());             
                //var newToDateEnd = (yyyy+"-"+("0" + (mm + 1)).slice(-2)+"-"+("0" + (dd + 1)).slice(-2));
                var newToDateEnd = (("0" + (dd + 1)).slice(-2)+"-"+("0" + (mm + 1)).slice(-2)+"-"+yyyy);
             
                /* console.log(newToDateStart);
                console.log(newToDateEnd); */
             
                var temp2 = ToDateVal.split('-');
                var convertToDateVal = temp2[2] + '-' + temp2[1] + '-' + temp2[0].slice(-2);
             
                var temp3 = newToDateStart.split('-');
                var convertnewToDateStart = temp3[2] + '-' + temp3[1] + '-' + temp3[0].slice(-2);
             
                var temp4 = newToDateEnd.split('-');
                var convertnewToDateEnd = temp4[2] + '-' + temp4[1] + '-' + temp4[0].slice(-2);
             
                var update_flag = 1;
                if(convertToDateVal == "") { update_flag = 1; }
                else if(new Date(convertToDateVal) >= new Date(convertnewToDateStart) && new Date(convertToDateVal) <= new Date(convertnewToDateEnd)) { update_flag = 0; }
             
                $('#close_date').datepicker('setStartDate', newToDateStart);
                $('#close_date').datepicker('setEndDate', newToDateEnd);
             
                if(update_flag == 1)
                {
                    $('#close_date').datepicker('update', newToDateStart);
                }             
             
              
            }).data('datepicker');
         
            var ToDateFun = $('#close_date').datepicker(
            {
              
                keyboardNavigation: true,
                forceParse: true,
                calendarWeeks: false,
                autoclose: true,
                format: "dd-mm-yyyy",               
                startDate:"<?php echo $current_date; ?>",
                endDate:"<?php echo $date_after_90_days; ?>"
            }); 

// External Fund 
$('#is_external_funding').on('click', function(){
	//$('#is_external_funding').val($(this).is(':checked'));
	if($(this).is(":checked")) {
		$("#external_fund").show(); 
		} else {
		$("#funding_amt").val();
		$("#external_fund").hide();
	}			
});

// Exclusively Challenge
$('.chk-exclusive').on('click', function(){		 
	var chk = $('input[name="is_exclusive_challenge"]:checked').val();		  
	if(chk == '1') {
		$("#challenge_ex_details").val();
		$("#excl_challenge").hide();
		
		} else {
		$("#excl_challenge").show(); 
	}			
});


$( "#techonogy_id" ).change(function() {
	var f_name = $("#techonogy_id").val();
	if(f_name != 0){
		$("#otherTech").hide();
		} else {
		$("#otherTech").show();
	}
	
});


$(".audi-chk").click(function () {
	var v_name = $(this).val();				
	if($(this).is(":checked")) {
		$( ".chk" ).prop( "checked", false );
		$("#otherAudience").show(); 
		} else {				
		$("#otherAudience").hide();
	}			
});

$(".chk").click(function () {		   
	if($(this).is(":checked")) {				
		$( ".audi-chk" ).prop( "checked", false );	
		$("#otherAudience").hide();	
	} 		
});

$("#is_agree").click(function () {		   
	if($(this).is(":checked")) {					
		$("#show-text").show();	
		} else {
		$("#show-text").hide();	
	}		
});

//fund-reward //fund_agree //fund_reward_div
$(".fund-chk").click(function () {		   
	if($(this).is(":checked")) {				
		//$( ".audi-chk" ).prop( "checked", false );	
		$("#fund_agree").show();	
		} else {
		$("#fund_agree").hide();	
	}		
});

$(".fund-reward").click(function () {		   
	if($(this).is(":checked")) {				
		//$( ".audi-chk" ).prop( "checked", false );	
		$("#fund_reward_div").show();	
		} else {
		$("#fund_reward_div").hide();	
	}			
});

});
</script>
<script type="text/javascript">
	
	function getWordCount(wordString) {
	  var words = wordString.split(" ");
	  words = words.filter(function(words) { 
			return words.length > 0
		}).length;
	  return words;
	}
	
	$(document).ready(function () {
	
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod('filesize', function (value, element, param) {
	     	return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		/* $.validator.setDefaults({
			submitHandler: function () {
				form.submit();
			}
		}); */
		
		/*jQuery.validator.addMethod("accept", function(value, element, param) {
			return value.match(new RegExp("." + param + "$"));
			});
			rules: {
			fileupload: { accept: "(docx?|doc|pdf)" }
		}*/
		
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
			$( ".choose-tags" ).change(function() {	
				var values = $(this).val();	
				if(jQuery.inArray("0", values) !== -1){
					//console.log(values);
					// $('#tags_id').val('0');					
					// $("#tags_id").select2();
					$("#otherTags").show();	
					$("#other_tag").attr('required', true);
				} else {  //console.log("Other =="+values);
					$("#otherTags").hide();
					$("#other_tag").attr('required', false);
				}
			});
			
			$( ".choose-tech" ).change(function() {	
				var values = $(this).val();	
				if(jQuery.inArray("0", values) !== -1){
					//console.log(values);
					// $('#techonogy_id').val('0');				
					 //$("#techonogy_id").select2();
					$("#otherTech").show();	
					$("#other_technology").attr('required', true);
				} else {
					$("#otherTech").hide();
					$("#other_technology").attr('required', false);
				}
			});
			
			$("input.numbers").keypress(function(event) {
			  return /\d/.test(String.fromCharCode(event.keyCode));
			});
		
		
		//add the custom validation method
		jQuery.validator.addMethod("minCount",
		function(value, element, params) {
		  var count = getWordCount(value);
		  if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("A minimum of {0} words is required here.")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
		  var count = getWordCount(value);
		  console.log(count);
		  if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("A maximum of {0} words is required here.")
		);
		
		$.validator.addMethod("valid_email", function(value, element) 
		{ 
			var email = value;
			var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			var result = pattern.test(email);	
			if(result){return true;}else {return false;}
		});
		
		$.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
		}, "Letters and numbers only please");
		
		$('#challengeform').validate({
			rules: {
				
				banner_img: {
					valid_img_format: true,filesize:2000000
				},
				company_profile: {
					maxCount:['200']
				},
				launch_date: {
					required: true
				},
				close_date: {
					required: true
				},
				"techonogy_id[]": {
					required: true
				},
				other_technology:{
					required: true ,
					nowhitespace:true
				},
				"tags_id[]": {
					required: true
				},
				other_tag:{
					required: true,
					nowhitespace:true,
					maxlength:50	
				},
				"audi_id[]": {
					required: true
				},
				other_audience:{
					required: true 
				},			
				contact_name: {
					required: true,
					maxCount:['100']
				},			 
				mobile: {
					required: true,
					number: true,
					minlength:10,
					maxlength:11
				},
				contact_email: {
					required: true,
					email: true,
					valid_email:true
				},
				office_no: {
					required: true,
					number: true
				},
				visibility: {
					required: true
				},            
				/*is_external_funding: {
					required: true
				},*/
				funding_amt: {
					required: true
				},
				is_exclusive_challenge: {
					required: true
				},
				challenge_ex_details: {
					required: true,
					maxCount:['100']
				}
			},
			messages: {   
				
				banner_img: {
					valid_img_format: "Please upload only image file"
				},
				company_profile: {
					required: "This field is required"
				},
				
				launch_date: {
					required: "This field is required"              
				},
				close_date: {
					required: "This field is required"
				},
				techonogy_id: {
					required: "This field is required"
				},
				other_technology: {
					required: "This field is required"
				},
				tags_id: {
					required: "This field is required"
				},
				other_tag:{
					required: "This field is required"
				},
				audi_id: {
					required: "This field is required"
				},
				other_audience: {
					required: "This field is required"
				},
				
				contact_name: {
					required: "This field is required"
				},
				mobile: {
					required: "This field is required"
				},
				contact_email: {
					required: "This field is required",
					valid_email: "Please enter valid email address"
				},
				office_no: {
					required: "This field is required"
				},			 
				visibility: {
					required: "This field is required"
				},            
				/*is_external_funding: {
					required: "This field is required"
				},*/
				funding_amt: {
					required: "This field is required",
					number: true
				},
				is_exclusive_challenge: {
					required: "This field is required"
				},
				challenge_ex_details: {
					required: "This field is required"
				}   
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
	
	$('.select2').select2({	});
	
	$(document).ready(function()
    {          
        $('input[type="file"]').change(function(e)
        {              
            var fileName = e.target.files[0].name;
            $(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
        });          
    });
</script>



