<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Closure Challenge</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Closure Challenge</li>
			</ol>
		</nav>
	</div>
</div> 
<?php
$challengeDecryption =  New Opensslencryptdecrypt();
 ?>
<div id="preloader-loader" style="display:none;"></div>
<section>
<form name="closureForm" id="closureForm" method="post" role="form" enctype="multipart/form-data">
   
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-6"> 
				<div class="formInfo">
					<div class="row">
						<div class="col-sm-12">										
												 
							<div class="form-group mt-4">															
								<input type="text" class="form-control video_link" id="youtube_link[0]" name="youtube_link[0]" value=""/>
								<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus" aria-hidden="true"></i></a>
								 <label class="form-control-placeholder" for="cname">Video<em>*</em></label>
							</div>
							<div class="field_wrapper">
							</div>	
							<button type="submit" class="btn btn-primary">Submit</button> 
						</div> 
					</div> 
				</div> 
			</div> 
		</div>
	</div>	
</form>
 </section>
 <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
 <script>
 
$(document).ready( function () {
		
	$.validator.addMethod("youtube", function(value, element) {
	 var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
	 return (value.match(p)) ? RegExp.$1 : false;
	}, "Please enter valid youtube url.");
	
	$.validator.addMethod("youtube_link", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } },'Please enter youtube link.');		
	$.validator.addClassRules("video_link", { youtube_link: true, youtube: true });
	
	
	var createValidation = function() {
	  $(".video_link").each(function() {
		$(this).rules('remove');
		$(this).rules('add', {
		  	
		  required: true,
		  messages: {			
			required: "This field is required"
		  }
		});
	  });
	}
	
	/*function createValidation(){
		
		function() {
		  $(".video_link").each(function() {
			$(this).rules('remove');
			$(this).rules('add', {		 
			  required: true,
			  messages: {			
				required: "Invalid Input"
			  }
			});
		  });
		}
		
	}*/
	
	//var emailCounter = 1;

	var validator = $("#closureForm").validate({
	  rules: {
		"youtube_link[]": "required"
	  },
	  messages: {
		"youtube_link[]": "This field is required"
	  }
	});
	
	// Multiple Input Fields For Youtube Link
	var maxField = 50; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    //var fieldHTML = '<div class="form-group mt-4"><input type="text" class="form-control video_link" id="youtube_link" name="youtube_link[]" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-remove" aria-hidden="true"></i></a><label class="form-control-placeholder" for="cname">Video<em>*</em></label></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
			//$('input').rules('add', 'required');	
            
            $(wrapper).append('<div class="form-group mt-4"><input type="text" class="form-control video_link" id="youtube_link[' + (x) + ']" name="youtube_link[' + (x) + ']" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-remove" aria-hidden="true"></i></a><label class="form-control-placeholder" for="cname">Video<em>*</em></label></div>'); //Add field html
			
			x++; //Increment field counter
			
			createValidation();
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
	
	// End For Video Link
	
}); // Document Event

// Kick validation
$(document).ready(function() {
  createValidation();
});
</script> 