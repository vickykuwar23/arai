<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<?php
$slotDecryption =  New Opensslencryptdecrypt();	
$positionshow = $this->master_model->getRecords("type_master", array('tid' => $slot_details[0]['slot_type']));
//echo $this->db->last_query();
$positionName = ucwords($positionshow[0]['name']);

//print_r($positionName);
$positionName = ucwords($positionshow[0]['name']);
$skillsets	= $slot_details[0]['skills'];
$span = '';
if($skillsets!=""){
												
	if($skillsets!="0"){
	
	$expSkillset = explode(",", $skillsets);
	
		foreach($expSkillset as $getSkillname){
			$getSkillDetails = $this->master_model->getRecords('skill_sets',array('id'=> $getSkillname));
			$get_skill_name = $slotDecryption->decrypt($getSkillDetails[0]['name']);
			$span .= '<span class="btn btn-info" style="margin: 5px 5px 5px 5px;">'.$get_skill_name.'</span>';
		}
	
	}
	
} 

 ?>
<div id="preloader-loader" style="display:none;"></div>
<section>
<form name="slot-application" id="slot-application" method="post" role="form">
    <div class="challenges-title">
      <div class="row">
        <div class="col-sm-12">
          <h3><a href="javascript:void(0);">Application Form For Team Slot Application</a></h3>
        </div>       
      </div>
    </div>
	<div class="container">
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><b>Apply For : </b> <?php echo $positionName ?></p>
			</div>
	  </div>
	  <div class="row">
			<div class="col-md-12"  style="width:100%;">
				<p><b>Skills Required : </b> </p>
				<p><?php echo $span ?></p>
			</div>
	  </div>
	  <div class="row">	  
		<div class="form-group" style="width:40%;">				
			<textarea rows="5" cols="12" name="intro_urself" id="intro_urself" class="form-control valid-new"  required=""></textarea>
			<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Introduce Yourself <em>*</em></label>
		</div>
	  </div>
	  <div class="row">
			<div class="form-group"
				<p><input type="checkbox" name="is_agree" id="is_agree" value="1" class="" /> <b>I give my concent to send above information to the challenge owner.</b> </p>
			</div>
	  </div>
	  <div class="col-md-12">
			<div id="remove-div">
				<a href="javascript:window.history.back()" class="btn btn-primary" >Cancel</a>
				<input type="submit" name="sub_btn" id="sub_btn" class="btn btn-success" value="Send Application" />
			</div>
	  </div>
	</div>
	<input type="hidden" name="team_id" id="team_id" value="<?php echo $slot_details[0]['team_id'] ?>" />
	<input type="hidden" name="ch_id" id="ch_id" value="<?php echo $slot_details[0]['c_id'] ?>" />
	<input type="hidden" name="slot_id" id="slot_id" value="<?php echo $slot_details[0]['slot_id'] ?>" />
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</form>
 </section>
 <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
 <script>
 
 // Word Count Validation
function getWordCount(wordString) {
	var words = wordString.split(" ");
	
	words = words.filter(function(words) { 
		return words.length > 0
	}).length;
	return words;
}

function capture_apply_slot(intro_urself, is_agree, team_id, ch_id, slot_id){
	
	swal(
		{
			title:"Confirmation:",
			text: "Are you sure?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
			//html:'You are requesting access to the owner details. '+'<input type="hidden" name="c_id" id="c_id" value="<?php echo $challenge_data[0]['c_id']  ?>" /><input type="hidden" name="o_id" id="o_id" value="<?php echo $challenge_data[0]['u_id']  ?>" /><input type="hidden" class="token" id="csrf_test_name" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash(); ?>" />'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();				
				 $.ajax({
					type:'POST',
					url: base_url+'challenge/slot_app_received',
					//data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,
					data:$('#slot-application').serialize(),					
					success:function(response){
						$("#preloader-loader").css("display", "none");
						var output = JSON.parse(response);
						var responsetype = output.success;
						var content_text = output.content_text;
						$(".token").val(output.token);
						if(responsetype == "success"){
							var alertContent = "Success!";
							var alerttype = "success";
						} else {
							var alertContent = "Warning!";
							var alerttype = "warning";
						}
						swal({
								title: alertContent,
								text: content_text,
								type: alerttype,
								icon: alerttype,
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						window.location.href = '<?php echo base_url('challenge/teamListing/'.base64_encode($slot_details[0]['c_id'])) ?>';	
					}
				});	// Ajax End
				
			}
		
		});
	
	
}

$(document).ready( function () {
	
	jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);			
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));	
		
		
	$("#slot-application" ).validate({			
			/*debug: true,
			 onkeyup: true,
			ignore: ':hidden:not("#resultId")',	*/
			ignore: ':hidden', 
			debug: false, 
			load: true,
			blur: true,
			change: true,
			keypress: true,
			keyup: true,
			keydown: true,
			onclick: true,
			rules: {
				intro_urself: {
					required: true,
					minCount:['3'],
					maxCount:['100']	
				},
				is_agree: {
					required: true	
				}
			},
			messages: {
				intro_urself: {
					required: "This field is required"
				},
				is_agree: {
					required: "This field is required"
				}
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {				
				//$("#preloader-loader").css("display", "block");
				var formData = $('#slot-application').serialize();
				var intro_urself = $("#intro_urself").val();
				var is_agree = $("#is_agree").val();
				var team_id = $("#team_id").val();
				var ch_id = $("#ch_id").val();
				var slot_id = $("#slot_id").val();
				capture_apply_slot(intro_urself, is_agree, team_id, ch_id, slot_id);
				/*$.ajax({
					url: "<?php echo base_url('challenge/slot_app_received'); ?>", 
					type: "POST",
					data:$('#slot-application').serialize(),
					cache: false,
					success: function(response) 
					{
						$("#preloader-loader").css("display", "none");
						var output = JSON.parse(response);
						var responsetype = output.success;
						var content_text = output.content_text;
						$(".token").val(output.token);
						if(responsetype == "success"){
							var alertContent = "Success!";
							var alerttype = "success!";
						} else {
							var alertContent = "Warning!";
							var alerttype = "warning";
						}
						swal({
								title: alertContent,
								text: content_text,
								type: alerttype,
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						
					}
				});*/
				return false;
			}
		});	
	
	
	$(".team-selector2").on("click", function () {	
		var team_status = $(this).attr('data-id');
		var team_id = $(this).attr('data-tid');
		var ch_id = $(this).attr('data-cid');
		//alert(team_status+"=="+team_id);
		
		swal(
		{
			title:"Confirmation:",
			text: "Are you sure?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{	
				//alert(team_status+"=="+team_id);return false;
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();							
				 $.ajax({
					type:'POST',
					url: base_url+'challenge/team_selection',
					data:'c_id='+ch_id+'&t_status='+team_status+'&t_id='+team_id+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);								
						$(".token").val(output.token);
						console.log(output);	
						var storetype = output.success;
						var textAlert = output.content_text;
						if(storetype == "success"){
							var alertContent = "Success!";
						} else {
							var alertContent = "Error!";
						}
						$('#remove-div').remove();
						swal({
								title: alertContent,
								text: textAlert,
								type: storetype,
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						
					}
				});	// Ajax End
				
			} // IF SWAL SUCCESS
		
		}); // TRUE
	}); // Click Event 
}); // Document Event
</script> 