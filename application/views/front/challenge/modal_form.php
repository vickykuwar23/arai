<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="SubscriptionModalLabel">Are you sure.?</h5>		
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="withdrawnForm" name="withdrawnForm" role="form">
		
		<div class="p-3">
			<p>You want to withdraw your challenge.</p>
			<div class="form-group" id="withdrawn-dropdown">				
				<select name="w_id" id="w_id" class="form-control withdraw-card" >
					<option value=""> -- Select Reason --</option>					
					<?php if(count($withdraw_reason) > 0): foreach($withdraw_reason as $res): ?>
					<option value="<?php echo $res['id'] ?>"><?php echo $res['reason'] ?></option>
					<?php endforeach; ?>
					<option value="other">Other</option>
					<?php else: ?>
					<?php endif; ?>
				</select>
			</div>			
			<div class="form-group">
				<div class="row">
					<div class="col-md-12">
						<div id="content-reason" style="display:none;"><textarea name="w_reason" placeholder="Enter Reason" class="form-control" id="w_reason" colspan="6" rowspan="4"></textarea></div>
					</div>
				</div>
			</div>
		<input type="hidden" name="c_id" id="c_id" value="<?php echo $cid; ?>"/>
		<div class="modal-footer" style="border-top:none; padding:0; justify-content:left">
			<button type="submit" class="btn btn-primary" id="email_subscription_btn_submit">Withdraw</button>
			<button type="button" class="btn btn-warning close_modal" name="close_modal" id="close_modal" data-dismiss="modal">Cancel</button>
		</div>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<script>
$(document).ready(function () {
	$(document).on('change','.withdraw-card',function(){	
		var getVal = this.value;
		if(getVal != ""){
			$('span#w_id-error').remove();
		}
		if(getVal == 'other'){
			$("#content-reason").show();
		} else {
			$("#content-reason").hide();
		}
	});
	
	$(document).on('click','.close_modal',function(){
		$('#withdrawnModal').modal('hide');
	});
	
	
	
	$( "#withdrawnForm" ).validate({
		onfocusout: false,
		onkeyup: false,
		onclick: false,
		/* debug: false, */
		rules: {
			w_id: {
				required: true
			},
			w_reason: {
				 required: function() {
					return $("#w_id").val() == 'other';  
				   } 
			}
			
		},
		messages: {
			w_id: {
				required: "This field is required"
			},
			w_reason: {
				required: "This field is required"
			}		
		},
		errorElement: 'span',
		   errorPlacement: function (error, element) {
			 error.addClass('invalid-feedback');
			 element.closest('.form-group').append(error);
		   },
		   highlight: function (element, errorClass, validClass) {
			 $(element).addClass('is-invalid');
		   },
		   unhighlight: function (element, errorClass, validClass) {
			 $(element).removeClass('is-invalid');
		   },
		submitHandler: function () {
			$("#preloader-loader").css("display", "block");
			//var withdraw_id 	= $(this).attr('');
			var withdraw_id 	= $('#withdraw_id').val();
			var reject_reason 	= $('#reject_reason').val();
			var c_id 			= $('#c_id').val();
			
			$.ajax({
				url: "<?php echo base_url('challenge/addWithdrawnReason'); ?>", 
				type: "POST",             
				//data: { "email" : email },
				data:$('#withdrawnForm').serialize(),
				dataType: 'JSON',
				cache: false,
				success: function(response) 
				{
					$("#withdrawnModal").modal('hide');
					$("#preloader-loader").css("display", "none");
					//var output = JSON.parse(response);		
					console.log(response.change_text);
					$('#remove-'+c_id).remove();
					$('#add-'+c_id).text(response.change_text);	
					swal({
							title: 'Success!',
							text: "Challenge successfully withdrawn.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				}
			});
			return false;
		}
	});

}); 
</script>