<?php								
                                 if(count($challenge_data) > 0 )
                                 {
                                 	foreach($challenge_data as $challenge){
                                 			
                                 		$limit = 200;
                                 		 if (strlen($challenge['challenge_details'] ) > $limit){
                                 			$challenge['challenge_details']  = substr($challenge['challenge_details'] , 0, strrpos(substr($challenge['challenge_details'] , 0, $limit), ' ')) . '...'; 
                                 		 }
                                 		 
                                 		 $limit2 = 48;
                                 		 if (strlen($challenge['challenge_title'] ) > $limit2){
                                 			$challenge['challenge_title']  = substr($challenge['challenge_title'] , 0, strrpos(substr($challenge['challenge_title'] , 0, $limit2), ' ')) . '...'; 
                                 		 }
                                 		
                                 		
                                 		if($challenge['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $challenge['challenge_close_date'])
                                 		{
                                 								
                                 			$date1=date_create(date("Y-m-d"));
                                 			$date2=date_create($challenge['challenge_close_date']);
                                 			$diff=date_diff($date1,$date2);
                                 			$showCnt = "Days left : ".($diff->format("%a")+1);							
                                 		} 
                                 		else 
                                 		{
                                 			$showCnt = "Closed Participations";
                                 		}
                                 		$fileExist = base_url('assets/challenge/'.$challenge['banner_img']);
                                 		 if ($challenge['banner_img']!="") {
                                 			$image = $fileExist;
                                 		} else {
                                 			$image = base_url('assets/news-14.jpg');
                                 		}
                                 		
                                 ?>
                              <div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" onclick="window.location.href='<?php echo base_url('challenge/challengeDetails/'.base64_encode($challenge['c_id'])); ?>'">
                                 <div class="desc-comp-offer-cont">
                                    <a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($challenge['c_id'])); ?>" >
                                       <div class="thumbnail-blogs">
                                          <div class="caption">
                                             <i class="fa fa-chain"></i>
                                          </div>
                                          <img src="<?php echo $image; ?>" class="img-fluid" alt="<?php echo $challenge['challenge_title'] ?>" style="">
                                       </div>
                                    </a>
                                    <div class="card-content">
                                       <h5><?php echo $challenge['company_name'] ?></h5>
                                       <h3><?php echo $challenge['challenge_title'] ?></h3>
                                       <p class="desc"><?php echo $challenge['challenge_details'] ?>
                                          <a href="javascript:void(0);" class="show_hide_new">Read More</a>
                                       </p>
                                       <div class="cardd-footer content-new ">
                                          <div>
                                             <?php 
                                                if($challenge['audience_pref']!= ''){	
                                                $audience = explode(",", $challenge['audience_pref']);
                                                foreach($audience as $audi){
                                                ?>
                                             <button class="btn btn-general btn-green-fresh remove-h" role="button"><?php echo $audi; ?></button>
                                             <?php } } else { ?>
                                             <button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>
                                             <?php } ?>
                                          </div>
                                          <div class="daysleft"><i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php } // Foreach End ?>	

                              <?php  if($new_start < count($total)){ ?>
                                 <div class="col-md-12 text-center" id="showMoreBtn">
                                 <a class="how_more btn btn-general btn-white" role="button" href="javascript:void(0)" onclick="getChallengeDataAjax('<?php echo $new_start;?>','<?php echo $nf_limit ?>',0,1)" >Show more</a>
                                 </div>
                              <?php } ?>
                              <?php	} else { // If End ?>
                                 <div class="col-md-12 text-center" >
                                 No Record Found
                                 </div>
                              <?php } ?>