<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">My Challenges</h1>        
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                 <div class="formInfo">
					
                    <div class="row">
                        <div class="col-md-4">                            
						<div class="form-group">
							
							<select class="form-control select2" id="techonogy_id" name="techonogy_id[]" multiple="multiple">
							  <option value=""></option>
								<?php foreach($technology_data as $techname){ ?>
								<option value="<?php echo $techname['id'] ?>" ><?php echo $techname['technology_name'] ?></option>								  
								<?php  } ?>								
							</select>	
							<label class="form-control-placeholder leftLable2">Technology Field </label>						
						</div>
                     </div>
					<div class="col-md-4">
						<div class="form-group">
							
							<select id="change_visibility" name="change_visibility"  class="form-control select2">                           
								<option value=""></option>
								<?php 
									foreach($challengetype as $visibility){
								?>
								<option value="<?php echo $visibility['challenge_type'] ?>"><?php echo $visibility['challenge_type'] ?></option>
								<?php } ?>
							</select>	
							<label class="form-control-placeholder leftLable2" >Challenge Visibility</label>					
						</div> 
					</div> 
					<div class="col-md-4">
						<div class="form-group">
							
							<select id="c_status" name="c_status"  class="form-control select2">                           
								<option value=""></option>
								<option value="Approved">Approved</option>								
								<option value="Closed">Closed</option>
								<option value="Pending">Pending</option>								
								<option value="Rejected">Rejected</option>
								<option value="Withdrawn">Withdrawn</option>
							</select>	
							<label class="form-control-placeholder leftLable2" for="technology">Challenge Status</label>					
						</div> 
					</div> 
					<div class="col-md-4">
						<div class="form-group">
							
							<select id="audience_pref" name="audience_pref[]" multiple="multiple" class="form-control select2">                           
									<option value=""></option>
								<?php 
									foreach($audience_data as $audi){
								?>
									<option value="<?php echo $audi['id'] ?>"><?php echo $audi['preference_name'] ?></option>
								<?php } ?>
							</select>	
							<label class="form-control-placeholder leftLable2" for="technology">Audience Preference</label>					
						</div> 
					</div>
					
					<div class="col-md-4 mt-3">
						<button type="submit" id="btnReset" class="btn btn-secondary">Clear All </button>
						<button type="submit" id="btn-click" class="btn btn-primary btn-click">Apply</button>
					</div>
					
                 </div>
				
                 <div class="row mt-12">
                    <div class="col-md-12">
					<?php /*if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
						 <?php echo $this->session->flashdata('success'); ?>
						</div>
					   <?php }*/  ?>
                        <div class="table-responsive">
                        <table id="challenge-list" class="table table-bordered table-hover challenges-listing-page">
                            <thead>
                              <tr>
								<th scope="col">ID</th>
								<th scope="col">Challenge Title</th>
                                <th scope="col">Launch Date</th>
                                <th scope="col">Close Date</th>
                                <th scope="col">Funding</th>
                                <th scope="col">Applications Received</th>
								<th scope="col">Applications Approved</th>
								<th scope="col">Challenge Status</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>							
                            </tbody>
                          </table>
                        </div>
					</div>
                 </div>    
                </div>    
            </div>
        </div>
        </div>
    </section>
<div class="modal fade" id="withdrawnModal" tabindex="-1" role="dialog" aria-labelledby="withdrawnModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_withdrawn">
	</div>
</div>	
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script type="text/javascript">      
function updateChallengeStatus(id)
{
	$('#preloader-loader').css('display', 'block');	
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('challenge/open_modal_ajax'); ?>",
		data: 'cid='+id,
		cache: false,
		success:function(data)
		{
			$('#preloader-loader').css('display', 'none');	
			if(data == "error")
			{
				location.reload();
			}
			else
			{
				$("#model_withdrawn").html(data);
				$("#withdrawnModal").modal('show');
			}
		}
	});
}
</script>	
<script>

/*function updateChallengeStatus(c_id){	
	
		var base_url = '<?php echo base_url() ?>';
		$.ajax({
				type:'POST',
				url: base_url+'challenge/getWithdrawnlist',
				dataType:"html",
				success:function(data){	
					console.log(data);
					var output = JSON.parse(data);
					//console.log(output.select_box);
					$("#withdrawn-dropdown").html(output.select_box);
					
				}
			});
		
		swal(
		{
			title:"Are you sure?",
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Withdrawn',
			html:'<form method="post" name="w-form" id="w-form"><div><b>Please Select Challenge Withdrawn Reason</b></div><br /><div id="withdrawn-dropdown"></div><br /><div id="content-reason" style="display:none;"><textarea name="reject_reason" placeholder="Enter Reason" class="form-control" id="reject_reason" colspan="6" rowspan="4"></textarea></div></form>',
			
		}).then(function (result) 
		{
			if (result.value) 
			{
				$('#preloader-loader').css('display', 'block');	
				var base_url 		= '<?php echo base_url() ?>';
				var cs_t 			= 	$('.token').val();
				var withdraw_id 	= $("#withdraw_id").val();
				var reject_reason 	= $("#reject_reason").val();
				 $.ajax({
					type:'POST',
					url: base_url+'challenge/addWithdrawnReason',
					data:'c_id='+c_id+'&csrf_test_name='+cs_t+'&w_id='+withdraw_id+'&w_reason='+reject_reason,				
					dataType:"text",
					success:function(data){ 
						$("#preloader-loader").css("display", "none");
						var output = JSON.parse(data);						
						$(".token").val(output.token);
						var replaceText = output.change_text;
						$('#remove-'+c_id).remove();
						$('#add-'+c_id).text(replaceText);						
						//window.location.reload();
						swal({
								title: 'Success!',
								text: "Challenge successfully withdrawn.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
					}
				});
				
			}
		
		});
	
}*/

  $(document).ready( function () {


		$(document).on('change','.withdraw-card',function(){	
			var getVal = this.value;
			if(getVal == 'other'){
				$("#content-reason").show();
			} else {
				$("#content-reason").hide();
			}
		});
		
		var base_path = '<?php echo base_url() ?>';
		var table = $('#challenge-list').DataTable({
			
			"ordering":false,
			"searching": false,
			"language": {
			"zeroRecords":"No matching records found.",
			"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"challenge/my_clist",
			"type":"POST",
			"data":function(data) {	
				
					data.techonogy_id 			= $("#techonogy_id").val();
					data.change_visibility 		= $("#change_visibility").val();
					data.c_status 				= $("#c_status").val();
					data.audience_pref 			= $("#audience_pref").val();
					//data.ip_clause 				= $("#ip_clause").val();
					//data.fund_sel 				= $("#fund_sel").val();
					//data.reward_sel 			= $("#reward_sel").val();
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
		});
		
		// Withdrawn Challenge
		/*$(document).on('click','.withdrawn-challenge',function(){	
			var cid = $(this).attr('data-id');
			var base_path = '<?php echo base_url() ?>';
			var cs_t = 	$('.token').val();
			//alert(cid);
			$.ajax({
				type:'POST',
				url: base_path+'challenge/withdrawChallenge',
				data:'id='+cid+'&csrf_test_name='+cs_t,				
				dataType:"text",
				success:function(data){ //console.log(data);
					$("#preloader").css("display", "none");
					var output = JSON.parse(data);						
					$(".token").val(output.token);
					$("#ch-status-"+cid).remove();
					//$("#ch-status-"+ID).html(status);
					swal({
							title: 'Success!',
							text: "Challenge successfully withdrawn.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				}
			});
		});*/
		
		

		$('.btn-click').click(function(){
			var techonogy_id 		= $('#techonogy_id').val();
			var change_visibility 	= $('#change_visibility').val();	
			var ip_clause 			= $('#ip_clause').val();
			var fund_sel 			= $('#fund_sel').val();
			var reward_sel 			= $('#reward_sel').val();
			table.ajax.reload();		
		});
		
		$("#btnReset").on("click", function () {
			//$("#techonogy_id").select2('val', '');
			//$("#audience_pref").select2('val', '');
			//$("#c_status").select2('val', ''); 
			//$("#change_visibility").select2('val', '');
			$("#techonogy_id").val([]).change();
			$("#audience_pref").val([]).change();
			$("#c_status").val([]).change();
			$("#change_visibility").val([]).change();
			//$('select').prop('selectedIndex', 0);			
			table.ajax.reload();	
		});
		
	
	$('.select2').select2({});

});



$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>