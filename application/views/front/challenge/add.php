<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Add Challenge </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Challenge</a></li>
				<li class="breadcrumb-item active" aria-current="page">Add Challenge </li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div> 

<style>
.datepicker table tr td, .datepicker table tr th 
{
    border-radius: 0;
    border: 1px solid rgba(0,0,0,0.1);
}

.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover 
{
    background: rgba(0,0,0,0.1);
    color: rgba(0,0,0,0.4);
    cursor: not-allowed;
    border: 1px solid #fff;
}

th.datepicker-switch, th.next, th.prev 
{
    border: none !important;
}

input[type=text]:disabled {  
  cursor: not-allowed;
}

/*#challengeform .btn-group.sw-btn-group { position: relative; }
#challengeform .btn-group.sw-btn-group .sw-btn-next { position: absolute; right: 135px; }
#challengeform .btn-group.sw-btn-group-extra { position: absolute; right: 0; }
#challengeform .btn-group.sw-btn-group-extra .draft-btn { border-radius: 5px; margin-left:5px; }*/
</style>

<?php 
	if($orgName!="")
	{
		$companyName = $orgName;
		} else {
		$companyName = set_value('company_name');
	}
	
	if($email!="")
	{
		$email_address = $email;
		} else {
		$email_address = set_value('contact_email');
	}
	
	if($fullname!="")
	{
		$contact_name = $fullname;
		} else {
		$contact_name = set_value('contact_name');
	}

//echo "<pre>Vicky";print_r($draft_data);
	
	//Technology
	$arrTech = array();
	if(isset($draft_data[0]['technology_id']) && $draft_data[0]['technology_id']!=""){
		
		$explodes = explode(",",$draft_data[0]['technology_id']);
		foreach($explodes as $ids){
			array_push($arrTech, $ids);
		}
		
	}
	
	// TagsDetail
	$arrTags = array();
	if(isset($draft_data[0]['tags_id']) && $draft_data[0]['tags_id']!=""){
		$exp = explode(",",$draft_data[0]['tags_id']);
		foreach($exp as $tid){
			array_push($arrTags, $tid);
		}
	}
	
	// Audience
	$arrPref = array();
	if(isset($draft_data[0]['audience_pref_id']) && $draft_data[0]['audience_pref_id']!=""){
		$prefID = explode(",",$draft_data[0]['audience_pref_id']);
		foreach($prefID as $t_id){
			array_push($arrPref, $t_id);
		}
	}
	
	$arrDomain = array();
	if(isset($draft_data[0]['domain_id']) && $draft_data[0]['domain_id']!=""){
		$domainCapture = explode(",",$draft_data[0]['domain_id']);
		foreach($domainCapture as $d_id){
			array_push($arrDomain, $d_id);
		}
	}
	
	
	$arrGeo = array();
	if(isset($draft_data[0]['geographical_id']) && $draft_data[0]['geographical_id']!=""){
		$geographicalCapture = explode(",",$draft_data[0]['geographical_id']);
		foreach($geographicalCapture as $g_id){
			array_push($arrGeo, $g_id);
		}
	}

?>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					<?php echo validation_errors(); ?>
					<?php if( $this->session->flashdata('success')){ ?>
						<div class="alert alert-success alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php }  ?>
					<?php if( $this->session->flashdata('error')){ ?>
						<div class="alert alert-error alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						  <h5><i class="icon fas fa-check"></i> Success!</h5>
							<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php }  ?> 
					
					
					<form method="POST" id="challengeform" name="challengeform" enctype="multipart/form-data">
						<div id="smartwizard_add_challenge">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
								<li><a href="#step-4">Step 4<br /><small></small></a></li>
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<input type="hidden" name="update_id" id="update_id" value="<?php if(isset($draft_data[0]['c_id'])) { echo ($draft_data[0]['c_id']) ?  $draft_data[0]['c_id'] :  null; } else { echo ''; } ?>" />
									<div class="row">					 
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="challenge_title" id="challenge_title"  value="<?php if(isset($draft_data[0]['challenge_title'])) { echo ($draft_data[0]['challenge_title']) ?  $draft_data[0]['challenge_title'] :  set_value('challenge_title'); } else { echo set_value('challenge_title'); } ?>">
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Challenge Title </label>
												<span><?php echo form_error('challenge_title'); ?></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="company_name" id="company_name"  value="<?php if(isset($draft_data[0]['company_name'])) { echo ($draft_data[0]['company_name']) ?  $draft_data[0]['company_name'] :  $companyName; } else { echo $companyName; } ?>">
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Company Name </label>
												<span><?php echo form_error('company_name'); ?></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Challenge Banner </button>
												<input type="file" class="form-control" name="banner_img" id="banner_img"  value="">
												<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
											</div>											
										</div>	
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Company Logo</button>
												<input type="file" class="form-control" name="comp_logo" id="comp_logo"  value="">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="company_profile" id="company_profile" class="form-control" cols="12" rows="4"><?php if(isset($draft_data[0]['company_profile'])) { echo ($draft_data[0]['company_profile']) ?  $draft_data[0]['company_profile'] :  set_value('company_profile'); } else { echo set_value('company_profile'); } ?></textarea>
												<label class="form-control-placeholder" for="cname">Company Profile <em></em></label>
												<span><?php echo form_error('company_name'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="challenge_details" id="challenge_details" class="form-control" cols="12" rows="4"><?php if(isset($draft_data[0]['challenge_details'])) { echo ($draft_data[0]['challenge_details']) ?  $draft_data[0]['challenge_details'] :  set_value('challenge_details'); } else { echo set_value('challenge_details'); } ?></textarea>
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Brief Info About Challenge </label>
												<span><?php echo form_error('challenge_details'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="company_details" id="company_details" class="form-control" cols="12" rows="4"><?php if(isset($draft_data[0]['challenge_abstract'])) { echo ($draft_data[0]['challenge_abstract']) ?  $draft_data[0]['challenge_abstract'] :  set_value('company_details'); } else { echo set_value('company_details'); } ?></textarea>
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Challenge Abstract </label>
												<span><?php echo form_error('company_details'); ?></span>
											</div>
										</div>
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group"> 
												<input type="text" class="form-control datepicker" name="launch_date" id="launch_date" value="<?php if(isset($draft_data[0]['challenge_launch_date'])) { echo ($draft_data[0]['challenge_launch_date']) ?  $draft_data[0]['challenge_launch_date'] :  set_value('launch_date'); } else { echo set_value('launch_date'); } ?>" readonly />
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Launch Date </label>
												<span><?php echo form_error('launch_date'); ?></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="close_date" id="close_date" value="<?php if(isset($draft_data[0]['challenge_close_date'])) { echo ($draft_data[0]['challenge_close_date']) ?  $draft_data[0]['challenge_close_date'] :  set_value('close_date'); } else { echo set_value('close_date'); } ?>" readonly />
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Closing Date </label>
												<span><?php echo form_error('close_date'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-placeholder leftLable2" for="cname"><em style="color:red;">*</em> Technology </label>
												<select class="form-control choose-tech select2" id="techonogy_id" name="techonogy_id[]" multiple="multiple">
													
													<?php foreach($technology_data as $tech){ ?>
														<option value="<?php echo $tech['id'] ?>" <?php if(in_array($tech['id'], $arrTech)): ?> selected="selected" <?php endif; ?>><?php echo $tech['technology_name'] ?></option>								  
													<?php  } ?>
													<option value="0">Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('techonogy_id'); ?></span>
											</div>
										</div>
										<div class="col-md-12" id="otherTech2">
											<div class="form-group">
												<input type="text" class="form-control" name="other_technology" id="other_technology"  value="<?php if(isset($draft_data[0]['other_techonology'])) { echo ($draft_data[0]['other_techonology']) ?  $draft_data[0]['other_techonology'] :  set_value('other_technology'); } else { echo set_value('other_technology'); } ?>"  disabled >
												<label class="form-control-placeholder" for="cname"><em style="color:red;"></em> Other Technology</label>
												<span>Please enter comma seprated technology</span>
												<span><?php echo form_error('other_technology'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-placeholder leftLable2 " for="cname"><em style="color:red;">*</em> Tags </label>
												<select class="form-control choose-tags select2" id="tags_id" name="tags_id[]" multiple="multiple">
													
													<?php foreach($tag_data as $tag){ ?>
														<option value="<?php echo $tag['id'] ?>" <?php if(in_array($tag['id'], $arrTags)): ?> selected="selected" <?php endif; ?>><?php echo $tag['tag_name'] ?></option>								  
													<?php  } ?>
													<option value="0">Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('tags_id'); ?></span>
											</div>
										</div>
										<div class="col-md-12" id="otherTags2"><br />
											<div class="form-group">
												<input type="text" class="form-control" name="other_tag" id="other_tag"  value="<?php if(isset($draft_data[0]['added_tag_name'])) { echo ($draft_data[0]['added_tag_name']) ?  $draft_data[0]['added_tag_name'] :  set_value('other_tag'); } else { echo set_value('other_tag'); } ?>" disabled>
												<label class="form-control-placeholder" for="cname"><em style="color:red;"></em> Other Tag</label>
												<span>Please enter comma separated tags</span>
												<span><?php echo form_error('other_tag'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Audience Preference  </label><br />
											<div class="form-group">
												<?php foreach($audi_data as $audience){ ?>
													<div style="float: left;">
														<div class="form-check form-check-inline">
															<input type="checkbox" class="chk" name="audi_id[]" id="audi_id" value="<?php echo $audience['id'] ?>" <?php if(in_array($audience['id'], $arrPref)): ?> checked="checked" <?php endif; ?>>
															&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> <?php echo $audience['preference_name'] ?></label>
														</div>									  
													</div>
												<?php  } ?>
												<!--<div style="float: left;">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="audi-chk" name="audi_id[]" id="audi_id" value="0" >
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> Other</label>
													</div>									  
												</div>	-->	

												<div style="float: left;">
													<div class="form-check form-check-inline">
														<input type="checkbox" class=""  id="checkAll"  >
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> All</label>
													</div>									  
												</div>	
												<span><?php echo form_error('audi_id'); ?></span>
											</div>
											
										</div>
										<div class="col-md-12" id="otherAudience"><br />
											<div class="form-group">
												<input type="text" class="form-control" name="other_audience" id="other_audience"  value="<?php echo set_value('other_technology'); ?>">
												<label class="form-control-placeholder" for="cname"></label>
												<span><?php echo form_error('other_audience'); ?></span>
											</div>
										</div>							
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-label" for="ip_clause" style="font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;">Funding & Rewards</label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-chk" value="Funding" name="if_funding" id="if_funding" <?php if(set_value('if_funding') == 'Funding' || (isset($draft_data[0]['if_funding']) && $draft_data[0]['if_funding'] == 'Funding')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1"> Funding</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-reward" value="Reward" name="if_reward" id="if_reward" <?php if(set_value('if_reward') == 'Reward' || (isset($draft_data[0]['if_reward']) && $draft_data[0]['if_reward'] == 'Reward')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2"> Reward</label>
													</div>
												</div>
												<span><?php //echo form_error('if_funding'); ?></span>
											</div>
										</div>
										<div class="col-md-6" id="fund_agree2">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="is_amount" id="is_amount"  value="<?php if(isset($draft_data[0]['is_amount'])) { echo ($draft_data[0]['is_amount']) ?  $draft_data[0]['is_amount'] :  set_value('is_amount'); } else { echo set_value('is_amount'); } ?>" disabled>
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Funding Amount(in INR) </label>
												<span><?php echo form_error('is_amount'); ?></span>
											</div>
										</div>
										<div class="col-md-6" id="fund_reward_div2">
											<div class="form-group">
												<input type="text" class="form-control" name="fund_reward" id="fund_reward" value="<?php if(isset($draft_data[0]['fund_reward'])) { echo ($draft_data[0]['fund_reward']) ?  $draft_data[0]['fund_reward'] :  set_value('fund_reward'); } else { echo set_value('fund_reward'); }  ?>" disabled>
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Reward Details  </label>
												<span><?php echo form_error('fund_reward'); ?></span>
											</div>
										</div>
										<div class="col-md-6" >
											<div class="form-group">
												<label  class="form-control-placeholder leftLable2" for="cname"><em style="color:red;">*</em> Expected TRL Solution </label>
												<select class="form-control select2" id="trl_solution" name="trl_solution" autocomplete="nope">
													<option value=""></option>
													<?php foreach($trl_data as $trl){ ?>
														<option value="<?php echo $trl['id'] ?>" <?php if((set_value('trl_solution') == $trl['id']) || (isset($draft_data[0]['trl_id']) && $draft_data[0]['trl_id'] == $trl['id'])): ?> selected="selected" <?php endif; ?>><?php echo $trl['trl_name'] ?></option>								  
													<?php  } ?>
												</select>
												<span><?php echo form_error('trl_solution'); ?></span>
											</div>
										</div>
									</div>
								</div>							
								
								<div id="step-3" class="mt-4">
									<div class="row">
										<div class="col-md-12" >	
											<div class="title-box">
												<div class="title"><b>Eligibility Expectations</b></div>                  
											</div>
										</div>
										<hr />
										<div class="col-md-12" >
											<div class="form-group">
												<input type="text" class="form-control" name="educational" id="educational"  value="<?php if(isset($draft_data[0]['education'])) { echo ($draft_data[0]['education']) ?  $draft_data[0]['education'] :  set_value('educational'); } else { echo set_value('educational'); }?>">
												<label class="form-control-placeholder" for="cname">Educational  <em></em></label>
											</div>
										</div>
										<div class="col-md-3" >
											<div class="form-group">
												<label class="form-control-placeholder leftLable2" for="cname">From Age <em></em></label>									
												<select class="form-control select2" id="from_age" name="from_age" autocomplete="nope">
													<option value=""></option>
													<?php
														for($a=10;$a<=80;$a++){
														?>
														<option value="<?php echo $a; ?>" <?php if(isset($draft_data[0]['from_age']) && $draft_data[0]['from_age'] == $a): ?> selected="selected" <?php endif; ?>><?php echo $a; ?></option>		
														<?php
														}
													?>
												</select>
											</div>
										</div>
										<div class="col-md-3" >
											<div class="form-group">		
												<label  class="form-control-placeholder leftLable2" for="cname">To Age <em></em></label>
												<select class="form-control select2" id="to_age" name="to_age" autocomplete="nope">
													<option value=""></option>
													<?php
														for($f=10;$f<=80;$f++){
														?>
														<option value="<?php echo $f; ?>" <?php if(isset($draft_data[0]['to_age']) && $draft_data[0]['to_age'] == $f): ?> selected="selected" <?php endif; ?>><?php echo $f; ?></option>		
														<?php
														}
													?>
												</select>
											</div>
										</div>
										<div class="col-md-6" >
											<div class="form-group">
												<label  class="form-control-placeholder leftLable2" for="cname"><em style="color:red;">*</em> Select Domain </label>
												<select class="form-control select2" id="domain" name="domain[]" autocomplete="nope" multiple="multiple">
													<option value=""></option>
													<?php foreach($domain_data as $domain_detail){ ?>
														<option value="<?php echo $domain_detail['id'] ?>" <?php if(in_array($domain_detail['id'], $arrDomain)): ?> selected="selected" <?php endif; ?>><?php echo $domain_detail['domain_name'] ?></option>								  
													<?php  } ?>
												</select>
												<span><?php //echo form_error('trl_solution'); ?></span>									
											</div>
										</div>
										<div class="col-md-6" >							
											<div class="form-group">
												<label  class="form-control-placeholder leftLable2" for="cname">Geographical (States) <em></em></label>
												<select class="form-control select2" id="geographical" name="geographical[]" autocomplete="nope" multiple="multiple">
													
													<?php foreach($state as $state_detail){ ?>
														<option value="<?php echo $state_detail['id'] ?>" <?php if(in_array($state_detail['id'], $arrGeo)): ?> selected="selected" <?php endif; ?>><?php echo $state_detail['name'] ?></option>								  
													<?php  } ?>
												</select>
												<span><?php //echo form_error('trl_solution'); ?></span>									
											</div>
										</div>
										<div class="col-md-3" >
											<div class="form-group">
												<label  class="form-control-placeholder leftLable2" for="cname"><em style="color:red;">*</em> Minimum Team </label>
												<select class="form-control select2" id="min_team" name="min_team" autocomplete="nope">
													<option value=""></option>
													<?php
														for($t=1;$t<=10;$t++){
														?>
														<option value="<?php echo $t; ?>" <?php if(isset($draft_data[0]['min_team']) && $draft_data[0]['min_team'] == $t): ?> selected="selected" <?php endif; ?>><?php echo $t; ?></option>		
														<?php
														}
													?>
												</select>
												<span><?php echo form_error('min_team'); ?></span>
											</div>
										</div>
										<div class="col-md-3" >
											<div class="form-group">
												<label  class="form-control-placeholder leftLable2" for="cname"><em style="color:red;">*</em> Maximum Team </label>
												<select class="form-control select2" id="max_team" name="max_team" autocomplete="nope">
													<option value=""></option>
													<?php
														for($m=1;$m<=10;$m++){
														?>
														<option value="<?php echo $m; ?>" <?php if(isset($draft_data[0]['max_team']) && $draft_data[0]['max_team'] == $m): ?> selected="selected" <?php endif; ?>><?php echo $m; ?></option>		
														<?php
														}
													?>
												</select>
												<span><?php echo form_error('max_team'); ?></span>
											</div>
										</div>										
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="challenge_terms" id="challenge_terms" class="form-control" cols="12" rows="4"><?php if(isset($draft_data[0]['terms_txt'])) { echo ($draft_data[0]['terms_txt']) ?  $draft_data[0]['terms_txt'] :  set_value('challenge_terms'); } else { echo set_value('challenge_terms'); } ?></textarea>
												<label class="form-control-placeholder" for="cname">Additional Terms & Conditions (if any) </label>
												<span><?php //echo form_error('challenge_terms'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">									
												<div class="">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="" name="is_agree" id="is_agree" value="1" <?php if((set_value('is_agree')) || (isset($draft_data[0]['is_agree']) && $draft_data[0]['is_agree'] == 1)){ ?> checked="checked" <?php } ?>>
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1"> <a href="<?php echo base_url('home/termsOfuse'); ?>" target="_blank"><em>*</em> Agree terms and conditions</a> </label>
													</div>									  
												</div>
												<span><?php echo form_error('is_agree'); ?></span>
											</div>
										</div>
										<!--<div class="col-md-12" id="show-text">
											<div class="form-group">
											<input type="text" class="form-control" name="terms_txt" id="terms_txt"  value="<?php echo set_value('terms_txt'); ?>">
											<label class="form-control-placeholder" for="cname">Additional <em></em></label>
											</div>
										</div>-->
									</div>
								</div>
								
								<div id="step-4" class="mt-4">
									<div class="row">										
										<div class="col-md-12" >
											<div class="form-group">
												<input type="text" class="form-control" name="contact_name" id="contact_name" value="<?php if(isset($draft_data[0]['contact_person_name'])) { echo ($draft_data[0]['contact_person_name']) ?  $draft_data[0]['contact_person_name'] :  $contact_name; } else { echo $contact_name; } ?>">
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Contact Person Name </label>
												<span><?php echo form_error('contact_name'); ?></span>
											</div>
										</div>
										
										<div class="col-md-12" >	
											<div class="title-box">
												<div class="title"><b>Contact Details </b></div>                  
											</div>
										</div>
										<hr />
										<div class="col-md-4" >
											<div class="form-group">
												<input type="email" class="form-control" name="contact_email" id="contact_email" value="<?php if(isset($draft_data[0]['email_id'])) { echo ($draft_data[0]['email_id']) ?  $draft_data[0]['email_id'] :  $email_address; } else { echo $email_address; } ?>">
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Email address  </label>
												<span><?php echo form_error('contact_email'); ?></span>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="form-group">
												<input type="text" class="form-control" name="mobile" maxlength="10" id="mobile" value="<?php if(isset($draft_data[0]['mobile_no'])) { echo ($draft_data[0]['mobile_no']) ?  $draft_data[0]['mobile_no'] :  set_value('mobile'); } else { echo set_value('mobile'); } ?>">
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Mobile Number </label>
												<span><?php echo form_error('mobile'); ?></span>
											</div>
										</div>
										<div class="col-md-4" >
											<div class="form-group">
												<input type="text" class="form-control" name="office_no" maxlength="11" id="office_no"  value="<?php if(isset($draft_data[0]['office_no'])) { echo ($draft_data[0]['office_no']) ?  $draft_data[0]['office_no'] : set_value('office_no'); } else { echo set_value('office_no'); } ?>" maxlength="11">
												<label class="form-control-placeholder" for="cname"><em style="color:red;">*</em> Office Number </label>
												<span><?php echo form_error('office_no'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">									
												<div class="">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="" name="details_share" id="details_share" value="1" <?php if(set_value('details_share') || (isset($draft_data[0]['share_details']) && $draft_data[0]['share_details'] == 1)){ ?> checked="checked" <?php } ?>>
														&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1" style="font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;"> Share Contact Details with Participants </label>
													</div>									  
												</div>
												<span><?php echo form_error('details_share'); ?></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label redioButon" for="ip_clause" style="font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;"><em style="color:red;">*</em> Challenge Visibility</label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input" value="Public" name="visibility" id="visibility" <?php if((set_value('visibility') == 'Public') || (isset($draft_data[0]['challenge_visibility']) && $draft_data[0]['challenge_visibility'] == 'Public')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label redioButon" for="techNovuus1" style="color: #000;"> Public</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input" value="Private" name="visibility" id="visibility" <?php if((set_value('visibility') == 'Private') || (isset($draft_data[0]['challenge_visibility']) && $draft_data[0]['challenge_visibility'] == 'Private')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label redioButon" for="techNovuus2" style="color: #000;"> Private</label>
													</div>
												</div>
												<span><?php echo form_error('visibility'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea name="future_opportunities" id="future_opportunities" class="form-control" cols="12" rows="4"><?php if(isset($draft_data[0]['future_opportunities'])) { echo ($draft_data[0]['future_opportunities']) ?  $draft_data[0]['future_opportunities'] : set_value('future_opportunities');  } else { echo set_value('future_opportunities'); } ?></textarea>
												<label class="form-control-placeholder" for="cname"> Future opportunities <em></em></label>
												<span><?php echo form_error('company_details'); ?></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label redioButon" for="ip_clause" style="font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;"><em style="color:red;">*</em> IP Clause</label>
												<div class="d-block">
													<?php foreach($ip_clause_data as $clause){ ?>
														<div class="form-check form-check-inline">
															<input type="radio" class="form-check-input" value="<?php echo $clause['id'] ?>" name="ip_cls" id="ip_cls" <?php if((set_value('ip_cls') == $clause['id']) || (isset($draft_data[0]['ip_clause']) && $draft_data[0]['ip_clause'] == $clause['id'])){ ?> checked="checked" <?php } ?>>
															<label class="form-check-label" for="techNovuus1" style="color: #000;"><?php echo $clause['ip_name'] ?></label>
														</div>
													<?php } ?>	
												</div>
												<span><?php echo form_error('ip_cls'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group form-check form-check-inline">
												<input type="checkbox" class="" name="is_external_funding" id="is_external_funding" value="1" <?php if((set_value('is_external_funding') == '1') || (isset($draft_data[0]['is_external_funding']) && $draft_data[0]['is_external_funding'] == 1)){ ?> checked="checked" <?php } ?>>
												&nbsp;&nbsp;<label class="textName" for="is_external_funding" style="margin:5px 0; font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;"> External funding required?  </label>
												<span><?php echo form_error('is_external_funding'); ?></span>
											</div>
										</div>
										<div class="col-md-12" id="external_fund2">
											<div class="form-group">
												<input type="text" class="form-control numbers" name="funding_amt" id="funding_amt"  value="<?php if(isset($draft_data[0]['external_fund_details'])) { echo ($draft_data[0]['external_fund_details']) ?  $draft_data[0]['external_fund_details'] : set_value('funding_amt');  } else { echo set_value('funding_amt'); } ?>" maxlength="2" disabled>
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Percentage Of Funding (%) </label>
												<span><?php echo form_error('funding_amt'); ?></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-label" for="ip_clause" style="font-size: 14px;color: rgb(51, 51, 51);font-weight: 500;"><em style="color:red;">*</em> Is this challenge exclusively listed on TechNovuus?</label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input chk-exclusive" value="1" name="is_exclusive_challenge" id="is_exclusive_challenge" <?php if((set_value('is_exclusive_challenge') == '1') || (isset($draft_data[0]['is_exclusive_challenge']) && $draft_data[0]['is_exclusive_challenge'] == '1')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1" style="color: #000;"> Yes</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="radio" class="form-check-input chk-exclusive" value="0" name="is_exclusive_challenge" id="is_exclusive_challenge" <?php if((set_value('is_exclusive_challenge') == '0') || (isset($draft_data[0]['is_exclusive_challenge']) && $draft_data[0]['is_exclusive_challenge'] == '0')){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2" style="color: #000;"> No</label>
													</div>
												</div>
												<span><?php echo form_error('is_exclusive_challenge'); ?></span>
											</div>
											<!--<div class="form-group">
												<input type="checkbox" class="chk-exclusive" name="is_exclusive_challenge" id="is_exclusive_challenge" value="1" <?php if(set_value('is_exclusive_challenge') == '1'){ ?> checked="checked" <?php } ?>>
												&nbsp;&nbsp;<label class="form-control-placeholder" for="cname"> Is this challenge exclusively listed on TechNovuus.? <em>*</em></label>
												<span><?php echo form_error('is_exclusive_challenge'); ?></span>
											</div>-->
										</div>
										<div class="col-md-12" id="excl_challenge2">
											<div class="form-group">
												<textarea name="challenge_ex_details" id="challenge_ex_details" class="form-control" cols="12" rows="4" disabled><?php if(isset($draft_data[0]['exclusive_challenge_details'])) { echo ($draft_data[0]['exclusive_challenge_details']) ?  $draft_data[0]['exclusive_challenge_details'] : set_value('challenge_ex_details'); } else { echo set_value('challenge_ex_details'); } ?></textarea>
												<label class="form-control-placeholder" for="cname"> <em style="color:red;">*</em> Challenge Exclusively Content </label>
												<span><?php echo form_error('challenge_ex_details'); ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function scroll_to_top(div_id)
	{
		if(div_id == '' || div_id==undefined) { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
	
	$(document).ready(function() 
	{
		//**** PREVENT FORM SUBMITTING WHEN PRESS ENTER ****
		$(window).keydown(function(event)
		{
			if(event.keyCode == 13) 
			{
				event.preventDefault();
				return false;
			}
		});
		
		//******* STEP SHOW EVENT *********
		$("#smartwizard_add_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') { $(".btnfinish").hide(); $(".sw-btn-next").show(); $("#prev-btn").addClass('disabled'); $(".btnfinish").addClass('d-none'); } 
			else if (stepPosition === 'final') { $(".sw-btn-next").hide(); $(".btnfinish").show(); $("#next-btn").addClass('disabled'); $(".btnfinish").removeClass('d-none'); } 
			else { $(".sw-btn-next").show(); $(".btnfinish").hide(); $("#prev-btn").removeClass('disabled'); $("#next-btn").removeClass('disabled'); }
		});
		
		//******* STEP WIZARD *********
		$('#smartwizard_add_challenge').smartWizard(
		{
			/* selected: 2, 
			 selected: 1,*/	
			//selected: 3,	
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */		
			keyNavigation: false,
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
					$('<button></button>').text('Submit')
					.addClass('btn btn-primary btnfinish')
					.on('click', function(e)
					{ 
						e.preventDefault();						
						var submit_flag = 0;
						
						if($("#challenge_ex_details").valid()==false) { submit_flag = 1; $("#challenge_ex_details").focus(); }
						if($("#is_exclusive_challenge").valid()==false) { submit_flag = 1; $("#is_exclusive_challenge").focus(); }
						if($("#funding_amt").valid()==false) { submit_flag = 1; $("#funding_amt").focus(); }
						if($("#is_external_funding").valid()==false) { submit_flag = 1; $("#is_external_funding").focus(); }
						if($("#ip_cls").valid()==false) { submit_flag = 1; $("#ip_cls").focus(); }
						//if($("#future_opportunities").valid()==false) { submit_flag = 1; $("#future_opportunities").focus(); }
						if($("#visibility").valid()==false) { submit_flag = 1; $("#visibility").focus(); }
						//if($("#details_share").valid()==false) { submit_flag = 1; $("#details_share").focus(); }
						if($("#office_no").valid()==false) { submit_flag = 1; $("#office_no").focus(); }
						if($("#mobile").valid()==false) { submit_flag = 1; $("#mobile").focus(); }
						if($("#contact_email").valid()==false) { submit_flag = 1; $("#contact_email").focus(); }
						if($("#contact_name").valid()==false) { submit_flag = 1; $("#contact_name").focus(); }	
						
						if($("#contact_name").valid()==false) { scroll_to_top('contact_name'); }
						else if($("#contact_email").valid()==false) { scroll_to_top('contact_email'); }
						else if($("#mobile").valid()==false) { scroll_to_top('mobile'); }
						else if($("#office_no").valid()==false) { scroll_to_top('office_no'); }
						else if($("#details_share").valid()==false) { scroll_to_top('details_share'); }
						else if($("#visibility").valid()==false) { scroll_to_top('visibility'); }
						else if($("#future_opportunities").valid()==false) { scroll_to_top('future_opportunities'); }
						else if($("#ip_cls").valid()==false) { scroll_to_top('ip_cls'); }
						else if($("#is_external_funding").valid()==false) { scroll_to_top('is_external_funding'); }
						else if($("#funding_amt").valid()==false) { scroll_to_top('funding_amt'); }
						else if($("#is_exclusive_challenge").valid()==false) { scroll_to_top('is_exclusive_challenge'); }
						else if($("#challenge_ex_details").valid()==false) { scroll_to_top('challenge_ex_details'); }
						
						if(submit_flag == 0)
						{
							swal(
							{
								title:"Submit",
								text: "Are you sure you want to submit the challenge?",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes!'
							}).then(function (result) { 
								if (result.value) { 
									$('#challengeform').submit(); 
								} 
							});
						}
					}),
					
					/*$('<button name="draft_btn" class="custom-tbn" value="Draft"></button>').text('Save To Draft').addClass('btn btn-danger draft-btn').on('click', function(){ 
						 
						e.preventDefault();						
						var submit_flag = 0;
						
						if($("#challenge_ex_details").valid()==false) { submit_flag = 1; $("#challenge_ex_details").focus(); }
						if($("#is_exclusive_challenge").valid()==false) { submit_flag = 1; $("#is_exclusive_challenge").focus(); }
						if($("#funding_amt").valid()==false) { submit_flag = 1; $("#funding_amt").focus(); }
						if($("#is_external_funding").valid()==false) { submit_flag = 1; $("#is_external_funding").focus(); }
						if($("#ip_cls").valid()==false) { submit_flag = 1; $("#ip_cls").focus(); }
						if($("#future_opportunities").valid()==false) { submit_flag = 1; $("#future_opportunities").focus(); }
						if($("#visibility").valid()==false) { submit_flag = 1; $("#visibility").focus(); }
						if($("#details_share").valid()==false) { submit_flag = 1; $("#details_share").focus(); }
						if($("#office_no").valid()==false) { submit_flag = 1; $("#office_no").focus(); }
						if($("#mobile").valid()==false) { submit_flag = 1; $("#mobile").focus(); }
						if($("#contact_email").valid()==false) { submit_flag = 1; $("#contact_email").focus(); }
						if($("#contact_name").valid()==false) { submit_flag = 1; $("#contact_name").focus(); }	
						
						if($("#contact_name").valid()==false) { scroll_to_top('contact_name'); }
						else if($("#contact_email").valid()==false) { scroll_to_top('contact_email'); }
						else if($("#mobile").valid()==false) { scroll_to_top('mobile'); }
						else if($("#office_no").valid()==false) { scroll_to_top('office_no'); }
						else if($("#details_share").valid()==false) { scroll_to_top('details_share'); }
						else if($("#visibility").valid()==false) { scroll_to_top('visibility'); }
						else if($("#future_opportunities").valid()==false) { scroll_to_top('future_opportunities'); }
						else if($("#ip_cls").valid()==false) { scroll_to_top('ip_cls'); }
						else if($("#is_external_funding").valid()==false) { scroll_to_top('is_external_funding'); }
						else if($("#funding_amt").valid()==false) { scroll_to_top('funding_amt'); }
						else if($("#is_exclusive_challenge").valid()==false) { scroll_to_top('is_exclusive_challenge'); }
						else if($("#challenge_ex_details").valid()==false) { scroll_to_top('challenge_ex_details'); }
												
						if(submit_flag == 0)
						{
							swal(
							{
								title:"Submit",
								text: "Are you sure you want to Save To Draft the challenge?",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes!'
							}).then(function (result) { if (result.value) { 
								//$('#challengeform').submit(); 
								//alert("Submit Your Challenge");
							} });
						}	
							
                         
					}),*/
				]
			}
		});
		
		$("#smartwizard_add_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{			
			
		
				var techSelectID = '<?php if(isset($draft_data[0]['technology_id'])) { echo $draft_data[0]['technology_id']; } else { echo ""; } ?>';
				var tagSelectID = '<?php if(isset($draft_data[0]['tags_id'])) { echo $draft_data[0]['tags_id']; } else { echo ""; } ?>';
				
				if(jQuery.inArray("0", techSelectID) !== -1){
					$( "#other_technology" ).prop( "disabled", false );	
					//$("#otherTech").show();						
				} else {
					$( "#other_technology" ).prop( "disabled", true );	
					//$("#otherTech").hide();					
				}

				if(jQuery.inArray("0", tagSelectID) !== -1){
					$("#other_tag").prop( 'disabled', false );	
					//$("#otherTags").show();					
				} else {
					$("#other_tag").prop( 'disabled', true );	
					//$("#otherTags").hide();					
				}

				var re_ward = $('input[name="if_reward"]:checked').val();
				if(re_ward == 'Reward'){ 
					$("#fund_reward").prop( 'disabled', false );		
					//$("#fund_reward_div").show();
				} else {
					$("#fund_reward").prop( 'disabled', true );	
					//$("#fund_reward_div").hide();
				}
				
				var funds_ward = $('input[name="if_funding"]:checked').val();
				if(funds_ward == 'Funding'){		
					$("#is_amount").prop( 'disabled', false );
					//$("#fund_agree").show();
				} else {
					$("#is_amount").prop( 'disabled', true );
					//$("#fund_agree").hide();
				}
		
		
				if($("#company_details").valid()==false) { isValidate= false; $("#company_details").focus(); }
				if($("#challenge_details").valid()==false) { isValidate= false; $("#challenge_details").focus(); }
				if($("#company_profile").valid()==false) { isValidate= false; $("#company_profile").focus(); }
				if($("#comp_logo").valid()==false) { isValidate= false; $("#comp_logo").focus(); }
				if($("#banner_img").valid()==false) { isValidate= false; $("#banner_img").focus(); }
				if($("#company_name").valid()==false) { isValidate= false; $("#company_name").focus(); }
				if($("#challenge_title").valid()==false) { isValidate= false; $("#challenge_title").focus(); }
				
				if($("#challenge_title").valid()==false) { scroll_to_top('challenge_title'); }
				else if($("#company_name").valid()==false) { scroll_to_top('company_name'); }
				else if($("#banner_img").valid()==false) { scroll_to_top('banner_img'); }
				else if($("#comp_logo").valid()==false) { scroll_to_top('comp_logo'); }
				else if($("#company_profile").valid()==false) { scroll_to_top('company_profile'); }
				else if($("#challenge_details").valid()==false) { scroll_to_top('challenge_details'); }
				else if($("#company_details").valid()==false) { scroll_to_top('company_details'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{		

				var myfund = $('input[name="is_external_funding"]:checked').val();
				if(myfund == 1){
					$("#funding_amt").prop( 'disabled', false );
					//$("#external_fund").show();
				} else {
					$("#funding_amt").prop( 'disabled', true );
					$("#external_fund").hide();
				}


				var chals = $('input[name="is_exclusive_challenge"]:checked').val();
				if(chals == '0'){
					$("#challenge_ex_details").prop( 'disabled', false );
					//$("#excl_challenge").show();
				} else {
					$("#challenge_ex_details").prop( 'disabled', true );
					//$("#excl_challenge").hide();
				}
				
				if($("#trl_solution").valid()==false) { isValidate= false; $("#trl_solution").focus(); }
				if($("#fund_reward").valid()==false) { isValidate= false; $("#fund_reward").focus(); }
				if($("#is_amount").valid()==false) { isValidate= false; $("#is_amount").focus(); }
				if($("#if_funding").valid()==false) { isValidate= false; $("#if_funding").focus(); }
				if($("#if_reward").valid()==false) { isValidate= false; $("#if_reward").focus(); }
				//if($("#other_audience").valid()==false) { isValidate= false; $("#other_audience").focus(); }
				if($("#audi_id").valid()==false) { isValidate= false; $("#audi_id").focus(); }
				if($("#other_tag").valid()==false) { isValidate= false; $("#other_tag").focus(); }
				if($("#tags_id").valid()==false) { isValidate= false; $("#tags_id").focus(); }
				if($("#other_technology").valid()==false) { isValidate= false; $("#other_technology").focus(); }
				if($("#techonogy_id").valid()==false) { isValidate= false; $("#techonogy_id").focus(); }
				if($("#close_date").valid()==false) { isValidate= false; $("#close_date").focus(); }
				if($("#launch_date").valid()==false) { isValidate= false; $("#launch_date").focus(); }
				
				if($("#launch_date").valid()==false) { scroll_to_top('launch_date'); }
				else if($("#close_date").valid()==false) { scroll_to_top('close_date'); }
				else if($("#techonogy_id").valid()==false) { scroll_to_top('techonogy_id'); }
				else if($("#other_technology").valid()==false) { scroll_to_top('other_technology'); }
				else if($("#tags_id").valid()==false) { scroll_to_top('tags_id'); }
				else if($("#other_tag").valid()==false) { scroll_to_top('other_tag'); }
				else if($("#audi_id").valid()==false) { scroll_to_top('audi_id'); }
				//else if($("#other_audience").valid()==false) { scroll_to_top('other_audience'); }
				//else if($("#if_funding").valid()==false) { scroll_to_top('if_funding'); }
				//else if($("#is_amount").valid()==false) { scroll_to_top('is_amount'); }
				//else if($("#fund_reward").valid()==false) { scroll_to_top('fund_reward'); }
				else if($("#trl_solution").valid()==false) { scroll_to_top('trl_solution'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==2 && stepDirection=="forward")
			{				
				if($("#is_agree").valid()==false) { isValidate= false; $("#is_agree").focus(); }
				//if($("#challenge_terms").valid()==false) { isValidate= false; $("#challenge_terms").focus(); }
				if($("#max_team").valid()==false) { isValidate= false; $("#max_team").focus(); }
				if($("#min_team").valid()==false) { isValidate= false; $("#min_team").focus(); }
				if($("#geographical").valid()==false) { isValidate= false; $("#geographical").focus(); }
				if($("#domain").valid()==false) { isValidate= false; $("#domain").focus(); }
				if($("#to_age").valid()==false) { isValidate= false; $("#to_age").focus(); }
				if($("#from_age").valid()==false) { isValidate= false; $("#from_age").focus(); }
				if($("#educational").valid()==false) { isValidate= false; $("#educational").focus(); }
				
				if($("#educational").valid()==false) { scroll_to_top('educational'); }
				else if($("#from_age").valid()==false) { scroll_to_top('from_age'); }
				else if($("#to_age").valid()==false) { scroll_to_top('to_age'); }
				else if($("#domain").valid()==false) { scroll_to_top('domain'); }
				else if($("#geographical").valid()==false) { scroll_to_top('geographical'); }
				else if($("#min_team").valid()==false) { scroll_to_top('min_team'); }
				else if($("#max_team").valid()==false) { scroll_to_top('max_team'); }
				//else if($("#challenge_terms").valid()==false) { scroll_to_top('challenge_terms'); }
				else if($("#is_agree").valid()==false) { scroll_to_top('is_agree'); }								
				else { scroll_to_top('smartwizard_add_challenge'); }
			}		
			
			if(stepDirection=="backward") { scroll_to_top('smartwizard_add_challenge') }
			return isValidate;
		})
	});
</script>

<?php
	"<br>Current date : ".$current_date = date('d-m-Y');
	"<br>After 90 days : ".$date_after_90_days = date('d-m-Y', strtotime("+3 months", strtotime($current_date)));
?>
<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){		
	
	// All Check/Uncheck checkboxes	
    $("#checkAll").change(function () {
		$(".chk").prop('checked', $(this).prop("checked"));
	});
	
	// Datepicker
	var FromDateFun = $('#launch_date').datepicker(
	{               
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy",              
		startDate:"<?php echo $current_date; ?>",
		endDate:"<?php echo $date_after_90_days; ?>"
	}).
	on('changeDate', function(ev)
	{             
		var FromDateVal = $("#launch_date").val();
		var ToDateVal = $("#close_date").val();
	 
		var newToDateStart = FromDateVal;
	 
		var temp1 = FromDateVal.split('-');
		var convertFromDateVal = temp1[2] + '-' + temp1[1] + '-' + temp1[0].slice(-2);         
		var d = new Date(convertFromDateVal);
		d.setMonth(d.getMonth() + 24);
		var yyyy = (d.getFullYear());
		var mm = (d.getMonth());
		var dd = (d.getDate());             
		//var newToDateEnd = (yyyy+"-"+("0" + (mm + 1)).slice(-2)+"-"+("0" + (dd + 1)).slice(-2));
		var newToDateEnd = (("0" + (dd + 1)).slice(-2)+"-"+("0" + (mm + 1)).slice(-2)+"-"+yyyy);
	 
		/* console.log(newToDateStart);
		console.log(newToDateEnd); */
	 
		var temp2 = ToDateVal.split('-');
		var convertToDateVal = temp2[2] + '-' + temp2[1] + '-' + temp2[0].slice(-2);
	 
		var temp3 = newToDateStart.split('-');
		var convertnewToDateStart = temp3[2] + '-' + temp3[1] + '-' + temp3[0].slice(-2);
	 
		var temp4 = newToDateEnd.split('-');
		var convertnewToDateEnd = temp4[2] + '-' + temp4[1] + '-' + temp4[0].slice(-2);
	 
		var update_flag = 1;
		if(convertToDateVal == "") { update_flag = 1; }
		else if(new Date(convertToDateVal) >= new Date(convertnewToDateStart) && new Date(convertToDateVal) <= new Date(convertnewToDateEnd)) { update_flag = 0; }
	 
		$('#close_date').datepicker('setStartDate', newToDateStart);
		$('#close_date').datepicker('setEndDate', newToDateEnd);
	 
		if(update_flag == 1)
		{
			$('#close_date').datepicker('update', newToDateStart);
		}             
	 
	  
	}).data('datepicker');
 
	var ToDateFun = $('#close_date').datepicker(
	{
	  
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy",               
		startDate:"<?php echo $current_date; ?>",
		endDate:"<?php echo $date_after_90_days; ?>"
	}); 
		
		
		$("#otherAudience").hide();
		$("#show-text").hide();		
		
		// External Fund 
		$('#is_external_funding').on('click', function(){
			//$('#is_external_funding').val($(this).is(':checked'));
			if($(this).is(":checked")) {
				//$("#external_fund").show();
				$("#funding_amt").attr('required', true);	
				$( "#funding_amt" ).prop( "disabled", false );
				} else {
				$("#funding_amt").val('');
				$("#funding_amt").attr('required', false);	
				$( "#funding_amt" ).prop( "disabled", true );
				$("#funding_amt-error").remove();
				//$("#external_fund").hide();
			}			
		});
		
		// Exclusively Challenge
		$('.chk-exclusive').on('click', function(){		 
			var chk = $('input[name="is_exclusive_challenge"]:checked').val();		  
			if(chk == '1') {
				$("#challenge_ex_details").val('');
				$("#challenge_ex_details").attr('required', false);
				$( "#challenge_ex_details" ).prop( "disabled", true );
				//$("#excl_challenge").hide();
				} else {
				$("#challenge_ex_details").attr('required', true);	
				$( "#challenge_ex_details" ).prop( "disabled", false );	
				$("#challenge_ex_details-error").remove();
				//$("#excl_challenge").show(); 
			}			
		});
		
		// Technology Fields
		$( "#techonogy_id" ).change(function() {
			var f_name = $("#techonogy_id").val();
			if(f_name != 0){
				//$("#otherTech").hide();
				$( "#other_technology" ).prop( "disabled", false );
			} else {
				//$("#otherTech").show();
				$( "#other_technology" ).val();
				$( "#other_technology" ).prop( "disabled", true );
			}
		});
		
		// Audience Preference Fields
		$(".audi-chk").click(function () {
			var v_name = $(this).val();				
			if($(this).is(":checked")) {
				$( ".chk" ).prop( "checked", false );
				$("#otherAudience").show(); 
				} else {				
				$("#otherAudience").hide();
			}			
		});
		
		
		$(".chk").click(function () {		   
			if($(this).is(":checked")) {				
				$( ".audi-chk" ).prop( "checked", false );	
				$("#otherAudience").hide();	
			} 		
		});
		
		
		$("#is_agree").click(function () {		   
			if($(this).is(":checked")) {					
				$("#show-text").show();	
				} else {
				$("#show-text").hide();	
			}		
		});
		
		//fund-reward //fund_agree //fund_reward_div
		$(".fund-chk").click(function () {		   
			if($(this).is(":checked")) {
				//$("#fund_agree").show();
				$("#is_amount").attr('required', true);
				$( "#is_amount" ).prop( "disabled", false );	
				} else {
				//$("#fund_agree").hide();
				$("#is_amount" ).val('');
				$("#is_amount").attr('required', false);
				$( "#is_amount" ).prop( "disabled", true );				
			}		
		});
		$(".fund-reward").click(function () {		   
			if($(this).is(":checked")) {
				//$("#fund_reward_div").show();
				$("#fund_reward").attr('required', true);
				$( "#fund_reward" ).prop( "disabled", false );		
				} else {
				//$("#fund_reward_div").hide();
				$("#fund_reward" ).val('');
				$("#fund_reward").attr('required', false);
				$( "#fund_reward" ).prop( "disabled", true );	
			}			
		});
		
		$( ".choose-tags" ).change(function() {	
			var values = $(this).val();	
			if(jQuery.inArray("0", values) !== -1){				
				//$("#otherTags").show();	
				$("#other_tag").attr('required', true);
				$("#other_tag").prop( 'disabled', false );
			} else {
				//$("#otherTags").hide();
				$( "#other_tag" ).val('');
				$("#other_tag").attr('required', false);
				$("#other_tag" ).prop( 'disabled', true );
				$("#other_tag-error").remove();
			}
		});
		
		$( ".choose-tech" ).change(function() {	
			var values = $(this).val();	
			if(jQuery.inArray("0", values) !== -1){				
				//$("#otherTech").show();	
				$("#other_technology").attr('required', true);
				$("#other_technology").prop( 'disabled', false );
			} else {
				//$("#otherTech").hide();					
				$("#other_technology" ).val('');
				$("#other_technology").attr('required', false);	
				$("#other_technology" ).prop( 'disabled', true );				
				$("#other_technology-error").remove();
				
			}
		});
			
	});

	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	$(document).ready(function () {
	
		// Validation Method Start
		
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		$.validator.addMethod("checkTeam", function(value, element) 
		{ 
			var maxVal = value;
			var minVal = parseInt($("#min_team").val());
			if(maxVal != "")
			{
				if(maxVal < minVal){return false;}else {return true;}
				
			} else return true;
		});	
		
		$.validator.addMethod("checkAge", function(value, element) 
		{ 
			var maxVal = value;
			var minVal = parseInt($("#from_age").val());
			if(maxVal != "")
			{
				if(maxVal < minVal){return false;}else {return true;}
				
			} else return true;
		});
		
		$("input.numbers").keypress(function(event) {
		  return /\d/.test(String.fromCharCode(event.keyCode));
		});
				
		/* $.validator.setDefaults({
			submitHandler: function () {
				form.submit();
			}
		}); */
		/*jQuery.validator.addMethod("accept", function(value, element, param) {
			return value.match(new RegExp("." + param + "$"));
			});
			rules: {
			fileupload: { accept: "(docx?|doc|pdf)" }
		}*/
		
		$.validator.addMethod("valid_email", function(value, element) 
		{ 
			var email = value;
			var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			var result = pattern.test(email);	
			if(result){return true;}else {return false;}
		});
		
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		//add the custom validation method
		jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);

		$.validator.addMethod('filesize', function (value, element, param) {
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');

    	$.validator.addMethod('filesize_logo', function (value, element, param) {
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 500 KB');
		
		$.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
		}, "Please enter only letters and spaces");

		/*jQuery.validator.addMethod("lettersonlys", function(value, element) {
		  return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
		}, "Letters only please");*/	

		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);
			//console.log(count);
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));
		
		$.validator.addMethod('is_all_number', function(value) {
        if (isNaN( value )) {
        return true;
        } else {
         return false;
        }
    
   		 }, 'This field should not contain all numbers');
		
		$('#challengeform').validate({
			ignore: ":disabled",
			rules: {
				challenge_title: {
					required: true,
					nowhitespace:true,
					//minCount:['25']
					maxCount:['100']
				},
				company_name: {
					required: true,
					nowhitespace:true,
					 is_all_number : true
				},
				banner_img: {
					required: true, valid_img_format: true,filesize:2000000
				},
				comp_logo: {
					/* required: true, */ valid_img_format: true,filesize_logo:500000
				},				
				company_profile: {
					maxCount:['200']
				},
				challenge_details: {
					required: true,
					minCount:['5'],
					maxCount:['50']
				},
				company_details: {
					required: true,
					minCount:['50'],
					maxCount:['400']
				},
				launch_date: {
					required: true
				},
				close_date: {
					required: true
				},
				"techonogy_id[]": {
					required: true
				},
				other_technology:{
					//required: true,
					nowhitespace:true,					
				},
				"tags_id[]": {
					required: true
				},
				other_tag:{
					//required: true,
					nowhitespace:true
						
				},
				"audi_id[]": {
					required: true
				},				
				to_age: {
					checkAge: true
				},
				is_amount: {
					required: true,
					number: true
				},
				fund_reward: {
					required: true
				},
				trl_solution: {
					required: true
				},
				"domain[]": {
					required: true
				},
				min_team: {
					required: true
				},
				max_team: {
					required: true,
					checkTeam:true
				},
				
				is_agree: {
					required: true
				},
				contact_name: {
					required: true,
					maxCount:['100']
				},			 
				mobile: {
					required: true,
					number: true,
					minlength:10,
					maxlength:11
				},
				contact_email: {
					required: true,
					email: true,
					valid_email:true
				},
				office_no: {
					required: true,
					number: true
				},
				visibility: {
					required: true
				},
				ip_cls: {
					required: true
				},
				
				funding_amt: {
					required: true,
					number: true
				},
				is_exclusive_challenge: {
					required: true
				},
				challenge_ex_details: {
					required: true,
					maxCount:['100']
				}
			},
			messages: {
				challenge_title: {
					required: "This field is required"
				},
				company_name: {
					required: "This field is required"
				},
				banner_img: {
					required: "This field is required", 
					valid_img_format: "Please upload only image file"
				},
				comp_logo: {
					valid_img_format: "Please upload only image file"
				},	
				company_profile: {
					required: "This field is required"
				},
				challenge_details: {
					required: "This field is required"
				},
				company_details: {
					required: "This field is required"
				},
				launch_date: {
					required: "This field is required"              
				},
				close_date: {
					required: "This field is required"
				},
				techonogy_id: {
					required: "This field is required"
				},
				other_technology: {
					required: "This field is required"
				},
				tags_id: {
					required: "This field is required"
				},
				other_tag:{
					required: "This field is required"
				},
				audi_id: {
					required: "This field is required"
				},				
				to_age: {
					checkAge: "The 'To Age' should be greater than the 'From Age'"
				},
				fund_reward: {
					required: "This field is required"
				},
				is_amount: {
					required: "This field is required"
				},
				trl_solution: {
					required: "This field is required"
				},
				"domain[]": {
					required: "This field is required"
				},
				min_team: {
					required: "This field is required"
				},
				max_team: {
					required: "This field is required",
					checkTeam: "Maximum Team size should not be less than minimum team size"
				},
				
				is_agree: {
					required: "This field is required"
				},
				contact_name: {
					required: "This field is required"
				},
				mobile: {
					required: "This field is required"
				},
				contact_email: {
					required: "This field is required",
					valid_email: "Please enter valid email address"
				},
				office_no: {
					required: "This field is required"
				},			 
				visibility: {
					required: "This field is required"
				},
				ip_cls: {
					required: "This field is required"
				},
				
				funding_amt: {
					required: "This field is required"
				},
				is_exclusive_challenge: {
					required: "This field is required"
				},
				challenge_ex_details: {
					required: "This field is required"
				}	
			},
			
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			},
			invalidHandler: function(e,validator) {
				//validator.errorList contains an array of objects, where each object has properties "element" and "message".  element is the actual HTML Input.
				for (var i=0;i<validator.errorList.length;i++){
					console.log(validator.errorList[i]);
				}
				//validator.errorMap is an object mapping input names -> error messages
				for (var i in validator.errorMap) {
				  console.log(i, ":", validator.errorMap[i]);
				}
			}
		});
	});
	
	// Select Box Reinitialization
	$('.select2').select2({});

</script>			