<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Participate</h1>
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav> -->
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <?php $dynamic_ht = "600"; 
                    if($challenge_data[0]['challenge_id'] == 'TNCID-000006') { $dynamic_ht = "1889"; } 
                    else if($challenge_data[0]['challenge_id'] == 'TNCID-000007') { $dynamic_ht = "1681"; } 
                    else if($challenge_data[0]['challenge_id'] == 'TNCID-000008') { $dynamic_ht = "1681"; } 
                    else if($challenge_data[0]['challenge_id'] == 'TNCID-000009') { $dynamic_ht = "1929"; } 
                    else if($challenge_data[0]['challenge_id'] == 'TNCID-000010') { $dynamic_ht = "1778"; } 
                    else if($challenge_data[0]['challenge_id'] == 'TNCID-000011') { $dynamic_ht = "1681"; } 
                  ?>
                 <iframe src="<?php echo $link; ?>" width="640" height="<?php echo $dynamic_ht; ?>" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
            </div>
        </div>
        </div>
    </section>
	