<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
.select2-container--default .select2-selection--single{height:35px!important; border:none}

#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}

</style>
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p CollaborativeTechnologySolutions text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">List of challenges</h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">List of challenges</li>
            </ol>
        </nav>

    
    </div>
    <!--/end container-->
</div>
<section id="story" data-aos="fade-up">
    
    <div class="filterBox">
        <section class="search-sec">
            <form action="#" method="post" id="filterData" novalidate="novalidate">
                <div class="container">
       
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 p-1">
<strong style="color: #FFF; font-size: 14px; font-weight: 500;">Challenge Close Date</strong>
								<input type="text" class="form-control search-slt datepicker" name="ch_date" id="ch_date"  value="" placeholder="Challenge Close Date" readonly />
							
                            </div>
							
                           <div class="col-lg-3 col-md-3 col-sm-12 p-1">
						   <strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select Technology</strong>

                                <select class="form-control search-slt select2" id="technologys" name="technologys[]" multiple="multiple">	
                                    <?php foreach($technology_data as $tech_val){ ?>
									<option value="<?php echo $tech_val['id'] ?>"><?php echo $tech_val['technology_name'] ?></option>
									<?php } ?>
                                </select>
								
                            </div>
							
                            <div class="col-lg-3 col-md-3 col-sm-12 p-1">
							<strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select Tag</strong>

                                <select class="form-control search-slt select2" id="tags" name="tags[]" multiple="multiple">
                                    <?php foreach($tag_data as $tag_val){ ?>
									<option value="<?php echo $tag_val['id'] ?>"><?php echo $tag_val['tag_name'] ?></option>
									<?php } ?>
                                </select>
                            </div>
							<!--<div class="col-lg-3 col-md-3 col-sm-12 p-1">
							<strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select Audience Preference</strong>

                                <select class="form-control search-slt select2" id="audience" name="audience[]" multiple="multiple">
                                    <?php foreach($audience_data as $ad_val){ ?>
									<option value="<?php echo $ad_val['id'] ?>"><?php echo $ad_val['preference_name'] ?></option>
									<?php } ?>
                                </select>
                            </div>-->
							<div class="col-lg-3 col-md-3 col-sm-12 p-1">
								<strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select TRL</strong>
                                <select class="form-control search-slt select2" id="trls" name="trls">
									<option value="">-- Select -- </option>
                                    <?php foreach($trl_data as $trl_val){ ?>
									<option value="<?php echo $trl_val['id'] ?>"><?php echo $trl_val['trl_name'] ?></option>
									<?php } ?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-3 col-sm-12 p-1">
                                <select class="form-control search-slt select2" id="visibility" name="visibility">
									<option value="Public">Public </option>
									<option value="Private">Private </option>                                    
                                </select>
                            </div>
							<div class="col-lg-3 col-md-3 col-sm-12 p-1 mt-2">
								<input type="checkbox" class="" name="if_funding" id="if_funding"  value="1"  />
								&nbsp;<label class="form-control-placeholder" for="cname" style="color:#FFF;  margin: 5px !important">Funded Challenges Only</label>
                                
                            </div>
							<div class="col-lg-3 col-md-3 col-sm-12 p-1 mt-2">
								<input type="checkbox" class="" name="if_exclusive" id="if_exclusive"  value="1"  />
								&nbsp;<label class="form-control-placeholder" for="cname" style="color:#FFF; margin: 5px !important">Exclusively On TechNovuus</label>
                                
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                                <button type="button" class="btn btn-primary" id="search-challenge">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </section>

      
  
</div>

    <div class="container">

      <div class="row">
		<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

        <!-- <div class="col-md-12">
          <h1 class="wow fadeInUp">featured challenges</h1>
          <div class="heading-border"></div>
        </div> -->
      </div>
      <div class="sortable-masonry1">
        <div class="filters">
          <ul class="filter-tabs filter-btns clearfix text-center">
		  
            <li class="filter main-cat active" data-role="button" data-filter=".all" data-id="0">All Challenges</li>
			<?php if(count($audience_data) > 0){
				
				foreach($audience_data as $get_pref_detail){
					
			?>
				<li class="filter main-cat <?php echo $activeClass; ?>" data-role="button" data-filter=".p1" data-id="<?php echo $get_pref_detail['id']  ?>">
					<?php echo $get_pref_detail['preference_name']  ?>
				</li>
			<?php 	}
				
			} ?>           
          </ul>
        </div>
		
		<div class="row items-containerx clearfix challengeCardsList">
			<?php								
				if(count($challenge_data) > 0 )
				{

					foreach($challenge_data as $challenge){
							
						$limit = 200;
						 if (strlen($challenge['challenge_details'] ) > $limit){
							$challenge['challenge_details']  = substr($challenge['challenge_details'] , 0, strrpos(substr($challenge['challenge_details'] , 0, $limit), ' ')) . '...'; 
						 }
						 
						 $limit2 = 48;
						 if (strlen($challenge['challenge_title'] ) > $limit2){
							$challenge['challenge_title']  = substr($challenge['challenge_title'] , 0, strrpos(substr($challenge['challenge_title'] , 0, $limit2), ' ')) . '...'; 
						 }
						
						//$datestr = $challenge['challenge_close_date'];//Your date
						//$date	 = strtotime($datestr);//Converted to a PHP date (a second count)

						//Calculate difference
						//$diff	= 	$date-time();//time returns current time in seconds
						//$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
						//$hours	=	round(($diff-$days*60*60*24)/(60*60));
						
						//$date2 = date('Y-m-d');
						//$date3 = strtotime($date2);
						//if($date3 >= $date){
						if($challenge['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $challenge['challenge_close_date'])
						{
						//if($date3 >= $date){{
							//$removeMinus = str_replace("-","", $days);							
							$date1=date_create(date("Y-m-d"));
							$date2=date_create($challenge['challenge_close_date']);
							$diff=date_diff($date1,$date2);
							$showCnt = "Days left : ".($diff->format("%a")+1);							
						} 
						else 
						{
							$showCnt = "Closed Participations";
						}
						
						/*if($days >= 0){
							$showCnt = "Days left : ".$days;
						} else {
							$showCnt = "Closed Paticipations";
						}*/	
						
						$fileExist = base_url('assets/challenge/'.$challenge['banner_img']);
						 if ($challenge['banner_img']!="") {
							$image = $fileExist;
						} else {
							$image = base_url('assets/news-14.jpg');
						}
						
			?>
			<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" >
				<div class="desc-comp-offer-cont">
					<a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($challenge['c_id'])); ?>" ><div class="thumbnail-blogs">
						<div class="caption">
						<i class="fa fa-chain"></i>
						</div>
						<img src="<?php echo $image; ?>" class="img-fluid" alt="<?php echo $challenge['challenge_title'] ?>" style="">
					</div></a>
					<div class="card-content">
						<h5><?php echo $challenge['company_name'] ?></h5>
						<h3><?php echo $challenge['challenge_title'] ?></h3>
						<p class="desc"><?php echo $challenge['challenge_details'] ?>
						<a href="javascript:void(0);" class="show_hide_new">Read More</a>
						</p>
						<div class="cardd-footer content-new ">
							
							<div>
								<?php 
									if($challenge['audience_pref']!= ''){	
									$audience = explode(",", $challenge['audience_pref']);
									foreach($audience as $audi){
								?>
									<button class="btn btn-general btn-green-fresh remove-h" role="button"><?php echo $audi; ?></button>
								<?php } } else { ?>
									<button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>
								<?php } ?>
							</div>
							
							<div class="daysleft"><i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></div>
						</div>
					</div>
				</div>
			</div> 
			<?php } // Foreach End ?>	
				<?php if(count($total) > 6){ ?>
				<div class="col-md-12 text-center  show_more_main" id="show_more_main<?php echo $challenge['c_id']; ?>">
					<span id="<?php echo $challenge['c_id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>
				<?php } ?>
				<?php	} else { // If End ?>
				<div class="col-md-12 text-center" >
					No Record Found
				</div>
				<?php } ?>
        </div>
      </div>
    </div>
  </section>
  <?php
if (count($consortium_list) > 0) {
?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Technology Platform Partners</h2>
                    <div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
                <?php foreach ($consortium_list as $consrotium) : ?>
                    <div class="col-sm-12"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></div>
                <?php endforeach; ?>
			</div>
			</div>
        </div>
    </section>
<?php } ?>
<section class="ministrylogo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center title-bar">
                <h4>Under Aegis of</h4>
                <div class="heading-border"></div>
                <img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo">
            </div>
        </div>
    </div>
</section>
<!--Client Logo Section Ends-->

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script type="text/javascript">
	function get_more_apply_css()
	{
		$('.thumbnail-blogs').hover(
			function() {
				$(this).find('.caption').slideDown(250)
			},
			function() {
				$(this).find('.caption').slideUp(205)
			}
			);
	}
	
	function get_alert_text(){
		
		// Read More/Less
	    $(".content-new").show();
		  $(".show_hide_new").on("click", function() {	
			console.log('HELLO');
			$(this).parent().next(".content-new").toggle();			
			if ($(this).text().trim() == "Read More") {
			  $(this).text("Read Less");
			} else {
			  $(this).text("Read More");
			}
		  });
	}

   $(document).ready(function () { 

		$('.datepicker').datepicker({
		   format: 'dd-mm-yyyy',
		   //minDate: '0',
		   //startDate:'+0d',
		   autoclose: true
	   });
	   
	   
	   // Read More/Less
	    $(".content-new").show();
		  $(".show_hide_new").on("click", function() {	
			console.log('HELLO');
			$(this).parent().next(".content-new").toggle();			
			if ($(this).text().trim() == "Read More") {
			  $(this).text("Read Less");
			} else {
			  $(this).text("Read More");
			}
		  });
	   
	    
		// Main Category Filter
		$(document).on('click','.main-cat',function(){	
			var base_url = '<?php echo base_url(); ?>';
			var cat_val = 	$(this).attr('data-id');
			var cs_t = 	$('.token').val();
			$(this).siblings().removeClass('active');
			$(this).addClass('active');	
			$.ajax({
				type:'POST',
				url: base_url+'challenge/category_filter',
				data:'cat_id='+cat_val+'&csrf_test_name='+cs_t,				
				dataType:"text",
				success:function(data){
					var output = JSON.parse(data);						
					$(".token").val(output.token);
					$('.challengeCardsList').html(output.html);

					get_more_apply_css();
					get_alert_text();
				}
			});
		});
	   
		// Form Filter
		$(document).on('click','#search-challenge',function(){
			$("ul.filter-tabs").find('li').removeClass('active');	
			var cs_t = 	$('.token').val();
			var base_url = '<?php echo base_url(); ?>'; 
			var ch_date = $('#ch_date').val();
			var technologys = $('#technologys').val();
			var tags = $('#tags').val();
			var audience = $('#audience').val();
			var trls = $('#trls').val();
			var visibility = $('#visibility').val();
			var if_funding = $('#if_funding').val();
			var if_exclusive = $('#if_exclusive').val();
			
			if ($('#if_funding').is(':checked')) {
				//alert($('#if_funding').val() + ' is checked');
			}
			
			if ($('#if_exclusive').is(':checked')) {
				//alert($('#if_exclusive' ).val() + ' is checked');
			}
			
			$.ajax({
				type:'POST',
				url: base_url+'challenge/filter_more',
				//data:'filter_date='+ch_date+'&csrf_test_name='+cs_t+'&technologys',
				data: $('#filterData').serialize(),
				dataType:"text",
				success:function(data){
					var output = JSON.parse(data);						
					$(".token").val(output.token);
					$('.challengeCardsList').html(output.html);

					get_more_apply_css();
					get_alert_text();
				}
			});
			
		 });
		 
		// Load More Code 
		$(document).on('click','.show_more',function(){
			// $(".token").remove();
			var base_url = '<?php echo base_url(); ?>'; 
			var ID = $(this).attr('id');
			var length = $('.p3').length;
			var cs_t = 	$('.token').val();
			$('.show_more').hide();
			$('.loding').show();
			$.ajax({
				type:'POST',
				url: base_url+'challenge/getmore',
				data:'id='+length+'&csrf_test_name='+cs_t,
				dataType:"text",
				success:function(data){
					//console.log(data);
					var output = JSON.parse(data);
					if(output.html == ""){
						$(".show_more_main").remove();
					}	
					$(".token").val(output.token);								
					$('#show_more_main'+ID).remove();				
					$('.challengeCardsList').append(output.html); 	

					get_more_apply_css();
					get_alert_text();					

				}
			});
		});
		
		
		
	 
   });
   

   
 $('.select2').select2({});
$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });	
</script>


