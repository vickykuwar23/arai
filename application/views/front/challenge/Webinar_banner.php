<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Webinar_banner 
Author : Vicky K
*/

class Webinar_banner extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		
		$this->load->helper('security');
		$this->load->library('Opensslencryptdecrypt');	
		if($this->session->userdata('admin_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		ini_set('upload_max_filesize', '100M');  
		ini_set('post_max_size', '100M');  	
    }

    public function index($value='')
    {
		//isLogin();
		$encrptopenssl =  New Opensslencryptdecrypt();
		$response_data = $this->master_model->getRecords("webinar_banner","","",array('id' => 'DESC'));	
		
		$res_arr = array();
		if(count($response_data)){	
						
			foreach($response_data as $row_val){
					
				$row_val['banner_name'] = $encrptopenssl->decrypt($row_val['banner_name']);	
				$row_val['banner_sub']  = $encrptopenssl->decrypt($row_val['banner_sub']);		
				$row_val['banner_img'] 	= $encrptopenssl->decrypt($row_val['banner_img']);
				$row_val['banner_desc'] = $encrptopenssl->decrypt($row_val['banner_desc']);
				$row_val['banner_url'] 	= $encrptopenssl->decrypt($row_val['banner_url']);
				$row_val['xOrder'] 		= $row_val['xOrder'];
				$res_arr[] = $row_val;
			}
			
		}
    	
    	$data['records'] = $res_arr; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Webinar_banner';	
    	$data['middle_content']='webinar-banner/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
        // Check Validation		
		$this->form_validation->set_rules('upload_type', 'Upload Type', 'required|xss_clean');
		$this->form_validation->set_rules('xOrder', 'Order', 'required|xss_clean');
		if(empty($_FILES['banner_img']['name']))
		{
			$this->form_validation->set_rules('banner_img', 'Upload Video/Banner', 'required|xss_clean');
		}
		
		
		if($this->form_validation->run())
		{	
			
			$upload_type = $this->input->post('upload_type');
			$xOrder      = $this->input->post('xOrder');
			if($upload_type == 'Image'){
				
				$banner_name = $encrptopenssl->encrypt($this->input->post('banner_name'));
				$banner_sub  = $encrptopenssl->encrypt($this->input->post('banner_sub'));
				$banner_desc = $encrptopenssl->encrypt($this->input->post('banner_desc'));
				$banner_url  = $encrptopenssl->encrypt($this->input->post('banner_url'));
				
				
			} else {
				
				$banner_name = "";
				$banner_sub  = "";
				$banner_desc = "";
				$banner_url  = "";
			}
			
			$banner_img  = $_FILES['banner_img']['name'];
			
			if($_FILES['banner_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/webinar_banner';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '500000000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}
			
			} // Banner Image End
			
			$file_names = $encrptopenssl->encrypt($b_image[0]);		
			
			$insertArr = array( 'upload_type' => $upload_type, 'banner_name' => $banner_name, 'banner_sub' => $banner_sub, 'banner_img' => $file_names, 'banner_desc' => $banner_desc,'banner_url' => $banner_url, 'xOrder' => $xOrder);			
			
			//echo "<pre>";print_r($insertArr);die();
			$insertQuery = $this->master_model->insertRecord('webinar_banner',$insertArr);
			if($insertQuery > 0){
				$this->session->set_flashdata('success','Webinar banner successfully uploaded.');
				redirect(base_url('xAdmin/webinar_banner'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/webinar_banner/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Webinar_banner';	
        $data['middle_content']='webinar-banner/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		
		$banner_data = $this->master_model->getRecords("webinar_banner", array('id' => $id));
		
		$res_arr = array();
		if(count($banner_data) > 0){
			$row_val['upload_type'] = $banner_data[0]['upload_type'];	
			$row_val['banner_name'] = $encrptopenssl->decrypt($banner_data[0]['banner_name']);
			$row_val['banner_sub']  = $encrptopenssl->decrypt($banner_data[0]['banner_sub']);
			$row_val['banner_img'] 	= $encrptopenssl->decrypt($banner_data[0]['banner_img']);
			$row_val['banner_desc'] = $encrptopenssl->decrypt($banner_data[0]['banner_desc']);
			$row_val['banner_url']  = $encrptopenssl->decrypt($banner_data[0]['banner_url']);
			$row_val['xOrder']      = $banner_data[0]['xOrder'];	
			
			$res_arr[] = $row_val;
		}
		
		$data['banner_data'] = $res_arr;
		
        // Check Validation		
		$this->form_validation->set_rules('upload_type', 'Upload Type', 'required|xss_clean');
		$this->form_validation->set_rules('xOrder', 'Order', 'required|xss_clean');
		if($this->form_validation->run())
		{	
			
			$banner_img  = $_FILES['banner_img']['name'];			
			$xOrder      = $this->input->post('xOrder');
			$upload_type = $this->input->post('upload_type');
			if($upload_type == 'Image'){
				
				$banner_name = $encrptopenssl->encrypt($this->input->post('banner_name'));
				$banner_sub  = $encrptopenssl->encrypt($this->input->post('banner_sub'));
				$banner_desc = $encrptopenssl->encrypt($this->input->post('banner_desc'));
				$banner_url  = $encrptopenssl->encrypt($this->input->post('banner_url'));
				
			} else {
				
				$banner_name = "";
				$banner_sub  = "";
				$banner_desc = "";
				$banner_url  = "";
			}
			
			if($_FILES['banner_img']['name']!=""){			
				
				$config['upload_path']      = 'assets/webinar_banner';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '50000000';         
				$config['encrypt_name']     = TRUE;
						 
				$upload_files  = @$this->master_model->upload_file('banner_img', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}
			
			} // Banner Image End
			
			
			if($b_image[0] == ""){
				
				$file_names = $banner_data[0]['banner_img'];
				
			} else {
				
				$file_names = $encrptopenssl->encrypt($b_image[0]);
			} 
									
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'upload_type' => $upload_type, 'banner_name' => $banner_name, 'banner_sub' => $banner_sub, 'banner_img' => $file_names, 'banner_desc' => $banner_desc, 'banner_url' => $banner_url, 'xOrder' => $xOrder, 'updatedAt' => $updateAt);			
			//echo "<pre>";print_r($updateArr);die();
			$updateQuery = $this->master_model->updateRecord('webinar_banner',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				$this->session->set_flashdata('success','Webinar banner successfully updated');
				redirect(base_url('xAdmin/webinar_banner'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/webinar_banner/edit/'.$id));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Webinar_banner';
        $data['middle_content']='webinar-banner/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('webinar_banner',array('status'=>$value),array('id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/webinar_banner'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('webinar_banner',array('status'=>$value),array('id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/webinar_banner'));		
		 }
		 
	 }
	 
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('webinar_banner',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','Webinar banner successfully deleted');
		 redirect(base_url('xAdmin/webinar_banner'));	
		 
	 }


}