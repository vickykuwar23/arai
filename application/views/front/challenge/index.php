<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
   .search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
   .select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
   .select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
   .select2-container--default .select2-selection--single{height:35px!important; border:none}
   #select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}


   .search-sec2{padding:1rem;background:#c80032;width:100%;margin-bottom:25px}
.filterBox2 .btn-primary{color:#c80032;background-color:#FFF;border-color:#c80032;width:100%;display:block;padding:12px;border-radius:0}
.filterBox2 .btn-primary:hover{color:#c80032;background-color:#FFF;border-color:#c80032}
   .filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}

   a.filter_load_more,a.filter_load_more_tech,a.filter_load_less_tech, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 
   .search_Box input[type=text] {height: 46px !important; background: #FFF; border-radius: 0; border: solid 1px #FFF; position: relative;}
  

 .search_Box input:after {
    position: absolute;
    width: 20px;
    height: 10px;
    background: #000;
    top: 0;
    right: 0;
  font-family:'FontAwesome';
    content: "\f095";

    z-index: 999;
}

</style>
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p CollaborativeTechnologySolutions text-center" data-aos="fade-down">
   <div class="container searchBox">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">List of challenges</h1>
      <nav aria-label="breadcrumb" class="">
         <ol class="breadcrumb wow fadeInUp">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">List of challenges</li>
         </ol>
      </nav>
   </div>
   <!--/end container-->
</div>
<section id="story" data-aos="fade-up">
      <!------------ START : BLOG SEARCH --------->
         <div class="filterBox">
            <section class="search-sec2">
               <div id="filterData" name="filterData">
                  <div class="container">   
                     <div class="row">
                        <div class="col-lg-12">
                           <div id="mySidenav" class="sidenav">
                              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
                              <div class="scroll"> 
                                 
                                 <?php if($i > 5){ ?>
                                 <a href="javascript:void(0);" class="filter_load_more" onclick="show_more_type('show')">Show More >> </a>
                                 <a href="javascript:void(0);" class="filter_load_less d-none" onclick="show_more_type('hide')">Show Less >></a><br>
                                 <?php } ?>  
                                 <ul>
                                    <li> <strong> Select Technology</strong> </li>                            
                                    <?php 
                                    
                                       $i=0;
                                       if(count($technology_data) > 0) 
                                       { 
                                          foreach($technology_data as $res)
                                          {  ?>                            
                                             <li class="<?php if($i >= 5) { ?>filter_technology d-none<?php } ?>">
                                                <input class="form-check-input search_technology" type="checkbox" value="<?php echo $res['id']; ?>" id="search_technologies" name="search_technologies[]" onchange="getChallengeDataAjax(0, '<?php echo $limit; ?>', 1, 0)">
                                                <label class="form-check-label"><?php echo $res['technology_name']; ?></label>
                                             </li>
                                             <?php $i++;
                                             // }
                                          }
                                       }  ?>
                                 </ul>
                                 <?php if($i > 5){ ?>
                                 <a href="javascript:void(0);" class="filter_load_more_tech" onclick="show_more_technology('show')">Show More >> </a>
                                 <a href="javascript:void(0);" class="filter_load_less_tech d-none" onclick="show_more_technology('hide')">Show Less >></a><br>
                                 <?php } ?>
                                 
                                 <ul>
                                    <li> <strong> Select Tags</strong> </li>                             
                                    <?php 
                                       $i=0;
                                       if(count($tag_data) > 0) 
                                       { 
                                          foreach($tag_data as $res)
                                          { ?>                             
                                             <li class="<?php if($i >= 5) { ?>filter_tag d-none<?php  } ?>">
                                                <input class="form-check-input search_tag" type="checkbox" value="<?php echo $res['id']; ?>" id="search_tags" name="search_tags[]" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                                <label class="form-check-label"><?php echo $res['tag_name']; ?></label>
                                             </li>
                                             <?php $i++;                                           
                                          }
                                       }  ?>
                                 </ul>
                                 <?php if(count($tag_data) > 5){ ?>
                                 <a href="javascript:void(0);" class="filter_load_more_tag" onclick="show_more_tag('show')">Show More >> </a>
                                 <a href="javascript:void(0);" class="filter_load_less_tag d-none" onclick="show_more_tag('hide')">Show Less >></a><br>
                                 <?php } ?>

                                 <ul>
                                    <li> <strong> Select TRL</strong> </li>                              
                                    <?php 
                                       $i=0;
                                       if(count($trl_data) > 0) 
                                       { 
                                          foreach($trl_data as $res)
                                          {  
                                             if(strtolower($res['trl_name']) != 'other')
                                             { ?>                             
                                             <li class="<?php if($i >= 5) { ?>filter_trl d-none<?php  } ?>">
                                                <input class="form-check-input search_trl" type="checkbox" value="<?php echo $res['id']; ?>" id="search_technologies" name="search_technologies[]" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                                <label class="form-check-label"><?php echo $res['trl_name']; ?></label>
                                             </li>
                                             <?php $i++;
                                             }
                                          }
                                       }  ?>
                                 </ul>
                                 <?php if($i > 5){ ?>
                                 <a href="javascript:void(0);" class="filter_load_more" onclick="show_more_trl('show')">Show More >> </a>
                                 <a href="javascript:void(0);" class="filter_load_less d-none" onclick="show_more_trl('hide')">Show Less >></a><br>
                                 <?php } ?>  
                                 
                                 <ul>
                                    <li> <strong> Select Challenge Type</strong> </li>                              
                                    <li class="">
                                       <input class="form-check-input search_chal_type" type="checkbox" value="Public" id="search_chal_types" name="search_chal_type[]" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                       <label class="form-check-label">Public</label>
                                    </li>                            
                                    <li class="">
                                       <input class="form-check-input search_chal_type" type="checkbox" value="Private" id="search_chal_types" name="search_chal_type[]" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                       <label class="form-check-label">Private</label>
                                    </li>
                                 </ul>

                                  <ul>
                                    <li class="">
                                       <input class="form-check-input funding_type" type="checkbox" value="1" id="funding_type" name="funding_type" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                       <label class="form-check-label">Funded Challenges Only</label>
                                    </li>                            
                                 </ul>

                                 <ul>
                                    <li class="">
                                       <input class="form-check-input is_exclusive" type="checkbox" value="1" id="is_exclusive" name="is_exclusive" onchange="getChallengeDataAjax( 0, '<?php echo $limit; ?>', 1, 0)">
                                       <label class="form-check-label">Exclusively On TechNovuus</label>
                                    </li>                            
                                 </ul>
                                
                              </div>                        
                           </div>
                           
                           <div class="row d-flex justify-content-center search_Box">  
                              <div class="col-md-3">
                              <input type="text" class="form-control datepicker" name="close_date" id="close_date"  value="" placeholder="Challenge Close Date" readonly />
                               </div>  

                               

                              <div class="col-md-5">
                                 <input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                                 <button type="button" class="btn btn-primary searchButton" onclick="getChallengeDataAjax(0, '<?php echo $limit; ?>', 1, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
                              </div>
                              <div class="clearAll">
                                 <a class="btn btn-primary" id="reset-val" href="javascript:void(0)" onclick="clear_search_form()">Clear All</a>
                              </div>
                              <?php if(count($type_data) > 0 || 1) { ?>
                                 <div class="filter d-nonex">
                                    <button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
                                 </div>
                              <?php } ?>                                                     
                              <div class="col-md-12"></div>                      
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!------------ END : BLOG SEARCH --------->
   <div class="filterBox d-none">
      <section class="search-sec">
         <form action="#" method="post" id="filterData" novalidate="novalidate">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <strong style="color: #FFF; font-size: 14px; font-weight: 500;">Challenge Close Date</strong>
                           <input type="text" class="form-control search-slt datepicker" name="ch_date" id="ch_date"  value="" placeholder="Challenge Close Date" readonly />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select Technology</strong>
                           <select class="form-control search-slt select2" id="technologys" name="technologys[]" multiple="multiple">
                              <?php foreach($technology_data as $tech_val){ ?>
                              <option value="<?php echo $tech_val['id'] ?>"><?php echo $tech_val['technology_name'] ?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select Tag</strong>
                           <select class="form-control search-slt select2" id="tags" name="tags[]" multiple="multiple">
                              <?php foreach($tag_data as $tag_val){ ?>
                              <option value="<?php echo $tag_val['id'] ?>"><?php echo $tag_val['tag_name'] ?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <strong style="color: #FFF; font-size: 14px; font-weight: 500;">Select TRL</strong>
                           <select class="form-control search-slt select2" id="trls" name="trls">
                              <option value="">-- Select -- </option>
                              <?php foreach($trl_data as $trl_val){ ?>
                              <option value="<?php echo $trl_val['id'] ?>"><?php echo $trl_val['trl_name'] ?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <select class="form-control search-slt select2" id="visibility" name="visibility">
                              <option value="Public">Public </option>
                              <option value="Private">Private </option>
                           </select>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1 mt-2">
                           <input type="checkbox" class="" name="if_funding" id="if_funding"  value="1"  />
                           &nbsp;<label class="form-control-placeholder" for="cname" style="color:#FFF;  margin: 5px !important">Funded Challenges Only</label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1 mt-2">
                           <input type="checkbox" class="" name="if_exclusive" id="if_exclusive"  value="1"  />
                           &nbsp;<label class="form-control-placeholder" for="cname" style="color:#FFF; margin: 5px !important">Exclusively On TechNovuus</label>
                        </div>
                        <input type="hidden" class="" name="mode" id="mode"  value="open"  />
                        <div class="col-lg-3 col-md-3 col-sm-12 p-1">
                           <button type="button" class="btn btn-primary" id="search-challenge">Search</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </section>
   </div>
   <div class="container">
      <div class="row">
         <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
      </div>
      <div class="sortable-masonry1">
         <div class="filters">
            <ul class="filter-tabs filter-btns clearfix text-center">
                <li class="filter ch_category main-catx active" id="ch_cat_id_0" data-role="button" data-filter=".all" data-id="0" onclick="getChallengeDataAjax(0, '<?php echo $limit; ?>', 1, 0,'<?php echo 0; ?>')">All Challenges</li>
               <?php if(count($audience_data) > 0){            
                  foreach($audience_data as $get_pref_detail){             
                  ?>
               <li class="filter ch_category main-catx" id="ch_cat_id_<?php echo $get_pref_detail['id']  ?>"  data-role="button" data-filter=".p1" data-id="<?php echo $get_pref_detail['id']  ?>" onclick="getChallengeDataAjax(0, '<?php echo $limit; ?>', 1, 0,'<?php echo $get_pref_detail['id']  ?>')">
                  <?php echo $get_pref_detail['preference_name']  ?>
               </li>
               <?php 	}
                  } ?>           
            </ul>
         </div>
         <div class="product_details">
            <ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
               <li class="nav-item">
                  <a class="nav-link tabLink active" data-toggle="tab1" data-id="open" href="javascript:void(0)">Open Challenge</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link tabLink" data-toggle="tab1" data-id="closed" href="<?php echo base_url('challenge/closed'); ?>">Closed Challenge</a>
               </li>
            </ul>
            <div class="tab-content" id="myTabContent">
               <div class="tab-pane fade show active" id="open">
                  <div class="container">
                     <div class="row justify-content-center">
                        <div class="col-md-12">
                           <div class="row items-containerx clearfix challengeCardsList" id="challenge_outer">
                          
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade show" id="closed">
                  <div class="container">
                     <div class="row justify-content-center">
                        <div class="col-md-12">
                           Closed Challenge
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php
   if (count($consortium_list) > 0) {
   ?>
<!--Client Logo Section-->
<section id="partners" data-aos="fade-down">
   <div class="container clientLogo2">
      <div class="row title-bar">
         <div class="col-md-12">
            <h2>Technology Platform Partners</h2>
            <div class="heading-border"></div>
         </div>
         <div id="client-logo25" class="clients-logo owl-carousel owl-theme">
            <?php foreach ($consortium_list as $consrotium) : ?>
            <div class="col-sm-12"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></div>
            <?php endforeach; ?>
         </div>
      </div>
   </div>
</section>
<?php } ?>
<section class="ministrylogo">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center title-bar">
            <h4>Under Aegis of</h4>
            <div class="heading-border"></div>
            <img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo">
         </div>
      </div>
   </div>
</section>
<!--Client Logo Section Ends-->
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php $this->load->view('front/layouts/site_popups'); ?>
<script type="text/javascript">
   function get_more_apply_css()
   {
   	$('.thumbnail-blogs').hover(
   		function() {
   			$(this).find('.caption').slideDown(250)
   		},
   		function() {
   			$(this).find('.caption').slideUp(205)
   		}
   		);
   }
   
   function get_alert_text(){
   	
   	// Read More/Less
       $(".content-new").show();
   	  $(".show_hide_new").on("click", function() {				
   		$(this).parent().next(".content-new").toggle();			
   		if ($(this).text().trim() == "Read More") {
   		  $(this).text("Read Less");
   		} else {
   		  $(this).text("Read More");
   		}
   	  });
   }
   
     $(document).ready(function () { 
   
   	$('.datepicker').datepicker({
   	   format: 'dd-mm-yyyy',
   	   //minDate: '0',
   	   //startDate:'+0d',
   	   autoclose: true
      });
      
      
      // Read More/Less
       $(".content-new").show();
   	  $(".show_hide_new").on("click", function() {	
   		console.log('HELLO');
   		$(this).parent().next(".content-new").toggle();			
   		if ($(this).text().trim() == "Read More") {
   		  $(this).text("Read Less");
   		} else {
   		  $(this).text("Read More");
   		}
   	  });
      
       
   	// Main Category Filter
   	$(document).on('click','.main-cat',function(){	
   		var base_url = '<?php echo base_url(); ?>';
   		var cat_val = 	$(this).attr('data-id');
   		var cs_t = 	$('.token').val();
   		$(this).siblings().removeClass('active');
   		$(this).addClass('active');	
   		$.ajax({
   			type:'POST',
   			url: base_url+'challenge/category_filter',
   			data:'cat_id='+cat_val+'&csrf_test_name='+cs_t+'&mode=open',				
   			dataType:"text",
   			success:function(data){
   				var output = JSON.parse(data);						
   				$(".token").val(output.token);
   				$('.challengeCardsList').html(output.html);
   
   				get_more_apply_css();
   				get_alert_text();
   			}
   		});
   	});
      
   	// Form Filter
   	$(document).on('click','#search-challenge',function(){
   		$("ul.filter-tabs").find('li').removeClass('active');	
   		var cs_t = 	$('.token').val();
   		var base_url = '<?php echo base_url(); ?>'; 
   		var ch_date = $('#ch_date').val();
   		var technologys = $('#technologys').val();
   		var tags = $('#tags').val();
   		var audience = $('#audience').val();
   		var trls = $('#trls').val();
   		var visibility = $('#visibility').val();
   		var if_funding = $('#if_funding').val();
   		var if_exclusive = $('#if_exclusive').val();
   		
   		if ($('#if_funding').is(':checked')) {
   			//alert($('#if_funding').val() + ' is checked');
   		}
   		
   		if ($('#if_exclusive').is(':checked')) {
   			//alert($('#if_exclusive' ).val() + ' is checked');
   		}
   		
   		$.ajax({
   			type:'POST',
   			url: base_url+'challenge/filter_more',
   			//data:'filter_date='+ch_date+'&csrf_test_name='+cs_t+'&technologys',
   			data: $('#filterData').serialize(),
   			dataType:"text",
   			success:function(data){
   				var output = JSON.parse(data);						
   				$(".token").val(output.token);
   				$('.challengeCardsList').html(output.html);
   
   				get_more_apply_css();
   				get_alert_text();
   			}
   		});
   		
   	 });
   	 
   	// Load More Code 
   	$(document).on('click','.show_more',function(){
   		// $(".token").remove();
   		var base_url = '<?php echo base_url(); ?>'; 
   		var ID = $(this).attr('id');
   		var length = $('.p3').length;
   		var cs_t = 	$('.token').val();
   		$('.show_more').hide();
   		$('.loding').show();
   		$.ajax({
   			type:'POST',
   			url: base_url+'challenge/getmore',
   			data:'id='+length+'&csrf_test_name='+cs_t+'&mode=open',
   			dataType:"text",
   			success:function(data){
   				//console.log(data);
   				var output = JSON.parse(data);
   				if(output.html == ""){
   					$(".show_more_main").remove();
   				}	
   				$(".token").val(output.token);								
   				$('#show_more_main'+ID).remove();				
   				$('.challengeCardsList').append(output.html); 	
   
   				get_more_apply_css();
   				get_alert_text();					
   
   			}
   		});
   	});
    
     });
     
   
     
   $('.select2').select2({});
   $(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
   $(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });

   function openNav() 
   {     
      var isMobile = false; //initiate as false
      // device detection
      if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
         isMobile = true;
         document.getElementById("mySidenav").style.width = "100%";
         } else {
         document.getElementById("mySidenav").style.width = "25%";
      }
      
   }	

   function closeNav() 
   {
      document.getElementById("mySidenav").style.width = "0";     
   }

   function show_more_technology(val)
   {
      if(val == 'show')
      {
         $( ".filter_technology" ).removeClass( "d-none" )
         $( ".filter_load_less_tech" ).removeClass( "d-none" )
         $( ".filter_load_more_tech" ).addClass( "d-none" )
      }
      else if(val == 'hide')
      {
         $( ".filter_technology" ).addClass( "d-none" )
         $( ".filter_load_less_tech" ).addClass( "d-none" )
         $( ".filter_load_more_tech" ).removeClass( "d-none" )
      }
   }

   function show_more_tag(val)
   {
      if(val == 'show')
      {
         $( ".filter_tag" ).removeClass( "d-none" )
         $( ".filter_load_less_tag" ).removeClass( "d-none" )
         $( ".filter_load_more_tag" ).addClass( "d-none" )
      }
      else if(val == 'hide')
      {
         $( ".filter_tag" ).addClass( "d-none" )
         $( ".filter_load_less_tag" ).addClass( "d-none" )
         $( ".filter_load_more_tag" ).removeClass( "d-none" )
      }
   }

   function show_more_trl(val)
   {
      if(val == 'show')
      {
         $( ".filter_trl" ).removeClass( "d-none" )
         $( ".filter_load_less" ).removeClass( "d-none" )
         $( ".filter_load_more" ).addClass( "d-none" )
      }
      else if(val == 'hide')
      {
         $( ".filter_trl" ).addClass( "d-none" )
         $( ".filter_load_less" ).addClass( "d-none" )
         $( ".filter_load_more" ).removeClass( "d-none" )
      }
   }
   getChallengeDataAjax(0,'<?php echo $limit; ?>', 0, 0,0);

   function clear_search_form() 
   { 
      $("#challenge_outer").html('');
      $("#search_txt").val('');  
      $('.search_trl:checkbox').prop('checked',false); 
      $('.search_technology:checkbox').prop('checked',false); 
      $('.search_tag:checkbox').prop('checked',false); 
      $('.search_chal_type:checkbox').prop('checked',false); 
      $('.funding_type:checkbox').prop('checked',false); 
      $('.is_exclusive:checkbox').prop('checked',false); 
      getChallengeDataAjax(0, '<?php echo $limit; ?>', 0, 0,0);
   }
   function getChallengeDataAjax( nf_start, nf_limit, is_search, is_show_more,category_id=null,challenge_status="open")
   {
      $("#showMoreBtn").remove();
      var cs_t =  $('.token').val();
      if (category_id==null) {
         category_id=$(".ch_category.active").data('id')
      }
      $(".ch_category").removeClass('active');
      $("#ch_cat_id_"+category_id).addClass('active');
      
      var selected_trl = [];
      $.each($("input.search_trl:checked"), function() { selected_trl.push($(this).val()); });
      
      var selected_technology = [];
      $.each($("input.search_technology:checked"), function() { selected_technology.push($(this).val()); });
      
      var selected_tag = [];
      $.each($("input.search_tag:checked"), function() { selected_tag.push($(this).val()); });
      
      var selected_chal_type = [];
      $.each($("input.search_chal_type:checked"), function() { selected_chal_type.push($(this).val()); });

      var funding_type= $('input[name="funding_type"]:checked').val();
      var is_exclusive= $('input[name="is_exclusive"]:checked').val();
      var keyword = encodeURIComponent($('#search_txt').val());
      var trl = encodeURIComponent(selected_trl);  
      var tag = encodeURIComponent(selected_tag);
      var challenage_type = encodeURIComponent(selected_chal_type);
      var selected_technology = encodeURIComponent(selected_technology);
      var close_date = $('#close_date').val();
      
      parameters= {  'nf_start':nf_start, 'nf_limit':nf_limit, 'is_show_more':is_show_more,'category_id':category_id, 'keyword':keyword, 'trl':trl, 'tag':tag, 'challenage_type':challenage_type, 'close_date':close_date,'funding_type':funding_type,'technology':selected_technology,'is_exclusive':is_exclusive,'challenge_status':challenge_status ,'cs_t':cs_t }
      $("#preloader-loader").show();
      $.ajax( 
      {
         type: "POST",
         url: "<?php echo site_url('challenge/getChalengeDataAjax'); ?>",
         data: parameters,
         cache: false,
         dataType: 'JSON',
         success:function(data)
         {
            if(data.flag == "success")
            {
               if(is_search == '1') { $("#challenge_outer").html(''); }
               $(".token").val(data.csrf_new_token)
               $("#challenge_outer").append(data.challenge_html);
            }
            else { sweet_alert_error("Error Occurred. Please try again."); }
            $("#preloader-loader").hide();
         }
      });
   }
</script>