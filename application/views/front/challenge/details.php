<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<?php 
	error_reporting(0);
	$datestr = $challenge_data[0]['challenge_close_date'];//Your date
	$date	 = strtotime($datestr);//Converted to a PHP date (a second count)
	
	//Calculate difference
	$diff	= 	$date-time();//time returns current time in seconds
	$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
	//$hours	=	round(($diff-$days*60*60*24)/(60*60));
	$style="";
	
	if($challenge_data[0]['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $challenge_data[0]['challenge_close_date'])
	{ 
							
		$date1=date_create(date("Y-m-d"));
		$date2=date_create($challenge_data[0]['challenge_close_date']);
		$diff=date_diff($date1,$date2);
		$showCnt = "Days left : ".($diff->format("%a")+1);		
		$digitShow = 1;
	} 
	else 
	{ 	
		$digitShow = 0;
		$showCnt = "Participation Closed";
	}	
	
	$bannerImg = base_url('assets/challenge/'.$challenge_data[0]['banner_img']);
	
	
	
	if($challenge_data[0]['company_logo']!=""){
		$fileExist = base_url('assets/challenge/'.$challenge_data[0]['company_logo']);
		$image = $fileExist;
		
		}else {
		
		if($challenge_data[0]['challenge_id'] == 'TNCID-000018'){
			$image = base_url('assets/arai_site.jpg');
			}else {
			$image = base_url('assets/not_available.png');
		}
		
	}

?>

<style>
.search-sec {background: #c80032;}
.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block;
font-weight: bold; padding: 10px 8px; border-radius: 0px;}
   
.mCSB_inside>.mCSB_container { padding:0 !important;}

   
.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important;}

.home-p.pages-head4 {
background: url(<?php echo $bannerImg; ?>) no-repeat center top;
background-size: 100% 100% !important; min-height: 100%;
}
#preloader-loader {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
z-index: 9999;
overflow: hidden;
background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
content: "";
position: fixed;
top: calc(50% - 30px);
left: calc(50% - 30px);
border: 6px solid #f2f2f2;
border-top: 6px solid #c80032;
border-radius: 50%;
width: 60px;
height: 60px;
-webkit-animation: animate-preloader 1s linear infinite;
animation: animate-preloader 1s linear infinite;
}

table#show-hide{display:none;}



#ParticipateModal .modal-header { border:1px solid #dee2e6; }
#ParticipateModal .agreement_content .agreement_content_inner { width: 100%; border: 1px solid #d3d3d3; border-radius: 5px; padding: 20px; }
#ParticipateModal .agreement_content .agreement_content_inner h4 { margin: 0 0 10px 0;text-align: center;font-size: 18px; }
#ParticipateModal .agreement_content .agreement_content_inner p { font-size: 14px; margin: 5px 0 20px 0; text-align: justify; word-break: break-all; white-space: normal; word-wrap: anywhere; }
#ParticipateModal .agreement_content .agreement_content_inner p.byt_terms_outer { margin-bottom:0; } 
#ParticipateModal .agreement_content .agreement_content_inner p.byt_terms_outer a { font-weight: 500; text-decoration: none; } 
#ParticipateModal #participate_form { padding:20px 20px 20px 20px; }	
/***********************/

/************* Challenge Closure CSS *****************/
#challenges-page2.owl-carousel .owl-dot,#challenges-page2.owl-carousel .owl-nav .owl-next,#challenges-page2.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
#challenges-page2.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
#challenges-page2.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
#challenges-page2 .owl-nav.disabled{display:block}
#challenges-page2 .owl-nav{position:absolute; top:40%; width:100%}
#challenges-page2 .owl-nav .owl-prev{left:-30px; position:absolute; background: #FFF;}
#challenges-page2 .owl-nav .owl-next{right:-30px; position:absolute; background: #FFF;}
#challenges-page2 .owl-item{ padding: 0 5px;}
#challenges-page2 .owl-item img{width: 100% !important; border-radius: 0; height: auto !important;     min-height: 228px !important;}


#challenges-page2 .owl-item{ padding: 0 5px;}
#challenges-page2 iframe{ width: 100%; min-height:235px; max-height:235px; padding: 0; margin: 0;}
.descriptionBox{padding:15px;border:1px solid #ccc; height: 140px;}
.descriptionBoxImg{padding:15px;border:1px solid #ccc}

.addMoreInfo{ display: block; margin: 15px 0 0 0;}
.addMoreInfo ul{ list-style: none; text-align: center; padding: 0; margin: 0;}
.addMoreInfo ul li{ display: inline-block; padding: 0; margin: 0 11px 0 0;}
.addMoreInfo a{text-decoration:none; padding:10px 15px; color:#fff;background:#e23751; text-align:center; border-radius:.25rem;}
.addMoreInfo a:hover{ text-decoration: none; color: #FFF; background: #333; }

.boxAll{box-shadow: 0 0px 15px 0 rgba(0, 0, 0, 0.2); transition: all .4s ease; padding: 25px;margin: 0 0 15px 0;}
.boxAll:hover{box-shadow:0 1px 3px 0 rgba(0,0,0,.21);transition:all .4s ease}
.owner-teammember a {
    text-decoration: none;
    border-radius: 0;
    padding: 0;
    color: #fff;
    background: #FFF;
    /* display: block; */
    text-align: center;
    margin-bottom: 0;
}
.owner-teammember img {
    width: 100% !important;
    height: 100% !important;
    border-radius: 0;
    margin: 0 auto;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down">
	<div class="container">     
	</div>
</div>

<!--====================================================
	CHALLENGE DETAILS WITH TAB STRUCTURE
======================================================-->
<section id="challenge-intro">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="img-box">
					<img src="<?php echo $image; ?>" class="img-fluid" alt="img">
				</div>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-6">
						<div class="title-bar">
							<div class="title">Proposed by ARAI</div>
							<h3><?php echo ucwords($challenge_data[0]['challenge_title']) ?></h3>
							<div class="heading-border-light"></div>
						</div>
						<?php if($challenge_data[0]['challenge_id']): ?>
						<div class="challenge-code">
							
							<?php echo $challenge_data[0]['challenge_id']; ?>
							
						</div>
						<?php endif; ?>
						
						<ul class="list-group list-group-horizontal">
							<?php /*if($digitShow > 0)
								{  ?>
								<a href="javascript:void(0)" class="btn btn-general btn-green" onclick="open_ParticipateModal()">Participate</a>
								<?php } 
								else 
								{ ?>
								<a href="javascript:void(0);" class="btn btn-general btn-green" role="button">Participation Closed</a>	
							<?php } */ ?>
							
							<?php if($digitShow > 0)
								{  
								$ch_visible = $challenge_data[0]['challenge_visibility'];
								$ch_id_n = $challenge_data[0]['c_id'];
								
									if($ch_visible == 'Private'){
									
										$accessAccepted = $this->master_model->getRecords("challenge_details_request", array("c_id" =>$ch_id_n, "sender_id" =>  $this->session->userdata('user_id')));
										if($accessAccepted[0]['status'] == 'Approved'){
											?>
											<a href="javascript:void(0)" class="btn btn-general btn-green" onclick="open_ParticipateModal()">Participate</a>
											<?php
										}
									} else {
										?>
										<a href="javascript:void(0)" class="btn btn-general btn-green" onclick="open_ParticipateModal()">Participate</a>
										<?php
									}
								 } 
								else 
								{ ?>
								<a href="javascript:void(0);" class="btn btn-general btn-green" role="button">Participation Closed</a>	
							<?php } ?>
						</ul>
						
					</div>
					<div class="col-md-6">
						<ul class="list-unstyled product_info">
							<li>
								<label>Teams:</label>
							<span> <a href="javascript:voiod(0);"><?php echo $challenge_data[0]['min_team'] ?> - <?php echo $challenge_data[0]['max_team'] ?> People</a></span></li>
							<li>
							<i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></li>
						</ul>
						<p><?php 
							
							/*$limit = 250;
							$str = $challenge_data[0]['company_profile'];
							if (strlen($challenge_data[0]['company_profile'] ) > $limit){
								$str  = substr($challenge_data[0]['company_profile'] , 0, strrpos(substr($challenge_data[0]['company_profile'] , 0, $limit), ' ')) . '...'; 
							}*/
							
						echo ucwords($challenge_data[0]['company_name']); ?></p>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</div>
</section>
<section>
	<div class="product_details">
		<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
			<li class="nav-item">
				<a class="nav-link tabLink active" data-toggle="tab" data-id="technology" href="#technology">About</a>
			</li>
			<li class="nav-item">
				<a class="nav-link tabLink" data-toggle="tab" data-id="features" href="#features">Challenge Info</a>
			</li>        
			<li class="nav-item">
				<a class="nav-link tabLink" data-toggle="tab" data-id="validation_achive" href="#validation_achive">Challenge Details</a>
			</li>		
			<?php 
				if($this->session->userdata('user_id') != $challenge_data[0]['u_id']){
					
					if($challenge_data[0]['share_details'] == '1'){ ?>     
						<li class="nav-item">
							<a class="nav-link tabLink" data-id="intellectual_Property" data-toggle="tab" href="#intellectual_Property">Contact Details</a>
						</li>
				<?php }
					
				}
			?>  
			<li class="nav-item">
				<a class="nav-link tabLink" data-id="teams" data-toggle="tab" href="#teams">Teams</a>
			</li>
			<li class="nav-item">
				<a class="nav-link tabLink" data-id="chupdate" data-toggle="tab" href="#closureupdates">Challenge Updates</a>
			</li>	
		</ul>
		
		<div class="tab-content" id="myTabContent">
		
			<div class="tab-pane fade show active" id="technology">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<?php if($challenge_data[0]['company_profile']!=""){ echo $challenge_data[0]['company_profile'];} else { echo "<p style='width:100%; text-align:center;font-weight:bold;'>No Details Found.</p>";} ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="tab-pane fade" id="features">
				<div class="container">
					<div class="row justify-content-center">             
						<div class="col-md-12">							
							<table class="table table-bordered">
								<tr>
									<td width="20%"><b>Brief Info About Challenge :</b></td>
									<td><?php echo ucfirst($challenge_data[0]['challenge_details']) ?></td>
								</tr>
								<tr>
									<td><b>Launch Date :</b></td>
									<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_launch_date'])); ?></td>
								</tr>
								<tr>
									<td><b>Close Date :</b></td>
									<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_close_date'])); ?></td>
								</tr>
								<tr>
									<td><b>Technology :</b></td>
									<td><?php echo $challenge_data[0]['technology_name']; 
										if(in_array("0", $challenge_data[0]['res_tech_id'])){ echo ', Other'; }
									?></td>
								</tr>
								<?php 
									
									if(in_array("0", $challenge_data[0]['res_tech_id']))
									{ 
									?>
									<tr>
										<td><b>Other Technology :</b></td>
										<td><?php echo $challenge_data[0]['other_technology_name']; ?></td>
									</tr>
								<?php } ?>
								<tr>
									<td><b>Audience Preference :</b></td>
									<td><?php echo $challenge_data[0]['preference_name']; ?></td>
								</tr>
								
							</table>
								
						</div>
					</div>
				</div>
			</div>    
			
			<div class="tab-pane fade" id="validation_achive">
				<div class="container">
					<div class="row">
						<div class="col-md-12">	<!--- Challenge Detail Start ---->

							<?php		
						if($this->session->userdata('user_id') == ""){ 
						?>
						
						<div class="row justify-content-center">			
							<div class="row"><h4>Ask challenge details?</h4></div>
							<p>This challenge is marked as "PRIVATE" by the challenge owner. <br />
							To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
						</div>
						<div class="row justify-content-center" align="center">				
							<a href="<?php echo base_url('login'); ?>" class="btn btn-general btn-green" role="button">REQUEST ACCESS</a>
						</div>
						<?php		
							} else {  // Login Users
							
							$style = 'display:none';
							if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
								$style = 'display:block'; 
							}
							
							$anchor = 'display:block'; 	
							if(count($request_user) > 0){
								$status_check = $request_user[0]['status']; 
								if($status_check == 'Approved'){
									$style = 'display:block'; 
									$anchor = 'display:none'; 
									} else {
									$style = 'display:none'; 
									$anchor = 'display:block';
								}
								
								} else { 
								$status_check = 'REQUEST ACCESS'; 
							}
							
							if($challenge_data[0]['challenge_visibility'] == 'Public'){ // Public Details
								
							?>					
							<table class="table table-bordered">
								<tr>
									<td width="30%"><b>Abstract About Challenge :</b></td>
									<td><?php echo ucfirst($challenge_data[0]['challenge_abstract']) ?></td>
								</tr>
								<tr>
									<td width="30%"><b>Tags :</b></td>
									<td><?php echo $challenge_data[0]['tag_name']; 
									if(in_array("0", $challenge_data[0]['res_tag_id'])){ echo ', Other'; } ?></td>
								</tr>
								<?php 
									if(in_array("0", $challenge_data[0]['res_tag_id']))
									{
									?>
									<tr>
										<td width="30%"><b>Other Tags :</b></td>
										<td><?php echo $challenge_data[0]['other_tag_name']; ?></td>
									</tr>
								<?php } ?>
								
								<?php if($challenge_data[0]['if_funding'] =='Funding' ): ?>
								<tr>
									<td width="30%"><b>Funding Amount(In INR) :</b></td>
									<td><?php echo $challenge_data[0]['is_amount']; ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if($challenge_data[0]['if_reward'] =='Reward' ): ?>
								<tr>
									<td width="30%"><b>Rewards Details:</b></td>
									<td><?php echo $challenge_data[0]['fund_reward']; ?></td>
								</tr>
								<?php endif; ?>								
								<tr>
									<td width="30%"><b>Expected TRL Solution:</b></td>
									<td><?php echo $challenge_data[0]['trl_solution']; ?></td>
								</tr>
								<tr>
									<td width="30%"><b>IP Clause: </b></td>
									<td><?php echo $challenge_data[0]['ip_name']; ?></td>
								</tr>								
							</table>							
							<table class="table table-bordered" id="show-hide">
								<tr>
									<td width="30%"><b>Educational:</b></td>
									<td><?php echo $challenge_data[0]['education']; ?></td>
								</tr>
								<tr>
									<td width="30%"><b>From Age:</b></td>
									<td><?php echo $challenge_data[0]['from_age']; ?></td>
								</tr>
								<tr>
									<td width="30%"><b>To Age:</b></td>
									<td><?php echo $challenge_data[0]['to_age']; ?></td>
								</tr>
								<tr>
									<td width="30%"><b>Domain:</b></td>
									<td><?php echo $challenge_data[0]['domain_name']; ?></td>
								</tr>
								<tr>
									<td width="30%"><b>Geographycal States:</b></td>
									<td><?php echo $challenge_data[0]['state_name']; ?></td>
								</tr>								
								
								<?php if($challenge_data[0]['terms_txt'] !="" ): ?>
								<tr>
									<td width="30%"><b>Terms & Conditions: </b></td>
									<td><?php echo $challenge_data[0]['terms_txt']; ?></td>
								</tr>
								<?php endif;  ?>
								<?php if($challenge_data[0]['future_opportunities'] !="" ): ?>
								<tr>
									<td width="30%"><b>Future Opportunities: </b></td>
									<td><?php echo $challenge_data[0]['future_opportunities']; ?></td>
								</tr>
								<?php endif;  ?>
								
								
								<?php if($challenge_data[0]['is_external_funding'] == '1' ): ?>
								<tr>
									<td width="30%"><b>Percentage of Funding(%) :</b></td>
									<td><?php echo $challenge_data[0]['external_fund_details']; ?></td>
								</tr>
								<?php endif;  ?>
								<tr>
									<td width="30%"><b>Is this challenge exclusively listed on Technovuus?: </b></td>
									<td><?php  /*if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Exclusively listed on Technovuus'; else: echo 'Also listed on other platforms'; endif;*/ 
									if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Yes'; else: echo 'No'; endif; 
									?></td>
								</tr>	
								<?php if($challenge_data[0]['is_exclusive_challenge'] == '0' ): ?>
								<tr>
									<td width="30%"><b>Challenge Exclusively Content :</b></td>
									<td><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></td>
								</tr>
								<?php endif; ?>
							</table>
							<div class="row">
								<a href="javascript:void(0);" class="btn-toggle-show btn btn-info btn-sm" style="margin-left: 14px;">Show More</a>
									<a href="javascript:void(0);" class="btn-toggle-hide btn btn-info btn-sm" style="display:none; margin-left: 14px;">Show Less</a>
							</div> 
							
							<?php	} else { 
								
								$myCss = '';
								$otherCss = "display:none;";
								if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
									$myCss = "display:none;";
									$otherCss = "display:block;";									
								}
								$rejectClass = "";
								if($request_user[0]['status'] == 'Rejected'){
									$rejectClass = "display:none;";
								} 
								
								if($request_user[0]['status'] == 'Pending'){
									$rejectClass = "display:none;";
								}
							?>
							<div style="<?php echo $myCss; ?>">
								<div class="row justify-content-center" style="<?php echo $rejectClass; ?>">			
									<h4 style="<?php echo $anchor; ?>">Ask challenge details?</h4>
									<p style="<?php echo $anchor; ?>">This challenge is marked as "PRIVATE" by the challenge owner. To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
								</div>
								<div class="row justify-content-center" align="center" >				
									<a href="javascript:void(0);" data-cid="<?php echo $challenge_data[0]['c_id']  ?>" data-oid="<?php echo $challenge_data[0]['u_id']  ?>" style="<?php echo $anchor; ?>" class="btn btn-general btn-green ask-details" onClick="return get_challenge_details(<?php echo $challenge_data[0]['c_id']  ?>, <?php echo $challenge_data[0]['u_id']  ?>, <?php echo count($request_user) ?>)" role="button"><?php echo $status_check ?></a>
								</div>
							</div>
							<?php 	
							
							if(($this->session->userdata('user_id') == $request_user[0]['sender_id']) && $request_user[0]['status'] == 'Approved')
							{ 
								$otherCss = "display:block;";
							}
							
							?>	
								<div style="<?php echo $otherCss; ?>">
									<table class="table table-bordered">
									<tr>
										<td width="20%"><b>Abstract About Challenge :</b></td>
										<td><?php echo ucfirst($challenge_data[0]['challenge_abstract']) ?></td>
									</tr>
									<tr>
										<td><b>Tags :</b></td>
										<td><?php echo $challenge_data[0]['tag_name']; 
										if(in_array("0", $challenge_data[0]['res_tag_id'])){ echo ', Other'; } ?></td>
									</tr>
									<?php 
										if(in_array("0", $challenge_data[0]['res_tag_id']))
										{
										?>
										<tr>
											<td><b>Other Tags :</b></td>
											<td><?php echo $challenge_data[0]['other_tag_name']; ?></td>
										</tr>
									<?php } ?>
									
									<?php if($challenge_data[0]['if_funding'] =='Funding' ): ?>
									<tr>
										<td><b>Funding Amount(In INR) :</b></td>
										<td><?php echo $challenge_data[0]['is_amount']; ?></td>
									</tr>
									<?php endif; ?>
									
									<?php if($challenge_data[0]['if_reward'] =='Reward' ): ?>
									<tr>
										<td><b>Rewards Details :</b></td>
										<td><?php echo $challenge_data[0]['fund_reward']; ?></td>
									</tr>
									<?php endif; ?>								
									<tr>
										<td><b>Expected TRL Solution:</b></td>
										<td><?php echo $challenge_data[0]['trl_solution']; ?></td>
									</tr>
									<tr>
										<td><b>IP Clause: </b></td>
										<td><?php echo $challenge_data[0]['ip_name']; ?></td>
									</tr>								
								</table>							
								<table class="table table-bordered" id="show-hide">
									<tr>
										<td width="20%"><b>Educational:</b></td>
										<td><?php echo $challenge_data[0]['education']; ?></td>
									</tr>
									<tr>
										<td><b>From Age:</b></td>
										<td><?php echo $challenge_data[0]['from_age']; ?></td>
									</tr>
									<tr>
										<td><b>To Age:</b></td>
										<td><?php echo $challenge_data[0]['to_age']; ?></td>
									</tr>
									<tr>
										<td><b>Domain:</b></td>
										<td><?php echo $challenge_data[0]['domain_name']; ?></td>
									</tr>
									<?php if($challenge_data[0]['state_name']!=""){ ?>
									<tr>
										<td><b>Geographycal States:</b></td>
										<td><?php echo $challenge_data[0]['state_name']; ?></td>
									</tr>								
									<?php } ?>
									<?php if($challenge_data[0]['terms_txt'] !="" ): ?>
									<tr>
										<td><b>Terms & Conditions: </b></td>
										<td><?php echo $challenge_data[0]['terms_txt']; ?></td>
									</tr>
									<?php endif;  ?>
									<?php if($challenge_data[0]['future_opportunities'] !="" ): ?>
									<tr>
										<td><b>Future Opportunities: </b></td>
										<td><?php echo $challenge_data[0]['future_opportunities']; ?></td>
									</tr>
									<?php endif;  ?>
									
									
									<?php if($challenge_data[0]['is_external_funding'] == '1' ): ?>
									<tr>
										<td><b>Percentage of Funding(%) :</b></td>
										<td><?php echo $challenge_data[0]['external_fund_details']; ?></td>
									</tr>
									<?php endif;  ?>
									<tr>
										<td><b>Is this challenge exclusively listed on Technovuus?: </b></td>
										<td><?php  /*if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Exclusively listed on Technovuus'; else: echo 'Also listed on other platforms'; endif; */
										if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Yes'; else: echo 'No'; endif; 
										?></td>
									</tr>	
									<?php if($challenge_data[0]['is_exclusive_challenge'] == '0' ): ?>
									<tr>
										<td><b>Challenge Exclusively Content :</b></td>
										<td><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></td>
									</tr>
									<?php endif; ?>
								</table>
								<div class="row">
									<a href="javascript:void(0);" class="btn-toggle-show btn btn-info btn-sm" style="margin-left: 14px;">Show More</a>
									<a href="javascript:void(0);" class="btn-toggle-hide btn btn-info btn-sm" style="display:none; margin-left: 14px;">Show Less</a>
								</div>
							</div>	
							<?php //} ?>	
							<?php
							} // Private Challenge
							
						} // Else End
						
					?>
							
						</div> <!--- Challenge Detail End ---->
						
					</div>
				</div>
			</div>
			
			<div class="tab-pane fade" id="intellectual_Property">		
				<div class="container">	
					
					<?php		
						if($this->session->userdata('user_id') == ""){ 
						?>
						
						<div class="row justify-content-center">			
							<div class="row"><h4>Ask challenge owner details.?</h4></div>
							<p>This challenge is marked as "PRIVATE" by the challenge owner. <br />
							To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
						</div>
						<div class="row justify-content-center" align="center">				
							<a href="<?php echo base_url('login'); ?>" class="btn btn-general btn-green" role="button">REQUEST ACCESS</a>
						</div>
						<?php		
							} else {  // Login Users
							
							$style = 'display:none';
							if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
								$style = 'display:block'; 
							}
							
							$anchor = 'display:block'; 	
							if(count($request_user) > 0){
								$status_check = $request_user[0]['status']; 
								if($status_check == 'Approved'){
									$style = 'display:block'; 
									$anchor = 'display:none'; 
									} else if($status_check == 'Rejected'){
									$style = 'display:none'; 
									$anchor = 'display:block'; 
									} else if($status_check == 'Pending'){
									$style = 'display:none'; 
									$anchor = 'display:block'; 
									} else {
									$style = 'display:none'; 
									$anchor = 'display:block';
								}
								
								} else { 
								$status_check = 'REQUEST ACCESS'; 
							}
							
							if($challenge_data[0]['challenge_visibility'] == 'Public'){ // Public Details
								
							?>					
							<div class="col-md-6">
								<table class="table table-bordered">
									<tr>
										<td><b>Contact Person Name :</b></td>
										<td><?php echo $challenge_data[0]['contact_person_name']; ?></td>
									</tr>
									<tr>
										<td><b>Email ID </b></td>
										<td><?php echo $challenge_data[0]['email_id']; ?></td>
									</tr>
									<tr>
										<td><b>Mobile Number </b></td>
										<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
									</tr>
									<tr>
										<td><b>Office Number</b></td>
										<td><?php echo $challenge_data[0]['office_no']; ?></td>
									</tr>
								</table>
							</div> 
							
							<?php	} else { 
								
								$rejectClass = "";
								if($request_user[0]['status'] == 'Rejected'){
									$rejectClass = "display:none;";
									//$anchor = 'display:block';
								}
								
								if($request_user[0]['status'] == 'Pending'){
									$rejectClass = "display:none;";
								}
							?>
							<div style="<?php echo $rejectClass; ?>">
							<div class="row justify-content-center">			
								<h4 style="<?php echo $anchor; ?>">Ask challenge owner details?</h4>
								<p style="<?php echo $anchor; ?>">This challenge is marked as "PRIVATE" by the challenge owner. To view challenge details and participate, kindly click the "REQUEST ACCESS" Button.</p>
							</div>
							</div>
							<div class="row justify-content-center" align="center">				
								<a href="javascript:void(0);" data-cid="<?php echo $challenge_data[0]['c_id']  ?>" data-oid="<?php echo $challenge_data[0]['u_id']  ?>" style="<?php echo $anchor; ?>" class="btn btn-general btn-green ask-details" onClick="return get_challenge_details(<?php echo $challenge_data[0]['c_id']  ?>, <?php echo $challenge_data[0]['u_id']  ?>, <?php echo count($request_user) ?>)" role="button"><?php echo $status_check ?></a>
							</div>
							<?php if(($this->session->userdata('user_id') == $request_user[0]['sender_id']) && $request_user[0]['status'] == 'Approved'){ ?>	
								<div class="col-md-6">
									<table class="table table-bordered">
										<tr>
											<td><b>Contact Person Name :</b></td>
											<td><?php echo $challenge_data[0]['contact_person_name']; ?></td>
										</tr>
										<tr>
											<td><b>Email ID </b></td>
											<td><?php echo $challenge_data[0]['email_id']; ?></td>
										</tr>
										<tr>
											<td><b>Mobile Number </b></td>
											<td><?php echo $challenge_data[0]['mobile_no']; ?></td>
										</tr>
										<tr>
											<td><b>Office Number</b></td>
											<td><?php echo $challenge_data[0]['office_no']; ?></td>
										</tr>
									</table>
								</div>	
							<?php } ?>	
							<?php
							} // Private Challenge
							
						} // Else End
						
					?>
					
				</div> 
				
			</div>
			
			<div class="tab-pane fade" id="teams">
				<div class="filterBox">
         <section class="search-sec">
            <div id="filterData" name="filterData">
               <div class="container">   
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row d-flex justify-content-center search_Box">                       
                           <div class="col-md-6">
                              <input type="text" class="form-control search-slt search_txt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                              <button type="button" class="btn btn-primary searchButton" onclick="load_team_listing_ajax(0,<?php echo $team_limit ?>,1)"><i class="fa fa-search" aria-hidden="true"></i></button>
                           </div>
                           <div class="clearAll">
                              <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form()">Clear All</a>
                           </div>
                           <div class="col-md-12"></div>                      
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
				<div class="container">
					<div class="row teams-listing" id="team_listing_outer">
						
							
					</div> 
				
				</div>
			</div>
			
			<div class="tab-pane fade" id="closureupdates">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<?php
							if(count($closureUpdates)>0){
								
								foreach($closureUpdates as $showUpdate){
									
									$getImages = $this->master_model->getRecords("closure_update_files", array("cu_id" => $showUpdate['id']));
									$imageVideo = "";
									$documentsShow = "";	
									foreach($getImages as $showImageContent){
										if($showImageContent['upload_type'] == 'Image'){	
												/*$imageVideo .= '<a href="'.base_url('assets/challenge/'.$showImageContent['upload_name']).'" target="_blank" title="" alt="" ><img src="'.base_url('assets/challenge/'.$showImageContent['upload_name']).'" class="" style="max-width:100%; max-height:100%; width:100px; height:80px; margin-left: 10px; border-radius: 20px;" class="img-fluid" /></a>';*/
												$img_url_ch =base_url('assets/challenge/'.$showImageContent['upload_name']);
												$imageVideo .= '<div class="owner-teammember '.$showUpdate['id'].'"><a href="  '.$img_url_ch.' " data-fancybox="gallery_images" data-caption=""><img src="'.base_url('assets/challenge/'.$showImageContent['upload_name']).'" class="img-fluid" /></a></div>';
										} 
										if($showImageContent['upload_type'] == 'Video'){
											
											$youtubeLine = $showImageContent['upload_name'];
											preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $youtubeLine, $matches);
											$youtubeid = $matches[1];
											/*$imageVideo .= '<a href="'.$youtubeLine.'" target="_blank" title="Youtube" alt="Youtube" ><img src="http://img.youtube.com/vi/'.$youtubeid.'/hqdefault.jpg" height="80px;" width="100px" style="margin:5px 5px 5px 5px; max-width:100%; max-height:100%; border-radius: 20px;"/></a>';
											$imageVideo .= '<div class="video-container">
											<iframe id="ytplayer" type="text/html" 
											src="https://www.youtube.com/embed/'.$youtubeid.'?rel=0&showinfo=0&color=white&iv_load_policy=3"
											frameborder="0" allowfullscreen></iframe></div>';*/
											
											$imageVideo .= '<div class="owner-teammember '.$showUpdate['id'].'"><iframe src="https://www.youtube.com/embed/'.$youtubeid.'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
										}
										
										if($showImageContent['upload_type'] == 'Document'){
											
											$file_parts = pathinfo($showImageContent['upload_name']);
											
											if($file_parts['extension'] == 'pdf' || $file_parts['extension'] == 'PDF'){					
											
												/*$documentsShow .= '<a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 24px; margin: 5px 5px 5px 5px;"></i></a>';*/
												$documentsShow .= '<li><a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank">'.strtoupper($file_parts['extension']).' <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
</a></li>';
											} 
											
											else if($file_parts['extension'] == 'xls' || $file_parts['extension'] == 'xlsx'){ 
												/*$documentsShow .= '<a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 24px; margin: 5px 5px 5px 5px;"></i></a>';*/
												
												$documentsShow .= '<li><a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank">'.strtoupper($file_parts['extension']).' <i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a></li>';
											}
											
											else if($file_parts['extension'] == 'doc' || $file_parts['extension'] == 'DOC' || $file_parts['extension'] == 'docx' || $file_parts['extension'] == 'DOCX'){ 
												/*$documentsShow .= '<a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank"><i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 24px; margin: 5px 5px 5px 5px;"></i></a>';*/
												
												$documentsShow .= '<li><a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank">'.strtoupper($file_parts['extension']).' <i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a></li>';
											}
											
											else {
												
												/*$documentsShow .= '<a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank"><i class="fa fa-files-o" aria-hidden="true" style="font-size: 24px; margin: 5px 5px 5px 5px;"></i></a>';*/
												
												$documentsShow .= '<li><a href="'.base_url('challenge/documentDownload/'.base64_encode($showImageContent['upload_name'])).'" title="'.strtoupper($file_parts['extension']).'" alt="'.strtoupper($file_parts['extension']).'" target="_blank">'.strtoupper($file_parts['extension']).' <i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a></li>';
												
												
											}
											
										} 
											
									} // Foreach End 
									
								?>
							<div class="boxAll">
								<div class="row">
									<div class="col-md-3">
										<div id="challenges-page2" >
										<?php echo $imageVideo;  ?>
										</div>
									</div>
									<div class="col-md-9">
										<h3 class="mt-2"><?php echo $showUpdate['closure_title']; ?></h3>
										<ul class="list-inline" style="color:#c80032;">
											<li>Posted: <span class="text-theme-colored2" style="color:#333;"> <?php echo date('d M, Y'); ?></span></li>
										</ul>
										<div class="descriptionBox text-justify">
											<p><?php echo $showUpdate['closure_desc']; ?></p>
										</div>
									</div>
									<?php if($showImageContent['upload_type'] == 'Document'){ ?>	
									<div class="col-md-12">
										<div class="addMoreInfo">
											<ul>
												<?php echo $documentsShow;  ?>
											</ul>
										</div>
									</div>
									<?php } ?>
								</div>                        
							</div>

							
							<?php	
								} // End Foreach
								
							} else {
							?>
							<p style="width: 100%;margin-top: 20px;font-size: 18px !important;font-weight: 500;text-align:center">No Update Found</p>
							<?php } // End IF Count ?>
						</div>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</section>
<?php if($this->session->userdata('user_id') == $challenge_data[0]['u_id']): ?>
<section>
	<div class="challenges-title">
		<div class="row">
			<div class="col-sm-9">
				<h3><a href="javascript:void(0);" class="show-hide1">Additional Statistics to be portrayed view options</a></h3>
			</div>       
		</div>
	</div>
	<div class="portrayed-content" >
		<div class="conatiner-fluid">
		  <div class="row">		  
				<div class="col-md-12" align="center">
					<a href="<?php echo base_url('challenge/teamListing/'.base64_encode($challenge_data[0]['c_id']).'/'.base64_encode('all')); ?>">
						<div class="circle circle-2">
							<div class="content">
								<span><?php echo count($total_apply_team) ?></span>
								<p> Total Team Applictions Received</p>
							</div>
							<i class="lni lni-pencil-alt"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id']).'/'.base64_encode('all')); ?>">
						<div class="circle circle-1">
							<div class="content">
								<span><?php echo count($total_received) ?></span>
								<p>Access Request</p>
							</div>
							<i class="lni lni-enter"></i>
						</div>
					</a>
				
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id']).'/'.base64_encode('Approved')); ?>">
						<div class="circle circle-3">
							<div class="content">
								<span><?php echo count($approved_cnt) ?></span>
								<p>Total Approved</p>
							</div>
							<i class="lni lni-checkmark-circle"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id']).'/'.base64_encode('Pending')); ?>">
						<div class="circle circle-4">
							<div class="content">
								<span><?php echo count($pending_cnt) ?></span>
								<p> Total Pending</p>
							</div>
							<i class="lni lni-timer"></i>
						</div>
					</a>
					<a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id']).'/'.base64_encode('Rejected')); ?>">
						<div class="circle circle-5">
							<div class="content">
								<span><?php echo count($rejected_cnt) ?></span>
								<p> Total Rejected</p>
							</div>
							<i class="lni lni-cross-circle"></i>
						</div>
					</a>
				</div>
			</div>
		</div>			
	</div>
</section>  
<hr>
<?php endif; ?>


<!--====================================================
	OUR PARTNERS
======================================================-->
<!--Client Logo Section-->
<?php
	if(count($consortium_list) > 0){
	?>
	<!--Client Logo Section-->
	<section id="partners" data-aos="fade-down">
		<div class="container clientLogo2">
			<div class="row title-bar">
				<div class="col-md-12">
					<h2><?php echo $partneTitle; ?></h2>
					<div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
					<?php foreach($consortium_list as $consrotium): ?>
					<div class="col-sm-12"><a href="<?php echo $consrotium['img_link']; ?>" target="_blank"><img src="<?php echo base_url('assets/partners/'.$consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>


<section class="ministrylogo">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center title-bar">
				<h4>Under Aegis of</h4>
				<div class="heading-border"></div>
				<a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="briefInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Brief Info about Team</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents">
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!--<a href="" onClick="return popitup('https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u')">Click Me</a>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />-->


<div class="modal fade" id="ParticipateModal" tabindex="-1" role="dialog" aria-labelledby="ParticipateModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outerParticipate">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="SubscriptionModalLabel">Participate for <?php echo $challenge_data[0]['challenge_title']; ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			
			<div class="agreement_content">
				<div class="agreement_content_inner">
					<?php if($challenge_data[0]['terms_txt'] != "") { ?>
						<h4>Challenge Terms</h4>
						<p><?php echo $challenge_data[0]['terms_txt'];?></p>
					<?php } ?>		
					
					<p class="byt_terms_outer">General <a href="<?php echo site_url('home/termsOfuse'); ?>" target="_blank">Terms & Conditions</a></p>
				</div>
				
				<form method="post" action="javascript:void(0)" id="participate_form" enctype="multipart/form-data">
					<div class="col-auto" style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input" id="i_agree" name="i_agree" required value="1">
							<label class="custom-control-label" for="i_agree">I Agree, the Terms of Reference</label>
						</div>
						<div id="i_agree_err"></div>
					</div>					
					<button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Confirm Participation</button>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script type="text/javascript">	
	function open_ParticipateModal() 
	{
		var openPopUpFlag = 0;
		<?php 
			if($challenge_data[0]['challenge_id'] == 'TNCID-000018' || $challenge_data[0]['challenge_id'] == 'TNCID-000019') 
			{	?> 
				openPopUpFlag = 1;
				window.location.href = "<?php echo site_url('challenge/participate_link/'.base64_encode($challenge_data[0]['c_id'])); ?>";
<?php	} ?>


		<?php 
			if($challenge_data[0]['challenge_status'] != 'Approved') //Check challenge approves or not
			{ ?>
				openPopUpFlag = 1;
				sweet_alert_warning('Challenge is not approved');
<?php	}
			else if($challenge_data[0]['status'] != 'Active') //Check challenge Active or not
			{ ?>
				openPopUpFlag = 1;
				sweet_alert_warning('Challenge is not Active');
<?php	}
			else 
			{
				if($challenge_data[0]['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $challenge_data[0]['challenge_close_date']) //Check challenge valid date
				{ }
				else  
				{	?>
					openPopUpFlag = 1;
					sweet_alert_warning('Participations closed for selected challenge');			
	<?php	}
			}
				
			if($this->session->userdata('user_id') == $challenge_data[0]['u_id'])//CHECK IF USER IS NOT CHALLENGE OWNER
			{	?>
				openPopUpFlag = 1;
				sweet_alert_warning('Challenge Owner cannot participate in this challenge');			
			<?php	}
			
			$already_applied = $this->master_model->getRecordCount('arai_byt_teams',array("user_id"=>$this->session->userdata('user_id'), 'c_id'=>$challenge_data[0]['c_id'],'status'=>'Active', 'is_deleted'=>'0'));
			if($already_applied > 0)//Check if user already applied for this challenge
			{	?>
				openPopUpFlag = 1;
				sweet_alert_warning('You have already created team for this challenge');	
<?php	}	?>
			
			if(openPopUpFlag == 0) { $("#ParticipateModal").modal('show'); }
		} 
		
		//******* JQUERY VALIDATION *********
		$("#participate_form").validate( 
		{
			rules: { i_agree: { required: true } },
			messages: { i_agree: { required: "Please accept the Terms of Reference" }, },
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "i_agree") { error.insertAfter("#i_agree_err"); }
				else { error.insertAfter(element); }
			},
			submitHandler: function(form) 
			{ 
				var csrf_test_name = 	$('.token').val();
				$('#preloader-loader').show();
				parameters= { 'challenge_id': "<?php echo base64_encode($challenge_data[0]['c_id']); ?>", 'csrf_test_name':csrf_test_name }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('team'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success: function(data) 
					{
						$(".token").val(data.csrf_test_name);
						if(data.flag == 'success') { window.location.replace("<?php echo base_url('team/create/'.base64_encode($challenge_data[0]['c_id'])); ?>"); }
						else { location.reload(); }
						$('#preloader-loader').hide();
					}
				});	
			}
		});
	</script>
	
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	
	
	<?php
	if($request_user[0]['status'] == 'Rejected'){
		$alertPopup = "Your request is rejected by owner";
	} else if($request_user[0]['status'] == 'Pending'){
		$alertPopup = "You already requested to owner for details.";
	}
	?>
<script>
	function popitup(url) 
	{
		newwindow=window.open(url,'name','height=600,width=1200,screenX=400,screenY=350');
		if (window.focus) {newwindow.focus()}
		return false;
	}
	function get_challenge_details(c_id, o_id, resp){
		if(resp > 0){
			console.log('<?php echo $request_user[0]['status'];  ?>');
				swal(
				{
					title:"Confirmation:",
					text: "<?php echo $alertPopup; ?>",
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6'
				});
			
		} else {
			
			swal(
				{
					title:"Confirmation:",
					text: "You are requesting access to the challenge details",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!',
					html:'You are requesting access to the owner details. '+'<input type="hidden" name="c_id" id="c_id" value="<?php echo $challenge_data[0]['c_id']  ?>" /><input type="hidden" name="o_id" id="o_id" value="<?php echo $challenge_data[0]['u_id']  ?>" /><input type="hidden" class="token" id="csrf_test_name" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash(); ?>" />'
				}).then(function (result) 
				{
					if (result.value) 
					{
						$('#preloader-loader').css('display', 'block');	
						var base_url = '<?php echo base_url() ?>';				
						var cs_t = 	$('.token').val();
						var c_id = $("#c_id").val();
						var o_id = $("#o_id").val();	
						//alert(c_id+"=="+o_id);return false;
						/*var c_id = $(this).attr('data-cid');
							var o_id = $(this).attr('data-oid');
							var cs_t = 	$('.token').val();
						var base_url = '<?php echo base_url() ?>';*/
						$.ajax({
							type:'POST',
							url: base_url+'challenge/askfordetails',
							data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
							success:function(data){
								$('#preloader-loader').css('display', 'none');	
								var output = JSON.parse(data);								
								$(".token").val(output.token); 	
								var textAlert = output.alert_text;
								var stat = output.status;
								if(stat == 'N'){
									$(".ask-details").text('Waiting For Owner Approval');
									location.reload();
								}
								swal({
									title: 'Success!',
									text: textAlert,
									type: 'success',
									showCancelButton: false,
									confirmButtonText: 'Ok'
								});
								
							}
						});	// Ajax End
						
					}
					
				});
		}
		
	}
	
	$(document).on('click','.click-more',function(){		
		//var priorityVal = $(this).val();			
		var cid	=	$(this).attr('data-id');
		//alert(cid);return false;
		$('#briefInf').modal('show');		
		
		var base_url = '<?php echo base_url('challenge/viewDetails'); ?>';
		$.ajax({
			url: base_url,
			type: "post",
			data: {id:cid},				
			success: function (response) {					
				$("#contents").html(response);				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});			
	});	
	
	$(document).on('click','.apply-popup',function(){
		
		swal({
			title:"",
			text: "Coming Soon",
			type: 'warning',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!',
			html:''
		});
		
	});
	
	jQuery(document).ready(function(){			
		$('.team_listing').owlCarousel({
			items: 1,
			autoplay: true,
			smartSpeed: 700,
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true
		});
		
		//***************** Load More ***********************			
		// Load More Code 
		$(document).on('click','.show_more',function(){
			// $(".token").remove();
			var base_url = '<?php echo base_url(); ?>'; 
			var ID = $(this).attr('id');
			var length = $('.team-box').length;
			var cs_t  = 	$('.token').val();
			var ch_id = 	'<?php echo $challenge_data[0]['c_id']  ?>';
			$('#preloader-loader').css('display', 'block');	
			$('.show_more').hide();
			$('.loding').show();
			$.ajax({
				type:'POST',
				url: base_url+'challenge/team_more',
				data:'id='+length+'&csrf_test_name='+cs_t+'&ch_id='+ch_id,
				dataType:"html",
				async:false,
				success:function(response){ 
					$('#preloader-loader').css('display', 'none');	
					var output = JSON.parse(response);
					if(output.html == ""){
						$(".show_more_main").remove();
					}	
					$(".token").val(output.token);								
					$('#show_more_main'+ID).remove();
					$('.teams-listing').append(output.html); 					
					
					$(".team_listing").owlCarousel({
						items: 1,
						autoplay: true,
						smartSpeed: 700,
						loop: false,
						nav: true,
						dots: false,
						navText: ["", ""],
						autoplayHoverPause: true
					});
					
					
				},
				error: function (jqXHR, exception) { 
					$('#preloader-loader').css('display', 'none');	
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
						msg = 'Time out error.';
						} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
						} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		//******************End Load More ********************		
		
		$(document).on('click','.participate-click',function(){
			window.open("https://forms.office.com/Pages/ResponsePage.aspx?id=xd6unZC810SeVJ6j4qUj1-Rp_BNq3UpDuEK9FDjLmpNUNUVPS1E2MkM4RUQ0U0kwMjUxNk5DWldESi4u");
		});
		
		$(document).on('click','.btn-toggle-show',function(){
			$('#show-hide').css('display', 'block');
			$('.btn-toggle-hide').css('display', 'block');
			$('.btn-toggle-show').css('display', 'none');
		});
		
		$(document).on('click','.btn-toggle-hide',function(){
			$('#show-hide').css('display', 'none');
			$('.btn-toggle-hide').css('display', 'none');
			$('.btn-toggle-show').css('display', 'block');
		});
		
		// Read More/Less
		$(".content-new").show();
		$(".show_hide_new").on("click", function() {	
			//console.log('HELLO');
			$(this).parent().next(".content-new").toggle();			
			if ($(this).text().trim() == "Read More") {
				$(this).text("Read Less");
				} else {
				$(this).text("Read More");
			}
		});
					
		/*$('.ask-details').on('click', function(){	
			var c_id = $(this).attr('data-cid');
			var o_id = $(this).attr('data-oid');
			var base_url = '<?php echo base_url() ?>';
			var cs_t = 	$('.token').val();	
			$.ajax({
			type:'POST',
			url: base_url+'challenge/askfordetails',
			data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
			success:function(data){
			var output = JSON.parse(data);								
			$(".token").val(output.token); 			
			
			var textAlert = output.alert_text;
			var stat = output.status;
			if(stat == 'N'){
			$(".ask-details").text('Waiting For Owner Approval');
			}
			swal({
			title: 'Success!',
			text: textAlert,
			type: 'success',
			showCancelButton: false,
			confirmButtonText: 'Ok'
			});
			
			}
			});	
			
		});*/


		$(document).on('click','.tabLink',function(){
			var dataID = $(this).attr('data-id');
			
			if(dataID!="chupdate"){
				//$('#closureupdates').hide();
				$("#closureupdates").removeClass('active');
				$("#closureupdates").removeClass('show');
			}
		});
	
		// owl crousel testimonial section
		$(".owl-carousel").each(function() {
			$(this).owlCarousel({
				items: 1,
				autoplay: true,
				smartSpeed: 700,
				autoHeight: true,
				loop: true,
				nav: true,
				dots: false,
				navText: ["", ""],
				autoplayHoverPause: true
			});			
		});
			
		(function($){
        $(window).on("load",function(){
            $(".descriptionBox").mCustomScrollbar({
                theme: "rounded-dark"
            });
        });
    })(jQuery);	
			
	});

	
	/*$(document).ready(function(){
		$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
			localStorage.setItem('activeTab', $(e.target).attr('href'));
		});
		var activeTab = localStorage.getItem('activeTab');
		if(activeTab){
			$('#myTab a[href="' + activeTab + '"]').tab('show');
		}
	});*/
</script>

<script type="text/javascript">
   
$(document).ready(function () { 
   load_team_listing_ajax(0,<?php echo $team_limit ?>,0)
});
function clear_search_form(){
	 $("#team_listing_outer").html('');
   $("#search_txt").val("");
   load_team_listing_ajax(0,<?php echo $team_limit ?>,0)
}
function load_team_listing_ajax(start,limit,is_search=''){

			var challenge_id = '<?php echo $this->uri->segment(3) ?>';
      var searchText =$("#search_txt").val();

      if (is_search==1) {
         $("#search_txt").attr('placeholder','');

      if (searchText=='') {
         $("#search_txt").css('border-color','red');
         $("#search_txt").attr('placeholder','Please enter text');
         return false;
      }
      }
         
       $("#no_rec_found").remove();
        $("#showMoreBtn").remove();
        $("#preloader").show();

        var postData = {'challenge_id':challenge_id, 'start':start,'limit':limit,'searchText':searchText}
        $.ajax(
        {
          type: "POST",
          url: "<?php echo site_url('challenge/load_team_listing_ajax'); ?>",
          cache: false,
          data:postData,
          dataType: 'JSON',
          success:function(data)
          {
            if (is_search==1) {
               $("#team_listing_outer").html('');
            }
            $("#team_listing_outer").append(data.teams_listing_html);
            $("#preloader").hide();
          }
        });
}  

</script>
	
	
		