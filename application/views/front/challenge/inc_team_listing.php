<?php  if(count($team_details) > 0){ 
							$m=1;
							foreach($team_details as $teamslist){
							$slotDecryption =  New Opensslencryptdecrypt();	
							$getSlotMaxMember = $this->master_model->getRecords('eligibility_expectation',array('c_id'=> $teamslist['c_id']));
							//print_r($getSlotMaxMember);
							// Team Slot Details
							$slotDetails  = $this->master_model->getRecords('byt_team_slots',array('c_id'=> $teamslist['c_id'], 'team_id'=> $teamslist['team_id'], 'is_deleted' => '0'));
							$maxTeam = $slotDecryption->decrypt($getSlotMaxMember[0]['max_team']);
							$minTeamCnt = count($slotDetails);
							$availableCnt = $maxTeam - $minTeamCnt;
							$array_skills = array();
							foreach($slotDetails as $slot_list)
							{
								if($slot_list['hiring_status'] == 'Open') { array_push($array_skills, $slot_list['skills']); }
							}
							
							$chDetails  = $this->master_model->getRecords('challenge',array('c_id'=> $teamslist['c_id']));
							$dCha = $slotDecryption->decrypt($chDetails[0]['challenge_details']);
							$combine = implode(",",$array_skills);
							$xp = explode(',',$combine);
							$arrayUnique = array_unique($xp); 
							if($teamslist['team_banner']!=""){
								$banner_team = base_url('uploads/byt/'.$teamslist['team_banner']);
								} else {
								$banner_team = base_url('assets/no-img.png');
							}
							
							$getAllSlotTeam = $this->master_model->getRecords("arai_byt_team_slots", array('c_id' => $teamslist['c_id'], 'team_id' => $teamslist['team_id'], 'is_deleted' => '0'));
							//echo $this->db->last_query(); exit;
							
							$totalSlotCnt = $totalPendingCnt = 0;
							foreach($getAllSlotTeam as $res)
							{
								if($res['hiring_status'] == 'Open') { $totalPendingCnt++; }
								$totalSlotCnt++;
							} ?>
							
							<div class="col-sm-12 justify-content-center mt-5 team-box">
								<div class="boxSection25">
									<ul class="list-group list-group-horizontal">
										<li class="list-group-item">
											<div class="inner-box"  style="border-bottom:none">
												
												<img src="<?php echo $banner_team; ?>" alt="Banner" class="img-fluid">
												<h3 class="minheight" style="font-size:16px;"><?php echo character_limiter($teamslist['team_name'],60) ?></h3>
												<ul>
													<li> <?php echo $totalSlotCnt ?> Members </li> 
													<li>-</li>
													<li><?php echo $totalPendingCnt; ?> Available Slots</li>
												</ul>
											</div>
											<!-- <div class="border_top_bottom">
												<a href="javascript:void(0);" class="d-inline-block chatButton apply-popup">Chat</a>
											</div> -->
										</li>
										
										<li class="list-group-item">
											<div class="inner-box">											
												<strong style="display:block; text-align:left; margin:15px 0px 0px 0px;">Brief Info about Team </strong>
												<p style="display:block; height:77px; text-align:left; padding: 0 0 15px 0"><?php echo substr($teamslist['brief_team_info'],0,90).'...<a href="javascript:void(0)" class="click-more" data-id="'.$teamslist['team_id'].'">View More</a>'; ?></p>
												<h4>Looking For</h4>
												<div class="Skillset text-center">
													<?php
														$s = 0;	
														foreach($arrayUnique as $commonSkill){
														if($s < 9)
														{
															$this->db->where("id = '".$commonSkill."' AND (status = 'Active' OR profile_id = '".$teamslist['user_id']."')",NULL, false);
															$skill_now = $this->master_model->getRecords('skill_sets');
															$skill_names = $slotDecryption->decrypt($skill_now[0]['name']); ?>															
															<span class="badge badge-pill badge-info"><?php echo $skill_names; ?></span>
															<?php }
															$a++;
														}
													?>												
												</div>
											</div>
											<!-- <div class="border_top_bottom">
												<a href="<?php echo base_url('myteams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>" class="btn btn-primary w-100 mt-02" >View Details</a>
											</div> -->
										</li>
										
										<li class="list-group-item" style="height: 0 !important;">
											<div class="team_listing owl-carousel owl-theme">
												<?php		
													$TeamSr = 1;
													foreach($slotDetails as $slotD)
													{	
														$skills = array();
														$skillTitle = 'Skills';
														$getSlotDetail = $this->master_model->getRecords('byt_slot_applications',array('slot_id'=> $slotD['slot_id'], 'team_id'=> $slotD['team_id'], 'c_id'=> $slotD['c_id'], 'status'=>'Approved'));
														
														if(count($getSlotDetail) > 0)
														{														
															$this->db->select('user_id, first_name, middle_name, last_name, user_category_id');
															$getUserDetail = $this->master_model->getRecords('registration',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
															
															$catID = $getUserDetail[0]['user_category_id'];	
															$getNames = $slotDecryption->decrypt($getUserDetail[0]['first_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['middle_name'])." ".$slotDecryption->decrypt($getUserDetail[0]['last_name']); //." ".$getUserDetail[0]['user_id']
															$fullname = ucwords($getNames);
															
															if($catID == 1)
															{															
																$this->db->select('profile_picture, skill_sets, other_skill_sets, skill_sets_search');
																$getProfileDetail = $this->master_model->getRecords('student_profile',array('user_id'=> $getSlotDetail[0]['apply_user_id']));
																
																$imgPath = base_url('assets/profile_picture/'.$getProfileDetail[0]['profile_picture']);	
																if ($getProfileDetail[0]['profile_picture']!="") 
																{
																	$imageUser = $imgPath;
																} 
																else 
																{
																	$imageUser = base_url('assets/no-img.png');
																}
																
																if($getProfileDetail[0]['skill_sets'] != "")
																{
																	$skill_set_ids = $slotDecryption->decrypt($getProfileDetail[0]['skill_sets']);
																	
																	if($skill_set_ids != "")
																	{
																		$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
																		$skills = $this->master_model->getRecords("arai_skill_sets");														
																	}
																}
																
																if($getProfileDetail[0]['other_skill_sets'] != "") 
																{ 
																	$skills[]['name'] = $getProfileDetail[0]['other_skill_sets']; 
																	}																	
																	}																
															else if($catID == 2)
															{																
																$getOrgDetail = $this->master_model->getRecords('profile_organization',array('user_id'=> $getUserDetail[0]['user_id']));
																
																if(count($getOrgDetail) > 0)
																{																	
																	$profilePic = $slotDecryption->decrypt($getOrgDetail[0]['org_logo']);
																	$imgPath = base_url('uploads/organization_profile/'.$profilePic);	
																	if ($profilePic!="") {
																		$imageUser = $imgPath;
																		} else {
																		$imageUser = base_url('assets/no-img.png');
																	}
																}	
																
																$skillTitle = 'Sector';
																if($getOrgDetail[0]['org_sector'] != "")
																{
																	$skill_set_ids = $getOrgDetail[0]['org_sector'];
																	
																	if($skill_set_ids != "")
																	{
																		$this->db->where("id IN (".$skill_set_ids.") AND status = 'Active'");
																		$skills = $this->master_model->getRecords("arai_organization_sector");														
																	}
																}
																
																if($getOrgDetail[0]['org_sector_other'] != "") 
																{ 
																	$skills[]['name'] = $slotDecryption->decrypt($getOrgDetail[0]['org_sector_other']); 
																}
															}								
															
															if($getSlotDetail[0]['apply_user_id'] == $teamslist['user_id'])
															{
																$styles_hide = "hide-class";
															} 
															else 
															{
																$styles_hide = "d-inline-block";
															}
														} 
														else 
														{
															$imageUser = base_url('assets/no-img.png');
															$styles_hide = "d-inline-block";
															$fullname = "Open Slot";											
															
															$skill = $slotD['skills'];											
															$this->db->where("id IN (".$skill.") AND (status = 'Active' OR profile_id = '".$slotD['user_id']."')",NULL, false);
															$skills = $this->master_model->getRecords( "arai_skill_sets",'','name');
														} // Else End
													?>
													
													<div class="item text-center">
														<img src="<?php echo $imageUser; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $fullname; ?>" class="check-<?php echo $getSlotDetail[0]['apply_user_id'] ?>" />
														<h4><?php echo $fullname; ?></h4>
														<div class="Skillset text-center">
															<?php 
															if (count($skills)) 
															{
																$i=0;
																if($i < 6)
																{
																	foreach ($skills as $key => $value) 
																	{ ?>													
																		<span class="badge badge-pill badge-info"><?php if($skillTitle != 'Sector') { if(strtolower($slotDecryption->decrypt($value['name'])) != 'other') { echo $slotDecryption->decrypt( $value['name'] ); } } else { if(strtolower($value['name']) != 'other') { echo $value['name']; } } ?></span>													
														<?php $i++;
																	}
																}
															} ?>
														</div>
														<h4 class="teamMember"  style="border-bottom:none">Team Member <?php echo $TeamSr; ?></h4>
														<?php //if($getSlotDetail[0]['status']!='Approved'): ?>
														<!-- <a href="javascript:void(0);" class="<?php echo $styles_hide; ?> apply-popup d-none">Apply</a> -->
														<?php //endif; ?>
													</div>
													
												<?php $m++; $TeamSr++; } ?>									
											</div> <!-- Slider End -->
											
											<!-- <a href="<?php echo base_url('myteams/applySlot/').base64_encode($slotD['team_id'])."/".base64_encode($slotD['c_id']) ?>" class="btn btn-primary">Apply</a> -->
											
										</li>
										
										<div class="container" style="clear:both;" >
											<div class="row buttonView mb-3">
												<div class="col-md-3">
													<a href="javascript:void(0);" class="btn btn-primary w-100 apply-popup">Chat</a>
												</div>
												<div class="col-md-6">
													<a href="<?php echo base_url('myteams/team_details_member/'.base64_encode($teamslist['team_id'])); ?>" class="btn btn-primary w-100">View Details</a>
												</div>
												<div class="col-md-3">
													<a href="<?php echo base_url('myteams/applySlot/').base64_encode($teamslist['team_id'])."/".base64_encode($teamslist['c_id']) ?>" class="btn btn-primary w-100">Apply</a>
												</div>
											</div>
										</div>
									</ul>
								</div>
							</div>
							<?php }  ?>
									<?php  if($new_start < $teams_all_count){ ?>
	               <div class="show_more_main text-center col-sm-12" id="showMoreBtn">
	                 <a class="how_more btn btn-general btn-white" role="button" href="javascript:void(0)" onclick="load_team_listing_ajax('<?php echo $new_start;?>','<?php echo $limit ?>',0)" >Load More</a>
	               </div>
	               <?php } ?>
						<?php }else{
								
								echo "<p id='no_rec_found' style='width: 100%;margin-top: 20px;font-size: 18px !important;font-weight: 500;text-align:center'>No Teams Found</p>";	
							 } ?>

							