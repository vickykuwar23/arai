<?php 
error_reporting(0);
$datestr = $challenge_data[0]['challenge_close_date'];//Your date
$date	 = strtotime($datestr);//Converted to a PHP date (a second count)

//Calculate difference
$diff	= 	$date-time();//time returns current time in seconds
$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
//$hours	=	round(($diff-$days*60*60*24)/(60*60));
$style="";
if($days > 0){
	$style="display:none";
	$showCnt = $days. " Days left";
} else {
	$showCnt = "Closed Paticipations";
}


$fileExist = base_url('assets/challenge/'.$challenge_data[0]['company_logo']);
 if (@GetImageSize($fileExist)) {
	$image = $fileExist;
} else {
	$image = base_url('assets/not_available.png');
}
//echo "<pre>";print_r($challenge_data);die();

$bannerImg = base_url('assets/challenge/'.$challenge_data[0]['banner_img']);
?>

<style>
.circle {
  float: left;
  margin-right: 10px;
  width: 150px;
  height: 150px;
  border-radius: 150px;
  font-size: 16px;
  color: #fef;
  text-align: center;
  position: relative;
}

.circle-1 { 
  background: #F2F2F2;
}
.circle-2 { 
  background: #CCC;
}
.circle-3 { 
  background: #32CD32; 
}
.circle-4 { 
  background: yellow;
}
.circle-5 { 
  background: red;
}

.circle p {
  margin: 0;
  transform: translate(-50%, -50%);
  top: 50%;
  left: 50%;
  position: absolute;
}

.home-p.pages-head4 {
	background: url(<?php echo $bannerImg; ?>) no-repeat center top;
	background-size: 100%; min-height: 100%;
}
</style>

 <div id="home-p" class="home-p pages-head4 text-center" data-aos="fade-down">
    <div class="container">
      <!-- <h1 class="wow fadeInUp" data-wow-delay="0.1s">Challenge Details</h1>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb wow fadeInUp">
          <li class="breadcrumb-item"><a href="#">Challenge</a></li>
          <li class="breadcrumb-item active" aria-current="page">Challenge Details </li>
        </ol>
      </nav> -->
    </div>
    <!--/end container-->
  </div>

<!--====================================================
        CHALLENGE DETAILS WITH TAB STRUCTURE
======================================================-->
<section id="challenge-intro">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="img-box">
            <img src="<?php echo $image //base_url('assets/challenge/'.$challenge_data[0]['company_logo']); ?>" class="img-fluid" alt="img">
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-6">
              <div class="title-bar">
                <div class="title">In Colaboration with ARAI</div>
                <h3><?php echo $challenge_data[0]['challenge_title'] ?></h3>
                <div class="heading-border-light"></div>
              </div>
              <ul class="list-group list-group-horizontal">
                <li class="list-group-item">
                  <button class="btn btn-general btn-green" role="button">Participate</button>
                </li>
                <!--<li class="list-group-item"><a href="" class="chshare"><i class="fa fa-share-alt"></i> Share</a></li>-->
              </ul>
            </div>
            <div class="col-md-6">
              <ul class="list-unstyled product_info">
                <li>
                  <label>Teams:</label>
                  <span> <a href="#"><?php echo $challenge_data[0]['min_team'] ?> - <?php echo $challenge_data[0]['max_team'] ?> People</a></span></li>
                <li>
                  <i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></li>
              </ul>
              <p><?php 
			  
					$limit = 250;
					$str = $challenge_data[0]['company_profile'];
					 if (strlen($challenge_data[0]['company_profile'] ) > $limit){
						$str  = substr($challenge_data[0]['company_profile'] , 0, strrpos(substr($challenge_data[0]['company_profile'] , 0, $limit), ' ')) . '...'; 
					 }
			  
					echo $str; ?></p>
            </div>

          </div>

        </div>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="product_details">
      <ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#technology">Brief Company Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " data-toggle="tab" href="#features">Brief Info About Challenge</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#application">Abstract About Challenge</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#validation_achive">Other Information</a>
        </li>
		
		<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#intellectual_Property">Challenge Owner Details</a>
        </li>
				
		
        <!--<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#intellectual_Property">Teams</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#abstract">Rules</a>
        </li>-->
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="technology">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <?php echo $challenge_data[0]['company_profile'] ?>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade " id="features">
          <div class="container">
            <div class="row justify-content-center">             
              <div class="col-md-10">
                 <?php echo $challenge_data[0]['challenge_details'] ?>                
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="application">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <p> <?php echo $challenge_data[0]['challenge_abstract'] ?>   </p>
               
              </div>

            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="validation_achive">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
			  
				<table class="table">
					<tr>
						<td width="20%"><b>Contact Person Name </b></td>
						<td width="80%"><?php echo $challenge_data[0]['contact_person_name']; ?></td>
					</tr>
					<tr>
						<td><b>Launch Date </b></td>
						<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_launch_date'])); ?></td>
					</tr>
					<tr>
						<td><b>Close Date </b></td>
						<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['challenge_close_date'])); ?></td>
					</tr>
					<tr>
						<td><b>Technology </b></td>
						<td><?php echo $challenge_data[0]['technology_name']; ?></td>
					</tr>
					<tr>
						<td><b>Tags </b></td>
						<td><?php echo $challenge_data[0]['tag_name']; ?></td>
					</tr>
					<tr>
						<td><b>Audience Preference </b></td>
						<td><?php echo $challenge_data[0]['preference_name']; ?></td>
					</tr>
					<tr>
						<td><b>If Funding </b></td>
						<td><?php if($challenge_data[0]['if_funding'] =='Funding' ): echo "Y"; else: 'No'; endif; ?></td>
					</tr>
					<?php if($challenge_data[0]['if_funding'] =='Funding' ): ?>
					<tr>
						<td><b>Funding Amount </b></td>
						<td><?php echo $challenge_data[0]['is_amount']; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><b>If Reward </b></td>
						<td><?php if($challenge_data[0]['if_reward'] =='Reward' ): echo " Y"; else: 'No'; endif; ?></td>
					</tr>
					<?php if($challenge_data[0]['if_reward'] =='Reward' ): ?>
					<tr>
						<td><b>Funding Reward </b></td>
						<td><?php echo $challenge_data[0]['fund_reward']; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><b>Audience Preference </b></td>
						<td><?php echo $challenge_data[0]['trl_solution']; ?></td>
					</tr>
					<tr>
						<td><b>Terms & Condition </b></td>
						<td><?php echo $challenge_data[0]['terms_txt']; ?></td>
					</tr>
					<tr>
						<td><b>Future Opportunities </b></td>
						<td><?php echo $challenge_data[0]['future_opportunities']; ?></td>
					</tr>
					<tr>
						<td><b>IP Clause </b></td>
						<td><?php echo $challenge_data[0]['ip_name']; ?></td>
					</tr>
					<tr>
						<td><b>External Funding </b></td>
						<td><?php if($challenge_data[0]['is_external_funding'] =='1' ): echo 'Y'; else: echo 'N'; endif; ?></td>
					</tr>
					<?php if($challenge_data[0]['is_external_funding'] =='1' ): ?>
					<tr>
						<td><b>Funding Amount(in %) </b></td>
						<td><?php echo $challenge_data[0]['external_fund_details']; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><b>Is this challenge Exclusive Listed On Technovuus.? </b></td>
						<td><?php  if($challenge_data[0]['is_exclusive_challenge'] == '1' ): echo 'Y'; else: echo 'N'; endif; ?></td>
					</tr>
					<?php if($challenge_data[0]['is_exclusive_challenge'] =='0' ): ?>
					<tr>
						<td><b>Details </b></td>
						<td><?php echo $challenge_data[0]['exclusive_challenge_details']; ?></td>
					</tr>
					<?php endif; ?>
				</table>
				
              </div>

            </div>
          </div>
        </div>
		<div class="tab-pane fade" id="intellectual_Property">
			 <div class="container">
		<?php
		
		if($this->session->userdata('user_id') !=""){ ?>		
         
			<?php 
				if($request_user[0]['status'] != 'Approve'){ 
				if($this->session->userdata('user_id') != $challenge_data[0]['u_id']){

				?>
				<div class="row justify-content-center">			
				  <h4>Ask challenge owner details.?</h4>
				  <p></p>
				</div>
				<?php } } ?>
            <div class="row justify-content-center" align="center">				
				 <?php 
						 $style = 'display:none';
						 if($this->session->userdata('user_id') == $challenge_data[0]['u_id']){
							 $style = 'display:block'; 
						 }
						 $anchor = 'display:block'; 	
						 if(count($request_user) > 0){
							 $status_check = $request_user[0]['status']; 
							 if($status_check == 'Approve'){
								$style = 'display:block'; 
								$anchor = 'display:none'; 
							 } else {
								$style = 'display:none'; 
								$anchor = 'display:block';
							 }
							 
						 } else { 
							$status_check = 'ASK Owner Details'; 
						 }
						 
						 
				 ?>
					<?php 
					if($this->session->userdata('user_id') != $challenge_data[0]['u_id']){
					?>
					<a href="javascript:void(0);" data-cid="<?php echo $challenge_data[0]['c_id']  ?>" data-oid="<?php echo $challenge_data[0]['u_id']  ?>" style="<?php echo $anchor; ?>" class="btn btn-general btn-green ask-details" role="button"><?php echo $status_check ?></a>
					<?php } ?>
					<table class="table" style="<?php echo $style ?>">
						<tr>
							<td width="20%"><b>Email ID </b></td>
							<td width="80%"><?php echo $challenge_data[0]['email_id']; ?></td>
						</tr>
						<tr>
							<td><b>Mobile Number </b></td>
							<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['mobile_no'])); ?></td>
						</tr>
						<tr>
							<td><b>Office Number</b></td>
							<td><?php echo date('d-m-Y', strtotime($challenge_data[0]['office_no'])); ?></td>
						</tr>
					</table>
				
			</div>
         
		  
		  <?php } else { // If Login CHeck End

			
		  ?>
				<div class="row justify-content-center">			
				  <h4>Ask challenge owner details.?</h4>
				  <p></p>
				</div>
				 <div class="row justify-content-center" align="center">				
				 <a href="<?php echo base_url('login'); ?>" class="btn btn-general btn-green" role="button">ASK Owner Details</a>
				</div>
		  
		  <?php } ?>
		   </div>
		  
        </div>
        
      </div>
  </section>
  <?php if($this->session->userdata('user_id') == $challenge_data[0]['u_id']): ?>
   <section>
    <div class="challenges-title">
      <div class="row">
        <div class="col-sm-9">
          <h3><a href="javascript:void(0);" class="show-hide">Additional Statistics to be portrayed view options</a></h3>
        </div>       
      </div>
    </div>
	<div class="portrayed-content" >
		<div class="conatiner-fluid">
		  <div class="row">
		  
			<div class="col-xs-12" align="center">
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-1">
				<p><?php echo count($total_received) ?>
				  <br> Access Request</p>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-2">
				<p><?php echo count($total_received) ?>
				  <br> Total Applicant</p>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-3">
				<p><?php echo count($approved_cnt) ?>
				  <br> Total Approved</p>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails/'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-4">
				<p><?php echo count($pending_cnt) ?>
				  <br> Total Pending</p>
			  </div>
			  </a>
			  <a href="<?php echo base_url('challenge/requestDetails'.base64_encode($challenge_data[0]['c_id'])); ?>">
			  <div class="circle circle-5">
				<p><?php echo count($rejected_cnt) ?>
				  <br> Total Rejected</p>
			  </div>
			  </a>
			</div>
		  </div>
		</div>			
   </div>
  </section>  
  <hr>
  <?php endif; ?>
  <!--====================================================
                      FEATURED CHALLENGES SLIDER
======================================================-->
  <?php if(count($featured_list) > 0){ ?>
  <section id="comp-offer" class="pt20" data-aos="fade-up">
    <div class="challenges-title">
      <div class="row">
        <div class="col-sm-9">
          <h3>Feature Challenges</h3>
        </div>
        <div class="col-sm-3 text-right">
          <a href="<?php echo base_url('home/featuredChallenges'); ?>" class="btn btn-general btn-white" role="button">view all challenges</a>
        </div>
      </div>
    </div>
    <div id="open-ch" class="owl-carousel owl-theme col-md-12 desc-comp-offer">
		<?php 		
			
			foreach($featured_list as $featuredList){
				
				$fileExist = base_url('assets/challenge/'.$featuredList['banner_img']);
				 if (@GetImageSize($fileExist)) {
					$image = $fileExist;
				} else {
					$image = base_url('assets/news-14.jpg');
				}
				
				if (strlen($featuredList['challenge_details']) > 300){
					$featuredList['challenge_details'] = substr($featuredList['challenge_details'], 0, 297) . '...';
				} 
				
				$datestr = $featuredList['challenge_close_date'];
				$date	 = strtotime($datestr);

				//Calculate difference
				$diff	= 	$date-time();
				$days	=	floor($diff/(60*60*24));
				
				if($days > 0){
					$showCnt = " Days left : ".$days;
				} else {
					$showCnt = " Closed Paticipations";
				}
		?>
      <div class="col-md-12 col-sm-12 desc-comp-offer">
        <div class="desc-comp-offer-cont">
          <div class="thumbnail-blogs">
            <div class="caption">
              <i class="fa fa-chain"></i>
            </div>
            <img src="<?php echo $image ?>" class="img-fluid" alt="...">
          </div>
          <div class="card-content">
            <span class="read"><a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($featuredList['c_id'])); ?>" class="btn btn-green-fresh">Launch</a></span>
            <h5><?php echo $featuredList['company_name']; ?></h5>
            <h3><?php echo $featuredList['challenge_title']; ?></h3>
            <p class="desc"><?php echo $featuredList['challenge_details']; ?></p>
            <div class="cardd-footer">
             
                            <div>
							
							<?php 
							if($featuredList['audience_pref']!=""){
							$implode = $featuredList['audience_pref'];
							$exp = explode(",", $implode);
							foreach($exp as $newname)
							{
							?>
								<button class="btn btn-general btn-green-fresh" role="button"><?php echo $newname; ?></button>
							<?php 
							}  } else {
							?>	
							<button class="btn btn-general btn-green-fresh" role="button">Other</button>
							<?php } ?>	
							</div>
							
              <div class="daysleft"><i class="fa fa-clock-o"></i><?php echo $showCnt; ?></div>
            </div>
          </div>
        </div>
      </div>
      <?php
			
				} // Foreach End
			
			?>
            
    </div>
  </section>
  
   <?php
	
		} // If End
	
	?>

  <!--====================================================
                      OUR PARTNERS
======================================================-->
  <!--Client Logo Section-->
 	<?php
	if(count($consortium_list) > 0){
	?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Technology Platform Partners</h2>
                    <div class="heading-border"></div>
                </div>
				<?php foreach($consortium_list as $consrotium): ?>
                <div class="col-sm-2"><img src="<?php echo base_url('assets/partners/'.$consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
	<?php } ?>
  <section class="ministrylogo">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center title-bar">
          <h4>Under Aegis of</h4>
          <div class="heading-border"></div>
          <img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo">
        </div>
      </div>
    </div>
  </section>
  <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<script>
jQuery(document).ready(function(){
	jQuery('.portrayed-content').toggle('hide');
    jQuery('a.show-hide').on('click', function(event) {      
         jQuery('.portrayed-content').toggle('show');
    });
	
	
	$('.ask-details').on('click', function(){	
		var c_id = $(this).attr('data-cid');
		var o_id = $(this).attr('data-oid');
		var base_url = '<?php echo base_url() ?>';
		var cs_t = 	$('.token').val();	
		 $.ajax({
			type:'POST',
			url: base_url+'challenge/askfordetails',
			data:'c_id='+c_id+'&o_id='+o_id+'&csrf_test_name='+cs_t,				
			success:function(data){
				var output = JSON.parse(data);								
				$(".token").val(output.token); 			
				
				var textAlert = output.alert_text;
				var stat = output.status;
				if(stat == 'N'){
					$(".ask-details").text('Waiting For Owner Approval');
				}
				swal({
						title: 'Success!',
						text: textAlert,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				
			}
		});	
		
	});
});
</script>


