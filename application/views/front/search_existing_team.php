<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Search For Existing Teams </h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="#">About BYT</a></li>
				<li class="breadcrumb-item active" aria-current="page">Search for Existing Teams</li>
			</ol>
		</nav>
	</div>
</div>
<div class="filterBox">
	<section class="search-sec">
		<form method="post" id="filterData" name="filterData">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="mySidenav" class="sidenav">
							` <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>×</strong></a>
							<div class="scroll">
								<ul>
									<li> <strong> Select Skillset </strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="226" id="skillset" name="skillset[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										1D-BOOS								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="55" id="skillset" name="skillset[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										ADAMS								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="57" id="skillset" name="skillset[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										AJAX								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="204" id="skillset" name="skillset[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										ANSYS Fluent								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="231" id="skillset" name="skillset[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										AUTO Cad								</label>
									</li>
									<span id="more_details">
										<li>
											<input class="form-check-input search-value" type="checkbox" value="64" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											AVL/GT								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="221" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											AVL/GT Software (ICE)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="65" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											AZURE								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="54" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Abaqus								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="199" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Accelerated Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="210" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Administration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="56" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Advisor								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="209" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											After Treatment Devices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="222" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Altair Hypermesh								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="58" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Amazon Webservices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="237" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Android								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="59" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Ansys								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="60" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Arduino								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="61" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											AutoCad								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="62" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											AutoForm								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="203" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Lighting and Light Signaling devices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="63" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Autosar								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="215" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Business Development &amp; Corporate Planning								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="66" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											C Programming								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="67" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											C# Programming								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="68" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											C++								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="69" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											CAN Protocol								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="70" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											CARLA Simulator								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="71" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											CarMaker								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="72" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Catia								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="201" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Certified Technical Auditor for NABL								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="73" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Chemical Mass Balance								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="74" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Chemkin								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="75" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Cloud Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="191" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Combustion Simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="76" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Communication Protocol								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="187" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Control Systems Development Tools								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="77" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											CorelDraw								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="78" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Creo								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="228" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Cruise								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="80" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											DELTA								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="42" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Design								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="198" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Design Validation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="81" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											DevOPs								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="82" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Dspace								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="197" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											EMC Simulation &amp; Validation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="83" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Eagle								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="218" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Emission Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="208" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engine Design								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="192" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engine design and development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="223" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											FEA softwares								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="225" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											FIRE								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="239" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Felucia								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="227" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											GT Power								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="229" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											GT-DRIVE								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="87" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											GT-Drive								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="85" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											GlyphWorks								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="220" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											HEV Powertrain platform								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="89" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											HTML								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="88" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Hardware in Loop (HIL)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="90" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Hyperworks								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="185" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											IPR								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="202" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											ISO/IEC 17025								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="91" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Industrial Source Complex Model								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="216" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Institutional Relations &amp; Collaborations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="92" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Iso26262								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="236" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											JAVA								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="93" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Javascript								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="95" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											LS Dyna								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="94" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											LabVIEW								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="205" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											MATLAB								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="240" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											MTS RPC								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="100" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											MTS RPC Pro								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="186" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Manufacturing simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="213" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Material Characterization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="96" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Matlab &amp; Simulink								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="97" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Medini								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="98" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Minitab								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="99" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Model in Loop								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="101" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											MultiSim								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="103" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											NCode								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="234" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											NXP								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="102" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Nastran								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="104" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Office Automation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="105" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Open ECU								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="106" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Oracle DBA								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="107" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											PHP								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="108" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Positive Matrix Factorization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="230" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Pro-E								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="110" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Python								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="111" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Qform								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="184" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Rapid Prototyping								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="112" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Rasberry Pi								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="190" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Real Time Powertrain Simulation (Plant Model)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="224" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Regulations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="195" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Regulatory Framework								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="212" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Resource Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="113" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Robot Operating System (ROS)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="114" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											SAP Process and DMU								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="115" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											SCRUM								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="121" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											SQL								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="116" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Sensor Fusion								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="233" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Siemens NX								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="206" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Simulink								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="117" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Software in Loop								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="232" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Solid Works								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="120" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Solidworks								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="196" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Structural Durability								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="214" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											System Design &amp; Implementation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="217" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Technology &amp; Innovation management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="122" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											TensorFlow								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="189" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Testing and Validation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="193" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Thermal Simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="211" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Training and Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="219" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Transmission Design								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="200" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Type approval and CoP								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="194" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle Type approval and certification								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="123" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vmware vSphere								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="207" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											dSPACE								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="235" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											nCode Glyphworks								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="50" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Other								</label>
										</li>
									</span>
									<a href="javascript:void(0);" class="skill-more" id="skill-readmore">Show More &gt;&gt; </a>
									<a href="javascript:void(0);" class="skill-less" style="display:none;" id="skill-readless">Show Less &gt;&gt;</a>
								</ul>
								<ul>
									<li> <strong> Select Domain Expertise </strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="159" id="domain_exp" name="domain_exp[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										ADAS								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="6" id="domain_exp" name="domain_exp[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										AI/ML								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="250" id="domain_exp" name="domain_exp[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										ARAI Directorship								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="133" id="domain_exp" name="domain_exp[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Administration								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="254" id="domain_exp" name="domain_exp[]">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Advanced Alternate Materials								</label>
									</li>
									<span id="more_results">
										<li>
											<input class="form-check-input search-value" type="checkbox" value="234" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Advanced Battery Solutions								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="240" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Advanced Computer Simulations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="134" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Advanced Emission Controls								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="162" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Air quality management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="255" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Alloy Design &amp; Process Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="163" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Alternate Fuels Technologies								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="16" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Alternative Fuel Characterization and Combustion								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="135" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Analysis for structural durability								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="164" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Artificial Intelligence								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="4" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="136" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Aerodynamics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="243" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive ECU Evaluation and Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="137" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Industry Standardisation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="138" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Lighting and Light Signalling devices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="244" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Plant models Simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="239" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Automotive Research								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="165" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Autonomous Control								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="139" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Battery/BMS Engineering								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="166" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Benchmarking								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="167" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Business Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="219" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Business Development &amp; Corporate Planning								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="141" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											CMVR type approval activities								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="168" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Calibration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="169" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Certification								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="170" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Certification Auditing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="140" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Chassis Controllers								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="171" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Combustion and Emission								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="142" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Combustion and Emission optimization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="264" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Composite Bonding								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="143" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Composites								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="172" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Computational fluid dynamics (CFD)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="173" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Computer Simulation (CAE)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="174" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Conformity of Production (CoP)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="175" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Control systems								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="144" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Controller design and optimization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="7" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Controls								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="176" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Corporate Law								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="145" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Corporate Planning								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="146" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Crash Safety								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="177" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Customer Relationship Management (CRM)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="178" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Cyber -Physical System								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="1" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Cyber Security								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="261" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											DIffusion Kinetics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="181" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Data Acquisition								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="182" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Data Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="183" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Data Mining								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="147" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Diesel combustion and emission development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="150" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											EMC Simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="194" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											EV Charger Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="195" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											EV Charger Protocol								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="221" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Education &amp; Training								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="184" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Education And Training								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="185" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electric Mobility								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="148" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electric Motors								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="149" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electrical System Integration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="187" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electro-Hydraulic Simulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="229" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electrochemical Storage								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="227" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electrochemistry								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="188" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electromagnetic Compatibility (EMC)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="189" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electromagnetic Interference(EMI)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="190" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Electronics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="3" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Embedded								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="235" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Emission Control								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="191" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Emission Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="151" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Energy Storage devices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="192" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engine Design								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="246" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engine/ Chassis Dyno Test Cell Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="245" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engine/ Chassis Dyno Test Cell Instrumentation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="252" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Engineering Materials &amp; Characterisation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="215" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Experimental Stress Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="152" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fatigue								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="216" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fatigue Life Evaluation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="153" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fatigue and Vibration Technology								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="196" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fatigue life estimation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="197" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Forging								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="231" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fuel Cells								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="198" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Fuel Cells and Supercapacitors								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="2" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Functional Safety								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="199" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Functional Safety Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="249" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											General Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="12" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											HIL								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="241" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											HIL with RPT								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="18" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											HR Management &amp; Admin								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="258" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											High Temperature Alloys								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="200" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Homologation Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="155" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Homologation for Safety Critical Components								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="201" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Human Resource Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="157" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Hybrid Energy Storage								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="156" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Hybrid powertrains								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="251" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											ICAT Directorship								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="232" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											ISRO								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="161" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											In-use vehicle emissions testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="10" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Information Technology								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="202" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Institutional Relations &amp; Collaborations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="203" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Instrumentation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="158" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Instrumentation calibration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="204" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Integrated Computational Materials Engineering								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="160" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Intellectual Property Rights								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="205" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Intelligent Transportation System								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="207" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											IoT								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="67" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Laboratory Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="233" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Launch Vehicle Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="93" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Legal								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="256" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Leightweighting								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="94" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Light Weighting								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="257" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Lightweight High strength Alloys								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="230" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Lithium Ion								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="11" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											MIL								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="96" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Machine Learning								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="98" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Manufacturing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="217" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Material Characterization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="253" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Material Fatigue &amp; Failure Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="99" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Material Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="103" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Mathematical System Modeling								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="262" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Metal Bonding								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="102" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Metal Forming Simulation and Modeling								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="228" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Metallic Corrosion								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="260" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Metalurgy								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="68" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Motor Controllers and drives								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="298" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											NGO-LM								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="69" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Nanotechnology								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="104" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Network Administration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="105" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Noise, Vibration and Harshness								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="70" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Occupant Safety								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="106" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Particulate matter evaluation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="71" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Passive Safety								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="72" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Physical Simulation of components/systems								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="263" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Polymer Bonding								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="73" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Power electronics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="9" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Powertrain								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="107" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Powertrain System Engineering								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="108" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Product Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="109" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Production Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="110" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Project Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="111" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Public Relation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="112" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Quality Systems Management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="113" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Rapid Prototyping &amp; Tools development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="114" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Recycling								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="115" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Reliability and Structural Durability								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="116" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Research and Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="117" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Robotics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="118" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Safety and Reliability								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="120" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Safety components Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="75" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Sensing Technologies								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="218" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Service Load Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="76" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Service Load Data Acquisition								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="77" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Simulation based Testing and Validation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="78" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Smart Materials								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="8" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Software								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="121" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Software Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="122" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Standard Formulation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="79" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Stochastic Optimization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="123" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Structural Dynamics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="124" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											System Debugging and Data Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="125" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											System Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="126" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											System Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="127" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Taxation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="80" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Technical Auditor (NABL)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="81" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Technical Trainer (RTO)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="220" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Technology &amp; Innovation management								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="237" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											TestBed Development &amp; Laboratory Instrumentation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="128" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Testing and Certification								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="259" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Titanium Alloys								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="83" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Traction Devices								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="247" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Training (RTO)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="248" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Training (Young Professionals)								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="84" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Transmission design								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="85" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Type Approval Certification &amp; Regulations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="129" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Type Approval Testing								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="86" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											V2X &amp; Connected Cars								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="87" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle / component testing solutions development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="130" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle Dynamics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="88" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle Evaluation for Dynamics								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="131" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle Integration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="89" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicle-to-Grid								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="238" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicular Regulations								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="236" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicular Safety								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="242" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Vehicular Testing, Evaluation and Data Analysis								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="132" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Verification and Validation								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="90" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Virtual engineering								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="301" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											jjj								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="302" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											kkk								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="91" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											xEV Conceptualization								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="92" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											xEV Sizing &amp; Development								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="209" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Other								</label>
										</li>
									</span>
									<a href="javascript:void(0);" class="domain-more" id="domain-readmore">Show More &gt;&gt; </a>
									<a href="javascript:void(0);" class="domain-less" style="display:none;" id="domain-readless">Show Less &gt;&gt;</a>
								</ul>
								<ul>
									<li> <strong> Employment Status </strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="Self Employed" id="emp_status" name="emp_status">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Self Employed								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="Consultant" id="emp_status" name="emp_status">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Consultant								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="Freelancer" id="emp_status" name="emp_status">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Freelancer								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="Company" id="emp_status" name="emp_status">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Company								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="Academia" id="emp_status" name="emp_status">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										Academia								</label>
									</li>
									<span id="more_emp">
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Administration" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Administration								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Blue Collar Worker" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Blue Collar Worker								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Entrepreneur" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Entrepreneur								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Executive" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Executive								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Homemaker" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Homemaker								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Independent Consultant" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Independent Consultant								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Intern / Trainee" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Intern / Trainee								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Professor" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Professor								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Retired" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Retired								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Service" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Service								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Student" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Student								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Subject Matter Expert" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Subject Matter Expert								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Teacher" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Teacher								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Technician" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Technician								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Other" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Other								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Test" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Test								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="Dr" id="skillset" name="skillset[]">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											Dr								</label>
										</li>
									</span>
									<a href="javascript:void(0);" class="employement-more" id="skill-readmore">Show More &gt;&gt; </a>
									<a href="javascript:void(0);" class="employement-less" style="display:none;" id="employement-readless">Show Less &gt;&gt;</a>
								</ul>
								<ul>
									<li> <strong> Year Of Experience </strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="5" id="year_of_exp" name="year_of_exp">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										5+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="10" id="year_of_exp" name="year_of_exp">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										10+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="15" id="year_of_exp" name="year_of_exp">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										15+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="20" id="year_of_exp" name="year_of_exp">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										20+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="25" id="year_of_exp" name="year_of_exp">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										25+								</label>
									</li>
									<span id="more_exp">
										<li>
											<input class="form-check-input search-value" type="checkbox" value="30" id="year_of_exp" name="year_of_exp">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											30+								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="35" id="year_of_exp" name="year_of_exp">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											35+								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="40" id="year_of_exp" name="year_of_exp">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											40+								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="45" id="year_of_exp" name="year_of_exp">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											45+								</label>
										</li>
										<li>
											<input class="form-check-input search-value" type="checkbox" value="50" id="year_of_exp" name="year_of_exp">
											<label class="form-check-label floatinglabel" for="defaultCheck1">
											50+								</label>
										</li>
									</span>
									<a href="javascript:void(0);" class="exp-more" id="exp-readmore">Show More &gt;&gt; </a>
									<a href="javascript:void(0);" class="exp-less" style="display:none;" id="exp-readless">Show Less &gt;&gt;</a>
								</ul>
								<ul>
									<li> <strong> No. Of Papers Publications </strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="10" id="no_of_pub" name="no_of_pub">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										10+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="20" id="no_of_pub" name="no_of_pub">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										20+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="30" id="no_of_pub" name="no_of_pub">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										30+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="40" id="no_of_pub" name="no_of_pub">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										40+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="50" id="no_of_pub" name="no_of_pub">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										50+								</label>
									</li>
								</ul>
								<ul>
									<li> <strong> No. Of Patents</strong> </li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="10" id="no_of_patents" name="no_of_patents">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										10+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="20" id="no_of_patents" name="no_of_patents">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										20+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="30" id="no_of_patents" name="no_of_patents">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										30+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="40" id="no_of_patents" name="no_of_patents">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										40+								</label>
									</li>
									<li>
										<input class="form-check-input search-value" type="checkbox" value="50" id="no_of_patents" name="no_of_patents">
										<label class="form-check-label floatinglabel" for="defaultCheck1">
										50+								</label>
									</li>
								</ul>
							</div>
						</div>
						<div class="row d-flex justify-content-center search_Box">
							<div class="col-md-6">
								<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword">
								<button type="button" class="btn btn-primary searchButton"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							<div class="clearAll"><a class="btn btn-primary clearAll" id="reset-val">Clear All</a>
							</div>
							<div class="filter"><button type="button" onclick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
							</div>
							<div class="col-md-12"></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row justify-content-center">
						<div class="boxSection25">
							<ul class="list-group list-group-horizontal search_existing_team_outer">
								<li class="list-group-item">
									<div class="inner-box">
										<img src="<?php echo base_url() ?>assets/img/aboutus.jpg" alt="Banner" class="img-fluid">
										<h3>Team Name</h3>
										<ul>
											<li> 5 Members </li>
											<li>-</li>
											<li>2 Available SLots</li>
										</ul>
									</div>
									<div class="border_top_bottom">
										<a href="#" class="d-inline-block chatButton">Chat</a>
									</div>
								</li>
								<li class="list-group-item">
									<div class="inner-box">
										<div class="form-group mt-4">
											<textarea class="form-control" id="exampleFormControlTextarea1" name="contents"></textarea>
											<label class="form-control-placeholder" for="exampleFormControlTextarea1">Brief Info about Challenge <em>*</em></label>
										</div>
										<h3 class="pt-0">Looking For (max 3 Lines)</h3>
										<div class="Skillset">
											<ul>
												<li>Skillset</li>
												<li>Skillset</li>
												<li>Skillset</li>
												<li>Skillset</li>
												<li>Skillset</li>
												<li>Skillset</li>
											</ul>
										</div>
									</div>
									<div class="border_top_bottom">
										<a href="#" class="btn btn-primary w-100">View Details</a>
									</div>
								</li>
								<li class="list-group-item">
									<div class="team_listing owl-carousel owl-theme" id="SearchTeamListingSlider">
										<div class="item text-center">
											<img src="<?php echo base_url() ?>assets/img/testimonial-2.jpg" alt="experts">
											<h4>Name (Team Owner)</h4>
											<div class="Skillset">
												<ul>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
												</ul>
											</div>
											<h4>Team Member 1</h4>
											<a href="#" class="d-inline-block">Apply</a>
										</div>
										<div class="item text-center">
											<img src="<?php echo base_url() ?>assets/img/testimonial-2.jpg" alt="experts">
											<h4>Name (Team Owner)</h4>
											<div class="Skillset">
												<ul>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
													<li>Skillset</li>
												</ul>
											</div>
											<h4>Team Member 1</h4>
											<a href="#" class="d-inline-block">Apply</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>	
	function openNav() {
		var isMobile = false; //initiate as false
		// device detection
		if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
		/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
	}
	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
	}
</script>