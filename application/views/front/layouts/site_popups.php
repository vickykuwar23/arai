<?php
$module_name=$this->router->fetch_class();
$mehtod_name=$this->router->fetch_method();

$tdf_popup = $this->master_model->getRecords("popups",array('slug'=>'tdf_popup','status'=>'Active'));

$cts_popup = $this->master_model->getRecords("popups",array('slug'=>'cts_popup','status'=>'Active'));

$byt_popup = $this->master_model->getRecords("popups",array('slug'=>'byt_popup','status'=>'Active'));


$resource_sharing_popup = $this->master_model->getRecords("popups",array('slug'=>'resource_sharing_popup','status'=>'Active'));

$technology_transfer_popup = $this->master_model->getRecords("popups",array('slug'=>'technology_transfer_popup','status'=>'Active'));


?>

<?php if(count($tdf_popup)>0){ ?>
<div class="modal" id="tdf_popup">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="col-md-12">
        <?php echo $tdf_popup[0]['content'] ?>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<?php } ?>

<?php if(count($cts_popup)>0){ ?>
<div class="modal" id="cts_popup">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="col-md-12">
        <?php echo $cts_popup[0]['content'] ?>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<?php } ?>

<?php if(count($byt_popup)>0){ ?>
<div class="modal" id="byt_popup">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="col-md-12">
        <?php echo $byt_popup[0]['content'] ?>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<?php } ?>

<?php if(count($resource_sharing_popup)>0){ ?>
<div class="modal" id="resource_sharing_popup">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="col-md-12">
        <?php echo $resource_sharing_popup[0]['content'] ?>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<?php } ?>

<?php if(count($technology_transfer_popup)>0){ ?>
<div class="modal" id="technology_transfer_popup">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
     <div class="row">
      <div class="col-md-12">
        <?php echo $technology_transfer_popup[0]['content'] ?>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
<?php } ?>



<script type="text/javascript">
jQuery(document).ready(function($) {
    <?php if($module_name=='feeds' && $mehtod_name=='index' && count($tdf_popup)>0){ ?>
        $('#tdf_popup').modal('show');
    <?php } ?>

    <?php if($module_name=='challenge' && $mehtod_name=='index' && count($cts_popup)>0){ ?>
        $('#cts_popup').modal('show');
    <?php } ?>

     <?php if($module_name=='home' && $mehtod_name=='aboutByt' && count($byt_popup)>0){ ?>
        $('#byt_popup').modal('show');
    <?php } ?>

     <?php if($module_name=='resource_sharing' && $mehtod_name=='index' && count($resource_sharing_popup)>0){ ?>
        $('#resource_sharing_popup').modal('show');
    <?php } ?>

      <?php if($module_name=='technology_transfer' && $mehtod_name=='index' && count($technology_transfer_popup)>0){ ?>
        $('#technology_transfer_popup').modal('show');
    <?php } ?>

    
  
});
</script>