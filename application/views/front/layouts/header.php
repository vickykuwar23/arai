<?php
   // Create Object
   $encrptopenssl_header =  New Opensslencryptdecrypt();
   $setting_header = $this->master_model->getRecords("setting",array('id'=>'1'));
   $pageSite_title = $encrptopenssl_header->decrypt($setting_header[0]['site_title']);
   ?>
<!--
   License: Creative Commons Attribution 4.0 Unported
   License URL: https://creativecommons.org/licenses/by/4.0/
   -->
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176431357-1"></script>
         <script>
           window.dataLayer = window.dataLayer || [];
           function gtag(){dataLayer.push(arguments);}
           gtag('js', new Date());
          
           gtag('config', 'UA-176431357-1');
         </script> -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <title><?php echo $pageSite_title; ?></title>
      <link rel="shortcut icon" href="<?php echo base_url('assets/front/') ?>img/favicon.ico">
      <!-- Global Stylesheets -->
      <link
         href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap"
         rel="stylesheet">
      <link href="<?php echo base_url('assets/front/') ?>css/bootstrap/newbootstrap/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>font-awesome-4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/LineIcons.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/line-awesome.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/owl-carousel/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/owl-carousel/owl.theme.default.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/animate.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/aos.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/style.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/custom.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/add.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/jquery.mCustomScrollbar.css">
      <link href="<?php echo base_url('assets/front/') ?>css/smart_wizard.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url('assets/front/') ?>css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/sweetalert2.min.css">
      <link href="<?php echo base_url('assets/front/') ?>css/select2.min.css" rel="stylesheet" />
      <script src="<?php echo base_url('assets/front/') ?>js/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.all.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/core.js"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/select2.min.js"></script>
      
      <?php 
      if($this->router->fetch_class() == 'home' && $this->router->fetch_method() == 'index')
      { ?>
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/home_page_featured_tdf.css">
      <?php } ?>
      
      <style>
         .error, .error p
         {
         color: red !important;
         font-size: 13px !important;
         margin-bottom:0;
         }
         .blank{
         color: red;
         }


 .progress-bar2 {
            position: relative;
            height: 35px;
            width: 35px;
            margin-left:5px;
        }

        .progress-bar2 div {
            position: absolute;
            height: 35px;
            width: 35px;
            border-radius: 50%;
        }

     .progress-bar2 div span {
	position: absolute;
	font-family: Arial;
	font-size: 11px;
	line-height: 30px;
	height: 31px;
	width: 31px;
	left: 2px;
	top: 2px;
	color: #FFF;
	text-align: center;
	border-radius: 50%;
	background-color: #c80032;
}

        .progress-bar2 .background {
            background-color: #b3cef6;
        }

        .progress-bar2 .rotate {
            clip: rect(0 25px 35px 0);
            background-color: #4b86db;
        }

        .progress-bar2 .left {
            clip: rect(0 25px 35px 0);
            opacity: 1;
            background-color: #b3cef6;
        }

        .progress-bar2 .right {
            clip: rect(0 25px 35px 0);
            transform: rotate(180deg);
            opacity: 0;
            background-color: #4b86db;
        }

        @keyframes toggle {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

      </style>


<style>
  .goog-te-banner-frame.skiptranslate {display: none !important;} 
body {top: 0px !important; }

.lang_active{
  background-color: #fff !important;
  color:#000 !important;
}

header .topmenu .top-data li a.log-top {
    font-weight: 400;
    font-size: 11px;   
    height: 32px;
}
#google_translate_element{width:300px;float:right;text-align:right;display:block}
.goog-te-banner-frame.skiptranslate { display: none !important;} 
body { top: 0px !important; }
#goog-gt-tt{display: none !important; top: 0px !important; } 
.goog-tooltip skiptranslate{display: none !important; top: 0px !important; } 
.activity-root { display: hide !important;} 
.status-message { display: hide !important;}
.started-activity-container { display: hide !important;}
</style>

      <?php if ($this->session->flashdata('error_pemission') != "") 
         { ?>
      <script type="text/javascript">
         setTimeout(function () {
           swal( 'Warning!','You don\'t have permission to access this page !','warning');
           }, 200);
      </script>
      <?php } ?>
      <?php if ($this->session->flashdata('error_approval') != "") 
         { ?>
      <script type="text/javascript">
         setTimeout(function () {
           swal( 'Warning!','Your account is not approved yet !','warning');
           }, 200);
      </script>
      <?php } ?>
   </head>
   <?php //CODE ADDED BY SAGAR FOR CHECKING THE TERM & CONDITIONS ARE ACCEPTED OR NOT FOR CREATE TEAM
      if($this->router->fetch_class() != 'team' && $this->router->fetch_method() != 'create')
      {
        $clear_session['TEAM_ACCEPT_USER_ID'] = $clear_session['TEAM_ACCEPT_CHALLENGE_ID'] = '';
        $this->session->set_userdata($clear_session);
      } 
      ?>
   <body id="page-top">
  
  
      <div id="preloader"></div>
      <!-- // HEADER -->
      <?php 
         //$url_segmanet=$this->uri->segment(1);if ( $url_segmanet != 'login' ) { ?>
      <header>
         <!-- Top Navbar  -->
         <div class="top-menubar" id="home">
            <div class="topmenu">
               <div class="container-fluid">
                  <div class="row" data-aos="fade-down">
                     <div class="col-md-9">
                        <ul class="list-inline top-contacts">
                           <li>
                              <a href="https://www.araiindia.com/"  target="_blank"  class="whtbg"> <img src="<?php echo base_url('assets/front/') ?>img/logo-header.png"></a>
                           </li>
                           <li>
                              <a href="<?php echo base_url('challenge') ?>">Collaborative Technology Solution</a>
                           </li>
                            <li>
                              |
                              <a href="<?php echo base_url('home/aboutByt') ?>">Build Your Team</a>
                           </li>
                           <li>
                              |
                              <a href="<?php echo base_url('home/viewAllExperts') ?>">Expert Connect</a>
                           </li>
                           <li>
                              |
                              <a href="<?php echo base_url('feeds') ?>">Technology Wall</a>
                           </li>
                           <li>
                              | <?php if($this->session->userdata('user_id') ==""): ?>
                              <a href="<?php echo base_url('login') ?>">Resource Sharing</a>
                              <?php else: ?>
                              <!-- <a href="javascript:void(0);" onClick="open_tools_modal();">Tool Connect</a> -->
                              <a href="<?php echo base_url('resource_sharing') ?>" >Resource Sharing</a>
                              <?php endif; ?>
                           </li>
                          
                           <li>
                              |
                              <a href="<?php echo base_url('technology_transfer') ?>">Technology Transfer</a>
                           </li>
                           <!--<li>
                              <a href="<?php echo base_url('challenge') ?>"> List of Open Challenges</a>|
                              </li>
                              <li>
                              <a href="#"> List of Public Webinars</a>
                              </li>-->
                        </ul>
                     </div>
                     <div class="col-md-3">
                        <ul class="list-inline top-data">
                           <!-- <li><a href="#" target="_empty">Skip to main Content</a>|</li>
                              <li><a href="#" target="_empty">A+</a>|</li>
                              <li><a href="#" target="_empty">A</a>|</li>
                              <li><a href="#" target="_empty">A-</a>|</li> -->
                           <?php
                              if ($this->session->userdata('user_id')!='') {
                              $encrptopenssl_he =  New Opensslencryptdecrypt(); 
                              $user_category=$this->session->userdata('user_category'); 
                              $user_sub_category=$this->session->userdata('user_sub_category'); 
                              $user_id = $this->session->userdata('user_id');
                              $user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
                              // print_r($user_data);
                              
                              $user_arr = array();
                              if(count($user_data)){  
                                             
                                 foreach($user_data as $row_val){        
                                             
                                     $row_val['title'] = $encrptopenssl_he->decrypt($row_val['title']);
                                     $row_val['first_name'] = $encrptopenssl_he->decrypt($row_val['first_name']);
                                     $row_val['middle_name'] = $encrptopenssl_he->decrypt($row_val['middle_name']);
                                     $row_val['last_name'] = $encrptopenssl_he->decrypt($row_val['last_name']);
                                    
                                     $user_arr[] = $row_val;
                                 }
                              
                              }
                              $full_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];
                              
                                 ?>
                           <li><?php echo "<small class='header_user_name'>Welcome &nbsp;<br> ".$full_name."</small>"; ?></li>
                           <li><a href="<?php echo base_url('login/logout') ?>" style="font-size: 12px; padding:5px;" class="log-top btn btn-primary btn-xs">Logout</a></li>
                           <li class="dropdown">
                              <a href="#" style="font-size: 12px; padding:5px;" class="log-top btn btn-success btn-xs dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>  </a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                 <?php
                                    if($user_category==2){
                                      $profile_url=base_url('organization');
                                    } 
                                    
                                    // if($user_sub_category==11 || $user_sub_category == 1){
                                    //   $profile_url=base_url('profile');
                                    // } 
                                    
                                    if($user_category==1){
                                      $profile_url=base_url('profile');
                                    } 
                                    
                                    if( $user_sub_category == 2){
                                      $profile_url=base_url('profile');
                                    } 
                                    ?>
                                 <?php if ($profile_url!=''): ?>
                                 <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo $profile_url ;?>'">My Profile</button>  
                                 <?php endif ?>
                                 <?php  if( $user_category == 2){ ?>
                                 <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/add') ?>'">Post a Challenge</button>
                                 <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/myChallenges') ?>'">My Challenges</button>
                                 <?php } ?>
                                 <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/applyChallengelist') ?>'">List of Applied Challenges</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('myteams/myCreatedteams') ?>'" type="button">My Teams</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('myspace/blogs/') ?>'" type="button">My Space</button>
                                 <?php /*
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('webinar/mywebinar') ?>'" type="button">My Webinars</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('questions_answers_forum/myQuestions') ?>'" type="button">My Questions</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('blogs_technology_wall/myBlogs') ?>'" type="button">My Blogs</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('knowledge_repository/my_knowledge_repository') ?>'" type="button">My Knowledge Repository</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('resource_sharing/my_resource') ?>'" type="button">My Resource Sharing</button>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('technology_transfer/my_technology_transfer') ?>'" type="button">My Technology transfer</button>
                                 */?>
                                 <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('profile/changePassword') ?>'" type="button">Change Password</button>
                                 <button class="dropdown-item user-class-dt" type="button">Delete Account</button>
                              </div>
                           </li>
                           <?php } else{?>
                           <li><a href="<?php echo base_url('login') ?>" class="log-top btn btn-primary btn-xs">Login</a></li>
                           <li><a href="<?php echo base_url('registration') ?>" class="log-top btn btn-success btn-xs" >Register</a></li>
                           
                           <?php } ?>

                      <?php if($this->session->userdata('user_id') !=""){ 
                        $profile_complete_data=$this->check_permissions->profile_complete_percentage();
                        $initial =$profile_complete_data['initial'];
                        $profile_complete_per =$profile_complete_data['profile_complete_per'];
                        ?> 
                           
                                <li>
                           <div class="progress-bar2 position" data-percent="<?php echo $profile_complete_per ?>" data-duration="5000" data-color="#333,#FFF"></div>
                           </li>

                       <?php } ?>

                        <li class="notranslate lang_sec" ><a id="eng_li" href="#googtrans(en|en)" class="lang-en lang-select log-top btn btn-primary btn-xs" data-lang="en">English</a></li>
                        <li class="notranslate lang_sec" ><a id="hi_li" href="#googtrans(hi|hi)" class="lang-es lang-select log-top btn btn-primary btn-xs" data-lang="hi">हिंदी</a></li>
                        </ul>

                        
                     </div>
                  </div>
               </div>
            </div>
         </div>

 <!-- <div class="progress-bar2 position" data-percent="10" data-duration="1000" data-color="#ccc,yellow"></div> -->

              

         <!-- Navbar -->
         <nav class="navbar navbar-expand-lg navbar-light" id="mainNav" data-toggle="affix">
            <div class="container-fluid" data-aos="fade-up">
               <a class="navbar-brand smooth-scroll" href="<?php echo base_url() ?>">
               <img src="<?php echo base_url('assets/front/') ?>img/logo1.png" alt="logo">
               </a>
               <!-- <a href="https://www.araiindia.com/" target="_blank" class="whtbg"> <img src="<?php echo base_url('assets/front/') ?>img/ARAI-Logo.png"></a> -->
               <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                  data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                  aria-label="Toggle navigation">
               <i class="fa fa-reorder"></i>
               </button>
               <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                     <li class="nav-item <?php if($page_title == "Home"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url() ?>">Home</a></li>
                   
                     <li class="nav-item dropdown show <?php if($page_title == "About"){ ?> active <?php } ?>">
                        <a class="nav-link dropdown-toggle smooth-scroll" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">About</a>
                        <div class="dropdown-menu dropdown-cust show" aria-labelledby="navbarDropdownMenuLink" style="display: none;">
                           <a class="dropdown-item" href="<?php echo base_url('home/aboutUs') ?>">TECHNOVUUS</a>
                           <a class="dropdown-item" href="<?php echo base_url('home/cts') ?>">COLLABORATIVE TECH. SOL. </a>
                           <a class="dropdown-item" href="<?php echo base_url('home/expert_connect') ?>">EXPERT CONNECT</a>
                           <a class="dropdown-item" href="<?php echo base_url('home/build_your_team') ?>">About BYT</a>
                           <a class="dropdown-item" href="<?php echo base_url('home/about_technology_wall') ?>">About Technology Wall</a>
                           <a class="dropdown-item" href="<?php echo base_url('home/about_technology_transfer') ?>">About Technology Transfer</a>
                           <a class="dropdown-item" href="<?php echo base_url('home/about_resource_sharing') ?>">About Resource Sharing</a>
                        </div>
                     </li>
                     <!--<li class="nav-item "><a class="nav-link smooth-scroll" href="<?php echo base_url('home/aboutUs') ?>" >About Us</a></li>-->
                     <li class="nav-item <?php if($page_title == "Blog"){ ?> active <?php } ?>" ><a class="nav-link smooth-scroll" href="<?php echo base_url('blogs') ?>">News & Updates</a></li>
                     <!--<li class="nav-item <?php if($page_title == "Challenge"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url('challenge/add') ?>">Submit Challenge</a></li>-->
                     <li class="nav-item <?php if($page_title == "FAQ"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url('home/faq') ?>">Help & FAQ<small>s</small></a></li>
                     <!--<li class="nav-item"><a href="#" class="nav-link smooth-scroll Email" data-toggle="modal" data-target="#exampleModal"> Subscription</a></li>-->            
                     <li class="nav-item"><a href="javascript:void(0)" class="nav-link smooth-scroll Email" onclick="open_subscription_modal()"> Subscription</a></li>
                     <li class="nav-item"><a class="nav-link smooth-scroll" href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                     <li>
                        <div class="top-menubar-nav menudisplayNone">
                           <div class="topmenu ">
                              <div class="container">
                                 <div class="row">
                                    <div class="col-md-9">
                                       <ul class="list-inline top-contacts">
                                          <li>
                                             <i class="fa fa-envelope"></i> Email: <a
                                                href="mailto:info@araiindia.com">info@araiindia.com</a>
                                          </li>
                                          <li>
                                             <i class="fa fa-phone"></i> Hotline: (1) 396 4587 99
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="col-md-3">
                                       <ul class="list-inline top-data">
                                          <li><a href="#" target="_empty"><i
                                             class="fa top-social fa-facebook"></i></a></li>
                                          <li><a href="#" target="_empty"><i
                                             class="fa top-social fa-twitter"></i></a></li>
                                          <li><a href="#" target="_empty"><i
                                             class="fa top-social fa-google-plus"></i></a></li>
                                          <li><a href="#" class="log-top" data-toggle="modal"
                                             data-target="#login-modal">Login</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>
      <?php //} ?>



