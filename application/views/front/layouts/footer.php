
<?php 
$encrptopenssl_footer =  New Opensslencryptdecrypt();
$footer_data = $this->master_model->getRecords("cms", array("id" => '1'));
$cmsArr = array();
foreach($footer_data as $row_val){
				
	$row_val['page_title'] 			= $encrptopenssl_footer->decrypt($row_val['page_title']);	
	$row_val['page_description'] 	= $encrptopenssl_footer->decrypt($row_val['page_description']);
	$row_val['id'] 			= $row_val['id'];	
	$cmsArr[] = $row_val;
}

if (strlen($cmsArr[0]['page_description']) > 100){
	$cmsArr[0]['page_description'] = substr($cmsArr[0]['page_description'], 0, 97) . '...';
} 
	
$social_data = $this->master_model->getRecords("setting", array("id" => '1'));	
$socialArr = array();
foreach($social_data as $sow_val){
				
	$sow_val['facebook_url'] 	= $encrptopenssl_footer->decrypt($sow_val['facebook_url']);	
	$sow_val['twitter_url'] 	= $encrptopenssl_footer->decrypt($sow_val['twitter_url']);
	$sow_val['linkedin_url'] 	= $encrptopenssl_footer->decrypt($sow_val['linkedin_url']);
	$sow_val['google_plus_url'] = $encrptopenssl_footer->decrypt($sow_val['google_plus_url']);
	$sow_val['contact_no'] 		= $encrptopenssl_footer->decrypt($sow_val['contact_no']);
	$sow_val['off_address'] 	= $encrptopenssl_footer->decrypt($sow_val['off_address']);
	$sow_val['email_id'] 		= $encrptopenssl_footer->decrypt($sow_val['email_id']);
	$sow_val['id'] 				= $sow_val['id'];	
	$socialArr[] = $sow_val;
}


?>
<!-- // FOOTER -->
<!-- Modal -->
<div class="modal fade" id="SubscriptionModal" tabindex="-1" role="dialog" aria-labelledby="SubscriptionModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_content_outer">
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="toolsModal" tabindex="-1" role="dialog" aria-labelledby="toolsModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_tools_outer">
	</div>
</div>




<?php //$url_segmanet=$this->uri->segment(1);if ( $url_segmanet != 'login' ) { ?>
    <footer>
        <div id="footer-s1" class="footer-s1">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-about mt-40">
                                <div class="logo">
                                    <h6><?php echo $cmsArr[0]['page_title']; ?></h6>
                                </div>
                                <p>An ecosystem to empower the right ideas for innovation and technology development...<a href="<?php echo base_url('home/aboutUs'); ?>">Read More</a>
<?php //echo $cmsArr[0]['page_description']; ?> </p>

                                <ul class="mt-20">
                                <?php if($socialArr[0]['linkedin_url']!=""): ?>
                                    <li><a href="<?php echo $socialArr[0]['linkedin_url']; ?>" target="_blank"> <i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
									<?php endif; ?>
                               
                                    </ul>


                                <!-- <ul class="mt-20">
									<?php if($socialArr[0]['facebook_url']!=""): ?>
                                    <li><a href="<?php echo $socialArr[0]['facebook_url']; ?>" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
									<?php endif; ?>
									<?php if($socialArr[0]['twitter_url']!=""): ?>
                                    <li><a href="<?php echo $socialArr[0]['twitter_url']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<?php endif; ?>
									<?php if($socialArr[0]['google_plus_url']!=""): ?>
                                    <li><a href="<?php echo $socialArr[0]['google_plus_url']; ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
									<?php endif; ?>
								   <?php if($socialArr[0]['linkedin_url']!=""): ?>
									<li><a href="<?php echo $socialArr[0]['linkedin_url']; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
									<?php endif; ?>
								</ul> -->
                            </div>
                           <div class="footerLogo">
                           <strong style="color: #c80032;">Powered by</strong>

                           <a href="https://www.araiindia.com" target="_blank"><img src="<?php echo base_url('assets/img/logo-footer.png'); ?>" alt="ARAI"></a>

                           

                           </div>

                            <!-- footer about -->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="footer-link mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Quick Links</h6>
                                </div>
                                <ul>
                                    <li><a href="<?php echo base_url() ?>"><i class="fa fa-angle-right"></i>Home</a></li>
                                    <li><a href="<?php echo base_url('home/aboutUs') ?>"><i class="fa fa-angle-right"></i>About Technovuus</a></li>
                                    <li><a href="<?php echo base_url('registration') ?>"><i class="fa fa-angle-right"></i>Registration</a></li>
                                    <li><a href="<?php echo base_url('challenge') ?>"><i class="fa fa-angle-right"></i>Collaborative Tech. Sol.</a></li>
                                    <li><a href="<?php echo base_url('home/aboutByt') ?>"><i class="fa fa-angle-right"></i>Build Your Team</a></li>    
                                </ul>
                                <ul>

                                    <li><a href="<?php echo base_url('home/viewAllExperts') ?>"><i class="fa fa-angle-right"></i>Expert Connect</a></li>
                                    <li><a href="<?php echo base_url('feeds') ?>"><i class="fa fa-angle-right"></i>Technology Wall</a></li>
                                    <li><a href="<?php echo base_url('resource_sharing') ?>"><i class="fa fa-angle-right"></i>Resource Sharing</a></li>
                                     <li><a href="<?php echo base_url('technology_transfer') ?>"><i class="fa fa-angle-right"></i>Technology Transfer</a></li>
                                    <li><a href="<?php echo base_url('home/contactUs') ?>"><i class="fa fa-angle-right"></i>Contact Us</a></li>
							   </ul>
                            </div>
                            <!-- footer link -->
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="footer-link support mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Support</h6>
                                </div>
                                <ul>
                                    <li><a href="<?php echo base_url('home/faq') ?>"><i class="fa fa-angle-right"></i>FAQs</a></li>
                                    <li><a href="<?php echo base_url('home/privacyPolicy') ?>"><i class="fa fa-angle-right"></i>Privacy Policies</a></li>
                                    <li><a href="<?php echo base_url('home/termsOfuse') ?>"><i class="fa fa-angle-right"></i>Terms of Use</a></li>
                                    <li><a href="<?php echo base_url('home/faq') ?>"><i class="fa fa-angle-right"></i>Help</a></li>
                                   
                                </ul>
                            </div>
                            <!-- support -->
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-address mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Registered Office</h6>
                                </div>
                                <ul>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                        <div class="cont">
                                            <p><?php echo $socialArr[0]['off_address'] ?></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="cont">
                                            <p><?php echo $socialArr[0]['contact_no'] ?></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="cont">
                                            <p><?php echo $socialArr[0]['email_id'] ?></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- footer address -->
                        </div>
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
                <!--/container -->
            </div>
        </div>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="footer-copyrights">
                            <p>Copyrights &copy; 2020 All Rights Reserved by ARAI. <a href="<?php echo base_url('home/privacyPolicy') ?>">Privacy Policy</a> <a
                                    href="<?php echo base_url('home/termsOfuse') ?>">Terms of
                                    Services</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#home" id="back-to-top" class="btn btn-sm btn-green btn-back-to-top smooth-scrolls hidden-sm hidden-xs"
            title="home" role="button">
            <i class="fa fa-angle-up"></i>
        </a>
    </footer>
    
<?php //} ?>

    <!-- // JavaScript Files -->
    <script src="<?php echo base_url('assets/front/') ?>js/popper/popper.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/isotope.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/counterup.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/aos.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/front/') ?>js/jquery.smartWizard.min.js"></script>

      <script src="<?php echo base_url('assets/front/') ?>js/jQuery-plugin-progressbar.js"></script>


    <?php 
      if($this->router->fetch_class() == 'home' && $this->router->fetch_method() == 'index')
      { ?>
      <script  src="<?php echo base_url('assets/front/') ?>js/home_page_featured_tdf.js"></script>
      <?php } ?>

    <!-- <script type="text/javascript" src="<?php echo base_url("assets/front/") ?>js/bootstrap-tabcollapse.js"></script>



 <script>
if (screen && screen.width < 768) {
    alert('preserve attached java script data!');
    
            $('.nav-tabs').tabCollapse();

        }
        </script>  -->

        
         <script>
  $(".progress-bar2").loading();
        $('input').on('click', function () {
            $(".progress-bar2").loading();
        });

     
</script>

        <script>
    (function($){
        $(window).on("load",function(){
            $(".Skillset, .boderBox, .boderBox65, .chatScrollbar").mCustomScrollbar({
                theme: "rounded-dark",
                scrollInertia: 0,
                mouseWheelPixels: 30,
                autoDraggerLength:false
            });
        });
    })(jQuery);
</script>


    <script>
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    </script>
<script type="text/javascript">      
function open_subscription_modal()
{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('home/open_subscription_modal_ajax'); ?>",
		data: '',
		cache: false,
		success:function(data)
		{
			//alert(data)
			if(data == "error")
			{
				location.reload();
			}
			else
			{
				$("#model_content_outer").html(data);
				$("#SubscriptionModal").modal('show');
			}
		}
	});
}
			
function open_tools_modal()
{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('home/open_tool_connect'); ?>",
		data: '',
		cache: false,
		success:function(data)
		{
			//alert(data)
			if(data == "error")
			{
				location.reload();
			}
			else
			{
				$("#model_tools_outer").html(data);
				$("#toolsModal").modal('show');
			}
		}
	});
}
</script>
<script>
  // News Home Page//

$('#newsHomePage').owlCarousel({
    items: 3,
    autoplay: true,
    smartSpeed: 700,
    loop: true,
    dots: false,
    nav: true,
    navText: ["", ""],
    autoplayHoverPause: true,
    responsive: {
            0: {
                items: 1
            },
            767: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
});
	
	//team_listing
	$('#SearchTeamListingSlider').owlCarousel({
		items: 1,
		autoplay: true,
		smartSpeed: 700,
		loop: false,
		nav: true,
		dots: false,
		navText: ["", ""],
		autoplayHoverPause: true
	});
	
    $('#owner-teamSlider').owlCarousel({ 

		autoplay: true,
		smartSpeed: 700,
		loop: false,
		nav: true,
		dots: false,
		navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
        0: {
            items: 1
        },
        480: {
            items: 1
        },
        768: {
            items: 2
        },
        992: {
            items: 4
        }
    }
	});	
    
    
	$('#teamSlider').owlCarousel({
            items: 4,
            autoplay: false,
            smartSpeed: 700,
            loop: false,
            nav: true,
            dots: false,
            navText: ["", ""],
    responsive: {
            0: {
                items: 1
            },
            767: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 4
            }
        }
        });
				
				$('#CreateTeamSlider').owlCarousel({
				items: 3,
				autoplay: false,
				smartSpeed: 700,
				loop: false,
				nav: true,
				dots: false,
                touchDrag:false,
                mouseDrag  : false,
				navText: ["", ""],
                responsive: {
            0: {
                items: 1
            },
            767: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
			});
			
			$('.teamSearchOwlCarousel').owlCarousel(
	{
			items: 1,
			autoplay: true,
			smartSpeed: 700,
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true
	});
    </script>

	<script>

	

  $("input[type='text']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
   else{
    $(this).parent().find('label').removeClass('floatinglabel');
   }
});




$("input[type='password']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
   else{
    $(this).parent().find('label').removeClass('floatinglabel');
   }
});

$("input[type='email']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
   else{
    $(this).parent().find('label').removeClass('floatinglabel');
   }
});



$("textarea").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
   else{
    $(this).parent().find('label').removeClass('floatinglabel');
   }
});

    $("select").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
   else{
    $(this).parent().find('label').removeClass('floatinglabel');
   }
});

//   $("input[type='password']").change( function() {
//    if ($(this).val() != '') {
//      $(this).parent().find('label').addClass('floatinglabel');
//    }
// });

//   $("input[type='email']").change( function() {
//    if ($(this).val() != '') {
//      $(this).parent().find('label').addClass('floatinglabel');
//    }
// });


function applyFloatingCls()
{
    $('input, textarea').each(function()//select, 
	{
        var val = '';
		var val = $.trim($(this).val());        
		if (val != '') { $(this).parent().find('label').addClass('floatinglabel'); }
		else if (val == '') { $(this).parent().find('label').removeClass('floatinglabel'); } 
	});
}
applyFloatingCls();
</script>

<?php 
if($this->uri->segment(1) == 'profile' && $this->uri->segment(2) == 'individual') 
{   ?>
<script>
function applyFloatingCls()
{
    $('input, select, textarea').each(function()
	{
        var val = '';
		var val = $.trim($(this).val());        
		if (val != '') { $(this).parent().find('label').addClass('floatinglabel'); }
	});
}
applyFloatingCls();
</script>
<?php  } ?>

<script>
 <?php    if($this->uri->segment(1) != 'technology_transfer') 
{   ?>
    $(document).ready(function()
    {          
        $('input[type="file"]').change(function(e)
        {   
   
            var fileName = e.target.files[0].name;
            $(this).parent().find('input').next("span").remove();
            $(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
        });          
    });

    <?php  } ?>

	
	// InActive ACT
	$(".user-class-dt").on("click", function(event){
		//alert("Hello");
		swal(
        {
            title:"Confirm" ,
            text: "Are you sure you want to delete your account from Technovuus?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        {
            if (result.value) 
            {
                var csrf_test_name = $("#csrf_token").val();                      
                $.ajax(
                { 
                    type: "POST", 
                    url: '<?php echo site_url("home/deleteAccount") ?>', 
                    data: {csrf_test_name:csrf_test_name}, 
					contentType: "application/json",
                    dataType: "json",
                    success:function(response) 
                    { 
						var message = response['message'];
                        //$("#csrf_token").val(data.csrf_new_token);                        
                        swal({ title: "Success", text: message, type: "success" });
						window.location = "<?php echo site_url(); ?>";
                    }
                });
            }
        });		
	});
	
     function delete_single_file(tbl_nm, pk_nm, del_id, input_nm, file_path, swal_text)
    { 
        swal(
        {
            title:"Confirm?" ,
            text: "Are you confirm to delete the "+swal_text+"?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        {
            if (result.value) 
            {
                var csrf_test_name = $("#csrf_token").val();
                var data = { 
                        'tbl_nm': encodeURIComponent($.trim(tbl_nm)), 
                        'pk_nm': encodeURIComponent($.trim(pk_nm)), 
                        'del_id': encodeURIComponent($.trim(del_id)), 
                        'input_nm': encodeURIComponent($.trim(input_nm)), 
                        'file_path': encodeURIComponent($.trim(file_path)), 
                        'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
                        };          
                $.ajax(
                { 
                    type: "POST", 
                    url: '<?php echo site_url("delete_single_file") ?>', 
                    data: data, 
                    dataType: 'JSON',
                    success:function(data) 
                    { 
                        $("#csrf_token").val(data.csrf_new_token);
                        $("#"+input_nm+"_outer").remove();
                        
                        swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" });
                    }
                });
            }
        });
    }   
</script>

<script>




function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}    

function delete_cookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}    
</script>

<?php $reminder = ''; 
if ( isset($_SESSION['profile_reminder'] ) && $_SESSION['profile_reminder']!=''  ) {
    $reminder = $_SESSION['profile_reminder'];
}

?>
<?php if($this->session->userdata('user_category')==1){ ?>
<script>
    $(document).ready(function(){  
    $('#login_modal').on('hidden.bs.modal', function () {


        $.get('login/remove_reminder', function() { });

    })
    var login_cookie_value = '<?php echo $reminder ?>';
    var profile_status = '<?php  $this->check_permissions->is_profile_complete_without_redirect(); ?>';
    if (profile_status=='false') {
    if (login_cookie_value!='') {
        $('#login_modal').modal({backdrop: 'static', keyboard: false})  ;
    
    }
    }

});
</script>
<?php } ?>

<script>
    $('.gallerySlider').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: false,
        nav: true,
        dots: false,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }
    
        });
   
        </script>

<script>
    function check_permissions_ajax(module_id){
        var permission;
        $.ajax(
        {
            type:'POST',
            url: '<?php echo site_url("feeds/check_permissions_ajax"); ?>',
            data:{ 'module_id':module_id },
            dataType:"JSON",
            async:false,
            success:function(data)
            {   if(data.success){
                    permission=true;
                }
                else{
                    permission=false;

                }
            }
        });
        return permission;

    }
// $(document).ready(function(){
// $(".social-open-menu").click(function () {
//     $(".social-itens").toggleClass("open");
//     $(".social-itens").toggleClass("hidden");
// });
// });

</script>

<script>
  $(window).on('load', function () {
        setTimeout(function(){ 
            var current_lang = $('html')[0].lang; 
            if (current_lang=='en') {
                $("#eng_li").addClass('lang_active');
            }else if(current_lang=='hi'){
                $("#hi_li").addClass('lang_active');
            }
        }, 1500);
    
  })
</script>

<script type="text/javascript">
    var site_url_js = '<?php echo base_url() ?>';
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
      var event;
      if (document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, true);
        element.dispatchEvent(event);
      } else {
        event = document.createEventObject();
        event.eventType = eventName;
        element.fireEvent('on' + event.eventType, event);
      }
    }

    jQuery('.lang-select').click(function() {
       $.ajax({
        url: site_url_js+"home/unset_lang_cookie",
        type:"POST",
        async:false,
          
        })
      var theLang = jQuery(this).attr('data-lang');
      jQuery('.goog-te-combo').val(theLang);
      window.location = jQuery(this).attr('href');
      location.reload();
    });
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 

</body>

</html>