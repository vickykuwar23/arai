
<!--====================================================
                      FOOTER
======================================================-->
    <footer>
        <div id="footer-s1" class="footer-s1">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-about mt-40">
                                <div class="logo">
                                    <h6>about arai</h6>
                                </div>
                                <p>Automotive Research Association of India (ARAI), established in 1966, is the leading
                                    automotive R&D organization of the country</p>
                                <ul class="mt-20">
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                            <!-- footer about -->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="footer-link mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Quick Links</h6>
                                </div>
                                <ul>
                                    <li><a href="<?php echo base_url() ?>"><i class="fa fa-angle-right"></i>Home</a></li>
                                    <li><a href="<?php echo base_url('home/aboutUs') ?>"><i class="fa fa-angle-right"></i>About us</a></li>
                                    <li><a href="<?php echo base_url('challenge') ?>"><i class="fa fa-angle-right"></i>Open Challenges</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Help</a></li>
                                    <li><a href="<?php echo base_url('registration') ?>"><i class="fa fa-angle-right"></i>Register</a></li>
                                </ul>
                                <ul>
                                    <li><a href="<?php echo base_url('home/featuredChallenges') ?>"><i class="fa fa-angle-right"></i>Featured Challenges</a></li>
                                    <li><a href="<?php echo base_url('challenge') ?>"><i class="fa fa-angle-right"></i>Apply for Challenge</a></li>
                                    <li><a href="<?php echo base_url('challenge/add') ?>"><i class="fa fa-angle-right"></i>Submit Challenge</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Online Hakathons</a></li>
                                    <li><a href="<?php echo base_url('home/contactUs') ?>"><i class="fa fa-angle-right"></i>Contact</a></li>
                                </ul>
                            </div>
                            <!-- footer link -->
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="footer-link support mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Support</h6>
                                </div>
                                <ul>
                                    <li><a href="<?php echo base_url('home/faq') ?>"><i class="fa fa-angle-right"></i>FAQS</a></li>
                                    <li><a href="<?php echo base_url('home/privacyPolicy') ?>"><i class="fa fa-angle-right"></i>Privacy Policy</a></li>
                                    <li><a href="<?php echo base_url('home/termsOfuse') ?>"><i class="fa fa-angle-right"></i>Terms of Use</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Support</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Documentation</a></li>
                                </ul>
                            </div>
                            <!-- support -->
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-address mt-40">
                                <div class="footer-title pb-25">
                                    <h6>Registered Office</h6>
                                </div>
                                <ul>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                        <div class="cont">
                                            <p>Survey No. 102, Vetal Hill, off Paud Road, Kothrud, Pune - 411 038 P. B.
                                                No. 832, Pune - 411 004</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="cont">
                                            <p>+91-020-30231111</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="cont">
                                            <p>info@araiindia.com</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- footer address -->
                        </div>
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
                <!--/container -->
            </div>
        </div>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="footer-copyrights">
                            <p>Copyrights &copy; 2020 All Rights Reserved by ARAI. <a href="<?php echo base_url('home/privacyPolicy') ?>">Privacy Policy</a> <a
                                    href="<?php echo base_url('home/termsOfuse') ?>">Terms of
                                    Services</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#home" id="back-to-top" class="btn btn-sm btn-green btn-back-to-top smooth-scrolls hidden-sm hidden-xs"
            title="home" role="button">
            <i class="fa fa-angle-up"></i>
        </a>
    </footer>


    <!--Global JavaScript -->
         
    <script src="<?php echo base_url('assets/front/') ?>js/popper/popper.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/isotope.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/counterup.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/aos.js"></script>
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url('assets/front/') ?>js/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/front/') ?>js/custom.js"></script>
 

    <script type="text/javascript" src="<?php echo base_url('assets/front/') ?>js/jquery.smartWizard.min.js"></script>
    <script>
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    </script>

    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
    <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->


    <script type="text/javascript">

    $(document).ready(function () {
        /*$.validator.setDefaults({
            
        });*/
        $( "#subscriptionForm" ).validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo base_url('home/validateEmail'); ?>",
                        type: "post"        
                    }
                }
            },
            messages: {
                email: {
                    required: "Please Enter Email Address",
                    remote : "This Email Address is already exist, please try with different email"
                },
            },

            submitHandler: function () {
                var email = $('#email_sub').val();
                $.ajax({
                    url: "<?php echo base_url('home/InsertEmail'); ?>", 
                    type: "POST",             
                    data: { "email" : email },
                    dataType: 'JSON',
                    cache: false,
                    success: function(response) 
                    {
                        swal({
                          title: response.flag,
                          text: response.message,
                          icon: response.flag,
                        }).then(function() {
                            location.reload();
                        });
                    }
                });
                return false;
            }
        }); 
    });  
    </script>
</body>

</html>