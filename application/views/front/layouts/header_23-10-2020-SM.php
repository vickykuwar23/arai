<?php
// Create Object
$encrptopenssl_header =  New Opensslencryptdecrypt();
$setting_header = $this->master_model->getRecords("setting",array('id'=>'1'));
$pageSite_title = $encrptopenssl_header->decrypt($setting_header[0]['site_title']);
?>
<!--
License: Creative Commons Attribution 4.0 Unported
License URL: https://creativecommons.org/licenses/by/4.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $pageSite_title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/front/') ?>img/favicon.ico">

    <!-- Global Stylesheets -->
    <link
        href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap"
        rel="stylesheet">
    <link href="<?php echo base_url('assets/front/') ?>css/bootstrap/newbootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/LineIcons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/line-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/aos.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/add.css">


     <link href="<?php echo base_url('assets/front/') ?>css/smart_wizard.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url('assets/front/') ?>css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/sweetalert2.min.css">
      <link href="<?php echo base_url('assets/front/') ?>css/select2.min.css" rel="stylesheet" />
      
      <script src="<?php echo base_url('assets/front/') ?>js/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.all.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.min.js" type="text/javascript"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
      <script src="<?php echo base_url('assets/front/') ?>js/select2.min.js"></script>

    <style>
  .error, .error p
  {
      color: red !important;
      font-size: 13px !important;
      margin-bottom:0;
    }
    .blank{
        color: red;
    }
    </style>  
    <?php if ($this->session->flashdata('error_pemission') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Warning!','You don\'t have permission to access this page !','warning');
                   }, 200);
               </script>
    <?php } ?>

     <?php if ($this->session->flashdata('error_approval') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Warning!','Your account is not approved yet !','warning');
                   }, 200);
               </script>
    <?php } ?>

    
</head>

<body id="page-top">
    <div id="preloader"></div>
    <!-- // HEADER -->
<?php 
 //$url_segmanet=$this->uri->segment(1);if ( $url_segmanet != 'login' ) { ?>
    <header>

        <!-- Top Navbar  -->
        <div class="top-menubar" id="home">
            <div class="topmenu">
                <div class="container-fluid">
                    <div class="row" data-aos="fade-down">
                        <div class="col-md-8">
                            <ul class="list-inline top-contacts">
                                <li>
                                    <a href="https://www.araiindia.com/"  target="_blank"  class="whtbg"> <img src="<?php echo base_url('assets/front/') ?>img/logo-header.png"></a>
                                </li>
								<li>
                                    <a href="<?php echo base_url('challenge') ?>">Collaborative Technology Solution</a>
                                </li>
                                <!--<li>
                                    <a href="<?php echo base_url('home/build_your_team') ?>">Build Your Team</a>|
                                </li>-->
                               
								<li>
									|
								<a href="<?php echo base_url('home/viewAllExperts') ?>">Expert Connect</a>
                                </li>
								<li>
									| <?php if($this->session->userdata('user_id') ==""): ?>
									<a href="<?php echo base_url('login') ?>">Tool Connect</a>
									<?php else: ?>
									<a href="javascript:void(0);" onClick="open_tools_modal();">Tool Connect</a>
									<?php endif; ?>
								</li>
								<li>
                                        |
                                    <a href="<?php echo base_url('home/aboutByt') ?>">Build Your Team</a>
                                </li>
                                <!--<li>
                                    <a href="<?php echo base_url('challenge') ?>"> List of Open Challenges</a>|
                                </li>
                                <li>
                                    <a href="#"> List of Public Webinars</a>
                                </li>-->
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="list-inline top-data">
                                <!-- <li><a href="#" target="_empty">Skip to main Content</a>|</li>
                                <li><a href="#" target="_empty">A+</a>|</li>
                                <li><a href="#" target="_empty">A</a>|</li>
                                <li><a href="#" target="_empty">A-</a>|</li> -->
                                <?php
                              

                                 if ($this->session->userdata('user_id')!='') {
                                $encrptopenssl_he =  New Opensslencryptdecrypt(); 
                                $user_category=$this->session->userdata('user_category'); 
                                $user_sub_category=$this->session->userdata('user_sub_category'); 


                                $user_id = $this->session->userdata('user_id');

                                $user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
                                // print_r($user_data);
                                
                                $user_arr = array();
                                if(count($user_data)){  
                                                
                                    foreach($user_data as $row_val){        
                                                
                                        $row_val['title'] = $encrptopenssl_he->decrypt($row_val['title']);
                                        $row_val['first_name'] = $encrptopenssl_he->decrypt($row_val['first_name']);
                                        $row_val['middle_name'] = $encrptopenssl_he->decrypt($row_val['middle_name']);
                                        $row_val['last_name'] = $encrptopenssl_he->decrypt($row_val['last_name']);
                                       
                                        $user_arr[] = $row_val;
                                    }
                    
                                }
                                $full_name = $user_arr[0]['title']." ".$user_arr[0]['first_name']." ".$user_arr[0]['last_name'];

                                    ?>


                                  <li><?php echo "<small>Welcome &nbsp;".$full_name."</small>"; ?></li>  

                                  <li><a href="<?php echo base_url('login/logout') ?>" class="log-top btn btn-primary btn-xs">Logout</a></li>  
                                 <li class="dropdown"><a href="#" class="log-top btn btn-success btn-xs dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>  </a>
                               
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">

                                    <?php
                                   

                                    if($user_category==2){
                                      $profile_url=base_url('organization');
                                    } 

                                    // if($user_sub_category==11 || $user_sub_category == 1){
                                    //   $profile_url=base_url('profile');
                                    // } 

                                    if($user_category==1){
                                      $profile_url=base_url('profile');
                                    } 

                                    if( $user_sub_category == 2){
                                      $profile_url=base_url('profile');
                                    } 
                                    ?>
                                    
                                    <?php if ($profile_url!=''): ?>
                                       <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo $profile_url ;?>'">My Profile</button>  
                                    <?php endif ?>
                                   
                                    <?php  if( $user_category == 2){ ?>

                                    <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/add') ?>'">Post a Challenge</button>
                                     <button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/myChallenges') ?>'">My Challenges</button>
                                    <?php } ?>

                                   
									<button class="dropdown-item" type="button" onclick="window.location.href='<?php echo base_url('challenge/applyChallengelist') ?>'">List of Applied Challenges</button>
																		
																		<button class="dropdown-item" onclick="window.location.href='<?php echo base_url('myteams/myCreatedteams') ?>'" type="button">My Teams</button>
																		
                                   <button class="dropdown-item" onclick="window.location.href='<?php echo base_url('profile/changePassword') ?>'" type="button">Change Password</button>
                                  </div>
                                </li>

                                <?php } else{?>

                                <li><a href="<?php echo base_url('login') ?>" class="log-top btn btn-primary btn-xs">Login</a></li>
                                <li><a href="<?php echo base_url('registration') ?>" class="log-top btn btn-success btn-xs" >Register</a></li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light" id="mainNav" data-toggle="affix">
            <div class="container-fluid" data-aos="fade-up">
                <a class="navbar-brand smooth-scroll" href="<?php echo base_url() ?>">
                    <img src="<?php echo base_url('assets/front/') ?>img/logo1.png" alt="logo">
                </a>
                <!-- <a href="https://www.araiindia.com/" target="_blank" class="whtbg"> <img src="<?php echo base_url('assets/front/') ?>img/ARAI-Logo.png"></a> -->
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-reorder"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item <?php if($page_title == "Home"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url() ?>">Home</a></li>
                        <!--<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle smooth-scroll" href="#" id="navbarDropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
                            <div class="dropdown-menu dropdown-cust" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="#">About ARAI</a>
                                <a class="dropdown-item" href="#">Expert Constultants</a>
                                <a class="dropdown-item" href="#">Meet Our Team</a>
                            </div>
                        </li>-->
						
						<li class="nav-item dropdown show <?php if($page_title == "About"){ ?> active <?php } ?>">
                            <a class="nav-link dropdown-toggle smooth-scroll" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">About</a>
                            <div class="dropdown-menu dropdown-cust show" aria-labelledby="navbarDropdownMenuLink" style="display: none;">
                                <a class="dropdown-item" href="<?php echo base_url('home/aboutUs') ?>">TECHNOVUUS</a>
                                <a class="dropdown-item" href="<?php echo base_url('home/cts') ?>">COLLABORATIVE TECH. SOL. </a>
                                <a class="dropdown-item" href="<?php echo base_url('home/expert_connect') ?>">EXPERT CONNECT</a>
								<a class="dropdown-item" href="<?php echo base_url('home/build_your_team') ?>">About BYT</a>
							</div>
                        </li>
						
						<!--<li class="nav-item "><a class="nav-link smooth-scroll" href="<?php echo base_url('home/aboutUs') ?>" >About Us</a></li>-->
                        <li class="nav-item <?php if($page_title == "Blog"){ ?> active <?php } ?>" ><a class="nav-link smooth-scroll" href="<?php echo base_url('blogs') ?>">News & Updates</a></li>
                        <!--<li class="nav-item <?php if($page_title == "Challenge"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url('challenge/add') ?>">Submit Challenge</a></li>-->
                        <li class="nav-item <?php if($page_title == "FAQ"){ ?> active <?php } ?>"><a class="nav-link smooth-scroll" href="<?php echo base_url('home/faq') ?>">Help & FAQ<small>s</small></a></li>
						<!--<li class="nav-item"><a href="#" class="nav-link smooth-scroll Email" data-toggle="modal" data-target="#exampleModal"> Subscription</a></li>-->						
						<li class="nav-item"><a href="javascript:void(0)" class="nav-link smooth-scroll Email" onclick="open_subscription_modal()"> Subscription</a></li>						
						<li class="nav-item"><a class="nav-link smooth-scroll" href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                        <!-- <li>
                            <i class="search fa fa-search search-btn"></i>
                            <div class="search-open">
                                <div class="input-group animated fadeInUp">
                                    <input type="text" class="form-control" placeholder="Search"
                                        aria-describedby="basic-addon2">
                                    <span class="input-group-addon" id="basic-addon2">Go</span>
                                </div>
                            </div>
                        </li> -->

                        <!-- Modal -->
                        <!--<div class="modal fade" id="exampleModal" style="z-indexz:9999999" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Subscription</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                                <form method="post" id="subscriptionForm" name="subscriptionForm" role="form">
                                    <div class="modal-body">
                                        <input type="text" name="email" id="email_sub" class="form-control" placeholder="Please Enter Email Id">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Subsribe</button>
                                    </div>
                                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                                </form>
                            </div>
                          </div>
                        </div>-->

                        <li>
                            <div class="top-menubar-nav menudisplayNone">
                                <div class="topmenu ">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <ul class="list-inline top-contacts">
                                                    <li>
                                                        <i class="fa fa-envelope"></i> Email: <a
                                                            href="mailto:info@araiindia.com">info@araiindia.com</a>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-phone"></i> Hotline: (1) 396 4587 99
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-inline top-data">
                                                    <li><a href="#" target="_empty"><i
                                                                class="fa top-social fa-facebook"></i></a></li>
                                                    <li><a href="#" target="_empty"><i
                                                                class="fa top-social fa-twitter"></i></a></li>
                                                    <li><a href="#" target="_empty"><i
                                                                class="fa top-social fa-google-plus"></i></a></li>
                                                    <li><a href="#" class="log-top" data-toggle="modal"
                                                            data-target="#login-modal">Login</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <?php //} ?>
