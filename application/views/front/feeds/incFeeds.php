<div class="row">
   <div class="col-md-8">
      <?php foreach ($nonfeatured_feeds as $key => $feed) { ?>
      <?php if ($feed['feeds_module_id']==1 && $feed['is_featured']=='N' ){ ?>
      <div class="resourceSharing mt-4">
         <h3 class="mb-4">New Challenge Added</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['banner_img']!=''){ ?>
               <img src="<?php echo base_url('assets/challenge/').$feed['banner_img'] ?>">
               <?php }else{ ?>
               <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?> 
            </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a title="<?php echo ucfirst($feed['challenge_title']); ?> " href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>">
                  <?php if(strlen($feed['challenge_title']) > 35 ){ echo ucfirst(substr($feed['challenge_title'], 0,35) )."..."; } else {echo ucfirst($feed['challenge_title']);} ?>
                  </a>
               </h4>
               <p>Challenge Brief : <?php 
               $read_more_url=base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               if(strlen($feed['challenge_details']) > 250 ){ echo ucfirst(substr($feed['challenge_details'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['challenge_details']);} ?>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                     <li class="d-none"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>

                     <li class="d-none"><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==2 && $feed['is_featured']=='N'){ ?>
      <div class="resourceSharing mt-4">
         <h3 class="mb-4">A New Expert on board</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['profile_picture']!=''){ ?>
               <img src="<?php echo base_url('assets/profile_picture/').$feed['profile_picture'] ?>"  class="img-fluid">
               <?php }else{ ?>
               <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>  
            </div>
            <div class="col-md-9 pl-0">
               <h4><?php $name =  $feed['title']." ".$feed['first_name']." ".$feed['last_name'] ?>
               <a title="<?php echo ucfirst($name); ?>" href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>"><?php if(strlen($name) > 35 ){ echo ucfirst(substr($name, 0,35) )."..."; } else {echo ucfirst($name);} ?></a>
                  
               </h4>
               <?php if ($feed['bio_data']!='') { ?>
               <p><?php 
                $read_more_url=base_url('home/viewProfile/').base64_encode($feed['user_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';
               if(strlen($feed['bio_data']) > 250 ){ echo ucfirst(substr($feed['bio_data'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['bio_data']);} ?>
                  <?php } ?>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                   
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                   
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==3 && $feed['is_featured']==0 ) { 
      $getTotalLikes_res = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $feed['r_id']), 'like_id'); 
      ?>
      <div class="resourceSharing">
         <h3 class="mb-4">New Resource on board</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['banner_img']!=''){ ?>
               <img src="<?php echo base_url('uploads/resource_banner_img/').$feed['banner_img'] ?>"  class="img-fluid">
               <?php }else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?> 
            </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a title="<?php echo ucfirst($feed['resouce_name']); ?>" href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>"><?php if(strlen($feed['resouce_name']) > 35 ){ echo ucfirst(substr($feed['resouce_name'], 0,35) )."..."; } else {echo ucfirst($feed['resouce_name']);} ?></a>
                  
               </h4>
               <p>
                  <?php 
                  $read_more_url=base_url('resource_sharing/details/').base64_encode($feed['r_id']) ;
                  $read_more='<a href='.$read_more_url.' >read more</a>';

                  if(strlen($feed['resource_details']) > 250 ){ echo ucfirst(substr($feed['resource_details'], 0,250) )."...".$read_more; } else {echo ucfirst($feed['resource_details']);} ?>
                  <!-- <a href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>" class="link">Read More</a>  -->
               </p>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                       <li style="cursor:auto" id="res_like_count_outer_<?php echo $feed['r_id']; ?>"><span style="cursor:auto; display:inline-block; text-align: center; background: #FFF; color: #c80032; padding: 3px;">  <?php echo $getTotalLikes_res ?> </span></li>

                     <?php if(in_array($feed['r_id'], $Actiondata['like_res'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                        else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                     <li id="res_like_unlike_btn_outer_<?php echo $feed['r_id']; ?>">
                        <span style="cursor:pointer;" onclick="like_unlike_resource('<?php echo base64_encode($feed['r_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                     </li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo site_url('resource_sharing/details/'.base64_encode($feed['r_id'])); ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                     <!-- <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li> -->
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('resource_sharing/details/').base64_encode($feed['r_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==4 && $feed['is_featured']=='No' ) { ?>
      <div class="resourceSharing">
         <h3 class="mb-4">New Webinar</h3>
         <div class="row resourceSharingListNew">
              <div class="col-md-3">
             <?php if($feed['banner_img']!=''){ ?>
               <img src="<?php echo base_url('assets/webinar/').$feed['banner_img'] ?>"  class="img-fluid">
               <?php }else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?> 
               </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>" title="<?php echo ucfirst($feed['webinar_name']); ?>"><?php if(strlen($feed['webinar_name']) > 35 ){ echo ucfirst(substr($feed['webinar_name'], 0,35) )."..."; } else {echo ucfirst($feed['webinar_name']);} ?></a>
                  
               </h4>

               <p><strong>Date:</strong> <?php echo date('jS F Y', strtotime($feed['webinar_date'])); ?></p>
               <p><strong>Time:</strong> <?php 
               $webinar_start_time        = $feed['webinar_start_time'];
               $webinar_end_time       = $feed['webinar_end_time'];
               $dispTime = "<span style='white-space:nowrap;'>".date('h:i A', strtotime($webinar_start_time))." - ".date('h:i A', strtotime($webinar_end_time))."</span>";
               echo $dispTime; 
               ?></p>

               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                      <li><span style="cursor:pointer;" onclick="share_feed('<?php echo site_url('webinar/viewDetail/'.base64_encode($feed['w_id'])); ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                     </ul>
               </div>
            </div>
            <a href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==5 && $feed['is_featured']==0) { 
       $getTotalLikes = $this->master_model->getRecordCount('arai_knowledge_repository_likes', array('kr_id' => $feed['kr_id']), 'like_id'); 
      ?>
      <div class="resourceSharing ">
         <h3 class="mb-4">Added to Repository</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['kr_banner']!=''){ ?>
               <img src="<?php echo base_url('uploads/kr_banner/').$feed['kr_banner'] ?>"  class="img-fluid">
               <?php }else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>
            </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a title="<?php echo ucfirst($feed['title_of_the_content']); ?>" href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>"><?php if(strlen($feed['title_of_the_content']) > 35 ){ echo ucfirst(substr($feed['title_of_the_content'], 0,35) )."..."; } else {echo ucfirst($feed['title_of_the_content']);} ?></a>
                  
               </h4>
               <p>
                  <strong>Auther Name: </strong>
                  <?php if(strlen($feed['author_name']) > 250 ){ echo ucfirst(substr($feed['author_name'], 0,250) )."..."; } else {echo ucfirst($feed['author_name']);} ?>
               </p>
               <p>
                  <strong>Posted On: </strong>
                  <?php echo date("jS F Y", strtotime($feed['created_on'])); ?>
               </p>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                     <li style="cursor:auto; " id="kr_like_count_outer_<?php echo $feed['kr_id']; ?>"><span style="cursor:auto;  text-align: center; background: #FFF; color: #c80032; "><?php echo $getTotalLikes ?> </span></li>

                     <?php if(in_array($feed['kr_id'], $Actiondata['like_kr'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                        else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                     <li id="kr_like_unlike_btn_outer_<?php echo $feed['kr_id']; ?>">
                        <span style="cursor:pointer;" onclick="like_unlike_kr('<?php echo base64_encode($feed['kr_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                     </li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo site_url('knowledge_repository/details/'.base64_encode($feed['kr_id'])); ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>"   class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==6 && $feed['is_featured']==0 ) { 
       $getTotalLikes_blog = $this->master_model->getRecordCount('arai_blogs_likes', array('blog_id' => $feed['blog_id']), 'like_id');
      ?>
      <div class="resourceSharing">
         <h3 class="mb-4">New Blog</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['blog_banner']!=''){ ?>
               <img src="<?php echo base_url('uploads/blog_banner/').$feed['blog_banner'] ?>"  class="img-fluid">
               <?php } else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>
            </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a title="<?php echo ucfirst($feed['blog_title']);  ?>" href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>"><?php if(strlen($feed['blog_title']) > 35 ){ echo ucfirst(substr($feed['blog_title'], 0,35) )."..."; } else {echo ucfirst($feed['blog_title']);} ?></a>
                  
               </h4>
               <p class="d-none">
                  <?php 
                  $read_more_url=base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ;
                  $read_more='<a href='.$read_more_url.' >read more</a>';

                  $desc=strip_tags(html_entity_decode($feed['blog_description'])); 
                  if(strlen($desc) > 250 ){ echo ucfirst(substr($desc, 0,250) )."...".$read_more; } else {echo ucfirst($desc);}  ?>
               </p>
               <strong>Date: </strong><?php echo date('jS F Y', strtotime($feed['created_on'])); ?>
               <br>
               <strong>Time: </strong><?php echo date('h:i A', strtotime($feed['created_on'])); ?>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                      <li style="cursor:auto; " id="blog_like_count_outer_<?php echo $feed['blog_id']; ?>"><span style="cursor:auto;  text-align: center; background: #FFF; color: #c80032; "><?php echo $getTotalLikes_blog ?> </span></li>
                     <?php if(in_array($feed['blog_id'], $Actiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                        else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                     <li id="blog_like_unlike_btn_outer_<?php echo $feed['blog_id']; ?>">
                        <span style="cursor:pointer;" onclick="like_unlike_blog('<?php echo base64_encode($feed['blog_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                     </li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                     <li><a href="<?php echo site_url('blogs_technology_wall/blogDetails/'.base64_encode($feed['blog_id']).'/1'); ?>"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>
      <?php }if ($feed['feeds_module_id']==7 && $feed['is_featured']==0 ) { 
         $getTotalLikes_feed = $this->master_model->getRecordCount('arai_admin_update_likes', array('feed_id' => $feed['feed_id']), 'like_id');
      ?>
      <div class="resourceSharing">
         <h3 class="mb-4">Technovuus Feed</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['banner']!=''){ ?>
               <img src="<?php echo base_url('uploads/admin_update_banner/').$feed['banner'] ?>"  class="img-fluid">
               <?php } else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>
            </div>
            <div class="col-md-9 pl-0">
               <h4>
              
               
               <a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>" title="<?php echo ucfirst($feed['feed_title']);  ?>"><?php if(strlen($feed['feed_title']) > 35 ){ echo ucfirst(substr($feed['feed_title'], 0,35) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a>
                </h4>
               <p><?php 
               $read_more_url=base_url('feeds/details/').base64_encode($feed['feed_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               $feed_desc=strip_tags(html_entity_decode($feed['feed_description'])); 
                  if(strlen($feed_desc) > 250 ){ echo ucfirst(substr($feed_desc, 0,250) )."...".$read_more; } else {echo ucfirst($feed_desc);} 
                  ?>
               </p>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                    <li style="cursor:auto" id="feed_like_count_outer_<?php echo $feed['feed_id']; ?>"><span style="cursor:auto; display:inline-block; text-align: center; background: #FFF; color: #c80032; padding: 3px;">  <?php echo $getTotalLikes_feed ?> </span></li>

                     <?php if(in_array($feed['feed_id'], $Actiondata['like_feed'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                        else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                     <li id="feed_like_unlike_btn_outer_<?php echo $feed['feed_id']; ?>">
                        <span style="cursor:pointer;" onclick="like_unlike_feed('<?php echo base64_encode($feed['feed_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                     </li>
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo site_url('feeds/details/'.base64_encode($feed['feed_id'])); ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                     <?php if ($feed['comment']==1 ) { ?>
                      <li><a href="<?php echo site_url('feeds/details/'.base64_encode($feed['feed_id']).'/1'); ?>"><i class="fa fa-comments" aria-hidden="true"></i> Comments</a></li>
                    <?php } ?>
                    
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>

     <?php }if ($feed['feeds_module_id']==8 && $feed['is_featured']==0 ) { ?>
      <div class="resourceSharing">
         <h3 class="mb-4">Technology Ready for Transfer</h3>
         <div class="row resourceSharingListNew">
            <div class="col-md-3">
               <?php if($feed['tile_image']!=''){ ?>
               <img src="<?php echo base_url('uploads/technology_transfer/tile_image/').$feed['tile_image'] ?>"  class="img-fluid">
               <?php } else{ ?>
                  <img src="<?php echo base_url('uploads/admin_update_files/banner/').$default_img_url ?>"  class="img-fluid">
               <?php } ?>
            </div>
            <div class="col-md-9 pl-0">
               <h4>
                  <a title="<?php echo ucfirst($feed['technology_title']);  ?>" href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>"><?php if(strlen($feed['technology_title']) > 35 ){ echo ucfirst(substr($feed['technology_title'], 0,35) )."..."; } else {echo ucfirst($feed['technology_title']);} ?></a>
                  
               </h4>
               <p><?php 
               $read_more_url=base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ;
               $read_more='<a href='.$read_more_url.' >read more</a>';

               $tech_desc=strip_tags(html_entity_decode($feed['technology_abstract'])); 
                  if(strlen($tech_desc) > 250 ){ echo ucfirst(substr($tech_desc, 0,250) )."...".$read_more; } else {echo ucfirst($tech_desc);}  ?>
               </p>
               <div class="resourceSharingListNew">
                  <ul class="mb-5">
                   
                     <li><span style="cursor:pointer;" onclick="share_feed('<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>')"><i  class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span></li>
                  </ul>
               </div>
            </div>
            <a href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>"  class="btn btn-primary pull-right">View Details</a>
         </div>
      </div>



      <?php  } ?>
      
      <?php } ?>   
      <?php if($new_start < $all_nonfeatured_feeds_count)
         { ?><br>
      <div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
         <a href="javascript:void(0)" class="click-more" onclick="getFeedDataAjax('<?php echo $new_start; ?>', '<?php echo $limit; ?>', 0,1 )">Show More</a>       
      </div>
      <?php 
         } 
         
         ?>  
     
   </div>
   <?php if ($is_show_more==0 && $feeds_count>0) { ?>
   <div class="col-md-4 mt-4">
      <?php
       if (count($trending_feeds)>0 ) { 
         ?>
      <div class="bs-example45">
         <h3>Trending on TechNovuus</h3>
         <div class="descriptionLinkSection text-justify">
            <ul class="pr-3">
               <?php foreach ($trending_feeds as $key => $feed) { ?>
               <?php if ($feed['feeds_module_id']==1  ){?>
               
               <li><a href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>">Challenge: <?php if(strlen($feed['challenge_details']) > 65 ){ echo ucfirst(substr($feed['challenge_details'], 0,65) )."..."; } else {echo ucfirst($feed['challenge_details']);} ?> </a></li>

               <?php }if ($feed['feeds_module_id']==2 ){ ?>
               
               <li><a href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>">Expert: <?php $name =  $feed['title']." ".$feed['first_name']." ".$feed['last_name'] ?>
                  <?php if(strlen($name) > 65 ){ echo ucfirst(substr($name, 0,65) )."..."; } else {echo ucfirst($name);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==3 ) { ?>
               <li><a href="<?php echo base_url('resource_sharing/details//').base64_encode($feed['r_id']) ?>">Resource: <?php if(strlen($feed['resouce_name']) > 65 ){ echo ucfirst(substr($feed['resouce_name'], 0,65) )."..."; } else {echo ucfirst($feed['resouce_name']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==4 ) { ?>
               <li><a href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>">Webinar: <?php if(strlen($feed['webinar_name']) > 65 ){ echo ucfirst(substr($feed['webinar_name'], 0,65) )."..."; } else {echo ucfirst($feed['webinar_name']);} ?></a></li>
               <?php }if ($feed['feeds_module_id']==5 ) { ?>
               
               <li><a href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>">Knowledge Repository:  <?php if(strlen($feed['title_of_the_content']) > 65 ){ echo ucfirst(substr($feed['title_of_the_content'], 0,65) )."..."; } else {echo ucfirst($feed['title_of_the_content']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==6  ) { ?>
               
               <li><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>">Blog: <?php if(strlen($feed['blog_title']) > 65 ){ echo ucfirst(substr($feed['blog_title'], 0,65) )."..."; } else {echo ucfirst($feed['blog_title']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==7  ) { ?>
              
               <li><a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>">Technovuus Feed: <?php if(strlen($feed['feed_title']) > 65 ){ echo ucfirst(substr($feed['feed_title'], 0,65) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==8 ) { ?>
                 <li><a href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>">Technology Transfers : <?php if(strlen($feed['technology_title']) > 65 ){ echo ucfirst(substr($feed['technology_title'], 0,65) )."..."; } else {echo ucfirst($feed['technology_title']);} ?></a></li>
               <?php } ?>
               <?php } ?>
            </ul>
         </div>
      </div>

      <?php } ?>
      <?php if (count($featured_feeds)>0) {
         $margin='';
         if (count($trending_feeds)>0 ) { 
            $margin='mt-4';
            }
       ?>

      <div class="bs-example45 <?php echo $margin ?>">
         <h3>Featured on TechNovuus</h3>
         <div class="descriptionLinkSection text-justify">
            <ul class="pr-3">
               <?php foreach ($featured_feeds as $key => $feed) { ?>
               <?php if ($feed['feeds_module_id']==1 && $feed['is_featured']=='Y' ){?>
               
               <li><a href="<?php echo base_url('challenge/challengeDetails/').base64_encode($feed['c_id']) ?>">Challenge: <?php if(strlen($feed['challenge_details']) > 65 ){ echo ucfirst(substr($feed['challenge_details'], 0,65) )."..."; } else {echo ucfirst($feed['challenge_details']);} ?> </a></li>

               <?php }if ($feed['feeds_module_id']==2 && $feed['is_featured']=='Y'){ ?>
               
               <li><a href="<?php echo base_url('home/viewProfile/').base64_encode($feed['user_id']) ?>">Expert: <?php $name =  $feed['title']." ".$feed['first_name']." ".$feed['last_name'] ?>
                  <?php if(strlen($name) > 65 ){ echo ucfirst(substr($name, 0,65) )."..."; } else {echo ucfirst($name);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==3 && $feed['is_featured']==1 ) { ?>
               <li><a href="<?php echo base_url('resource_sharing/details//').base64_encode($feed['r_id']) ?>">Resource: <?php if(strlen($feed['resouce_name']) > 65 ){ echo ucfirst(substr($feed['resouce_name'], 0,65) )."..."; } else {echo ucfirst($feed['resouce_name']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==4 && $feed['is_featured']=='Yes' ) { ?>
               <li><a href="<?php echo base_url('webinar/viewDetail/').base64_encode($feed['w_id']) ?>">Webinar: <?php if(strlen($feed['webinar_name']) > 65 ){ echo ucfirst(substr($feed['webinar_name'], 0,65) )."..."; } else {echo ucfirst($feed['webinar_name']);} ?></a></li>
               <?php }if ($feed['feeds_module_id']==5 && $feed['is_featured']==1) { ?>
               
               <li><a href="<?php echo base_url('knowledge_repository/details/').base64_encode($feed['kr_id']) ?>">Knowledge Repository:  <?php if(strlen($feed['title_of_the_content']) > 65 ){ echo ucfirst(substr($feed['title_of_the_content'], 0,65) )."..."; } else {echo ucfirst($feed['title_of_the_content']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==6 && $feed['is_featured']==1 ) { ?>
               
               <li><a href="<?php echo base_url('blogs_technology_wall/blogDetails/').base64_encode($feed['blog_id']) ?>">Blog: <?php if(strlen($feed['blog_title']) > 65 ){ echo ucfirst(substr($feed['blog_title'], 0,65) )."..."; } else {echo ucfirst($feed['blog_title']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==7 && $feed['is_featured']==1 ) { ?>
              
               <li><a href="<?php echo base_url('feeds/details/').base64_encode($feed['feed_id']) ?>">Technovuus Feed: <?php if(strlen($feed['feed_title']) > 65 ){ echo ucfirst(substr($feed['feed_title'], 0,65) )."..."; } else {echo ucfirst($feed['feed_title']);} ?></a></li>

               <?php }if ($feed['feeds_module_id']==8 && $feed['is_featured']==1 ) { ?>
                 <li><a href="<?php echo base_url('technology_transfer/details/').base64_encode($feed['tech_id']) ?>">Technology Transfers : <?php if(strlen($feed['technology_title']) > 65 ){ echo ucfirst(substr($feed['technology_title'], 0,65) )."..."; } else {echo ucfirst($feed['technology_title']);} ?></a></li>
               <?php } ?>
               <?php } ?>
            </ul>
         </div>
      </div>
      <?php } ?>
   </div>
   <?php }   ?>
</div>

  <?php if ($feeds_count ==0) { ?>
      <div class="col-md-12 text-center">
         <p style="font-weight:bold; text-align: center;"><br>No Records Available</p>
      </div>
      <?php } ?> 