<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}

.technical-queryText{display:block; font-size:13px; padding:10px 0; background:#FFF; padding-left:10px;}

#more_details {display: none;}

a.skill-more{background:none; font-size: 14px; float: right;}
a.skill-less{background:none;font-size: 14px; float: right;}


#more_results {display: none;}
a.domain-more{background:none; font-size: 14px; float: right;}
a.domain-less{background:none;font-size: 14px; float: right;}

#more_emp {display: none;}
a.employement-more{background:none; font-size: 14px; float: right;}
a.employement-less{background:none;font-size: 14px; float: right;}

#more_exp {display: none;}
a.exp-more{background:none; font-size: 14px; float: right;}
a.exp-less{background:none;font-size: 14px; float: right;}

#flotingButton1 {
    position: fixed;
    top:45%;
		left:0px; z-index:999;
		transition: all 0.5s ease;
		width: 140px;
		box-shadow: 0 5px 25px 0 rgb(0 0 0 / 20%);
} 

.expertsList a {
	color: #fff;
	background: #e23751;
	padding: 5px 10px;
	border: solid 1px #e23751;
	text-decoration: none;

	text-align: center;

}





#flotingButton1 a{
	position: relative;
	padding: 10px;
	background: #af2338;
	border: 1px solid #af2238;
		color: #fff; font-size:13px;
			display: block;

}
#flotingButton1 a:hover{
	background: #333;
	border: 1px solid #333;
		color: #FFF; text-decoration: none;
		transition: all 0.3s ease;
}

/* #flotingButton:hover {
		left: 0px;
		
} 
a.connect-to-expert{
	position: relative;
	padding: 10px 30px 10px 10px;
}
a.connect-to-expert:hover{
	background: #af2338;
	border: 1px solid #af2238;
		color: #fff;
		transition: all 0.3s ease;
}
a.connect-to-expert::after{
	content: "\f138";
	font-family:'FontAwesome';
	position: absolute;
	top: -1px;
	right: -44px;
	padding: 6px 15px 5px;
	background: #af2338;
	color: #fff;
	font-size: 20px;
} */
 
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="profileinfo" data-aos="fade-down">
  <div class="container">
      <div class="row mt-4">
         <div class="col-md-2">
            <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="" class="img-fluid bor">
         </div>
         <div class="col-md-8">  
            <h3 class="card-title mt-5">Our Experts</h3>
         </div>
      </div>
   </div>
</div>

<div class="filterBox">
	<section class="search-sec">
		<form method="post" id="filterData" name="filterData">
			<div class="container">   
			<div class="row">
				<div class="col-lg-12">
					<div id="mySidenav" class="sidenav">
	`					<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
					<div class="scroll">					
						<ul>
							<li> <strong> Select Skillset </strong> </li>
							<?php $i=0; 
								foreach($skill_set as $skills){
									
										if($i < 5){
											
								?>
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills['id'] ?>" id="skillset" name="skillset[]" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $skills['name'] ?>
								</label>
							</li>
							<?php }
								$i++;
							} ?>
							<span id="more_details">
							<?php $i=0; foreach($skillmore as $skills_add){
								if($i >= 5){
							?>
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $skills_add['id'] ?>" id="skillset" name="skillset[]" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $skills_add['name'] ?>
								</label>
							</li>
								<?php } $i++; } ?>
							</span>
							<?php if(count($skillmore) > 5): ?>
							<a href="javascript:void(0);" class="skill-more" id="skill-readmore">Show More >> </a>
							<a href="javascript:void(0);" class="skill-less" style="display:none;"  id="skill-readless">Show Less >></a>
							<?php endif; ?>
						</ul> 
						
						<ul>
							<li> <strong> Select Domain Expertise </strong> </li>
							<?php $d=0;  foreach($domain_list as $domain_val){
									if($d < 5){
							?>
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $domain_val['id'] ?>" id="domain_exp" name="domain_exp[]" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $domain_val['domain_name'] ?>
								</label>
							</li>
							<?php } $d++; } ?>
							
							<span id="more_results">
							<?php $d=0; foreach($domain_more as $domain_add){
								if($d >= 5){
							?>
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $domain_add['id'] ?>" id="skillset" name="skillset[]" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $domain_add['domain_name'] ?>
								</label>
							</li>
								<?php } $d++; } ?>
							</span>
							<?php if(count($domain_more) > 5): ?>
							<a href="javascript:void(0);" class="domain-more" id="domain-readmore">Show More >> </a>
							<a href="javascript:void(0);" class="domain-less" style="display:none;"  id="domain-readless">Show Less >></a>
							<?php endif; ?>
						</ul> 
						
						<ul>
							<li> <strong> Employment Status </strong> </li>
							<?php $e=0; foreach($employement_status as $emp_status){
								if($e < 5){
							?>	
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $emp_status['name'] ?>" id="emp_status" name="emp_status" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $emp_status['name'] ?>
								</label>
							</li>
							<?php } $e++; } ?>						
							
							<span id="more_emp">
							<?php $e=0; foreach($employement_more as $employeement_add){
								if($e >= 5){
							?>
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $employeement_add['name'] ?>" id="skillset" name="skillset[]" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $employeement_add['name'] ?>
								</label>
							</li>
								<?php } $e++; } ?>
							</span>
							
							<?php if(count($employement_more) > 5): ?>
							<a href="javascript:void(0);" class="employement-more" id="skill-readmore">Show More >> </a>
							<a href="javascript:void(0);" class="employement-less" style="display:none;"  id="employement-readless">Show Less >></a>
							<?php endif; ?>
						</ul> 
						<ul>
							<li> <strong> Year Of Experience </strong> </li>
							<?php $y=0; foreach($year_of_exp as $exp_no){ 
									if($y < 5){
							?>	
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $exp_no['year_exp'] ?>" id="year_of_exp" name="year_of_exp" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $exp_no['year_exp']."+" ?>
								</label>
							</li>
							<?php } $y++;  } ?>
							
							<span id="more_exp">
							<?php $y=0; 
							foreach($year_of_exp_more as $exp_sno){ 
									if($y >= 5){
							?>	
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $exp_sno['year_exp'] ?>" id="year_of_exp" name="year_of_exp" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $exp_sno['year_exp']."+" ?>
								</label>
							</li>
							<?php } $y++;  } ?>							
							</span>
							
							<?php if(count($year_of_exp_more) > 5): ?>
							<a href="javascript:void(0);" class="exp-more" id="exp-readmore">Show More >> </a>
							<a href="javascript:void(0);" class="exp-less" style="display:none;"  id="exp-readless">Show Less >></a>
							<?php endif; ?>
							
						</ul> 
						<ul>
							<li> <strong> No. Of Papers Publications </strong> </li>
							<?php foreach($no_of_publication as $publication_no){ ?>	
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $publication_no['paper_no'] ?>" id="no_of_pub" name="no_of_pub" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $publication_no['paper_no']."+" ?>
								</label>
							</li>
							<?php } ?>
						</ul> 
						<ul>
							<li> <strong> No. Of Patents</strong> </li>
							<?php foreach($no_of_patents as $patent_no){ ?>	
							<li>
								<input class="form-check-input search-value" type="checkbox" value="<?php echo $patent_no['patent_no'] ?>" id="no_of_patents" name="no_of_patents" >
								<label class="form-check-label" for="defaultCheck1">
									<?php echo $patent_no['patent_no']."+" ?>
								</label>
							</li>
							<?php } ?>
						</ul> 
					</div>
				</div>
					<div class="row d-flex justify-content-center search_Box">
                    
					<div class="col-md-6">
				
					
							<input type="text" class="form-control search-slt" name="search_txt" id="search_txt"  value="" placeholder="Enter Search Keyword" />	
                            <button type="button" class="btn btn-primary searchButton" ><i class="fa fa-search" aria-hidden="true"></i></button>

							
					</div>
                    <div class="clearAll"><a class="btn btn-primary clearAll" id="reset-val">Clear All</a>
                    </div>
                     <div class="filter"><button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
                    </div>
				
					<div class="col-md-12"></div>
					   
					</div>
				</div>
			</div>
		</div>
		</form>
	</section>
</div>
<div id="flotingButton1">
				<span class="technical-queryText">Have any </br>technical query ?</span>
				<a href="javascript:void(0);" class="connect-to-expert" onclick="open_expert_modal()">Connect with Expert</a>
			 </div>


 <?php if(count($all_featured_experts) > 0) { ?>
<section class="expertsList" data-aos="fade-up">
      <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
     <div class="container">
         <div class="row title-bar">
			<div class="col-md-12 mb-4">
				
			 </div>
             <div class="col-md-12 text-center mb-4">
             	<div class="mb-3">
             	<span id="expert_count"><?php echo count($count_featured); ?> 	</span>Experts
             
             	</div>
                 <h2>All Experts</h2>

             </div>
          </div>
             <div id="expertsInfox" class="owl-carouselx owl-themex">
            <div class="row custom_load_more_append">
            
                 <?php foreach ($all_featured_experts as $key => $user) {
						
						$expert_encrypt =  New Opensslencryptdecrypt();
						$expertise_val = $user['domain_area_of_expertise'];					
						// Get Details
						$exp_exploades = explode(",",$expertise_val);
				 ?>
                     <div class="col-md-4 p3">
                        <div class="expertsListBox allExperts">
                           <?php if($user['profile_picture']) { ?>
                               <img src="<?php echo base_url('assets/profile_picture/'.$user['profile_picture'].'') ?>" alt="experts">
                           <?php } else { ?>
                               <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="experts">
                            <?php } ?>
                           <div class="experts">
                               <span class="read"><a href="<?php echo base_url(); ?>home/viewProfile/<?php echo base64_encode($user['user_id']) ?>">View Profile</a></span>
                              
								 <h3><?php echo $user['title'].' '.$user['first_name'].' '.$user['last_name'] ?></h3>	
								<p class="c_name"><?php echo $user['company_name'] ?></p>
							<p class="d_name"><?php  print_r($user['designation'][0]);  ?></p>
                           </div>
						   <!-- <div class="cardd-footer">
								<div>
								<?php if($exp_exploades!=""){
									foreach($exp_exploades as $explist){ 
									$expDetailsget 	= $this->master_model->getRecords('domain_master',array('id'=>$explist));
									$expertiseName  = @$expert_encrypt->decrypt($expDetailsget[0]['domain_name']);
									?>
									<button class="btn btn-general btn-green-fresh" role="button"><?php echo $expertiseName; ?></button>	
								<?php } } ?>
								</div>
							</div> -->
                           <div class="expertsBox">
                               <ul>
                                   <?php if($user['years_of_experience']) { ?>
                                       <li><span><?php echo $user['years_of_experience'] ?></span>Years of Experience</li>
                                   <?php } else { ?>
                                       <li><span>0</span>Years of Experience</li>
                                   <?php } ?>                                  
                                   <?php if($user['no_of_paper_publication']) { ?>
                                       <li><span><?php echo $user['no_of_paper_publication'] ?></span> Academic Publications</li>
                                   <?php } else { ?>
                                       <li><span>0</span> Academic Publications</li>
                                   <?php } ?>
                               </ul>
                           </div>
                        </div>
                     </div>
                 <?php } ?>
				 <?php if(count($count_featured) > 6){ ?>
					<div class="col-md-12 text-center  show_more_main" id="show_more_main<?php echo $user['user_id']; ?>">
						<span id="<?php echo $user['user_id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts">Show more</span>
						<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
					</div> 
				<?php } ?>
             </div> 
				
			   <!--<div class="show_more_main" id="show_more_main<?php echo $user['user_id']; ?>">
				   <span id="<?php echo $user['user_id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts" onclick="show_more_ajax('<?php echo $user['user_id']; ?>')">Show more</span>
				   <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>
			 </div>-->
     </div>
</section>
<?php }  ?>


<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <!-- <h4 class="modal-title">Modal Heading</h4> -->
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
         <div class="row">
<div class="col-md-12">

<p> Hello User,<br><br> 
If you are a domain expert, TechNovuus welcomes you and looks forward to empanelling you on the platform.<br>
Please send us an email with a brief profile on <a href="mailto:info@technovuus.araiindia.com">info@technovuus.araiindia.com</a> and we will contact you soon.<br><br>
Thanks,<br>
TechNovuus Team.<br><br></p>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="connectExpertModal" tabindex="-1" role="dialog" aria-labelledby="connectExpertModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="model_expert_modal">
	</div>
</div>
<script type="text/javascript">      
function open_expert_modal()
{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('home/open_connect_expert'); ?>",
		data: '',
		cache: false,
		success:function(data)
		{
			//alert(data)
			if(data == "error")
			{
				location.reload();
			}
			else
			{
				$("#model_expert_modal").html(data);
				$("#connectExpertModal").modal('show');
			}
		}
	});
}
</script>
<script type="text/javascript">
   
   $(document).ready(function () { 
   
		//$('#preloader-loader').css('display', 'block');		
		
		$(document).on('click','.skill-more',function(){
			$('#more_details').css('display', 'block');
			$('.skill-less').css('display', 'block');
			$('.skill-more').css('display', 'none');
		});

		$(document).on('click','.skill-less',function(){
			$('#more_details').css('display', 'none');
			$('.skill-less').css('display', 'none');
			$('.skill-more').css('display', 'block');
		});	
		
		$(document).on('click','.domain-more',function(){
			$('#more_results').css('display', 'block');
			$('.domain-less').css('display', 'block');
			$('.domain-more').css('display', 'none');
		});

		$(document).on('click','.domain-less',function(){
			$('#more_results').css('display', 'none');
			$('.domain-less').css('display', 'none');
			$('.domain-more').css('display', 'block');
		});
		
		$(document).on('click','.employement-more',function(){
			$('#more_emp').css('display', 'block');
			$('.employement-less').css('display', 'block');
			$('.employement-more').css('display', 'none');
		});

		$(document).on('click','.employement-less',function(){
			$('#more_emp').css('display', 'none');
			$('.employement-less').css('display', 'none');
			$('.employement-more').css('display', 'block');
		});
		
		 
		
		$(document).on('click','.exp-more',function(){
			$('#more_exp').css('display', 'block');
			$('.exp-less').css('display', 'block');
			$('.exp-more').css('display', 'none');
		});

		$(document).on('click','.exp-less',function(){
			$('#more_exp').css('display', 'none');
			$('.exp-less').css('display', 'none');
			$('.exp-more').css('display', 'block');
		});	
		
		// Load More Code 
		$(document).on('click','.show_more',function(){
			// $(".token").remove();
			var base_url = '<?php echo base_url(); ?>'; 
			var ID = $(this).attr('id');
			var length = $('.p3').length;
			var cs_t = 	$('.token').val();
			$('.show_more').hide();
			$('.loding').show();
			$.ajax({
				type:'POST',
				url: base_url+'home/getmore',
				data:'id='+length+'&csrf_test_name='+cs_t,
				dataType:"html",
				async:false,
				success:function(response){ 
					var output = JSON.parse(response);
					if(output.html == ""){
						$(".show_more_main").remove();
					}	
					$(".token").val(output.token);								
					$('#show_more_main'+ID).remove();				
					$('.custom_load_more_append').append(output.html); 						

				},
				 error: function (jqXHR, exception) { 
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		// Filter Apply		
		$(document).on('click','.searchButton',function(){
		
			$('#preloader-loader').css('display', 'block');	
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html); 
					$('#expert_count').html(output.count_of_experts); 
					$('#preloader-loader').css('display', 'none');	

				},
				 error: function (jqXHR, exception) { 
				  $('#preloader-loader').css('display', 'none');
					var msg = '';
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		
		$(document).on('click','.search-value',function(){
			$('#preloader-loader').css('display', 'block');
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html);
					$('#expert_count').html(output.count_of_experts);
					$('#preloader-loader').css('display', 'none');	

				},
				 error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		
		$(document).on('keydown','.search-slt',function(){
			$('#preloader-loader').css('display', 'block');
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			var skillset 		= $('#skillset').val();
			var domain_exp 		= $('#domain_exp').val();
			var emp_status 		= $('#emp_status').val();
			var year_of_exp 	= $('#year_of_exp').val();
			var no_of_pub 		= $('#no_of_pub').val();
			var no_of_patents 	= $('#no_of_patents').val();
			var keyword 		= $('#search_txt').val();
			//console.log(keyword);
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html);
					$('#expert_count').html(output.count_of_experts);
					$('#preloader-loader').css('display', 'none');	

				},
				 error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});
		
		$(document).on('click','a#reset-val',function(){
			$('#preloader-loader').css('display', 'block');
			//alert();
			$("#filterData").trigger("reset");
			var cs_t 			=  $('.token').val();
			var base_url 		= '<?php echo base_url(); ?>'; 
			$.ajax({
				type:'POST',
				url: base_url+'home/expert_filter',
				data: $('#filterData').serialize(),
				dataType:"text",
				//async:false,
				success:function(response){ 
					//console.log(response);
					
					var output = JSON.parse(response);					
					$(".token").val(output.token);
					$('.custom_load_more_append').html(output.html); 
					$('#expert_count').html(output.count_of_experts);
					$('#preloader-loader').css('display', 'none');	

				},
				 error: function (jqXHR, exception) { 
					var msg = '';
					$('#preloader-loader').css('display', 'none');
					if (jqXHR.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (jqXHR.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (jqXHR.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'parsererror') {
						msg = 'Requested JSON parse failed.';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + jqXHR.responseText;
					}
					//console.log(msg);
					//$('#post').html(msg);
				},
			});
		});

		$('#myModal').modal('show');
	});
   $('.select2').select2({});

function openNav() {
	
 var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
		isMobile = true;
		document.getElementById("mySidenav").style.width = "100%";
	} else {
		document.getElementById("mySidenav").style.width = "25%";
	}
 
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";

}

</script>
 <script>
		 // ====================================================
// BACK TO TOP
// ====================================================
// (function($) {

//     $(window).scroll(function() {

//         if ($(this).scrollTop() < 61) {
          
//             $("nav").removeClass("vesco-top-nav");
//             $("#flotingButton").fadeOut();

//         } else {
           
//             $("nav").addClass("vesco-top-nav");
//             $("#flotingButton").fadeIn();
//         }
//     });
// })(jQuery); 
// 		 </script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>          