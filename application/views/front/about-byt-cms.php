<?php 
//echo "<pre>";print_r($about_byt);
$banner_img = base_url('uploads/byt/'.$about_byt[0]['banner_img']);
?>
<style>
.build-your-team2{ background: url(<?php echo $banner_img ?>) top center no-repeat; background-size: 100% 100%; width: 100%; padding: 15% 0 5% 0; position: relative; overflow: hidden;}
.build-your-team2::after{content:"";position:absolute; top:0; left:0; width:100%; background:rgba(0,0,0,.5); height:100vh;z-index:9}
.build-your-team2 blockquote p{font-size:38px; margin:95px 0 0 0; padding:0; color:#fff; text-align:right; position:relative; z-index:999}
.build-your-team2 blockquote footer{ background: none;} 
.build-your-team2 .blockquote-footer { color:#fff; text-align:right;  position:relative; z-index:999 }
.colorSection{ background: #333; padding: 25px 0; }
.colorSection h3{ color: #FFF;}
.colorSection p{ color: #FFF; margin: 0; display: contents;}
.exploreChallengeTeams a{ text-align: center; color: #FFF; margin:15px 0 0 0; display: block; background:#91d250; border:solid 1px #91d250; padding: 8px 25px;}
.exploreChallengeTeams a:hover{ text-align: center; color: #91d250; border:solid 1px #91d250; text-decoration: none; background: #FFF; padding: 8px 25px;}

.colorSection2{ padding: 25px 0; }
.colorSection2 h3{ color: #333; font-size:24px !important}
.colorSection2 p{ color: #333; margin: 0; display: contents;}
</style>
<div class="build-your-team2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<blockquote class="blockquote text-center">
					<p><?php echo $about_byt[0]['banner_quote'] ?></p>
					<!--<footer class="blockquote-footer">Henry Ford</footer>-->
				  </blockquote>
				</h1>
			</div>
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
		<div class="col-md-12 mt-4 mb-4">
				<!--<h3>This is Content (CMS Backend)</h3>-->
				<p><?php echo $about_byt[0]['byt_content'] ?></p>
		</div>
    </div>
</div>
<section class="bg-parallax thought-bg mt-4 bt-4">
    <div class="">
        <div id="thought-desc" class="owl-carousel owl-theme">
            <?php foreach ($how_it_works as $key => $howitworks) { ?>
                <div class="hw-item how-item-1" style="background-image: url('<?php echo base_url(); ?>assets/background_image/<?php echo $how_it_works[$key]['background_image'] ?>');">
                    <div class="container" data-aos="fade-up">
                        <div class="row title-bar">
                            <div class="col-md-12">
                                <h2>How It Works</h2>
                                <div class="heading-border"></div>
                                <h3 class="sub-title"><?php echo $how_it_works[$key]['subject_title']; ?></h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 col-lg-4 prpl5">
                                <div class="icon_box_hiw">
                                    <div class="icon">
                                        <div class="list_tag float-right">
                                            <p>1</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['first_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['first_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['first_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 prpl5 mt20-xxsd">
                                <div class="icon_box_hiw">
                                    <div class="icon middle">
                                        <div class="list_tag float-right">
                                            <p>2</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['second_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['second_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['second_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 prpl5 mt20-xxsd">
                                <div class="icon_box_hiw">
                                    <div class="icon">
                                        <div class="list_tag float-right">
                                            <p>3</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['third_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['third_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['third_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>


<div class="colorSection2">
	<div class="container">
		<div class="row">
				<div class="col-md-12 mt-4 mb-4">
					  
					   <div class="row">
						   <div class="col-md-3 d-flex align-items-center">
                            <a href="<?php echo base_url('challenge'); ?>">
                           <h3>Solve<br/> TechNovuus <br/>Challenges</h3>
                           </a>
                           </div>
						   <div class="col-md-7 d-flex align-items-center">
							<p class="text-justify"> <?php echo $about_byt[0]['first_content'] ?></p>
						   </div>
						   <div class="col-md-2 align-items-center">
							   <div class="exploreChallengeTeams">
                               <img src="<?php echo base_url('assets/Picture1.png'); ?>" alt="icon" class="img-fluid">
								   <a href="<?php echo base_url('challenge'); ?>">Explore</a>
							   </div>
						   </div>

					   </div>
				</div>
		</div>
	</div>
</div>

<div class="colorSection">
    <div class="container">
        <div class="row">
                <div class="col-md-12 mt-4 mb-4">
                      
                       <div class="row">
                         
                        <div class="col-md-3 d-flex align-items-center">
                        <a href="<?php echo base_url('tasks/taskSearch'); ?>">
                        <h3>Build your<br/>own Team</h3>
                        </a>

                        </div>
                           <div class="col-md-7 d-flex align-items-center">
                            <p class="text-justify"><?php echo $about_byt[0]['second_content'] ?></p>
                           </div>
                           <div class="col-md-2 d-flex align-items-center">
                           
                           <div class="exploreChallengeTeams">
                           <img src="<?php echo base_url('assets/Picture1.png'); ?>" alt="icon" class="img-fluid">
                                <a href="<?php echo base_url('tasks/taskSearch'); ?>" class="">Explore</a>
                            </div>
                           </div>
                       </div>
                </div>
        </div>
    </div>
</div>
<!-- <div class="colorSection">
	<div class="container">
		<div class="row">
				<div class="col-md-12 mt-4 mb-4">
					  
					   <div class="row">
						   <div class="col-md-2 d-flex align-items-center"><img src="<?php echo base_url('assets/Picture1.png'); ?>" alt="icon" class="img-fluid"></div>
						   <div class="col-md-7">
							<p class="text-justify"><?php echo $about_byt[0]['third_content'] ?></p>
						   </div>
						   <div class="col-md-3 d-flex align-items-center">
							   <div class="exploreChallengeTeams">
								   <a href="javascript:void(0);" class="apply-popup">Search Suitable Team</a>
							   </div>
						   </div>
	
					   </div>
				</div>
		</div>
	</div>
</div> -->
<!--======== This Css added on custom css =====-->
<style>
#client-logo25.owl-carousel .owl-dot,
#client-logo25.owl-carousel .owl-nav .owl-next,
#client-logo25.owl-carousel .owl-nav .owl-prev {
	font-family: fontAwesome
}

#client-logo25.owl-carousel .owl-nav .owl-prev:before {
	content: "\f177"
}

#client-logo25.owl-carousel .owl-nav .owl-next:after {
	content: "\f178"
}
</style>
<?php
if (count($consortium_list) > 0) {
?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Technology Platform Partners</h2>
                    <div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
                <?php foreach ($consortium_list as $consrotium) : ?>
                    <div class="col-sm-12"><a href="<?php echo $consrotium['img_link'] ?>" target="_blank"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
                <?php endforeach; ?>
			</div>
			</div>
        </div>
    </section>
<?php } ?>

<section class="ministrylogo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center title-bar">
                <h4>Under Aegis of</h4>
                <div class="heading-border"></div>
                <a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('front/layouts/site_popups'); ?>
<script>
$(document).on('click','.apply-popup',function(){
		
		swal({
				title:"",
				text: "This facility is coming soon - Stay Tuned",
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK!',
				html:''
		});
		
	});
</script>


