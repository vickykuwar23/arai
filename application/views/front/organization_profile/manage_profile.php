 <?php if ($this->session->flashdata('error_profile_competion') != "" ) 
        {  ?>
           <script type="text/javascript">
             setTimeout(function () {
               swal( 'Warning!','Please complete your profile.','warning');
               }, 200);
           </script>
<?php } ?>
<style>
.details { color:#2196f3; } 



.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 30px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 22px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#preloader-loader {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
z-index: 9999;
overflow: hidden;
background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
content: "";
position: fixed;
top: calc(50% - 30px);
left: calc(50% - 30px);
border: 6px solid #f2f2f2;
border-top: 6px solid #c80032;
border-radius: 50%;
width: 60px;
height: 60px;
-webkit-animation: animate-preloader 1s linear infinite;
animation: animate-preloader 1s linear infinite;
}

.guideline-tooltip{
  position: relative;
  display: inline-block;
  margin-left: 10px;
}
.guideline-tooltip:hover p {
  opacity: 1;
  -webkit-transform: translate(-35%, 0);
          transform: translate(-35%, 0);
    margin-top: 10px;
  visibility: visible;
}
.guideline-tooltip p {
    position: absolute;
    left: 50%;
    top: 100%;
    opacity: 0;
    padding: 1em;
    background-color: #e7f0ff;
    font-size: 14px;
    line-height: 1.6;
    text-align: left;
    white-space: nowrap;
    -webkit-transform: translate(-35%, 1em);
    transform: translate(-35%, 1em);
    -webkit-transition: all 0.15s ease-in-out;
    transition: all 0.15s ease-in-out;
    color: #000;
  z-index: 99;
    font-weight: 400;
  visibility: hidden;
}
.guideline-tooltip p::before {
    content: '';
    position: absolute;
    top: -16px;
    left: 33%;
    width: 0;
    height: 0;
    border: 0.6em solid transparent;
    border-top-color: #e7f0ff;
    transform: rotate(180deg);
}
</style>

<div id="preloader-loader" style="display:none;"></div>
<div class="profileinfo" data-aos="fade-down">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<?php 
				if(!empty($profile_info) && $profile_info['org_logo'] != "") 
				{ ?>
					<img src="<?php echo base_url('uploads/organization_profile/').$profile_info['org_logo']; ?>" class="img-fluid bor">
			<?php	} else { ?> <img src="<?php echo base_url('assets/profile_picture/') ?>user_dummy.png" alt="" class="img-fluid bor"> <?php } ?>
			</div>
			<div class="col-md-6">
				<h3 class="mt-4"><?php echo $personal_info['institution_full_name']; ?></h3>
				<ul>
					<li><a href="javascript:void(0)" style="cursor: auto;"><i class="fa fa-paper-plane"></i> <?php echo $personal_info['email'] ?></a></li>
					<li><i class="fa fa-user"></i> <?php echo $personal_info['mobile'] ?></li>
				</ul>
				<div class="form-group upload-btn-wrapper2">
					<a class="btn btn-upload2" href="<?php echo site_url('organization/personalInfo'); ?>"><i class="fa fa-pencil"></i> Edit Info</a>
					<a class="btn btn-upload2" href="<?php echo site_url('organization/profile'); ?>"><i class="fa fa-pencil"></i> Edit Company Profile</a>
				</div>
				
				<!-- <div class="progress d-nonex" style="height:30px;max-width:400px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $profile_complete_per; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $profile_complete_per; ?>%; height:30px">Your profile is <?php echo $profile_complete_per; ?>% complete</div>
				</div> -->

				<div class="progress" style="max-width:400px;">
                      <?php 
                      if ($profile_complete_per<=50) {
                        $class='bg-danger';
                      }elseif ($profile_complete_per>50 && $profile_complete_per<=79 ) {
                        $class='bg-warning';
                      }else{
                        $class='bg-success';
                      }
                      ?>
					<div class="progress-bar <?php echo $class ?>" role="progressbar" aria-valuenow="<?php echo $profile_complete_per; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $profile_complete_per; ?>%; ">Your profile is <?php echo $profile_complete_per; ?>% complete</div>
					</div> 
			</div>
			
			     <div class="col-md-4">
                  <label for="" style="color: #fff"> Discoverable </label>
                 <div class="guideline-tooltip"><i style="color: #fff" class="fa fa-question-circle-o fa-lg"></i>
                 <p>Let registered members discover you on Technovuus
                 </p>
                 </div>
                  <label class="switch">
                    <input type="checkbox" id="discoverable" <?php if($personal_info['discoverable']=='yes'){ echo 'checked';} ?>> 
                    <span class="slider round"></span>
                  </label>
                 
                 
                 
                      </br>
                  <label for="" style="color: #fff"> Contactable  </label>
                  <div class="guideline-tooltip"><i style="color: #fff" class="fa fa-question-circle-o fa-lg"></i>
                 <p>Let registered members connect with you via Chat on Technovuus
                 </p>
                 </div>
                  <label class="switch">
                    <input type="checkbox" id="contactable" <?php if($personal_info['contactable']=='yes'){ echo 'checked';} ?> > 
                    <span class="slider round"></span>
                  </label>
                
                  
              </div>
				 
			<?php /* <div class="col-md-4">
				<div class="countBox">
					<h4>02</h4>
					<span>Challenges</span>
				</div>
				<div class="countBox">
					<h4>50</h4>
					<span>Points</span>
				</div>
			</div> */ ?>
		</div>
	</div>
</div>

<section id="registration-form" class="inner-page" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div id="content" class="content content-full-width">
            <div class="profile-content">
							<div class="tab-content p-0">
								<div class="tab-pane fade in active show" id="profile-about">
									<div class="table-responsive col-9">
										<table class="table table-profile">
											<thead>
												<tr>
													<th colspan="2">
														<h4>Personal Info<small></small></h4>
													</th>
												</tr>
											</thead>
											
											<tbody>
												<tr class="highlight">
													<td class="field">Representative Name</td>
													<td class="details"> <?php echo $personal_info['spoc_name']; ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Gender</td>
													<td class="details"> <?php echo $personal_info['gender']; ?></td>
												</tr>																										 
												
												<tr>
													<td class="field">Representative Mobile</td>
													<td class="details"><?php echo $personal_info['iso']." ".$personal_info['phonecode']." - ".$personal_info['mobile'] ?></td>
												</tr>
												<tr>
													<td class="field">Representative Email</td>
													<td class="details"><?php echo $personal_info['email'] ?></td>
												</tr>
												<tr>
													<td class="field">User Category</td>
													<td class="details"><?php echo $personal_info['user_category'] ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Organization Type</td>
													<td class="details">
														
														<?php 
															if($personal_info['other_subcat_type'] != "") { echo $personal_info['other_subcat_type']; 
															}else{
													 			echo $personal_info['sub_catname'];

															}

															 ?>
													
													</td>
												</tr>

										


												<?php /*<tr class="highlight">
													<td class="field">Institution Type</td>
													<td class="details"><?php echo $personal_info['institution_name'] ?></td>
												</tr> */ ?>
												<tr class="highlight">
													<td class="field">Name of Organization</td>
													<td class="details"><?php echo $personal_info['institution_full_name'] ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Domain/ Industry & Sector</td>
													<td class="details">
														<?php echo ucfirst(str_replace('other, ','',strtolower($personal_info['domain_name']))); 
															if($personal_info['other_domain_industry'] != "") { echo ", ".$personal_info['other_domain_industry']; } ?>
													</td>
												</tr>
												<tr class="highlight">
													<td class="field">Public/Private status</td>
													<td class="details">
														<?php echo ucfirst(str_replace('other','',strtolower($personal_info['public_status_name'])));

															if($personal_info['other_public_prvt'] != "") { 
																echo $personal_info['other_public_prvt']; 
															} 
														 ?>
													</td>
												</tr>
											</tbody>
											
											<thead>
												<tr>
													<th colspan="2">
														<h4>Profile Details<small></small></h4>
													</th>
												</tr>
											</thead>
											<tbody>
												<tr class="highlight">
													<td class="field">Organization Sector </td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['org_sector_name'] != "") { echo ucfirst(str_replace(', other','',strtolower($profile_info['org_sector_name']))); if($profile_info['org_sector_other'] != "") { echo ", ".$profile_info['org_sector_other']; } } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Company Overview  </td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['overview'] != "") { echo $profile_info['overview']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Flat/House No/Building/Apt/Company</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr1'] != "") { echo $profile_info['spoc_bill_addr1']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Area/Colony/Street/Village</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr2'] != "") { echo $profile_info['spoc_bill_addr2']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Town/City</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_city'] != "") { echo $profile_info['spoc_bill_addr_city']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">State</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_state'] != "") { echo $profile_info['spoc_bill_addr_state']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Country</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_country'] != "") { echo $profile_info['spoc_bill_addr_country']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Pincode</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_pin'] != "") { echo $profile_info['spoc_bill_addr_pin']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Secondary Number1</td>
													<td class="details">
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code1'] != "" && $profile_info['spoc_sec_num1'] != "") 
															{ 
																echo $profile_info['spoc_sec_num_country_code1']." - ".$profile_info['spoc_sec_num1']; 
															} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Secondary Number2</td>
													<td class="details">
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code2'] != "" && $profile_info['spoc_sec_num2'] != "") 
															{
																echo $profile_info['spoc_sec_num_country_code2']." - ".$profile_info['spoc_sec_num2']; 
															} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Specialities & products</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['specialities_products'] != "") { echo $profile_info['specialities_products']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Year of Establishment</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['establishment_year'] != "") { echo $profile_info['establishment_year']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight d-none">
													<td class="field">Organization Size</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['institution_size'] != "") { echo $profile_info['institution_size']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight d-none">
													<td class="field">Company Evaluation</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['company_evaluation'] != "") { echo $profile_info['company_evaluation']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Website</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['website'] != "") { echo $profile_info['website']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">LinkedIn Page</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['linkedin_page'] != "") { echo $profile_info['linkedin_page']; } else { echo '<span class="blank">--</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">PAN Card</td> 
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['pan_card'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['pan_card']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['pan_card'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['pan_card']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Organization Registration Certificate</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['institution_reg_certificate'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['institution_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Self Declaration</td>
													<td class="details">
														<?php 
														if(!empty($profile_info) && $profile_info['self_declaration'] != "") 
														{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['self_declaration']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['self_declaration'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['self_declaration']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">--</span>'; } ?>	
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Address Proof</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['address_proof'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['address_proof']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['address_proof'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['address_proof']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">GST Registration Certificate</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['gst_reg_certificate'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['gst_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">--</span>'; } ?>
													</td>
												</tr>
												
												<?php /* if(empty($bod_info))
												{	?>
													<tr class="highlight">
														<td class="field">Board Of Directors</td>
														<td class="details"><span class="blank">--</span></td>
													</tr>
										<?php	}
												else
												{	?>
												<tr class="highlight">
													<td class="field" colspan="2">Board Of Directors</td>
												</tr>												
												<tr>
													<th colspan="2" style="padding-left: 0;padding-right: 0;">
														<table class="table table-bordered">
															<thead>
																<th class='text-center'>Sr. No</th>
																<th class='text-center'>Name</th>
																<th class='text-center'>Designation</th>
																<th class='text-center'>Since</th>
															</thead>
															<tbody>
																<?php if(empty($bod_info))
																	{	?>
																	<tr><td colspan='4' class='text-center'>No data available</td></tr>
																	<?php	}
																	else
																	{
																		$i=1;
																		foreach($bod_info as $bod_key => $bod_val)
																		{	?>
																		<tr>
																			<td class='text-center'><?php echo $i; $i++; ?></td>
																			<td><?php echo $bod_val['bod_name']; ?></td>
																			<td><?php echo $bod_val['bod_designation']; ?></td>
																			<td><?php echo $bod_val['bod_since']; ?></td>
																		</tr>
																		<?php }
																	}	?>
															</tbody>
														</table>
													</th>
												</tr>
												<?php } */ ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
        $('#discoverable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $this->session->userdata('user_id') ; ?>';
            
            var discoverable 
            if ($('#discoverable:checked').val()=='on') {
              discoverable = 'yes';
            $("#contactable").prop("disabled", false);
            }else{
              discoverable = 'no';
              $('#contactable').prop('checked', false); // Unchecks it
              $("#contactable").prop("disabled", true);
            

            }

         var datastring='discoverable='+ discoverable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_discoverable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
          
          $('#contactable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $this->session->userdata('user_id') ; ?>';
            
            var contactable 
            if ($('#contactable:checked').val()=='on') {
              contactable = 'yes';
            }else{
              contactable = 'no';
            }

            var datastring='contactable='+ contactable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_contactable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
   
        
$(window).on('load', function () {
      var  discoverable = '<?php echo $personal_info['discoverable'] ; ?>';
        if (discoverable =='no') {
              ;
           
                $("#contactable").prop("disabled", true);
            }else{
              $("#contactable").prop("disabled", false);
            

            }
      
});          
</script>


<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>