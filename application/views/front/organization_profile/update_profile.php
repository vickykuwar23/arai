<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Update Profile</h1> 
		 <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo site_url('organization'); ?>" class="btn btn-upload2">Back To Profile</a>
        </div>
		<!-- <nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url('organization'); ?>">Manage Profile</a></li>
				<li class="breadcrumb-item active" aria-current="page">Update Profile </li>
			</ol>
		</nav> -->
	</div>
</div>

<?php $enc_obj =  New Opensslencryptdecrypt(); ?>

<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer a.img_outer img { max-width: 300px; max-height: 100px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
</style>

<section id="registration-form" class="inner-page" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="<?php echo site_url('organization/profile'); ?>" id="org_profile_form_step1" enctype="multipart/form-data">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					<input type="hidden" name="mode" value="<?php echo $mode; ?>" />
					
					<div class="formInfo">
						<div id="smartwizard_profile_org">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
								<?php /* <li><a href="#step-4">Step 4<br /><small></small></a></li> */ ?>
							</ul>
							<div>
								<div id="step-1" class="mt-4">
									<div class="row">										
									<?php $explode_org_sector_arr = array();
										if($mode == 'Add') { if(set_value('org_sector') != '') {  $explode_org_sector_arr = set_value('org_sector'); } } 
										else 
										{
											if(isset($form_data['org_sector']) && $form_data['org_sector'] != "")
											{
												$explode_org_sector_arr = explode(",",$form_data['org_sector']);
											}
										}	?>
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-control-placeholder flotingCss" for="org_sector">Organization Sector <em>*</em></label>
												<select class="form-control select2_common" name="org_sector[]" id="org_sector" required multiple autofocus onchange="show_hide_other_org_sector()">
														<?php if(!empty($org_sector_data))
														{
															foreach($org_sector_data as $res) 
															{ ?>
															<option value="<?php echo $res['id'] ?>" <?php if(in_array($res['id'],$explode_org_sector_arr)) { echo "selected"; } ?>><?php echo ucfirst($res['name']); ?></option>
															<?php }
														}	?>												
												</select>
												<?php if(form_error('org_sector')!=""){ ?><span class="error"><?php echo form_error('org_sector'); ?></span> <?php } ?>
											</div>
										</div>
									
										<div class="col-md-6 d-none" id="org_sector_other_div">
											<div class="form-group">
												<input type="text" name="org_sector_other" id="org_sector_other" class="form-control" required="" value="<?php if($mode == 'Add') { echo set_value('org_sector_other'); } else { echo $form_data['org_sector_other']; } ?>">
												<label for="org_sector_other" class="form-control-placeholder">Other Organization Sector <em class="mandatory">*</em></label>
												<?php if(form_error('org_sector_other')!=""){ ?><span class="error"><?php echo form_error('org_sector_other'); ?></span> <?php } ?>
											</div>
										</div>
								</div>
								
									<div class="row">										
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control" name="overview" id="overview" value="<?php if($mode == 'Add') { echo set_value('overview'); } else { echo $form_data['overview']; } ?>" required maxlength="255" />
												<label class="form-control-placeholder" for="overview">Company Overview <em>*</em></label>
												<?php if(form_error('overview')!=""){ ?><span class="error"><?php echo form_error('overview'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-12"><h4 class="titleBox">Representative Billing Address</h4></div>																			
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr1" id="spoc_bill_addr1" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr1'); } else { echo $form_data['spoc_bill_addr1']; } ?>" required maxlength="100" />
												<label for="spoc_bill_addr1" class="form-control-placeholder">Flat/House No/Building/Apt/Company <em>*</em></label>												
												<?php if(form_error('spoc_bill_addr1')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr1'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr2" id="spoc_bill_addr2" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr2'); } else { echo $form_data['spoc_bill_addr2']; } ?>" required maxlength="100" />
												<label for="spoc_bill_addr2" class="form-control-placeholder">Area/Colony/Street/Village <em>*</em></label>
												<?php if(form_error('spoc_bill_addr2')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr2'); ?></span> <?php } ?>
											</div>					
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr_city" id="spoc_bill_addr_city" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr_city'); } else { echo $form_data['spoc_bill_addr_city']; } ?>" required maxlength="25" />
												<label for="spoc_bill_addr_city" class="form-control-placeholder">Town/City <em>*</em></label>
												<?php if(form_error('spoc_bill_addr_city')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr_city'); ?></span> <?php } ?>
											</div>					
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr_state" id="spoc_bill_addr_state" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr_state'); } else { echo $form_data['spoc_bill_addr_state']; } ?>" required maxlength="25" />
												<label for="spoc_bill_addr_state" class="form-control-placeholder">State <em>*</em></label>
												<?php if(form_error('spoc_bill_addr_state')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr_state'); ?></span> <?php } ?>
											</div>					
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr_country" id="spoc_bill_addr_country" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr_country'); } else { echo $form_data['spoc_bill_addr_country']; } ?>" required maxlength="25" />
												<label for="spoc_bill_addr_country" class="form-control-placeholder">Country <em>*</em></label>
												<?php if(form_error('spoc_bill_addr_country')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr_country'); ?></span> <?php } ?>
											</div>					
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_bill_addr_pin" id="spoc_bill_addr_pin" value="<?php if($mode == 'Add') { echo set_value('spoc_bill_addr_pin'); } else { echo $form_data['spoc_bill_addr_pin']; } ?>" required maxlength="6" />
												<label for="spoc_bill_addr_pin" class="form-control-placeholder">Pincode <em>*</em></label>
												<?php if(form_error('spoc_bill_addr_pin')!=""){ ?><span class="error"><?php echo form_error('spoc_bill_addr_pin'); ?></span> <?php } ?>
											</div>					
										</div>
										
										<div class="col-md-12"><h4 class="titleBox">Representative Secondary Number</h4></div>
										<div class="col-md-3">
											<div class="form-group">
												<select class="form-control select2_common" name="spoc_sec_num_country_code1" id="spoc_sec_num_country_code1">
													<!-- <option value="">Country Code</option> -->
													<?php if(!empty($country_codes))
														{
															foreach($country_codes as $code)
															{	?>
															<option value="<?php echo $code['id'] ?>" <?php if($mode == 'Add') { if(set_value('spoc_sec_num_country_code1') == $code['id']) { echo "selected"; } else if($code['id'] == '99'){ echo "selected"; } } else if($mode == 'Update') { if($form_data['spoc_sec_num_country_code1'] == $code['id']) { echo "selected"; } else if($code['id'] == '99'){ echo "selected"; } } ?>><?php echo $code['iso']." ".$code['phonecode'] ?></option>
															<?php }
														}	?>
												</select>
												<?php /* <label class="form-control-placeholder" for="spoc_sec_num_country_code1">Select Country </label> */ ?>
											    <label class="form-control-placeholder leftLable2 flotingCss" for="cname">Country
                                            	</label>
												<div id="spoc_sec_num_country_code1_error"></div>
												<?php if(form_error('spoc_sec_num_country_code1')!=""){ ?><span class="error"><?php echo form_error('spoc_sec_num_country_code1'); ?></span> <?php } ?>
											</div>
										</div>
										
                    <div class="col-md-9">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_sec_num1" id="spoc_sec_num1" value="<?php if($mode == 'Add') { echo set_value('spoc_sec_num1'); } else { echo $form_data['spoc_sec_num1']; } ?>"  maxlength="15" />
												<label class="form-control-placeholder" for="spoc_sec_num1">Number</label>
												<div id="spoc_sec_num1_error"></div>
												<?php if(form_error('spoc_sec_num1')!=""){ ?><span class="error"><?php echo form_error('spoc_sec_num1'); ?></span> <?php } ?>
											</div>
										</div>
                    
										<?php /* ###
                    <div class="col-md-3">
											<button class="btn btn-primary mt-3 send_otp_button btn-sm" type="button" id="send_otp1">Send OTP</button>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<input type="text" class="form-control" name="otp1" id="otp1" value="<?php if($mode == 'Add') { echo set_value('otp1'); } ?>" maxlength="6">
												<label class="form-control-placeholder" for="otp1">Enter OTP </label>
												<?php if(form_error('otp1')!=""){ ?><span class="error"><?php echo form_error('otp1'); ?></span> <?php } ?>
											</div>
										</div> */?>
										
										<div class="col-md-3">
											<div class="form-group">
												<select class="form-control select2_common" name="spoc_sec_num_country_code2" id="spoc_sec_num_country_code2">
													<option value="">Country Code</option>
													<?php if(!empty($country_codes))
														{
															foreach($country_codes as $code)
															{	?>
															<option value="<?php echo $code['id'] ?>" <?php if($mode == 'Add') { if(set_value('spoc_sec_num_country_code2') == $code['id']) { echo "selected"; } else if($code['id'] == '99'){ echo "selected"; } } else if($mode == 'Update') { if($form_data['spoc_sec_num_country_code2'] == $code['id']) { echo "selected"; } else if($code['id'] == '99'){ echo "selected"; } } ?>><?php echo $code['iso']." ".$code['phonecode'] ?></option>
															<?php }
														}	?>
												</select>
												<?php /* <label class="form-control-placeholder" for="spoc_sec_num_country_code2">Select Country </label> */ ?>
												<div id="spoc_sec_num_country_code2_error"></div>
												<?php if(form_error('spoc_sec_num_country_code2')!=""){ ?><span class="error"><?php echo form_error('spoc_sec_num_country_code2'); ?></span> <?php } ?>
											</div>
										</div>
										
                    <div class="col-md-9">
											<div class="form-group">
												<input type="text" class="form-control" name="spoc_sec_num2" id="spoc_sec_num2" value="<?php if($mode == 'Add') { echo set_value('spoc_sec_num2'); } else { echo $form_data['spoc_sec_num2']; } ?>" maxlength="15" />
												<label class="form-control-placeholder" for="spoc_sec_num2">Number</label>
												<div id="spoc_sec_num2_error"></div>
												<?php if(form_error('spoc_sec_num2')!=""){ ?><span class="error"><?php echo form_error('spoc_sec_num2'); ?></span> <?php } ?>
											</div>
										</div>
                    
										<?php /* ###
                    <div class="col-md-3">
											<button class="btn btn-primary mt-3 send_otp_button btn-sm" type="button" id="send_otp2">Send OTP</button>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<input type="text" class="form-control" name="otp2" id="otp2" value="<?php if($mode == 'Add') { echo set_value('otp2'); } ?>" maxlength="6" />
												<label class="form-control-placeholder" for="otp2">Enter OTP </label>
												<?php if(form_error('otp2')!=""){ ?><span class="error"><?php echo form_error('otp2'); ?></span> <?php } ?>
											</div>
										</div> */ ?>
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="specialities_products" id="specialities_products" value="<?php if($mode == 'Add') { echo set_value('specialities_products'); } else { echo $form_data['specialities_products']; } ?>" required maxlength="255" />
												<label class="form-control-placeholder" for="specialities_products">Specialities & products <em>*</em></label>
												<?php if(form_error('specialities_products')!=""){ ?><span class="error"><?php echo form_error('specialities_products'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<select class="form-control" name="establishment_year" id="establishment_year" required>
													<option value=""></option>
													<?php if(!empty($year_arr))
														{
															foreach($year_arr as $year)
															{	?>
															<option value="<?php echo $year ?>" <?php if($mode == 'Add') { if(set_value('establishment_year') == $year) { echo "selected"; } } else { if($form_data['establishment_year'] == $year) { echo "selected"; } } ?>><?php echo $year; ?></option>
															<?php }
														}	?>
												</select>
												<label class="form-control-placeholder flotingCss" for="establishment_year">Year of Establishment <em>*</em></label>
												<?php if(form_error('establishment_year')!=""){ ?><span class="error"><?php echo form_error('establishment_year'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-6 d-none">
											<div class="form-group">
												<input type="text" class="form-control" name="institution_size" id="institution_size" value="<?php if($mode == 'Add') { echo set_value('institution_size'); } else { echo $form_data['institution_size']; } ?>" maxlength="10" />
												<label class="form-control-placeholder" for="institution_size">Organization Size <em></em></label>
												<?php if(form_error('institution_size')!=""){ ?><span class="error"><?php echo form_error('institution_size'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-6 d-none">
											<div class="form-group">
												<input type="text" class="form-control" name="company_evaluation" id="company_evaluation" value="<?php if($mode == 'Add') { echo set_value('company_evaluation'); } else { echo $form_data['company_evaluation']; } ?>" maxlength="10" />
												<label class="form-control-placeholder" for="company_evaluation">Company Evaluation <em></em></label>
												<?php if(form_error('company_evaluation')!=""){ ?><span class="error"><?php echo form_error('company_evaluation'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="test" class="form-control" name="website" id="website" value="<?php if($mode == 'Add') { echo set_value('website'); } else { echo $form_data['website']; } ?>" required maxlength="255" />
												<label class="form-control-placeholder" for="website">Website <em>*</em></label>
												<?php if(form_error('website')!=""){ ?><span class="error"><?php echo form_error('website'); ?></span> <?php } ?>
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
												<input type="test" class="form-control" name="linkedin_page" id="linkedin_page" value="<?php if($mode == 'Add') { echo set_value('linkedin_page'); } else { echo $form_data['linkedin_page']; } ?>" required maxlength="255" />
												<label class="form-control-placeholder" for="linkedin_page">LinkedIn Page <em>*</em></label>
												<?php if(form_error('linkedin_page')!=""){ ?><span class="error"><?php echo form_error('linkedin_page'); ?></span> <?php } ?>
											</div>
										</div>										
									</div>
								</div>
								
								<?php /*<div id="step-3" class="mt-4">
								<div class="row">
									<div class="col-md-12"><h4 class="titleBox">Board of Directors</h4></div>
									
								</div>	
									<?php 
										if($mode == 'Add') { $bod_id_arr = set_value('bod_id'); $bod_name_arr = set_value('bod_name'); $bod_designation_arr = set_value('bod_designation'); $bod_since_arr = set_value('bod_since'); } 
										else { $bod_id_arr = $bod_form_data; }
									?>
									<input type="hidden" name="bod_row_cnt" id="bod_row_cnt" value="0">									
									<div id="bod_row_0">
										<input type="hidden" name="bod_id[]" id="bod_id0" value="<?php if($mode == 'Add') { echo "0"; } else { if(isset($bod_form_data[0]['bod_id'])) { echo $bod_form_data[0]['bod_id']; } } ?>">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<input type="test" class="form-control" name="bod_name[]" id="bod_name0" value="<?php if($mode == 'Add') { if(isset($bod_name_arr[0])) { echo $bod_name_arr[0]; } } else { if(isset($bod_form_data[0]['bod_name'])) { echo $bod_form_data[0]['bod_name']; } } ?>" maxlength="100" />
													<label class="form-control-placeholder" for="bod_name">Name <em></em></label>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<input type="test" class="form-control" name="bod_designation[]" id="bod_designation0" value="<?php if($mode == 'Add') { if(isset($bod_designation_arr[0])) { echo $bod_designation_arr[0]; } } else { if(isset($bod_form_data[0]['bod_designation'])) { echo $bod_form_data[0]['bod_designation']; } } ?>" maxlength="100" />
													<label class="form-control-placeholder" for="bod_designation">Designation <em></em></label>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<select class="form-control" name="bod_since[]" id="bod_since0">
														<option value=""></option>
														<?php if(!empty($year_arr))
															{
																foreach($year_arr as $year)
																{	?>
																<option value="<?php echo $year ?>" <?php if($mode == 'Add') { if(isset($bod_since_arr[0]) && $bod_since_arr[0] == $year) { echo "selected"; } } else { if(isset($bod_form_data[0]['bod_since']) && $bod_form_data[0]['bod_since'] == $year) { echo "selected"; } } ?>><?php echo $year; ?></option>
																<?php }
															}	?>
													</select>
													<label class="form-control-placeholder" for="bod_since">Since <em></em></label>
												</div>
											</div>	
											
											<div class="col-md-1"><div class="form-group"></div></div>
										</div>
									</div>
									
									<?php if(!empty($bod_id_arr))
										{
											for($i=0; $i < count($bod_id_arr); $i++)
											{
												if($i > 0)
												{	?>
												<div id="bod_row_<?php echo $i; ?>">
													<input type="hidden" name="bod_id[]" id="bod_id<?php echo $i; ?>" value="<?php if($mode == 'Add') { echo "0"; } else { if(isset($bod_form_data[$i]['bod_id'])) { echo $bod_form_data[$i]['bod_id']; } } ?>">
													<div class="row">
														<div class="col-md-4">
															<div class="form-group">
																<input type="test" class="form-control" name="bod_name[]" id="bod_name<?php echo $i; ?>" value="<?php if($mode == 'Add') { if(isset($bod_name_arr[$i])) { echo $bod_name_arr[$i]; } } else { if(isset($bod_form_data[$i]['bod_name'])) { echo $bod_form_data[$i]['bod_name']; } } ?>" maxlength="100" />
																<label class="form-control-placeholder" for="bod_name">Name <em></em></label>
															</div>
														</div>
														
														<div class="col-md-4">
															<div class="form-group">
																<input type="test" class="form-control" name="bod_designation[]" id="bod_designation<?php echo $i; ?>" value="<?php if($mode == 'Add') { if(isset($bod_designation_arr[$i])) { echo $bod_designation_arr[$i]; } } else { if(isset($bod_form_data[$i]['bod_designation'])) { echo $bod_form_data[$i]['bod_designation']; } } ?>" maxlength="100" />
																<label class="form-control-placeholder" for="bod_designation">Designation <em></em></label>
															</div>
														</div>
														
														<div class="col-md-3">
															<div class="form-group">
																<select class="form-control" name="bod_since[]" id="bod_since<?php echo $i; ?>">
																	<option value=""></option>
																	<?php if(!empty($year_arr))
																		{
																			foreach($year_arr as $year)
																			{	?>
																			<option value="<?php echo $year ?>" <?php if($mode == 'Add') { if(isset($bod_since_arr[$i]) && $bod_since_arr[$i] == $year) { echo "selected"; } } else { if(isset($bod_form_data[$i]['bod_since']) && $bod_form_data[$i]['bod_since'] == $year) { echo "selected"; } } ?>><?php echo $year; ?></option>
																			<?php }
																		}	?>
																</select>
																<label class="form-control-placeholder" for="bod_since">Since <em></em></label>
															</div>
														</div>	
														
														<div class="col-md-1">
															<div class="form-group">
																<button type="button" class="btn btn-primary" onclick="remove_current_div('<?php echo $i; ?>')"><i class="fa fa-trash"></i></button>
															</div>
														</div>
													</div>
												</div>
												<?php	}
											}
										}	?>										
										<button type="button" id="bod_insert_before_div" class="btn btn-primary btn-sm" onclick="append_bod_row()"><i class="fa fa-plus"></i> Add More</button>
								</div> */ ?>
								
								<div id="step-3" class="mt-4">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Logo</button>
												<input type="file" class="form-control" name="org_logo" id="org_logo" <?php /* if($mode == "Add") { ?>required<?php } */ ?> />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png image formats below 2MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('org_logo')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('org_logo'); ?></span> <?php } ?>
												<?php if($org_logo_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $org_logo_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['org_logo'] != '')
											{	?>
											<div class="col-md-6" id="org_logo_outer">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['org_logo']; ?>" target="_blank">
															<?php $disp_img_name = $form_data['org_logo']; ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
														<a class="btn btn-danger btn-sm" onclick="delete_single_file('<?php echo $enc_obj->encrypt('arai_profile_organization'); ?>', '<?php echo $enc_obj->encrypt('org_profile_id'); ?>', '<?php echo $enc_obj->encrypt($form_data['org_profile_id']); ?>', 'org_logo', '<?php echo $enc_obj->encrypt('./uploads/organization_profile/'); ?>', 'Logo')" href="javascript:void(0)">Delete</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> <span class="text-danger">*</span> PAN Card</button>
												<input type="file" class="form-control" name="pan_card" id="pan_card" <?php if($mode == "Add") { ?>required<?php } ?> />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png, .pdf file formats below 5MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('pan_card')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('pan_card'); ?></span> <?php } ?>
												<?php if($pan_card_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $pan_card_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['pan_card'] != '')
											{	?>
											<div class="col-md-6">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['pan_card']; ?>" target="_blank">
															<?php if(strtolower(pathinfo($form_data['pan_card'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'blank_pdf.png'; } else { $disp_img_name = $form_data['pan_card']; } ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> <span class="text-danger">*</span> Organization Registration Certificate</button>
												<input type="file" class="form-control" name="institution_reg_certificate" id="institution_reg_certificate" <?php if($mode == "Add") { ?>required<?php } ?> />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png, .pdf file formats below 5MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('institution_reg_certificate')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('institution_reg_certificate'); ?></span> <?php } ?>
												<?php if($institution_reg_certificate_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $institution_reg_certificate_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['institution_reg_certificate'] != '')
											{	?>
											<div class="col-md-6">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['institution_reg_certificate']; ?>" target="_blank">
															<?php if(strtolower(pathinfo($form_data['institution_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'blank_pdf.png'; } else { $disp_img_name = $form_data['institution_reg_certificate']; } ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> <span class="text-danger">*</span> Self Declaration for all of the above data</button>
												<input type="file" class="form-control" name="self_declaration" id="self_declaration" <?php if($mode == "Add") { ?>required<?php } ?> />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png, .pdf file formats below 5MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('self_declaration')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('self_declaration'); ?></span> <?php } ?>
												<?php if($self_declaration_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $self_declaration_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['self_declaration'] != '')
											{	?>
											<div class="col-md-6">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['self_declaration']; ?>" target="_blank">
															<?php if(strtolower(pathinfo($form_data['self_declaration'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'blank_pdf.png'; } else { $disp_img_name = $form_data['self_declaration']; } ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> Address Proof</button>
												<input type="file" class="form-control" name="address_proof" id="address_proof" />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png, .pdf file formats below 5MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('address_proof')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('address_proof'); ?></span> <?php } ?>
												<?php if($address_proof_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $address_proof_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['address_proof'] != '')
											{	?>
											<div class="col-md-6" id="address_proof_outer">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['address_proof']; ?>" target="_blank">
															<?php if(strtolower(pathinfo($form_data['address_proof'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'blank_pdf.png'; } else { $disp_img_name = $form_data['address_proof']; } ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
														<a class="btn btn-danger btn-sm" onclick="delete_single_file('<?php echo $enc_obj->encrypt('arai_profile_organization'); ?>', '<?php echo $enc_obj->encrypt('org_profile_id'); ?>', '<?php echo $enc_obj->encrypt($form_data['org_profile_id']); ?>', 'address_proof', '<?php echo $enc_obj->encrypt('./uploads/organization_profile/'); ?>', 'Address Proof')" href="javascript:void(0)">Delete</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group upload-btn-wrapper">
												<button class="btn btn-upload"><i class="fa fa-plus"> </i> GST Registration Certificate</button>
												<input type="file" class="form-control" name="gst_reg_certificate" id="gst_reg_certificate" />
												<div class="clearfix"></div>
												<span class="small">Note : Only .jpg, .jpeg, .png, .pdf file formats below 5MB are accepted</span><div class="clearfix"></div>
												<?php if(form_error('gst_reg_certificate')!=""){ ?><div class="clearfix"></div><span class="error"><?php echo form_error('gst_reg_certificate'); ?></span> <?php } ?>
												<?php if($gst_reg_certificate_error!=""){ ?><div class="clearfix"></div><span class="error"><?php echo $gst_reg_certificate_error; ?></span> <?php } ?>
											</div>
										</div>
										
										<?php
											if($mode == 'Update' && $form_data['gst_reg_certificate'] != '')
											{	?>
											<div class="col-md-6" id="gst_reg_certificate_outer">
												<div class="form-group">
													<div class="previous_img_outer">
														<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$form_data['gst_reg_certificate']; ?>" target="_blank">
															<?php if(strtolower(pathinfo($form_data['gst_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'blank_pdf.png'; } else { $disp_img_name = $form_data['gst_reg_certificate']; } ?>
															<img src="<?php echo base_url().'uploads/organization_profile/'.$disp_img_name; ?>">
														</a>
														<a class="btn btn-danger btn-sm" onclick="delete_single_file('<?php echo $enc_obj->encrypt('arai_profile_organization'); ?>', '<?php echo $enc_obj->encrypt('org_profile_id'); ?>', '<?php echo $enc_obj->encrypt($form_data['org_profile_id']); ?>', 'gst_reg_certificate', '<?php echo $enc_obj->encrypt('./uploads/organization_profile/'); ?>', 'GST Registration Certificate')" href="javascript:void(0)">Delete</a>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>								
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>					

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	$('.select2_common').select2();
	
	/******* SWEET ALERT POP UP *********/
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
		
	function show_hide_other_org_sector() 
	{
		var selected = $("#org_sector").find('option:selected', this);
		var results = [];
		
		selected.each(function() { results.push($(this).text().toLowerCase()); });
		var display_flag=0;

		$.each(results,function(i)
		{
			if(results[i]=='other')
			{
				display_flag = 1;
			}
		});

		if (display_flag==1) 
		{
			$('#org_sector_other_div').removeClass('d-none');
		}
		else
		{
			$('#org_sector_other').val('');
			$('#org_sector_other_div').addClass('d-none');
		}
	}
	show_hide_other_org_sector();
	
	/* //******* ADD NEW BOARD OF DIRECTORS ROW WHEN CLICK ON ADD MORE BUTTON *********
	function append_bod_row()
	{
		var total_div_cnt = $('input[name*="bod_id[]"]').length;
		if(total_div_cnt >= 50)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" Board of Directors", type: "warning" });
		}
		else
		{		
			var bod_row_cnt = $("#bod_row_cnt").val();
			var bod_row_cnt_new = parseInt(bod_row_cnt)+1;
			
			var year_arr = <?php echo json_encode($year_arr); ?>;			
			var append_html = '';		
			append_html += '<div id="bod_row_'+bod_row_cnt_new+'">';
			append_html += '	<input type="hidden" name="bod_id[]" id="bod_id'+bod_row_cnt_new+'" value="0">';
			append_html += '	<div class="row">';
			append_html += '		<div class="col-md-4">';
			append_html += '			<div class="form-group">';
			append_html += '				<input type="test" class="form-control" name="bod_name[]" id="bod_name'+bod_row_cnt_new+'" value="" maxlength="100" />';
			append_html += '				<label class="form-control-placeholder" for="bod_name">Name <em></em></label>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-4">';
			append_html += '			<div class="form-group">';
			append_html += '				<input type="test" class="form-control" name="bod_designation[]" id="bod_designation'+bod_row_cnt_new+'" value="" maxlength="100" />';
			append_html += '				<label class="form-control-placeholder" for="bod_designation">Designation <em></em></label>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-3">';
			append_html += '			<div class="form-group">';
			append_html += '				<select class="form-control" name="bod_since[]" id="bod_since'+bod_row_cnt_new+'">';
			append_html += '					<option value=""></option>';
			
			$.each(year_arr,function(index, value) 
			{
				append_html += '	<option value="'+value+'">'+value+'</option>';
			});
			
			append_html += '				</select>';
			append_html += '				<label class="form-control-placeholder" for="bod_since">Since <em></em></label>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div('+bod_row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';													
			append_html += '	</div>';
			append_html += '</div>';
			
			$("#bod_row_cnt").val(bod_row_cnt_new);
			$(append_html).insertBefore("#bod_insert_before_div");
			
			$("input").change( function() 
			{
				 if ($(this).val() != '') { $(this).parent().find('label').addClass('floatinglabel'); }
				 else { $(this).parent().find('label').removeClass('floatinglabel'); }
			});
			
			$("select").change( function() 
			{
				 if ($(this).val() != '') { $(this).parent().find('label').addClass('floatinglabel'); }
				 else { $(this).parent().find('label').removeClass('floatinglabel'); }
			});
		}
	}	
		
	///******* REMOVE BOARD OF DIRECTORS ROW WHEN CLICK ON DELETE BUTTON *********
	function remove_current_div(div_no)
	{
		swal(
		{
			title:"Confirm?" ,
			text: "Are you confirm to delete the record?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#bod_row_"+div_no).remove();
			}
		});	
	} */
	// <?php 
	// $this->load->library('user_agent');
	// $agent =$this->agent->browser();
	// ?>	
	
	function scroll_to_top(div_id)
	{

		// var browser = '<?php echo $agent;  ?>';

		if(div_id == '' || div_id==undefined) { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
	
	
	/******* ON ENTER, PREVENT FORM SUBMIT *********/
	$(window).keydown(function(event)
	{
		if(event.keyCode == 13) 
		{ 
			$("#org_profile_form_step1").submit(function(e) { e.preventDefault(); });
			$("#org_profile_form_step1").validate();
		}
	});
	
	$(document).ready(function() 
	{
		/******* STEP SHOW EVENT *********/
    $("#smartwizard_profile_org").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') 
			{ 
				$(".sw-btn-prev").addClass('d-none');	
				$(".sw-btn-next").removeClass('d-none'); 
			 $(".btnfinish").addClass('d-none'); 
				$(".btnBack").addClass('d-none'); 
			} 
			else if (stepPosition === 'final') 
			{ 
				$(".sw-btn-prev").removeClass('d-none');	
				$(".sw-btn-next").addClass('d-none'); 
				$(".btnfinish").removeClass('d-none');
				$(".btnBack").removeClass('d-none');
			} 
			else 
			{ 
				$(".sw-btn-prev").removeClass('d-none');	
				$(".sw-btn-next").removeClass('d-none'); 
				$(".btnfinish").addClass('d-none');
				$(".btnBack").addClass('d-none');
		     }
		});
		
		/******* STEP WIZARD *********/
		$('#smartwizard_profile_org').smartWizard(
		{
			/* selected: 3,  */
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */
			keyNavigation: false,
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
					$('<button></button>').text('Submit').addClass('btn btn-primary btnfinish').on('click', function(e)
					{ 
						e.preventDefault();						
						var submit_flag = 0;
						
						if($("#gst_reg_certificate").valid()==false) { submit_flag = 1; $("#gst_reg_certificate").focus(); }
						if($("#address_proof").valid()==false) { submit_flag = 1; $("#address_proof").focus(); }
						if($("#self_declaration").valid()==false) { submit_flag = 1; $("#self_declaration").focus(); }
						if($("#institution_reg_certificate").valid()==false) { submit_flag = 1; $("#institution_reg_certificate").focus(); }
						if($("#pan_card").valid()==false) { submit_flag = 1; $("#pan_card").focus(); }	
						if($("#org_logo").valid()==false) { submit_flag = 1; $("#org_logo").focus(); }	
						
						if($("#org_logo").valid()==false) { scroll_to_top('org_logo'); }
						else if($("#pan_card").valid()==false) { scroll_to_top('pan_card'); }
						else if($("#institution_reg_certificate").valid()==false) { scroll_to_top('institution_reg_certificate'); }
						else if($("#self_declaration").valid()==false) { scroll_to_top('self_declaration'); }
						else if($("#address_proof").valid()==false) { scroll_to_top('address_proof'); }
						else if($("#gst_reg_certificate").valid()==false) { scroll_to_top('gst_reg_certificate'); }
												
						if(submit_flag == 0)
						{
							swal(
							{
								title:"Confirm?",
								text: "Are you sure you want to update the details?",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes!'
							}).then(function (result) { if (result.value) { $('#org_profile_form_step1').submit(); } });
						}
					}),
					/* $('<a style="margin-left: 5px;border-radius: 4px;" href="<?php echo site_url('organization'); ?>"></a>').text('Back').addClass('btn btn-primary btnBack')					 */
				]
			}
		});
				
		$("#smartwizard_profile_org").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{				
				/* ### if($("#otp2").valid()==false) { isValidate= false; $("#otp2").focus(); } */
				if($("#spoc_sec_num2").valid()==false) { isValidate= false; $("#spoc_sec_num2").focus(); }
				if($("#spoc_sec_num_country_code2").valid()==false) { isValidate= false; $("#spoc_sec_num_country_code2").focus(); }
				/* ### if($("#otp1").valid()==false) { isValidate= false; $("#otp1").focus(); } */
				if($("#spoc_sec_num1").valid()==false) { isValidate= false; $("#spoc_sec_num1").focus(); }
				if($("#spoc_sec_num_country_code1").valid()==false) { isValidate= false; $("#spoc_sec_num_country_code1").focus(); }
				if($("#spoc_bill_addr_pin").valid()==false) { isValidate= false; $("#spoc_bill_addr_pin").focus(); }
				if($("#spoc_bill_addr_country").valid()==false) { isValidate= false; $("#spoc_bill_addr_country").focus(); }
				if($("#spoc_bill_addr_state").valid()==false) { isValidate= false; $("#spoc_bill_addr_state").focus(); }
				if($("#spoc_bill_addr_city").valid()==false) { isValidate= false; $("#spoc_bill_addr_city").focus(); }
				if($("#spoc_bill_addr2").valid()==false) { isValidate= false; $("#spoc_bill_addr2").focus(); }
				if($("#spoc_bill_addr1").valid()==false) { isValidate= false; $("#spoc_bill_addr1").focus(); }
				if($("#overview").valid()==false) { isValidate= false; $("#overview").focus(); }
				if($("#org_sector_other").valid()==false) { isValidate= false; $("#org_sector_other").focus(); }
				if($("#org_sector").valid()==false) { isValidate= false; $("#org_sector").focus(); }
				
				if($("#org_sector").valid()==false) { scroll_to_top('org_sector'); }
				else if($("#org_sector_other").valid()==false) { scroll_to_top('org_sector_other'); }
				else if($("#overview").valid()==false) { scroll_to_top('overview'); }
				else if($("#spoc_bill_addr1").valid()==false) { scroll_to_top('spoc_bill_addr1'); }
				else if($("#spoc_bill_addr2").valid()==false) { scroll_to_top('spoc_bill_addr2'); }
				else if($("#spoc_bill_addr_city").valid()==false) { scroll_to_top('spoc_bill_addr_city'); }
				else if($("#spoc_bill_addr_state").valid()==false) { scroll_to_top('spoc_bill_addr_state'); }
				else if($("#spoc_bill_addr_country").valid()==false) { scroll_to_top('spoc_bill_addr_country'); }
				else if($("#spoc_bill_addr_pin").valid()==false) { scroll_to_top('spoc_bill_addr_pin'); }
				else if($("#spoc_sec_num_country_code1").valid()==false) { scroll_to_top('spoc_sec_num_country_code1'); }
				else if($("#spoc_sec_num1").valid()==false) { scroll_to_top('spoc_sec_num1'); }
				/* ### else if($("#otp1").valid()==false) { scroll_to_top('otp1'); } */
				else if($("#spoc_sec_num_country_code2").valid()==false) { scroll_to_top('spoc_sec_num_country_code2'); }
				else if($("#spoc_sec_num2").valid()==false) { scroll_to_top('spoc_sec_num2'); }
				/* ### else if($("#otp2").valid()==false) { scroll_to_top('otp2'); } */
				else { scroll_to_top('smartwizard_profile_org'); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				if($("#linkedin_page").valid()==false) { isValidate= false; $("#linkedin_page").focus(); }
				if($("#website").valid()==false) { isValidate= false; $("#website").focus(); }
				if($("#company_evaluation").valid()==false) { isValidate= false; $("#company_evaluation").focus(); }
				if($("#institution_size").valid()==false) { isValidate= false; $("#institution_size").focus(); }
				if($("#establishment_year").valid()==false) { isValidate= false; $("#establishment_year").focus(); }
				if($("#specialities_products").valid()==false) { isValidate= false; $("#specialities_products").focus(); }	
				
				if($("#specialities_products").valid()==false) { scroll_to_top('specialities_products'); }
				else if($("#establishment_year").valid()==false) { scroll_to_top('establishment_year'); }
				else if($("#institution_size").valid()==false) { scroll_to_top('institution_size'); }
				else if($("#company_evaluation").valid()==false) { scroll_to_top('company_evaluation'); }
				else if($("#website").valid()==false) { scroll_to_top('website'); }
				else if($("#linkedin_page").valid()==false) { scroll_to_top('linkedin_page'); }
				else { scroll_to_top('smartwizard_profile_org'); }
			}
			if(stepDirection=="backward") { scroll_to_top('smartwizard_profile_org'); }
			
						
			return isValidate;
		}) 
	});
	
	/******* DELETE ORGANIZATION PROFILE IMAGES *********/
	// function delete_single_file(tbl_nm, pk_nm, del_id, input_nm, file_path, swal_text)
	// { 
	// 	swal(
	// 	{
	// 		title:"Confirm?" ,
	// 		text: "Are you confirm to delete the "+swal_text+"?",
	// 		type: 'warning',
	// 		showCancelButton: true,
	// 		confirmButtonColor: '#3085d6',
	// 		cancelButtonColor: '#d33',
	// 		confirmButtonText: 'Yes!'
	// 	}).then(function (result) 
	// 	{
	// 		if (result.value) 
	// 		{
	// 			var csrf_test_name = $("#csrf_token").val();
	// 			var data = { 
	// 					'tbl_nm': encodeURIComponent($.trim(tbl_nm)), 
	// 					'pk_nm': encodeURIComponent($.trim(pk_nm)), 
	// 					'del_id': encodeURIComponent($.trim(del_id)), 
	// 					'input_nm': encodeURIComponent($.trim(input_nm)), 
	// 					'file_path': encodeURIComponent($.trim(file_path)), 
	// 					'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
	// 					};			
	// 			$.ajax(
	// 			{ 
	// 				type: "POST", 
	// 				url: '<?php echo site_url("delete_single_file") ?>', 
	// 				data: data, 
	// 				dataType: 'JSON',
	// 				success:function(data) 
	// 				{ 
	// 					$("#csrf_token").val(data.csrf_new_token);
	// 					$("#"+input_nm+"_outer").remove();
						
	// 					swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" });
	// 				}
	// 			});
	// 		}
	// 	});
	// }
	
	function delete_org_profile_files_bk(org_profile_id, input_name)
	{ 
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				var csrf_test_name = $("#csrf_token").val();
				var data = { 'org_profile_id': encodeURIComponent($.trim(org_profile_id)), 'input_name': encodeURIComponent($.trim(input_name)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name))  };			
				$.ajax(
				{ 
					type: "POST", 
					url: "<?php echo site_url('organization/delete_org_profile_files') ?>", 
					data: data, 
					dataType: 'JSON',
					success:function(data) 
					{ 
						$("#csrf_token").val(data.csrf_new_token);
						//location.reload(); 
						$("#"+input_name+"_outer").remove();
						sweet_alert_success('File successfully deleted');
					}
				});
			}
		});
	}
		
	/******* GENERATE OTP *********/
	function generate_otp(div_id)
	{
		var mobile = $.trim($("#"+div_id).val());
		var csrf_test_name = $("#csrf_token").val();
		if(mobile != '')
		{
			parameters= { 'mobile':encodeURIComponent($.trim(mobile)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) }
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('organization/send_otp'); ?>",
				data: parameters,
				cache: false,
				dataType: 'JSON',
				success:function(data)
				{
					$("#csrf_token").val(data.csrf_new_token);
					if(data.flag == "success")
					{ 
						if(div_id == 'spoc_sec_num1') 
						{ 
							$("#send_otp1").text('OTP Sent'); 
							$('#send_otp1').prop('disabled', true);
							setTimeout(function() { $('#send_otp1').text('Resend OTP'); $('#send_otp1').prop('disabled', false); }, 30*1000);
						}
						else if(div_id == 'spoc_sec_num2') 
						{ 
							$("#send_otp2").html('OTP Sent'); 
							$('#send_otp2').prop('disabled', true);
							setTimeout(function() { $('#send_otp2').text('Resend OTP'); $('#send_otp2').prop('disabled', false); }, 30*1000);
						}
					}
					else {	}
				}
			});
		}
	}
			
	$('.send_otp_button').click(function(e)
	{
		var otp_btn_id = (this.id);
		if(otp_btn_id == 'send_otp1') { var id_num = 1; }
		else if(otp_btn_id == 'send_otp2') { var id_num = 2; }
		
		var spoc_sec_num_country_code = $.trim($("#spoc_sec_num_country_code"+id_num).val());
		var spoc_sec_num = $.trim($("#spoc_sec_num"+id_num).val());
		
		if(spoc_sec_num_country_code == '') 
		{ 
			$("#spoc_sec_num_country_code"+id_num+"_error").html('<span id="spoc_sec_num_country_code'+id_num+'" class="error">Please enter the country code</span>');
		}
		else { $("#spoc_sec_num_country_code"+id_num+"_error").html(''); }
		
		if(spoc_sec_num == '') 
		{ 
			$("#spoc_sec_num"+id_num+"_error").html('<span id="spoc_sec_num'+id_num+'" class="error">Please enter the number</span>');
		}
		else { $("#spoc_sec_num"+id_num+"_error").html(''); }
		
		if ($("#spoc_sec_num_country_code"+id_num).valid()==false || $("#spoc_sec_num"+id_num).valid()==false) 
		{
			return false;
		}
		else
		{
			generate_otp('spoc_sec_num'+id_num);
		}
	});	
</script>
		
<script type="text/javascript">
	$(document ).ready( function() 
	{
		/******* JQUERY VALIDATION METHOD TO CHECK FILE SIZE *********/
		/* jQuery.validator.addMethod("filesize_max", function(value, element, param) 
		{
			var isOptional = this.optional(element), file;
			if(isOptional) { return isOptional; }
			
			if ($(element).attr("type") === "file") 
			{
				if (element.files && element.files.length) 
				{
					file = element.files[0]; 
					return ( file.size && file.size <= param ); 
				}
			}
			return false;
		}, "File size is too large."); */
		
		/******* JQUERY VALIDATION METHOD TO CHECK MOBILE NUMBERS NOT EQUAL *********/
		jQuery.validator.addMethod("notEqual", function(value, element, param) 
		{
			var check_val = $.trim($(param).val());
			return this.optional(element) || value != check_val;
		});


    	$.validator.addMethod('not_All_digits', function(value) {
	    	if(value.match(/^\d+$/)) {
	    		return false;
			}else{
				return true;
			}	
    	}, 'Please enter valid details');
		
		/******* JQUERY VALIDATION METHOD TO CHECK OTP VALIDATION *********/
		$.validator.addMethod("custom_validate_otp1", function(value, element)
		{
			var otp_value = $.trim(value);
			var mode = "<?php echo $mode; ?>";
			var spoc_num = $.trim($("#spoc_sec_num1").val());
			
			if(mode == 'Add' && spoc_num == '')
			{
				return true;
			}
			else
			if(mode == 'Add' && spoc_num != '' && otp_value == '')
			{
				$.validator.messages.custom_validate_otp1 = 'Please enter the otp';
				return false;
			}
			else
			{
				if(spoc_num != '')
				{
					var isSuccess = false;
					var csrf_test_name = $("#csrf_token").val();
					var parameter = { "otp":encodeURIComponent($.trim(otp_value)), "spoc_num":encodeURIComponent($.trim(spoc_num)), "mode":encodeURIComponent($.trim(mode)), "input":"spoc_sec_num1", "csrf_test_name":encodeURIComponent($.trim(csrf_test_name))  }
					
					$.ajax(
					{
						type: "POST",
						url: "<?php echo site_url('organization/verify_otp') ?>",
						data: parameter,
						async: false,
						dataType: 'JSON',
						success: function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							if($.trim(data.flag) == 'success')
							{
								isSuccess = true;
							} else { isSuccess = false; }
							
							$.validator.messages.custom_validate_otp1 = data.response;
						}
					});
					
					return isSuccess;
				}
				else { return true; }
			}
		});
				
		/******* JQUERY VALIDATION METHOD TO CHECK OTP VALIDATION *********/
		$.validator.addMethod("custom_validate_otp2", function(value, element)
		{
			var otp_value = $.trim(value);
			var mode = "<?php echo $mode; ?>";
			var spoc_num = $.trim($("#spoc_sec_num2").val());
			
			if(mode == 'Add' && spoc_num == '')
			{
				return true;
			}
			else if(mode == 'Add' && spoc_num != '' && otp_value == '')
			{
				$.validator.messages.custom_validate_otp2 = 'Please enter the otp';
				return false;
			}
			else
			{
				if(spoc_num != '')
				{
					var isSuccess = false;
					var csrf_test_name = $("#csrf_token").val();
					var parameter = { "otp":encodeURIComponent($.trim(otp_value)), "spoc_num":encodeURIComponent($.trim(spoc_num)), "mode":encodeURIComponent($.trim(mode)), "input":"spoc_sec_num2", "csrf_test_name":encodeURIComponent($.trim(csrf_test_name))  }
					
					$.ajax(
					{
						type: "POST",
						url: "<?php echo site_url('organization/verify_otp') ?>",
						data: parameter,
						async: false,
						dataType: 'JSON',
						success: function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							if($.trim(data.flag) == 'success')
							{
								isSuccess = true;
							} else { isSuccess = false; }
							
							$.validator.messages.custom_validate_otp2 = data.response;
						}
					});
					
					return isSuccess;
				}
				else { return true; }
			}
		});
			
		/******* JQUERY VALIDATION *********/
		$("#org_profile_form_step1").validate( 
		{
			onkeyup: false,
			rules:
			{
				'org_sector[]': { required: true },
				org_sector_other: { required: true, nowhitespace: true },
				overview: { required: true, nowhitespace: true },
				spoc_bill_addr1: { required: true, nowhitespace: true },
				spoc_bill_addr2: { required: true, nowhitespace: true },
				spoc_bill_addr_city: { required: true, nowhitespace: true,not_All_digits:true },
				spoc_bill_addr_state: { required: true, nowhitespace: true,not_All_digits:true },
				spoc_bill_addr_country: { required: true, nowhitespace: true,not_All_digits:true },
				spoc_bill_addr_pin: { required: true, nowhitespace: true, digits:true, minlength:6, maxlength:6 },
						
				spoc_sec_num_country_code1: 
				{ 
					required: function() { var spoc_num1 = $.trim($("#spoc_sec_num1").val()); if(spoc_num1 == '') { return false; } else { return true; } }
				},
				spoc_sec_num1: 
				{ 
					digits:function()
					{
						var spoc_num1 = $.trim($("#spoc_sec_num1").val());
						if(spoc_num1 == '') { } else { $("#spoc_sec_num1_error").html(''); }											
						return true;
					}, 
					minlength:8, maxlength:15, min:1,
					remote: 
					{ 
						depends: function()
						{
							var spoc_num1 = $.trim($("#spoc_sec_num1").val());
							if(spoc_num1 == '') { return false; } else { return true; }
						},
						param: 
						{											
							url: "<?php echo site_url('organization/check_spoc_sec_num_exist') ?>", 
							type: "post", 
							async: false,
							dataType: 'JSON',
							data: 
							{ 
								"spoc_num": function() { return $.trim($("#spoc_sec_num1").val()) }, 
								"csrf_test_name": function() { return $.trim($("#csrf_token").val()) }, 
								"col_name": function() { return 'spoc_sec_num1'; } 
							}
						}
					}
				},
				/* ### otp1: { digits:true, minlength:6, maxlength:6, custom_validate_otp1:true }, */
				spoc_sec_num_country_code2: 
				{ 
					required: function() { var spoc_num2 = $.trim($("#spoc_sec_num2").val()); if(spoc_num2 == '') { return false; } else { return true; } }
				},
				spoc_sec_num2: 
				{ 
					digits:function()
					{
						var spoc_num2 = $.trim($("#spoc_sec_num2").val());
						if(spoc_num2 == '') { } else { $("#spoc_sec_num2_error").html(''); }											
						return true;
					}, 
					minlength:8, maxlength:15, min:1, notEqual: "#spoc_sec_num1",
					remote: 
					{ 
						depends: function()
						{
							var spoc_num2 = $.trim($("#spoc_sec_num2").val());
							if(spoc_num2 == '') { return false; } else { return true; }
						},
						param: 
						{											
							url: "<?php echo site_url('organization/check_spoc_sec_num_exist') ?>", 
							type: "post", 
							async: false,
							data: { "spoc_num": function() { return $.trim($("#spoc_sec_num2").val()) }, "col_name": function() { return 'spoc_sec_num2'; } }
						}
					}
				},
				/* ### otp2: { digits:true, minlength:6, maxlength:6, custom_validate_otp2:true }, */
				
				org_logo: { <?php /*  if($mode == "Add") { ?>required: true, <?php } */ ?> valid_img_format: true, maxsize: 2000000 },
				pan_card: { <?php if($mode == "Add") { ?>required: true, <?php } ?>valid_img_pdf_format: true, maxsize: 5000000 },
				institution_reg_certificate: { <?php if($mode == "Add") { ?>required: true, <?php } ?> valid_img_pdf_format: true, maxsize: 5000000 },
				self_declaration: { <?php if($mode == "Add") { ?>required: true, <?php } ?> valid_img_pdf_format: true, maxsize: 5000000 },
				specialities_products: { required: true, nowhitespace: true },
				establishment_year: { required: true },
				institution_size: { digits:true, min: 1 },
				company_evaluation: { digits:true, min: 1 },
				website: { required: true, nowhitespace: true },
				linkedin_page: { required: true, nowhitespace: true },									
				address_proof: { valid_img_pdf_format: true, maxsize: 5000000 },
				gst_reg_certificate: { valid_img_pdf_format: true, maxsize: 5000000 }
			},
			messages:
			{
				'org_sector[]': { required: "Please select the Organization Sector" },
				org_sector_other: { required: "Please enter the Other Organization Sector", nowhitespace: "Please enter the Other Organization Sector" },
				overview: { required: "Please enter the Company Overview", nowhitespace: "Please enter the Company Overview" },
				spoc_bill_addr1: { required: "Please enter the Flat/House No/Building/Apt/Company", nowhitespace: "Please enter the Flat/House No/Building/Apt/Company" },
				spoc_bill_addr2: { required: "Please enter the Area/Colony/Street/Village", nowhitespace: "Please enter the Area/Colony/Street/Village" },
				spoc_bill_addr_city: { required: "Please enter the Town/City", nowhitespace: "Please enter the Town/City" },
				spoc_bill_addr_state: { required: "Please enter the State", nowhitespace: "Please enter the State" },
				spoc_bill_addr_country: { required: "Please enter the Country", nowhitespace: "Please enter the Country" },
				spoc_bill_addr_pin: { required: "Please enter the Pincode", nowhitespace: "Please enter the Pincode", digits: "Please enter only digits in Pincode" },
				
				spoc_sec_num_country_code1: { required: "Please select the Country Code" },
				
				spoc_sec_num1: { minlength: "Please enter minimum 8 numbers", maxlength: "Please enter maximum 10 numbers", min: "Please enter valid number", remote:"Number already exist" },
				otp1: { digits: "Please enter the only digits", required: "Please enter the OTP", minlength: "Please enter the 6 digit OTP", maxlength: "Please enter the 6 digit OTP", remote: "Please enter the Valid OTP", custom_validate_otp11: "" },
				spoc_sec_num_country_code2: { required: "Please select the Country Code" },
				spoc_sec_num2: { minlength: "Please enter minimum 8 numbers", maxlength: "Please enter maximum 10 numbers", min: "Please enter valid number", remote:"Number already exist" , notEqual:"Same number not allowed" },
				otp2: { digits: "Please enter the only digits", required: "Please enter the OTP", minlength: "Please enter the 6 digit OTP", maxlength: "Please enter the 6 digit OTP", remote: "Please enter the Valid OTP" },
				
				org_logo: { required: "Please upload the PAN Card", valid_img_format: "Please upload only image file", maxsize: "File size must not exceed 2MB" },
				pan_card: { required: "Please upload the PAN Card", valid_img_pdf_format: "Please upload only image or pdf file", maxsize: "File size must not exceed 5MB" },
				institution_reg_certificate: { required: "Please upload the Institution Registration Certificate", valid_img_pdf_format: "Please upload only image or pdf file", maxsize: "File size must not exceed 5MB"  },
				self_declaration: { required: "Please upload the Self Declaration for all of the above data", valid_img_pdf_format: "Please upload only image or pdf file", maxsize: "File size must not exceed 5MB"  },
				specialities_products: { required: "Please enter the Specialities & products", nowhitespace: "Please enter the Specialities & products" },
				establishment_year: { required: "Please select the Year of Establishment" },
				institution_size: { },
				company_evaluation: { },
				website: { required: "Please enter the Website", nowhitespace: "Please enter the Website" },
				linkedin_page: { required: "Please enter the Linked-In Page", nowhitespace: "Please enter the Linked-In Page" },
				board_of_directors: { },
				address_proof: { valid_img_pdf_format: "Please upload only image or pdf file", maxsize: "File size must not exceed 5MB"  },
				gst_reg_certificate: { valid_img_pdf_format: "Please upload only image or pdf file", maxsize: "File size must not exceed 5MB"  }
			},
			errorElement: 'span',
			errorPlacement: function (error, element) { element.closest('.form-group').append(error); },
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); }
		});
	});
</script>
		
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
		
<script>	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>	

			