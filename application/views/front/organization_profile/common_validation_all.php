
		<?php /* <script type="text/javascript" src="<?php echo base_url('assets/admin/plugins/jquery-validation/jquery.validate.js'); ?>"></script>*/ ?>
		<script> 
			$(document).ready(function() 
			{
				$.validator.addMethod("valid_img_format", function(value, element) 
				{ 
					if(value != "")
					{
						var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
						var fileExt = value.toLowerCase();
						fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
						if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
					}else return true;
				});
				
				$.validator.addMethod("valid_img_pdf_format", function(value, element) 
				{ 
					if(value != "")
					{
						var validExts = new Array(".png", ".jpeg", ".jpg", ".gif", ".pdf");
						var fileExt = value.toLowerCase();
						fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
						if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
					}else return true;
				});
			
				$.validator.addMethod("valid_email", function(value, element) 
				{
					if ($.trim(value) != '')
					{
						var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
						return pattern.test(value);
					} else { return true; }
				});
			
				$.validator.addMethod("lettersonly", function(value, element) { return this.optional(element) || /^[a-z]+$/i.test(value); }),
			
				$.validator.addMethod("letterswithspace", function(value, element) { return this.optional(element) || /^[a-zA-Z\s]+$/.test(value); }),
			
				$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
			
				$.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })// For chosen validation
			})
		</script>