<style>.details { color:#2196f3; } </style>

<div class="profileinfo" data-aos="fade-down">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="" class="img-fluid bor">
			</div>
			<div class="col-md-6">
				<h3 class="mt-4"><?php echo $personal_info['spoc_name']; ?></h3>
				<ul>
					<li><a href="javascript:void(0)"><i class="fa fa-paper-plane"></i> <?php echo $personal_info['email'] ?></a></li>
					<li><i class="fa fa-user"></i> <?php echo $personal_info['mobile'] ?></li>
				</ul>
				<div class="form-group upload-btn-wrapper2">
					<a class="btn btn-upload2" href="<?php echo site_url('organization/personalInfo'); ?>"><i class="fa fa-pencil"></i> Edit Personal Info</a>
					<a class="btn btn-upload2" href="<?php echo site_url('organization/profile'); ?>"><i class="fa fa-pencil"></i> Edit Profile</a>
				</div>
			</div>
			<?php /* <div class="col-md-4">
				<div class="countBox">
					<h4>02</h4>
					<span>Challenges</span>
				</div>
				<div class="countBox">
					<h4>50</h4>
					<span>Points</span>
				</div>
			</div> */ ?>
		</div>
	</div>
</div>

<section id="registration-form" class="inner-page" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div id="content" class="content content-full-width">
            <div class="profile-content">
							<div class="tab-content p-0">
								<div class="tab-pane fade in active show" id="profile-about">
									<div class="table-responsive col-9">
										<table class="table table-profile">
											<thead>
												<tr>
													<th colspan="2">
														<h4>Personal <small>Info</small></h4>
													</th>
												</tr>
											</thead>
											
											<tbody>
												<tr class="highlight">
													<td class="field">Spoc Name</td>
													<td class="details"> <?php echo $personal_info['spoc_name']; ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Gender</td>
													<td class="details"> <?php echo $personal_info['gender']; ?></td>
												</tr>																										 
												
												<tr>
													<td class="field">Spoc Mobile</td>
													<td class="details"><?php echo $personal_info['iso']." ".$personal_info['phonecode']." - ".$personal_info['mobile'] ?></td>
												</tr>
												<tr>
													<td class="field">Spoc Email</td>
													<td class="details"><?php echo $personal_info['email'] ?></td>
												</tr>
												<tr>
													<td class="field">User Category</td>
													<td class="details"><?php echo $personal_info['user_category'] ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">User Sub Category</td>
													<td class="details"><?php echo $personal_info['sub_catname'] ?></td>
												</tr>
												<?php /*<tr class="highlight">
													<td class="field">Institution Type</td>
													<td class="details"><?php echo $personal_info['institution_name'] ?></td>
												</tr> */ ?>
												<tr class="highlight">
													<td class="field">Institution Name</td>
													<td class="details"><?php echo $personal_info['institution_full_name'] ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Domain/ Industry & Sector</td>
													<td class="details"><?php echo $personal_info['domain_name'] ?></td>
												</tr>
												<tr class="highlight">
													<td class="field">Public/Private status</td>
													<td class="details"><?php echo $personal_info['public_status_name'] ?></td>
												</tr>
											</tbody>
											
											<thead>
												<tr>
													<th colspan="2">
														<h4>Profile <small>Details</small></h4>
													</th>
												</tr>
											</thead>
											<tbody>
												<tr class="highlight">
													<td class="field">Overview  </td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['overview'] != "") { echo $profile_info['overview']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Flat/House No/Building/Apt/Company</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr1'] != "") { echo $profile_info['spoc_bill_addr1']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Area/Colony/Street/Village</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr2'] != "") { echo $profile_info['spoc_bill_addr2']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Town/City</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_city'] != "") { echo $profile_info['spoc_bill_addr_city']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">State</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_state'] != "") { echo $profile_info['spoc_bill_addr_state']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Country</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_country'] != "") { echo $profile_info['spoc_bill_addr_country']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Pincode</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_pin'] != "") { echo $profile_info['spoc_bill_addr_pin']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Secondary Number1</td>
													<td class="details">
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code1'] != "") { echo $profile_info['spoc_sec_num_country_code1']; } ?>
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num1'] != "") { echo " - ".$profile_info['spoc_sec_num1']; } else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Secondary Number2</td>
													<td class="details">
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code2'] != "") { echo $profile_info['spoc_sec_num_country_code2']; } ?>
														<?php if(!empty($profile_info) && $profile_info['spoc_sec_num2'] != "") { echo " - ".$profile_info['spoc_sec_num2']; } else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Specialities & products</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['specialities_products'] != "") { echo $profile_info['specialities_products']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Year of Establishment</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['establishment_year'] != "") { echo $profile_info['establishment_year']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Institution Size</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['institution_size'] != "") { echo $profile_info['institution_size']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Company Evaluation</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['company_evaluation'] != "") { echo $profile_info['company_evaluation']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Website</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['website'] != "") { echo $profile_info['website']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Linked-In Page</td>
													<td class="details"><?php if(!empty($profile_info) && $profile_info['linkedin_page'] != "") { echo $profile_info['linkedin_page']; } else { echo '<span class="blank">?</span>'; } ?></td>
												</tr>
												
												<tr class="highlight">
													<td class="field">PAN Card</td> 
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['pan_card'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['pan_card']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['pan_card'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['pan_card']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Institution Registration Certificate</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['institution_reg_certificate'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['institution_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Self Declaration</td>
													<td class="details">
														<?php 
														if(!empty($profile_info) && $profile_info['self_declaration'] != "") 
														{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['self_declaration']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['self_declaration'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['self_declaration']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">?</span>'; } ?>	
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">Address Proof</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['address_proof'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['address_proof']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['address_proof'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['address_proof']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												
												<tr class="highlight">
													<td class="field">GST Registration Certificate</td>
													<td class="details">
														<?php 
															if(!empty($profile_info) && $profile_info['gst_reg_certificate'] != "") 
															{ ?>
															<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; ?>" target="_blank">
																<?php if(strtolower(pathinfo($profile_info['gst_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; } ?>
																<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
															</a>
														<?php	} else { echo '<span class="blank">?</span>'; } ?>
													</td>
												</tr>
												<?php if(empty($bod_info))
												{	?>
													<tr class="highlight">
														<td class="field">Board Of Directors</td>
														<td class="details">--</td>
													</tr>
										<?php	}
												else
												{	?>
												<tr class="highlight">
													<td class="field" colspan="2"><strong>Board Of Directors</strong></td>
												</tr>												
												<tr>
													<th colspan="2" style="padding-left: 0;padding-right: 0;">
														<table class="table table-bordered">
															<thead>
																<th class='text-center'>Sr. No</th>
																<th class='text-center'>Name</th>
																<th class='text-center'>Designation</th>
																<th class='text-center'>Since</th>
															</thead>
															<tbody>
																<?php if(empty($bod_info))
																	{	?>
																	<tr><td colspan='4' class='text-center'>No data available</td></tr>
																	<?php	}
																	else
																	{
																		$i=1;
																		foreach($bod_info as $bod_key => $bod_val)
																		{	?>
																		<tr>
																			<td class='text-center'><?php echo $i; $i++; ?></td>
																			<td><?php echo $bod_val['bod_name']; ?></td>
																			<td><?php echo $bod_val['bod_designation']; ?></td>
																			<td><?php echo $bod_val['bod_since']; ?></td>
																		</tr>
																		<?php }
																	}	?>
															</tbody>
														</table>
													</th>
												</tr>
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>