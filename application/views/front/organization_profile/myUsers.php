<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My Users</h1> 
		<?php /* <nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
			<li class="breadcrumb-item"><a href="<?php echo site_url('organization'); ?>">Manage Profile</a></li>
			<li class="breadcrumb-item active" aria-current="page">My Users </li>
			</ol>
		</nav> */ ?>
	</div>
</div>
<style>
ul.organization_promocode_listing { margin:0; padding:0; list-style:none; }
ul.organization_promocode_listing li { display: inline-block; background: #c80032; color: #fff; padding: 7px 25px; margin: 0 10px 5px 0; }
ul.organization_promocode_listing li::before { content: ""; }
</style>

<section id="registration-form" class="inner-page" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div id="content" class="content content-full-width">
            <div class="profile-content">
							<div class="tab-content p-0">
								<div class="tab-pane fade in active show" id="profile-about">
									<div class="table-responsive col-9">
										<table class="table table-profile">
											
											<?php if(!empty($org_promocode_data)) {	?>
												<thead>
													<tr>
														<th colspan="2">
															<h4>My Promo <small>Code<?php if(count($org_promocode_data) > 1) { echo "s"; } ?></small></h4>
														</th>
													</tr>
												</thead>
											
												<tbody>
													<tr>
														<td colspan="2">
															<ul class="organization_promocode_listing">
															<?php 
																foreach($org_promocode_data as $org_promocode)
																{	
																	echo "<li>".$encrypt_obj->decrypt($org_promocode['promo_code'])."</li>";
																}	
																?>
															</ul>
														</td>
													</tr>
												</tbody>
												
												<?php /* <tbody>
													<tr>
														<td colspan="2">
															<table>
																<thead>
																	<th>Sr. No.</th>
																	<th>Code</th>
																</thead>
																
																<tbody class='table table-bordered'>
																	<?php	$sr_no = 1;
																	foreach($org_promocode_data as $org_promocode)
																	{	?>
																		<tr>
																			<td class='text-center'><?php echo $sr_no; ?></td>
																			<td><?php echo $encrypt_obj->decrypt($org_promocode['promo_code']); ?></td>
																		</tr>
														<?php	$sr_no++;
																	}	?>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody> */ ?>
											<?php } ?>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>