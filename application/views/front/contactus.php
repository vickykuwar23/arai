<style>
#select2-country_code-container{padding-left:0}
.form-group.captcha p {
    color: red;
    font-size: 12px;
} 
</style>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $contact_data[0]['page_title']; ?></h1>
    </div>
    <!--/end container-->
</div>
    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">
               
                 <div class="formInfo">
                    <div class="row contact-info">

                        <?php echo $contact_data[0]['page_description']; ?>
                     

                        <div class="col-md-6 mt-5">
                        <?php if( $this->session->flashdata('success')){ ?>
                        <div class="alert alert-success alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h5><i class="icon fa fa-check"></i> Success!</h5>
                         <?php echo $this->session->flashdata('success'); ?>
                        </div>
                       <?php }  ?>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                            <div class="form">
                                <form name="contactfrm" id="contactfrm" method="post">
                                    <div class="form-group">
                                        <input type="text" id="cname" name="cname" class="form-control" >
                                        <label class="form-control-placeholder" for="cname">Your Name
                                            <em>*</em></label>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" id="email_id" name="email_id" class="form-control" >
                                        <label class="form-control-placeholder" for="cname">Your Email
                                            <em>*</em></label>
                                    </div>
																		<div class="row">
																		<div class="col-md-4">

									<div class="form-group">
                                        <select class="form-control select2" id="country_code" name="country_code" required=""><em class="mandatory">*</em>
										  <option value=""></option>
										   <?php foreach ($country_codes as $key => $code) { ?>
											 <option value="<?php echo $code['id'] ?>" <?php if($code['id']=='99'){ ?> selected="selected" <?php } ?>><?php echo $code['iso']." ".$code['phonecode'] ?></option>
										  <?php } ?>
									   </select>
                                        <label class="form-control-placeholder flotingCss leftLable2" for="cname">Country Code
                                            <em>*</em></label>
                                    </div>
                                    </div>
																		<div class="col-md-7">

                                    <div class="form-group">
                                        <input type="text" id="mobile" name="mobile" class="form-control" maxlength="10">
                                        <label class="form-control-placeholder" for="cname">Mobile No
                                            <em>*</em></label>
                                    </div>
                       </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="subject" name="subject" class="form-control" >
                                        <label class="form-control-placeholder" for="cname">Subject
                                            <em>*</em></label>
                                    </div>
                                    <div class="form-group mt-5">
                                        <textarea class="form-control"
                                            id="exampleFormControlTextarea1" name="contents"></textarea>
                                        <label class="form-control-placeholder"
                                            for="exampleFormControlTextarea1">Description <em>*</em></label>

                                    </div>
                                    <div class="row">
									<div class="col-md-6">
									 <div class="form-group">
									<span id="captImg"><?php echo $captchaImg; ?></span>
									<a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh" aria-hidden="true"></i></a>
									</div>
                  </div>
                
                  <div class="col-md-6">
                  <div class="form-group captcha">
                  <input type="text" name="captcha" value="" class="form-control"  style="margin-top:8px;" />
                  <label for="password" class="form-control-placeholder">Captcha<em class="mandatory">*</em></label>
									<?php echo form_error('captcha'); ?>
                        </div>
                        </div>
                        </div>
                                      <button type="submit" class="btn btn-primary">Send Message</button>
                               
                                </form>
                              
                        </div>
                      </div>
                      <div class="col-md-6 mt-5">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.1120585395597!2d73.81258171482816!3d18.523837587407034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bfa1b598d11d%3A0xc1341299016c69fa!2sAutomotive%20Research%20Association%20of%20India!5e0!3m2!1sen!2sin!4v1597228246512!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                              </div>
                     
                 </div>
    
                </div>
    
            </div>
        </div>
	</div>
    </section>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>

<script type="text/javascript"> 

  $(document).ready(function () {

	$.validator.addMethod("valid_email", function(value, element) 
	{ 
		var email = value;
		var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		var result = pattern.test(email);	
		if(result){return true;}else {return false;}
	});
		
  
     $('#contactfrm').validate({
       rules: {
             cname: {
               required: true,
               minlength:2             
             },
             email_id: {
               required: true,
			   valid_email:true,
               email:true
             },
             subject: {
               required: true
             },
             contents: {
               required: true
             },
             mobile: {
               required: true,
               number: true,
               minlength:10
             },
			 country_code: {
               required: true
             },
			 captcha:{
				required: true 
			 }
           },
       messages: {
           
             cname: {
               required: "This field is required"
             },
             email_id: {
               required: "This field is required",
			   valid_email:"Please enter valid email address.",
             },
             subject: {
               required: "This field is required"
             },
             contents: {
               required: "This field is required"
             },
             mobile: {
               required: "This field is required"
             },
			 country_code:{
				required: "This field is required" 
			 },
			 captcha:{
				required: "This field is required" 
			 }
               
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
       },
       highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
       },
       unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
       }
     });
   });

$('.select2').select2({
	});
</script>
<script>
   $(document).ready(function(){
	   $('.refreshCaptcha').on('click', function(){
		   $.get('<?php echo base_url().'xAdmin/admin/refresh'; ?>', function(data){
			   $('#captImg').html(data);
		   });
	   });
   });
</script>

