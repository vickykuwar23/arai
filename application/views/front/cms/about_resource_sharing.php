<div id="home-p" class="home-p expertConnect text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $cms_data[0]['page_title'] ?></h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $cms_data[0]['page_title'] ?></li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">		   
				<div class="formInfo">
					<?php echo $cms_data[0]['page_description'] ?>
				</div>
			</div>
		</div>
        <?php  if($cms_data[0][0]['video_url']!=''){ ?>
        <div class="row">
            <div class="col-md-12">        
               <iframe width="420" height="315" src="<?php echo $cms_data[0][0]['video_url'] ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <?php } ?>
	</div>
</section>
<?php
if (count($consortium_list) > 0) {
?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Technology Platform Partners</h2>
                    <div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
                <?php foreach ($consortium_list as $consrotium) : ?>
                    <div class="col-sm-12"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></div>
                <?php endforeach; ?>
			</div>
			</div>
        </div>
    </section>
<?php } ?>
<section class="ministrylogo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center title-bar">
                <h4>Under Aegis of</h4>
                <div class="heading-border"></div>
                <img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo">
            </div>
        </div>
    </div>
</section>
<!--Client Logo Section Ends-->



