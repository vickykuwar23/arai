<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $privacy_data[0]['page_title'] ?></h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $privacy_data[0]['page_title'] ?></li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">		   
				<div class="formInfo">
					<?php echo $privacy_data[0]['page_description'] ?>
				</div>
			</div>
		</div>
	</div>
</section>



