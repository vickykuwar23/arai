<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Featured Challenge List</h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Featured Challenge</li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<section id="story" data-aos="fade-up" class="pt-5 pb-5">
    
	<div class="filterBox"></div>

    <div class="container">
       
      <div class="row">
		<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

        <!-- <div class="col-md-12">
          <h1 class="wow fadeInUp">featured challenges</h1>
          <div class="heading-border"></div>
        </div> -->
      </div>
      <div class="sortable-masonry">

		<div class="filters">
          
        </div>
        <div class="row items-container clearfix challengeCardsList">
			<?php
				//echo ">>>>".count($total);
				if(count($challenge_data) > 0 )
				{

					foreach($challenge_data as $challenge){
							
						$limit = 200;
						 if (strlen($challenge['challenge_details'] ) > $limit){
							$challenge['challenge_details']  = substr($challenge['challenge_details'] , 0, strrpos(substr($challenge['challenge_details'] , 0, $limit), ' ')) . '...'; 
						 }
						 
						 $limit2 = 48;
						 if (strlen($challenge['challenge_title'] ) > $limit2){
							$challenge['challenge_title']  = substr($challenge['challenge_title'] , 0, strrpos(substr($challenge['challenge_title'] , 0, $limit2), ' ')) . '...'; 
						 }
						
						$datestr = $challenge['challenge_close_date'];//Your date
						$date	 = strtotime($datestr);//Converted to a PHP date (a second count)

						//Calculate difference
						$diff	= 	$date-time();//time returns current time in seconds
						$days	=	floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
						//$hours	=	round(($diff-$days*60*60*24)/(60*60));
						if($days > 0){
							$showCnt = "Days left : ".$days;
						} else {
							$showCnt = "Closed Paticipations";
						}	
						
						$fileExist = base_url('assets/challenge/'.$challenge['banner_img']);
						
						if ($challenge['banner_img']!="") {
							$image = $fileExist;
						} else {
							$image = base_url('assets/news-14.jpg');
						}
						
			?>
			<div class="col-md-4 col-sm-6 col-xs-12 masonry-item all p2 p4 p1 p3" onclick="document.location.href='<?php echo base_url('challenge/challengeDetails/'.base64_encode($challenge['c_id'])); ?>'">
				<a href="<?php echo base_url('challenge/challengeDetails/'.base64_encode($challenge['c_id'])); ?>" ><div class="desc-comp-offer-cont">
					<div class="thumbnail-blogs">
						<div class="caption">
							<i class="fa fa-chain"></i>
						</div>
						<img src="<?php echo $image; ?>" class="img-fluid" alt="<?php echo $challenge['challenge_title'] ?>" style="">
					</div>
					<div class="card-content">
						<h5><?php echo $challenge['company_name'] ?></h5>
						<h3><?php echo $challenge['challenge_title'] ?></h3>
						<p class="desc"><?php echo $challenge['challenge_details'] ?></p>
						<div class="cardd-footer">
							<div>
								<?php 						
									$audience = explode(",", $challenge['audience_pref']);
									foreach($audience as $audi){
								?>
									<button class="btn btn-general btn-green-fresh remove-h" role="button"><?php echo $audi; ?></button>
								<?php } ?>
							</div>
							
							<div class="daysleft"><i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></div>
						</div>
					</div>
				</div></a>
			</div> 
			<?php } // Foreach End ?>	
				
				<div class="show_more_main col-md-12" id="show_more_main<?php echo $challenge['c_id']; ?>">
					<span id="<?php echo $challenge['c_id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>
				
				
		<?php	} else {  ?>
		<p style="font-weight:bold; text-align:center; width:100%;"> No Record Found</p>
		<?php } ?>
        </div>
      </div>
    </div>
  </section>


<script type="text/javascript">
	function get_more_apply_css()
	{
		$('.thumbnail-blogs').hover(
			function() {
				$(this).find('.caption').slideDown(250)
			},
			function() {
				$(this).find('.caption').slideUp(205)
			}
			);
	}
	

   $(document).ready(function () {    
	 
	 $(document).on('click','.show_more',function(){
		// $(".token").remove();
		var base_url = '<?php echo base_url(); ?>'; 
        var ID = $(this).attr('id');
		var length = $('.p3').length;
		//alert(length);
		var cs_t = 	$('.token').val();
        $('.show_more').hide();
        $('.loding').show();
        $.ajax({
            type:'POST',
            url: base_url+'home/featuredlist',
            data:'id='+length+'&csrf_test_name='+cs_t,
			dataType:"text",
            success:function(data){
				//console.log(data);
				var output = JSON.parse(data);
				if(output.html == ""){
					$(".show_more_main").remove();
				}	
				$(".token").val(output.token);								
                $('#show_more_main'+ID).remove();				
				$('.challengeCardsList').append(output.html);
				get_more_apply_css();		
            }
        });
    });
	 
   });
</script>

