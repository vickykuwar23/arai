<?php 
	/* echo $featured_webinar_qry; */
	if(count($featured_webinar) > 0)
	{ ?>								
	<div class="formInfoNew">
		<div class="owl-carousel mb-2 owl-theme" id="FeaturedWebinarsSlider">
			<?php foreach($featured_webinar as $featuredSlide)
				{	 ?>
				<div class="owner-teammember inner-img">
					<div class="boxShadow">
						<a href="<?php echo base_url('webinar/viewDetail/'.base64_encode($featuredSlide['w_id'])); ?>"><img src="<?php echo base_url('assets/webinar/'.$featuredSlide['banner_img']); ?>" alt="experts" style="height: 196px!important;"></a>
						<div class="borderBoxNewClass">
							<h3><?php echo character_limiter($featuredSlide['webinar_name'], 25); ?></h3>
							<ul>
								<li>
									<i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("jS M Y", strtotime($featuredSlide['webinar_date'])) ?>
								</li>
								<li>|</li>
								<li>
									<i class="fa fa-clock-o" aria-hidden="true"></i> <?php
										echo date("h:i A", strtotime($featuredSlide['webinar_start_time']))." - ".date("h:i A", strtotime($featuredSlide['webinar_end_time'])); ?> 
								</li>
							</ul>
						</div>
					</div>
				</div>
			<?php } ?>	
		</div>
	</div>
	
	<script>
		$('#FeaturedWebinarsSlider').owlCarousel(
		{ 
			autoplay: true,
			smartSpeed: 700,
			loop: false,
			nav: true,
			dots: false,
			navText: ["", ""],
			autoplayHoverPause: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				768: {
					items: 2
				},
				992: {
					items: 3
				}
			}
		});
	</script>
	<?php } 
	else 
	{ ?>	
	<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;">No Featured Webinar Available</p></div>
<?php } ?>