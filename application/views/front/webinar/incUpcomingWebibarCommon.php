<?php if(count($upcoming_webinar) > 0)
{
	foreach($upcoming_webinar as $commingWebinar)
	{ ?>
		<div class="col-md-4 upcoming-webinar">
			<div class="boxShadow">
				<a href="<?php echo base_url('webinar/viewDetail/'.base64_encode($commingWebinar['w_id'])); ?>"><img src="<?php echo base_url('assets/webinar/'.$commingWebinar['banner_img']); ?>" class="img-fluid" alt="experts" style="height: 196px!important;"></a>
				<div class="borderBoxNewClass">
					<h3><?php echo character_limiter($commingWebinar['webinar_name'], 25); ?></h3>
					<ul>
						<li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("jS M Y", strtotime($commingWebinar['webinar_date'])) ?></li>
						<li>|</li>
						<li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
						echo date("h:i A", strtotime($commingWebinar['webinar_start_time']))." - ".date("h:i A", strtotime($commingWebinar['webinar_end_time'])); ?></li>
					</ul>
				</div>
			</div>
		</div>
	<?php } // Foreach End 
	
	if($new_start < $total_upcoming_webinar_count)
	{ ?>
		<div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtnUpcoming">
			<a href="javascript:void(0)" class="click-more" onclick="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $upcoming_webinar_limit; ?>', 0, 1)">Show More</a>				
		</div>
<?php }
} 
else 
{ ?>
	<div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;">No Upcoming Webinar Available</p></div>
<?php } ?>	