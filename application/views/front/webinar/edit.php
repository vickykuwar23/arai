<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.datepicker table tr td, .datepicker table tr th { border-radius: 0 !important; border: 1px solid rgba(0,0,0,0.1) !important; }
	.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover { background: rgba(0,0,0,0.1) !important; color: rgba(0,0,0,0.4) !important; cursor: not-allowed !important; border: 1px solid #fff; }
	th.datepicker-switch, th.next, th.prev { border: none !important; }

	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
	cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
	/**
	* Tooltip Styles
	*/
	
	/* Add this attribute to the element that needs a tooltip */
	[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
	}
	
	/* Hide the tooltip content by default */
	[data-tooltip]:before,
	[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
	}
	
	/* Position tooltip above the element */
	[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
	}
	
	/* Triangle hack to make tooltip look like a speech bubble */
	[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
	}
	
	/* Show tooltip content on hover */
	[data-tooltip]:hover:before,
	[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
	}
	
	.form-control-placeholder{top: -14px !important;}
	textarea.error {color: #000000!important;}
	input.error{color:#000000!important;}
</style>

<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css"> */ ?>
<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/css/') ?>jquery.datetimepicker.min.css"> */ ?>
<?php /* <script src="<?php echo base_url('assets/front/js/') ?>jquery.datetimepicker.js"></script> */ ?>

<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Edit Webinar</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('webinar') ?>">Webinar</a></li>
				<li class="breadcrumb-item active" aria-current="page">Edit Webinar </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="webinarform" name="webinarform" enctype="multipart/form-data">			
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_id" id="webinar_id" value="<?php echo $webinar_data[0]['webinar_id']; ?>"  readonly >
									<label class="form-control-placeholder floatinglabel">Webinar ID <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_name" id="webinar_name" value="<?php echo $webinar_data[0]['webinar_name']; ?>"  >
									<label class="form-control-placeholder floatinglabel">Webinar's Name <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control datepicker" name="start_date" id="start_date" readonly value="<?php echo date('d-m-Y', strtotime($webinar_data[0]['webinar_date'])); ?>"  >
									<label class="form-control-placeholder floatinglabel" style="top:-13px!important;">Date <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control clockpicker" name="start_time" id="start_time" value="<?php echo date('h:iA', strtotime($webinar_data[0]['webinar_start_time'])); ?>" readonly>
									<label class="form-control-placeholder floatinglabel" style="top:-10px!important;">Start Time <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control clockpicker" name="end_time" id="end_time" value="<?php echo date('h:iA', strtotime($webinar_data[0]['webinar_end_time'])); ?>" readonly>
									<label class="form-control-placeholder floatinglabel" style="top:-10px!important;">End Time <em>*</em></label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="margin: 0 0 15px 0;">	
								<label class="form-control-placeholder floatinglabel">Type of Registration <em>*</em></label>
							</div>
						</div>
						
						<div class="row">							
							<div class="col-md-4" style="margin-bottom:20px">									
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type1" name="cost_type" class="custom-control-input get-type" value="FREE" onchange="show_hide_div()" <?php if($webinar_data[0]['webinar_cost_type'] == 'FREE'){?> checked="checked"  <?php } ?>>
									<label class="custom-control-label" for="upload_type1">Free</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type2" name="cost_type" class="custom-control-input get-type" value="PAID" onchange="show_hide_div()" <?php if($webinar_data[0]['webinar_cost_type'] == 'PAID'){?> checked="checked"  <?php } ?>>
									<label class="custom-control-label" for="upload_type2">Paid</label>
								</div>								
								<div id="upload_type_err"></div>
							</div>
							
							<div class="col-md-8" style="margin-top:8px;">								
								<div class="row" id="paid-amt"><br />
									<div class="col-md-3" style="margin-top: -25px;">
										<div class="form-group">
											<select name="currency" id="currency" class="form-control">
												<?php /* <option value="USD" disabled>$</option>
												<option value="EUR" disabled>€</option>
												<option value="CRC" disabled>₡</option>
												<option value="GBP" disabled>£</option>
												<option value="JPY" disabled>¥</option>
												<option value="ILS" disabled>₪</option>	*/ ?>
												<option value="INR (₹)">INR (₹)</option>
											</select>
											<label class="form-control-placeholder floatinglabel">Currency </label>
										</div>
									</div>
									<div class="col-md-9" style="margin-top: -25px;">
										<div class="form-group">
											<input type="text" class="form-control allowd_only_float" name="cost_price" id="cost_price" value="<?php echo $webinar_data[0]['cost_price']; ?>"  >
											<label class="form-control-placeholder floatinglabel">Price <em>*</em></label>
										</div>
									</div>
								</div>
							</div>	
						</div>	
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Webinar Banner </button>
									<input type="file" class="form-control" name="banner_img" id="banner_img"  value="">
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>											
							</div>	
							
							<?php
								$dynamic_cls = 'd-none';
								$full_img = $onclick_fun = '';
								if( $webinar_data[0]['banner_img'] != '' && file_exists('./assets/webinar/'.$webinar_data[0]['banner_img']))
								{	
									$dynamic_cls = '';
									$full_img = base_url().'assets/webinar/'.$webinar_data[0]['banner_img'];
									$onclick_fun = "remove_banner_img('1','".$encrypt_obj->encrypt('arai_webinar')."', '".$encrypt_obj->encrypt('w_id')."', '".$encrypt_obj->encrypt($webinar_data[0]['w_id'])."', 'banner_img', '".$encrypt_obj->encrypt('./assets/webinar/')."', 'Webinar Banner')";
								} ?>
								
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="banner_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
											<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
										</div>
									</div>
								</div>
							</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Webinar Technology <em>*</em></label>
									<div class="boderBox65x">
										<select data-key="" class="form-control select2_common cls_team_skillset" name="webinar_technology[]" id="webinar_technology" data-placeholder="Webinar Technology" multiple onchange="show_hide_webinar_technology_other()">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														if($webinar_data[0]['webinar_technology'] != "") { $webinar_technology_arr = explode(",",$webinar_data[0]['webinar_technology']); } else { $webinar_technology_arr = array(); } ?>
															<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$webinar_technology_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
											<?php }	
													} ?>
										</select> 																	
									</div>
								</div>
							</div>
							
							<div class="col-md-6" id="webinar_technology_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_technology_other" id="webinar_technology_other" value="<?php echo $webinar_data[0]['webinar_technology_other']; ?>" required>
									<label class="form-control-placeholder floatinglabel">Webinar Technology Other <em>*</em></label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="margin-bottom: 15px;margin-top: 10px;">		
								<label class="form-control-placeholder floatinglabel">Exclusive Technovuus Event <em>*</em></label>
								<div style="margin-top:15px;">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="exclusive_technovuus_event1" name="exclusive_technovuus_event" class="custom-control-input get-type" value="Yes" onchange="show_hide_reg_link()" <?php if($webinar_data[0]['exclusive_technovuus_event'] == 'Yes'){?> checked="checked"  <?php } ?>>
										<label class="custom-control-label" for="exclusive_technovuus_event1">Yes</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="exclusive_technovuus_event2" name="exclusive_technovuus_event" class="custom-control-input get-type" value="No" onchange="show_hide_reg_link()" <?php if($webinar_data[0]['exclusive_technovuus_event'] == 'No'){?> checked="checked"  <?php } ?>>
										<label class="custom-control-label" for="exclusive_technovuus_event2">No</label>
									</div>
								</div>
								<div id="exclusive_technovuus_event_err"></div>
							</div>
							
							<div class="col-md-12" id="reg_link_outer">
								<div class="form-group">
									<textarea class="form-control" name="registration_link" id="registration_link" style='word-break: break-all; min-height: 95px;'><?php echo $webinar_data[0]['registration_link']; ?></textarea>
									<label class="form-control-placeholder floatinglabel">Registration Link <em>*</em>  <a href="javascript:void(0)" data-tooltip="Host to give info of webinar platform and also provide link for the same"><i class="fa fa-info-circle"></i></i></a></label>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control" name="hosting_link" id="hosting_link" style='word-break: break-all; min-height: 95px;'><?php echo ($webinar_data[0]['hosting_link']); ?></textarea>
									<label class="form-control-placeholder floatinglabel">Hosting Link <em>*</em></label>									
								</div>
							</div>
						
							<!----<div class="col-md-12" id="uploadImg">	
								<div class="file-details" style="min-height:100px;">
								<input type="hidden" name="customFileCount" id="customFileCount" value="0">
								<div class="form-group file-remove" id="row_files_0">
								<div class="row">										
								<div class="col-md-12">
								<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload0" />
								</div>
								</div>
								</div>
								
								<div id="last_team_file_id"></div>
								<div class="row">
								<div class="col-md-12">
								<button type="button" class="custom-file-upload mt-3 btn btn-primary" onclick="append_image_files_row()"><i class="fa fa-plus-circle"></i> Add File</label>
								</div>
								</div>
								</div><br /><br />
							</div>-->
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="breif_desc" id="breif_desc"><?php echo $webinar_data[0]['webinar_breif_info']; ?></textarea>
											<label class="form-control-placeholder floatinglabel" for="breif_desc">Brief About Webinar <em>*</em></label>
											<span class="error"><?php echo form_error('breif_desc'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="key_points" id="key_points"><?php echo $webinar_data[0]['webinar_key_points']; ?></textarea>
											<label class="form-control-placeholder floatinglabel" for="key_points">Key Takeaway Points <em>*</em></label>
											<span class="error"><?php echo form_error('key_points'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="about_author" id="about_author"><?php echo $webinar_data[0]['webinar_about_author']; ?></textarea>
											<label class="form-control-placeholder floatinglabel" for="about_author">About Author<em>*</em></label>
											<span class="error"><?php echo form_error('about_author'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							
							<div class="col-md-12" ><br />
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary btnCancelCustom">Cancel</a> 
								<button type="submit" class="btn btn-primary add_button float-right">Submit</button> 
							</div>	
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.js'); ?>"></script>

<script>	
	$('.clockpicker').clockpicker(
	{
    /* placement: 'top',
		autoclose:true,
    align: 'left',*/
		twelvehour: true,
		donetext: 'Done' 
	});
	
	$('.select2_common').select2();
	
	function show_hide_webinar_technology_other()
	{
		$("#webinar_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#webinar_technology').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#webinar_technology_other_outer").show();
		}
		else
		{
			$("#webinar_technology_other_outer").hide();
			$("#webinar_technology_other").val('');
		}
	}
	show_hide_webinar_technology_other();
	
	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//CURRENTLY NOT IN USE
	/*function append_invite_row()
		{
		var total_div_cnt = $('input[name*="youtube_link[]"]').length;
		if(total_div_cnt >= 100)
		{
		swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" youtube link", type: "warning" });
		}
		else
		{		
		var row_cnt = $("#invite_row_cnt").val();
		var row_cnt_new = parseInt(row_cnt)+1;
		
		var append_html = '';	
		append_html += '	<div class="row vid-remove" id="byt_invite_row_'+row_cnt_new+'">';
		append_html += '		<div class="col-md-11">';
		append_html += '			<div class="form-group upload-btn-wrapper" style="margin-top:0">';			
		append_html += '				<input type="text" class="form-control youtube_input video_link" name="youtube_link[]" id="youtube_link'+row_cnt_new+'" />';
		append_html += '			</div>';
		append_html += '		</div>';
		append_html += '		<div class="col-md-1">';
		append_html += '			<div class="form-group" style="margin-top:0">';
		append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
		append_html += '			</div>';
		append_html += '		</div>';
		append_html += '	</div>';
		
		$("#invite_row_cnt").val(row_cnt_new);
		$(append_html).insertBefore("#byt_invite_last");
		}
		}
		
		function remove_current_div_invite(div_no)
		{
		swal(
		{
		title:"DELETE?" ,
		text: "Are you confirm to delete this row?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
		if (result.value) 
		{
		$("#byt_invite_row_"+div_no).remove();
		}
		});	
		}
		
		
		// DOCUMENT UPLOAD
		function append_doc_files_row()
		{
		var total_div_cnt = $('input[name*="doc_file[]"]').length;
		if(total_div_cnt >= 100)
		{
		swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
		var row_cnt = $("#docFileCount").val();
		var row_cnt_new = parseInt(row_cnt)+1;
		
		var append_html = '';	
		append_html += '	<div class="form-group" id="row_doc_'+row_cnt_new+'">';
		append_html += '		<div class="row">';
		append_html += '			<div class="col-md-11">';
		append_html += '				<input type="file" class="form-control doc_file doc_upload" name="doc_file[]" id="doc-upload'+row_cnt_new+'" />';
		append_html += '			</div>';
		append_html += '			<div class="col-md-1">';
		append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_doc_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
		append_html += '			</div>';
		append_html += '		</div>';
		append_html += '	</div>';
		
		$("#docFileCount").val(row_cnt_new);
		$(append_html).insertBefore("#last_doc_file_id");
		
		$('input[type="file"]').change(function(e)
		{              
		var fileName = e.target.files[0].name;
		$(this).parent().find('input').next("span").remove();
		$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
		});
		}
		}
		
		function remove_doc_div(div_no)
		{
		swal(
		{
		title:"DELETE?" ,
		text: "Are you sure you want to delete this entry?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
		if (result.value) 
		{
		$("#row_doc_"+div_no).remove();
		}
		});	
		}
		
		
		function append_image_files_row()
		{
		var total_div_cnt = $('input[name*="closer_file[]"]').length;
		if(total_div_cnt >= 100)
		{
		swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
		var row_cnt = $("#customFileCount").val();
		var row_cnt_new = parseInt(row_cnt)+1;
		
		var append_html = '';	
		append_html += '	<div class="form-group file-remove" id="row_files_'+row_cnt_new+'">';
		append_html += '		<div class="row">';
		append_html += '			<div class="col-md-11">';
		append_html += '				<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload'+row_cnt_new+'" />';
		append_html += '			</div>';
		append_html += '			<div class="col-md-1">';
		append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_files_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
		append_html += '			</div>';
		append_html += '		</div>';
		append_html += '	</div>';
		
		$("#customFileCount").val(row_cnt_new);
		$(append_html).insertBefore("#last_team_file_id");
		
		$('input[type="file"]').change(function(e)
		{              
		var fileName = e.target.files[0].name;
		$(this).parent().find('input').next("span").remove();
		$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
		});
		}
		}
		
		
		function remove_files_div(div_no)
		{
		swal(
		{
		title:"DELETE?" ,
		text: "Are you sure you want to delete this entry?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
		if (result.value) 
		{
		$("#row_files_"+div_no).remove();
		}
		});	
		}
		
		//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
		function TeamFilesPreview(input, public_private_flag) 
		{
		console.log(public_private_flag);
		if (input.files && input.files[0]) 
		{
		$("#preloader").css("display", "block");
		
		if(public_private_flag == 'public')
		{			
		var customFileCount = $("#customFileCount").val();
		var disp_filename_final = '';
		var disp_filename = input.files[0].name;
		var reader = new FileReader();
		var j = customFileCount;
		}
		
		
		var disp_img_name = "";
		var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
		disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
		
		var id_name = '';
		var btn_id_name = '';
		var input_cls_name = '';
		var input_type_name = '';
		var fun_var_parameter = "";
		var file_upload_common = '';
		if(public_private_flag == 'public')
		{
		id_name = 'team_files_outer'+j;
		btn_id_name = 'addFileBtnOuter';
		input_cls_name = 'fileUploadTeams';
		input_type_name = 'closer_file[]';
		fun_var_parameter = "'public'";
		file_upload_common = 'file-upload';
		}			
		
		var append_str = '';
		append_str += '	<div class="col-md-2 team_files_outer_common" id="'+id_name+'">';
		append_str += '		<div class="file-list">';
		append_str += '			<a href="javascript:void(0)" onclick="remove_team_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
		//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
		append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
		append_str += '		</div>';
		append_str += '	</div>';				
		
		var btn_str = '';
		btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
		btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
		btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="TeamFilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
		btn_str +=	'	</div>';
		
		if(public_private_flag == 'public')
		{
		$("#addFileBtnOuter").addClass('d-none');
		$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
		$("#addFileBtnOuter").removeAttr( "id" );
		
		$(append_str).insertBefore("#last_team_file_id");
		$(btn_str).insertAfter("#last_team_file_id");
		$("#customFileCount").val(parseInt(customFileCount)+1);
		}
		
		
		$("#preloader").css("display", "none");
		}
		}
		$(".fileUploadTeams").change(function() { TeamFilesPreview(this,'public'); });	
		
		
		function show_hide_div()
		{
		$('#uploadImg').hide();
		$('#uploadVideo').hide();
		
		var values = $("input[name='upload_type']:checked").val();
		if(values == "Image")
		{
		$('.vid-remove:not(:first)').remove();
		$('#customFileCount').val(0);
		$("#youtube_link1").val('');
		$('#uploadImg').show();
		$('#uploadVideo').hide();
		}
		else if(values == "Video")
		{
		$('.file-remove:not(:first)').remove();
		$('#invite_row_cnt').val(1);
		$("#file-upload0").val(null);
		$("span.disp_filename").remove();
		$('#uploadVideo').show();
		$('#uploadImg').hide();
		}
		}
	show_hide_div();*/
	
	function show_hide_div()
	{
		
		$('#paid-amt').hide();
		
		var values = $("input[name='cost_type']:checked").val();
		if(values == "PAID")
		{
			$('#paid-amt').show();
		}
		else 
		{			
			$('#paid-amt').hide();
			$("#cost_price").val(null);			
		}
	}
	show_hide_div();
	
	function show_hide_reg_link()
	{		
		$('#reg_link_outer').hide();
		
		var values = $("input[name='exclusive_technovuus_event']:checked").val();
		if(values == "No")
		{
			$('#reg_link_outer').show();
		}
		else 
		{			
			$('#reg_link_outer').hide();
			$("#reg_link_outer").val("");			
		}
	}
	show_hide_reg_link();
	
	/*var FromDateFun = $('#start_date').datepicker(
		{               
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy"              
		
	});*/
	
	
	
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	function Converttimeformat(time) 
	{
		// var time = $("#starttime").val();
		//var time = document.getElementById('txttime').value;
		var hrs = Number(time.match(/^(\d+)/)[1]);
		var mnts = Number(time.match(/:(\d+)/)[1]);
		var format = time.match(/\s(.*)$/)[1];
		if (format == "PM" && hrs < 12) hrs = hrs + 12;
		if (format == "AM" && hrs == 12) hrs = hrs - 12;
		var hours = hrs.toString();
		var minutes = mnts.toString();
		if (hrs < 10) hours = "0" + hours;
		if (mnts < 10) minutes = "0" + minutes;
		return (hours + ":" + minutes);
	}
	//Converttimeformat('10:00 PM');
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		
		//TimePicke Example
		/* $('.datetimepicker1').datetimepicker({
			datepicker:false,
			formatTime:"h:i a",
			format:"h:i a",
			step:15
		}); */
		
		
		var date = new Date();
		$('#start_date').datepicker({
			format: 'dd-mm-yyyy',
			startDate: date,
			autoclose: true,
			ignoreReadonly: true,
			minDate:new Date(),
			endDate:"<?php echo date('d-m-Y', strtotime('+365days', strtotime(date('d-m-Y')))); ?>"
		});
		
		//$('#start_time').pickatime({});
		
		var selectedType = '<?php echo $webinar_data[0]['webinar_cost_type']; ?>';
		if(selectedType == 'PAID'){
			$('#paid-amt').show();
		}
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		// Using Class Add Validation To array Image upload
		$.validator.addMethod("image_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		$.validator.addClassRules("image_file", { image_required: true, valid_img_format:true, filesize:2000000 });
		
		// Textbox Validation required 
		$.validator.addMethod("youtube_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		// Youtube URL Validation
		$.validator.addMethod("youtube_input_valid", function(value, element) 
		{
			var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Please enter valid youtube url.");	
		
		$.validator.addClassRules("youtube_input", { youtube_required: true, youtube_input_valid: true });
		
		
		// ALPHANUMERIC Validation
		/*$.validator.addMethod("alphanumeric", function(value, element) 
			{
			var p = /^[A-Za-z0-9 _.-]+$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Only Alphabets and Numbers allowed");*/
		
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		$.validator.addMethod("chk_valid_time", function(value, element)
			{
				if($.trim(value).length == 0)
				{
					return true;
				}
				else
				{
					var startTime = $.trim($("#start_time").val());
					var endTime = $.trim(value);
					
					if(startTime.length == 0)
					{
						return true;
					}
					else
					{
						startTime = startTime.replace("AM", " AM");
						startTime = startTime.replace("PM", " PM");
						startTime = Converttimeformat(startTime);
						
						endTime = endTime.replace("AM", " AM");
						endTime = endTime.replace("PM", " PM");
						endTime = Converttimeformat(endTime);
						
						//console.log(startTime+" >> "+endTime);
						if(startTime > endTime) { return false; }
						else { return true; }
						/* return false; */
					}
				}
			}, 'Please enter valid time');
		
		$.validator.addMethod("valid_url", function(value, element) {
			return this.optional(element) || /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i.test(value);
		}, "Please enter valid url");
		
		// Doc Validation .png, jpeg, jpg etc.
		$.validator.addMethod("doc_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".txt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		}, 'Please upload only .pdf, .xls, .xlsx, .doc, .docx, .ppt, .txt file format');
		
		$.validator.addClassRules("doc_upload", { doc_format:true, filesize:2000000 });
		
		//******* JQUERY VALIDATION *********
		$("#webinarform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			rules:
			{
				webinar_name: { required: true, nowhitespace: true, valid_value: true, maxCount:['50'] },
				start_date: { required: true},
				start_time: { required: true},
				end_time: { required: true, chk_valid_time:true },
				upload_type: { required: true},
				cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },
				banner_img: { valid_img_format: true, filesize:2000000 },
				"webinar_technology[]": { required: true },
				exclusive_technovuus_event:{required: true },
				registration_link:{required: true,valid_url:true },
				hosting_link:{required: true,valid_url:true },
				breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
				key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
			},
			messages:
			{
				webinar_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				start_date:{required: "This field is required"},
				start_time:{required: "This field is required"},
				end_time:{required: "This field is required"},
				upload_type:{required: "This field is required"},
				cost_price:{required: "This field is required"},
				banner_img:{valid_img_format: "Please upload only .png, .jpeg, .jpg, .gif file format"},
				exclusive_technovuus_event:{required: "This field is required"},
				registration_link:{required: "This field is required", nowhitespace: "This field is required"},
				hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
				breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
				key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}							
			},
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "cost_type") 
				{
					error.insertAfter("#upload_type_err");
				}
				else { element.closest('.form-group').append(error); }
			}
		});
	});
</script>

<script>
	function remove_banner_img(flag,table_name,pk_id,del_id,input_name,path)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the banner image?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				if(flag == 0)
				{
					$('#banner_img').val('');
					$("#banner_img").parent().find('input').next("span").remove();
				}
				else
				{
					var csrf_test_name = $("#csrf_token").val();
					var data = { 
						'tbl_nm': table_name, 
						'pk_nm': pk_id, 
						'del_id': del_id, 
						'input_nm': input_name, 
						'file_path': path, 
						'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
					};          
					$.ajax(
					{ 
						type: "POST", 
						url: '<?php echo site_url("delete_single_file") ?>', 
						data: data, 
						dataType: 'JSON',
						success:function(data) 
						{ 
							$("#csrf_token").val(data.csrf_new_token);
							/* $("#"+input_nm+"_outer").remove();
								
							swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" }); */
						}
					});
				}
				
				$("#banner_img_outer").addClass('d-none');
				$("#banner_img_outer .previous_img_outer img").attr("src", "");
				$("#preloader").css("display", "none");
			}
		});
	}
	
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BannerPreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#banner_img_outer").removeClass('d-none');
					//$("#banner_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#banner_img_outer .previous_img_outer img").attr("src", e.target.result);
					$("#banner_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#banner_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#banner_img").change(function() { BannerPreview(this); });		
	</script>		