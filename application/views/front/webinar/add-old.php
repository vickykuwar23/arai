<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
    cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	
</style>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<link rel="stylesheet" href="<?php echo base_url('assets/front/css/') ?>jquery.timepicker.min.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('assets/front/js/') ?>jquery.timepicker.min.js"></script>
<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Add Webinar</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Webinar</a></li>
				<li class="breadcrumb-item active" aria-current="page">Add Webinar </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-8">               
				<div class="formInfo">	
					<?php echo validation_errors(); ?>
					<form method="POST" id="webinarform" name="webinarform" enctype="multipart/form-data">			
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_id" id="webinar_id" value="<?php echo $webinar_code; ?>"  readonly >
									<label class="form-control-placeholder floatinglabel">Webinar ID <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_name" id="webinar_name" value=""  >
									<label class="form-control-placeholder floatinglabel">Webinar Name <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control datepicker" name="start_date" id="start_date" value=""  >
									<label class="form-control-placeholder floatinglabel" style="top:-13px!important;">Start Date <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="time" class="form-control timepicker" name="start_time" id="start_time" value=""  >
									<label class="form-control-placeholder floatinglabel" style="top:-10px!important;">Start Time <em>*</em></label>
								</div>
							</div>
							<div class="col-md-12" style="margin: 0 0 25px 0;">	
								<label class="form-control-placeholder floatinglabel">Cost Type <em>*</em></label>
							</div>
							<div class="col-md-4">								
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type1" name="cost_type" class="custom-control-input get-type" value="FREE" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type1">Free</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type2" name="cost_type" class="custom-control-input get-type" value="PAID" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type2">Paid</label>
								</div>								
								<div id="upload_type_err"></div>
							</div>
							<div class="col-md-8">
							<div class="row" id="paid-amt"><br />
								<div class="col-md-6" style="margin-top: -25px;">
									<div class="form-group">
										<select name="" id="" class="form-control">
											<option value="USD">$</option>
											<option value="EUR">€</option>
											<option value="CRC">₡</option>
											<option value="GBP">£</option>
											<option value="ILS">₪</option>
											<option value="INR" selected="selected">₹</option>
											<option value="JPY">¥</option>
										</select>
										<label class="form-control-placeholder floatinglabel">Price <em>*</em></label>
									</div>
								</div>
								<div class="col-md-6" style="margin-top: -25px;">
									<div class="form-group">
										<input type="text" class="form-control " name="cost_price" id="cost_price" value=""  >
										<label class="form-control-placeholder floatinglabel">Price <em>*</em></label>
									</div>
								</div>
							</div>
							</div>	
							<div class="col-md-12">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Webinar Banner </button>
									<input type="file" class="form-control" name="banner_img" id="banner_img"  value="">
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>											
							</div>	
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control " name="registration_link" id="registration_link" value=""  >
									<label class="form-control-placeholder floatinglabel">Registration Link <em>*</em></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control " name="hosting_link" id="hosting_link" value=""  >
									<label class="form-control-placeholder floatinglabel">Hosting Link <em>*</em></label>
								</div>
							</div>
							<!----<div class="col-md-12" id="uploadImg">	
								<div class="file-details" style="min-height:100px;">
									<input type="hidden" name="customFileCount" id="customFileCount" value="0">
									<div class="form-group file-remove" id="row_files_0">
										<div class="row">										
											<div class="col-md-12">
												<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload0" />
											</div>
										</div>
									</div>
										
									<div id="last_team_file_id"></div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="custom-file-upload mt-3 btn btn-primary" onclick="append_image_files_row()"><i class="fa fa-plus-circle"></i> Add File</label>
										</div>
									</div>
								</div><br /><br />
							</div>-->
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="breif_desc" id="breif_desc"></textarea>
											<label class="form-control-placeholder floatinglabel" for="breif_desc">Brief About Webinar <em>*</em></label>
											<span class="error"><?php echo form_error('breif_desc'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="key_points" id="key_points"></textarea>
											<label class="form-control-placeholder floatinglabel" for="key_points">Key Takeaway Points <em>*</em></label>
											<span class="error"><?php echo form_error('key_points'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="about_author" id="about_author"></textarea>
											<label class="form-control-placeholder floatinglabel" for="about_author">About Author<em>*</em></label>
											<span class="error"><?php echo form_error('about_author'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							<div class="col-md-12" ><br />
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary add_button">Cancel</a> 
								<button type="submit" class="btn btn-primary add_button">Submit</button> 
							</div>	
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	
	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	
	//CURRENTLY NOT IN USE
	/*function append_invite_row()
	{
		var total_div_cnt = $('input[name*="youtube_link[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" youtube link", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#invite_row_cnt").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="row vid-remove" id="byt_invite_row_'+row_cnt_new+'">';
			append_html += '		<div class="col-md-11">';
			append_html += '			<div class="form-group upload-btn-wrapper" style="margin-top:0">';			
			append_html += '				<input type="text" class="form-control youtube_input video_link" name="youtube_link[]" id="youtube_link'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '		<div class="col-md-1">';
			append_html += '			<div class="form-group" style="margin-top:0">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_current_div_invite('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
			
			$("#invite_row_cnt").val(row_cnt_new);
			$(append_html).insertBefore("#byt_invite_last");
		}
	}
	
	function remove_current_div_invite(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you confirm to delete this row?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#byt_invite_row_"+div_no).remove();
			}
		});	
	}
	
	
	// DOCUMENT UPLOAD
	function append_doc_files_row()
	{
		var total_div_cnt = $('input[name*="doc_file[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#docFileCount").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="form-group" id="row_doc_'+row_cnt_new+'">';
			append_html += '		<div class="row">';
			append_html += '			<div class="col-md-11">';
			append_html += '				<input type="file" class="form-control doc_file doc_upload" name="doc_file[]" id="doc-upload'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '			<div class="col-md-1">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_doc_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
						
			$("#docFileCount").val(row_cnt_new);
			$(append_html).insertBefore("#last_doc_file_id");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	function remove_doc_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete this entry?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#row_doc_"+div_no).remove();
			}
		});	
	}
	
	
	function append_image_files_row()
	{
		var total_div_cnt = $('input[name*="closer_file[]"]').length;
		if(total_div_cnt >= 100)
		{
			swal({ title: "Alert", text: "You can not add more than "+total_div_cnt+" files", type: "warning" });
		}
		else
		{		
			var row_cnt = $("#customFileCount").val();
			var row_cnt_new = parseInt(row_cnt)+1;
			
			var append_html = '';	
			append_html += '	<div class="form-group file-remove" id="row_files_'+row_cnt_new+'">';
			append_html += '		<div class="row">';
			append_html += '			<div class="col-md-11">';
			append_html += '				<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload'+row_cnt_new+'" />';
			append_html += '			</div>';
			append_html += '			<div class="col-md-1">';
			append_html += '				<button type="button" class="btn btn-primary btn-sm" onclick="remove_files_div('+row_cnt_new+')"><i class="fa fa-trash"></i></button>';
			append_html += '			</div>';
			append_html += '		</div>';
			append_html += '	</div>';
						
			$("#customFileCount").val(row_cnt_new);
			$(append_html).insertBefore("#last_team_file_id");
			
			$('input[type="file"]').change(function(e)
			{              
				var fileName = e.target.files[0].name;
				$(this).parent().find('input').next("span").remove();
				$(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');			
			});
		}
	}
	
	
	function remove_files_div(div_no)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete this entry?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#row_files_"+div_no).remove();
			}
		});	
	}
	
	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function TeamFilesPreview(input, public_private_flag) 
	{
		console.log(public_private_flag);
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			
			var disp_img_name = "";
			var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
			disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
			
			var id_name = '';
			var btn_id_name = '';
			var input_cls_name = '';
			var input_type_name = '';
			var fun_var_parameter = "";
			var file_upload_common = '';
			if(public_private_flag == 'public')
			{
				id_name = 'team_files_outer'+j;
				btn_id_name = 'addFileBtnOuter';
				input_cls_name = 'fileUploadTeams';
				input_type_name = 'closer_file[]';
				fun_var_parameter = "'public'";
				file_upload_common = 'file-upload';
			}			
			
			var append_str = '';
			append_str += '	<div class="col-md-2 team_files_outer_common" id="'+id_name+'">';
			append_str += '		<div class="file-list">';
			append_str += '			<a href="javascript:void(0)" onclick="remove_team_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
			//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
			append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
			append_str += '		</div>';
			append_str += '	</div>';				
			
			var btn_str = '';
			btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
			btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
			btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="TeamFilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
			btn_str +=	'	</div>';
			
			if(public_private_flag == 'public')
			{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
				
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
			}
			
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadTeams").change(function() { TeamFilesPreview(this,'public'); });	
	
	
	function show_hide_div()
	{
		$('#uploadImg').hide();
		$('#uploadVideo').hide();
		
		var values = $("input[name='upload_type']:checked").val();
		if(values == "Image")
		{
			$('.vid-remove:not(:first)').remove();
			$('#customFileCount').val(0);
			$("#youtube_link1").val('');
			$('#uploadImg').show();
			$('#uploadVideo').hide();
		}
		else if(values == "Video")
		{
			$('.file-remove:not(:first)').remove();
			$('#invite_row_cnt').val(1);
			$("#file-upload0").val(null);
			$("span.disp_filename").remove();
			$('#uploadVideo').show();
			$('#uploadImg').hide();
		}
	}
	show_hide_div();*/
	
	function show_hide_div()
	{
		
		$('#paid-amt').hide();
		
		var values = $("input[name='cost_type']:checked").val();
		if(values == "PAID")
		{
			$('#paid-amt').show();
		}
		else 
		{			
			$('#paid-amt').hide();
			$("#cost_price").val(null);			
		}
	}
	show_hide_div();
	
	/*var FromDateFun = $('#start_date').datepicker(
	{               
		keyboardNavigation: true,
		forceParse: true,
		calendarWeeks: false,
		autoclose: true,
		format: "dd-mm-yyyy"              
		
	});*/
	
	

</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		var date = new Date();
		$('#start_date').datepicker({
			format: 'dd-mm-yyyy',
			startDate: date,
			autoclose: true,
			ignoreReadonly: true,
			minDate:new Date()	
		});
		
		//$('#start_time').pickatime({});
		
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		// Using Class Add Validation To array Image upload
		$.validator.addMethod("image_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		$.validator.addClassRules("image_file", { image_required: true, valid_img_format:true, filesize:2000000 });
		
		// Textbox Validation required 
		$.validator.addMethod("youtube_required", function(value, element) 
		{ 
			if($.trim(value) == "") { return false; } else { return true; } 
		},'This field is required');
		
		// Youtube URL Validation
		$.validator.addMethod("youtube_input_valid", function(value, element) 
		{
			var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Please enter valid youtube url.");	
		
		$.validator.addClassRules("youtube_input", { youtube_required: true, youtube_input_valid: true });
		
		
		// ALPHANUMERIC Validation
		/*$.validator.addMethod("alphanumeric", function(value, element) 
		{
			var p = /^[A-Za-z0-9 _.-]+$/;
			return (value.match(p)) ? RegExp.$1 : false;
		}, "Only Alphabets and Numbers allowed");*/
		
		
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		// Doc Validation .png, jpeg, jpg etc.
		$.validator.addMethod("doc_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".txt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		}, 'Please upload only .pdf, .xls, .xlsx, .doc, .docx, .ppt, .txt file format');
		
		$.validator.addClassRules("doc_upload", { doc_format:true, filesize:2000000 });
		
		//******* JQUERY VALIDATION *********
		$("#webinarform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			rules:
			{
				webinar_name: { required: true, nowhitespace: true, valid_value: true },
				start_date: { required: true},
				start_time: { required: true},
				upload_type: { required: true},
				cost_price: { required: function(){return $("#upload_type2").val() == 'Paid'; } },	
				banner_img: { required: true, valid_img_format: true,filesize:2000000},
				registration_link:{required: true,url:true },
				hosting_link:{required: true,url:true },
				breif_desc:{required: true, nowhitespace: true, maxCount:['200'] },		
				key_points:{required: true, nowhitespace: true, maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, maxCount:['200'] },
			},
			messages:
			{
				webinar_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				start_date:{required: "This field is required"},
				start_time:{required: "This field is required"},
				upload_type:{required: "This field is required"},
				cost_price:{required: "This field is required"},
				banner_img:{required: "This field is required"},
				registration_link:{required: "This field is required", nowhitespace: "This field is required"},
				hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
				breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
				key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}							
			},
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "cost_type") 
				{
					error.insertAfter("#upload_type_err");
				}
				else { element.closest('.form-group').append(error); }
			}
		});
	});
</script>