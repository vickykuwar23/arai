<div class="container d-nonex">
	<div class="row"> 
		<div class="col-md-12 product_details">
			<ul class="nav nav-tabs align-item-center  justify-content-center"  id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'TechNovuus Feed' || $this->uri->segment(1)=='feeds'  ) { echo "active"; } ?>" data-togglex="tab" href="<?php echo base_url('feeds') ?>">TechNovuus Feed</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'Questions & Answers Forum') { echo "active"; } ?>" data-togglex="tab" href="<?php if($page_title == 'Questions And Answers Forum') { echo "javascript:void(0)"; } else { echo site_url('questions_answers_forum'); } ?>">Questions And Answers Forum</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'Blogs') { echo "active"; } ?>" data-togglex="tab" href="<?php if($page_title == 'Blogs') { echo "javascript:void(0)"; } else { echo site_url('blogs_technology_wall'); } ?>">Blogs</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'Knowledge Repository'  || $this->uri->segment(1)=='knowledge_repository'  ) { echo "active"; } ?>" data-togglex="tab" href="<?php if($page_title == 'Knowledge Repository') { echo "javascript:void(0)"; } else { echo site_url('knowledge_repository'); } ?>">Knowledge Repository</a>
				</li>
				<?php /*
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'Resource Sharing'  || $this->uri->segment(1)=='resource_sharing'  ) { echo "active"; } ?>" data-togglex="tab" href="<?php if($page_title == 'Resource Sharing') { echo "javascript:void(0)"; } else { echo site_url('resource_sharing'); } ?>">Resource Sharing</a>
				</li>
				*/ ?>

				
				<li class="nav-item">
					<a class="nav-link <?php if($page_title == 'Webinar Wall') { echo "active"; } ?>" data-togglex="tab" href="<?php if($page_title == 'Webinar Wall') { echo "javascript:void(0)"; } else { echo site_url('webinar'); } ?>">Webinar Wall</a>
				</li>
			</ul>
		</div>
	</div>
</div>