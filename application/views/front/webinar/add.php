<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<style>
.datepicker table tr td, .datepicker table tr th { border-radius: 0 !important; border: 1px solid rgba(0,0,0,0.1) !important; }
.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover { background: rgba(0,0,0,0.1) !important; color: rgba(0,0,0,0.4) !important; cursor: not-allowed !important; border: 1px solid #fff; }
th.datepicker-switch, th.next, th.prev { border: none !important; }


	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
	p.team_member_display_cnt { text-align:center; font-weight:500; }
	.formInfo .btn-group.sw-btn-group-extra.sw-btn-group-extra-step1 { position: unset; }
	.formInfo .btn-primary.btnCancelCustom.btnCancelCustomStep1 { margin-left: 5px; border-radius: 4px; position: absolute; left: 5px; top: 10px; }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	
	.floatingfile { -webkit-transform: translateY(-14px) scale(1); transform: translateY(-14px) scale(1);
	cursor: pointer;}
	input.datepicker  { color: #000 !important;}
	input.doc_file { color: #000 !important;}
	input.error{color:#000 !important;font-size: 1rem;}
	
	/**
	* Tooltip Styles
	*/
	
	/* Add this attribute to the element that needs a tooltip */
	[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
	}
	
	/* Hide the tooltip content by default */
	[data-tooltip]:before,
	[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
	}
	
	/* Position tooltip above the element */
	[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
	}
	
	/* Triangle hack to make tooltip look like a speech bubble */
	[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
	}
	
	/* Show tooltip content on hover */
	[data-tooltip]:hover:before,
	[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
	}
	
	.form-control-placeholder{top: -14px !important;}
	textarea.error {color: #000000!important;}
	input.error{color:#000000!important;}
	
	/**overwrite css**/
	.form-check-input {position: absolute; margin-top: .3rem !important; margin-left: -1.25rem !important;}
	
</style>
<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css"> */ ?>
<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/css/') ?>jquery.datetimepicker.min.css"> */ ?>
<?php /* <script src="<?php echo base_url('assets/front/js/') ?>jquery.datetimepicker.js"></script> */ ?>

<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>

<div id="home-p" class="home-p pages-head3 text-center">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Add Webinar</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('challenge') ?>">Webinar</a></li>
				<li class="breadcrumb-item active" aria-current="page">Add Webinar </li>
			</ol>
		</nav>
	</div>
</div> 

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">               
				<div class="formInfo">	
					
					<form method="POST" id="webinarform" name="webinarform" enctype="multipart/form-data">			
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_id" id="webinar_id" value="<?php echo $webinar_code; ?>"  readonly >
									<label class="form-control-placeholder floatinglabel">Webinar ID <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_name" id="webinar_name" value=""  >
									<label class="form-control-placeholder floatinglabel">Webinar's Name <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control datepicker" name="start_date" id="start_date" value=""  readonly>
									<label class="form-control-placeholder floatinglabel" style="top:-13px!important;">Date <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control clockpicker" name="start_time" id="start_time" value="" readonly>
									<label class="form-control-placeholder floatinglabel" style="top:-10px!important;">Start Time <em>*</em></label>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control clockpicker" name="end_time" id="end_time" value="" readonly>
									<label class="form-control-placeholder floatinglabel" style="top:-10px!important;">End Time <em>*</em></label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="margin: 0 0 15px 0;">	
								<label class="form-control-placeholder floatinglabel">Type of Registration <em>*</em></label>
							</div>
						</div>
							
						<div class="row">							
							<div class="col-md-4" style="margin-bottom:20px">								
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type1" name="cost_type" class="custom-control-input get-type" value="FREE" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type1">Free</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="upload_type2" name="cost_type" class="custom-control-input get-type" value="PAID" onchange="show_hide_div()">
									<label class="custom-control-label" for="upload_type2">Paid</label>
								</div>								
								<div id="upload_type_err"></div>
							</div>
							
							<div class="col-md-8" style="margin-top:8px;">
								<div class="row" id="paid-amt"><br />
									<div class="col-md-3" style="margin-top: -25px;">
										<div class="form-group">
											<select name="currency" id="currency" class="form-control">
												<?php /* <option value="USD" disabled>$</option>
												<option value="EUR" disabled>€</option>
												<option value="CRC" disabled>₡</option>
												<option value="GBP" disabled>£</option>
												<option value="JPY" disabled>¥</option>
												<option value="ILS" disabled>₪</option>	*/ ?>
												<option value="INR (₹)">INR (₹)</option>
											</select>
											<label class="form-control-placeholder floatinglabel">Currency </label>
										</div>
									</div>
									<div class="col-md-9" style="margin-top: -25px;">
										<div class="form-group">
											<input type="text" class="form-control allowd_only_float" name="cost_price" id="cost_price" value=""  >
											<label class="form-control-placeholder floatinglabel">Price <em>*</em></label>
										</div>
									</div>
								</div>
							</div>	
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button class="btn btn-upload"><i class="fa fa-plus"> </i> <em style="color:red;">*</em> Webinar Banner </button>
									<input type="file" class="form-control" name="banner_img" id="banner_img"  value="">
									<p>Only .jpg, .jpeg, .png image formats below 2MB are accepted</p>
								</div>											
							</div>	
							
							<?php $dynamic_cls = 'd-none';
								$full_img = $onclick_fun = ''; ?>
								
							<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="banner_img_outer">
								<div class="form-group">
									<div class="previous_img_outer">
										<img src="<?php echo $full_img; ?>">
										<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
									</div>
								</div>
							</div>
						</div>
												
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;">Webinar Technology <em>*</em></label>
									<div class="boderBox65x">
										<select data-key="<?php echo $res['technology_name']; ?>" class="form-control select2_common cls_team_skillset" name="webinar_technology[]" id="webinar_technology" data-placeholder="Webinar Technology" multiple onchange="show_hide_webinar_technology_other()">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														if(set_value('webinar_technology[]') != "") { $webinar_technology_arr = set_value('webinar_technology[]'); }
														else { $webinar_technology_arr = array(); } ?>
															<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$webinar_technology_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
											<?php }	
													} ?>
										</select> 	
										  <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>																
									</div>
								</div>
							</div>
							
							<div class="col-md-6" id="webinar_technology_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<input type="text" class="form-control" name="webinar_technology_other" id="webinar_technology_other" value="" required>
									<label class="form-control-placeholder floatinglabel">Webinar Technology Other <em>*</em></label>
								</div>
							</div>
						</div>		 
						
						<div class="row">
							<div class="col-md-12" style="margin-bottom: 15px;margin-top: 10px;">		
								<label class="form-control-placeholder floatinglabel">Exclusive Technovuus Event <em>*</em></label>
								<div style="margin-top:15px;">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="exclusive_technovuus_event1" name="exclusive_technovuus_event" class="custom-control-input get-type" value="Yes" checked onchange="show_hide_reg_link()">
										<label class="custom-control-label" for="exclusive_technovuus_event1">Yes</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="exclusive_technovuus_event2" name="exclusive_technovuus_event" class="custom-control-input get-type" value="No" onchange="show_hide_reg_link()">
										<label class="custom-control-label" for="exclusive_technovuus_event2">No</label>
									</div>
								</div>
								<div id="exclusive_technovuus_event_err"></div>
							</div>
							
							<div class="col-md-12" id="reg_link_outer">
								<div class="form-group">
									<textarea class="form-control" name="registration_link" id="registration_link" style='word-break: break-all; min-height: 95px;'></textarea>
									<label class="form-control-placeholder floatinglabel">Registration Link <em>*</em> 	<a style="margin:0 0 0 15px" href="javascript:void(0)" data-tooltip="Host to give info of webinar platform and also provide link for the same"><i class="fa fa-info-circle"></i></i></a></label>
								</div>
							</div>
							
							<?php /*<div class="col-md-2" style="margin-top: 20px;">
								<a href="javascript:void(0)" data-tooltip="Host to give info of webinar platform and also provide link for the same"><i class="fa fa-info-circle"></i></i></a>
							</div> */ ?>
						
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control" name="hosting_link" id="hosting_link" style='word-break: break-all; min-height: 95px;'></textarea>
									<label class="form-control-placeholder floatinglabel">Hosting Link <em>*</em></label>
								</div>
							</div>
						
							<!----<div class="col-md-12" id="uploadImg">	
								<div class="file-details" style="min-height:100px;">
								<input type="hidden" name="customFileCount" id="customFileCount" value="0">
								<div class="form-group file-remove" id="row_files_0">
								<div class="row">										
								<div class="col-md-12">
								<input type="file" class="form-control image_file" name="closer_file[]" id="file-upload0" />
								</div>
								</div>
								</div>
								
								<div id="last_team_file_id"></div>
								<div class="row">
								<div class="col-md-12">
								<button type="button" class="custom-file-upload mt-3 btn btn-primary" onclick="append_image_files_row()"><i class="fa fa-plus-circle"></i> Add File</label>
								</div>
								</div>
								</div><br /><br />
							</div>-->
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="breif_desc" id="breif_desc"></textarea>
											<label class="form-control-placeholder floatinglabel" for="breif_desc">Brief About Webinar <em>*</em></label>
											<span class="error"><?php echo form_error('breif_desc'); ?></span>
										</div>											
									</div>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="key_points" id="key_points"></textarea>
											<label class="form-control-placeholder floatinglabel" for="key_points">Key Takeaway Points <em>*</em></label>
											<span class="error"><?php echo form_error('key_points'); ?></span>
										</div>											
									</div>
								</div>
							</div>
						
							<div class="col-md-12">
								<div class="row">					 
									<div class="col-md-12"><br />
										<div class="form-group">
											<textarea type="text" class="form-control" name="about_author" id="about_author"></textarea>
											<label class="form-control-placeholder floatinglabel" for="about_author">About Author<em>*</em></label>
											<span class="error"><?php echo form_error('about_author'); ?></span>
										</div>											
									</div>
								</div>
							</div>
						
							<div class="col-md-12" ><br />
								<a href="javascript:javascript:history.go(-1)" class="btn btn-primary btnCancelCustom">Cancel</a> 
								<button type="submit" class="btn btn-primary add_button float-right">Submit</button> 
							</div>	
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.js'); ?>"></script>

<script>	
	$('.clockpicker').clockpicker(
	{
    /* placement: 'top',
		autoclose:true
    align: 'left',*/
		twelvehour: true,
		donetext: 'Done'
	});
	
	$('.select2_common').select2();
	
	function show_hide_webinar_technology_other()
	{
		$("#webinar_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#webinar_technology').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#webinar_technology_other_outer").show();
		}
		else
		{
			$("#webinar_technology_other_outer").hide();
			$("#webinar_technology_other").val('');
		}
	}
	show_hide_webinar_technology_other();
	
	
	/* $( "#techonogy_id" ).change(function() {
			var f_name = $("#techonogy_id").val();
			if(f_name != 0){
				//$("#otherTech").hide();
				$( "#other_technology" ).prop( "disabled", false );
			} else {
				//$("#otherTech").show();
				$( "#other_technology" ).val();
				$( "#other_technology" ).prop( "disabled", true );
			}
		}); */
	
	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	
	function show_hide_div()
	{		
		$('#paid-amt').hide();
		
		var values = $("input[name='cost_type']:checked").val();
		if(values == "PAID")
		{
			$('#paid-amt').show();
		}
		else 
		{			
			$('#paid-amt').hide();
			$("#cost_price").val(null);			
		}
	}
	show_hide_div();
	
	function show_hide_reg_link()
	{		
		$('#reg_link_outer').hide();
		
		var values = $("input[name='exclusive_technovuus_event']:checked").val();
		if(values == "No")
		{
			$('#reg_link_outer').show();
		}
		else 
		{			
			$('#reg_link_outer').hide();
			$("#reg_link_outer").val("");			
		}
	}
	show_hide_reg_link();
</script>

<script src="<?php echo base_url('assets/front/js/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/front/js/jquery-validation/additional-methods.min.js'); ?>"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	function Converttimeformat(time) 
	{
		// var time = $("#starttime").val();
		//var time = document.getElementById('txttime').value;
		var hrs = Number(time.match(/^(\d+)/)[1]);
		var mnts = Number(time.match(/:(\d+)/)[1]);
		var format = time.match(/\s(.*)$/)[1];
		if (format == "PM" && hrs < 12) hrs = hrs + 12;
		if (format == "AM" && hrs == 12) hrs = hrs - 12;
		var hours = hrs.toString();
		var minutes = mnts.toString();
		if (hrs < 10) hours = "0" + hours;
		if (mnts < 10) minutes = "0" + minutes;
		return (hours + ":" + minutes);
	}
	//Converttimeformat('10:00 PM');

	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{
		/****New DateTimepicker**/			
			//TimePicke Example
			/* $('.datetimepicker1').datetimepicker({
				datepicker:false,
				formatTime:"h:i a",
				format:"h:i a",
				step:15
			});	 */		
			
			//DatePicker Example
			/*$('#datetimepicker').datetimepicker();
				
				//TimePicke Example
				$('.datetimepicker1').datetimepicker({
				datepicker:false,
				formatTime:"h:i a",
				format:"h:i a",
				step:60
				});
				
				//Inline DateTimePicker Example
				$('#datetimepicker2').datetimepicker({
				format:'Y-m-d H:i',
				inline:true
				});
				
				//minDate and maxDate Example
				$('#datetimepicker3').datetimepicker({
				format:'Y-m-d',
				timepicker:false,
				minDate:'-1970/01/02', //yesterday is minimum date
				maxDate:'+1970/01/02' //tomorrow is maximum date
				});
				
				//allowTimes options TimePicker Example
				$('#datetimepicker4').datetimepicker({
				datepicker:false,
				allowTimes:[
				'11:00', '13:00', '15:00', 
				'16:00', '18:00', '19:00', '20:00'
				]
			});*/
			
			
			
			$("a").tooltip();
			var date = new Date();
			$('#start_date').datepicker({
				format: 'dd-mm-yyyy',
				startDate: date,
				autoclose: true,
				ignoreReadonly: true,
				todayHighlight:true,
				minDate:new Date(),
				endDate:"<?php echo date('d-m-Y', strtotime('+365days', strtotime(date('d-m-Y')))); ?>"
			});
			
			$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
			
			// Words MIN Count Validation
			$.validator.addMethod("minCount", function(value, element, params) 
			{
				var count = getWordCount(value);
				if(count >= params[0]) { return true; }
			}, jQuery.validator.format("Minimum {0} Words Required"));
			
			// Words MAX Count Validation
			$.validator.addMethod("maxCount", function(value, element, params) 
			{
				var count = getWordCount(value);
				if(count <= params[0]) { return true; }
			}, jQuery.validator.format("Maximum {0} words are allowed."));
			
			// Image Type Validation .png, jpeg, jpg etc.
			$.validator.addMethod("valid_img_format", function(value, element) 
			{ 
				if(value != "")
				{
					var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
					var fileExt = value.toLowerCase();
					fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
					if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
				}else return true;
			});
			
			$.validator.addMethod("valid_url", function(value, element) {
				return this.optional(element) || /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i.test(value);
			}, "Please enter valid url");
			
			// File Size Validation
			$.validator.addMethod('filesize', function (value, element, param) { 
				return this.optional(element) || (element.files[0].size <= param)
			}, 'File size must be less than 2 MB');
			
			// Using Class Add Validation To array Image upload
			$.validator.addMethod("image_required", function(value, element) 
			{ 
				if($.trim(value) == "") { return false; } else { return true; } 
			},'This field is required');
			
			$.validator.addClassRules("image_file", { image_required: true, valid_img_format:true, filesize:2000000 });
			
			// Textbox Validation required 
			$.validator.addMethod("youtube_required", function(value, element) 
			{ 
				if($.trim(value) == "") { return false; } else { return true; } 
			},'This field is required');
			
			// Youtube URL Validation
			$.validator.addMethod("youtube_input_valid", function(value, element) 
			{
				var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
				return (value.match(p)) ? RegExp.$1 : false;
			}, "Please enter valid youtube url.");	
			
			$.validator.addClassRules("youtube_input", { youtube_required: true, youtube_input_valid: true });
			
			
			// ALPHANUMERIC Validation
			/*$.validator.addMethod("alphanumeric", function(value, element) 
				{
				var p = /^[A-Za-z0-9 _.-]+$/;
				return (value.match(p)) ? RegExp.$1 : false;
			}, "Only Alphabets and Numbers allowed");*/
			
			
			
			$.validator.addMethod("valid_value", function(value, element) {
				return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
			}, "Title must contain only letters, numbers, or dashes.");
			
			$.validator.addMethod("chk_valid_time", function(value, element)
			{
				if($.trim(value).length == 0)
				{
					return true;
				}
				else
				{
					var startTime = $.trim($("#start_time").val());
					var endTime = $.trim(value);
					
					if(startTime.length == 0)
					{
						return true;
					}
					else
					{
						startTime = startTime.replace("AM", " AM");
						startTime = startTime.replace("PM", " PM");
						startTime = Converttimeformat(startTime);
						
						endTime = endTime.replace("AM", " AM");
						endTime = endTime.replace("PM", " PM");
						endTime = Converttimeformat(endTime);
						
						//console.log(startTime+" >> "+endTime);
						if(startTime > endTime) { return false; }
						else { return true; }
						/* return false; */
					}
				}
			}, 'Please enter valid time');
		
			// Doc Validation .png, jpeg, jpg etc.
			$.validator.addMethod("doc_format", function(value, element) 
			{ 
				if(value != "")
				{
					var validExts = new Array(".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".txt");
					var fileExt = value.toLowerCase();
					fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
					if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
				}else return true;
			}, 'Please upload only .pdf, .xls, .xlsx, .doc, .docx, .ppt, .txt file format');
			
			$.validator.addClassRules("doc_upload", { doc_format:true, filesize:2000000 });
			
			//******* JQUERY VALIDATION *********
			$("#webinarform").validate( 
			{
				/* onkeyup: false, 
				ignore: ":disabled",*/
				rules:
				{
					webinar_name: { required: true, nowhitespace: true, valid_value: true },
					start_date: { required: true},
					start_time: { required: true},
					end_time: { required: true, chk_valid_time:true },
					cost_type: { required: true},
					cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
					banner_img: { required: true, valid_img_format: true,filesize:2000000},
					"webinar_technology[]": { required: true },
					exclusive_technovuus_event:{required: true },
					registration_link:{required: true,valid_url:true },
					hosting_link:{required: true,valid_url:true },
					breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
					key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
					about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				},
				messages:
				{
					webinar_name: { required: "This field is required", nowhitespace: "Please enter the title" },
					start_date:{required: "This field is required"},
					start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					banner_img:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
					about_author:{required: "This field is required", nowhitespace: "This field is required"}							
				},
				errorElement: 'span',
				errorPlacement: function(error, element) // For replace error 
				{
					if (element.attr("name") == "cost_type") 
					{
						error.insertAfter("#upload_type_err");
					}
					else if (element.attr("name") == "exclusive_technovuus_event") 
					{
						error.insertAfter("#exclusive_technovuus_event_err");
					}
					else { element.closest('.form-group').append(error); }
				}
			});
		});
	</script>
	
	<script type="text/javascript">
		//DISPLAY IMAGE PREVIEW WHEN BROWSE
		function BannerPreview(input) 
		{
			if (input.files && input.files[0]) 
			{
				$("#preloader").css("display", "block");
				var file_name = input.files[0].name;
				var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
				if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
				{
					var reader = new FileReader();
					reader.onload = function(e) 
					{
						$("#banner_img_outer").removeClass('d-none');
						//$("#banner_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
						$("#banner_img_outer .previous_img_outer img").attr("src", e.target.result);
						$("#banner_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
					}				
					reader.readAsDataURL(input.files[0]); // convert to base64 string			
				}
				else
				{
					$("#banner_img_outer").addClass('d-none');
				}
				$("#preloader").css("display", "none");
			}
		}
		$("#banner_img").change(function() { BannerPreview(this); });	
	</script>	