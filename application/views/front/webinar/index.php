<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
	.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
	.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
	.select2-container--default .select2-selection--single{height:35px!important; border:none}
	#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
	#WebinarWallSlider.owl-carousel .owl-dot,#WebinarWallSlider.owl-carousel .owl-nav .owl-next,#WebinarWallSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
	#WebinarWallSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	#WebinarWallSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	#WebinarWallSlider .owl-nav.disabled{display:block}
	#WebinarWallSlider .owl-nav{position:absolute; top:40%; width:100%}
	#WebinarWallSlider.owl-theme .owl-dots .owl-dot.active span,#WebinarWallSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
	#WebinarWallSlider.owl-theme .owl-nav [class*=owl-]{color:#000}
	#WebinarWallSlider .owl-nav .owl-prev{left:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff; color: 000;}  
	#WebinarWallSlider .owl-nav .owl-next{right:10px; position:absolute; background:#32f0ff; border:solid 1px #32f0ff;  color: #000;}
	#WebinarWallSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #32f0ff;color:#32f0ff}
	#WebinarWallSlider .owl-item{ padding: 0 5px;}
	#WebinarWallSlider .owl-item img{width:100%; }
	#WebinarWallSlider .owl-dots{position:absolute;width:100%;bottom:25px}
	#WebinarWallSlider .owl-item{ padding: 0;}
	#FeaturedWebinarsSlider.owl-carousel .owl-dot,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	#FeaturedWebinarsSlider .owl-nav.disabled{display:block}
	#FeaturedWebinarsSlider .owl-nav{position:absolute; top:40%; width:100%}
	#FeaturedWebinarsSlider .owl-nav [class*=owl-]:hover{background:#c80032!important;border:solid 1px #c80032;color:#FFF}
	#FeaturedWebinarsSlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#FFF;color:#c80032; border: solid 1px #c80032;}
	#FeaturedWebinarsSlider .owl-nav .owl-next{right:-30px;position:absolute;background:#FFF;color:#c80032; border: solid 1px #c80032;}
	#FeaturedWebinarsSlider .owl-item{ padding: 0 15px;}
	#FeaturedWebinarsSlider .owl-item img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2);}
	.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}
	/* #FeaturedWebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
	.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
	.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
	.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
	.product_details{padding: 20px 0 0 0;}
	/** Blog**/
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}
	.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
	.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}
	.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
	.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.tag ul li a:hover{ background: #000; color: #32f0ff; }
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
	.shareList ul li a:hover{ background: #000; color: #32f0ff; }
	.userPhoto-info h3{ font-size: 19px !important; }
	.userPhoto-info img{ border-radius: 50%;}
	.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
	.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.descriptionBox45{padding:15px;border:1px solid #CCC; height: 330px; }
	.descriptionBox45 p{color: #333;}
	.descriptionBox45 ul{color: #333; list-style: none; padding: 0; margin:0;  font-size: 14px;}
	.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
	.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#333}
	.bs-example45{border-radius:6px;background:#FFF;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
	.bs-example45 h3{color:#333;font-size:19px;font-weight:800}
	.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
	/** Color Change**/
	.nav-tabs{border-bottom:none}
	.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#FFF;background-color:#c80032 ;border-color:#c80032}
	/* .product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {
	border-color: #32f0ff #32f0ff #32f0ff;
	} */
	.title-bar .heading-border{background-color:#c80032}
	.search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
	.filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
	.filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
	.owner-teammember img{height:100%!important; width:100%!important;border-radius: 0%!important;}
	.owner-teammember a{background:none;}
	.owner-teammember a:hover{background:none;}
	
	/**overwrite css**/
	.search-sec {padding: 1rem;background: #c80032; width: 100%; margin-bottom: 0px;}
	
	/* .filterBox .btn-primary {color: #32f0ff;	background-color: #020001; border-color: #020001;	width: 100%;display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}
	.filterBox .btn-primary:hover {color: #000; background-color: #32f0ff; border-color: #000}
	
	.clearAll{ background:#000 !important; color:#32f0ff !important; border-color: #32f0ff !important; margin-right:15px}
	.clearAll:hover{ background:#32f0ff !important; color:#000 !important; border-color: #000 !important;} */

	.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%;display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}

.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important;}
	
	.form-check-input {position: absolute; margin-top: .3rem !important; /* margin-left: -1.25rem !important; */ }
	#AnswersForum h2{font-size:1.75rem}
	
	a.filter_load_more, a.filter_load_less {background:none; font-size: 14px; float: right;}  
	#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
	#flotingButton2 a {color: #FFF; background: #c80032; padding: 10px 15px; border: solid 1px #c80032; text-decoration: none; display: block; text-align: center; border-radius: 15px;font-size: 14px;}

#flotingButton2 a:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none;}

.ButtonAllWebinars a:hover {background: #FFF; color: #c80032; text-decoration: none;}
.ButtonAllWebinars a {background: #c80032; color: #FFF; border: solid 1px #c80032; text-decoration: none; padding:  10px; border-radius: .25rem;}


.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}

</style>

<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Webinar Wall</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">List of Webinar's</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php $this->load->view('front/webinar/inc_technology_wall_tabs.php'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane active show fade  pt-0" id="WebinarWall">
			<!------------ START : WEBINAR SEARCH --------->
			<div class="filterBox">
				<section class="search-sec">
					<div id="filterData" name="filterData">
						<div class="container">   
							<div class="row">
								<div class="col-lg-12">
									<div id="mySidenav" class="sidenav">
										<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
										<div class="scroll">	
											<ul>
												<li> <strong> Select Technologies</strong> </li>										
												<?php 
													$i=0;
													if(count($technology_data) > 0) 
													{ 
														foreach($technology_data as $res)
														{	
															if(strtolower($res['technology_name']) != 'other')
															{ ?>										
															<li class="<?php if($i >= 5) { ?>filter_technology d-none<?php	} ?>">
																<input class="form-check-input search_technology" type="checkbox" value="<?php echo $res['id']; ?>" id="search_technologies" name="search_technologies[]" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
																<label class="form-check-label"><?php echo $res['technology_name']; ?></label>
															</li>
															<?php $i++;
															}
														}
													}	?>
											</ul>
											<?php if(count($technology_data) > 5){ ?>
											<a href="javascript:void(0);" class="filter_load_more" onclick="show_more_technology('show')">Show More >> </a>
											<a href="javascript:void(0);" class="filter_load_less d-none" onclick="show_more_technology('hide')">Show Less >></a>
											<?php } ?><br>
											
											<ul>
												<li> <strong> Select Pay Type</strong> </li>										
												<li class="">
													<input class="form-check-input search_pay_type" type="checkbox" value="FREE" id="search_pay_types" name="search_pay_type[]" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
													<label class="form-check-label">FREE</label>
												</li>										
												<li class="">
													<input class="form-check-input search_pay_type" type="checkbox" value="PAID" id="search_pay_types" name="search_pay_type[]" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
													<label class="form-check-label">PAID</label>
												</li>
											</ul>
											<ul>
												<li> <strong> Select Webinar Status</strong> </li>	
											<li>
											<div class="form-check">
										  <input class="form-check-input webinar_time" type="radio" name="webinar_time" id="webinar_time0" value="all" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
										  <label class="form-check-label" for="webinar_time0">
										    All
										  </label>
										</div>
											<div class="form-check">
										  <input class="form-check-input webinar_time" type="radio" name="webinar_time" id="webinar_time1" value="upcoming" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
										  <label class="form-check-label" for="webinar_time1">
										    Upcoming
										  </label>
										</div>
										<div class="form-check">
										  <input class="form-check-input webinar_time" type="radio" name="webinar_time" id="webinar_time2" value="closed" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)">
										  <label class="form-check-label" for="webinar_time2">
										    Closed
										  </label>
										</div>
										</li>
										</ul>
										</div>								
									</div>
									
									<div class="row d-flex justify-content-center search_Box">								
										<div class="col-md-6">
											<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />	
											<button type="button" class="btn btn-primary searchButton" onclick="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
										<div class="clearAll">
											<a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_webinar_search_form()">Clear All</a>
										</div>
										<?php if(count($technology_data) > 0) { ?>
											<div class="filter d-nonex">
												<button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
											</div>
										<?php } ?>		

									<div class="col-md-2">
									<select name="" id="" class="form-control" onchange="getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0,this.value)">
									    <option value="desc">Newest</option>
									    <option value="asc">Oldest</option>
									</select>  
									</div>																	
										<div class="col-md-12"></div>								
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!------------ END : WEBINAR SEARCH --------->
			
			<?php 
				$encrptopenssl =  New Opensslencryptdecrypt();	
				if(count($webinar_banner) > 0)
				{ ?>
					<div id="WebinarWallSlider" class="owl-carousel mb-2 owl-theme">
						<?php 
							foreach($webinar_banner as $wbanner)
							{ ?>
								<div class="owner-teammember">
									<img src="<?php echo base_url('assets/webinar_banner/'.$encrptopenssl->decrypt($wbanner['banner_img'])); ?>" alt="<?php $encrptopenssl->decrypt($wbanner['banner_name']) ?>" title="<?php echo $wbanner['id'] ?>">
								</div>
				<?php } ?>                        
					</div>
	<?php } ?>		
			
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-bar">
							<h2>Featured Webinars </h2>
							<div class="heading-border"></div>
						</div>						
						<div id="FeaturedWebinarDataOuter"></div>						
							
						
						<div class="title-bar">
							<h2>Webinars</h2>
							<div class="heading-border"></div>
						</div>	
						
						<div class="formInfoNew">
							<div class="row upcoming-list" id="UpcomingWebibarDataOuter">
							</div>						
						</div>						
													
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="flotingButton2" class="d-none">
	<a href="<?php echo base_url('webinar/add'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post Webinar</a>
</div>
<div id="social-share">
                    <div class="social-open-menu">
                        <a class="btn-share"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    </div>
                    <ul class="social-itens hidden">
                    	 <li>
                            <a class="btn-share social-item-5" href="<?php echo base_url('webinar/add'); ?>">Share / Post Webinar</a>
                        </li>
                          <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('resource_sharing/add'); ?>">Share / Post a Resource</a>
                        </li> 

                         <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('technology_transfer/add'); ?>">Share / Post Technology to Transfer </a>
                        </li> 
                        <li>
                            <a class="btn-share social-item-1" href="<?php echo base_url('knowledge_repository/add'); ?>">Share Knowledge </a>
                        </li>
                        <li>
                            <a class="btn-share social-item-2" href="<?php echo base_url('questions_answers_forum/add_qa'); ?>">Ask Question</a>
                        </li>
                        <li>
                            <a class="btn-share social-item-3" href="<?php echo base_url('blogs_technology_wall/add_blog'); ?>">Write a Blog</a>
                        </li>

                       
                    </ul>

                </div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<?php $this->load->view('front/inc_sweet_alert_common'); ?>

<script>	
	function show_more_technology(val)
	{
		if(val == 'show')
		{
			$( ".filter_technology" ).removeClass( "d-none" )
			$( ".filter_load_less" ).removeClass( "d-none" )
			$( ".filter_load_more" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_technology" ).addClass( "d-none" )
			$( ".filter_load_less" ).addClass( "d-none" )
			$( ".filter_load_more" ).removeClass( "d-none" )
		}
	}
	
	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0);
		} 
	});
	
	function clear_webinar_search_form() 
	{ 
		$("#search_txt").val('');  
		$('.search_technology:checkbox').prop('checked',false); 
		$('.search_pay_type:checkbox').prop('checked',false); 
		
		$('.webinar_time').prop('checked', false);


		
		getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 1, 0);
	}
	
	//START : FEATURED & UPCOMING WEBINARS DATA
	function getFeaturedUpcomingWebibarDataAjax(f_start, f_limit, u_start, u_limit, is_search, is_show_more,order_by='')
	{
		$("#showMoreBtnUpcoming").remove();
		var cs_t = 	$('.token').val();
		
		var selected_technology = [];
		$.each($("input.search_technology:checked"), function() { selected_technology.push($(this).val()); });
		
		var selected_pay_type = [];
		$.each($("input.search_pay_type:checked"), function() { selected_pay_type.push($(this).val()); });

		var webinar_time = $('input[name="webinar_time"]:checked').val();
		if (webinar_time==undefined) {webinar_time='';}

				
		var keyword = encodeURIComponent($('#search_txt').val());
		var technology = encodeURIComponent(selected_technology);	
		var pay_type = encodeURIComponent(selected_pay_type);
		
		parameters= { 'f_start':f_start, 'f_limit':f_limit, 'u_start':u_start, 'u_limit':u_limit, 'is_show_more':is_show_more, 'keyword':keyword, 'technology':technology, 'pay_type':pay_type, 'cs_t':cs_t,'webinar_time':webinar_time,'order_by':order_by}
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('webinar/getFeaturedUpcomingWebibarDataAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					if(is_search == '1') { $("#UpcomingWebibarDataOuter").html(''); }
					
					$(".token").val(data.csrf_new_token)
					if(is_show_more == 0) { $("#FeaturedWebinarDataOuter").html(data.Featured_response); }
					$("#UpcomingWebibarDataOuter").append(data.Upcoming_response);
				}else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getFeaturedUpcomingWebibarDataAjax(0, '<?php echo $featured_webinar_limit; ?>', 0, '<?php echo $upcoming_webinar_limit; ?>', 0, 0);
	//END : FEATURED & UPCOMING WEBINARS DATA
	
	$( document ).ready(function() 
	{
		$('#WebinarWallSlider').owlCarousel(
		{
			items: 1,
			autoplay: true,
			smartSpeed: 700,
			autoHeight: true,
			loop: true,
			nav: true,
			dots: true,
			navText: ["", ""],
			autoplayHoverPause: true
		});
	});	
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	
	function openNav() 
	{		
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
		
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";		
	}

$(document).ready(function(){
$(".social-open-menu").click(function () {
    $(".social-itens").toggleClass("open");
    $(".social-itens").toggleClass("hidden");
});
});
</script>