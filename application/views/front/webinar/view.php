<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<style>
	.search-slt{display:block;width:100%;font-size:.875rem;line-height:1.5;color:#000;background-color:#fff!important;height:35px!important; border:none}
	.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:38px}
	.select2-container--default .select2-selection--single .select2-selection__arrow{height:35px;position:absolute;top:1px;right:1px;width:20px}
	.select2-container--default .select2-selection--single{height:35px!important; border:none}
	#select2-trls-container,#select2-visibility-container{padding:0 0 0 15px!important}
	.body-background{background-color:#6698FF;}
	.web-color{color:#fff;}
	
	
	.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);padding:85px 15px 15px 15px;min-height:400px}
	.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
	.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
	
	.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}
	
	.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
	
	.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#FFF;background:#47a44b;margin-top:10px}
	
	.bs-example2 .buttonNew2:hover{color:#FFF;background:#39843c;text-decoration:none}
	.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff; }
	.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
	.borderBoxNewClass p{ font-size: 14px;color: #FFF; text-align: center; text-transform: uppercase; font-weight: 700; margin:15px 0 ; }
	h5 {text-transform:uppercase;}
	p {font-family: "Montserrat", sans-serif; color: #64707b; font-size: 15px; font-weight: 300;}

	.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}

</style>
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<div id="home-p" class="home-p CollaborativeTechnologySolutions text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Webinar Details</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo base_url('webinar'); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Webinar Details</li>
			</ol>
		</nav>
	</div>
	<!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-4 bs-example2" data-aos="fade-right">
				<div class="">
					<h3 class="text-center"><?php echo $webinar_details[0]['webinar_name'] ?></h3> 
					<div class="borderBoxNewClass">
						<ul>
							<li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date('d M Y', strtotime($webinar_details[0]['webinar_date'])) ?></li>
							<li>|</li>
							<li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php 
								echo date('h:i A', strtotime($webinar_details[0]['webinar_start_time']))." - ".date('h:i A', strtotime($webinar_details[0]['webinar_end_time']));  ?></li>
						</ul>
						<p><?php if($webinar_details[0]['webinar_cost_type'] == "FREE"){ }else{echo $webinar_details[0]['currency']." ".$webinar_details[0]['cost_price'];}echo "&nbsp;&nbsp;".$webinar_details[0]['webinar_cost_type'] ?></p>
					</div>
					
					<?php
						if($this->session->userdata('user_id') != $webinar_details[0]['user_id'])
						{
							if(count($check_entry) == 0)
							{ ?>
								<a href="javascript:void(0)<?php //echo $webinar_details[0]['registration_link'] ?>" class="buttonNew register-link" id="<?php echo $webinar_details[0]['w_id'] ?>">Register Now</a>
							<?php }else if($webinar_details[0]['hosting_link']!=''){ 
						if($webinar_details[0]['exclusive_technovuus_event'] == "Yes") { 		
						echo '<a class="buttonNew" target="_blank" href='.$webinar_details[0]['hosting_link'].'>Go to Webinar</a>';}else{
							echo '<a class="buttonNew" target="_blank" href='.$webinar_details[0]['hosting_link'].'>Hosting Link</a>';
							} 
						}
					} ?>
					<?php if($webinar_details[0]['exclusive_technovuus_event'] == "No") { ?>
					<a href="javascript:void(0)" class="buttonNew2 share-url" id="<?php echo $webinar_details[0]['w_id'] ?>"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a>
					<?php } ?>						
				</div>   
			</div>
			<div class="col-md-8" data-aos="fade-left">
				<div class="aboutInnerPage">
					<img src="<?php echo base_url('assets/webinar/'.$webinar_details[0]['banner_img']); ?>" alt="About Inner Page" class="img-fluid" style="padding:0 0 0 15px">
				</div>
				<!-- <div class="textInfoAbout2">
					<h2>About Us</h2>
					<p>In the report, BNEF outlines that electric vehicles (EVs) will hit 10% of global passenger vehicle sales in 2025, with that number rising to 28% in 2030 and 58% in 2040. According to the study, EVs currently make up 3% of global car sales.
					</p>
				</div> -->
			</div>
			<div class="col-md-12 mt-4">
				<h5>About Us</h5>
				<p style="white-space:break-spaces"><?php echo $webinar_details[0]['webinar_breif_info'] ?></p>
				<hr/>
				<h5>Key Takeaway</h5>
				<p style="white-space:break-spaces"><?php echo $webinar_details[0]['webinar_key_points'] ?></p>
				<!--<ul class="detail-list">
					<li>Through their entire lifetime, electric cars are better for the climate.</li>
					<li>Published in the journal Nature Climate Change yesterday.</li>
				</ul>-->
				<hr/>
				<h5>About Author</h5>
				<p style="white-space:break-spaces"><?php echo $webinar_details[0]['webinar_about_author'] ?> </p>
				
				<?php if($disp_webinar_technology != "") { ?>
				<hr/>
				<h5>Webinar Technology</h5>
				<p><?php echo $disp_webinar_technology; ?> </p>
				<?php } ?>
			</div>
		</div>
	</div>
</section>



<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="WebinarShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"><?php echo $webinar_details[0]['registration_link'] ?></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('WebinarShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<script>
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
			
		//alert("Contents copied to clipboard.");
		return false;
	}

	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script type="text/javascript">
	$(document).ready(function () 
	{ 		 
		// Load More Code 
		$(document).on('click','.register-link',function()
		{	
			module_id=28;
			if(check_permissions_ajax(module_id) == false){
             	swal( 'Warning!','You don\'t have permission to access this page !','warning');
             	return false;
			}

			var base_url = '<?php echo base_url(); ?>'; 
			var id = $(this).attr('id');
			var cs_t	= $('.token').val();
			
			$.ajax({
				type:'POST',
				url: base_url+'webinar/addAttendee',
				data:'id='+id+'&csrf_test_name='+cs_t,
				dataType:"text",
				async:false,
				success:function(data)
				{
					$(".register-link").hide();
					//console.log(data);
					var output = JSON.parse(data);					
					$(".token").val(output.token);
					var titles = output.success;	
					var msg = output.msg;
					var type = output.success;

					swal(
					{
						title: titles,
						text: msg,
						type: type,
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function(){ 
   					location.reload();
   				}
   				);
					
					if(output.exclusive_technovuus_event == "No")
					{
						var linkHref = '<?php echo $webinar_details[0]['registration_link'] ?>';
						window.open(linkHref, '_blank');
					}
				}
			});
		});
		
	});
	
	$(document).on('click','.share-url',function(){	  
		$('#share-popup').modal('show');
	});
	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });	
</script>


