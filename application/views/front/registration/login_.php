<div id="home-p" class="home-p loginBanner text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Login </h1>
    </div>
    <!--/end container-->
</div>

  <?php if ($this->session->flashdata('success_message_register') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Registration Successful ! Please Login and Complete your Profile.','success');
                   }, 200);
               </script>
    <?php } ?>

      <?php if ($this->session->flashdata('success_message_cp') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Password Updated Successful ! Please Login.','success');
                   }, 200);
               </script>
    <?php } ?>
    <?php if ($this->session->flashdata('error_is_logged_in') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
                   }, 200);
               </script>
    <?php } ?>




    <section id="registration-form" class="inner-page">
        <div class="container">
         
                <div class="col-md-12">
                 <div class="formInfo col-md-8">

            <?php if ($this->session->flashdata('error_message') != "") 
            { ?>
               <div class="alert alert-danger alert-dismissable">
               <i class="fa fa-ban"></i>
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
            <?php } ?>
            <!-- <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
            <?php } 
            ?> -->
            <?php 
            if ( validation_errors()!="" )
              { ?> 
               <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo validation_errors();  ?>
               </div>
               <?php }
               ?>
                 <form method="POST" id="login_form">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

  <div class="form-group">
    
    <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo set_value('email')?>" required="" >
    <label for="email" class="form-control-placeholder">Email address</label>
  </div>
  <div class="form-group">
    
    <input type="password" class="form-control" name="password" id="password" placeholder="" required="">
    <label for="password" class="form-control-placeholder">Password</label>
  </div>
   
 
   <div class="form-group row captcha">
                <label class="col-lg-4 control-label text-left cptcha_img">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-6">
                  <input type="text" name="code" id="captcha" placeholder="" class="form-control" > 
                    <div class="m-t m-t-mini error" style="color:#F00"> <?php echo form_error('code');?></div>
                  
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
        </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit" >Submit</button>
    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo base_url('login/forgot_password') ?>'">Forgot Password</button> 
  </div>
<!-- <a href="<?php echo $login_url ?>" class="btn btn-info">Login With Google</a> -->
  
</form>
                </div>
    
          
    
    
            </div>
        </div>
    </section>




<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<!-- <script>
jQuery.validator.addMethod("password", function(value, element) {
  return this.optional( element ) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/.test( value );
}, 'Password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.');
</script> -->
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
    form.submit();
    }
  });

  $('#login_form').validate({
    rules: {
 
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
          },
          code: {
            required: true
          },

          
        },
    messages: {
          email: {
            required: "This field is required",
          },
          password: {
            required: "This field is required",
           
          },
         
          code: {
            required: "This field is required",
          },

    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'login/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>

<script src="<?php echo base_url('assets/front/js/'); ?>sha1.min.js"></script>
<script>
  $(document).ready(function(){
    $("#submit").on('click',function(){
 if ($("#login_form").valid()==true) {
      var pswd = $("#password").val();
      
    if (pswd!='') {
            var hash = sha1(pswd);
            $("#password").val(hash);
         }
      
      }

    });
    });
</script>

<script>

$("#membership_type").parent().find('label').addClass('floatinglabel');

  $("input[type='text']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

    $("select").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='password']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='email']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});
</script>




