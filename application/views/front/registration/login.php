<div id="home-p" class="home-p loginBanner text-center d-none" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Login </h1>
    </div>
</div>

<style>
.ordash h4{text-align:center; font-size:20px; margin:15px 0 ;  position:relative; color: #afabab;}
.ordash h4 span{background:#fff;padding:0 10px;z-index:999;position:relative}
.ordash h4::after{border-bottom:2px solid #afabab;position:absolute;content:"";top:10px;width:100%;left:0}
.boxShadow{ box-shadow: .3s ease, border .3s; box-shadow:-5px 10px 15px 5px rgba(46, 61, 73, .3); padding: 25px;   -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; background: #FFF;}
.boxShadow a{ text-decoration: none; color: #000; font-size: 14px; display: inline-block; }
.boxShadow a:hover{ text-decoration: none; color: #c80032;}

.registeredText span{color: #c80032;}
.registeredText a {
    color: #c80032;
}
.text-center.registeredText{display:block; font-size: 14px;}
.loginImg{width:100%; height:100vh; object-fit: cover;}
.back_to_home { float: right;}
.back_to_home a {text-decoration: none;	font-weight: bold; color:#e23751;}
.back_to_home a:hover {text-decoration: none;	font-weight: bold; color:#333}
.cptcha_img{margin:0}
.loginimgTitle { position: relative;}
.loginimgTitle h2 {position: absolute; color: #fff;font-size: 28px; text-align: center; padding: 50px 20px 0; width: 98%;font-weight: 600;}

@media screen and (max-width:767px) {
.loginImg{width:100%;height:auto;object-fit:contain}
.boxShadow {  margin:25px 0; }
.loginimgTitle { padding-right: 0; width:100%; height:45vh}


.loginimgTitle h2{ padding: 20px 10px 0; width: 100%; font-size: 16px;}

}

</style>

  <?php if ($this->session->flashdata('success_message_register') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Registration Successful ! Please Login and Complete your Profile.','success');
                   }, 200);
               </script>
    <?php } ?>

      <?php if ($this->session->flashdata('success_message_cp') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Password Updated Successfully ! Please Login.','success');
                   }, 200);
               </script>
    <?php } ?>

    <?php if ($this->session->flashdata('error_is_logged_in') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
                   }, 200);
               </script>
    <?php } ?>



    <section class="inner-page login-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 pl-0 loginimgTitle" style="background-image: url(<?php echo base_url('assets/setting/'.$content[0]['content_img']) ?>);background-size: 100% 100%; background-repeat: no-repeat">
					       <h2><?Php echo $content[0]['text_content']; ?></h2>
					

                </div>
                <div class="col-md-4 d-flex align-items-center">
                <!-- <span class="back_to_home">Go to Home <a href="<?php echo base_url() ?>" >Home</a></span> -->
                 
                  

                  
                    <div class="w-100 pt-4 pb-4" >
                    <?php if ($this->session->flashdata('error_message') != "") 
            { ?>
               <div class="alert alert-danger alert-dismissable">
               <i class="fa fa-ban"></i>
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
            <?php } ?>
            
            <?php 
            if ( validation_errors()!="" )
              { ?> 
               <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo validation_errors();  ?>
               </div>
               <?php }
               ?>
               <p class="mb-4 enter_login" >Enter your login details</p>
                    <div class="boxShadow">
                    <div class="login-head">
                        <!-- <p>Log in via your social media accounts</p> -->
                        <div class="social-buttons text-center">
                           <a href="<?=$loginURL?>" ><img src="<?php echo base_url('assets/') ?>img/google-login.jpg" alt="google login"></a> 
                           <!--  <a href="#" class="ml-4"><img src="<?php echo base_url('assets/') ?>img/linkedin-button.jpg" alt="linkedin"></a> -->
                         
                        </div>
                        <div class="ordash">
                            <h4><span>Or</span></h4>
                        </div>
                    </div>
                    <form method="POST" id="login_form" novalidate="novalidate">


                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                <div class="form-group">
                  
                  <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo set_value('email')?>" required="" >
                  <label for="email" class="form-control-placeholder">Email address</label>
                </div>
                <div class="form-group">
                  
                  <input type="password" class="form-control" name="password" id="password" placeholder="" required="">
                  <label for="password" class="form-control-placeholder">Password</label>
                    </div>


   <div class="form-group row captcha">
                <label class="col-lg-5 pr-0 control-label text-left cptcha_img" style="transform:translateY(-0px) scale(1)">
                  <span id="captImg"><?php echo $image; ?></span>
                    
                  </label>
                <div class="col-lg-5">
                  <input type="text" name="code" id="captcha" placeholder="" class="form-control" > 
                  
                </div>
                <div class="col-lg-2">
                  <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                </div>
        </div>

                        </div> 

                        <div class="form-group mt-5">
                            <button type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit" >Sign In</button>
                            <a style="color:#000; text-decoration:none; float:right;" href="<?php echo base_url('login/forgot_password') ?>" >Forgot Password ?</a>
                        </div>

                         <?php 
                          /*
                          if($this->session->userdata('sess_logged_in')==0){?>
                            <a href="<?=$loginURL?>"class="waves-effect waves-light btn red"><i class="fa fa-google left"></i>Google login</a>
                          <?php }else{?>
                            <a href="<?=base_url()?>auth/logout" class="waves-effect waves-light btn red"><i class="fa fa-google left"></i>Google logout</a>
                          <?php }
                          */
                          ?>
                          <strong class="text-center mt-4 registeredText">
                            Haven't registered with Technovus yet?  <br><a href="<?php echo base_url('registration') ?>"><span>LET'S GET STARTED NOW!</span></a>
                        </strong>
                        <!-- <a href="" class="btn btn-info">Login With Google</a> -->
                        
                    </form>
                </div>
              
                </div>
               

            </div>
        </div>
    </section>







<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<!-- <script>
jQuery.validator.addMethod("password", function(value, element) {
  return this.optional( element ) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/.test( value );
}, 'Password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.');
</script> -->
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
    form.submit();
    }
  });

  $('#login_form').validate({
    rules: {
 
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
          },
          code: {
            required: true
          },

          
        },
    messages: {
          email: {
            required: "This field is required",
          },
          password: {
            required: "This field is required",
           
          },
         
          code: {
            required: "This field is required",
          },

    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

<script>
         $(document).ready(function(){
             $('.refreshCaptcha').on('click', function(){
                 $.get('<?php echo base_url().'login/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>

<script src="<?php echo base_url('assets/front/js/'); ?>sha1.min.js"></script>
<script>
  $(document).ready(function(){
    $("#submit").on('click',function(){
 if ($("#login_form").valid()==true) {
      var pswd = $("#password").val();
      
    if (pswd!='') {
            var hash = sha1(pswd);
            $("#password").val(hash);
         }
      
      }

    });
    });
</script>

<script>

$("#membership_type").parent().find('label').addClass('floatinglabel');

  $("input[type='text']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

    $("select").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='password']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='email']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});
</script>




