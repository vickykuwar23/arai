<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s">Forgot Password</h1> 
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb wow fadeInUp">
        <li class="breadcrumb-item"><a href="<?php echo site_url('profile'); ?>">Login</a></li>
        <li class="breadcrumb-item active" aria-current="page">Forgot Password</li>
      </ol>
    </nav>
  </div>
</div>


<section id="registration-form" class="inner-page" data-aos="fade-up">
  	<div class="container">
	   <style>
		   .error {
	color: red !important;
	font-size: 13px !important;
}
	   </style>

	    <div class="row">
	    	<div class="col-md-12">
			
	    		<div class="formInfo col-md-8">
				<?php if ($this->session->flashdata('success') != "") { ?>
		        <div class="alert alert-success alert-dismissible">
		            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		            <h5><i class="fa fa-check"></i> Success!</h5>
		            <?php echo $this->session->flashdata('success'); ?>
		        </div>
		        <?php } ?>
		              <?php if ($this->session->flashdata('error') != "") { ?>
		            <div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h5><i class="fa fa-check"></i> Error!</h5>
		                <?php echo $this->session->flashdata('error'); ?>
		            </div>

		        <?php } ?>
		        <?php 
		            if(validation_errors()!="" ){
		                validation_errors();  
		            }
				?>
				

		    		<form method="post" id="forgotPasswordForm" action="<?php echo site_url('login/forgot_password') ?>">
		    			 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		    			<div class="row">
		    				<div class="col-md-12">

			

								<div class="form-group">
									<input type="text" class="form-control" id="email" required="" name="email">
									<label class="form-control-placeholder" for="exampleInputEmail1">Registered Email Address</label>

									<span class="error"><?php echo form_error('email'); ?></span>
								</div> 
	                    	</div>
		    			</div>
		    			
		    			<div class="row">
	                  		<button type="submit" class="btn btn-danger mt-3 ml-3">Submit</button>
	                	</div>
		    		</form>
		    	</div>
	    	</div>
	    </div>
  	</div>
</section>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

	$.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  	});
	
	$('#forgotPasswordForm').validate({
	    rules: {
	      email: {
	        required: true,
	        email : true,
	      },
	    },
	    messages: {
	      email: {
	        required: "Please Enter Email Address",
	        email:"Please Enter Valid Email Address",
	      },
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
  	});
</script>