<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
  <div class="container">
    <h1 class="wow fadeInUp" data-wow-delay="0.1s">Change Your Password</h1> 
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb wow fadeInUp">
        <li class="breadcrumb-item"><a href="<?php echo site_url('profile'); ?>">Login</a></li>
        <li class="breadcrumb-item active" aria-current="page">Change Password</li>
      </ol>
    </nav>
  </div>
</div>

<section id="registration-form" class="inner-page" data-aos="fade-up">
  	<div class="container">
	    <div class="row">
	      <div class="col-md-">
		     
	      </div>
	    </div>

	    <div class="row">
	    	<div class="col-md-12">
	    		<div class="formInfo col-md-6">
				   <?php if ($this->session->flashdata('success') != "") { ?>
			        <div class="alert alert-success alert-dismissible">
			            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			            <h5><i class="fa fa-check"></i> Success!</h5>
			            <?php echo $this->session->flashdata('success'); ?>
			        </div>
			        <?php } ?>
			              <?php if ($this->session->flashdata('error') != "") { ?>
			            <div class="alert alert-danger alert-dismissible">
			                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                <h5><i class="fas fa-check"></i> Error!</h5>
			                <?php echo $this->session->flashdata('error'); ?>
			            </div>

			        <?php } ?>
			        <?php 
			            if(validation_errors()!="" ){
			                validation_errors();  
			            }
			        ?>
		    		<form method="post" id="forgotYourPasswordForm" action="<?php echo site_url('login/storeYourNewPassword') ?>">
		    			<input type="hidden" name="id" id="user_id" value="<?php echo $user_id ?>">
		    			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		    			<div class="row">
		    				<div class="col-md-12">
								<div class="form-group">
						           <div class="input-group" id="show_hide_password"> 
									<input type="password" class="form-control" id="new_password"  name="new_password">
									<label class="form-control-placeholder" for="exampleInputEmail1">New Password</label>
									<div class="input-group-addon">
		                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
		                            </div>
		                          </div>
									<span class="error"><?php echo form_error('new_password'); ?></span>
								</div> 
	                    	</div>
		    			</div>
		    			<div class="row">
		    				<div class="col-md-12">
								<div class="form-group">
									<div class="input-group" id="show_hide_password_confirm"> 
									<input type="password" class="form-control" id="confirm_password"  name="confirm_password">
									<label class="form-control-placeholder" for="exampleInputEmail1">Confirm New Password</label>
									 <div class="input-group-addon">
			                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
			                            </div>
			                          </div>
									<span class="error"><?php echo form_error('confirm_password'); ?></span>
								</div> 
	                    	</div>
		    			</div>
		    			
		    			<div class="row">
	                  		<button type="submit" name="btn_submit" class="btn btn-danger mt-3 ml-3">Submit</button>
	                	</div>
		    		</form>
		    	</div>
	    	</div>
	    </div>
  	</div>
</section>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>

<script src="<?php echo base_url('assets/front/js/'); ?>sha1.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
	
	 jQuery.validator.addMethod("password_complex", function(value, element) {
     return this.optional( element ) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/.test( value );
   }, 'Password must be minimum 8 characters, at least one uppercase letter, one lowercase letter, one number and one special character.');	
	$.validator.setDefaults({
    
	 submitHandler: function (form) {
        

                swal({
                title:'Confirm' ,
                text: "Are you sure you want to update your password ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
                }).then(function (result) {
                if (result.value) {

                   if ($("#forgotYourPasswordForm").valid()==true) {
                    var pswd = $("#new_password").val();
                     if (pswd!='') {
                        var hash = sha1(pswd);
                        $("#new_password").val(hash);
                     }
                
                  }
                 
                 
                  form.submit();
              }
            }); 
       
       }
   })

	 $.validator.setDefaults({
      onkeyup: function () {
        var originalKeyUp = $.validator.defaults.onkeyup;
        var customKeyUp =  function (element, event) {
          if ($("#confirm_password")[0] === element) {
            return false;
          }
          else {
            return originalKeyUp.call(this, element, event);
          }
        }

        return customKeyUp;
      }()
  	});

	
	$('#forgotYourPasswordForm').validate({
	    rules: {
		      new_password: {
		        required: true,
		        password_complex:true,
	          	remote:{
	              url: "<?php echo base_url();?>login/check_pass/",
	              type: "post",
	              data: {
	                 new_password: function() {
	                 return $( "#new_password" ).val();
	                 },
	                 user_id: function() {
	                 return $( "#user_id" ).val();
	                 }
	               
	              }
	          	}
		      },

	      confirm_password: {
	        required: true,
	        password_complex:true,
	        equalTo : "#new_password"
	      },
	    },
	    messages: {
	      new_password: {
	        required: "Please Enter New Password",
	         remote : "Old and new password should not be same"
	      },
	      confirm_password: {
	        required: "Please Enter Confirm Password",
	        equalTo : "Password do not match"
	      },
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
  	});

	});

    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });

    $("#show_hide_password_confirm a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password_confirm input').attr("type") == "text"){
            $('#show_hide_password_confirm input').attr('type', 'password');
            $('#show_hide_password_confirm i').addClass( "fa-eye-slash" );
            $('#show_hide_password_confirm i').removeClass( "fa-eye" );
        }else if($('#show_hide_password_confirm input').attr("type") == "password"){
            $('#show_hide_password_confirm input').attr('type', 'text');
            $('#show_hide_password_confirm i').removeClass( "fa-eye-slash" );
            $('#show_hide_password_confirm i').addClass( "fa-eye" );
        }
    });
</script>