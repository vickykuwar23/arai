<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Pricing Tables </h1>
    </div>
    <!--/end container-->
</div>





    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                 <div class="formInfo">
                  
                    <h4 class="plantext">Individual PLans</h4>  
                    <div class="row">
                        <?php foreach ($plans_ind as $key => $plan) { ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="pricingTable">
                                <div class="pricing-icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="price-Value">
                                    <span class="currency">&#x20B9;</span><?php echo $plan['plan_amount']; ?>/-
                                </div>
                                <span class="month"><?php echo $plan['plan_duration']; ?></span>
                                <div class="pricingHeader">
                                    <h3 class="title"><?php echo $plan['plan_name'] ?></h3>
                                </div>
                                <div class="pricing-content">
                                  <?php echo $plan['plan_desc'] ?>
                                   <!--  <ul>
                                        <li>Accounts &amp; Contact</li>
                                        <li>Task &amp; Event Tracking</li>
                                        <li>Google Apps</li>
                                        <li>Mobile Access</li>
                                        <li>Content library</li>
                                    </ul> -->
                                </div>
                                
                              

                            </div>
                        </div>
                         <?php } ?>
                   
                       
                 </div>
                 <br>   
                 <h4  class="plantext">Organization PLans</h4>  
                    <div class="row">
                        <?php foreach ($plans_org as $key => $plan) { ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="pricingTable">
                                <div class="pricing-icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="price-Value">
                                    <span class="currency">&#x20B9;</span><?php echo $plan['plan_amount']; ?>/-
                                </div>
                                <span class="month"><?php echo $plan['plan_duration']; ?></span>
                                <div class="pricingHeader">
                                    <h3 class="title"><?php echo $plan['plan_name'] ?></h3>
                                </div>
                                <div class="pricing-content">
                                  <?php echo $plan['plan_desc'] ?>
                                   <!--  <ul>
                                        <li>Accounts &amp; Contact</li>
                                        <li>Task &amp; Event Tracking</li>
                                        <li>Google Apps</li>
                                        <li>Mobile Access</li>
                                        <li>Content library</li>
                                    </ul> -->
                                </div>
                                
                              

                            </div>
                        </div>
                         <?php } ?>
                   
                       
                 </div>

                 <div class="row">
                     <div class="col-md-10 pt-5">
                      To register as an Organization, please contact us on our WhatsApp helpline (link & QR Code) or email to us on - <a href="mailto: info@technovuus.araiindia.com">info@technovuus.araiindia.com</a> 
                     Facing issues registering on TechNovuus? Get in touch with our helpdesk.
                  
                     
                   </div>

                    <div class="qr_image col-md-2" >
                    <img class="img-fluid" height="200" src="<?php echo base_url('assets/qrcode.jpg') ?>" alt="">
                         </div> 

                   </div>

              
               
                </div>
    
          
    
    
            </div>
        </div>
    </section>

