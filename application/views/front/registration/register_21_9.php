
<?php 

if ($this->session->flashdata('error_message_register') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Error!','Something went wrong please try again.','error');
                   }, 200);
               </script>
<?php } ?>

<style>
  .ajax-loader {visibility: hidden; background-color: rgba(255,255,255,0.7); position: fixed ; z-index:99999 !important; width:100%; height:100%; top:0px; left:0px; overflow: auto;}

.ajax-loader img {position: relative; top:35%; left:30%; }

.qr_image{
  position: absolute;
  right: 0;
  bottom: 52px;
  max-width: 190px;
}
.otp_msg p {position:relative;top:-1.5em;line-height:1.2;}
@media screen and (max-width:767px) {
    .qr_image{
  position: relative;
  right: 0;
  bottom: 0;
}
    }
</style>
<div id="loader" class="ajax-loader"><img height="300" src="<?php echo base_url('assets/img/Preloader.gif') ?>" alt="" class="img-responsive" ></div>
   <!--====================================================
                       HOME-P
======================================================-->
<div id="home-p" class="home-p registrationBanner text-center">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Registration</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <!-- <li class="breadcrumb-item"><a href="#">Registration</a></li> -->
                <li class="breadcrumb-item active" aria-current="page">Registration </li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>





    <section id="registration-form" class="inner-page" data-aos="fade-down">
        <div class="container">
            <div class="row">
            <div class="col-md-6">
            <?php if ($this->session->flashdata('error_message') != "") 
            { ?>
               <div class="alert alert-danger alert-dismissable">
               <i class="fa fa-ban"></i>
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <b>Alert!</b> <?php echo $this->session->flashdata('error_message'); ?>          
               </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_message') != "") { ?>
               <div class="alert alert-success alert-dismissable">
               <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
               <?php echo $this->session->flashdata('success_message'); ?>          
               </div>
            <?php } 
            ?>
            <?php 
             if(validation_errors()!="" ){
               validation_errors();  
               }
            
               ?>
            </div> 
                <div class="col-md-12">
                 <div class="formInfo">
                      <form method="POST" id="reg_form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                        <div class="form-group">
                          <div class="row">
                                <!--   <div class="col-md-12">

<small> Hello User,<br> 
If you are a domain expert, TechNovuus welcomes you and looks forward to empanelling you on the platform.<br>
Please share your email address along with a brief profile on <a href="mailto:info@technovuus.araiindia.com">info@technovuus.araiindia.com</a>  and we will contact you soon.<br>
Thanks,<br>
TechNovuus Team.<br><br></small>
                              </div> -->
                          </div>
                           <div class="row">
                              <div class="col-md-2">
                                 <label for="category" class="form-control-placeholder redioButon textName pt-3">Select Profile<em class="mandatory">*</em></label>
                              </div>
                              <div class="col-md-5">
                                 <?php foreach ($categories as $key => $category) { ?>
                                 <div class="form-check-inline mr-0">
                                    <label class="form-check-label redioButon button_category">
                                    <input  type="radio" class="form-check-input category_class button_category<?php echo $key ?>" value="<?php echo $category['id'] ?>" <?php echo  set_radio('category', $category['id']); ?> name="category" id="category" ><?php echo $category['user_category'] ?>
                                    </label>
                                 </div>

                                
                                 <?php } ?>
                              <!--    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="<span>dgg</span>Users & experts who wish to participate in challenges, expand their network, access platform resources and collaborate to innovate!"><i class="fa fa-question-circle-o fa-lg" aria-hidden="true"></i>
                                  </a> -->

                                 <div class="error" style="color:#F00"><?php echo form_error('category'); ?> </div>
                              </div>
                        
                           </div>
                           <div class="row">
                            <div class="col-md-12 pt-2">
                              <span id="profile_notice"></span>
                            </div>
                           </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-6">
                           
                           <select class="form-control subcategory" id="subcategory" name="subcategory" required="">
                              <option value=""></option>
                           </select>
                           <label for="subcategory" id="lable_subcat" class="form-control-placeholder">Register As<em class="mandatory">*</em></label>
                           <div class="error" style="color:#F00"><?php echo form_error('subcategory'); ?> </div>
                        </div>

                        <div class="form-group col-md-6 d-none" id="other_sub_cat_div">
                                   <input type="text" name="other_subcat_type" id="other_subcat_type" class="form-control" required="">
                                    <label for="other_subcat_type" class="form-control-placeholder">Other Institution Type  <em class="mandatory">*</em></label>
                                   <div class="error" style="color:#F00"><?php echo form_error('other_subcat_type'); ?> </div>
                            </div>
                       
                        </div>

                       
      
                        <div id="paid_organization" class="d-none">
                                    
                                    <div class="form-group d-none" id="Are_you_Organization_div">
                                          <div class="row">
                                             <div class="col-md-3">
                                                <label for="Are_you_Organization"> Are you Organization or Individual?</label>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="form-check-inline">
                                                   <label class="form-check-label">
                                                   <input type="radio" class="form-check-input" value="Organisation" name="Are_you_Organization" <?php echo  set_radio('Are_you_Organization', 'Organisation'); ?>>Organisation</label>
                                                     <div class="error" style="color:#F00"><?php echo form_error('Are_you_Organization'); ?> </div>
                                                </div>
                                                <div class="form-check-inline">
                                                   <label class="form-check-label">
                                                   <input type="radio" class="form-check-input" value="Inividual" name="Are_you_Organization" <?php echo  set_radio('Are_you_Organization', 'Inividual'); ?>>Inividual</label>
                                                     <div class="error" style="color:#F00"><?php echo form_error('Are_you_Organization'); ?> </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                     

                                        <div class="row">
                                           <!--  <div class="col-md-md-6">
                                            <div class="form-group">
                                               
                                                <label for="exampleInputEmail1">Domain / Area of Expertise</label>
                                                <select name="domain_and_area_of_expertise[]" class="domainName form-control" autocomplete="nope" multiple="multiple" required id="domain_area_of_expertise">
                                                    <?php foreach ($domain as $key => $value) { ?>
                                                        <?php if(in_array($area_of_interest[$key]['id'], $Domain)) { ?>
                                                            <option value="<?php echo $domain[$key]['id'] ?>" selected="selected"><?php echo $domain[$key]['domain_name'] ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $domain[$key]['id'] ?>"><?php echo $domain[$key]['domain_name'] ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <span class="error"><?php echo form_error('domain_and_area_of_expertise'); ?></span>
                                            </div>
                                        </div> -->
                                         <!-- <div class="form-group col-md-6" id="institution_type_div">

                                             <select class="form-control" id="institution_type" multiple=""  name="institution_type[]" required="">
                                              <option value="">Selct Institution Type</option>
                                              <?php foreach ($institution as $key => $institute) { ?>
                                              <option value="<?php echo $institute['id'] ?>"><?php echo $institute['institution_name'] ?></option>
                                            <?php } ?>
                                             </select>
                                             <label for="institution_type" class="form-control-placeholder leftLable">Institution Type<em>*</em></label>

                                            
                                               <div class="error" style="color:#F00"><?php echo form_error('institution_type'); ?> </div>
                                          </div>  -->
                                          <div class="form-group col-md-6" id="institution_full_name_div">
                                             <input type="text" name="institution_full_name" id="institution_full_name" class="form-control" required="">
                                              <label for="institution_full_name" class="form-control-placeholder">Institution Full Name  <em class="mandatory">*</em></label>
                                             <div class="error" style="color:#F00"><?php echo form_error('institution_full_name'); ?> </div>
                                          </div>
                     
                                           <div class="form-group d-none" id="Person_Full_Name_div">
                                             <input type="text" name="Person_Full_Name" id="Person_Full_Name" class="form-control" required="">
                                             <label for="Person_Full_Name" class="form-control-placeholder">Person Full Name  <em>*</em></label>
                                             <div class="error" style="color:#F00"><?php echo form_error('Person_Full_Name'); ?> </div>
                                          </div>
                                          </div>
                
                                        
                                         <div class="row">
                                       
                                          <div class="form-group col-md-6 d-none" id="other_institution_div">
                                             <input type="text" name="other_institution_type" id="other_institution_type" class="form-control" required="">
                                              <label for="other_institution_type" class="form-control-placeholder">Other Institution Type  <em class="mandatory">*</em><em>*</em></label>
                                             <div class="error" style="color:#F00"><?php echo form_error('other_institution_type'); ?> </div>
                                          </div>
                     
                                           
                                          </div>



                                     <div class="row">
                                          <div class="form-group col-md-6">
                                                        <label for="domain_industry" class="form-control-placeholder leftLable">Domain/ Industry & Sector <em class="mandatory">*</em></label>
                                             <select class="form-control" multiple="" id="domain_industry" name="domain_industry[]" required="">
                                              <!-- <option value="">Select Domain/ Industry & Sector</option> -->
                                              <?php foreach ($domain as $key => $dom) { ?>
                                                <?php if ($dom['id']!='1'): ?>
                                              <option value="<?php echo $dom['id'] ?>"><?php echo $dom['domain_name'] ?></option>
                                              <?php endif ?>
                                              <?php } ?>
                                             </select>
                                             <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>

                                  
                                             <div class="error" style="color:#F00"><?php echo form_error('domain_industry'); ?> </div>
                                          </div>

                                          <div class="form-group col-md-6 d-none" id="other_domain_industry_div">
                                             <input type="text" name="other_domain_industry" id="other_domain_industry" class="form-control" required="">
                                              <label for="other_domain_industry" class="form-control-placeholder">Other Domain/ Industry & Sector <em class="mandatory">*</em></label>
                                             <div class="error" style="color:#F00"><?php echo form_error('other_domain_industry'); ?> </div>
                                          </div>
                                      

                                         




                                          </div>

                                          <div class="row">
                                         <div class="form-group col-md-6" id="public_prvt_div">
                                             <select class="form-control" id="public_prvt" name="public_prvt" required="">
                                              <option value=""></option>
                                                <?php foreach ($public_status as $key => $public) { ?>
                                                  <?php if ($public['id']!='1'): ?>
                                                      <option value="<?php echo $public['id'] ?>"><?php echo $public['public_status_name'] ?></option>
                                                  <?php endif ?>
                                              
                                                <?php } ?>
                                             </select>
                                              <label for="public_prvt" class="form-control-placeholder">Status <em class="mandatory">*</em></label>
                                             <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                             
                                             <div class="error" style="color:#F00"><?php echo form_error('public_prvt_div'); ?> </div>
                                          </div>
                                          

                                          <div class="form-group col-md-6 d-none" id="other_public_prvt_div">
                                             <input type="text" name="other_public_prvt" id="other_public_prvt" class="form-control" required="">
                                              <label for="other_public_prvt" class="form-control-placeholder">Other  status  <em class="mandatory">*</em></label>
                                             <div class="error" style="color:#F00"><?php echo form_error('other_public_prvt'); ?> </div>
                                          </div>
                     
                                           
                                          </div>

                                       

                                          
                                  </div>
                          <div class="row" id="spoc_details_lable">
                            <div class="col-md-12"><h3>Representative Details</h3></div>
                          </div>
                            <div class="row">
                           <div class="col-md-2">
                              <div class="form-group">
                                 
                               <select class="form-control" id="title" name="title" required="">
                                  <option value=""></option>
                                  <option value="Mr." <?php echo  set_select('title', 'Mr.'); ?>>Mr.</option>
                                  <option value="Mrs." <?php echo  set_select('title', 'Mrs.'); ?>>Mrs.</option>
                                  <option value="Miss." <?php echo  set_select('title', 'Miss.'); ?>>Miss.</option>
                                  <option value="Dr." <?php echo  set_select('title', 'Dr.'); ?>>Dr.</option>
                                  <option value="Prof." <?php echo  set_select('title', 'Prof.'); ?>>Prof.</option>
                               </select>
                                 <label id="lable_title" class="form-control-placeholder" for="title">Title<em class="mandatory">*</em></label>
                                 <div class="error" style="color:#F00"><?php echo form_error('title'); ?> </div>
                              </div>
                           </div>
                           
                           <div class="col-md-3">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="first_name" id="first_name" placeholder="" value="<?php echo set_value('first_name')?>" required="">
                                 <label id="lable_fname" class="form-control-placeholder" for="first_name">First Name<em class="mandatory">*</em></label>
                                  <div class="error" style="color:#F00"><?php echo form_error('first_name'); ?> </div>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                
                                 <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="" value="<?php echo set_value('middle_name')?>" >
                                  <label id="lable_mbame" class="form-control-placeholder" for="middle_name">Middle Name</label>
                                 <div class="error" style="color:#F00"><?php echo form_error('middle_name'); ?> </div>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 
                                 <input type="text" class="form-control" name="last_name" id="last_name" placeholder="" value="<?php echo set_value('last_name')?>" required="">
                                 <label id="lable_lname" for="last_name" class="form-control-placeholder" >Last Name<em class="mandatory">*</em></label>
                                 <div class="error" style="color:#F00"><?php echo form_error('last_name'); ?> </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                 
                                 <select class="form-control" id="gender" name="gender" required="">
                                    <option value=""></option>
                                    <option value="Male" <?php echo  set_select('gender', 'Mr.'); ?>>Male</option>
                                    <option value="Female" <?php echo  set_select('gender', 'Mrs.'); ?>>Female</option>
                                    <option value="Other" <?php echo  set_select('gender', 'Miss.'); ?>>Other</option>
                                 </select>
                                 <label id="lable_title" class="form-control-placeholder" for="title">Gender<em class="mandatory">*</em></label>
                                 <div class="error" style="color:#F00"><?php echo form_error('title'); ?> </div>
                              </div>
                           </div>
                        </div>

                          <div id="employement_status_div">
                              <div class="row">
                                        <div class="col-md-6">                               
                                            <div class="form-group">
                                                
                                        <select class="form-control" name="employement_status" id="employement_status" >
                                          <option value=""></option>
                                        <?php foreach ($employement_status as $key => $status_value) { ?>
                                            <option value="<?php echo $status_value['name'] ?>"  ><?php echo $status_value['name'] ?></option>
                                        <?php  } ?>

                                        </select>
                                         <label class="form-control-placeholder leftLable2" for="cname">Professional Status
                                            <em>*</em></label>
                                        <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                       
                                        <span class="error"><?php echo form_error('employement_status'); ?></span>
                                            </div>
                                        </div>

                                    
                                         <div class="col-md-6">
                                            <div class="form-group d-none" id="other_emp_status_div">
                                                   <input type="text" name="other_emp_status"  id="other_emp_status" class="form-control" required="">
                                                    <label for="other_emp_status" class="form-control-placeholder">Other Professional Status  <em class="mandatory">*</em></label>
                                                   <div class="error" style="color:#F00"><?php echo form_error('other_emp_status'); ?> </div>
                                          </div>
                                        </div>
                                        
                                       
                                    </div>

                                    </div>


                        <div class="row"> 
                         <div class="form-group col-md-2">
                         
                           <select class="form-control country_code" id="country_code" name="country_code" required="">
                              <!-- <option value="">Country Code</option> -->
                               <?php foreach ($country_codes as $key => $code) { ?>
                                 <option <?php if($code['id']==99) echo 'selected' ?> value="<?php echo $code['id'] ?>"><?php echo $code['iso']." ".$code['phonecode'] ?></option>
                              <?php } ?>
                           </select>
                          <!-- <label for="country_code" class="form-control-placeholder"><em class="mandatory">*</em></label> -->
                          <label class="form-control-placeholder leftLable2" for="cname">Country
                                            <em>*</em></label>

                          
                           <div class="error" style="color:#F00"><?php echo form_error('country_code'); ?> </div>
                        </div>
                        
                        <div class="form-group col-md-4">
                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="" value="<?php echo set_value('mobile')?>" required="" maxlenght="10">
                            <label for="mobile" id="label_mobile" class="form-control-placeholder">Phone Number<em class="mandatory">*</em></label>
                           <div class="error" style="color:#F00"><?php echo form_error('mobile'); ?> </div>
                           <span id="mobile_exist" class="error"></span>
                        </div>
                        
                       <!--  <div class="form-group col-md-3 mt-4">
                           <button type="button"  class="btn btn-primary btn-sm" id="send_OTP">Send OTP</button>
                           <div class="d-none" id="timer_div_otp">Resend In <span id="timer_otp"></span></div>
                        </div>
                        <div class="form-group col-md-2">
                           <input type="text" class="form-control" name="otp" id="otp" placeholder="" value="<?php echo set_value('mobile')?>" required="">
                           <label for="otp" class="form-control-placeholder">Enter OTP</label>

                           <div class="error" style="color:#F00"><?php echo form_error('otp'); ?> </div>
                        </div> -->
                        
                        </div>

                       
                   
                  
                           <div class="row">
                        <div class="form-group col-md-6">
                           
                           <input type="email" class="form-control" name="email" id="email" autocomplete="off"  placeholder="" value="<?php echo set_value('email')?>" required="">
                           <label for="email" id="label_email" class="form-control-placeholder">Email address<em class="mandatory">*</em></label>
                           <br>
                           <!-- <span><small>(Please login to your mail account and retrieve the OTP. If you have not received the OTP, Please check your Junk/Spam folder)</small></span> -->
                          
       
                           <div class="error" style="color:#F00"><?php echo form_error('email'); ?> </div>
                        </div>
                        <div class="form-group col-md-3 mt-4">
                           <button type="button"  class="btn btn-primary btn-sm" id="send_verification_code">Send Verification Code</button>
                           <div class="d-none" id="timer_div">Resend In <span id="timer"></span></div>
                        </div>
                         <div class="form-group col-md-2">
                          <div class="input-group">
                           <input type="text" class="form-control" name="verification_code" id="verification_code" placeholder="" value="<?php echo set_value('mobile')?>" required="">
                           <label for="verification_code" class="form-control-placeholder">Enter Code<em class="mandatory">*</em></label>
                           <div class="input-group-addon d-none" id="code_check_success">
                            <i class="fa fa-check" style="color:green;" aria-hidden="true"></i>
                            </div>
                            <div class="input-group-addon d-none" id="code_check_failed">
                            <i class="fa fa-cross" style="color:red;" aria-hidden="true"></i>
                            </div>
                          </div>

                           <div class="error" style="color:#F00"><?php echo form_error('verification_code'); ?> </div>
                        </div>
                        </div>

                         <div class="row otp_msg mt-0 pt-0">
                          <div class="col-md-6 offset-md-6">
                             <span>
                          
                              <small> <p>Please enter any of the Verification Code received either on Mobile Or Email Address(In Case you have not received email OTP, please check your Junk/Spam folder)</small></p></span>
                              

                          </div>
                        </div>

                        <div class="row">
                          <div class="form-group col-md-6" >
                           <div class="input-group" id="show_hide_password"> 
                           <input type="password" class="form-control" name="password" id="password" placeholder="" required="">
                           <label for="password" class="form-control-placeholder">Password<em class="mandatory">*</em></label>

                            <div class="input-group-addon">
                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            </div>
                          </div>

                            

                           <div class="error" style="color:#F00"><?php echo form_error('password'); ?> </div>
                        </div>
                        <div class="form-group col-md-6">
                          
                           <div class="input-group" id="show_hide_password_confirm"> 
                           <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="" required="">
                           <label for="confirm_password" class="form-control-placeholder">Confirm Password<em class="mandatory">*</em></label>
                           
                            <div class="input-group-addon">
                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            </div>
                          </div>

                        </div>
                        </div>

                    <div id="student_div" >

                    


                       <div class="row">
                        <div class="form-group col-md-6 d-none">
                           <select class="form-control" id="membership_type" name="membership_type" required="">
                            <option value=""></option>
                              <option id="membership_free" value="Free" <?php echo  set_select('membership_type', 'Free'); ?>>Free</option>
                               <option id="membership_paid" value="Paid" <?php echo  set_select('membership_type', 'Paid'); ?>>Paid</option> 
                           </select>
                           <label for="membership_type" class="form-control-placeholder floatinglabel">Membership Type<em class="mandatory">*</em></label>
                            <div class="error" style="color:#F00"><?php echo form_error('membership_type'); ?> </div>
                        </div>
                        
                     
                          </div>
                      
                     
                   


                      
                        </div>
                    <!--     <div class="row d-none" id="plans_div">
                           <div class="col-md-2">
                           <a href="<?php echo base_url('registration/plans') ?>" target="_blank" class="btn btn-primary mt-2 btn-sm">View Plans</a>
                         </div>
                        </div> -->
                        <div class="row">
                        
                      
                          
                         <div class="form-group captcha col-md-2">
                           <label class="control-label text-left cptcha_img">
                            <span id="captImg"><?php echo $image; ?></span>
                           </label>
                         </div>
                         <div class="form-group captcha col-md-5">
                       
                              <input type="text" name="code" id="captcha" placeholder="" class="form-control" required=""> 
                               <label for="password" class="form-control-placeholder">Captcha<em class="mandatory">*</em></label>
                              <div class="error"> <?php echo form_error('code');?></div>
                        </div>
                         <div class="form-group captcha col-md-1">
                            <a style="color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fa fa-refresh"></i></a> 
                         </div>
                        
                       


                        </div>

                        <div class="form-group">
                           <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" name="terms_condition" id="terms_condition" name="example1">
                              <label class="custom-control-label form-control-placeholder" for="terms_condition"><a href="<?php echo base_url('home/termsOfuse')?>" target="_blank">Read the terms and conditions</a> <em class="mandatory">*</em></label>
                               <div class="error" style="color:#F00"><?php echo form_error('terms_condition'); ?> </div>
                           </div>
                        </div>
                        <!--  <div class="form-group d-none" id="subscription_fee_div">
                           <input type="button" class="btn btn-primary" name="subscription_fee"  id="subscription_fee" placeholder="" value="Planls">
                        </div> -->
                     
                        <div class="form-group">
                           <input type="hidden" name="submit_button" value="submit_button">
                           <button type="submit" class="btn btn-primary" name="btn_submit" value="btn_submit" id="btn_submit" >Submit</button> 
                        </div>

                      <div class="row">
                   <div class="col-md-12">
                     Facing issues registering on TechNovuus? Get in touch with our helpdesk by clicking on the link or scanning the QR Code.
                    <a href="https://wa.me/message/BWX6SYO2JAXDH1" target="_blank">https://wa.me/message/BWX6SYO2JAXDH1</a>
                     
                   </div>

                     <div class="qr_image" >
                    <img class="img-fluid" height="200" src="<?php echo base_url('assets/qrcode.jpg') ?>" alt="">
                         </div> 
                 </div>

                   </form>
                 
                 </div>
                </div>
            </div>
        </div>
    </section>
<!-- End #main -->
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
 <script>
   jQuery.validator.addMethod("password_complex", function(value, element) {
     return this.optional( element ) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/.test( value );
   }, 'Password must be minimum 8 characters, at least one uppercase letter, one lowercase letter, one number and one special character.');
  
     $.validator.addMethod('lowercasesymbols', function(value) {
    return value.match(/^[^A-Z]+$/);
    }, 'You must use only lowercase letters in email');

     $.validator.addMethod('NotAllZero', function(value) {
    return value.match(/^(?!0*$).*$/);
    }, 'Invalid Mobile Number');

    $.validator.addMethod("valid_email", function(value, element) 
    { 
      var email = value;
      var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
      var result = pattern.test(email); 
      if(result){return true;}else {return false;}
    });

     
   </script>
<script src="<?php echo base_url('assets/front/js/'); ?>sha1.min.js"></script>
 <script type="text/javascript">
   $(document).ready(function () {
     $.validator.setDefaults({
       submitHandler: function (form) {
            // form.submit();
         var membership_type = $("#membership_type").val();
          var category= $("input:radio[name=category]:checked").val();
           var label= 'Yes!';
          if (membership_type=='Paid' || category==2)  {
            var text = 'Please ensure all details entered are correct and you have a promo code before proceeding.';
            var label= 'Proceed';
          }else{
            var text = 'Are you sure you want to submit the form ?';
            var label= 'Yes!';
          }

                swal({
                title:'Confirm' ,
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: label
                }).then(function (result) {
                if (result.value) {

                   if ($("#reg_form").valid()==true) {
                    var pswd = $("#password").val();
                     if (pswd!='') {
                        var hash = sha1(pswd);
                        $("#password").val(hash);
                     }
                
                  }
                 
                 
                  form.submit();
              }
            }); 
       
       }
     });
   
    
    var category_id= $("input:radio[name=category]:checked").val();
    var sub_category_id= $("#subcategory").val();
     $('#reg_form').validate({

       rules: {
         
            // studen form
            category: {
               required: true,
             },
             subcategory: {
               required: true,
             },
             other_subcat_type:{
                required: function() {
                    return $("input:radio[name=category]:checked").val() == 2 && $("#subcategory").val() == 10;  
                  },
                maxlength:100,  
              },

            employement_status: {
                required: function() {
                  return $("input:radio[name=category]:checked").val() == 1 || $("input:radio[name=category]:checked").val() == undefined;   
                }
            },
           
            other_emp_status:{
                required: function() {
                    return $("input:radio[name=category]:checked").val() == 1 && $("#employement_status").val() == "Other";  
                  },
                normalizer: function(value) {
                return $.trim($("#other_emp_status").val());
                }
              },

             // membership_type: {
             //   required: function() {
             //      return $("input:radio[name=category]:checked").val() == 1 || $("input:radio[name=category]:checked").val() == undefined;   
             //    }
               
             //  },


               title: {
               required: true
               
              },
             first_name: {
               required: true,
              normalizer: function(value) {
                return $.trim($("#first_name").val());
             }
              
             },
              // middle_name: {
              //  required: function() {
              //    return $("input:radio[name=category]:checked").val() == 1 || $("input:radio[name=category]:checked").val() == undefined;  
              //     } 
               
              // },
              last_name: {
               required: true,
                normalizer: function(value) {
                return $.trim($("#last_name").val());
                }

               
              },

              country_code:{
               required:true,
              },
        
              mobile: {
                  required: true,
                  normalizer: function(value) {
                    return $.trim($("#mobile").val());
                  },
                  number: function() {
                    return true;  
                  },
                  minlength: function() {
                    return 10;  
                  },
                  maxlength: function() {
                    return 10;  
                  },
                  NotAllZero:true,
                  remote: {
                    url: "<?php echo base_url();?>registration/unique_mobile_ajax/",
                    type: "post",
                    async:false,

              },

              },
             // otp: {
             //   required:true,
             //   normalizer: function(value) {
             //        return $.trim($("#mobile").val());
             //      },
             //   remote:{
             //      url: "<?php echo base_url();?>registration/verify_otp/",
             //      type: "post",
             //      data: {
             //         mobile: function() {
             //         return $( "#mobile" ).val();
             //         },
             //         otp: function() {
             //         return $( "#otp" ).val();
             //         },
             //      }
             //   },

             // },


             email: {
              valid_email:true,
              required: true,
               normalizer: function(value) {
                    return $.trim($("#email").val());
                },
               email: true,

               remote: {
                         url: "<?php echo base_url();?>registration/unique_email_ajax",
                         type: "post"
               },
               lowercasesymbols : true,
             },

              verification_code: {
               required:true,
               normalizer: function(value) {
                    return $.trim($("#verification_code").val());
                  },
               remote:{
                  url: "<?php echo base_url();?>registration/verify_verification_code/",
                  type: "post",
                  data: {
                     email: function() {
                     return $( "#email" ).val();
                     },
                     mobile: function() {
                     return $( "#mobile" ).val();
                     },
                     verification_code: function() {
                     return $( "#verification_code" ).val();
                     },
                  },
                  complete: function(data){
                          if( data.responseText == "true" ) {
                            $("#code_check_success" ).removeClass('d-none');
                            $("#code_check_failed" ).addClass('d-none');
                            }else{
                             $("#code_check_success" ).addClass('d-none');
                             $("#code_check_failed" ).removeClass('d-none');
                            }
                       }

               },

             },


              //  are_you_student: {
              //  required: function() {
              //  return $("input:radio[name=category]:checked").val() == 1 || $("input:radio[name=category]:checked").val() == undefined;  
              //  }
               
              // },
              
             // end studen form
             
             // institution_type: {
             //   required: function() {
             //   return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
             //   },
             //   normalizer: function(value) {
             //        return $.trim($("#institution_type").val());
             //    }
               
             //  },
          
              // institution_full_name: {
              //  required: function() {
              //  return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
              //  }
               
              // },
              institution_full_name: {
               required: function() {
               return ($("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() == '') || ($("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9) || ($("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() == 9 && $("input:radio[name=Are_you_Organization]:checked").val()=='Organisation');
               },
               normalizer: function(value) {
                    return $.trim($("#institution_full_name").val());
                },
               
              },
              domain_industry: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 ;  
               },
              
               
              },
              public_prvt: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
               },
               
               
              },
              public_prvt: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
               },
               
               
              },
              name_of_spoc: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 ;  
               },
                            
              },

             // SPOC_Phone_Number: {
             //   required: function() {
             //   return $("input:radio[name=category]:checked").val() == 2;  
             //   },
             //   number: function() {
             //   return true;  
             //   },
             //   minlength: function() {
             //   return 10;  
             //   },
             //   maxlength: function() {
             //   return 10;  
             //   }
            
             // },
             // SPOC_Email_ID: {
             //  required: function() {
             //  return $("input:radio[name=category]:checked").val() == 2;  
             //   },
             //   email: function() {
             //   return true;  
             //   }
             // },
              // want_to_buy_User_Bundle: {
              //  required: function() {
              //  return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
              //  }
               
              // },

             // have_promo_code: {
             //   required: function() {
             //   return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() != 9;  
             //   }
               
             //  },

             Are_you_Organization: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() == 9;  
               }
               
              },

             Person_Full_Name: {
               required: function() {
               return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() == 9 && $("input:radio[name=Are_you_Organization]:checked").val()=='Inividual';  
               }
               
              },
             // institution_full_name: {
             //   required: function() {
             //   return $("input:radio[name=category]:checked").val() == 2 &&  $("#subcategory").val() == 9 && $("input:radio[name=Are_you_Organization]:checked").val()=='Organisation';  
             //   }
               
             //  },

             // username: {
             //   required: true,
             // },
             password: {
              password_complex : true,
               required: true,
             },

             confirm_password : {
                    equalTo : "#password"
                },

             
             terms_condition: {
               required: true,
             },

             code: {
               required: true,
                remote: {
                         url: "<?php echo base_url();?>registration/check_captcha_ajax",
                         type: "post",
                         async:false,

               },
             },


           },
       messages: {
         category: {
               required: "This field is required",
             },
          subcategory: {
               required: "This field is required",
             },
             // membership_type: {
             //   required: "This field is required",
             // },
              title: {
               required: "This field is required",
             },
             first_name: {
               required: "This field is required",
             },
             middle_name: {
               required: "This field is required",
             },
               last_name: {
               required: "This field is required"
             },
             mobile: {
               required: "This field is required",
               remote : "Mobile already exist."
             },
              otp: {
               required: "This field is required",
               remote : "Invalid OTP."
             },

             verification_code: {
               required: "This field is required",
               remote : "Invalid verification code."
             },
             email: {
               required: "This field is required",
               valid_email:"Please enter valid email address.",
               remote:"Email already exist"
             },
             // are_you_student: {
             //   required: "This field is required",
             // },

             // institution_type: {
             //   required: "This field is required",
             // },
              institution_full_name: {
               required: "This field is required",
             },
              domain_industry: {
               required: "This field is required",
             },
              public_prvt: {
               required: "This field is required",
             },
              public_prvt: {
               required: "This field is required",
             },
              name_of_spoc: {
               required: "This field is required",
             },
             //  SPOC_Phone_Number: {
             //   required: "This field is required",
             // },
             //  SPOC_Email_ID: {
             //   required: "This field is required",
             // },
             //  want_to_buy_User_Bundle: {
             //   required: "This field is required",
             // },
             //  have_promo_code: {
             //   required: "This field is required",
             // },
             
             
             password: {
               required: "This field is required",
              
             },
              confirm_password: {
               equalTo: "Password does not match",
              
             },
             
               // username: {
               //   required: "This field is required",
               // },
             terms_condition: {
               required: "This field is required",
             },
             
             code: {
               required: "This field is required",
               remote:"Invalid captch"
             },
   
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
       },
       highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
       },
       unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
       }
     });
   });
</script>

<script type="text/javascript">
   
$(window).on('load', function () {
      var category= $("input:radio[name=category]:checked").val();
      if (category==2) {
        
        $("#plans_div").removeClass("d-none");
         $("#paid_organization").removeClass("d-none").show();
         $("#student_div").addClass("d-none").show();
         $("#employement_status_div").addClass("d-none").show();

           $("#profile_notice").html('Organizational account operated and administered by an authorized representative.<br><strong>  "<i>This form is to be filled exclusively by authorized representatives of organizations, and a promo code will be needed to complete the registration. Please contact <a href="mailto: info@technovuus.araiindia.com">info@technovuus.araiindia.com</a> OR the helpdesk below for more details</i>"</strong>');
         
      }else{
        // $(".button_category0").prop("checked", true);
        $("#plans_div").addClass("d-none");
        $(".button_category0").attr('checked', 'checked');
         $("#spoc_details_lable").addClass("d-none")

         $('.button_category').removeClass("active_radio");
        $('.button_category0').parents('.button_category').addClass( "active_radio" );
        category=1;
        $("#student_div").removeClass("d-none").show();
        $("#employement_status_div").removeClass("d-none").show();
        $("#paid_organization").addClass("d-none");
        
         $("#profile_notice").html("Users & experts who wish to participate in challenges, expand their network, access platform resources and collaborate to innovate!");
      }
      if (category != undefined) {   
      
      var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
      var setSelect = '<?php echo $this->input->post('subcategory') ?>';
      var datastring='cat_id='+ category + '&' + csrfName + '='+csrfHash + '&setSelect='+setSelect;
      $.ajax({
        type: 'POST',
        data:datastring,
        async: false, 
        url: "<?php echo base_url();?>registration/get_sub_category",
        success: function(res){ 
          var json = $.parseJSON(res);
          $('#subcategory').html(json.str);
          //csrfName=json.name;
          //csrfHash=json.value;
        }
      });


    }
   
 });
   
$(document).ready(function () {

  // $('#institution_type').append('<option value="Other"> Other </option>');
  $('#domain_industry').append('<option value="1"> Other </option>');
  $('#public_prvt').append('<option value="1"> Other </option>');

  // $('#institution_type').change(function(){
  //   if ($(this).val()=='Other') {
  //     $('#institution_type').val();
  //     $('#other_institution_div').removeClass('d-none');
  //   }else{
  //     $('#other_institution_div').addClass('d-none');
  //   }
  // });

  // $('#domain_industry').change(function(){
  //   if ($(this).val()=='1') {
  //     $('#other_domain_industry_div').removeClass('d-none');
  //   }else{
  //     $('#other_domain_industry_div').addClass('d-none');
  //   }
  // });

     $('#domain_industry').on('change', function() {
                var selected = $(this).find('option:selected', this);
                var results = [];

                selected.each(function() {
                    results.push($(this).val());
                });

                var display_flag=0;
                $.each(results,function(i){
                   if(results[i]=='1')
                   {
                      display_flag = 1;
                   }
                  
                });

                 if (display_flag==1) {
                    $('#other_domain_industry_div').removeClass('d-none');
                 }else{
                    $('#other_domian').val('');
                    $('#other_domain_industry_div').addClass('d-none');
                 }

                //  $.each(results,function(i){
                //    if(results[i]=='1')
                //    {
                //      $('#other_domain_industry_div').removeClass('d-none');
                //    }else{
                //     $('#other_domian').val('');
                //      $('#other_domain_industry_div').addClass('d-none');
                //    }
                  
                // });

            });

  $('#public_prvt').change(function(){
    if ($(this).val()=='1') {
      $('#other_public_prvt_div').removeClass('d-none');
    }else{
      $('#other_public_prvt_div').addClass('d-none');
    }
  });


    $("#email").on("input", function() {
      $("#verification_code").val('');
       $("#code_check_success" ).addClass('d-none');
         $("#code_check_failed" ).addClass('d-none')
    });

      $('#employement_status').change(function(){
            if ($(this).val()=='Other') {
              $('#other_emp_status_div').removeClass('d-none');
            }else{
              $("#other_emp_status").val('');  
              $('#other_emp_status_div').addClass('d-none');
            }
        });


   var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
   
 // $('input').click(function() {
 //                $('input:not(:checked)').parent().removeClass("active");
 //                $('input:checked').parent().addClass("active");
 //  });


    // $('.category_class').click(function(){
    //     // $('.button_category').removeClass('active_radio');
    //     $('.button_category').parent().find('label').addClass('active_radio');
    //     // $('.button_category').addClass('active_radio').closest('').removeClass('active_radio');       
    // });


   $('input[type=radio][name=category]').change(function() {

      $('.button_category').removeClass("active_radio");
      $(this).parents('.button_category').addClass( "active_radio" );


        var validator = $("#reg_form").validate();

       //  $("#formID input[type=text]").val("");
       //  $("#formID select").prop("selectedIndex", 0);
       //  $("#formID input[type=radio]").prop("checked", false);
       // $("#formID input[type=checkbox]").prop("checked", false);

        $('input:text').val(''); 
        $('#email').val(''); 
        $('#password').val(''); 
        $('#confirm_password').val(''); 
        $('#captcha').val(''); 
        $('#code_check_success').addClass("d-none");
        $('#code_check_failed').addClass("d-none");
        
        $('#title').prop("selectedIndex", 0);
        $('#gender').prop("selectedIndex", 0);
        // $('#country_code').prop("selectedIndex", 0);
        $('#send_verification_code').prop('disabled',true)
        $('#send_verification_code').text('Send Verification Code');

        $('#send_OTP').prop('disabled',true)
        $('#send_OTP').text('Send Verification Code');
        
        $('#timer_div').addClass('d-none');
         $("#other_sub_cat_div").addClass("d-none");
        // $('input:password').val(''); 
        // $("select").prop("selectedIndex", 0);
        // $("#reg_form")[0].reset()

         validator.resetForm();
       if ($(this).val()==1) {

        $("#plans_div").addClass("d-none");


$("#profile_notice").html("Users & experts who wish to participate in challenges, expand their network, access platform resources and collaborate to innovate!");
          // $("#label_email").text('Email address ');
          // $("#label_mobile").text('Phone Number');

          //  $("#lable_title").html('Title');
          // $("#lable_fname").html('First Name');
          // $("#lable_mbame").html('Middle Name');
          // $("#lable_lname").html('Last Name');

          $("#student_div").removeClass("d-none").show();
          $("#employement_status_div").removeClass("d-none").show();
          $("#paid_organization").addClass("d-none");
         // $("#have_promo_code_div").addClass("d-none");
         // $("#lable_subcat").text('Last Name');
          $("#lable_subcat").html('Register As<em class="mandatory">*</em>');
          $("#spoc_details_lable").addClass("d-none")



       }else if($(this).val()==2){
                $("#plans_div").removeClass("d-none");

                $("#profile_notice").html('Organizational account operated and administered by an authorized representative.<br><strong>  "<i>This form is to be filled exclusively by authorized representatives of organizations, and a promo code will be needed to complete the registration. Please contact <a href="mailto: info@technovuus.araiindia.com">info@technovuus.araiindia.com</a> OR the helpdesk below for more details</i>"</strong>');
         // $("#label_email").text('Email address ');
          // $("#label_mobile").text('Phone Number');
          
          // $("#lable_title").text('Title');
          // $("#lable_fname").text('First Name');
          // $("#lable_mbame").text('Middle Name');
          // $("#lable_lname").text('Last Name');


          $("#lable_subcat").html('Institution Type<em class="mandatory">*</em>');


          $("#paid_organization").removeClass("d-none").show();
          $("#spoc_details_lable").removeClass("d-none")
          //$("#have_promo_code_div").removeClass("d-none").show();
          $("#student_div").addClass("d-none");
          $("#employement_status_div").addClass("d-none");

       }
       $('#membership_type').prop('selectedIndex',0);
         var datastring='cat_id='+ $(this).val() + '&' + csrfName + '='+csrfHash;
         $.ajax({
             type: 'POST',
             data:datastring,
             url: "<?php echo base_url();?>registration/get_sub_category",
             beforeSend: function(){
               $('.ajax-loader').css("visibility", "visible");
             },
             success: function(res){ 
               var json = $.parseJSON(res);
               $('#subcategory').html(json.str);
               //csrfName=json.name;
               //csrfHash=json.value;
             },
             complete: function(){
                      $('.ajax-loader').css("visibility", "hidden");
              },

           });
   
   });
   
   // $('input[type=radio][name=have_promo_code]').change(function() {
       
   //    $("#promo_code").val('');
   //     if (this.value == 'Yes') {
   //         $("#promo_code_div").removeClass("d-none").show();
   //     }
   //     else{
   //         $("#promo_code_div").addClass("d-none");
   //     }
   // })
   // $('input[type=radio][name=want_to_buy_User_Bundle]').change(function() {
       
   //     if (this.value == 'Yes') {
   //         $("#User_Bundle_Quanity_div").removeClass("d-none").show();
   //     }
   //     else{
   //         $("#User_Bundle_Quanity_div").addClass("d-none");
   //     }
   // })




   $('#membership_type').change(function() {
     var membership_type = $(this).val();
     var category= $('input[name="category"]:checked').val();
     if (category==1 && membership_type=='Paid') {
       $("#subscription_fee_div").removeClass("d-none").show();
      // $("#have_promo_code_div").removeClass("d-none").show();
   
     }else{
       $("#subscription_fee_div").addClass("d-none");
       //$("#have_promo_code_div").addClass("d-none");
     }
   
    });


   $('#subcategory').change(function() {
     var subcategory = $(this).val();
     var category= $("input:radio[name=category]:checked").val();
     
     var hide_flag=0
     if (subcategory==11) {
       var hide_flag=1
     }

     if (hide_flag==1) {
        $("#membership_free").addClass('d-none');
        // $("#membership_paid").attr("selected","selected")
     }else{
        $("#membership_free").removeClass('d-none'); 
        // $("#membership_free").attr("selected","selected")
        // $('#membership_type').prop("selectedIndex", 0);
     }

     if (category==2) {

     if (subcategory==10) {
        $("#other_sub_cat_div").removeClass("d-none").show();
      }else{
        $("#other_sub_cat_div").addClass("d-none");
      } 
      
     if (subcategory==9) {
       $("#Are_you_Organization_div").removeClass("d-none").show();
       // $("#institution_type_div").addClass("d-none");
       $("#institution_full_name_div").addClass("d-none");
       $("#public_prvt_div").addClass("d-none");
        // $("#want_to_buy_User_Bundle_div").addClass("d-none");
         //$("#have_promo_code_div").addClass("d-none");
         $("#subscription_fee_div").addClass("d-none");
       
     }else{
       $("#Are_you_Organization_div").addClass("d-none");
       // $("#institution_type_div").removeClass("d-none").show();
       $("#public_prvt_div").removeClass("d-none").show();
       // $("#want_to_buy_User_Bundle_div").removeClass("d-none").show();
       //$("#have_promo_code_div").removeClass("d-none").show();
       $("#subscription_fee_div").removeClass("d-none").show();
     }
     }
   
    }); 
   
   
      $('input[type=radio][name=Are_you_Organization]').change(function() {
       if (this.value == 'Organisation') {
           $("#institution_full_name_div").removeClass("d-none").show();
           $("#Person_Full_Name_div").addClass("d-none");
       }
       else if (this.value == 'Inividual'){
         
         $("#Person_Full_Name_div").removeClass("d-none").show();
           $("#institution_full_name_div").addClass("d-none");
       }
   })

   
   });


</script>


<script>
   
   $(document).ready(function(){
    $('#send_verification_code').prop('disabled',true);
     // $('#send_OTP').prop('disabled',true)

    $("#email").mouseleave(function() {

        if ($("#email").val()!=''  && $("#email").valid()==true) {
         $('#send_verification_code').prop('disabled',false);
        }
        
    });

    //  $("#mobile").mouseleave(function() {

    //     if ($("#mobile").val()!=''  && $("#mobile").valid()==true) {
    //      $('#send_OTP').prop('disabled',false);
    //     }
        
    // });




     $('#send_OTP').click(function(e){
      if ($("#mobile").valid()==false) {
         return false;
      }

       var mobile = $('#mobile').val();
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
            var datastring='mobile='+ mobile + '&' + csrfName + '='+csrfHash;
             $.ajax({
             type: 'POST',

             data:datastring,
             url: "<?php echo base_url();?>registration/send_otp",

              beforeSend: function(){
               $('.ajax-loader').css("visibility", "visible");
             },
             success: function(res){ 

               var json = $.parseJSON(res);
               if (json.success==true) {

                 // $('#send_OTP').text('OTP sent');
                 // $('#send_OTP').prop('disabled', true);

                 // setTimeout(function(){
                 //      $('#send_OTP').text('Resend OTP');
                 //      $('#send_OTP').prop('disabled', false);
                 //   }, 60*1000);

                  $('#send_OTP').text('OTP sent');
                  $('#send_OTP').prop('disabled', true);
                  $('#timer_div_otp').removeClass('d-none');
                  timer_otp(180);


                  setTimeout(function(){
                  $('#send_OTP').text('Resend Code');
                  $('#send_OTP').prop('disabled', false);
                  }, 60*3000);

               
               }
             
             },
             complete: function(){
                      $('.ajax-loader').css("visibility", "hidden");
              },
            });
      });

     // $('#send_verification_code').click(function(e){
     //  if ($("#email").valid()==false) {
     //     return false;
     //  }

     //  if ($("#email").val()=='') {
     //     return false;
     //  }

     //   var email = $('#email').val();
     //    var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
     //        var datastring='email='+ email + '&' + csrfName + '='+csrfHash;
     //         $.ajax({
     //         type: 'POST',

     //         data:datastring,
     //         url: "<?php echo base_url();?>registration/send_verification_code",
     //         success: function(res){ 

     //           var json = $.parseJSON(res);
     //           if (json.success==true) {
     //             $('#send_verification_code').text('Verification Code sent');
     //             $('#send_verification_code').prop('disabled', true);
     //             $('#timer_div').removeClass('d-none');
     //             timer(180);


     //             setTimeout(function(){
     //                  $('#send_verification_code').text('Resend Code');
     //                  $('#send_verification_code').prop('disabled', false);
     //               }, 60*3000);

     //           }
             
     //         }
     //        });
     //  });


     $('#send_verification_code').click(function(e){
      if ($("#email").valid()==false) {
         return false;
      }

      if ($("#email").val()=='') {
         return false;
      }

       if ($("#mobile").valid()==false) {
         return false;
      }

      if ($("#mobile").val()=='') {
         return false;
      }



       var email = $('#email').val();
       var mobile = $('#mobile').val();
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
            var datastring='email='+ email + '&mobile='+ mobile + '&' + csrfName + '='+csrfHash;
             $.ajax({
             type: 'POST',

             data:datastring,
             url: "<?php echo base_url();?>registration/send_codes",
             beforeSend: function(){
               $('.ajax-loader').css("visibility", "visible");
             },
             success: function(res){ 

               var json = $.parseJSON(res);
               if (json.success==true) {
                 $('#send_verification_code').text('Verification Code sent');
                 $('#send_verification_code').prop('disabled', true);
                 $('#timer_div').removeClass('d-none');
                 timer(180);


                 setTimeout(function(){
                      $('#send_verification_code').text('Resend Code');
                      $('#send_verification_code').prop('disabled', false);
                   }, 60*3000);

               }
             
             },
             complete: function(){
                      $('.ajax-loader').css("visibility", "hidden");
              },
            });
      });

     

     // $('#institution_type').select2({
     //    closeOnSelect : true,
     //    width: '100%',
     // });
     $('#domain_industry').select2({
          width: '100%',
          tags: true
     
     });
      $('#country_code').select2();

       $('#employement_status').select2({
        // placeholder : "Please Select Domain / Area of Expertise",
        closeOnSelect : true,
        width: '100%',
    });


       $('.refreshCaptcha').on('click', function(){
           $.get('<?php echo base_url().'registration/refresh_reg'; ?>', function(data){
               $('#captImg').html(data);
           });
       });
    
      
     //  $("#btn_submit").on('click',function(){
       
     //   if($("#reg_form").valid()){
         
     //    }
     // });


  });
</script>


<script>
  $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip(); 

    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });

    $("#show_hide_password_confirm a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password_confirm input').attr("type") == "text"){
            $('#show_hide_password_confirm input').attr('type', 'password');
            $('#show_hide_password_confirm i').addClass( "fa-eye-slash" );
            $('#show_hide_password_confirm i').removeClass( "fa-eye" );
        }else if($('#show_hide_password_confirm input').attr("type") == "password"){
            $('#show_hide_password_confirm input').attr('type', 'text');
            $('#show_hide_password_confirm i').removeClass( "fa-eye-slash" );
            $('#show_hide_password_confirm i').addClass( "fa-eye" );
        }
    });
    
});

let timerOn = true;

function timer(remaining) {
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  document.getElementById('timer').innerHTML = m + ':' + s;
  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    return;
  }
  
  // Do timeout stuff here
  $('#timer_div').addClass('d-none');

  // alert('Timeout for otp');
}


function timer_otp(remaining) {
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  document.getElementById('timer_otp').innerHTML = m + ':' + s;
  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer_otp(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    return;
  }
  
  // Do timeout stuff here
  $('#timer_div_otp').addClass('d-none');

  // alert('Timeout for otp');
}

</script>



<script>

$("#membership_type").parent().find('label').addClass('floatinglabel');

  $("input[type='text']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

    $("select").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='password']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});

  $("input[type='email']").change( function() {
   if ($(this).val() != '') {
     $(this).parent().find('label').addClass('floatinglabel');
   }
});
</script>

