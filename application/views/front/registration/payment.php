<?php if ($this->session->flashdata('error_message_register') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Error!','Invalid Promo Code.','error');
                   }, 200);
               </script>
<?php } ?>

<style>
  .ajax-loader {visibility: hidden; background-color: rgba(255,255,255,0.7); position: fixed ; z-index:99999 !important; width:100%; height:100%; top:0px; left:0px; overflow: auto;}

.ajax-loader img {position: relative; top:35%; left:30%; }
</style>
<div id="loader" class="ajax-loader"><img height="300" src="<?php echo base_url('assets/img/Preloader.gif') ?>" alt="" class="img-responsive" ></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Pricing Tables </h1>
    </div>
    <!--/end container-->
</div>





    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                 <div class="formInfo">
                  <form method="POST" id="reg_form">
                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    <div class="row">
                        <?php foreach ($plans as $key => $plan) { ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="pricingTable">
                                
                                <div class="pricingHeader">
                                    <h3 class="title"><?php echo $plan['plan_name'] ?></h3>
                                </div>
                                <div class="pricing-icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="price-Value">
                                    <span class="currency">&#x20B9;</span><?php echo $plan['plan_amount']; ?>/-
                                </div>
                                <span class="month"><?php echo $plan['plan_duration']; ?></span>
                                
                                <div class="pricing-content mb-3">
                                  <?php echo $plan['plan_desc'] ?>
                                    <!-- <ul>
                                        <li>Accounts &amp; Contact</li>
                                        <li>Task &amp; Event Tracking</li>
                                        <li>Google Apps</li>
                                        <li>Mobile Access</li>
                                        <li>Content library</li>
                                    </ul> -->
                                </div>
                                
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input  type="hidden" name="plan_price" value="<?php echo $plan['plan_amount']; ?>" id="plan_price">   
                                    <input <?php if($key==0){echo  "checked";} ?> type="radio" class="form-check-input plan" value="<?php echo $plan['plan_id'] ?>" <?php echo  set_radio('plan', $plan['plan_id']); ?> name="plan" data-price="<?php echo $plan['plan_amount']; ?>" ><b>Select Plan</b>
                                    </label>
                              </div>

                            </div>
                        </div>
                         <?php } ?>
                   
                       
                 </div>

                  <div class="form-group pt-5" id="have_promo_code_div">
                           <div class="row">
                              <div class="col-6">
                                 <label for="first_name"> Do you have Promo Code/Corporate Link Code?</label>
                              </div>
                              <div class="col-3">
                                 <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" checked class="form-check-input have_promo_code" value="Yes" name="have_promo_code" <?php echo  set_radio('have_promo_code', 'Yes'); ?>>Yes</label>
                                 </div>
                                 <div class="form-check-inline">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input have_promo_code" value="No" name="have_promo_code" <?php echo  set_radio('have_promo_code', 'No'); ?>>No</label>
                                 </div>
                              </div>
                               <div class="error" style="color:#F00"><?php echo form_error('have_promo_code'); ?> </div>
                               
                           </div>

                        </div>
                        <div class="row" id="promo_code_div">
                        <div class="form-group col-6" >
                          
                           <input type="text" class="form-control" name="promo_code" id="promo_code" placeholder="" value="<?php echo set_value('promo_code')?>">
                            <label class="form-control-placeholder" for="promo_code">Enter Promo Code/Corporate Link Code</label>


                           <input type="hidden" name="promo_code_type" id="promo_code_type" value="">
                            <input type="hidden" name="org_id" id="org_id" value="">
                           <input type="hidden" name="promo_code_id" id="promo_code_id" value="">

                           

                           <div class="error" style="color:#F00"><?php echo form_error('promo_code'); ?> </div>

                        </div>
                         <div class="form-group col-2 mt-4">
                           <button type="button"  class="btn btn-primary" id="apply_promo_code">Apply</button>
                        </div>
                        <div class="form-group  mt-4">
                           <span style="color:green" id="promo_success"></span>
                           <span style="color:red" id="promo_error"></span>
                        </div>
                        </div>

                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th class="text-center">Plan Amount</th>
                        <th class="text-center">Discounted Price</th>
                        <th class="text-center">Total Amount to Pay</th>
                      </tr>
                    </thead>
                     <tbody>
                       <tr>
                        <td>
                          <input type="text" readonly id="plan_amount" class="form-control"> 
                        </td>
                        <td>
                           <input type="text" readonly id="discounted_price" name="discounted_price" class="form-control">
                        </td>
                        <td>
                         <input type="text" readonly id="subscription_fee" name="subscription_fee" class="form-control">
                        </td>
                         
                       </tr>
                     </tbody>
                   </table>    

                   <div class="col-12 pt-2"><small>
                               <strong>Note</strong>: Dear Subscriber, we are in the process of setting up our payment gateway. In the meanwhile, please <a href="<?php echo base_url('home/contactUs') ?>" target="_blank">contact us</a>  to get a Promo Code and continue your registration" Link to contact form on the underlined section</small>
                               </div>
 


                 <div class="row mt-4">
                     <!-- <div class="col-md-4">
                        <div class="form-group">
                         
                           <input type="text" readonly id="plan_amount" class="form-control" required="">
                              <label class="form-control-placeholder" for="technology">Plan Amount
                                <em>*</em></label>
                        </div>
                     </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          
                            <input type="text" readonly id="discounted_price" name="discounted_price" class="form-control" required="">
                             <label class="form-control-placeholder"  for="cname">Discounted Price
                                <em>*</em></label>
                            
                        </div>
                    </div>
             
                    <div class="col-md-4">
                        <div class="form-group">
                          
                                 <input type="text" readonly id="subscription_fee" name="subscription_fee" class="form-control" required="">
                                  <label class="form-control-placeholder"  for="cname">Total Amount to Pay
                                <em>*</em></label>
                            
                        </div>
                    </div> -->
                  
                    <div class="col-md-8">
                         <div class="form-group">
                           <input type="hidden" name="submit_button" value="submit_button">
                           <button type="submit" class="btn btn-primary" name="btn_submit" value="btn_submit" id="btn_submit" >Register</button> 
                        </div>

                    </div>

                  

                 </div>

                   <div class="row">
                     <div class="col-md-10 pt-5">
                      To register as an Organization, please contact us on our WhatsApp helpline (link & QR Code) or email to us on - <a href="mailto: info@technovuus.araiindia.com">info@technovuus.araiindia.com</a> 
                     Facing issues registering on TechNovuus? Get in touch with our helpdesk by clicking on the link or scanning the QR Code.
                    <a href="https://wa.me/message/BWX6SYO2JAXDH1" target="_blank">https://wa.me/message/BWX6SYO2JAXDH1</a>

                  
                     
                   </div>

                    <div class="qr_image col-md-2" >
                    <img class="img-fluid" height="200" src="<?php echo base_url('assets/qrcode.jpg') ?>" alt="">
                    </div> 

                   </div>
                  </form>
                </div>
    
          
    
    
            </div>
        </div>
    </section>





<!-- End #main -->
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
 <script type="text/javascript">
   $(document).ready(function () {
     $.validator.setDefaults({
       submitHandler: function (form) {
            // form.submit();
                swal({
                title:'Confirm' ,
                text: "Are you sure you want to submit the form ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
                }).then(function (result) {
                if (result.value) {
                  form.submit();
              }
            }); 
       
       }
     });
   
    
     $('#reg_form').validate({

       rules: {
         
            // studen form
            plan: {
               required: true,
             },

             have_promo_code:{
              required: true,
             },

             promo_code: {
               required: function() {
                  return $("input:radio[name=have_promo_code]:checked").val() == "Yes" ;   
                }
               
              },
            
           },
       messages: {
         plan: {
               required: "This field is required",
             },

           
   
       },
       errorElement: 'span',
       errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
       },
       highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
       },
       unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
       }
     });
   });
</script>

<script>
$(document).ready(function () {
   $('input[type=radio][name=plan]').change(function() {
       $('#btn_submit').prop('disabled', true);
       $("#promo_success").html("");
       $("#promo_error").html("");
       $("#promo_code_type").val("");
       $("#org_id").val("");
       $("#promo_code_id").val("");
       $("#promo_code").val("");

       $("#plan_amount").val("");
       $("#discounted_price").val("");
       $("#subscription_fee").val("");
   })

   $('#promo_code').on('input', function() {
    $('#btn_submit').prop('disabled', true);
     $("#promo_success").html("");
     $("#promo_error").html("");
     $("#promo_code_type").val("");
     $("#org_id").val("");
     $("#promo_code_id").val("");

     $("#plan_amount").val("");
     $("#discounted_price").val("");
     $("#subscription_fee").val("");
 });
   
   $('#btn_submit').prop('disabled', true);
      $('input[type=radio][name=have_promo_code]').change(function() {
       
      $("#promo_code").val('');
       if (this.value == 'Yes') {
         $('#promo_code').prop('readonly', false);
           // $("#promo_code_div").removeClass("d-none").show();
       }
       else{

           $('#btn_submit').prop('disabled', true);
           $('#promo_code').prop('readonly', true);
           // $("#promo_code_div").addClass("d-none");
       }
      })


    $('#apply_promo_code').click(function(e){
      
      if ($(".plan").valid()==false) {
           swal( 'Error!',"Please choose a plan",'error');
                  return false;
      }

      if ($(".have_promo_code").valid()==false) {
        return false;
      }
      
      if ($("input:radio[name=have_promo_code]:checked").val() == "No") {
        return false; 
      }

      if ($("#promo_code").valid()==false) {
         return false;
      }
       $("#promo_success").html("");
       $("#promo_error").html("");
       $("#promo_code_type").val("");
       $("#org_id").val("");
       $("#promo_code_id").val("");
       


       var promo_code = $('#promo_code').val();
       
        var plan= $("input:radio[name=plan]:checked").val();
       
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
        var user_category='<?php echo $user_category ?>';
            var datastring='promo_code='+ promo_code + '&plan='+ plan + '&user_category='+ user_category + '&' + csrfName + '='+csrfHash;
             $.ajax({
             type: 'POST',
              beforeSend: function(){
                     $('.ajax-loader').css("visibility", "visible");
              },

             data:datastring,
             url: "<?php echo base_url();?>registration/verify_promo_code",
             success: function(res){ 

               var json = $.parseJSON(res);
               if (json.success==true) {
                 // $('#apply_promo_code').text('OTP sent');
                 // $('#apply_promo_code').prop('disabled', true);
                 $("#promo_code").css('border-color','green');
                 $("#promo_success").html(json.msg);
                 $("#promo_code_type").val(json.type);
                 $("#org_id").val(json.org_id);
                 $("#promo_code_id").val(json.promo_code_id);




                 var price= $("input:radio[name=plan]:checked").attr("data-price");
                 var discounted_total=parseInt(json.discounted_total);

                 $("#plan_amount").val(price);
                 $("#discounted_price").val(discounted_total);
                 $("#subscription_fee").val(discounted_total);

                 if (discounted_total==0) {  
                 // $("#subscription_fee_div").removeClass("d-none").show();
                 $('#btn_submit').prop('disabled', false);
               }else{
                $('#btn_submit').prop('disabled', true);
               }

        

               }else{
                  $('#btn_submit').prop('disabled', true);
                 $("#promo_code").css('border-color','red');  
                $("#promo_error").html(json.msg);
               }
             
             },
             complete: function(){
                       $('.ajax-loader').css("visibility", "hidden");
              },
            });
      });




})
</script>
<!-- <script language="JavaScript">

function goodbye(e) {
        if(!e) e = window.event;
        e.cancelBubble = true;
        e.returnValue = ''; 

        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
    }
window.onbeforeunload=goodbye;
</script> -->