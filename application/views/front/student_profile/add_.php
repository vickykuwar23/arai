<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>datepicker/datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<style>
    .mandatory {
    color: red;
}
 .previous_img_outer { position: relative; }
    .previous_img_outer a.img_outer img { max-width: 300px; max-height: 100px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
    .previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }

</style>
    <div class="profileinfo" data-aos="fade-down">
        <div class="container">
            <div class="row">
<?php $enc_obj =  New Opensslencryptdecrypt(); ?>
                <div class="col-md-2" id="profile_header_iamge">
                    <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
                      
                    <?php }else{ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
                  <?php } ?>
                </div>
                <div class="col-md-6">
                    <h3 class="mt-4"><?php echo $user[0]['title']." ".$user[0]['first_name']; ?></h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-paper-plane"></i> <?php echo $user[0]['email'] ?></a></li>
                        <li><i class="fa fa-user"></i> <?php echo $user[0]['sub_catname'] ?></li>
                    </ul>
                    <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo base_url('profile') ?>" class="btn btn-upload2">Back To Profile</a>
                    </div>
                   
                </div>
                <!-- <div class="col-md-4">
                    <div class="countBox">
                   <h4>02</h4>
                        <span>Challenges</span> 
                    </div>
                    <div class="countBox">
                         <h4>50</h4>
                        <span>Points</span> 
                    </div>

                </div> -->
            </div>
        </div>
    </div>

<section id="registration-form" class="inner-page" data-aos="fade-up">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php if ($this->session->flashdata('success') != "") { ?>
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> Success!</h5>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php } ?>
              <?php if ($this->session->flashdata('error') != "") { ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Error!</h5>
                <?php echo $this->session->flashdata('error'); ?>
            </div>

        <?php } ?>
        <?php 
            if(validation_errors()!="" ){
                validation_errors();  
            }
        ?>
      </div>
    </div>
    <div class="row formInfo">
      <div class="col-md-12">
        <form method="post" id="student_profile_form" action="<?php //echo site_url('profile/student_profile'); ?>" enctype="multipart/form-data">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
          <input type="hidden" name="type" value="Student">
          <?php 
              $ProfileId = '';
              if(isset($profile_information[0]['id']))
              {
                  $ProfileId = $profile_information[0]['id'];
              }
          ?>
          <input type="hidden" name="profileId" value="<?php echo $ProfileId ?>">
          <div id="smartwizard_student_profile">
              <ul>
                <li><a href="#step-1">Step 1<br /><small>University Information And Interests</small></a></li>
               
                <li><a href="#step-2">Step 2<br /><small>Accomplishments and Projects/Internships</small></a></li>
                <li><a href="#step-3">Step 3<br /><small>Billing Address</small></a></li>
              </ul>
              <div>
              <div id="step-1" class="mt-4">
                
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <?php 
                        $Status = '';
                        if(isset($profile_information[0]['status']))
                        {
                            $Status = $profile_information[0]['status'];
                        }
                      ?>
                      <label for="exampleInputEmail1">Student Status<em class="mandatory">*</em></label>
                      <select class="form-control" name="status" id="status">
                        <option value="">Select Status</option>
                         <?php foreach ($status as $key => $status_value) { ?>
                            <option value="<?php echo $status_value['degree_name'] ?>" <?php if($Status == $status_value['degree_name']) { ?> selected="selected" <?php } ?> ><?php echo $status_value['degree_name'] ?></option>
                        <?php  } ?>

                        <!-- 
                        <option value="UG" <?php if($Status == "UG") { ?> selected="selected" <?php } ?> >UG</option>
                        <option value="PG" <?php if($Status == "PG") { ?> selected="selected" <?php } ?> >PG</option>
                        <option value="Other" <?php if($Status == "Other") { ?> selected="selected" <?php } ?> >Other</option> -->


                      </select>
                       <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                      <span class="error"><?php echo form_error('status'); ?></span>
                    </div>
                  </div>

                     <div class="col-md-3"> 

                        <div class="form-group upload-btn-wrapper">
                            <button class="btn btn-upload"><i class="fa fa-plus"> </i> Profile Picture </button>
                            <input type="file" class="form-control" name="profile_picture" id="profile_picture" placeholder="" value="">
                        </div>
                        <p style="color:red;font-size: 11px" >Note: Please upload .jpg or .png or .jpeg file less than 500 KB</p>
                     
                       </div>
                        <!--  <div class="col-md-2">
                                <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { ?>
                                <img width="100" src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" >
                                  
                                <?php } ?>
                                
                            </div> -->

                              <div class="col-md-2">
                                                    <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { 


                                                        ?>

                                            <div class="col-md-6" id="profile_picture_outer">
                                                <div class="form-group">
                                                    <div class="previous_img_outer">
                                                        <a class="img_outer" href="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" target="_blank">
                                                           
                                                            <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>">
                                                        </a>
                                                        <a class="btn btn-danger btn-sm" onclick="delete_single_file_expert('<?php echo $enc_obj->encrypt('arai_student_profile'); ?>', '<?php echo $enc_obj->encrypt('id'); ?>', '<?php echo $enc_obj->encrypt($profile_information[0]['id']); ?>', 'profile_picture', '<?php echo $enc_obj->encrypt('./assets/profile_picture/'); ?>', 'Profile Picture')" href="javascript:void(0)">Delete</a>
                                                    </div>
                                                </div>
                                            </div>

                                                      
                                                    <?php } ?>
                                        
                                                </div>
                </div>
                <div id="university_row">
                    <?php if (isset($profile_information[0]['university_course']) && count($profile_information[0]['university_course'])> 0 ) { 
                      $universitySinceYear=$profile_information[0]['university_since_year'];
                      $universityToYear=$profile_information[0]['university_to_year'];
                      $universityName=$profile_information[0]['university_name'];
                      $universityLocation=$profile_information[0]['university_location'];
                   foreach ($profile_information[0]['university_course'] as $key => $value) {?>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Course Name<em class="mandatory">*</em></label>
                            <input type="text" class="form-control university_course" id="university_course" placeholder="Enter University Course" name="university_course[]" value="<?php echo $value; ?>" maxlength="200">
                            <span class="error"><?php echo form_error('university_course'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">University since<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_since_year datepicker" id="university_since_year" placeholder="Select University Since Year" name="university_since_year[]" value="<?php echo date('d-m-Y',strtotime($universitySinceYear[$key])) ?>" maxlength="200">
                          <span class="error"><?php echo form_error('university_since_year'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">University Till<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_to_year datepicker_till" id="university_to_year" placeholder="Select University To Year" name="university_to_year[]" value="<?php echo date('d-m-Y',strtotime($universityToYear[$key]))  ?>" maxlength="200">
                          <span class="error"><?php echo form_error('university_to_year'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">Institution Name<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_name" id="university_name" placeholder="Enter Institution Name" name="university_name[]" value="<?php echo $universityName[$key]; ?>" maxlength="200">
                          <span class="error"><?php echo form_error('university_name'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                         
                          <label for="exampleInputEmail1">Location<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_location" id="university_location" placeholder="Select University Location" name="university_location[]" value="<?php echo $universityLocation[$key]; ?>" maxlength="200">
                          <span class="error"><?php echo form_error('university_location'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <?php if ($key==0) {?>
                            <button class="add_university_row_btn btn btn-success btn-sm">Add</button>
                          <?php }else{ ?>
                            <button class="remove_university_row_btn btn btn-danger btn-sm">Remove</button>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <?php } }
                     else{
                      ?>
                      <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Course Name<em class="mandatory">*</em></label>
                            <input type="text" class="form-control university_course" id="university_course" placeholder="Enter University Course" name="university_course[]" maxlength="200">
                            <span class="error"><?php echo form_error('university_course'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">University since<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_since_year datepicker" id="university_since_year" placeholder="Select University Since Year" name="university_since_year[]" maxlength="200">
                          <span class="error"><?php echo form_error('university_since_year'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">University Till<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_to_year datepicker_till" id="university_to_year" placeholder="Select University To Year" name="university_to_year[]" maxlength="200">
                          <span class="error"><?php echo form_error('university_to_year'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">Institution Name<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_name" id="university_name" placeholder="Enter Institution Name" name="university_name[]" maxlength="200">
                          <span class="error"><?php echo form_error('university_name'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">                 
                        <div class="form-group">
                          <label for="exampleInputEmail1">Location<em class="mandatory">*</em></label>
                          <input type="text" class="form-control university_location" id="university_location" placeholder="Select University Location" name="university_location[]" maxlength="200">
                          <span class="error"><?php echo form_error('university_location'); ?></span>
                        </div>  
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <button class="add_university_row_btn btn btn-success btn-sm">Add</button>
                        </div>
                      </div>
                    </div>
                    <?php } ?> 
                </div>
                <div class="row">
           
                      </div>
                  <div id="currentFieldsOfStudy_row" class="d-none">
                  <?php if (isset($profile_information[0]['current_study_course']) && count($profile_information[0]['current_study_course'])> 0 ) { 
                      $currentStudySinceYear=$profile_information[0]['current_study_since_year'];
                      $currentStudyToYear=$profile_information[0]['current_study_to_year'];
                      $currentStudyDescription=$profile_information[0]['current_study_description'];
                   foreach ($profile_information[0]['current_study_course'] as $key => $value) {?>
                  <div class="row">
                    <div class="col-md-3">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Course Name<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_course" id="current_study_course" placeholder="Enter Current Study Course" name="current_study_course[]" value="<?php echo $value; ?>" maxlength="200">
                        <span class="error"><?php echo form_error('current_study_course'); ?></span>
                      </div>  
                    </div>
                    <div class="col-md-2">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">From<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_since_year datepicker" id="current_study_since_year" placeholder="Select Since Year" name="current_study_since_year[]" value="<?php echo date('d-m-Y',strtotime($currentStudySinceYear[$key])) ; ?>" maxlength="200">
                        <span class="error"><?php echo form_error('current_study_since_year'); ?></span>
                      </div>  
                    </div>
                    <div class="col-md-2">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">To<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_to_year datepicker" id="current_study_to_year" placeholder="Select to Year" name="current_study_to_year[]" value="<?php echo date('d-m-Y',strtotime($currentStudyToYear[$key])) ; ?>" maxlength="200">
                        <span class="error"><?php echo form_error('current_study_to_year'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description<em class="mandatory">*</em></label>
                        <textarea name="current_study_description[]" class="form-control current_study_description" id="current_study_description"><?php echo $currentStudyDescription[$key]; ?></textarea>
                        <span class="error"><?php echo form_error('current_study_description'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="form-group">
                        <?php if ($key==0) {?>
                          <button class="add_current_fields_of_study_btn btn btn-success btn-sm">Add</button>
                          <?php }else{ ?>
                          <button class="remove_current_fields_of_study_btn btn btn-danger btn-sm">Remove</button>
                          <?php } ?>
                      </div>  
                    </div>
                  </div>
                  <?php } }
                     else{
                      ?>
                    <div class="row">
                    <div class="col-md-3">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Course Name<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_course" id="current_study_course" placeholder="Enter Current Study Course" name="current_study_course[]">
                        <span class="error"><?php echo form_error('current_study_course'); ?></span>
                      </div>  
                    </div>
                    <div class="col-md-2">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">From<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_since_year datepicker" id="current_study_since_year" placeholder="Select Since Year" name="current_study_since_year[]">
                        <span class="error"><?php echo form_error('current_study_since_year'); ?></span>
                      </div>  
                    </div>
                    <div class="col-md-2">                 
                      <div class="form-group">
                        <label for="exampleInputEmail1">To<em class="mandatory">*</em></label>
                        <input type="text" class="form-control current_study_to_year datepicker" id="current_study_to_year" placeholder="Select to Year" name="current_study_to_year[]">
                        <span class="error"><?php echo form_error('current_study_to_year'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description<em class="mandatory">*</em></label>
                        <textarea name="current_study_description[]" class="form-control current_study_description" id="current_study_description"></textarea>
                        <span class="error"><?php echo form_error('current_study_description'); ?></span>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="form-group">
                          <button class="add_current_fields_of_study_btn btn btn-success btn-sm">Add</button>
                      </div>  
                    </div>
                  </div>
                  <?php } ?> 
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <?php 
                          $Domain = [];
                          if(isset($profile_information[0]['domain_area_of_expertise']))
                          {
                              $Domain = $profile_information[0]['domain_area_of_expertise'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Domain / Area of Interest<em class="mandatory">*</em></label>
                      <select name="domain_and_area_of_expertise[]" id="domain_and_area_of_expertise" class="domainName form-control" autocomplete="nope" multiple="multiple">
                          <?php foreach ($domain as $key => $value) { ?>
                              <?php if(in_array($domain[$key]['id'], $Domain)) { ?>
                                  <option data-id='<?php echo $domain[$key]['domain_name'] ?>' value="<?php echo $domain[$key]['id'] ?>" selected="selected"><?php echo $domain[$key]['domain_name'] ?></option>
                              <?php } else { ?>
                                  <option data-id='<?php echo $domain[$key]['domain_name'] ?>' value="<?php echo $domain[$key]['id'] ?>"><?php echo $domain[$key]['domain_name'] ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                       <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                      <span class="error"><?php echo form_error('domain_and_area_of_expertise'); ?></span>
                    </div>
                  </div>

                  <?php 
                        $other_domian = '';
                        $class='d-none';
                        if(isset($profile_information[0]['other_domian'] ) && $profile_information[0]['other_domian']!='')
                        {
                            $class='';
                            $other_domian = $profile_information[0]['other_domian'];
                        }
                    ?>

              <div class="col-md-6">
                <div class="form-group <?php echo $class ?>" id="other_domian_div">
                   <label for="other_domian" >Other Domain / Area of Expertise  <em class="mandatory">*</em></label>
                       <input type="text" name="other_domian" value="<?php echo $other_domian ?>" id="other_domian" class="form-control" required="">
                       
                         <!-- <span><small>Please enter comma seprated domain / area of expertise</small></span> -->
                       <div class="error" style="color:#F00"><?php echo form_error('other_domian'); ?> </div>
              </div>
            </div>
             </div>
                  <div class="row d-none">                
                  <div class="col-md-6">
                    <div class="form-group">
                      <?php 
                          $AreaOfInterest = [];
                          if(isset($profile_information[0]['areas_of_interest']))
                          {
                              $AreaOfInterest = $profile_information[0]['areas_of_interest'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Areas of Interest<em class="mandatory">*</em></label>
                      <select name="areas_of_interest[]" id="areas_of_interest" class="areaOfInterestName form-control" autocomplete="nope" multiple="multiple">
                        <?php foreach ($area_of_interest as $key => $value) { ?>
                            <?php if(in_array($area_of_interest[$key]['id'], $AreaOfInterest)) { ?>
                                <option data-id='<?php echo $area_of_interest[$key]['area_of_interest'] ?>' value="<?php echo $area_of_interest[$key]['id'] ?>" selected="selected"><?php echo $area_of_interest[$key]['area_of_interest'] ?></option>
                                <?php } else { ?>
                                <option data-id='<?php echo $area_of_interest[$key]['area_of_interest'] ?>' value="<?php echo $area_of_interest[$key]['id'] ?>"><?php echo $area_of_interest[$key]['area_of_interest'] ?></option>
                                <?php } ?>
                        <?php } ?>
                      </select>
                       <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                      <span class="error"><?php echo form_error('areas_of_interest'); ?></span>
                    </div>
                  </div>
                  <?php 
                        $other_area_int = '';
                        $class='d-none';
                        if(isset($profile_information[0]['other_area_int'] ) && $profile_information[0]['other_area_int']!='')
                        {
                            $class='';
                            $other_area_int = $profile_information[0]['other_area_int'];
                        }
                    ?>
                    <div class="col-md-6">
                          <div class="form-group <?php echo $class ?>" id="other_area_div">
                            <label for="other_area_int">Other Areas of Interest  <em class="mandatory">*</em></label>
                                 <input type="text" name="other_area_int" value="<?php echo $other_area_int ?>" id="other_area_int" class="form-control" required="">
                                  
                                 
                                 <div class="error" style="color:#F00"><?php echo form_error('other_area_int'); ?> </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <?php 
                            $skillSets = [];
                            if(isset($profile_information[0]['skill_sets']))
                            {
                                $skillSets = $profile_information[0]['skill_sets'];
                            }
                        ?>
                        <label for="exampleInputEmail1">Skill Sets<em class="mandatory">*</em></label>
                        <select name="skill_sets[]" id="skill_sets" class="skillSets form-control" autocomplete="nope" multiple="multiple">
                        <?php foreach ($skill_sets as $key => $value) { ?>
                            <?php if(in_array($skill_sets[$key]['id'], $skillSets)) { ?>
                            <option data-id='<?php echo $skill_sets[$key]['name'] ?>' value="<?php echo $skill_sets[$key]['id'] ?>" selected="selected"><?php echo $skill_sets[$key]['name'] ?></option>
                            <?php } else { ?>
                                <option data-id='<?php echo $skill_sets[$key]['name'] ?>' value="<?php echo $skill_sets[$key]['id'] ?>"><?php echo $skill_sets[$key]['name'] ?></option>
                                <?php } ?>
                        <?php } ?>
                        </select>
                         <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                        <span class="error"><?php echo form_error('skill_sets'); ?></span>
                    </div>
                  </div>

                    <?php 
                          $other_skill_sets = '';
                          $class='d-none';
                          if(isset($profile_information[0]['other_skill_sets'] ) && $profile_information[0]['other_skill_sets']!='')
                          {
                              $class='';
                              $other_skill_sets = $profile_information[0]['other_skill_sets'];
                          }
                      ?>
                             <div class="col-md-6">
                                <div class="form-group <?php echo $class ?>" id="other_skill_sets_div">
                                     <label for="other_skill_sets" >Other Skill Sets  <em class="mandatory">*</em></label>
                                       <input type="text" name="other_skill_sets" value="<?php echo $other_skill_sets ?>" id="other_skill_sets" class="form-control" required="">
                                     
                                       <div class="error" style="color:#F00"><?php echo form_error('other_skill_sets'); ?> </div>
                              </div>
                        </div>


                </div>


              </div>
              
             
              
              <div id="step-2" class="mt-4">
                <div id="compititionHackathon_row">
                  <label><b>Notable Accomplishments</b></label>
                  <?php if (isset($profile_information[0]['event_experience_in_year']) && count($profile_information[0]['event_experience_in_year'])> 0 ) { 
                      $eventDescriptionOfWork=$profile_information[0]['event_description_of_work'];
                   foreach ($profile_information[0]['event_experience_in_year'] as $key => $value) {?>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Year</label>
                        <input type="text" class="form-control event_experience_in_year only_year" id="event_experience_in_year" placeholder="Enter Event Experience Year" name="event_experience_in_year[]" value="<?php echo $value ; ?>">
                        <span class="error"><?php echo form_error('event_experience_in_year'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description of Work</label>
                        <textarea name="event_description_of_work[]" class="form-control event_description_of_work" id="event_description_of_work"><?php echo $eventDescriptionOfWork[$key]; ?></textarea>
                        <span class="error"><?php echo form_error('event_description_of_work'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <?php if ($key==0) {?>
                        <button class="add_compitition_hackathon_btn btn btn-success btn-sm">Add</button>
                         <?php }else{ ?>
                         <button class="remove_compitition_hackathon_btn btn btn-danger btn-sm">Remove</button>
                         <?php } ?>
                      </div>
                    </div>
                  </div>
                  <?php } }
                     else{
                      ?>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Year</label>
                        <input type="text" class="form-control event_experience_in_year only_year" id="event_experience_in_year " placeholder="Enter Event Experience Year" name="event_experience_in_year[]">
                        <span class="error"><?php echo form_error('event_experience_in_year'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description of Work</label>
                        <textarea name="event_description_of_work[]" class="form-control event_description_of_work" id="event_description_of_work"></textarea>
                        <span class="error"><?php echo form_error('event_description_of_work'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <button class="add_compitition_hackathon_btn btn btn-success btn-sm">Add</button>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <div id="projectInternship_row">
                  <label><b>Projects / Internships</b></label>
                  <?php if (isset($profile_information[0]['technical_experience_in_year']) && count($profile_information[0]['technical_experience_in_year'])> 0 ) { 
                      $technicalDescriptionOfWork=$profile_information[0]['technical_description_of_work'];

                       $technical_experience_to_year=$profile_information[0]['technical_experience_to_year'];

                   foreach ($profile_information[0]['technical_experience_in_year'] as $key => $value) {?>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputEmail1">From</label>
                        <input type="text" class="form-control technical_experience_in_year only_year" id="technical_experience_in_year" placeholder="Enter Technical Experience Year" name="technical_experience_in_year[]" value="<?php echo $value ; ?>">
                        <span class="error"><?php echo form_error('technical_experience_in_year'); ?></span>
                      </div> 
                    </div>

                     <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputEmail1">To</label>
                        <input type="text" class="form-control technical_experience_to_year only_year" id="technical_experience_to_year" placeholder="Enter Technical Experience Year" name="technical_experience_to_year[]" value="<?php echo $technical_experience_to_year[$key] ; ?>">
                        <span class="error"><?php echo form_error('technical_experience_to_year'); ?></span>
                      </div> 
                    </div>


                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description of Work</label>
                        <textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" id="technical_description_of_work"><?php echo $technicalDescriptionOfWork[$key] ?></textarea>
                        <span class="error"><?php echo form_error('technical_description_of_work'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <?php if ($key==0) {?>
                        <button class="add_project_internship_btn btn btn-success btn-sm">Add</button>
                        <?php }else{ ?>
                        <button class="remove_project_internship_btn btn btn-danger btn-sm">Remove</button>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <?php } }
                     else{
                      ?>
                      <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputEmail1">From</label>
                        <input type="text" class="form-control technical_experience_in_year only_year" id="technical_experience_in_year" placeholder="Enter Technical Experience Year" name="technical_experience_in_year[]">
                        <span class="error"><?php echo form_error('technical_experience_in_year'); ?></span>
                      </div> 
                    </div>

                     <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputEmail1">To</label>
                        <input type="text" class="form-control technical_experience_to_year only_year" id="technical_experience_to_year" placeholder="Enter Technical Experience Year" name="technical_experience_to_year[]">
                        <span class="error"><?php echo form_error('technical_experience_to_year'); ?></span>
                      </div> 
                    </div>


                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description of Work</label>
                        <textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" id="technical_description_of_work"></textarea>
                        <span class="error"><?php echo form_error('technical_description_of_work'); ?></span>
                      </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <button class="add_project_internship_btn btn btn-success btn-sm">Add</button>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>

                 <div class="row">
                          <div class="col-md-8">
                              <div class="form-group">
                                  <?php 
                                      $additional_detail = '';
                                      if(isset($profile_information[0]['additional_detail']))
                                      {
                                          $additional_detail = $profile_information[0]['additional_detail'];
                                      }
                                  ?>
                                   <label   for="exampleInputEmail1">Additional Details</label>
                                <textarea name="additional_detail" class="form-control additional_detail" rows="1" id="additional_detail"><?php echo $additional_detail ?></textarea>
                             
                                <span class="error"><?php echo form_error('additional_detail'); ?></span>
                              </div>
                          </div>

                          

                      </div>
              </div>
              
              <div id="step-3" class="mt-4">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php 
                          $pincode = '';
                          if(isset($profile_information[0]['pincode']))
                          {
                              $pincode = $profile_information[0]['pincode'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Pincode</label>
                      <input type="text" class="form-control" id="pincode"  name="pincode" value="<?php echo $pincode; ?>" maxlength="200">
                      <span class="error"><?php echo form_error('pincode'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php 
                          $Flat = '';
                          if(isset($profile_information[0]['flat_house_building_apt_company']))
                          {
                              $Flat = $profile_information[0]['flat_house_building_apt_company'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Flat / House No. / Building / Apt / Company</label>
                      <input type="text" class="form-control" id="flat_house_building_apt_company"  name="flat_house_building_apt_company" value="<?php echo $Flat; ?>" maxlength="200">
                      <span class="error"><?php echo form_error('flat_house_building_apt_company'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php 
                          $Area = '';
                          if(isset($profile_information[0]['area_colony_street_village']))
                          {
                              $Area = $profile_information[0]['area_colony_street_village'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Area / Colony / Street / Village</label>
                      <input type="text" class="form-control" id="area_colony_street_village"  name="area_colony_street_village" value="<?php echo $Area; ?>" maxlength="200">
                      <span class="error"><?php echo form_error('area_colony_street_village'); ?></span>
                    </div> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php 
                          $Town = '';
                          if(isset($profile_information[0]['town_city_and_state']))
                          {
                              $Town = $profile_information[0]['town_city_and_state'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Town / City and State</label>
                      <input type="text" class="form-control" id="town_city_and_state"  name="town_city_and_state" value="<?php echo $Town; ?>" maxlength="200">
                      <span class="error"><?php echo form_error('town_city_and_state'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php 
                          $Country = '';
                          if(isset($profile_information[0]['country']))
                          {
                              $Country = $profile_information[0]['country'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Country</label>
                      <input type="text" class="form-control" id="country"  name="country" value="<?php echo $Country; ?>" maxlength="200">
                      <span class="error"><?php echo form_error('country'); ?></span>
                    </div>
                  </div>
                </div>

               
                <div class="row">
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <?php 
                          $studentIdProof = '';
                          if(isset($profile_information[0]['student_id_proof']))
                          {
                              $studentIdProof = $profile_information[0]['student_id_proof'];
                          }
                      ?>
                      <label for="exampleInputEmail1">Proof of Affiliation (Student ID)<em class="mandatory">*</em></label>
                      <input type="file" class="form-control" id="student_id_proof" placeholder="Upload Student ID Proof" name="student_id_proof" value="<?php echo $studentIdProof; ?>">
                      <?php if ($studentIdProof!=''): ?>
                        <a href="<?php echo base_url('assets/id_proof/'.$studentIdProof) ?>" target="_blank"><small>View Already Uploaded File.</small> </a>
                      <?php endif ?>
                      <p style="color:red;font-size: 11px" >Note: Please upload .jpg or .png or .jpeg or .pdf file less than 5 MB</p>
                      <span><?php echo form_error('student_id_proof'); ?></span>
                    </div>
                  </div>
                </div>

               <!--  <div class="row">
                  <button type="submit" class="btn btn-primary mt-3 ml-3">Submit</button>
                </div> -->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() {
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $("#university_row"); //Fields wrapper
      var add_button      = $(".add_university_row_btn"); //Add button ID
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="row"><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Course Name</label><input type="text" class="form-control university_course" id="university_course" placeholder="Enter University Course" name="university_course[]" maxlength="200"><span class="error"></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">University since</label><input type="text" class="form-control university_since_year datepicker" id="university_since_year" placeholder="Select University Since Year"name="university_since_year[]" maxlength="200"><span class="error"></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">University Till</label><input type="text" class="form-control university_to_year datepicker_till" id="university_to_year" placeholder="Select University To Year" name="university_to_year[]" maxlength="200"><span class="error"></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Institution Name</label> <input type="text" class="form-control university_name" id="university_name" placeholder="Enter Institution Name" name="university_name[]" maxlength="200"><span class="error"></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Location</label><input type="text" class="form-control university_location" id="university_location" placeholder="Select University Location" name="university_location[]" maxlength="200"><span class="error"></span></div>  </div><div class="col-md-4"><div class="form-group"><button class="remove_university_row_btn btn btn-danger btn-sm">Remove</button></div></div></div>'); //add input box
          }


             setTimeout(function(){ 
              $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy',
                    endDate: '+0d',
                    autoclose: true
                }); 
              }, 500);

               setTimeout(function(){ 
              $('.datepicker_till').datepicker({
                    format: 'dd-mm-yyyy',
                    autoclose: true
                }); 
              }, 500);

             



      });

      $(wrapper).on("click",".remove_university_row_btn", function(e){ //user click on remove text

          e.preventDefault(); 

          $(this).closest('.row').remove(); 

          x--;

      })
  });

    $(document).ready(function() {
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $("#currentFieldsOfStudy_row"); //Fields wrapper
      var add_button      = $(".add_current_fields_of_study_btn"); //Add button ID
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="row"><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Course</label><input type="text" class="form-control current_study_course" id="current_study_course" placeholder="Enter Current Study Course" name="current_study_course[]" maxlength="200"><span class="error"><?php echo form_error('current_study_course'); ?></span></div></div><div class="col-md-2"><div class="form-group"><label for="exampleInputEmail1">Since Year</label><input type="text" class="form-control current_study_since_year datepicker" id="current_study_since_year" placeholder="Select Since Year" name="current_study_since_year[]" maxlength="200"><span class="error"><?php echo form_error('current_study_since_year'); ?></span></div></div><div class="col-md-2"><div class="form-group"><label for="exampleInputEmail1">To Year</label><input type="text" class="form-control current_study_to_year datepicker" id="current_study_to_year" placeholder="Select to Year" name="current_study_to_year[]" maxlength="200"><span class="error"><?php echo form_error('current_study_to_year'); ?></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Description</label><textarea name="current_study_description[]" class="form-control current_study_description" id="current_study_description"></textarea><span class="error"><?php echo form_error('current_study_description'); ?></span></div></div><div class="col-md-1"><div class="form-group"><button class="remove_current_fields_of_study_btn btn btn-danger btn-sm">Remove</button></div></div></div>'); //add input box
          }

       setTimeout(function(){ 
              $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy',
                    endDate: '+0d',
                    autoclose: true
                }); 
              }, 500);

      });

      $(wrapper).on("click",".remove_current_fields_of_study_btn", function(e){ //user click on remove text

          e.preventDefault(); 

          $(this).closest('.row').remove(); 

          x--;

      })
  });

    $(document).ready(function() {
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $("#compititionHackathon_row"); //Fields wrapper
      var add_button      = $(".add_compitition_hackathon_btn"); //Add button ID
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="row"><div class="col-md-5"><div class="form-group"><label for="exampleInputEmail1">Year</label><input type="text" class="form-control event_experience_in_year only_year" id="event_experience_in_year" placeholder="Enter Event Experience Year" name="event_experience_in_year[]"><span class="error"><?php echo form_error('event_experience_in_year'); ?></span></div></div><div class="col-md-5"><div class="form-group"><label for="exampleInputEmail1">Description of Work</label><textarea name="event_description_of_work[]" class="form-control event_description_of_work" id="event_description_of_work"></textarea><span class="error"><?php echo form_error('event_description_of_work'); ?></span></div> </div><div class="col-md-2"><div class="form-group"><button class="remove_compitition_hackathon_btn btn btn-danger btn-sm">Remove</button></div></div></div>'); //add input box
          }
        setTimeout(function(){ 

                $('.only_year').datepicker(
                    {
                         /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                // format: "yyyy",
                                endDate: '+0d',
                                // viewMode: "years",
                                // minViewMode: "years"
                                format: "mm-yyyy",
                                viewMode: "months", 
                                minViewMode: "months"
                       
                    }
                    ); 

            }, 500);   

      });

      $(wrapper).on("click",".remove_compitition_hackathon_btn", function(e){ //user click on remove text

          e.preventDefault(); 

          $(this).closest('.row').remove(); 

          x--;

      })
  });

  $(document).ready(function() {
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $("#projectInternship_row"); //Fields wrapper
      var add_button      = $(".add_project_internship_btn"); //Add button ID
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="row"><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">From</label><input type="text" class="form-control technical_experience_in_year only_year" id="technical_experience_in_year" placeholder="Enter Technical Experience Year" name="technical_experience_in_year[]"></span></div></div><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">To</label><input type="text" class="form-control technical_experience_to_year only_year" id="technical_experience_to_year" placeholder="Enter Technical Experience Year" name="technical_experience_to_year[]"></span></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Description of Work</label><textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" id="technical_description_of_work"></textarea><span class="error"></span></div></div><div class="col-md-2"><div class="form-group"><button class="remove_project_internship_btn btn btn-danger btn-sm">Remove</button></div></div></div>'); //add input box
          }
       setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                // format: "yyyy",
                                endDate: '+0d',
                                // viewMode: "years",
                                // minViewMode: "years"
                                format: "mm-yyyy",
                                viewMode: "months", 
                                minViewMode: "months"
                               
                            }
                            ); 

                    }, 500);    

      });

      $(wrapper).on("click",".remove_project_internship_btn", function(e){ //user click on remove text

          e.preventDefault(); 

          $(this).closest('.row').remove(); 

          x--;

      })
  });

  // $.validator.setDefaults({
  //   submitHandler: function () {
  //     //alert( "Form successful submitted!" );
  //   form.submit();
  //   }
  // });

  $('.domainName').select2({
      placeholder : "Please Select Domain / Area of Expertise",
      closeOnSelect : true,
      width: '100%',
  });
  $('.areaOfInterestName').select2({
      placeholder : "Please Select Areas of Interest",
      closeOnSelect : true,
      width: '100%',
  });
  $('.skillSets').select2({
      placeholder : "Please Select Skill Sets",
      closeOnSelect : true,
      width: '100%',
  });

  function scroll_to_top(div_id='')
  {
    if(div_id == '') { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
    else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
  }
  scroll_to_top();
  
  $(document).ready(function() 
  {

     setTimeout(function(){ 
      $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+0d',
            autoclose: true
        }); 
      }, 200);

     
         setTimeout(function(){ 
        $('.datepicker_till').datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true
          }); 
        }, 500);

      setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                // format: "yyyy",
                                endDate: '+0d',
                                // viewMode: "years",
                                // minViewMode: "years"
                                format: "mm-yyyy",
                                viewMode: "months", 
                                minViewMode: "months"
                               
                            }
                            ); 

                    }, 100);
    /**** PREVENT FORM SUBMITTING WHEN PRESS ENTER ****/
    // $(window).keydown(function(event)
    // {
    //   if(event.keyCode == 13) 
    //   {
    //     event.preventDefault();
    //     return false;
    //   }
    // });
            
    /******* STEP SHOW EVENT *********/
       // Step show event
    $("#smartwizard_student_profile").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        if (stepPosition === 'first') {
            $("#prev-btn").addClass('disabled');
             $(".sw-btn-prev").css('display','none');
            $(".btnfinish").addClass('d-none');
        } else if (stepPosition === 'final') {
            $("#next-btn").addClass('disabled');
            $(".sw-btn-next").css('display','none');

            $(".btnfinish").removeClass('d-none');
        } else {
          $(".btnfinish").addClass('d-none');
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
            $(".sw-btn-prev").css('display','block');
            $(".sw-btn-next").css('display','block');
        }
    });
    
    /******* STEP WIZARD *********/
    $('#smartwizard_student_profile').smartWizard(
    {
      /* selected: 2, */
      theme: 'arrows',
      transitionEffect: 'fade',
      showStepURLhash: false,
			keyNavigation: false,
        toolbarSettings: {
            toolbarExtraButtons: [
                $('<button></button>').text('Finish')
                            .addClass('btn btn-primary btnfinish')
                            .on('click', function(e){ 
                             e.preventDefault();
                            
                            var isValid=true;

                            
                            if ($("#student_id_proof").valid()==false) {
                                
                              isValid= false;
                            } 

                            var cat = '<?php echo $this->session->userdata('user_sub_category'); ?>';


                            if (cat==11) {
                            if ($(".specify_fields_area_that_you_would_like").valid()==false) {
                                
                              isValid= false;
                            } 

                            if ($(".bio_data").valid()==false) {
                              
                              isValid= false;
                            } 
                             if ($("#years_of_experience").valid()==false) {
                              isValid= false;
                            } 

                             if ($("#no_of_paper_publication").valid()==false) {
                              isValid= false;
                            } 

                              if ($("#no_of_patents").valid()==false) {
                              isValid= false;
                            } 
                            }

                            if (isValid==true) {

                                swal({
                                title:'Confirm' ,
                                text: 'Are you sure you want to update your details?',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes!'
                                }).then(function (result) {
                                if (result.value) {
                                  $('#student_profile_form').submit();

                              }

                            });       
                        }

                            }),
                // $('<button></button>').text('Cancel')
                //             .addClass('btn btn-danger')
                //             .on('click', function(){ 
                //                 alert('Cancel button click');                            
                //             })
            ]
        },
      /* enableURLhash:true,
      enableAllAnchors: false, */         
    });
        
    $("#smartwizard_student_profile").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
    {
      var isValidate = true;      
      
      if(stepNumber==0 && stepDirection=="forward")
      {       
        if($("#status").valid()==false) { isValidate= false; $("#status").focus(); }
        $(".university_course").each(function(){

            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter University Course"
                }
            } );            
        });     
        $(".university_since_year").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter University Since Year"
                }
            } );            
        });       
        $(".university_to_year").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter University To Year"
                }
            } );            
        });      
        $(".university_name").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter University Name"
                }
            } );            
        });  
        $(".university_location").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Enter University Location"
                }
            } );            
        });
        if($(".university_course").valid()==false) { isValidate= false; }
        if($(".university_since_year").valid()==false) { isValidate= false; }
        if($(".university_to_year").valid()==false) { isValidate= false;  }
        if($(".university_name").valid()==false) { isValidate= false;  }
        if($(".university_location").valid()==false) { isValidate= false;  }
         if($("#domain_and_area_of_expertise").valid()==false) { isValidate= false; $("#domain_and_area_of_expertise").focus(); }
        // if($("#areas_of_interest").valid()==false) { isValidate= false; $("#areas_of_interest").focus(); }
        if($("#skill_sets").valid()==false) { isValidate= false; $("#skill_sets").focus(); }
         if ($("#profile_picture").valid()==false) {
                    isValidate= false;
            }
       return isValidate;
      }
      
      // if(stepNumber==0 && stepDirection=="forward")
      // {       
        // $(".current_study_course").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Current Study Course"
        //         }
        //     } );            
        // });
        // $(".current_study_since_year").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Current Study Since Year"
        //         }
        //     } );            
        // });
        // $(".current_study_to_year").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Current Study To Year"
        //         }
        //     } );            
        // });
        // $(".current_study_description").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Current Study Description"
        //         }
        //     } );            
        // });


        // if($(".current_study_course").valid()==false) { isValidate= false; }
        // if($(".current_study_since_year").valid()==false) { isValidate= false;  }
        // if($(".current_study_to_year").valid()==false) { isValidate= false;  }
        // if($(".current_study_description").valid()==false) { isValidate= false;  }
       



      //   return isValidate;
      // }

      if(stepNumber==2 && stepDirection=="forward")
      {       
        // $(".event_experience_in_year").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Event Expertise Year"
        //         }
        //     } );            
        // });
        // $(".event_description_of_work").each(function(){
        //     $(this).rules("add", {
        //         required: true,
        //         messages: {
        //             required: "Please Enter Event Description of Work"
        //         }
        //     } );            
        // });
        // if($(".event_experience_in_year").valid()==false) { isValidate= false; }
        // if($(".event_description_of_work").valid()==false) { isValidate= false; }

        // return isValidate;
      }
      
      
    }) 
  });

  $(document).ready( function() 
  {
   $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
   }, 'File size must be less than 500 kb');

   $.validator.addMethod('filesize_id', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
   }, 'File size must be less than 5 MB');
    // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })// For chosen validation

    $("#student_profile_form").validate( 
    {
      rules: {
            status: {
                required : true,
            },
            profile_picture: {
                 extension: "JPEG|JPG|PNG|jpeg|jpg|png",
                 filesize:500000
            },
            // university_course: {
            //     required : true,
            // },
            // university_since_year: {
            //     required : true,
            // },
            // university_to_year: {
            //     required : true,
            // },
            // university_name: {
            //     required : true,
            // },
            // university_location: {
            //     required : true,
            // },
            // current_study_course: {
            //     required : true,
            // },
            // current_study_since_year: {
            //     required : true,
            // },
            // current_study_to_year: {
            //     required : true,
            // },
            // current_study_description: {
            //     required : true,
            // },
            "domain_and_area_of_expertise[]": {
                required : true,
            },
            // "areas_of_interest[]": {
            //     required : true,
            // },
            "skill_sets[]": {
                required : true,
            },
            // event_experience_in_year: {
            //     required : true,
            // },
            // event_description_of_work: {
            //     required : true,
            // },
            // technical_experience_in_year: {
            //     required : true,
            // },
            // technical_description_of_work: {
            //     required : true,
            // },
            // pincode: {
            //     required : true,
            //     minlength: 6,
            //     maxlength: 6,
            // },
            // flat_house_building_apt_company: {
            //     required : true,
            // },
            // area_colony_street_village: {
            //     required : true,
            // },
            // town_city_and_state: {
            //     required : true,
            // },
            // country: {
            //     required : true,
            // },
            student_id_proof: {
                required: function() {
                  
                  var file_name=($('#student_id_proof').attr('value'))
                    return  file_name  =='';  
                  },
                extension: "JPEG|JPG|PNG|jpeg|jpg|png|pdf|PDF",
                filesize_id:5000000
            },
        },
      messages:
      {
        status: { 
          required: "Please Select Status" 
        },
        // university_course: { 
        //   required: "Please Enter University Course" 
        // },
        // university_since_year: { 
        //   required: "Please Enter University Since Year" 
        // },
        // university_to_year: { 
        //   required: "Please Enter University To Year" 
        // },
        // university_name: { 
        //   required: "Please Enter University Name" 
        // },
        // university_location: { 
        //   required: "Please Enter University Location" 
        // },
        // current_study_course: { 
        //   required: "Please Enter Current Study Course" 
        // },
        // current_study_since_year: { 
        //   required: "Please Enter Current Study Since Year" 
        // },
        // current_study_to_year: { 
        //   required: "Please Current Study To Year" 
        // },
        // current_study_description: { 
        //   required: "Please Enter Current Study Description" 
        // },
        "domain_and_area_of_expertise[]": { 
          required: "Please Select Domain Area of Expertise" 
        },
        // "areas_of_interest[]": { 
        //   required: "Please Select Area of Interest" 
        // },
        "skill_sets[]": { 
          required: "Please Select Skill Sets" 
        },
        // event_experience_in_year: { 
        //   required: "Please Enter Event Expertise in Year" 
        // },
        // event_description_of_work: { 
        //   required: "Please Enter Event Description of Work" 
        // },
        // technical_experience_in_year: { 
        //   required: "Please Enter Technical Expertise in Year" 
        // },
        // technical_description_of_work: { 
        //   required: "Please Enter Technical Description of Work" 
        // },
        pincode: {
            required : "Please Enter Pincode",
        },
        flat_house_building_apt_company: {
            required : "Please Enter Flat / House / Building / Apartment / Company",
        },
        area_colony_street_village: {
            required : "Please Enter Area / Colony / Street / Village",
        },
        town_city_and_state: {
            required : "Please Enter Town / City and State",
        },
        country: {
            required : "Please Enter Country",
        },
        // student_id_proof: {
        //     required : "Please Enter Student ID Proof",
        // },
        
      },
      errorElement: 'span',
      errorPlacement: function (error, element) { element.closest('.form-group').append(error); },
      highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
      unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); }      
    });
  });


// $(document).ready(function()
//     {          
//         $('input[type="file"]').change(function(e)
//         {              
//             var fileName = e.target.files[0].name;
//             $(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
//         });          
//     });
</script>

<script>
        $(document).ready(function() {
          // $('#employement_status').change(function(){
          //   if ($(this).val()=='Other') {
          //     $('#other_emp_status_div').removeClass('d-none');
          //   }else{
          //     $("#other_emp_status").val('');  
          //     $('#other_emp_status_div').addClass('d-none');
          //   }
          // });

            $('#domain_and_area_of_expertise').on('change', function() {
                var selected = $(this).find('option:selected', this);
                console.log(selected)
                var results = [];

                selected.each(function() {
                    results.push($(this).data('id'));
                });

                var display_flag=0;
                 $.each(results,function(i){
                   if(results[i]=='Other')
                   {
                      display_flag = 1;
                     
                   }
                  
                });

                 if (display_flag==1) {
                  $('#other_domian_div').removeClass('d-none');
                 }else{
                   $('#other_domian').val('');
                   $('#other_domian_div').addClass('d-none');
                 }

            });



          $('#areas_of_interest').on('change', function() {
                var selected = $(this).find('option:selected', this);
                var results = [];

                selected.each(function() {
                    results.push($(this).data('id'));
                });


                 var display_flag=0;
                 $.each(results,function(i){
                   if(results[i]=='Other')
                   {
                      display_flag = 1;
                     
                   }
                  
                });

                 if (display_flag==1) {
                  $('#other_area_div').removeClass('d-none');
                 }else{
                   $('#other_area_int').val('');
                   $('#other_area_div').addClass('d-none');
                 }


            });

            $('#skill_sets').on('change', function() {
                var selected = $(this).find('option:selected', this);
                var results = [];

                selected.each(function() {
                    results.push($(this).data('id'));
                });


                  var display_flag=0;
                 $.each(results,function(i){
                   if(results[i]=='Other')
                   {
                      display_flag = 1;
                     
                   }
                  
                });

                 if (display_flag==1) {
                  $('#other_skill_sets_div').removeClass('d-none');
                 }else{
                   $('#other_skill_sets').val('');
                   $('#other_skill_sets_div').addClass('d-none');
                 }

              

            });


            });


  function delete_single_file_expert(tbl_nm, pk_nm, del_id, input_nm, file_path, swal_text)
    { 
        swal(
        {
            title:"Confirm?" ,
            text: "Are you confirm to delete the "+swal_text+"?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        {
            if (result.value) 
            {
                var csrf_test_name = $("#csrf_token").val();
                var data = { 
                        'tbl_nm': encodeURIComponent($.trim(tbl_nm)), 
                        'pk_nm': encodeURIComponent($.trim(pk_nm)), 
                        'del_id': encodeURIComponent($.trim(del_id)), 
                        'input_nm': encodeURIComponent($.trim(input_nm)), 
                        'file_path': encodeURIComponent($.trim(file_path)), 
                        'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
                        };          
                $.ajax(
                { 
                    type: "POST", 
                    url: '<?php echo site_url("delete_single_file") ?>', 
                    data: data, 
                    dataType: 'JSON',
                    success:function(data) 
                    { 
                        $("#csrf_token").val(data.csrf_new_token);
                        $("#"+input_nm+"_outer").remove();

                        $("#profile_header_iamge").html('<img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">');

                        
                        swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" });
                    }
                });
            }
        });
    }

      </script>

  </script>
 