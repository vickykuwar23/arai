<div class="profileinfo" data-aos="fade-down">
  <div class="container">
      <div class="row mt-4">
         <div class="col-md-2">
            <?php if($profile_information[0]['profile_picture']) { ?>
               <img src="<?php echo base_url('assets/profile_picture/'.$profile_information[0]['profile_picture'].'') ?>" alt="experts" width="100px">
            <?php } else { ?>
               <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="" class="img-fluid bor">
            <?php } ?>
         </div>
         <div class="col-md-8">  
            <h3 class="card-title mt-5">Profile Information</h3>
         </div>
      </div>
   </div>
</div>
<section id="individual-profile-form" class="inner-page mt-5" data-aos="fade-up">
   <div class="container">
      <?php foreach ($profile_information as $key => $value) { ?>
        <div class="row mt-4">
            <div class="col-md-6">
               <?php if($value['status']) { ?>
                  <label><b>Status :-</b></label>
                  <?php echo $value['status'] ?>
               <?php } else { ?>
                  <label><b>Employement Status :-</b></label>
                  <?php echo $value['employement_status'] ?>
               <?php } ?>  
            </div>
            <div class="col-md-6">
                  <label><b>Company Name :-</b></label>
                  <?php echo $value['company_name'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Designation Since Year :-</b></label>
                  <?php echo $value['designation_since_year'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Designation Description :-</b></label>
                  <?php echo $value['designation_description'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Technical Experience in Year :-</b></label>
                  <?php echo $value['technical_experience_in_year'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Technical Description of Work :-</b></label>
                  <?php echo $value['technical_description_of_work'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Pincode :-</b></label>
                  <?php echo $value['pincode'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Flat / House / Company :-</b></label>
                  <?php echo $value['flat_house_building_apt_company'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Area / Street :-</b></label>
                  <?php echo $value['area_colony_street_village'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Town / City :-</b></label>
                  <?php echo $value['town_city_and_state'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Country :-</b></label>
                  <?php echo $value['country'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Fields Area that you would like :-</b></label>
                  <?php echo $value['specify_fields_area_that_you_would_like'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Bio Data :-</b></label>
                  <?php echo $value['bio_data'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Year of Experience :-</b></label>
                  <?php echo $value['years_of_experience'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>No of Paper Publication :-</b></label>
                  <?php echo $value['no_of_paper_publication'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Paper Year :-</b></label>
                  <?php echo $value['paper_year'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Paper Conf. Name :-</b></label>
                  <?php echo $value['paper_conf_name'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Paper Title :-</b></label>
                  <?php echo $value['paper_title'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>No of Patents :-</b></label>
                  <?php echo $value['no_of_patents'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Patent Year :-</b></label>
                  <?php echo $value['patent_year'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Phone Number :-</b></label>
                  <?php echo $value['patent_number'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Patent Title :-</b></label>
                  <?php echo $value['patent_title'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Portal Name :-</b></label>
                  <?php echo $value['portal_name'] ?>
            </div>
            <div class="col-md-6">
                  <label><b>Portal Link :-</b></label>
                  <?php echo $value['portal_link'] ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Portal Description :-</b></label>
                  <?php echo $value['portal_description'] ?>
            </div>
            <div class="col-md-6">
               <label><b>Area of Interest :-</b></label>
               <?php foreach ($area_of_interest as $key => $value) { ?>
                  <?php echo $value[0]['area_of_interest'].',' ?>
               <?php } ?>
            </div>
         </div>
         <div class="row mt-4">
            <div class="col-md-6">
               <label><b>Domain :-</b></label>
               <?php foreach ($domain as $key => $value) { ?>
                  <?php echo $value[0]['domain_name'].',' ?>
               <?php } ?>
            </div>
            <div class="col-md-6">
               <label><b>Skill Sets :-</b></label>
               <?php foreach ($skill_sets as $key => $value) { ?>
                  <?php echo $value[0]['name'].',' ?>
               <?php } ?>
            </div>
         </div>
      <?php } ?>
   </div>
</section>