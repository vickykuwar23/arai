 <style>
     .details{
        color:#2196f3;
     }
 </style>

    <div class="profileinfo" data-aos="fade-down">
        <div class="container">
            <div class="row">

                <div class="col-md-2">
                    <?php if (isset($profile_information[0]['profile_picture'])){ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
                      
                    <?php }else{ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
                  <?php } ?>
                </div>
                <div class="col-md-6">
                    <h3 class="mt-4"><?php echo $user[0]['title']." ".$user[0]['first_name']; ?></h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-paper-plane"></i> <?php echo $user[0]['email'] ?></a></li>
                        <li><i class="fa fa-user"></i> <?php echo $user[0]['sub_catname']." ".$user[0]['user_category'] ?></li>
                    </ul>
                    <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo base_url('profile') ?>" class="btn btn-upload2">Back To Profile</a>
                    </div>
                   
                </div>
                <div class="col-md-4">
                    <div class="countBox">
                        <h4>02</h4>
                        <span>Challenges</span>
                    </div>
                    <div class="countBox">
                        <h4>50</h4>
                        <span>Points</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
 




    <!--====================================================
                  REGISTRATION-FORM
======================================================-->
<section id="registration-form" class="inner-page" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="formInfo">
                  
                      <form method="POST" id="reg_form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                        <div class="row">
                           <div class="col-2">
                              <div class="form-group">
                                 <label  for="title">Title</label>
                                 
                                 <select class="form-control" id="title" name="title" required="">
                                    <option value=""></option>
                                    <option  value="Mr." <?php if($user[0]['title']=='Mr.') echo 'selected' ?>>Mr.</option>
                                    <option value="Mrs." <?php if($user[0]['title']=='Mrs.') echo 'selected' ?>>Mrs.</option>
                                    <option value="Miss." <?php if($user[0]['title']=='Miss.') echo 'selected' ?>>Miss.</option>
                                 </select>
                                 <div class="error" style="color:#F00"><?php echo form_error('title'); ?> </div>
                              </div>
                           </div>
                           <div class="col">
                              <div class="form-group">
                                 <label for="first_name">First Name</label>
                                 <input type="text" class="form-control" name="first_name" id="first_name" placeholder="" value="<?php echo $user[0]['first_name'] ?>" required="">
                                  <div class="error" style="color:#F00"><?php echo form_error('first_name'); ?> </div>
                              </div>
                           </div>
                           <div class="col">
                              <div class="form-group">
                                  <label  for="middle_name">Middle Name</label>
                                
                                 <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="" value="<?php echo $user[0]['middle_name'];?>" required="">
                                 <div class="error" style="color:#F00"><?php echo form_error('middle_name'); ?> </div>
                              </div>
                           </div>
                           <div class="col">
                              <div class="form-group">
                                 <label for="last_name" >Last Name</label>
                                 
                                 <input type="text" class="form-control" name="last_name" id="last_name" placeholder="" value="<?php echo $user[0]['last_name'] ?>" required="">
                                 <div class="error" style="color:#F00"><?php echo form_error('last_name'); ?> </div>
                              </div>
                           </div>
                        </div>
                            <div class="row">
                        <div class="form-group col-6">
                           <label for="email" id="label_email" >Email address</label>
                           
                           <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $user[0]['email'] ?>" required="">
                           <div class="error" style="color:#F00"><?php echo form_error('email'); ?> </div>
                        </div>
                        <div class="form-group col-3 mt-4">
                           <button type="button"  class="btn btn-primary btn-sm" id="send_verification_code">Send Verification Code</button>
                        </div>
                         <div class="form-group col-2">
                           <label for="verification_code" >Enter Code</label>

                           <input type="text" class="form-control" name="verification_code" id="verification_code" placeholder="" value="<?php echo set_value('mobile')?>" required="">

                           <div class="error" style="color:#F00"><?php echo form_error('verification_code'); ?> </div>
                        </div>
                        </div>
                        <div class="row"> 
                         <div class="form-group col-2">
                               <label for="country_code">Country Code</label>
                           <select class="form-control country_code" id="country_code" name="country_code" required="">
                              <option value="">Select Country</option>
                               <?php foreach ($country_codes as $key => $code) { ?>
                                 <option <?php if($user[0]['country_code']==$code['id']) echo 'selected' ?> value="<?php echo $code['id'] ?>"><?php echo $code['iso']." ".$code['phonecode'] ?></option>
                              <?php } ?>
                           </select>

                          
                           <div class="error" style="color:#F00"><?php echo form_error('country_code'); ?> </div>
                        </div>
                        
                        <div class="form-group col-4">
                            <label for="mobile" id="label_mobile">Phone Number</label>

                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="" value="<?php echo $user[0]['mobile'] ?>" required="">
                           <div class="error" style="color:#F00"><?php echo form_error('mobile'); ?> </div>
                           <span id="mobile_exist" class="error"></span>
                        </div>
                        <div class="form-group col-3 mt-4">
                           <button type="button"  class="btn btn-primary btn-sm" id="send_OTP">Send OTP</button>
                        </div>
                        <div class="form-group col-2">
                           <label for="otp" >Enter OTP</label>

                           <input type="text" class="form-control" name="otp" id="otp" placeholder="" value="<?php echo set_value('mobile')?>" required="">

                           <div class="error" style="color:#F00"><?php echo form_error('otp'); ?> </div>
                        </div>
                        </div>
                        <div class="form-group">
                           <input type="hidden" name="submit_button" value="submit_button">
                           <button type="submit" class="btn btn-primary" name="btn_submit" value="btn_submit" id="btn_submit" >Submit</button> 
                        </div>

                      </form>
                  
            </div>

        </div>




    </div>
</section>