<div class="profileinfo" data-aos="fade-down">
  <div class="container">
      <div class="row mt-4">
         <div class="col-md-2">
            <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="" class="img-fluid bor">
         </div>
         <div class="col-md-8">  
            <h3 class="card-title mt-5">All Experts Profile Information</h3>
         </div>
      </div>
   </div>
</div>

<section class="expertsList" data-aos="fade-up">
      <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
     <div class="container">
         <div class="row title-bar">
             <div class="col-md-12 text-center mb-4">
                 <h2>All Featured Experts</h2>
             </div>
          </div>
             <div id="expertsInfox" class="owl-carouselx owl-themex">
            <div class="row custom_load_more_append">
             <?php if ($all_featured_experts) { ?>
                 <?php foreach ($all_featured_experts as $key => $user) { ?>
                     <div class="col-md-4 p3">
                        <div class="expertsListBox allExperts">
                           <?php if($user['profile_picture']) { ?>
                               <img src="<?php echo base_url('assets/profile_picture/'.$user['profile_picture'].'') ?>" alt="experts">
                           <?php } else { ?>
                               <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="experts">
                            <?php } ?>
                           <div class="experts">
                               <span class="read"><a href="<?php echo base_url(); ?>profile/viewProfile/<?php echo $user['user_id'] ?>">View Profile</a></span>
                               <?php if(isset($user['first_name'])) { ?>
                                   <h3><?php echo $user['first_name'].' '.$user['last_name'] ?></h3>
                               <? } else { ?>
                                   <h3><?php echo $user['institution_full_name'] ?></h3>
                               <?php } ?>
                               <p><?php echo $user['designation_description'] ?></p>
                           </div>
                           <div class="expertsBox">
                               <ul>
                                   <?php if($user['years_of_experience']) { ?>
                                       <li><span><?php echo $user['years_of_experience'] ?></span>Years of Experience</li>
                                   <?php } else { ?>
                                       <li><span>0</span>Years of Experience</li>
                                   <?php } ?>
                                   <?php if($user['no_of_patents']) { ?>
                                       <li><span><?php echo $user['no_of_patents'] ?></span> Patents</li>
                                   <?php } else { ?>
                                       <li><span>0</span> Patents</li>
                                   <?php } ?>
                                   <?php if($user['no_of_paper_publication']) { ?>
                                       <li><span><?php echo $user['no_of_paper_publication'] ?></span> Academic Publications</li>
                                   <?php } else { ?>
                                       <li><span>0</span> Academic Publications</li>
                                   <?php } ?>
                               </ul>
                           </div>
                        </div>
                     </div>
                 <?php } ?>
             <?php } else { ?>
                 <p>No Records Found in featured experts</p>
             <?php } ?>
             </div>

             <div id="xxx"></div>
            <!--<div class="show_more_main" id="show_more_main<?php echo $user['user_id']; ?>">
               <span id="<?php echo $user['user_id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts" onclick="show_more_ajax('<?php echo $user['user_id']; ?>')">Show more</span>
               <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
            </div>-->
         </div>
     </div>
</section>

<script type="text/javascript">
   function show_more_ajax(id)
   {
      var base_url = '<?php echo base_url(); ?>';
      var ID = id;
      var length = $('.p3').length;
      var cs_t =  $('.token').val();
      $('.show_more').hide();
      $('.loding').show();

      $.ajax({
         type:'POST',
         url: base_url+'profile/getmore',
         data:'id='+length+'&csrf_test_name='+cs_t,
         dataType:"text",
         success:function(data)
         {
            $(".custom_load_more_append").append(data);
            $('.loding').hide();
            $('.show_more').show();
         }
      });
   }
</script>
         