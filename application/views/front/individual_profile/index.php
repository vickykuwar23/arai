 <style>
     .details{
        color:#2196f3;
     }
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 30px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 22px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#preloader-loader {
position: fixed;
top: 0;
left: 0;
right: 0;
bottom: 0;
z-index: 9999;
overflow: hidden;
background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
content: "";
position: fixed;
top: calc(50% - 30px);
left: calc(50% - 30px);
border: 6px solid #f2f2f2;
border-top: 6px solid #c80032;
border-radius: 50%;
width: 60px;
height: 60px;
-webkit-animation: animate-preloader 1s linear infinite;
animation: animate-preloader 1s linear infinite;
}

.guideline-tooltip{
  position: relative;
  display: inline-block;
  margin-left: 10px;
}
.guideline-tooltip:hover p {
  opacity: 1;
  -webkit-transform: translate(-35%, 0);
          transform: translate(-35%, 0);
    margin-top: 10px;
  visibility: visible;
}
.guideline-tooltip p {
    position: absolute;
    left: 50%;
    top: 100%;
    opacity: 0;
    padding: 1em;
    background-color: #e7f0ff;
    font-size: 14px;
    line-height: 1.6;
    text-align: left;
    white-space: nowrap;
    -webkit-transform: translate(-35%, 1em);
    transform: translate(-35%, 1em);
    -webkit-transition: all 0.15s ease-in-out;
    transition: all 0.15s ease-in-out;
    color: #000;
  z-index: 99;
    font-weight: 400;
  visibility: hidden;
}
.guideline-tooltip p::before {
    content: '';
    position: absolute;
    top: -16px;
    left: 33%;
    width: 0;
    height: 0;
    border: 0.6em solid transparent;
    border-top-color: #e7f0ff;
    transform: rotate(180deg);
}
 </style>
<?php if ($this->session->flashdata('success_profile') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Profile Updated Successfully.','success');
                   }, 200);
               </script>
    <?php } ?>

    <?php if ($this->session->flashdata('success_claim_expert') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!','Expert Application Submitted Successfully.','success');
                   }, 200);
               </script>
    <?php } ?>


    
   <?php if ($this->session->flashdata('error_profile_competion') != "" ) 
        {  ?>
           <script type="text/javascript">
             setTimeout(function () {
               swal( 'Warning!','Please complete your profile.','warning');
               }, 200);
           </script>
<?php } ?>


 <?php if ($this->session->flashdata('success_personal_info') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Success!',' Personal Info updated successfully.','success');
                   }, 200);
               </script>
<?php } ?>
 <?php if ($this->session->flashdata('alreday_claim_expert') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( '','Your request is currently under review, you will be notified when the status of your application changes In case of any queries please contact the TechNovuus Admin at <a href="mailto:info@technovuus.araiindia.com." >info@technovuus.araiindia.com.</a>','warning');
                   }, 200);
               </script>
<?php } ?>





   
<div id="preloader-loader" style="display:none;"></div>

    <div class="profileinfo" data-aos="fade-down">
        <div class="container">
            <div class="row">

                <div class="col-md-2">
                    <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
                      
                    <?php }else{ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
                  <?php } ?>
                </div>
                <div class="col-md-6">
                                 <h3 class="mt-4"><?php echo $user[0]['title']." ".$user[0]['first_name']." ".$user[0]['last_name']; ?></h3>
                    <ul>
                        <li><a href="javascript:void(0)" style="cursor:auto"><i class="fa fa-paper-plane"></i> <?php echo $user[0]['email'] ?></a></li>
                        <li><i class="fa fa-user"></i> <?php echo $user[0]['sub_catname'] ?></li>
                     
                    </ul>
                    <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo base_url('personalInfoInd') ?>" class="btn btn-upload2"><i class="fa fa-pencil"></i> Update Personal Info</a>
                        
                        <?php 
                          $user_category=$this->session->userdata('user_category'); 
                          $user_sub_category=$this->session->userdata('user_sub_category'); 

                          if($user_category==2){
                            $edit_profile_url=base_url('organization');
                          } 

                          if($user_sub_category==11 || $user_sub_category == 1){
                            $edit_profile_url=base_url('profile/individual');
                          } 

                          if( $user_sub_category == 2){
                            $edit_profile_url=base_url('profile/student');
                            } 
                          ?>
                        <?php if ($edit_profile_url!='') { ?>
                          <a href="<?php echo $edit_profile_url; ?>" class="btn btn-upload2"><i class="fa fa-pencil"></i> Edit Profile</a>  
                        <?php } ?>
                        

                            <?php if ( $user[0]['user_sub_category_id']  !=11 &&  $user[0]['user_sub_category_id'] !=2 )  { 


                              ?>


                          <!-- && $profile_information[0]['aplied_for_expert']=='no'  -->
                        <a href="<?php echo base_url('profile/claim_as_expert') ?>" class="btn btn-upload2"><i class="fa fa-pencil"></i> Claim As Expert</a>

                      <?php } ?>
                    </div>
                   
                    <div class="progress" style="max-width:400px;">
                      <?php 
                      if ($profile_complete_per<=50) {
                        $class='bg-danger';
                      }elseif ($profile_complete_per>50 && $profile_complete_per<=79 ) {
                        $class='bg-warning';
                      }else{
                        $class='bg-success';
                      }
                      ?>
                      <div class="progress-bar <?php echo $class ?>" role="progressbar" aria-valuenow="<?php echo $profile_complete_per; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $profile_complete_per; ?>%; ">Your profile is <?php echo $profile_complete_per; ?>% complete</div>
                    </div>
                   
                </div>
                
                
             <div class="col-md-4">
                  <label for="" style="color: #fff"> Discoverable </label>
                 <div class="guideline-tooltip"><i style="color: #fff" class="fa fa-question-circle-o fa-lg"></i>
                 <p>Let registered members discover you on Technovuus
                 </p>
                 </div>
                  <label class="switch">
                    <input type="checkbox" id="discoverable" <?php if($user[0]['discoverable']=='yes'){ echo 'checked';} ?>> 
                    <span class="slider round"></span>
                  </label>
                 
                 
                 
                      </br>
                  <label for="" style="color: #fff"> Contactable  </label>
                  <div class="guideline-tooltip"><i style="color: #fff" class="fa fa-question-circle-o fa-lg"></i>
                 <p>Let registered members connect with you via Chat on Technovuus
                 </p>
                 </div>
                  <label class="switch">
                    <input type="checkbox" id="contactable" <?php if($user[0]['contactable']=='yes'){ echo 'checked';} ?> > 
                    <span class="slider round"></span>
                  </label>
                
                  
              </div>

                <!-- <div class="col-md-4">
                    <div class="countBox">
                      <h4>02</h4>
                        <span>Challenges</span>
                    </div>
                    <div class="countBox">
                        <h4>50</h4>
                        <span>Points</span>
                    </div>

                </div> -->
            </div>
        </div>
    </div>
 




    <!--====================================================
                  REGISTRATION-FORM
======================================================-->
<section id="registration-form" class="inner-page" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="formInfo">
                  <div class="row">
                    <div class="col-12">
                     <div class="table-responsive">
                        <table class="table table-profile">
                           <thead>
                              <tr>
                                 <th colspan="2">
                                    <h4>Personal Information</h4>

                                    
                                 </th>

                              </tr>
                           </thead>
                           <tbody>
                              <tr class="highlight">
                                 <td class="field">Full Name</td>
                                 <td class="details"> <?php echo $user[0]['title']." ".$user[0]['first_name']." ".$user[0]['middle_name']." ".$user[0]['last_name']; ?></td>
                              </tr>
                     
                              <tr>
                                 <td class="field">Mobile</td>
                                 <td class="details"><?php echo $user[0]['mobile'] ?></td>
                              </tr>
                              <tr>
                                 <td class="field">Email</td>
                                 <td class="details"><?php echo $user[0]['email'] ?></td>
                              </tr>
                             
                                <tr class="highlight">
                                 <td class="field">Membership Type</td>
                                 <td class="details"><?php echo $user[0]['membership_type'] ?></td>
                              </tr>
                         
                              
                             <thead>
                              <tr>
                                 <th colspan="2">
                                    <h4>Profile Details</h4>
                                 </th>

                              </tr>
                            </thead>

                            


              <?php if ($this->session->userdata('user_sub_category')==1 || $this->session->userdata('user_sub_category')==11  ) { ?>
                             <tr class="highlight">
                                 <td class="field" colspan="2"><strong>Professional Information</strong></td>
                              </tr>
                              
                            <tr class="highlight">
                                 <td class="field">Employment Status  </td>
                                 <td class="details">
                                  <?php  if(isset($profile_information[0]['employement_status'])){  

                                      if ($profile_information[0]['employement_status']!='Other') {
                                        echo $profile_information[0]['employement_status'];
                                      }else{
                                        echo $profile_information[0]['other_emp_status'];
                                      } 

                                  }else{
                                    echo '<span class="blank">?</span>';
                                  }

                                   ?>
                                   
                                 </td>
                            </tr>


                           <!--  <?php if(isset($profile_information[0]['other_emp_status'] ) && $profile_information[0]['other_emp_status']!='') { ?>
                                <tr class="highlight">
                                 <td class="field">Other Employment Status  </td>
                                 <td class="details"><?php echo  $profile_information[0]['other_emp_status']; ?></td>
                            </tr>
                          <?php } ?> -->
                            <tr class="highlight">
                                 <td class="field">Name of Organization</td>
                                 <td class="details"><?php echo isset($profile_information[0]['company_name']) ? $profile_information[0]['company_name'] : '<span class="blank">?</span>'; ?></td>
                            </tr>


                       <?php } ?>
                             <tr class="highlight">
                              <?php if ($this->session->userdata('user_sub_category')==2){ ?>
                                 <td class="field">Domain / Areas of Interest </td>
                               <?php } else{ ?>
                                 <td class="field">Domain / Areas of Expertise </td>
                               <?php } ?>


                                 <td class="details">
                                  <?php if (count($domain_info)>0) {

                                    foreach ($domain_info as $key => $value) {
                                      if (strtolower($value['domain_name']) !='other') {
                                        echo $value['domain_name'].'<br>';  
                                      }
                                      
                                         
                                      }
                                    if(isset($profile_information[0]['other_domian'] ) && $profile_information[0]['other_domian']!='') { 
                                      echo  $profile_information[0]['other_domian'];
                                     } 

                                  }else{echo '<span class="blank">?</span>';} ?>
                                    
                                  </td>
                            </tr>


                            <tr class="highlight">
                                 <td class="field">Organization Sector </td>
                                 <td class="details">
                                  <?php if (count($org_sector_info)>0) {

                                    foreach ($org_sector_info as $key => $value) {
                                      if ( strtolower($value['name'])!='other' ) {
                                        echo $value['name'].'<br>';  
                                      }
                                      
                                         
                                      }
                                    if(isset($profile_information[0]['org_sector_other'] ) && $profile_information[0]['org_sector_other']!='') { 
                                      echo  $profile_information[0]['org_sector_other'];
                                     } 

                                  }else{echo '<span class="blank">?</span>';} ?>
                                    
                                  </td>
                            </tr>

                         <!--    <?php if(isset($profile_information[0]['other_domian'] ) && $profile_information[0]['other_domian']!='') { ?>
                                <tr class="highlight">
                                 <td class="field">Other Domain/area of expertise  </td>
                                 <td class="details"><?php echo  $profile_information[0]['other_domian']; ?></td>
                            </tr>
                          <?php } ?> -->

                           <!--   <tr class="highlight">
                                 <td class="field">Areas of Interest </td>
                                 <td class="details">
                                  <?php if (count($intrest_info)>0) {

                                    foreach ($intrest_info as $key => $value) {
                                    echo $value['area_of_interest'].'<br>';
                                         
                                      }
                                  }else{echo '<span class="blank">?</span>';} ?>
                                    
                                  </td>
                            </tr> -->
                            <tr class="highlight">
                                 <td class="field">Skill Set </td>
                                 <td class="details">
                                  <?php if (count($skill_info)>0) {

                                    foreach ($skill_info as $key => $value) {
                                     if ( strtolower($value['name']) !='other') {
                                         echo $value['name'].'<br>';
                                      }
                                         
                                      }
                                     if(isset($profile_information[0]['other_skill_sets'] ) && $profile_information[0]['other_skill_sets']!='') { 
                                      echo $profile_information[0]['other_skill_sets'];
                                     }

                                  }else{echo '<span class="blank">?</span>';} ?>
                                    
                                  </td>
                            </tr>

                            <tr class="highlight">
                                 <td class="field">Years of experience</td>
                                 <td class="details"><?php echo isset($profile_information[0]['years_of_experience']) ? $profile_information[0]['years_of_experience'] : '<span class="blank">?</span>'; ?></td>
                              </tr>

                            
              <?php if ($this->session->userdata('user_sub_category')==1 || $this->session->userdata('user_sub_category')==11  ) { ?>

                         <!--  <?php if(isset($profile_information[0]['other_skill_sets'] ) && $profile_information[0]['other_skill_sets']!='') { ?>
                                <tr class="highlight">
                                 <td class="field">Other Skill Set  </td>
                                 <td class="details"><?php echo  $profile_information[0]['other_skill_sets']; ?></td>
                            </tr>
                          <?php } ?> -->
                     

                         

                           </tbody>
                        </table>
                        </div>
                      
                      <div class="table-responsive">
                        <table class="table table-profile">
                             <thead>
                              <tr>
                                
                                  <th>
                                    Designation
                                  </th>
                                 <!-- <th>
                                    Year
                                 </th> -->
                                 <th>
                                    Description
                                 </th>

                              </tr>
                           </thead>
                           <?php if (isset($profile_information[0]['designation']) && count($profile_information[0]['designation']) > 0 ) { ?>
                                 <tr class="highlight">
                                 <td colspan="1"><?php foreach ($profile_information[0]['designation'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                  <?php  } ?></td>
                                   <!-- <td><?php foreach ($profile_information[0]['designation_since_year'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td> -->
                                    <td><?php foreach ($profile_information[0]['designation_description'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td>
                                </tr>
                              <?php } else { ?>
                              <tr>
                                <td><span class="blank">?</span></td>
                                <!-- <td><span class="blank">?</span></td> -->
                                <td><span class="blank">?</span></td>
                              </tr>
                            <?php } ?>  
                          </table>
                        </div>
                  


                        <!-- <div class="table-responsive">
                        <table class="table table-profile">
                             <thead>
                              <tr>
                                 <th>
                                    Technical Experience In Year
                                 </th>
                                 <th>
                                    Technical Description Of Work
                                 </th>
                              </tr>

                           </thead>
                            <?php if (isset($profile_information[0]['technical_experience_in_year']) && count($profile_information[0]['technical_experience_in_year']) > 0 ) { 
                              ?>
                              <tr class="highlight">
                                 <td colspan="1">
                                  <?php foreach ($profile_information[0]['technical_experience_in_year'] as  $value) { ?>
                                      <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                  <?php  } ?>
                                </td>
                                <td>
                                  <?php foreach ($profile_information[0]['technical_description_of_work'] as $key =>  $value) { ?>
                                      <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?>
                                  </td>
                                </tr>
                            <?php } else { ?>
                              <tr>
                                <td><span class="blank">?</span></td>
                                <td><span class="blank">?</span></td>
                              </tr>
                            <?php } ?>  
                                

                          </table>
                        </div> -->

                      <?php } ?>
                        <?php if ($this->session->userdata('user_sub_category')==11  ) { ?>
                          <!-- $profile_information[0]['aplied_for_expert']=='yes') -->
                        <div class="table-responsive">
                        <table class="table table-profile">
                          <tr>
                                 <th colspan="2">
                                    <h4>Personal Information</h4>

                                    
                                 </th>

                              </tr>
                             
                            <?php if (isset($profile_information[0]['specify_fields_area_that_you_would_like']) && count($profile_information[0]['specify_fields_area_that_you_would_like']) > 0 ) { ?>
                        <!--       <tr class="">
                                 <td> Specify Fields or Area that you would like to be an expert, guide us on</td>
                                 <td colspan="1"><?php foreach ($profile_information[0]['specify_fields_area_that_you_would_like'] as  $key=>$value) { ?>
                                    <table>
                                      <tr>
                                      <td><?php echo ($key+1).")".$value ?></td>
                                      </tr>
                                      </table>
                                  <?php  } ?>
                                </td>
                                  
                                </tr> -->
                            <?php } else { ?>
                             <!--  <tr>
                                <td><span class="blank">?</span></td>
                               
                              </tr> -->
                            <?php } ?> 
                            <tr class="highlight">
                                 <td class="field">Profile in Brief</td>
                                 <td class="details"><?php echo isset($profile_information[0]['bio_data']) ? $profile_information[0]['bio_data'] : '<span class="blank">?</span>'; ?></td>
                              </tr>
                               
                              <tr class="highlight">
                                 <td class="field">No Of Paper</td>
                                 <td class="details"><?php echo isset($profile_information[0]['no_of_paper_publication']) ? $profile_information[0]['no_of_paper_publication'] : '<span class="blank">?</span>'; ?></td>
                              </tr>
                              <?php 

                              if(isset($profile_information[0]['no_of_paper_publication']) && $profile_information[0]['no_of_paper_publication']>0 ){ ?>
                              <tr>
                                <td colspan="2">
                                <table class="table table-profile">
                                  <tr>
                                <th>Year</th>  
                                <th>Conference Name  </th>  
                                <th>Title</th> 
                                </tr>
                                
                                  <?php  foreach ($profile_information[0]['paper_year'] as  $key=>$value) { 
                                    $paper_conf_name=$profile_information[0]['paper_conf_name'];
                                    $paper_title=$profile_information[0]['paper_title'];
                                    ?>

                                    <tr>
                                    <td><?php echo $value ?></td>
                                    <td><?php echo $paper_conf_name[$key] ?></td>
                                    <td><?php echo $paper_title[$key] ?></td>
                                    </tr>
                                  <?php } ?>
                              </table>
                              </td>
                            </tr>
                          <?php } ?>


                              <tr class="highlight">
                                 <td class="field">No Of Patents</td>
                                 <td class="details"><?php echo isset($profile_information[0]['no_of_patents']) ? $profile_information[0]['no_of_patents'] : '<span class="blank">?</span>'; ?></td>
                              </tr>
            
                    <?php if(isset($profile_information[0]['no_of_patents']) && $profile_information[0]['no_of_patents']>0 ){ ?>
                              <tr>
                                
                                <td colspan="2">
                                <table class="table table-profile">
                                  <tr>
                                <th>Year</th>  
                                <th>Patent Number</th>  
                                <th>Title</th> 
                                </tr>
                                
                                  <?php  foreach ($profile_information[0]['patent_year'] as  $key=>$value) { 
                                    $patent_number=$profile_information[0]['patent_number'];
                                    $patent_title=$profile_information[0]['patent_title'];
                                    ?>

                                    <tr>
                                    <td><?php echo $value ?></td>
                                    <td><?php echo $patent_number[$key] ?></td>
                                    <td><?php echo $patent_title[$key] ?></td>
                                    </tr>
                                  <?php } ?>
                                   
                              </table>
                              </td>
                            </tr>
                             <?php } ?>

                            <?php if(isset($profile_information[0]['portal_name_multiple']) && $profile_information[0]['portal_name_multiple']>0 ){ ?>

                              <tr>
                                
                                <td colspan="2">
                                <table class="table table-profile">
                             <h4>Portal Information</h4>
                                  
                                  <tr>
                                <th>Portal Name</th>  
                                <th>Portal Link</th>  
                                <th>Portal Description</th> 
                                </tr>
                                
                                  <?php  foreach ($profile_information[0]['portal_name_multiple'] as  $key=>$value) { 
                                    $portal_link_multiple=$profile_information[0]['portal_link_multiple'];
                                    $portal_description_multiple=$profile_information[0]['portal_description_multiple'];
                                    ?>

                                    <tr>
                                    <td><?php echo $value ?></td>
                                    <td><?php echo $portal_link_multiple[$key] ?></td>
                                    <td><?php echo $portal_description_multiple[$key] ?></td>
                                    </tr>
                                  <?php } ?>
                                   
                              </table>
                              </td>
                            </tr>
                             <?php } ?>
                       <!--      
                             <tr class="highlight">
                                 <td class="field">Portal Name</td>
                                 <td class="details"><?php echo isset($profile_information[0]['portal_name']) ? $profile_information[0]['portal_name'] : '<span class="blank">?</span>'; ?></td>
                              </tr>
                              <tr class="highlight">
                                 <td class="field">Portal Link</td>
                                 <td class="details"><?php echo isset($profile_information[0]['portal_link']) ? $profile_information[0]['portal_link'] : '<span class="blank">?</span>'; ?></td>
                              </tr>
                              <tr class="highlight">
                                 <td class="field">Portal Description</td>
                                 <td class="details"><?php echo isset($profile_information[0]['portal_description']) ? $profile_information[0]['portal_description'] : '<span class="blank">?</span>'; ?></td>
                              </tr> -->

                          </table>
                        </div>
                      <?php } ?>
              
              <?php if ($this->session->userdata('user_sub_category')==2) { ?>
                

               <table class="table table-profile">
                  <thead>
                    <tr>
                      <h4>Student Information</h4>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="highlight">
                      <td class="field">Status</td>
                      <?php if(isset($student_profile[0]['status'])) { ?>
                        <td class="details"><?php echo $student_profile[0]['status'] ?></td>
                      <?php } ?>
                    </tr>
                    <tr class="highlight">
                      <td class="field">University Course</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['university_course'])) { ?>
                          <?php foreach ($student_profile[0]['university_course'] as $key => $value) { 
                          echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">University Since Year</td>
                      <td class="details">
                         <?php if(isset($student_profile[0]['university_since_year'])) { ?>
                            <?php foreach ($student_profile[0]['university_since_year'] as $key => $value) { 
                              echo $value.'<br>';
                            } ?>
                          <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">University To Year</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['university_to_year'])) { ?>
                          <?php foreach ($student_profile[0]['university_to_year'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Institution Name</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['university_name'])) { ?>
                          <?php foreach ($student_profile[0]['university_name'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">University Location</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['university_location'])) { ?>
                          <?php foreach ($student_profile[0]['university_location'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>

                
                    <tr class="highlight">
                      <td class="field">University Location</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['university_location'])) { ?>
                          <?php foreach ($student_profile[0]['university_location'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php if(isset($student_profile[0]['additional_detail']) && $student_profile[0]['additional_detail']!='') { ?>
                    <tr class="highlight">
                      <td class="field">Additional Details</td>
                     
                        <td class="details"><?php echo $student_profile[0]['additional_detail'] ?></td>
                     
                    </tr>
                     <?php } ?>
                    <!-- <tr class="highlight">
                      <td class="field">Current Study Course</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['current_study_course'])) { ?>
                          <?php foreach ($student_profile[0]['current_study_course'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Current Study Since Year</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['current_study_since_year'])) { ?>
                          <?php foreach ($student_profile[0]['current_study_since_year'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Current Study To Year</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['current_study_to_year'])) { ?>
                          <?php foreach ($student_profile[0]['current_study_to_year'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Current Study Description</td>
                      <td class="details">
                        <?php if(isset($student_profile[0]['current_study_description'])) { ?>
                          <?php foreach ($student_profile[0]['current_study_description'] as $key => $value) { 
                            echo $value.'<br>';
                          } ?>
                        <?php } ?>
                      </td>
                    </tr> -->
                   
                  </tbody>
                </table>

                 <div class="table-responsive">
                  <h4> Notable Accomplishments </h4>
                        <table class="table table-profile">
                             <thead>
                              <tr>
                                
                                  <th>
                                    Year
                                    
                                  </th>
                                 <!-- <th>
                                    Year
                                 </th> -->
                                 <th>
                                    Description of Work
                                 </th>

                              </tr>
                           </thead>
                           <?php 
                           if (isset($profile_information[0]['event_experience_in_year']) && count($profile_information[0]['event_experience_in_year']) > 0 ) { 
                            ?>
                                 <tr class="highlight">
                                 <td colspan="1"><?php foreach ($profile_information[0]['event_experience_in_year'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                  <?php  } ?></td>
                                   <!-- <td><?php foreach ($profile_information[0]['designation_since_year'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td> -->
                                    <td><?php foreach ($profile_information[0]['event_description_of_work'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td>
                                </tr>
                              <?php } else { ?>
                              <tr>
                                <td><span class="blank">?</span></td>
                                <!-- <td><span class="blank">?</span></td> -->
                                <td><span class="blank">?</span></td>
                              </tr>
                            <?php } ?>  
                          </table>
                        </div>

                        <div class="table-responsive">
                          <h4>Projects / Internships</h4>
                        <table class="table table-profile">
                             <thead>
                              <tr>
                                
                                  <th>
                                     From
                                  </th>
                                 <th>
                                    To
                                 </th>
                                 <th>
                                    Description of Work
                                 </th>

                              </tr>
                           </thead>
                           <?php 
                           if (isset($profile_information[0]['technical_experience_in_year']) && count($profile_information[0]['technical_experience_in_year']) > 0 ) { 
                            ?>
                                 <tr class="highlight">
                                 <td colspan="1"><?php foreach ($profile_information[0]['technical_experience_in_year'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                  <?php  } ?></td>
                                   <td><?php foreach ($profile_information[0]['technical_experience_to_year'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td>
                                    <td><?php foreach ($profile_information[0]['technical_description_of_work'] as  $value) { ?>
                                       <p><?php echo isset($value) && $value!='' ? $value  : '-<br>'; ?></p>
                                    <?php  } ?></td>
                                </tr>
                              <?php } else { ?>
                              <tr>
                                <td><span class="blank">?</span></td>
                                <!-- <td><span class="blank">?</span></td> -->
                                <td><span class="blank">?</span></td>
                              </tr>
                            <?php } ?>  
                          </table>
                        </div>
                <?php } ?>
                

                 <table class="table table-profile">
                  <thead>
                    <tr>
                      
                      <h4>Billing Information</h4>
                    </tr>
                  </thead>
                  <tbody>

                    <tr class="highlight">
                      <td class="field">Pincode</td>
                      <td class="details">
                        <?php echo isset($profile_information[0]['pincode']) ? $profile_information[0]['pincode'] : '<span class="blank">?</span>'; ?>
           
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Flat / House / Building</td>
                      <td class="details">
                         <?php echo isset($profile_information[0]['flat_house_building_apt_company']) ? $profile_information[0]['flat_house_building_apt_company'] : '<span class="blank">?</span>'; ?>
                        
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Area / Colony</td>
                      <td class="details">
                         <?php echo isset($profile_information[0]['area_colony_street_village']) ? $profile_information[0]['area_colony_street_village'] : '<span class="blank">?</span>'; ?>
                        
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Town / City / State</td>
                      <td class="details">
                        <?php echo isset($profile_information[0]['town_city_and_state']) ? $profile_information[0]['town_city_and_state'] : '<span class="blank">?</span>'; ?>
                      </td>
                    </tr>
                    <tr class="highlight">
                      <td class="field">Country</td>
                      <td class="details">
                        <?php echo isset($profile_information[0]['country']) ? $profile_information[0]['country'] : '<span class="blank">?</span>'; ?>
                      </td>
                    </tr>
                     </tbody>
                </table>
                  
                </div>
                
                  <!-- <div class="col-3">
                    <p>Profile Status</p>
                    <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </div> -->


                </div>
                  
            
       
                </div>
            </div>

        </div>




    </div>
</section>

<script>
        $('#discoverable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $this->session->userdata('user_id') ; ?>';
            
            var discoverable 
            if ($('#discoverable:checked').val()=='on') {
              discoverable = 'yes';
            $("#contactable").prop("disabled", false);
            }else{
              discoverable = 'no';
              $('#contactable').prop('checked', false); // Unchecks it
              $("#contactable").prop("disabled", true);
            

            }

         var datastring='discoverable='+ discoverable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_discoverable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
          
          $('#contactable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $this->session->userdata('user_id') ; ?>';
            
            var contactable 
            if ($('#contactable:checked').val()=='on') {
              contactable = 'yes';
            }else{
              contactable = 'no';
            }

            var datastring='contactable='+ contactable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_contactable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
   
        
$(window).on('load', function () {
      var  discoverable = '<?php echo $user[0]['discoverable'] ; ?>';
        if (discoverable =='no') {
              ;
           
                $("#contactable").prop("disabled", true);
            }else{
              $("#contactable").prop("disabled", false);
            

            }
      
});          
</script>