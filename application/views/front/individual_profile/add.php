<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>datepicker/datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<style>
    .mandatory {
    color: red;
    ::-webkit-datetime-edit { color: transparent; }
    :focus::-webkit-datetime-edit { color: #000; }
    }

    .previous_img_outer { position: relative; }
    .previous_img_outer a.img_outer img { max-width: 300px; max-height: 100px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
    .previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
</style>
<?php $enc_obj =  New Opensslencryptdecrypt(); ?>
    <div class="profileinfo" data-aos="fade-down">
        <div class="container">
            <div class="row">

                <div class="col-md-2" id="profile_header_iamge">
                    <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
                      
                    <?php }else{ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
                  <?php } ?>
                </div>
                <div class="col-md-10">
                    <h3 class="mt-4"><?php echo $user[0]['title']." ".$user[0]['first_name']." ".$user[0]['last_name']; ?></h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-paper-plane"></i> <?php echo $user[0]['email'] ?></a></li>
                        <li><i class="fa fa-user"></i> <?php echo $user[0]['sub_catname'] ?></li>
                    </ul>
                    <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo base_url('profile') ?>" class="btn btn-upload2">Back To Profile</a>
                    </div>
                   
                </div>
                <<!-- div class="col-md-4">
                    <div class="countBox">
                       
                    </div>
                    <div class="countBox">
                       
                    </div>

                </div> -->
            </div>
        </div>
    </div>
<section id="individual-profile-form" class="inner-page" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="formInfo">
                     <div class="col-12">
                        <?php if ($this->session->flashdata('success') != "") { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="fa fa-check"></i> Success!</h5>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php } ?>
                              <?php if ($this->session->flashdata('error') != "") { ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Error!</h5>
                                <?php echo $this->session->flashdata('error'); ?>
                            </div>

                        <?php } ?>
                    <?php 
                        if(validation_errors()!="" ){
                            validation_errors();  
                        }
                    ?>
                    </div>
                   <form method="post" id="individualProfileForm" name="frm" role="form" enctype="multipart/form-data">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                        <?php 
                            $ProfileId = '';
                            if(isset($profile_information[0]['id']))
                            {
                                $ProfileId = $profile_information[0]['id'];
                            }
                        ?>
                        <input type="hidden" name="profileId" value="<?php echo $ProfileId ?>">

                        <div id="smartwizard">
                            <ul>
                                <li><a href="#step-1">Step 1<br /><small>Professional Details</small></a></li>
                                <li><a href="#step-2">Step 2<br /><small>Billing Details</small></a></li>
                                <?php if ( $user[0]['user_sub_category_id'] == 11 ): ?>
                                <li><a href="#step-3">Step 3<br /><small>Details to Claim Your Expert Status</small></a></li>
                                    
                                <?php endif ?>

                            </ul>                         
                            <div class="tab-content mt-4">
                                <div id="step-1" class="tab-pane" role="tabpanel">
                                    <div class="row">
                                        <input type="hidden" name="type" value="Individual">
                                        <div class="col-md-6">                               
                                            <div class="form-group">
                                                <?php 
                                                    $employementStatus = '';
                                                    if(isset($profile_information[0]['employement_status']))
                                                    {
                                                        $employementStatus = $profile_information[0]['employement_status'];
                                                    }
                                                ?>
                                                
                                        <select class="form-control" name="employement_status" id="employement_status">


                                        <?php foreach ($employement_status as $key => $status_value) { ?>
                                            <option value="<?php echo $status_value['name'] ?>" <?php if($employementStatus == $status_value['name']) { ?> selected="selected" <?php } ?> ><?php echo $status_value['name'] ?></option>
                                        <?php  } ?>



                                            <!-- <option value="">Employement Status</option> -->
                                        <!--     <option value="Self Employed" <?php if($employementStatus == "Self Employed") { ?> selected="selected" <?php } ?> >Self Employed</option>
                                            <option value="Consultant" <?php if($employementStatus == "Consultant") { ?> selected="selected" <?php } ?> >Consultant</option>
                                            <option value="Freelancer" <?php if($employementStatus == "Freelancer") { ?> selected="selected" <?php } ?> >Freelancer</option>
                                            <option value="Company" <?php if($employementStatus == "Company") { ?> selected="selected" <?php } ?> >Company</option>
                                                
                                            <option <?php if($employementStatus == "Academia") { ?> selected="selected" <?php } ?> value="Academia">Academia</option>
                                            <option <?php if($employementStatus == "Administration") { ?> selected="selected" <?php } ?> value="Administration">Administration</option>
                                            <option <?php if($employementStatus == "Blue Collar Worker") { ?> selected="selected" <?php } ?> value="Blue Collar Worker">Blue Collar Worker</option>
                                            <option <?php if($employementStatus == "Entrepreneur") { ?> selected="selected" <?php } ?> value="Entrepreneur">Entrepreneur</option>
                                            <option <?php if($employementStatus == "Executive") { ?> selected="selected" <?php } ?> value="Executive">Executive</option>
                                            <option <?php if($employementStatus == "Homemaker") { ?> selected="selected" <?php } ?> value="Homemaker">Homemaker</option>
                                            <option <?php if($employementStatus == "Independent Consultant") { ?> selected="selected" <?php } ?> value="Independent Consultant">Independent Consultant</option>
                                            <option <?php if($employementStatus == "Intern / Trainee") { ?> selected="selected" <?php } ?> value="Intern / Trainee">Intern / Trainee</option>
                                            <option <?php if($employementStatus == "Professor") { ?> selected="selected" <?php } ?> value="Professor">Professor</option>
                                            <option <?php if($employementStatus == "Retired") { ?> selected="selected" <?php } ?> value="Retired">Retired</option>
                                            <option <?php if($employementStatus == "Service") { ?> selected="selected" <?php } ?> value="Service">Service</option>
                                            <option <?php if($employementStatus == "Student") { ?> selected="selected" <?php } ?> value="Student">Student</option>
                                            <option <?php if($employementStatus == "Subject Matter Expert") { ?> selected="selected" <?php } ?> value="Subject Matter Expert">Subject Matter Expert</option>
                                            <option <?php if($employementStatus == "Teacher") { ?> selected="selected" <?php } ?> value="Teacher">Teacher</option>
                                            <option <?php if($employementStatus == "Technician") { ?> selected="selected" <?php } ?> value="Technician">Technician</option>
                                            <option <?php if($employementStatus == "Working Professional") { ?> selected="selected" <?php } ?> value="Working Professional">Working Professional</option>
                                            <option value="Other" <?php if($employementStatus == "Other") { ?> selected="selected" <?php } ?> >Other</option> -->
                                        </select>
                                        <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                                <label class="form-control-placeholder leftLable2 flotingCss" for="cname">Professional Status
                                            <em>*</em></label>
                                                <span class="error"><?php echo form_error('employement_status'); ?></span>
                                            </div>
                                        </div>

                                        <?php 
                                                    $other_emp_status = '';
                                                    $class='d-none';
                                                    if(isset($profile_information[0]['other_emp_status'] ) && $profile_information[0]['other_emp_status']!='')
                                                    {
                                                        $class='';
                                                        $other_emp_status = $profile_information[0]['other_emp_status'];
                                                    }
                                                ?>
                                         <div class="col-md-6">
                                            <div class="form-group <?php echo $class ?>" id="other_emp_status_div">
                                                   <input type="text" name="other_emp_status" value="<?php echo $other_emp_status ?>" id="other_emp_status" class="form-control" required="">
                                                    <label for="other_emp_status" class="form-control-placeholder">Other Employement Status  <em class="mandatory">*</em></label>
                                                   <div class="error" style="color:#F00"><?php echo form_error('other_emp_status'); ?> </div>
                                          </div>
                                        </div>
                                        
                                       
                                    </div>


                                    <div class="row">

                                        <div class="col-md-6">                 
                                            <div class="form-group">
                                                <?php 
                                                    $companyName = '';
                                                    if(isset($profile_information[0]['company_name']))
                                                    {
                                                        $companyName = $profile_information[0]['company_name'];
                                                    }
                                                ?>
                                             
                                                <input type="text" class="form-control" id="company_name"  name="company_name" value="<?php echo $companyName; ?>" maxlength="200">
                                                <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Name of Organization</label>
                                                <span class="error"><?php echo form_error('company_name'); ?></span>
                                            </div>  
                                        </div>
                                        
                                    </div>

                                    <div class="row">                                       
                                    <?php $explode_org_sector_arr = array();
                                    
                                        
                                            if(isset($profile_information[0]['org_sector']) && $profile_information[0]['org_sector'] != "")
                                            {
                                                $explode_org_sector_arr = explode(",",$profile_information[0]['org_sector']);
                                            }
                                            

                                            ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-placeholder" for="org_sector">Organization Sector <em>*</em></label>
                                                <select class="form-control select2_common" name="org_sector[]" id="org_sector" required multiple autofocus onchange="show_hide_other_org_sector()">
                                                        <?php if(!empty($org_sector_data))
                                                        {
                                                            foreach($org_sector_data as $res) 
                                                            { ?>
                                                            <option value="<?php echo $res['id'] ?>" <?php if(in_array($res['id'],$explode_org_sector_arr)) { echo "selected"; } ?>><?php echo ucfirst($res['name']); ?></option>
                                                            <?php }
                                                        }   ?>                                              
                                                </select>
                                                 <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                                <?php if(form_error('org_sector')!=""){ ?><span class="error"><?php echo form_error('org_sector'); ?></span> <?php } ?>
                                            </div>
                                        </div>
                                    
                                            <?php 
                                                    $org_sector_other = '';
                                                    $class='d-none';
                                                    if(isset($profile_information[0]['org_sector_other'] ) && $profile_information[0]['org_sector_other']!='')
                                                    {
                                                        $class='';
                                                        $org_sector_other = $profile_information[0]['org_sector_other'];
                                                    }
                                                ?>
                                          
                                          <div class="col-md-6 d-none" id="org_sector_other_div">
                                            <div class="form-group">
                                                <input type="text" name="org_sector_other" id="org_sector_other" class="form-control" required="" value="<?php echo $org_sector_other  ?>">
                                                <label for="org_sector_other" class="form-control-placeholder">Other Organization Sector <em class="mandatory">*</em></label>
                                                <?php if(form_error('org_sector_other')!=""){ ?><span class="error"><?php echo form_error('org_sector_other'); ?></span> <?php } ?>
                                            </div>
                                        </div>

                                      
                                </div>

                                    <div id="designation_row">

                                    <?php if (isset($profile_information[0]['designation']) && count($profile_information[0]['designation'])> 0 ) { 
                                        $since_year=$profile_information[0]['designation_since_year'];
                                        $desn_desc=$profile_information[0]['designation_description'];
                                       foreach ($profile_information[0]['designation'] as $key => $value) {?>
                                    <div class="row">
                                        <div class="col-md-5">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="<?php echo $value; ?>" class="form-control designation" id="designation" placeholder="Enter Designation" name="designation[]" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Designation <em class="mandatory">*</em></label>
                                              <span class="error"><?php echo form_error('designation'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-3 d-none">                 
                                            <div class="form-group">
                                               
                                              <input type="date" class="form-control designation_since_year" id="designation_since_year" placeholder="" name="designation_since_year[]" value="<?php echo $since_year[$key]  ?>" maxlength="200">
                                              <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">From<em class="mandatory">*</em></label>

                                              <span class="error"><?php echo form_error('designation_since_year'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                               
                                                <textarea name="designation_description[]" class="form-control designation_description" id="designation_description0" rows="1"><?php echo $desn_desc[$key]; ?></textarea>
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Designation Description</label>

                                                <!-- <span type="button" class="error"><?php echo form_error('designation_description'); ?></span> -->
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                             <?php if ($key==0) {?>
                                                <button class="add_field_button btn btn-success btn-sm">+</button>
                                             <?php }else{ ?>   
                                                <button type="button" class="remove_field btn btn-danger btn-sm">-</button>
                                            <?php } ?>
                                            
                                            </div>
                                            </div>
                                    </div>
                                         
                                     <?php } }
                                     else{
                                      ?>

                                        <div class="row">
                                        <div class="col-md-5">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="" class="form-control designation" id="designation" name="designation[]" maxlength="200">
                                              <label  class="form-control-placeholder" for="exampleInputEmail1">Designation<em class="mandatory">*</em></label>

                                              <span class="error"><?php echo form_error('designation'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-3 d-none">                 
                                            <div class="form-group">
                                               
                                          
                                              <input type="date" class="form-control designation_since_year" id="designation_since_year" placeholder="" name="designation_since_year[]" maxlength="200">
                                                <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">From<em class="mandatory">*</em></label>
                                              <span class="error"><?php echo form_error('designation_since_year'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                               
                                               
                                                <textarea name="designation_description[]" class="form-control designation_description" id="designation_description" rows="1"></textarea>
                                                 <label class="form-control-placeholder" for="exampleInputEmail1">Designation Description</label>
                                                <!-- <span type="button" class="error"><?php echo form_error('designation_description'); ?></span> -->
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                                <button class="add_field_button btn btn-success btn-sm">+</button>
                                            </div>
                                            </div>
                                    </div>


                                     <?php } ?> 
                                    </div>

                                
                                    <div class="row">
                                   
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <?php 
                                                    $Domain = '';
                                                    if(isset($profile_information[0]['domain_area_of_expertise']))
                                                    {
                                                        $Domain = $profile_information[0]['domain_area_of_expertise'];
                                                    }
                                                    // print_r($Domain);
                                                ?>
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Domain / Areas of Expertise<em class="mandatory">*</em></label>
                                                <select name="domain_and_area_of_expertise[]" class="domainName form-control" size="50" autocomplete="nope" multiple="multiple" required id="domain_area_of_expertise">
                                                    <?php foreach ($domain as $key => $value) { ?>
                                                        <?php if(in_array($domain[$key]['id'], $Domain)) { ?>
                                                            <option value="<?php echo $domain[$key]['id'] ?>" selected="selected" data-id='<?php echo $domain[$key]['domain_name'] ?>'><?php echo $domain[$key]['domain_name'] ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $domain[$key]['id'] ?>" data-id='<?php echo $domain[$key]['domain_name'] ?>'><?php echo $domain[$key]['domain_name'] ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                                <span class="error"><?php echo form_error('domain_and_area_of_expertise'); ?></span>
                                            </div>
                                        </div>

                                         <?php 
                                                    $other_domian = '';
                                                    $class='d-none';
                                                    if(isset($profile_information[0]['other_domian'] ) && $profile_information[0]['other_domian']!='')
                                                    {
                                                        $class='';
                                                        $other_domian = $profile_information[0]['other_domian'];
                                                    }
                                                ?>
                                         <div class="col-md-6">
                                            <div class="form-group <?php echo $class ?>" id="other_domian_div">
                                                   <input type="text" name="other_domian" value="<?php echo $other_domian ?>" id="other_domian" class="form-control" required="">
                                                    <label for="other_domian" class="form-control-placeholder">Other Domain / Areas of Expertise  <em class="mandatory">*</em></label>
                                                     <span><small>Please enter comma seprated domain / areas of expertise</small></span>
                                                   <div class="error" style="color:#F00"><?php echo form_error('other_domian'); ?> </div>
                                          </div>
                                        </div>

                                        
                                        <div class="col-md-6 d-none">
                                            <div class="form-group">
                                                <?php 
                                                    $AreaOfInterest = '';
                                                    if(isset($profile_information[0]['areas_of_interest']))
                                                    {
                                                        $AreaOfInterest = $profile_information[0]['areas_of_interest'];
                                                    }
                                                ?>
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Areas of Interest<em class="mandatory">*</em></label>
                                              <select name="areas_of_interest[]" class="areaOfInterestName form-control" autocomplete="nope" multiple="multiple" required id="areas_of_interest">
                                                <?php foreach ($area_of_interest as $key => $value) { ?>
                                                    <?php if(in_array($area_of_interest[$key]['id'], $AreaOfInterest)) { ?>
                                                        <option value="<?php echo $area_of_interest[$key]['id'] ?>" selected="selected"><?php echo $area_of_interest[$key]['area_of_interest'] ?></option>
                                                        <?php } else { ?>
                                                        <option value="<?php echo $area_of_interest[$key]['id'] ?>"><?php echo $area_of_interest[$key]['area_of_interest'] ?></option>
                                                        <?php } ?>
                                                <?php } ?>
                                              </select>
                                              <span class="error"><?php echo form_error('areas_of_interest'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="row">
                                    
                                         
                                           <div class="col-md-6">
                                            <div class="form-group">
                                                <?php 
                                                    $skillSets = '';
                                                    if(isset($profile_information[0]['skill_sets']))
                                                    {
                                                        $skillSets = $profile_information[0]['skill_sets'];
                                                    }
                                                ?>
                                                <select name="skill_sets[]" class="skillSets form-control" autocomplete="nope" multiple="multiple" required id="skill_sets">
                                                <?php foreach ($skill_sets as $key => $value) { ?>
                                                    <?php if(in_array($skill_sets[$key]['id'], $skillSets)) { ?>
                                                    <option value="<?php echo $skill_sets[$key]['id'] ?>" selected="selected" data-id='<?php echo $skill_sets[$key]['name'] ?>'><?php echo $skill_sets[$key]['name'] ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $skill_sets[$key]['id'] ?>" data-id='<?php echo $skill_sets[$key]['name'] ?>'><?php echo $skill_sets[$key]['name'] ?></option>
                                                        <?php } ?>
                                                <?php } ?>
                                                </select>
                                                <small><strong>Note:</strong> If the requisite field is not listed, kindly select "Other" and enter the desired text.</small>
                                                <label class="form-control-placeholder leftLable2 flotingCss" for="exampleInputEmail1">Skill Sets<em class="mandatory">*</em></label>

                                                <span class="error"><?php echo form_error('skill_sets'); ?></span>
                                            </div>
                                        </div>

                                        <?php 
                                                    $other_skill_sets = '';
                                                    $class='d-none';
                                                    if(isset($profile_information[0]['other_skill_sets'] ) && $profile_information[0]['other_skill_sets']!='')
                                                    {
                                                        $class='';
                                                        $other_skill_sets = $profile_information[0]['other_skill_sets'];
                                                    }
                                                ?>
                                         <div class="col-md-6">
                                            <div class="form-group <?php echo $class ?>" id="other_skill_sets_div">
                                                   <input type="text" name="other_skill_sets" value="<?php echo $other_skill_sets ?>" id="other_skill_sets" class="form-control" required="">
                                                    <label for="other_skill_sets" class="form-control-placeholder">Other Skill Sets  <em class="mandatory">*</em></label>
                                                    <span><small>Please enter comma seprated skill sets</small></span>
                                                   <div class="error" style="color:#F00"><?php echo form_error('other_skill_sets'); ?> </div>
                                          </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <?php 
                                                    $yearsOfExperience = '';
                                                    if(isset($profile_information[0]['years_of_experience']))
                                                    {
                                                        $yearsOfExperience = $profile_information[0]['years_of_experience'];
                                                    }
                                                ?>
                                               <label class="form-control-placeholder flotingCss" for="exampleInputEmail1">Years of Experience<em class="mandatory">*</em></label>
                                                <select name="years_of_experience" id="years_of_experience" class="form-control" autocomplete="nope" >
                                                    <option value=""></option>
                                                    <?php for ($i=0; $i <=100 ; $i++) { ?>
                                                        <option <?php if($i == $yearsOfExperience) {  echo "selected"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                    <?php }?>
                                          
                                                </select>
                                                 
                                                <span><?php echo form_error('years_of_experience'); ?></span>
                                            </div>
                                        </div>
                                   <!--      <div class="col-md-6">
                                            <div class="form-group">
                                                <?php 
                                                    $corporateLinkageCode = '';
                                                    if(isset($profile_information[0]['corporate_linkage_code']))
                                                    {
                                                        $corporateLinkageCode = $profile_information[0]['corporate_linkage_code'];
                                                    }
                                                ?>
                                              <input type="text" class="form-control" id="corporate_linkage_code"  name="corporate_linkage_code" value="<?php echo $corporateLinkageCode; ?>" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Corporate Linkage Code</label>

                                              <span class="error"><?php echo form_error('corporate_linkage_code'); ?></span>
                                            </div> 
                                        </div>
 -->


                                              <div class="form-group col-6" >
                                              
                                               <input type="text" class="form-control"  name="promo_code" id="promo_code" placeholder="" value="<?php echo set_value('promo_code')?>">
                                                <label class="form-control-placeholder" for="promo_code">Enter Corporate Linkage Code</label>
                                               <input type="hidden" name="promo_code_type" id="promo_code_type" value="">
                                               <input type="hidden" name="org_id" id="org_id" value="">
                                               <input type="hidden" name="promo_code_id" id="promo_code_id" value="">

                                               

                                               <div class="error" style="color:#F00"><?php echo form_error('promo_code'); ?> </div>

                                                 <span style="color:green" id="promo_success"></span>
                                               <span style="color:red" id="promo_error"></span>

                                            </div>

                                            
                                             

                                  </div>
                      

                                       


                                 
            


                                    <!-- ------------------------------------------------- -->

                                    
                                    <div class="row">
                                            <div class="col-md-3"> 

                                            <div class="form-group upload-btn-wrapper">
                                                <button class="btn btn-upload"><i class="fa fa-plus"> </i> Profile Picture </button>
                                                <input type="file" class="form-control" name="profile_picture" id="profile_picture" placeholder="" value="">
                                            </div>
                                            <p style="color:red;font-size: 11px" >Note: Please upload .jpg or .png or .jpeg file less than 500 KB</p>
                                         
                                        </div>

                                     
                                             <div class="col-md-2">
                                                    <?php if (isset($profile_information[0]['profile_picture']) &&  $profile_information[0]['profile_picture'] !='' ) { 


                                                        ?>

                                            <div class="col-md-6" id="profile_picture_outer">
                                                <div class="form-group">
                                                    <div class="previous_img_outer">
                                                        <a class="img_outer" href="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" target="_blank">
                                                           
                                                            <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>">
                                                        </a>
                                                        <a class="btn btn-danger btn-sm" onclick="delete_single_file_expert('<?php echo $enc_obj->encrypt('arai_student_profile'); ?>', '<?php echo $enc_obj->encrypt('id'); ?>', '<?php echo $enc_obj->encrypt($profile_information[0]['id']); ?>', 'profile_picture', '<?php echo $enc_obj->encrypt('./assets/profile_picture/'); ?>', 'Profile Picture')" href="javascript:void(0)">Delete</a>
                                                    </div>
                                                </div>
                                            </div>

                                                      
                                                    <?php } ?>
                                        
                                                </div>
                                    </div>
                                  

                               </div>
                                    
                                <div id="step-2" class="tab-pane" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php 
                                                    $BillingPincode = '';
                                                    if(isset($profile_information[0]['pincode']))
                                                    {
                                                        $BillingPincode = $profile_information[0]['pincode'];
                                                    }
                                                ?>
                                             
                                              <input type="text" class="form-control" id="pincode"  name="pincode" value="<?php echo $BillingPincode ?>" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Pincode</label>
                                              <span class="error"><?php echo form_error('pincode'); ?></span>
                                            </div> 
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php 
                                                    $Flat = '';
                                                    if(isset($profile_information[0]['flat_house_building_apt_company']))
                                                    {
                                                        $Flat = $profile_information[0]['flat_house_building_apt_company'];
                                                    }
                                                ?>
                                              <input type="text" class="form-control" id="flat_house_building_apt_company"  name="flat_house_building_apt_company" value="<?php echo $Flat ?>" maxlength="200">
                                              <label  class="form-control-placeholder" for="exampleInputEmail1">Flat / House No. / Building / Apt / Company</label>

                                              <span class="error"><?php echo form_error('flat_house_building_apt_company'); ?></span>
                                            </div> 
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php 
                                                    $Area = '';
                                                    if(isset($profile_information[0]['area_colony_street_village']))
                                                    {
                                                        $Area = $profile_information[0]['area_colony_street_village'];
                                                    }
                                                ?>
                                              <input type="text" class="form-control" id="area_colony_street_village"  name="area_colony_street_village" value="<?php echo $Area ?>" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Area / Colony / Street / Village</label>

                                              <span class="error"><?php echo form_error('area_colony_street_village'); ?></span>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php 
                                                    $Town = '';
                                                    if(isset($profile_information[0]['town_city_and_state']))
                                                    {
                                                        $Town = $profile_information[0]['town_city_and_state'];
                                                    }
                                                ?>
                                                <input type="text" class="form-control" id="town_city_and_state"  name="town_city_and_state" value="<?php echo $Town ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Town / City and State</label>

                                                <span class="error"><?php echo form_error('town_city_and_state'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php 
                                                    $Country = '';
                                                    if(isset($profile_information[0]['country']))
                                                    {
                                                        $Country = $profile_information[0]['country'];
                                                    }
                                                ?>
                                              <input type="text" class="form-control" id="country"  name="country" value="<?php echo $Country ?>" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Country</label>

                                              <span class="error"><?php echo form_error('country'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>
                                <?php if ( $user[0]['user_sub_category_id']  == 11 ){ ?>
                                <div id="step-3" class="tab-pane" role="tabpanel">
                                    <div id="wrapper_div"> 
                               
                                 <?php if (isset($profile_information[0]['specify_fields_area_that_you_would_like']) && count($profile_information[0]['specify_fields_area_that_you_would_like'])> 0 ) { 
                                        $since_year=$profile_information[0]['designation_since_year'];
                                        $desn_desc=$profile_information[0]['designation_description'];
                                       foreach ($profile_information[0]['designation'] as $key => $value) {?>
                                    <!--   <div class="row">
                                        <div class="col-md-11">
        
                                             
                                            <div class="form-group">
                                                
                                                 <textarea name="specify_fields_area_that_you_would_like[]" class="form-control specify_fields_area_that_you_would_like" id="specify_fields_area_that_you_would_like" rows="1" maxlength="400"><?php echo $value; ?></textarea>
                                                 <label class="form-control-placeholder" for="exampleInputEmail1">Please mention your areas of expertise<em class="mandatory">*</em></label>
                                                <span class="error"><?php echo form_error('specify_fields_area_that_you_would_like'); ?></span>
                                            </div>

                                            </div>
                                            

                                             <div class="col-md-1">
                                             <div class="form-group">
                                             <?php if ($key==0) {?>
                                                <button class="add_field_button_specify btn btn-success btn-sm">+</button>
                                             <?php }else{ ?>   
                                                <button type="button" class="remove_field_button_specify btn btn-danger btn-sm">-</button>
                                            <?php } ?>
                                            
                                            </div>
                                            </div>

                                       </div> -->

                                   <?php }}else{ ?>
                                      
                                    <!--    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                               
                                                 <textarea name="specify_fields_area_that_you_would_like[]" class="form-control specify_fields_area_that_you_would_like" id="specify_fields_area_that_you_would_like" rows="1" maxlength="400"></textarea>
                                                   <label  class="form-control-placeholder" for="exampleInputEmail1">Please mention your areas of expertise<em class="mandatory">*</em></label>
                                                <span class="error"><?php echo form_error('specify_fields_area_that_you_would_like'); ?></span>
                                            </div>

                                            </div>
                                             <div class="col-md-1">
                                             <div class="form-group">
                                                <button class="add_field_button_specify btn btn-success btn-sm">+</button>
                                                                                     
                                            </div>
                                            </div>

                                       </div> -->
          

                                     <?php } ?>
                                       </div>
                                       <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <?php 
                                                    $bioData = '';
                                                    if(isset($profile_information[0]['bio_data']))
                                                    {
                                                        $bioData = $profile_information[0]['bio_data'];
                                                    }
                                                ?>
                                            
                                              <textarea name="bio_data" class="form-control bio_data" rows="1" id="bio_data"><?php echo $bioData ?></textarea>
                                                <label  class="form-control-placeholder" for="exampleInputEmail1">Profile in Brief<em class="mandatory">*</em></label>
                                              <span class="error"><?php echo form_error('bio_data'); ?></span>
                                            </div>
                                        </div>

                                        

                                    </div>
                                    <div class="row">
                                    
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <?php 
                                                    $numberOfPaperPublication = '';
                                                    if(isset($profile_information[0]['no_of_paper_publication']))
                                                    {
                                                        $numberOfPaperPublication = $profile_information[0]['no_of_paper_publication'];
                                                    }
                                                ?>
                                              
                                              <select name="no_of_paper_publication" id="no_of_paper_publication" class="form-control paperPublication" autocomplete="nope" >
                                                <option value=""></option>

                                                <?php for ($i=0; $i<=50  ; $i++) {  ?>
                                                    <option value="<?php echo $i ?>" <?php if($numberOfPaperPublication == $i ) { ?> selected="selected" <?php } ?> > <?php echo $i ?> </option>
                                                <?php } ?>
                                               
                                            <!--     <option value="0" <?php if($numberOfPaperPublication == "0") { ?> selected="selected" <?php } ?> >0</option>
                                                <option value="1" <?php if($numberOfPaperPublication == "1") { ?> selected="selected" <?php } ?> >1</option>
                                                <option value="2" <?php if($numberOfPaperPublication == "2") { ?> selected="selected" <?php } ?> >2</option>
                                                <option value="3" <?php if($numberOfPaperPublication == "3") { ?> selected="selected" <?php } ?> >3</option>
                                                <option value="4" <?php if($numberOfPaperPublication == "4") { ?> selected="selected" <?php } ?> >4</option>
                                                <option value="5" <?php if($numberOfPaperPublication == "5") { ?> selected="selected" <?php } ?> >5</option>
                                                <option value="6" <?php if($numberOfPaperPublication == "6") { ?> selected="selected" <?php } ?> >6</option>
                                                <option value="7" <?php if($numberOfPaperPublication == "7") { ?> selected="selected" <?php } ?> >7</option>
                                                <option value="8" <?php if($numberOfPaperPublication == "8") { ?> selected="selected" <?php } ?> >8</option>
                                                <option value="9" <?php if($numberOfPaperPublication == "9") { ?> selected="selected" <?php } ?> >9</option>
                                                <option value="10" <?php if($numberOfPaperPublication == "10") { ?> selected="selected" <?php } ?> >10</option> -->
                                              </select>
                                              <label  class="form-control-placeholder flotingCss" for="exampleInputEmail1">No. of Paper Publication<em class="mandatory">*</em></label>
                                              <span><?php echo form_error('no_of_paper_publication'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                   <div id="paperDetails_wrapper">

                                     <?php if (isset($profile_information[0]['paper_year']) && count($profile_information[0]['paper_year'])> 0 ) { 
                                        $paper_conf_name=$profile_information[0]['paper_conf_name'];
                                        $paper_title=$profile_information[0]['paper_title'];
                                       foreach ($profile_information[0]['paper_year'] as $key => $value) {?>
                                        
                                        <div class="row paperDetails">
                                            <div class="col-md-1"><div class="form-group"><label class="form-control-placeholder" for="exampleInputEmail1"><?php echo $key+1 ?></label></div></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control only_year"   name="paper_year[]" value="<?php echo $value; ?>" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Year</label>

                                                </div></div>

                                                    <div class="col-md-4"><div class="form-group">
                                                    <input type="text" class="form-control" value="<?php echo $paper_conf_name[$key]; ?>" name="paper_conf_name[]" value="" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Conference Name</label>
                                                </div></div>

                                                    <div class="col-md-4"><div class="form-group" class="form-control-placeholder">
                                                    <input type="text" class="form-control"   name="paper_title[]" value="<?php echo $paper_title[$key]; ?>" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Title</label>
                                                </div></div></div>


                                       <?php } } ?>
                                  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <?php 
                                                    $numberOfPatents = '';
                                                    if(isset($profile_information[0]['no_of_patents']))
                                                    {
                                                        $numberOfPatents = $profile_information[0]['no_of_patents'];
                                                    }
                                                ?>
                                             
                                              <select name="no_of_patents" id="no_of_patents" class="form-control Patents" autocomplete="nope" >
                                                <option value=""></option>

                                                <?php for ($i=0; $i<=50  ; $i++) {  ?>
                                                    <option value="<?php echo $i ?>" <?php if($numberOfPatents == $i ) { ?> selected="selected" <?php } ?> > <?php echo $i ?> </option>

                                                <?php } ?>


                                               <!--  <option value="0" <?php if($numberOfPatents == "0") { ?> selected="selected" <?php } ?> >0</option>
                                                <option value="1" <?php if($numberOfPatents == "1") { ?> selected="selected" <?php } ?> >1</option>
                                                <option value="2" <?php if($numberOfPatents == "2") { ?> selected="selected" <?php } ?> >2</option>
                                                <option value="3" <?php if($numberOfPatents == "3") { ?> selected="selected" <?php } ?> >3</option>
                                                <option value="4" <?php if($numberOfPatents == "4") { ?> selected="selected" <?php } ?> >4</option>
                                                <option value="5" <?php if($numberOfPatents == "5") { ?> selected="selected" <?php } ?> >5</option>
                                                <option value="6" <?php if($numberOfPatents == "6") { ?> selected="selected" <?php } ?> >6</option>
                                                <option value="7" <?php if($numberOfPatents == "7") { ?> selected="selected" <?php } ?> >7</option>
                                                <option value="8" <?php if($numberOfPatents == "8") { ?> selected="selected" <?php } ?> >8</option>
                                                <option value="9" <?php if($numberOfPatents == "9") { ?> selected="selected" <?php } ?> >9</option>
                                                <option value="10" <?php if($numberOfPatents == "10") { ?> selected="selected" <?php } ?> >10</option> -->
                                              </select>
                                               <label class="form-control-placeholder flotingCss"  for="exampleInputEmail1">No. of Patents<em class="mandatory">*</em></label>
                                              <span><?php echo form_error('no_of_patents'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (isset($profile_information[0]['patent_year']) && count($profile_information[0]['patent_year'])> 0 ) { ?>
                                    <label class="patentDetails">Patent Details</label>
                                <?php } ?>
                                    <div id="patentDetails_div">
                                        
                                         <?php if (isset($profile_information[0]['patent_year']) && count($profile_information[0]['patent_year'])> 0 ) { 
                                        
                                        $patent_number=$profile_information[0]['patent_number'];
                                        $patent_title=$profile_information[0]['patent_title'];
                                        
                                        foreach ($profile_information[0]['patent_year'] as $key => $value) {?>
                                                    
                                            <div class="row patentDetails">
                                                <div class="col-md-1"><div class="form-group"><label for="exampleInputEmail1"> 1 </label></div>
                                                </div>
                                            
                                            <div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"  name="patent_year[]" value="<?php echo $value; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Year</label>
                                            </div></div>

                                            <div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="patent_number[]" value="<?php echo $patent_number[$key]; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Patent Number</label>

                                            </div></div>

                                            <div class="col-md-4"><div class="form-group"><input type="text" class="form-control"  placeholder="Enter Title" name="patent_title[]" value="<?php echo $patent_title[$key]; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Title</label>
                                            </div></div>
                                            </div>        

                                          <?php } } ?>
                                  
                                     </div>
                                    <label>Links to referal Links, Portals that you are operating from. Where else can we find you?</label>


                                     <div id="portal_row">

                                    <?php if (isset($profile_information[0]['portal_name_multiple']) && count($profile_information[0]['portal_name_multiple'])> 0 ) { 
                                        
                                        $portal_link=$profile_information[0]['portal_link_multiple'];
                                        $portal_description=$profile_information[0]['portal_description_multiple'];
                                       
                                       foreach ($profile_information[0]['portal_name_multiple'] as $key => $value) { 

                                        ?>
                                    <div class="row">
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="<?php echo $value; ?>" class="form-control portal_name" id="portal_name"  name="portal_name[]" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Portal Name </label>
                                              <span class="error"><?php echo form_error('portal_name'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                               
                                              <input type="text" class="form-control portal_link" id="portal_link" placeholder="" name="portal_link[]" value="<?php echo $portal_link[$key]  ?>" maxlength="200">
                                              <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link</label>

                                              <span class="error"><?php echo form_error('portal_link'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-5">
                                            <div class="form-group">
                                               
                                                <textarea name="portal_description[]" class="form-control portal_description" id="portal_description0" rows="1"><?php echo $portal_description[$key]; ?></textarea>
                                            <label class="form-control-placeholder" for="exampleInputEmail1">Portal Description</label>

                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                             <?php if ($key==0) {?>
                                                <button class="add_portal_field_button btn btn-success btn-sm">+</button>
                                             <?php }else{ ?>   
                                                <button type="button" class="remove_portal_field btn btn-danger btn-sm">-</button>
                                            <?php } ?>
                                            
                                            </div>
                                            </div>
                                    </div>
                                         
                                     <?php } }
                                     else{
                                      ?>

                                        <div class="row">
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="" class="form-control portal_name" id="portal_name" name="portal_name[]" maxlength="200">
                                              <label  class="form-control-placeholder" for="exampleInputEmail1">Portal Name</label>

                                              <span class="error"><?php echo form_error('portal_name'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-4">                 
                                            <div class="form-group">
                                               
                                          
                                              <input type="text" class="form-control portal_link" id="portal_link" placeholder="" name="portal_link[]" maxlength="200">
                                                <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link</label>
                                              <span class="error"><?php echo form_error('portal_link'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-4">
                                            <div class="form-group">
                                               
                                               
                                                <textarea name="portal_description[]" class="form-control portal_description" id="portal_description" rows="1"></textarea>
                                                 <label class="form-control-placeholder" for="exampleInputEmail1">Portal Description</label>
                                             
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                                <button class="add_portal_field_button btn btn-success btn-sm">+</button>
                                            </div>
                                            </div>
                                    </div>


                                     <?php } ?> 
                                    </div>

                               </div>  
                           <?php } ?>
                            </div>
                        </div>

                        
                        <!-- /.card-body -->
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End #main -->


<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">

    function show_hide_other_org_sector() 
    {
        var selected = $("#org_sector").find('option:selected', this);
        var results = [];
        
        selected.each(function() { results.push($(this).text().toLowerCase()); });
        var display_flag=0;

        $.each(results,function(i)
        {
            if(results[i]=='other')
            {
                display_flag = 1;
            }
        });
        if (display_flag==1) 
        {
            $('#org_sector_other_div').removeClass('d-none');
        }
        else
        {
            $('#org_sector_other').val('');
            $('#org_sector_other_div').addClass('d-none');
        }
    }
    
    show_hide_other_org_sector();


    var paperPublicationValue = $('.paperPublication').val();
    if(paperPublicationValue == 0)
    {
        $('.paperDetails').hide();
    }
    if(paperPublicationValue != 0)
    {
        $('.paperDetails').show();
    }
    $(document).on('change','.paperPublication', function(){
        var paperPublicationValue = $('.paperPublication').val();
        if(paperPublicationValue == 0)
        {
            $('.paperDetails').hide();
        }
        if(paperPublicationValue != 0)
        {
            $('.paperDetails').show();
        }
    });

    var patentValue = $('.Patents').val();
    if(patentValue == 0)
    {
        $('.patentDetails').hide();
    }
    if(patentValue != 0)
    {
        $('.patentDetails').show();
    }
    $(document).on('change','.Patents', function(){
        var patentValue = $('.Patents').val();
        if(patentValue == 0)
        {
            $('.patentDetails').hide();
        }
        if(patentValue != 0)
        {
            $('.patentDetails').show();
        }
    });

    $('.domainName').select2({
        // placeholder : "Please Select Domain / Area of Expertise",
        closeOnSelect : true,
        width: '100%',
    });

     $('.select2_common').select2({
        // placeholder : "Please Select Domain / Area of Expertise",
        closeOnSelect : true,
        width: '100%',
    });

    

    $('#years_of_experience').select2({
        // placeholder : "Please Select Domain / Area of Expertise",
        closeOnSelect : true,
        width: '100%',
    });

     $('#employement_status').select2({
        // placeholder : "Please Select Domain / Area of Expertise",
        closeOnSelect : true,
        width: '100%',
    });

    
    
    $('.areaOfInterestName').select2({
        // placeholder : "Please Select Areas of Interest",
        closeOnSelect : true,
        width: '100%',
    });
    $('.skillSets').select2({
        // placeholder : "Please Select Skill Sets",
        closeOnSelect : true,
        width: '100%',
    });

    // Smart Wizard
$(document).ready(function() {

    // Step show event
    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        
        if (stepPosition === 'first') {
            $("#prev-btn").addClass('disabled');
            $(".sw-btn-prev").css('display','none');
            $(".btnfinish").addClass('d-none');
            $(".sw-btn-next").css('display','block');
        } else if (stepPosition === 'final') {
            $("#next-btn").addClass('disabled');
            $(".sw-btn-next").css('display','none');
            $(".sw-btn-prev").css('display','block');
            $(".btnfinish").removeClass('d-none');
        } else {
            $(".btnfinish").addClass('d-none');
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
            $(".sw-btn-prev").css('display','block');
            $(".sw-btn-next").css('display','block');
        }
    });

    $('#smartwizard').smartWizard({
        theme: 'arrows',
        transitionEffect: 'fade',
        showStepURLhash: false,
				keyNavigation: false,
        toolbarSettings: {
            toolbarExtraButtons: [
                $('<button></button>').text('Finish')
                            .addClass('btn btn-primary btnfinish')
                            .on('click', function(e){ 
                             e.preventDefault();
                            
                            var isValid=true;

                            var cat = '<?php echo $this->session->userdata('user_sub_category'); ?>';

                               // if ($("#area_colony_street_village").valid()==false) {
                                  
                               //    isValid= false;
                               //  } 
                              if ($("#town_city_and_state").valid()==false) {
                                  
                                  isValid= false;
                                } 
                               
                              if ($("#country").valid()==false) {
                                  
                                  isValid= false;
                                } 
                               


                            if (cat==11) {
                            // if ($(".specify_fields_area_that_you_would_like").valid()==false) {
                                
                            //   isValid= false;
                            // } 

                            if ($(".bio_data").valid()==false) {
                              
                              isValid= false;
                            } 
                           

                             if ($("#no_of_paper_publication").valid()==false) {
                              isValid= false;
                            } 

                              if ($("#no_of_patents").valid()==false) {
                              isValid= false;
                            } 
                            }

                            if (isValid==true) {

                                swal({
                                title:'Confirm' ,
                                text: 'Are you sure you want to update your details?',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes!'
                                }).then(function (result) {
                                if (result.value) {
                                  $('#individualProfileForm').submit();

                              }

                            });       
                        }

                            }),
                // $('<button></button>').text('Cancel')
                //             .addClass('btn btn-danger')
                //             .on('click', function(){ 
                //                 alert('Cancel button click');                            
                //             })
            ]
        },

    });

     $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        var isValidate = true;
        
        if(stepNumber==0 && stepDirection=="forward"){
          
            $(".designation").each(function(){
                $(this).rules("add", {
                    required: true,
                    // letter_with_space: true,
                    messages: {
                        required: "Please Enter Designation"
                    }
                } );            
            });

             // $(".designation_since_year").each(function(){
             //        $(this).rules("add", {
             //            required: true,
             //            messages: {
             //                required: "Please Select Year"
             //            }
             //        } );            
             //    });

               // $(".designation_description").each(function(){
               //      $(this).rules("add", {
               //          required: true,
               //          letter_with_space: true,
               //          messages: {
               //              required: "Please Enter Designation Description",
                           

               //          }
               //      } );            
               //  });

               // $(".technical_experience_in_year").each(function(){
               //      $(this).rules("add", {
               //          number: true,
                      
               //      } );            
               //  });

               

            if ($("#employement_status").valid()==false) {
                isValidate= false;
            }

             $(".designation").each(function(){
                // alert()
                if($(this).valid() == false) {  isValidate= false;}
            });
            //   $(".technical_experience_in_year").each(function(){
            //     // alert()
            //     if($(this).valid() == false) {  isValidate= false;}
            // });

            //   $(".designation_description").each(function(){
            //     // alert()
            //     if($(this).valid() == false) {  isValidate= false;}
            // });
             

            // if ($(".designation").valid()==false) {
            //         isValidate= false;
            // }
            // if ($(".designation_since_year").valid()==false) {
            //         isValidate= false;
            // }
            
            // if ($(".designation_description").valid()==false) {
            //         isValidate= false;
            // }
            
            if ($("#other_emp_status").valid()==false) {
                    isValidate= false;
            }

            if ($("#other_domian").valid()==false) {
                    isValidate= false;
            }
            if ($("#other_skill_sets").valid()==false) {
                    isValidate= false;
            }


            
            if ($("#domain_area_of_expertise").valid()==false) {
                    isValidate= false;
            }
            // if ($("#areas_of_interest").valid()==false) {
            //         isValidate= false;
            // }
             if ($("#skill_sets").valid()==false) {
                    isValidate= false;
            }

            if ($("#profile_picture").valid()==false) {
                    isValidate= false;
            }

            if ($("#years_of_experience").valid()==false) {
                          isValidate= false;
            } 


            

            if (  $("#company_name").val() !='' && $("#company_name").valid()==false) {
                isValidate= false;
            }

            if($("#org_sector_other").valid()==false) { isValidate= false; $("#org_sector_other").focus(); }
            if($("#org_sector").valid()==false) { isValidate= false; $("#org_sector").focus(); }

            if($("#promo_code").valid()==false) { isValidate= false; $("#promo_code").focus(); }


            
        

            return isValidate;


        }  
        

        if(stepNumber==1 && stepDirection=="forward"){
            isValidate =true;
              
               if ($("#town_city_and_state").valid()==false) {
                  isValidate= false;
                } 
               
              if ($("#country").valid()==false) {
                  isValidate= false;
                } 
             return isValidate; 

        }

          
    }) 

});

  // $.validator.addMethod('filesize', function (value, element, arg) {
  //           var minsize=1000; // min 1kb
  //           if(value<=arg){
  //               return true;
  //           }else{
  //               return false;
  //           }
  //       });

    $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

   $.validator.addMethod('letter_with_space', function(value) {
    return value.match(/^[a-zA-Z\s]+$/);
    }, 'Letters only please');
   

   $.validator.addMethod('is_all_number', function(value) {
        if (isNaN( value )) {
        return true;
        } else {
         return false;
        }
    
    }, 'This field should not contain all numbers');

   $.validator.addMethod('not_All_digits', function(value) {
            if(value.match(/^\d+$/)) {
                return false;
            }else{
                return true;
            }   
        }, 'Please enter valid details');


    function isPromoCodePresent() {
     return $('#promo_code').val().length > 0;
    }

  

    $('#individualProfileForm').validate({

         onkeyup: function(element, event) {
            if ($(element).attr('name') == "promo_code") {
                return false; // disable onkeyup for your element named as "name"
            } else { // else use the default on everything else
                if ( event.which === 9 && this.elementValue( element ) === "" ) {
                    return;
                } else if ( element.name in this.submitted || element === this.lastElement ) {
                    this.element( element );
                }
            }
         },

        rules: {
            employement_status: {
                required : true,
            },
            company_name: {
                is_all_number : true,
            },
            other_emp_status:{
                required: function() {
                    return $("#employement_status").val() == "Other";  
                  },
                normalizer: function(value) {
                return $.trim($("#other_emp_status").val());
                }
              },

          'org_sector[]': { required: true },
          org_sector_other: { required: true },

            other_domian:{
                required: function() {
                        var selected = $('#other_domian').find('option:selected', $('#other_domian') );
                        var results = [];

                        selected.each(function() {
                            results.push($(this).data('id'));
                        });

                         $.each(results,function(i){
                           if(results[i]=='Other')
                           {
                 
                            return true;
                           }else{
                            return false;
                           }
                          
                        });  
                  },
                  normalizer: function(value) {
                        return $.trim($("#other_domian").val());
                }

              },

               other_skill_sets:{
                required: function() {
                        var selected = $('#other_skill_sets').find('option:selected', $('#other_skill_sets') );
                        var results = [];

                        selected.each(function() {
                            results.push($(this).data('id'));
                        });

                         $.each(results,function(i){
                           if(results[i]=='Other')
                           {
                 
                            return true;
                           }else{
                            return false;
                           }
                          
                        });  
                  },
                  normalizer: function(value) {
                        return $.trim($("#other_skill_sets").val());
                }
              },

              

              
            
            // "designation_description[]":{

            //     required:true,
            //      letter_with_space: true,

            // },
            // designation: {
            //     lettersonly: true
            // },
            // designation_since_year: {
            //     required : true,
            // },
            // designation_description: {
            //     required : true,
            // },
            // corporate_linkage_code: {
            //     required : true,
            // },

             promo_code: {
               // depends: function()
               //          {
               //              var promo_code_val = $.trim($("#promo_code").val());
               //              if(promo_code_val == '') { return false; } else { return true; }
               //          },
               remote:{
                  
               
                  
                  url: "<?php echo base_url();?>profile/verify_linkage_code/",
                  type: "post",
                
                   beforeSend: function(){
                     $('.ajax-loader').css("visibility", "visible");
                   },
                 
                  data: {
                     promo_code: function() {
                     return $( "#promo_code" ).val();
                     },
                     
                  },
                  dataFilter: function(res_data){
                  
                     $("#promo_success").html("");
                    $("#promo_error").html("");
                    $("#promo_code_type").val("");
                    $("#org_id").val("");
                    $("#promo_code_id").val("");

                   var json_decode = $.parseJSON(res_data);
                 
                   if (json_decode.success==true) {
                     
                     $("#promo_code").css('border-color','green');
                     $("#promo_success").html(json_decode.msg);
                     $("#promo_code_type").val(json_decode.type);
                     $("#org_id").val(json_decode.org_id);
                     $("#promo_code_id").val(json_decode.promo_code_id);

                      return JSON.stringify(json_decode.success)

                  
                   }else{
                    // $('#btn_submit').prop('disabled', true);
                    $("#promo_code").css('border-color','red');  
                    $("#promo_error").html(json_decode.msg);
                    return JSON.stringify(json_decode.success)
                    // return false;

                   }
                 
                 },
                     complete: function(){
                       $('.ajax-loader').css("visibility", "hidden");
                    },
                 },

               },



            technical_experience_in_year: {
                required : true,
            },
            technical_description_of_work: {
                required : true,
            },
            pincode: {
                number : true,
                minlength : 6,
                maxlength : 6,
            },
            // flat_house_building_apt_company: {
            //     required : true,
            // },
            // area_colony_street_village: {
            //     not_All_digits:true 
            // },
            town_city_and_state: {
                // required : true,
                not_All_digits:true 
            },
            country: {
                // required : true,
                not_All_digits:true 
            },
            // "specify_fields_area_that_you_would_like[]": {
            //     required : true,
            // },
            bio_data: {
                required : true,
            },
            profile_picture: {
                 extension: "JPEG|JPG|PNG|jpeg|jpg|png",
                 filesize:500000
            },
            years_of_experience: {
                required : true,
            },
            no_of_paper_publication: {
                required : true,
            },
            paper_year: {
                required : true,
            },
            paper_conf_name: {
                required : true,
            },
            paper_title: {
                required : true,
            },
            no_of_patents: {
                required : true,
            },
            patent_year: {
                required : true,
            },
            patent_number: {
                required : true,
            },
            patent_title: {
                required : true,
            },
            // portal_name: {
            //     required : true,
            // },
            // portal_link: {
            //     required : true,
            //     url : true,
            // },
            // portal_description: {
            //     required : true,
            // },
        },
        messages: {
            employement_status: {
                required: "Please Select Employement Status",
            },
            company_name: {
                required: "Please Enter Company Name",
            },
            // designation: {
            //     required: "Please Enter Designation",
            // },
            // designation_since_year: {
            //     required: "Please Select Year",
            // },
            // designation_description: {
            //     required: "Please Enter Description",
            // },
            corporate_linkage_code: {
                required: "Please Enter Corporate Linkage Code",
            },
            promo_code:{
              remote:""
             },
            technical_experience_in_year: {
                required : "Please Enter Technical Expertise in Year",
            },
            technical_description_of_work: {
                required : "Please Enter Technical Description of Work",
            },
            // pincode: {
            //     required : "Please Enter Pincode",
            // },
            // flat_house_building_apt_company: {
            //     required : "Please Enter Flat / House / Building / Apartment / Company",
            // },
            // area_colony_street_village: {
            //     required : "Please Enter Area / Colony / Street / Village",
            // },
            // town_city_and_state: {
            //     required : "Please Enter Town / City and State",
            // },
            // country: {
            //     required : "Please Enter Country",
            // },
            "specify_fields_area_that_you_would_like[]": {
                required : "Please Enter This Field",
            },
            bio_data: {
                required : "Please Enter Bio Data",
            },
            profile_picture: {
                extension : "Please upload .jpg or .png or .jpeg file",
                filesize : "File size must be less than 500 KB"
            },
            years_of_experience: {
                required : "Please Select Years of Expertise",
            },
            no_of_paper_publication: {
                required : "Please Select Number of Paper Publication",
            },
            paper_year: {
                required : "Please Enter Paper Year",
            },
            paper_conf_name: {
                required : "Please Enter Paper Conf. Name",
            },
            paper_title: {
                required : "Please Enter Paper Title",
            },
            no_of_patents: {
                required : "Please Select No of Patents",
            },
            patent_year: {
                required : "Please Enter Patent Year",
            },
            patent_number: {
                required : "Please Enter Patent Number",
            },
            patent_title: {
                required : "Please Enter Patent Title",
            },
            // portal_name: {
            //     required : "Please Enter Portal Name",
            // },
            // portal_link: {
            //     required : "Please Enter Portal Link",
            // },
            // portal_description: {
            //     required : "Please Enter Portal Description",
            // },
        },
       errorElement: 'span',
       errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
       },
       highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
       },
       unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
       }
     });
</script>

 <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $("#designation_row"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-5"><div class="form-group"><input type="text"  class="form-control designation" id="designation" name="designation[]" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Designation</label></div></div><div class="col-md-3 d-none"><div class="form-group"><input type="date" class="form-control designation_since_year" id="designation_since_year" placeholder name="designation_since_year[]"  maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">From</label></div>  </div><div class="col-md-6"><div class="form-group"><textarea name="designation_description[]" class="form-control designation_description" id="designation_description'+x+'" rows="1"></textarea><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Designation Description</label><span class="error"></span></div></div><div class="col-md-1"><button class="remove_field btn btn-danger btn-sm">-</button></div></div>'); //add input box

                }

            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>


       <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $("#portal_row"); //Fields wrapper
            var add_button      = $(".add_portal_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-3"><div class="form-group"><input type="text"  class="form-control portal_name" id="portal_name" name="portal_name[]" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Name</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control portal_link" id="portal_link" placeholder name="portal_link[]"  maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link</label></div>  </div><div class="col-md-4"><div class="form-group"><textarea name="portal_description[]" class="form-control portal_description" id="portal_description'+x+'" rows="1"></textarea><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Description</label><span class="error"></span></div></div><div class="col-md-1"><button type="button" class="remove_portal_field btn btn-danger btn-sm">-</button></div></div>'); //add input box

                }

            });

            $(wrapper).on("click",".remove_portal_field", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>



    <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper_exp         = $("#experience_row"); //Fields wrapper
            var add_button_exp      = $(".add_field_button_experience"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button_exp).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper_exp).append('<div class="row"><div class="col-md-5"><div class="form-group"><input type="text" class="form-control technical_experience_in_year" id="technical_experience_in_year" placeholder="" name="technical_experience_in_year[]" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Years of Experience</label></div> </div><div class="col-md-6"><div class="form-group"><textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" rows="1" id="technical_description_of_work"></textarea ><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Description of Work</label></div> </div><div class="col-md-1"><div class="form-group"><button class="remove_field_button_experience btn btn-danger btn-sm">-</button></div></div></div>'); //add input box

                }

            });

            $(wrapper_exp).on("click",".remove_field_button_experience", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>

       <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper_exp         = $("#event_row"); //Fields wrapper
            var add_button_exp      = $(".add_field_button_event"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button_exp).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper_exp).append('<div class="row"><div class="col-md-5"><div class="form-group"><input type="text" class="form-control technical_event_in_year" id="technical_event_in_year"  name="technical_event_in_year[]" maxlength="200"><label class="form-control-placeholder" for="exampleInputEmail1">Event Experience In Year</label></div> </div><div class="col-md-6"><div class="form-group"><textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" rows="1" id="technical_description_of_work"></textarea ><label class="form-control-placeholder" for="exampleInputEmail1">Description of Work</label></div> </div><div class="col-md-1"><div class="form-group"><button class="remove_field_button_event btn btn-danger btn-sm">-</button></div></div></div>'); //add input box

                }

            });

            $(wrapper_exp).on("click",".remove_field_button_event", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>

      <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $("#wrapper_div"); //Fields wrapper
            var add_button      = $(".add_field_button_specify"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-11"><div class="form-group"><textarea name="specify_fields_area_that_you_would_like[]" class="form-control specify_fields_area_that_you_would_like"  rows="1"></textarea></div></div><div class="col-md-1"><div class="form-group"><button class="add_field_button_specify btn btn-danger btn-sm">-</button></div></div></div>'); //add input box

                }

            });

            $(wrapper).on("click",".remove_field_button_specify", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })


            $("#no_of_paper_publication").change(function(event) {
                    $('.paperDetails').remove();
                    var no_of_paper_publication = $(this).val();
             
                      setTimeout(function(){ paperDetails_append(no_of_paper_publication);  }, 500);

                      setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                format: "yyyy",
                                viewMode: "years",
                                minViewMode: "years"
                               
                            }
                            ); 

                    }, 500);
            });



             $('body').on('change', '#no_of_patents', function() {

                    $('.patentDetails').remove();
                     // $('.patentDetails').remove(); 
                    var no_of_patents = $(this).val();

                    setTimeout(function(){ patentDetails_append(no_of_patents);  }, 500);

                    setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                format: "yyyy",
                                viewMode: "years",
                                minViewMode: "years"
                               
                            }
                            ); 

                    }, 500);

            });


            });



function paperDetails_append(argument) {
                  if (argument > 10) {
                      argument = 10;
                   } 
                    var wrapper         = $("#paperDetails_wrapper"); //Fields wrapper
                    var x = 1; //initlal text box count
                    var template='';
                    for (var i = 1; i <= argument; i++) {
                            template +='<div class="row paperDetails"><div class="col-md-1"><div class="form-group"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">'+ i +'</label></div></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"   name="paper_year[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Year</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="paper_conf_name[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Conference Name</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="paper_title[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Title</label></div></div></div>'; //add input box

                    }
                         // alert(template)

                    $(wrapper).append(template)
            }       

    function patentDetails_append(argument) {
                   if (argument > 10) {
                      argument = 10;
                   } 
                   var wrapper         = $("#patentDetails_div"); //Fields wrapper
                    var x = 1; //initlal text box count
                    var template_two='';
                    for (var i = 1; i <= argument; i++) {
                            template_two +='<div class="row patentDetails"><div class="col-md-1"><div class="form-group"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">'+(i)+'</label></div></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"   name="patent_year[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Year</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="patent_number[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Patent Number</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"  name="patent_title[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Title</label></div></div></div>'; //add input box

                    }
                         // alert(template)

                    $(wrapper).append(template_two)
    }


      </script>

      <script>
        $(document).ready(function() {
          $('#employement_status').change(function(){
            if ($(this).val()=='Other') {
              $('#other_emp_status_div').removeClass('d-none');
            }else{
              $("#other_emp_status").val('');  
              $('#other_emp_status_div').addClass('d-none');
            }
          });

            $('#domain_area_of_expertise').on('change', function() {
                var selected = $(this).find('option:selected', this);
                var results = [];

                selected.each(function() {
                    results.push($(this).data('id'));
                });

                //  $.each(results,function(i){
                //    if(results[i]=='Other')
                //    {
                //      $('#other_domian_div').removeClass('d-none');
                //    }else{
                //     $('#other_domian').val('');
                //      $('#other_domian_div').addClass('d-none');
                //    }
                  
                // });


                var display_flag=0;
                 $.each(results,function(i){
                   if(results[i]=='Other')
                   {
                      display_flag = 1;
                     
                   }
                  
                });

                 if (display_flag==1) {
                  $('#other_domian_div').removeClass('d-none');
                 }else{
                   $('#other_domian').val('');
                   $('#other_domian_div').addClass('d-none');
                 }

            });



            $('#skill_sets').on('change', function() {
                var selected = $(this).find('option:selected', this);
                var results = [];

                selected.each(function() {
                    results.push($(this).data('id'));
                });

                //  $.each(results,function(i){
                //    if(results[i]=='Other')
                //    {
                //      $('#other_skill_sets_div').removeClass('d-none');
                //    }else{
                //     $('#other_skill_sets').val('');
                //      $('#other_skill_sets_div').addClass('d-none');
                //    }
                  
                // });

                var display_flag=0;
                 $.each(results,function(i){
                   if(results[i]=='Other')
                   {
                      display_flag = 1;
                     
                   }
                  
                });

                 if (display_flag==1) {
                  $('#other_skill_sets_div').removeClass('d-none');
                 }else{
                   $('#other_skill_sets').val('');
                   $('#other_skill_sets_div').addClass('d-none');
                 }




            });

            

          
        //     $("#domain_area_of_expertise").on("change", function () {

        //     var flag=0;
        //     var responseId = $(this).attr('data-id');

            
        //   const originalString = responseId.toString();
        //     const splitString = originalString.split(",");

        // $.each(splitString,function(i){
        //    if(splitString[i]=='other')
        //    {
        //     alert()
        //      flag=1;
        //    }
          
        // });

        // if(flag==1)
        //    {
           
        //     $('#other_emp_status_div').removeClass('d-none');
        //    }
        //    else
        //    {
        //   $('#other_emp_status_div').addClass('d-none');
        //    }
          
    
         
        //   });

          

          
          $('.only_year').datepicker(
            {
                /* todayBtn: "linked",  */
                keyboardNavigation: true,
                forceParse: true,
                calendarWeeks: false,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
               
            }
            );
          });

    // $(document).ready(function()
    // {          
    //     $('input[type="file"]').change(function(e)
    //     {              
    //         var fileName = e.target.files[0].name;
    //         $(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
    //     });          
    // });


     function delete_single_file_expert(tbl_nm, pk_nm, del_id, input_nm, file_path, swal_text)
    { 
        swal(
        {
            title:"Confirm?" ,
            text: "Are you confirm to delete the "+swal_text+"?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        {
            if (result.value) 
            {
                var csrf_test_name = $("#csrf_token").val();
                var data = { 
                        'tbl_nm': encodeURIComponent($.trim(tbl_nm)), 
                        'pk_nm': encodeURIComponent($.trim(pk_nm)), 
                        'del_id': encodeURIComponent($.trim(del_id)), 
                        'input_nm': encodeURIComponent($.trim(input_nm)), 
                        'file_path': encodeURIComponent($.trim(file_path)), 
                        'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
                        };          
                $.ajax(
                { 
                    type: "POST", 
                    url: '<?php echo site_url("delete_single_file") ?>', 
                    data: data, 
                    dataType: 'JSON',
                    success:function(data) 
                    { 
                        $("#csrf_token").val(data.csrf_new_token);
                        $("#"+input_nm+"_outer").remove();

                        $("#profile_header_iamge").html('<img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">');

                        
                        swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" });
                    }
                });
            }
        });
    }


   
    

      </script>

<!-- 
<script>
      for(var x=0; x<5; x++){   
            $('#patentDetails').append('<div class="row"><div class="col-md-3"><label for="exampleInputEmail1">Year</label><input type="date" class="form-control" id="patent_year" placeholder="Enter Year" name="patent_year" value="" maxlength="200"></div></div>');  
      } 
</script> -->

