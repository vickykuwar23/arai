<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Update Personal Info</h1> 
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url('profile'); ?>">Manage Profile</a></li>
				<li class="breadcrumb-item active" aria-current="page">Update Personal Info </li>
			</ol>
		</nav>
	</div>
</div>

<section id="registration-form" class="inner-page" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="<?php echo site_url('personalInfoInd'); ?>" id="ind_personal_info_form" enctype="multipart/form-data">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					<div class="formInfo"> 
						<div class="row">										
							<div class="col-md-3">
								<div class="form-group">
									<select class="form-control" id="title" name="title" required="">
											<option value=""></option>
											<option value="Mr." <?php if($form_data['title'] == 'Mr.') { echo "selected"; } ?>>Mr.</option>
											<option value="Mrs." <?php if($form_data['title'] == 'Mrs.') { echo "selected"; } ?>>Mrs.</option>
											<option value="Miss." <?php if($form_data['title'] == 'Miss.') { echo "selected"; } ?>>Miss.</option>
											 <option value="Dr."  <?php if($form_data['title'] == 'Dr.') { echo "selected"; } ?>>Dr.</option>
                                  			<option value="Prof." <?php if($form_data['title'] == 'Prof.') { echo "selected"; } ?>>Prof.</option>
									 </select>
									<label class="form-control-placeholder flotingCss" for="title">Title <em>*</em></label>
									<?php if(form_error('title')!=""){ ?><span class="error"><?php echo form_error('title'); ?></span> <?php } ?>
								</div>
							</div>										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $form_data['first_name']; ?>" required />
									<label class="form-control-placeholder" for="first_name">First Name <em>*</em></label>
									<?php if(form_error('first_name')!=""){ ?><span class="error"><?php echo form_error('first_name'); ?></span> <?php } ?>
								</div>
							</div>										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="middle_name" id="middle_name" value="<?php echo $form_data['middle_name']; ?>" />
									<label class="form-control-placeholder" for="middle_name">Middle Name <em></em></label>
									<?php if(form_error('middle_name')!=""){ ?><span class="error"><?php echo form_error('middle_name'); ?></span> <?php } ?>
								</div>
							</div>										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $form_data['last_name']; ?>" required />
									<label class="form-control-placeholder" for="last_name">Last Name <em>*</em></label>
									<?php if(form_error('last_name')!=""){ ?><span class="error"><?php echo form_error('last_name'); ?></span> <?php } ?>
								</div>
							</div>
						
							<div class="col-md-5">
								<div class="form-group">
									<input type="text" class="form-control" name="email" id="email" value="<?php echo $form_data['email']; ?>" required onkeyup="email_verification_code_btn()" onkeypress="email_verification_code_btn()" onfocus="email_verification_code_btn()" onblur="email_verification_code_btn()"  />
									<label class="form-control-placeholder" for="email">Email address <em>*</em></label>
									<div id="email_error"></div>
									<?php if(form_error('email')!=""){ ?><span class="error"><?php echo form_error('email'); ?></span> <?php } ?>
								</div>
							</div>
                    
							<div class="col-md-3">
								<button class="btn btn-primary mt-3 send_otp_button btn-sm" type="button" id="send_email_code" onclick="send_codes_email()" disabled>Send Verification Code</button>
								<div class="d-none" id="email_timer_div">Resend In <span id="timer"></span></div>
							</div>
										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="email_code" id="email_code" value="" maxlength="6" readonly>
									<label class="form-control-placeholder" for="email_code">Enter Verification Code </label>
									<?php if(form_error('email_code')!=""){ ?><span class="error"><?php echo form_error('email_code'); ?></span> <?php } ?>
									</div>
								</div>

							<div class="col-md-1">
								<div class="form-group">
									<div class="input-group-addon d-none" id="code_check_success_email">
										<i class="fa fa-check" style="color:green;" aria-hidden="true"></i>
								</div>
								</div>	
							</div>							
						
							<div class="col-md-2">
								<div class="form-group">
									<select class="form-control select2_common" name="country_code" id="country_code" required>
										<!-- <option value="">Country Code</option> -->
										<?php if(!empty($country_codes))
											{
												foreach($country_codes as $code)
												{	?>
												<option value="<?php echo $code['id'] ?>" <?php if($form_data['country_code'] == $code['id']) { echo "selected"; } ?>><?php echo $code['iso']." ".$code['phonecode'] ?></option>
												<?php }
											}	?>
									</select>
									<?php /* <label class="form-control-placeholder" for="country_code">Select Country <em>*</em></label> */ ?>
									    <label class="form-control-placeholder leftLable2 flotingCss" for="cname">Country
                                            <em>*</em></label>
									<div id="country_code_error"></div>
									<?php if(form_error('country_code')!=""){ ?><span class="error"><?php echo form_error('country_code'); ?></span> <?php } ?>
								</div>
							</div>
										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="mobile" id="mobile" value="<?php echo $form_data['mobile']; ?>"  maxlength="15" required onkeyup="mobile_verification_otp_btn()" onkeypress="mobile_verification_otp_btn()" onfocus="mobile_verification_otp_btn()" onblur="mobile_verification_otp_btn()" />
									<label class="form-control-placeholder" for="mobile">Phone Number <em>*</em></label>
									<div id="mobile_error"></div>
									<?php if(form_error('mobile')!=""){ ?><span class="error"><?php echo form_error('mobile'); ?></span> <?php } ?>
								</div>
							</div>


							<div class="col-md-3">
								<button class="btn btn-primary mt-3 send_otp_button btn-sm" type="button" id="send_mobile_code" onclick="send_codes_mobile()" disabled>Send OTP Code</button>
								<div class="d-none" id="mobile_timer_div">Resend In <span id="timer_mobile_span"></span></div>
							</div>
										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="mobile_code" id="mobile_code" value="" maxlength="6" readonly>
									<label class="form-control-placeholder" for="mobile_code">Enter OTP Code </label>
									<?php if(form_error('mobile_code')!=""){ ?><span class="error"><?php echo form_error('mobile_code'); ?></span> <?php } ?>
									</div>
								</div>

							<div class="col-md-1">
								<div class="form-group">
									<div class="input-group-addon d-none" id="code_check_success_mobile">
										<i class="fa fa-check" style="color:green;" aria-hidden="true"></i>
								</div>
								</div>	
							</div>	
									
						<!-- 	<div class="col-md-3">
								<button class="btn btn-primary mt-3 send_otp_button btn-sm" type="button" id="send_otp" onclick="send_mobile_verification_otp()" disabled>Send OTP</button>
							</div>
										
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="form-control" name="otp" id="otp" value="" maxlength="6" readonly>
									<label class="form-control-placeholder" for="otp">Enter OTP </label>
									<?php if(form_error('otp')!=""){ ?><span class="error"><?php echo form_error('otp'); ?></span> <?php } ?>
								</div>
							</div>	 -->
							
							<div class="col-md-6">
								<div class="form-group">
									<select class="form-control" id="gender" name="gender" required="">
											<option value=""></option>
											<option value="Male" <?php if($form_data['gender'] == 'Male') { echo "selected"; } ?>>Male</option>
											<option value="Female" <?php if($form_data['gender'] == 'Female') { echo "selected"; } ?>>Female</option>
											<option value="Other" <?php if($form_data['gender'] == 'Other') { echo "selected"; } ?>>Other</option>
									 </select>
									<label class="form-control-placeholder flotingCss" for="gender">Gender <em>*</em></label>
									<?php if(form_error('gender')!=""){ ?><span class="error"><?php echo form_error('gender'); ?></span> <?php } ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
						<a href="<?php echo base_url('profile') ?>" class="btn btn-primary mt-3">Back</a>
						<button class="btn btn-primary mt-3 float-right" type="submit" id="submit_btn">Update</button>
						</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>					

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<?php $this->load->view('front/organization_profile/common_validation_all'); ?>
<script type="text/javascript">
	$('.select2_common').select2();
	
	/******* SWEET ALERT POP UP *********/
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
		
<script type="text/javascript">
	
	var timerOn_mobile = true;

	function timer_mobile(remaining_mobile) 
	{


		var m = Math.floor(remaining_mobile / 60);
		var s = remaining_mobile % 60;
		
		m = m < 10 ? '0' + m : m;
		s = s < 10 ? '0' + s : s;
		document.getElementById('timer_mobile_span').innerHTML = m + ':' + s;
		remaining_mobile -= 1;
		
		if(remaining_mobile >= 0 && timerOn_mobile) 
		{
			setTimeout(function() { timer_mobile(remaining_mobile); }, 1000);

			return;
		}		

		if(!timerOn_mobile) 
		{
			// Do validate stuff here
			return;
		}
		
		// Do timeout stuff here
		$('#mobile_timer_div').addClass('d-none');
		// alert('Timeout for otp');
	}

	var timerOn = true;
	function timer(remaining) 
	{
		var m = Math.floor(remaining / 60);
		var s = remaining % 60;
		
		m = m < 10 ? '0' + m : m;
		s = s < 10 ? '0' + s : s;
		document.getElementById('timer').innerHTML = m + ':' + s;
		remaining -= 1;
		
		if(remaining >= 0 && timerOn) 
		{
			setTimeout(function() { timer(remaining); }, 1000);
			return;
		}		

		if(!timerOn) 
		{
			// Do validate stuff here
			return;
		}
		
		// Do timeout stuff here
		$('#email_timer_div').addClass('d-none');
		// alert('Timeout for otp');
	}



	

	function email_verification_code_btn()
	{
		var current_email = "<?php echo $form_data['email']; ?>";
		var new_email = $.trim($("#email").val());
		if(current_email != new_email)
		{
			$('#send_email_code').prop('disabled', false);
			$('#email_code').prop('readonly', false);
		}
		else
		{
			$('#send_email_code').prop('disabled', true);
			$('#email_code').prop('readonly',true);
			$("#email_code").val("");
		}		
	}
	
	function send_email_verification_code()
	{
		var email = $.trim($("#email").val());
		var email_code = $.trim($("#email_code").val());
		
		if(email == '') 
		{ 
			$("#email_error").html('<span id="email_error_span" class="error">Please enter the email</span>');
		}
		else { $("#email_error").html(''); }
		
		if ($("#email").valid()==false) 
		{
			return false;
		}
		else
		{
			$("#preloader").css("display", "block");
			var csrf_test_name = $("#csrf_token").val();
			if(email != '')
			{
				parameters= { 'email':encodeURIComponent($.trim(email)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('personalInfoInd/send_email_verification_code'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						$("#csrf_token").val(data.csrf_new_token);
						if(data.flag == "success")
						{ 
							$("#send_email_code").text('Verification Code Sent'); 
							$('#send_email_code').prop('disabled', true);
							
							$('#email_timer_div').removeClass('d-none');
							timer(60);							
							setTimeout(function() { $('#send_email_code').text('Resend Verification Code'); $('#send_email_code').prop('disabled', false); }, 60*1000);
						}
						else {	}
						$("#preloader").css("display", "none");
					}
				});
			}
		}
	}

	function send_codes_email()
	{
		var email = $.trim($("#email").val());
		var email_code = $.trim($("#email_code").val());
		
		if(email == '') 
		{ 
			$("#email_error").html('<span id="email_error_span" class="error">Please enter the email</span>');
		}
		else { $("#email_error").html(''); }
		
		if ($("#email").valid()==false) 
		{
			return false;
		}
		else
		{
			$("#preloader").css("display", "block");
			var csrf_test_name = $("#csrf_token").val();
			if(email != '')
			{
				parameters= { 'email':email, 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('personalInfoInd/send_codes_email'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						$("#csrf_token").val(data.csrf_new_token);
						if(data.flag == "success")
						{ 
							$("#send_email_code").text('Verification Code Sent'); 
							$('#send_email_code').prop('disabled', true);
							
							$('#email_timer_div').removeClass('d-none');
							timer(180);							
							setTimeout(function() { $('#send_email_code').text('Resend Verification Code'); $('#send_email_code').prop('disabled', false); }, 180*1000);
						}
						else {	}
						$("#preloader").css("display", "none");
					}
				});
			}
		}
	}

	function send_codes_mobile()
	{
		var mobile = $.trim($("#mobile").val());
		var mobile_code = $.trim($("#mobile_code").val());
		
		if(mobile == '') 
		{ 
			$("#mobile_error").html('<span id="mobile_error_span" class="error">Please enter the mobile</span>');
		}
		else { $("#mobile_error").html(''); }
		
		if ($("#mobile").valid()==false) 
		{
			return false;
		}
		else
		{
			$("#preloader").css("display", "block");
			var csrf_test_name = $("#csrf_token").val();
			if(mobile != '')
			{
				parameters= { 'mobile':mobile, 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('personalInfoInd/send_codes_mobile'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						$("#csrf_token").val(data.csrf_new_token);
						if(data.flag == "success")
						{ 
							$("#send_mobile_code").text('Verification Code Sent'); 
							$('#send_mobile_code').prop('disabled', true);
							
							$('#mobile_timer_div').removeClass('d-none');
							timer_mobile(180);							
							// setTimeout(function() { $('#send_mobile_code').text('Resend Verification Code'); $('#send_mobile_code').prop('disabled', false); }, 180*1000);
						}
						else {	}
						$("#preloader").css("display", "none");
					}
				});
			}
		}
	}
	
	function mobile_verification_otp_btn()
	{
		var current_mobile = "<?php echo $form_data['mobile']; ?>";
		var new_mobile = $.trim($("#mobile").val());
		if(current_mobile != new_mobile)
		{
			$('#send_mobile_code').prop('disabled', false);
			$('#mobile_code').prop('readonly', false);
		}
		else
		{

			$('#send_mobile_code').prop('disabled', true);
			$('#mobile_code').prop('readonly',true);
			$("#mobile_code").val("");
		}		
	}
	
	function send_mobile_verification_otp()
	{
		var country_code = $.trim($("#country_code").val());
		var mobile = $.trim($("#mobile").val());		
		
		if(country_code == '') 
		{ 
			$("#country_code_error").html('<span id="country_code_error_span" class="error">Please select the Country Code</span>');
		}
		else { $("#country_code_error").html(''); }
		
		if(mobile == '') 
		{ 
			$("#mobile_error").html('<span id="mobile_error_span" class="error">Please enter the  Phone Number</span>');
		}
		else { $("#mobile_error").html(''); }
		
		if($("#country_code").valid()==false || $("#mobile").valid()==false) 
		{
			return false;
		}
		else
		{
			$("#preloader").css("display", "block");
			var csrf_test_name = $("#csrf_token").val();
			if(country_code != '' && mobile != '')
			{
				parameters= { 'country_code':encodeURIComponent($.trim(country_code)), 'mobile':encodeURIComponent($.trim(mobile)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('personalInfoInd/send_otp_personal_info'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						$("#csrf_token").val(data.csrf_new_token);
						if(data.flag == "success")
						{ 
							$("#send_otp").text('OTP Sent'); 
							$('#send_otp').prop('disabled', true);
							setTimeout(function() { $('#send_otp').text('Resend OTP'); $('#send_otp').prop('disabled', false); }, 30*1000);
						}
						else {	}
						$("#preloader").css("display", "none");
					}
				});
			}
		}		
	}
	
	
	$(document ).ready( function() 
	{
		/******* JQUERY VALIDATION METHOD TO CHECK EMAIL VERFICATION CODE *********/
		$.validator.addMethod("custom_validate_email_code", function(value, element)
		{
			var current_email = "<?php echo $form_data['email']; ?>";
			var new_email = $.trim($("#email").val());
			
			if(current_email == new_email) 
			{ 
				return true; 
			}
			else
			{ 
				var email_code_value = $.trim(value);
				if(email_code_value == '')
				{
					$.validator.messages.custom_validate_email_code = 'Please enter the code';
					return false;
				}
				else
				{
					var isSuccess = false;
					var csrf_test_name = $("#csrf_token").val();
					var parameter = { "email_code_value":encodeURIComponent($.trim(email_code_value)), "new_email":encodeURIComponent($.trim(new_email)), "csrf_test_name":encodeURIComponent($.trim(csrf_test_name))  }
						
					$.ajax(
					{
						type: "POST",
						url: "<?php echo site_url('personalInfoInd/verify_codes_email') ?>",
						data: parameter,
						async: false,
						dataType: 'JSON',
						success: function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							if($.trim(data.flag) == 'success')
							{
								isSuccess = true;
								$("#code_check_success_email" ).removeClass('d-none');
							} 
							else 
							{ 
								isSuccess = false; 
								$("#code_check_success_email" ).addClass('d-none');
							}
							
							$.validator.messages.custom_validate_email_code = data.response;
						}
					});
						
					return isSuccess;
				}
			}
		});
		
		$.validator.addMethod("custom_validate_mobile_code", function(value, element)
		{
			var current_mobile = "<?php echo $form_data['mobile']; ?>";
			var new_mobile = $.trim($("#mobile").val());
			
			if(current_mobile == new_mobile) 
			{ 
				return true; 
			}
			else
			{ 
				var mobile_otp_value = $.trim(value);
				if(mobile_otp_value == '')
				{
					$.validator.messages.custom_validate_mobile_code = 'Please enter the OTP';
					return false;
				}
				else
				{
					var isSuccess = false;
					var csrf_test_name = $("#csrf_token").val();
					var parameter = { "mobile_otp_value":encodeURIComponent($.trim(mobile_otp_value)), "new_mobile":encodeURIComponent($.trim(new_mobile)), "csrf_test_name":encodeURIComponent($.trim(csrf_test_name))  }
						
					$.ajax(
					{
						type: "POST",
						url: "<?php echo site_url('personalInfoInd/verify_codes_mobile') ?>",
						data: parameter,
						async: false,
						dataType: 'JSON',
						success: function(data)
						{
							$("#csrf_token").val(data.csrf_new_token);
							if($.trim(data.flag) == 'success')
							{
								isSuccess = true;
								$("#code_check_success_mobile" ).removeClass('d-none');

							} else { 
								isSuccess = false; 
								$("#code_check_success_mobile" ).addClass('d-none');

							}
							
							$.validator.messages.custom_validate_mobile_code = data.response;
						}
					});
						
					return isSuccess;
				}
			}
		});
				
		$.validator.addMethod('lowercasesymbols', function(value) 
		{
			return value.match(/^[^A-Z]+$/);
    }, 'You must use only lowercase letters in email')
		
		/******* JQUERY VALIDATION *********/
		$("#ind_personal_info_form").validate( 
		{		
			/* debug: true,
			onkeyup: false, */
			rules:
			{
				title: { required: true },
				first_name: { required: true, normalizer: function(value) { return $.trim($("#first_name").val()); } },
				middle_name: {  },
				last_name: { required: true, normalizer: function(value) { return $.trim($("#last_name").val()); } },
				email: { required: true, normalizer: function(value) { return $.trim($("#email").val()); }, valid_email: true, lowercasesymbols : true, remote: { url: "<?php echo site_url('personalInfoInd/check_personal_email_exist_ajax') ?>", type: "post"/* , async: false */ } },
				email_code: 
				{ 
					/* digits:true, */ minlength:6, maxlength:6,
					required: 
					{
						depends: function () 
						{ 
							var current_email = "<?php echo $form_data['email']; ?>";
							var new_email = $.trim($("#email").val());
		
							if(current_email != new_email) { return true; }
							else { return false; }
						}
					},				
					custom_validate_email_code:true 
				},				
				country_code: { required: true },
				mobile: { required: true, normalizer: function(value) { return $.trim($("#mobile").val()); }, digits:true, minlength:10, maxlength:11, min:1, remote: { url: "<?php echo site_url('personalInfoInd/check_personal_mobile_exist_ajax') ?>", type: "post"/* , async: false */ } },
			    mobile_code: 
				{ 
					digits:true, minlength:6, maxlength:6,
					required: 
					{
						depends: function () 
						{ 
							var current_mobile = "<?php echo $form_data['mobile']; ?>";
							var new_mobile = $.trim($("#mobile").val());
		
							if(current_mobile != new_mobile) { return true; }
							else { return false; }
						}
					},				
					custom_validate_mobile_code:true 
				}, 
				gender: { required: true },
			},
			messages:
			{
				title: { required: "Please select the  Title" },
				first_name: { required: "Please enter the  First Name", normalizer: "Please enter the  First Name" },
				middle_name: { },
				last_name: { required: "Please enter the  Last Name", normalizer: "Please enter the  Last Name" },
				email: { required: "Please enter the  Email address", normalizer: "Please enter the  Email address", valid_email: "Please enter the valid Email address", remote: " Email address already exist" },
				email_code: { required: "Please enter the Code", minlength: "Please enter only 6 numbers", maxlength: "Please enter only 6 numbers" },
				country_code: { required: "Please select the Country Code" },
				mobile: { required: "Please enter the  Phone Number", normalizer: "Please enter the  Phone Number", minlength: "Please enter minimum 10 numbers", maxlength: "Please enter maximum 11 numbers", min: "Please enter valid number", remote: " Phone Number already exist"},
				otp: { required: "Please enter the OTP", minlength: "Please enter only 6 numbers", maxlength: "Please enter only 6 numbers" },
				gender: { required: "Please select the  Gender" },
			},
			submitHandler: function(form)
			{
				/* form.submit(); */
				swal(
				{
					title:"UPDATE?",
					text: "Are you sure you want to update your personal information?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes!'
				}).then(function (result) { if (result.value) { form.submit(); } });
			},
			errorElement: 'span',
			errorPlacement: function (error, element) { element.closest('.form-group').append(error); },
			highlight: function (element, errorClass, validClass) { $(element).addClass('is-invalid'); },
			unhighlight: function (element, errorClass, validClass) { $(element).removeClass('is-invalid'); }
		});
	});
</script>
		
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
		
<script>	
	/* $(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); }); */
</script>				