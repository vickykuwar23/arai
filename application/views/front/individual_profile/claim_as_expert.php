<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>datepicker/datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
    <div class="profileinfo" data-aos="fade-down">
        <div class="container">
            <div class="row">

                <div class="col-md-2">
                    <?php if (isset($profile_information[0]['profile_picture'])){ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/<?php echo $profile_information[0]['profile_picture']; ?>" alt="" class="img-fluid bor">
                      
                    <?php }else{ ?>
                    <img src="<?php echo base_url('assets/') ?>profile_picture/user_dummy.png" alt="" class="img-fluid bor">
                  <?php } ?>
                </div>
                <div class="col-md-6">
                    <h3 class="mt-4"><?php echo $user[0]['title']." ".$user[0]['first_name']; ?></h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-paper-plane"></i> <?php echo $user[0]['email'] ?></a></li>
                        <li><i class="fa fa-user"></i> <?php echo $user[0]['sub_catname'] ?></li>
                    </ul>
                    <div class="form-group upload-btn-wrapper2">
                        <a href="<?php echo base_url('profile') ?>" class="btn btn-upload2">Back To Profile</a>
                    </div>
                   
                </div>
                <!-- <div class="col-md-4">
                    <div class="countBox">
                        <h4>02</h4>
                        <span>Challenges</span>
                    </div>
                    <div class="countBox">
                        <h4>50</h4>
                        <span>Points</span>
                    </div>

                </div> -->
            </div>
        </div>
    </div>
<section id="individual-profile-form" class="inner-page" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="formInfo">
                     <div class="col-12">
                        <?php if ($this->session->flashdata('success') != "") { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="fa fa-check"></i> Success!</h5>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php } ?>
                              <?php if ($this->session->flashdata('error') != "") { ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Error!</h5>
                                <?php echo $this->session->flashdata('error'); ?>
                            </div>

                        <?php } ?>
                    <?php 
                        if(validation_errors()!="" ){
                            validation_errors();  
                        }
                    ?>
                    </div>
                   <form method="post" id="individualProfileForm" name="frm" role="form" enctype="multipart/form-data">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                        <?php 
                            $ProfileId = '';
                            if(isset($profile_information[0]['id']))
                            {
                                $ProfileId = $profile_information[0]['id'];
                            }
                        ?>
                        <input type="hidden" name="profileId" value="<?php echo $ProfileId ?>">

                                       <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <?php 
                                                $bioData = '';
                                                if(isset($profile_information[0]['bio_data']))
                                                {
                                                    $bioData = $profile_information[0]['bio_data'];
                                                }
                                            ?>
                                        
                                          <textarea name="bio_data" class="form-control bio_data" rows="1" id="bio_data"><?php echo $bioData ?></textarea>
                                            <label  class="form-control-placeholder" for="exampleInputEmail1">Profile in Brief<em class="mandatory">*</em></label>
                                          <span class="error"><?php echo form_error('bio_data'); ?></span>
                                        </div>
                                    </div>

                                    

                                </div>
                                    <div class="row">
                                    
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <?php 
                                                    $numberOfPaperPublication = '';
                                                    if(isset($profile_information[0]['no_of_paper_publication']))
                                                    {
                                                        $numberOfPaperPublication = $profile_information[0]['no_of_paper_publication'];
                                                    }
                                                ?>
                                              
                                              <select name="no_of_paper_publication" id="no_of_paper_publication" class="form-control paperPublication" autocomplete="nope" >
                                                <option value=""></option>

                                                <?php for ($i=0; $i<=50  ; $i++) {  ?>
                                                    <option value="<?php echo $i ?>" <?php if($numberOfPaperPublication == $i ) { ?> selected="selected" <?php } ?> > <?php echo $i ?> </option>
                                                <?php } ?>
                                               
                                          
                                              </select>
                                              <label  class="form-control-placeholder flotingCss" for="exampleInputEmail1">No. of Paper Publication<em class="mandatory">*</em></label>
                                              <span><?php echo form_error('no_of_paper_publication'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                   <div id="paperDetails_wrapper">

                                     <?php if (isset($profile_information[0]['paper_year']) && count($profile_information[0]['paper_year'])> 0 ) { 
                                        $paper_conf_name=$profile_information[0]['paper_conf_name'];
                                        $paper_title=$profile_information[0]['paper_title'];
                                       foreach ($profile_information[0]['paper_year'] as $key => $value) {?>
                                        
                                        <div class="row paperDetails">
                                            <div class="col-md-1"><div class="form-group"><label class="form-control-placeholder" for="exampleInputEmail1"><?php echo $key+1 ?></label></div></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control only_year"   name="paper_year[]" value="<?php echo $value; ?>" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Year</label>

                                                </div></div>

                                                    <div class="col-md-4"><div class="form-group">
                                                    <input type="text" class="form-control" value="<?php echo $paper_conf_name[$key]; ?>" name="paper_conf_name[]" value="" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Conference Name</label>
                                                </div></div>

                                                    <div class="col-md-4"><div class="form-group" class="form-control-placeholder">
                                                    <input type="text" class="form-control"   name="paper_title[]" value="<?php echo $paper_title[$key]; ?>" maxlength="200">
                                                    <label for="exampleInputEmail1" class="form-control-placeholder">Title</label>
                                                </div></div></div>


                                       <?php } } ?>
                                  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <?php 
                                                    $numberOfPatents = '';
                                                    if(isset($profile_information[0]['no_of_patents']))
                                                    {
                                                        $numberOfPatents = $profile_information[0]['no_of_patents'];
                                                    }
                                                ?>
                                             
                                              <select name="no_of_patents" id="no_of_patents" class="form-control Patents" autocomplete="nope" >
                                                <option value=""></option>

                                                <?php for ($i=0; $i<=50  ; $i++) {  ?>
                                                    <option value="<?php echo $i ?>" <?php if($numberOfPatents == $i ) { ?> selected="selected" <?php } ?> > <?php echo $i ?> </option>

                                                <?php } ?>


                                              </select>
                                               <label class="form-control-placeholder flotingCss"  for="exampleInputEmail1">No. of Patents<em class="mandatory">*</em></label>
                                              <span><?php echo form_error('no_of_patents'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <label class="patentDetails">Patent Details</label>
                                    <div id="patentDetails_div">
                                        
                                         <?php if (isset($profile_information[0]['patent_year']) && count($profile_information[0]['patent_year'])> 0 ) { 
                                        $patent_number=$profile_information[0]['patent_number'];
                                        $patent_title=$profile_information[0]['patent_title'];
                                        foreach ($profile_information[0]['patent_year'] as $key => $value) {?>
                                                    
                                            <div class="row patentDetails">
                                                <div class="col-md-1"><div class="form-group"><label for="exampleInputEmail1"> 1 </label></div>
                                                </div>
                                            
                                            <div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"  name="patent_year[]" value="<?php echo $value; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Year</label>
                                            </div></div>

                                            <div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="patent_number[]" value="<?php echo $patent_number[$key]; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Patent Number</label>

                                            </div></div>

                                            <div class="col-md-4"><div class="form-group"><input type="text" class="form-control"  placeholder="Enter Title" name="patent_title[]" value="<?php echo $patent_title[$key]; ?>" maxlength="200">
                                                <label class="form-control-placeholder" for="exampleInputEmail1">Title</label>
                                            </div></div>
                                            </div>        

                                          <?php } } ?>
                                  
                                     </div>
                                    <label>Links to referal Links, Portals that you are operating from. Where else can we find you?</label>


                                       <div id="portal_row">

                                    <?php if (isset($profile_information[0]['portal_name_multiple']) && count($profile_information[0]['portal_name_multiple'])> 0 ) { 
                                        
                                        $portal_link=$profile_information[0]['portal_link_multiple'];
                                        $portal_description=$profile_information[0]['portal_description_multiple'];
                                       
                                       foreach ($profile_information[0]['portal_name_multiple'] as $key => $value) { 

                                        ?>
                                    <div class="row">
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="<?php echo $value; ?>" class="form-control portal_name" id="portal_name"  name="portal_name[]" maxlength="200">
                                              <label class="form-control-placeholder" for="exampleInputEmail1">Portal Name <em class="mandatory">*</em></label>
                                              <span class="error"><?php echo form_error('portal_name'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                               
                                              <input type="text" class="form-control portal_link" id="portal_link" placeholder="" name="portal_link[]" value="<?php echo $portal_link[$key]  ?>" maxlength="200">
                                              <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link<em class="mandatory">*</em></label>

                                              <span class="error"><?php echo form_error('portal_link'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-5">
                                            <div class="form-group">
                                               
                                                <textarea name="portal_description[]" class="form-control portal_description" id="portal_description0" rows="1"><?php echo $portal_description[$key]; ?></textarea>
                                            <label class="form-control-placeholder" for="exampleInputEmail1">Portal Description</label>

                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                             <?php if ($key==0) {?>
                                                <button class="add_portal_field_button btn btn-success btn-sm">+</button>
                                             <?php }else{ ?>   
                                                <button type="button" class="remove_portal_field btn btn-danger btn-sm">-</button>
                                            <?php } ?>
                                            
                                            </div>
                                            </div>
                                    </div>
                                         
                                     <?php } }
                                     else{
                                      ?>

                                        <div class="row">
                                        <div class="col-md-3">                 
                                            <div class="form-group">
                                       
                                              <input type="text" value="" class="form-control portal_name" id="portal_name" name="portal_name[]" maxlength="200">
                                              <label  class="form-control-placeholder" for="exampleInputEmail1">Portal Name</label>

                                              <span class="error"><?php echo form_error('portal_name'); ?></span>
                                            </div>  
                                        </div>
                                        <div class="col-md-4">                 
                                            <div class="form-group">
                                               
                                          
                                              <input type="text" class="form-control portal_link" id="portal_link" placeholder="" name="portal_link[]" maxlength="200">
                                                <label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link</label>
                                              <span class="error"><?php echo form_error('portal_link'); ?></span>
                                            </div>  
                                        </div>
                                         <div class="col-md-4">
                                            <div class="form-group">
                                               
                                               
                                                <textarea name="portal_description[]" class="form-control portal_description" id="portal_description" rows="1"></textarea>
                                                 <label class="form-control-placeholder" for="exampleInputEmail1">Portal Description</label>
                                             
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                             <div class="form-group">
                                                <button class="add_portal_field_button btn btn-success btn-sm">+</button>
                                            </div>
                                            </div>
                                    </div>


                                     <?php } ?> 
                                    </div>
                                    <div class="d-flex justify-content-center">
                                    <button type="submit" name="btn_submit" value="btn_submit" class="btn btn-primary">Submit</button>
                               </div>  
                               </div> 
                                    </div>
                                    
                                  
                               

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End #main -->

<!-- <script src="<?php echo base_url('assets/front/js/jquery/jquery.js'); ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/front/js/select2.min.js'); ?>"></script> -->
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">
  
    // var paperPublicationValue = $('.paperPublication').val();
    // if(paperPublicationValue == 0)
    // {
    //     $('.paperDetails').hide();
    // }
    // if(paperPublicationValue != 0)
    // {
    //     $('.paperDetails').show();
    // }
    // $(document).on('change','.paperPublication', function(){
    //     var paperPublicationValue = $('.paperPublication').val();
    //     if(paperPublicationValue == 0)
    //     {
    //         $('.paperDetails').hide();
    //     }
    //     if(paperPublicationValue != 0)
    //     {
    //         $('.paperDetails').show();
    //     }
    // });

    // var patentValue = $('.Patents').val();
    // if(patentValue == 0)
    // {
    //     $('.patentDetails').hide();
    // }
    // if(patentValue != 0)
    // {
    //     $('.patentDetails').show();
    // }
    // $(document).on('change','.Patents', function(){
    //     var patentValue = $('.Patents').val();
    //     if(patentValue == 0)
    //     {
    //         $('.patentDetails').hide();
    //     }
    //     if(patentValue != 0)
    //     {
    //         $('.patentDetails').show();
    //     }
    // });


    // Smart Wizard
$(document).ready(function() {


      $.validator.setDefaults({
           submitHandler: function (form) {
                // form.submit();
           
                var text = 'Are you sure you want to submit the form ?';

                    swal({
                    title:'Confirm' ,
                    text: text,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                    }).then(function (result) {
                    if (result.value) {
                      form.submit();
                  }
                }); 
           
           }
         });

    $('#individualProfileForm').validate({

        rules: {
           
            "specify_fields_area_that_you_would_like[]": {
                required : true,
            },
            bio_data: {
                required : true,
            },
            // profile_picture: {
            //     required : true,
            // },
            years_of_experience: {
                required : true,
            },
            no_of_paper_publication: {
                required : true,
            },
            paper_year: {
                required : true,
            },
            paper_conf_name: {
                required : true,
            },
            paper_title: {
                required : true,
            },
            no_of_patents: {
                required : true,
            },
            patent_year: {
                required : true,
            },
            patent_number: {
                required : true,
            },
            patent_title: {
                required : true,
            },
            portal_name: {
                required : true,
            },
            portal_link: {
                required : true,
                url : true,
            },
            portal_description: {
                required : true,
            },
        },
        messages: {
           
            specify_fields_area_that_you_would_like: {
                required : "Please Enter Fields / Area that you would like",
            },
            bio_data: {
                required : "Please Enter Bio Data",
            },
        
            years_of_experience: {
                required : "Please Select Years of Expertise",
            },
            no_of_paper_publication: {
                required : "Please Select Number of Paper Publication",
            },
            paper_year: {
                required : "Please Enter Paper Year",
            },
            paper_conf_name: {
                required : "Please Enter Paper Conf. Name",
            },
            paper_title: {
                required : "Please Enter Paper Title",
            },
            no_of_patents: {
                required : "Please Select No of Patents",
            },
            patent_year: {
                required : "Please Enter Patent Year",
            },
            patent_number: {
                required : "Please Enter Patent Number",
            },
            patent_title: {
                required : "Please Enter Patent Title",
            },
            portal_name: {
                required : "Please Enter Portal Name",
            },
            portal_link: {
                required : "Please Enter Portal Link",
            },
            portal_description: {
                required : "Please Enter Portal Description",
            },
        },
       errorElement: 'span',
       errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.form-group').append(error);
       },
       highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
       },
       unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
       }
     });
});

</script>

<script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $("#portal_row"); //Fields wrapper
            var add_button      = $(".add_portal_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-3"><div class="form-group"><input type="text"  class="form-control portal_name" id="portal_name" name="portal_name[]" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Name</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control portal_link" id="portal_link" placeholder name="portal_link[]"  maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Link</label></div>  </div><div class="col-md-4"><div class="form-group"><textarea name="portal_description[]" class="form-control portal_description" id="portal_description'+x+'" rows="1"></textarea><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Portal Description</label><span class="error"></span></div></div><div class="col-md-1"><button type="button" class="remove_portal_field btn btn-danger btn-sm">-</button></div></div>'); //add input box

                }

            });

            $(wrapper).on("click",".remove_portal_field", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>

 <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $("#wrapper_div"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"><div class="col-md-11"><div class="form-group"><textarea name="specify_fields_area_that_you_would_like[]" class="form-control specify_fields_area_that_you_would_like"  rows="1"></textarea></div></div><div class="col-md-1"><div class="form-group"><button class="remove_field_button btn btn-danger btn-sm">-</button></div></div></div>'); //add input box

                }

            });

            $(wrapper).on("click",".remove_field_button", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })



            });

      </script>


    <script>
            $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper_exp         = $("#experience_row"); //Fields wrapper
            var add_button_exp      = $(".add_field_button_experience"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button_exp).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper_exp).append('<div class="row"><div class="col-md-5"><div class="form-group"><label for="exampleInputEmail1">Technical Experience Year</label><input type="text" class="form-control technical_experience_in_year" id="technical_experience_in_year" placeholder="Enter Technical Experience in Year" name="technical_experience_in_year[]" maxlength="200"></div> </div><div class="col-md-6"><div class="form-group"><label for="exampleInputEmail1">Description of Work</label><textarea name="technical_description_of_work[]" class="form-control technical_description_of_work" rows="1" id="technical_description_of_work"></textarea ></div> </div><div class="col-md-1"><div class="form-group"><button class="remove_field_button_experience btn btn-danger btn-sm">-</button></div></div></div>'); //add input box

                }

            });

            $(wrapper_exp).on("click",".remove_field_button_experience", function(e){ //user click on remove text

                e.preventDefault(); 

                $(this).closest('.row').remove(); 

                x--;

            })

            
            $("#no_of_paper_publication").change(function(event) {
                    $('.paperDetails').remove();
                    var no_of_paper_publication = $(this).val();
             
                      setTimeout(function(){ paperDetails_append(no_of_paper_publication);  }, 500);

                      setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                format: "yyyy",
                                viewMode: "years",
                                minViewMode: "years"
                               
                            }
                            ); 

                    }, 500);
            });



             $('body').on('change', '#no_of_patents', function() {

                    $('.patentDetails').remove();
                     // $('.patentDetails').remove(); 
                    var no_of_patents = $(this).val();

                    setTimeout(function(){ patentDetails_append(no_of_patents);  }, 500);

                    setTimeout(function(){ 

                        $('.only_year').datepicker(
                            {
                                /* todayBtn: "linked",  */
                                keyboardNavigation: true,
                                forceParse: true,
                                calendarWeeks: false,
                                autoclose: true,
                                format: "yyyy",
                                viewMode: "years",
                                minViewMode: "years"
                               
                            }
                            ); 

                    }, 500);

            });

              $('.only_year').datepicker(
            {
                /* todayBtn: "linked",  */
                keyboardNavigation: true,
                forceParse: true,
                calendarWeeks: false,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
               
            }
            );


            });
     

function paperDetails_append(argument) {
                  if (argument > 10) {
                      argument = 10;
                   } 
                    var wrapper         = $("#paperDetails_wrapper"); //Fields wrapper
                    var x = 1; //initlal text box count
                    var template='';
                    for (var i = 1; i <= argument; i++) {
                            template +='<div class="row paperDetails"><div class="col-md-1"><div class="form-group"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">'+ i +'</label></div></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"   name="paper_year[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" class="form-control-placeholder floatinglabel" for="exampleInputEmail1">Year</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="paper_conf_name[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Conference Name</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="paper_title[]" value="" maxlength="200"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Title</label></div></div></div>'; //add input box

                    }
                         // alert(template)

                    $(wrapper).append(template)
            }       

    function patentDetails_append(argument) {
                   if (argument > 10) {
                      argument = 10;
                   } 
                   var wrapper         = $("#patentDetails_div"); //Fields wrapper
                    var x = 1; //initlal text box count
                    var template_two='';
                    for (var i = 1; i <= argument; i++) {
                            template_two +='<div class="row patentDetails"><div class="col-md-1"><div class="form-group"><label class="form-control-placeholder floatinglabel" for="exampleInputEmail1">'+(i)+'</label></div></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control only_year"   name="patent_year[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Year</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"   name="patent_number[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Patent Number</label></div></div><div class="col-md-4"><div class="form-group"><input type="text" class="form-control"  name="patent_title[]" value="" maxlength="200"><label for="exampleInputEmail1" class="form-control-placeholder floatinglabel">Title</label></div></div></div>'; //add input box

                    }
                         // alert(template)

                    $(wrapper).append(template_two)
    }

      </script>
<!-- 
<script>
      for(var x=0; x<5; x++){   
            $('#patentDetails').append('<div class="row"><div class="col-md-3"><label for="exampleInputEmail1">Year</label><input type="date" class="form-control" id="patent_year" placeholder="Enter Year" name="patent_year" value="" maxlength="200"></div></div>');  
      } 
</script> -->