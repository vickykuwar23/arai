<div id="home-p" class="home-p faqBanner text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Our Most Frequently asked Questions</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item active" aria-current="page">FAQ </li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>


    <section id="registration-form" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
               
                 <div class="formInfo">
                    <div class="faq-p1-cont">
                        <div class="service-h-tab"> 
                          <!--<nav class="nav nav-tabs" id="myTab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-expanded="true">Payment</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile">Account</a> 
                            <a class="nav-item nav-link" id="my-profile-tab" data-toggle="tab" href="#my-profile" role="tab" aria-controls="my-profile">Basic</a> 
                          </nav>-->
                          <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">      
								<?php if(count($faq_data) > 0){
								  
									foreach($faq_data as $faq_c){
										
								?>
								
							  <div class="toggle">
                                <div class="toggle-title ">
                                  <h3>
                                  <i class="fa fa-sort-desc" aria-hidden="true"></i>
                                  <span class="title-name"><?php echo ucfirst($faq_c['faq_title']); ?></span>
                                  </h3>
                                </div>
                                <div class="toggle-inner">
                                  <p><?php echo ucfirst($faq_c['faq_desc']); ?></p>
								  <?php if($faq_c['faq_img']!=""){ ?>
								  <form name="file_up" id="file_up" method="post">
								  <input type="hidden" name="faq_file" id="faq_file" value="<?php echo $faq_c['faq_img']; ?>" />
								  <!--<p><input type="submit" name="faq_btn" id="faq_btn" class="btn btn-primary" value="Download"/></p>-->
								  <p><button type="submit" name="faq_btn" id="faq_btn" value="Download"><i class="fa fa-download"></i></button></p>
								  </form>
								  <?php } ?>
                                </div>
                              </div>

								<?php 
								
									} // Foreach End
									
								} // If END
								
								?>
                              
                            </div>
                            
                          </div>
                        </div>
                      </div>
                 </div>
    
                </div>
    
            </div>
        </div>
    </section>
	<script>
            /*if( jQuery(".toggle .toggle-title").hasClass('active') ){
                    jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
                }
                jQuery(".toggle .toggle-title").click(function(){

                    if( jQuery(this).hasClass('active') ){
                        jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
                    }
                    else{   jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
                    }
                });*/
				
				if( jQuery(".toggle .toggle-title").hasClass('active') ){
					
                    jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
                }
                jQuery(".toggle .toggle-title").click(function(){ 
					jQuery('.toggle').find('.toggle-title').not(this).removeClass('active');
					 jQuery('.toggle').find('.toggle-inner').not(this).slideUp(200);
                    if( jQuery(this).hasClass('active') ){
                        jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
                    }
                    else{   jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
                    }
                });	
				
        </script>