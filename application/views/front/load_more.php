<?php if ($all_featured_experts) { ?>
 <?php foreach ($all_featured_experts as $key => $user) { ?>
     <div class="col-md-4 p3">
        <div class="expertsListBox allExperts">
           <?php if($user['profile_picture']) { ?>
               <img src="<?php echo base_url('assets/profile_picture/'.$user['profile_picture'].'') ?>" alt="experts">
           <?php } else { ?>
               <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="experts">
            <?php } ?>
           <div class="experts">
               <span class="read"><a href="<?php echo base_url(); ?>profile/viewProfile/<?php echo $user['user_id'] ?>">View Profile</a></span>
               <?php if(isset($user['first_name'])) { ?>
                   <h3><?php echo $user['first_name'].' '.$user['last_name'] ?></h3>
               <? } else { ?>
                   <h3><?php echo $user['institution_full_name'] ?></h3>
               <?php } ?>
               <p><?php echo $user['designation_description'] ?></p>
           </div>
           <div class="expertsBox">
               <ul>
                   <?php if($user['years_of_experience']) { ?>
                       <li><span><?php echo $user['years_of_experience'] ?></span>Years of Experience</li>
                   <?php } else { ?>
                       <li><span>0</span>Years of Experience</li>
                   <?php } ?>
                   <?php if($user['no_of_patents']) { ?>
                       <li><span><?php echo $user['no_of_patents'] ?></span> Patents</li>
                   <?php } else { ?>
                       <li><span>0</span> Patents</li>
                   <?php } ?>
                   <?php if($user['no_of_paper_publication']) { ?>
                       <li><span><?php echo $user['no_of_paper_publication'] ?></span> Academic Publications</li>
                   <?php } else { ?>
                       <li><span>0</span> Academic Publications</li>
                   <?php } ?>
               </ul>
           </div>
        </div>
     </div>
 <?php } ?>
<?php } ?>