<div id="home-p" class="home-p buildYourTeam text-center" data-aos="fade-down">
    <div class="container searchBox">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s"><?php echo $build_data[0]['page_title'] ?></h1>
        <nav aria-label="breadcrumb" class="">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $build_data[0]['page_title'] ?></li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>
<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">		   
				<div class="formInfo">
					<?php echo $build_data[0]['page_description'] ?>
				</div>
			</div>
		</div>
		<?php  if($build_data[0][0]['video_url']!=''){ ?>
        <div class="row">
            <div class="col-md-12">        
               <iframe width="420" height="315" src="<?php echo $build_data[0][0]['video_url'] ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    <?php } ?>
	</div>
</section>
<!--<section id="registration-form" class="inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-8" data-aos="fade-left">
				<div class="aboutInnerPage">
					<img src="img/aboutInnerPage.jpg" alt="About Inner Page" class="img-fluid">
				</div>
				<div class="textInfoAbout">
					<h2>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</h2>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					<ul class="detail-list">
						<li>Software development using Model Based Design (MBD) approach</li>
						<li>Monitoring of every cell Voltage , pack current and temperature</li>
						<li>Cell balancing (Passive)</li>
						<li>Advanced Estimation Techniques for State of Charge (SoC) and State of Health (SoH)</li>
						<li>Advanced Estimation Techniques for State of Power (SoP) and State of Safety (SoS)</li>
						<li>Active Monitoring and Derating</li>
						<li>Thermal Management</li>
						<li>Compatible with wide range of lithium-ion cells</li>
						<li>Failure detection and Diagnostics</li>
						<li>Modular and Scalable Architecture</li>
						<li>State of Art GUI for Monitoring, Configuration and Calibration</li>
					</ul>
				</div>
            </div>
            <div class="col-md-4" data-aos="fade-right">
				<div class="bs-example1">
				  <h3>Featured Video</h3> 
				  <div class="card-body">
						<img src="img/video.jpg" alt="video" class="img-fluid">
						<a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
				  </div>
					<a href="#" class="buttonNew">View All Work</a>

					<a href="#" class="buttonNew2">Get A Free Estimate</a>

				</div>
             <div class="bs-example mt-3">
                <h3>Featured Video</h3> 
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                </div>

                <div class="listAbout bs-example mt-4">
                <div class="row">
                    <div class="col-lg-5 col-sm-5"> 
                        <img src="img/aboutInnerPage.jpg" alt="About Inner Page" class="img-fluid">
                    </div>
                    <div class="col-lg-7 col-sm-7 pl-0">
                            <strong><a href="#">What is Lorem Ipsum? &amp; content goes here</a></strong>
                        <p><i class="fa fa-clock-o" aria-hidden="true"></i> November 21, 2020</p>
                    </div>
                    <div class="col-md-12">
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis...</p>
                    </div>
                </div>

					<div class="row mt-4">
						<div class="col-lg-5 col-sm-5"> 
							<img src="img/aboutInnerPage.jpg" alt="About Inner Page" class="img-fluid">

						</div>
						<div class="col-lg-7 col-sm-7 pl-0">
								<strong><a href="#">What is Lorem Ipsum? &amp; content goes here</a></strong>
							<p><i class="fa fa-clock-o" aria-hidden="true"></i> November 21, 2020</p>
						</div>
						<div class="col-md-12">
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae error sit voluptatem accusantium doloremque...</p>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>--->



