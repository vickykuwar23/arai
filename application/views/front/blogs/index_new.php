<style type="text/css">
   .search-sec {background: #c80032;}
.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block;
font-weight: bold; padding: 10px 8px; border-radius: 0px;}
   
.mCSB_inside>.mCSB_container { padding:0 !important;}

   
.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important;}
</style>
<div id="home-p" class="home-p newsUpdate text-center" data-aos="fade-down">
   <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s">News & Updates</h1>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb wow fadeInUp">
            <li class="breadcrumb-item"><a href="#">News</a></li>
            <li class="breadcrumb-item active" aria-current="page">News & Updates </li>
         </ol>
      </nav>
   </div>
</div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<section id="single-news-p1" class="single-news-p1">
  <div class="text-center mb-3">
     <a class="how_more btn btn-general btn-white" role="button" href="javascript:void(0)" onclick="open_subscription_modal()" >Not yet subscribed for our Newsletter, please subscribe here</a>
  </div>
   <div class="filterBox">
         <section class="search-sec">
            <div id="filterData" name="filterData">
               <div class="container">   
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row d-flex justify-content-center search_Box">                       
                           <div class="col-md-6">
                              <input type="text" class="form-control search-slt search_txt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />   
                              <button type="button" class="btn btn-primary searchButton" onclick="load_news_ajax(0,12,1)"><i class="fa fa-search" aria-hidden="true"></i></button>
                           </div>
                           <div class="clearAll">
                              <a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form(0,12)">Clear All</a>
                           </div>
                           <div class="col-md-12"></div>                      
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <div class="blog-list">
               <div id="news_outer"></div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="ad-box-sn" id="current_news_heading">
               <h3  style="color:#777;" class="pb-2">Current News</h3>
               <div id="featued_news_outer"></div>          
            </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript">
   
$(document).ready(function () { 
   load_news_ajax(1,12,0)
});
function clear_search_form(){
   $("#search_txt").val("");
   load_news_ajax(1,12,0,0)
}
function load_news_ajax(start,limit,is_search='',is_show_more=0){

      var searchText =$("#search_txt").val();

      if (is_search==1) {
         $("#search_txt").attr('placeholder','');

      if (searchText=='') {
         $("#search_txt").css('border-color','red');
         $("#search_txt").attr('placeholder','Please enter text');
         return false;
      }
      }
         
       $("#no_rec_found").remove();
        $("#showMoreBtn").remove();
        $("#preloader").show();

        var postData = {'start':start,'limit':limit,'searchText':searchText,'is_show_more':is_show_more}
        $.ajax(
        {
          type: "POST",
          url: "<?php echo site_url('blogs/load_news_ajax'); ?>",
          cache: false,
          data:postData,
          dataType: 'JSON',
          success:function(data)
          {
            if (is_search==1) {
               $("#news_outer").html('');
               $("#featued_news_outer").html('');
            }
            var news_html = data.news_html;
            if (data.featured_blog_count==0) {
               $("#current_news_heading").addClass('d-none');
            }else{
               $("#current_news_heading").removeClass('d-none');
            }
            $("#news_outer").append(news_html);
            $("#featued_news_outer").append(data.featured_news_html);
            $("#preloader").hide();
          }
        });
}  

</script>