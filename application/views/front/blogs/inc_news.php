<?php if(count($blog_data) > 0){
                  	
                  	foreach($blog_data as $blog_detail){
                  		$limit = 160;
                  		$strins = trim($blog_detail['blog_desc']);
                  		 
                  		$limit2 = 40;
                  		 if (strlen($blog_detail['blog_name'] ) > $limit2){
                  			$blog_detail['blog_name']  = substr($blog_detail['blog_name'] , 0, strrpos(substr($blog_detail['blog_name'] , 0, $limit2), ' ')) . '...'; 
                  		 }
                  
                  		if($blog_detail['blog_img']!=""){
                  			$fileExist = base_url('assets/blog/'.$blog_detail['blog_img']);
                  			$image = $fileExist;
                  		} else {
                  			$image = base_url('assets/News_default.png');
                  			
                  		}
                  		$str = strip_tags($strins);
                  		$chk_add  = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...';	
                  		 
                  ?>
               <article class="single-news">
                  <div class="row">
                     <div class="col-md-4 d-flex" style="position:relative">
                        <img src="<?php echo $image; ?>" class="img-fluid" alt="...">
                        <div class="text">
                           <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        </div>
                     </div>
                     <div class="col-md-8 d-flex pl-0">
                        <div class="p-3">
                           <div class="" style="margin: 0;font-size: 20px;font-weight: 500;line-height:22px;">
                              <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>">
                              <?php echo ucwords($blog_detail['blog_name']) ?>
                              </a>
                           </div>
                           <ul class="list-inline" style="color:#c80032;">
                              <li>Posted: <span class="text-theme-colored2" style="color:#333;"> <?php echo date('d M, Y', strtotime($blog_detail['createdAt'])) ?></span></li>
                           </ul>
                           <p><?php echo $chk_add; ?></p>
                           <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                     </div>
                  </div>
               </article>
               <?php } ?>
               
               <?php if($new_start < $news_all_count){ ?>
               <div class="show_more_main" id="showMoreBtn">
                 <a class="how_more btn btn-general btn-white" role="button" href="javascript:void(0)" onclick="load_news_ajax('<?php echo $new_start;?>','<?php echo $limit ?>',0,1)" >Load More</a>
               </div>
               <?php } ?>

<?php }else{ ?>
  <div id="no_rec_found" class="text-center">
     <span>No records found.</span>
  </div>
<?php } ?>