<?php foreach($featured_blog as $featuredata): ?>
               <div class="card">
                  <div class="desc-comp-offer-cont">
                     <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>">
                        <div class="thumbnail-blogs">
                           <div class="caption">
                              <i class="fa fa-chain"></i>
                           </div>
                           <?php 					
                              if($featuredata['blog_img']!=""){
                              	$imgPath = base_url('assets/blog/'.$featuredata['blog_img']);
                              	$image = $imgPath;	
                              	
                              } else {
                              	$image = base_url('assets/News_default.png');
                              }
                              
                              ?>
                           <img src="<?php echo $image; ?>" class="img-fluid" alt="...">
                        </div>
                     </a>
                     <div class="p-3">
                        <h3><?php 
                           $start = 80;
                           	 if (strlen($featuredata['blog_name'] ) > $start){
                           		$featuredata['blog_name']  = substr($featuredata['blog_name'] , 0, strrpos(substr($featuredata['blog_name'] , 0, $start), ' ')) . '...'; 
                           	 }
                           
                           echo $featuredata['blog_name'] ?></h3>
                        <p class="desc"><?php 
                           $limit = 200;
                           
                           $limit = 200;
                           $strins2 = trim($featuredata['blog_desc']);
                           $strsss = strip_tags($strins2);
                           $chk_ddd  = substr($strsss , 0, strrpos(substr($strsss , 0, $limit), ' ')) . '...';
                           echo $chk_ddd; ?></p>
                        <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
                     </div>
                  </div>
               </div>
               <?php endforeach; ?>    