 
  <div id="home-p" class="home-p newsUpdate text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">News & Updates</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">News</a></li>
                <li class="breadcrumb-item active" aria-current="page">News & Updates </li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>

  
<!--====================================================
                        single-news-p1
======================================================--> 
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<section id="single-news-p1" class="single-news-p1">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
			<div class="blog-list">
		 <?php
			if(count($blog_data) > 0){
				
				foreach($blog_data as $blog_detail){
					
					/*$fileExist = base_url('assets/blog/'.$blog_detail['blog_img']);
				
				
					 if (@GetImageSize($fileExist)) {
						$image = $fileExist;
					} else {
						$image = base_url('assets/news-14.jpg');
					}*/
					
					$limit = 300;
					 if (strlen($blog_detail['blog_desc'] ) > $limit){
						$blog_detail['blog_desc']  = substr($blog_detail['blog_desc'] , 0, strrpos(substr($blog_detail['blog_desc'] , 0, $limit), ' ')) . '...'; 
					 }
		 ?>
          <div class="single-news-p1-cont" style="margin-bottom: 30px; box-shadow: 1px 1px 1px rgba(0,0,0,0.1);">
            <?php //if($blog_detail[0]['blog_img']!=""): ?>
			<!--<div class="single-news-img">
              <img src="<?php echo base_url('assets/blog/'.$blog_detail['blog_img']); ?>" alt="" class="img-fluid">
            </div>-->
			<?php //} ?>
            <div class="single-news-desc m-0 pt-1 pb-0 px-0">
              <h3><?php echo $blog_detail['blog_name'] ?></h3>
              <ul class="list-inline">
                <li>Posted: <span class="text-theme-colored2"> <?php echo date('d M, Y', strtotime($blog_detail['createdAt'])) ?></span></li>
                <!--<li>By: <span class="text-theme-colored2">Admin</span></li>
                <li><i class="fa fa-comments-o"></i> 1 comments</li>-->
              </ul>
              <hr>
              <div class="bg-light-gray">
                <p><?php echo $blog_detail['blog_desc'] ?></p> 
                <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
              </div> 
            </div>
          </div>
		  <?php		  
			}
			?>
				<div class="show_more_main" id="show_more_main<?php echo $blog_detail['id']; ?>">
					<span id="<?php echo $blog_detail['id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts">Show more</span>
					<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
				</div>
			<?php
		  }
		 ?>
          
			</div>
        </div>
		<?php /* ?>
        <div class="col-md-4">
          <div class="small-news-box">
		  <?php foreach($featured_blog as $featureList){
				$fileExist = base_url('assets/blog/'.$featureList['blog_img']);
				
				
					 if (@GetImageSize($fileExist)) {
						$image = $fileExist;
					} else {
						$image = base_url('assets/news-14.jpg');
					}
		  ?>
            <a href="">
              <div class="right-side-sn-cont">
                <img src="<?php echo $image ?>" alt="" class="img-fluid" height="100" width="100" style="margin: 5px;">
                <p><?php echo $featureList['blog_name'] ?></p>
                <small><fa class="fa-watch"> <?php echo date('M d, Y', strtotime($featureList['createdAt'])) ?></fa></small>
              </div>
            </a>
			<?php } ?>
            
          </div>
          
        </div> 
		<?php }*/ ?>
		<div class="col-md-4">           
            <div class="ad-box-sn"> 
              <h3 style="color:#777;" class="pb-2">Current News</h3>
			  <?php foreach($featured_blog as $featuredata): ?>
              <div class="card">
                <div class="desc-comp-offer-cont">				
                 <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>"><div class="thumbnail-blogs">
                    <div class="caption">
                     <i class="fa fa-chain"></i>
                    </div>
					<?php 
						
					/*$imgPath = base_url('assets/blog/'.$featuredata['blog_img']);	
					if (@GetImageSize($imgPath)) {
						$image = $imgPath;
					} else {
						$image = base_url('assets/blogs/not_available.png');
					}*/
					
					$imgPath = base_url('assets/blog/'.$featuredata['blog_img']);	
					if ($featuredata['blog_img']!="") {
						$image = $imgPath;
					} else {
						$image = base_url('assets/blogs/not_available.png');
					}
					
					//$image = base_url('assets/not_available.png');
					?>
                    <img src="<?php echo $image; ?>" class="img-fluid" alt="...">
                </div></a>
				<div class="p-3">
                <h3><?php 
				
				$start = 80;
					 if (strlen($featuredata['blog_name'] ) > $start){
						$featuredata['blog_name']  = substr($featuredata['blog_name'] , 0, strrpos(substr($featuredata['blog_name'] , 0, $start), ' ')) . '...'; 
					 }
				
				echo $featuredata['blog_name'] ?></h3>
                <p class="desc"><?php 
					$limit = 200;
					 if (strlen($featuredata['blog_desc'] ) > $limit){
						$featuredata['blog_desc']  = substr($featuredata['blog_desc'] , 0, strrpos(substr($featuredata['blog_desc'] , 0, $limit), ' ')) . '...'; 
					 }
				echo $featuredata['blog_desc'] ?></p>               
				
				<a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
                
				</div>
				</div>
              </div>
			  <?php endforeach; ?>              
            </div>
          </div> 
      </div>
    </div>
  </section>
<script type="text/javascript">

   $(document).ready(function () { 
	 
	 $(document).on('click','.show_more',function(){
		// $(".token").remove();
		
		var base_url = '<?php echo base_url(); ?>'; 
        var ID = $(this).attr('id');
		var length = $('.single-news-p1-cont').length;
		var cs_t = 	$('.token').val();
        $('.show_more').hide();
        $('.loding').show();
        $.ajax({
            type:'POST',
            url: base_url+'blogs/get_more',
            data:'id='+length+'&csrf_test_name='+cs_t,
			dataType:"text",
            success:function(data){
				//console.log(data);
				var output = JSON.parse(data);
				if(output.html == ""){
					$(".show_more_main").remove();
				}	
				$(".token").val(output.token);								
                $('#show_more_main'+ID).remove();				
                $('.blog-list').append(output.html);
            }
        });
    });
	 
   });
</script>


