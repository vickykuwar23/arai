
  <div id="home-p" class="home-p newsUpdate text-center" data-aos="fade-down">
    <div class="container">
        <h1 class="wow fadeInUp" data-wow-delay="0.1s">News & Updates</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb wow fadeInUp">
                <li class="breadcrumb-item"><a href="#">News</a></li>
                <li class="breadcrumb-item active" aria-current="page">News & Updates </li>
            </ol>
        </nav>
    </div>
    <!--/end container-->
</div>

  
<!--====================================================
                        single-news-p1
======================================================--> 


<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<section id="single-news-p1" class="single-news-p1">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
		<!-- <article>
			<div class="row">
			
			<div class="col-md-4">
			<img src="https://tipapp01uat.araiindia.com/arai_testing/assets/blog/d_1600433093.jpg" class="img-fluid" alt="...">
			</div>

			<div class="col-md-8 pl-0">
			<div class="p-3">
			<h3 class="mt-2">New Test Newssss</h3>
			<ul class="list-inline" style="color:#c80032;">
                <li>Posted: <span class="text-theme-colored2" style="color:#333;"> 16 Sep, 2020</span></li>
			  </ul>
			  <p>Signs of green shoots in passenger vehicle and two-wheeler segment: two-wheeler segment: Exide Industries bossSigns ofExide...</p>
			  <a href="#" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
			</div>
			
			</div>
		

			</div>
			<div class="text">
			<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>

			</div>
			</article> -->
			
			
			<div class="blog-list">
		 <?php
			if(count($blog_data) > 0){
				
				foreach($blog_data as $blog_detail){
					$limit = 160;
					$strins = trim($blog_detail['blog_desc']);
					 
					$limit2 = 40;
					 if (strlen($blog_detail['blog_name'] ) > $limit2){
						$blog_detail['blog_name']  = substr($blog_detail['blog_name'] , 0, strrpos(substr($blog_detail['blog_name'] , 0, $limit2), ' ')) . '...'; 
					 }

					if($blog_detail['blog_img']!=""){
						$fileExist = base_url('assets/blog/'.$blog_detail['blog_img']);
						$image = $fileExist;
						/* if (@GetImageSize($fileExist)) {
							$image = $fileExist;
						} else {
							$image = base_url('assets/news-14.jpg');
						} */
						
					} else {
						
						$image = base_url('assets/News_default.png');
						
					}

					$str = strip_tags($strins);
				$chk_add  = substr($str , 0, strrpos(substr($str , 0, $limit), ' ')) . '...';	
					
					 
		 ?>
			<article class="single-news">
				<div class="row">				
					<div class="col-md-4 d-flex" style="position:relative">
						<img src="<?php echo $image; ?>" class="img-fluid" alt="...">
						
						<div class="text">
							<a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>"><i class="fa fa-bars" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="col-md-8 d-flex pl-0">
						<div class="p-3">
							<div class="" style="margin: 0;font-size: 20px;font-weight: 500;line-height:22px;">
								<a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>">
									<?php echo ucwords($blog_detail['blog_name']) ?>
								</a>
							</div>
							<ul class="list-inline" style="color:#c80032;">
								<li>Posted: <span class="text-theme-colored2" style="color:#333;"> <?php echo date('d M, Y', strtotime($blog_detail['createdAt'])) ?></span></li>
							</ul>
							<p><?php echo $chk_add; ?></p>
							<a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
						</div>					
					</div>
				</div>				
			</article>
		 
          <!--<div class="single-news-p1-cont" style="margin-bottom: 30px; box-shadow: 1px 1px 1px rgba(0,0,0,0.1);">           
            <div class="single-news-desc m-0 pt-1 pb-0 px-0">
              <h3><?php echo $blog_detail['blog_name'] ?></h3>
              <ul class="list-inline">
                <li>Posted: <span class="text-theme-colored2"> <?php echo date('d M, Y', strtotime($blog_detail['createdAt'])) ?></span></li>
               
              </ul>
              <hr>
              <div class="bg-light-gray">
                <p><?php echo $blog_detail['blog_desc'] ?></p> 
                <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($blog_detail['id'])) ?>" class="mb-2">Read More <i class="fa fa-long-arrow-right"></i></a>
              </div> 
            </div>
          </div>-->
		  <?php		  
			}
			?>
			<div class="show_more_main" id="show_more_main<?php echo $blog_detail['id']; ?>">
				<span id="<?php echo $blog_detail['id']; ?>" class="show_more btn btn-general btn-white" title="Load more posts">Show more</span>
				<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
			</div>
			<?php
		  }
		 ?>
          
			</div>
        </div>		
		<div class="col-md-4">           
            <div class="ad-box-sn"> 
              <h3 style="color:#777;" class="pb-2">Current News</h3>
			  <?php foreach($featured_blog as $featuredata): ?>
              <div class="card">
                <div class="desc-comp-offer-cont">				
                 <a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>"><div class="thumbnail-blogs">
                    <div class="caption">
                     <i class="fa fa-chain"></i>
                    </div>
					<?php 					
					if($featuredata['blog_img']!=""){
						$imgPath = base_url('assets/blog/'.$featuredata['blog_img']);
						$image = $imgPath;	
						
					} else {
						$image = base_url('assets/News_default.png');
					}
					
					?>
                    <img src="<?php echo $image; ?>" class="img-fluid" alt="...">
                </div></a>
				<div class="p-3">
                <h3><?php 
				
				$start = 80;
					 if (strlen($featuredata['blog_name'] ) > $start){
						$featuredata['blog_name']  = substr($featuredata['blog_name'] , 0, strrpos(substr($featuredata['blog_name'] , 0, $start), ' ')) . '...'; 
					 }
				
				echo $featuredata['blog_name'] ?></h3>
                <p class="desc"><?php 
					$limit = 200;
					 /*if (strlen($featuredata['blog_desc'] ) > $limit){
						$featuredata['blog_desc']  = substr($featuredata['blog_desc'] , 0, strrpos(substr($featuredata['blog_desc'] , 0, $limit), ' ')) . '...'; 
					 }*/
					$limit = 200;
					$strins2 = trim($featuredata['blog_desc']);
					$strsss = strip_tags($strins2);
					$chk_ddd  = substr($strsss , 0, strrpos(substr($strsss , 0, $limit), ' ')) . '...';
				echo $chk_ddd; ?></p>               
				
				<a href="<?php echo base_url('blogs/blogDetails/'.base64_encode($featuredata['id'])) ?>"><i class="fa fa-arrow-circle-o-right"></i> Learn More</a>
                
				</div>
				</div>
              </div>
			  <?php endforeach; ?>              
            </div>
          </div> 
      </div>
    </div>
  </section>
<script type="text/javascript">

   $(document).ready(function () { 
	 
	 $(document).on('click','.show_more',function(){
		// $(".token").remove();
		
		var base_url = '<?php echo base_url(); ?>'; 
        var ID = $(this).attr('id');
		//var length = $('.single-news-p1-cont').length;
		var length = $('.single-news').length;
		var cs_t = 	$('.token').val();
        $('.show_more').hide();
        $('.loding').show();
        $.ajax({
            type:'POST',
            url: base_url+'blogs/get_more',
            data:'id='+length+'&csrf_test_name='+cs_t,
			dataType:"text",
            success:function(data){
				//console.log(data);
				var output = JSON.parse(data);
				if(output.html == ""){
					$(".show_more_main").remove();
				}	
				$(".token").val(output.token);								
                $('#show_more_main'+ID).remove();				
                $('.blog-list').append(output.html);
            }
        });
    });
	 
   });
</script>


