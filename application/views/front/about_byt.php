<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">About BYT</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="#">About</a></li>
				<li class="breadcrumb-item active" aria-current="page">About BYT</li>
			</ol>
		</nav>
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="formInfo">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h3> How BYT Works</h3>
							<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Velit maiores doloribus minima eveniet est accusantium, quia consequuntur, rem repellendus earum corrupti vel omnis? Nisi, eveniet corporis cum sed delectus molestias?</p>
							<div class="twoButton">
								<ul>
									<li><a href="javascript:void(0)">BYT For Your Task</a></li>
									<li><a href="<?php echo site_url('challenge'); ?>"> Build / Join a Team For Solving Existing Challenges</a></li>
									<li class="mt-4"><a href="<?php echo site_url('myteams/teamSearch'); ?>"> Search For Existing Teams</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	if (count($consortium_list) > 0) {
	?>
	<!--Client Logo Section-->
	<section id="partners" data-aos="fade-down">
		<div class="container clientLogo2">
			<div class="row title-bar">
				<div class="col-md-12">
					<h2>Technology Platform Partners</h2>
					<div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
					<?php foreach ($consortium_list as $consrotium) : ?>
					<div class="col-sm-12"><a href="<?php echo $consrotium['img_link'] ?>" target="_blank"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

<section class="ministrylogo">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center title-bar">
				<h4>Under Aegis of</h4>
				<div class="heading-border"></div>
				<a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
			</div>
		</div>
	</div>
</section>