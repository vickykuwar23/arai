<script type="text/javascript">
	$('.select2_common').select2();
	
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", html: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", html: msg, type: "error" }); }
	function sweet_alert_warning(msg) { swal({ title: "Warning", html: msg, type: "warning" }); }
</script>

<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('warning')) { ?><script>sweet_alert_warning("<?php echo $this->session->flashdata('warning'); ?>"); </script><?php } ?>
