<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
textarea {
        resize: none; padding:10px 0;
    }
	
	#toolsModal .modal-dialog {
    top: 10%;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header" style="text-align: center;display: block;">
		<img src="<?php echo base_url('assets/front/img/logo1.png'); ?>" alt="logo" style="width: 40%;">
		<h5 style="display: block;clear: both;width: 100%;background: #e23751;color: #FFF; margin-top:10px; font-size:16px;border-radius: 5px;" class="modal-title" id="SubscriptionModalLabel">Need access to Tools, let us know !</h5>
		<button style="margin-top: -100px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="toolsForm_res" name="toolsForm_res" role="form">
		
		<div class="pl-3 pr-3 pb-3">
			<div class="form-group">
				<!--<input type="text" name="requirements" id="requirements" class="form-control" value=""  required="" />-->
				<textarea rows="1" cols="12" name="requirements" id="requirements" class="form-control"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Your Requirement <em>*</em></label>
			</div>			
			<div class="form-group">
				<!--<input type="text" name="purpose" id="purpose" class="form-control" value=""  required="" />-->
				<textarea rows="1" cols="12" name="purpose" id="purpose" class="form-control"  required=""></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Purpose <em>*</em></label>
			</div>
			<div class="form-group">
				<!--<input type="text" name="tools_required" id="tools_required" class="form-control" value=""  />-->
				<textarea rows="2" cols="12" name="tools_required" id="tools_required"   class="form-control" ></textarea>
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Tools/Facilities required </label>
			</div>
			<div class="form-group">				
				<textarea rows="2" cols="12" name="tools_remark" id="tools_remark"  class="form-control"></textarea>					
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Remarks (If any)</label>
			</div>

		<div class="modal-footer" style="border-top:none; padding:0;">
			<button type="submit" class="btn btn-primary" id="email_subscription_btn_submit">Submit</button>
		</div>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>



<script>

	// Word Count Validation
	function getWordCount(wordString) {
		var words = wordString.split(" ");
		words = words.filter(function(words) { 
			return words.length > 0
		}).length;
		return words;
	}
	$(document).ready(function () {

		// Word Count Validation
		/*function getWordCount(wordString) {
			var words = wordString.split(" ");
			words = words.filter(function(words) { 
				return words.length > 0
			}).length;
			return words;
		}*/
	
		jQuery.validator.addMethod("minCount",
		function(value, element, params) {
			var count = getWordCount(value);
			if(count >= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Minimum {0} Words Required")
		);
		
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
			var count = getWordCount(value);
			//console.log(count);
			if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("Maximum {0} words are allowed."));
		
		$('#toolsForm_res').on("keyup keypress", function(e) {
			var code = e.keyCode || e.which; 
			if (code === 13) {               
				e.preventDefault();
				return false;
			}
		});
	
		$("#toolsForm_res" ).validate({
			//onfocusout: false,
			/*load: true,
			blur: true,
			change: true,
			keypress: true,
			keyup: true,
			keydown: true,
			onclick: true,*/
			onkeyup: false,
			onclick: false,
			/* debug: false, */
			rules: {
				requirements: {
					required: true,
					minCount:['1'],
					maxCount:['100']	
				},
				purpose: {
					required: true,
					minCount:['1'],
					maxCount:['100']	
				},
				tools_required: {	
					maxCount:['100']	
				},
				tools_remark: {	
					maxCount:['100']	
				}
			},
			messages: {
				requirements: {
					required: "This field is required"
				},
				purpose: {
					required: "This field is required"
				},
				tools_required: {
					maxCount: "Maximum 100 words are allowed."
				},
				tools_remark: {
					maxCount: "Maximum 100 words are allowed."
				}
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				
				$("#preloader-loader").css("display", "block");
				var requirements  	= $('#requirements').val();
				var purpose 		= $('#purpose').val();
				var tools_required 	= $('#tools_required').val();
				var tools_remark 	= $('#tools_remark').val();
				$.ajax({
					url: "<?php echo base_url('home/open_tool_form_insert'); ?>", 
					type: "POST",             
					//data: { "email" : email },
					data:$('#toolsForm_res').serialize(),
					dataType: 'JSON',
					cache: false,
					success: function(response) 
					{
						$("#toolsModal").modal('hide');
						$("#preloader-loader").css("display", "none");
						swal({
							title: response.flag,
							text: response.message,
							icon: response.flag,
							}).then(function() {
							//location.reload();
						});
					}
				});
				return false;
			}
		});
	
	}); 
</script>