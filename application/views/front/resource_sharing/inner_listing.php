<?php $getTotalLikes = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $res['r_id']), 'like_id'); 
$read_more_url=base_url('resource_sharing/details/').base64_encode($res['r_id']) ;
$read_more='<a href='.$read_more_url.' >read more</a>';
?>
<div class="<?php echo $col_class; ?>">
   <div class="boxShadow75 custom_box_class">
      <a style="color: #000;cursor: pointer;" href="<?php echo $read_more_url ?>">
      <h4 class="text-center" title="<?php echo $res['resouce_name']; ?>"><?php if(strlen($res['resouce_name']) > 20 ){ echo ucfirst(substr($res['resouce_name'], 0,20) )."..."; } else {echo ucfirst($res['resouce_name']);} ?></h4>
   </a>
      <img class="img-fluid" src="<?php echo base_url('uploads/resource_banner_img/').$res['banner_img'] ?>" alt=" img" style="cursor: pointer;" onclick="window.open('<?php echo $read_more_url ?>','_self')">
      <ul style="text-align: center">
         <li class="id_disp"><a href="<?php echo $read_more_url ?>" style="cursor:pointer; background: #FFF; color: #c80032;"><i class="fa fa-ticket" aria-hidden="true"></i> <?php echo $res['id_disp']; ?></a> 
         </li>
      </ul>
      <ul>
         <li style="cursor:auto" id="like_count_outer_<?php echo $res['r_id']; ?>"><span style="cursor:auto; display:inline-block; text-align: center; background: #FFF; color: #c80032; padding: 3px;">  <?php echo $getTotalLikes ?> </span></li>
         <?php if(in_array($res['r_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
            else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
         <li id="like_unlike_btn_outer_<?php echo $res['r_id']; ?>">
            <span style="cursor:pointer;" onclick="like_unlike('<?php echo base64_encode($res['r_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
         </li>
         <li class="share_li float-right">
            <span style="cursor:pointer;" onclick="share_blog('<?php echo site_url('resource_sharing/details/'.base64_encode($res['r_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
         </li>
      </ul>
      <ul>
         <li style="color: #000; font-weight: bold;"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php if($res['city_name']=="Other"){ echo $res['location_other'] ;}else{ echo $res['city_name'];} ?></li>
         
      </ul>
      <div class="mt-3 mb-3 ">
         <p class="mb-0 text-justify res_details">
            <?php 
               $res_details=trim($res['resource_details']) ;
               
               if(strlen($res_details) > 100 ){ 
                     $srt_to_show= ucfirst(substr($res_details, 0,100) )."...";
                     echo wordwrap($srt_to_show, 37, "\n", true).$read_more;
                   } else {echo ucfirst($res_details);} ?>
         </p>
      </div>
      <?php if($res['DispTags'] != "" || $res['tag_other']!='') 
         { ?>
      <div >
         <strong>Tags:</strong> 
         <div class="owl-carousel mb-2 owl-theme tagSlider">
            <?php if($res['DispTags'] != "") 
               { 
                   $tags_arr = explode("##",$res['DispTags']); 
                    foreach($tags_arr as $tag)
                           {   ?>
            <div><a style="cursor:auto;"><?php if(strlen($tag) > 20 ) {echo substr($tag,0, 20)."..." ; }else{ echo $tag; }; ?></a></div>
            <?php } }   ?>
            <?php if ($res['DispTags'] == "" && $res['tag_other']!='') {
               $style='cursor:auto;min-width:250px ';
               }else{
               $style='cursor:auto;  ';
               } ?>
            <?php if ($res['tag_other']!=''){ 
               $tag_other_arr=explode(',', $res['tag_other']);
               foreach ($tag_other_arr as $key => $tag_other) {
                 ?>
            <div><a style="<?php echo $style ?>">
               <?php 
                  if(strlen($tag_other) > 20 ) {echo substr($tag_other,0, 20)."..." ; }else{ echo $tag_other; }; ?>
               </a>
            </div>
            <?php  } } ?>
         </div>
      </div>
      <?php } ?>
      <a href="<?php echo base_url('resource_sharing/details/').base64_encode($res['r_id'])  ?>" class="btn btn-primary2">View Detail</a>
      <?php
         $date=date("d M, Y", strtotime( $res['created_on']));
         if(in_array($res['r_id'], $BlogActiondata['reported_blog'])) { $Reported_label = "Request Already Sent"; $ReportedFlag = '0';  }
           else { $Reported_label = "Report Blog"; $ReportedFlag = '1'; } ?>
      <span id="report_blog_btn_outer_<?php echo $res['r_id']; ?>"> 
      <?php if( $this->session->userdata('user_id')!= $res['user_id']){ ?>  
      <button type="submit" class="btn btn-primary float-right" style="margin-top:0px !important;"  onclick="connect_resource('<?php echo base64_encode($res['r_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $res['id_disp']; ?>', '<?php echo $res['resouce_name']; ?>' ,  '<?php echo $login_user_name; ?>', '<?php echo $login_user_email; ?>','<?php echo $date; ?>')" title="<?php echo $Reported_label; ?>">Connect</button>
      <?php } ?>
      </span>
   </div>
</div>

