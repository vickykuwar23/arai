<!-- DataTables -->
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>	
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<style>
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	
	.MyTeamsListingTab ul li:before { display:none; }
</style>
<div id="preloader-loader" style="display:none;"></div>	
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">My resource Applications</h1>        
	</div>
</div>

<section id="registration-form" class="inner-page">
	<div class="container">
		<div class="row">			
			<div class="col-md-12">
				<div class="formInfo">
					
					<section>
						<div class="product_details MyTeamsListingTab">


							<ul class="nav nav-tabs align-item-center justify-content-center" id="myTab" role="tablist">
								<li class="nav-item"><a href="<?php echo base_url('resource_sharing/my_resource'); //#TeamTab1 ?>"    class="nav-link " >My Resources</a></li>
								<li class="nav-item"><a href="javascript:void(0)" class="nav-link active" >My Resource Applications</a></li>
							</ul>
							<!-- <ul class="nav nav-tabs align-item-center justify-content-center d-none" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link active" href="javascript:void(0)">My Webinar's</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo base_url('webinar/appliedWebinar'); ?>">Webinar's I have Applied To</a></li>
							</ul> -->
							
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="TeamTab1">
									
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
												<div class="table-responsive">
													<table id="blog-list" class="table table-bordered table-hover challenges-listing-page">
														<thead>
															<tr>
																<th scope="col">Sr. No</th>
																<th scope="col">Application ID</th>
																<th scope="col">Resource ID</th>
																<th scope="col">Resource Name</th>
																<th scope="col">Purpose</th>
																<th scope="col">Submit Date</th>
															</tr>
														</thead>
														<tbody>	
															
														<?php foreach ($applications as $key => $value) { ?>
															<tr>
															<td><?php echo$key+1 ?></td>	
															<td><?php echo $value['id_disp'] ?></td>
															<td><?php echo $value['app_id'] ?></td>
															<td><?php echo $value['resouce_name'] ?></td>
															<td><?php echo $value['comments'] ?></td>	
															<td><?php echo date("d M, Y h:i a", strtotime($value['created_on'])) ?>
															</td>
															</tr>	
														<?php } ?>
																			
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="BlogLikes" tabindex="-1" aria-labelledby="BlogLikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogLikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogLikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>	
<script>
  $(document).ready( function () 
	{	  
		var base_path = '<?php echo base_url() ?>';
		var table = $('#blog-list').DataTable({
			"ordering":false,
			"searching": false,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
		});
	});
	
	
	
	function delete_resource(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected resource sharing?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("resource_sharing/delete_resource"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$(".token").val(data.token);
						
						$('#blog-list').DataTable().ajax.reload();	
						swal(
						{
							title: 'Success!',
							text: "Resource sharing successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	
	
	
	$(document).ajaxStart(function() { $("#preloader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader").css("display", "none"); });
</script>