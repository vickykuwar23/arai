<?php $encObj =  New Opensslencryptdecrypt(); ?>

		<div class="">
                    <div class="boxShadow75 shareKR">
                        <img src="<?php echo base_url('uploads/kr_banner/').$res['kr_banner'] ?>" alt=" img">
                        <ul>

					            	<?php if(in_array($res['kr_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
									else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
									<li id="like_unlike_btn_outer_<?php echo $res['kr_id']; ?>">
										<span onclick="like_unlike_blog('<?php echo base64_encode($res['kr_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
									</li>
                                        
                                        <li ><a href="javascript:void(0)" style="cursor:auto"><i class="fa fa-ticket" aria-hidden="true"style="cursor:auto"></i> </a> <span style="cursor:auto"><?php echo $res['kr_id_disp']; ?></span></li>
                                   <li>
									<span onclick="share_blog('<?php echo site_url('knowledge_repository/details/'.base64_encode($res['kr_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
									</li> 
                    
                                    </ul>
                        <p><?php echo ucfirst($res['title_of_the_content']); ?></p>

                        <div class="bgNewBox65 mt-3 mb-3">
                            <p class="mb-0">Provided by: <strong><?php echo $res['author_name']; ?></strong></p>
                            <p class="mb-0">Type: <strong><?php echo $res['type_name']; ?></strong></p>
                            <p class="mb-0">Published Date : <strong><?php echo date("d-m-Y", strtotime($res['created_on'])); ?></strong></p>
                        </div>

			        <?php if($res['DispTags'] != "") 
						{ 
						$tags_arr = explode("##",$res['DispTags']); ?>
						<div class="tag2">
							<ul>
								<li style='font-weight:600'>Tags:</li> 
								<?php foreach($tags_arr as $tag)
									{	?>
									<li><span style="cursor:auto"><?php echo $tag; ?></span></li> 
								<?php }	?>
							</ul>
						</div>
					<?php } ?>



                          <a href="<?php echo base_url('knowledge_repository/details/').base64_encode($res['kr_id'])  ?>"  class="btn btn-primary2">Read More </a>

                        <button  class="btn btn-primary float-right" onclick="open_download_modal('<?php echo $res['kr_id'] ?>')">Download</button>
                    </div>
             </div>
