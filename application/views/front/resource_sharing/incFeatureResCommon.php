<?php
if (count($featured_kr) > 0) {
    ?>
         <div class="owl-carousel mb-2 owl-theme resourceSharingSlider">

            <?php foreach ($featured_kr as $res) {
        $data['res']       = $res;
        $data['col_class'] = '';
        $this->load->view('front/resource_sharing/inner_listing', $data);

    }?>
        </div>

    <script>

 $('.KnowledgeRepositorySlider').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: false,
        nav: true,
        dots: false,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }

        });

    </script>

    <script>
        (function($){
        $(window).on("load",function(){
            $(".descriptionBox45").mCustomScrollbar({
                theme: "rounded-dark",
                scrollInertia: 0,
                mouseWheelPixels: 30,
                autoDraggerLength:false
            });
        });
    })(jQuery);
    </script>





<script>
    // owl crousel testimonial section
        $('.resourceSharingSlider').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: false,
        nav: true,
        dots: false,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }

        });
        </script>





    <script>
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    </script>
<?php }?>