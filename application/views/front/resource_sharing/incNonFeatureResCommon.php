<?php if (count($non_featured_kr) > 0) {
    ?>
        <div class="row">
        <?php
foreach ($non_featured_kr as $res) {
        $data['res'] = $res;
        $data['col_class'] = 'col-md-4';
        $this->load->view('front/resource_sharing/inner_listing', $data);
    }?>
        </div>
        <?php
if ($new_start < $total_non_featured_kr_count) {?><br>
        <div class="col-md-12 ButtonAllWebinars text-center" id="showMoreBtn">
            <a href="javascript:void(0)" class="click-more" onclick="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', '<?php echo $new_start; ?>', '<?php echo $non_featured_limit; ?>', 0, 1)">Show More</a>
        </div>
        <?php }
} else {
    if ($featured_kr_total_cnt == 0) {?>
    <div class="col-md-12 text-center"><p style="font-weight:bold; text-align: center;"><br>No Records Available</p></div>
<?php }
}?>
<script>

    $('.tagSlider').owlCarousel({
        items: 1,
        autoplay: false,
        smartSpeed: 700,
        autoWidth:true,
        loop: true,
        nav: true,
        dots: false,
        margin:15,
        navText: ["", ""],
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 3
            }
        }

        });

</script>