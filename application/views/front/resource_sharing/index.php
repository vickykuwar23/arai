<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<?php $encrptopenssl =  New Opensslencryptdecrypt(); ?>
<style>
.res_details {
    min-height: 70px;
}
.scroll_details{ overflow-y: scroll; height: 80px; }
.scroll_tags{overflow-y: scroll; height: 80px; }

.custom_box_class .img-fluid {height: 240px; margin: 0 auto; display: block;}
	#preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
	#preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
	border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
	


.formInfoNew img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2); display: block;}


/* .WebinarsSlider .owl-item{ padding:0 15px;  box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); } */
.borderBoxNewClass h3{ font-size: 16px; text-align: center; margin:0 0 10px 0;}
.borderBoxNewClass ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;}
.borderBoxNewClass ul li{ padding: 0; margin: 0; display: inline-block;}
.borderBoxNewClass{background:#fff;padding:10px;margin:-45px 0 0 0}
.boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}

.boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}

/* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
.product_details{padding: 20px 0 0 0;}


/** Blog**/

.bs-example2{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px;min-height:400px}
.bs-example2 h3{color:#fff;font-size:22px;font-weight:800}
.textInfoAbout2{margin-top:-195px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15);overflow:auto;padding:25px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.textInfoAbout3{margin-top:-50px!important;background:#FFF;position:relative;box-shadow:.3s ease,border .3s;box-shadow:0 0 5px 0 rgba(46,61,73,.15); padding:15px;width:95%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.bs-example2 .buttonNew{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#000;background:#32f0ff;border:1px #32f0ff solid;margin-top:10px}

.bs-example2 .buttonNew:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}

.bs-example2 .buttonNew2{text-align:center;display:block;padding:10px 15px;text-decoration:none;color:#32f0ff;background:#fff;border:1px #32f0ff solid;margin-top:10px}

.bs-example2 .buttonNew2:hover{color:#32f0ff;background:0 0;border:1px #32f0ff solid;text-decoration:none}
.borderBoxNewClass2 ul{ list-style: none; padding: 0; margin: 0; text-align: center; font-size: 14px;color: #32f0ff;}
.borderBoxNewClass2 ul li{ padding: 0; margin: 0; display: inline-block;}

.FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}


.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag ul li a:hover{ background: #000; color: #32f0ff; }


.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.shareList ul li a:hover{ background: #000; color: #32f0ff; }

.userPhoto-info h3{ font-size: 19px !important; }
.userPhoto-info img{ border-radius: 50%;}
.postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
.postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
.descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
.descriptionBox45 p{color: #FFF;}
.descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
.descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}

.descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
.bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
.bs-example45 h3{color:#fff;font-size:19px;font-weight:800}




.formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}

.formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
.formInfo65 p a{ text-decoration: none; font-weight: 800; color: #000;}
.formInfo65 p a:hover{ text-decoration: none; font-weight: 800; color: #32f0ff;}





/** Color Change**/
.nav-tabs{border-bottom:none}
.product_details .nav-tabs .nav-item.show .nav-link,.product_details .nav-tabs .nav-link.active{color:#FFF;background-color:#c80032 ;border-color:#c80032}
/* .product_details .nav-tabs .nav-link:focus, .product_details .nav-tabs .nav-link:hover {border-color: #96c832;} */
/* .title-bar .heading-border{background-color:#c80032} */
.search-sec2{padding:1rem;background:#c80032;width:100%;margin-bottom:25px}
.filterBox2 .btn-primary{color:#96c832;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
.filterBox2 .btn-primary:hover{color:#000;background-color:#96c832;border-color:#000}

/**Questions And Answers Forum**/
.userPhoto-info h3 span {font-size: 14px !important; color: #a3a3a3; text-transform: initial; }
#AnswersForum h2{font-size:1.75rem}
.bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}

.listViewinfo h3 {font-size: 24px !important;}
.listViewinfo h3 span {float: right;}
.listViewinfo h3 span a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
.listViewinfo h3 span a:hover {background: #000; color: #32f0ff; text-align: center; }


.listViewinfo img {border-radius: 50%;}
.imgBoxView img {border-radius: 0%;}
.listViewinfo ul{ list-style: none; padding: 0; margin: 0; font-size: 15px;}
.listViewinfo ul li{ list-style: none; padding: 0; margin: 0; color: #333; font-weight: 800;}
.listViewinfo ul li span{font-weight: normal; color: #000;}

.likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
.likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
.likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
.likeShare ul li:last-child{ float: right; margin: 0;}
.likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
.likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
.dropdown-menu{ padding: 0px !important;}
.dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
.dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }


.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}


.imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}




/**Blog Icon Added Css**/
.blogProfileicon{ position: absolute; top:8px; right:25px; width: 45px; height: 45px; border-radius:5px; background:#32f0ff;  padding: 5px; text-align: center;}
.blogProfileicon img{ width: 50%;}



/** Knowledge Repository Slider **/

#flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
#flotingButton2 a{color:#FFF;background:#c80032;padding:10px 15px; border:solid 1px #c80032; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
#flotingButton2 a:hover{color:#c80032;background:#FFF;border:solid 1px #c80032;text-decoration:none}
a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 

#flotingButton3{position:fixed;top:45%;left:5px;z-index:999;transition:all .5s ease}
#flotingButton3 a{color:#FFF;background:#c80032;padding:10px 15px; border:solid 1px #c80032; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
#flotingButton3 a:hover{color:#c80032;background:#FFF;border:solid 1px #c80032;text-decoration:none}


	
.shareKR ul li span{  background: #32f0ff; color: #000; text-decoration: none;  cursor:pointer; }
.shareKR ul li span:hover{ background: #000; color: #32f0ff; }
.ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
.ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}


</style>



<style>
        
/** Resource SharingSlider Slider **/
.title-bar .heading-border {background-color: #c80032;}
.bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
.resourceSharingSlider.owl-carousel .owl-dot,.resourceSharingSlider.owl-carousel .owl-nav .owl-next,.resourceSharingSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
.resourceSharingSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
.resourceSharingSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
.resourceSharingSlider .owl-nav.disabled{display:block}
.resourceSharingSlider .owl-nav{position:absolute; bottom:-15px; width:100%}


.resourceSharingSlider .owl-nav{position:absolute; bottom:55%; width:100%}

/* .resourceSharingSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #c80032;color:#FFF} */

.resourceSharingSlider .owl-nav .owl-prev{left:-30px; position: absolute; background:#fff;color:#c80032; border: solid 1px #c80032;}
.resourceSharingSlider .owl-nav .owl-next{right:-30px; position: absolute; background:#c80032;color:#FFF; border: solid 1px #c80032;}

/* .resourceSharingSlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #96c832;color:#96c832} */

.resourceSharingSlider .owl-item{ padding:10px;}
.resourceSharingSlider .owl-item img{width:100%; height: 250px; }
.resourceSharingSlider .owl-dots{position:absolute;width:100%;bottom:25px}


.boxShadow75{margin:0 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px; }
.boxShadow75:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
.boxShadow75 ul{ padding: 0; margin:13px 0; list-style: none;}


.boxShadow75 ul li{ padding: 0; margin:0 5px 10px 0; display: inline-block; color: #64707b; font-size: 15px; position: relative;}
.boxShadow75 ul li span{ background: #FFF; color: #c80032; padding: 5px; border:solid 1px #c80032; border-radius: 5px;font-size: 13px !important}


.boxShadow75 ul li span:hover{ background: #c80032; color: #FFF; padding: 5px; border:solid 1px #c80032; border-radius: 5px;}
.boxShadow75 ul li a {background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; border-radius: 5px; padding: 5px;font-size: 13px !important}
.boxShadow75 ul li a:hover { background: #c80032; color: #FFF;}

.tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; }
.tag2 ul li:first-child{ display:block;}
.tag2 ul li{padding: 0; margin:0 5px 10px 0 !important;}
.tag2 ul li a{  background: #96c832; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
.tag2 ul li a:hover{ background: #000; color: #96c832; }

.boxShadow75 .btn-primary {color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px}
.boxShadow75 .btn-primary:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none;}


.boxShadow75 .btn-primary2 {color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px}
.boxShadow75 .btn-primary2:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none;}

/* .boxShadow75 .btn-primary2{color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary2:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none; }

.boxShadow75 .btn-primary{color: #000; background: #96c832; padding: .375rem .75rem; border: solid 1px #96c832; text-decoration: none; text-align: center; font-size: 14px;}
.boxShadow75 .btn-primary:hover {color: #96c832; background: #000; border: solid 1px #000; text-decoration: none; } */

.ButtonAllWebinars a{ background: #c80032; color: #FFF;  border: solid 1px #c80032; text-decoration: none; padding:10px; border-radius:.25rem}
.ButtonAllWebinars a:hover{ background: #FFF; color: #c80032; text-decoration: none;}


	/* .clearAll {background: #000 !important; color: #96c832 !important; border-color: #96c832 !important; margin-right: 15px; }
	.clearAll:hover {background: #96c832 !important; color: #000 !important; border-color: #000 !important;}
	.filterBox .btn-primary {color: #96c832; background-color: #000; border-color: #000; width: 100%; display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}
	.filterBox .btn-primary:hover {color: #000; background-color: #96c832; border-color: #000; } */
	.filterBox .btn-primary {color: #fff; background-color: #333; border-color: #333; width: 100%; display: block; font-weight: bold; padding: 10px 8px; border-radius: 0px;}

	.filterBox .btn-primary:hover {color: #FFF !important; background-color: #333 !important; border-color: #333 !important; }

	.custom_box_class {min-height: 600px !important;}
    </style>

<style>
	.tagSlider div a {background: #FFF; color: #c80032; border:solid 1px #c80032; display: block; text-align: center; text-decoration:  none; border-radius: 5px; padding:6px; font-size: 12px !important;font-weight: 700}
     .tagSlider a:hover {color: #c80032;}
     .tagSlider {width: 85%; margin: 0 auto; position: relative;}
                               
     .tagSlider.owl-carousel .owl-dot,.tagSlider.owl-carousel .owl-nav .owl-next,.tagSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
     .tagSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
     .tagSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
     .tagSlider .owl-nav.disabled{display:block}
     /*.tagSlider .owl-nav{position:absolute; bottom:-15px; width:100%}*/
    
    
.tagSlider .owl-nav {
    position: absolute;
    bottom: 69px;
    width: 100%;
    z-index: 99;
}
    
     .tagSlider .owl-nav [class*=owl-]:hover{background:#FFF!important;border:solid 1px #c80032;color:#c80032}
    
     .tagSlider .owl-nav .owl-prev{left:-35px;position:absolute;background:#fff;color:#c80032; }
     .tagSlider .owl-nav .owl-next{right:-36px;position:absolute;background:#fff;color:#c80032; }
    
    
     .tagSlider .owl-item{ padding:2px;}
     .tagSlider .owl-item img{width:100%; }
     .tagSlider .owl-dots{position:absolute;width:100%;bottom:25px}

     .tagSlider.owl-theme .owl-nav [class*="owl-"] {font-size: 12px; margin: 0; padding: 5px;display: inline-block; cursor: pointer; border-radius:0px;  border: solid 1px #c80032;}

	#popupTechTitle {font-size: 15px; color: #000;}
.modal-footer {justify-content: left; position: relative;}
.modal-open .modal .modal-dialog {top: 13%;}
.rightButton {position: absolute; right: 20px; top: auto;}

.home-p {padding: 40px 0}
.home-p h1 {line-height: 24px;}

.datepicker {background: #FFF !important; border-radius: 0;}
.float-right {float: right !important; margin-top: 6px !important;}

</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center" data-aos="fade-down">
	<div class="container searchBox">
		<h1 class="wow fadeInUp" data-wow-delay="0.1s">Resource Sharing</h1>
		<nav aria-label="breadcrumb" class="">
			<ol class="breadcrumb wow fadeInUp">
				<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">List of Resource Sharing</li>
			</ol>
		</nav>
	</div>
</div>

<section id="story" data-aos="fade-up">   
	<?php //$this->load->view('front/webinar/inc_technology_wall_tabs.php'); ?>
	
	<div class="tab-content active" id="myTabContent">
		<div class="tab-pane fade tab-pane active show fade pt-0" id="KnowledgeRepository">
			
			<!------------ START : BLOG SEARCH --------->
			<div class="filterBox">
				<section class="search-sec2">
					<div id="filterData" name="filterData">
						<div class="container">   
							<div class="row">
								<div class="col-lg-12">
									<div id="mySidenav" class="sidenav">
										<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span>Filter</span> <strong>&times;</strong></a>
										<div class="scroll">	
											<ul class="d-none">
												<li> <strong> Select Type</strong> </li>										
												<?php 
													$i=0;
													if(count($type_data) > 0) 
													{ 
														foreach($type_data as $res)
														{	
															if(strtolower($res['type_name']) != 'other')
															{ ?>										
															<li class="<?php if($i >= 5) { ?>filter_technology d-none<?php	} ?>">
																<input class="form-check-input search_technology" type="checkbox" value="<?php echo $res['id']; ?>" id="search_technologies" name="search_technologies[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
																<label class="form-check-label"><?php echo $res['type_name']; ?></label>
															</li>
															<?php $i++;
															}
														}
													}	?>
											</ul>
											<?php if($i > 5){ ?>
											<a href="javascript:void(0);" class="filter_load_more" onclick="show_more_technology('show')">Show More >> </a>
											<a href="javascript:void(0);" class="filter_load_less d-none" onclick="show_more_technology('hide')">Show Less >></a><br>
											<?php } ?>	
											
											<ul>
												<li> <strong> Select Tags</strong> </li>										
												<?php 
													$i=0;
													if(count($tag_data) > 0) 
													{ 
														foreach($tag_data as $res)
														{ ?>										
															<li class="<?php if($i >= 5) { ?>filter_tag d-none<?php	} ?>">
																<input class="form-check-input search_tag" type="checkbox" value="<?php echo $res['id']; ?>" id="search_tags" name="search_tags[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
																<label class="form-check-label"><?php echo $res['tag_name']; ?></label>
															</li>
															<?php $i++;															
														}
													}	?>
											</ul>
											<?php if(count($tag_data) > 5){ ?>
											<a href="javascript:void(0);" class="filter_load_more_tag" onclick="show_more_tag('show')">Show More >> </a>
											<a href="javascript:void(0);" class="filter_load_less_tag d-none" onclick="show_more_tag('hide')">Show Less >></a><br>
											<?php } ?>
											
											<ul class="">
												<li> <strong> Commercial terms</strong> </li>										
												<li class="">
													<input class="form-check-input search_resource_terms" type="checkbox" value="Free" id="search_resource_termss" name="search_resource_terms[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
													<label class="form-check-label">Free</label>
												</li>										
												<li class="">
													<input class="form-check-input search_resource_terms" type="checkbox" value="Paid" id="search_resource_termss" name="search_resource_terms[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
													<label class="form-check-label">Paid</label>
												</li>
                                                <li class="">
                                                    <input class="form-check-input search_resource_terms" type="checkbox" value="Contact for more details" id="search_resource_termss" name="search_resource_terms[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
                                                    <label class="form-check-label">Contact for more details</label>
                                                </li>
                                                
											</ul>
											
											<ul class="d-none">
												<li> <strong> Type Of Blogs</strong> </li>										
												<li class="">
													<input class="form-check-input search_resource_terms2" type="checkbox" value="0" id="search_resource_termss2" name="search_resource_terms2[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
													<label class="form-check-label">Organization</label>
												</li>										
												<li class="">
													<input class="form-check-input search_resource_terms2" type="checkbox" value="1" id="search_resource_termss2" name="search_resource_terms2[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
													<label class="form-check-label">Individual</label>
												</li>									
												<li class="">
													<input class="form-check-input search_resource_terms2" type="checkbox" value="11" id="search_resource_termss2" name="search_resource_terms2[]" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)">
													<label class="form-check-label">Experts</label>
												</li>
											</ul>
										</div>								
									</div>
									
									<div class="row d-flex justify-content-center search_Box">	

                                        <div class="col-md-2">
                                        <input type="text" class="form-control datepicker" name="from_date" id="from_date"  value="" placeholder="From Date" readonly />
                                        </div>  

                                        <div class="col-md-2">
                                        <input type="text" class="form-control datepicker" name="to_date" id="to_date"  value="" placeholder="To Date" readonly />
                                        </div>

										<div class="col-md-4">
											<input type="text" class="form-control search-slt" name="search_txt" id="search_txt" value="" placeholder="Enter Search Keyword" autocomplete="off" />	
											<button type="button" class="btn btn-primary searchButton" onclick="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0)"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
										<div class="clearAll">
											<a class="btn btn-primary clearAll" id="reset-val" href="javascript:void(0)" onclick="clear_search_form()">Clear All</a>
										</div>
										<?php if(count($tag_data) > 0) { ?>
											<div class="filter d-nonex">
												<button type="button" onClick="openNav()" class="btn btn-primary expert-search" id="expert-search"><i class="fa fa-filter" aria-hidden="true"></i>Filter</button>
											</div>
										<?php } ?>													
                                        <div class="col-md-2 d-none">
                                          <select name="" id="" class="form-control" onchange="getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0,0,this.value)">
                                              <option value="desc">Newest</option>
                                              <option value="asc">Oldest</option>
                                          </select>  
                                        </div>				
										<div class="col-md-12"></div>								
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!------------ END : BLOG SEARCH --------->
			

			<div class="container">	
				<div class="title-bar" id="FeaturedBlogSliderOuterTitle">
					<h2>Featured Resources</h2>
					<div class="heading-border"></div>
				</div>
				<div id="FeaturedBlogSliderOuter" class="mt-4"></div>
				 <div class="col-md-12">
                <div class="title-bar">
                   <h2>All Resources</h2>
                    <div class="heading-border"></div>
                </div>
    
            </div>
				<div id="NonFeaturedBlogOuter"></div>	
			</div>	


		</div>
	</div>
</section>

<div id="flotingButton2" class="d-none">
	<a href="<?php echo base_url('resource_sharing/add'); ?>" class="connect-to-expert"><i class="fa fa-video-camera" aria-hidden="true"></i> Post a Resource Sharing</a>
</div>

<div id="social-share">
                    <div class="social-open-menu">
                        <a class="btn-share"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    </div>
                    <ul class="social-itens hidden">
                          <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('resource_sharing/add'); ?>">Share / Post a Resource</a>
                        </li> 

                         <li>
                            <a class="btn-share social-item-4" href="<?php echo base_url('technology_transfer/add'); ?>">Share / Post Technology to Transfer </a>
                        </li> 
                        <li>
                            <a class="btn-share social-item-1" href="<?php echo base_url('knowledge_repository/add'); ?>">Share Knowledge </a>
                        </li>
                        <li>
                            <a class="btn-share social-item-2" href="<?php echo base_url('questions_answers_forum/add_qa'); ?>">Ask Question</a>
                        </li>
                        <li>
                            <a class="btn-share social-item-3" href="<?php echo base_url('blogs_technology_wall/add_blog'); ?>">Write a Blog</a>
                        </li>

                        <li>
                            <a class="btn-share social-item-5" href="<?php echo base_url('webinar/add'); ?>">Share / Post Webinar</a>
                        </li>
                    </ul>
</div>

<div id="flotingButton3">
	<a href="javascript:void(0)" class="connect-to-expert" onclick="open_tools_modal();"><i class="fa fa-video-camera" aria-hidden="true"></i> Connect With Us</a>
</div>
<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

<div class="modal fade" id="BlogDescriptionDetail" tabindex="-1" aria-labelledby="BlogDescriptionDetailLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BlogDescriptionDetailLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="BlogDescriptionDetailContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Share</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>					
				<span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="UserDetailModal" tabindex="-1" role="dialog" aria-labelledby="UserDetailModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" id="model_content_outer">
		<div class="modal-content" id="UserDetailModalContentOuter">			
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" id="download_model_content_outer">
	</div>
</div>
<!-- Modal -->

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BlogReportpopupLabel">Resource Sharing Request</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
				<div class="bgNewBox65">
				<p><strong>Resource ID :</strong> <span id="popupBlogId"></span><strong> Date: </strong> <span id="popupDate"></span> </p>
				<h5> <span id="popupBlogTitle"></span></h5>
				</div>
					
					<div class="form-group">
						<label for="popupBlogReportComment" class="">Purpose:(Optional) </label>
						<textarea class="form-control" id="popupBlogReportComment" name="popupBlogReportComment" required rows="6"></textarea>
						<span id="popupBlogReportComment_err" class='error'></span>
					</div>
				</div>	
				<input type="hidden" id="resource_id">
				<div class="bgNewBox65">
				<strong>Application:</strong>	
				<p><strong>Name:</strong> <span id="popupOwnerName"></span></p>
				<p><strong>Email:</strong> <span id="popupOwneMail"></span></p>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>			
					<button type="button" class="btn btn-primary rightButton" onclick="submit_blog_report()">Submit</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php $this->load->view('front/layouts/site_popups'); ?>
<?php if($this->session->flashdata('success_blog_del_msg')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success_blog_del_msg'); ?>"); </script><?php } ?>
<?php
 $action_flag = 0;  ?>
<script>
$(document).ready(function () { 

    $('.datepicker').datepicker({
       format: 'dd-mm-yyyy',
       //minDate: '0',
       //startDate:'+0d',
       autoclose: true
   });
 });

function open_connect_with_us_modal()
{
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('resource_sharing/open_tool_connect'); ?>",
		data: '',
		cache: false,
		success:function(data)
		{
			//alert(data)
			if(data == "error")
			{
				location.reload();
			}
			else
			{
				$("#model_tools_outer").html(data);
				$("#toolsModal").modal('show');
			}
		}
	});
}

	function show_more_technology(val)
	{
		if(val == 'show')
		{
			$( ".filter_technology" ).removeClass( "d-none" )
			$( ".filter_load_less" ).removeClass( "d-none" )
			$( ".filter_load_more" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_technology" ).addClass( "d-none" )
			$( ".filter_load_less" ).addClass( "d-none" )
			$( ".filter_load_more" ).removeClass( "d-none" )
		}
	}
	
	function show_more_tag(val)
	{
		if(val == 'show')
		{
			$( ".filter_tag" ).removeClass( "d-none" )
			$( ".filter_load_less_tag" ).removeClass( "d-none" )
			$( ".filter_load_more_tag" ).addClass( "d-none" )
		}
		else if(val == 'hide')
		{
			$( ".filter_tag" ).addClass( "d-none" )
			$( ".filter_load_less_tag" ).addClass( "d-none" )
			$( ".filter_load_more_tag" ).removeClass( "d-none" )
		}
	}
	
	$('#search_txt').keypress(function (e)
	{ 
		if (e.which == 13) 
		{ 
			getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0);
		} 
	});
	
	function clear_search_form() 
	{ 
        $("#to_date").attr("placeholder", "To date");
        $("#from_date").attr("placeholder", "From date");

        $("#from_date").val('');
        $("#to_date").val(''); 
		$("#search_txt").val('');  
		$('.search_technology:checkbox').prop('checked',false); 
		$('.search_tag:checkbox').prop('checked',false); 
		$('.search_resource_terms:checkbox').prop('checked',false); 
		$('.search_resource_terms2:checkbox').prop('checked',false); 
		
		getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0);
	}
	
	function copyToClipboardLink(containerid) 
	{
		var id = containerid;
		var el = document.getElementById(id);
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		document.execCommand('copy');
		
		$("#link_copy_msg").html('Link copied');
		
		$("#link_copy_msg").slideDown(function() {
			setTimeout(function() 
			{
				$("#link_copy_msg").slideUp();
				sel.removeRange(range);
			}, 1500);
		});
			
		//alert("Contents copied to clipboard.");
		return false;
	}
	
	//ON CLICK ON USER IMAGE, OPEN USER DETAIL POP UP
	function get_user_details(user_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'user_id':user_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('myteams/CommonUserDetailsAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$(".token").val(data.csrf_new_token)					
					$("#UserDetailModalContentOuter").html(data.response)
					$("#UserDetailModal").modal('show')
					$("#preloader-loader").hide();
				}
				else 
				{ 
					$("#preloader-loader").hide();
					sweet_alert_error("Error Occurred. Please try again."); 
				}
			}
		});	
	}
	
	//START : FEATURED & NON-FEATURED BLOG DATA
	function getFeaturedNonFeaturedResourceDataAjax(f_start, f_limit, nf_start, nf_limit, is_search, is_show_more,kr_type_id_load_more=false,order_by='')
	{
        
        $("#to_date").css('border-color','#88888');
        $("#from_date").css('border-color','#88888');
        $("#to_date").attr("placeholder", "To date");
        $("#from_date").attr("placeholder", "From date");

		$("#showMoreBtn").remove();
		var cs_t = 	$('.token').val();
		
		var selected_technology = [];
		$.each($("input.search_technology:checked"), function() { selected_technology.push($(this).val()); });
		
		var selected_tag = [];
		$.each($("input.search_tag:checked"), function() { selected_tag.push($(this).val()); });
		
		var selected_resource_terms = [];
		$.each($("input.search_resource_terms:checked"), function() { selected_resource_terms.push($(this).val()); });
		
		
		
		var keyword = encodeURIComponent($('#search_txt').val());
		var technology = encodeURIComponent(selected_technology);	
		var tag = encodeURIComponent(selected_tag);
		var resource_terms = encodeURIComponent(selected_resource_terms);
        var order_by = encodeURIComponent(order_by);
        var from_date = $("#from_date").val();
        var to_date  = $("#to_date").val();

        if (from_date!='') {
            if(to_date==''){
            $("#to_date").css('border-color','red');
            $("#to_date").attr("placeholder", "Select to date");
            return false;
            }
        }

        if (to_date!='') {

            if(from_date==''){
                $("#from_date").css('border-color','red');
                $("#from_date").attr("placeholder", "Select from date");
                return false 
            }

            var startDate = new Date($('#from_date').val().split("-").reverse().join("-") );
            var endDate = new Date($('#to_date').val().split("-").reverse().join("-"));

        

            if (  startDate > endDate  ){
                $("#to_date").css('border-color','red');
                $("#to_date").val('');
                $("#to_date").attr('placeholder','Invalid End Date');
                return false;
            }

        }
		
		parameters= { 'f_start':f_start, 'f_limit':f_limit, 'nf_start':nf_start, 'nf_limit':nf_limit, 'is_show_more':is_show_more, 'keyword':keyword, 'technology':technology, 'tag':tag, 'resource_terms':resource_terms,'cs_t':cs_t ,'kr_type_id_load_more':kr_type_id_load_more,'from_date':from_date,'to_date':to_date,'order_by': order_by}
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('resource_sharing/getFeaturedNonFeaturedResourceDataAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
				if(data.flag == "success")
				{
					if(is_search == '1') { $("#NonFeaturedBlogOuter").html(''); }
					
					$(".token").val(data.csrf_new_token)
					if(is_show_more == 0) 
					{ 
						if(data.Featured_response.trim() == "") { $("#FeaturedBlogSliderOuterTitle").css("display", "none"); } 
						else { $("#FeaturedBlogSliderOuterTitle").css("display", "block"); }
						
						$("#FeaturedBlogSliderOuter").html(data.Featured_response); 
					}
					
					$("#NonFeaturedBlogOuter").append(data.NonFeatured_response);
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
					
					
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 0, 0);
	//END : FEATURED & NON-FEATURED BLOG DATA
	
	function read_more_description(blog_id)
	{
		var cs_t = 	$('.token').val();
		parameters= { 'blog_id':blog_id, 'cs_t':cs_t }
		$("#preloader-loader").show();
		$.ajax( 
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/getread_more_descriptionAjax'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{
					$("#BlogDescriptionDetailLabel").html(data.blog_title);
					$("#BlogDescriptionDetailContent").html(data.blog_description);
					$("#BlogDescriptionDetail").modal('show');
				}else { sweet_alert_error("Error Occurred. Please try again."); }
				$("#preloader-loader").hide();
			}
		});
	}
	
	function delete_blog_from_user_listing(id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected blog?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var cs_t = 	$('.token').val();
				$.ajax(
				{
					type:'POST',
					url: '<?php echo site_url("blogs_technology_wall/delete_blog"); ?>',
					data:{ 'id':id, 'csrf_test_name':cs_t },
					dataType:"JSON",
					success:function(data)
					{
						$("#preloader").css("display", "none");
						$(".token").val(data.token);
						
						getFeaturedNonFeaturedResourceDataAjax(0, '<?php echo $featured_limit; ?>', 0, '<?php echo $non_featured_limit; ?>', 1, 0);
						
						swal(
						{
							title: 'Success!',
							text: "Blog successfully deleted.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
		});
	}
	
	function like_unlike(id, flag)
	{		
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("resource_sharing/like_unlike_ajax"); ?>',
			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$("#preloader").css("display", "none");
				$(".token").val(data.token);
				
				$("#like_unlike_btn_outer_"+data.r_id).html(data.response);
				$("#like_count_outer_"+data.r_id).html(data.total_likes_html);
			}
		});
	}
	
	function share_blog(url)
	{
		$("#BlogShareLink").html(url);
		$("#share-popup").modal('show');
	}
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
	
	function openNav() 
	{		
		var isMobile = false; //initiate as false
		// device detection
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			isMobile = true;
			document.getElementById("mySidenav").style.width = "100%";
			} else {
			document.getElementById("mySidenav").style.width = "25%";
		}
		
	}
	
	function closeNav() 
	{
		document.getElementById("mySidenav").style.width = "0";		
	}

	function open_download_modal(kr_id)
	{
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('resource_sharing/open_download_modal_ajax'); ?>",
			data: {'kr_id':kr_id},
			cache: false,
			success:function(data)
			{
				//alert(data)
				if(data == "error")
				{
					location.reload();
				}
				else
				{
					$("#download_model_content_outer").html(data);
					$("#DownloadModal").modal('show');
				}
			}
		});
	}

	function connect_resource(kr_id, ReportedFlag, blog_disp_id, blog_title,owner_name,owner_mail,date){
		<?php if($action_flag == 1) { ?>
			swal(
			{
				title: 'Alert!',
				html: "You can not send the request as the resource status is Pending or Blocked",
				type: 'warning'				
			});
		<?php }else { ?>
			if(ReportedFlag == "1")
			{
				$("#popupBlogId").html(blog_disp_id);
				$("#popupBlogTitle").html(blog_title);
				$("#resource_id").val(kr_id)
				$("#popupOwnerName").html(owner_name)
				$("#popupOwneMail").html(owner_mail)
				$("#popupDate").html(date);
				$("#popupBlogReportComment_err").html('');
				$("#BlogReportpopup").modal('show');
				//$("#popupBlogReportComment").val('');
				$("#popupBlogReportComment").focus();
			}
			else
			{
				swal(
				{
					title: 'Alert!',
					text: "You have already sent a request",
					type: 'alert',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
			}
		<?php } ?>
	}

	function check_report_validation()
	{
		return true;
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		if(popupBlogReportComment.trim() == "") { $("#popupBlogReportComment_err").html('Please enter the comment'); $("#popupBlogReportComment").focus(); }
		else { $("#popupBlogReportComment_err").html(''); }
	}


	function submit_blog_report()
	{	
		// check_report_validation();
		module_id=35;
		if(check_permissions_ajax(module_id) == false){
         	swal( 'Warning!','You don\'t have permission to access this page !','warning');
         	return false;
		}
		kr_id=$("#resource_id").val();
		
		var popupBlogReportComment = $("#popupBlogReportComment").val();
		
			$("#popupBlogReportComment_err").html('');
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to send the request for this resource?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("resource_sharing/connect_resource_ajax"); ?>',
						data:{ 'kr_id':kr_id, 'popupBlogReportComment':popupBlogReportComment, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#report_blog_btn_outer_"+data.kr_id).html(data.response);						
							$("#BlogReportpopup").modal('hide');
							
							swal(
							{
								title: 'Success!',
								text: "Your successfully submitted your request. Application ID: "+data.app_id,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
	}	


$(document).ready(function () { 

        $(".social-open-menu").click(function () {
            $(".social-itens").toggleClass("open");
            $(".social-itens").toggleClass("hidden");
        });
 });

</script>