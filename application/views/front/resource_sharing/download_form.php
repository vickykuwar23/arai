<?php 
// Create Object
$encrptopen_ssl =  New Opensslencryptdecrypt();
$subcription_data = $this->master_model->getRecords("newsletter", array("email_id" => $this->session->userdata('user_email')));
$subscrEmail = "";
if(count($subcription_data) > 0){
	
	$subscrEmail = $encrptopen_ssl->decrypt($this->session->userdata('user_email'));
	
} else {
	
	$subscrEmail = $encrptopen_ssl->decrypt($this->session->userdata('user_email'));
}

?>
<style>
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}

#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="SubscriptionModalLabel">Download Knowledge Repository</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<form method="post" id="downloadForm" name="downloadForm" role="form">
		
		<div class="p-3">
			<div class="form-group">
				<input type="hidden" name="" id="kr_id" value="<?php echo $form_data[0]['kr_id'] ?>">
				<input type="text" name="kr_id_disp" id="kr_id_disp" class="form-control" value="<?php echo $form_data[0]['kr_id_disp']; ?>"  readonly />
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">KR ID </label>
			</div>
			<div class="form-group">
				<input type="text" name="kr_title" id="kr_title" class="form-control" value="<?php echo $form_data[0]['title_of_the_content']; ?>"  readonly />
				<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">KR Title </label>
			</div>

			<div class="form-group">
				<div class="row">
			<label for="email" class="form-control-placeholder floatinglabel" style="position: absolute;left: 0;">Select Files To Download </label>
			</div>
			</div>
			<div class="form-group" style=" margin-bottom:0px;">
				<div class="row">
					<div class="col-md-1">
						<input type="checkbox" name="all_check_files" id="all_check_files"  class="all_check_files"  />
					</div>
					<div class="col-md-11 pl-0">
						<label for="email">Select All</label>
					</div>
				</div>
			</div>

			<?php 
			foreach ($files as $key => $value) { ?>
				
				<div class="form-group"  style=" margin:0px;">
				<div class="row">
					<div class="col-md-1">
						<input type="checkbox" name="files_to_download[]" id="files_to_download" value="<?php echo $value['file_id'] ?>" class="files_to_download"  />
					</div>
					<div class="col-md-11 pl-0">
						<label for="email" data-toggle="tooltip" data-placement="left" title="<?php echo $value['file_name'] ?>"><?php echo 'File '.($key+1) ?></label>
					</div>
				</div>
			</div>


		  	<?php	}
			?>
			<!-- <div class="form-group">
				
              	<span><i class="fa fa-file"></i><?php echo $files_count."files ".$files_size  ?></span>
			</div> -->

		   <div class="form-group row captcha"  style=" margin-top:0px;">
	                <label class="col-lg-5 pr-0 control-label text-left cptcha_img" style="transform:translateY(-0px) scale(1)">
	                  <span id="captImg"><?php echo $image; ?></span>
	                    
	                  </label>
	                <div class="col-lg-5">
	                  <input type="text"  style="margin-top:10px;" name="code" id="captcha" placeholder="" class="form-control" > 
	                  
	                </div>
	                <div class="col-lg-2">
	                  <a style="margin-left:5px; margin-top:10px; color: #fff;" class="btn btn-primary refreshCaptcha_dw"><i class="fa fa-refresh"></i></a> 
	                </div>
	        </div>
			
			
			

		<div class="modal-footer" style="border-top:none; padding:0; justify-content:center">
			<button type="submit" class="btn btn-primary" id="download_btn_submit">Download</button>
		</div>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		</div>
	</form>
</div>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">
	

$("#all_check_files").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
</script>

<script>
	$(document).ready(function () {
		/*$.validator.setDefaults({
			
		});*/
		
		
		

		
		
		$( "#downloadForm" ).validate({
			onfocusout: false,
			onkeyup: false,
			onclick: false,
			/* debug: false, */
			rules: {
			
			 code: {
               	required: true,
                	remote: {
                         url: "<?php echo base_url();?>knowledge_repository/check_captcha_ajax",
                         type: "post",
                         async:false,

               		},
             	},
             "files_to_download[]":{required:true},	
			},
			messages: {
				code: {
					required: "Please Enter Captcha",
					remote : "Invalid Captcha."
				},				
			},
			errorElement: 'span',
			   errorPlacement: function (error, element) {
				 error.addClass('invalid-feedback');
				 element.closest('.form-group').append(error);
			   },
			   highlight: function (element, errorClass, validClass) {
				 $(element).addClass('is-invalid');
			   },
			   unhighlight: function (element, errorClass, validClass) {
				 $(element).removeClass('is-invalid');
			   },
			submitHandler: function () {
				// $("#preloader-loader").css("display", "block");
				var selected_files = [];
				$.each($("input.files_to_download:checked"), function() { selected_files.push($(this).val()); });
				var arrStr = encodeURIComponent(selected_files);

				var kr_id = $('#kr_id').val();
				document.location =  "<?php echo base_url('knowledge_repository/download_kr/'); ?>"+arrStr+"/"+kr_id;
				window.setTimeout(function () {
				  window.location.reload();
				}, 2000);
				  // window.location.reload();

				// $.ajax({
				// 	url: "<?php echo base_url('knowledge_repository/download_kr'); ?>", 
				// 	type: "POST",             
				// 	data: { "kr_id" : kr_id },
				// 	dataType: 'JSON',
				// 	cache: false,
				// 	success: function(response) 
				// 	{
				// 		$("#SubscriptionModal").modal('hide');
				// 		$("#preloader-loader").css("display", "none");
				// 		swal({
				// 			title: response.flag,
				// 			text: response.message,
				// 			icon: response.flag,
				// 			}).then(function() {
				// 			//location.reload();
				// 		});
				// 	}
				// });

			}
		});
	
	}); 
</script>
<script>
         $(document).ready(function(){
             $('.refreshCaptcha_dw').on('click', function(){
                 $.get('<?php echo base_url().'login/refresh'; ?>', function(data){
                     $('#captImg').html(data);
                 });
             });
         });
</script>