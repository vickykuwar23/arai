<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url('assets/fancybox/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.min.css') ?>">
<style>
   #preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
   #preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
   border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}
   .boxShadow{margin:75px 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out}
   .boxShadow:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
   /* .formInfoNew{ border: solid 1px #32f0ff; padding: 15px;} */
   /** Blog**/
   .FiltersList label{position:relative;top:0;bottom:15px;left:0;width:100%;transition:.2s;color:#fff}
   .tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .tag ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag ul li a:hover{ background: #000; color: #32f0ff; }
   .shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .shareList ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .shareList ul li a:hover{ background: #000; color: #32f0ff; }
   .userPhoto-info h3{ font-size: 19px !important; }
   .userPhoto-info img{ border-radius: 50%;}
   .postInfo ul{ list-style: none; padding: 0; margin:0; display: flex; font-size: 14px; }
   .postInfo ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .descriptionBox45{padding:15px;border:1px solid rgb(107, 107, 107); height: 330px; }
   .descriptionBox45 p{color: #FFF;}
   .descriptionBox45 ul{color: #FFF; list-style: none; padding: 0; margin:0;  font-size: 14px;}
   .descriptionBox45 ul li{ float: left; padding: 0 0 0 20px; margin:0 0 5px 0; position: relative;}
   .descriptionBox45 li::before{position:absolute;left:0;font-family:FontAwesome;content:"\f00c";color:#FFF}
   .bs-example45{border-radius:6px;background:#333;text-align:left;position:relative;transition:box-shadow .3s ease,border .3s;	box-shadow: 0 6px 25px 0 rgba(0, 0, 0, 0.2);padding:15px; }
   .bs-example45 h3{color:#fff;font-size:19px;font-weight:800}
   .formInfo65{background:#fff;position:relative;box-shadow:0 6px 25px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out;border:1px solid #eee;overflow:auto;padding:25px;width:100%;margin:0 auto;z-index:999;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}
   .formInfo65:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
   .formInfo65 p a{ text-decoration: none; font-weight: 800; color: #000;}
   .formInfo65 p a:hover{ text-decoration: none; font-weight: 800; color: #32f0ff;}
   /** Color Change**/
   .nav-tabs{border-bottom:1px solid #32f0ff}
   .title-bar .heading-border{background-color:#c80032}
   .search-sec2{padding:1rem;background:#32f0ff;width:100%;margin-bottom:25px}
   .filterBox2 .btn-primary{color:#32f0ff;background-color:#000;border-color:#000;width:100%;display:block;padding:12px;border-radius:0}
   .filterBox2 .btn-primary:hover{color:#000;background-color:#32f0ff;border-color:#000}
   /**Questions And Answers Forum**/
   .userPhoto-info h3 span {font-size: 14px !important; color: #a3a3a3; text-transform: initial; }
   #AnswersForum h2{font-size:1.75rem}
   .bgNewBox{ background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;}
   .imgBoxView img {border-radius: 0%;}
   .likeShare{background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; overflow:hidden;}
   .likeShare ul{ list-style: none; padding: 0; margin:0; font-size: 15px;}
   .likeShare ul li{ list-style: none; float: left; padding: 0; margin:0 10px 0 0; font-weight: normal; color: #333; }
   .likeShare ul li:last-child{ float: right; margin: 0;}
   .likeShare ul li a {background: #32f0ff; border-radius: 5px; color: #000;  text-align: center; padding:5px 8px ; font-size: 15px; font-weight: bold;}
   .likeShare ul li a:hover {background: #000; color: #32f0ff; text-align: center; }
   .dropdown-menu{ padding: 0px !important;}
   .dropdown-menu a {background:none !important; border-radius: 0px !important; color: #000;  text-align: left !important; padding:5px 10px !important; font-size: 15px; font-weight: normal !important;}
   .dropdown-menu a:hover {background: #32f0ff !important; color: #000 !important;  }
   .bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
   .imgBoxView .img-fluid {max-width: 100%; width: 100%; height: auto;}
   /**Blog Icon Added Css**/
   .blogProfileicon{ position: absolute; top:8px; right:25px; width: 45px; height: 45px; border-radius:5px; background:#32f0ff;  padding: 5px; text-align: center;}
   .blogProfileicon img{ width: 50%;}
   .boxShadow75{margin:0 0 35px 0;box-shadow:0 6px 15px 0 rgba(0,0,0,.2);transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px;}
   .boxShadow75:hover{box-shadow:0px 1px 3px 0px rgba(0, 0, 0, 0.21)}
   .boxShadow75 ul{ padding: 0; margin:13px 0; list-style: none;}
   .boxShadow75 ul li{ padding: 0; margin: 0; display: inline-block; color: #64707b; font-size: 15px; position: relative;}
   .boxShadow75 ul li span{ background: #FFF;
            color: #c80032;
            border: solid 1px #c80032;
            text-decoration: none;
            border-radius: 5px;
            padding: 5px;}

   .boxShadow75 ul li span:hover{ background: #c80032;
color: #FFF;}
   .boxShadow75 ul li a {background: #FFF; color: #c80032; border: 1px solid #c80032; text-decoration: none; border-radius: 5px; padding: 5px;}
   .boxShadow75 ul li a:hover { background: #c80032; color: #FFF;}
   .tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .tag2 ul li{ float: left; padding: 0; margin:0 5px 0px 0 !important;}
   .tag2 ul li a{  background: #32f0ff; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag2 ul li a:hover{ background: #000; color: #32f0ff; }
   .boxShadow75 .btn-primary2{color: #32f0ff; background: #000; padding: .375rem .75rem; border: solid 1px #000; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary2:hover {color: #000; background: #32f0ff; border: solid 1px #32f0ff; text-decoration: none; }
   .boxShadow75 .btn-primary{color: #000; background: #32f0ff; padding: .375rem .75rem; border: solid 1px #32f0ff; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary:hover {color: #32f0ff; background: #000; border: solid 1px #000; text-decoration: none; }
   #flotingButton2{position:fixed;top:45%;right:5px;z-index:999;transition:all .5s ease}
   #flotingButton2 a{color:#000;background:#32f0ff;padding:10px 15px; border:solid 1px #000; text-decoration:none;display:block;text-align:center;border-radius:15px;font-size:14px}
   #flotingButton2 a:hover{color:#32f0ff;background:#000;border:solid 1px #32f0ff;text-decoration:none}
   a.filter_load_more, a.filter_load_less, a.filter_load_more_tag, a.filter_load_less_tag {background:none; font-size: 14px; float: right;} 
   .shareKR ul li span{  background: #32f0ff; color: #000; text-decoration: none;  cursor:pointer; }
   .shareKR ul li span:hover{ background: #000; color: #32f0ff; }
   .ButtonAllWebinars a{ background: #32f0ff; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
   .ButtonAllWebinars a:hover{ background: #000; color: #32f0ff; text-decoration: none;}
</style>
<style>
   /** Resource SharingSlider Slider **/
   /* .title-bar .heading-border {background-color: #96c832;} */
   .bgNewBox65 {background: #f8f8f8; padding: 15px; border: solid 1px #eeeded; -webkit-border-radius: 5px;	-moz-border-radius: 5px; border-radius: 5px;}
   .bgNewBox65 p strong{ font-weight: bold;}
   .gallerySlider.owl-carousel .owl-dot,.gallerySlider.owl-carousel .owl-nav .owl-next,.gallerySlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
   .gallerySlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
   .gallerySlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
   .gallerySlider .owl-nav.disabled{display:block}
   .gallerySlider .owl-nav{position:absolute; bottom:-15px; width:100%}
   .gallerySlider .owl-nav{position:absolute; bottom:-15px; width:100%}
   .gallerySlider .owl-nav [class*=owl-]:hover{background:#000!important;border:solid 1px #000;color:#c80032}
   .gallerySlider .owl-nav .owl-prev{background:#fff;color:#c80032; border: solid 1px #c80032;}
   .gallerySlider .owl-nav .owl-next{background:#fff;color:#c80032; border: solid 1px #c80032;}
   .gallerySlider .owl-nav [class*=owl-]:hover{background:#c80032 !important; border:solid 1px #c80032; color:#FFF}
   .gallerySlider .owl-item{ padding:10px;}
   .gallerySlider .owl-item img{width:100%; min-height:240px; height:240px; }
   .gallerySlider .owl-dots{position:absolute;width:100%;bottom:25px}
   .boxShadow75{margin:0 0 35px 0;box-shadow:none;transition:all .5s ease-in-out;-webkit-transition:all .3s ease-in-out; padding: 15px; border:solid 1px #f8f8f8;}
   .boxShadow75:hover{box-shadow:none;}
   .boxShadow75 ul{ padding: 0; margin:13px 0; list-style: none;}
   .boxShadow75 ul li{ padding: 0; margin: 0; display: inline-block; color: #64707b; font-size: 15px; position: relative;}
   /* .boxShadow75 ul li span{ background: #96c832; color: #000; padding: 5px; border-radius: 5px;}
   .boxShadow75 ul li a {background: #96c832; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .boxShadow75 ul li a:hover { background: #000; color: #96c832;} */
   .tag2 ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .tag2 ul li{ float: left; padding: 0; margin:0 5px 0px 0 !important;}
   .tag2 ul li a{  background: #96c832; color: #000; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag2 ul li a:hover{ background: #000; color: #96c832; }
   .tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
   .tag ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
   .tag ul li a{  background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; border-radius: 5px; padding: 5px;}
   .tag ul li a:hover{ background: #c80032; color: #FFF; }
   .boxShadow75 .btn-primary2{color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary2:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none; }
   .boxShadow75 .btn-primary{color: #FFF; background: #c80032; padding: .375rem .75rem; border: solid 1px #c80032; text-decoration: none; text-align: center; font-size: 14px;}
   .boxShadow75 .btn-primary:hover {color: #c80032; background: #FFF; border: solid 1px #c80032; text-decoration: none; }
   .btn-primary2{color: #96c832; background: #000; padding: .375rem .75rem; border: solid 1px #000; text-decoration: none; text-align: center; font-size: 14px;}
   .btn-primary2:hover {color: #000; background: #96c832; border: solid 1px #96c832; text-decoration: none; }
   .btn-primary{color: #000; background: #96c832; padding: .375rem .75rem; border: solid 1px #96c832; text-decoration: none; text-align: center; font-size: 14px;}
   .btn-primary:hover {color: #96c832; background: #000; border: solid 1px #000; text-decoration: none; }
   .ButtonAllWebinars a{ background: #96c832; color: #000; text-decoration: none; padding:10px; border-radius:.25rem}
   .ButtonAllWebinars a:hover{ background: #000; color: #96c832; text-decoration: none;}
   .textSmall{ font-weight: normal;}
   .textSmall a{ color: #96c832;}
   .textSmall a:hover{ color: #000;}
   .btn_outer {margin-top: -35px;}
   .btn_outer1 {margin-bottom: 40px;}
   #popupTechTitle {font-size: 15px; color: #000;}
   .modal-footer {justify-content: left; position: relative;}
   .modal-open .modal .modal-dialog {top: 13%;}
   .rightButton {position: absolute; right: 20px; top: auto;}
   .home-p {padding: 70px 0}
   .home-p h1 {line-height: 24px;}
   .mt_Top{margin-top:-65px !important; position: relative; border-radius: 5px; z-index: 99; background: #fff; margin-bottom:10px; box-shadow:none;}
     .mt_Top:hover{ box-shadow:none;}
</style>
<div id="preloader-loader" style="display:none;"></div>
<div id="home-p" class="home-p pages-head3 text-center">
   <!-- <div class="container">
      <h1 class="wow fadeInUp" data-wow-delay="0.1s"> Resource Sharing </h1>
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb wow fadeInUp">
            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Resource Sharing Details </li>
         </ol>
      </nav>
   </div> -->
</div>
<section id="story">
   <div class="container mt-4">
      <div class="boxShadow75 mt_Top">
         <div class="row">
            <div class="col-md-4">
               <img src="<?php echo base_url('uploads/resource_banner_img/').$form_data[0]['banner_img'] ?>" alt="img" class="img-fluid" style="max-width: 100%; height: 230px; width: 100%;">
            </div>
            <div class="col-md-8">
               <h4><?php echo ucfirst($form_data[0]['resouce_name']); ?></h4>
               <strong><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo ucfirst($form_data[0]['city_name']); ?></strong>
               <ul>
                  <li><a href="#" style="cursor:auto; background: #FFF; color: #c80032;"><i class="fa fa-ticket" aria-hidden="true"></i><?php echo ucfirst($form_data[0]['id_disp']); ?></a> </li>
                  
               </ul>
               <?php $getTotalLikes = $this->master_model->getRecordCount('arai_resource_sharing_likes', array('r_id' => $form_data[0]['r_id']), 'like_id');
               ?>
                <ul>
                  <li style="cursor:auto" id="like_count_outer_<?php echo $form_data[0]['r_id']; ?>"><span style="cursor:auto">  <?php echo $getTotalLikes ?> </span></li>
                  <?php if(in_array($form_data[0]['r_id'], $BlogActiondata['like_blog'])) { $Like_label = "Unlike"; $LikeFlag = '0'; $likeIcon = 'fa-thumbs-down';  }
                     else { $Like_label = "Like"; $LikeFlag = '1'; $likeIcon = 'fa-thumbs-up'; } ?>
                  <li id="like_unlike_btn_outer_<?php echo $form_data[0]['r_id']; ?>">
                     <span style="cursor:pointer;" onclick="like_unlike('<?php echo base64_encode($form_data[0]['r_id']); ?>','<?php echo $LikeFlag; ?>')"><i class="fa <?php echo $likeIcon; ?>" aria-hidden="true"></i> <?php echo $Like_label; ?></span>
                  </li>
                  <li class="share_li">
                     <span style="cursor:pointer;" onclick="share_blog('<?php echo site_url('resource_sharing/details/'.base64_encode($form_data[0]['r_id'])); ?>')"><i class="fa fa-share-alt-square" aria-hidden="true"></i> Share</span>
                  </li>
               </ul>
            </div>
            <div class="col-md-12">
               <ul>
                  <li class="float-right"><a href="javascript:void(0)" style="cursor:auto; background: #FFF; color: #c80032;"><i class="fa fa-calendar" aria-hidden="true"></i> Posted On: <?php echo date("jS F Y", strtotime( $form_data[0]['created_on'] )); ?></a>
                     </span>
                  </li>
               </ul>
            </div>
           </div>
          </div> 
       <div class="boxShadow75">
         <div class="row">
            <div class="col-md-12">
               <div class="">
                  <p><strong>Description:</strong> <?php echo ucfirst($form_data[0]['resource_details']); ?></p>
                  <p><strong>Specification:</strong> <?php echo ucfirst($form_data[0]['resource_specification']); ?></p>
                  <?php if ($form_data[0]['other_details']!='') { ?>
                  <p><strong>Other Details:</strong> <?php echo ucfirst($form_data[0]['other_details']); ?></p>
                  <?php } ?>
                  <p ><strong>Proposed/Offered by:</strong> <?php echo ucfirst($form_data[0]['owner_name']); ?></p>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <?php if (count($files)) { ?> 
               <div class="title-bar pt-0">
                  <h2>Gallery</h2>
                  <div class="heading-border"></div>
               </div>
               <div class="owl-carousel mb-2 mt-4 owl-theme gallerySlider">
                  <?php 
                     foreach ($files as $key => $value) { ?>
                  <div>
                     <div class="boxShadow75">
												<a href="<?php echo base_url('uploads/resource_files/'.$value['file_name']); ?>" data-fancybox="gallery_images" data-caption="<?php echo $this->Common_model_sm->convertValidStringForJs($res['question']); ?>">

                        <img src="<?php echo base_url('uploads/resource_files/').$value['file_name']; ?>" alt=" img">
                        </a>
                     </div>
                  </div>
                  <?php
                     } ?>
               </div>
               <?php } ?>
               <br>
               <div class="tag">
                  <?php if($form_data[0]['DispTags'] != "") 
                     { 
                     $tags_arr = explode("##",$form_data[0]['DispTags']); ?>
                  <ul>
                     <li><strong style="color:#000;">Tags:</strong></li>
                     <?php foreach($tags_arr as $tag)
                        {	?>
                     <li><a style="cursor:auto; background: #FFF; color: #c80032;"><?php echo $tag; ?></a></li>
                     <?php }	?>
                     <?php if ($form_data[0]['tag_other']!=''){ 
                        $tag_other_arr=explode(',', $form_data[0]['tag_other']);
                         foreach ($tag_other_arr as $key => $tag_other) {
                        ?>
                     <li><a style="cursor:auto;background: #FFF; color: #c80032; "><?php echo $tag_other; ?></a></li>
                     <?php } } ?>
                     <li>
                  </ul>
                  <?php } ?>
                  <?php if ($form_data[0]['web_url']!='') { ?>
                  <strong>Web URL: <span class="textSmall"> <a href="<?php echo $form_data[0]['web_url']; ?>" target="_blank"><?php echo $form_data[0]['web_url']; ?></a></span> </strong>
                  <?php } ?>
                  <br>
                  <strong>Commercial terms: <span class="textSmall"><?php echo ucfirst($form_data[0]['commercial_terms']); ?> <?php if($form_data[0]['commercial_terms']=='Paid'){echo $form_data[0]['currency']." ".$form_data[0]['cost_price'];} ?> </span></strong>
                  <?php if ($form_data[0]['terms_condition']!='') { ?>
                  <br>	
                  <strong>Terms & Conditions: <span class="textSmall"> <?php echo nl2br($form_data[0]['terms_condition']); ?></span> </strong>
                  <?php } ?>
                  <br>
                  <br>
                  <?php if( $this->session->userdata('user_id')== $form_data[0]['user_id']){ 
                     $btn_class='btn_outer1';
                     }else{
                     $btn_class='';
                     }
                     ?>
                  <button type="submit" class="btn btn-primary2 <?php echo $btn_class ?>" onclick="window.history.go(-1); return false;">Back</button>
                  <?php 
                     $date=date("d M, Y", strtotime( $form_data[0]['created_on']));
                     if(in_array($form_data[0]['r_id'], $BlogActiondata['reported_blog'])) { $Reported_label = "Request Already Sent"; $ReportedFlag = '0';  }
                     else { $Reported_label = "Connect"; $ReportedFlag = '1'; } 
                     ?>
                  <div id="report_blog_btn_outer_<?php echo $form_data[0]['r_id']; ?>" class="btn_outer">
                     <?php if( $this->session->userdata('user_id')!= $form_data[0]['user_id']){ ?>	
                     <button type="submit" class="btn btn-primary float-right" onclick="connect_resource('<?php echo base64_encode($form_data[0]['r_id']); ?>', '<?php echo $ReportedFlag; ?>', '<?php echo $form_data[0]['id_disp']; ?>', '<?php echo $form_data[0]['resouce_name']; ?>' , '<?php echo $login_user_name; ?>', '<?php echo $login_user_email; ?>','<?php echo $date; ?>')"  title="<?php echo $Reported_label; ?>">Connect</button>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Share</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
            <div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>
            <span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>				
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogReportpopupLabel">Resource Sharing Request</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div class="bgNewBox65">
                  <p><strong>Resource ID :</strong> <span id="popupBlogId"></span><strong> Date: </strong> <span id="popupDate"></span> </p>
                  <h5> <span id="popupBlogTitle"></span></h5>
               </div>
               <div class="form-group">
                  <label for="popupBlogReportComment" class="">Purpose:(Optional) </label>
                  <textarea class="form-control" id="popupBlogReportComment" name="popupBlogReportComment" required rows="6"></textarea>
                  <span id="popupBlogReportComment_err" class='error'></span>
               </div>
            </div>
            <div class="bgNewBox65">
               <strong>Application:</strong>	
               <p><strong>Name:</strong> <span id="popupOwnerName"></span></p>
               <p><strong>Email:</strong> <span id="popupOwneMail"></span></p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>			
               <button type="button" class="btn btn-primary rightButton" onclick="submit_blog_report('<?php echo base64_encode($form_data[0]['r_id']); ?>')">Submit</button>			
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="DownloadModal" tabindex="-1" role="dialog" aria-labelledby="DownloadModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document" id="download_model_content_outer">
   </div>
</div>
<!-- Modal -->
<?php $this->load->view('front/inc_sweet_alert_common'); ?>
<?php
   if($form_data[0]['admin_status'] == 1 && $form_data[0]['is_block'] == 0) { $action_flag = 0; }
   else { $action_flag = 1; } ?>
<script>
   function like_unlike(id, flag)
   {     
      var cs_t =  $('.token').val();
      $.ajax(
      {
         type:'POST',
         url: '<?php echo site_url("resource_sharing/like_unlike_ajax"); ?>',
         data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
         dataType:"JSON",
         success:function(data)
         {
            $("#preloader").css("display", "none");
            $(".token").val(data.token);
            
            $("#like_unlike_btn_outer_"+data.r_id).html(data.response);
            $("#like_count_outer_"+data.r_id).html(data.total_likes_html);
         }
      });
   }

   function open_download_modal(kr_id)
   {
   	$.ajax({
   		type: "POST",
   		url: "<?php echo site_url('knowledge_repository/open_download_modal_ajax'); ?>",
   		data: {'kr_id':kr_id},
   		cache: false,
   		success:function(data)
   		{
   			//alert(data)
   			if(data == "error")
   			{
   				location.reload();
   			}
   			else
   			{
   				$("#download_model_content_outer").html(data);
   				$("#DownloadModal").modal('show');
   			}
   		}
   	});
   }
   function delete_blog_from_user_listing(id)
   {
   	swal(
   	{  
   		title:"Confirm?",
   		text: "Are you sure you want to delete the selected blog?",
   		type: 'warning',
   		showCancelButton: true,
   		confirmButtonColor: '#3085d6',
   		cancelButtonColor: '#d33',
   		confirmButtonText: 'Yes!'
   	}).then(function (result) 
   	{ 
   		if (result.value) 
   		{
   			var cs_t = 	$('.token').val();
   			$.ajax(
   			{
   				type:'POST',
   				url: '<?php echo site_url("knowledge_repository/delete_blog"); ?>',
   				data:{ 'id':id, 'csrf_test_name':cs_t },
   				dataType:"JSON",
   				success:function(data)
   				{ 
   					window.location.replace("<?php echo site_url('knowledge_repository'); ?>");
   				}
   			});
   		} 
   	});		
   }	
   
   function connect_resource(kr_id, ReportedFlag, blog_disp_id, blog_title,owner_name,owner_mail,date)
   {
   	module_id=35;
   	if(check_permissions_ajax(module_id) == false){
           	swal( 'Warning!','You don\'t have permission to access this page !','warning');
           	return false;
   	}
   
   	<?php if($action_flag == 1) { ?>
   		swal(
   		{
   			title: 'Alert!',
   			html: "You can not send the request as the resource status is Pending or Blocked",
   			type: 'warning'				
   		});
   	<?php }else { ?>
   		if(ReportedFlag == "1")
   		{
   			$("#popupBlogId").html(blog_disp_id);
   			$("#popupBlogTitle").html(blog_title);
   			$("#resource_id").val(kr_id)
   			$("#popupOwnerName").html(owner_name)
   			$("#popupOwneMail").html(owner_mail)
   			$("#popupDate").html(date);
   			$("#popupBlogReportComment_err").html('');
   			$("#BlogReportpopup").modal('show');
   			$("#popupBlogReportComment").focus();
   			
   		}
   		else
   		{
   			swal(
   			{
   				title: 'Alert!',
   				text: "You have already sent a connect request",
   				type: 'alert',
   				showCancelButton: false,
   				confirmButtonText: 'Ok'
   			});
   		}
   	<?php } ?>
   }	
   
   function check_report_validation()
   {
   	var popupBlogReportComment = $("#popupBlogReportComment").val();
   	if(popupBlogReportComment.trim() == "") { $("#popupBlogReportComment_err").html('Please enter the comment'); $("#popupBlogReportComment").focus(); }
   	else { $("#popupBlogReportComment_err").html(''); }
   }
   
   function submit_blog_report(kr_id)
   {	
   	// check_report_validation();
   	
   	var popupBlogReportComment = $("#popupBlogReportComment").val();
   	
   		$("#popupBlogReportComment_err").html('');
   		swal(
   		{  
   			title:"Confirm?",
   			text: "Are you sure you want to send the connect request for this resource?",
   			type: 'warning',
   			showCancelButton: true,
   			confirmButtonColor: '#3085d6',
   			cancelButtonColor: '#d33',
   			confirmButtonText: 'Yes!'
   		}).then(function (result) 
   		{ 
   			if (result.value) 
   			{
   				var cs_t = 	$('.token').val();
   				$.ajax(
   				{
   					type:'POST',
   					url: '<?php echo site_url("resource_sharing/connect_resource_ajax"); ?>',
   					data:{ 'kr_id':kr_id, 'popupBlogReportComment':popupBlogReportComment, 'csrf_test_name':cs_t },
   					dataType:"JSON",
   					success:function(data)
   					{
   						$(".token").val(data.token);
   						$("#report_blog_btn_outer_"+data.kr_id).html(data.response);						
   						$("#BlogReportpopup").modal('hide');
   						
   						swal(
   						{
   							title: 'Success!',
   							text: "Request Sent successfully",
   							type: 'success',
   							showCancelButton: false,
   							confirmButtonText: 'Ok'
   						});
   					}
   				});
   			} 
   		});
   }	
   
   function like_unlike_blog(id, flag)
   {	
   	<?php if($action_flag == 1) { ?>
   		swal(
   		{
   			title: 'Alert!',
   			html: "You can not Like the KR as its status is Pending or Blocked",
   			type: 'warning'
   		});
   	<?php }else { ?>
   		var cs_t = 	$('.token').val();
   		$.ajax(
   		{
   			type:'POST',
   			url: '<?php echo site_url("knowledge_repository/like_unlike_blog_ajax"); ?>',
   			data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
   			dataType:"JSON",
   			success:function(data)
   			{
   				$(".token").val(data.token);
   				
   				$("#like_unlike_btn_outer_"+data.kr_id).html(data.response);
   			}
   		});
   	<?php } ?>
   }
   
   
   
   function share_blog(url) 
   {
   	<?php if($action_flag == 1) { ?>
   		swal(
   		{
   			title: 'Alert!',
   			html: "You can not share the blog as its status is Pending or Blocked", 
   			type: 'warning'
   		});
   	<?php }else { ?>
   		$("#BlogShareLink").html(url);
   		$("#share-popup").modal('show');
   	<?php } ?>
   }
   
   
   
   
   
   
   
   function copyToClipboardLink(containerid) 
   {
   	var id = containerid;
   	var el = document.getElementById(id);
   	var range = document.createRange();
   	range.selectNodeContents(el);
   	var sel = window.getSelection();
   	sel.removeAllRanges();
   	sel.addRange(range);
   	document.execCommand('copy');
   	
   	$("#link_copy_msg").html('Link copied');
   	
   	$("#link_copy_msg").slideDown(function() {
   		setTimeout(function() 
   		{
   			$("#link_copy_msg").slideUp();
   			sel.removeRange(range);
   		}, 1500);
   	});
   		
   	//alert("Contents copied to clipboard.");
   	return false;
   }
   
   
   
   $(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
   $(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>
<script>
   (function($){
   $(window).on("load",function(){
       $(".descriptionBox45").mCustomScrollbar({
           theme: "rounded-dark",
           scrollInertia: 0,
           mouseWheelPixels: 30,
           autoDraggerLength:false
       });
   });
   })(jQuery);
</script>
<script>
   AOS.init({
       duration: 1000,
       delay: 50,
   });
</script>

