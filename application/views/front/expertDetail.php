<div class="profileinfo" data-aos="fade-down">
  <div class="container">
      <div class="row mt-4">
         <div class="col-md-2">
            <?php  if($profile_information[0]['profile_picture']) { ?>
               <img src="<?php echo base_url('assets/profile_picture/'.$profile_information[0]['profile_picture'].'') ?>" alt="experts" class="img-fluid">
            <?php } else { ?>
               <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="" class="img-fluid bor">
            <?php } ?>
         </div>
         <div class="col-md-7">  
            <h3 class="card-title mt-5"><?php echo $profile_information[0]['title']." ".$profile_information[0]['first_name']." ".$profile_information[0]['middle_name']." ".$profile_information[0]['last_name'] ?></h3>
         </div>
         <?php  if($profile_information[0]['contactable']=='yes' && $admin_expert_connect=='Y'){ ?>
         <div class="col-md-3">
            <div class="mb-2 mt-3">
            <button type="button" style="width: 100%;" class="btn btn-primary" onclick="comming_soon_chat()">Chat with this Expert</button>
            </div>
            <div>
            <button style="width: 100%;" onclick="open_expert_email_popup('<?php echo $profile_information[0]['user_id'] ?>')" type="button" class="btn btn-primary">Send an Email</button>
            </div>
         </div>
         <?php } ?>
      </div>
   </div>
</div>
<section id="individual-profile-form" class="inner-page " data-aos="fade-up">
   <div class="container">
      <div class="row">
         <div class="formInfo">
      <?php 
	  //echo "<pre>";print_r($profile_information);
	  foreach ($profile_information as $key => $value) { ?>
        <div class="row">
            
            <div class="col-md-12">
				<div class="bg">
					  <label><b>Name of Organization:</b></label>
					  <?php echo $value['company_name'] ?>
				   </div>
			</div>
            <div class="col-md-12">
            <div class="bg">
                 <label><b>Designation:</b></label>
                 <?php echo $value['designation'][0] ?>
               </div>
         </div>
       
		 
		 
		 
		 
		 
		 
         
         <!--    <div class="col-md-12">
            <div class="bg">
               <label><b>Technical Experience in Years:</b></label>
                  <?php echo $value['technical_experience_in_year'] ?>
                  </div>
            </div>
            <div class="col-md-12">
            <div class="bg">
                  <label><b>Technical Description of Work:</b></label>
                  <?php echo $value['technical_description_of_work'] ?>
            </div>
            </div> -->
         
         
         
            <div class="col-md-12">
            <div class="bg">
               <label><b>Brief Bio:</b></label>
                  <?php echo $value['bio_data'] ?>
            </div>
            </div>
            <div class="col-md-12">
            <div class="bg">
                  <label><b>Years of experience:</b></label>
                  <?php echo $value['years_of_experience'] ?>
            </div>
            </div>
            <div class="col-md-12">
            <div class="bg">
               <label><b>No. of Paper Publications:</b></label>
                  <?php echo $value['no_of_paper_publication'] ?>
            </div>
            </div>
            <div class="col-md-12">
            <div class="bg">
               <label><b>No. of Patents:</b></label>
                  <?php echo $value['no_of_patents'] ?>
            </div>
         </div>
         
            <div class="col-md-12">
            <div class="bg">
               <label><b>Domain / Area of Expertise:</b></label>
               <?php foreach ($domain as $key => $d_value) { ?>
                  <?php if ($d_value[0]['domain_name']!='Other'): ?>
                  <?php echo $d_value[0]['domain_name'].'<br>' ?>
                  <?php endif ?>
               <?php } ?>

               <?php   if ($value['other_domian']!=''){
                  echo $value['other_domian'];
             
                } ?>
               
            </div>
               </div>
			   <div class="col-md-12">
            <div class="bg">
               <label><b>Skill Sets:</b></label>
               <?php foreach ($skill_sets as $key => $skill_value) { ?>
                  <?php if ($skill_value[0]['name']!='Other'): ?>
                  <?php echo $skill_value[0]['name'].'<br>' ?>
                  <?php endif ?>
               <?php } ?>
               <?php   if ($value['other_skill_sets']!=''){
                  echo $value['other_skill_sets'];
             
                } ?>
               
            </div>
            </div>
             
            
         </div>
      <?php } ?>
   </div>
   </div>           </div>
</section>

<div class="modal fade" id="connectExpertModal" tabindex="-1" role="dialog" aria-labelledby="connectExpertModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document" id="model_expert_modal">
   </div>
</div>

<script type="text/javascript">      
function comming_soon_chat(){
      swal({
         title:"",
         text: "Coming Soon",
         type: 'warning',
         showCancelButton: false,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'OK!',
         html:''
      });
}   
function open_expert_email_popup(expert_id)
{
   $.ajax({
      type: "POST",
      url: "<?php echo site_url('home/open_expert_email_popup'); ?>",
      data: {'expert_id':expert_id},
      cache: false,
      success:function(data)
      {
         //alert(data)
         if(data == "error")
         {
            location.reload();
         }
         else
         {
            $("#model_expert_modal").html(data);
            $("#connectExpertModal").modal('show');
         }
      }
   });
}
</script>