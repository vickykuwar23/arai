<!--<section id="intro">
    <div class="videoINfo">
        <span>An Ecosystem to Empower Ideas for Innovation & Technology Development
    </div>
    <video width="100%" autoplay="" loop="" muted="" id="myvideo">
        <source src="<?php echo base_url(); ?>assets/front/img/video/technovuus-new.mp4" type="video/mp4">
        <source src="<?php echo base_url(); ?>assets/front/img/video/technovuus-new.webm" type="video/ogg">
    </video>
</section>-->
<!-- End of Video Section -->

<?php /*?> <section id="intro">
        <div class="intro-container">
            <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

                <ol class="carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

					<?php
					
					foreach($banner_mgt as $key => $banners_list){
							
							if($key == 0){
								$class="active";
							} else {
								$class="";
							}					
					?>
                    <div class="carousel-item <?php echo $class; ?>" style="background-image: url(<?php echo base_url('assets/banner/'.$banners_list['banner_img']) ?>)">
                        <div class="carousel-container">
                            <div class="container">
                                <h2 class="animate__animated animate__fadeInDown"><?php echo $banners_list['banner_name'] ?>
                                    <a href="" class="typewrite" data-period="2000"
                                        data-type='[ " <?php echo $banners_list['banner_sub'] ?> " ]'>
                                        <span class="wrap"></span>
                                    </a>
                                </h2>
                                <p class="animate__animated animate__fadeInUp"><?php echo $banners_list['banner_desc'] ?></p>
                                <a href="<?php echo $banners_list['banner_url'] ?>"
                                    class="btn-get-started scrollto animate__animated animate__fadeInUp" target="_blank">View Challenges</a>
                            </div>
                        </div>
                    </div>
					<?php }  ?>
                </div>
                <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </section> <?php */ ?>
<!-- End Intro Section -->
<style>
.carousel-item video {
    object-fit: fill;
}
    .videot {
    width: 100%  !important; max-width: 100% !important;
    height: 100%  !important; 
 }

 #intro .carousel-item.firstBanner::before {
	content: '';
	background-color: rgba(0, 0, 0, 0);
}

    #homePage span{ display: inline !important;}
    #homePage .custom_box_class{ background: #FFF !important;}

.tagSlider div a {background: #FFF; color: #c80032; border:solid 1px #c80032; display: block; text-align: center; text-decoration:  none; border-radius: 5px; padding:6px; font-size: 12px !important;font-weight: 700}
     .tagSlider a:hover {color: #c80032;}
     .tagSlider {width: 85%; margin: 0 auto; position: relative;}
                               
     .tagSlider.owl-carousel .owl-dot,.tagSlider.owl-carousel .owl-nav .owl-next,.tagSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
     .tagSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
     .tagSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
     .tagSlider .owl-nav.disabled{display:block}
     /*.tagSlider .owl-nav{position:absolute; bottom:-15px; width:100%}*/
    
    
.tagSlider .owl-nav {
    position: absolute;
    bottom: 69px;
    width: 100%;
    z-index: 99;
}
    
     .tagSlider .owl-nav [class*=owl-]:hover{background:#FFF!important;border:solid 1px #c80032;color:#c80032}
    
     .tagSlider .owl-nav .owl-prev{left:-35px;position:absolute;background:#fff;color:#c80032; }
     .tagSlider .owl-nav .owl-next{right:-36px;position:absolute;background:#fff;color:#c80032; }
    
    
     .tagSlider .owl-item{ padding:2px;}
     .tagSlider .owl-item img{width:100%; }
     .tagSlider .owl-dots{position:absolute;width:100%;bottom:25px}

     .tagSlider.owl-theme .owl-nav [class*="owl-"] {font-size: 12px; margin: 0; padding: 5px;display: inline-block; cursor: pointer; border-radius:0px;  border: solid 1px #c80032;}
     .res_details, .tech_details{ min-height: 70px;}
.newsHome strong {display: initial !important;}

.tag ul{ list-style: none; padding: 0; margin:0 0 15px 0; } 
	.tag ul li { float: none; padding: 0; margin: 5px 5px 5px 0; display: inline-block; }
	.tag ul li span{  background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; border-radius: 5px; padding: 5px; cursor:auto; font-size:12px; }
	.tag ul li span:hover{ background: #FFF; color: #c80032; }
	
	
	.shareList ul{ list-style: none; padding: 0; margin:0 0 15px 0; display: flex; }
	.shareList ul li{ float: left; padding: 0; margin:0 5px 0px 0;}
	.shareList ul li span{  background: #FFF; color: #c80032; border:solid 1px #c80032; text-decoration: none; border-radius: 5px; padding: 5px; cursor:pointer; font-size:12px;}
	.shareList ul li span:hover{ background: #c80032; color: #FFF; }


    .blogSlider.owl-carousel .owl-dot,.blogSlider.owl-carousel .owl-nav .owl-next,.blogSlider.owl-carousel .owl-nav .owl-prev, .TrendingblogSlider.owl-carousel .owl-dot,.TrendingblogSlider.owl-carousel .owl-nav .owl-next,.TrendingblogSlider.owl-carousel .owl-nav .owl-prev {font-family:fontAwesome}
	.blogSlider.owl-carousel .owl-nav .owl-prev:before, .TrendingblogSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	.blogSlider.owl-carousel .owl-nav .owl-next:after, .TrendingblogSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	.blogSlider .owl-nav.disabled, .TrendingblogSlider .owl-nav.disabled{display:block}
	.blogSlider .owl-nav, .TrendingblogSlider .owl-nav{position:relative; bottom:0px; width:100%}
	.blogSlider.owl-theme .owl-dots .owl-dot.active span,.blogSlider.owl-theme .owl-dots .owl-dot:hover span,
	.TrendingblogSlider.owl-theme .owl-dots .owl-dot.active span,.TrendingblogSlider.owl-theme .owl-dots .owl-dot:hover span{background:#32f0ff!important}
	.blogSlider.owl-theme .owl-nav, .TrendingblogSlider.owl-theme .owl-nav [class*=owl-]{color:#c80032}
	.blogSlider .owl-nav .owl-prev, .TrendingblogSlider .owl-nav .owl-prev{background:#FFF; border:solid 1px #c80032; color: #c80032;}  
	.blogSlider .owl-nav .owl-next, .TrendingblogSlider .owl-nav .owl-next{  background:#FFF; border:solid 1px #c80032;  color: #c80032s;}
	.blogSlider .owl-nav [class*=owl-]:hover, .TrendingblogSlider .owl-nav [class*=owl-]:hover{background:#c80032!important;border:solid 1px #c80032;color:#FFF}
	
	.blogSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0 5px;}
	.blogSlider .owl-item img, .TrendingblogSlider .owl-item img{width:100%; }
	.blogSlider .owl-dots, .TrendingblogSlider .owl-dots{position:absolute;width:100%;bottom:25px}
	.blogSlider .owl-item, .TrendingblogSlider .owl-item{ padding: 0;}


    #FeaturedWebinarsSlider.owl-carousel .owl-dot,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next,#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev{font-family:fontAwesome}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-prev:before{content:"\f177"}
	#FeaturedWebinarsSlider.owl-carousel .owl-nav .owl-next:after{content:"\f178"}
	#FeaturedWebinarsSlider .owl-nav.disabled{display:block}
	#FeaturedWebinarsSlider .owl-nav{position:absolute; top:40%; width:100%}
	#FeaturedWebinarsSlider .owl-nav [class*=owl-]:hover{background:#c80032!important;border:solid 1px #c80032;color:#FFF}
	#FeaturedWebinarsSlider .owl-nav .owl-prev{left:-30px;position:absolute;background:#FFF;color:#c80032; border: solid 1px #c80032;}
	#FeaturedWebinarsSlider .owl-nav .owl-next{right:-30px;position:absolute;background:#FFF;color:#c80032; border: solid 1px #c80032;}
	#FeaturedWebinarsSlider .owl-item{ padding: 0 15px;}
	#FeaturedWebinarsSlider .owl-item img{width:90%;margin:10px auto 0;position:relative;top:-45px;border-radius:15px; box-shadow: 0 6px 25px 0 rgba(0,0,0,.2);}

    .owner-teammember img {height: 100% !important; width: 100% !important; border-radius: 0% !important;}
.owner-teammember a{background: #FFF;}
.owner-teammember a:hover{background: #FFF;}

.blog_type_highlighted_text { font-size: 12px; background: #c80032; padding: 1px 6px 2px; border-radius: 3px; font-weight: 500; color: #FFF; line-height: 16px; position: absolute; top: 0; right: 0; }
.blog_type_highlighted_text {
	font-size: 12px !important;
	background: #c80032;
	padding: 1px 6px 2px;
	border-radius: 3px;
	font-weight: 500;
	color: #FFF !important;
	line-height: 16px;
	position: absolute;
	top: 0;
	right: 0;
}

    #preloader-loader {position: fixed;top: 0;left: 0;right: 0;bottom: 0;z-index: 9999;overflow: hidden;background: rgba(0,0,0,0.5);}
    #preloader-loader:before {content: "";position: fixed;top: calc(50% - 30px);left: calc(50% - 30px);border: 6px solid #f2f2f2;border-top: 6px solid #c80032;
    border-radius: 50%;width: 60px;height: 60px;-webkit-animation: animate-preloader 1s linear infinite;animation: animate-preloader 1s linear infinite;}

</style>

<div id="preloader-loader" style="display:none;"></div>
<section id="intro">
	<div class="videoINfo">
		<span>An Ecosystem to Empower Ideas for Innovation & Technology Development</span>
	</div>
	<div class="intro-container">
		<div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

			<ol class="carousel-indicators"></ol>

			<div class="carousel-inner" role="listbox">

			<?php
			//echo "<pre>";print_r($banner_mgt);die();
			foreach($banner_mgt as $key => $banners_list){
					
					if($key == 0){
						$class="active";
					} else {
						$class="";
					}	
					if($banners_list['upload_type'] == 'Image'){
						
                //$abn_link = $banners_list['banner_url'];		
                
                $banner_url = $banners_list['banner_url'];
			?>
                <div class="carousel-item <?php echo $class; ?>" style="background-size: 100% 100%;background-image: url(<?php echo base_url('assets/banner/'.$banners_list['banner_img']) ?>); <?php if($banner_url != "") { ?> cursor:pointer; <?php } ?>" 
                <?php if($banner_url!= "") { ?> 
                    onclick="redirectbanner('<?php echo $banners_list['banner_url'] ?>')"                     
                <?php } ?>>
					<div class="carousel-container" id="<?php echo $abn_link; ?>">
						<div class="container">
						<?php if($banners_list['banner_name']!=""){ ?>
							<h2 class="animate__animated animate__fadeInDown"><?php echo $banners_list['banner_name'] ?>
								
								<?php if($banners_list['banner_sub']!=""): ?>
								<a href="" class="typewrite" data-period="2000"
									data-type='[ " <?php echo $banners_list['banner_sub'] ?> " ]'>
									<span class="wrap"></span>
								</a>
								<?php endif; ?>
							</h2>
							<?php } ?>
							<p class="animate__animated animate__fadeInUp"><?php echo $banners_list['banner_desc'] ?></p>
							<?php /*if($banners_list['banner_url']!=""){ ?>
							<a href="<?php echo $banners_list['banner_url'] ?>"
								class="btn-get-started scrollto animate__animated animate__fadeInUp" target="_blank">View Details</a>
								<?php } */ ?>
						</div>
					</div>
				</div>
				<?php } else {
				$ext = pathinfo($banners_list['banner_img'], PATHINFO_EXTENSION);
				?>
				<div class="carousel-item <?php echo $class; ?> ">
				   <video width="100%" autoplay="" loop="" muted="" id="myvideo">
						<source src="<?php echo base_url('assets/banner/'.$banners_list['banner_img']) ?>" type="video/<?php echo $ext ?>">
					</video>
				</div>
				
				<?php  } 
				
				}  ?>
			</div>
			<a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>

			<a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>

		</div>
	</div>
</section>  
<!-- End Intro Section -->

<?php
if (count($service_mgt) > 0) {
?>
    <!-- Featured Services Section Starts -->
    <section style="background-color: #fafafa;" data-aos="fade-down">
        <div class="container">
            <div id="thumbplus" class="owl-carousel owl-theme">
                <?php
                foreach ($service_mgt as $services) {

                    if (strlen($services['service_desc']) > 200) {
                        $services['service_desc'] = substr($services['service_desc'], 0, 197) . '...';
                    }

                ?>
                    <div class="col-md-12 col-sm-12" onclick="window.location.href='<?php echo $services['service_url']; ?>'">
                        <div class="module1">
                            <div class="box box1">
                                <div class="box-img">
                                    <div class="greybg">
                                        <img src="<?php echo base_url('assets/service_icon/' . $services['service_icon']); ?>" class="services-icon" />
                                        <h4 class="title"><?php echo $services['service_title']; ?></h4>
                                    </div>
                                    <p><?php echo $services['service_desc']; ?></p>
                                </div>
                                <div class="content">
                                    <a href="<?php echo $services['service_url']; ?>" class="read-more orng">Launch</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                }
                ?>
            </div>
        </div>
    </section>
<?php
} // IF END
?>
<!-- // FEATURED CHALLENGES SLIDER -->
<?php
if (count($featured_list) > 0) :
?>
    <section id="comp-offer" data-aos="fade-up">
    <div class="container">

        <div class="challenges-title">
            <div class="row">
                <div class="col-md-9">
                    <h3>Featured Challenges</h3>
                </div>
               
                <div class="col-md-3 text-right">
                    <a href="<?php echo base_url('challenge'); ?>" class="btn btn-general btn-white" role="button">view all challenges</a>
                </div>
              

            </div>
        </div>
        <div id="open-ch" class="owl-carousel owl-theme col-md-12 desc-comp-offer">

            <?php

            foreach ($featured_list as $featuredList) {

                $fileExist = base_url('assets/challenge/' . $featuredList['banner_img']);
                if ($featuredList['banner_img']!="") {
                    $image = $fileExist;
                } else {
                    $image = base_url('assets/news-14.jpg');
                }

                if (strlen($featuredList['challenge_details']) > 300) {
                    $featuredList['challenge_details'] = substr($featuredList['challenge_details'], 0, 297) . '...';
                }

                $datestr = $featuredList['challenge_close_date'];
                
								if($featuredList['challenge_launch_date'] <= date("Y-m-d") && date("Y-m-d") <= $featuredList['challenge_close_date'])
								{
								//if($date3 >= $date){{
									//$removeMinus = str_replace("-","", $days);							
									$date1=date_create(date("Y-m-d"));
									$date2=date_create($featuredList['challenge_close_date']);
									$diff=date_diff($date1,$date2);
									$showCnt = "Days left : ".($diff->format("%a")+1);							
								} 
								else 
								{
									$showCnt = "Closed Participations";
								}
								
            ?>
                <div class="col-md-12 col-sm-12 desc-comp-offer" onclick="document.location.href='<?php echo base_url('challenge/challengeDetails/' . base64_encode($featuredList['c_id'])); ?>'">
                    <div class="desc-comp-offer-cont">
                        <a href="<?php echo base_url('challenge/challengeDetails/' . base64_encode($featuredList['c_id'])); ?>">
						<div class="thumbnail-blogs">
                            <div class="caption">
                                <i class="fa fa-chain"></i>
                            </div>
                            <img src="<?php echo $image ?>" class="img-fluid" alt="...">
                        </div></a>
                        <div class="card-content">
                            <!-- <span class="read"><a href="<?php //echo base_url('challenge/challengeDetails/'.base64_encode($featuredList['c_id'])); 
                                                                ?>" class="btn btn-green-fresh">Launch</a></span> -->
                            <h5><?php echo ucwords($featuredList['company_name']); ?></h5>
                            <h3><?php echo ucwords($featuredList['challenge_title']); ?></h3>
                            <p class="desc"><?php 
							$limitDesc = 140;
							$chaDesc = trim($featuredList['challenge_details']);
							$descTest   = strip_tags($chaDesc);
							echo $chkDesc  = substr($descTest , 0, strrpos(substr($descTest , 0, $limitDesc), ' ')) . '...'; ?></p>
                            <div class="cardd-footer">
                                <?php if ($featuredList['audience_pref'] != "") { ?>
                                    <div>
                                        <?php

                                        $implode = $featuredList['audience_pref'];
                                        $exp = explode(",", $implode);
                                        foreach ($exp as $newname) {
                                        ?>
                                            <button class="btn btn-general btn-green-fresh remove-h" role="button"><?php echo $newname; ?></button>
                                        <?php
                                        }
                                        ?>
                                    <?php } else { ?>

                                        <button class="btn btn-general btn-green-fresh remove-h" role="button">Other</button>
                                    <?php } ?>
                                    </div>


                                    <div class="daysleft"><i class="fa fa-clock-o"></i> <?php echo $showCnt; ?></div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php

            } // Foreach End

            ?>
    </div>
        </div>
    </section>
<?php endif;  ?>


<!-- // FEATURED TECH TRANSFER SLIDER -->
<?php
if (count($featured_tech) > 0) :
?>
<section id="comp-offer2" data-aos="fade-up" class="newsHome">
    <div class="container">
        <?php /*
        <div class="challenges-title">
            <div class="row">
                <div class="col-md-9">
                    <h3>Featured Technology Transfer</h3>
                </div>
                <div class="col-md-3 text-right">
                    <a href="<?php echo base_url('technology_transfer'); ?>" class="btn btn-general btn-white" role="button">view all technology transfer</a>
                </div>
            </div>
        </div>
        */?>
        <div class="row title-bar">
        <div class="col-md-12">
            <h2>Featured Technology Transfer</h2>
            <div class="heading-border"></div>
        </div>
        </div>
        <div class="owl-carousel mb-2 mt-4 owl-theme resourceSharingSlider" id="homePage">

            <?php foreach ($featured_tech as $res) {
                $data['res'] = $res; 
             ?>
            <div> 

                <?php $this->load->view('front/technology_transfer/inc_tech_listing_inner', $data); ?>
            </div>  
            <?php } ?>  <!-- Foreach End -->

            
        </div>
    </div>
</section>
<?php endif;  ?>
<!-- END FEATURED TECH TRANSFER SLIDER -->



<!-- // FEATURED RESOURCE SHARING SLIDER SLIDER -->
<?php
if (count($featured_resource) > 0) :
?>
<section id="comp-offer2" data-aos="fade-up">
    <div class="container">
        <?php /*
        <div class="challenges-title">
            <div class="row">
                <div class="col-md-9">
                    <h3>Featured Technology Transfer</h3>
                </div>
                <div class="col-md-3 text-right">
                    <a href="<?php echo base_url('technology_transfer'); ?>" class="btn btn-general btn-white" role="button">view all technology transfer</a>
                </div>
            </div>
        </div>
        */?>
        <div class="row title-bar">
        <div class="col-md-12">
            <h2>Featured Resource Sharing </h2>
            <div class="heading-border"></div>
        </div>
        </div>
        <div class="owl-carousel mb-2 mt-4 owl-theme resourceSharingSlider">

            <?php foreach ($featured_resource as $res) {
                $data['res'] = $res; 
             ?>
            <div> 
                <?php $this->load->view('front/resource_sharing/inner_listing', $data); ?>
            </div>  
            <?php } ?>  <!-- Foreach End -->

            
        </div>
    </div>
</section>
<?php endif;  ?>
<!-- END FEATURED RESOURCE SHARING SLIDER -->


<!-- // FEATURED BLOG SLIDER SLIDER -->
<?php
if (count($featured_blog) > 0) :

?>
<section id="comp-offer2" data-aos="fade-up" class="newsHome">
    <div class="container">
        <div class="row title-bar">
        <div class="col-md-12">
            <h2>Featured Blog </h2>
            <div class="heading-border"></div>
        </div>
        </div>
        <div class="formInfo65" style="padding:0">
        <div  class="owl-carousel mb-2 owl-theme blogSlider">
            <?php foreach($featured_blog as $res)
                {    $data['res'] = $res; $data['blog_type_flag'] = $blog_type_flag = 'featured';   ?>
                
                <div class="" style="padding:25px">                         
                    <?php $dispHighlight = ''; if($res['user_category_id'] == 2) { $dispHighlight = 'Organization'; } else { if($res['user_sub_category_id'] == 11) { $dispHighlight = 'Expert'; } } 
            
                    if($dispHighlight != '') { echo ' <span class="blog_type_highlighted_text">'.$dispHighlight.'</span>'; } ?>
                    <?php $data['dispHighlight'] = $dispHighlight; $this->load->view('front/blogs_technology_wall/inc_blog_listing_inner', $data); ?>
                </div>              
            <?php } ?> 
        </div>
    </div>
    </div>
</section>
<?php endif;  ?>
<!-- END FEATURED BLOG SLIDER -->


<!-- // FEATURED WEBINAR SLIDER SLIDER -->
<?php
if (count($featured_webinar) > 0) :
?>
<section id="comp-offer2" data-aos="fade-up">
    <div class="container">
        <?php /*
        <div class="challenges-title">
            <div class="row">
                <div class="col-md-9">
                    <h3>Featured Technology Transfer</h3>
                </div>
                <div class="col-md-3 text-right">
                    <a href="<?php echo base_url('technology_transfer'); ?>" class="btn btn-general btn-white" role="button">view all technology transfer</a>
                </div>
            </div>
        </div>
        */?>
        <div class="row title-bar">
        <div class="col-md-12">
            <h2>Featured Webinar </h2>
            <div class="heading-border"></div>
        </div>
        </div>
        <div class="formInfoNew">
        <div class="owl-carousel mb-2 owl-theme" id="FeaturedWebinarsSlider">
            <?php foreach($featured_webinar as $featuredSlide)
                {    ?>
                <div class="owner-teammember inner-img">
                    <div class="boxShadow">
                        <a href="<?php echo base_url('webinar/viewDetail/'.base64_encode($featuredSlide['w_id'])); ?>"><img src="<?php echo base_url('assets/webinar/'.$featuredSlide['banner_img']); ?>" alt="experts" style="height: 196px!important;"></a>
                        <div class="borderBoxNewClass">
                            <h3><?php echo substr($featuredSlide['webinar_name'], 0,100)  ?></h3>
                            <ul>
                                <li>
                                    <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("jS M Y", strtotime($featuredSlide['webinar_date'])) ?>
                                </li>
                                <li>|</li>
                                <li>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i> <?php
                                        echo date("h:i A", strtotime($featuredSlide['webinar_start_time']))." - ".date("h:i A", strtotime($featuredSlide['webinar_end_time'])); ?> 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>  
        </div>
    </div>
    </div>
</section>
<?php endif;  ?>
<!-- END FEATURED WEBINAR SLIDER -->

<?php
if (count($featuredNews) > 0) {
?>
    <section class="newsHome" data-aos="fade-down">
        <div class="container">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Featured News / Automotive Updates</h2>
                    <div class="heading-border"></div>
                </div>
                <div id="newsHomePage" class="newsInfo owl-carousel owl-theme">
                    <?php foreach ($featuredNews as $news) { ?>
                        <div class="col-md-12" style="cursor: pointer;" onclick="window.location.href='<?php echo base_url('blogs/blogDetails/' . base64_encode($news['id'])); ?>'">
                            <div class="newsBoxHome">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="p-3">
                                            <a href="<?php echo base_url('blogs/blogDetails/' . base64_encode($news['id'])); ?>">
                                                <strong>
                                                    <?php
                                                    if (strlen($news['blog_name']) > 30) {
                                                        $news['blog_name'] = substr($news['blog_name'], 0, 27) . '...';
                                                    }
                                                    echo $news['blog_name']; ?>
                                                </strong></a>
                                            <span>
                                                <?php
                                                /*if (strlen($news['blog_desc']) > 60) {
                                                    $news['blog_desc'] = substr($news['blog_desc'], 0, 57) . '...';
                                                }
                                                echo $news['blog_desc'];*/
												$limitStr = 60;
												$strins = trim($news['blog_desc']);
												$strTest   = strip_tags($strins);
												echo $chk_add  = substr($strTest , 0, strrpos(substr($strTest , 0, $limitStr), ' ')) . '...';
                                                ?>

                                            </span>
                                            <em>Post On: <?php
                                                            $strtoTime = strtotime($news['createdAt']);
                                                            echo date('M Y', $strtoTime); ?></em>
                                        </div>
                                    </div>
                                    <div class="col-md-4 newsLogo">
                                        <?php if ($news['blog_img'] != "") : ?>
                                            <img src="<?php echo base_url('assets/blog/' . $news['blog_img']); ?>" alt="Featured Blog Image">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End News Section -->
<?php } ?>

<!-- // HOW IT WORKS -->
<section class="bg-parallax thought-bg ">
    <div class="">
        <div id="thought-desc" class="owl-carousel owl-theme">
            <?php foreach ($how_it_works as $key => $howitworks) { ?>
                <div class="hw-item how-item-1" style="background-image: url('<?php echo base_url(); ?>assets/background_image/<?php echo $how_it_works[$key]['background_image'] ?>');">
                    <div class="container" data-aos="fade-up">
                        <div class="row title-bar">
                            <div class="col-md-12">
                                <h2>How It Works</h2>
                                <div class="heading-border"></div>
                                <h3 class="sub-title"><?php echo $how_it_works[$key]['subject_title']; ?></h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 col-lg-4 prpl5">
                                <div class="icon_box_hiw">
                                    <div class="icon">
                                        <div class="list_tag float-right">
                                            <p>1</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['first_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['first_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['first_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 prpl5 mt20-xxsd">
                                <div class="icon_box_hiw">
                                    <div class="icon middle">
                                        <div class="list_tag float-right">
                                            <p>2</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['second_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['second_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['second_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 prpl5 mt20-xxsd">
                                <div class="icon_box_hiw">
                                    <div class="icon">
                                        <div class="list_tag float-right">
                                            <p>3</p>
                                        </div><img src="<?php echo base_url(); ?>assets/icon_image/<?php echo $how_it_works[$key]['third_icon_image'] ?>" width="100px" alt="icon">
                                    </div>
                                    <div class="details">
                                        <h4><?php echo $how_it_works[$key]['third_title']; ?></h4>
                                        <p><?php echo $how_it_works[$key]['third_description']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
</section>
<?php  if(count($featured_experts) > 0) { ?>
    <section class="expertsList" data-aos="fade-up">
        <div class="container">
            <div class="row title-bar">
                <div class="col-md-12 text-center mb-4">
                    <h2>Featured Experts</h2>
                    <div class="heading-border"></div>
                    <!--<p></p>-->
                 
                    <a href="<?php echo base_url('home/viewAllExperts') ?>">View all Experts</a>
             
                </div>

                <div id="expertsInfo" class="owl-carousel owl-theme">
                    <?php foreach ($featured_experts as $key => $user) {
                        $expert_encrypt =  new Opensslencryptdecrypt();
                        $expertise_val = $user['domain_area_of_expertise'];
                        // Get Details
                        $exp_exploades = explode(",", $expertise_val);




                    ?>
                        <div class="col-md-12" style="cursor: pointer;" onclick="window.location.href='<?php echo base_url(); ?>home/viewProfile/<?php echo base64_encode($user['user_id']) ?>'">
                            <div class="expertsListBox">
                                <?php if ($user['profile_picture']) { ?>
                                    <img src="<?php echo base_url('assets/profile_picture/' . $user['profile_picture'] . '') ?>" alt="experts" data-id="<?php echo $user['user_id'] ?>" >
                                <?php } else { ?>
                                    <img src="<?php echo base_url('assets/front/') ?>img/testimonial-1.jpg" alt="experts" data-id="<?php echo $user['user_id'] ?>" />
                                <?php } ?>
                                <div class="experts">
                                    <span class="read"><a href="<?php echo base_url(); ?>home/viewProfile/<?php echo base64_encode($user['user_id']) ?>">View Profile</a></span>
                                    <?php /*if(isset($user['first_name'])) { ?>
								<h3><?php echo $user['first_name'].' '.$user['last_name'] ?></h3>
							<? } else { ?>
								<h3><?php echo $user['company_name'] ?></h3>
							<?php } */ ?>
                                    <h3><?php echo $user['title']." ".$user['first_name'] . ' ' . $user['last_name'] ?></h3>
                                    <?php //if (!empty($user['company_name'])) : ?>
                                        <p class="c_name"><?php echo $user['company_name'] ?></p>

                                    <?php //endif ?>

                                    <p class="d_name"><?php echo $user['designation'][0];  ?></p>
                                    <!-- <p>
                            <?php if ($exp_exploades != "") {

                                foreach ($exp_exploades as $explist) {


                                    $expDetailsget  = $this->master_model->getRecords('domain_master', array('id' => $explist));

                                    $expertiseName  = $expert_encrypt->decrypt($expDetailsget[0]['domain_name']);
                            ?>
                                <?php echo substr($expertiseName, 0, 15); ?> 
                            <?php }
                            } ?>
                            </p> -->

                                </div>
                                <!-- <div class="cardd-footer"><div></div></div>  -->

                                <div class="expertsBox">
                                    <ul>
                                        <?php if ($user['years_of_experience']) { ?>
                                            <li><span><?php echo $user['years_of_experience'] ?></span>Years of Experience</li>
                                        <?php } else { ?>
                                            <li><span>0</span>Years of Experience</li>
                                        <?php } ?>
                                        <?php /*if($user['no_of_patents']) { ?>
									<li><span><?php echo $user['no_of_patents'] ?></span> Patents</li>
								<?php } else { ?>
									<li><span>0</span> Patents</li>
								<?php } */ ?>
                                        <?php if ($user['no_of_paper_publication']) { ?>
                                            <li><span><?php echo $user['no_of_paper_publication'] ?></span> Academic Publications</li>
                                        <?php } else { ?>
                                            <li><span>0</span> Academic Publications</li>
                                        <?php } ?>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!-- Facts Section Starts -->
<section id="facts">
    <div class="container" data-aos="fade-up">

        <div class="row title-bar">
            <div class="col-md-12">
                <h2>Facts And Figures</h2>
                <div class="heading-border"></div>
                <!--<p></p>-->
            </div>
        </div>

        <div class="row counters">
            <div class="col-lg-3 col-6 text-center">
                <span class="counter-count"><?php echo count($no_register_user); ?></span>
                <p>Registered Users</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span class="counter-count"><?php echo count($no_experts_user); ?></span>
                <p>Registered Experts</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span class="counter-count"><?php echo count($no_of_challenge); ?></span>
                <p>Challenges Posted</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span class="counter-count"><?php echo count($no_of_challenge_closed); ?></span>
                <p>Challenges Completed/ Closed</p>
            </div>

        </div>
    </div>
</section>
<!-- End Facts Section -->

<!-- Testimonials Section Starts -->
<?php
if (count($testimonial_list) > 0) {
?>
    <section id="testimonials" class="section-bg">
        <div class="container" data-aos="fade-up">

            <div class="row title-bar">
                <div class="col-md-12">
                    <h2><?php echo $quoteTitle; ?></h2>
                    <div class="heading-border"></div>

                </div>
            </div>

            <div class="owl-carousel testimonials-carousel text-center">
                <?php
                foreach ($testimonial_list as $testimonials) {
                ?>
                    <div class="testimonial-item">
                        <img src="<?php echo base_url('assets/testimonial/' . $testimonials['profile_img']) ?>" class="testimonial-img" alt="">
                        <h3 class="white"><?php echo $testimonials['author_name'] ?></h3>
                        <h4><?php echo $testimonials['author_designation'] ?></h4>
                        <p>
                            <img src="<?php echo base_url('assets/front/') ?>img/quote-sign-left.png" class="quote-sign-left" alt="">
                            <?php echo $testimonials['author_content'] ?>
                            <img src="<?php echo base_url('assets/front/') ?>img/quote-sign-right.png" class="quote-sign-right" alt="">
                        </p>
                    </div>
                <?php } ?>
            </div>

        </div>
    </section>
<?php } ?>

<?php
if (count($government_list) > 0) {
?>
    <section class="ourgovPartner" data-aos="fade-down">
        <div class="container">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2>Our Government Partners</h2>
                    <div class="heading-border"></div>
                </div>
                <div id="client-logo" class="clients-logo owl-carousel owl-theme">
                    <?php foreach ($government_list as $govern) : ?>
                        <div class="col-md-12">
                            <img src="<?php echo base_url('assets/partners/' . $govern['partner_img']); ?>" alt="<?php echo $govern['partner_name'] ?>" title="<?php echo $govern['partner_name'] ?>" />
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonials Section -->
<?php } ?>

<!-- // OUR PARTNERS -->
<?php
if (count($consortium_list) > 0) {
?>
    <!--Client Logo Section-->
    <section id="partners" data-aos="fade-down">
        <div class="container clientLogo2">
            <div class="row title-bar">
                <div class="col-md-12">
                    <h2><?php echo $partneTitle; ?></h2>
                    <div class="heading-border"></div>
				</div>
				<div id="client-logo25" class="clients-logo owl-carousel owl-theme">
                <?php foreach ($consortium_list as $consrotium) : ?>
                    <div class="col-sm-12"><a href="<?php echo $consrotium['img_link'] ?>" target="_blank"><img src="<?php echo base_url('assets/partners/' . $consrotium['partner_img']); ?>" alt="<?php echo $consrotium['partner_name'] ?>" title="<?php echo $consrotium['partner_name'] ?>" class="img-fluid" /></a></div>
                <?php endforeach; ?>
			</div>
			</div>
        </div>
    </section>
<?php } ?>
<section class="ministrylogo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center title-bar">
                <h4>Under Aegis of</h4>
                <div class="heading-border"></div>
                <a href="https://dhi.nic.in/" target="_blank"><img src="<?php echo base_url('assets/front/') ?>img/ministrylogo.png" alt="ministrylogo"></a>
            </div>
        </div>
    </div>
</section>

<!-- The Modal -->
<div class="modal" id="login_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Welcome to TechNovuus! </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
<div class="login_msg">

    
<p> In order to ensure seamless access to the different features on our portal such as:</p>
<ul>
<li>Participating in different challenges and problem statement solutions</li>
<li>Networking with like minded individuals and collaborating with them for solutions</li>
<li>Connecting with experts for their insights</li>
<li>Getting your solutions reviewed by Industry experts</li>
<li>Accessing our knowledge bank and resource pool
and a lot more...</li>

</ul>
<p>We recommend you complete your profile with all the requisite information. Stay Connected and Keep Innovating!</p>


        </div>
      </div>

     

    </div>
  </div>
</div>

<div class="modal fade" id="share-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Share</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
                <div id="BlogShareLink" style="border: 1px solid #e4e4e4;width: 100%;font-size: 13px;padding: 5px;background: #f2f2f2;color: #64707b;height: auto;white-space: normal;word-wrap: anywhere;word-break: break-all; position:relative;"></div>                 
                <span id="link_copy_msg" style="color: #47a44b; font-size: 12px; position: absolute; width: 100%; left: 15px; bottom:8px;"></span>
            </div>  
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismissc="modal" onclick="copyToClipboardLink('BlogShareLink')">Copy Link</button>             
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="BlogReportpopup" tabindex="-1" role="dialog" aria-labelledby="BlogReportpopupLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="BlogReportpopupLabel">Resource Sharing Request</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form>
            <div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
               <div class="bgNewBox65">
                  <p><strong>Resource ID :</strong> <span id="popupBlogId"></span><strong> Date: </strong> <span id="popupDate"></span> </p>
                  <h5> <span id="popupBlogTitle"></span></h5>
               </div>
               <div class="form-group">
                  <label for="popupBlogReportComment" class="">Purpose:(Optional) </label>
                  <textarea class="form-control" id="popupBlogReportComment" name="popupBlogReportComment" required rows="6"></textarea>
                  <span id="popupBlogReportComment_err" class='error'></span>
               </div>
            </div>
            <div class="bgNewBox65">
               <strong>Application:</strong>    
               <p><strong>Name:</strong> <span id="popupOwnerName"></span></p>
               <p><strong>Email:</strong> <span id="popupOwneMail"></span></p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>           
               <button type="button" class="btn btn-primary rightButton" onclick="submit_res_Connect('<?php echo base64_encode($form_data[0]['r_id']); ?>')">Submit</button>            
            </div>
         </form>
      </div>
   </div>
</div>
<script>
function redirectbanner(redirect_url)
{
    if(redirect_url != "")
    {
        window.open(redirect_url, '_blank');
    }
}

function like_unlike(id, flag)
    {       
        var user_id = '<?php echo $this->session->userdata('user_id'); ?>';
        if(user_id == ''){
           swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
            return false;
        }

        var cs_t =  $('.token').val();
        $.ajax(
        {
            type:'POST',
            url: '<?php echo site_url("resource_sharing/like_unlike_ajax"); ?>',
            data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
            dataType:"JSON",
            success:function(data)
            {
                $("#preloader").css("display", "none");
                $(".token").val(data.token);
                
                $("#like_unlike_btn_outer_"+data.r_id).html(data.response);
                $("#like_count_outer_"+data.r_id).html(data.total_likes_html);
            }
        });
    }

    function like_unlike_blog(id, flag)
    {       
        var user_id = '<?php echo $this->session->userdata('user_id'); ?>';
        if(user_id == ''){
           swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
            return false;
        }
        var cs_t =  $('.token').val();
        $.ajax(
        {
            type:'POST',
            url: '<?php echo site_url("blogs_technology_wall/like_unlike_blog_ajax"); ?>',
            data:{ 'id':id, 'flag':flag, 'csrf_test_name':cs_t },
            dataType:"JSON",
            success:function(data)
            {
                $("#preloader").css("display", "none");
                $(".token").val(data.token);
                
                $("#like_unlike_btn_outer_"+data.blog_id).html(data.response);
            }
        });
    }

    function share_blog(url)
    {
        $("#BlogShareLink").html(url);
        $("#share-popup").modal('show');
    }

    function copyToClipboardLink(containerid) 
    {
        var id = containerid;
        var el = document.getElementById(id);
        var range = document.createRange();
        range.selectNodeContents(el);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        document.execCommand('copy');
        
        $("#link_copy_msg").html('Link copied');
        
        $("#link_copy_msg").slideDown(function() {
            setTimeout(function() 
            {
                $("#link_copy_msg").slideUp();
                sel.removeRange(range);
            }, 1500);
        });
            
        //alert("Contents copied to clipboard.");
        return false;
    }

      function connect_resource(kr_id, ReportedFlag, blog_disp_id, blog_title,owner_name,owner_mail,date)
   {
    var user_id = '<?php echo $this->session->userdata('user_id'); ?>';
    if(user_id == ''){
       swal( '','Dear User,<br>Please login to access all features of Technovuus.<br> If you don\'t have an account yet please register.','info');
        return false;
    }
    module_id=35;
    if(check_permissions_ajax(module_id) == false){
            swal( 'Warning!','You don\'t have permission to access this page !','warning');
            return false;
    }
   
    <?php if($action_flag == 1) { ?>
        swal(
        {
            title: 'Alert!',
            html: "You can not send the request as the resource status is Pending or Blocked",
            type: 'warning'             
        });
    <?php }else { ?>
        if(ReportedFlag == "1")
        {
            $("#popupBlogId").html(blog_disp_id);
            $("#popupBlogTitle").html(blog_title);
            $("#resource_id").val(kr_id)
            $("#popupOwnerName").html(owner_name)
            $("#popupOwneMail").html(owner_mail)
            $("#popupDate").html(date);
            $("#popupBlogReportComment_err").html('');
            $("#BlogReportpopup").modal('show');
            $("#popupBlogReportComment").focus();
            
        }
        else
        {
            swal(
            {
                title: 'Alert!',
                text: "You have already sent a connect request",
                type: 'alert',
                showCancelButton: false,
                confirmButtonText: 'Ok'
            });
        }
    <?php } ?>
   }

     function submit_res_Connect(kr_id)
   {    
    // check_report_validation();
    
    var popupBlogReportComment = $("#popupBlogReportComment").val();
    
        $("#popupBlogReportComment_err").html('');
        swal(
        {  
            title:"Confirm?",
            text: "Are you sure you want to send the connect request for this resource?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        { 
            if (result.value) 
            {
                var cs_t =  $('.token').val();
                $.ajax(
                {
                    type:'POST',
                    url: '<?php echo site_url("resource_sharing/connect_resource_ajax"); ?>',
                    data:{ 'kr_id':kr_id, 'popupBlogReportComment':popupBlogReportComment, 'csrf_test_name':cs_t },
                    dataType:"JSON",
                    success:function(data)
                    {
                        $(".token").val(data.token);
                        $("#report_blog_btn_outer_"+data.kr_id).html(data.response);                        
                        $("#BlogReportpopup").modal('hide');
                        
                        swal(
                        {
                            title: 'Success!',
                            text: "Request Sent successfully",
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    }
                });
            } 
        });
   }    

    $(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
    $(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>



<?php $this->load->view('front/technology_transfer/connect',$captcha); ?>

<!--Client Logo Section Ends-->