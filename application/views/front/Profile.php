<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Student_profile
Author : Suraj M
*/


class Profile extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');

		$this->load->model('Common_model_sm');
		
		$this->load->library('Opensslencryptdecrypt');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('login'));
		}	
    }

    public function index($value='')
    {
    	$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		 $user_id = $this->session->userdata('user_id');
		 				$this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
		 				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
		 				$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
		$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
		// print_r($user_data);
		
		$user_arr = array();
		if(count($user_data)){	
						
			foreach($user_data as $row_val){		
						
				$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
				$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
				$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
				$row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
				$row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$user_arr[] = $row_val;
			}
			
		}

		
		$profileInfo = $this->master_model->getRecords("student_profile",array('user_id' => $user_id));

		$student_arr = array();
		if(count($profileInfo))
		{
			foreach($profileInfo as $row_val){
				$row_val['status'] = $encrptopenssl->decrypt($row_val['status']);
				$row_val['university_course'] = json_decode($row_val['university_course']);
				$row_val['university_since_year'] = json_decode($row_val['university_since_year']);
				$row_val['university_to_year'] = json_decode($row_val['university_to_year']);
				$row_val['university_name'] = json_decode($row_val['university_name']);
				$row_val['university_location'] = json_decode($row_val['university_location']);
				$row_val['current_study_course'] = json_decode($row_val['current_study_course']);
				$row_val['current_study_since_year'] = json_decode($row_val['current_study_since_year']);
				$row_val['current_study_to_year'] = json_decode($row_val['current_study_to_year']);
				$row_val['current_study_description'] = json_decode($row_val['current_study_description']);
				$row_val['domain_and_area_of_training_and_study'] = explode(',',$encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']));
				$row_val['areas_of_interest'] = $encrptopenssl->decrypt($row_val['areas_of_interest']);
				$row_val['skill_sets'] = $encrptopenssl->decrypt($row_val['skill_sets']);
				$row_val['pincode'] = $encrptopenssl->decrypt($row_val['pincode']);
				$row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
				$row_val['area_colony_street_village'] = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
				$row_val['town_city_and_state'] = $encrptopenssl->decrypt($row_val['town_city_and_state']);
				$row_val['country'] = $encrptopenssl->decrypt($row_val['country']);
				$row_val['student_id_proof'] = $encrptopenssl->decrypt($row_val['student_id_proof']);
				$student_arr[] = $row_val;
			}
		}

		$data['student_profile'] = $student_arr;

		$skill_sets = $this->master_model->getRecords("arai_skill_sets");
		$skill_sets_arr = array();
		if(count($skill_sets)){	
						
			foreach($skill_sets as $row_val){		
						
				$row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
				$skill_sets_arr[] = $row_val;
			}
			
		}
		$data['skill_sets'] = $skill_sets_arr;

		if (count($profileInfo)) {
			$domain_info=explode(',', $encrptopenssl->decrypt($profileInfo[0]['domain_area_of_expertise']));
				foreach ($domain_info as $key) {
				$this->db->or_where('id',$key);
					}
			$domain_data = $this->master_model->getRecords("arai_domain_master",'','domain_name');
			$domain_arr = array();
			if(count($domain_data)){	
							
				foreach($domain_data as $row_val){		
							
					$row_val['domain_name'] = $encrptopenssl->decrypt($row_val['domain_name']);
					$domain_arr[] = $row_val;
				}
				
			}

			$data['domain_info'] = $domain_arr;
		}else{
			
			$data['domain_info']=array();
		}
		if (count($profileInfo)) {
			$intrest_info=explode(',', $encrptopenssl->decrypt($profileInfo[0]['areas_of_interest']));
				foreach ($intrest_info as $key) {
				$this->db->or_where('id',$key);
					}
			$intrest_data = $this->master_model->getRecords("area_of_interest",'','area_of_interest');
			$intrest_arr = array();
			if(count($intrest_data)){	
							
				foreach($intrest_data as $row_val){		
							
					$row_val['area_of_interest'] = $encrptopenssl->decrypt($row_val['area_of_interest']);
					$intrest_arr[] = $row_val;
				}
				
			}

			$data['intrest_info'] = $intrest_arr;
		}else{
			
			$data['intrest_info']=array();
		}

		if (count($profileInfo)) {
			$skill_info=explode(',', $encrptopenssl->decrypt($profileInfo[0]['skill_sets']));
				foreach ($skill_info as $key) {
				$this->db->or_where('id',$key);
					}
			$skill_data = $this->master_model->getRecords("skill_sets",'','name');
			$skill_arr = array();
			if(count($skill_data)){	
							
				foreach($skill_data as $row_val){		
							
					$row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
					$skill_arr[] = $row_val;
				}
				
			}

			$data['skill_info'] = $skill_arr;
		}else{
			
			$data['skill_info']=array();
		}
			

		$data['profile_information'] =$this->profile_data($user_id);
		// echo "<pre>";print_r($data['profile_information']);die;
		$data['user'] = $user_arr;
    	$data['page_title']='Individual_profile';
		$data['middle_content']='individual_profile/index';
		$this->load->view('front/front_combo',$data);
    	
    }

    public function edit($value='')
    {
    	$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		 $user_id = $this->session->userdata('user_id');
		 				$this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
		 				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
		 				$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
		$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
		// print_r($user_data);
			$user_arr = array();
		if(count($user_data)){	
						
			foreach($user_data as $row_val){		
						
				$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
				$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
				$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
				$row_val['country_code'] = $encrptopenssl->decrypt($row_val['country_code']);
				$row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
				$row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$user_arr[] = $row_val;
			}
			
		}
		
		
		$data['country_codes']  = $this->master_model->getRecords('country','','',array('iso'=>'asc'));
		$data['user'] = $user_arr;

    	$data['page_title']='Individual_profile';
		$data['middle_content']='individual_profile/edit_personal';
		$this->load->view('front/front_combo',$data);
    	
    }

    public function profile_data($userId)
    {
    	$encrptopenssl =  New Opensslencryptdecrypt();
    	$profileInfo = $this->master_model->getRecords("student_profile",array('user_id' => $userId));
		$profile_info_arr = array();
		if(count($profileInfo)){			
			foreach($profileInfo as $row_val){		
				$row_val['status'] = $encrptopenssl->decrypt($row_val['status']);		
				$row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);
				$row_val['university_course'] = $encrptopenssl->decrypt($row_val['university_course']);
				$row_val['university_since_year'] = $encrptopenssl->decrypt($row_val['university_since_year']);
				$row_val['university_to_year'] = $encrptopenssl->decrypt($row_val['university_to_year']);
				$row_val['university_name'] = $encrptopenssl->decrypt($row_val['university_name']);
				$row_val['company_name'] = $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['university_location'] = $encrptopenssl->decrypt($row_val['university_location']);
				$row_val['current_study_course'] = $encrptopenssl->decrypt($row_val['current_study_course']);
				$row_val['current_study_since_year'] = $encrptopenssl->decrypt($row_val['current_study_since_year']);
				$row_val['current_study_to_year'] = $encrptopenssl->decrypt($row_val['current_study_to_year']);
				$row_val['current_study_description'] = $encrptopenssl->decrypt($row_val['current_study_description']);
				
				$row_val['designation'] = json_decode($row_val['designation']);		
				$row_val['designation_since_year'] = json_decode($row_val['designation_since_year']);
				$row_val['designation_description'] = json_decode($row_val['designation_description']);

				$row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
				$row_val['domain_area_of_expertise'] = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));
				$row_val['areas_of_interest'] = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
				$row_val['corporate_linkage_code'] = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
				$row_val['skill_sets'] = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));
				$row_val['event_experience_in_year'] = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
				$row_val['event_description_of_work'] = $row_val['event_description_of_work'];
				
				$row_val['technical_experience_in_year'] = json_decode($row_val['technical_experience_in_year']);
				$row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);

				$row_val['pincode'] = $encrptopenssl->decrypt($row_val['pincode']);		
				$row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
				$row_val['area_colony_street_village'] = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
				$row_val['town_city_and_state'] = $encrptopenssl->decrypt($row_val['town_city_and_state']);
				$row_val['country'] = $encrptopenssl->decrypt($row_val['country']);
				$row_val['student_id_proof'] = $encrptopenssl->decrypt($row_val['student_id_proof']);
			
				$row_val['specify_fields_area_that_you_would_like'] = json_decode($row_val['specify_fields_area_that_you_would_like']);

				$row_val['bio_data'] = $encrptopenssl->decrypt($row_val['bio_data']);
				$row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);
				
				$row_val['no_of_paper_publication'] = $row_val['no_of_paper_publication'];
				$row_val['paper_year'] = json_decode($row_val['paper_year']);
				$row_val['paper_conf_name'] = json_decode($row_val['paper_conf_name']);
				$row_val['paper_title'] = json_decode($row_val['paper_title']);		
				
				$row_val['no_of_patents'] = $row_val['no_of_patents'];
				$row_val['patent_year'] = json_decode($row_val['patent_year']);
				$row_val['patent_number'] = json_decode($row_val['patent_number']);
				$row_val['patent_title'] = json_decode($row_val['patent_title']);
				
				$row_val['portal_name'] = $encrptopenssl->decrypt($row_val['portal_name']);
				$row_val['portal_link'] = $encrptopenssl->decrypt($row_val['portal_link']);
				$row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);
				$row_val['profile_picture'] = $row_val['profile_picture'];
				$profile_info_arr[] = $row_val;
			}
			
		}

		return $profile_info_arr;

    }
    public function individual($value='')
    {
    	$userId = $this->session->userdata('user_id');
		$encrptopenssl =  New Opensslencryptdecrypt();

    	 
		 				$this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,mobile,membership_type');
		 				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
		 				$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
		$user_data = $this->master_model->getRecords("registration",array('user_id'=>$userId));

    	$user_arr = array();
		if(count($user_data)){	
						
			foreach($user_data as $row_val){		
						
				$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
				$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
				$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
				$row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
				$row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$user_arr[] = $row_val;
			}
			
		}

		$this->load->library('upload');

		$domain_data = $this->master_model->getRecords("arai_domain_master");
		
		$domain_arr = array();
		if(count($domain_data)){	
						
			foreach($domain_data as $row_val){		
						
				$row_val['domain_name'] = $encrptopenssl->decrypt($row_val['domain_name']);
				$domain_arr[] = $row_val;
			}
			
		}
		$data['domain'] = $domain_arr;

		$area_of_interest = $this->master_model->getRecords("arai_area_of_interest");
		$area_of_interest_arr = array();
		if(count($area_of_interest)){	
						
			foreach($area_of_interest as $row_val){		
						
				$row_val['area_of_interest'] = $encrptopenssl->decrypt($row_val['area_of_interest']);
				$area_of_interest_arr[] = $row_val;
			}
			
		}
		$data['area_of_interest'] = $area_of_interest_arr;

		$skill_sets = $this->master_model->getRecords("arai_skill_sets");
		$skill_sets_arr = array();
		if(count($skill_sets)){	
						
			foreach($skill_sets as $row_val){		
						
				$row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
				$skill_sets_arr[] = $row_val;
			}
			
		}
		$data['skill_sets'] = $skill_sets_arr;

		$profileInfo = $this->master_model->getRecords("student_profile",array('user_id' => $userId));
		$profile_info_arr = array();
		if(count($profileInfo)){			
			foreach($profileInfo as $row_val){		
				$row_val['status'] = $encrptopenssl->decrypt($row_val['status']);		
				$row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);
				$row_val['university_course'] = $encrptopenssl->decrypt($row_val['university_course']);
				$row_val['university_since_year'] = $encrptopenssl->decrypt($row_val['university_since_year']);
				$row_val['university_to_year'] = $encrptopenssl->decrypt($row_val['university_to_year']);
				$row_val['university_name'] = $encrptopenssl->decrypt($row_val['university_name']);
				$row_val['company_name'] = $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['university_location'] = $encrptopenssl->decrypt($row_val['university_location']);
				$row_val['current_study_course'] = $encrptopenssl->decrypt($row_val['current_study_course']);
				$row_val['current_study_since_year'] = $encrptopenssl->decrypt($row_val['current_study_since_year']);
				$row_val['current_study_to_year'] = $encrptopenssl->decrypt($row_val['current_study_to_year']);
				$row_val['current_study_description'] = $encrptopenssl->decrypt($row_val['current_study_description']);
				
				$row_val['designation'] = json_decode($row_val['designation']);		
				$row_val['designation_since_year'] = json_decode($row_val['designation_since_year']);
				$row_val['designation_description'] = json_decode($row_val['designation_description']);

				$row_val['domain_and_area_of_training_and_study'] = $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']);
				$row_val['domain_area_of_expertise'] = explode(',', $encrptopenssl->decrypt($row_val['domain_area_of_expertise']));
				$row_val['areas_of_interest'] = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
				$row_val['corporate_linkage_code'] = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
				$row_val['skill_sets'] = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));
				$row_val['event_experience_in_year'] = $encrptopenssl->decrypt($row_val['event_experience_in_year']);
				$row_val['event_description_of_work'] = $encrptopenssl->decrypt($row_val['event_description_of_work']);
				
				$row_val['technical_experience_in_year'] = json_decode($row_val['technical_experience_in_year']);
				$row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);

				$row_val['pincode'] = $encrptopenssl->decrypt($row_val['pincode']);		
				$row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
				$row_val['area_colony_street_village'] = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
				$row_val['town_city_and_state'] = $encrptopenssl->decrypt($row_val['town_city_and_state']);
				$row_val['country'] = $encrptopenssl->decrypt($row_val['country']);
				$row_val['student_id_proof'] = $encrptopenssl->decrypt($row_val['student_id_proof']);
				
				$row_val['specify_fields_area_that_you_would_like'] = json_decode($row_val['specify_fields_area_that_you_would_like']);

				$row_val['bio_data'] = $encrptopenssl->decrypt($row_val['bio_data']);
				$row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);
				
				$row_val['no_of_paper_publication'] = $row_val['no_of_paper_publication'];
				$row_val['paper_year'] = json_decode($row_val['paper_year']);
				$row_val['paper_conf_name'] = json_decode($row_val['paper_conf_name']);
				$row_val['paper_title'] = json_decode($row_val['paper_title']);		
				
				$row_val['no_of_patents'] = $row_val['no_of_patents'];
				$row_val['patent_year'] = json_decode($row_val['patent_year']);
				$row_val['patent_number'] = json_decode($row_val['patent_number']);
				$row_val['patent_title'] = json_decode($row_val['patent_title']);
				
				$row_val['portal_name'] = $encrptopenssl->decrypt($row_val['portal_name']);
				$row_val['portal_link'] = $encrptopenssl->decrypt($row_val['portal_link']);
				$row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);

				$row_val['profile_picture'] = $row_val['profile_picture'];
				$profile_info_arr[] = $row_val;
			}
			
		}

		$data['profile_information'] = $profile_info_arr;
		// echo "<pre>";
		// print_r($data['profile_information'][0]['id']);die;

		// if (isset($_POST['btn_submit'])) {
		// Check Validation
		$this->form_validation->set_rules('employement_status', 'Employement Status', 'required');
		if ($this->input->post('employement_status')=='Company') {
			$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		}
		
		$this->form_validation->set_rules('designation[]', 'Designation', 'required');
		$this->form_validation->set_rules('designation_since_year[]', 'Designation Since Year', 'required');
		$this->form_validation->set_rules('designation_description[]', 'Description', 'required');
		//$this->form_validation->set_rules('corporate_linkage_code', 'Corporate Linkage Code', 'required');
		
		// $this->form_validation->set_rules('technical_experience_in_year[]', 'Technical Experience', 'required');
		// $this->form_validation->set_rules('technical_description_of_work[]', 'Description of Work', 'required');
		
		// $this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
		// $this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
		// $this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
		// $this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
		// $this->form_validation->set_rules('country', 'Country', 'required');
		
		if ($this->session->userdata('user_sub_category')==11) {
		$this->form_validation->set_rules('specify_fields_area_that_you_would_like[]', 'specify fields area that you would like to be an expert', 'required');
		$this->form_validation->set_rules('bio_data', 'Bio Data', 'required');
		$this->form_validation->set_rules('years_of_experience', 'Years of Experience', 'required');
		$this->form_validation->set_rules('no_of_paper_publication', 'No of Paper Publication', 'required');
		// $this->form_validation->set_rules('paper_year', 'Paper Year', 'required');
		// $this->form_validation->set_rules('paper_conf_name', 'Paper Conf. Name', 'required');
		// $this->form_validation->set_rules('paper_title', 'Paper Title', 'required');
		$this->form_validation->set_rules('no_of_patents', 'No of Patents', 'required');
		// $this->form_validation->set_rules('patent_year', 'Patent Year', 'required');
		// $this->form_validation->set_rules('patent_number', 'Patent Number', 'required');
		// $this->form_validation->set_rules('patent_title', 'Patent Title', 'required');
		$this->form_validation->set_rules('portal_name', 'Portal Name', 'required');
		$this->form_validation->set_rules('portal_link', 'Portal Link', 'required');
		$this->form_validation->set_rules('portal_description', 'Portal Description', 'required');
		}



		if($this->form_validation->run())
		{	
			$type = $this->input->post('type');
			$employement_status = $encrptopenssl->encrypt($this->input->post('employement_status'));
			$company_name = $encrptopenssl->encrypt($this->input->post('company_name'));
			

			$designation=json_encode($this->input->post('designation[]') );
			$designation_since_year=json_encode($this->input->post('designation_since_year[]') );
			$designation_description=json_encode($this->input->post('designation_description[]'));

			$technical_experience_in_year='';
			$technical_description_of_work='';
			$technical_experience_in_year_arr = $this->input->post('technical_experience_in_year[]');

			if (count($technical_experience_in_year_arr) > 0) {
				$technical_experience_in_year=json_encode($technical_experience_in_year_arr);
				$technical_description_of_work=json_encode($this->input->post('technical_description_of_work[]'));
			}
			
			

			$corporate_linkage_code = $encrptopenssl->encrypt($this->input->post('corporate_linkage_code'));
			
			$pincode = $encrptopenssl->encrypt($this->input->post('pincode'));
			$flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
			$area_colony_street_village = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
			$town_city_and_state = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
			$country = $encrptopenssl->encrypt($this->input->post('country'));
			
			if ($this->session->userdata('user_sub_category')==11) {
			
			$specify_fields_area_that_you_would_like = json_encode($this->input->post('specify_fields_area_that_you_would_like'));
			$bio_data = $encrptopenssl->encrypt($this->input->post('bio_data'));
			$years_of_experience = $encrptopenssl->encrypt($this->input->post('years_of_experience'));
			
			$no_of_paper_publication = $this->input->post('no_of_paper_publication');
			
			$paper_year = $this->input->post('paper_year[]');
			$paper_conf_name = $this->input->post('paper_conf_name[]');
			$paper_title = $this->input->post('paper_title[]');
			
			$no_of_patents = $this->input->post('no_of_patents');
			
			$patent_year = $this->input->post('patent_year[]');
			$patent_number = $this->input->post('patent_number[]');
			$patent_title = $this->input->post('patent_title[]');
			
			$portal_name = $encrptopenssl->encrypt($this->input->post('portal_name'));
			$portal_link = $encrptopenssl->encrypt($this->input->post('portal_link'));
			$portal_description = $encrptopenssl->encrypt($this->input->post('portal_description'));

			}

			$domain = $this->input->post('domain_and_area_of_expertise');
			$domainAreaOfExpertise = $encrptopenssl->encrypt(implode(',', $domain));
			
			$areaofInterest = $this->input->post('areas_of_interest');
			$AreaOfInterest = $encrptopenssl->encrypt(implode(',', $areaofInterest));
			
			$skillSets = $this->input->post('skill_sets');
			$SkillSets = $encrptopenssl->encrypt(implode(',', $skillSets));



		

			// if($_FILES['profile_picture']['name']!=""){			
				
			// 	$config['upload_path']      = 'assets/profile_picture';
			// 	$config['allowed_types']    = '*';  
			// 	$config['max_size']         = '5000';         
			// 	$config['encrypt_name']     = TRUE;
							 
			// 	$upload_files  = @$this->master_model->upload_file('profile_picture', $_FILES, $config, FALSE);
			//     $b_image = "";
			// 	if(isset($upload_files[0]) && !empty($upload_files[0])){
					
			// 		$b_image = $upload_files[0];
						
			// 	}

			// 	$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
			// 	$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
			// 	if(!array_key_exists($ext, $allowed)){
						
			// 		$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
			// 		$this->session->set_flashdata('error',$data['imageError']);
			// 		redirect(base_url('profile/individual'));	
			// 	}
						
			// 	$filesize = $_FILES['profile_picture']['size'];
			// 	// Verify file size - 5MB maximum
			// 	$maxsize = 5 * 1024 * 1024;
			// 	$file_names = $b_image[0];				   
			// } // Student ID Proof Image End
			// else {
			// 	$file_names = $data['profile_information'][0]['profile_picture'];
			// }
			if ($this->session->userdata('user_sub_category')==11) {
				$insertArr = array(
							'type' => $type,
							'employement_status' => $employement_status,
							'company_name' => $company_name,
							'designation' => $designation,
							'designation_since_year' => $designation_since_year,
							'designation_description' => $designation_description,
							'corporate_linkage_code' => $corporate_linkage_code,
							'technical_experience_in_year' => $technical_experience_in_year,
							'technical_description_of_work' => $technical_description_of_work, 
							'pincode' => $pincode,
							'flat_house_building_apt_company' => $flat_house_building_apt_company,
							'area_colony_street_village' => $area_colony_street_village,
							'town_city_and_state' => $town_city_and_state, 'country' => $country,
							
							'specify_fields_area_that_you_would_like' => $specify_fields_area_that_you_would_like,
							'bio_data' => $bio_data,
							'years_of_experience' => $years_of_experience,
							'no_of_paper_publication' => $no_of_paper_publication,
							'paper_year' => json_encode($paper_year),
							'paper_conf_name' => json_encode($paper_conf_name),
							'paper_title' => json_encode($paper_title),
							'no_of_patents' => $no_of_patents,
							'patent_year' => json_encode($patent_year),
							'patent_number' => json_encode($patent_number),
							'patent_title' => json_encode($patent_title),
							'portal_name' => $portal_name,
							'portal_link' => $portal_link,
							'portal_description' => $portal_description,

							// 'profile_picture' => $file_names,
							
							'domain_area_of_expertise' => $domainAreaOfExpertise,
							'areas_of_interest' => $AreaOfInterest,
							'skill_sets' => $SkillSets,
							'user_id' => $userId,
							'updatedAt' => date('Y-m-d H:i:s')
						);
			}else{
				$insertArr = array(
							'type' => $type,
							'employement_status' => $employement_status,
							'company_name' => $company_name,
							'designation' => $designation,
							'designation_since_year' => $designation_since_year,
							'designation_description' => $designation_description,
							'corporate_linkage_code' => $corporate_linkage_code,
							'technical_experience_in_year' => $technical_experience_in_year,
							'technical_description_of_work' => $technical_description_of_work, 
							'pincode' => $pincode,
							'flat_house_building_apt_company' => $flat_house_building_apt_company,
							'area_colony_street_village' => $area_colony_street_village,
							'town_city_and_state' => $town_city_and_state, 'country' => $country,
							// 'profile_picture' => $file_names,
							'domain_area_of_expertise' => $domainAreaOfExpertise,
							'areas_of_interest' => $AreaOfInterest,
							'skill_sets' => $SkillSets,
							'user_id' => $userId,
							'updatedAt' => date('Y-m-d H:i:s')
						);
			}


			$file_upload_flag = 0;
			if($_FILES['profile_picture']['name'] != "")
					{
						$profile_res = $this->Common_model_sm->upload_single_file("profile_picture", array('png','jpg','jpeg'), "profile_picture_".date("YmdHis"), "./assets/profile_picture", "png|jpeg|jpg");
						if($profile_res['response'] == 'error')
						{
							$data['profile_picture_error'] = $profile_res['message'];
							$file_upload_flag = 1;
						}
						else if($profile_res['response'] == 'success')
						{
							$file_names = $profile_res['message'];	
							$insertArr['profile_picture']=$file_names;
							//@unlink("./uploads/organization_profile/".$organization_data[0]['pan_card']);
						}
		 }

			// echo "<pre>";
			// print_r($insertArr);
			// die;
			
			$id = $this->input->post('profileId');
			$insertQuery='';
			if(!empty($id)) {
				$updateQuery = $this->master_model->updateRecord('student_profile',$insertArr,array('id' => $id));
			}
			else {
				$insertQuery = $this->master_model->insertRecord('student_profile',$insertArr);
			}

			if($insertQuery > 0){
				$this->session->set_flashdata('success_profile','Profile Updated Successfully.');
				redirect(base_url('profile'));
			} else if($updateQuery > 0){
				$this->session->set_flashdata('success_profile','Profile Updated Successfully.');
				redirect(base_url('profile'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('profile/individual'));
			}
		}else{
			echo validation_errors();
		}
       
		$data['page_title']='Individual_profile';
		$data['user'] = $user_arr;
		$data['middle_content']='individual_profile/add';
		$this->load->view('front/front_combo',$data);
   	}

   	public function student($value='')
   	{
   		$userId = $this->session->userdata('user_id');
   		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');

		$domain_data = $this->master_model->getRecords("arai_domain_master");
		
		$domain_arr = array();
		if(count($domain_data)){	
						
			foreach($domain_data as $row_val){		
						
				$row_val['domain_name'] = $encrptopenssl->decrypt($row_val['domain_name']);
				$domain_arr[] = $row_val;
			}
			
		}
		$data['domain'] = $domain_arr;

		$area_of_interest = $this->master_model->getRecords("arai_area_of_interest");
		$area_of_interest_arr = array();
		if(count($area_of_interest)){	
						
			foreach($area_of_interest as $row_val){		
						
				$row_val['area_of_interest'] = $encrptopenssl->decrypt($row_val['area_of_interest']);
				$area_of_interest_arr[] = $row_val;
			}
			
		}
		$data['area_of_interest'] = $area_of_interest_arr;

		$skill_sets = $this->master_model->getRecords("arai_skill_sets");
		$skill_sets_arr = array();
		if(count($skill_sets)){	
						
			foreach($skill_sets as $row_val){		
						
				$row_val['name'] = $encrptopenssl->decrypt($row_val['name']);
				$skill_sets_arr[] = $row_val;
			}
			
		}
		$data['skill_sets'] = $skill_sets_arr;

		$profileInfo = $this->master_model->getRecords("student_profile",array('user_id' => $userId));
		$profile_info_arr = array();
		if(count($profileInfo)){			
			foreach($profileInfo as $row_val){		
				$row_val['status'] = $encrptopenssl->decrypt($row_val['status']);		
				$row_val['employement_status'] = $encrptopenssl->decrypt($row_val['employement_status']);
				$row_val['university_course'] = json_decode($row_val['university_course']);
				$row_val['university_since_year'] = json_decode($row_val['university_since_year']);
				$row_val['university_to_year'] = json_decode($row_val['university_to_year']);
				$row_val['university_name'] = json_decode($row_val['university_name']);
				$row_val['university_location'] = json_decode($row_val['university_location']);
				$row_val['company_name'] = $encrptopenssl->decrypt($row_val['company_name']);
				$row_val['current_study_course'] = json_decode($row_val['current_study_course']);
				$row_val['current_study_since_year'] = json_decode($row_val['current_study_since_year']);
				$row_val['current_study_to_year'] = json_decode($row_val['current_study_to_year']);
				$row_val['current_study_description'] = json_decode($row_val['current_study_description']);
				$row_val['designation'] = $encrptopenssl->decrypt($row_val['designation']);		
				$row_val['designation_since_year'] = $encrptopenssl->decrypt($row_val['designation_since_year']);
				$row_val['designation_description'] = $encrptopenssl->decrypt($row_val['designation_description']);
				$row_val['domain_and_area_of_training_and_study'] = explode(',', $encrptopenssl->decrypt($row_val['domain_and_area_of_training_and_study']));
				$row_val['areas_of_interest'] = explode(',', $encrptopenssl->decrypt($row_val['areas_of_interest']));
				$row_val['corporate_linkage_code'] = $encrptopenssl->decrypt($row_val['corporate_linkage_code']);
				$row_val['skill_sets'] = explode(',', $encrptopenssl->decrypt($row_val['skill_sets']));
				$row_val['event_experience_in_year'] = json_decode($row_val['event_experience_in_year']);
				$row_val['event_description_of_work'] = json_decode($row_val['event_description_of_work']);
				$row_val['technical_experience_in_year'] = json_decode($row_val['technical_experience_in_year']);
				$row_val['technical_description_of_work'] = json_decode($row_val['technical_description_of_work']);
				$row_val['pincode'] = $encrptopenssl->decrypt($row_val['pincode']);		
				$row_val['flat_house_building_apt_company'] = $encrptopenssl->decrypt($row_val['flat_house_building_apt_company']);
				$row_val['area_colony_street_village'] = $encrptopenssl->decrypt($row_val['area_colony_street_village']);
				$row_val['town_city_and_state'] = $encrptopenssl->decrypt($row_val['town_city_and_state']);
				$row_val['country'] = $encrptopenssl->decrypt($row_val['country']);
				$row_val['student_id_proof'] = $encrptopenssl->decrypt($row_val['student_id_proof']);
				$row_val['specify_fields_area_that_you_would_like'] = $encrptopenssl->decrypt($row_val['specify_fields_area_that_you_would_like']);
				$row_val['bio_data'] = $encrptopenssl->decrypt($row_val['bio_data']);
				$row_val['years_of_experience'] = $encrptopenssl->decrypt($row_val['years_of_experience']);
				$row_val['no_of_paper_publication'] = $encrptopenssl->decrypt($row_val['no_of_paper_publication']);
				$row_val['paper_year'] = $encrptopenssl->decrypt($row_val['paper_year']);
				$row_val['paper_conf_name'] = $encrptopenssl->decrypt($row_val['paper_conf_name']);
				$row_val['paper_title'] = $encrptopenssl->decrypt($row_val['paper_title']);		
				$row_val['no_of_patents'] = $encrptopenssl->decrypt($row_val['no_of_patents']);
				$row_val['patent_year'] = $encrptopenssl->decrypt($row_val['patent_year']);
				$row_val['patent_number'] = $encrptopenssl->decrypt($row_val['patent_number']);
				$row_val['patent_title'] = $encrptopenssl->decrypt($row_val['patent_title']);
				$row_val['portal_name'] = $encrptopenssl->decrypt($row_val['portal_name']);
				$row_val['portal_link'] = $encrptopenssl->decrypt($row_val['portal_link']);
				$row_val['portal_description'] = $encrptopenssl->decrypt($row_val['portal_description']);
				$row_val['profile_picture'] = $encrptopenssl->decrypt($row_val['profile_picture']);
				$profile_info_arr[] = $row_val;
			}
			
		}

		$data['profile_information'] = $profile_info_arr;

		// echo "<pre>";
		// print_r($data);die;

		if(isset($_POST) && count($_POST) > 0)
		{
			// echo "<pre>";
			// print_r($this->input->post());die;
			//Check Validation
			$this->form_validation->set_rules('status', 'Status', 'required');
			$this->form_validation->set_rules('university_course[]', 'Course', 'required');
			$this->form_validation->set_rules('university_since_year[]', 'Since Year', 'required');
			$this->form_validation->set_rules('university_to_year[]', 'To Year', 'required');
			$this->form_validation->set_rules('university_name[]', 'Name', 'required');
			$this->form_validation->set_rules('university_location[]', 'Location', 'required');
			$this->form_validation->set_rules('current_study_course[]', 'Study Course', 'required');
			$this->form_validation->set_rules('current_study_since_year[]', 'Location', 'required');
			$this->form_validation->set_rules('current_study_to_year[]', 'Location', 'required');
			$this->form_validation->set_rules('current_study_description[]', 'Location', 'required');
			// $this->form_validation->set_rules('event_experience_in_year', 'Event Experience', 'required');
			// $this->form_validation->set_rules('event_description_of_work', 'Description of Work', 'required');
			// $this->form_validation->set_rules('technical_experience_in_year', 'Technical Experience', 'required');
			// $this->form_validation->set_rules('technical_description_of_work', 'Description of Work', 'required');
			$this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
			$this->form_validation->set_rules('flat_house_building_apt_company', 'Flat / House / Building / Apt / Company', 'required');
			$this->form_validation->set_rules('area_colony_street_village', 'Area / Colony/ Street / Village', 'required');
			$this->form_validation->set_rules('town_city_and_state', 'Town / City & State', 'required');
			$this->form_validation->set_rules('country', 'Country', 'required');

			if($this->form_validation->run())
			{
				// echo "<pre>";
				// print_r($this->input->post());die;	
				$type = $this->input->post('type');
				$status = $encrptopenssl->encrypt($this->input->post('status'));
				$university_course = $encrptopenssl->encrypt($this->input->post('university_course'));
				$university_course=json_encode($this->input->post('university_course[]'));
				$university_since_year=json_encode($this->input->post('university_since_year[]'));
				$university_to_year=json_encode($this->input->post('university_to_year[]'));
				$university_name=json_encode($this->input->post('university_name[]'));
				$university_location=json_encode($this->input->post('university_location[]'));
				$current_study_course = json_encode($this->input->post('current_study_course'));
				$current_study_since_year = json_encode($this->input->post('current_study_since_year'));
				$current_study_to_year = json_encode($this->input->post('current_study_to_year'));
				$current_study_description = json_encode($this->input->post('current_study_description'));
				$domain = $interestArea = $skillSets = [];
				$domain = $this->input->post('domain_and_area_of_expertise');
				$domainString = $encrptopenssl->encrypt(implode(',', $domain));
				$interestArea = $this->input->post('areas_of_interest');
				$AreaOfInterest = $encrptopenssl->encrypt(implode(',', $interestArea));
				$skillSets = $this->input->post('skill_sets');
				$SkillSets = $encrptopenssl->encrypt(implode(',', $skillSets));

				$event_experience_in_year = json_encode(array_filter($this->input->post('event_experience_in_year')));
				$event_description_of_work = json_encode(array_filter($this->input->post('event_description_of_work')));
				$technical_experience_in_year = json_encode(array_filter($this->input->post('technical_experience_in_year')));
				$technical_description_of_work = json_encode(array_filter($this->input->post('technical_description_of_work')));

				$pincode = $encrptopenssl->encrypt($this->input->post('pincode'));
				$flat_house_building_apt_company = $encrptopenssl->encrypt($this->input->post('flat_house_building_apt_company'));
				$area_colony_street_village = $encrptopenssl->encrypt($this->input->post('area_colony_street_village'));
				$town_city_and_state = $encrptopenssl->encrypt($this->input->post('town_city_and_state'));
				$country = $encrptopenssl->encrypt($this->input->post('country'));

				$student_id_proof  = $_FILES['student_id_proof']['name'];
				
				if($_FILES['student_id_proof']['name']!=""){			
					
					$config['upload_path']      = 'assets/id_proof';
					$config['allowed_types']    = '*';  
					$config['max_size']         = '5000';         
					$config['encrypt_name']     = TRUE;
								 
					$upload_files  = @$this->master_model->upload_file('student_id_proof', $_FILES, $config, FALSE);
				    $b_image = "";
					if(isset($upload_files[0]) && !empty($upload_files[0])){
						
						$b_image = $upload_files[0];
							
					}

					$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
					
					$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
									
					if(!array_key_exists($ext, $allowed)){
							
						$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
						$this->session->set_flashdata('error',$data['imageError']);
						redirect(base_url('profile/student'));	
					}
							
					$filesize = $_FILES['student_id_proof']['size'];
					// Verify file size - 5MB maximum
					$maxsize = 5 * 1024 * 1024;				   
				} else {
					$file_names = $data['profile_information'][0]['student_id_proof'];
				}
				
				$file_names = $encrptopenssl->encrypt($b_image[0]);
				
				
				$insertArr = array(
								'type' => $type,
								'status' => $status,
								'university_course' => $university_course,
								'university_since_year' => $university_since_year,
								'university_to_year' => $university_to_year,
								'university_name' => $university_name,
								'university_location' => $university_location,
								'current_study_course' => $current_study_course,
								'current_study_since_year' => $current_study_since_year,
								'current_study_to_year' => $current_study_to_year,
								'current_study_description' => $current_study_description,
								'domain_and_area_of_training_and_study' => $domainString,
								'areas_of_interest' => $AreaOfInterest,
								'skill_sets' => $SkillSets,
								'event_experience_in_year' => $event_experience_in_year,
								'event_description_of_work' => $event_description_of_work,
								'technical_experience_in_year' => $technical_experience_in_year,
								'technical_description_of_work' => $technical_description_of_work,
								'pincode' => $pincode,
								'flat_house_building_apt_company' => $flat_house_building_apt_company,
								'area_colony_street_village' => $area_colony_street_village,
								'town_city_and_state' => $town_city_and_state,
								'country' => $country,
								'student_id_proof' => $file_names,
								'user_id' => $userId,
								'updatedAt' => date('Y-m-d H:i:s')
							);

				$id = $this->input->post('profileId');
				$insertQuery ='';
				if(!empty($id)) {
					$updateQuery = $this->master_model->updateRecord('student_profile',$insertArr,array('id' => $id));
				}
				else {
					$insertQuery = $this->master_model->insertRecord('student_profile',$insertArr);
				}

				if($insertQuery > 0){
					$this->session->set_flashdata('success','Student Profile successfully created');
					redirect(base_url('profile/student'));
				} else if($updateQuery > 0){
					$this->session->set_flashdata('success','Student Profile Updated Successfully');
					redirect(base_url('profile/student'));
				} else {
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('profile/student'));
				}

			}
		}

        $data['page_title']='Student_profile';
		$data['middle_content']='student_profile/add';
		$this->load->view('front/front_combo',$data);
   	}

	public function claim_as_expert($value='')
   	{

   		$encrptopenssl =  New Opensslencryptdecrypt();
		$this->load->library('upload');
		 $user_id = $this->session->userdata('user_id');

		 				$this->db->select('user_category,sub_catname,title,first_name,middle_name,last_name,email,country_code,mobile,membership_type');
		 				$this->db->join('registration_usersubcategory','registration.user_sub_category_id=registration_usersubcategory.subcat_id');
		 				$this->db->join('registration_usercategory','registration.user_category_id=registration_usercategory.id');
		$user_data = $this->master_model->getRecords("registration",array('user_id'=>$user_id));
		// print_r($user_data);
		
		$user_arr = array();
		if(count($user_data)){	
						
			foreach($user_data as $row_val){		
						
				$row_val['title'] = $encrptopenssl->decrypt($row_val['title']);
				$row_val['first_name'] = $encrptopenssl->decrypt($row_val['first_name']);
				$row_val['middle_name'] = $encrptopenssl->decrypt($row_val['middle_name']);
				$row_val['last_name'] = $encrptopenssl->decrypt($row_val['last_name']);
				$row_val['email'] = $encrptopenssl->decrypt($row_val['email']);
				$row_val['mobile'] = $encrptopenssl->decrypt($row_val['mobile']);
				$row_val['membership_type'] = $encrptopenssl->decrypt($row_val['membership_type']);
				$row_val['user_category'] = $encrptopenssl->decrypt($row_val['user_category']);
				$row_val['sub_catname'] = $encrptopenssl->decrypt($row_val['sub_catname']);
				$user_arr[] = $row_val;
			}
			
		}

 
		$this->load->library('upload');

		

		$this->form_validation->set_rules('specify_fields_area_that_you_would_like[]', 'specify fields area that you would like to be an expert', 'required');
		$this->form_validation->set_rules('bio_data', 'Bio Data', 'required');
		$this->form_validation->set_rules('years_of_experience', 'Years of Experience', 'required');
		$this->form_validation->set_rules('no_of_paper_publication', 'No of Paper Publication', 'required');
		// $this->form_validation->set_rules('paper_year', 'Paper Year', 'required');
		// $this->form_validation->set_rules('paper_conf_name', 'Paper Conf. Name', 'required');
		// $this->form_validation->set_rules('paper_title', 'Paper Title', 'required');
		$this->form_validation->set_rules('no_of_patents', 'No of Patents', 'required');
		// $this->form_validation->set_rules('patent_year', 'Patent Year', 'required');
		// $this->form_validation->set_rules('patent_number', 'Patent Number', 'required');
		// $this->form_validation->set_rules('patent_title', 'Patent Title', 'required');
		$this->form_validation->set_rules('portal_name', 'Portal Name', 'required');
		$this->form_validation->set_rules('portal_link', 'Portal Link', 'required');
		$this->form_validation->set_rules('portal_description', 'Portal Description', 'required');



		if($this->form_validation->run())
		{	
			
			$type = $this->input->post('type');
		
			
			$specify_fields_area_that_you_would_like = json_encode($this->input->post('specify_fields_area_that_you_would_like'));
			$bio_data = $encrptopenssl->encrypt($this->input->post('bio_data'));
			$years_of_experience = $encrptopenssl->encrypt($this->input->post('years_of_experience'));
			
			$no_of_paper_publication = $this->input->post('no_of_paper_publication');
			
			$paper_year = $this->input->post('paper_year[]');
			$paper_conf_name = $this->input->post('paper_conf_name[]');
			$paper_title = $this->input->post('paper_title[]');
			
			$no_of_patents = $this->input->post('no_of_patents');
			
			$patent_year = $this->input->post('patent_year[]');
			$patent_number = $this->input->post('patent_number[]');
			$patent_title = $this->input->post('patent_title[]');
			
			$portal_name = $encrptopenssl->encrypt($this->input->post('portal_name'));
			$portal_link = $encrptopenssl->encrypt($this->input->post('portal_link'));
			$portal_description = $encrptopenssl->encrypt($this->input->post('portal_description'));




			/* $profile_picture  = $_FILES['profile_picture']['name'];
			
			if($_FILES['profile_picture']['name']!=""){			
				
				$config['upload_path']      = 'assets/profile_picture';
				$config['allowed_types']    = '*';  
				$config['max_size']         = '5000';         
				$config['encrypt_name']     = TRUE;
							 
				$upload_files  = @$this->master_model->upload_file('profile_picture', $_FILES, $config, FALSE);
			    $b_image = "";
				if(isset($upload_files[0]) && !empty($upload_files[0])){
					
					$b_image = $upload_files[0];
						
				}

				$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");	
				
				$ext = pathinfo($b_image[0], PATHINFO_EXTENSION);
								
				if(!array_key_exists($ext, $allowed)){
						
					$data['imageError'] = "Upload file in .jpg, .jpeg, .png format only";
					$this->session->set_flashdata('error',$data['imageError']);
					redirect(base_url('profile/individual_profile'));	
				}
						
				$filesize = $_FILES['profile_picture']['size'];
				// Verify file size - 5MB maximum
				$maxsize = 5 * 1024 * 1024;
				$file_names = $b_image[0];				   
			} // Student ID Proof Image End
			else {
				$file_names = $data['profile_information'][0]['profile_picture'];
			} */
			
	
				$insertArr = array(
							'user_id' => $user_id,
							'specify_fields_area_that_you_would_like' => $specify_fields_area_that_you_would_like,
							'bio_data' => $bio_data,
							'years_of_experience' => $years_of_experience,
							'no_of_paper_publication' => $no_of_paper_publication,
							'paper_year' => json_encode($paper_year),
							'paper_conf_name' => json_encode($paper_conf_name),
							'paper_title' => json_encode($paper_title),
							'no_of_patents' => $no_of_patents,
							'patent_year' => json_encode($patent_year),
							'patent_number' => json_encode($patent_number),
							'patent_title' => json_encode($patent_title),
							'portal_name' => $portal_name,
							'portal_link' => $portal_link,
							'portal_description' => $portal_description,
							'aplied_for_expert'=>'yes',
							'updatedAt' => date('Y-m-d H:i:s')
						);
			

			// echo "<pre>";
			// print_r($insertArr);
			// die;
			
			$id = $this->input->post('profileId');
			
			if(!empty($id)) {
				$updateQuery = $this->master_model->updateRecord('student_profile',$insertArr,array('id' => $id));
			}
			else {
				$insertQuery = $this->master_model->insertRecord('student_profile',$insertArr);
			}

			if($insertQuery > 0){
				$this->session->set_flashdata('success_claim_expert','Application submitted successfully created');
				redirect(base_url('profile'));
			} else if($updateQuery > 0){
				$this->session->set_flashdata('success_claim_expert','Application submitted successfully created');
				redirect(base_url('profile'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('profile/claim_as_expert'));
			}
		}
       
		$data['page_title']='Individual_profile';
		$data['profile_information'] =$this->profile_data($user_id);
		$data['user'] = $user_arr;
		$data['middle_content']='individual_profile/claim_as_expert';
		$this->load->view('front/front_combo',$data);
   		
   	}

   	public function changePassword()
   	{
   		$data['page_title']='change_password';
		$data['middle_content']='change_password/index';
		$this->load->view('front/front_combo',$data);
   	}


   	public function checkCurrentPassword()
   	{
   		$currentPassword = $this->input->post('current_password');
   		$user = $this->master_model->getRecords('registration',array('password'=>$currentPassword));
		if($user)
		{
			echo "true";
		}
		else
		{
			echo "false";
		}
   	}

   	public function storeNewPassword()
   	{
   		$userId = $this->session->userdata('user_id');
   		$newPassword = $this->input->post('new_password');
   		$confirmPassword = $this->input->post('confirm_new_password');
   		if($newPassword == $confirmPassword)
   		{
   			$insertArr = array('password' => $newPassword);
   			$updateQuery = $this->master_model->updateRecord('registration',$insertArr,array('user_id' => $userId));
   			if($updateQuery > 0)
   			{
   				$this->session->set_flashdata('success','Password Updated Successfully');
					redirect(base_url('Profile/changePassword'));
   			}
   		}
   		else
   		{
   			$this->session->set_flashdata('success','Something went wrong');
			redirect(base_url('Profile/changePassword'));
   		}	
   	}
}