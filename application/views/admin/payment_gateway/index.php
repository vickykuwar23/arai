<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Payment Gateway</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Setting</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Payment Gateway
			  </h3>
				  <a href="<?php echo base_url('xAdmin/payment_gateway') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } //echo "<pre>";print_r($setting_data);die();?>
				<div class="card-body">
					 <form method="post" id="payfrm" name="payfrm" role="form" >
						  <div class="row">
							<div class="col-6">
							 <div class="form-group">
								<label for="exampleInputEmail1">Live Key</label>
								<input type="text" class="form-control" id="live_key" placeholder="Enter Live Key" value="<?php echo $setting_data[0]['live_key']; ?>" name="live_key" >
								<span><?php echo form_error('live_key'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Live Secret Key</label>
								<input type="text" class="form-control" id="live_secret_key" placeholder="Enter Live Secret Key" value="<?php echo $setting_data[0]['live_secret_key']; ?>" name="live_secret_key" >
								<span><?php echo form_error('live_secret_key'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Live URL</label>
								<input type="text" class="form-control" id="live_url" placeholder="Enter Live URL" value="<?php echo $setting_data[0]['live_url']; ?>" name="live_url" >
								<span><?php echo form_error('live_url'); ?></span>
							  </div>	
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Test Key</label>
								<input type="text" class="form-control" id="test_key" placeholder="Enter Test Key" value="<?php echo $setting_data[0]['test_key']; ?>" name="test_key" >
								<span><?php echo form_error('test_key'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Test Secret Key</label>
								<input type="text" class="form-control" id="test_secret_key" placeholder="Enter Test Secret Key" value="<?php echo $setting_data[0]['test_secret_key']; ?>" name="test_secret_key" >
								<span><?php echo form_error('test_secret_key'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Test URL</label>
								<input type="text" class="form-control" id="test_url" placeholder="Enter Test URL" value="<?php echo $setting_data[0]['test_url']; ?>" name="test_url" >
								<span><?php echo form_error('test_url'); ?></span>
							  </div>
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Status</label>
								<div class="form-check">
								  <input class="form-check-input" type="radio" id="status" name="status" <?php if($setting_data[0]['status'] == 'Active'){ ?> checked="checked" <?php } ?> value="0">
								  <label for="customRadio1" class="form-check-label">Live Mode</label>
								</div>
								<div class="form-check">
								  <input class="form-check-input" type="radio" id="status" name="status" <?php if($setting_data[0]['status'] == 'Block'){ ?> checked="checked" <?php } ?> value="1">
								  <label for="customRadio2" class="form-check-label">Test Mode</label>
								</div>
								<span><?php echo form_error('test_url'); ?></span>
							  </div>
							  <div class="card-footer1">
									<button type="submit" class="btn btn-primary">Edit</button>							 
							</div>	
								
							</div>								
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#payfrm').validate({
    rules: {
      live_key: {
        required: true
      },
	  live_secret_key: {
        required: true
      },
	  live_url: {
        required: false,
		url:true
      },
	  test_key: {
        required: true
      },
	  test_secret_key: {
        required: true
      },
	  test_url: {
        required: false,
		url:true
      }
    },
    messages: {
      live_key: {
        required: "This field is required"
      },
	  live_secret_key: {
        required: "This field is required"
      },
	  live_url: {
        required: "This field is required", 
        url: "Enter URL with http:// or https://"		
      },
	  test_key: {
        required: "This field is required"
      },
	  test_secret_key: {
        required: "This field is required"
      },
	  test_url: {
		required: "This field is required", 
        url: "Enter URL with http:// or https://"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


