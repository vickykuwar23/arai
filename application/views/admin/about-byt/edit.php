<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">About BYT </h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">About BYT </li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit About BYT 
			  </h3>
				  <a href="<?php echo base_url('xAdmin/about_byt') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php  if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
          <form method="post" id="frm" name="frm" role="form" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Quote</label>
					<textarea id="quote_text" name="quote_text" placeholder="Enter Quote " class="form-control ckeditor"><?php echo $byt_data[0]['banner_quote']; ?></textarea>
                    <span><?php echo form_error('quote_text'); ?></span>
					<script>
						CKEDITOR.replace('quote_text');
					</script>	
                  </div>  
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">BYT Description</label>
                    <textarea id="byt_content" name="byt_content" placeholder="Please Enter Description" class="form-control"><?php echo $byt_data[0]['byt_content']; ?></textarea>
                    <span><?php echo form_error('byt_content'); ?></span>
					<script>
						CKEDITOR.replace('byt_content');
					</script>
                  </div>
                </div>
              </div>
			  <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Content One</label>
                    <textarea id="first_content" name="first_content" placeholder="Please Content" class="form-control"><?php echo $byt_data[0]['first_content']; ?></textarea>
                    <span><?php echo form_error('first_content'); ?></span>
					<script>
						CKEDITOR.replace('first_content');
					</script>
                  </div>
                </div>
              </div>
			  <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Content Two</label>
                    <textarea id="second_content" name="second_content" placeholder="Please Content" class="form-control"><?php echo $byt_data[0]['second_content']; ?></textarea>
                    <span><?php echo form_error('second_content'); ?></span>
					<script>
						CKEDITOR.replace('second_content');
					</script>
                  </div>
                </div>
              </div>
			  <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Content Three</label>
                    <textarea id="third_content" name="third_content" placeholder="Please Content" class="form-control"><?php echo $byt_data[0]['third_content']; ?></textarea>
                    <span><?php echo form_error('third_content'); ?></span>
					<script>
						CKEDITOR.replace('third_content');
					</script>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Banner Image</label>
                    <input type="file" class="form-control" id="banner_img" name="banner_img" >
                    <span><?php echo form_error('banner_img'); ?></span>
                  </div>  
                </div>
                <div class="col-md-6">
                    <img src="<?php echo base_url(); ?>uploads/byt/<?php echo $byt_data[0]['banner_img'] ?>" width="100px" height="100px" alt="Banner">
                </div>
              </div>          
                <div class="card-footer1">
                  <button type="submit" class="btn btn-primary">Update</button>               
                </div>
              
                <!-- /.card-body -->

               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
          </form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	
	
  $.validator.addMethod("valid_img_format", function(value, element)
	{	
		if(value != "")
		{
			var uploadType = $("#upload_type").val();
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");		
			var fileExt = value.toLowerCase();
			fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
			if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
		}else return true;
		
	});

	$.validator.addMethod('filesize', function (value, element, param) {
	 return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than 2 MB');



	jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var idname = $(element).attr('id');
			alert(idname);	
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
            if (ckValue.length === 0) {  
				//if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
				//If not empty then leave the value as it is  
                $(element).val(editor.getData());  
            }  
            return $(element).val().length > 0;  
        }, "This field is required"); 

		function GetTextFromHtml(html) {
			var dv = document.createElement("DIV");
			dv.innerHTML = html;
			return dv.textContent || dv.innerText || "";
		}
	
	
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#frm').validate({
    rules: {
      quote_text: {
		ckrequired: true
      },
      byt_content: {
		ckrequired: true
      },
      first_content: {
		ckrequired: true
      },
      second_content: {
		ckrequired: true
      },
      third_content: {
		ckrequired: true
      },
	  banner_img: {
        //required: true,
		valid_img_format: true, 
		filesize:2000000 
      }
		
    },
    messages: {
      quote_text: {
        required: "Please Enter Service Title",
        minlength: "Enter quote must be at least {0} characters long"
      },
      byt_content: {
        required: "Please Enter Description",
      },
      first_content: {
        required: "Please Enter Content One"
      },
      second_content: {
        required: "Please Enter Content Two"
      },
      third_content: {
        required: "Please Enter Content Third"
      },
	  banner_img: {
        //required: "This field is required",
		valid_img_format: "Please upload only .jpg, .jpeg, .png, .gif images."
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
