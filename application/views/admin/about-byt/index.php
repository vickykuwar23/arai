<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">About BYT</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">About BYT</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  About BYT List
              </h3>
                 <!--<a href="<?php echo base_url('xAdmin/about_byt/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>-->
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Banner Image</th>
                     <th>Quote</th>
					 <th>Description</th>
					 <th>Content One</th>
					 <th>Content Two</th>
					 <th>Content Three</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $byt) 
                     { 
					 $banner_quote =  strlen($byt['banner_quote']) >= 60 ? substr($byt['banner_quote'], 0, 57) . ' ...' : $byt['banner_quote'];					
					 $byt_content =  strlen($byt['byt_content']) >= 60 ? substr($byt['byt_content'], 0, 57) . ' ...' : $byt['byt_content'];					
					 $first_content =  strlen($byt['first_content']) >= 60 ? substr($byt['first_content'], 0, 57) . ' ...' : $byt['first_content'];					
					 $second_content =  strlen($byt['second_content']) >= 60 ? substr($byt['second_content'], 0, 57) . ' ...' : $byt['second_content'];	
					$third_content =  strlen($byt['third_content']) >= 60 ? substr($byt['third_content'], 0, 57) . ' ...' : $byt['third_content'];						
					 ?>
                  <tr>
                    <td style="width:5%"><?php echo $i; ?></td>
                    <td style="width:15%"><img src="<?php echo base_url('uploads/byt/'.$byt['banner_img']); ?>" height="100px" width="100px" /></td>
                    <td style="width:35%"><?php echo ucfirst($banner_quote); ?></td>
					<td style="width:15%"><?php echo ucfirst($byt_content); ?></td>	
					<td style="width:15%"><?php echo ucfirst($first_content); ?></td> 
					<td style="width:15%"><?php echo ucfirst($second_content); ?></td>
					<td style="width:15%"><?php echo ucfirst($third_content); ?></td>	
                    <td style="width:15%">
                        <a href="<?php echo base_url();?>xAdmin/about_byt/edit/<?php echo base64_encode($byt['a_id']);  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>
						 <a href="javascript:void(0);" data-id="<?php echo $byt['a_id'] ?>" class="view-more"><button class="btn btn-primary btn-sm ">View</button></a>
					</td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">About BYT</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents" style="overflow:auto;">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script>
  $("body").on("click", "#example1 tbody tr .view-more", function (e) { 			
			
		$('#exampleModal').modal('show');			
		var aid	=	$(this).attr('data-id');
		
		var base_url = '<?php echo base_url('xAdmin/about_byt/viewDetails'); ?>';
		 $.ajax({
			url: base_url,
			type: "post",
			data: {id:aid},				
			success: function (response) {					
				$("#contents").html(response);				
			},
			error: function(jqXHR, textStatus, errorThrown) {
			   console.log(textStatus, errorThrown);
			}
		});
		
	});		
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
    });
</script>