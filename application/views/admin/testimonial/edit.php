﻿<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Testimonials</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Testimonials</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Testimonials
			  </h3>
				  <a href="<?php echo base_url('xAdmin/testimonials') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="testimonialfrm" name="testimonialfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">	
							 <div class="form-group">
								<label for="exampleInputEmail1">Author Name</label>
								<input type="text" class="form-control" id="author_name" placeholder="Enter Author Name" name="author_name" value="<?php echo $testimonial_data[0]['author_name']; ?>">								
								<span><?php echo form_error('author_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Author Designation</label>
								<input type="text" class="form-control" id="author_designation" placeholder="Enter Author Designation" name="author_designation" value="<?php echo $testimonial_data[0]['author_designation']; ?>">								
								<span><?php echo form_error('author_designation'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">Profile Pic</label>
								<input type="file" class="form-control" id="profile_img" placeholder="Profile Pic" name="profile_img" >
								<span><?php echo form_error('profile_img'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Author Content</label>
								<textarea name="author_content" id="author_content" class="form-control" cols="20" rows="6"><?php echo $testimonial_data[0]['author_content']; ?></textarea>
								<span><?php echo form_error('profile_img'); ?></span>
							  </div>	
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  $.validator.addMethod("valid_img_format", function(value, element)
	{
		if(value != "")
		{
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
			var fileExt = value.toLowerCase();
			fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
			if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
		}else return true;
	});
  
  $('#testimonialfrm').validate({
    rules: {
      author_name: {
        required: true,
		minlength:2
      },
	  author_designation: {
        required: true,
		minlength:2
      },
	  author_content: {
        required: true,
		minlength:10
      },
	  profile_img: {       
		valid_img_format: true
      }
    },
    messages: {
      author_name: {
        required: "This field is required",
		minlength: "Enter Author Name must be at least {0} characters long"
      },
	  author_designation: {
        required: "This field is required",
		minlength: "Enter Author Designation must be at least {0} characters long"
      },
	  author_content: {
        required: "This field is required",
		minlength: "Enter Content must be at least {0} characters long"
      },
	  profile_img: {        
		valid_img_format:"Please upload only .png, .jpg, .gif and .jpeg image"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


