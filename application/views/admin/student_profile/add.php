<!-- Content Wrapper. Contains page content -->
<head>
  <style>
    .error{
      color: red;
    }
  </style>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Student Profile</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Student Profile</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Student Profile
			  </h3>
				  <a href="<?php echo base_url('xAdmin/student_profile') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="frm" name="frm" role="form" enctype="multipart/form-data">
						    <div class="row">
                  <input type="hidden" name="type" value="Student">
                  <div class="col-md-6">							   
                    <div class="form-group">
								      <label for="exampleInputEmail1">Status</label>
  								    <select class="form-control" name="status">
                        <option value="">Select Status</option>
                        <option value="UG">UG</option>
                        <option value="PG">PG</option>
                      </select>
								      <span class="error"><?php echo form_error('status'); ?></span>
                    </div>
                  </div>
                </div>
                <label>University Information</label>
                <div class="row">
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Course</label>
                      <input type="text" class="form-control" id="university_course" placeholder="Enter University Course" name="university_course" maxlength="200">
                      <span class="error"><?php echo form_error('university_course'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Since Year</label>
                      <input type="date" class="form-control" id="university_since_year" placeholder="Select University Since Year" name="university_since_year" maxlength="200">
                      <span class="error"><?php echo form_error('university_since_year'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">To Year</label>
                      <input type="date" class="form-control" id="university_to_year" placeholder="Select University To Year" name="university_to_year" maxlength="200">
                      <span class="error"><?php echo form_error('university_to_year'); ?></span>
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
                      <input type="text" class="form-control" id="university_name" placeholder="Enter University Name" name="university_name" maxlength="200">
                      <span class="error"><?php echo form_error('university_name'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Location</label>
                      <input type="text" class="form-control" id="university_location" placeholder="Select University Location" name="university_location" maxlength="200">
                      <span class="error"><?php echo form_error('university_location'); ?></span>
                    </div>  
                  </div>
                </div>
                <hr>
                <label>Current Fields of Study</label>
                <div class="row">
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Course</label>
                      <input type="text" class="form-control" id="current_study_course" placeholder="Enter Current Study Course" name="current_study_course" maxlength="200">
                      <span class="error"><?php echo form_error('current_study_course'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Since Year</label>
                      <input type="date" class="form-control" id="current_study_since_year" placeholder="Select Since Year" name="current_study_since_year" maxlength="200">
                      <span class="error"><?php echo form_error('current_study_since_year'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-4">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">To Year</label>
                      <input type="date" class="form-control" id="current_study_to_year" placeholder="Select to Year" name="current_study_to_year" maxlength="200">
                      <span class="error"><?php echo form_error('current_study_to_year'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description</label>
                      <textarea name="current_study_description" class="form-control" id="current_study_description"></textarea>
                      <span class="error"><?php echo form_error('current_study_description'); ?></span>
                    </div>
                  </div>
                </div>
                <hr> 
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Domain and Area of Training and Study</label>
                      <select name="domain_and_area_of_training_and_study[]" class="domainName form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="Radiator">Radiator</option>
                        <option value="Refrigerator">Refrigerator</option>
                        <option value="Blowers">Blowers</option>
                      </select>
                      <span class="error"><?php echo form_error('domain_and_area_of_training_and_study'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Areas of Interest</label>
                      <select name="areas_of_interest[]" class="areaOfInterestName form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="Programming">Programming</option>
                        <option value="Designing">Designing</option>
                        <option value="Testing">Testing</option>
                        <option value="Drawing">Drawing</option>
                      </select>
                      <span class="error"><?php echo form_error('areas_of_interest'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Skill Sets</label>
                      <select name="skill_sets[]" class="skillSets form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="JAVA">JAVA</option>
                        <option value="PHP">PHP</option>
                        <option value="PYTHON">PYTHON</option>
                        <option value="CPP">CPP</option>
                      </select>
                      <span class="error"><?php echo form_error('skill_sets'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <label>Event Experience</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Year</label>
                      <input type="text" class="form-control" id="event_experience_in_year" placeholder="Enter Event Experience in Year" name="event_experience_in_year" maxlength="200">
                      <span class="error"><?php echo form_error('event_experience_in_year'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description of Work</label>
                      <textarea name="event_description_of_work" class="form-control" id="event_description_of_work"></textarea>
                      <span class="error"><?php echo form_error('event_description_of_work'); ?></span>
                    </div> 
                  </div>
                </div>
                <hr>
                <label>Technical Experience</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Year</label>
                      <input type="text" class="form-control" id="technical_experience_in_year" placeholder="Enter Technical Experience in Year" name="technical_experience_in_year" maxlength="200">
                      <span class="error"><?php echo form_error('technical_experience_in_year'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description of Work</label>
                      <textarea name="technical_description_of_work" class="form-control" id="technical_description_of_work"></textarea>
                      <span class="error"><?php echo form_error('technical_description_of_work'); ?></span>
                    </div> 
                  </div>
                </div>
                <hr>
                <label>Billing Address</label>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Pincode</label>
                      <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="pincode" maxlength="200">
                      <span class="error"><?php echo form_error('pincode'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Flat / House No. / Building / Apt / Company</label>
                      <input type="text" class="form-control" id="flat_house_building_apt_company" placeholder="Enter Flat / House No. / Building / Apt / Company" name="flat_house_building_apt_company" maxlength="200">
                      <span class="error"><?php echo form_error('flat_house_building_apt_company'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Area / Colony / Street / Village</label>
                      <input type="text" class="form-control" id="area_colony_street_village" placeholder="Enter Area / Colony / Street / Village" name="area_colony_street_village" maxlength="200">
                      <span class="error"><?php echo form_error('area_colony_street_village'); ?></span>
                    </div> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Town / City and State</label>
                      <input type="text" class="form-control" id="town_city_and_state" placeholder="Enter Town / City & State" name="town_city_and_state" maxlength="200">
                      <span class="error"><?php echo form_error('town_city_and_state'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Country</label>
                      <input type="text" class="form-control" id="country" placeholder="Enter Country" name="country" maxlength="200">
                      <span class="error"><?php echo form_error('country'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Proof of Affiliation (Student ID)</label>
                      <input type="file" class="form-control" id="student_id_proof" placeholder="Upload Student ID Proof" name="student_id_proof" >
                      <span><?php echo form_error('student_id_proof'); ?></span>
                    </div>
                  </div>
                </div>
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							
								<!-- /.card-body -->
						  
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $('.domainName').select2({
    placeholder : "Please Select Domain and Area of Training and Study",
    closeOnSelect : false
  });
  $('.areaOfInterestName').select2({
    placeholder : "Please Select Areas of Interest",
    closeOnSelect : false
  });
  $('.skillSets').select2({
    placeholder : "Please Select Skill Sets",
    closeOnSelect : false
  });
</script>


