<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">News & Updates</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">News & Updates</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add News & Updates
			  </h3>
				  <a href="<?php echo base_url('xAdmin/blog') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="blogfrm" name="blogfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-12">	
							 <div class="form-group">
								<label for="exampleInputEmail1">News Title</label>
								<input type="text" class="form-control" id="blog_name" placeholder="Enter News Title " name="blog_name" value="">								
								<span><?php echo form_error('blog_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">News Description</label>
								<textarea class="form-control" id="blog_desc ckeditor" placeholder="Enter News Description " name="blog_desc" rows="6" cols="12"></textarea>								
								<script>
									CKEDITOR.replace('blog_desc');
								</script>
								<span><?php echo form_error('blog_desc'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">News URL</label>
								<input type="text" class="form-control" id="blog_url" placeholder="Enter News URL " name="blog_url" value="">								
								<span><?php echo form_error('blog_url'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">News Image (Upload only jpg, jpeg ,gif & png format only)</label>
								<input type="file" class="form-control" id="blog_img" placeholder="Upload News" name="blog_img" >
								
							  </div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  $.validator.addMethod("valid_img_format", function(value, element)
	{
		if(value != "")
		{
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
			var fileExt = value.toLowerCase();
			fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
			if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
		}else return true;
	});
	
	$.validator.addMethod('filesize', function (value, element, param) {
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		
	jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
            if (ckValue.length === 0) {  
				//if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
				//If not empty then leave the value as it is  
                $(element).val(editor.getData());  
            }  
            return $(element).val().length > 0;  
        }, "This field is required"); 

		function GetTextFromHtml(html) {
			var dv = document.createElement("DIV");
			dv.innerHTML = html;
			return dv.textContent || dv.innerText || "";
		}	
		
		
  $('#blogfrm').validate({
    rules: {
      blog_name: {
        required: true,
		minlength:5
      },
	  blog_desc: {
        ckrequired: true
      },
	  blog_url: {
        required: true,
		url:true
      },
	  blog_img: {
        valid_img_format: true, 
		filesize:2000000
		//maxsize: 5000000
      }
    },
    messages: {
      blog_name: {
        required: "This field is required",
		minlength: "Enter News Title must be at least {0} characters long"
      },
	  blog_desc: {
        required: "This field is required"
      },
	  blog_url: {
        required: "This field is required",
		url: "Enter News url in proper format using http:// or https://"
      },
	  blog_img: {
        valid_img_format: "Upload Only .jpg, .jpeg, .png or .gif image only",
		filesize: "Upload below 2MB image only."
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


