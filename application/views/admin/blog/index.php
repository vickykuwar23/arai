<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">News & Updates</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">News & Updates</li>
					</ol>
				</div>
				
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<?php if( $this->session->flashdata('success')){ ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5><i class="icon fas fa-check"></i> Success!</h5>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>
			<div class="card ">
				<div class="card-header">
					<h3 class="card-title">
						News & Updates
					</h3>
					<a href="<?php echo base_url('xAdmin/blog/add') ?>" class="btn btn-primary btn-sm float-right">Add News & Updates</a>
					
				</div>
				
				<!-- Small boxes (Stat box) -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No.</th>
								<th>News Title</th>
								<th>News Description</th>
								<th>News URL</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  
								$i=1;
								foreach($records as $blog_detail) 
								{ ?>
								<tr>
									<td style="width:5%"><?php echo $i; ?></td>
									<td style="width:25%"><?php echo $blog_detail['blog_name'] ; ?></td>
									<td style="width:40%"><?php echo character_limiter(strip_tags($blog_detail['blog_desc']),200); ?></td>
									<td style="width:15%"><?php echo $blog_detail['blog_url'] ; ?></td>	
									<td style="width:25%">
										<a href="<?php echo base_url();?>xAdmin/blog/details/<?php echo base64_encode($blog_detail['id']);  ?>" class="con_view"><button class="btn btn-warning btn-sm">View</button></a>
										<a href="<?php echo base_url();?>xAdmin/blog/edit/<?php echo $blog_detail['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
										<!--<a href="<?php echo base_url();?>xAdmin/blog/delete/<?php echo $blog_detail['id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>-->
										
										<?php      
											if($blog_detail['status']=="Active"){
												$status = 'Block';
											?>
											<a href="<?php echo base_url(); ?>xAdmin/blog/changeStatus/<?php echo $blog_detail['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
											else if($blog_detail['status']=="Block"){
												$status = 'Active';
											?>
											<a href="<?php echo base_url(); ?>xAdmin/blog/changeStatus/<?php echo $blog_detail['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
										?>
										
										<?php      
											if($blog_detail['is_featured']=="Y"){							
											?>
											<a href="javascript:void(0);" data-id="<?php echo $blog_detail['id'] ?>" data-status="N" class="featured-chk btn btn-primary btn-sm" id="featured-<?php echo $blog_detail['id'] ?>">Featured </a><?php }
											else if($blog_detail['is_featured']=="N"){
												
											?>
											<a href="javascript:void(0);" class="featured-chk btn btn-primary btn-sm" data-id="<?php echo $blog_detail['id'] ?>" data-status="Y" class="featured-chk" id="featured-<?php echo $blog_detail['id'] ?>">Not-Featured</a><?php }
										?>
									</td>
								</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
					<!-- ./col -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		$("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		   "bStateSave": true,
		});
		
		$(document).on('click','.featured-chk',function(){
			var status = $(this).attr('data-status'); //$("#ch_id").val();
			var ID = $(this).attr('data-id');
			var cs_t = 	$('.token').val();
			//alert(status+"=="+ID);return false;
			var base_url = '<?php echo base_url() ?>';
			$.ajax({
				type:'POST',
				url: base_url+'xAdmin/blog/markfeatured',
				data:'status='+status+'&id='+ID+'&csrf_test_name='+cs_t,				
				success:function(data){
					var output = JSON.parse(data);
					var status = output.u_featured;	
					var newStatus = output.changeVal;	
					$(".token").val(output.token);
					$("#featured-"+ID).html(status);
					$("#featured-"+ID).data('status', newStatus);
					console.log($("#featured-"+ID).data("status"));
					window.location.reload();
					swal({
						title: 'Success!',
						text: "News featured status updated successfully.",
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				}
			});
			
		});	
		
	});
</script>