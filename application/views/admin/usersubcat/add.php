<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Sub-Categories</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Sub-Categories</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add User Sub-Categories
			  </h3>
				  <a href="<?php echo base_url('xAdmin/usersubcat') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="subcatfrm" name="subcatfrm" role="form" >
						  <div class="row">
							<div class="col-6">
							 <div class="form-group">
								<label for="exampleInputEmail1">User Category Name</label>
								<select name="catname" id="catname" class="form-control" >
								<option value="">-- Select Category --</option>
								<?php 
								if(count($usertype_cat) > 0){
									foreach($usertype_cat as $catdetails){
								?>
								<option value="<?php echo $catdetails['id'] ?>"><?php echo $catdetails['user_category'] ?></option>	
								<?php 
									} // For End
								} // If End
								?>
								</select>								
								<span><?php echo form_error('catname'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">User Sub-Categories</label>
								<input type="text" class="form-control" id="subcatname" placeholder="Enter User Sub-Categories" name="subcatname" maxlength="200" required>
								<span><?php echo form_error('subcatname'); ?></span>
							  </div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#subcatfrm').validate({
    rules: {
	  catname: {
        required: true
      },	
      subcatname: {
        required: true,
		minlength:4
      },
    },
    messages: {
	catname: {
        required: "This field is required"
      },
      subcatname: {
        required: "This field is required",
		minlength: "Enter User Sub-Categories must be at least {0} characters long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


