<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Expert Query</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Expert Query</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
		  <div class="card-header">
			  <h3 class="card-title">
				  Expert Query Listing
			  </h3>
		  </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                      <th>ID</th>
                     <th>Fullname</th>
					 <th>Email ID</th>
					 <th>Subject</th>
					 <th>Question</th>
					 <th>Remark</th>
					 <th>Expert Name</th>
					 <th nowrap>Added On</th>
					 <th>Action</th>
					 <!--<th>Content</th>
                     <th>Action</th>-->
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $exp_query) 
                     { 
					 $encrptopenssl =  New Opensslencryptdecrypt();
					$fullname = ucfirst($encrptopenssl->decrypt($exp_query['first_name']))." ".ucfirst($encrptopenssl->decrypt($exp_query['middle_name']))." ".ucfirst($encrptopenssl->decrypt($exp_query['last_name']));
					$expert_fullname='-';
					if ($exp_query['connect_to_expert']!='') {
						$expert_fullname = ucfirst($encrptopenssl->decrypt($exp_query['ToFname']))." ".ucfirst($encrptopenssl->decrypt($exp_query['ToMname']))." ".ucfirst($encrptopenssl->decrypt($exp_query['ToLname']));
					}
					
					$expert_subject =  strlen($exp_query['expert_subject']) >= 27 ? substr($exp_query['expert_subject'], 0, 30) . ' ...' : $exp_query['expert_subject'];					
					$expert_question =  strlen($exp_query['expert_question']) >= 40 ? substr($exp_query['expert_question'], 0, 37) . ' ...' : $exp_query['expert_question'];
					$expert_remark =  strlen($exp_query['expert_remark']) >= 70 ? substr($exp_query['expert_remark'], 0, 67) . ' ...' : $exp_query['expert_remark'];						
					?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
					 <td style="width:5%"><?php echo $exp_query['user_id']; ?></td>
           <td style="width:15%"><?php echo $fullname; ?></td> 
					 <td style="width:10%"><?php echo $encrptopenssl->decrypt($exp_query['email']); ?></td> 
					 <td style="width:20%"><?php echo $expert_subject; ?></td>
					 <td style="width:20%"><?php echo $expert_question; ?></td>
					 <td style="width:20%"><?php echo $expert_remark; ?></td>
					 <td style="width:20%"><?php echo $expert_fullname; ?></td>
					 
					<td style="width:10%" nowrap><?php echo $exp_query['createdAt']; ?></td>
					<td><a href="javascript:void(0);" class="btn btn-primary expert-popup" data-id="<?php  echo $exp_query['id'] ?>">View</a></td>	
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                                                                                    
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Expert Query</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$("body").on("click", "#example1 tbody tr .expert-popup", function (e) { 			
			//var priorityVal = $(this).val();	
			//$('#exampleModal').html(response).modal('show');
			$('#exampleModal').modal('show');			
			var eid	=	$(this).attr('data-id');
			var base_url = '<?php echo base_url('xAdmin/expert_query/viewDetails'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:eid},				
				success: function (response) {					
					$("#contents").html(response);				
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
		});	
    });
</script>