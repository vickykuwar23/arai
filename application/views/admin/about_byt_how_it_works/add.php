<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">BYT How It Works</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">BYT How It Works</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add How It Works
			  </h3>
				  <a href="<?php echo base_url('xAdmin/how_it_works') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div>
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="frm" name="frm" role="form" enctype="multipart/form-data">
						  <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Subject Title</label>
                    <input type="text" class="form-control" id="subject_title" placeholder="Enter Subject Title" name="subject_title" maxlength="50">
                    <span><?php echo form_error('subject_title'); ?></span>
                  </div>
                </div>
                <div class="col-md-6">
                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">				   
  							  <div class="form-group">
                    <label for="exampleInputEmail1">First Title</label>
  								  <input type="text" class="form-control" id="first_title" placeholder="Enter First Title" name="first_title" maxlength="50">
  								  <span><?php echo form_error('first_title'); ?></span>
  							  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Description</label>
                    <textarea name="first_description" placeholder="Enter First Description" maxlength="150" class="form-control"></textarea>
                    <span><?php echo form_error('first_description'); ?></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Icon Image</label>
                    <input type="file" class="form-control" id="first_icon_image" name="first_icon_image" maxlength="21">
                    <span><?php echo form_error('first_icon_image'); ?></span>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">           
                  <div class="form-group">
                    <label for="exampleInputEmail1">Second Title</label>
                    <input type="text" class="form-control" id="second_title" placeholder="Enter Second Title" name="second_title" maxlength="50">
                    <span><?php echo form_error('second_title'); ?></span>
                </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Second Description</label>
                    <textarea name="second_description" placeholder="Enter Second Description" maxlength="150" class="form-control"></textarea>
                    <span><?php echo form_error('second_description'); ?></span>
                </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Second Icon Image</label>
                    <input type="file" class="form-control" id="second_icon_image" name="second_icon_image" maxlength="21">
                    <span><?php echo form_error('second_icon_image'); ?></span>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">           
                  <div class="form-group">
                    <label for="exampleInputEmail1">Third Title</label>
                    <input type="text" class="form-control" id="third_title" placeholder="Enter Third Title" name="third_title" maxlength="50">
                    <span><?php echo form_error('third_title'); ?></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Third Description</label>
                    <textarea name="third_description" placeholder="Enter Third Description" maxlength="150" class="form-control"></textarea>
                    <span><?php echo form_error('third_description'); ?></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Third Icon Image</label>
                    <input type="file" class="form-control" id="third_icon_image" name="third_icon_image" maxlength="21">
                    <span><?php echo form_error('third_icon_image'); ?></span>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Background Image</label>
                    <input type="file" class="form-control" id="background_image" name="background_image" maxlength="21">
                    <span><?php echo form_error('background_image'); ?></span>
                  </div>
                </div>
              </div>					  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#frm').validate({
    rules: {
      subject_title: {
        required: true,
        minlength:3
      },
      first_title: {
        required: true,
		    minlength:3
      },
      first_description: {
        required: true,
      },
      first_icon_image: {
        required: true,
      },
      second_title: {
        required: true,
        minlength:3
      },
      second_description: {
        required: true,
      },
      second_icon_image: {
        required: true,
      },
      third_title: {
        required: true,
        minlength:3
      },
      third_description: {
        required: true,
      },
      third_icon_image: {
        required: true,
      },
      background_image: {
        required: true,
      },
    },
    messages: {
      subject_title: {
        required: "Please Enter Subject Title",
        minlength: "Enter Subject Title must be at least {0} characters long"
      },
      first_title: {
        required: "Please Enter First Title",
		    minlength: "Enter First Title must be at least {0} characters long"
      },
      first_description: {
        required: "Please Enter First Description",
      },
      first_icon_image: {
        required: "Please Enter First Icon Image",
      },
      second_title: {
        required: "Please Enter Second Title",
        minlength: "Enter Second Title must be at least {0} characters long"
      },
      second_description: {
        required: "Please Enter Second Description",
      },
      second_icon_image: {
        required: "Please Enter Second Icon Image",
      },
      third_title: {
        required: "Please Enter Third Title",
        minlength: "Enter Third Title must be at least {0} characters long"
      },
      third_description: {
        required: "Please Enter Third Description",
      },
      third_icon_image: {
        required: "Please Enter Third Icon Image",
      },
      background_image: {
        required: "Please Enter Background Image",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>