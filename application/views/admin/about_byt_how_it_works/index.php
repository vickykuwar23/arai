<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">BYT How It Works</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">BYT How It Works</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  BYT How It Works List
              </h3>
                 <a href="<?php echo base_url('xAdmin/about_how_it_work/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Subject Title</th>
                     <th>First Title</th>
                     <th>First Description</th>
                     <th>Second Title</th>
                     <th>Second Description</th>
                     <th>Third Title</th>
                     <th>Third Description</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $HowItWorks) 
                     { ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:10%"><?php echo $HowItWorks['subject_title'] ; ?></td>
                     <td style="width:10%"><?php echo $HowItWorks['first_title'] ; ?></td>
                     <td style="width:15%"><?php echo $HowItWorks['first_description'] ; ?></td>
                     <td style="width:10%"><?php echo $HowItWorks['second_title'] ; ?></td>
                     <td style="width:15%"><?php echo $HowItWorks['second_description'] ; ?></td>
                     <td style="width:10%"><?php echo $HowItWorks['third_title'] ; ?></td>
                     <td style="width:15%"><?php echo $HowItWorks['third_description'] ; ?></td>
                     <td style="width:10%">
						
                        <a href="<?php echo base_url();?>xAdmin/about_how_it_work/edit/<?php echo $HowItWorks['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
                        <!--<a href="<?php echo base_url();?>xAdmin/usercategories/delete/<?php echo $technology['id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>-->
						
						<?php      
						if($HowItWorks['status']=="Active"){
							$status = 'Block';
						?>
                        <a href="<?php echo base_url(); ?>xAdmin/about_how_it_work/changeStatus/<?php echo $HowItWorks['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                           else if($HowItWorks['status']=="Block"){
							$status = 'Active';
						   ?>
                        <a href="<?php echo base_url(); ?>xAdmin/about_how_it_work/changeStatus/<?php echo $HowItWorks['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                           ?>
						
						
					 </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
    });
</script>