<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Banner Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Banner Management</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Banner Management
			  </h3>
				  <a href="<?php echo base_url('xAdmin/banner') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				   
				<div class="card-body">
					 <form method="post" id="bannerfrm" name="bannerfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">	
							 <div class="form-group">
								<label for="exampleInputEmail1">Banner Title</label>
								<input type="text" class="form-control" id="banner_name" placeholder="Enter Banner Title " name="banner_name" value="<?php echo $banner_data[0]['banner_name']; ?>">								
								<span><?php echo form_error('banner_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Banner Sub Title</label>
								<input type="text" class="form-control" id="banner_sub" placeholder="Enter Banner Sub Title " name="banner_sub" value="<?php echo $banner_data[0]['banner_sub']; ?>">								
								<span><?php echo form_error('banner_sub'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Banner Description</label>
								<textarea class="form-control" id="banner_desc" placeholder="Enter Banner Description " name="banner_desc" rows="6" cols="12"><?php echo $banner_data[0]['banner_desc']; ?></textarea>								
								<span><?php echo form_error('banner_desc'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">Banner URL</label>
								<input type="text" class="form-control" id="banner_url" placeholder="Enter Banner URL " name="banner_url" value="<?php echo $banner_data[0]['banner_url']; ?>">								
								<span><?php echo form_error('banner_url'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">Upload Banner</label>
								<input type="file" class="form-control" id="banner_img" placeholder="Upload Banner" name="banner_img" >
								<span><?php //echo form_error('banner_img'); ?></span>
							  </div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#bannerfrm').validate({
    rules: {
      banner_name: {
        required: true,
		minlength:5
      },
	  banner_sub: {
        required: true,
		minlength:3
      },
	  banner_desc: {
        required: true
      },
	  banner_url: {
        required: true,
		url:true
      }
    },
    messages: {
      banner_name: {
        required: "This field is required",
		minlength: "Enter Banner Name must be at least {0} characters long"
      },
	  banner_desc: {
        required: "This field is required"
      },
	  banner_url: {
        required: "This field is required",
		url: "Enter Banner url in proper format."
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


