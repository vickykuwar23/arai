<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Details</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">User Details</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <!--<div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/user4-128x128.jpg"
                       alt="User profile picture">
                </div>-->

                <h3 class="profile-username text-center"><?php echo $fullname = $user_data[0]['title']."&nbsp;".ucfirst($user_data[0]['first_name'])."&nbsp;".ucfirst($user_data[0]['middle_name'])."&nbsp;".ucfirst($user_data[0]['last_name']); ?></h3>
                <!--<p class="text-muted text-center">Membership Type : <b><?php echo $user_data[0]['membership_type'] ?></b></p>
				<p class="text-muted text-center"><b><?php echo $user_data[0]['user_category']."&nbsp;-&nbsp;".$user_data[0]['sub_catname'] ?></b></p>-->

                <!--<ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!--<strong><i class="fas fa-book mr-1"></i> Education</strong>
                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
                <hr>-->

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted"><?php echo ucfirst($user_data[0]['town_city_and_state']) ?>, <?php echo ucfirst($user_data[0]['country']) ?></p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                <p class="text-muted">
				<?php 
					$skills = $user_data[0]['skill_sets'];
					$explode = explode(",", $skills);
					foreach($explode as $aquireskills){						
					
				?>	
                 
                  <span class="btn btn-success btn-sm"><?php echo $aquireskills; ?></span>
				  <!--<span class="tag tag-danger">UI Design</span>
                  <span class="tag tag-info">Javascript</span>
                  <span class="tag tag-warning">PHP</span>
                  <span class="tag tag-primary">Node.js</span>-->
				  <?php } ?>
                </p>

                <!--<hr>
                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>
                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
				-->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Personal Details</a></li>                  
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">University Details</a></li>
				  <li class="nav-item"><a class="nav-link" href="#experience" data-toggle="tab">Experience Details</a></li>
				  <li class="nav-item"><a class="nav-link" href="#expert" data-toggle="tab">Expert Details</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">                     
						<p class="text-muted"><b>Membership Type : </b><?php echo $user_data[0]['membership_type'] ?></p>
						<p class="text-muted"><b>Category Name : </b><?php echo ucwords($user_data[0]['user_category']) ?></p>                    
						<p class="text-muted"><b>Subcategory Name : </b><?php echo ucwords($user_data[0]['sub_catname']) ?></p> 
						<p class="text-muted"><b>Email ID : </b><?php echo $user_data[0]['email'] ?></p> 
						<p class="text-muted"><b>Mobile No. : </b><?php echo $user_data[0]['country_code']."-".$user_data[0]['mobile'] ?></p> 	
						<p class="text-muted"><b>Address : </b><?php echo ucwords($user_data[0]['area_colony_street_village']) ?></p>						
						<p class="text-muted"><b>Pincode : </b><?php echo $user_data[0]['pincode'] ?></p>
					</div>
                    <!-- /.post -->
					
                    <!--<div class="post clearfix">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
                        <span class="username">
                          <a href="#">Sarah Ross</a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Sent you a message - 3 days ago</span>
                      </div>
                    
                      <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                      </p>

                      <form class="form-horizontal">
                        <div class="input-group input-group-sm mb-0">
                          <input class="form-control form-control-sm" placeholder="Response">
                          <div class="input-group-append">
                            <button type="submit" class="btn btn-danger">Send</button>
                          </div>
                        </div>
                      </form>
                    </div>-->
                    <!-- /.post -->                    
                  </div>
                  <!-- /.tab-pane -->
                  <!--<div class="tab-pane" id="timeline">
                    
                    <div class="timeline timeline-inverse">
						<p class="text-muted"><b>University Name : </b><?php echo $user_data[0]['university_name'] ?></p>
                        <p class="text-muted"><b>Course Name : </b><?php echo $user_data[0]['university_course'] ?></p>
						<p class="text-muted"><b>From Year : </b><?php echo $user_data[0]['university_since_year'] ?></p>
						<p class="text-muted"><b>To Year : </b><?php echo $user_data[0]['university_to_year'] ?></p>						
						<p class="text-muted"><b>Location : </b><?php echo $user_data[0]['university_location'] ?></p>
						<p class="text-muted"><b>Current Study Course : </b><?php echo $user_data[0]['current_study_course'] ?></p>	
						<p class="text-muted"><b>Location : </b><?php echo $user_data[0]['university_location'] ?></p>
						<p class="text-muted"><b>Current Study Since Year : </b><?php echo $user_data[0]['current_study_since_year'] ?></p>
						<p class="text-muted"><b>Current Study To Year : </b><?php echo $user_data[0]['current_study_to_year'] ?></p>
						<p class="text-muted"><b>Details : </b><?php echo $user_data[0]['current_study_description'] ?></p>
						<p class="text-muted"><b>Training & Study : </b><?php echo $user_data[0]['domain_and_area_of_training_and_study'] ?></p>
					</div>
                  </div>-->
                  

					<div class="tab-pane" id="settings">
						<p class="text-muted"><b>University Name : </b><?php echo ucwords($user_data[0]['university_name']) ?></p>
                        <p class="text-muted"><b>Course Name : </b><?php echo strtoupper($user_data[0]['university_course']) ?></p>
						<p class="text-muted"><b>From Year : </b><?php echo $user_data[0]['university_since_year'] ?></p>
						<p class="text-muted"><b>To Year : </b><?php echo $user_data[0]['university_to_year'] ?></p>						
						<p class="text-muted"><b>Location : </b><?php echo $user_data[0]['university_location'] ?></p>
						<p class="text-muted"><b>Current Study Course : </b><?php echo strtoupper($user_data[0]['current_study_course']) ?></p>	
						<p class="text-muted"><b>Location : </b><?php echo ucwords($user_data[0]['university_location']) ?></p>
						<p class="text-muted"><b>Current Study Since Year : </b><?php echo $user_data[0]['current_study_since_year'] ?></p>
						<p class="text-muted"><b>Current Study To Year : </b><?php echo $user_data[0]['current_study_to_year'] ?></p>
						<p class="text-muted"><b>Details : </b><?php echo ucfirst($user_data[0]['current_study_description']) ?></p>
						<p class="text-muted"><b>Training & Study : </b><?php echo ucfirst($user_data[0]['domain_and_area_of_training_and_study']) ?></p>
					</div>
					
					<div class="tab-pane" id="experience">
						<p class="text-muted"><b>Experience (in Years) : </b><?php echo $user_data[0]['event_experience_in_year'] ?></p>
                        <p class="text-muted"><b>Description : </b><?php echo ucfirst($user_data[0]['event_description_of_work']) ?></p>
						<p class="text-muted"><b>Technical Experience (in Years) : </b><?php echo $user_data[0]['technical_experience_in_year'] ?></p>
						<p class="text-muted"><b>Technical Description : </b><?php echo ucfirst($user_data[0]['technical_description_of_work']) ?></p>						
												
					</div>
					
					<div class="tab-pane" id="expert">
						<form name="frm" id="frm" method="post">
							<!--<div class="form-group">
								<p class="text-muted">Image</p>	
								<p class="text-muted"><b>Specific Fields Area : </b><?php echo $user_data[0]['specify_fields_area_that_you_would_like'] ?></p>	
								<p class="text-muted"><b>Bio Data : </b><?php echo $user_data[0]['bio_data'] ?></p>	
								<p class="text-muted"><b>Year OF Experience : </b><?php echo $user_data[0]['years_of_experience'] ?></p>	
								<p class="text-muted"><b>No Of Publication : </b><?php echo $user_data[0]['no_of_paper_publication'] ?></p>	
								<p class="text-muted"><b>Paper Area : </b><?php echo $user_data[0]['paper_year'] ?></p>	
								<p class="text-muted"><b>Paper Conf. Name : </b><?php echo $user_data[0]['paper_conf_name'] ?></p>	
								<p class="text-muted"><b>Sub Cat Name : </b><?php echo $user_data[0]['sub_catname'] ?></p>	
								<p class="text-muted"><b>No Of Patents : </b><?php echo $user_data[0]['no_of_patents'] ?></p>	
								<p class="text-muted"><b>Patent Year : </b><?php echo $user_data[0]['patent_year'] ?></p>	
								<p class="text-muted"><b>Patent Number : </b><?php echo $user_data[0]['patent_number'] ?></p>	
								<p class="text-muted"><b>Patent Title : </b><?php echo $user_data[0]['patent_title'] ?></p>
								<p class="text-muted"><b>Portal Name : </b><?php echo $user_data[0]['portal_name'] ?></p>
								<p class="text-muted"><b>Portal Link : </b><?php echo $user_data[0]['portal_link'] ?></p>
							</div>-->
							<?php //if($user_data[0]['user_sub_category_id'] != '11'): ?>	
							<div class="form-group">
								<div class="form-check form-check-inline">
									
									<input type="checkbox" class="chk-expert" data-cat_id="<?php echo $user_data[0]['user_sub_category_id'] ?>" data-pre_id="<?php echo $user_data[0]['previous_user_sub_cat_id'] ?>"  data-uid="<?php echo $user_data[0]['user_id'] ?>" <?php if($user_data[0]['aplied_for_expert'] == "approved"): ?> checked="checked" <?php endif; ?> name="is_expert" id="is_expert" value="approved" >
									&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1">Is Expert.? <?php //echo $user_data[0]['user_id'] ?></label>
								</div>	
							</div>
							<?php //endif; ?>
							
						</form>
					</div>
					
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</div>
<script>
$(document).ready(function(){
	$('.chk-expert').on('click', function(){		 
		
		if($(this).is(":checked")) {				
			//$( ".audi-chk" ).prop( "checked", false );	
			var val = 'approved';	
			} else {
			var val = 'no';	
		}

		var uid = $(this).attr('data-uid');	
		var sid = $(this).attr('data-pre_id');
		var cid = $(this).attr('data-cat_id');
		var cs_t = 	$('.token').val();	
		
		var base_url = '<?php echo base_url() ?>';
		 $.ajax({
			type:'POST',
			url: base_url+'xAdmin/users/markexpert',
			data:'isexpert='+val+'&uid='+uid+'&csrf_test_name='+cs_t+'&sid='+sid+'&cid='+cid,				
			success:function(data){
				var output = JSON.parse(data);
				//var status = output.u_featured;					
				$(".token").val(output.token); //data-cat_id //data-pre_id
				//$("#featured-"+ID).html(status);
				var cat_id = output.user_cat_id;
				var prev_id = output.previous_cat_id;
				$(".chk-expert").attr("data-cat_id", cat_id);
				$(".chk-expert").attr("data-pre_id", prev_id);
				swal({
						title: 'Success!',
						text: "Expert status successfully updated.",
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				
			}
		});	
	});
});	

</script>
