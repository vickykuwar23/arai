<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Users</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php }  ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Users
              </h3>
                 <!--<a href="<?php echo base_url('xAdmin/users/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>-->
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Fullname</th>
					 <th>Email</th>
					 <th>Mobile</th>
					 <th>Category</th>
					 <th>Subcategory</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $user_data) 
                     { 
					 
					 
					 if($user_data['cat_id'] == 1){
						$file = 'viewDetails'; 
						$fullname = $user_data['title'].ucfirst($user_data['first_name'])."&nbsp;".ucfirst($user_data['middle_name'])."&nbsp;".ucfirst($user_data['last_name']);
						
					 } else {
						$file = 'organizationDetails';
						$fullname = ucwords($user_data['institution_full_name']);
						
					 }
					 
					 if($user_data['is_featured'] == 'Y'){
						$textShow = "Featured"; 
					 } else {
						$textShow = "Not-Featured"; 
					 }
					 
					 ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:15%"><?php echo $fullname; ?></td> 
					 <td style="width:20%"><?php echo $user_data['email'] ; ?></td> 
					 <td style="width:10%"><?php echo $user_data['country_code'].$user_data['mobile']; ?></td>
					 <td style="width:10%"><?php echo $user_data['user_category']; ?></td>	
					 <td style="width:20%"><?php echo $user_data['sub_catname']; ?></td>	
                     <td style="width:35%">
						
                       <!--<a href="<?php echo base_url('xAdmin/users/edit/'.base64_encode($user_data['user_id'])) ?>"><button class="btn btn-success btn-sm">Edit</button></a>-->
					   <a href="<?php echo base_url('xAdmin/users/'.$file.'/'.$user_data['user_id']) ?>">
							<button class="btn btn-primary btn-sm"> View</button>
					   </a>
					   <?php //echo base_url('xAdmin/users/changeStatus/'.base64_encode($user_data['user_id'])) ?>
						<a href="javascript:void(0)" data-id="<?php echo $user_data['user_id'] ?>" class="status-check" >
							<button class="btn btn-warning btn-sm" id="change-val-<?php echo $user_data['user_id'] ?>"> <?php echo $user_data['status'] ?></button>
						</a>
						<?php if($user_data['cat_id'] == 1){ ?>
							<a href="javascript:void(0)" data-id="<?php echo $user_data['user_id'] ?>" class="featured-check"  >
								<button class="btn btn-info btn-sm" id="change-stac-<?php echo $user_data['user_id'] ?>"> <?php echo $textShow; ?></button>
							</a>	
					   	<?php } ?>
                     </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                                                                                    
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$('.status-check').on('click', function(){	            
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/updateStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					//$("#change-val-"+id).prop("value", status); 
					$("#change-val-"+id).html(status);
					swal({
							title: 'Success!',
							text: "User status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });	 
	   
	   $('.featured-check').on('click', function(){		
            
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/changeStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					//$("#change-val-"+id).prop("value", status); 
					$("#change-stac-"+id).html(status);
					swal({
							title: 'Success!',
							text: "User featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	
    });
</script>