<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>

<style>
	/* Prelaoder */
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	@-webkit-keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	@keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
</style>
<script>
	// Preloader
	$(window).on('load', function() {
	if ($('#preloader-loader').length) {
	$('#preloader-loader').delay(50).fadeOut('slow', function() {
	/* $(this).remove(); */
	});
	}
	});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Question</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Question</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
				<div class="card-header">
					<h3 class="card-title">
						Edit Question
					</h3>
					<a href="<?php echo base_url('xAdmin/questions_answers_forum') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div>
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="post" id="Questionform" name="Questionform" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="custum_question_id">Question ID <em style="color: red;"></em></label>
									<input type="text" class="form-control" name="custum_question_id" id="custum_question_id" value="<?php echo $form_data[0]['custum_question_id']; ?>"  readonly >
								</div>	
								
								<div class="form-group">
									<label for="Question_title">Question <em style="color: red;">*</em></label>
									<textarea name="question" id="question" class="form-control" cols="12" rows="4"><?php echo $form_data[0]['question']; ?></textarea>
									<?php if(form_error('question')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('question'); ?></label> <?php } ?>									
								</div>
								
								<div class="form-group">
									<label class="form-label">Tags <em>*</em></label>
									<div class="boderBox65x">
										<select class="choose-tech select2" name="tags[]" id="tags" data-placeholder="Tags" multiple style="width: 100%;" onchange="show_hide_tags_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
													$tags_arr = explode(",",$form_data[0]['tags']); ?>
													<option value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
								
								<div class="form-group" id="tags_other_outer" <?php if($form_data[0]['tags_other'] == "") { ?> style="display:none;"<?php } ?>>
									<label class="form-control-placeholder floatinglabel">Tags Other <em>*</em></label>
									<input type="text" class="form-control" name="tags_other" id="tags_other" value="<?php echo $form_data[0]['tags_other']; ?>">
									<?php if(form_error('tags_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('tags_other'); ?></label> <?php } ?>
								</div>
								
								<div class="form-group">
									<label for="qa_image">Question Image (Only .jpg, .jpeg, .png image formats below 2MB are accepted)</label>
									<input type="file" class="form-control" id="qa_image" placeholder="" name="qa_image" >
									<?php if($qa_image_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $qa_image_error; ?></label> <?php } ?>
								</div>
								<div id="qa_image_outer" style='position:relative'>
									<?php if($form_data[0]['qa_image']!=""){ ?> 
										<img src="<?php echo base_url('uploads/qa_image/'.$form_data[0]['qa_image']); ?>" style="max-height:150px" />
										
										<a class="btn btn-danger btn-sm" onclick="delete_single_file('<?php echo $encrypt_obj->encrypt('arai_qa_forum'); ?>', '<?php echo $encrypt_obj->encrypt('q_id'); ?>', '<?php echo $encrypt_obj->encrypt($form_data[0]['q_id']); ?>', 'qa_image', '<?php echo $encrypt_obj->encrypt('./uploads/qa_image/'); ?>', 'Question Image')" href="javascript:void(0)" style='position: absolute; left: 0; bottom: 0;'><i class="fa fa-trash" aria-hidden="true"></i></a>
									<?php } ?>
								</div><br>
								
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">	
	function delete_single_file(tbl_nm, pk_nm, del_id, input_nm, file_path, swal_text)
    { 
        swal(
        {
            title:"Confirm?" ,
            text: "Are you confirm to delete the "+swal_text+"?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function (result) 
        {
            if (result.value) 
            {
                var csrf_test_name = $("#csrf_token").val();
                var data = { 
                        'tbl_nm': encodeURIComponent($.trim(tbl_nm)), 
                        'pk_nm': encodeURIComponent($.trim(pk_nm)), 
                        'del_id': encodeURIComponent($.trim(del_id)), 
                        'input_nm': encodeURIComponent($.trim(input_nm)), 
                        'file_path': encodeURIComponent($.trim(file_path)), 
                        'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) 
                        };          
                $.ajax(
                { 
                    type: "POST", 
                    url: '<?php echo site_url("delete_single_file") ?>', 
                    data: data, 
                    dataType: 'JSON',
                    success:function(data) 
                    { 
                        $("#csrf_token").val(data.csrf_new_token);
                        $("#"+input_nm+"_outer").remove();
                        
                        swal({ title: "Success", text: swal_text+' successfully deleted', type: "success" });
                    }
                });
            }
        });
    }
		
	function show_hide_tags_other()
	{
		$("#tags_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#tags_other_outer").show();
			$("#tags_other").prop("required", true);
		}
		else
		{
			$("#tags_other_outer").hide();
			$("#tags_other").prop("required", false);
			$("#tags_other").val('');
		}
	}	
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		//******* JQUERY VALIDATION *********
		$("#Questionform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
         {
            question: { required: true, nowhitespace: true, /* valid_value: true */ },
            "tags[]": { required: true},				
            qa_image: { valid_img_format: true,filesize:2000000},
         },
         messages:
         {
            question: { required: "This field is required", nowhitespace: "Please enter the title" },
            "tags[]":{required: "This field is required"},
            qa_image:{ valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},									
         },			
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}			
		});
	});
</script>	