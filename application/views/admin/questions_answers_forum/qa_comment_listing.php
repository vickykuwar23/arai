<!-- Content Wrapper. Contains page content -->
<style>
	/* Prelaoder */
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	@-webkit-keyframes animate-preloader {
	0% {
	transform: rotate(0deg);
	}
	100% {
	transform: rotate(360deg);
	}
	}
	@keyframes animate-preloader {
	0% {
	transform: rotate(0deg);
	}
	100% {
	transform: rotate(360deg);
	}
	}
	
	
	/* Important part */
	.modal-dialog{
	overflow-y: initial !important
	}
	/*.modal-body{
	height: 60vh;
	overflow-y: auto;
	}*/
	
	#blog_reason-error{color:red;font-size:14px;}
</style>

<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>

<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Comment Listing</h1>
				</div>
				
				<div class="col-sm-6"> 
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Comment Listing</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<?php if( $this->session->flashdata('success')){ ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5><i class="icon fas fa-check"></i> Success!</h5>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>
			<div class="card ">
				<?php if(count($question_data) > 0) { echo '<p style="margin:5px 10px 5px 10px;font-weight:600;">Question : '.$question_data[0]['question'].'</p>'; } ?>
								
				<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<!-- Small boxes (Stat box) -->
				<div class="card-body">
					<div class="table-responsive">
						<table id="example1" class="table table-bordered table-hover dataTables-examplexx" style="width:100%">
							<thead>
								<tr>
									<th class="text-center no-sort" style="width:60px;">No.</th>
									<th class="text-center">ANSID</th>
									<th class='text-center'>Answered By</th>	
									<th class='text-center'>Answered On</th>	
									<th class='text-center'>Answer</th>	
									<th class='text-center'>Replies</th>	
									<th class='text-center'>Block</th>								
									<th class='text-center'>Likes</th>
								</tr>
							</thead>
							
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!---------- MODAL POP UP FOR SHOW LIKES USER DATA FOR COMMENT / ANSWER  --------------------->
<div class="modal fade" id="LikesPopUp" tabindex="-1" aria-labelledby="LikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LikesLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="LikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="QuestionBlockpopup" tabindex="-1" role="dialog" aria-labelledby="QuestionBlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="QuestionBlockpopupLabel"><b>Block Comment?</b></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
					<div>
						<label><b>Comment ID :</b> <span id="popupQuestionId" style="font-weight:500"></span></label>
					</div>
					<div>
						<label><b>Comment :</b> <span id="popupQuestionTitle" style="font-weight:500"></span></label>
					</div>
					
					<div>
						<label for="popupQuestionBlockReason"><b>Block reason <em>*</em></b></label>
						<textarea class="form-control" id="popupQuestionBlockReason" name="popupQuestionBlockReason" required onkeyup="check_block_validation()"></textarea>
						<span id="popupQuestionBlockReason_err" class='error' style='color:red'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button id="modal_submit_btn" type="button">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<script>
	function show_comment_likes(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/show_comment_likes_user_data_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#LikesLabel").html(data.comment);				
					$("#LikesContent").html(data.response);				
					$("#LikesPopUp").modal('show');	
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function block_unblock_comment(id, type, q_disp_id, q_title)
	{
		if(type == "Block")
		{
			$("#popupQuestionId").html(q_disp_id);
			$("#popupQuestionTitle").html(q_title);
			$("#popupQuestionBlockReason_err").html('');
			
			var onclick_fun = "submit_blog_block('"+id+"', '"+type+"')";
			$("#modal_submit_btn").replaceWith('<button id="modal_submit_btn" type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
			
			$("#QuestionBlockpopup").modal('show');
			$("#popupQuestionBlockReason").val('');
			$("#popupQuestionBlockReason").focus();			
		}
		else
		{
			submit_blog_block(id, type)
		}		
	}
	
	function check_block_validation()
	{
		var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
		if(popupQuestionBlockReason.trim() == "") { $("#popupQuestionBlockReason_err").html('Please enter the block reason'); $("#popupQuestionBlockReason").focus(); }
		else { $("#popupQuestionBlockReason_err").html(''); }
	}
	
	function submit_blog_block(q_id, type)
	{	
		check_block_validation();
		
		var flag = 0;
		if(type == 'Block')
		{
			var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
			if(popupQuestionBlockReason.trim() == "")	{ flag = 1;}
		}
				
		if(flag == 0)
		{
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to "+type+" the selected comment?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("xAdmin/questions_answers_forum/block_unblock_comment"); ?>',
						data: {'id':q_id, 'type':type, 'popupQuestionBlockReason':popupQuestionBlockReason, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#QuestionBlockpopup").modal('hide');
							
							$('#example1').DataTable().ajax.reload();
							swal(
							{
								title: 'Success!',
								text: "Comment successfully "+type+"ed.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}
	
	$(document).ready(function()
	{
		var table = $('#example1').DataTable(
		{
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": '<?php echo site_url("xAdmin/questions_answers_forum/get_parent_comment_listing_data"); ?>',
				"type": "POST",
				"data": function ( d ) 
				{
					d.q_id = "<?php echo $q_id; ?>";
				}
			},
			"lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
			"language": 
			{
				"lengthMenu": "_MENU_",
			},
			/* "dom": '<"top"lf><"clear"><i>rt<"bottom row"<"col-sm-12 col-md-5" and i><"col-sm-12 col-md-7" and p>><"clear">', */
			pageLength: 10,
			responsive: true,
			rowReorder: true,
			"columnDefs": 
			[
				{"targets": 'no-sort', "orderable": false, },
				{"targets": [0], "className": "text-center"},
				{"targets": [3], "className": "text-right"},
				{"targets": [5], "className": "text-center"},
				{"targets": [6], "className": "text-center"},
				{"targets": [7], "className": "text-center"}
			],
			"aaSorting": [],
			"stateSave": false,					
			'drawCallback': function(settings)
			{	}				
		});
	});
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>