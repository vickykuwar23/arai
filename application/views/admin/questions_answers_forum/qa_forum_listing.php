<!-- Content Wrapper. Contains page content -->
<style>
	/* Prelaoder */
	#preloader-loader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 9999;
	overflow: hidden;
	background: rgba(0,0,0,0.5);
	}
	#preloader-loader:before {
	content: "";
	position: fixed;
	top: calc(50% - 30px);
	left: calc(50% - 30px);
	border: 6px solid #f2f2f2;
	border-top: 6px solid #c80032;
	border-radius: 50%;
	width: 60px;
	height: 60px;
	-webkit-animation: animate-preloader 1s linear infinite;
	animation: animate-preloader 1s linear infinite;
	}
	@-webkit-keyframes animate-preloader {
	0% {
	transform: rotate(0deg);
	}
	100% {
	transform: rotate(360deg);
	}
	}
	@keyframes animate-preloader {
	0% {
	transform: rotate(0deg);
	}
	100% {
	transform: rotate(360deg);
	}
	}
	
	
	/* Important part */
	.modal-dialog{
	overflow-y: initial !important
	}
	/*.modal-body{
	height: 60vh;
	overflow-y: auto;
	}*/
	
	#blog_reason-error{color:red;font-size:14px;}
</style>

<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>

<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">QA Forum Listing</h1>
				</div>
				
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">QA Forum Listing</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<?php if( $this->session->flashdata('success')){ ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5><i class="icon fas fa-check"></i> Success!</h5>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>
			<div class="card ">
				<div class="card-header">
					<form method="post" name="frm-exp" id="frm-exp">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<input type="hidden" class="seach-key form-control" id="keyword" name="keyword" placeholder="Search By Question Id, Question Title, Author Name">
								</div>
							</div>
							<div class="col-6">					
								<input type="submit" class="btn btn-primary btn-sm pull-right float-right" id="export" name="export" value="Export To Excel">					
							</div>
						</div>
					</form>
				</div>
				
				<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<!-- Small boxes (Stat box) -->
				<div class="card-body">
					<div class="table-responsive">
						<table id="example1" class="table table-bordered table-hover dataTables-examplexx" style="width:100%">
							<thead>
								<tr>
									<th class="no-sort" style="width:60px;">No.</th>
									<th class="text-center">ID</th>
									<th class='text-center'>QA ID</th>	
									<th class='text-center'>Question</th>								
									<th class='text-center no-sort'>Posted By</th>								
									<th class='text-center'>Posted On</th>								
									<th class='text-center'>Admin Status</th>
									<th class='text-center'>xOrder</th>
									<th class='text-center'>Likes</th>
									<th class='text-center'>Comments</th>
									<th class='text-center'>Reports</th>
									<th class='text-center'>Priority</th>								
									<th class="text-center no-sort" style="width:90px;">Action</th>
								</tr>
							</thead>
							
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!---------- MODAL POP UP FOR SHOW LIKES USER DATA FOR QUESTION --------------------->
<div class="modal fade" id="LikesPopUp" tabindex="-1" aria-labelledby="LikesLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LikesLabel" style="font-size:16px;"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="LikesContent"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="briefInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Question Rejection Reason</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents">
				<form method="post" action="javascript:void(0)" name="blog_reject_form" id="blog_reject_form">
					<div class="col-auto" style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<label>Rejection Reason</label>
						<div class="custom-checkbox mr-sm-2">
							<textarea name="blog_reason" id="blog_reason" class="form-control"></textarea>
						</div>
						<div id="blog_reason_err"></div>
						<input type="hidden" name="q_id" id="q_id" value="" />
					</div>					
					<button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Submit</button>
				</form>
			</div>
			
		</div>
	</div>
</div>

<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Applicant Listing</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body " id="contents">
				<div id="des-show"></div>
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="QuestionBlockpopup" tabindex="-1" role="dialog" aria-labelledby="QuestionBlockpopupLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="QuestionBlockpopupLabel"><b>Block Question?</b></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
			</div>
			<form>
				<div class="modal-body" id="contents" style="padding:10px 15px 30px 15px;">
					<div>
						<label><b>Question ID :</b> <span id="popupQuestionId" style="font-weight:500"></span></label>
					</div>
					<div>
						<label><b>Question :</b> <span id="popupQuestionTitle" style="font-weight:500"></span></label>
					</div>
					
					<div>
						<label for="popupQuestionBlockReason"><b>Block reason <em>*</em></b></label>
						<textarea class="form-control" id="popupQuestionBlockReason" name="popupQuestionBlockReason" required onkeyup="check_block_validation()"></textarea>
						<span id="popupQuestionBlockReason_err" class='error' style='color:red'></span>
					</div>
				</div>	
				
				<div class="modal-footer">
					<button id="modal_submit_btn" type="button">Submit</button>			
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			
				</div>
			</form>			
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>

<script>
	function show_question_likes(id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/show_likes_user_data_ajax"); ?>',
			data:{ 'id':id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#LikesLabel").html(data.question);				
					$("#LikesContent").html(data.response);				
					$("#LikesPopUp").modal('show');	
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function show_question_reports(q_id)
	{
		var cs_t = 	$('.token').val();
		$.ajax(
		{
			type:'POST',
			url: '<?php echo site_url("questions_answers_forum/show_reported_user_ajax"); ?>',
			data:{ 'q_id':q_id, 'csrf_test_name':cs_t },
			dataType:"JSON",
			success:function(data)
			{
				$(".token").val(data.token);
				if(data.flag == 'success')
				{
					$("#LikesLabel").html(data.question+' <br> '+data.question_name);				
					$("#LikesContent").html(data.response);				
					$("#LikesPopUp").modal('show');				
				}
				else { sweet_alert_error("Error Occurred. Please try again."); }
			}
		});
	}
	
	function update_sort_order(xOrder, q_id)
	{	
		var xOrder = xOrder;		
		var csrf_test_name	= $('.token').val();			
		var base_url = "<?php echo base_url('xAdmin/questions_answers_forum/setOrder'); ?>";
		$.ajax({
			url: base_url,
			type: "post",
			//async: false,
			data: {xOrder:xOrder, q_id:q_id, csrf_test_name:csrf_test_name},				
			success: function (response) {
				var returndata = JSON.parse(response);
				var token = returndata.token;
				$('.token').val(token);
				var status = returndata.msg;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});				
	}
	
	function block_unblock_question(id, type, q_disp_id, q_title)
	{
		if(type == "Block")
		{
			$("#popupQuestionId").html(q_disp_id);
			$("#popupQuestionTitle").html(q_title);
			$("#popupQuestionBlockReason_err").html('');
			
			var onclick_fun = "submit_blog_block('"+id+"', '"+type+"')";
			$("#modal_submit_btn").replaceWith('<button id="modal_submit_btn" type="button" class="btn btn-primary" onclick="'+onclick_fun+'">Submit</button>');
			
			$("#QuestionBlockpopup").modal('show');
			$("#popupQuestionBlockReason").val('');
			$("#popupQuestionBlockReason").focus();			
		}
		else
		{
			submit_blog_block(id, type)
		}		
	}
	
	function check_block_validation()
	{
		var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
		if(popupQuestionBlockReason.trim() == "") { $("#popupQuestionBlockReason_err").html('Please enter the block reason'); $("#popupQuestionBlockReason").focus(); }
		else { $("#popupQuestionBlockReason_err").html(''); }
	}
	
	function submit_blog_block(q_id, type)
	{	
		check_block_validation();
		
		var flag = 0;
		if(type == 'Block')
		{
			var popupQuestionBlockReason = $("#popupQuestionBlockReason").val();
			if(popupQuestionBlockReason.trim() == "")	{ flag = 1;}
		}
				
		if(flag == 0)
		{
			swal(
			{  
				title:"Confirm?",
				text: "Are you sure you want to "+type+" the selected question?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result) 
			{ 
				if (result.value) 
				{
					var cs_t = 	$('.token').val();
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("xAdmin/questions_answers_forum/block_unblock_question"); ?>',
						data: {'id':q_id, 'type':type, 'popupQuestionBlockReason':popupQuestionBlockReason, 'csrf_test_name':cs_t },
						dataType:"JSON",
						success:function(data)
						{
							$(".token").val(data.token);
							$("#QuestionBlockpopup").modal('hide');
							
							$('#example1').DataTable().ajax.reload();
							swal(
							{
								title: 'Success!',
								text: "Question successfully "+type+"ed.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				} 
			});
		}
	}	
	
	function change_question_priority(q_id, priority)
	{
		$('#preloader-loader').css('display', 'block');	
		parameters = { 'q_id':q_id, 'priority':priority, 'csrf_test_name':$('.token').val() }
		$.ajax({
			type:'POST',
			url: "<?php echo site_url('xAdmin/questions_answers_forum/change_question_priority_ajax'); ?>",
			data:parameters,	
			dataType: 'JSON',
			success:function(data)
			{
				$('#preloader-loader').css('display', 'none');					
				$(".token").val(data.token);
				//location.reload();
				
				//$('#example1').DataTable({ "stateSave": true }).ajax.reload();
				if(data.status == 'success')
				{					
					swal({
						title: 'Success!',
						text: "Question priority updated successfully.",
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				}
				else
				{
					/* swal({
						title: 'Error!',
						text: "Error Occurred. Please try again.",
						type: 'error',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}); */
					location.reload();
				}
			}
		});
	}
	
	function delete_question(q_id)
	{
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to delete the selected question?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var id = q_id; 
				var csrf_test_name = 	$('.token').val();			
				$('#preloader-loader').css('display', 'block');		
				$.ajax(
				{
					url: "<?php echo site_url('xAdmin/questions_answers_forum/deleted'); ?>",
					type: "post",
					data: {id:id,csrf_test_name:csrf_test_name},				
					success: function (response) 
					{
						$('#preloader-loader').css('display', 'none');	
						var returndata = JSON.parse(response);
						var token = returndata.token;
						
						$('.txt_csrfname').val(token);				
						$('#example1').DataTable().ajax.reload();
						swal({
							title: 'Success!',
							text: "Question deleted successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
						console.log(textStatus, errorThrown);
					}
				});
			}
		})	
	}	
	
	function featured_notfeatured_question(q_id, text)
	{ 
		swal(
		{  
			title:"Confirm?",
			text: "Are you sure you want to Mark the selected Question as "+text+"?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{ 
			if (result.value) 
			{
				var id = q_id; 
				var csrf_test_name = 	$('.token').val();			
				$('#preloader-loader').css('display', 'block');		
				
				$.ajax(
				{
					url: "<?php echo site_url('xAdmin/questions_answers_forum/featured'); ?>",
					type: "post",
					data: {id:id,csrf_test_name:csrf_test_name},				
					success: function (response) 
					{
						$('#preloader-loader').css('display', 'none');	
						var returndata = JSON.parse(response);
						var token = returndata.token;
						var c_status = returndata.c_status;
						var status = returndata.u_featured;
						$('.txt_csrfname').val(token);
						//console.log(status);	
						$('#example1').DataTable().ajax.reload();
						swal({
							title: 'Success!',
							text: "Question featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
						console.log(textStatus, errorThrown);
					}
				});			
			}
		})
	}
		
	
	$(document).ready(function()
	{
		var table = $('#example1X').DataTable(
		{
			"responsive": true,
			"autoWidth": false,
			"serverMethod": 'post',			
			"ordering":true,
			"searching": false,
			 "bStateSave": true,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode
			 
			"ajax": {
				"url": '<?php echo site_url("xAdmin/questions_answers_forum/get_listing_data"); ?>',
				"type": "POST",
				"data": function ( d ) 
				{
					/* d.delete_ids_str = $("#selcted_checkbox_all_hidden").val(); */
				}
			},
			"lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
			"language": 
			{
				"lengthMenu": "_MENU_",
			},
			/* "dom": '<"top"lf><"clear"><i>rt<"bottom row"<"col-sm-12 col-md-5" and i><"col-sm-12 col-md-7" and p>><"clear">', */
			pageLength: 10,
			responsive: true,
			rowReorder: true,
			"columnDefs": 
			[
				{"targets": 'no-sort', "orderable": false, },
				{"targets": [0], "className": "text-center"},
				{"targets": [1], "className": "text-center"},
				{"targets": [5], "className": "text-right"},
				{"targets": [6], "className": "text-center"},
				{"targets": [7], "className": "text-center"},
				{"targets": [8], "className": "text-center"},
				{"targets": [9], "className": "text-center"},
				{"targets": [10], "className": "text-center"},
				{"targets": [11], "className": "text-center"},
				{"targets": [12], "className": "text-center"}
			],
			"aaSorting": [],
			"stateSave": false,					
			'drawCallback': function(settings)
			{
				/* $( ".checkboxlist_new" ).click(function() { checkboxlist_new_function(); });
				$('#checkboxlist_all_new').prop('checked', false);
				checkboxlist_new_function();
				$('input[type=checkbox][data-toggle^=toggle]').bootstrapToggle(); */
			}				
		});
	});

		var base_path = '<?php echo base_url(); ?>';
		var table = $('#example1').DataTable(
		{
			"responsive": true,
			"autoWidth": false,
			"serverMethod": 'post',			
			"ordering":true,
			"searching": false,
			 "bStateSave": true,
			"language": {
				"zeroRecords":"No matching records found.",
				"infoFiltered":""
			},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			
			// Load data for the table's content from an Ajax source
			"ajax": 
			{
				"url": base_path+"xAdmin/questions_answers_forum/get_listing_data",
				"type":"POST",
				"data":function(data) {			
					data.keyword 	= $("#keyword").val();
					
					/*data.c_type 	= $("#c_type").val();
						data.com_code 	= $("#com_code").val();
						data.username 	= $("#username").val();
						data.district_id = $("#district_id").val();
						data.c_subtype 	 = $("#c_subtype").val();
					data.sro_office  = $("#sro_office").val();*/
					
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": 
				{
					401:function(responseObject, textStatus, jqXHR) {	},
				},
			}	
		});
			
	$(document).ready( function () 
	{		
		/* $('#frm-exp').keypress(function (e) {
			if (e.which === 13) {		
			e.preventDefault(); 
			}
		}); */
		
		var base_path = '<?php echo base_url(); ?>';
		var blogdata = $('#example1XXX').DataTable(
		{
			"responsive": true,
			"autoWidth": false,
			"serverMethod": 'post',			
			"ordering":true,
			"searching": false,
			 "bStateSave": true,
			 "columnDefs":
			[
				{"targets": 'no-sort', "orderable": false, },
				{"targets": [0], "className": "text-center"},
			 	
			],
			"aaSorting": [],
			"language": {
							"zeroRecords":"No matching records found.",
							"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			"ajax": 
			{
				"url": base_path+"xAdmin/questions_answers_forum/get_listing_data",
				"type":"POST",
				"data":function(data) {			
					data.keyword 	= $("#keyword").val();
					
					/*data.c_type 	= $("#c_type").val();
						data.com_code 	= $("#com_code").val();
						data.username 	= $("#username").val();
						data.district_id = $("#district_id").val();
						data.c_subtype 	 = $("#c_subtype").val();
					data.sro_office  = $("#sro_office").val();*/
					
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": 
				{
					401:function(responseObject, textStatus, jqXHR) {	},
				},
			}	
		});
		
		// Admin Status Update
		$(document).on('change','.change-status',function()
		{
			var status = $(this).val();
			if(status!= '2')//Rejected
			{				
				var ID = $(this).closest("tr").find("#q_id").val();
				var cs_t = 	$('.token').val();
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';
				$.ajax({
					type:'POST',
					url: base_url+'xAdmin/questions_answers_forum/updateStatus',
					data:'status='+status+'&id='+ID+'&csrf_test_name='+cs_t,				
					success:function(data)
					{
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);	
						$(".token").val(output.token);
						$('#example1').DataTable().ajax.reload();
						swal({
							title: 'Success!',
							text: "Question status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});
			} 
			else 
			{
				var ID = $(this).closest("tr").find("#q_id").val();
				$("#q_id").val(ID);	
				$("#blog_reject_form")[0].reset();
				$("#blog_reason-error").html('');
				$("#briefInf").modal('show');				
			}			
		});	
		
		//******* JQUERY VALIDATION *********
		$("#blog_reject_form").validate( 
		{
			rules: { blog_reason: { required: true } },
			messages: { blog_reason: { required: "Please enter rejection reason" }, },
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "blog_reason") { error.insertAfter("#blog_reason_err"); }
				else { error.insertAfter(element); }
			},
			submitHandler: function(form) 
			{ 
				var csrf_test_name 	= 	$('.token').val();
				var q_id = 	$('#q_id').val();
				var blog_reason 	= 	$('#blog_reason').val();
				$('#preloader-loader').show();
				parameters= { 'reject_reason': blog_reason, 'id': q_id, 'status': "2", 'csrf_test_name':csrf_test_name }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('xAdmin/questions_answers_forum/updateStatus'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success: function(data) 
					{  
						//$(".token").val(data.csrf_test_name);
						/*
							var output = JSON.parse(data);
							console.log(output);
							$(".token").val(output.token);
						*/	
						$('#preloader-loader').css('display', 'none');	
						$("#briefInf").modal('hide');
						$('#example1').DataTable().ajax.reload();
						swal({
							title: 'Success!',
							text: "Question status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
					}
				});	
			}
		});
	});
	
	$(document).ajaxStart(function() { $("#preloader-loader").css("display", "block"); });
	$(document).ajaxComplete(function() { $("#preloader-loader").css("display", "none"); });
</script>