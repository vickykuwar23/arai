<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>

<style>
	/* Prelaoder */
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	@-webkit-keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	@keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
</style>
<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Blog</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Blog</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Blog
					</h3>
				  <a href="<?php echo base_url('xAdmin/blogs_technology_wall') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div>
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="post" id="blogform" name="blogform" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="blog_id_disp">Blog ID <em style="color: red;"></em></label>
									<input type="text" class="form-control" name="blog_id_disp" id="blog_id_disp" value="<?php echo $form_data[0]['blog_id_disp']; ?>"  readonly >
								</div>	
								
								<div class="form-group">
									<label for="blog_title">Blog Title <em style="color: red;">*</em></label>
									<input type="text" class="form-control" id="blog_title" placeholder="" name="blog_title" value="<?php echo $form_data[0]['blog_title']; ?>" required>								
									<?php if(form_error('blog_title')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('blog_title'); ?></label> <?php } ?>
								</div>	
								
								<div class="form-group">
									<label>Blog Description <em style="color: red;">*</em></label>
									<textarea name="blog_description" id="blog_description" class="form-control" cols="12" rows="4"><?php echo $form_data[0]['blog_description']; ?></textarea>
									<?php if(form_error('blog_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('blog_description'); ?></label> <?php } ?>									
								</div>
															
								<div class="form-group">
									<label for="blog_banner">Blog Banner (Upload only jpg, jpeg ,gif & png format only)</label>
									<input type="file" class="form-control" id="blog_banner" placeholder="" name="blog_banner" >
									<?php if($blog_banner_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $blog_banner_error; ?></label> <?php } ?>
								</div>
								<div>
									<?php if($form_data[0]['blog_banner']!=""){ ?> 
										<img src="<?php echo base_url('uploads/blog_banner/'.$form_data[0]['blog_banner']); ?>" style="max-height:150px" />
									<?php } ?>
								</div><br>
								
								<div class="form-group">
									<label class="form-label">Blog Technology <em>*</em></label>
									<div class="boderBox65x">
										<select class="choose-tech select2" name="technology_ids[]" id="technology_ids" data-placeholder="Blog Technology" multiple onchange="show_hide_blog_technology_other()" style="width: 100%;">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														$technology_ids_arr = explode(",",$form_data[0]['technology_ids']); ?>
													<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$technology_ids_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
													<?php }
												} ?>
										</select> 
										<?php if($technology_ids_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $technology_ids_error; ?></label> <?php } ?>
									</div>
								</div>
								
								<div class="form-group" id="blog_technology_other_outer" <?php if($form_data[0]['technology_other'] == "") { ?> style="display:none;"<?php } ?>>
									<label class="form-label">Blog Technology Other <em>*</em></label>
									<input type="text" class="form-control" name="technology_ids_other" id="technology_ids_other" value="<?php echo $form_data[0]['technology_other']; ?>">
									<?php if(form_error('technology_ids_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('technology_ids_other'); ?></label> <?php } ?>
								</div>
								
								<div class="form-group">
									<label class="form-label">Tags <em>*</em></label>
									<div class="boderBox65x">
										<select class="choose-tech select2" name="tags[]" id="tags" data-placeholder="Tags" multiple onchange="show_hide_tag_other()" style="width: 100%;">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = explode(",",$form_data[0]['tags']); ?>
													<option value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>

								<div class="col-md-6" id="tag_other_outer" style="margin-top:26px;">
		               <div class="form-group">
		                  <input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php if($mode == 'Add') { echo set_value('tag_other'); } else { echo $form_data[0]['tag_other']; } ?>">
		                  <label class="form-control-placeholder floatinglabel">Other Tag <em>*</em></label>
		                  <?php if(form_error('tag_other')!=""){ ?> 
		                  <div class="clearfix"></div>
		                  <label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
		               </div>
		            </div>
								
								<div class="form-group" style="margin-bottom: 15px;margin-top: 10px;">		
									<label class="form-label">Blog Type <em>*</em></label>
									<div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="is_technical1" name="is_technical" class="custom-control-input get-type" value="1" <?php if($form_data[0]['is_technical'] == 1) { echo 'checked'; } ?>>
											<label class="custom-control-label" for="is_technical1">Technical</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="is_technical0" name="is_technical" class="custom-control-input get-type" value="0" <?php if($form_data[0]['is_technical'] == 0) { echo 'checked'; } ?>>
											<label class="custom-control-label" for="is_technical0">Non Technical</label>
										</div>								
									</div>								
									<div id="upload_type_err"></div>
								</div>
								
								<div class="form-group" style="margin:25px 0 10px 0;">	
									<label style="margin: 0;display: block;background: #ededed;padding: 10px 10px;font-weight: 500;">Author Section</label>
								</div>
								
								<div class="form-group">
									<label class="form-label">Author Name <em></em></label>
									<input type="text" class="form-control" name="author_name" id="author_name" value="<?php echo $form_data[0]['author_name']; ?>">
									<?php if(form_error('author_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_name'); ?></label> <?php } ?>
								</div>
								
								<div class="custom-control custom-checkbox" style="margin-bottom:5px;">
									<input type="checkbox" class="custom-control-input" id="author_professional_status_chk" name="author_professional_status_chk" onchange="enable_disable_inputs('author_professional_status_chk','author_professional_status')" <?php if($form_data[0]['author_professional_status'] != "") { echo 'checked'; } ?>>
									<label class="custom-control-label" for="author_professional_status_chk">Include My professional Status while posting this blog</label>
								</div>
								
								<?php $professional_status_flag = 'readonly disabled';
								if($form_data[0]['author_professional_status'] != "") { $professional_status_flag = ''; } ?>
								<div class="form-group">
									<label class="form-label">Professional Status <em>*</em></label>
									<input type="text" class="form-control" name="author_professional_status" id="author_professional_status" value="<?php echo $form_data[0]['author_professional_status']; ?>" <?php echo $professional_status_flag; ?>>
									<?php if(form_error('author_professional_status')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_professional_status'); ?></label> <?php } ?>
								</div>
								
								<div class="custom-control custom-checkbox" style="margin-bottom:5px;">
									<input type="checkbox" class="custom-control-input" id="author_org_name_chk" name="author_org_name_chk" onchange="enable_disable_inputs('author_org_name_chk','author_org_name')" <?php if($form_data[0]['author_org_name'] != "") { echo 'checked'; } ?>>
									<label class="custom-control-label" for="author_org_name_chk">Include My Organization Name while posting this blog</label>
								</div>
							
								<?php $org_name_flag = 'readonly disabled';
								if($form_data[0]['author_org_name'] != "") { $org_name_flag = ''; } ?>
								<div class="form-group">
									<label class="form-label">Organization Name <em>*</em></label>
									<input type="text" class="form-control" name="author_org_name" id="author_org_name" value="<?php echo $form_data[0]['author_org_name']; ?>" <?php echo $org_name_flag; ?> maxlength="100">
									<?php if(form_error('author_org_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_org_name'); ?></label> <?php } ?>
								</div>
								
								<div class="custom-control custom-checkbox" style="margin-bottom:5px;">
									<input type="checkbox" class="custom-control-input" id="author_description_chk" name="author_description_chk" onchange="enable_disable_inputs('author_description_chk','author_description')" <?php if($form_data[0]['author_description'] != "") { echo 'checked'; } ?>>
									<label class="custom-control-label" for="author_description_chk">Include below while posting this blog</label>
								</div>
								
								<?php $author_description_flag = 'readonly disabled';
								if($form_data[0]['author_description'] != "") { $author_description_flag = ''; } ?>
								<div class="form-group">
									<label class="form-label">Additional Personal Details <em>*</em></label>
									<textarea class="form-control" name="author_description" id="author_description" <?php echo $author_description_flag; ?> maxlength="50"><?php echo $form_data[0]['author_description']; ?></textarea>
									<?php if(form_error('author_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_description'); ?></label> <?php } ?>
								</div>
								
								<div class="custom-control custom-checkbox" style="margin-bottom:5px;">
									<input type="checkbox" class="custom-control-input" id="author_about_chk" name="author_about_chk" onchange="enable_disable_inputs('author_about_chk','author_about')" <?php if($form_data[0]['author_about'] != "") { echo 'checked'; } ?>>
									<label class="custom-control-label" for="author_about_chk">About Author</label>
								</div>
								
								<?php $author_about_flag = 'readonly disabled';
								if($form_data[0]['author_about'] != "") { $author_about_flag = ''; } ?>
								<div class="form-group">
									<label class="form-label">Author Brief <em>*</em></label>
									<textarea class="form-control" name="author_about" id="author_about" <?php echo $author_about_flag; ?> maxlength="400"><?php echo $form_data[0]['author_about']; ?></textarea>
									<?php if(form_error('author_about')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_about'); ?></label> <?php } ?>
								</div>
								
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<script type="text/javascript">	
	CKEDITOR.replace('blog_description');
	
	function enable_disable_inputs(checkbox_id, input_id)
	{
		if($("#"+checkbox_id).prop("checked") == true)
		{
			$("#"+input_id).prop("disabled", false);
			$("#"+input_id).prop("readonly", false);
			$("#"+input_id).prop("required", true);
			$("#"+input_id).focus();
			
			if(input_id == 'author_professional_status' || input_id == 'author_org_name')
			{
				$("#preloader-loader").css("display", "block");
				parameters = { 'checkbox_id':checkbox_id, 'input_id':input_id, 'csrf_test_name':$('.token').val() }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						if(data.flag == "success")
						{ 
							$(".token").val(data.token);
							if(input_id == 'author_professional_status') { $("#author_professional_status").val(data.professional_status); }
							if(input_id == 'author_org_name') { $("#author_org_name").val(data.org_name); }						
							$("#preloader-loader").css("display", "none");
						}
						else 
						{	}
					}
				});
			}
		}
		else if($("#"+checkbox_id).prop("checked") == false)
		{
			$("#"+input_id).prop("disabled", true);
			$("#"+input_id).prop("readonly", true);
			$("#"+input_id).prop("required", false);
			//$("#"+input_id).val("");
		}
	}

	function show_hide_tag_other()
	{
		$("#tag_other_outer").hide();

		var disp_other_flg = 0;
		var selected = $('#tags').select2("data");
		for (var i = 0; i <= selected.length-1; i++)
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}

		if(disp_other_flg == 1)
		{
			$("#tag_other_outer").show();
			$("#tag_other").prop("required", true);
		}
		else
		{
			$("#tag_other_outer").hide();
			$("#tag_other").prop("required", false);
			$("#tag_other").val('');
		}
	}
	show_hide_tag_other();
	
	function get_author_name()
	{
		$("#preloader-loader").css("display", "block");
		parameters = { 'input_id':'author_name', 'csrf_test_name':$('.token').val() }
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{ 
					$(".token").val(data.token);
					$("#author_name").val(data.author_name);					
					$("#preloader-loader").css("display", "none");
				}
				else 
				{	}
			}
		});
	}
</script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
				
		//******* JQUERY VALIDATION *********
		$("#blogform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				blog_title: { required: true, nowhitespace: true },
				blog_description: 
				{
					required: function() 
					{
						CKEDITOR.instances.blog_description.updateElement();
					},							
					minlength:1
				},
				blog_banner: { <?php if($mode == 'Add') { ?>required: true,<?php } ?> valid_img_format: true,filesize:2000000},
				"technology_ids[]": { required: true},
				"tags[]": { required: true},
				is_technical: { required: true},
								
				/* end_time: { required: true, chk_valid_time:true },
					cost_type: { required: true},
					cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
					"webinar_technology[]": { required: true },
					exclusive_technovuus_event:{required: true },
					registration_link:{required: true,valid_url:true },
					hosting_link:{required: true,valid_url:true },
					breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
					key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] }, */
			},
			messages:
			{
				blog_title: { required: "This field is required", nowhitespace: "Please enter the title" },
				blog_description: { required: "This field is required", nowhitespace: "Please enter the title" },				
				blog_banner:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				is_technical:{required: "This field is required"},
								
				/* start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}		 */					
			},			
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}			
		});
	});
	
	function show_hide_blog_technology_other()
	{
		$("#blog_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#technology_ids').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#blog_technology_other_outer").show();
			$("#technology_ids_other").prop("required", true);
		}
		else
		{
			$("#blog_technology_other_outer").hide();
			$("#technology_ids_other").prop("required", false);
			$("#technology_ids_other").val('');
		}
	}
	//show_hide_blog_technology_other();
</script>