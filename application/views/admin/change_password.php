

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Change Password</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Change Password</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				 Change Password
			  </h3>
				  <a href="<?php echo base_url('xAdmin/admin') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->				 
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				   <?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Success!</h5>
					 <?php echo $this->session->flashdata('success'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="cmfrm" name="cmfrm" role="form" >
						  <div class="row">
							<div class="col-6">							   
							  <div class="form-group">
							   
								<label for="exampleInputEmail1">Old Password </label>
								<input type="password" class="form-control" id="old_password" placeholder="Enter Old Password" name="old_password" maxlength="150">
								<span><?php echo form_error('old_password'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">New Password </label>
								<input type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password" maxlength="150">
								<span ><?php echo form_error('new_password'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">Confirm Password </label>
								<input type="password" class="form-control" id="crm_password" placeholder="Enter Confirm Password" name="crm_password" maxlength="150">
								<span><?php echo form_error('crm_password'); ?></span>
							  </div>	
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div> 
						 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
       && /[a-z]/.test(value) // has a lowercase letter
       && /\d/.test(value) || /~!@#$%^&*_-+=[]\{}|;':",.<> /.test(value) // has a digit
	   
});

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {		
	  form.submit();
    }
  });
  $('#cmfrm').validate({
    rules: {
      old_password: {
        required: true
      },
	  new_password: {
        required: true,
		minlength:8,
		pwcheck: true
      },
	  crm_password: {
        required: true,
		minlength:8,
		equalTo: "#new_password"
      },
    },
    messages: {
      old_password: {
        required: "This field is required"
      },
	  new_password: {
        required: "This field is required",
		minlength: "Enter new password must be at least {0} characters long",
		pwcheck: "Password should contain atleast one upparcase, number and one special character"
      },
	  crm_password: {
        required: "This field is required",
		minlength: "Enter confirm password must be at least {0} characters long",
		equalTo: "Please enter the same password as above"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>