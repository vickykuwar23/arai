<style>
/* Prelaoder */
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}
#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
@-webkit-keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 60vh;
    overflow-y: auto;
}
</style>
<script>
// Preloader
$(window).on('load', function() {
    if ($('#preloader-loader').length) {
        $('#preloader-loader').delay(50).fadeOut('slow', function() {
            /* $(this).remove(); */
        });
    }
});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Challenge Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Challenge Listing</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
				<div class="card-header">
					<!--<h3 class="card-title">
					  Challenge Listing
					</h3>
                 <a href="<?php echo base_url('xAdmin/blog/add') ?>" class="btn btn-primary btn-sm float-right">Add News & Blog</a>-->
					<form method="post" name="frm-exp" id="frm-exp">
					<div class="row">
						 <div class="col-6">
							 <div class="form-group">
								<input type="text" class="seach-key form-control" id="keyword" name="keyword" placeholder="Search By Challenge Code, Challenge Name, Owner Name, Company Name">
							 </div>
						 </div>
						 <div class="col-6">					
								<input type="submit" class="btn btn-primary btn-sm pull-right float-right" id="export" name="export" value="Export To Excel">					
						 </div>
					</div>
				</form>
				</div>
		<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th class="no-sort">No.</th>
					 <th>ID</th>
					 <th>Challenge Code</th>	
                     <th>Challenge Title</th>
					 <th>Challenge Owner</th>
					  <th>Company Name</th>
					  <th>Launch Date</th>
					  <th>Close Date</th>
					  <th>Is Featured</th>
                     <th class="no-sort">Action</th>
                  </tr>
               </thead>
				<tbody>                
                </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		/* $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});*/
		
		 $('#frm-exp').keypress(function (e) {
			if (e.which === 13) {		
				e.preventDefault(); 
			 }
		 });
		
		var base_path = '<?php echo base_url(); ?>';
		var challengeDataTable = $('#example1').DataTable({
			"responsive": true,
			"autoWidth": false,
			"serverMethod": 'post',			
			"ordering":true,
			"searching": false,
			 "bStateSave": true,
			 "columnDefs":
			[
				{"targets": 'no-sort', "orderable": false, },
				{"targets": [0], "className": "text-center"},
			 	
			],
			"aaSorting": [],
			"language": {
							"zeroRecords":"No matching records found.",
							"infoFiltered":""
						},
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			 
			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"xAdmin/challenge/get_challenge_data",
			"type":"POST",
			"data":function(data) {			
				data.keyword 	= $("#keyword").val();
				
				/*data.c_type 	= $("#c_type").val();
				data.com_code 	= $("#com_code").val();
				data.username 	= $("#username").val();
				data.district_id = $("#district_id").val();
				data.c_subtype 	 = $("#c_subtype").val();
				data.sro_office  = $("#sro_office").val();*/
				
				},
			"error":function(x, status, error) {
				
			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {			
						},
					},
			}
			
			
		});
		
		// Get Search Filter Values
		/*$('.searchVal').change(function(){			
			userDataTable.ajax.reload();
		});*/
		
		$('.seach-key').keyup(function(){ console.log();			
			challengeDataTable.ajax.reload();	
		});
		
		$(document).on('change','.up-status',function(){
			var status = $(this).val(); //$("#ch_id").val();
			var ID = $(this).closest("tr").find("#ch_id").val();
			var cs_t = 	$('.token').val();
			$('#preloader-loader').css('display', 'block');	
			var base_url = '<?php echo base_url() ?>';
			 $.ajax({
				type:'POST',
				url: base_url+'xAdmin/challenge/updateStatus',
				data:'status='+status+'&id='+ID+'&csrf_test_name='+cs_t,				
				success:function(data){
					$('#preloader-loader').css('display', 'none');	
					var output = JSON.parse(data);	
					$(".token").val(output.token);
					$('#example1').DataTable().ajax.reload();
					swal({
							title: 'Success!',
							text: "Challenge status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				}
			});
			
		});	
		
		
		 $("body").on("click", "#example1 tbody tr a.featured-check", function (e) {    
		  	var id = $(this).attr('data-id'); //alert(id);return false;
			var csrf_test_name = 	$('.token').val();
			//var csrf_test_name	= $('.txt_csrfname').val();	
			$('#preloader-loader').css('display', 'block');		
			var base_url = '<?php echo base_url('xAdmin/challenge/featured'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					$('#preloader-loader').css('display', 'none');	
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var c_status = returndata.c_status;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					//console.log(status);	
					$("a#change-stac-"+id).html(status);
					$('#example1').DataTable().ajax.reload();
					swal({
							title: 'Success!',
							text: "Challenge featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	
});
</script>