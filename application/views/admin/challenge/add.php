<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">News & Blog</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">News & Blog</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add News & Blog
			  </h3>
				  <a href="<?php echo base_url('xAdmin/blog') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="blogfrm" name="blogfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">	
							 <div class="form-group">
								<label for="exampleInputEmail1">Blog Title</label>
								<input type="text" class="form-control" id="blog_name" placeholder="Enter Blog Title " name="blog_name" value="">								
								<span><?php echo form_error('blog_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Blog Description</label>
								<textarea class="form-control" id="blog_desc" placeholder="Enter Blog Description " name="blog_desc" rows="6" cols="12"></textarea>								
								<span><?php echo form_error('blog_desc'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">Blog URL</label>
								<input type="text" class="form-control" id="blog_url" placeholder="Enter Blog URL " name="blog_url" value="">								
								<span><?php echo form_error('blog_url'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">Blog Image</label>
								<input type="file" class="form-control" id="blog_img" placeholder="Upload Blog" name="blog_img" >
								
							  </div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#blogfrm').validate({
    rules: {
      blog_name: {
        required: true,
		minlength:5
      },
	  blog_desc: {
        required: true
      },
	  blog_url: {
        required: true,
		url:true
      }
    },
    messages: {
      blog_name: {
        required: "This field is required",
		minlength: "Enter Blog Title must be at least {0} characters long"
      },
	  blog_desc: {
        required: "This field is required"
      },
	  blog_url: {
        required: "This field is required",
		url: "Enter Blog url in proper format using http:// or https://"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


