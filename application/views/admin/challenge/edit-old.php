<style> 
	.previous_img_outer { position: relative; }
	.previous_img_outer a.img_outer img { max-width: 300px; max-height: 100px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }
</style>
<link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>bootstrap-datepicker.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url(); ?>assets/front/css/smart_wizard.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/front/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery.smartWizard.min.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Challenge Listing</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Challenge Listing</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Challenge Listing
					</h3>
				  <a href="<?php echo base_url('xAdmin/challenge') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div> 
				<!-- form start -->
				<?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
					<?php } 
					
				  $explodes = explode(",",$response_data[0]['audience_pref_id']);
				  $arr = array();
					foreach($explodes as $ids){
						array_push($arr, $ids);
					}
				  // print_r($arr);
				  
				  
				  // TagsDetail
				  
				  $exp = explode(",",$response_data[0]['tags_id']);
				  $arrTags = array();
					foreach($exp as $tid){
						array_push($arrTags, $tid);
					}
					
					
					// Technology
				  $tecID = explode(",",$response_data[0]['technology_id']);
				  $arrTech = array();
					foreach($tecID as $t_id){
						array_push($arrTech, $t_id);
					}
					
					
					
					
				?>
				<?php echo validation_errors(); ?>
				<div class="card-body">
					<form method="post" id="chfrm" name="chfrm" role="form" enctype="multipart/form-data">
						<div id="smartwizard_add_challenge">
							<ul>
								<li><a href="#step-1">Step 1<br /><small></small></a></li>
								<li><a href="#step-2">Step 2<br /><small></small></a></li>
								<li><a href="#step-3">Step 3<br /><small></small></a></li>
								<li><a href="#step-4">Step 4<br /><small></small></a></li>
							</ul>
							
							<div>
								<div id="step-1" class="mt-4">
									<div class="row">
										<div class="col-12">										
											<div class="form-group">
												<label>Challenge Title *</label>
												<input type="text" class="form-control" name="challenge_title" placeholder="Enter Challenge Title" value="<?php echo $response_data[0]['challenge_title']; ?>" readonly >
											</div>
											
											<div class="form-group">
												<label>Company Name *</label>
												<input type="text" class="form-control" name="company_name" placeholder="Enter Company Name" value="<?php echo $response_data[0]['company_name']; ?>" readonly>
											</div>		
													
											<div class="form-group row">
												<div class="col-6">
													<label>Banner Image</label>
													<input type="file" class="form-control" id="banner_img" placeholder="" name="banner_img" value="">
												</div>
											
												<?php
													if($response_data[0]['banner_img'] != '')
													{	?>								
													<div class="col-6">
														<label>Old Banner Image</label>
														<div class="previous_img_outer">
															<a class="img_outer" href="<?php echo base_url().'assets/challenge/'.$response_data[0]['banner_img']; ?>" target="_blank">
																<img src="<?php echo base_url().'assets/challenge/'.$response_data[0]['banner_img']; ?>">
															</a>
														</div>
													</div>
												<?php } ?>
											</div>
								
											<div class="form-group">
												<label>Company Profile</label>
												<textarea class="form-control" id="company_profile" placeholder="Enter Company Profile " name="company_profile" rows="6" cols="12"><?php echo $response_data[0]['company_profile']; ?></textarea>								
												<span><?php echo form_error('company_profile'); ?></span>
											</div>
											
											<div class="form-group">
												<label>Brief Info About Challenge *</label>
												<textarea class="form-control" name="challenge_details" placeholder="Enter Brief Info About Challenge" rows="6" cols="12" readonly><?php echo $response_data[0]['challenge_details']; ?></textarea>								
											</div>
											
											<div class="form-group">
												<label>Company Abstract About Challenge  *</label>
												<textarea class="form-control" name="company_details" placeholder="Enter Company Abstract About Challenge " rows="6" cols="12" readonly><?php echo $response_data[0]['challenge_abstract']; ?></textarea>								
											</div>
										</div>
									</div>
								</div>
								
								<div id="step-2" class="mt-4">
									<div class="row">
										<div class="col-12">
											<div class="form-group row">
												<div class="col-6">
													<label for="exampleInputEmail1">Launch Date *</label>
													<input type="date" class="form-control " id="launch_date" placeholder="Enter Launch Date " name="launch_date" value="<?php echo $response_data[0]['challenge_launch_date']; ?>">								
													<span><?php echo form_error('launch_date'); ?></span>
												</div>
												<div class="col-6">
													<label>Close Date *</label>
													<input type="date" class="form-control " id="close_date" placeholder="Enter Close Date " name="close_date" value="<?php echo $response_data[0]['challenge_close_date']; ?>">								
													<span><?php echo form_error('close_date'); ?></span>
												</div>	
											</div>	
								
											<div class="form-group">
												<label>Technology *</label>
												<select class="choose-tech select2" multiple="multiple" id="techonogy_id" name="techonogy_id[]" style="width: 100%;">
													<option value="">Please Select</option>
													<?php foreach($technology_data as $tech){ ?>
														<option value="<?php echo $tech['id'] ?>" <?php if(in_array($tech['id'], $arrTech)): ?> selected="selected" <?php endif; ?>><?php echo $tech['technology_name'] ?></option>								  
													<?php  } ?>
													<option value="0" <?php if(in_array(0, $arrTech)): ?> selected="selected" <?php endif; ?>>Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('techonogy_id'); ?></span>
											</div>
											
											<div class="col-md-12" id="otherTech">
												<div class="form-group">
													<label class="form-control-placeholder" for="cname">Other <em>*</em></label>													
													<input type="text" class="form-control" name="other_technology" id="other_technology" placeholder="Other Option" value="<?php  echo $response_data[0]['other_techonology'];  ?>">
													
													<span><?php //echo form_error('other_technology'); ?></span>
												</div>
											</div>
								
											<div class="form-group">
												<label>Tags *</label>
												<select class="choose-tags select2" multiple="multiple" id="tags_id" name="tags_id[]" style="width: 100%;">
													<option value="">Please Select</option>
													<?php foreach($tag_data as $tag){ ?>
														<option value="<?php echo $tag['id'] ?>" <?php if(in_array($tag['id'], $arrTags)): ?> selected="selected" <?php endif; ?>><?php echo $tag['tag_name'] ?></option>								  
													<?php  } ?>
													<option value="0" <?php if(in_array(0, $arrTags)): ?> selected="selected" <?php endif; ?>>Other</option>
												</select>
												<span><b>Note:</b> If the requisite field is not listed, kindly select "Other" and enter the desired text.</span>
												<span><?php echo form_error('tags_id'); ?></span>
											</div>
											
											<div class="col-md-12" id="otherTags"><br />
												<div class="form-group">
													<label class="form-control-placeholder" for="cname">Other <em>*</em></label>													
													<input type="text" class="form-control" name="other_tag" id="other_tag" placeholder="" value="<?php echo $response_data[0]['added_tag_name']; ?>">
													
													<span><?php //echo form_error('other_tag'); ?></span>
												</div>
											</div>
								
											<div class="form-group">
												<label>Audience Preference *</label><div class="clearfix"></div>
												<?php										 
													foreach($audience_data as $audience){ ?>
													<div class="form-check" style="display:inline-block; margin-right:10px;">
														<input class="form-check-input audi-chk" type="checkbox" id="audi_id" name="audi_id[]" value="<?php echo $audience['id'] ?>" <?php if(in_array($audience['id'], $arr)): ?> checked="checked" <?php endif; ?>>
														<label class="form-check-label"><?php echo $audience['preference_name'] ?></label>
													</div>
												<?php  } ?>
												<div class="form-check" style="display:inline-block;">
													<input class="form-check-input chk" type="checkbox" id="checkAll" >
													<label class="form-check-label">All</label>
												</div>
												<!--<div class="form-check" style="display:inline-block;">
													<input class="form-check-input chk" type="checkbox" id="audi_id" name="audi_id[]" value="0" <?php if(in_array(0, $arr)): ?> checked="checked" <?php endif; ?>>
													<label class="form-check-label">Other</label>
												</div>-->
											</div>
								
											<div class="form-group" id="otherAudience" <?php if(in_array(0, $arr)) { ?> style="display:block" <?php } else { ?> style="display:none" <?php } ?>>
												<input type="text" class="form-control" name="other_audience" id="other_audience" placeholder="Other Audience Preference" value="<?php echo $response_data[0]['other_audience']; ?>">
												<span><?php echo form_error('other_technology'); ?></span>
											</div>
								
											<div class="form-group">
												<label>Funding In Rupees & Reward<em></em></label>
												<div class="d-block">
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-chk" value="Funding" name="if_funding" id="if_funding" <?php if($response_data[0]['if_funding'] == 'Funding'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus1"> Funding</label>
													</div>
													<div class="form-check form-check-inline">
														<input type="checkbox" class="form-check-input fund-reward" value="Reward" name="if_reward" id="if_reward" <?php if($response_data[0]['if_reward'] == 'Reward'){ ?> checked="checked" <?php } ?>>
														<label class="form-check-label" for="techNovuus2"> Reward</label>
													</div>
												</div>									
											</div>
								
											<div class="form-group row">
												<div class="col-6" id="fund_agree">
													<label>Amount <em>*</em></label>
													<input type="text" class="form-control" name="is_amount" id="is_amount" placeholder="Enter Amount / Reward" value="<?php echo $response_data[0]['is_amount']; ?>">
												</div>
												
												<div class="col-6" id="fund_reward_div">
													<label>Reward  <em>*</em></label>
													<input type="text" class="form-control" name="fund_reward" id="fund_reward" placeholder="Enter Reward" value="<?php echo $response_data[0]['fund_reward']; ?>">
												</div>
											</div>
								
											<div class="form-group">
												<label>Expected TRL Solution *</label>
												<input type="text" class="form-control" placeholder="Enter Expected TRL Solution" value="<?php echo $response_data[0]['tr_name']; ?>" readonly>
											</div>
										</div>
									</div>
								</div>
								
								<div id="step-3" class="mt-4">
									<div class="row">
										<div class="col-12">								
											<div class="form-group">
												<label>Educational</label>
												<input type="text" class="form-control" name="educational"  placeholder="Educational" value="<?php echo $response_data[0]['education']; ?>" readonly >
											</div>
											
											<div class="form-group row">
												<div class="col-6">
													<label>From Age </label>
													<input type="text" class="form-control" name="from_age"  placeholder="From Age " value="<?php echo $response_data[0]['from_age']; ?>" readonly>
												</div>
											
												<div class="col-6">
													<label>To Age </label>									
													<input type="text" class="form-control" name="to_age" placeholder="To Age " value="<?php echo $response_data[0]['to_age']; ?>" readonly>
												</div>
											</div>
								
											<div class="form-group row">
												<div class="col-6">
													<label>Domain</label>									
													<input type="text" class="form-control" name="domain"  placeholder="Domain" value="<?php echo $response_data[0]['domainlist']; ?>" readonly>
												</div>
											
												<div class="col-6">
													<label>Geographical (States)</label>
													<input type="text" class="form-control" name="geographical" placeholder="Geographical (States)" value="<?php echo $response_data[0]['stateData']; ?>" readonly>
												</div>
											</div>
								
											<div class="form-group row">
												<div class="col-6">
													<label>Minimum Team</label>
													<input type="text" class="form-control" name="min_team"   placeholder="Minimum Team" value="<?php echo $response_data[0]['min_team']; ?>" readonly>
												</div>
											
												<div class="col-6">
													<label>Maximum Team</label>
													<input type="text" class="form-control"  name="max_team"  placeholder="Maximum Team" value="<?php echo $response_data[0]['max_team']; ?>" readonly>
												</div>
											</div>
								
											<div class="form-group">
												<label>Challenge Terms & Conditions </label>
												<textarea class="form-control" name="challenge_terms" cols="12" rows="4" readonly ><?php echo $response_data[0]['terms_txt']; ?></textarea>
											</div>
										</div>
									</div>
								</div>
								
								<div id="step-4" class="mt-4">
									<div class="row">
										<div class="col-12">
											<div class="form-group row">
												<div class="col-6">
													<label for="exampleInputEmail1">Contact Person Name *</label>
													<input type="text" class="form-control" id="contact_name" placeholder="Enter Contact Person Name" name="contact_name" value="<?php echo $response_data[0]['contact_person_name']; ?>">								
													<span><?php echo form_error('contact_name'); ?></span>
												</div>
											
												<div class="col-6">
													<label for="exampleInputEmail1">Contact Email ID *</label>
													<input type="email" class="form-control" id="contact_email" placeholder="Enter Email ID" name="contact_email" value="<?php echo $response_data[0]['email_id']; ?>">								
													<span><?php echo form_error('contact_email'); ?></span>
												</div>
											</div>
								
											<div class="form-group row">
												<div class="col-6">
													<label for="exampleInputEmail1">Contact Mobile No *</label>
													<input type="text" class="form-control" id="mobile" placeholder="Enter Mobile No" name="mobile" value="<?php echo $response_data[0]['mobile_no']; ?>" maxlength="10">								
													<span><?php echo form_error('mobile'); ?></span>
												</div>
												<div class="col-6">
													<label for="exampleInputEmail1">Office No. *</label>
													<input type="text" class="form-control" id="office_no" placeholder="Enter Office No" name="office_no" value="<?php echo $response_data[0]['office_no']; ?>">								
													<span><?php echo form_error('office_no'); ?></span>
												</div>
											</div>
								
											<div class="form-group">
												<input type="checkbox" class="" value="1" name="details_share" id="details_share" <?php if($response_data[0]['share_details'] == '1'){ ?> checked="checked" <?php } ?> >
												<label>Share contact details with site</label>
											</div>
								
											<div class="form-group">
												<label for="subcategory">Challenge Visibility *</label>
												<select class="form-control select2bs4" id="visibility" name="visibility">
													<option value="Public" <?php if($response_data[0]['challenge_visibility'] == 'Public'): ?> selected="selected" <?php endif; ?>>Public</option>
													<option value="Private" <?php if($response_data[0]['challenge_visibility'] == 'Private'): ?> selected="selected" <?php endif; ?>>Private</option>								  	
												</select>
												<span><?php echo form_error('challenge_visibility'); ?></span>
											</div>
								
											<div class="form-group">
												<label>Future opportunities</label>									
												<textarea class="form-control" cols="12" name="future_opportunities" rows="4" readonly ><?php echo $response_data[0]['future_opportunities']; ?></textarea>
											</div>
								
											<div class="form-group">
												<label>IP Clause</label>
												<div class="d-block">
													<?php foreach($ip_clause_data as $clause){ ?>
														<div class="form-check form-check-inline">
															<input type="radio" class="form-check-input" value="<?php echo $clause['id'] ?>" name="ip_cls" id="ip_cls" <?php if($response_data[0]['ip_clause'] == $clause['id']){ ?> checked="checked" <?php } ?> readonly >
															<label class="form-check-label" for="techNovuus1"><?php echo $clause['ip_name'] ?></label>
														</div>
													<?php } ?>	
												</div>									
											</div>
								
											<div class="form-group">
												<div class="form-check">
													<input class="form-check-input ex-ch" type="checkbox" name="external_funding" id="external_funding" value="1" <?php if($response_data[0]['is_external_funding'] == 1):  ?> checked="checked" <?php endif; ?>>
													<label class="form-check-label">External funding required?</label>
												</div>
											</div>
											<div class="form-group" id="external_fund">
												<label for="exampleInputEmail1">Percentage Of Funding (%) *</label>
												<input type="text" class="form-control" id="funding_amt" placeholder="" name="funding_amt" maxlength="2" value="<?php echo $response_data[0]['external_fund_details']; ?>">								
												<span><?php echo form_error('funding_amt'); ?></span>
											</div>
											<div class="form-group">
												<label class="form-check-label">Is this challenge exclusively listed on TechNovuus.? *</label><div class="clearfix"></div>
												<div class="form-check" style="display:inline-block; margin-right:10px;">
													<input class="form-check-input get-radio" type="radio" name="is_exclusive_challenge" id="is_exclusive_challenge" value="1" <?php if($response_data[0]['is_exclusive_challenge'] == 1):  ?> checked="checked" <?php endif; ?>>
													<label class="form-check-label">Yes</label>
												</div>
												<div class="form-check" style="display:inline-block; margin-right:10px;">
													<input class="form-check-input get-radio" type="radio" name="is_exclusive_challenge" id="is_exclusive_challenge" value="0" <?php if($response_data[0]['is_exclusive_challenge'] == 0):  ?> checked="checked" <?php endif; ?>>
													<label class="form-check-label">No</label>
												</div>
												<!--<div class="form-check">
													<input class="form-check-input" type="checkbox" name="is_exclusive" id="is_exclusive" value="1" <?php if($response_data[0]['is_exclusive_challenge'] == 1):  ?> checked="checked" <?php endif; ?>>
													<label class="form-check-label">Is this challenge exclusively listed on TechNovuus.? *</label>
												</div>-->
											</div>
											
											<div class="form-group" id="excl_challenge">
												<label for="first_name">Challenge Exclusively Content *</label>
												<textarea name="challenge_ex_details" id="challenge_ex_details" class="form-control" cols="12" rows="4"><?php echo $response_data[0]['exclusive_challenge_details']; ?></textarea>
												<span><?php echo form_error('challenge_ex_details'); ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->

<script type="text/javascript">
	function scroll_to_top(div_id='')
	{
		if(div_id == '') { $('html, body').animate({ scrollTop: $("body").offset().top }, 1000); } 
		else { $('html, body').animate({ scrollTop: $("#"+div_id).offset().top - 100 }, 1000); }
	}
	scroll_to_top();
	
	$(document).ready(function() 
	{
		//**** PREVENT FORM SUBMITTING WHEN PRESS ENTER ****
		$(window).keydown(function(event)
		{
			if(event.keyCode == 13) 
			{
				event.preventDefault();
				return false;
			}
		});
		
		//******* STEP SHOW EVENT *********
		$("#smartwizard_add_challenge").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) 
		{  
			if(stepNumber == 1){
				
				var myarray = '<?php echo $response_data[0]['technology_id'] ?>';		
				var myarray2 = '<?php echo $response_data[0]['tags_id'] ?>';
				console.log(myarray+"==="+myarray2);
				if(jQuery.inArray('0', myarray) !== -1){
					console.log("innnnnnnnn");
					$("#otherTech").show();
					$("#other_technology").attr('required', true);
				} else { console.log("outttttttt");
					$("#otherTech").hide();
					$("#other_technology").attr('required', false);
				}
				
				if(jQuery.inArray('0', myarray2) !== -1){
					$("#otherTags").show();
					$("#other_tag").attr('required', true);
					console.log("Tagggggggggggs innnnnnn");
				} else {
					console.log("Tagggggggggggs outtttt");
					$("#otherTags").hide();
					$("#other_tag").attr('required', false);
				}
				
				
			}
			//alert("You are on step "+stepNumber+" now");
			if (stepPosition === 'first') { $("#prev-btn").addClass('disabled'); $(".btnfinish").addClass('d-none'); } 
			else if (stepPosition === 'final') { $("#next-btn").addClass('disabled'); $(".btnfinish").removeClass('d-none'); } 
			else { $("#prev-btn").removeClass('disabled'); $("#next-btn").removeClass('disabled'); }
		});
		
		//******* STEP WIZARD *********
		$('#smartwizard_add_challenge').smartWizard(
		{
			/* selected: 2, */
			theme: 'arrows',
			transitionEffect: 'fade',
			showStepURLhash: false,
			/* enableURLhash:true,
			enableAllAnchors: false, */	
			toolbarSettings: 
			{
				toolbarExtraButtons: 
				[
					$('<button></button>').text('Update')
					.addClass('btn btn-primary btnfinish')
					.on('click', function(e)
					{   //alert();
						e.preventDefault();						
						var submit_flag = 0;
						
						if($("#challenge_ex_details").valid()==false) { submit_flag = 1; $("#challenge_ex_details").focus(); }
						if($("#is_exclusive_challenge").valid()==false) { submit_flag = 1; $("#is_exclusive_challenge").focus(); }
						if($("#funding_amt").valid()==false) { submit_flag = 1; $("#funding_amt").focus(); }
						if($("#external_funding").valid()==false) { submit_flag = 1; $("#external_funding").focus(); }
						if($("#visibility").valid()==false) { submit_flag = 1; $("#visibility").focus(); }
						if($("#office_no").valid()==false) { submit_flag = 1; $("#office_no").focus(); }
						if($("#mobile").valid()==false) { submit_flag = 1; $("#mobile").focus(); }
						if($("#contact_email").valid()==false) { submit_flag = 1; $("#contact_email").focus(); }
						if($("#contact_name").valid()==false) { submit_flag = 1; $("#contact_name").focus(); }	
						
						if($("#contact_name").valid()==false) { scroll_to_top('contact_name'); }
						else if($("#contact_email").valid()==false) { scroll_to_top('contact_email'); }
						else if($("#mobile").valid()==false) { scroll_to_top('mobile'); }
						else if($("#office_no").valid()==false) { scroll_to_top('office_no'); }
						else if($("#visibility").valid()==false) { scroll_to_top('visibility'); }
						else if($("#external_funding").valid()==false) { scroll_to_top('external_funding'); }
						else if($("#funding_amt").valid()==false) { scroll_to_top('funding_amt'); }
						else if($("#is_exclusive_challenge").valid()==false) { scroll_to_top('is_exclusive_challenge'); }
						else if($("#challenge_ex_details").valid()==false) { scroll_to_top('challenge_ex_details'); }
						
						if(submit_flag == 0)
						{
							//$('#chfrm').submit();
							swal(
							{
								title:"Confirm?",
								text: "Are you confirm to Edit challenge?",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Yes!'
							}).then(function (result) { if (result.value) { $('#chfrm').submit(); } });
						}
					}),
				]
			}
		});
		
		$("#smartwizard_add_challenge").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) 
		{
			var isValidate = true;			
			
			if(stepNumber==0 && stepDirection=="forward")
			{				
				if($("#company_profile").valid()==false) { isValidate= false; $("#company_profile").focus(); }
				if($("#banner_img").valid()==false) { isValidate= false; $("#banner_img").focus(); }
				
				if($("#banner_img").valid()==false) { scroll_to_top('banner_img'); }
				else if($("#company_profile").valid()==false) { scroll_to_top('company_profile'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==1 && stepDirection=="forward")
			{				
				
				if($("#fund_reward").valid()==false) { isValidate= false; $("#fund_reward").focus(); }
				if($("#is_amount").valid()==false) { isValidate= false; $("#is_amount").focus(); }
				if($("#if_funding").valid()==false) { isValidate= false; $("#if_funding").focus(); }
				if($("#other_audience").valid()==false) { isValidate= false; $("#other_audience").focus(); }
				if($("#audi_id").valid()==false) { isValidate= false; $("#audi_id").focus(); }
				if($("#tags_id").valid()==false) { isValidate= false; $("#tags_id").focus(); }
				if($("#techonogy_id").valid()==false) { isValidate= false; $("#techonogy_id").focus(); }
				if($("#close_date").valid()==false) { isValidate= false; $("#close_date").focus(); }
				if($("#launch_date").valid()==false) { isValidate= false; $("#launch_date").focus(); }
				if($("#other_tag").valid()==false) { isValidate= false; $("#other_tag").focus(); }
				if($("#other_technology").valid()==false) { isValidate= false; $("#other_technology").focus(); }
				
				
				if($("#launch_date").valid()==false) { scroll_to_top('launch_date'); }
				else if($("#close_date").valid()==false) { scroll_to_top('close_date'); }
				else if($("#techonogy_id").valid()==false) { scroll_to_top('techonogy_id'); }
				else if($("#tags_id").valid()==false) { scroll_to_top('tags_id'); }
				else if($("#audi_id").valid()==false) { scroll_to_top('audi_id'); }
				else if($("#other_audience").valid()==false) { scroll_to_top('other_audience'); }
				else if($("#if_funding").valid()==false) { scroll_to_top('if_funding'); }
				else if($("#is_amount").valid()==false) { scroll_to_top('is_amount'); }
				else if($("#fund_reward").valid()==false) { scroll_to_top('fund_reward'); }
				else if($("#other_tag").valid()==false) { scroll_to_top('other_tag'); }
				else if($("#other_technology").valid()==false) { scroll_to_top('other_technology'); }
				else { scroll_to_top('smartwizard_add_challenge'); }
			}
			
			if(stepNumber==2 && stepDirection=="forward")
			{				
				scroll_to_top('smartwizard_add_challenge');
			}
			
			
			if(stepDirection=="backward") { scroll_to_top('smartwizard_add_challenge') }
			return isValidate;
		})
	});
</script>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
	function getWordCount(wordString) {
	  var words = wordString.split(" ");
	  words = words.filter(function(words) { 
			return words.length > 0
		}).length;
	  return words;
	}
	
	
	
	$(document).ready(function () 
	{	
	
	$("#checkAll").change(function () {
		$(".chk").prop('checked', $(this).prop("checked"));
	});
	
		/*var date = new Date();
		var d = new Date();        
		d.setDate(date.getDate());
		var dateToday = new Date(); 	
		$("#launch_date").datepicker({
			todayBtn:  false,
			autoclose: true,
			//endDate: d,
			//startDate: '-0m',
			startDate: new Date(), 
			//minDate: dateToday,
			numberOfMonths: 3,
			todayHighlight: true,
			format: 'dd-mm-yyyy',
			}).on('changeDate', function (selected) {
			var minDate = new Date(selected.date.valueOf());
			$('#close_date').datepicker('setStartDate', minDate);
			$('#close_date').datepicker('setDate', minDate); 
		});
		$("#close_date").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true,
			//startDate: new Date(),
			endDate: '+3m',
			}).on('changeDate', function (selected) {
			var maxDate = new Date(selected.date.valueOf());
			$('#launch_date').datepicker('setEndDate', maxDate);
		});*/
	
		
	
		$("#fund_agree").hide();
		$("#fund_reward_div").hide();
		
		var re_ward = $('input[name="if_reward"]:checked').val();
		if(re_ward == 'Reward') { $("#fund_reward_div").show(); } else { $("#fund_reward_div").hide(); }
		
		var funds_ward = $('input[name="if_funding"]:checked').val();
		if(funds_ward == 'Funding'){ $("#fund_agree").show(); } else { $("#fund_agree").hide(); }
		
		//fund-reward //fund_agree //fund_reward_div
		$(".fund-chk").click(function () 
		{		   
			if($(this).is(":checked")) 
			{
				//$( ".audi-chk" ).prop( "checked", false );	
				$("#fund_agree").show();	
			}
			else { $("#fund_agree").hide();	 }		
		});

		$(".fund-reward").click(function () 
		{		   
			if($(this).is(":checked")) 
			{				
				//$( ".audi-chk" ).prop( "checked", false );	
				$("#fund_reward_div").show();	
			} 
			else { $("#fund_reward_div").hide();	}			
		});
	
		$(".audi-chk").click(function ()
		{
			var v_name = $(this).val();	
			if($(this).is(":checked")) 
			{
				$( ".chk" ).prop( "checked", false );
				$("#otherAudience").hide(); 
			}	
		});
		
		$(".chk").click(function () 
		{		   
			if($(this).is(":checked")) 
			{				
				$( ".audi-chk" ).prop( "checked", false );	
				$("#otherAudience").show();	
			} 
			else { $("#otherAudience").hide();  }
		});
		
		//challenge_ex_details
		var  valRadio = $("input[name='is_exclusive']:checked").val();
		
		if (valRadio == 0){			
			$("#excl_challenge").show();
			} else {			
			$("#excl_challenge").hide();			
		}
		
		$('.get-radio').on('click', function(){
			var red_val = $(this).val();
			if(red_val == 0) {
				$("#excl_challenge").show(); 
				} else {				
				$("#excl_challenge").hide();
			}			
		});
		
		//alert($('.ex-ch').is(':checked'));
		if ($('.ex-ch').is(':checked')){				
			$("#external_fund").show();
			} else { 
			$("#external_fund").hide();			
		}
		
		$('.ex-ch').on('click', function(){		  
			if($(this).is(":checked")) {
				$("#external_fund").show(); 
				} else {
				$("#funding_amt").val();
				$("#external_fund").hide();
			}			
		});	
		
		$( ".choose-tags" ).change(function() {	
				var values = $(this).val();	
				if(jQuery.inArray("0", values) !== -1){
					//console.log(values);
					 //$('#tags_id').val('0');					
					// $("#tags_id").select2();
					$("#otherTags").show();	
					$("#other_tag").attr('required', true);
				} else {  //console.log("Other =="+values);
					$("#otherTags").hide();
					$("#other_tag").attr('required', false);
				}
			});
			
			$( ".choose-tech" ).change(function() {	
				var values = $(this).val();	
				if(jQuery.inArray("0", values) !== -1){
					//console.log(values);
					// $('#techonogy_id').val('0');				
					// $("#techonogy_id").select2();
					$("#otherTech").show();	
					$("#other_technology").attr('required', true);
				} else {
					$("#otherTech").hide();
					$("#other_technology").attr('required', false);
				}
			});
				
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});		
				
		//add the custom validation method
		jQuery.validator.addMethod("maxCount",
		function(value, element, params) {
		  var count = getWordCount(value);
		  console.log(count);
		  if(count <= params[0]) {
				return true;
			}
		},
		jQuery.validator.format("A maximum of {0} words is required here.")
		);
		
		$('#chfrm').validate({
			rules: {
				banner_img: {
					valid_img_format: true
				},
				company_profile: {
					maxCount:['200']
				},
				launch_date: {
					required: true
				},
				close_date: {
					required: true
				},
				"techonogy_id[]": {
					required: true
				},
				other_technology:{
					required: true 
				},
				"tags_id[]": {
					required: true
				},
				"audi_id[]": {
					required: true
				},
				other_audience:{
					required: true 
				},			
				contact_name: {
					required: true,
					maxCount:['100']
				},			 
				mobile: {
					required: true,
					number: true,
					minlength:10,
					maxlength:11
				},
				contact_email: {
					required: true,
					email: true
				},
				office_no: {
					required: true,
				},
				challenge_visibility: {
					required: true
				},            
				/*external_funding: {
					required: true
				},*/
				funding_amt:{
					required: true,
					number: true
				},
				is_amount: {
					required: true,
					number: true
				},
				fund_reward:{
					required: true
				},
				is_exclusive_challenge: {
					required: true
				},
				challenge_ex_details: {
					required: true,
					maxCount:['100']
				}
			},
			messages: {
				banner_img: {
					valid_img_format: "Please upload only image file"
				},
				company_profile: {
					required: "This field is required"
				},
				
				launch_date: {
					required: "This field is required"              
				},
				close_date: {
					required: "This field is required"
				},
				techonogy_id: {
					required: "This field is required"
				},
				other_technology: {
					required: "This field is required"
				},
				tags_id: {
					required: "This field is required"
				},
				audi_id: {
					required: "This field is required"
				},
				other_audience: {
					required: "This field is required"
				},
				
				contact_name: {
					required: "This field is required"
				},
				mobile: {
					required: "This field is required"
				},
				contact_email: {
					required: "This field is required"
				},
				office_no: {
					required: "This field is required"
				},			 
				challenge_visibility: {
					required: "This field is required"
				},            
				/*external_funding: {
					required: "This field is required"
				},*/
				funding_amt:{
					required: "This field is required"
				},
				is_amount: {
					required: "This field is required"
				},
				fund_reward:{
					required: "This field is required"
				},
				is_exclusive_challenge: {
					required: "This field is required"
				},
				challenge_ex_details: {
					required: "This field is required"
				}  
			},
			errorElement: 'span',			
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
	
	$(document).ready(function()
    {          
        $('input[type="file"]').change(function(e)
        {              
            var fileName = e.target.files[0].name;
            $(this).parent().find('input').after('<span class="disp_filename" style="font-size:12px">'+fileName+'</span>');
        });          
    });
</script>


