<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Newsletter Unsubscribe List </h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Newsletter Unsubscribe List </li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
		  <div class="card-header">
			  <h3 class="card-title">
				  Newsletter Unsubscribe List
			  </h3>
		  </div>
		
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th style="width:5%">Sr.No.</th>
					 <th style="width:5%">ID</th>
					 <th style="width:75%">Email ID</th>
					 <!--<th style="width:10%">Email</th>
					 <th style="width:10%">SMS</th>-->
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $newsletter) 
                     { 
					 
					 if($newsletter['is_deleted'] == 1){
						$userShow = "Unsubscribe"; 
					 } else {
						$userShow = "Subscribe"; 
					 }
					 ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
					 <td style="width:5%"><?php echo $newsletter['id']; ?></td>
                     <td style="width:40%"><?php echo $newsletter['email_id'] ; ?></td> 
					 <!--<td style="width:10%" class="text-center">
						<div class="form-check">							
						  <input class="form-check-input check-notification" type="checkbox" name="chk_email" id="chk_email" value="<?php echo $newsletter['id'] ?>" <?php if($newsletter['email_notification'] == 'Y'): ?> checked="checked" <?php endif; ?>>
						</div>
					 </td> 
					 <td style="width:10%" class="text-center">
						<div class="form-check">
						  <input class="form-check-input check-alerts" type="checkbox" name="chk_sms" id="chk_sms" value="<?php echo $newsletter['id'] ?>" <?php if($newsletter['sms_notification'] == 'Y'): ?> checked="checked" <?php endif; ?>>
						</div>
					 </td> 	-->
                     <td style="width:35%" nowrap>
					<?php      
					if($newsletter['status']=="Active"){
						$status = 'Block';
					?>
					<a href="<?php echo base_url(); ?>xAdmin/newsletter/changeStatus/<?php echo $newsletter['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
					   else if($newsletter['status']=="Block"){
						$status = 'Active';
					   ?>
					<a href="<?php echo base_url(); ?>xAdmin/newsletter/changeStatus/<?php echo $newsletter['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
					 ?>
					 
					
					<a href="javascript:void(0)" data-id="<?php echo $newsletter['id'] ?>" class="deleted-check btn btn-danger btn-sm"  id="deleted-stac-<?php echo $newsletter['id'] ?>">
						Restore
					</a>
					
					 
                     </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                                                                                    
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$('.check-notification').on('click', function(){			
			
            if ($(this).prop("checked") == true) {
                var chkStatus = "Y";
            } else {
                var chkStatus = "N";
            }
		  	var id = $(this).val();
			var csrf_test_name	= $('.txt_csrfname').val();
			var post_type = 'Email';
			var base_url = '<?php echo base_url('xAdmin/newsletter/modifiedAlerts'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,post_type:post_type,chkStatus:chkStatus,csrf_test_name:csrf_test_name},				
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					$('.txt_csrfname').val(token);
					swal({
							title: 'Success!',
							text: "Email status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	   
	   $('.check-alerts').on('click', function(){			
			
            if ($(this).prop("checked") == true) {
                var chkStatus = "Y";
            } else {
                var chkStatus = "N";
            }
		  	var id = $(this).val();
			var csrf_test_name	= $('.txt_csrfname').val();
			var post_type = 'SMS';
			var base_url = '<?php echo base_url('xAdmin/newsletter/modifiedAlerts'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,post_type:post_type,chkStatus:chkStatus,csrf_test_name:csrf_test_name},				
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					$('.txt_csrfname').val(token);
					swal({
							title: 'Success!',
							text: "SMS status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
						
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	   
	   $("body").on("click", "#example1 tbody tr a.deleted-check", function (e) {
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/newsletter/deleteStatus'); ?>';			
			swal({
			  title: "Are you sure?",
			  text: "Do you want to change status for this record?",
			  type: "warning",
			  buttons: true,
			  dangerMode: true,
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, proceed it!'
			})
			.then((test) => {
			  if (test.value) {
				 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var userId = returndata.response_id;
					var textShow = returndata.dynamicText;
					var newText = returndata.spanText;
					var removeID = returndata.remove_id;
					//alert(status);
					$('.txt_csrfname').val(token);					
					$("a#deleted-stac-"+id).text(newText);	
					//$('.remove-tr-'+removeID).remove();
					$('.remove-tr-'+removeID).next('tr.child').remove();
					$('.remove-tr-'+removeID).remove();
					swal({
							title: 'Success!',
							text: "User "+textShow+" Successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
						location.reload();
						
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			  } else {
				//swal("Your imaginary file is safe!");
			  }
			});
			
			
       });
	
    });
</script>