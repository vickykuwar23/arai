<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Promo Code</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Promo Code</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Promo Code
			  </h3>
				  <a href="<?php echo base_url('xAdmin/promocode') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="pfrm" name="pfrm" role="form" >
						  <div class="row">
							<div class="col-6">

							  <div class="form-group">
								<label for="exampleInputEmail1">Plan *</label>
								<select name="plan_id" id="plan_id" class="form-control">
									<option value="">-- Select --</option>
									<?php foreach ($plans as $key => $value): ?>
									<option value="<?php echo $value['plan_id'] ?>"><?php echo $value['plan_name'];if($value['plan_category']==1) {echo '(Individual)';}else if($value['plan_category']==2) {echo '(Organization)';}  ?>
										

									</option>
									<?php endforeach ?>
								</select>
								<span><?php echo form_error('code_type'); ?></span>
							  </div>

							  <div class="form-group">
								<label for="exampleInputEmail1">Type *</label>
								<select name="code_type" id="code_type" class="form-control">
									<option value="">-- Select --</option>
									<option value="Discount">Discount Promocode</option>
									<option value="Corporate">Corporate Linkage Promocode</option>
								</select>
								<span><?php echo form_error('code_type'); ?></span>
							  </div>	
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Start Date *</label>
								<input type="date" class="form-control" id="start_date" placeholder="Select Start Date" value="" name="start_date"  >
								<span><?php echo form_error('start_date'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">End Date *</label>
								<input type="date" class="form-control" id="end_date" placeholder="Select End Date" value="" name="end_date"  >
								<span><?php echo form_error('end_date'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Allowed Usage Count *</label>
								<input type="text" class="form-control" id="use_cnt" placeholder="Enter Usage Count" value="1" name="use_cnt"  >
								<span><?php echo form_error('use_cnt'); ?></span>
							  </div>
							<div class="form-group">
								<label for="exampleInputEmail1">Promo Code Discount (in Percentage) *</label>
								<input type="text" class="form-control" id="discount_apply" placeholder="Enter Discount " value="" name="discount_apply" maxlength="150">
								<span><?php echo form_error('discount_apply'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Generate Promo Code *<span>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn-click" style="cursor:pointer;font-size:12px;">Click Here</a></span></label>
								<input type="text" class="form-control" id="p_code" placeholder="Generate Promo Code" value="" name="p_code" maxlength="6" readonly >
								<span><?php echo form_error('p_code'); ?></span>
							  </div>
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#pfrm').validate({
    rules: {
	  code_type:{
		required: true  
	  },
	 plan:{
    required: true  
    },	
    	
      discount_apply: {
        required: true
      },
	  p_code: {
        required: true
      },
	  start_date: {
        required: true
      },
	  end_date: {
        required: true
      },
	  use_cnt: {
        required: true,
		number:true
      }
    },
    messages: {
	  code_type:{
		required: "This field is required"
	  },	
      discount_apply: {
        required: "This field is required"
      },
	  p_code: {
        required: "This field is required"
      },
	  start_date: {
        required: "This field is required"
      },
	  end_date: {
        required: "This field is required"
      },
	  use_cnt: {
         required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
  $(document).on("click", ".btn-click", function(e){
	  var cs_t = 	$('.token').val();
	  var base_url = '<?php echo base_url(); ?>'; 
		$.ajax({
            type:'POST',
            url: base_url+'xAdmin/promocode/generateCode',
            data:'csrf_test_name='+cs_t,
			dataType:"html",
            success:function(data){
				var output = JSON.parse(data);	
				console.log(output.random);
				$("#p_code").val(output.random);
				$(".token").val(output.token);
            }
        });
  });
  
});
</script>


