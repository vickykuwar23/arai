<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Participation Log</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Participation Log</li>
					</ol>
				</div>
				
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<?php if( $this->session->flashdata('success')){ ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5><i class="icon fas fa-check"></i> Success!</h5>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>
			<div class="card ">
				<div class="card-header">
					<h3 class="card-title">
						Participation Log
					</h3>
				</div>
				
				<!-- Small boxes (Stat box) -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th class="text-center">Challenge Id</th>
								<th class="text-center">Challenge Name</th>
								<th class="text-center">User Name</th>
								<th class="text-center">Date</th>
							</tr>
						</thead>
						
						<tbody>
							<?php  
								$i=1;
								foreach($records as $res) 
								{ ?>
								<tr>
									<td class="text-center"><?php echo $i; ?></td>
									<td><?php echo $res['challenge_id'] ; ?></td>
									<td><?php echo $res['challenge_title'] ; ?></td>
									<td><?php echo $res['user_name'] ; ?></td>
									<td><span class='d-none'><?php echo $res['created_on']; ?></span><?php echo date("d M, Y h:i a", strtotime($res['created_on'])); ?></td>
								</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
					<!-- ./col -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		$("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});	
	});
</script>