<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Promocode Association</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Promocode Association</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php }  ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Users
              </h3>
                 <!--<a href="<?php echo base_url('xAdmin/users/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>-->
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Fullname</th>
					 <th>Email</th>
					
                     <th>Promo Code</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $user_data) 
                     { 
					 
					 
					 if($user_data['cat_id'] == 1){
						$file = 'viewDetails'; 
						$fullname = $user_data['title'].ucfirst($user_data['first_name'])."&nbsp;".ucfirst($user_data['middle_name'])."&nbsp;".ucfirst($user_data['last_name']);
						
					 } else {
						$file = 'organizationDetails';
						$fullname = ucwords($user_data['institution_full_name']);
						
					 }
					 
					 if($user_data['is_featured'] == 'Y'){
						$textShow = "Featured"; 
					 } else {
						$textShow = "Not-Featured"; 
					 }

					 if($user_data['admin_approval'] == 'yes'){
						$ApprovalStr = "Approved"; 
					 } else {
						$ApprovalStr = "Not-Approved"; 
					 }
					 
					 $countryCode = $this->master_model->getRecords("country", array("id" => $user_data['country_code']));
					 $codes = $countryCode[0]['phonecode'];
					 
					 ?>
                  <?php if ($fullname!=''): ?>
                  	
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:15%"><?php echo $fullname; ?></td> 
					
					 <td style="width:20%"><?php echo $user_data['email'] ; ?></td>
					 <td style="width:20%"><?php echo $user_data['promo_code'] ; ?></td> 

					 
					
					
                     
                  </tr>
                  <?php endif ?>
                  
                  <?php $i++; } ?>
                  </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                                                                                    
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
});
		
		
</script>