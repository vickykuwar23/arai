<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Configure Email/SMS</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Configure Email/SMS</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Configure Email/SMS
			  </h3>
				  
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="settingfrm" name="settingfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">
							 
							  <div class="form-group">
								<label for="exampleInputEmail1">Live Username</label>
								<input type="text" class="form-control" id="live_user" placeholder="Enter Live Username" value="<?php echo $setting_data[0]['live_user']; ?>" name="live_user" >
								<span><?php echo form_error('live_user'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Live Password</label>
								<input type="text" class="form-control" id="live_pass" placeholder="Enter Live Password" value="<?php echo $setting_data[0]['live_pass']; ?>" name="live_pass" >
								<span><?php echo form_error('live_pass'); ?></span>
							  </div>	
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Live Port</label>
								<input type="text" class="form-control" id="live_port" placeholder="Enter Live Port" name="live_port" value="<?php echo $setting_data[0]['live_port']; ?>">
								<span><?php echo form_error('live_port'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Live Host</label>
								<input type="text" class="form-control" id="live_host" placeholder="Enter Live Host" name="live_host" value="<?php echo $setting_data[0]['live_host']; ?>">
								<span><?php echo form_error('live_host'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">SMS Username</label>
								<input type="text" class="form-control" id="sms_user" placeholder="Enter SMS Username" name="sms_user" value="<?php echo $setting_data[0]['sms_user']; ?>">
								<span><?php echo form_error('sms_user'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">SMS Pin</label>
								<input type="text" class="form-control" id="sms_pin" placeholder="Enter SMS Pin" name="sms_pin" value="<?php echo $setting_data[0]['sms_pin']; ?>">
								<span><?php echo form_error('sms_pin'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Email Protocol</label>
								<input type="text" class="form-control" id="protocol" placeholder="Enter Email Protocol" value="<?php echo $setting_data[0]['protocol']; ?>" name="protocol" >
								<span><?php echo form_error('protocol'); ?></span>
							  </div>	
								
							</div>
								<!-- /.card-body -->
							<div class="col-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Test Username</label>
									<input type="text" class="form-control" id="test_user" placeholder="Enter Test Username" value="<?php echo $setting_data[0]['test_user']; ?>" name="test_user" >
									<span><?php echo form_error('test_user'); ?></span>
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Test Password</label>
									<input type="text" class="form-control" id="test_pass" placeholder="Enter Test Password" value="<?php echo $setting_data[0]['test_pass']; ?>" name="test_pass" >
									<span><?php echo form_error('test_pass'); ?></span>
								  </div>	
								  
								  <div class="form-group">
									<label for="exampleInputEmail1">Test Port</label>
									<input type="text" class="form-control" id="test_port" placeholder="Enter Test Port" name="test_port" value="<?php echo $setting_data[0]['test_port']; ?>">
									<span><?php echo form_error('test_port'); ?></span>
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Test Host</label>
									<input type="text" class="form-control" id="test_host" placeholder="Enter Test Host" name="test_host" value="<?php echo $setting_data[0]['test_host']; ?>">
									<span><?php echo form_error('test_host'); ?></span>
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">SMS Signature</label>
									<input type="text" class="form-control" id="sms_sign" placeholder="Enter SMS Signature" name="sms_sign" value="<?php echo $setting_data[0]['sms_sign']; ?>">
									<span><?php echo form_error('sms_sign'); ?></span>
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Aditional Field</label>
									<input type="text" class="form-control" id="sms_field_1" placeholder="Additional Field" name="sms_field_1" value="<?php echo $setting_data[0]['sms_field_1']; ?>">
									<span><?php //echo form_error('sms_field_1'); ?></span>
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Status</label>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="status" name="status" <?php if($setting_data[0]['status'] == 'Active'){ ?> checked="checked" <?php } ?> value="0">
									  <label for="customRadio1" class="form-check-label">Live Mode</label>
									</div>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="status" name="status" <?php if($setting_data[0]['status'] == 'Block'){ ?> checked="checked" <?php } ?> value="1">
									  <label for="customRadio2" class="form-check-label">Test Mode</label>
									</div>
									<span></span>
								  </div>
							</div>
							<div class="col-12">
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>	
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#settingfrm').validate({
    rules: {
      protocol: {
        required: true
      },
	  live_user: {
        required: true
      },
	  live_pass: {
		  required: true
      },
	  live_port: {
        required: true
      },
	  live_host: {
        required: true
      },
	  test_user: {
        required: true
      },
	  test_pass: {
        required: true
      },
	  test_port: {
        required: true
      },
	  test_host: {
        required: true
      },
	  sms_user: {
        required: true
      },
	  sms_sign: {
        required: true
      },
	  sms_pin: {
        required: true
      }
    },
    messages: {
      protocol: {
        required: "This field is required"
      },
	  live_user: {
        required: "This field is required"
      },
	  live_pass: {
        required: "This field is required"		
      },
	  live_port: {
        required: "This field is required"
      },
	  live_host: {
        required: "This field is required"
      },
	  test_user: {
        required: "This field is required"
      },
	  test_pass: {
        required: "This field is required"
      },
	  test_port: {
        required: "This field is required"
      },
	  test_host: {
        required: "This field is required"
      },
	  sms_user: {
        required: "This field is required"
      },
	  sms_sign: {
        required: "This field is required"
      },
	  sms_pin: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


