<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Setting</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Setting</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Setting
			  </h3>
				  <a href="<?php echo base_url('xAdmin/setting') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				    <?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Success!</h5>
					 <?php echo $this->session->flashdata('success'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="settingfrm" name="settingfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">
							 <div class="form-group">
								<label for="exampleInputEmail1">Site Title</label>
								<input type="text" class="form-control" id="site_title" placeholder="Enter Site Title" value="<?php echo $setting_data[0]['site_title']; ?>" name="site_title" >
								<span><?php echo form_error('site_title'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Email ID</label>
								<input type="text" class="form-control" id="email_id" placeholder="Enter Email ID" value="<?php echo $setting_data[0]['email_id']; ?>" name="email_id" >
								<span><?php echo form_error('email_id'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Contact No.</label>
								<input type="text" class="form-control" id="contact_no" placeholder="Enter Contact No." value="<?php echo $setting_data[0]['contact_no']; ?>" name="contact_no" >
								<span><?php echo form_error('contact_no'); ?></span>
							  </div>	
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Upload Favicon</label>
								<input type="file" class="form-control" id="favicon_icon" placeholder="Upload Favicon" name="favicon_icon" >
								<span><?php echo form_error('favicon_icon'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Upload Logo</label>
								<input type="file" class="form-control" id="site_logo" placeholder="Upload Logo" name="site_logo" >
								<span><?php echo form_error('site_logo'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Meta Title</label>
								<input type="text" class="form-control" id="meta_title" placeholder="Enter Meta Title" value="<?php echo $setting_data[0]['meta_title']; ?>" name="meta_title" >
								<span><?php echo form_error('meta_title'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Meta Description</label>
								<textarea class="form-control" id="meta_description" placeholder="Enter Meta Description" name="meta_description" ><?php echo $setting_data[0]['meta_description']; ?></textarea>
								<span><?php echo form_error('meta_description'); ?></span>
							  </div>							 
								<div class="form-group">
								<div class="form-check" style="display:inline-block;">
									<input class="form-check-input check-notification" type="checkbox" name="challenge_alert" id="challenge_alert" value="Y" <?php if($setting_data[0]['challenge_alert'] == 'Y'): ?> checked="checked" <?php endif; ?>>
									<label class="form-check-label">New Challenges Alert To Subscribe Users</label>
								</div>
									
							  </div>
							  
							  
								
							</div>
								<!-- /.card-body -->
							<div class="col-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Facebook Page URL</label>
									<input type="url" class="form-control" id="facebook_url" placeholder="Enter Facebook Page URL" name="facebook_url" value="<?php echo $setting_data[0]['facebook_url']; ?>">								
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Twitter Page URL</label>
									<input type="url" class="form-control" id="twitter_url" placeholder="Enter Twitter Page URL" name="twitter_url" value="<?php echo $setting_data[0]['twitter_url']; ?>">								
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">LinkedIn Page URL</label>
									<input type="url" class="form-control" id="linkedin_url" placeholder="Enter LinkdIn Page URL" name="linkedin_url" value="<?php echo $setting_data[0]['linkedin_url']; ?>">								
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Google+ Page URL</label>
									<input type="url" class="form-control" id="google_plus_url" placeholder="Enter Google+ Page URL" name="google_plus_url" value="<?php echo $setting_data[0]['google_plus_url']; ?>">								
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Additional Field 1</label>
									<input type="text" class="form-control" id="field_1" placeholder="Enter Content 1 " name="field_1" value="<?php echo $setting_data[0]['field_1']; ?>">								
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Additional Field 2</label>
									<input type="text" class="form-control" id="field_2" placeholder="Enter Content 2" name="field_2" value="<?php echo $setting_data[0]['field_2']; ?>">								
								  </div>
								<div class="form-group">
								<label for="exampleInputEmail1">Office Address</label>
								<textarea class="form-control" id="off_address" placeholder="Enter Office Address" name="off_address" ><?php echo $setting_data[0]['off_address']; ?></textarea>
								<span><?php echo form_error('off_address'); ?></span>
							  </div>
							  <div class="form-group">
								<div class="form-check" style="display:inline-block;">
									<input class="form-check-input check-notification" type="checkbox" name="news_alert" id="news_alert" value="Y" <?php if($setting_data[0]['news_alert'] == 'Y'): ?> checked="checked" <?php endif; ?>>
									<label class="form-check-label">Latest News & Updates To Subscribe Users</label>
								</div>
									
							  </div>
							</div>
							<div class="col-12">
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>	
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#settingfrm').validate({
    rules: {
      site_title: {
        required: true,
		minlength:5
      },
	  email_id: {
        required: true,
		email:true
      },
	  contact_no: {
        required: true
		//number:true,
		//minlength:10,
		//maxlength:10
      },
	  meta_title: {
        required: true
      },
	  meta_description: {
        required: true
      },
	  facebook_url: {
        required: false,
		url:true
      },
	  twitter_url: {
        required: false,
		url:true
      },
	  linkedin_url: {
        required: false,
		url:true
      },
	  google_plus_url: {
        required: false,
		url:true
      },
	  off_address: {
        required: true
      }
    },
    messages: {
      site_title: {
        required: "This field is required",
		minlength: "Enter User Type must be at least {0} characters long"
      },
	  email_id: {
        required: "This field is required"
      },
	  contact_no: {
        required: "This field is required"
		//minlength: "Enter Contact No. must be at least {0} digit long",
		//maxlength: "Enter Contact No. must be at least {0} characters"
		
      },
	  meta_title: {
        required: "This field is required",
		minlength: "Enter User Type must be at least {0} characters long"
      },
	  meta_description: {
        required: "This field is required",
		minlength: "Enter User Type must be at least {0} characters long"
      },
	  facebook_url: {
        url: "Enter Facebook URL with http:// or https://"
      },
	  twitter_url: {
        url: "Enter Twitter URL with http:// or https://"
      },
	  linkedin_url: {
        url: "Enter LinkedIn URL with http:// or https://"
      },
	  google_plus_url: {
        url: "Enter Google+ URL with http:// or https://"
      },
	  off_address: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


