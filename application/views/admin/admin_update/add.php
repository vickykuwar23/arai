<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>

<style>
	/* Prelaoder */
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
	.currency_div {
    	margin-top: 40px;
	}
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }

	.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 15px !important;
		}
	.error{color: red;}
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	@-webkit-keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	@keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	  .custom-file input[type="file"] {
            display: none;
        }
        
        .custom-file .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .file-details .file-list {
	border: 1px solid #ddd;
	padding: 8px 5px;
	border-radius: 4px;
	text-align: center;
	position: relative;
	margin: 0 0 15px 0;
	font-size: 12px;
	word-break: break-all;
	height:100px;
}

.file-details .file-list .file_ext_title h4 {
	margin: 0;
	font-size: 18px !important;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	line-height: 80px;
}

.file-details .file-list a.file-close {
	position: absolute;
	right: 0;
	top: 0;
	width: 25px;
	color: #e23751;
	background: #fff;
	border: 1px solid #ddd;
}

.file-details .file-list span {
	width: 100%;
	display: inline-block;
	font-size: 42px;
	color: #FFC107;
	height: 50px;
}
label em {
	color: red;
}

.mt-45 {
	margin-top: 36px;
}
</style>
<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Technovuus Updates</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Technovuus Updates</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						<?php if($mode=='Add'){echo 'Add'; }else{ echo 'Update';} ?> Technovuus Updates
					</h3>
				  <a href="<?php echo base_url('xAdmin/admin_update') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div>
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="POST" id="ResourceForm" name="BlogForm" enctype="multipart/form-data">		
						<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

						<?php if ($mode == 'Update') { ?>
							<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">News ID  </label>
									<input type="text" class="form-control"  value="<?php echo $form_data[0]['id_disp']  ?>" readonly>
									
								</div>
							</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">News Title<em>*</em></label>
									<input type="text" class="form-control" name="news_title" id="news_title" value="<?php if($mode == 'Add') { echo set_value('news_title'); } else { echo $form_data[0]['feed_title']; } ?>">
									<?php if(form_error('news_title')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('news_title'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row">
						    	<div class="col-md-12">

								<div class="form-group">
									<label>News Description <em style="color: red;">*</em></label>
									<textarea name="news_description" id="news_description" class="form-control" cols="12" rows="4"><?php if($mode=='Update'){ echo $form_data[0]['feed_description']; }else{echo set_value('news_description');}?></textarea>
									<?php if(form_error('news_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('news_description'); ?></label> <?php } ?>									
								</div>

							</div>
						</div>

						

						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum pl-0">
							    <label class="form-check-label" for="exampleCheck1"><strong>Include Banner</strong></label>
								 <input type="hidden" name="banner_check" value="0">
							    <input type="checkbox" value="1" name="banner_check" id="banner_check" class="banner_check" <?php if($mode == 'Update' && $form_data[0]['banner_check']==1){echo 'checked';} ?>  value="1" />
							  </div>
							  </div>
							
							</div>
						</div>

						<?php if($mode == 'Update' && $form_data[0]['banner_check']==1){
							$display_banner= '';
						}else{
							$display_banner= 'd-none';
						} ?>

						<div class="row banner_row <?php echo $display_banner ?>">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button type="button" class="btn btn-upload"> <em style="color:red;">*</em><strong> Banner</strong> </button>
									
									<input  type="file" class="form-control" name="banner_img"  id="banner_img" <?php if($mode == 'Add') { echo 'required'; } ?>>
									<p>Only .jpg, .jpeg, .png image formats below 100 kb are accepted</p>
								</div>		
								<?php if($banner_img_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $banner_img_error; ?></label> <?php } ?>
							</div>	
							
							<?php $dynamic_cls = $full_img = $onclick_fun = '';
								if($mode == 'Add')
								{
									$dynamic_cls = 'd-none';
								}
								else
								{
									$full_img = base_url().'uploads/admin_update_banner/'.$form_data[0]['banner'];
								} ?>
								
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="banner_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
										</div>
									</div>
								</div>
						</div>


						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum pl-0">
							    <label class="form-check-label" for="exampleCheck1"><strong>Include Video</strong></label>
								<input type="hidden" name="video_check" value="0">
							    <input type="checkbox" name="video_check" id="video_check" class="video_check"  value="1" <?php if($mode == 'Update' && $form_data[0]['video_check']==1){echo 'checked';} ?> />
							  </div>
							  </div>
							
							</div>
						</div>

					<?php if($mode == 'Update' && $form_data[0]['video_check']==1){
					?>
					<div class="video_wrapper">
					<?php foreach ($video_data as $key => $video) { ?>
					
					<div class="appended_div_<?php echo $key ?>">
						<div class="row video_row ">
							<div class="col-md-4">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Caption</label><em style="color: red;">*</em>
									<input type="text" class="form-control caption" name="caption[]" id="caption1" value="<?php if($mode == 'Add') { echo set_value('caption'); } else { echo $video['caption']; } ?>">
									<?php if(form_error('caption')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('caption'); ?></label> <?php } ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Link</label><em style="color: red;">*</em>
									<input type="text" class="form-control" name="video_url[]" id="video_url1" value="<?php if($mode == 'Add') { echo set_value('video_url'); } else { echo $video['video_url']; } ?>">
									<?php if(form_error('video_url')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('video_url'); ?></label> <?php } ?>
								</div>
							</div>
							<?php if ($key==0){ ?>
								
							<div class="col-md-2">
								<div class="form-group">
								<button type="button" class="btn btn-success btn-sm mt-45" onclick="append_video_row()">+</button>
								</div>
							</div>
							<?php } else{ ?>
							<div class="col-md-2">
								<div class="form-group">
								<button type="button" class="btn btn-danger btn-sm mt-45" onclick="remove_video_row('<?php echo $key ?>')">-</button>
								</div>
							</div>
							<?php } ?>	

						<input type="hidden" value="<?php if($mode=='Add'){echo 1;}else{echo count($video_data);} ?>" name="video_warapper_count" id="video_warapper_count"> 
						</div>
						</div>
							
						<?php } ?>
						</div>

						<?php }else{ ?>
						<div class="video_wrapper d-none">
						<div class="row video_row ">
							<div class="col-md-4">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Caption</label><em style="color: red;">*</em>
									<input type="text" class="form-control caption" name="caption[]" id="caption1" value="">
									<?php if(form_error('caption')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('caption'); ?></label> <?php } ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Link</label><em style="color: red;">*</em>
									<input type="text" class="form-control" name="video_url[]" id="video_url1" value="">
									<?php if(form_error('video_url')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('video_url'); ?></label> <?php } ?>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
								<button type="button" class="btn btn-success btn-sm mt-45" onclick="append_video_row()">+</button>
								</div>
							</div>

						<input type="hidden" value="1" name="video_warapper_count" id="video_warapper_count"> 
						</div>
						</div>
						<?php } ?>
						

						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum pl-0">
							    <label class="form-check-label" for="exampleCheck1"><strong>Include Files</strong></label>
								<input type="hidden" name="files_check" value="0">
							    <input type="checkbox" name="files_check" id="files_check" class="files_check"  value="1" <?php if($mode == 'Update' && $form_data[0]['files_check']==1){echo 'checked';} ?> />
							  </div>
							  </div>
							
							</div>
						</div>
						<?php if($mode == 'Update' && $form_data[0]['files_check']==1){
							$files_Disaplay= '';
						}else{
							$files_Disaplay= 'd-none';
						} ?>


						
						<div class="files_wrapper <?php echo $files_Disaplay ?>">
						<div class="row"><div class="col-md-12"></div></div>
						<div class="file-details" style="min-height:100px;">
							
							<?php if($mode == 'Update' && count($files_data) > 0)
							{	
								echo '<div class="row">';
								foreach($files_data as $res)
								{	?>
									<div class="col-md-2" id="r_files_outer<?php echo $res['feed_id']."_".$res['file_id']; ?>">
										<div class="file-list">
												<a href="javascript:void(0)" onclick="remove_content_file('<?php echo $res['feed_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close">X</a>
												<a class="file_ext_title" href="<?php echo base_url().'uploads/admin_update_files/'.$res['file_name']; ?>" target="_blank">
													<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/admin_update_files/'.$res['file_name']; }
													else { $disp_img_name = ''; } ?>
													
													<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
													else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
													<!--span><i class="fa fa-file"></i></span> View-->
												</a>
										</div>
									</div>
							<?php	}
								echo '</div>';
							}	?>
							
							<input type="hidden" name="customFileCount" id="customFileCount"  value="<?php if($mode=='Update'){echo count($files_data); }else{ echo 0;} ?>">
							<div class="row">											
								<div id="last_team_file_id"></div>
								<div class="col-md-2 custom-file" id="addFileBtnOuter">
									<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
									<input type="file" class="form-control fileUploadKr" name="r_files[]" id="file-upload" />
								</div>
							</div>
							<div class="r_files_err_new"></div>
						</div>
						</div>

						<div class="row">	
								<div class="col-md-6">
									<label class="form-control-placeholder floatinglabel">Comment: </label>
									<label class="switch" style="margin-top:25px;">
										<input type="hidden" name="comment" value="0">
										<input type="checkbox" value="1" id="comment" name="comment" <?php if($mode == 'Update') { if($form_data[0]['comment']=='1'){echo 'checked';} } ?>> 
										<span class="slider round"></span>
									</label>
							</div>
						</div>
						
						 <div class="row pt-2">
    						<div class="col text-center">
								<button type="submit" class="btn btn-primary add_button"><?php if($mode=='Update') {echo 'Update';}else{echo 'Submit';} ?></button> 
							</div>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<script>
	function append_video_row(){
		var count = $("#video_warapper_count").val();
		var id = parseInt(count)+1;

		if (id>6) {
			swal('Warning','Only 6 videos are allowed','warning')
			return false;
		}

		var append_sring = '';
		append_sring =  '<div class="row appended_div_'+id+'">'
		append_sring +=  '<div class="col-md-4">'
		append_sring +=   '<div class="form-group"><label class="form-control-placeholder floatinglabel">Caption</label><em style="color: red;">*</em>';
		append_sring +=    '<input type="text" class="form-control caption" name="caption[]" id="caption'+id+'" value="">';
		append_sring += 	'</div></div>'
		
		append_sring +=		'<div class="col-md-6"><div class="form-group"><label class="form-control-placeholder floatinglabel">Link</label><em style="color: red;">*</em>';
									
		append_sring +=		'<input type="text" class="form-control" name="video_url[]" id="video_url'+id+'" value="">';
		append_sring +=		'</div></div>';
		append_sring +=		'<div class="col-md-2"><div class="form-group">';
								
		append_sring +=	'<button type="button" class="btn btn-danger btn-sm mt-45" onclick="remove_video_row('+id+')">-</button>';
		append_sring +=	 '</div></div>';
		append_sring +=	 '</div>';
		
		var wrapper =  $(".video_wrapper");
		$(wrapper).append(append_sring); 

		$("#video_warapper_count").val(parseInt(count)+1);


	}

	function remove_video_row(div_id){
		var count = $("#video_warapper_count").val();
		$("#video_warapper_count").val(parseInt(count)-1);
		$(".appended_div_"+div_id).remove();
	}
</script>

<script type="text/javascript">	
	CKEDITOR.replace('news_description');


	$("#banner_check").change(function () {

	    if ($('input.banner_check').is(':checked')) {
	    	$(".banner_row").removeClass('d-none');
	    }else{
	    	$("#other_details").val("");
	    	$(".banner_row").addClass('d-none');
	    }

	});

	$("#video_check").change(function () {

	    if ($('input.video_check').is(':checked')) {
	    	$(".video_wrapper").removeClass('d-none');
	    }else{
	    	$("#terms_condition").val("");
	    	$(".video_wrapper").addClass('d-none');
	    }

	});

	$("#files_check").change(function () {

	    if ($('input.files_check').is(':checked')) {
	    	$(".files_wrapper").removeClass('d-none');
	    }else{
	    	$("#terms_condition").val("");
	    	$(".files_wrapper").addClass('d-none');
	    }

	});
	
	
	function confirm_clear_all()
	{
		swal(
		{  
			title:"Confirm?",
			text: "This action will clear the complete form. Please confirm if you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); location.reload(); } });
	}
</script>



<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod("valid_files_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".pdf",".xls",".doc",".csv",".xlsx",".pptx",".ppt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');

	 	$.validator.addMethod("valid_email", function(value, element) 
	    { 
	      var email = value;
	      var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	      var result = pattern.test(email); 
	      if(result){return true;}else {return false;}
	    });

	 	$.validator.addMethod("youtube_url", function(value, element) {
     var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
     return (value.match(p)) ? RegExp.$1 : false;
    }, "Enter correct youtube video URL");

	 	$.validator.addMethod('NotAllZero', function(value) {
	      return value.match(/^(?!0*$).*$/);
	    }, 'Invalid Mobile Number');
		
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 100 KB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');
		
		
		//******* JQUERY VALIDATION *********
		var mode = '<?php echo $mode; ?>';
		$("#ResourceForm").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				news_title: { required: true, nowhitespace: true, valid_value: true },
				// resource_details: { required: true, nowhitespace: true, valid_value: true },
				// owner_name: { required: true, nowhitespace: true, valid_value: true },
				commercial_terms: { required: true, nowhitespace: true, valid_value: true },
				banner_img: {
	               required: function() {
	               return ($("input[name=banner_check]:checked").val()==1) && (mode =='Add'); 
	               },
	               valid_img_format: true,
	               filesize:100000
               
              	},
              	"caption[]": {
	               required: function() {
	               return $("input[name=video_check]:checked").val() == 1; 
	               }
               
              	},

              	"video_url[]": {
	               required: function() {
           			return $("input[name=video_check]:checked").val() == 1; 
	               },
	               url:function () {
	               	return $("input[name=video_check]:checked").val() == 1; 
	               },
	               youtube_url:function () {
	               	return $("input[name=video_check]:checked").val() == 1; 
	               },
	               
               
              	},
              	"r_files[]": { 
              		custum_required: function(){
          			return $("input[name=files_check]:checked").val() == 1; 
              		}
              	},

              	news_description: 
				{
					required: function() 
					{
						CKEDITOR.instances.news_description.updateElement();
					},							
					minlength:1
				},

				// banner_img: { valid_img_format: true,filesize:100000},
			
				//"video_url[]":{  url: true},
				
			},
			messages:
			{
				title_of_the_content: { required: "This field is required", nowhitespace: "Please enter the title" },
				kr_description: { required: "This field is required", nowhitespace: "Please enter the description" },				
				banner_img:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				"r_files[]":{required: "This field is required"},
				author_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				accept_terms:{required: "This field is required"},
				"r_files[]":{required:"This field is required"},
				news_description: { required: "This field is required", nowhitespace: "Please enter the title" },

				
				"video_url[]":{url: "Please enter valid url"}
									
			},
			
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "accept_terms") 
				{
					error.insertAfter("#accept_terms_err");
				}else if(element.attr("name") == "commercial_terms"){
					error.insertAfter("#commercial_terms_err");
				}
				else if (element.attr("name") == "r_files[]"){
					$("#file-upload-error").remove();
					error.insertAfter(".r_files_err_new");
					
				}
				else
				{
					element.closest('.form-group').append(error);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BlogImagePreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#banner_img_outer").removeClass('d-none');
					//$("#banner_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#banner_img_outer .previous_img_outer img").attr("src", e.target.result);
					//$("#banner_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#banner_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#banner_img").change(function() { BlogImagePreview(this); });	



	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function FilesPreview(input, public_private_flag) 
	{
		
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			var upload_file_size = input.files[0].size; //1000000 Bytes = 1 Mb 
			if(upload_file_size < 5000000)
			{
				var disp_img_name = "";
				var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
				if(extension == 'png' || extension == 'jpeg' || extension == 'jpg' ||   extension == 'pdf' || extension == 'xlsx' || extension == 'xls' || extension == 'doc' || extension == 'docx' || extension == 'ppt' || extension == 'pptx')
				{
				disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
					
				var id_name = '';
				var btn_id_name = '';
				var input_cls_name = '';
				var input_type_name = '';
				var fun_var_parameter = "";
				var file_upload_common = '';
				if(public_private_flag == 'public')
				{
					id_name = 'r_files_outer'+j;
					btn_id_name = 'addFileBtnOuter';
					input_cls_name = 'fileUploadKr';
					input_type_name = 'r_files[]';
					fun_var_parameter = "'public'";
					file_upload_common = 'file-upload';
				}
			
				
				var append_str = '';
				append_str += '	<div class="col-md-2 r_files_outer_common" id="'+id_name+'">';
				append_str += '		<div class="file-list">';
				append_str += '			<a href="javascript:void(0)" onclick="remove_content_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove">X</i></a>';
				//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
				append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
				append_str += '		</div>';
				append_str += '	</div>';				
					
				var btn_str = '';
				btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
				btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
				btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="FilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
				btn_str +=	'	</div> <div class="r_files_err"></div';
					
				if(public_private_flag == 'public')
				{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
					
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
				}
				}else{
					swal( 'Error!','Invalid File Extension','error');
				}
			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 5 MB','error');
			}
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadKr").change(function() { FilesPreview(this,'public'); });	



	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_content_file(div_id, file_id, public_private_flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				var customFileCount = $("#customFileCount").val();
				$("#customFileCount").val(parseInt(customFileCount)-1);
				
				if(file_id!= '' && file_id != '0')
				{
					
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("xAdmin/admin_update/delete_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				
				if(public_private_flag == 'public')
				{
				$("#r_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				
				
				$("#preloader").css("display", "none");
			}
		});
	}
	
	$("#file-upload").change(function(){
		if ($("#file-upload").valid()==true) {
			$("#file-upload-error").remove();
		}
	})
</script>