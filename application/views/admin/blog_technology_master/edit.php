<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Blog Technology</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Blog Technology</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Blog Technology
					</h3>
				  <a href="<?php echo base_url('xAdmin/blog_technology_master') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div> 
				<?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
						<?php echo $this->session->flashdata('error'); ?>
					</div>
				<?php } ?>
				<div class="card-body">
					<form method="post" id="frm" name="frm" role="form" >
						<div class="row">
							<div class="col-6">							   
							  <div class="form-group">
									<label for="exampleInputEmail1">Blog Technology Name</label>
									<input type="text" class="form-control" id="technology_name" placeholder="Enter Blog Technology Name" value="<?php echo $technology_data[0]['technology_name']; ?>" name="technology_name" maxlength="150">
									<span><?php echo form_error('technology_name'); ?></span>
								</div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$.validator.setDefaults({
			submitHandler: function () {
				//alert( "Form successful submitted!" );
				form.submit();
			}
		});
		$('#frm').validate({
			rules: {
				technology_name: {
					required: true,
					minlength:4
				},
			},
			messages: {
				technology_name: {
					required: "This field is required",
					minlength: "Enter Blog Technology Name must be at least {0} characters long"
				},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>


