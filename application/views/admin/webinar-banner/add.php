<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Webinar Banner</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Webinar Banner</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Webinar Banner
			  </h3>
				  <a href="<?php echo base_url('xAdmin/webinar_banner') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				   
				  
				<div class="card-body">
					 <form method="post" id="bannerfrm" name="bannerfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-6">
							 <div class="form-group">
								<label for="exampleInputEmail1">Type</label>
								<select name="upload_type" id="upload_type" class="form-control get-type">
									<option value="Image">Image</option>
									<option value="Video">Video</option>
								</select>
								<span><?php echo form_error('upload_type'); ?></span>
							  </div>
							<div id="selection_type">	
							 <div class="form-group">
								<label for="exampleInputEmail1">Banner Title</label>
								<input type="text" class="form-control" id="banner_name" placeholder="Enter Banner Title " name="banner_name" value="">								
								<span><?php echo form_error('banner_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Banner Sub Title</label>
								<input type="text" class="form-control" id="banner_sub" placeholder="Enter Banner Sub Title " name="banner_sub" value="">								
								<span><?php echo form_error('banner_sub'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Banner Description</label>
								<textarea class="form-control" id="banner_desc" placeholder="Enter Banner Description " name="banner_desc" rows="6" cols="12"></textarea>								
								<span><?php echo form_error('banner_desc'); ?></span>
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">Banner URL</label>
								<input type="text" class="form-control" id="banner_url" placeholder="Enter Banner URL " name="banner_url" value="">								
								<span><?php echo form_error('banner_url'); ?></span>
							  </div>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">Upload Video/Banner</label>
								<input type="file" class="form-control" id="banner_img" placeholder="Upload Banner" name="banner_img" >
								<span><?php echo form_error('banner_img'); ?></span>
								<span>For Banner upload .jpg, .png, .jpeg, .gif images only. <br />For Video upload .mp4, .webm only</span>
							  </div>
								<div class="form-group">
								<label for="exampleInputEmail1">xOrder</label>
								<input type="text" class="form-control" id="xOrder" placeholder="Enter Video/Banner xOrder " name="xOrder" value="">								
								<span><?php echo form_error('xOrder'); ?></span>
							  </div>
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  $( ".get-type" ).change(function() {	
		var values = $(this).val();
		if(values == "Image"){
			$('#selection_type').show();
		}else{
			$('#selection_type').hide();
		}
  });
  
  $.validator.addMethod("valid_img_format", function(value, element)
{
	if(value != "")
	{
		var uploadType = $("#upload_type").val();
		if(uploadType == "Image"){
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
		}else if(uploadType == "Video"){
			var validExts = new Array(".mp4", ".webm");
		}		
		var fileExt = value.toLowerCase();
		fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
		if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
	}else return true;
});

	$.validator.addMethod('filesize', function (value, element, param) {
	 return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than 5 MB');
  
  $('#bannerfrm').validate({
    rules: {
	  upload_type: {
		required: true  
	  },	
      banner_name: {
		 required: function() {
			return $("#upload_type").val() == 'Image';  
		   }, 
			minlength:5
      },
	  banner_sub: {
        required: function() {
			return $("#upload_type").val() == 'Image';  
		   },
		   minlength:3
      },
	  banner_desc: {
        required: function() {
			return $("#upload_type").val() == 'Image';  
		   }
      },
	  banner_url: {
        required: function() {
			return $("#upload_type").val() == 'Image';  
		   },
		url:true
      }, 
	  banner_img:{
		required: true,   
		valid_img_format: true 
		//filesize:5000000 
	  },
	  xOrder: {
		required: true,
		number: true
	  }
    },
    messages: {
	  upload_type: {
		required: "This field is required" 
	  },	
      banner_name: {
        required: "This field is required",
		minlength: "Enter Banner Name must be at least {0} characters long"
      },
	  banner_desc: {
        required: "This field is required"
      },
	  banner_url: {
        required: "This field is required",
		url: "Enter Banner url in proper format using http:// or https://"
      },
	  banner_img: {
        required: "This field is required",
		valid_img_format: "Please upload only mention extention video/images."
      },
	  xOrder: {
		required: "This field is required"
	  }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


