<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<style>
/* Prelaoder */
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}
#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
@-webkit-keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
</style>
<script>
// Preloader
$(window).on('load', function() {
    if ($('#preloader-loader').length) {
        $('#preloader-loader').delay(50).fadeOut('slow', function() {
            /* $(this).remove(); */
        });
    }
});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Triger Mail To Subscribe/Register Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Subscribe Mail</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Subscribe Mail
			  </h3>
				  <a href="<?php echo base_url('xAdmin/subscriber_email/add') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="emailfrm" name="emailfrm" role="form" >
						  <div class="row">
							<div class="col-12">
							 <div class="form-group">
								<label for="exampleInputEmail1">Select Category <em>*</em></label>
								<select name="category" id="category" class="form-control option-check">
									<option value="">-- Please Select --</option>
									<option value="5">All Users</option>
									<option value="4">Subscribed Users</option>
									<option value="3">All Registered Users</option>
									<option value="1">Registered Individual Users</option>
									<option value="2">Registered Organization Users</option>
								</select>
								<span><?php echo form_error('email_title'); ?></span>
							  </div>
							  
								<div class="form-group">
									<label for="exampleInputEmail1">Select Subcategory <em></em></label>
									<select class="form-control subcategory" id="subcategory" name="subcategory" >
										<option value="">-- Please Select --</option>
								    </select>
								<span><?php //echo form_error('subcategory'); ?></span>
							  </div>
							  
							  <div class="form-group">
								<label for="exampleInputEmail1">Email Subject <em>*</em></label>
								<input type="text" class="form-control" id="email_title" placeholder="Enter Email Title" name="email_title" maxlength="200">
								<span><?php echo form_error('email_title'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Email Body <em>*</em></label>
								<textarea id="email_description" name="email_description" class="form-control ckeditor" placeholder="Enter Email Description"></textarea>
								<script>
									CKEDITOR.replace('email_description');
								</script>								
								<span><?php echo form_error('email_description'); ?></span>
							  </div>
							  <!--<div class="form-group">
								<label for="exampleInputEmail1">Slug</label>
								<input type="text" class="form-control" id="slug" placeholder="Enter Email Slug" name="slug" maxlength="100">
								<span><?php echo form_error('slug'); ?></span>
							  </div>-->
							  <div class="form-group">
								<label for="exampleInputEmail1">From Email ID <em>*</em></label>
								<input type="email" class="form-control" id="from_email" placeholder="Enter Email ID" name="from_email" maxlength="300">
								<span><?php echo form_error('from_email'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Another Email ID</label>
								<input type="email" class="form-control" id="cc_email_id" placeholder="Enter Email ID" name="cc_email_id" maxlength="300">
								<span><?php //echo form_error('cc_email_id'); ?></span>
							  </div>	
								<div class="card-footer1">
									<button type="submit" name="btn_send" id="btn_send" class="btn btn-primary">Send Email</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
function refreshPage() {
    location.reload(true);
}
$(document).ready(function () {
	// $("#preloader-loader").css("display", "block");
  $.validator.setDefaults({
    submitHandler: function () {     
	  //form.submit();
		var catid 				= $('#category').val();
		var subcategory 		= $('#subcategory').val();
		var email_title 		= $('#email_title').val();
		var email_description 	= $('#email_description').val();
		var from_email 			= $('#from_email').val();
		var cc_email_id 		= $('#cc_email_id').val();
		
		$.ajax({
			 type: 'POST',
			 data:$('#emailfrm').serialize(),
			 url: "<?php echo base_url();?>/xAdmin/subscriber_email/notify_users",
			 beforeSend: function(){
			   //$('.ajax-loader').css("visibility", "visible");
			   $("#preloader-loader").css("display", "block");
			 },
			 success: function(res){ 
				var returndata = JSON.parse(res);			
				var status = returndata.success;
				//console.log(status);
				swal({
						title: 'Success!',
						text: status,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
			   //location.reload();	
				refreshPage();
			 },
			 complete: function(){
				 $("#preloader-loader").css("display", "none");
				 swal({
						title: 'Success!',
						text: 'Email successfully send.',
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				 //$('.ajax-loader').css("visibility", "hidden");
			  },

		});
	  
    }
  });
  
  
	$('.option-check').change(function() {
	    var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>', csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
		var selectedOption = $(this).val();
		var datastring='cat_id='+ $(this).val() + '&' + csrfName + '='+csrfHash;
		 $.ajax({
             type: 'POST',
             data:datastring,
             url: "<?php echo base_url();?>/xAdmin/subscriber_email/get_sub_category",
             beforeSend: function(){
               $('.ajax-loader').css("visibility", "visible");
             },
             success: function(res){ 
               var json = $.parseJSON(res);
               $('#subcategory').html(json.str);
               //csrfName=json.name;
               //csrfHash=json.value;
             },
             complete: function(){
                      $('.ajax-loader').css("visibility", "hidden");
              },

           });
	  
	  
	});  
  
  jQuery.validator.addMethod("ckrequired", function (value, element) {  
		var idname = $(element).attr('id');  
		var editor = CKEDITOR.instances[idname];  
		var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
		if (ckValue.length === 0) {  
			//if empty or trimmed value then remove extra spacing to current control  
			$(element).val(ckValue);  
		} else {  
			//If not empty then leave the value as it is  
			$(element).val(editor.getData());  
		}  
		return $(element).val().length > 0;  
	}, "This field is required"); 

	function GetTextFromHtml(html) {
		var dv = document.createElement("DIV");
		dv.innerHTML = html;
		return dv.textContent || dv.innerText || "";
	}
  
 
  $('#emailfrm').validate({
	 ignore: [],
     debug: false, 
    rules: {
      email_title: {
        required: true,
		minlength:5
      },
	  category:{
		required: true,  
	  },
	  /*subcategory:{
		 required: function() {
			return $("#category").val() == 1 ||  $("#category").val() == 2;  
		   }  
	  },*/
	  email_description: {
        ckrequired: true
      },
	  from_email: {
        required: true
      }
    },
    messages: {
	  category:{
		required: "This field is required",
	  },
	  /*subcategory:{
		 required: "This field is required", 
	  },*/	
      email_title: {
        required: "This field is required",
		minlength: "Enter email title must be at least {0} characters long"
      },
	  email_description: {
			required: function() 
			{
			 CKEDITOR.instances.email_description.updateElement();
			},
			minlength:10
      },
	  from_email: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


