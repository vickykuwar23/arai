<!-- Content Wrapper. Contains page content -->
<style>


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 60vh;
    overflow-y: auto;
}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Contact Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Contact Listing</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
		  <div class="card-header">
			  <h3 class="card-title">
				  Contact Listing
			  </h3>
		  </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Fullname</th>
					 <th>Email ID</th>
					 <th>Mobile No.</th>
					 <th>Subject Name</th>
					 <th>Content</th>
					 <th>Added On</th>
                     <!--<th>Action</th>-->
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $contact_detail) 
                     { 
					 $cid = $contact_detail['country_code'];
					 $country_name = $this->master_model->getRecords("country",array('id' => $cid));
					 $cotentDet =  strlen($contact_detail['content_received']) >= 57 ? substr($contact_detail['content_received'], 0, 60) . ' ...<a href="javascript:void(0);" class="contact-popup" data-id="'.$contact_detail['id'].'">View</a>' : $contact_detail['content_received'];					
					 ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:5%"><?php echo $contact_detail['fullname'] ; ?></td> 
					 <td style="width:10%"><?php echo $contact_detail['email_id'] ; ?></td>
					 <td style="width:10%"><?php echo "+".$country_name[0]['phonecode']."-".$contact_detail['mobile_no']; ?></td>
					 <td style="width:20%"><?php echo $contact_detail['subject_details'] ; ?></td>
					 <td style="width:55%"><?php echo $cotentDet; ?></td>	
					 <td style="width:5%"><?php echo date('d-m-Y H:i:s', strtotime($contact_detail['createdAt'])) ; ?></td>	 	
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Details</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$("body").on("click", "#example1 tbody tr .contact-popup", function (e) { 			
			//var priorityVal = $(this).val();	
			//$('#exampleModal').html(response).modal('show');
			$('#exampleModal').modal('show');			
			var eid	=	$(this).attr('data-id');
			var base_url = '<?php echo base_url('xAdmin/contactus/viewDetails'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:eid},				
				success: function (response) {					
					$("#contents").html(response);				
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
		});	
	
    });
</script>