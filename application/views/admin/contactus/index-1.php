<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Contact Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Contact Listing</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
		  <div class="card-header">
			  <h3 class="card-title">
				  Contact Listing
			  </h3>
		  </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Fullname</th>
					 <th>Email ID</th>
					 <th>Mobile No.</th>
					 <th>Subject Name</th>
					 <th>Content</th>
                     <!--<th>Action</th>-->
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $contact_detail) 
                     { 
					 $cid = $contact_detail['country_code'];
					 $country_name = $this->master_model->getRecords("country",array('id' => $cid));
					 
					 ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:10%"><?php echo $contact_detail['fullname'] ; ?></td> 
					 <td style="width:10%"><?php echo $contact_detail['email_id'] ; ?></td>
					 <td style="width:10%"><?php echo $country_name[0]['phonecode']."-".$contact_detail['mobile_no'] ; ?></td>
					 <td style="width:20%"><?php echo $contact_detail['subject_details'] ; ?></td>
					 <td style="width:45%"><?php echo $contact_detail['content_received'] ; ?></td>	
                     <!--<td style="width:20%">						
                        <a href="<?php echo base_url();?>xAdmin/contactus/edit/<?php echo $banners_detail['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
                        
						<?php      
						if($banners_detail['status']=="Active"){
							$status = 'Block';
						?>
                        <a href="<?php echo base_url(); ?>xAdmin/contactus/changeStatus/<?php echo $banners_detail['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                           else if($banners_detail['status']=="Block"){
							$status = 'Active';
						   ?>
                        <a href="<?php echo base_url(); ?>xAdmin/contactus/changeStatus/<?php echo $banners_detail['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                           ?>
                     </td>-->
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		
	
    });
</script>