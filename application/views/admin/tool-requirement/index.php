<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Tool Requirement</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Tool Requirement</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
		  <div class="card-header">
			  <h3 class="card-title">
				  Tool Requirement Listing
			  </h3>
		  </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>ID</th>
                     <th>Fullname</th>
					 <th>Email ID</th>
					 <th>Your Requirement</th>
					 <th>Purpose</th>
					 <th>Tools/Facilities Required</th>
					 <th>Remarks</th>
					 <th nowrap>Added On</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $tool_query) 
                     { 
					 $encrptopenssl =  New Opensslencryptdecrypt();
					$fullname = ucfirst($encrptopenssl->decrypt($tool_query['first_name']))." ".ucfirst($encrptopenssl->decrypt($tool_query['middle_name']))." ".ucfirst($encrptopenssl->decrypt($tool_query['last_name']));
					
					$toolRe =  strlen($tool_query['tool_requirements']) >= 50 ? substr($tool_query['tool_requirements'], 0, 47) . ' ...' : $tool_query['tool_requirements'];					
					$tool_pur =  strlen($tool_query['tool_purpose']) >= 60 ? substr($tool_query['tool_purpose'], 0, 57) . ' ...' : $tool_query['tool_purpose'];					
					$tool_re =  strlen($tool_query['tools_required']) >= 60 ? substr($tool_query['tools_required'], 0, 57) . ' ...' : $tool_query['tools_required'];					
					$tool_mark =  strlen($tool_query['tools_remark']) >= 70 ? substr($tool_query['tools_remark'], 0, 67) . ' ...' : $tool_query['tools_remark'];					
					?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
					 <td style="width:5%"><?php echo $tool_query['user_id']; ?></td>
					 <td style="width:5%"><?php echo $fullname; ?></td>
                     <td style="width:10%"><?php echo $encrptopenssl->decrypt($tool_query['email']); ?></td> 
					 <td style="width:20%"><?php echo $toolRe; ?></td>
					 <td style="width:20%"><?php echo $tool_pur; ?></td>
					 <td style="width:20%"><?php echo $tool_re; ?></td>	
					 <td style="width:25%"><?php echo $tool_mark; ?></td>
					 <td style="width:10%" nowrap><?php echo $tool_query['createdAt']; ?></td>
					 <td><a href="javascript:void(0);" class="btn btn-primary tool-popup" data-id="<?php  echo $tool_query['id'] ?>">View</a></td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Tools Requirement</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$("body").on("click", "#example1 tbody tr .tool-popup", function (e) { 			
			//var priorityVal = $(this).val();			
			var tid	=	$(this).attr('data-id');
			$('#exampleModal').modal('show');			
			
			var base_url = '<?php echo base_url('xAdmin/tool_requirement/viewDetails'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:tid},				
				success: function (response) {					
					$("#contents").html(response);				
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
		});	
    });
</script>