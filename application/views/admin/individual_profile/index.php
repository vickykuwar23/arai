<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Individual / Experts Profile</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Individual / Experts Profile</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
      <?php if( $this->session->flashdata('success')){ ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-check"></i> Success!</h5>
       <?php echo $this->session->flashdata('success'); ?>
      </div>
      <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Individual / Experts Profile List
              </h3>
                 <a href="<?php echo base_url('xAdmin/individual_profile/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Company Name</th>
                     <th>Designation</th>
                     <th>Action</th>
                  </tr>
               </thead>
                  <tbody>
                    <?php  
                       $i=1;
                       foreach($records as $data) 
                       { ?>
                    <tr>
                       <td style="width:5%"><?php echo $i; ?></td>
                       <td style="width:65%"><?php echo $data['company_name'] ; ?></td>
                       <td style="width:65%"><?php echo $data['designation'] ; ?></td>                     
                       <td style="width:18%">
              
                          <a href="#" ><button class="btn btn-success btn-sm">Edit</button></a>
                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
       $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      
      
   
    });
</script>