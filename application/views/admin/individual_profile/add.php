<!-- Content Wrapper. Contains page content -->
<head>
  <style>
    .error{
      color: red;
    }
  </style>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Individual / Experts Profile</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Individual / Experts Profile</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Individual / Experts Profile
			  </h3>
				  <a href="<?php echo base_url('xAdmin/individual_profile') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="frm" name="frm" role="form" enctype="multipart/form-data">
						    <div class="row">
                  <input type="hidden" name="type" value="Individual">
                  <div class="col-md-6">							   
                    <div class="form-group">
								      <label for="exampleInputEmail1">Employement Status</label>
  								    <select class="form-control" name="employement_status">
                        <option value="">Select Status</option>
                        <option value="Self Employed">Self Employed</option>
                        <option value="Consultant">Consultant</option>
                        <option value="Freelancer">Freelancer</option>
                        <option value="Company">Company</option>
                        <option value="Other">Other</option>
                      </select>
								      <span class="error"><?php echo form_error('employement_status'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Company Name</label>
                      <input type="text" class="form-control" id="company_name" placeholder="Enter Company Name" name="company_name" maxlength="200">
                      <span class="error"><?php echo form_error('company_name'); ?></span>
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Designation</label>
                      <input type="text" class="form-control" id="designation" placeholder="Enter Designation" name="designation" maxlength="200">
                      <span class="error"><?php echo form_error('designation'); ?></span>
                    </div>  
                  </div>
                  <div class="col-md-6">                 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Since Year</label>
                      <input type="date" class="form-control" id="designation_since_year" placeholder="Select University Location" name="designation_since_year" maxlength="200">
                      <span class="error"><?php echo form_error('designation_since_year'); ?></span>
                    </div>  
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description</label>
                      <textarea name="designation_description" class="form-control" id="designation_description"></textarea>
                      <span class="error"><?php echo form_error('designation_description'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Domain / Area of Expertise</label>
                      <select name="domain_and_area_of_expertise[]" class="domainName form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="Radiator">Radiator</option>
                        <option value="Refrigerator">Refrigerator</option>
                        <option value="Blowers">Blowers</option>
                      </select>
                      <span class="error"><?php echo form_error('domain_and_area_of_expertise'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Areas of Interest</label>
                      <select name="areas_of_interest[]" class="areaOfInterestName form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="Programming">Programming</option>
                        <option value="Designing">Designing</option>
                        <option value="Testing">Testing</option>
                        <option value="Drawing">Drawing</option>
                      </select>
                      <span class="error"><?php echo form_error('areas_of_interest'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Corporate Linkage Code</label>
                      <input type="text" class="form-control" id="corporate_linkage_code" placeholder="Enter Corporate Linkage Code" name="corporate_linkage_code" maxlength="200">
                      <span class="error"><?php echo form_error('corporate_linkage_code'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Skill Sets</label>
                      <select name="skill_sets[]" class="skillSets form-control" autocomplete="nope" multiple="multiple" required>
                        <option value="JAVA">JAVA</option>
                        <option value="PHP">PHP</option>
                        <option value="PYTHON">PYTHON</option>
                        <option value="CPP">CPP</option>
                      </select>
                      <span class="error"><?php echo form_error('skill_sets'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <label>Technical Experience</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Year</label>
                      <input type="text" class="form-control" id="technical_experience_in_year" placeholder="Enter Technical Experience in Year" name="technical_experience_in_year" maxlength="200">
                      <span class="error"><?php echo form_error('technical_experience_in_year'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description of Work</label>
                      <textarea name="technical_description_of_work" class="form-control" id="technical_description_of_work"></textarea>
                      <span class="error"><?php echo form_error('technical_description_of_work'); ?></span>
                    </div> 
                  </div>
                </div>
                <hr>
                <label>Billing Address</label>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Pincode</label>
                      <input type="text" class="form-control" id="pincode" placeholder="Enter Pincode" name="pincode" maxlength="200">
                      <span class="error"><?php echo form_error('pincode'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Flat / House No. / Building / Apt / Company</label>
                      <input type="text" class="form-control" id="flat_house_building_apt_company" placeholder="Enter Flat / House No. / Building / Apt / Company" name="flat_house_building_apt_company" maxlength="200">
                      <span class="error"><?php echo form_error('flat_house_building_apt_company'); ?></span>
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Area / Colony / Street / Village</label>
                      <input type="text" class="form-control" id="area_colony_street_village" placeholder="Enter Area / Colony / Street / Village" name="area_colony_street_village" maxlength="200">
                      <span class="error"><?php echo form_error('area_colony_street_village'); ?></span>
                    </div> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Town / City and State</label>
                      <input type="text" class="form-control" id="town_city_and_state" placeholder="Enter Town / City & State" name="town_city_and_state" maxlength="200">
                      <span class="error"><?php echo form_error('town_city_and_state'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Country</label>
                      <input type="text" class="form-control" id="country" placeholder="Enter Country" name="country" maxlength="200">
                      <span class="error"><?php echo form_error('country'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Specify Fields or Area that you would like to be an expert, guide us on</label>
                      <input type="text" class="form-control" id="specify_fields_area_that_you_would_like" placeholder="Specify Fields or Area that you would like to be an expert, guide us on" name="specify_fields_area_that_you_would_like" maxlength="200">
                      <span class="error"><?php echo form_error('specify_fields_area_that_you_would_like'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Bio Data</label>
                      <textarea name="bio_data" class="form-control" id="bio_data"></textarea>
                      <span class="error"><?php echo form_error('bio_data'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Profile Picture</label>
                      <input type="file" class="form-control" id="profile_picture" placeholder="Upload Profile Picture" name="profile_picture" >
                      <span><?php echo form_error('profile_picture'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <label for="exampleInputEmail1">Years of Experience</label>
                      <select name="years_of_experience" class="form-control" autocomplete="nope" required>
                        <option value="">Select Experience</option>
                        <option value="0-10">0-10</option>
                        <option value="10-15">10-15</option>
                        <option value="15-20">15-20</option>
                        <option value="20-25">20-25</option>
                        <option value="25+">25+</option>
                      </select>
                      <span><?php echo form_error('years_of_experience'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <label for="exampleInputEmail1">No. of Paper Publication</label>
                      <select name="no_of_paper_publication" class="form-control paperPublication" autocomplete="nope" required>
                        <option value="">Select No. of Paper Publication</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select>
                      <span><?php echo form_error('no_of_paper_publication'); ?></span>
                    </div>
                  </div>
                </div>
                <label class="paperDetails">Paper Details</label>
                <div class="row paperDetails">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Year</label>
                      <input type="date" class="form-control" id="paper_year" placeholder="Enter Year" name="paper_year" maxlength="200">
                      <span class="error"><?php echo form_error('paper_year'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Conf. Name</label>
                      <input type="text" class="form-control" id="paper_conf_name" placeholder="Enter Conf. Name" name="paper_conf_name" maxlength="200">
                      <span class="error"><?php echo form_error('paper_conf_name'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Title</label>
                      <input type="text" class="form-control" id="paper_title" placeholder="Enter Title" name="paper_title" maxlength="200">
                      <span class="error"><?php echo form_error('paper_title'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6"> 
                    <div class="form-group">
                      <label for="exampleInputEmail1">No. of Patents</label>
                      <select name="no_of_patents" class="form-control Patents" autocomplete="nope" required>
                        <option value="">Select No. of Patents</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select>
                      <span><?php echo form_error('no_of_patents'); ?></span>
                    </div>
                  </div>
                </div>
                <label class="patentDetails">Patent Details</label>
                <div class="row patentDetails">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Year</label>
                      <input type="date" class="form-control" id="patent_year" placeholder="Enter Year" name="patent_year" maxlength="200">
                      <span class="error"><?php echo form_error('patent_year'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Patent Number</label>
                      <input type="text" class="form-control" id="patent_number" placeholder="Enter Conf. Name" name="patent_number" maxlength="200">
                      <span class="error"><?php echo form_error('patent_number'); ?></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Title</label>
                      <input type="text" class="form-control" id="patent_title" placeholder="Enter Title" name="patent_title" maxlength="200">
                      <span class="error"><?php echo form_error('patent_title'); ?></span>
                    </div>
                  </div>
                </div>
                <hr>
                <label>Links to referal Links, Portals that you are operating from. Where else can we find you?</label>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Portal Name</label>
                      <input type="text" class="form-control" id="portal_name" placeholder="Enter Portal Name" name="portal_name" maxlength="200">
                      <span class="error"><?php echo form_error('portal_name'); ?></span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Link</label>
                      <input type="text" class="form-control" id="portal_link" placeholder="Enter Link" name="portal_link" maxlength="200">
                      <span class="error"><?php echo form_error('portal_link'); ?></span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description</label>
                      <textarea name="portal_description" class="form-control" id="portal_description"></textarea>
                      <span class="error"><?php echo form_error('portal_description'); ?></span>
                    </div>
                  </div>
                </div>
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							
								<!-- /.card-body -->
						  
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).on('change','.paperPublication', function(){
    var paperPublicationValue = $('.paperPublication').val();
    if(paperPublicationValue == 0)
    {
      $('.paperDetails').hide();
    }
    if(paperPublicationValue != 0)
    {
      $('.paperDetails').show();
    }
  });
  $(document).on('change','.Patents', function(){
    var patentValue = $('.Patents').val();
    if(patentValue == 0)
    {
      $('.patentDetails').hide();
    }
    if(patentValue != 0)
    {
      $('.patentDetails').show();
    }
  });
  $('.domainName').select2({
    placeholder : "Please Select Domain / Area of Expertise",
    closeOnSelect : false
  });
  $('.areaOfInterestName').select2({
    placeholder : "Please Select Areas of Interest",
    closeOnSelect : false
  });
  $('.skillSets').select2({
    placeholder : "Please Select Skill Sets",
    closeOnSelect : false
  });
</script>


