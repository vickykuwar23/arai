<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">CMS Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">CMS Listing</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit CMS Listing
			  </h3>
				  <a href="<?php echo base_url('xAdmin/cms') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="challengecatfrm" name="challengecatfrm" role="form" >
						  <div class="row">
							<div class="col-12">							   
							  <div class="form-group">
								<label for="exampleInputEmail1">Page Title</label>
								<input type="text" class="form-control" id="page_title" placeholder="Enter Page Title" value="<?php echo $cms_data[0]['page_title']; ?>" name="page_title" maxlength="150">
								<span><?php echo form_error('page_title'); ?></span>
							  </div>
							 <div class="form-group">
								<textarea id="page_description" name="page_description" class="form-control ckeditor" placeholder="Enter Page Description"><?php echo $cms_data[0]['page_description']; ?></textarea>
								<script>
								CKEDITOR.replace( 'page_description', {
									filebrowserBrowseUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/ckfinder.html?Type=Images',
									filebrowserUploadUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserWindowWidth : '1000',
									filebrowserWindowHeight : '700'
								});
								</script>								
								<span><?php echo form_error('page_description'); ?></span>
							  </div>	

               <div class="form-group">
                <label for="exampleInputEmail1">Video URL</label>
                <input type="text" class="form-control" value="<?php echo $cms_data[0]['video_url']?>" id="video_url" placeholder="Enter Page Title" name="video_url">
                <span><?php echo form_error('video_url'); ?></span>
                </div>

								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#cmsfrm').validate({
    rules: {
      page_title: {
        required: true,
		minlength:5
      },
	  page_description: {
        required: true
      },
     video_url: {url:true,
           // youtube_url:true,
      },
    },
    messages: {
      page_title: {
        required: "This field is required",
		minlength: "Enter Page Title must be at least {0} characters long"
      },
	  page_description: {
        required: "This field is required"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


