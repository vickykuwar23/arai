<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon -->
  <link rel="icon" href="<?php echo base_url('assets/front/') ?>images/favicon.png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- <style>
    .login-page{
      background: rgba(4, 40, 68, 0.85);
    }
  </style> -->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">
    <img src='<?php echo base_url("assets/admin/img/admin_logo.png") ?>' alt="Logo" class="img-fluid" style="width: 360px">
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="col-12">
        <?php if ($this->session->flashdata('success_message') != "") { ?>
       <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> 
            <?php echo $this->session->flashdata('success_message'); ?>
         </div>
         <?php } ?>

         <?php if ($this->session->flashdata('error_message') != "") { ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> 
            <?php echo $this->session->flashdata('error_message'); ?>
         </div>
         <?php } ?>
        
         <?php 
            if ($msg != "" || validation_errors()!="" )
                  { ?> 
         <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fa fa-ban"></i> Alert!</h5>
            <?php echo validation_errors();  echo $msg;?>
         </div>
         <?php } ?> 
</div>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Admin Login</p>

      <form action="" method="post">
        <div class="input-group mb-3">
         <input type="text" placeholder="Username" class="form-control" autofocus="" name="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" placeholder="Password" class="form-control" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
         <div class="form-group">
            <span id="captImg"><?php echo $captchaImg; ?></span>
            <a style="margin-left:5px; color: #fff;" class="btn btn-primary refreshCaptcha"><i class="fas fa-sync-alt"></i></a>
            <input type="text" name="captcha" value="" class="form-control" placeholder="Submit the word you see above" style="margin-top:8px;" />
            </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" name="submit" value="Sign In" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
		 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.min.js"></script>
<script>
       $(document).ready(function(){
           $('.refreshCaptcha').on('click', function(){
               $.get('<?php echo base_url().'xAdmin/admin/refresh'; ?>', function(data){
                   $('#captImg').html(data);
               });
           });
       });
   </script>

</body>
</html>
