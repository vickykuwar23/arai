<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Public / Private Status</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Public / Private Status</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
      <?php if( $this->session->flashdata('success')){ ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-check"></i> Success!</h5>
       <?php echo $this->session->flashdata('success'); ?>
      </div>
      <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Status List
              </h3>
                 <a href="<?php echo base_url('xAdmin/public_status_master/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Public / Private Status Name</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $publicStatusData) 
                     { ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:65%"><?php echo $publicStatusData['public_status_name'] ; ?></td>                     
                     <td style="width:18%">
                  
                        <a href="<?php echo base_url();?>xAdmin/public_status_master/edit/<?php echo $publicStatusData['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
                        <!--<a href="<?php echo base_url();?>xAdmin/usercategories/delete/<?php echo $technology['id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>-->
                  
                  <?php      
                  if($publicStatusData['status']=="Active"){
                     $status = 'Block';
                  ?>
                        <a href="<?php echo base_url(); ?>xAdmin/public_status_master/changeStatus/<?php echo $publicStatusData['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                           else if($publicStatusData['status']=="Block"){
                     $status = 'Active';
                     ?>
                        <a href="<?php echo base_url(); ?>xAdmin/public_status_master/changeStatus/<?php echo $publicStatusData['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                           ?>
                  
                  
                </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
       $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      
      
   
    });
</script>