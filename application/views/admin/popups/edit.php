<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Pop Up</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Pop Up</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <section class="content">
      <div class="container-fluid">
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Pop Up
			  </h3>
				  <a href="<?php echo base_url('xAdmin/popups') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
             <?php if(validation_errors()!="" ){ ?>
              <div class="alert alert-danger alert-dismissible cpl-md-6">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Error!</h5>
            <?php echo  validation_errors(); ; ?>
          </div>
        </div>
        
      <?php }
      ?>
				<div class="card-body">
					 <form method="post" id="emailfrm" name="emailfrm" role="form" >
						  <div class="row">
							<div class="col-12">							   
							  <div class="form-group">
								<label for="exampleInputEmail1">Title</label>
								<input type="text" class="form-control" id="title" placeholder="Enter Email Title" value="<?php echo $popup_data[0]['title']; ?>" name="title" maxlength="150">
								<span><?php echo form_error('title'); ?></span>
							  </div>
							 <div class="form-group">
								<textarea id="content" name="content" class="form-control ckeditor" placeholder="Enter Email Description"><?php echo $popup_data[0]['content']; ?></textarea>
								<script>
									CKEDITOR.replace('content');
								</script>								
								<span><?php echo form_error('content'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Slug</label>
								<input type="text" class="form-control" id="slug" placeholder="Enter Email Slug" name="slug" maxlength="100" value="<?php echo $popup_data[0]['slug']; ?>" readonly >
								<p>Please do not remove or update slug</p>
								<span><?php echo form_error('slug'); ?></span>
							  </div>
							 

							
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
         </div>
      </div>
   </section>
</div>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
	  form.submit();
    }
  });
  $('#emailfrm').validate({
    rules: {
      title: {
        required: true,
		minlength:5
      },
	  content: {
        required: true
      },
	  from_emai: {
        required: true
      },
	  slug: {
        required: true
      }
    },
    messages: {
      title: {
        required: "This field is required",
		minlength: "Enter Page Title must be at least {0} characters long"
      },
	  content: {
        required: "This field is required"
      },
	  from_emai: {
        required: "This field is required"
      },
	  slug: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


