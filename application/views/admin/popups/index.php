<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Pop Up</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Pop up</li>
               </ol>
            </div>
         </div>
      </div>
   </div>
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>

     <?php
     
      if(validation_errors()!="" ){ ?>
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-check"></i> Error!</h5>
        <?php echo  validation_errors(); ; ?>
        </div>
        
      <?php }
      ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                 Pop Up
              </h3>
                 <!-- <a href="<?php echo base_url('xAdmin/popups/add') ?>" class="btn btn-primary btn-sm float-right">Add </a> -->
              </div>
         <div class="card-body table-responsive">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                <tr>
                <th>No.</th>
                <th>Email Title</th>
                <th>From Email ID</th>
                <th>Slug</th>                     
                <th>Action</th>
                </tr>
               </thead>
               <tbody>
                <?php  
                  $i=1;
                 foreach($records as $popup) 
                  { ?>
                  <tr>
                  <td ><?php echo $i; ?></td>
                  <td ><?php echo $popup['title'] ; ?></td> 
                  <td ><?php echo  $popup['content'] ; ?></td>
                  <td ><?php echo $popup['slug'] ; ?></td>
                  <td >
                  <a href="<?php echo base_url();?>xAdmin/popups/edit/<?php echo $popup['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                     
                  <?php      
                  if($popup['status']=="Active"){
                  $status = 'Block';
                  ?>
                  <a href="<?php echo base_url(); ?>xAdmin/popups/changeStatus/<?php echo $popup['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                  else if($popup['status']=="Block"){
                  $status = 'Active';
                  ?>
                  <a href="<?php echo base_url(); ?>xAdmin/popups/changeStatus/<?php echo $popup['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                  ?>
                  </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
         </div>
         </div>
      </div>
   </section>
</div>

<script>
$(document).ready( function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
});
</script>