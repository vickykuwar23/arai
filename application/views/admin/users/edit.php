<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Admin Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Admin Master</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Admin Master
			  </h3>
				  <a href="<?php echo base_url('xAdmin/usermaster') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="usermasterfrm" name="usermasterfrm" role="form" >
						  <div class="row">
							<div class="col-6">							   
							  <div class="form-group">
								<label for="exampleInputEmail1">Fullname</label>
								<input type="text" class="form-control" id="admin_name" placeholder="Enter Admin Name" name="admin_name" maxlength="300" value="<?php echo $user_data[0]['admin_name'] ?>">
								<span><?php echo form_error('admin_name'); ?></span>
							  </div>		
							  <div class="form-group">
								<label for="exampleInputEmail1">Username</label>
								<input type="text" class="form-control" id="admin_username" placeholder="Enter Username" name="admin_username" maxlength="150" value="<?php echo $user_data[0]['admin_username'] ?>">
								<span><?php echo form_error('admin_username'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Password</label>
								<input type="password" class="form-control" id="admin_password" placeholder="****************" name="admin_password" maxlength="150" value="">
								<span><?php echo form_error('admin_password'); ?></span>
							  </div>
                              <div class="form-group">
								<label for="exampleInputEmail1">Email ID</label>
								<input type="email" class="form-control" id="admin_email" placeholder="Enter Email ID" name="admin_email" maxlength="255" value="<?php echo $user_data[0]['admin_email'] ?>">
								<span><?php echo form_error('admin_email'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Admin Type</label>
								<select name="role_id" id="role_id" class="form-control">
									<option value="">-- Select --</option>
									<?php foreach($admin_type as $typelist): ?>
									<option value="<?php echo $typelist['id']; ?>" <?php if($user_data[0]['admin_roles'] == $typelist['id']): ?> selected="selected" <?php endif; ?>><?php echo $typelist['admin_type_name']; ?></option>
									<?php endforeach; ?>
								</select>
								<span><?php echo form_error('admin_name'); ?></span>
							  </div>							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#usermasterfrm').validate({
    rules: {
      admin_username: {
        required: true,
		minlength:3
      },
	  
	  admin_name: {
        required: true,
		minlength:3
      },
	  admin_email: {
        required: true
      },
	  role_id: {
        required: true
      }
    },
    messages: {
      admin_username: {
        required: "This field is required",
		minlength: "Enter Username must be at least {0} characters long"
      },
	  
	  admin_name: {
        required: "This field is required",
		minlength: "Enter Fullname must be at least {0} characters long"
      },
	  admin_email: {
        required: "This field is required"
      },
	  role_id: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


