<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Users</li>
               </ol>
            </div>

            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if ($this->session->flashdata('success')) {?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php }?>
         <div class="card ">
              <div class="card-header">
              <!--<h3 class="card-title">
                  Users
              </h3>
                 <a href="<?php echo base_url('xAdmin/users/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>-->
				<form method="post" name="frm-exp" id="frm-exp">
					<div class="row">
						 <div class="col-6">
							 <div class="form-group">
								<input type="text" class="seach-key form-control" id="keyword" name="keyword" placeholder="Search">
							 </div>
						 </div>
						 <div class="col-3">
							<div class="form-group">
								<select name="profile_type" id="profile_type" class="form-control seach-key">
									<option value="">-- Select --</option>
									<option value="1">Individual User</option>
									<option value="2">Organization</option>
								</select>
							 </div>
						 </div>
						 <div class="col-3">
								<input type="button" class="btn btn-primary btn-sm pull-right float-right" id="export" name="export" onclick="export_to_excel()" value="Export To Excel">
						 </div>
					</div>
				</form>
			  </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover table-responsive">
               <thead>
                  <tr>
           <th class="no-sort">No.</th>
					 <th>ID.</th>
           <th>Fullname</th>
					 <th>Email</th>
					 <th>Mobile</th>
					 <th>Category</th>
					 <th>Subcategory</th>
					 <th>xOrder</th>
					 <th>Priority</th>
					 <th>Admin Approval</th>
                     <th width="20%">Action</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
	function update_sort_order(order_val, id)
	{

		var getVal = order_val;
		var csrf_test_name	= $('.txt_csrfname').val();
		var base_url = '<?php echo base_url('xAdmin/users/setOrder'); ?>';
		 $.ajax({
			url: base_url,
			type: "post",
			//async: false,
			data: {order_val:getVal, id:id, csrf_test_name:csrf_test_name},
			success: function (response) {
				var returndata = JSON.parse(response);
				var token = returndata.token;
				$('.txt_csrfname').val(token);
				var status = returndata.msg;

				/* swal({
						title: 'Success!',
						text: status,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}); */
			},
			error: function(jqXHR, textStatus, errorThrown) {
				 console.log(textStatus, errorThrown);
			}
		});
	}

  $(document).ready( function () {
		/* $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});*/

		var base_path = '<?php echo base_url(); ?>';
		var userDataTable = $('#example1').DataTable({
			"responsive": true,
			"columnDefs":
			[
				{"targets": 'no-sort', "orderable": false, },
				{"targets": [0], "className": "text-center"},
			 	
			],
			"aaSorting": [],
			"autoWidth": false,
			"serverMethod": 'post',
			"ordering":true,
			"searching": false,
	    "bStateSave": true,
			"language": {
							"zeroRecords":"No matching records found.",
							"infoFiltered":""
						},
			"processing":true, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.

			// Load data for the table's content from an Ajax source
			"ajax": {
			"url": base_path+"xAdmin/users/get_user_data",
			"type":"POST",
			"data":function(data) {
				data.keyword 		= $("#keyword").val();
				data.profile_type 	= $("#profile_type").val();
				},			 
			"error":function(x, status, error) {

			},
			"statusCode": {
			401:function(responseObject, textStatus, jqXHR) {
						},
					},
			}


		});

		//Custom Filter
		$('.seach-key').keyup(function(){
			userDataTable.ajax.reload();
		});

		//Custom Filter
		$('.seach-key').change(function(){
			userDataTable.ajax.reload();
		});

		//$('.user-priority').change(function(){
		$("body").on("change", "#example1 tbody tr .user-priority", function (e) {
			var priorityVal = $(this).val();
			var uid	=	$(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();
			var base_url = '<?php echo base_url('xAdmin/users/updatePriority'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:uid,csrf_test_name:csrf_test_name,priority:priorityVal},
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					//var status = returndata.u_status;
					// location.reload();
					swal({
							title: 'Success!',
							text: "User Priority updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
		});


		//profile_type

		$("body").on("click", "#example1 tbody tr a.status-check", function (e) {
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();
			var base_url = '<?php echo base_url('xAdmin/users/updateStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_status;
					//alert(status);
					$('.txt_csrfname').val(token);
					$("a#change-val-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});

       });


		$("body").on("click", "#example1 tbody tr a.deleted-check", function (e) {
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();
			var base_url = '<?php echo base_url('xAdmin/users/deleteStatus'); ?>';
			swal({
			  title: "Are you sure?",
			  text: "Do you want to delete this record?",
			  type: "warning",
			  buttons: true,
			  dangerMode: true,
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			})
			.then((test) => {
			  if (test.value) {
				 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},
				success: function (response) {

					var returndata = JSON.parse(response);
					var token = returndata.token;
					var userId = returndata.response_id;
					var textShow = returndata.dynamicText;
					var newText = returndata.spanText;
					var removeID = returndata.remove_id;
					//alert(status);
					$('.txt_csrfname').val(token);
					$("a#deleted-stac-"+id).text(newText);
					//$('.remove-tr-'+removeID).remove();
					$('.remove-tr-'+removeID).next('tr.child').remove();
					$('.remove-tr-'+removeID).remove();
					swal({
							title: 'Success!',
							text: "User "+textShow+" Successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});


				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			  } else {
				//swal("Your imaginary file is safe!");
			  }
			});


       });

        $("body").on("click", "#example1 tbody tr a.featured-check", function (e) {
		  	var id = $(this).attr('data-id');
			//alert(id);
			var csrf_test_name	= $('.txt_csrfname').val();
			var base_url = '<?php echo base_url('xAdmin/users/changeStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},
				success: function (response) {

					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					console.log(status);
					$("a#change-stac-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});

       });

         $("body").on("click", "#example1 tbody tr a.approval-check", function (e) {
		  	var id = $(this).attr('data-id');
			//alert(id);
			var csrf_test_name	= $('.txt_csrfname').val();
			var base_url = '<?php echo base_url('xAdmin/users/changeApproval'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},
				success: function (response) {

					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_status;
					$('.txt_csrfname').val(token);
					console.log(status);
					$("a#change-appr-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User approval status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});

       });


	   $('#frm-exp').keypress(function (e) {
		if (e.which === 13) {
			e.preventDefault();
		 }
	 });




    });

$(".allowd_only_numbers").keydown(function (e)
{
	// Allow: backspace, delete, tab, escape, enter
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
	// Allow: Ctrl+A
	(e.keyCode == 65 && e.ctrlKey === true) ||
	// Allow: home, end, left, right
	(e.keyCode >= 35 && e.keyCode <= 39))
	{
		// let it happen, don't do anything
		return;
	}

	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

function export_to_excel()
{
	var keyword = $("#keyword").val();
	var profile_type = $("#profile_type").val();
	if(keyword == '') { keyword = 0; }
	if(profile_type == '') { profile_type = 0; }
	
	var redirect_url = '<?php echo site_url("xAdmin/users/export_to_excel/"); ?>'+keyword+'/'+profile_type;
	var win = window.open(redirect_url, '_blank');
}
</script>