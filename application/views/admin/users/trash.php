<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Users</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php }  ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Users Trash Listing
              </h3>
                 <!--<a href="<?php echo base_url('xAdmin/users/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>-->
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>Fullname</th>
					 <th>Email</th>
					 <th>Mobile</th>
					 <th>Category</th>
					 <th>Subcategory</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $user_data) 
                     { 
					 
					 
					 if($user_data['cat_id'] == 1){
						$file = 'viewDetails'; 
						$fullname = $user_data['title'].ucfirst($user_data['first_name'])."&nbsp;".ucfirst($user_data['middle_name'])."&nbsp;".ucfirst($user_data['last_name']);
						
					 } else {
						$file = 'organizationDetails';
						$fullname = ucwords($user_data['institution_full_name']);
						
					 }
					 
					 if($user_data['is_deleted'] == '1'){
						$userShow = "Restore"; 
					 } else {
						$userShow = "Delete"; 
					 }
					 
					 $countryCode = $this->master_model->getRecords("country", array("id" => $user_data['country_code']));
					 $codes = $countryCode[0]['phonecode'];
					
					 ?>
                  <tr class="remove-tr-<?php echo $user_data['user_id'] ?>">
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:15%"><?php echo $fullname; ?></td> 
					 <td style="width:20%"><?php echo $user_data['email'] ; ?></td> 
					 <td style="width:10%" nowrap><?php echo $codes."-".$user_data['mobile']; ?></td>
					 <td style="width:10%" nowrap><?php echo $user_data['user_category']; ?></td>	
					 <td style="width:20%"><?php echo $user_data['sub_catname']; ?></td>						
                     <td style="width:35%">
						
						<a href="javascript:void(0)" data-id="<?php echo $user_data['user_id'] ?>" class="deleted-check btn btn-success btn-sm"  id="deleted-stac-<?php echo $user_data['user_id'] ?>">
								<?php echo $userShow; ?>
							</a>
						
                     </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
			<input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">                                                                                                                    
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		$("body").on("click", "#example1 tbody tr a.status-check", function (e) { 		
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/updateStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_status;
					//alert(status);
					$('.txt_csrfname').val(token);					
					$("a#change-val-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });	

	   var table2 = $('#example1').DataTable();
		$("body").on("click", "#example1 tbody tr a.deleted-check", function (e) { 		
		  	var id = $(this).attr('data-id');
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/deleteStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var userId = returndata.response_id;
					var textShow = returndata.dynamicText;
					var newText = returndata.spanText;
					var removeID = returndata.remove_id;
					//alert(status);
					
					$('.txt_csrfname').val(token);					
					$("a#deleted-stac-"+id).text(newText);
					$('#remove-tr-'+removeID).remove();
					$(this).parents("tr").remove();
					//remove-tr
					//$("a#deleted-stac-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User "+textShow+" Successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
						location.reload(true);
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	   
	    $(document).on("keypress", ".order-class", function(e){			
			
			
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
			{
				swal({
						title: 'Error!',
						text: "Only Numeric Value Allowed",
						type: 'error',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});	
					
			} else {
				var getVal = $(this).val();
				var id = $(this).attr('data-id');
				var csrf_test_name	= $('.txt_csrfname').val();			
				var base_url = '<?php echo base_url('xAdmin/users/setOrder'); ?>';
				 $.ajax({
					url: base_url,
					type: "post",
					data: {order:getVal, id:id,csrf_test_name:csrf_test_name},				
					success: function (response) {
						var returndata = JSON.parse(response);
						var token = returndata.token;
						$('.txt_csrfname').val(token);
						var status = returndata.msg;					
						swal({
								title: 'Success!',
								text: status,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});	
					},
					error: function(jqXHR, textStatus, errorThrown) {
					   console.log(textStatus, errorThrown);
					}
				});
				
				
			}				
			
			
		 });	
			
	   
	   //$('.featured-check').on('click', function(){	
         $("body").on("click", "#example1 tbody tr a.featured-check", function (e) {    
		  	var id = $(this).attr('data-id');
			//alert(id);
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/changeStatus'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					console.log(status);	
					$("a#change-stac-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });

         $("body").on("click", "#example1 tbody tr a.approval-check", function (e) {    
		  	var id = $(this).attr('data-id');
			//alert(id);
			var csrf_test_name	= $('.txt_csrfname').val();			
			var base_url = '<?php echo base_url('xAdmin/users/changeApproval'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var status = returndata.u_status;
					$('.txt_csrfname').val(token);
					console.log(status);	
					$("a#change-appr-"+id).html(status);
					// location.reload();
					swal({
							title: 'Success!',
							text: "User approval status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       });
	   
	    $(".allownumericwithoutdecimal").on("keypress",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

	
    });
</script>