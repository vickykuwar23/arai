<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Organization Details</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Organization Details</li>
					</ol>
				</div>
				
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
  <!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<!-- Profile Image -->
					<div class="card card-primary card-outline">
						<div class="card-body box-profile">
							<?php 
							if(!empty($profile_info) && $profile_info['org_logo'] != "") 
							{ ?>
								<img src="<?php echo base_url('uploads/organization_profile/').$profile_info['org_logo']; ?>" class="img-fluid bor" style='max-width: 100%; max-height: 100px;margin: 0 auto;display: block;'>
				<?php	} else { ?> <img src="<?php echo base_url('assets/profile_picture/') ?>user_dummy.png" alt="" class="img-fluid bor" style='max-width: 100%; max-height: 100px;margin: 0 auto;display: block;'> <?php } ?>

							<h3 class="profile-username text-center"><?php echo $fullname = $personal_info['institution_full_name']; ?></h3>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
					
					<!-- About Me Box -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">About Organization</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<strong><i class="fas fa-envelope mr-1"></i> Email :</strong>
							<?php echo $personal_info['email']; if($personal_info['valid_email'] == '1') echo ' (Verified)' ?>
							<p class="text-muted"></p>
							<strong><i class="fas fa-mobile mr-1"></i> Mobile :</strong>
							<?php echo $personal_info['iso']." ".$personal_info['phonecode']." - ".$personal_info['mobile'];  if($personal_info['valid_mobile'] == '1') echo ' (Verified)'; ?>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
				<div class="col-md-9">
					<div class="card">
						<div class="card-header p-2">
							<ul class="nav nav-pills">
								<li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Personal Details</a></li>                  
								<li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">ID Proof Images</a></li>
								<li class="nav-item"><a class="nav-link" href="#experience" data-toggle="tab">Website Links</a></li>
								<li class="nav-item"><a class="nav-link" href="#expert" data-toggle="tab">Other Details</a></li>
								<?php /* <li class="nav-item"><a class="nav-link" href="#bod" data-toggle="tab">Board Of Directors</a></li> */ ?>
							</ul>
						</div><!-- /.card-header -->
						<div class="card-body">
							<div class="tab-content">
								<div class="active tab-pane" id="activity">
									<!-- Post -->
									<div class="post"> 
										<p class="text-muted"><b>Representative Name : </b><?php echo $personal_info['spoc_name']; ?></p>                
										<p class="text-muted"><b>Gender : </b><?php echo $personal_info['gender']; ?></p>                
										<p class="text-muted"><b>User Category : </b><?php echo $personal_info['user_category'] ?></p>                
										<p class="text-muted"><b>Institution Type : </b><?php echo $personal_info['sub_catname'] ?></p>                
										<p class="text-muted"><b>Institution Name : </b><?php echo $personal_info['institution_full_name'] ?></p>                
										<p class="text-muted"><b>Domain/ Industry & Sector : </b><?php echo ucfirst(str_replace('other, ','',strtolower($personal_info['domain_name']))); if($personal_info['other_domain_industry'] != "") { echo ", ".$personal_info['other_domain_industry']; } ?></p>                
										<p class="text-muted"><b>Public/Private status : </b><?php echo ucfirst(str_replace('other','',strtolower($personal_info['public_status_name']))); if($personal_info['other_public_prvt'] != "") { echo $personal_info['other_public_prvt']; } ?></p> 
										<p class="text-muted"><b>Organization Sector : </b><?php if(!empty($profile_info) && $profile_info['org_sector_name'] != "") { echo ucfirst(str_replace(', other','',strtolower($profile_info['org_sector_name']))); if($profile_info['org_sector_other'] != "") { echo ", ".$profile_info['org_sector_other']; } } else { echo '--'; } ?></p> 
										<p class="text-muted"><b>Company Overview : </b><?php if(!empty($profile_info) && $profile_info['overview'] != "") { echo $profile_info['overview']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Flat/House No/Building/Apt/Company : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr1'] != "") { echo $profile_info['spoc_bill_addr1']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Area/Colony/Street/Village : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr2'] != "") { echo $profile_info['spoc_bill_addr2']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Town/City : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_city'] != "") { echo $profile_info['spoc_bill_addr_city']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>State : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_state'] != "") { echo $profile_info['spoc_bill_addr_state']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Country : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_country'] != "") { echo $profile_info['spoc_bill_addr_country']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Pincode : </b><?php if(!empty($profile_info) && $profile_info['spoc_bill_addr_pin'] != "") { echo $profile_info['spoc_bill_addr_pin']; } else { echo '--'; } ?></p>
										<p class="text-muted"><b>Secondary Number1 : </b>
											<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code1'] != "" && $profile_info['spoc_sec_num1'] != "") 
											{ 
												echo $profile_info['spoc_sec_num_country_code1']." - ".$profile_info['spoc_sec_num1']; 
											} else { echo '--'; } ?>
										</p>
										<p class="text-muted"><b>Secondary Number2 : </b>
											<?php if(!empty($profile_info) && $profile_info['spoc_sec_num_country_code2'] != "" && $profile_info['spoc_sec_num2'] != "") 
											{
												echo $profile_info['spoc_sec_num_country_code2']." - ".$profile_info['spoc_sec_num2']; 
											} else { echo '--'; } ?>
										</p>
									</div>                  
								</div>
								
								<div class="tab-pane" id="settings">
									<p class="text-muted"><b>PAN Card : </b>
										<?php 
										if(!empty($profile_info) && $profile_info['pan_card'] != "") 
										{ ?><br>
										<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['pan_card']; ?>" target="_blank">
											<?php if(strtolower(pathinfo($profile_info['pan_card'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['pan_card']; } ?>
											<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
											</a>
									<?php	} else { echo '<span class="blank">--</span>'; } ?>
									</p>
									<p class="text-muted"><b>Institution Registration Certificate : </b>
										<?php 
											if(!empty($profile_info) && $profile_info['institution_reg_certificate'] != "") 
											{ ?><br>
											<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; ?>" target="_blank">
												<?php if(strtolower(pathinfo($profile_info['institution_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['institution_reg_certificate']; } ?>
												<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
											</a>
										<?php	} else { echo '<span class="blank">--</span>'; } ?>
									</p>
									<p class="text-muted"><b>Self Declaration : </b>
										<?php 
										if(!empty($profile_info) && $profile_info['self_declaration'] != "") 
										{ ?><br>
											<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['self_declaration']; ?>" target="_blank">
												<?php if(strtolower(pathinfo($profile_info['self_declaration'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['self_declaration']; } ?>
												<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
											</a>
										<?php	} else { echo '<span class="blank">--</span>'; } ?>
									</p>
									<p class="text-muted"><b>Address Proof : </b>
										<?php 
											if(!empty($profile_info) && $profile_info['address_proof'] != "") 
											{ ?><br>
											<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['address_proof']; ?>" target="_blank">
												<?php if(strtolower(pathinfo($profile_info['address_proof'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['address_proof']; } ?>
												<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
											</a>
										<?php	} else { echo '<span class="blank">--</span>'; } ?>
									</p>
									<p class="text-muted"><b>GST Registration Certificate : </b>
										<?php 
											if(!empty($profile_info) && $profile_info['gst_reg_certificate'] != "") 
											{ ?><br>
											<a class="img_outer" href="<?php echo base_url().'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; ?>" target="_blank">
												<?php if(strtolower(pathinfo($profile_info['gst_reg_certificate'], PATHINFO_EXTENSION)) == 'pdf') { $disp_img_name = 'assets/blank_pdf.png'; } else { $disp_img_name = 'uploads/organization_profile/'.$profile_info['gst_reg_certificate']; } ?>
												<img src="<?php echo base_url().$disp_img_name; ?>" class="img-thumbnail" style="max-height:100px; max-width:200px;">
											</a>
										<?php	} else { echo '<span class="blank">--</span>'; } ?>
									</p>
								</div>
								
								<div class="tab-pane" id="experience">
									<p class="text-muted"><b>Website : </b><?php if(!empty($profile_info) && $profile_info['website'] != "") { echo $profile_info['website']; } else { echo '<span class="blank">--</span>'; } ?></p>
									<p class="text-muted"><b>LinkedIn : </b><?php if(!empty($profile_info) && $profile_info['linkedin_page'] != "") { echo $profile_info['linkedin_page']; } else { echo '<span class="blank">--</span>'; } ?></p>						
									
								</div>
								
								<div class="tab-pane" id="expert">
									<p class="text-muted"><b>Specialities & products : </b><?php if(!empty($profile_info) && $profile_info['specialities_products'] != "") { echo $profile_info['specialities_products']; } else { echo '<span class="blank">--</span>'; } ?></p>
									<p class="text-muted"><b>Year of Establishment : </b><?php if(!empty($profile_info) && $profile_info['establishment_year'] != "") { echo $profile_info['establishment_year']; } else { echo '<span class="blank">--</span>'; } ?></p>
									<p class="text-muted d-none"><b>Institution Size : </b><?php if(!empty($profile_info) && $profile_info['institution_size'] != "") { echo $profile_info['institution_size']; } else { echo '<span class="blank">--</span>'; } ?></p>
									<p class="text-muted d-none"><b>Company Evaluation : </b><?php if(!empty($profile_info) && $profile_info['company_evaluation'] != "") { echo $profile_info['company_evaluation']; } else { echo '<span class="blank">--</span>'; } ?></p>
								</div>
								
								<div class="tab-pane" id="bod">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<th class='text-center'>Sr. No</th>
												<th class='text-center'>Name</th>
												<th class='text-center'>Designation</th>
												<th class='text-center'>Since</th>
											</thead>
											<tbody>
												<?php if(empty($bod_info))
													{	?>
													<tr><td colspan='4' class='text-center'>No data available</td></tr>
													<?php	}
													else
													{
														$i=1;
														foreach($bod_info as $bod_key => $bod_val)
														{	?>
														<tr>
															<td class='text-center'><?php echo $i; $i++; ?></td>
															<td><?php echo $bod_val['bod_name']; ?></td>
															<td><?php echo $bod_val['bod_designation']; ?></td>
															<td><?php echo $bod_val['bod_since']; ?></td>
														</tr>
														<?php }
													}	?>
											</tbody>
										</table>													
									</div>
								</div>
								
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div><!-- /.card-body -->
						
				<div class="col-md-4">
                  <label for="" > Discoverable </label>
                 
                  <label class="switch">
                    <input type="checkbox" id="discoverable" <?php if($personal_info['discoverable']=='yes'){ echo 'checked';} ?>> 
                    <span class="slider round"></span>
                  </label>
                 
                 
                 
                      </br>
                  <label for="" > Contactable  </label>
                  
                  <label class="switch">
                    <input type="checkbox" id="contactable" <?php if($personal_info['contactable']=='yes'){ echo 'checked';} ?> > 
                    <span class="slider round"></span>
                  </label>
                
                  
              </div>
						
						
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div><!-- /.container-fluid -->
	</section>
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</div>


<script>
        $('#discoverable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $personal_info['user_id'] ; ?>';
            
            var discoverable 
            if ($('#discoverable:checked').val()=='on') {
              discoverable = 'yes';
            $("#contactable").prop("disabled", false);
            }else{
              discoverable = 'no';
              $('#contactable').prop('checked', false); // Unchecks it
              $("#contactable").prop("disabled", true);
            

            }

         var datastring='discoverable='+ discoverable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_discoverable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
          
          $('#contactable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $$personal_info['user_id'] ; ?>';
            
            var contactable 
            if ($('#contactable:checked').val()=='on') {
              contactable = 'yes';
            }else{
              contactable = 'no';
            }

            var datastring='contactable='+ contactable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_contactable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
   
        
$(window).on('load', function () {
      var  discoverable = '<?php echo $personal_info['discoverable'] ; ?>';
        if (discoverable =='no') {
              ;
           
                $("#contactable").prop("disabled", true);
            }else{
              $("#contactable").prop("disabled", false);
            

            }
      
});          
</script>

