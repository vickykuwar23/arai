<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Details</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">User Details</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                 <div class="text-center">
				<?php if($user_data[0]['profile_picture']!=""){

						$imgPath = base_url('assets/profile_picture/'.$user_data[0]['profile_picture']);	
						if ($user_data[0]['profile_picture']!="") {
							$image = $imgPath;
						} else {
							$image = base_url('assets/no-img.png');
						}
				?>
                 
				<?php } else {
					
					$image = base_url('assets/no-img.png');
				} ?>
					 <img class="profile-user-img img-fluid img-circle"
                       src="<?php echo $image ?>"
                       alt="User profile picture" />
                </div>

                <h3 class="profile-username text-center"><?php echo $fullname = $user_data[0]['title']."&nbsp;".ucfirst($user_data[0]['first_name'])."&nbsp;".ucfirst($user_data[0]['middle_name'])."&nbsp;".ucfirst($user_data[0]['last_name']); ?></h3>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted"><?php echo ucfirst($user_data[0]['town_city_and_state']) ?>, <?php echo ucfirst($user_data[0]['country']) ?></p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                <p class="text-muted">
				<?php 
					//echo $user_data[0]['user_id'];
					$encrptopenssl_expert =  New Opensslencryptdecrypt();
					$skills = $user_data[0]['skill_sets'];
					if($skills!=""){
					$explode = explode(",", $skills);
					foreach($explode as $aquireskills){						
					$skilData = $this->master_model->getRecords("skill_sets",array('id' => $aquireskills));
					//echo $this->db->last_query();
					$skillNames = $encrptopenssl_expert->decrypt($skilData[0]['name']);
				?>	
                 
                  <span class="btn btn-success btn-sm"><?php echo $skillNames; ?></span>
				  
				   <?php } } ?>
                </p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Personal Details</a></li>                  
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">University Details</a></li>
				  <li class="nav-item"><a class="nav-link" href="#experience" data-toggle="tab">Experience Details</a></li>
				  <li class="nav-item"><a class="nav-link" href="#expert" data-toggle="tab">Expert Details</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post"> 
						<?php $getCountryCodes = $this->master_model->getRecords("country",array('id' => $user_data[0]['country_code'])); ?>
						<p class="text-muted"><b>Membership Type : </b><?php echo $user_data[0]['membership_type'] ?></p>
						<p class="text-muted"><b>Category Name : </b><?php echo ucwords($user_data[0]['user_category']) ?></p>                    
						<p class="text-muted"><b>Subcategory Name : </b><?php echo ucwords($user_data[0]['sub_catname']) ?></p> 
						<p class="text-muted"><b>Email ID : </b><?php echo $user_data[0]['email']; if($user_data[0]['valid_email'] == '1') echo ' (Verified)' ?></p> 
						<p class="text-muted"><b>Mobile No. : </b><?php echo $getCountryCodes[0]['phonecode']."-".$user_data[0]['mobile']; if($user_data[0]['valid_mobile'] == '1') echo ' (Verified)'; ?></p> 	
						<p class="text-muted"><b>Address : </b><?php echo ucwords($user_data[0]['area_colony_street_village']) ?></p>						
						<p class="text-muted"><b>Pincode : </b><?php echo $user_data[0]['pincode'] ?></p>
					</div>
                    <!-- /.post -->
					
                                       
                  </div>
                  
                  

					<div class="tab-pane" id="settings">
						<p class="text-muted"><b>University Name : </b><?php echo ucwords($user_data[0]['university_name']) ?></p>
                        <p class="text-muted"><b>Course Name : </b><?php echo strtoupper($user_data[0]['university_course']) ?></p>
						<p class="text-muted"><b>From Year : </b><?php echo $user_data[0]['university_since_year'] ?></p>
						<p class="text-muted"><b>To Year : </b><?php echo $user_data[0]['university_to_year'] ?></p>						
						<p class="text-muted"><b>Location : </b><?php echo $user_data[0]['university_location'] ?></p>
						<p class="text-muted"><b>Current Study Course : </b><?php echo strtoupper($user_data[0]['current_study_course']) ?></p>	
						<p class="text-muted"><b>Location : </b><?php echo ucwords($user_data[0]['university_location']) ?></p>
						<p class="text-muted"><b>Current Study Since Year : </b><?php echo $user_data[0]['current_study_since_year'] ?></p>
						<p class="text-muted"><b>Current Study To Year : </b><?php echo $user_data[0]['current_study_to_year'] ?></p>
						<p class="text-muted"><b>Details : </b><?php echo ucfirst($user_data[0]['current_study_description']) ?></p>
						<p class="text-muted"><b>Training & Study : </b><?php echo ucfirst($user_data[0]['domain_and_area_of_training_and_study']) ?></p>
					</div>
					
					<div class="tab-pane" id="experience">
						<p class="text-muted"><b>Experience (in Years) : </b><?php echo $user_data[0]['event_experience_in_year'] ?></p>
                        <p class="text-muted"><b>Description : </b><?php echo ucfirst($user_data[0]['event_description_of_work']) ?></p>
						<p class="text-muted"><b>Technical Experience (in Years) : </b><?php echo $user_data[0]['technical_experience_in_year'] ?></p>
						<p class="text-muted"><b>Technical Description : </b><?php echo ucfirst($user_data[0]['technical_description_of_work']) ?></p>						
												
					</div>
					
					<div class="tab-pane" id="expert">
						<form name="frm" id="frm" method="post">
							<div class="form-group">
								<p class="text-muted"></p>	
								<p class="text-muted"><b>Fields Area that you like : </b><br>
                  <?php if(!empty($expert_data[0]['specify_fields_area_that_you_would_like'])) { ?>
                  <?php foreach ($expert_data[0]['specify_fields_area_that_you_would_like'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
								<p class="text-muted"><b>Bio Data : </b>
                  <?php if(!empty($expert_data[0]['bio_data'])) { ?>
                    <?php echo $expert_data[0]['bio_data'] ?>
                  <?php } ?>
                </p>
                <p class="text-muted"><b>Years of Experience : </b>
                  <?php if(!empty($expert_data[0]['years_of_experience'])) { ?>
                    <?php echo $expert_data[0]['years_of_experience'] ?>
                  <?php } ?>
                </p>
                <p class="text-muted"><b>Number of Paper Publications : </b>
                  <?php if(!empty($expert_data[0]['no_of_paper_publication'])) { ?>
                    <?php echo $expert_data[0]['no_of_paper_publication'] ?>
                  <?php } ?>
                </p>
                <p class="text-muted"><b>Paper Year : </b><br>
                  <?php if(!empty($expert_data[0]['paper_year'])) { ?>
                  <?php foreach ($expert_data[0]['paper_year'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <p class="text-muted"><b>Paper Conf. Name : </b><br>
                  <?php if(!empty($expert_data[0]['paper_conf_name'])) { ?>
                  <?php foreach ($expert_data[0]['paper_conf_name'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <p class="text-muted"><b>Paper Title : </b><br>
                  <?php if(!empty($expert_data[0]['paper_title'])) { ?>
                  <?php foreach ($expert_data[0]['paper_title'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <p class="text-muted"><b>Number of Patents : </b>
                  <?php if(!empty($expert_data[0]['no_of_patents'])) { ?>
                    <?php echo $expert_data[0]['no_of_patents'] ?>
                  <?php } ?>
                </p>
                <p class="text-muted"><b>Patent Year : </b><br>
                  <?php if(!empty($expert_data[0]['patent_year'])) { ?>
                  <?php foreach ($expert_data[0]['patent_year'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <p class="text-muted"><b>Patent Number : </b><br>
                  <?php if(!empty($expert_data[0]['patent_number'])) { ?>
                  <?php foreach ($expert_data[0]['patent_number'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <?php if(!empty($expert_data[0]['patent_title'])) { ?>
                <p class="text-muted"><b>Patent Title : </b><br>
                  <?php foreach ($expert_data[0]['patent_title'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>


                <p class="text-muted"><b>Portal Name : </b><br>
                  <?php if(!empty($expert_data[0]['portal_name_multiple'])) { ?>
                  <?php foreach ($expert_data[0]['portal_name_multiple'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>
                <p class="text-muted"><b>Portal Link : </b><br>
                  <?php if(!empty($expert_data[0]['portal_link_multiple'])) { ?>
                  <?php foreach ($expert_data[0]['portal_link_multiple'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>

                <p class="text-muted"><b>Portal Description : </b><br>
                  <?php if(!empty($expert_data[0]['portal_description_multiple'])) { ?>
                  <?php foreach ($expert_data[0]['portal_description_multiple'] as $key => $value) { ?>
                    <?php echo $key+1,') '; echo $value; ?><br> 
                  <?php }} ?>
                </p>


				</div>
							<?php 
							//echo $user_data[0]['user_sub_category_id'].">>>".$user_data[0]['aplied_for_expert']."==".$user_data[0]['previous_user_sub_cat_id'];
							//if($user_data[0]['user_sub_category_id'] != '11'): ?>	
							<div class="form-group d-none">
								<div class="form-check form-check-inline">
									
									<input type="checkbox" class="chk-expert" data-cat_id="<?php echo $user_data[0]['user_sub_category_id'] ?>" data-pre_id="<?php echo $user_data[0]['previous_user_sub_cat_id'] ?>"  data-uid="<?php echo $user_data[0]['user_id'] ?>" <?php if(($user_data[0]['aplied_for_expert'] == "approved") || ($user_data[0]['user_sub_category_id'] == "11")): ?> checked="checked" <?php endif; ?> name="is_expert" id="is_expert" value="approved" >
									&nbsp;&nbsp;<label class="form-check-label" for="Checkbox1">Is Expert.? <?php //echo $user_data[0]['user_id'] ?></label>
								</div>	

                </div>
                
                 
							<?php //endif; ?>
							
						</form>
            <?php if($expert_data[0]['aplied_for_expert'] == "yes"): ?>
              
                <div>
                  <div class="form-group col-md-6">
                  <label>Is Expert.? </label>
                  <select name="claim_status" id="claim_status" class="form-control">
                    <?php echo $expert_claim_admin_approval = $expert_data[0]['expert_claim_admin_approval']; ?>

                  <option <?php if($expert_claim_admin_approval=='Pending') {echo "selected";} ?>  value="Pending">Pending</option>
                  <option <?php if($expert_claim_admin_approval=='Approved') {echo "selected";} ?> value="Approved">Approved</option>
                  <option <?php if($expert_claim_admin_approval=='Rejected') {echo "selected";} ?> value="Rejected">Reject</option>

                  </select>
                  <span id="status_error" style="color: red;font-size: 12px"></span>
                  


                  </div>

                  <div class="form-group col-md-6">
                    <label>Reason</label>
                    <input value="<?php echo $expert_data[0]['claim_expert_reason']  ?>" type="text" class="form-control" id="reason" name="reason">
                    <span id="reason_error" style="color: red;font-size: 12px"></span>
                  </div>
                  <div class="form-group col-md-3">
                  <button id="updated_status" class="btn btn-primary" data-cat_id="<?php echo $user_data[0]['user_sub_category_id'] ?>" data-pre_id="<?php echo $user_data[0]['previous_user_sub_cat_id'] ?>"  data-uid="<?php echo $user_data[0]['user_id'] ?>">Update Status</button>

                  </div>
                </div>
            <?php endif ?>


					</div>
					
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            
             <div class="col-md-4">
                  <label for="" > Discoverable </label>
                 
                  <label class="switch">
                    <input type="checkbox" id="discoverable" <?php if($user_data[0]['discoverable']=='yes'){ echo 'checked';} ?>> 
                    <span class="slider round"></span>
                  </label>
                 
                 
                 
                      </br>
                  <label for="" > Contactable  </label>
                  
                  <label class="switch">
                    <input type="checkbox" id="contactable" <?php if($user_data[0]['contactable']=='yes'){ echo 'checked';} ?> > 
                    <span class="slider round"></span>
                  </label>
                
                  
              </div>
              
              
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
	<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
</div>

<script>
$(document).ready(function(){
	$('.chk-expert').on('click', function(){		 
		
		if($(this).is(":checked")) {				
			//$( ".audi-chk" ).prop( "checked", false );	
			var val = 'approved';	
			} else {
			var val = 'no';	
		}

		var uid = $(this).attr('data-uid');	
		var sid = $(this).attr('data-pre_id');
		var cid = $(this).attr('data-cat_id');
		var cs_t = 	$('.token').val();	
		
		var base_url = '<?php echo base_url() ?>';
		 $.ajax({
			type:'POST',
			url: base_url+'xAdmin/users/markexpert',
			data:'isexpert='+val+'&uid='+uid+'&csrf_test_name='+cs_t+'&sid='+sid+'&cid='+cid,				
			success:function(data){

				var output = JSON.parse(data);
				//var status = output.u_featured;					
				$(".token").val(output.token); //data-cat_id //data-pre_id
				//$("#featured-"+ID).html(status);
				var cat_id = output.user_cat_id;
				var prev_id = output.previous_cat_id;
				$(".chk-expert").attr("data-cat_id", cat_id);
				$(".chk-expert").attr("data-pre_id", prev_id);
				swal({
						title: 'Success!',
						text: "Expert status successfully updated.",
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				
			}
		});	
	});
});	


$('#updated_status').click(function(){
        $("#reason_error").html("");  
        $("#status_error").html("");
        var reason =  $('#reason').val();
        if (reason=='') {
          $("#reason_error").html("Please enter reason");
          return false;
        }

        var  val = $("#claim_status").val();

        if (val=='Pending') {
           $("#status_error").html("Please select valid status");
            return false;
        }
        
         swal(
          {
              title:"Confirm?" ,
              text: "Are you sure you want to change the expert status of this user?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
          }).then(function (result) 
          {
          if (result.value) 
          {

          var uid = $("#updated_status").attr('data-uid'); 
          var sid = $("#updated_status").attr('data-pre_id');
          var cid = $("#updated_status").attr('data-cat_id');
          var cs_t =  $('.token').val();

        
 
          


        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

        


              var base_url = '<?php echo base_url() ?>';

           var datastring='reason='+reason+'&isexpert='+val+'&uid='+uid+'&csrf_test_name='+cs_t+'&sid='+sid+'&cid='+cid;
             $.ajax({
                 type: 'POST',
                 data:datastring,
                  url: base_url+'xAdmin/users/markexpertNew',
                 beforeSend: function(){
                   $('#preloader-loader').css("display", "block");
                 },
                 success: function(data){ 
                    
                    var output = JSON.parse(data);
                  //var status = output.u_featured;         
                  $(".token").val(output.token); //data-cat_id //data-pre_id
                  //$("#featured-"+ID).html(status);
                  var cat_id = output.user_cat_id;
                  var prev_id = output.previous_cat_id;
                  $("#updated_status").attr("data-cat_id", cat_id);
                  $("#updated_status").attr("data-pre_id", prev_id);
                  swal({
                      title: 'Success!',
                      text: "Expert status successfully updated.",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function() {
                       location.reload(); 
                  });
                 },
                 complete: function(){
                          $('#preloader-loader').css("display", "none");
                  },

               });

                }
           } )  

          })




</script>


<script>
        $('#discoverable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $user_data[0]['user_id'] ; ?>';
            
            var discoverable 
            if ($('#discoverable:checked').val()=='on') {
              discoverable = 'yes';
            $("#contactable").prop("disabled", false);
            }else{
              discoverable = 'no';
              $('#contactable').prop('checked', false); // Unchecks it
              $("#contactable").prop("disabled", true);
            

            }

         var datastring='discoverable='+ discoverable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_discoverable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
          
          $('#contactable').change(function(){
              
              var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>';
              var  csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';

              var  user_id = '<?php echo $user_data[0]['user_id'] ; ?>';
            
            var contactable 
            if ($('#contactable:checked').val()=='on') {
              contactable = 'yes';
            }else{
              contactable = 'no';
            }

            var datastring='contactable='+ contactable + '&'+'user_id='+ user_id + '&' + csrfName + '='+csrfHash;
           $.ajax({
               type: 'POST',
               data:datastring,
               url: "<?php echo base_url();?>profile/is_contactable",
               beforeSend: function(){
                 $('#preloader-loader').css("display", "block");
               },
               success: function(res){ 
                 // var json = $.parseJSON(res);
                 // $('#subcategory').html(json.str);
                 //csrfName=json.name;
                 //csrfHash=json.value;
               },
               complete: function(){
                        $('#preloader-loader').css("display", "none");
                },

             });


            
          })
          
   
        
$(window).on('load', function () {
      var  discoverable = '<?php echo $user_data[0]['discoverable'] ; ?>';
        if (discoverable =='no') {
              ;
           
                $("#contactable").prop("disabled", true);
            }else{
              $("#contactable").prop("disabled", false);
            

            }
      
});          
</script>
