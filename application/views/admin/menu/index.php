<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Menu</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Menu</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Menu List
              </h3>
                 <a href="<?php echo base_url('xAdmin/menu/add') ?>" class="btn btn-primary btn-sm float-right">Add Menu</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
                     <th>User Name</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $info) 
                     { ?>
                  <tr>
                     <td><?php echo $i; ?></td>
                     <td><?php echo $info['menu_name'] ; ?></td>
                     <td><?php      if($info['status']=="1"){ ?>
                        <a href="<?php echo base_url(); ?>xAdmin/users/changeStatus/<?php echo $info['menu_id'].'/'.$info['status']; ?>"><button class="btn btn-success btn-sm">Active </button> </a><?php }
                           else if($info['status']=="0"){ ?>
                        <a href="<?php echo base_url(); ?>xAdmin/users/changeStatus/<?php echo $info['menu_id'].'/'.$info['status']; ?>"><button class="btn btn-danger btn-sm">Block</button> </a><?php }
                           ?>
                     </td>
                     <td>
                        <a href="<?php echo base_url();?>xAdmin/users/edit/<?php echo $info['menu_id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>
                        &nbsp;
                        <a href="<?php echo base_url();?>xAdmin/users/details/<?php echo $info['menu_id'];  ?>" ><button class="btn btn-info btn-sm">Details</button></a>
                        &nbsp;
                        <a href="<?php echo base_url();?>xAdmin/users/delete/<?php echo $info['menu_id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>
                     </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
     $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    } );
</script>