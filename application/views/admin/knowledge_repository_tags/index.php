<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Knowledge Repository Tag</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Knowledge Repository Tag</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<?php if( $this->session->flashdata('success')){ ?>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h5><i class="icon fas fa-check"></i> Success!</h5>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } ?>
			<div class="card ">
				<div class="card-header">
					<h3 class="card-title">
						Knowledge Repository Tag List
					</h3>
					<a href="<?php echo base_url('xAdmin/knowledge_repository_tags/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>
				</div>
				
				<div class="card-body">
					<table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tag Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  
								$i=1;
								foreach($records as $tag_data) 
								{ ?>
								<tr>
									<td style="width:5%"><?php echo $i; ?></td>
									<td style="width:65%"><?php echo $tag_data['tag_name'] ; ?></td>                     
									<td style="width:18%">
										<a href="<?php echo base_url();?>xAdmin/knowledge_repository_tags/edit/<?php echo $tag_data['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
										<!--<a href="<?php echo base_url();?>xAdmin/usercategories/delete/<?php echo $tag_data['id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>-->
										<?php      
											if($tag_data['status']=="Active"){
												$status = 'Block';
											?>
											<a href="<?php echo base_url(); ?>xAdmin/knowledge_repository_tags/changeStatus/<?php echo $tag_data['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
											else if($tag_data['status']=="Block"){
												$status = 'Active';
											?>
											<a href="<?php echo base_url(); ?>xAdmin/knowledge_repository_tags/changeStatus/<?php echo $tag_data['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
										?>
									</td>
								</tr>
							<?php $i++; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
  $(document).ready( function () 
	{
		$("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
	});
</script>