<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Plan</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Plan</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Plan
			  </h3>
				  <a href="<?php echo base_url('xAdmin/plan') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="planfrm" name="planfrm" role="form" >
						  <div class="row">
							<div class="col-6">
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Category *</label>
								<select name="p_category" id="p_category" class="form-control drop">
									<option value="">-- Select Plan Type --</option>
									<option value="1" <?php if($plan_data[0]['plan_category'] == "1"):  ?> selected="selected" <?php endif; ?>>Individual</option>
									<option value="2" <?php if($plan_data[0]['plan_category'] == "2"):  ?> selected="selected" <?php endif; ?>>Organizational</option>
								</select>
								<span><?php echo form_error('p_category'); ?></span>
							  </div>	
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Type *</label>
								<select name="p_type" id="p_type" class="form-control">
									<option value="">-- Select Plan Type --</option>
									<option value="1" <?php if($plan_data[0]['plan_type'] == "General"):  ?> selected="selected" <?php endif; ?>>General</option>
									<option value="2" <?php if($plan_data[0]['plan_type'] == "Upgrade"):  ?> selected="selected" <?php endif; ?>>Upgrade</option>
								</select>
								<span><?php echo form_error('discount_apply'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Name</label>
								<input type="text" class="form-control" id="p_name" placeholder="Enter Plan Name" value="<?php echo $plan_data[0]['plan_name'] ?>" name="p_name" maxlength="20"  >
								<span><?php echo form_error('p_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Description *</label>
								<textarea class="form-control" id="plan_desc" placeholder="Enter Plan Description " name="plan_desc" rows="6" cols="12"><?php echo $plan_data[0]['plan_desc'] ?></textarea>								
								<span><?php echo form_error('plan_desc'); ?></span>
							  </div>
							  <div class="form-group" id="hide-once" <?php if($plan_data[0]['plan_category'] == 2): ?> style="display:block;" <?php endif; ?>>
								<label for="exampleInputEmail1">No Of Users *</label>
								<input type="text" class="form-control" id="no_of_users" placeholder="No Of Users" value="<?php echo $plan_data[0]['no_of_users'] ?>" name="no_of_users"  >
								<span><?php echo form_error('no_of_users'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Duration *</label>
								<select name="p_duration" id="p_duration" class="form-control">
									<option value="">-- Plan Duration --</option>
									<option value="Monthly" <?php if($plan_data[0]['plan_duration'] == "Monthly"):  ?> selected="selected" <?php endif; ?>>Monthly</option>
									<option value="Quarterly" <?php if($plan_data[0]['plan_duration'] == "Quarterly"):  ?> selected="selected" <?php endif; ?>>Quarterly</option>
									<option value="Half_Yearly" <?php if($plan_data[0]['plan_duration'] == "Half_Yearly"):  ?> selected="selected" <?php endif; ?>>Half Yearly</option>
									<option value="Yearly" <?php if($plan_data[0]['plan_duration'] == "Yearly"):  ?> selected="selected" <?php endif; ?>>Yearly</option>
									<option value="2" <?php if($plan_data[0]['plan_duration'] == "2"):  ?> selected="selected" <?php endif; ?>>2 Years</option>
									<option value="3" <?php if($plan_data[0]['plan_duration'] == "3"):  ?> selected="selected" <?php endif; ?>>3 Years</option>
									<option value="4" <?php if($plan_data[0]['plan_duration'] == "4"):  ?> selected="selected" <?php endif; ?>>4 Years</option>
									<option value="5" <?php if($plan_data[0]['plan_duration'] == "5"):  ?> selected="selected" <?php endif; ?>>5 Years</option>
								</select>
								<span><?php echo form_error('p_duration'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Plan Amount *</label>
								<input type="text" class="form-control" id="p_amount" placeholder="Enter Plan Amount" value="<?php echo $plan_data[0]['plan_amount'] ?>" name="p_amount"  >
								<span><?php echo form_error('p_amount'); ?></span>
							  </div>
							  	
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
 $('#planfrm').validate({
    rules: {
	  p_category: {
        required: true
      },	
      p_type: {
        required: true
      },
	  p_name: {
        required: true
      },
	  plan_desc: {
        required: true
      },
	  p_amount: {
        required: true,
		number:true
      },
	  no_of_users: {
        required: true,
		number:true
      },
	  p_duration: {
        required: true
      }
    },
    messages: {
	  p_category: {
        required: "This field is required"
      },	
      p_type: {
        required: "This field is required"
      },
	  p_name: {
        required: "This field is required"
      },
	  plan_desc: {
        required: "This field is required"
      },
	  p_amount: {
        required: "This field is required"
      },
	  no_of_users: {
        required: "This field is required"
      },
	  p_duration: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
  //$("#hide-once").hide(); 
   $(document).on('change','.drop',function(){
	   var catname = $("#p_category").val();
	   
	   if(catname == 2){
		 $("#hide-once").show();  
	   } else {
		  $("#hide-once").hide();  
	   }
	});
  
  
  
});
</script>


