<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Plan</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Plan</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Plan List
              </h3>
                 <a href="<?php echo base_url('xAdmin/plan/add') ?>" class="btn btn-primary btn-sm float-right">Add</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>Plan Category</th>
                     <th>Plan Type</th>
					 <th>Plan Name</th>
					 <th>Plan Amount</th>
					 <th>Plan Duration</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $plan_data) 
                     { 
					 
					 if($plan_data['plan_category'] == 1){$ctypes = 'Individual';}else {$ctypes = 'Organizational';}
					 
					 if($plan_data['plan_duration'] == "Half_Yearly"): $planD = 'Half Yearly'; else: $planD = $plan_data['plan_duration']; endif;
					 ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
                     <td style="width:20%"><?php echo $ctypes; ?></td>
					 <td style="width:20%"><?php echo $plan_data['plan_type']; ?></td>
					 <td style="width:15%"><?php echo $plan_data['plan_name']; ?></td>
					 <td style="width:20%"><?php echo $plan_data['plan_amount']; ?></td>	
					 <td style="width:20%"><?php echo $planD; ?></td>	
                     <td style="width:18%">
						
                        <a href="<?php echo base_url();?>xAdmin/plan/edit/<?php echo $plan_data['plan_id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
                        	<?php      
						if($plan_data['status']=="Active"){
							$status = 'Block';
						?>
                        <a href="<?php echo base_url(); ?>xAdmin/plan/changeStatus/<?php echo $plan_data['plan_id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                           else if($plan_data['status']=="Block"){
							$status = 'Active';
						   ?>
                        <a href="<?php echo base_url(); ?>xAdmin/plan/changeStatus/<?php echo $plan_data['plan_id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                           ?>
						
					 </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		
	
    });
</script>