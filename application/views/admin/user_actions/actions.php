<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Actions</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Actions</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				 Edit User Actions
			  </h3>
				 
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="actionfrm" name="actionfrm" role="form" >
						  <div class="row">
							<div class="col-6">
							 <div class="form-group">
								<label for="exampleInputEmail1">Select Category</label>
								<select name="category_id" id="category_id" class="form-control" >
								<option value="">-- Select Category --</option>
								<?php 
								if(count($categories_details) > 0){
									foreach($categories_details as $details){
								?>
								<option value="<?php echo $details['id'] ?>"><?php echo $details['category_name'] ?></option>	
								<?php 
									} // For End
								} // If End
								?>
								</select>								
								<span><?php echo form_error('identity_group'); ?></span>
							  </div>
								<div class="form-group" id="permission">

								</div>
							  <!--<div class="form-group">
								<label for="exampleInputEmail1">User Access Name</label>
								<input type="text" class="form-control" id="user_access" placeholder="Enter User Access Name" name="user_access" maxlength="200" required>
								<span><?php echo form_error('user_access'); ?></span>
							  </div>-->							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Edit</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" id="csrf" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#actionfrm').validate({
    rules: {
	  identity_group: {
        required: true
      },	
      user_access: {
        required: true,
		minlength:2
      },
    },
    messages: {
	identity_group: {
        required: "This field is required"
      },
      user_access: {
        required: "This field is required",
		minlength: "Enter Identity Group Name must be at least {0} characters long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
  
  $('#category_id').change(function(){
    var cat_id = $(this).val();
	var hashValue = $('#csrf').val();
    $.ajax({
		 url:'<?php echo base_url('xAdmin/useractions/get_actions')?>',
		 method: 'post',
		 data: {category_id: cat_id, '<?php echo $this->security->get_csrf_token_name(); ?>':hashValue},
		 //dataType: 'json',
		 success: function(response){
		   $('#permission').html(response);        
		 }
	   });   
   }); // Category End
  
});
</script>


