<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">FAQ Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">FAQ Management</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit FAQ Management
			  </h3>
				  <a href="<?php echo base_url('xAdmin/faq') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="faqfrm" name="faqfrm" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-12">	
							 <div class="form-group">
								<label for="exampleInputEmail1">FAQ Title</label>
								<input type="text" class="form-control" id="faq_title" placeholder="Enter FAQ Title " name="faq_title" value="<?php echo $faq_data[0]['faq_title']; ?>">								
								<span><?php echo form_error('faq_title'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">FAQ Description</label>
								<textarea class="form-control" id="faq_desc" placeholder="Enter FAQ Description ckeditor" name="faq_desc" rows="6" cols="12"><?php echo $faq_data[0]['faq_desc']; ?></textarea>								
								<script>
								/*CKEDITOR.replace( 'faq_desc', {
									filebrowserBrowseUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/ckfinder.html?Type=Images',
									filebrowserUploadUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl: '<?php echo base_url(); ?>assets/front/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserWindowWidth : '1000',
									filebrowserWindowHeight : '700'
								});*/
								CKEDITOR.replace('faq_desc');
								</script>
								<span><?php echo form_error('faq_desc'); ?></span>
							  </div>							
							  	<div class="form-group">
								<label for="exampleInputEmail1">Upload</label>
								<input type="file" class="form-control" id="faq_img" placeholder="Upload News" name="faq_img" >
								<?php if($faq_data[0]['faq_img']!=""){ ?>
								<p>Filename: <b><a href="<?php echo base_url('assets/faq/'.$faq_data[0]['faq_img']); ?>" target="_blank"><?php echo $faq_data[0]['faq_img']; ?></a></b></p>	
								<?php } ?>
								
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">xOrder</label>
								<input type="text" class="form-control" id="faq_order" placeholder="Enter FAQ Order" name="faq_order" value="<?php echo $faq_data[0]['xOrder']; ?>">								
								<span><?php //echo form_error('faq_order'); ?></span>
							  </div>						  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  
  jQuery.validator.addMethod("ckrequired", function (value, element) {  
		var idname = $(element).attr('id');  
		var editor = CKEDITOR.instances[idname];  
		var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
		if (ckValue.length === 0) {  
			//if empty or trimmed value then remove extra spacing to current control  
			$(element).val(ckValue);  
		} else {  
			//If not empty then leave the value as it is  
			$(element).val(editor.getData());  
		}  
		return $(element).val().length > 0;  
	}, "This field is required"); 

	function GetTextFromHtml(html) {
		var dv = document.createElement("DIV");
		dv.innerHTML = html;
		return dv.textContent || dv.innerText || "";
	}	
	
	
	$.validator.addMethod("valid_img_format", function(value, element)
	{
		if(value != "")
		{
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
			var fileExt = value.toLowerCase();
			fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
			if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
		}else return true;
	});

	$.validator.addMethod('filesize', function (value, element, param) {
		 return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
  
  
  $('#faqfrm').validate({
    rules: {
      faq_title: {
        required: true,
		minlength:5
      },
	  faq_desc: {
        ckrequired: true
      },
	  faq_img:{
		//valid_img_format: true, 
		filesize:2000000
	  }
    },
    messages: {
      faq_title: {
        required: "This field is required",
		minlength: "Enter FAQ Name must be at least {0} characters long"
      },
	  faq_desc: {
        required: "This field is required"
      },
	  faq_img: {
        //valid_img_format: "Upload Only .jpg, .jpeg, .png or .gif image only",
		filesize: "Upload below 2MB image only."
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


