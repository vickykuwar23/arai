<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>

<style>
	/* Prelaoder */
	.currency_div {
    	margin-top: 40px;
	}
	.previous_img_outer { position: relative; }
	.previous_img_outer img { max-width: 800px; max-height: 200px; border: 3px solid #ccc; padding: 6px; background: #fff; border-radius: 5px; }
	.previous_img_outer a.btn { position: absolute; left: 0; bottom: 0; padding: 5px 10px 6px; font-size: 12px; border-radius: 5px; line-height: 12px; }

	.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 15px !important;
		}
	.error{color: red;}
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	@-webkit-keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	@keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	  .custom-file input[type="file"] {
            display: none;
        }
        
        .custom-file .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .file-details .file-list {
	border: 1px solid #ddd;
	padding: 8px 5px;
	border-radius: 4px;
	text-align: center;
	position: relative;
	margin: 0 0 15px 0;
	font-size: 12px;
	word-break: break-all;
	height:100px;
}

.file-details .file-list .file_ext_title h4 {
	margin: 0;
	font-size: 18px !important;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	line-height: 80px;
}

.file-details .file-list a.file-close {
	position: absolute;
	right: 0;
	top: 0;
	width: 25px;
	color: #e23751;
	background: #fff;
	border: 1px solid #ddd;
}

.file-details .file-list span {
	width: 100%;
	display: inline-block;
	font-size: 42px;
	color: #FFC107;
	height: 50px;
}
</style>
<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Resource Sharing</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Resource Sharing</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Resource Sharing
					</h3>
				  <a href="<?php echo base_url('xAdmin/resource_sharing') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div>
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="POST" id="ResourceForm" name="BlogForm" enctype="multipart/form-data">		
						<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

						<?php if ($mode == 'Update') { ?>
							<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Resource ID  </label>
									<input type="text" class="form-control"  value="<?php echo $form_data[0]['id_disp']  ?>" readonly>
									
								</div>
							</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Name of Resource<em>*</em></label>
									<input type="text" class="form-control" name="resouce_name" id="resouce_name" value="<?php if($mode == 'Add') { echo set_value('resouce_name'); } else { echo $form_data[0]['resouce_name']; } ?>">
									<?php if(form_error('resouce_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('resouce_name'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row">
						    	<div class="col-md-12">

								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Resource Details<em>*</em></label>
									<input type="text" class="form-control" name="resource_details" id="resource_details" value="<?php if($mode == 'Add') { echo set_value('resource_details'); } else { echo $form_data[0]['resource_details']; } ?>" required >
									<?php if(form_error('resource_details')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('resource_details'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row">
						    	<div class="col-md-12">

								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Resource Specification<em>*</em></label>
									<input type="text" class="form-control" name="resource_specification" id="resource_specification" value="<?php if($mode == 'Add') { echo set_value('resource_specification'); } else { echo $form_data[0]['resource_specification']; } ?>" required >
									<?php if(form_error('resource_specification')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('resource_specification'); ?></label> <?php } ?>
								</div>
							</div>
							
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Owner Name<em>*</em></label>
									<input type="text" class="form-control" name="owner_name" id="owner_name" value="<?php if($mode == 'Add') { echo set_value('owner_name'); } else { echo $form_data[0]['owner_name']; } ?>">
									<?php if(form_error('owner_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('owner_name'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Owner Email<em>*</em></label>
									<input type="text" class="form-control" name="owner_email" id="owner_email" value="<?php if($mode == 'Add') { echo set_value('owner_email'); } else { echo $form_data[0]['owner_email']; } ?>">
									<?php if(form_error('owner_email')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('owner_email'); ?></label> <?php } ?>
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
		                          <label class="form-control-placeholder leftLable flotingCss" for="cname">Country<em>*</em></label>
		                           <select class="form-control country_code" id="country_code" name="country_code" required="">
		                               <?php foreach ($country_codes as $key => $code) { ?>
		                                 <option <?php if($code['id']==99 && $mode=='Add') {echo 'selected';}elseif($code['id']==$form_data[0]['country_code']){echo 'selected';} ?> value="<?php echo $code['id'] ?>"><?php echo $code['iso']." ".$code['phonecode'] ?></option>
		                              <?php } ?>
		                           </select>
		                           <div class="error" style="color:#F00"><?php echo form_error('country_code'); ?> </div>
		                           </div>
                       		 </div>
							<div class="col-md-10">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Owner Contact Number<em>*</em></label>
									<input type="text" class="form-control" name="owner_contact_number" id="owner_contact_number" value="<?php if($mode == 'Add') { echo set_value('owner_contact_number'); } else { echo $form_data[0]['owner_contact_number']; } ?>">
									<?php if(form_error('owner_contact_number')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('owner_contact_number'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						

						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum pl-0">
							    <label class="form-check-label" for="exampleCheck1"><strong>Other Details</strong></label>
								 <input type="hidden" name="other_details_check" value="0">
							    <input type="checkbox" name="other_details_check" id="other_details_check" class="other_details_check" <?php if($mode == 'Update' && $form_data[0]['other_details_check']==1){echo 'checked';} ?>  value="1" />
							  </div>
							  </div>
							
							</div>
						</div>

						<?php if($mode == 'Update' && $form_data[0]['other_details_check']==1){
							$details_display= '';
						}else{
							$details_display= 'd-none';
						} ?>

						<div class="row other_details_row <?php echo $details_display ?>">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Other Details<em>*</em></label>
									<input type="text" class="form-control" name="other_details" id="other_details" value="<?php if($mode == 'Add') { echo set_value('other_details'); } else { echo $form_data[0]['other_details']; } ?>">
									<?php if(form_error('other_details')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('other_details'); ?></label> <?php } ?>
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum pl-0">
							    <label class="form-check-label" for="exampleCheck1"><strong>Terms & Conditions</strong></label>
								<input type="hidden" name="terms_condition_check" value="0">
							    <input type="checkbox" name="terms_condition_check" id="terms_condition_check" class="terms_condition_check"  value="1" <?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){echo 'checked';} ?> />
							  </div>
							  </div>
							
							</div>
						</div>
						<?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){
							$terms_condition_display= '';
						}else{
							$terms_condition_display= 'd-none';
						} ?>

						<div class="row terms_condition_row <?php echo $terms_condition_display ?>">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Terms & Conditions<em>*</em></label>
									<input type="text" class="form-control" name="terms_condition" id="terms_condition" value="<?php if($mode == 'Add') { echo set_value('terms_condition'); } else { echo $form_data[0]['terms_condition']; } ?>">
									<?php if(form_error('terms_condition')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('terms_condition'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group upload-btn-wrapper">
									<button type="button" class="btn btn-upload"> <em style="color:red;">*</em><strong> Resource Image</strong> </button>
									<input type="file" class="form-control" name="banner_img" id="banner_img" <?php if($mode == 'Add') { echo 'required'; } ?>>
									<p>Only .jpg, .jpeg, .png image formats below 100 kb are accepted</p>
								</div>		
								<?php if($banner_img_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $banner_img_error; ?></label> <?php } ?>
							</div>	
							
							<?php $dynamic_cls = $full_img = $onclick_fun = '';
								if($mode == 'Add')
								{
									$dynamic_cls = 'd-none';
								}
								else
								{
									$full_img = base_url().'uploads/resource_banner_img/'.$form_data[0]['banner_img'];
								} ?>
								
								<div class="col-sm-6 <?php echo $dynamic_cls; ?>" id="banner_img_outer">
									<div class="form-group">
										<div class="previous_img_outer">
											<img src="<?php echo $full_img; ?>">
											<!-- <a class="btn btn-danger btn-sm" onclick="<?php echo $onclick_fun; ?>" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
										</div>
									</div>
								</div>
						</div>

						<div class="row">
								<div class="col-md-12">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;"><strong>Tags</strong> <em>*</em> </label>
									<div class="boderBox65x">
										<select class="form-control select2_common" name="tags[]" id="tags" data-placeholder="Tags" multiple  onchange="show_hide_tag_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = array();
														if($mode == 'Add') { if(set_value('tags[]') != "") { $tags_arr = set_value('tags[]'); } }
													else { $tags_arr = explode(",",$form_data[0]['tags']); } ?>
													<option data-id='<?php echo $res['tag_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>

							<div class="col-md-12" id="tag_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Other Tag <em>*</em></label>
									<input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php if($mode == 'Add') { echo set_value('tag_other'); } else { echo $form_data[0]['tag_other']; } ?>">
									<?php if(form_error('tag_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
								</div>
							</div>
						</div>


						<div class="row"><div class="col-md-12"><h4 class="titleBox">Gallery Images</h4></div></div>
						<div class="file-details" style="min-height:100px;">
							<?php /* <div class="form-group"><label class="form-control-placeholder"></label></div><br> */ ?>
							<?php if($mode == 'Update' && count($r_files_data) > 0)
							{	
								echo '<div class="row">';
								foreach($r_files_data as $res)
								{	?>
									<div class="col-md-2" id="r_files_outer<?php echo $res['r_id']."_".$res['file_id']; ?>">
										<div class="file-list">
												<a href="javascript:void(0)" onclick="remove_rs_file('<?php echo $res['r_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close">X</a>
												<a class="file_ext_title" href="<?php echo base_url().'uploads/resource_files/'.$res['file_name']; ?>" target="_blank">
													<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/resource_files/'.$res['file_name']; }
													else { $disp_img_name = ''; } ?>
													
													<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
													else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
													<!--span><i class="fa fa-file"></i></span> View-->
												</a>
										</div>
									</div>
					<?php	}
								echo '</div>';
							}	?>
							
							<input type="hidden" name="customFileCount" id="customFileCount"  value="<?php if($mode=='Update'){echo count($r_files_data); }else{ echo 0;} ?>">
							<div class="row">											
								<div id="last_team_file_id"></div>
								<div class="col-md-2 custom-file" id="addFileBtnOuter">
									<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
									<input type="file" class="form-control fileUploadKr" name="r_files[]" id="file-upload" />
									<div class="r_files_err"></div>
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-label mb-0" style="font-size: 14px;font-weight: 500;"><strong>Location</strong> <em>*</em></label>
									<div class="boderBox65x">
										<select class="form-control select2_common" name="location" id="location" data-placeholder="Please select"  onchange="show_hide_location_other()">
											<option value="">Please select</option>
											<?php if(count($city_data) > 0)
												{	
													foreach($city_data as $res)
													{	$location='';
														if($mode == 'Add') { if(set_value('location') != "") { $location = set_value('location'); } }
													else { $location = $form_data[0]['location']; } ?>
													<option data-id='<?php echo $res['city_name'] ?>' value="<?php echo $res['city_id']; ?>" <?php if($res['city_id']==$location) { echo 'selected'; } ?> ><?php echo $res['city_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										<?php if($location_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $location_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" id="location_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Other Location <em>*</em></label>
									<input type="text" class="form-control" name="location_other" id="location_other" value="<?php if($mode == 'Add') { echo set_value('location_other'); } else { echo $form_data[0]['location_other']; } ?>">
									<?php if(form_error('location_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('location_other'); ?></label> <?php } ?>
								</div>
							</div>
						
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Web URL</label>
									<input type="text" class="form-control" name="web_url" id="web_url" value="<?php if($mode == 'Add') { echo set_value('web_url'); } else { echo $form_data[0]['web_url']; } ?>">
									<?php if(form_error('web_url')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('web_url'); ?></label> <?php } ?>
								</div>
							</div>
						</div>


						<div class="row">
						<div class="form-group col-md-4">
							
						    <label class="col-sm-12">Commercial Terms<em style="color: red">*</em></label>
						    <div class="form-check radio_check">
							  <input class="form-check-input" type="radio" name="commercial_terms" id="exampleRadios1" value="Free" <?php if($mode == 'Update' && $form_data[0]['commercial_terms']=="Free"){echo 'checked';} ?>>
							  <label class="form-check-label" for="exampleRadios1">
							   Free
							  </label>
							</div>
							<div class="form-check radio_check">
							  <input class="form-check-input" type="radio" name="commercial_terms" id="exampleRadios2" value="Paid" <?php if($mode == 'Update' && $form_data[0]['commercial_terms']=="Paid"){echo 'checked';} ?>>
							  <label class="form-check-label" for="exampleRadios2">
							    Paid 
							  </label>

							</div>

							<div class="form-check radio_check">
							  <input class="form-check-input" type="radio" name="commercial_terms" id="exampleRadios2" value="Contact for more details" <?php if($mode == 'Update' && $form_data[0]['commercial_terms']=="Contact for more details"){echo 'checked';} ?>>
							  <label class="form-check-label" for="exampleRadios2">
							    Contact for more details
							  </label>
							</div>

						</div>
						<div class="row ml-2" ><span id="commercial_terms_err"></span></div>

						<?php 
						if($form_data[0]['commercial_terms']=='Paid'){
							$class='';
							$required='required';
						}else{
							$class='d-none';
							$required='';
						}
						?>
						<div class="form-group col-md-2 <?php echo $class ?> currency_div">
									
									<select name="currency" id="currency" class="form-control valid" aria-invalid="false">
									<?php foreach ($currency as $key => $value) { ?>
										<option value="<?php echo $value['c_id'] ?>" <?php if($value['c_id']==1 && $mode=='Add') {echo 'selected';}elseif($value['c_id']==$form_data[0]['currency']){echo 'selected';} ?>><?php echo $value['currency'] ?></option>
									<?php } ?>	
									</select>
									<label class="form-control-placeholder floatinglabel">Currency </label>

									
								</div>
							<div class="form-group <?php echo $class ?> col-md-4 currency_div">
							<input type="text" class="form-control allowd_only_float" name="cost_price" id="cost_price" value="<?php if($mode == 'Add') { echo set_value('cost_price'); } else { echo $form_data[0]['cost_price']; } ?>" <?php echo $required ?>>
							<label class="form-control-placeholder floatinglabel">Price </label>
							</div>
						
						</div>
						

				
						
						<div class="row">
							
							
							<div class="">
								<button type="submit" class="btn btn-primary add_button"><?php if($mode=='Update') {echo 'Update';}else{echo 'Submit';} ?></button> 
							</div>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>


<script type="text/javascript">	
	// CKEDITOR.replace('blog_description');


	$('input[type=radio][name=commercial_terms]').change(function() {
	    if (this.value == 'Paid') {
	       $(".currency_div").removeClass('d-none');
			$("#currency").prop("required", true);
			$("#cost_price").prop("required", true);
	        
	    }
	    else {
	        $(".currency_div").addClass('d-none');
	        $("#currency").prop("required", false);
			$("#cost_price").prop("required", false);
			$("#cost_price").val("");

	    }
	});

	$("#other_details_check").change(function () {

	    if ($('input.other_details_check').is(':checked')) {
	    	$(".other_details_row").removeClass('d-none');
	    }else{
	    	$("#other_details").val("");
	    	$(".other_details_row").addClass('d-none');
	    }

	});

	$("#terms_condition_check").change(function () {

	    if ($('input.terms_condition_check').is(':checked')) {
	    	$(".terms_condition_row").removeClass('d-none');
	    }else{
	    	$("#terms_condition").val("");
	    	$(".terms_condition_row").addClass('d-none');
	    }

	});

	


	
	
	
	
	function enable_disable_inputs(checkbox_id, input_id)
	{
		if($("#"+checkbox_id).prop("checked") == true)
		{
			$("#"+input_id+"_outer").css("display", 'block');
			$("#"+input_id).prop("disabled", false);
			$("#"+input_id).prop("readonly", false);
			$("#"+input_id).prop("required", true);
			$("#"+input_id).focus();
			
			if(input_id == 'author_professional_status' || input_id == 'author_org_name')
			{
				//$("#preloader").css("display", "block");
				parameters = { 'checkbox_id':checkbox_id, 'input_id':input_id, 'csrf_test_name':$('.token').val() }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						if(data.flag == "success")
						{ 
							$(".token").val(data.token);
							if(input_id == 'author_professional_status') { $("#author_professional_status").val(data.professional_status); }
							if(input_id == 'author_org_name') { $("#author_org_name").val(data.org_name); }						
							$("#preloader").css("display", "none");
						}
						else 
						{	}
					}
				});
			}
		}
		else if($("#"+checkbox_id).prop("checked") == false)
		{
			$("#"+input_id+"_outer").css("display", 'none');
			$("#"+input_id).prop("disabled", true);
			$("#"+input_id).prop("readonly", true);
			$("#"+input_id).prop("required", false);
			//$("#"+input_id).val("");
		}
	}
	
	function get_author_name()
	{
		//$("#preloader").css("display", "block");
		parameters = { 'input_id':'author_name', 'csrf_test_name':$('.token').val() }
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{ 
					$(".token").val(data.token);
					$("#author_name").val(data.author_name);					
					$("#preloader").css("display", "none");
				}
				else 
				{	}
			}
		});
	}
	
	function confirm_clear_all()
	{
		swal(
		{  
			title:"Confirm?",
			text: "This action will clear the complete form. Please confirm if you want to continue?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) { if (result.value) { $("#preloader-loader").css("display", "block"); location.reload(); } });
	}
</script>

<?php if($mode == 'Add' && set_value('author_name') == '') { ?>
	<script>get_author_name();</script>
<?php } ?>

<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<script type="text/javascript">
	//******* SWEET ALERT POP UP *********
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod("valid_files_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".pdf",".xls",".doc",".csv",".xlsx",".pptx",".ppt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');

	 	$.validator.addMethod("valid_email", function(value, element) 
	    { 
	      var email = value;
	      var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	      var result = pattern.test(email); 
	      if(result){return true;}else {return false;}
	    });

	 	$.validator.addMethod('NotAllZero', function(value) {
	      return value.match(/^(?!0*$).*$/);
	    }, 'Invalid Mobile Number');
		
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 100 KB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		
		//******* JQUERY VALIDATION *********
		$("#ResourceForm").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				resouce_name: { required: true, nowhitespace: true, valid_value: true },
				resource_details: { required: true, nowhitespace: true, valid_value: true },
				resource_specification: { required: true, nowhitespace: true, valid_value: true },
				owner_name: { required: true, nowhitespace: true, valid_value: true },
				// owner_email: { required: true, nowhitespace: true, valid_value: true },
				// owner_contact_number: { required: true, nowhitespace: true, valid_value: true },
				location: { required: true, nowhitespace: true, valid_value: true },
				commercial_terms: { required: true, nowhitespace: true, valid_value: true },
				other_details: {
	               required: function() {
	               return $("input[name=other_details_check]:checked").val() == 1; 
	               }
               
              	},
              	terms_condition: {
	               required: function() {
	               return $("input[name=terms_condition_check]:checked").val() == 1; 
	               }
               
              	},

	          	owner_email: {
	              valid_email:true,
	              required: true,
	               normalizer: function(value) {
	                    return $.trim($("#owner_email").val());
	                },
	               email: true,
	             },

               owner_contact_number: {
                  required: true,
                  normalizer: function(value) {
                    return $.trim($("#owner_contact_number").val());
                  },
                  number: function() {
                    return true;  
                  },
                  minlength: function() {
                    return 10;  
                  },
                  maxlength: function() {
                    return 10;  
                  },
                  NotAllZero:true,
              },

				// kr_description: 
				// {
				// 	required: true,
				// 	minCount:['5'],
				// 	maxCount:['400']
				// },
				banner_img: { <?php if($mode == 'Add') { ?>required: true,<?php } ?> valid_img_format: true,filesize:100000},
				"tags[]": { required: true},
				// accept_terms: { required: true},
				location: { required: true},
				web_url:{  url: true},
				cost_price: { number:true, min:0 }
				
				/* end_time: { required: true, chk_valid_time:true },
					cost_type: { required: true},
					cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
					"webinar_technology[]": { required: true },
					exclusive_technovuus_event:{required: true },
					registration_link:{required: true,valid_url:true },
					hosting_link:{required: true,valid_url:true },
					breif_desc:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },		
					key_points:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] },
				about_author:{required: true, nowhitespace: true, minCount:['5'], maxCount:['200'] }, */
			},
			messages:
			{
				title_of_the_content: { required: "This field is required", nowhitespace: "Please enter the title" },
				kr_description: { required: "This field is required", nowhitespace: "Please enter the description" },				
				banner_img:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				"r_files[]":{required: "This field is required"},
				author_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				accept_terms:{required: "This field is required"},
				"r_files[]":{required:"This field is required"},

				 owner_email: {
	               valid_email:"Please enter valid email address."
	             },
	         	owner_contact_number: {
	               required: "This field is required",
	             },
				web_url:{url: "Please enter valid url"}
				/* start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}		 */					
			},
			
			errorElement: 'span',
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "accept_terms") 
				{
					error.insertAfter("#accept_terms_err");
				}else if(element.attr("name") == "commercial_terms"){
					error.insertAfter("#commercial_terms_err");
				}
				else if (element.attr("name") == "r_files[]"){
					error.insertAfter(".r_files_err");
					
				}
				else
				{
					element.closest('.form-group').append(error);
				}
			}
		});
	});
</script>

<script type="text/javascript">
	//DISPLAY IMAGE PREVIEW WHEN BROWSE
	function BlogImagePreview(input) 
	{
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			var file_name = input.files[0].name;
			var extension = file_name.substr( (file_name.lastIndexOf('.') +1) ).toLowerCase();
			if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
			{
				var reader = new FileReader();
				reader.onload = function(e) 
				{
					$("#banner_img_outer").removeClass('d-none');
					//$("#banner_img_outer .bannerAdd").css("background-image", "url(" + e.target.result + ")");
					$("#banner_img_outer .previous_img_outer img").attr("src", e.target.result);
					//$("#banner_img_outer .previous_img_outer a.btn").attr("onclick", "remove_banner_img(0)");
				}				
				reader.readAsDataURL(input.files[0]); // convert to base64 string			
			}
			else
			{
				$("#banner_img_outer").addClass('d-none');
			}
			$("#preloader").css("display", "none");
		}
	}
	$("#banner_img").change(function() { BlogImagePreview(this); });	



	//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function FilesPreview(input, public_private_flag) 
	{
		
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			var upload_file_size = input.files[0].size; //1000000 Bytes = 1 Mb 
			if(upload_file_size < 5000000)
			{
				var disp_img_name = "";
				var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
				if(extension == 'png' || extension == 'jpeg' || extension == 'jpg'   )
				{
				disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
					
				var id_name = '';
				var btn_id_name = '';
				var input_cls_name = '';
				var input_type_name = '';
				var fun_var_parameter = "";
				var file_upload_common = '';
				if(public_private_flag == 'public')
				{
					id_name = 'r_files_outer'+j;
					btn_id_name = 'addFileBtnOuter';
					input_cls_name = 'fileUploadKr';
					input_type_name = 'r_files[]';
					fun_var_parameter = "'public'";
					file_upload_common = 'file-upload';
				}
			
				
				var append_str = '';
				append_str += '	<div class="col-md-2 r_files_outer_common" id="'+id_name+'">';
				append_str += '		<div class="file-list">';
				append_str += '			<a href="javascript:void(0)" onclick="remove_rs_file('+j+', 0, '+fun_var_parameter+')" class="file-close"><i class="fa fa-remove"></i></a>';
				//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
				append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
				append_str += '		</div>';
				append_str += '	</div>';				
					
				var btn_str = '';
				btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
				btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
				btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="FilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
				btn_str +=	'	</div> <div class="r_files_err"></div';
					
				if(public_private_flag == 'public')
				{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
					
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
				}
				}else{
					swal( 'Error!','Invalid File Extension','error');
				}
			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 5 MB','error');
			}
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadKr").change(function() { FilesPreview(this,'public'); });	



	//REMOVE TEAM FILE PREVIEW / FILES
	function remove_rs_file(div_id, file_id, public_private_flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				var customFileCount = $("#customFileCount").val();
					$("#customFileCount").val(parseInt(customFileCount)-1);
				
				if(file_id!= '' && file_id != '0')
				{
					
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("resource_sharing/delete_rs_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				
				if(public_private_flag == 'public')
				{
				$("#r_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				
				
				$("#preloader").css("display", "none");
			}
		});
	}
	
</script>