<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Banner Management</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Banner Management</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
              <div class="card-header">
              <h3 class="card-title">
                  Banner Management
              </h3>
                 <a href="<?php echo base_url('xAdmin/banner/add') ?>" class="btn btn-primary btn-sm float-right">Add Banner Management</a>
              
              </div>

         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>Upload File</th>
                     <th>Banner Title</th>
					 <th>Banner Sub Title</th>
					<!-- <th>Banner Description</th>-->
					 <th>Banner URL</th>
					 <th>Banner Image</th>
					 <th>xOrder</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;
                     foreach($records as $banners_detail) 
                     { ?>
                  <tr>
                     <td style="width:5%"><?php echo $i; ?></td>
					  <td style="width:30%"><?php echo $banners_detail['upload_type'] ; ?></td>
                     <td style="width:30%"><?php echo $banners_detail['banner_name'] ; ?></td>
					 <td style="width:30%"><?php echo $banners_detail['banner_sub'] ; ?></td>
					 <!--<td style="width:30%"><?php echo $banners_detail['banner_desc'] ; ?></td>-->
					 <td style="width:30%"><?php echo $banners_detail['banner_url'] ; ?></td>
						
					 <td style="width:45%">
						<?php 
						if($banners_detail['upload_type'] !="Video"){
						?>
						<img src="<?php echo base_url('assets/banner/'.$banners_detail['banner_img']); ?>" height="100" width="200" />
						<?php } else { 
						$filename = $banners_detail['banner_img'];
						$ext = pathinfo($filename, PATHINFO_EXTENSION);
						?>						
						 <video width="100%" autoplay="" loop="" muted="" id="myvideo">
							<source src="<?php echo base_url('assets/banner/'.$banners_detail['banner_img']); ?>" type="video/<?php echo $ext; ?>">
						</video>
						
						<?php } ?>
						
					 </td> 
					<td style="width:10%"><?php echo $banners_detail['xOrder'] ; ?></td>	
                     <td style="width:20%">
						
                        <a href="<?php echo base_url();?>xAdmin/banner/edit/<?php echo $banners_detail['id'];  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
                        <!--<a href="<?php echo base_url();?>xAdmin/usercategories/delete/<?php echo $banners_detail['id'];  ?>" class="con_delete"><button class="btn btn-danger btn-sm">Delete</button></a>-->
						
						<?php      
						if($banners_detail['status']=="Active"){
							$status = 'Block';
						?>
                        <a href="<?php echo base_url(); ?>xAdmin/banner/changeStatus/<?php echo $banners_detail['id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
                           else if($banners_detail['status']=="Block"){
							$status = 'Active';
						   ?>
                        <a href="<?php echo base_url(); ?>xAdmin/banner/changeStatus/<?php echo $banners_detail['id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Block</button> </a><?php }
                           ?>
                     </td>
                  </tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready( function () {
		 $("#example1").DataTable({
		  "responsive": true,
		  "autoWidth": false,
		});
		
		
	
    });
</script>