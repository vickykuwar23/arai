
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Services</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Services</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Service
			  </h3>
				  <a href="<?php echo base_url('xAdmin/services') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="frm" name="frm" role="form" enctype="multipart/form-data">
						  <div class="row">
  							<div class="col-md-6">
  							  <div class="form-group">
  								  <label for="exampleInputEmail1">Service Title</label>
  								  <input type="text" class="form-control" id="service_title" placeholder="Enter Service Title" name="service_title" maxlength="50">
  								  <span><?php echo form_error('service_title'); ?></span>
  							  </div>
								</div>                
							  </div>
							  <div class="row">
								<div class="col-md-6">
								  <div class="form-group">
									<label for="exampleInputEmail1">Service Description</label>
									<textarea id="service_desc" name="service_desc" placeholder="Please Enter Service Description" class="form-control"></textarea>
									<span><?php echo form_error('service_desc'); ?></span>
								  </div>
								</div>
							  </div>
							  <div class="row">
								<div class="col-md-6">
								  <div class="form-group">
									<label for="exampleInputEmail1">Service Icon</label>
									<input type="file" class="form-control" id="service_icon" name="service_icon" maxlength="250">
									<span><?php echo form_error('service_icon'); ?></span>
								  </div>  
								</div>
							  </div>
								<div class="row">
									<div class="col-md-6">
									  <div class="form-group">
										  <label for="exampleInputEmail1">Service URL</label>
										  <input type="text" class="form-control" id="service_url" placeholder="Enter Service URL" name="service_url">
										  <span><?php echo form_error('service_url'); ?></span>
									  </div>
									</div>                
								  </div>
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							
								<!-- /.card-body -->

						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#frm').validate({
    rules: {
      service_title: {
        required: true,
		    minlength:3
      },
      service_desc: {
        required: true,
      },
      service_icon: {
        required: true,
      }, 
	  service_url: {
        required: true,
		url:true
      } 
    },
    messages: {
      service_title: {
        required: "Please Enter Service Title",
		    minlength: "Enter Service Title Name must be at least {0} characters long"
      },
      service_desc: {
        required: "Please Enter Service Description"
      },
      service_icon: {
        required: "Please Enter Service Icon"
      },
	  service_url: {
        required: "Please Enter Service URL"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


