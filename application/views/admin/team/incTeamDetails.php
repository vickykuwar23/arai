<style>
	ul.file_attachment_outer_common {
	list-style: none;
	margin: 0;
	padding: 0;
	}
	
	ul.file_attachment_outer_common li:before { display:none; }
	
	ul.file_attachment_outer_common li {
	padding: 3px;
	margin: 0 5px 10px 0;
	width: 80px;
	height: 60px;
	border: 1px solid #ccc;
	border-radius: 5px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
	}
	
	ul.file_attachment_outer_common li a h4 {
	margin: 0;
	line-height: 55px;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	font-size: 15px !important;
	}
	
	ul.file_attachment_outer_common li a img
	{
	max-width:100%;
	max-height:100%;
	}

	input[type="checkbox"][readonly] {
	  pointer-events: none;
	}
</style>
							<div class="col-md-12">
								<p ><strong>Public Files & Attachments :</strong></p>
<?php if(count($team_files_public) > 0) { ?>							
								<ul class="file_attachment_outer_common">
									<?php 
										foreach ($team_files_public as $key => $file) 
										{ ?>
										<li>
											<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
												<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
												else { $disp_img_name = ''; } ?>
												
												<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
												else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
											</a>
										</li>
									<?php } ?>
								</ul>
						<?php } ?>
								<br>
							</div>
							<div class="col-md-12">
								<p ><strong>Private Files & Attachments :</strong></p>
						<?php
						if(count($team_files_private) > 0) { ?>							
								<ul class="file_attachment_outer_common">
									<?php 
										foreach ($team_files_private as $key => $file) 
										{ ?>
										<li>
											<a href="<?php echo base_url('uploads/byt/').$file['file_name'] ?>" target='_blank'>
												<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
													if(in_array(strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/byt/'.$file['file_name']; }
												else { $disp_img_name = ''; } ?>
												
												<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
												else { echo "<h4>".strtolower(pathinfo($file['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
											</a>
										</li>
									<?php } ?>
								</ul>
								<?php } ?> 
								<br>
							</div>
							
							<div class="col-md-12">
								<strong style="display: block;">Share Files</strong>
								<label class="switch">
									<input type="checkbox" id="share_files" <?php if($team_details[0]['share_files']=='yes'){echo 'checked';} ?> onclick="return false"> 
									<span class="slider round"></span>
								</label>
							</div>
						