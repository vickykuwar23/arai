<style>
/* Prelaoder */
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}
#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
@-webkit-keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 60vh;
    overflow-y: auto;
}
</style>

<script>
// Preloader
$(window).on('load', function() {
    if ($('#preloader-loader').length) {
        $('#preloader-loader').delay(50).fadeOut('slow', function() {
            /* $(this).remove(); */
        });
    }
});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Team Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Team Listing</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
				<div class="card-header">
					
					<!--<form method="post" name="frm-exp" id="frm-exp">
					<div class="row">
						 <div class="col-6">
							 <div class="form-group">
								<input type="text" class="seach-key form-control" id="keyword" name="keyword" placeholder="Search By Challenge Code, Challenge Name, Owner Name, Company Name">
							 </div>
						 </div>
						 <div class="col-6">					
								<input type="submit" class="btn btn-primary btn-sm pull-right float-right" id="export" name="export" value="Export To Excel">					
						 </div>
					</div>
				</form>-->
				</div>
		<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover team-table">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>ID</th>
					 <th>Team Name</th>
					 <th>Challenge ID</th>	
                     <th>Challenge Name</th>
					 <!-- <th>Team Size</th> -->
					 <th>Team Status</th>
					 <th>Challenge Owner ApplicationStatus</th>
					 <th>Added On</th>
                     <th>Action</th>
                  </tr>
               </thead>
				<tbody>    
				            
                </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Team Details</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body" id="contents">
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script>


 $(document).ready( function () {		
		
	 $('#frm-exp').keypress(function (e) {
		if (e.which === 13) {		
			e.preventDefault(); 
		 }
	 });
	
	var base_path = '<?php echo base_url(); ?>';
	var teamDataTable = $('#example1').DataTable({
		"responsive": true,
		"autoWidth": false,
		"serverMethod": 'post',			
		"ordering":true,
		"searching": false,
		 "bStateSave": true,
		"language": {
						"zeroRecords":"No matching records found.",
						"infoFiltered":""
					},
		"processing":false, //Feature control the processing indicator.
		"serverSide":true, //Feature control DataTables' server-side processing mode.		
		 
		// Load data for the table's content from an Ajax source
		"ajax": {
		"url": base_path+"xAdmin/team/get_team_data",
		"type":"POST",
		"data":function(data) {			
			data.keyword 	= $("#keyword").val();
			
			/*data.c_type 	= $("#c_type").val();
			data.com_code 	= $("#com_code").val();
			data.username 	= $("#username").val();
			data.district_id = $("#district_id").val();
			data.c_subtype 	 = $("#c_subtype").val();
			data.sro_office  = $("#sro_office").val();*/
			
			},
		"error":function(x, status, error) {
			
		},
		"statusCode": {
		401:function(responseObject, textStatus, jqXHR) {			
					},
				},
		}
		
	}); // Datatable End
	
	// Get Search Filter Values
	/*$('.searchVal').change(function(){			
		userDataTable.ajax.reload();
	});*/
	
	$('.seach-key').keyup(function(){ console.log();			
		teamDataTable.ajax.reload();	
	});
	
	$("body").on("click", "#example1 tbody tr .view-more", function (e) { 			
			
		$('#exampleModal').modal('show');			
		var tid	=	$(this).attr('data-id');
		var base_url = '<?php echo base_url('xAdmin/team/viewDetails'); ?>';
		 $.ajax({
			url: base_url,
			type: "post",
			data: {id:tid},				
			success: function (response) {					
				$("#contents").html(response);				
			},
			error: function(jqXHR, textStatus, errorThrown) {
			   console.log(textStatus, errorThrown);
			}
		});
		
	});	
	
	
	$("body").on("change", "#example1 tbody tr .up-status", function (e) { 			
		var teamStatus = $(this).val();			
		var tid	=	$(this).attr('data-id');		
		var csrf_test_name	= $('.token').val();			
		var base_url = '<?php echo base_url('xAdmin/team/updateTeamStatus'); ?>';
		$('#preloader-loader').css('display', 'block');	
		 $.ajax({
			url: base_url,
			type: "post",
			data: {id:tid,csrf_test_name:csrf_test_name,tstatus:teamStatus},				
			success: function (response) {
				$('#preloader-loader').css('display', 'none');	
				var returndata = JSON.parse(response);
				var token = returndata.token;
				$(".token").val(token); 	
				swal({
						title: 'Success!',
						text: "Team Status updated successfully.",
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});	
			},
			error: function(jqXHR, textStatus, errorThrown) {
			   console.log(textStatus, errorThrown);
			}
		});
	});
	
});
function teamDelete(team_id){
	//var $tr = $(this).closest('tr');
	//var teamDataTable = $('#example1').DataTable({});
	//console.log($tr);
	swal(
		{
			title:"Confirmation:",
			text: "Are you sure?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';				
				var cs_t = 	$('.token').val();
				//alert(team_id);return false;
				 $.ajax({
					type:'POST',
					url: base_url+'xAdmin/team/deleteTeam',
					data:'team_id='+team_id+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);								
						$(".token").val(output.token); 	
						var textAlert = output.message;
						$('#example1').DataTable().ajax.reload();
						swal({
								title: 'Success!',
								text: textAlert,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						
					}
				});	// Ajax End
				
			}
		
		});
	
	
} // Delete Function
</script>