
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Subcategory</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Subcategory</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add User subcategory
			  </h3>
				  <a href="<?php echo base_url('xAdmin/front_registration_usersubcategory') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="frm" name="frm" role="form" >
						  <div class="row">
							<div class="col-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">User Category</label>
                  <select name="usercategory_name" id="usercategory_name" class="form-control" >
                    <option value="">-- Select User Category --</option>
                    <?php 
                    if(count($usercategory_data) > 0){
                      foreach($usercategory_data as $catdetails){
                      ?>
                      <option value="<?php echo $catdetails['id'] ?>"><?php echo ucfirst($catdetails['usercategory_name']) ?></option> 
                      <?php 
                      } // For End
                    } // If End
                    ?>
                  </select>               
                  <span><?php echo form_error('usercategory_name'); ?></span>
                </div>					   
							  <div class="form-group">
								<label for="exampleInputEmail1">User Subcategory Name</label>
								<input type="text" class="form-control" id="usersubcategory_name" placeholder="Enter User Subcategory Name" name="usersubcategory_name" maxlength="200">
								<span><?php echo form_error('usersubcategory_name'); ?></span>
							  </div>						  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#frm').validate({
    rules: {
      usersubcategory_name: {
        required: true,
		    minlength:3
      },
      usercategory_name: {
        required: true,
      },
    },
    messages: {
      usersubcategory_name: {
        required: "Please Enter Subcategory Name",
		    minlength: "Enter Usersubcategory Name must be at least {0} characters long"
      },
      usercategory_name: {
        required: "Please Select User Category"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


