<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/') ?>css/bootstrap-datepicker.css"> */ ?>
<?php /* <link rel="stylesheet" href="<?php echo base_url('assets/front/css/') ?>jquery.datetimepicker.min.css"> */ ?>
<?php /* <script src="<?php echo base_url('assets/front/js/') ?>jquery.datetimepicker.js"></script> */ ?>

<link rel="stylesheet" href="<?php echo base_url('assets/front/datepicker/') ?>datepicker3.css">
<script src="<?php echo base_url('assets/front/') ?>datepicker/bootstrap-datepicker.js"></script>

<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Webinar</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Webinar</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Webinar
					</h3>
				  <a href="<?php echo base_url('xAdmin/webinar') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div> 
				<!-- form start -->
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="post" id="webinarform" name="webinarform" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="exampleInputEmail1">Webinar ID <em style="color: red;">*</em></label>
									<input type="text" class="form-control" name="webinar_id" id="webinar_id" value="<?php echo $webinar_data[0]['webinar_id']; ?>"  readonly >
								</div>		
								<div class="form-group">
									<label for="exampleInputEmail1">Webinar's Name <em style="color: red;">*</em></label>
									<input type="text" class="form-control" id="webinar_name" placeholder="" name="webinar_name" value="<?php echo $webinar_data[0]['webinar_name']; ?>">								
									<span><?php echo form_error('webinar_name'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Start Date <em style="color: red;">*</em></label>
									<input type="date" class="form-control" name="start_date" id="start_date" 
									value="<?php echo $webinar_data[0]['webinar_date']; ?>"  >
									<span><?php echo form_error('start_date'); ?></span>
								</div>
								
							  <div class="form-group">
									<label for="start_time">Start Time <em style="color: red;">*</em></label>
									<input type="text" class="form-control clockpicker" name="start_time" id="start_time" value="<?php echo date("h:iA", strtotime($webinar_data[0]['webinar_start_time'])); ?>" readonly>
									<span><?php echo form_error('start_time'); ?></span>
								</div>
								
							  <div class="form-group">
									<label for="start_time">End Time <em style="color: red;">*</em></label>
									<input type="text" class="form-control clockpicker" name="end_time" id="end_time" value="<?php echo date("h:iA", strtotime($webinar_data[0]['webinar_end_time'])); ?>" readonly>
									<span><?php echo form_error('end_time'); ?></span>
								</div>
								
								<div class="form-group">
									<label>Type of Registration <em style="color: red;">*</em></label>
									<div class="d-block">
										<div class="form-check form-check-inline">
											<input type="radio" id="upload_type1" name="cost_type" class="form-check-input get-type" value="FREE" onchange="show_hide_div()" 
											<?php if($webinar_data[0]['webinar_cost_type'] == 'FREE'){ ?> checked="checked" <?php } ?>>
											<label class="form-check-label" for="techNovuus1">FREE</label>
										</div>
										<div class="form-check form-check-inline">
											<input type="radio" id="upload_type2" name="cost_type" class="form-check-input get-type" value="PAID" onchange="show_hide_div()" 
											<?php if($webinar_data[0]['webinar_cost_type'] == 'PAID'){ ?> checked="checked" <?php } ?>>
											<label class="form-check-label" for="techNovuus1">PAID</label>
										</div>
									</div>									
								</div>
								
								<div class="row" id="paid-amt">
									<div class="col-md-2">
										<div class="form-group">
											<label class="">Currency <em style="color: red;">*</em></label>
											<select name="currency" id="currency" class="form-control">
												<?php /*<<option value="USD" disabled>$</option>
													<option value="EUR" disabled>€</option>
													<option value="CRC" disabled>₡</option>
													<option value="GBP" disabled>£</option>
													<option value="ILS" disabled>₪</option>
												<option value="JPY" disabled>¥</option> */ ?>
												<option value="INR (₹)">INR (₹)</option>
											</select>
										</div>									
									</div>
									<div class="col-md-10">
										<div class="form-group">
											<label class="exampleInputEmail1">Price <em style="color: red;">*</em></label>
											<input type="text" class="form-control allowd_only_float" name="cost_price" id="cost_price" value="<?php echo $webinar_data[0]['cost_price']; ?>"  >
											
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label for="exampleInputEmail1">Webinar Banner (Upload only jpg, jpeg ,gif & png format only)</label>
									<input type="file" class="form-control" id="banner_img" placeholder="" name="banner_img" >
								</div>
								<div>
									<?php if($webinar_data[0]['banner_img']!=""){ ?> 
										<img src="<?php echo base_url('assets/webinar/'.$webinar_data[0]['banner_img']); ?>" height="100px" width="200px" />
									<?php } ?>
								</div><br>
								
								<div class="form-group">
									<label class="form-label">Webinar Technology <em>*</em></label>
									<div class="boderBox65x">
										<select data-key="" class="choose-tech select2" name="webinar_technology[]" id="webinar_technology" data-placeholder="Webinar Technology" multiple onchange="show_hide_webinar_technology_other()" style="width: 100%;">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
													if($webinar_data[0]['webinar_technology'] != "") { $webinar_technology_arr = explode(",",$webinar_data[0]['webinar_technology']); } else { $webinar_technology_arr = array(); } ?>
													<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$webinar_technology_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
													<?php }	
												} ?>
										</select> 																	
									</div>
								</div>
								
								<div class="form-group" id="webinar_technology_other_outer" <?php if($webinar_data[0]['webinar_technology_other'] == "") { ?> style="display:none;"<?php } ?>>
									<label class="form-label">Webinar Technology Other <em>*</em></label>
									<input type="text" class="form-control" name="webinar_technology_other" id="webinar_technology_other" value="<?php echo $webinar_data[0]['webinar_technology_other']; ?>" required>
								</div>
								
								<div class="form-group" style="margin-bottom: 15px;margin-top: 10px;">		
									<label class="form-label">Exclusive Technovuus Event <em>*</em></label>
									<div>
										<div class="form-check form-check-inline">
											<input type="radio" id="exclusive_technovuus_event1" name="exclusive_technovuus_event" class="form-check-input get-type" value="Yes" onchange="show_hide_reg_link()" <?php if($webinar_data[0]['exclusive_technovuus_event'] == 'Yes'){?> checked="checked"  <?php } ?>>
											<label class="form-check-label" for="exclusive_technovuus_event1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
											<input type="radio" id="exclusive_technovuus_event2" name="exclusive_technovuus_event" class="form-check-input get-type" value="No" onchange="show_hide_reg_link()" <?php if($webinar_data[0]['exclusive_technovuus_event'] == 'No'){?> checked="checked"  <?php } ?>>
											<label class="form-check-label" for="exclusive_technovuus_event2">No</label>
										</div>
									</div>
									<div id="exclusive_technovuus_event_err"></div>
								</div>
								
								<div class="form-group" id="reg_link_outer">
									<label class="form-label">Registration Link <em style="color: red;">*</em></label>
									<textarea class="form-control" name="registration_link" id="registration_link" style='word-break: break-all; min-height: 95px;' placeholder="Enter Registration Link " ><?php echo $webinar_data[0]['registration_link']; ?></textarea>
									<span><?php echo form_error('registration_link'); ?></span>
								</div>
								
								<div class="form-group">
									<label for="exampleInputEmail1">Hosting Link <em style="color: red;">*</em></label>									
									<textarea class="form-control" name="hosting_link" id="hosting_link" style='word-break: break-all; min-height: 95px;' placeholder="Enter Hosting Link " ><?php echo ($webinar_data[0]['hosting_link']); ?></textarea>
									<span><?php echo form_error('hosting_link'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Brief About Webinar <em style="color: red;">*</em></label>
									<textarea class="form-control" id="breif_desc" placeholder="Enter Brief About Webinar " name="breif_desc" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_breif_info']; ?></textarea>
									<span><?php echo form_error('breif_desc'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Key Takeaway Points <em style="color: red;">*</em></label>
									<textarea class="form-control" id="key_points" placeholder="Enter Key Takeaway Points " name="key_points" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_key_points']; ?></textarea>
									<span><?php echo form_error('key_points'); ?></span>
								</div>	
								<div class="form-group">
									<label for="exampleInputEmail1">About Author <em style="color: red;">*</em></label>
									<textarea class="form-control" id="about_author" placeholder="Enter About Author " name="about_author" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_about_author']; ?></textarea>
									<span><?php echo form_error('about_author'); ?></span>
								</div>		
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/front/clockpicker/bootstrap-clockpicker.min.js'); ?>"></script>
<script>	
	$('.clockpicker').clockpicker(
	{
    /* placement: 'top',
		autoclose:true
    align: 'left',*/
		twelvehour: true,
		donetext: 'Done'
	});
</script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">	
	/* $('.datetimepicker1').datetimepicker({
		datepicker:false,
		formatTime:"h:i a",
		format:"h:i a",
		step:15
	}); */
	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	function show_hide_div()
	{
		
		$('#paid-amt').hide();
		
		var values = $("input[name='cost_type']:checked").val();
		if(values == "PAID")
		{
			$('#paid-amt').show();
		}
		else 
		{			
			$('#paid-amt').hide();
			$("#cost_price").val(null);			
		}
	}
	show_hide_div();
	
	function show_hide_reg_link()
	{		
		$('#reg_link_outer').hide();
		
		var values = $("input[name='exclusive_technovuus_event']:checked").val();
		if(values == "No")
		{
			$('#reg_link_outer').show();
		}
		else 
		{			
			$('#reg_link_outer').hide();
			$("#reg_link_outer").val("");			
		}
	}
	show_hide_reg_link();
	
	
	function getWordCount(wordString) 
	{
		var words = wordString.split(" ");
		words = words.filter(function(words) { return words.length > 0 }).length;
		return words;
	}
	
	
	function Converttimeformat(time) 
	{
		// var time = $("#starttime").val();
		//var time = document.getElementById('txttime').value;
		var hrs = Number(time.match(/^(\d+)/)[1]);
		var mnts = Number(time.match(/:(\d+)/)[1]);
		var format = time.match(/\s(.*)$/)[1];
		if (format == "PM" && hrs < 12) hrs = hrs + 12;
		if (format == "AM" && hrs == 12) hrs = hrs - 12;
		var hours = hrs.toString();
		var minutes = mnts.toString();
		if (hrs < 10) hours = "0" + hours;
		if (mnts < 10) minutes = "0" + minutes;
		return (hours + ":" + minutes);
	}
	
	$(document).ready(function () 
	{
		$.validator.setDefaults({
			submitHandler: function () {
				//alert( "Form successful submitted!" );
				form.submit();
			}
		});
		
		$.validator.addMethod("valid_img_format", function(value, element)
		{
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif", ".jfif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});
		
		$.validator.addMethod('filesize', function (value, element, param) {
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		jQuery.validator.addMethod("ckrequired", function (value, element) {  
			var idname = $(element).attr('id');  
			var editor = CKEDITOR.instances[idname];  
			var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
			if (ckValue.length === 0) {  
				//if empty or trimmed value then remove extra spacing to current control  
				$(element).val(ckValue);  
				} else {  
				//If not empty then leave the value as it is  
				$(element).val(editor.getData());  
			}  
			return $(element).val().length > 0;  
		}, "This field is required"); 
		
		function GetTextFromHtml(html) {
			var dv = document.createElement("DIV");
			dv.innerHTML = html;
			return dv.textContent || dv.innerText || "";
		}
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
		
		$.validator.addMethod("valid_url", function(value, element) {
			return this.optional(element) || /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i.test(value);
		}, "Please enter valid url");
		
		$.validator.addMethod("chk_valid_time", function(value, element)
		{
			if($.trim(value).length == 0)
			{
				return true;
			}
			else
			{
				var startTime = $.trim($("#start_time").val());
				var endTime = $.trim(value);
				
				if(startTime.length == 0)
				{
					return true;
				}
				else
				{
					startTime = startTime.replace("AM", " AM");
					startTime = startTime.replace("PM", " PM");
					startTime = Converttimeformat(startTime);
					
					endTime = endTime.replace("AM", " AM");
					endTime = endTime.replace("PM", " PM");
					endTime = Converttimeformat(endTime);
					
					//console.log(startTime+" >> "+endTime);
					if(startTime > endTime) { return false; }
					else { return true; }
					/* return false; */
				}
			}
		}, 'Please enter valid time');
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		$('#webinarform').validate({
			rules: {
				webinar_name: {
					required: true,
					valid_value: true,
					maxCount:['50']
				},
				start_date: { required: true},
				start_time: { required: true},
				end_time: { required: true, chk_valid_time:true },
				cost_type: { required: true},
				cost_price: { required: function(){return $("#upload_type2").val() == 'PAID'; }, number:true, min:0 },	
				registration_link: { required: true, valid_url:true},
				hosting_link: { required: true, valid_url:true},
				breif_desc: { required: true},
				key_points: { required: true},
				about_author: { required: true},
				banner_img: { valid_img_format: true, filesize: 2000000}	
			},
			messages: {
				webinar_name: {
					required: "This field is required"
				},
				start_date:{required: "This field is required"},
				start_time:{required: "This field is required"},
				end_time:{required: "This field is required"},
				cost_type:{required: "This field is required"},
				cost_price:{required: "This field is required"},
				registration_link:{required: "This field is required"},
				hosting_link:{required: "This field is required"},
				breif_desc:{required: "This field is required"},
				key_points:{required: "This field is required"},
				about_author:{required: "This field is required"},
				banner_img:{valid_img_format: "Please upload only .png, .jpeg, .jpg, .gif file format"},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
	
	function show_hide_webinar_technology_other()
	{
		$("#webinar_technology_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#webinar_technology').select2("data");
		
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#webinar_technology_other_outer").show();
		}
		else
		{
			$("#webinar_technology_other_outer").hide();
			$("#webinar_technology_other").val('');
		}
	}
	/* show_hide_webinar_technology_other(); */
</script>


