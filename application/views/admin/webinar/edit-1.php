<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<style>

</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Webinar</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Edit Webinar</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Webinar
			  </h3>
				  <a href="<?php echo base_url('xAdmin/webinar') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				 
				<div class="card-body">
					 <form method="post" id="webinarform" name="webinarform" role="form" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-md-6">	
							 <div class="form-group">
								<label for="exampleInputEmail1">Webinar ID <em style="color: red;">*</em></label>							
								<input type="text" class="form-control" name="webinar_id" id="webinar_id" value="<?php echo $webinar_data[0]['webinar_id']; ?>"  readonly >
							
							</div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Webinar Name <em style="color: red;">*</em></label>
								<input type="text" class="form-control" name="webinar_name" id="webinar_name" value="<?php echo $webinar_data[0]['webinar_name']; ?>"  >
								<span><?php echo form_error('webinar_name'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Start Date <em style="color: red;">*</em></label>
								<input type="date" class="form-control" name="start_date" id="start_date" 
								value="<?php echo $webinar_data[0]['webinar_date']; ?>"  >
								<span><?php echo form_error('start_date'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Start Time <em style="color: red;">*</em></label>
								<input type="time" class="form-control" name="start_time" id="start_time" value="<?php echo $webinar_data[0]['webinar_time']; ?>"  style="width:22%">
								<span><?php echo form_error('start_time'); ?></span>
							  </div>
							  <div class="form-group">
								<label>Cost Type <em style="color: red;">*</em></label>
								<div class="d-block">
									<div class="form-check form-check-inline">
										<input type="radio" id="upload_type1" name="cost_type" class="form-check-input get-type" value="FREE" onchange="show_hide_div()" 
										<?php if($webinar_data[0]['webinar_cost_type'] == 'FREE'){ ?> checked="checked" <?php } ?>>
										<label class="form-check-label" for="techNovuus1">FREE</label>
									</div>
									<div class="form-check form-check-inline">
										<input type="radio" id="upload_type1" name="cost_type" class="form-check-input get-type" value="PAID" onchange="show_hide_div()" 
										<?php if($webinar_data[0]['webinar_cost_type'] == 'PAID'){ ?> checked="checked" <?php } ?>>
										<label class="form-check-label" for="techNovuus1">PAID</label>
									</div>
								</div>									
							</div>
							<div class="col-md-12" id="paid-amt">
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label class="exampleInputEmail1">&nbsp;</label>
											<select name="currency" id="currency" class="form-control">
												<option value="USD" disabled>$</option>
												<option value="EUR" disabled>€</option>
												<option value="CRC" disabled>₡</option>
												<option value="GBP" disabled>£</option>
												<option value="ILS" disabled>₪</option>
												<option value="INR" selected="selected">₹</option>
												<option value="JPY" disabled>¥</option>
											</select>
										</div>									
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="exampleInputEmail1">Price <em style="color: red;">*</em></label>
											<input type="text" class="form-control " name="cost_price" id="cost_price" value="<?php echo $webinar_data[0]['cost_price']; ?>"  >
											
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Webinar Banner (Upload only jpg, jpeg ,gif & png format only)</label>
								<input type="file" class="form-control" id="banner_img" placeholder="" name="banner_img" >
								
							  </div>
							 <div class="form-group">
								<label for="exampleInputEmail1">Registration Link <em style="color: red;">*</em></label>
								<input type="text" class="form-control" id="registration_link" placeholder="Enter Registration Link " name="registration_link" value="<?php echo $webinar_data[0]['registration_link']; ?>">								
								<span><?php echo form_error('registration_link'); ?></span>
							  </div>
							<div class="form-group">
								<label for="exampleInputEmail1">Hosting Link <em style="color: red;">*</em></label>
								<input type="text" class="form-control" id="hosting_link" placeholder="Enter Hosting Link " name="hosting_link" value="<?php echo $webinar_data[0]['hosting_link']; ?>">								
								<span><?php echo form_error('hosting_link'); ?></span>
							 </div>
							<div class="form-group">
								<label for="exampleInputEmail1">Brief About Webinar <em style="color: red;">*</em></label>
								<textarea class="form-control" id="breif_desc" placeholder="Enter Brief About Webinar " name="breif_desc" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_breif_info']; ?></textarea>
								<span><?php echo form_error('breif_desc'); ?></span>
							 </div>	
							<div class="form-group">
								<label for="exampleInputEmail1">Key Takeaway Points <em style="color: red;">*</em></label>
								<textarea class="form-control" id="key_points" placeholder="Enter Key Takeaway Points " name="key_points" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_key_points']; ?></textarea>
								<span><?php echo form_error('key_points'); ?></span>
							 </div>	
							<div class="form-group">
								<label for="exampleInputEmail1">About Author <em style="color: red;">*</em></label>
								<textarea class="form-control" id="about_author" placeholder="Enter About Author " name="about_author" rows="6" cols="12"><?php echo $webinar_data[0]['webinar_about_author']; ?></textarea>
								<span><?php echo form_error('about_author'); ?></span>
							 </div>								  							  
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
function show_hide_div()
{
	
	$('#paid-amt').hide();
	
	var values = $("input[name='cost_type']:checked").val();
	if(values == "PAID")
	{
		$('#paid-amt').show();
	}
	else 
	{			
		$('#paid-amt').hide();
		$("#cost_price").val(null);			
	}
}
show_hide_div();
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  $.validator.addMethod("valid_img_format", function(value, element)
	{
		if(value != "")
		{
			var validExts = new Array(".png", ".jpeg", ".jpg", ".gif", ".jfif");
			var fileExt = value.toLowerCase();
			fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
			if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
		}else return true;
	});
	
	$.validator.addMethod('filesize', function (value, element, param) {
	     return this.optional(element) || (element.files[0].size <= param)
	    }, 'File size must be less than 2 MB');
		
		
		jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();  
            if (ckValue.length === 0) {  
				//if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
				//If not empty then leave the value as it is  
                $(element).val(editor.getData());  
            }  
            return $(element).val().length > 0;  
        }, "This field is required"); 

		function GetTextFromHtml(html) {
			var dv = document.createElement("DIV");
			dv.innerHTML = html;
			return dv.textContent || dv.innerText || "";
		}

			
		
		
  $('#webinarform').validate({
    rules: {
			webinar_name: { required: true, nowhitespace: true, valid_value: true },
			start_date: { required: true},
			start_time: { required: true},
			upload_type: { required: true},
			cost_price: { required: function(){return $("#upload_type2").val() == 'Paid'; } },	
			banner_img: { valid_img_format: true, filesize:2000000 },
			registration_link:{required: true,url:true },
			hosting_link:{required: true,url:true },
			breif_desc:{required: true, nowhitespace: true, maxCount:['200'] },		
			key_points:{required: true, nowhitespace: true, maxCount:['200'] },
			about_author:{required: true, nowhitespace: true, maxCount:['200'] },
    },
    messages: {
			webinar_name: { required: "This field is required", nowhitespace: "Please enter the title" },
			start_date:{required: "This field is required"},
			start_time:{required: "This field is required"},
			upload_type:{required: "This field is required"},
			cost_price:{required: "This field is required"},
			banner_img:{valid_img_format: "Please upload only .png, .jpeg, .jpg, .gif file format"},
			registration_link:{required: "This field is required", nowhitespace: "This field is required"},
			hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
			breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
			key_points:{required: "This field is required", nowhitespace: "This field is required"},
			about_author:{required: "This field is required", nowhitespace: "This field is required"}	
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


