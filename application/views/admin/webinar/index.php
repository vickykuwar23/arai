<!-- Content Wrapper. Contains page content -->
<style>
/* Prelaoder */
#preloader-loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    overflow: hidden;
    background: rgba(0,0,0,0.5);
}
#preloader-loader:before {
    content: "";
    position: fixed;
    top: calc(50% - 30px);
    left: calc(50% - 30px);
    border: 6px solid #f2f2f2;
    border-top: 6px solid #c80032;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    -webkit-animation: animate-preloader 1s linear infinite;
    animation: animate-preloader 1s linear infinite;
}
@-webkit-keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes animate-preloader {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}


/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
/*.modal-body{
    height: 60vh;
    overflow-y: auto;
}*/

#webinar_reason-error{color:red;font-size:14px;}
</style>
<script>
// Preloader
$(window).on('load', function() {
    if ($('#preloader-loader').length) {
        $('#preloader-loader').delay(50).fadeOut('slow', function() {
            /* $(this).remove(); */
        });
    }
});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Webinar Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Webinar Listing</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   <?php if( $this->session->flashdata('success')){ ?>
		<div class="alert alert-success alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h5><i class="icon fas fa-check"></i> Success!</h5>
		 <?php echo $this->session->flashdata('success'); ?>
		</div>
	   <?php } ?>
         <div class="card ">
				<!--<div class="card-header">
					<form method="post" name="frm-exp" id="frm-exp">
						<div class="row">
							 <div class="col-6">
								 <div class="form-group">
									<input type="text" class="seach-key form-control" id="keyword" name="keyword" placeholder="Search By Challenge Code, Challenge Name, Owner Name, Company Name">
								 </div>
							 </div>
							 <div class="col-6">					
									<input type="submit" class="btn btn-primary btn-sm pull-right float-right" id="export" name="export" value="Export To Excel">					
							 </div>
						</div>
					</form>
				</div>-->
		<input type="hidden" class="token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th class="no-sort">No.</th>
						<th>ID</th>
						<th>Webinar ID</th>	
						<th>Webinar Title</th>
						<th>Date</th>
						<th>Time</th>
						<th>Costing</th>
						<th>Webinar Status</th>
						<th>Application Received</th>
						<th>Is Featured</th>
						<th>xOrder</th>
						<th class="no-sort">Action</th>
					</tr>
				</thead>
					<tbody>                
					</tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="briefInf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Webinar Rejection</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="contents">
				<form method="post" action="javascript:void(0)" name="webinar_form" id="webinar_form">
					<div class="col-auto" style="padding: 0 0 0 0;margin: 0 0 15px 0;">
						<label>Rejection Reason</label>
						<div class="custom-checkbox mr-sm-2">
							<textarea name="webinar_reason" id="webinar_reason" class="form-control"></textarea>
						</div>
						<div id="webinar_reason_err"></div>
						<input type="hidden" name="web_id" id="web_id" value="" />
					</div>					
					<button type="submit" class="btn btn-primary" style="margin: 0 auto;display: block;">Submit</button>
				</form>
			</div>
			
		</div>
	</div>
</div>

<!-- /.content-wrapper -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Applicant Listing</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body " id="contents">
		<div id="des-show"></div>
	  </div>	
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/front/js/'); ?>jquery-validation/additional-methods.min.js"></script>
<script>
function update_sort_order(order_val, id)
{	
	var getVal = order_val;		
	var csrf_test_name	= $('.token').val();			
	var base_url = '<?php echo base_url('xAdmin/webinar/setOrder'); ?>';
	 $.ajax({
		url: base_url,
		type: "post",
		//async: false,
		data: {order_val:getVal, id:id, csrf_test_name:csrf_test_name},				
		success: function (response) {
			var returndata = JSON.parse(response);
			var token = returndata.token;
			$('.token').val(token);
			var status = returndata.msg;
			
			/* swal({
					title: 'Success!',
					text: status,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}); */
		},
		error: function(jqXHR, textStatus, errorThrown) {
			 console.log(textStatus, errorThrown);
		}
	});				
}

  $(document).ready( function () {
				
		$("body").on("click", "#example1 tbody tr .view-modal", function (e) { 			
			$('#exampleModal').modal('show');			
			var id	=	$(this).attr('data-id');
			var base_url = '<?php echo base_url('xAdmin/webinar/viewDetails'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id},				
				success: function (response) { 			
					$("#des-show").html(response);
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});			
		});
		
		 $('#frm-exp').keypress(function (e) {
			if (e.which === 13) {		
				e.preventDefault(); 
			 }
		 });
		
	var base_path = '<?php echo base_url(); ?>';
		var table = $('#example1').DataTable(
		{
				"responsive": true,
				"autoWidth": false,
				"serverMethod": 'post',			
				"ordering":true,
				"searching": false,
				"bStateSave": true,
				"aaSorting":[],
				"language": {
					"zeroRecords":"No matching records found.",
					"infoFiltered":"",
					},
				"columnDefs": [ {
	            "targets": 'no-sort',
	            "orderable": false,
	        } ],
			"processing":false, //Feature control the processing indicator.
			"serverSide":true, //Feature control DataTables' server-side processing mode.		
			
			// Load data for the table's content from an Ajax source
			"ajax": 
			{
				"url": base_path+"xAdmin/webinar/get_webinar_data",
				"type":"POST",
				"data":function(data) {			
					data.keyword 	= $("#keyword").val();
				},
				"error":function(x, status, error) {
					
				},
				"statusCode": 
				{
					401:function(responseObject, textStatus, jqXHR) {	},
				},

			}	
		});
		
		// Admin Status Update
		/*$(document).on('change','.change-status',function(){
			var status = $(this).val(); //$("#ch_id").val();
			var ID = $(this).closest("tr").find("#w_id").val();
			var cs_t = 	$('.token').val();
			$('#preloader-loader').css('display', 'block');	
			var base_url = '<?php echo base_url() ?>';
			 $.ajax({
				type:'POST',
				url: base_url+'xAdmin/webinar/updateStatus',
				data:'status='+status+'&id='+ID+'&csrf_test_name='+cs_t,				
				success:function(data){
					$('#preloader-loader').css('display', 'none');	
					var output = JSON.parse(data);	
					$(".token").val(output.token);
					$('#example1').DataTable().ajax.reload();
					swal({
							title: 'Success!',
							text: "Webinar status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});
				}
			});
			
		});	*/
		
		$(document).on('change','.change-status',function(){
			var status = $(this).val();
			if(status!= 'Rejected'){				
				var ID = $(this).closest("tr").find("#w_id").val();
				var cs_t = 	$('.token').val();
				$('#preloader-loader').css('display', 'block');	
				var base_url = '<?php echo base_url() ?>';
				 $.ajax({
					type:'POST',
					url: base_url+'xAdmin/webinar/updateStatus',
					data:'status='+status+'&id='+ID+'&csrf_test_name='+cs_t,				
					success:function(data){
						$('#preloader-loader').css('display', 'none');	
						var output = JSON.parse(data);	
						$(".token").val(output.token);
						$('#example1').DataTable().ajax.reload();
						swal({
								title: 'Success!',
								text: "Webinar status updated successfully.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
					}
				});
				
				
			} else {
				var ID = $(this).closest("tr").find("#w_id").val();
				$("#web_id").val(ID);	
				$("#webinar_form")[0].reset();
				$("#webinar_reason-error").html('');
				$("#briefInf").modal('show');				
			}			
				
		});	
		
		//******* JQUERY VALIDATION *********
		$("#webinar_form").validate( 
		{
			rules: { webinar_reason: { required: true } },
			messages: { webinar_reason: { required: "Please enter rejection reason" }, },
			errorPlacement: function(error, element) // For replace error 
			{
				if (element.attr("name") == "webinar_reason") { error.insertAfter("#webinar_reason_err"); }
				else { error.insertAfter(element); }
			},
			submitHandler: function(form) 
			{ 
				var csrf_test_name 	= 	$('.token').val();
				var web_id 			= 	$('#web_id').val();
				var webinar_reason 	= 	$('#webinar_reason').val();
				$('#preloader-loader').show();
				parameters= { 'webinar_reason': webinar_reason, 'id': web_id, 'status': "Rejected", 'csrf_test_name':csrf_test_name }
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('xAdmin/webinar/updateStatus'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success: function(data) 
					{  
						//$(".token").val(data.csrf_test_name);
						/*
						var output = JSON.parse(data);
							console.log(output);
						$(".token").val(output.token);
						*/	
						$('#preloader-loader').css('display', 'none');	
						$("#briefInf").modal('hide');
						$('#example1').DataTable().ajax.reload();
						swal({
								title: 'Success!',
								text: "Webinar status updated successfully.",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
					}
				});	
			}
		});
				
		
		// Featured Functionality
		 $("body").on("click", "#example1 tbody tr a.featured-check", function (e) {    
		  	var id = $(this).attr('data-id'); 
			var csrf_test_name = 	$('.token').val();			
			$('#preloader-loader').css('display', 'block');		
			var base_url = '<?php echo base_url('xAdmin/webinar/featured'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					$('#preloader-loader').css('display', 'none');	
					var returndata = JSON.parse(response);
					var token = returndata.token;
					var c_status = returndata.c_status;
					var status = returndata.u_featured;
					$('.txt_csrfname').val(token);
					//console.log(status);	
					$("a#change-stac-"+id).html(status);
					$('#example1').DataTable().ajax.reload();
					swal({
							title: 'Success!',
							text: "Webinar featured status updated successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       }); 
	   
	   $("body").on("click", "#example1 tbody tr a.remove", function (e) {    
		  	var id = $(this).attr('data-id'); 
			var csrf_test_name = 	$('.token').val();			
			$('#preloader-loader').css('display', 'block');		
			var base_url = '<?php echo base_url('xAdmin/webinar/deleted'); ?>';
			 $.ajax({
				url: base_url,
				type: "post",
				data: {id:id,csrf_test_name:csrf_test_name},				
				success: function (response) {
					$('#preloader-loader').css('display', 'none');	
					var returndata = JSON.parse(response);
					var token = returndata.token;
					
					$('.txt_csrfname').val(token);				
					$('#example1').DataTable().ajax.reload();
					swal({
							title: 'Success!',
							text: "Webinar deleted successfully.",
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						});	
				},
				error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
				}
			});
			
       }); 
	
});
</script>