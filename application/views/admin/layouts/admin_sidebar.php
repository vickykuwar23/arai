<?php $url = $this->uri->segment(2); ?>
<?php $url2 = $this->uri->segment(4); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
     <a href="javascript:void(0);" class="brand-link">
      <!--<img src="<?php echo base_url('assets/admin/img/admin_logo.png') ?>" alt="AARAI LOGO" class="brand-image img-circle elevation-3"
           style="opacity: .8">-->
      <span class="brand-text font-weight-light">ARAI</span>
    </a> 
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
          <img src="<?php echo base_url('assets/front/') ?>img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
          <div class="info">
			  <a href="#" class="d-block"><?php echo $this->session->userdata('admin_name'); ?></a>
			</div>
		  </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/admin/dashboard');?>" class="nav-link <?php if($module_name=='Dashboard') echo 'active' ?>">
					<i class="nav-icon fas fa-th"></i>
					<p>Dashboard</p>
				</a>
			</li>	
			<?php if($this->check_permissions->is_authorise_admin_link(1)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/usermaster');?>" class="nav-link <?php if($module_name=='Usermaster') echo 'active' ?>">
					<i class="nav-icon fas fa-th"></i>
					<p>Admin Master</p>
				</a>
			</li>
		<?php } ?>
		<?php if($this->check_permissions->is_authorise_admin_link(2)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/users');?>" class="nav-link <?php if($module_name=='Users') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>User/Organization Listing</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(3)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/challenge');?>" class="nav-link <?php if($module_name=='Challenge') echo 'active' ?>">
				   <i class="nav-icon fas fa-th"></i>
				  <p>Challenge Listing</p>
				</a>
			</li>
			<?php } ?>
			
			<?php if($this->check_permissions->is_authorise_admin_link(3)){ ?>
			<li class="nav-item has-treeview <?php if($module_name=='Master'): echo 'menu-open'; endif; ?>">
				<a href="#" class="nav-link">
				  <i class="nav-icon fas fa-book"></i>
				  <p>
					Masters
					<i class="fas fa-angle-left right"></i>
				  </p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/promocode');?>" class="nav-link <?php if($module_name=='Promocode') echo 'active' ?>">
						   <i class="nav-icon fas fa-th"></i>
						  <p>Promo Code</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/promocode_assoc');?>" class="nav-link <?php if($module_name=='promocode_assoc') echo 'active' ?>">
						   <i class="nav-icon fas fa-th"></i>
						  <p>Promo Association </p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/plan');?>" class="nav-link <?php if($module_name=='Plan') echo 'active' ?>">
						   <i class="nav-icon fas fa-th"></i>
						  <p>Plan</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/admin_type');?>" class="nav-link <?php if($submodule_name=='Admin_Type') echo 'active' ?>">
							<i class="far fa-circle nav-icon"></i>
							<p>Admin Type</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/usertype');?>" class="nav-link <?php if($submodule_name=='Usertype') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>User Roles & Permissions</p>
						</a>
					</li> 
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/categories');?>" class="nav-link <?php if($submodule_name=='Cagegories') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Category</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/subcategory');?>" class="nav-link <?php if($submodule_name=='Subcategories') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Subcategory</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/partners');?>" class="nav-link <?php if($submodule_name=='Partners') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Government Partners</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/consortiumpartners');?>" class="nav-link <?php if($submodule_name=='C_Partners') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Consortium Partners</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/trl');?>" class="nav-link <?php if($submodule_name=='TRL') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>TRL Master</p>
						</a>
					</li>
				
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/cms');?>" class="nav-link <?php if($submodule_name=='CMS') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>CMS Listing</p>
						</a>
					</li>
					 
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/banner');?>" class="nav-link <?php if($submodule_name=='Banner') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Banner Management</p>
						</a>
					</li>	
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/contactus');?>" class="nav-link <?php if($submodule_name=='Contactus') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Contact Listing</p>
						</a>
					</li>	
					<!--
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/membership');?>" class="nav-link <?php if($submodule_name=='Membership') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Membership</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/payment_gateway');?>" class="nav-link <?php if($submodule_name=='Payment_Gateway') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Payment Gateway</p>
						</a>
					</li>-->
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/tags');?>" class="nav-link <?php if($submodule_name=='Tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Tags</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/blog_tags');?>" class="nav-link <?php if($submodule_name=='blog_tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Blog Tags</p>
						</a>
					</li>	
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/qa_forum_tags');?>" class="nav-link <?php if($submodule_name=='qa_forum_tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>QA Forum Tags</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/knowledge_repository_tags');?>" class="nav-link <?php if($submodule_name=='knowledge_repository_tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Knowledge Repository Tags</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/resource_sharing_tags');?>" class="nav-link <?php if($submodule_name=='resource_sharing_tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Resource Sharing tags</p>
						</a>
					</li>	

					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/technology_transfer_tags');?>" class="nav-link <?php if($submodule_name=='technology_transfer_tags') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Technology transfer tags</p>
						</a>
					</li>	

					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/currency_master');?>" class="nav-link <?php if($submodule_name=='currency_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Currency</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/audience_preference');?>" class="nav-link <?php if($submodule_name=='Audience_Preference') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Audience Preference</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/technology_master');?>" class="nav-link <?php if($submodule_name=='Technology') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Technology</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/webinar_technology');?>" class="nav-link <?php if($submodule_name=='Webinar_Technology') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Webinar Technology</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/blog_technology_master');?>" class="nav-link <?php if($submodule_name=='Blog_technology_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Blog Technology</p>
						</a>
					</li>
					
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/knowledge_repository_type_master');?>" class="nav-link <?php if($submodule_name=='Knowledge_repository_type_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Knowledge repository type master</p>
						</a>
					</li>
					
					

					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/knowledge_repository_technology_master');?>" class="nav-link <?php if($submodule_name=='knowledge_repository_technology_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Knowledge repository technology</p>
						</a>
					</li>


					
					
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/institution_master');?>" class="nav-link <?php if($submodule_name=='Institution') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Institution</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/domain_master');?>" class="nav-link <?php if($submodule_name=='Domain') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Domain/Expertise</p>
						</a>
					</li>

					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/domain_industry_master');?>" class="nav-link <?php if($submodule_name=='domain_industry_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Domain/Industry</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/front_registration_usercategory');?>" class="nav-link <?php if($submodule_name=='Front_registration_usercategory') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>UserCategory</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/front_registration_usersubcategory');?>" class="nav-link <?php if($submodule_name=='Front_registration_usersubcategory') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>UserSubcategory</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/Employementmaster');?>" class="nav-link <?php if($submodule_name=='Employementmaster') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Employement Status Master</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/public_status_master');?>" class="nav-link <?php if($submodule_name=='Public_status_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Public / Private Status</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/student_profile');?>" class="nav-link <?php if($submodule_name=='Student_profile') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Student Profile</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/individual_profile');?>" class="nav-link <?php if($submodule_name=='Individual_profile') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Individual / Experts Profile</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/area_of_interest_master');?>" class="nav-link <?php if($submodule_name=='Area_of_interest_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Area of Interest</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/skill_sets_master');?>" class="nav-link <?php if($submodule_name=='Skill_sets_master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Skill Sets</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/how_it_works');?>" class="nav-link <?php if($submodule_name=='How_it_works') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>How It Works</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/services');?>" class="nav-link <?php if($submodule_name=='Services') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Services</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/studentmaster');?>" class="nav-link <?php if($submodule_name=='Student Master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Student Status Master</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/organization_sector');?>" class="nav-link <?php if($submodule_name=='Organization_sector') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Organization Sector</p>
						</a>
					</li>					
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/about_byt');?>" class="nav-link <?php if($submodule_name=='About_BYT') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>About BYT</p>
						</a>
					</li>			
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/login_page');?>" class="nav-link <?php if($submodule_name=='Login Page') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Login Page</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/type_master');?>" class="nav-link <?php if($submodule_name=='Type_Master') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Type Master</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/subscriber_email/add');?>" class="nav-link <?php if($submodule_name=='Subscriber_email') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Notification To Users</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/webinar_banner');?>" class="nav-link <?php if($submodule_name=='Webinar_banner') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Webinar Banners</p>
						</a>
					</li>		
				</ul>
			</li>
			<li class="nav-item has-treeview <?php if($module_name=='Reason'): echo 'menu-open'; endif; ?>">
				<a href="#" class="nav-link">
				  <i class="nav-icon fas fa-book"></i>
				  <p>
					Reason Master
					<i class="fas fa-angle-left right"></i>
				  </p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/challenge_rejection');?>" class="nav-link <?php if($submodule_name=='Challenge_rejection') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Challenge Access Rejection</p>
						</a>
					</li>			
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/challenge_withdrawal');?>" class="nav-link <?php if($submodule_name=='Challenge_withdrawal') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Challenge Withdrawal</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/challenge_closure');?>" class="nav-link <?php if($submodule_name=='challenge_close') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Challenge Closure</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('xAdmin/team_withdraw');?>" class="nav-link <?php if($submodule_name=='Team_Withdraw') echo 'active' ?>">
						  <i class="far fa-circle nav-icon"></i>
						  <p>Team Member Remove Reason</p>
						</a>
					</li>
				</ul>
			</li>	
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(5)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/faq');?>" class="nav-link <?php if($module_name=='FAQ') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>FAQ Management</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(6)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/newsletter');?>" class="nav-link <?php if($module_name=='Newsletter') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Newsletter Subscribe</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(7)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/blog');?>" class="nav-link <?php if($module_name=='Blog') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>News & Updates</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(8)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/team/index_new');?>" class="nav-link <?php if($module_name=='Team') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>BYT Team</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(9)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/task/index_new');?>" class="nav-link <?php if($module_name=='Task') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Task Team</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(10)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/webinar');?>" class="nav-link <?php if($module_name=='Webinar') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Webinar</p>
				</a>
			</li>	
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(11)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/blogs_technology_wall');?>" class="nav-link <?php if($module_name=='Blog technology Wall') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Blog</p>
				</a>
			</li>	
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(12)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/questions_answers_forum');?>" class="nav-link <?php if($module_name=='Question & Answer Forum') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>QA Forum</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(13)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/knowledge_repository');?>" class="nav-link <?php if($module_name=='Knowledge repository') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Knowledge Repository</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(14)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/resource_sharing');?>" class="nav-link <?php if($module_name=='Resource sharing') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Resource Sharing</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(15)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/technology_transfer');?>" class="nav-link <?php if($module_name=='Technology transfer') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Technology transfer</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(16)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/admin_update');?>" class="nav-link <?php if($this->uri->segment(2)=='admin_update') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Technovuus Updates</p>
				</a>
			</li>
			<?php } ?>
			
			<?php if($this->check_permissions->is_authorise_admin_link(17)){ ?>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/testimonials');?>" class="nav-link <?php if($module_name=='Testimonial') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Testimonials</p>
				</a>
			</li>		
			<?php } ?>	 
			<?php if($this->check_permissions->is_authorise_admin_link(18)){ ?> 
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/expert_query');?>" class="nav-link <?php if($module_name=='Expert_query') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Expert Query</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(19)){ ?> 	 
				<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/expert_emails');?>" class="nav-link <?php if($module_name=='Expert Emails') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Expert Emails</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(20)){ ?> 	 
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/tool_requirement');?>" class="nav-link <?php if($module_name=='Tool_requirement') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Tools Requirement</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(21)){ ?> 	
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/participation_log');?>" class="nav-link <?php if($module_name=='Participation Log') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Participation Log</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(22)){ ?> 
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/email_master');?>" class="nav-link <?php if($module_name=='email_master') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Email Master</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(23)){ ?> 
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/subscriber_email');?>" class="nav-link <?php if($module_name=='Subscriber_email') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Email Sending Master</p>
				</a>
			</li>
			<?php } ?>
			<?php if($this->check_permissions->is_authorise_admin_link(24)){ ?> 
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/popups');?>" class="nav-link <?php if($module_name=='popups') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Popups</p>
				</a>
			</li>
			<?php } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>