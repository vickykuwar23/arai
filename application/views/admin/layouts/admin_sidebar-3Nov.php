<?php $url = $this->uri->segment(2); ?>
<?php $url2 = $this->uri->segment(4); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
     <a href="javascript:void(0);" class="brand-link">
      <!--<img src="<?php echo base_url('assets/admin/img/admin_logo.png') ?>" alt="AARAI LOGO" class="brand-image img-circle elevation-3"
           style="opacity: .8">-->
      <span class="brand-text font-weight-light">ARAI</span>
    </a> 
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
          <img src="<?php echo base_url('assets/front/') ?>img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
          <div class="info">
			  <a href="#" class="d-block"><?php echo $this->session->userdata('admin_name'); ?></a>
			</div>
		  </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?php echo base_url('xAdmin/admin/dashboard');?>" class="nav-link <?php if($module_name=='Dashboard') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </a>
          </li>	
		  <li class="nav-item">
            <a href="<?php echo base_url('xAdmin/usermaster');?>" class="nav-link <?php if($module_name=='Usermaster') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>Admin Master</p>
							</a>
          </li>
		  <li class="nav-item">
			<a href="<?php echo base_url('xAdmin/users');?>" class="nav-link <?php if($module_name=='Users') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>User/Organization Listing</p>
			</a>
		  </li>
			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/challenge');?>" class="nav-link <?php if($module_name=='Challenge') echo 'active' ?>">
				   <i class="nav-icon fas fa-th"></i>
				  <p>Challenge Listing</p>
				</a>
			  </li>
			  <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/promocode');?>" class="nav-link <?php if($module_name=='Promocode') echo 'active' ?>">
				   <i class="nav-icon fas fa-th"></i>
				  <p>Promo Code</p>
				</a>
			  </li>

        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/promocode_assoc');?>" class="nav-link <?php if($module_name=='promocode_assoc') echo 'active' ?>">
           <i class="nav-icon fas fa-th"></i>
          <p>Promo Association </p>
        </a>
        </li>
			  
			  <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/plan');?>" class="nav-link <?php if($module_name=='Plan') echo 'active' ?>">
				   <i class="nav-icon fas fa-th"></i>
				  <p>Plan</p>
				</a>
			  </li>
		  <li class="nav-item has-treeview <?php if($module_name=='Master') echo 'menu-open' ?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Masters
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/admin_type');?>" class="nav-link <?php if($submodule_name=='Admin_Type') echo 'active' ?>">
				  <i class="far fa-circle nav-icon"></i>
				  <p>Admin Type</p>
				</a>
			  </li>
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/usertype');?>" class="nav-link <?php if($submodule_name=='Usertype') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Roles & Permissions</p>
                </a>
              </li> 
			  <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/identitygroup');?>" class="nav-link <?php if($submodule_name=='Identitygroup') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Identity Group</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/useraccesstype');?>" class="nav-link <?php if($submodule_name=='User_TYPE_ACCESS') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Access Type</p>
                </a>
              </li>-->
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/categories');?>" class="nav-link <?php if($submodule_name=='Cagegories') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Category</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/subcategory');?>" class="nav-link <?php if($submodule_name=='Subcategories') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Subcategory</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/partners');?>" class="nav-link <?php if($submodule_name=='Partners') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Partners</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/consortiumpartners');?>" class="nav-link <?php if($submodule_name=='C_Partners') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Consortium Partners</p>
                </a>
              </li>
			  
			 <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/challengetype');?>" class="nav-link <?php if($submodule_name=='Challengetype') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Challenge Visiblility</p>
                </a>
              </li>-->
			  
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/trl');?>" class="nav-link <?php if($submodule_name=='TRL') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>TRL Master</p>
                </a>
              </li>
			 	
			 <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/challengecategories');?>" class="nav-link <?php if($submodule_name=='Challengecategories') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Challenge Categories</p>
                </a>
              </li>-->	
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/cms');?>" class="nav-link <?php if($submodule_name=='CMS') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>CMS Listing</p>
                </a>
              </li>
			  <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/membership');?>" class="nav-link <?php if($submodule_name=='Membership') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Membership</p>
                </a>
              </li> 
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/responses');?>" class="nav-link <?php if($submodule_name=='Response') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Response</p>
                </a>
              </li> -->
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/banner');?>" class="nav-link <?php if($submodule_name=='Banner') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner Management</p>
                </a>
              </li> 
			  
			  <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/email_master');?>" class="nav-link <?php if($submodule_name=='Email') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Email Master</p>
                </a>
              </li>
			   <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/sms');?>" class="nav-link <?php if($submodule_name=='SMS') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>SMS Master</p>
                </a>
              </li>-->
			  <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/contactus');?>" class="nav-link <?php if($submodule_name=='Contactus') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact Listing</p>
                </a>
              </li>	
			  <!--<li class="nav-item">
                <a href="<?php echo base_url('xAdmin/payment_gateway');?>" class="nav-link <?php if($submodule_name=='Payment_Gateway') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Gateway</p>
                </a>
              </li>-->
			  <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/tags');?>" class="nav-link <?php if($submodule_name=='Tags') echo 'active' ?>">
				  <i class="far fa-circle nav-icon"></i>
				  <p>Tags</p>
				</a>
			  </li>	
			  <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/audience_preference');?>" class="nav-link <?php if($submodule_name=='Audience_Preference') echo 'active' ?>">
				  <i class="far fa-circle nav-icon"></i>
				  <p>Audience Preference</p>
				</a>
			  </li>
			  <li class="nav-item">
				<a href="<?php echo base_url('xAdmin/technology_master');?>" class="nav-link <?php if($submodule_name=='Technology') echo 'active' ?>">
				  <i class="far fa-circle nav-icon"></i>
				  <p>Technology</p>
				</a>
			  </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/institution_master');?>" class="nav-link <?php if($submodule_name=='Institution') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Institution</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/domain_master');?>" class="nav-link <?php if($submodule_name=='Domain') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Domain/Expertise</p>
        </a>
        </li>

        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/domain_industry_master');?>" class="nav-link <?php if($submodule_name=='domain_industry_master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Domain/Industry</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/front_registration_usercategory');?>" class="nav-link <?php if($submodule_name=='Front_registration_usercategory') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>UserCategory</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/front_registration_usersubcategory');?>" class="nav-link <?php if($submodule_name=='Front_registration_usersubcategory') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>UserSubcategory</p>
        </a>
        </li>
         <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/Employementmaster');?>" class="nav-link <?php if($submodule_name=='Employementmaster') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Employement Status Master</p>
        </a>
        </li>

        
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/public_status_master');?>" class="nav-link <?php if($submodule_name=='Public_status_master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Public / Private Status</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/student_profile');?>" class="nav-link <?php if($submodule_name=='Student_profile') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Student Profile</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/individual_profile');?>" class="nav-link <?php if($submodule_name=='Individual_profile') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Individual / Experts Profile</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/area_of_interest_master');?>" class="nav-link <?php if($submodule_name=='Area_of_interest_master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Area of Interest</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/skill_sets_master');?>" class="nav-link <?php if($submodule_name=='Skill_sets_master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Skill Sets</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/how_it_works');?>" class="nav-link <?php if($submodule_name=='How_it_works') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>How It Works</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/services');?>" class="nav-link <?php if($submodule_name=='Services') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Services</p>
        </a>
        </li>
		<li class="nav-item">
        <a href="<?php echo base_url('xAdmin/studentmaster');?>" class="nav-link <?php if($submodule_name=='Student Master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Student Status Master</p>
        </a>
        </li>
		<li class="nav-item">
        <a href="<?php echo base_url('xAdmin/organization_sector');?>" class="nav-link <?php if($submodule_name=='Organization_sector') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Organization Sector</p>
        </a>
        </li>
		<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/challenge_rejection');?>" class="nav-link <?php if($submodule_name=='Challenge_rejection') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>Challenge Access Rejection Reason</p>
			</a>
			</li>
			
			<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/challenge_withdrawal');?>" class="nav-link <?php if($submodule_name=='Challenge_withdrawal') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>Challenge Withdrawal Reason</p>
			</a>
			</li>
			<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/about_byt');?>" class="nav-link <?php if($submodule_name=='About_BYT') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>About BYT</p>
			</a>
			</li>
			<!--<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/about_how_it_work');?>" class="nav-link <?php if($submodule_name=='BYT_how_it_works') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>About BYT How It Works</p>
			</a>
			</li>
			<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/type_master');?>" class="nav-link <?php if($submodule_name=='type_master') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>Type Master</p>
			</a>
			</li>-->
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/login_page');?>" class="nav-link <?php if($submodule_name=='Login Page') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Login Page</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo base_url('xAdmin/type_master');?>" class="nav-link <?php if($submodule_name=='Type_Master') echo 'active' ?>">
          <i class="far fa-circle nav-icon"></i>
          <p>Type Master</p>
        </a>
        </li>
		<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/subscriber_email/add');?>" class="nav-link <?php if($module_name=='Master') echo 'active' ?>">
			  <i class="far fa-circle nav-icon"></i>
			  <p>Notification To Users</p>
			</a>
		  </li>
			 <!--
			 <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/usercategories');?>" class="nav-link <?php if($submodule_name=='User_Categories') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Categories</p>
                </a>
              </li>
			 <li class="nav-item">
                <a href="<?php echo base_url('xAdmin/usersubcat');?>" class="nav-link <?php if($submodule_name=='User_SubCategories') echo 'active' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User SubCategories</p>
                </a>
              </li>-->	
            </ul>
          </li>
		<li class="nav-item">
            <a href="<?php echo base_url('xAdmin/faq');?>" class="nav-link <?php if($module_name=='FAQ') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>FAQ Management</p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="<?php echo base_url('xAdmin/newsletter');?>" class="nav-link <?php if($module_name=='Newsletter') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>Newsletter Subscribe</p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="<?php echo base_url('xAdmin/blog');?>" class="nav-link <?php if($module_name=='Blog') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>News & Updates</p>
            </a>
          </li>
		  <li class="nav-item">
			<a href="<?php echo base_url('xAdmin/team');?>" class="nav-link <?php if($module_name=='Team') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>BYT Team</p>
			</a>
		  </li>
		  <!--<li class="nav-item">
            <a href="<?php echo base_url('xAdmin/setconfigure');?>" class="nav-link <?php if($module_name=='Configuration') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>Configure Email/SMS</p>
            </a>
          </li>-->
		  <li class="nav-item">
			<a href="<?php echo base_url('xAdmin/testimonials');?>" class="nav-link <?php if($module_name=='Testimonial') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>Testimonials</p>
			</a>
		  </li>
		  
		  <li class="nav-item">
			<a href="<?php echo base_url('xAdmin/expert_query');?>" class="nav-link <?php if($module_name=='Expert_query') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>Expert Query</p>
			</a>
		  </li>
		  <li class="nav-item">
			<a href="<?php echo base_url('xAdmin/tool_requirement');?>" class="nav-link <?php if($module_name=='Tool_requirement') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>Tools Requirement</p>
			</a>
		  </li>
			<li class="nav-item">
			<a href="<?php echo base_url('xAdmin/participation_log');?>" class="nav-link <?php if($module_name=='Participation Log') echo 'active' ?>">
			  <i class="nav-icon fas fa-th"></i>
			  <p>Participation Log</p>
			</a>
		  </li>
		  
          <!--<li class="nav-item">
            <a href="<?php echo base_url('xAdmin/menu');?>" class="nav-link <?php if($url=='menu') echo 'active' ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Menu
              </p>
            </a>
          </li>-->
      
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>