<?php $this->check_permissions->is_admin_exist(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ARAI | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>dist/css/adminlte.min.css">
  
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>dist/css/sweetalert2.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/summernote/summernote-bs4.css">
  
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
 
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- jQuery -->
	<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   <?php if ($this->session->flashdata('error_pemission') != "") 
            { ?>
               <script type="text/javascript">
                 setTimeout(function () {
                   swal( 'Warning!','You don\'t have permission to access this page !','warning');
                   }, 200);
               </script>
    <?php } ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url('xAdmin/setting'); ?>" class="nav-link <?php if($module_name=='Setting') echo 'active' ?>">Setting</a>
      </li>
	  <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url('xAdmin/task_setting'); ?>" class="nav-link <?php if($module_name=='Task Setting') echo 'active' ?>">Task Setting</a>
      </li>
     <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>-->
    </ul>


    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-user" ></i>
          <?php if($this->session->userdata('id')) echo $this->session->userdata('admin_username');  ?>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
           
             <a href="<?php echo base_url('xAdmin/admin/change_password') ?>" class="dropdown-item">
              Change Password
            </a>   
            <a href="<?php echo base_url('xAdmin/admin/logout') ?>" class="dropdown-item">
              Logout
            </a> 
          
          <div class="dropdown-divider"></div>
        </div>
      </li>
  
      
    </ul>
  </nav>
  <!-- /.navbar -->