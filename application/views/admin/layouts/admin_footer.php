<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="https://esds.co.in" target="_blank">ESDS Software Solution Pvt. Ltd.</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
  
  $(function () {
  
   //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	});
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script>

<script src="<?php echo base_url('assets/admin/') ?>dist/js/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>dist/js/sweetalert2.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/select2/js/select2.full.min.js"></script>
<!--<script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script>-->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/demo.js"></script> -->

<!-- <link href="<?php  echo base_url('assets/front/css/select2.min.css')?>" rel="stylesheet" />
<link href="<?php  echo base_url('assets/front/js/select2.min.js')?>" rel="stylesheet" /> -->

<script type="text/javascript">
    $(function () {
    $('.select2_common').select2();
      });

<?php if($this->uri->segment(2)=='knowledge_repository'){ ?>
  function show_hide_tag_other()
  {
    $("#tag_other_outer").hide();
    
    var disp_other_flg = 0;
    var selected = $('#tags').select2("data");
    for (var i = 0; i <= selected.length-1; i++) 
    {
      if((selected[i].text).toLowerCase() == 'other')
      {
        disp_other_flg = 1;
        break;
      }
    }
    
    if(disp_other_flg == 1)
    {
      $("#tag_other_outer").show();
      $("#tag_other").prop("required", true);
    }
    else
    {
      $("#tag_other_outer").hide();
      $("#tag_other").prop("required", false);
      $("#tag_other").val('');
    }
  }

  function show_hide_technology_domain_other()
  {
    $("#blog_technology_other_outer").hide();
    
    var disp_other_flg = 0;
    var selected = $('#technology_ids').select2("data");
    for (var i = 0; i <= selected.length-1; i++) 
    {
      if((selected[i].text).toLowerCase() == 'other')
      {
        disp_other_flg = 1;
        break;
      }
    }
    
    if(disp_other_flg == 1)
    {
      $("#blog_technology_other_outer").show();
      $("#technology_ids_other").prop("required", true);
    }
    else
    {
      $("#blog_technology_other_outer").hide();
      $("#technology_ids_other").prop("required", false);
      $("#technology_ids_other").val('');
    }
  }

    function show_hide_kr_type_other()
  {
    $("#kr_type_other_outer").hide();
    
    var disp_other_flg = 0;
    var selected = $('#kr_type').select2("data");
    for (var i = 0; i <= selected.length-1; i++) 
    {
      if((selected[i].text).toLowerCase() == 'other')
      {
        disp_other_flg = 1;
        break;
      }
    }
    
    if(disp_other_flg == 1)
    {
      $("#kr_type_other_outer").show();
      $("#kr_type_other").prop("required", true);
    }
    else
    {
      $("#kr_type_other_outer").hide();
      $("#kr_type_other").prop("required", false);
      $("#kr_type_other").val('');
    }
  }
 
  
   $(function () {
  show_hide_tag_other();
  show_hide_technology_domain_other();
   show_hide_kr_type_other();
   });
<?php } ?>

<?php if($this->uri->segment(2)=='resource_sharing'){ ?>

  function show_hide_tag_other()
  {
    $("#tag_other_outer").hide();
    
    var disp_other_flg = 0;
    var selected = $('#tags').select2("data");
    for (var i = 0; i <= selected.length-1; i++) 
    {
      if((selected[i].text).toLowerCase() == 'other')
      {
        disp_other_flg = 1;
        break;
      }
    }
    
    if(disp_other_flg == 1)
    {
      $("#tag_other_outer").show();
      $("#tag_other").prop("required", true);
    }
    else
    {
      $("#tag_other_outer").hide();
      $("#tag_other").prop("required", false);
      $("#tag_other").val('');
    }
  }
  
  
  function show_hide_location_other()
  {
    $("#location_other_outer").hide();
    
    var disp_other_flg = 0;
    var selected = $('#location').select2("data");
    for (var i = 0; i <= selected.length-1; i++) 
    {
      if((selected[i].text).toLowerCase() == 'other')
      {
        disp_other_flg = 1;
        break;
      }
    }
    
    if(disp_other_flg == 1)
    {
      $("#location_other_outer").show();
      $("#location_other").prop("required", true);
    }
    else
    {
      $("#location_other_outer").hide();
      $("#location_other").prop("required", false);
      $("#location_other").val('');
    }
  }
  $(function () {

  show_hide_location_other();
  show_hide_tag_other();

   });

  <?php } ?>
</script>
</body>
</html>
