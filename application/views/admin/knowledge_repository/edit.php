<?php $encrypt_obj =  New Opensslencryptdecrypt(); ?>
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>



<style>
	/* Prelaoder */
	#preloader-loader { position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 9999; overflow: hidden; background: rgba(0,0,0,0.5); }
	#preloader-loader:before { content: ""; position: fixed; top: calc(50% - 30px); left: calc(50% - 30px); border: 6px solid #f2f2f2; border-top: 6px solid #c80032; border-radius: 50%; width: 60px; height: 60px; -webkit-animation: animate-preloader 1s linear infinite; animation: animate-preloader 1s linear infinite; }
	@-webkit-keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	@keyframes animate-preloader { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
	.custom-file-upload, .custom-file-upload-private { margin-top:0 !important; }
	  .custom-file input[type="file"] {
            display: none;
        }
        
        .custom-file .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        .file-details .file-list {
	border: 1px solid #ddd;
	padding: 8px 5px;
	border-radius: 4px;
	text-align: center;
	position: relative;
	margin: 0 0 15px 0;
	font-size: 12px;
	word-break: break-all;
	height:100px;
}

.file-details .file-list .file_ext_title h4 {
	margin: 0;
	font-size: 18px !important;
	text-decoration: none;
	text-transform: uppercase;
	font-weight: 600;
	line-height: 80px;
}

.file-details .file-list a.file-close {
	position: absolute;
	right: 0;
	top: 0;
	width: 25px;
	color: #e23751;
	background: #fff;
	border: 1px solid #ddd;
}

.file-details .file-list span {
	width: 100%;
	display: inline-block;
	font-size: 42px;
	color: #FFC107;
	height: 50px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 18px!important;
}
</style>
<script>
	// Preloader
	$(window).on('load', function() {
    if ($('#preloader-loader').length) {
			$('#preloader-loader').delay(50).fadeOut('slow', function() {
				/* $(this).remove(); */
			});
		}
	});
</script>
<div id="preloader-loader" style="display:none;"></div>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Knowledge Repository</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Knowledge Repository</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content">
		<div class="container-fluid">
			<div class="card ">
			  <div class="card-header">
					<h3 class="card-title">
						Edit Knowledge Repository
					</h3>
				  <a href="<?php echo base_url('xAdmin/knowledge_repository') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
				</div>
				
				<div class="card-body">
					<?php echo validation_errors(); ?>
					<form method="post" id="blogform" name="blogform" role="form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="blog_id_disp">Knowledge Repository ID <em style="color: red;"></em></label>
									<input type="text" class="form-control" name="kr_id_disp" id="kr_id_disp" value="<?php echo $form_data[0]['kr_id_disp']; ?>"  readonly >
								</div>	
								
								<div class="form-group">
									<label for="blog_title">Title of the Content <em style="color: red;">*</em></label>
									<input type="text" class="form-control" id="title_of_the_content" placeholder="" name="title_of_the_content" value="<?php echo $form_data[0]['title_of_the_content']; ?>" required>								
									<?php if(form_error('title_of_the_content')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('title_of_the_content'); ?></label> <?php } ?>
								</div>	

								<div class="form-group">
									<label for="blog_title">Content Provider Name / Organization Name​ <em style="color: red;">*</em></label>
									<input type="text" class="form-control" id="author_name" placeholder="" name="author_name" value="<?php echo $form_data[0]['author_name']; ?>" required>								
									<?php if(form_error('author_name')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('author_name'); ?></label> <?php } ?>
								</div>

								<div class="form-group">
									<label for="kr_banner">Blog Banner (Upload only jpg, jpeg ,gif & png format only)<em style="color: red;">*</em></label>
									<input type="file" class="form-control" id="kr_banner" placeholder="" name="kr_banner" >
									<?php if($kr_banner_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $kr_banner_error; ?></label> <?php } ?>
								</div>
								<div>
									<?php if($form_data[0]['kr_banner']!=""){ ?> 
										<img src="<?php echo base_url('uploads/kr_banner/'.$form_data[0]['kr_banner']); ?>" style="max-height:150px" />
									<?php } ?>
								</div><br>

							<div class="col-md-12">
								<div class="form-group">
									<label class="form-label mb-0" > Technology Domain <em style="color: red;">*</em></label>
									<!-- <div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dignissim enim sed nunc dapibus iaculis. Phasellus a massa at massa aliquam lacinia. Sed iaculis vulputate eros quis porttitor.</p>
									</div> -->
									<div class="boderBox65x">
										<select class="form-control select2_common" name="technology_ids[]" id="technology_ids" data-placeholder="Technology Domain" multiple onchange="show_hide_technology_domain_other()">
											<?php if(count($technology_data) > 0)
												{	
													foreach($technology_data as $res)
													{	
														$technology_ids_arr = array();
														if($mode == 'Add') { if(set_value('technology_ids[]') != "") { $technology_ids_arr = set_value('technology_ids[]'); } }
													else { $technology_ids_arr = explode(",",$form_data[0]['technology_ids']); } ?>
													<option data-id='<?php echo $res['technology_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$technology_ids_arr)) { echo 'selected'; } ?> ><?php echo $res['technology_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										<?php if($technology_ids_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $technology_ids_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" id="blog_technology_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Technology Domain Other <em style="color: red;">*</em></label>
									<input type="text" class="form-control" name="technology_ids_other" id="technology_ids_other" value="<?php if($mode == 'Add') { echo set_value('technology_ids_other'); } else { echo $form_data[0]['technology_other']; } ?>">
									
									<?php if(form_error('technology_ids_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('technology_ids_other'); ?></label> <?php } ?>
								</div>
							</div>


							<div class="col-md-12">
								<div class="form-group">
									<label class="form-label mb-0" >Repository Type <em style="color: red;">*</em></label>
									<!-- <div class="guideline-tooltip"><i class="fa fa-info-circle fa-lg"></i>
										 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dignissim enim sed nunc dapibus iaculis. Phasellus a massa at massa aliquam lacinia. Sed iaculis vulputate eros quis porttitor.</p>
									</div> -->
									<div class="boderBox65x">
										<select class="form-control select2_common" name="kr_type" id="kr_type" data-placeholder="Knowledge Repository Type"  onchange="show_hide_kr_type_other()">
											<?php if(count($type_data) > 0)
												{	
													foreach($type_data as $res)
													{	
														$kr_type_arr = array();
														if($mode == 'Add') { if(set_value('kr_type[]') != "") { $kr_type_arr = set_value('kr_type[]'); } }
													else { $kr_type_arr = explode(",",$form_data[0]['kr_type']); } ?>
													<option data-id='<?php echo $res['type_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$kr_type_arr)) { echo 'selected'; } ?> ><?php echo $res['type_name']; ?></option>
													<?php }
												} ?>
										</select> 	
										<?php if($kr_type_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $kr_type_error; ?></label> <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-12" id="kr_type_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<label class="form-control-placeholder floatinglabel">Other Repository Type <em style="color: red;">*</em></label>
									<input type="text" class="form-control" name="kr_type_other" id="kr_type_other" value="<?php if($mode == 'Add') { echo set_value('kr_type_other'); } else { echo $form_data[0]['kr_type_other']; } ?>">
									
									<?php if(form_error('kr_type_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('kr_type_other'); ?></label> <?php } ?>
								</div>
							</div>
				



								
								<div class="form-group">
									<label>Brief about the content up to 400 words​ <em style="color: red;">*</em></label>
									<textarea name="kr_description" id="kr_description" class="form-control" cols="12" rows="4"><?php echo $form_data[0]['kr_description']; ?></textarea>
									<?php if(form_error('kr_description')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('kr_description'); ?></label> <?php } ?>									
								</div>

							<div class="col-md-12">
							<div class="form-group">	
							<div class="form-check form_check_custum" style="padding-left: 0px">
								<input type="hidden" name="terms_condition_check" value="0">
							    <input type="checkbox" name="terms_condition_check" id="terms_condition_check" class="terms_condition_check"  value="1" <?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){echo 'checked';} ?> />
							    <label class="form-check-label " for="exampleCheck1"><strong>Content Providers Terms and Condition</strong></label>
							  </div>
							  </div>
							
							</div>
						<?php if($mode == 'Update' && $form_data[0]['terms_condition_check']==1){
							$terms_condition_display= '';
						}else{
							$terms_condition_display= 'd-none';
						} ?>

						<div class="terms_condition_row <?php echo $terms_condition_display ?>">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="terms_condition" id="terms_condition" value="<?php if($mode == 'Add') { echo set_value('terms_condition'); } else { echo $form_data[0]['terms_condition']; } ?>">
									<label class="form-control-placeholder floatinglabel">Terms & Conditions<em style="color: red;">*</em></label>
									<?php if(form_error('terms_condition')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('terms_condition'); ?></label> <?php } ?>
								</div>
							</div>
						</div>

						
															
								
								
								
							
								
								<div class="form-group">
									<label class="form-label">Tags<em style="color: red;">*</em></label>
									<div class="boderBox65x">
										<select class="choose-tech select2" name="tags[]" id="tags" data-placeholder="Tags" multiple style="width: 100%;" onchange="show_hide_tag_other()">
											<?php if(count($tag_data) > 0)
												{	
													foreach($tag_data as $res)
													{	
														$tags_arr = explode(",",$form_data[0]['tags']); ?>
													<option data-id='<?php echo $res['tag_name'] ?>' value="<?php echo $res['id']; ?>" <?php if(in_array($res['id'],$tags_arr)) { echo 'selected'; } ?> ><?php echo $res['tag_name']; ?></option>
													<?php }
												} ?>
										</select> 
										<?php if($tags_error!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo $tags_error; ?></label> <?php } ?>
									</div>
								</div>
								
								<div class="col-md-12" id="tag_other_outer" style="margin-top:32px;">
								<div class="form-group">
									<label class="form-label">Other Tag <em style="color: red;">*</em></label>
									<input type="text" class="form-control" name="tag_other" id="tag_other" value="<?php echo $form_data[0]['tag_other']; ?>">
									<?php if(form_error('tag_other')!=""){ ?> <div class="clearfix"></div><label class="error"><?php echo form_error('tag_other'); ?></label> <?php } ?>
								</div>
								</div>

								<div class="form-group">
								<label class="form-label">Files to be Published <em style="color: red;">*</em></label>
								<div class="file-details" style="min-height:100px;">
									<?php /* <div class="form-group"><label class="form-control-placeholder"></label></div><br> */ ?>
									<?php if($mode == 'Update' && count($kr_files_data) > 0)
									{	
										echo '<div class="row">';
										foreach($kr_files_data as $res)
										{	?>
											<div class="col-md-2" id="kr_files_outer<?php echo $res['kr_id']."_".$res['file_id']; ?>">
												<div class="file-list">
														<a href="javascript:void(0)" onclick="remove_kr_file('<?php echo $res['kr_id']."_".$res['file_id']; ?>','<?php echo $encrypt_obj->encrypt($res['file_id']); ?>', 'public')" class="file-close">X</a>
														<a class="file_ext_title" href="<?php echo base_url().'uploads/kr_files/'.$res['file_name']; ?>" target="_blank">
															<?php $img_ext_arr = array('jpg', 'jpeg', 'png', 'gif');
															if(in_array(strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION)), $img_ext_arr)) { $disp_img_name = 'uploads/kr_files/'.$res['file_name']; }
															else { $disp_img_name = ''; } ?>
															
															<?php if($disp_img_name != "") { ?><img src="<?php echo base_url().$disp_img_name; ?>" style='max-width: 100%;max-height: 100%;'><?php }
															else { echo "<h4>".strtolower(pathinfo($res['file_name'], PATHINFO_EXTENSION))."</h4>"; } ?>
															<!--span><i class="fa fa-file"></i></span> View-->
														</a>
												</div>
											</div>
							<?php	}
										echo '</div>';
									}	?>
									
									<input type="hidden" name="customFileCount" id="customFileCount" value="0">
									<div class="row">											
										<div id="last_team_file_id"></div>
										<div class="col-md-2 custom-file" id="addFileBtnOuter">
											<label for="file-upload" class="custom-file-upload mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>
											<input type="file" class="form-control fileUploadKr" name="kr_files[]" id="file-upload" />
										</div>
									</div>
								</div>
								</div>
							
								
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
						</div>
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>



<script type="text/javascript">	

	$("#terms_condition_check").change(function () {

	    if ($('input.terms_condition_check').is(':checked')) {
	    	$(".terms_condition_row").removeClass('d-none');
	    }else{
	    	$("#terms_condition").val("");
	    	$(".terms_condition_row").addClass('d-none');
	    }

	});
	// CKEDITOR.replace('blog_description');
	
	function enable_disable_inputs(checkbox_id, input_id)
	{
		if($("#"+checkbox_id).prop("checked") == true)
		{
			$("#"+input_id).prop("disabled", false);
			$("#"+input_id).prop("readonly", false);
			$("#"+input_id).prop("required", true);
			$("#"+input_id).focus();
			
			if(input_id == 'author_professional_status' || input_id == 'author_org_name')
			{
				$("#preloader-loader").css("display", "block");
				parameters = { 'checkbox_id':checkbox_id, 'input_id':input_id, 'csrf_test_name':$('.token').val() }
				$.ajax(
				{
					type: "POST",
					url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
					data: parameters,
					cache: false,
					dataType: 'JSON',
					success:function(data)
					{
						if(data.flag == "success")
						{ 
							$(".token").val(data.token);
							if(input_id == 'author_professional_status') { $("#author_professional_status").val(data.professional_status); }
							if(input_id == 'author_org_name') { $("#author_org_name").val(data.org_name); }						
							$("#preloader-loader").css("display", "none");
						}
						else 
						{	}
					}
				});
			}
		}
		else if($("#"+checkbox_id).prop("checked") == false)
		{
			$("#"+input_id).prop("disabled", true);
			$("#"+input_id).prop("readonly", true);
			$("#"+input_id).prop("required", false);
			//$("#"+input_id).val("");
		}
	}
	
	function get_author_name()
	{
		$("#preloader-loader").css("display", "block");
		parameters = { 'input_id':'author_name', 'csrf_test_name':$('.token').val() }
		$.ajax(
		{
			type: "POST",
			url: "<?php echo site_url('blogs_technology_wall/get_autor_details'); ?>",
			data: parameters,
			cache: false,
			dataType: 'JSON',
			success:function(data)
			{
				if(data.flag == "success")
				{ 
					$(".token").val(data.token);
					$("#author_name").val(data.author_name);					
					$("#preloader-loader").css("display", "none");
				}
				else 
				{	}
			}
		});
	}
</script>

<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">	
	$(".allowd_only_float").keypress(function (e) 
	{
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) 
		{
			// let it happen, don't do anything
			return;
		}
		
		// Ensure that it is a number and stop the keypress
		if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
			e.preventDefault();
		}
	});
	
	//JQUERY VALIDATIONS
	$(document).ready(function () 
	{			
		$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
		
		// Words MIN Count Validation
		$.validator.addMethod("minCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count >= params[0]) { return true; }
		}, jQuery.validator.format("Minimum {0} Words Required"));
		
		// Words MAX Count Validation
		$.validator.addMethod("maxCount", function(value, element, params) 
		{
			var count = getWordCount(value);
			if(count <= params[0]) { return true; }
		}, jQuery.validator.format("Maximum {0} words are allowed."));
		
		// Image Type Validation .png, jpeg, jpg etc.
		$.validator.addMethod("valid_img_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".gif");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod("valid_files_format", function(value, element) 
		{ 
			if(value != "")
			{
				var validExts = new Array(".png", ".jpeg", ".jpg", ".pdf",".xls",".doc",".csv",".xlsx",".pptx",".ppt");
				var fileExt = value.toLowerCase();
				fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
				if (validExts.indexOf(fileExt) < 0)  { return false; } else return true;
			}else return true;
		});

		$.validator.addMethod('custum_required', function (value, element, param) { 
			var customFileCount = $("#customFileCount").val();
			// alert(customFileCount)
			if(customFileCount>0){
				return true;
			}else{
				return false;
			}
		}, 'This field is required');
		
		
		// File Size Validation
		$.validator.addMethod('filesize', function (value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than 2 MB');
		
		$.validator.addMethod("valid_value", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9_\- \ ]+$/i.test(value);
		}, "Title must contain only letters, numbers, or dashes.");
				
		//******* JQUERY VALIDATION *********
		$("#blogform").validate( 
		{
			/* onkeyup: false, 
			ignore: ":disabled",*/
			ignore: [], // For Ckeditor
			debug: false, // For Ckeditor
			rules:
			{
				title_of_the_content: { required: true, nowhitespace: true, valid_value: true },
				kr_description: 
				{
					required: true,
					minCount:['5'],
					maxCount:['400']
				},
				kr_banner: { <?php if($mode == 'Add') { ?>required: true,<?php } ?> valid_img_format: true,filesize:2000000},
				"technology_ids[]": { required: true},
				"tags[]": { required: true},
				"kr_files[]": { custum_required: true},
				author_name: { required: true, nowhitespace: true },
				accept_terms: { required: true},
				terms_condition: {
	               required: function() {
	               return $("input[name=terms_condition_check]:checked").val() == 1; 
	               }
               
              	},
			},
			messages:
			{
				title_of_the_content: { required: "This field is required", nowhitespace: "Please enter the title" },
				kr_description: { required: "This field is required", nowhitespace: "Please enter the description" },				
				kr_banner:{required: "This field is required",valid_img_format: "Please upload only .jpg, .png, .jpeg format images"},
				"technology_ids[]":{required: "This field is required"},
				"tags[]":{required: "This field is required"},
				"kr_files[]":{required: "This field is required"},
				author_name: { required: "This field is required", nowhitespace: "Please enter the title" },
				accept_terms:{required: "This field is required"},
				"kr_files[]":{required:"This field is required"}
								
				/* start_time:{required: "This field is required"},
					end_time:{required: "This field is required"},
					cost_type:{required: "This field is required"},					
					cost_price:{required: "This field is required"},
					exclusive_technovuus_event:{required: "This field is required"},
					registration_link:{required: "This field is required", nowhitespace: "This field is required"},
					hosting_link:{required: "This field is required", nowhitespace: "This field is required"},
					breif_desc:{required: "This field is required", nowhitespace: "This field is required"},
					key_points:{required: "This field is required", nowhitespace: "This field is required"},
				about_author:{required: "This field is required", nowhitespace: "This field is required"}		 */					
			},			
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}			
		});
	});
	
	function show_hide_blog_technology_other()
	{
		$("#kr_type_other_outer").hide();
		
		var disp_other_flg = 0;
		var selected = $('#kr_type').select2("data");
		for (var i = 0; i <= selected.length-1; i++) 
		{
			if((selected[i].text).toLowerCase() == 'other')
			{
				disp_other_flg = 1;
				break;
			}
		}
		
		if(disp_other_flg == 1)
		{
			$("#kr_type_other_outer").show();
			$("#kr_type_other").prop("required", true);
		}
		else
		{
			$("#kr_type_other_outer").hide();
			$("#kr_type_other").prop("required", false);
			$("#kr_type_other").val('');
		}
	}
	//show_hide_blog_technology_other();


		//DISPLAY PUBLIC TEAM FILE PREVIEW WHEN BROWSE
	function FilesPreview(input, public_private_flag) 
	{
		console.log(public_private_flag);
		if (input.files && input.files[0]) 
		{
			$("#preloader").css("display", "block");
			
			if(public_private_flag == 'public')
			{			
				var customFileCount = $("#customFileCount").val();
				var disp_filename_final = '';
				var disp_filename = input.files[0].name;
				var reader = new FileReader();
				var j = customFileCount;
			}
			
			var upload_file_size = input.files[0].size; //1000000 Bytes = 1 Mb 
			if(upload_file_size < 10000000)
			{
				var disp_img_name = "";
				var extension = disp_filename.substr( (disp_filename.lastIndexOf('.') +1) ).toLowerCase();
				disp_filename_final = "<h4 style='line-height:50px;'>"+extension+"</h4>"; 
					
				var id_name = '';
				var btn_id_name = '';
				var input_cls_name = '';
				var input_type_name = '';
				var fun_var_parameter = "";
				var file_upload_common = '';
				if(public_private_flag == 'public')
				{
					id_name = 'kr_files_outer'+j;
					btn_id_name = 'addFileBtnOuter';
					input_cls_name = 'fileUploadKr';
					input_type_name = 'kr_files[]';
					fun_var_parameter = "'public'";
					file_upload_common = 'file-upload';
				}
			
				
				var append_str = '';
				append_str += '	<div class="col-md-2 kr_files_outer_common" id="'+id_name+'">';
				append_str += '		<div class="file-list">';
				append_str += '			<a href="javascript:void(0)" onclick="remove_kr_file('+j+', 0, '+fun_var_parameter+')" class="file-close">X</a>';
				//append_str += '			<span><i class="fa fa-file"></i></span> '+disp_filename;
				append_str += '			<div class="file_ext_title">'+disp_filename_final+disp_filename.substring(0,30)+'</div>';
				append_str += '		</div>';
				append_str += '	</div>';				
					
				var btn_str = '';
				btn_str += '	<div class="col-md-2 custom-file" id="'+btn_id_name+'">';
				btn_str +=	'		<label for="'+file_upload_common+'" class="custom-'+file_upload_common+' mt-3 btn btn-primary"><i class="fa fa-plus-circle"></i> Add File</label>';
				btn_str +=	'		<input type="file" class="form-control '+input_cls_name+'" onchange="FilesPreview(this, '+fun_var_parameter+')" name="'+input_type_name+'" id="'+file_upload_common+'" />';
				btn_str +=	'	</div>';
					
				if(public_private_flag == 'public')
				{
				$("#addFileBtnOuter").addClass('d-none');
				$("#addFileBtnOuter").addClass('btnOuterForDel'+j+'');
				$("#addFileBtnOuter").removeAttr( "id" );
					
				$(append_str).insertBefore("#last_team_file_id");
				$(btn_str).insertAfter("#last_team_file_id");
				$("#customFileCount").val(parseInt(customFileCount)+1);
				}
				
			}
			else
			{
				// alert("Files should be less than 10 MB")
				 swal( 'Error!','File size should be less than 10 MB','error');
			}
			
			$("#preloader").css("display", "none");
		}
	}
	$(".fileUploadKr").change(function() { FilesPreview(this,'public'); });	


	function remove_kr_file(div_id, file_id, public_private_flag)
	{
		swal(
		{
			title:"DELETE?" ,
			text: "Are you sure you want to delete the file?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(function (result) 
		{
			if (result.value) 
			{
				$("#preloader").css("display", "block");
				
				if(file_id!= '' && file_id != '0')
				{
					<?php if($mode == 'Update') { ?>
					var csrf_test_name = $("#csrf_token").val();					
					var data = { 'file_id': encodeURIComponent($.trim(file_id)), 'csrf_test_name': encodeURIComponent($.trim(csrf_test_name)) };          
					$.ajax(
					{ 
							type: "POST", 
							url: '<?php echo site_url("xAdmin/knowledge_repository/delete_kr_file_ajax") ?>', 
							data: data, 
							dataType: 'JSON',
							success:function(data) 
							{ 
									$("#csrf_token").val(data.csrf_new_token);															
									/* swal({ title: "Success", text: 'File successfully deleted', type: "success" }); */
							}
					});
				<?php } ?>
				}
				
				if(public_private_flag == 'public')
				{
				$("#kr_files_outer"+div_id).remove();
				$(".btnOuterForDel"+div_id).remove();
				}
				
				
				$("#preloader").css("display", "none");
			}
		});
	}
</script>