<!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url(); ?>assets/front/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Email Template</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Email Template</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add Email Template
			  </h3>
				  <a href="<?php echo base_url('xAdmin/email_master') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="emailfrm" name="emailfrm" role="form" >
						  <div class="row">
							<div class="col-12">							   
							  <div class="form-group">
								<label for="exampleInputEmail1">Email Subject</label>
								<input type="text" class="form-control" id="email_title" placeholder="Enter Email Title" name="email_title" maxlength="200">
								<span><?php echo form_error('email_title'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Email Body</label>
								<textarea id="email_description" name="email_description" class="form-control ckeditor" placeholder="Enter Email Description"></textarea>
								<script>
									CKEDITOR.replace('email_description');
								</script>								
								<span><?php echo form_error('email_description'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Slug</label>
								<input type="text" class="form-control" id="slug" placeholder="Enter Email Slug" name="slug" maxlength="100">
								<span><?php echo form_error('slug'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">From Email ID</label>
								<input type="email" class="form-control" id="from_emai" placeholder="Enter Email ID" name="from_emai" maxlength="300">
								<span><?php echo form_error('from_emai'); ?></span>
							  </div>
							  <!--<div class="form-group">
								<label for="exampleInputEmail1">Another Email ID</label>
								<input type="email" class="form-control" id="cc_email_id" placeholder="Enter Email ID" name="cc_email_id" maxlength="300">
								<span><?php //echo form_error('cc_email_id'); ?></span>
							  </div>-->	
								<div class="card-footer1">
									<button type="submit" class="btn btn-primary">ADD</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  $('#emailfrm').validate({
	 ignore: [],
     debug: false, 
    rules: {
      email_title: {
        required: true,
		minlength:5
      },
	  email_description: {
        required: true
      },
	  from_emai: {
        required: true
      },
	  slug: {
        required: true
      }
    },
    messages: {
      email_title: {
        required: "This field is required",
		minlength: "Enter Page Title must be at least {0} characters long"
      },
	  email_description: {
			required: function() 
					{
					 CKEDITOR.instances.email_description.updateElement();
					},
					minlength:10
      },
	  from_emai: {
        required: "This field is required"
      },
	  slug: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


