<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Task Setting</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Task Setting</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Task Setting
			  </h3>
				 <!-- <a href="<?php echo base_url('xAdmin/task_setting/add') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>-->
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				    <?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Success!</h5>
					 <?php echo $this->session->flashdata('success'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="taskfrm" name="taskfrm" role="form" >
						  <div class="row">
							<div class="col-6">							 
							  <div class="form-group">
								<label for="exampleInputEmail1">Minimum Slot <em class="error">*</em></label>
								<input type="text" class="form-control" id="min_no" placeholder="Enter Minimum Slot" value="<?php echo $task_data[0]['min_no']; ?>" name="min_no" >
								<span><?php echo form_error('min_no'); ?></span>
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1">Maximum Slot <em class="error">*</em></label>
								<input type="text" class="form-control" id="max_no" placeholder="Enter Maximum Slot" name="max_no" value="<?php echo $task_data[0]['max_no']; ?>" >
								<span><?php echo form_error('max_no'); ?></span>
							  </div>

								<div class="card-footer">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });  
  
  $.validator.addMethod("numberNotStartWithZero", function(value, element) { 
		return this.optional(element) || /^[1-9][0-9]+$/i.test(value); 
		//return this.optional(element) || /^[0]$/i.test(value);
	}, "Please enter a valid number. (Do not start with zero)");

$.validator.addMethod("checkMax", function(value, element) 
{ 
	var maxVal = value;
	var minVal = parseInt($("#min_no").val());
	if(maxVal != "")
	{
		if(maxVal < minVal){return false;}else {return true;}
		
	} else return true;
}, "The 'Max No' should be greater than the 'Min No'");
  
  $('#taskfrm').validate({
    rules: {
      min_no: {
        required: true,
		digits: true		
      },
	  max_no: {
        required: true,
		digits: true,
		checkMax: true
      }
    },
    messages: {
     min_no: {
        required: "This field is required"
      },
	  max_no: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


