<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">User Actions</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Actions</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				 Edit User Actions For <b>"<?php echo $user_type_name; ?>"</b>
			  </h3>
				<a href="<?php echo base_url('xAdmin/usertype') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a> 
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Success!</h5>
					 <?php echo $this->session->flashdata('success'); ?>
					</div>
				   <?php } 
				    $explode = explode(",", $added_values[0]['sub_id']);
					$sel_ids = array();
					foreach($explode as $selval){						
						array_push($sel_ids, $selval);
					}					
				   ?>
				<div class="card-body">
					 <form method="post" id="actionfrm" name="actionfrm" role="form" >
						  <div class="row">							
							<div class="col-12">
							<?php 
							foreach($categories_data as $catname){
								
								foreach($catname as $key => $subcat){
									
									?>
									<h3><?php echo $key; ?></h3>
									<?php
									foreach($subcat as $subKey => $subCatname){										
										?>
										<div class="form-check" style="margin-left: 50px;">
										  <input class="form-check-input chBox" type="checkbox" name="sub_cat_id[]" id="sub_cat_id"  value="<?php echo $subKey ?>" <?php if(in_array($subKey, $sel_ids)){ echo 'checked'; } ?>>
										    <label class="form-check-label"><?php echo $subCatname."( ID: ".$subKey.")"; ?></label>
										</div>
										<?php 
										
									} // Subcategory Name End
									
									echo "<hr>";
									
								} // Category Name Foreach End
								
							} // Main Foreach end
							
							?>							 
								<span><?php echo form_error('sub_cat_id'); ?></span>
								</div>							 						  
								<div class="card-footer">
									<button type="submit" id="btn_val" class="btn btn-primary">Submit</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" id="csrf" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
	  form.submit();
    }
  });
  
  
  /*$("#btn_val").click(function () {
		if ($('.chBox  :checked').length < 1) {
			alert('Please Select atleast one CheckBox');
			return false;
		}
		return false;
	});*/
  
  $('#actionfrm').validate({
    rules: {
	  "sub_cat_id[]": {
        required: true
      }
    },
    messages: {
	sub_cat_id: {
        required: "This field is required"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  
  
  $('#category_id').change(function(){
    var cat_id = $(this).val();
	var hashValue = $('#csrf').val();
    $.ajax({
		 url:'<?php echo base_url('xAdmin/useractions/get_actions')?>',
		 method: 'post',
		 data: {category_id: cat_id, '<?php echo $this->security->get_csrf_token_name(); ?>':hashValue},
		 //dataType: 'json',
		 success: function(response){
		   $('#permission').html(response);        
		 }
	   });   
   }); // Category End
  
});
</script>


